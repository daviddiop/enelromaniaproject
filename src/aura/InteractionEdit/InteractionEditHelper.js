({
    getIndexInList: function (accountList, customerInteraction) {
        return accountList.findIndex(function (account) {
            if (account.id === customerInteraction.id) {
                return account;
            }
        });
    },
    insertInteraction: function (component, callback) {
        var self = this;
        component.find('apexService').builder()
            .setMethod('insertInteraction')
            .setResolve(function (interaction) {
                if (interaction) {
                    if (callback) {
                        callback(interaction);
                    } else {
                        component.set('v.interaction', interaction);
                        component.set('v.recordId', interaction.id);
                        if (interaction.id) {
                            component.find('cookieSvc').setInteractionId(interaction.id);
                            self.updateUrl(component, interaction.id);
                        }
                    }
                }
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    upsertInteraction: function (component, interaction, callback) {
        component.find('apexService').builder()
            .setMethod('upsertInteraction')
            .setInput(interaction)
            .setResolve(function (updatedInteraction) {
                if (callback) {
                    callback(updatedInteraction);
                } else {
                    component.set('v.interaction', updatedInteraction);
                }
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    removeInterlocutor: function (component, interactionId, callback) {
        component.find('apexService').builder()
            .setMethod('removeInterlocutor')
            .setInput({
                'interactionId': interactionId
            })
            .setResolve(function (interaction) {
                if (callback) {
                    callback(interaction);
                } else {
                    component.set('v.interaction', interaction);
                }
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    findById: function (component, interactionId, callback) {
        component.find('apexService').builder()
            .setMethod('findById')
            .setInput({
                'interactionId': interactionId
            })
            .setResolve(function (interaction) {
                if (interaction) {
                    if (callback) {
                        callback(interaction);
                    } else {
                        component.set('v.interaction', interaction);
                    }
                }
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    refreshAccountForm: function (component, helper) {
        var interaction = component.get('v.interaction');
        this.findById(component, interaction.id, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.set('v.rendered', false);
        });
    },
    goToInteractionView: function (component) {
        var thisPageReference = component.get("v.pageReference");
        var interactionId = component.get("v.recordId") || thisPageReference.state.c__id;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: interactionId,
                objectApiName: 'Interaction__c',
                actionName: "view"

            }
        };
        navService.navigate(pageReference);
    },
    updateUrl: function (component, interactionId) {
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__InteractionEdit',
            },
            state: {
                "c__id": interactionId
            }
        };
        navService.navigate(pageReference, true);
    },
    openModal: function (component) {
        component.set('v.rendered', true);
    },
    closeModal: function (component) {
        component.set('v.interlocutorFields', null);
        component.set('v.customerFields', null);
        component.set('v.rendered', false);
    },
    switchOffIndividualBackBtn: function (component) {
        var isHide = component.get('v.individualEditBtnHide');
        if (isHide === true) {
            component.set('v.individualEditBtnHide', false);
        }
    }
})