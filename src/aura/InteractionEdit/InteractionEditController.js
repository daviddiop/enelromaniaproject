({
    onInit: function (component, event, helper) {
        var thisPageReference = component.get("v.pageReference");
        var interactionId = component.get("v.recordId") || thisPageReference.state.c__id;
        if (interactionId) {
            helper.findById(component, interactionId, function (interaction) {
                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function (response) {
                        let focusedTabId = response.tabId;
                        workspaceAPI.setTabLabel({
                            tabId: focusedTabId,
                            label: interaction.name
                        });
                    });
                component.set('v.interaction', interaction);
                if (!interaction.isClosed) {
                    component.find('cookieSvc').setInteractionId(interaction.id);
                } else {
                    component.find('cookieSvc').clearInteractionId();
                }
            });
        } else {
            helper.insertInteraction(component);
        }
    },
    onPageReferenceChanged: function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    onInterlocutorSelect: function (component, event, helper) {
        var interlocutor = event.getParam('individual');
        var interaction = component.get('v.interaction');
        interaction.interlocutorId = interlocutor.Id;
        helper.upsertInteraction(component, interaction, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            if (updatedInteraction.customerInteractionIds && updatedInteraction.customerInteractionIds.length) {
                helper.closeModal(component);
            } else {
                component.set('v.currentComponent', 'individualRelations');
            }
        });
    },
    onInterlocutorSearch: function (component, event, helper) {
        component.set('v.interlocutorFields', event.getParam('searchFields'));
        component.set('v.interlocutorList', event.getParam('individualList'));
        component.set('v.currentComponent', 'individualList');
    },
    interlocutorRemoveHandler: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        if (interaction && !interaction.isClosed) {
            var thisPageReference = component.get("v.pageReference");
            var interactionId = component.get("v.recordId") || thisPageReference.state.c__id;
            helper.removeInterlocutor(component, interactionId, function (interaction) {
                component.set('v.interaction', interaction);
                component.set('v.currentComponent', 'individualSearch');
                helper.closeModal(component);
            });
        }
    },
    searchHandler: function (component, event, helper) {
        component.set('v.customerFields', event.getParam('account'));
        component.set('v.customerList', event.getParam('accountList'));
        component.set('v.currentComponent', 'accountList');
    },
    handleAccountSelection: function (component, event, helper) {
        var accountId = event.getParam('accountId');
        var customerInteractionId = event.getParam('customerInteractionId');
        var interlocutorId = event.getParam('interlocutorId');
        if (accountId) {
            if (interlocutorId) {
                var interaction = component.get('v.interaction');
                helper.findById(component, interaction.id, function (updatedInteraction) {
                    component.set('v.interaction', updatedInteraction);
                    helper.closeModal(component);
                });
            } else {
                component.set('v.data', {
                    'customerId': accountId,
                    'customerInteractionId': customerInteractionId
                });
                component.set('v.currentComponent', 'accountRelations');
            }
        }
    },
    onRelationForIndividualSelect: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        helper.findById(component, interaction.id, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.set('v.currentComponent', 'individualSearch');
            helper.closeModal(component);
        });
    },
    onRelationForAccountSelect: function (component, event, helper) {
        component.set('v.customerFields', null);
        helper.refreshAccountForm(component, helper);
    },
    refreshAndCloseModal: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        helper.findById(component, interaction.id, function (interaction) {
            component.set('v.interaction', interaction);
            helper.closeModal(component);
        });
    },
    refresh: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        helper.findById(component, interaction.id);
    },
    goToNewAccount: function (component, event, helper) {
        component.set('v.currentComponent', 'accountEdit');
    },
    onEditComment: function (component, event, helper) {
        component.set('v.isEdit', true);
    },
    onSaveComment: function (component, event, helper) {
        component.find('interactionEditForm').submit();
    },
    onCancelCommentEdit: function (component, event, helper) {
        component.set('v.isEdit', false);
        var interaction = component.get('v.interaction');
        component.set('v.interaction', interaction);
    },
    onCloseInteraction: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        interaction.status = 'Closed';
        helper.upsertInteraction(component, interaction, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.find('cookieSvc').clearInteractionId();
            helper.goToInteractionView(component);
        });
    },
    onCancelInteraction: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        interaction.status = 'Canceled';
        helper.upsertInteraction(component, interaction, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.find('cookieSvc').clearInteractionId();
            helper.goToInteractionView(component);
        });
    },
    handleSuccess: function (component, event, helper) {
        component.set('v.isEdit', false);
    },
    upgradeInterlocutor: function (component, event, helper) {
        component.set('v.currentComponent', 'individualEdit');
        var interaction = component.get('v.interaction');
        var searchFields = {
            FirstName: interaction.interlocutorFirstname,
            LastName: interaction.interlocutorLastname,
            NationalIdentityNumber: interaction.interlocutorNationalId,
            Email: interaction.interlocutorEmail,
            Phone: interaction.interlocutorPhone
        };
        component.set('v.interlocutorFields', searchFields);
        component.set('v.individualEditBtnHide', true);
        helper.openModal(component);
    },
    openInterlocutorModal: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        if (interaction && !interaction.isClosed) {
            component.set('v.currentComponent', 'individualSearch');
            helper.switchOffIndividualBackBtn(component);
            helper.openModal(component);
        }
    },
    openAccountModal: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        if (interaction && !interaction.isClosed) {
            component.set('v.currentComponent', 'accountSearch');
            helper.switchOffIndividualBackBtn(component);
            helper.openModal(component);
        }
    },
    redirectHandler: function (component, event, helper) {
        var page = event.getParam('page');
        if (page) {
            component.set('v.currentComponent', page);
        }
    },
    close: function (component, event, helper) {
        var interaction = component.get('v.interaction');
        helper.findById(component, interaction.id);
        helper.closeModal(component);
    }
})