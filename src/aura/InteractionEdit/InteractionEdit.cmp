<aura:component implements="lightning:isUrlAddressable,force:hasRecordId,lightning:actionOverride">
    <aura:attribute name="recordId" type="String" />
    <aura:attribute name="interaction" type="Map" access="public" />

    <aura:attribute name="interlocutorList" type="List" access="public"/>
    <aura:attribute name="interlocutorFields" type="Map" access="public"/>

    <aura:attribute name="customerList" type="List" access="public"/>
    <aura:attribute name="customerFields" type="Map" access="public" />

    <aura:attribute name="rendered" type="Boolean" default="false"/>
    <aura:attribute name="currentComponent" type="String" access="private" />
    <aura:attribute name="data" type="Map" default="{}" access="private" />
    <aura:attribute name="isEdit" type="Boolean" default="false" />
    <aura:attribute name="individualEditBtnHide" type="Boolean" default="false" />

    <aura:attribute name="isUndentifiedInterlocutor" type="Boolean"
                    default="{!and(v.interaction.isSaveUnIdentifiedInterlocutorAllowed, or(v.interaction.interlocutorFirstname, v.interaction.interlocutorLAstname))}"/>

    <aura:handler name="init" value="{!this}" action="{!c.onInit}" />
    <aura:handler name="change" value="{!v.pageReference}" action="{!c.onPageReferenceChanged}" />

    <c:ApexServiceLibrary aura:id="apexService" controller="InteractionCnt" />
    <lightning:navigation aura:id="navService" />
    <lightning:workspaceAPI aura:id="workspace" />
    <lightning:notificationsLibrary aura:id="ntfLib" />
    <c:notificationSvc aura:id="ntfSvc"/>
    <c:cookieSvc aura:id="cookieSvc" />

    <div class="slds-m-bottom--small">
        <div class="slds-page-header slds-page-header_record-home">
            <div class="slds-page-header__row">
                <div class="slds-page-header__col-title">
                    <div class="slds-media">
                        <div class="slds-media__figure">
                            <lightning:icon iconName="custom:custom71"  />
                        </div>
                        <div class="slds-media__body">
                            <div class="slds-page-header__name">
                                <div class="slds-page-header__name-title">
                                    <h1>
                                        <span>{!$Label.c.Interaction}</span>
                                        <aura:if isTrue="{!v.interaction}">
                                            <lightning:formattedText class="slds-page-header__title slds-truncate" value="{!v.interaction.name}" title="{!v.interaction.name}" />
                                        </aura:if>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slds-page-header__col-actions">
                    <div class="slds-page-header__controls">
                        <div class="slds-page-header__control">
                            <lightning:buttonGroup>
                                <lightning:button label="{!$Label.c.Cancel}" onclick="{!c.onCancelInteraction}" disabled="{!v.interaction.isClosed}"/>
                                <lightning:button variant="brand" label="{!$Label.c.Close}" onclick="{!c.onCloseInteraction}" disabled="{!v.interaction.isClosed}"/>
                            </lightning:buttonGroup>
                        </div>
                    </div>
                </div>
            </div>
            <aura:if isTrue="{!v.interaction}">
                <lightning:recordEditForm aura:id="interactionEditForm" recordId="{!v.interaction.id}" objectApiName="Interaction__c" onsuccess="{!c.handleSuccess}">
                    <div class="slds-page-header__row slds-page-header__row_gutters">
                        <div class="slds-page-header__col-details">
                            <lightning:layout class="slds-page-header__detail-row">
                                <lightning:layoutItem size="2" class="slds-page-header__detail-block">
                                    <lightning:outputField fieldName="CreatedDate" variant="label-stacked"/>
                                </lightning:layoutItem>
                                <lightning:layoutItem size="2" class="slds-page-header__detail-block">
                                    <lightning:outputField fieldName="Channel__c" variant="label-stacked"/>
                                </lightning:layoutItem>
                                <lightning:layoutItem size="2" class="slds-page-header__detail-block">
                                    <lightning:outputField fieldName="Status__c" variant="label-stacked"/>
                                </lightning:layoutItem>
                                <lightning:layoutItem size="2" class="slds-page-header__detail-block">
                                    <aura:if isTrue="{!v.isEdit}">
                                        <lightning:inputField fieldName="Comments__c" variant="label-stacked"/>
                                        <div class="slds-float--right">
                                            <lightning:button variant="neutral" label="{!$Label.c.Cancel}" title="{!$Label.c.Cancel}"
                                                              onclick="{!c.onCancelCommentEdit}" disabled="{!v.interaction.isClosed}"/>
                                            <lightning:button variant="brand" label="{!$Label.c.Save}" title="{!$Label.c.Save}"
                                                              onclick="{!c.onSaveComment}" disabled="{!v.interaction.isClosed}"/>
                                        </div>
                                        <aura:set attribute="else">
                                            <lightning:buttonIcon iconName="utility:edit" variant="bare" onclick="{!c.onEditComment}"
                                                                  class="slds-float_right z-index-10" disabled="{!v.interaction.isClosed}"/>
                                            <lightning:outputField fieldName="Comments__c" variant="label-stacked"/>
                                        </aura:set>
                                    </aura:if>
                                </lightning:layoutItem>
                            </lightning:layout>
                        </div>
                    </div>
                </lightning:recordEditForm>
            </aura:if>
        </div>
    </div>
    <div class="slds-m-bottom--small">
        <lightning:card variant="Narrow" title="{!$Label.c.InterlocutorSearchAndSelection}">
            <div class="slds-p-horizontal_small content">
                <aura:if isTrue="{!or(v.interaction.interlocutor, v.isUndentifiedInterlocutor)}">
                    <div class="slds-box slds-box_x-small slds-media slds-m-around_xxx-small slds-theme_default">
                        <div class="slds-media w-100">
                            <div class="slds-media__figure slds-media__figure_fixed-width slds-align_absolute-center">
                                <aura:if isTrue="{!v.interaction.interlocutor}">
                                    <lightning:avatar src="" fallbackIconName="standard:avatar" variant="circle" size="medium" />
                                    <aura:set attribute="else">
                                        <lightning:helptext content="{!$Label.c.UnidentifiedInterlocutor}" iconName="standard:avatar_loading"
                                                            class="small-icon"/>
                                    </aura:set>
                                </aura:if>
                            </div>
                            <div class="slds-media__body slds-clearfix slds-border_left slds-border_right slds-p-left_small h-4rem">
                                <div class="slds-form__row">
                                    <div class="slds-form__item" role="listitem">
                                        <lightning:formattedText class="slds-welcome-mat__tile-title slds-text-heading_small slds-p-horizontal_xx-small"
                                                                 value="{!v.interaction.interlocutor ? v.interaction.interlocutor.name : v.interaction.interlocutorFullname}" />
                                    </div>
                                </div>
                                <aura:if isTrue="{!v.interaction.interlocutor}">
                                    <lightning:recordViewForm recordId="{!v.interaction.interlocutor.id}" objectApiName="Individual" class="w-100">
                                        <div class="slds-form__row">
                                            <div class="slds-form__item" role="listitem">
                                                <lightning:outputField class="slds-form-element__static" fieldName="NationalIdentityNumber__c" variant="label-stacked"/>
                                            </div>
                                        </div>
                                    </lightning:recordViewForm>
                                </aura:if>
                            </div>
                            <div class="slds-media__figure slds-media__figure_reverse slds-clearfix">
                                <div class="slds-align_absolute-center min-w-5rem h-4rem l-h-4rem">
                                    <aura:if isTrue="{!v.isUndentifiedInterlocutor}">
                                        <lightning:buttonIcon iconName="utility:upload" title="{!$Label.c.Upgrade}" class="slds-p-horizontal_xx-small brand-icon"
                                                              onclick="{!c.upgradeInterlocutor}" disabled="{!v.interaction.isClosed}" variant="bare-inverse"/>
                                    </aura:if>
                                    <lightning:buttonIcon iconName="utility:close" class="slds-p-horizontal_xx-small" onclick="{!c.interlocutorRemoveHandler}"
                                                          title="{!if(v.interaction.customerInteractionIds.length, $Label.c.InterlocutorRemoveDenied, $Label.c.Remove)}"
                                                          disabled="{!or(v.interaction.isClosed, v.interaction.customerInteractionIds.length)}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <aura:set attribute="else">
                        <aura:if isTrue="{!v.interaction.isClosed}">
                            <lightning:formattedText value="{!$Label.c.NoInterlocutorSelected}" class="slds-welcome-mat__tile-title slds-text-heading_small slds-p-left_large"/>
                            <aura:set attribute="else">
                                <div class="slds-box slds-box_x-small slds-media slds-m-around_xxx-small slds-theme_default">
                                    <div class="slds-media w-100">
                                        <div class="slds-media__figure slds-media__figure_fixed-width slds-align_absolute-center">
                                            <lightning:icon iconName="utility:add"/>
                                        </div>
                                        <div class="slds-media__body slds-clearfix slds-border_left slds-p-left_small h-4rem">
                                            <div class="slds-form__row slds-p-left_medium slds-p-top_x-small">
                                                <div class="slds-form__item" role="listitem">
                                                    <a onclick="{!c.openInterlocutorModal}" class="slds-text-link">
                                                        <lightning:formattedText value="{!$Label.c.SearchForInterlocutor}"
                                                                                 class="slds-welcome-mat__tile-title slds-text-heading_small"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="slds-form__row slds-p-left_medium slds-p-vertical_x-small">
                                                <div class="slds-form__item" role="listitem">
                                                    <lightning:formattedText value="{!$Label.c.NoInterlocutorSelected}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </aura:set>
                         </aura:if>
                    </aura:set>
                </aura:if>
            </div>
        </lightning:card>
    </div>
    <div class="slds-m-bottom--small">
        <lightning:card variant="Narrow" title="{!$Label.c.CustomerSearchAndSelection}" class="customer-list slds-scrollable_y">
            <div class="slds-p-horizontal_small content">
                <c:customerInteractionList aura:id="customerInteractionList" interactionId="{!v.interaction.id}" onnew="{!c.openAccountModal}"
                                             onremove="{!c.refresh}"/>
            </div>
        </lightning:card>
    </div>

    <aura:if isTrue="{!v.rendered}">
        <div>
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1"
                     class="slds-modal slds-fade-in-open slds-modal_medium">
                <div class="slds-modal__container">
                    <header class="slds-modal__header slds-modal__header_empty">
                        <lightning:buttonIcon title="{!$Label.c.Close}" iconName="utility:close" variant="bare-inverse"
                                              class="slds-modal__close" onclick="{!c.close}" />
                    </header>
                    <div class="slds-modal__content" id="modal-content-id-1">
                        <aura:if isTrue="{!v.currentComponent == 'individualSearch'}">
                            <c:individualSearch interactionId="{!v.interaction.id}" searchFields="{!v.interlocutorFields}" onsearch="{!c.onInterlocutorSearch}"/>
                        </aura:if>
                        <aura:if isTrue="{!v.currentComponent == 'individualList'}">
                            <c:individualList interlocutorList="{!v.interlocutorList}" interactionId="{!v.interaction.id}" searchFields="{!v.interlocutorFields}"
                                               onselect="{!c.onInterlocutorSelect}" onredirect="{!c.redirectHandler}"/>
                        </aura:if>
                        <aura:if isTrue="{!v.currentComponent == 'individualEdit'}">
                            <c:individualEdit interactionId="{!v.interaction.id}" isRedirectAllowed="false" searchFields="{!v.interlocutorFields}"
                                                onsuccess="{!c.refreshAndCloseModal}" onback="{!c.redirectHandler}" onduplicatefound="{!c.onInterlocutorSearch}"
                                                disableFilled="{!v.isUndentifiedInterlocutor}" hideCancel="true" hideBack="{!v.individualEditBtnHide}"/>
                        </aura:if>
                        <aura:if isTrue="{!v.currentComponent == 'individualRelations'}">
                            <c:accountContactRelationsList individualId="{!v.interaction.interlocutorId}" interactionId="{!v.interaction.id}"
                                                             onselect="{!c.onRelationForIndividualSelect}"
                                                             onredirect="{!c.redirectHandler}" isRedirectAllowed="false" />
                        </aura:if>
                        <aura:if isTrue="{!v.currentComponent == 'accountSearch'}">
                            <c:accountSearch interactionId="{!v.interaction.id}" onsearch="{!c.searchHandler}" searchFields="{!v.customerFields}"/>
                        </aura:if>
                        <aura:if isTrue="{!v.currentComponent == 'accountList'}">
                            <c:accountList accountList="{!v.customerList}" interactionId="{!v.interaction.id}" isRedirectAllowed="false"
                                             onnew="{!c.goToNewAccount}" onselection="{!c.handleAccountSelection}" onback="{!c.redirectHandler}" />
                        </aura:if>
                        <aura:if isTrue="{!v.currentComponent == 'accountEdit'}">
                            <c:accountEdit interactionId="{!v.interaction.id}" showRole="true" account="{!v.customerFields}" onsuccess="{!c.refreshAndCloseModal}"
                                             onback="{!c.redirectHandler}"/>
                        </aura:if>
                        <aura:if isTrue="{!v.currentComponent == 'accountRelations'}">
                            <c:accountContactRelationsList aura:id="acrl" customerId="{!v.data.customerId}" customerInteractionId="{!v.data.customerInteractionId}"
                                                           onselect="{!c.onRelationForAccountSelect}"
                                                           isRedirectAllowed="false" onredirect="{!c.redirectHandler}"/>
                        </aura:if>
                    </div>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
    </aura:if>
</aura:component>