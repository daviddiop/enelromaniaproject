/**
 * Created by Vlad Mocanu on 18/02/2020.
 */

({
    initialize: function (component) {
        let self = this;
        self.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let parentCaseId = myPageRef.state.c__caseId;
        let genericRequestDossierId = myPageRef.state.c__genericRequestDossierId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let interactionId = component.find('cookieSvc').getInteractionId();

        component.set("v.accountId", accountId);
        component.set("v.dossierId", dossierId);
        component.set("v.parentCaseId", parentCaseId);
        component.set("v.genericRequestDossierId", genericRequestDossierId);
        component.find('apexService').builder()
            .setMethod("initialize")
            .setInput({
                'accountId': accountId,
                'dossierId': dossierId,
                'parentCaseId' : parentCaseId,
                'genericRequestDossierId' : genericRequestDossierId,
                'interactionId': interactionId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                component.set('v.accountId', response.accountId);
                component.set('v.dossierId', response.dossierId);

                if (response.dossier) {
                    component.set("v.dossier", response.dossier);
                }

                if (response.caseTile) {
                    component.set('v.caseTile', response.caseTile);
                    component.set('v.caseTileLength', response.caseTile.length);
                }

                if (response.parentDossierId) {
                    component.set('v.parentDossierId', response.parentDossierId);
                }

                if (response.parentCaseSupplyId) {
                    component.set('v.supplyId', response.parentCaseSupplyId)
                    self.getContractId(component, response.parentCaseSupplyId);
                }

                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId, response.parentCaseId, response.parentDossierId);
                }
                component.set("v.step", 0);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = "Bailment Management | Salesforce";
                    if (!window.location.hash && component.get("v.step") === 1) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
        });
        document.title = "Bailment Management | Salesforce";
    },
    updateUrl: function (component, accountId, dossierId, parentCaseId, parentDossierId) {
        let navService = component.find("navService");
        let state = {
            "c__accountId": accountId,
            "c__dossierId": dossierId
        };
        if (parentCaseId){
            state["c__caseId"] = parentCaseId;
        }
        if (parentDossierId){
            state["c__parentDossierId"] = parentDossierId;
        }
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_BailmentManagementWizard',
            },
            state: state
        };
        navService.navigate(pageReference, true);
    },
    getContractId: function (component, supplyId) {
        let self = this;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let dossierId = component.get('v.dossierId');

        component.find('apexService').builder()
            .setMethod("getContractIdBySupplyId")
            .setInput({
                'supplyId': supplyId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                component.set("v.contractId", response.contractId);
                component.set("v.contractNumber", response.contractNumber);
                component.set("v.contractIntegrationKey", response.contractIntegrationKey);
                component.set("v.accountIntegrationKey", response.accountIntegrationKey);
                component.set("v.contractAccountId", response.contractAccountId);

                self.createDraftCase(component, dossierId);
                component.set("v.step", 1);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    handleSaveDraft: function(component, helper) {
        let self = this;
        let dossierId = component.get("v.dossierId");
        let caseId = component.get("v.caseId");
        let selectedChannel = component.get("v.selectedChannel");
        let selectedOrigin = component.get("v.selectedOrigin");
        let selectedWarranties = component.get('v.selectedWarranties').toString();
        let withdrawReason = component.get('v.selectedWithdrawReason');
        let totalWarrantyValue = component.get('v.totalWarrantyValue');
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        component.find("apexService").builder()
            .setMethod("saveDraftCase")
            .setInput({
                "dossierId": dossierId,
                "caseId": caseId,
                "listOfWarranties": selectedWarranties,
                "withdrawReason": withdrawReason,
                'selectedOrigin': selectedOrigin,
                'selectedChannel': selectedChannel,
                'totalWarrantyValue': totalWarrantyValue
            })
            .setResolve(function(response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function(response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },
    handleSave: function (component, helper) {
        let self = this;
        let dossierId = component.get('v.dossierId');
        let caseId = component.get('v.caseId');
        let selectedWarranties = component.get('v.selectedWarranties').toString();
        let withdrawReason = component.get('v.selectedWithdrawReason');
        let selectedChannel = component.get("v.selectedChannel");
        let selectedOrigin = component.get("v.selectedOrigin");
        let totalWarrantyValue = component.get('v.totalWarrantyValue');
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        if (dossierId === "") {
            ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredFields'));
            component.set("v.step", 0)
        } else {
            helper.showSpinner(component);
            component.find('apexService').builder()
                .setMethod("saveCase")
                .setInput({
                    'dossierId': dossierId,
                    'caseId': caseId,
                    'listOfWarranties': selectedWarranties,
                    "withdrawReason": withdrawReason,
                    'selectedOrigin': selectedOrigin,
                    'selectedChannel': selectedChannel,
                    'totalWarrantyValue': totalWarrantyValue
                })
                .setResolve(function (response) {
                    helper.hideSpinner(component);
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    self.redirectToDossier(component, helper);
                })
                .setReject(function (response) {
                    helper.hideSpinner(component);
                    const errors = response.getError();
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                    return;
                })
                .executeAction();
        }
    },
    createDraftCase: function (component, dossierId) {
        let self = this;
        let parentCaseId = component.get("v.parentCaseId");
        let supplyId = component.get("v.supplyId");
        let contractId = component.get("v.contractId");
        let accountId = component.get("v.accountId");
        let selectedOrigin = component.get('v.selectedOrigin');
        let selectedChannel = component.get('v.selectedChannel');
        let caseList = [];
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        self.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("createCase")
            .setInput({
                'parentCaseId': parentCaseId,
                'dossierId': dossierId,
                'contractId': contractId,
                'accountId': accountId,
                'supplyId': supplyId,
                'selectedOrigin': selectedOrigin,
                'selectedChannel': selectedChannel
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                caseList = response.caseList;

                if (caseList.length > 0) {
                    component.set('v.caseTile', caseList);
                    component.set('v.caseId', caseList[0].Id);
                    component.set("v.osiTableView", caseList.length > component.get("v.tileNumber"));
                    component.set("v.caseTileLength", caseList.length);
                    component.set("v.bailmentManagementRecordTypeId", caseList[0].RecordTypeId);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    deleteCase: function (component, event) {
        let deleteRecordId = event.getParam("deleteRecord");
        let caseList = component.get("v.caseTile");
        let items = [];

        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        component.set('v.caseTileLength', items.length)
        component.set("v.caseTile", items);
        component.set("v.step", 0);
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error on Console Navigation ", error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    }
});