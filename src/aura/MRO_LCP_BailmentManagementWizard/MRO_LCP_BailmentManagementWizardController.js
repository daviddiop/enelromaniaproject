/**
 * Created by Vlad Mocanu on 18/02/2020.
 */

({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        helper.initConsoleNavigation(component, $A.get('$Label.c.BailmentManagement'));
        helper.initialize(component);
    },
    handleSelectionOriginChannel: function (component, event) {
        let selectedOrigin = event.getParam("selectedOrigin");
        let selectedChannel = event.getParam("selectedChannel");

        component.set('v.selectedOrigin', selectedOrigin);
        component.set('v.selectedChannel', selectedChannel);

        if (selectedOrigin != null && selectedChannel !== null) {
            component.set('v.originAndChannelSelected', true)
        }
    },
    handleSupplyResult: function (component, event, helper) {
        let selectedSupplyId = String(event.getParam("selected"));
        component.set('v.supplyId', selectedSupplyId);

        helper.initialize(component);
        helper.getContractId(component, selectedSupplyId);
    },
    handleWithdrawReasonChange: function (component) {
        let selectedReason = component.find('reasonPicklist').get("v.value");

        component.set('v.selectedWithdrawReason', selectedReason);
    },
    handleError: function (component, event) {
        let errors = event.getParams();
        let ntfSvc = component.find('ntfSvc');
        let ntfLib = component.find('ntfLib');
        ntfSvc.error(ntfLib, errors);
    },
    handleConfirmedWarranties: function (component, event) {
        let selectedWarranties = event.getParam('selectedWarranties');
        let selectedWarrantyIds = [];

        selectedWarranties.forEach(warr => {
            selectedWarrantyIds.push(warr.id);
        });

        let totalWarrantyValue = selectedWarranties.reduce(function(totalValue, selectedWarranty) {
            totalValue += selectedWarranty.amount;
            return totalValue;
        }, 0);

        component.set('v.selectedWarranties', selectedWarrantyIds);
        component.set('v.totalWarrantyValue', totalWarrantyValue);
        component.set('v.step', 3);
    },
    handleDeleteCaseTile: function (component, event, helper) {
        helper.deleteCase(component, event, helper);
    },
    editStep: function (component, event) {
        let buttonPressed = event.getSource().getLocalId();

        if (buttonPressed === "returnStep1") {
            component.set("v.step", 1);
        }
    },
    nextStep: function (component, event) {
        let buttonPressed = event.getSource().getLocalId();

        if (buttonPressed === "confirmStep1") {
            component.set("v.step", 2);
        }
    },
    cancel: function (component) {
        component.set("v.showCancelBox", true);
    },
    saveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    },
    closeCancelModal: function (component, event, helper) {
        component.set("v.showCancelBox", false);
    },
    saveDraft: function (component, event, helper) {
        helper.handleSaveDraft(component, helper);
    },
    save: function (component, event, helper) {
        helper.handleSave(component, helper)
    },
    closeCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
});