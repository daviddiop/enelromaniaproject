<!--
 - Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.02.20.
 -->

<aura:component implements="flexipage:availableForAllPageTypes,lightning:isUrlAddressable">

    <!-- Wizard header -->
    <aura:attribute name="originSelected" type="String" access="private"/>
    <aura:attribute name="channelSelected" type="String" access="private"/>
    <aura:attribute name="disableOriginChannel" type="Boolean" default="false"/>

    <!-- Url params -->
    <aura:attribute access="public" name="accountId" type="String"/>
    <aura:attribute access="public" name="account" type="Object"/>
    <aura:attribute access="public" name="opportunityId" type="String"/>
    <aura:attribute access="public" name="opportunity" type="Object"/>
    <aura:attribute access="public" name="dossierId" type="String"/>
    <aura:attribute access="public" name="dossier" type="Object"/>
    <aura:attribute access="public" name="consumption" type="Object"/>
    <aura:attribute access="public" name="servicePointCode" type="String"/>
    <aura:attribute access="public" name="isPersonAccount" type="Boolean"/>
    <aura:attribute access="public" name="refundMethod" type="String"/>

    <!-- Case tile -->
    <aura:attribute access="public" name="caseRecord" type="Object"/>
    <aura:attribute access="private" name="selectCaseFields" type="List"
                    default="['CaseNumber', 'Supply__c', 'AccountName__c', 'ContactEmail', 'ServiceSite__c']"/>
    <aura:attribute access="private" name="selectCaseTileFields" type="List"
                    default="['CaseNumber','Status', 'Supply__c', 'AccountName__c', 'ContactEmail',  'ServiceSite__c']"/>

    <aura:attribute access="private" name="consumptionFields" type="List"/>
    <aura:attribute access="public" name="supplyId" type="String"/>

    <!-- Contract selection -->
    <aura:attribute access="public" name="contractId" type="String"/>
    <aura:attribute access="public" name="contractName" type="String"/>
    <aura:attribute access="public" name="contractType" type="String"/>
    <aura:attribute access="public" name="contractTerm" type="String"/>
    <aura:attribute access="public" name="companySignedId" type="String"/>
    <aura:attribute access="public" name="salesman" type="String"/>
    <aura:attribute access="public" name="contractStartDate" type="Date"/>
    <aura:attribute access="private" name="contractDataIsValid" type="Boolean" default="false"/>

    <aura:attribute access="public" name="billingProfileId" type="String"/>
    <aura:attribute access="public" name="billingProfileValid" type="String"/>
    <aura:attribute name="contractAccountFilter" type="Map" default="{}"/>

    <!-- Wizard params -->
    <aura:attribute access="public" name="step" type="Integer" default="0"/>
    <aura:attribute access="public" name="isClosed" type="Boolean" default="false"/>
    <aura:attribute access="public" name="showCancelBox" type="Boolean" default="false"/>
    <aura:attribute access="public" name="isSaving" type="Boolean" default="false"/>
    <aura:attribute access="public" name="salesSupportUser" type="String"/>

    <!-- Handlers -->
    <aura:handler name="init" value="{!this}" action="{!c.init}"/>
    <aura:handler name="change" value="{!v.pageReference}" action="{!c.init}"/>

    <!-- Services -->
    <c:ApexServiceLibrary aura:id="apexService" controller="MRO_LC_ProsumersWizard"/>
    <lightning:navigation aura:id="navService"/>
    <lightning:notificationsLibrary aura:id="notifLib"/>
    <c:notificationSvc aura:id="notify"/>
    <lightning:workspaceAPI aura:id="workspace"/>
    <c:cookieSvc aura:id="cookieSvc"/>

    <aura:if isTrue="{!not(empty(v.dossierId))}">
        <c:mroPreliminaryDataCheck recordId="{!v.dossierId}" doCheckOnInit="true"/>
    </aura:if>

    <aura:if isTrue="{!not(empty(v.opportunityId))}">
        <c:mroWizardHeader accountId="{!v.accountId}"
                           opportunityId="{!v.opportunityId}"
                           ononselectoriginchannel="{!c.handleOriginChannelSelection}"
                           disableOriginChannel="{!v.disableOriginChannel}"
                           wizardLabel="{!$Label.c.Prosumers}">
        </c:mroWizardHeader>
    </aura:if>

    <section aura:id="spinnerSection" role="dialog" tabindex="-1"
             class="slds-modal slds-fade-in-open slds-modal_small slds-backdrop">
        <lightning:spinner aura:id="spinner" variant="brand" size="large"/>
    </section>

    <lightning:card class="slds-var-m-vertical_small">
        <ol class="slds-setup-assistant">


            <!-- STEP 1 SERVICE POINT SELECTION -->
            <li class="slds-setup-assistant__item">
                <article class="slds-setup-assistant__step">
                    <div class="slds-summary-detail slds-is-open">
                        <div class="slds-container_fluid">
                            <div class="slds-summary-detail__title">
                                <div class="slds-up-assistant__step-summary">
                                    <div class="slds-media">
                                        <div class="slds-media__body slds-m-top_x-small">
                                            <div class="slds-media">
                                                <div class="slds-setup-assistant__step-summary-content slds-media__body">
                                                    <h3 class="slds-setup-assistant__step-summary-title slds-text-heading_small">
                                                        {!$Label.c.ServiceSiteSelection}</h3>
                                                    <p>{!$Label.c.SearchServiceSite}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="slds-summary-detail__content" id="step-0-summary-action">
                                <div class="slds-setup-assistant__step-detail">
                                    <div class="slds-grid slds-wrap slds-gutters">
                                        <div class="slds-col slds-size_3-of-12">
                                            <article class="slds-tile slds-tile_board slds-box">
                                                <c:mroAdvancedSearch
                                                        disabled="{! v.step != 1 || v.isClosed || v.caseRecord}"
                                                        aura:id="supplySelection"
                                                        supplyStatus="['Active']"
                                                        accountId="{!v.accountId}"
                                                        commodity="Electric"
                                                        andConditions="ServiceSite__c != null"
                                                        onselected="{!c.handleSupplyResult}">
                                                </c:mroAdvancedSearch>
                                            </article>
                                        </div>
                                        <aura:if isTrue="{!v.caseRecord}">
                                            <div class="case-tile-max-width">
                                                <div class="slds-col">
                                                    <c:mroCaseTile disabled="{!v.step != 1 || v.isClosed}"
                                                                   hideEditIcon="true"
                                                                   isPowerVoltage="true"
                                                                   selectCaseFields="{!v.selectCaseFields}"
                                                                   selectCaseTileFields="{!v.selectCaseTileFields}"
                                                                   caseId="{!v.caseRecord.Id}"
                                                                   isPowerVoltageWizard="true"
                                                                   tileTitle="{!$Label.c.CaseEditTitleFromTechnicalDataChangeTile}"
                                                                   ondelete="{!c.handleCaseDelete}"/>
                                                </div>
                                            </div>
                                        </aura:if>
                                    </div>
                                </div>
                                <aura:if isTrue="{!and(v.caseRecord, v.caseRecord.Supply__c)}">
                                    <div class="slds-col slds-size_6-of-12 slds-m-top_large">
                                        <div class="slds-setup-assistant__step-detail">
                                            <lightning:recordEditForm aura:id="supplyEditForm" recordId="{!v.caseRecord.Supply__c}"
                                                                      objectApiName="Supply__c"
                                                                      onerror="{!c.handleSupplyError}"
                                                                      onsuccess="{!c.handleSupplySuccess}">
                                                <lightning:inputField
                                                        aura:id="supplyProductionTechnology"
                                                        required="true"
                                                        fieldName="ProductionTechnology__c"
                                                        disabled="{!v.isClosed || v.step != 1}"/>
                                            </lightning:recordEditForm>
                                        </div>
                                    </div>
                                </aura:if>
                                <div class="slds-float_right slds-m-top_medium">
                                    <lightning:button disabled="{! v.step != 1 || empty(v.caseRecord)}" variant="brand"
                                                      aura:id="confirmStep1" name="confirmStep1"
                                                      iconName="utility:right"
                                                      label="{!$Label.c.Next}"
                                                      onclick="{!c.nextStep}">
                                    </lightning:button>
                                    <aura:if isTrue="{!v.step > 1}">
                                        <lightning:button variant="brand" disabled="{!v.isClosed}"
                                                          aura:id="editStep1" name="editStep1"
                                                          label="{!$Label.c.Edit}"
                                                          iconName="utility:edit"
                                                          onclick="{!c.editStep}">
                                        </lightning:button>
                                    </aura:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>

            <!-- STEP 2 CONTRACT SELECTION -->
            <li class="slds-setup-assistant__item">
                <article class="slds-setup-assistant__step">
                    <div class="slds-summary-detail slds-is-open">
                        <div class="slds-container_fluid">
                            <div class="slds-summary-detail__title">
                                <div class="slds-setup-assistant__step-summary">
                                    <div class="slds-media">
                                        <div class="slds-media__body slds-m-top_x-small">
                                            <div class="slds-media">
                                                <div class="slds-setup-assistant__step-summary-content slds-media__body">
                                                    <h3 class="slds-setup-assistant__step-summary-title slds-text-heading_small">
                                                            {!$Label.c.ContractAddition}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slds-summary-detail__content">
                                <div class="slds-setup-assistant__step-detail">
                                    <aura:if isTrue="{!not(empty(v.accountId))}">
                                        <c:mroContractSelection aura:id="contractSelectionComponent"
                                                                disabled="{!(v.step != 2) || (v.isClosed)}"
                                                                accountId="{!v.accountId}"
                                                                contractName="{!v.contractName}"
                                                                contractType="{!v.contractType}"
                                                                contractStartDate="{!v.contractStartDate}"
                                                                contractTerm="{!v.contractTerm}"
                                                                companySignedId="{!v.companySignedId}"
                                                                salesman="{!v.salesman}"
                                                                salesChannel="{!v.channelSelected}"
                                                                isContractTypeDisabled="true"
                                                                hideList="true"
                                                                hideCustomerSignedDate="true"
                                                                showContractStartDate="true"
                                                                contractStartDateRequired="true"
                                                                ongetcontract="{!c.getContractData}">
                                        </c:mroContractSelection>
                                    </aura:if>
                                </div>
                                <div class="slds-float_right slds-p-top_medium">
                                    <lightning:button
                                            disabled="{!v.step != 2 || v.isClosed || not(v.contractDataIsValid)}"
                                            variant="brand" aura:id="confirmStep2" name="confirmStep2"
                                            label="{!$Label.c.Next}" iconName="utility:right"
                                            onclick="{!c.nextStep}">
                                    </lightning:button>
                                    <aura:if isTrue="{!v.step > 2}">
                                        <lightning:button variant="brand" disabled="{!v.isClosed}"
                                                          aura:id="editStep2" name="editStep2"
                                                          label="{!$Label.c.Edit}"
                                                          iconName="utility:edit" onclick="{!c.editStep}">
                                        </lightning:button>
                                    </aura:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>

            <!-- STEP 3 CONTRACT ACCOUNT SELECTION -->
            <li class="slds-setup-assistant__item">
                <article class="slds-setup-assistant__step">
                    <div class="slds-summary-detail slds-is-open">
                        <div class="slds-container_fluid">
                            <div class="slds-summary-detail__title">
                                <div class="slds-up-assistant__step-summary">
                                    <div class="slds-media">
                                        <div class="slds-media__body slds-m-top_x-small">
                                            <div class="slds-media">
                                                <div class="slds-setup-assistant__step-summary-content slds-media__body">
                                                    <h3 class="slds-setup-assistant__step-summary-title slds-text-heading_small">
                                                        Billing Profile Selection</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div aria-hidden="false" class="slds-summary-detail__content">
                                <div class="slds-setup-assistant__step-detail">
                                    <aura:if isTrue="{!v.step > 2}">
                                        <c:mroBillingProfileSelection aura:id="billingProfileSelection"
                                                                      disabled="{!v.step > 3 || v.isClosed}"
                                                                      accountId="{!v.accountId}"
                                                                      billingProfileRecordId="{!v.billingProfileId}"
                                                                      onselected="{!c.handleBillingProfileSelection}"
                                                                      isProsumers="true"
                                                                      showHeader="false"
                                                                      showRefundColumns="true"
                                                                      refundOnClone="true"
                                                                      showEditButton="true"/>
                                    </aura:if>

                                </div>

                                <div class="slds-float_right slds-m-top_medium">
                                    <lightning:button
                                            disabled="{!v.step != 3 || empty(v.billingProfileId) || v.isClosed || not(v.billingProfileValid)}"
                                            variant="brand" aura:id="confirmStep3" name="confirmStep3"
                                            iconName="utility:right" label="{!$Label.c.Next}"
                                            onclick="{!c.nextStep}">
                                    </lightning:button>
                                    <aura:if isTrue="{!v.step > 3}">
                                        <lightning:button variant="brand" disabled="{!v.isClosed}"
                                                          aura:id="editStep3" name="editStep3"
                                                          label="{!$Label.c.Edit}"
                                                          iconName="utility:edit"
                                                          onclick="{!c.editStep}">
                                        </lightning:button>
                                    </aura:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>

            <!-- STEP 5 CASE DETAILS FORM -->
            <li class="slds-setup-assistant__item">
                <article class="slds-setup-assistant__step">
                    <div class="slds-summary-detail slds-is-open">
                        <div class="slds-container_fluid">
                            <div class="slds-summary-detail__title">
                                <div class="slds-up-assistant__step-summary">
                                    <div class="slds-media">
                                        <div class="slds-media__body slds-m-top_x-small">
                                            <div class="slds-media">
                                                <div class="slds-setup-assistant__step-summary-content slds-media__body">
                                                    <h3 class="slds-setup-assistant__step-summary-title slds-text-heading_small">
                                                        Case Details</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div aria-hidden="false" class="slds-summary-detail__content">
                                <div class="slds-setup-assistant__step-detail">
                                    <aura:if isTrue="{!and(v.step > 4, v.caseRecord)}">
                                        <lightning:recordEditForm aura:id="caseEditForm" recordId="{!v.caseRecord.Id}"
                                                                  objectApiName="Case"
                                                                  onerror="{!c.handleCaseError}"
                                                                  onsuccess="{!c.handleCaseSuccess}">
                                            <div class="slds-p-around--medium">
                                                <div class="slds-grid">
                                                    <div class="slds-size_1-of-3">
                                                        <lightning:inputField
                                                                aura:id="caseAvailablePower"
                                                                required="true"
                                                                fieldName="AvailablePower__c"
                                                                value="{!v.caseRecord.AvailablePower__c}"
                                                                disabled="{!v.isClosed || v.step != 5}"/>
                                                    </div>
                                                    <div class="slds-size_1-of-3">
                                                        <lightning:inputField
                                                                aura:id="caseConnectionCerNumber"
                                                                required="true"
                                                                fieldName="ConnectionCertificateNumber__c"
                                                                value="{!v.caseRecord.ConnectionCertificateNumber__c}"
                                                                disabled="{!v.isClosed || v.step != 5}"/>
                                                    </div>
                                                    <div class="slds-size_1-of-3">
                                                        <lightning:inputField
                                                                aura:id="caseCerIssuingDate"
                                                                required="true"
                                                                fieldName="CerIssuingDate__c"
                                                                value="{!v.caseRecord.CerIssuingDate__c}"
                                                                disabled="{!v.isClosed || v.step != 5}"/>
                                                    </div>
                                                </div>
                                                <div class="slds-grid">
                                                    <div class="slds-size_1-of-3">
                                                        <lightning:inputField
                                                                aura:id="caseANRELicense"
                                                                fieldName="ANRELicense__c"
                                                                value="{!v.caseRecord.ANRELicense__c}"
                                                                disabled="{!v.isClosed || v.step != 5}"/>
                                                    </div>
                                                    <div class="slds-size_1-of-3">
                                                        <lightning:inputField
                                                                aura:id="caseBatteries"
                                                                required="true"
                                                                fieldName="Batteries__c"
                                                                value="{!v.caseRecord.Batteries__c}"
                                                                disabled="{!v.isClosed || v.step != 5}"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </lightning:recordEditForm>
                                    </aura:if>

                                </div>

                                <div class="slds-float_right slds-m-top_medium">
                                    <lightning:button
                                            disabled="{!v.step != 5}"
                                            variant="brand" aura:id="confirmStep5" name="confirmStep5"
                                            iconName="utility:right" label="{!$Label.c.Next}"
                                            onclick="{!c.nextStep}">
                                    </lightning:button>
                                    <aura:if isTrue="{!v.step > 5}">
                                        <lightning:button variant="brand" disabled="{!v.isClosed}"
                                                          aura:id="editStep5" name="editStep5"
                                                          label="{!$Label.c.Edit}"
                                                          iconName="utility:edit"
                                                          onclick="{!c.editStep}">
                                        </lightning:button>
                                    </aura:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>

            <!-- STEP 6 CONSUMPTION DETAILS FORM -->
            <li class="slds-setup-assistant__item">
                <article class="slds-setup-assistant__step">
                    <div class="slds-summary-detail slds-is-open">
                        <div class="slds-container_fluid">
                            <div class="slds-summary-detail__title">
                                <div class="slds-up-assistant__step-summary">
                                    <div class="slds-media">
                                        <div class="slds-media__body slds-m-top_x-small">
                                            <div class="slds-media">
                                                <div class="slds-setup-assistant__step-summary-content slds-media__body">
                                                    <h3 class="slds-setup-assistant__step-summary-title slds-text-heading_small">
                                                        {!$Label.c.ConsumptionDetails}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div aria-hidden="false" class="slds-summary-detail__content">
                                <div class="slds-setup-assistant__step-detail">
                                    <aura:if isTrue="{!and(v.step > 5, v.consumption)}">
                                        <div class="slds-box_small slds-theme_shade">
                                                {!v.servicePointCode}
                                            </div>
                                        <div class="slds-p-around--medium slds-scrollable_x">
                                            <aura:if isTrue="{!v.consumptionFields}">
                                                <table class="slds-table slds-table_bordered slds-table_col-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <aura:iteration items="{!v.consumptionFields}"
                                                                        var="consumptionField">
                                                            <aura:if isTrue="{!consumptionField.visible}">
                                                                <th title="{!consumptionField.label}">{!consumptionField.label}</th>
                                                            </aura:if>
                                                        </aura:iteration>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="slds-truncate" title="{!$Label.c.EstimatedConsumptionKWh}">{!$Label.c.EstimatedConsumptionKWh}</td>
                                                        <aura:iteration items="{!v.consumptionFields}"
                                                                        var="consumptionField">
                                                            <aura:if isTrue="{!consumptionField.visible}">
                                                                <td class="slds-truncate" title="{!consumptionField.value}">{!consumptionField.value}</td>
                                                            </aura:if>
                                                        </aura:iteration>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </aura:if>
                                        </div>
                                    </aura:if>
                                </div>

                                <div class="slds-float_right slds-m-top_medium">
                                    <lightning:button
                                            disabled="{!v.step != 6}"
                                            variant="brand" aura:id="confirmStep6" name="confirmStep6"
                                            iconName="utility:right" label="{!$Label.c.Next}"
                                            onclick="{!c.nextStep}">
                                    </lightning:button>
                                    <aura:if isTrue="{!v.step > 6}">
                                        <lightning:button variant="brand" disabled="{!v.isClosed}"
                                                          aura:id="editStep6" name="editStep6"
                                                          label="{!$Label.c.Edit}"
                                                          iconName="utility:edit"
                                                          onclick="{!c.editStep}">
                                        </lightning:button>
                                    </aura:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>

            <!-- STEP 7 SENDING CHANNEL SELECTION -->
            <li class="slds-setup-assistant__item">
                <article class="slds-setup-assistant__step">
                    <div class="slds-summary-detail slds-is-open">
                        <div class="slds-container_fluid">
                            <div class="slds-summary-detail__title">
                                <div class="slds-setup-assistant__step-summary">
                                    <div class="slds-media">
                                        <div class="slds-media__body slds-m-top_x-small">
                                            <div class="slds-media">
                                                <div class="slds-setup-assistant__step-summary-content slds-media__body">
                                                    <h3 class="slds-setup-assistant__step-summary-title slds-text-heading_small">
                                                        Select Channel for Customer Communication</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div aria-hidden="false" class="slds-summary-detail__content">
                                <div class="slds-setup-assistant__step-detail">
                                    <aura:if isTrue="{!v.dossierId != null}">
                                        <c:mroSendingChannelSelection dossierId="{!v.dossierId}"
                                                                      disabledInput="{!v.step != 7 || v.isClosed}"
                                                                      aura:id="sendingChannelSelection"/>
                                    </aura:if>
                                    <div class="slds-float_right slds-m-top_medium">
                                        <lightning:button disabled="{! (v.isClosed) || (v.step != 7) }"
                                                          variant="brand" aura:id="confirmStep7" name="confirmStep7"
                                                          label="{!$Label.c.Next}" iconName="utility:right"
                                                          onclick="{!c.nextStep}">
                                        </lightning:button>
                                        <aura:if isTrue="{!v.step > 5}">
                                            <lightning:button variant="brand" disabled="{!v.isClosed}"
                                                              aura:id="editStep7" name="editStep7"
                                                              label="{!$Label.c.Edit}"
                                                              iconName="utility:edit" onclick="{!c.editStep}">
                                            </lightning:button>
                                        </aura:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>

            <!-- STEP 8 - GDPR PRIVACY AND CONTACTS -->
            <aura:if isTrue="{!v.isPersonAccount}">
                <li class="slds-setup-assistant__item">
                    <article class="slds-setup-assistant__step">
                        <div class="slds-summary-detail slds-is-open">
                            <div class="slds-container_fluid">

                                <c:mroTitleSection title="{!$Label.c.GdprPrivacyAndContacts}"/>

                                <div aria-hidden="false" class="slds-summary-detail__content">
                                    <div class="slds-setup-assistant__step-detail">
                                        <aura:if isTrue="{!not(empty(v.opportunityId))}">
                                            <c:mroPrivacyChange disabled="{!v.step != 8 || (v.isClosed)}"
                                                                onsuccess="{!c.getPrivacyId}" aura:id="privacyChange"
                                                                opportunityId="{!v.opportunityId}"
                                                                dossierId="{!v.dossierId}"/>
                                        </aura:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </li>
            </aura:if>
        </ol>
        <footer class="slds-clearfix slds-p-right_medium">
            <ul class="slds-button-group-list slds-float_right">
                <li>
                    <lightning:button disabled="{!v.isClosed}" variant="destructive"
                                      iconName="utility:cancel_file_request" onclick="{!c.cancel}">
                        {!$Label.c.Cancel}</lightning:button>
                </li>
                <li>
                    <lightning:button disabled="{!v.isClosed}" variant="neutral" onclick="{!c.saveDraft}"
                                      iconName="standard:drafts">
                        {!$Label.c.SaveDraft}</lightning:button>
                </li>
                <li>
                    <lightning:button disabled="{!v.step != 8 || v.isClosed}" variant="brand" onclick="{!c.save}"
                                      iconName="utility:save">
                        {!$Label.c.Save}</lightning:button>
                </li>
            </ul>
        </footer>
    </lightning:card>

    <aura:if isTrue="{!v.showCancelBox}">
        <section aura:id="modalCancel" role="dialog" tabindex="1"
                 class="slds-modal slds-fade-in-open slds-modal_medium slds-backdrop">
            <div class="slds-modal__container">
                <div class="slds-modal__header slds-p-around_none">
                    <lightning:buttonIcon title="{!$Label.c.Close}" alternativeText="{!$Label.c.Close}" size="large"
                                          iconName="utility:close" variant="bare-inverse" class="slds-modal__close"
                                          onclick="{!c.onCloseCancelBox}"/>
                </div>
                <div class="slds-modal__content slds-p-around_medium mro-modal-small">
                    <c:mroCancellationRequest aura:id="cancellationRequest" objectId="{!v.dossierId}"  objectName="Dossier__c"
                                              objectRecordType="{!v.dossier.RecordType.DeveloperName}" originPhase="{!v.dossier.Phase__c}"
                                              isModal="true"
                                              onsuccess="{!c.onSaveCancelReason}" oncancel="{!c.onCloseCancelBox}"/>
                </div>
            </div>
        </section>
    </aura:if>
</aura:component>