/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.02.20.
 */
({
    initialize: function (component) {
        let that = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        let genericRequestDossierId = myPageRef.state.c__genericRequestId;

        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "opportunityId": opportunityId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestDossierId": genericRequestDossierId
            })
            .setResolve(function (response) {
                if (response.opportunityId && !opportunityId) {
                    that.updateUrl(component, accountId, response.opportunityId, response.dossierId);
                    return;
                }
                if(response.account){
                    component.set('v.contractType', response.account.IsPersonAccount ? 'Residential' : 'Business');
                    component.set('v.contractAccountFilter', {'dataType':'string','field':'BillingProfile__r.RefundMethod__c','operator':'=','value':response.account.IsPersonAccount ? 'Invoice' : 'Bank Transfer'});
                    component.set('v.isPersonAccount', response.account.IsPersonAccount);
                    component.set('v.refundMethod', response.account.IsPersonAccount ? 'Invoice' : 'Bank Transfer');
                }
                if(response.contractType){
                    component.set('v.contractType', response.contractType);
                }

                that.setStep(component, response);
                that.setContractFields(component, response.opportunity, response.contract);

                component.set("v.consumption", response.consumption);
                component.set("v.servicePointCode", response.servicePointCode);
                component.set("v.isClosed", response.isClosed);
                component.set("v.billingProfileId", response.billingProfileId);
                component.set('v.billingProfileValid', response.billingProfileId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.opportunity", response.opportunity);
                component.set("v.opportunityId", response.opportunityId);
                component.set("v.caseRecord", response.caseRecord);
                component.set("v.salesSupportUser", response.salesSupportUser);

                that.buildConsumptionFields(component, response);

                that.hideSpinner(component);
            })
            .setReject(function (error) {
                that.showError(component, error);
            })
            .executeAction();
    },

    buildConsumptionFields: function(component, response){
        let consumption = response.consumption,
            labels = response.consumptionLabels;
        if(!consumption){
            return;
        }

        let consumptionFields = [];
        labels.forEach(function(v){
            consumptionFields.push({
                name: v.name,
                label: v.value,
                value: consumption[v.name],
                visible: consumption[v.name] !== 0
            });
        });
        component.set('v.consumptionFields', consumptionFields);
    },

    createCase: function (component, supplyIds) {
        let supplyId = component.get('v.supplyId');
        if(supplyIds){
            supplyId = supplyIds[0];
            component.set('v.supplyId', supplyId);
        }
        let that = this;
        that.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("createCase")
            .setInput({
                supplyId: supplyId,
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (!response.error) {
                    if(response.contractType){
                        component.set('v.contractType', response.contractType);
                    }
                    component.set('v.caseRecord', response.caseRecord);
                }

            })
            .setReject(function (error) {
                that.showError(component, error);
                that.hideSpinner(component);
            })
            .executeAction();

    },
    createConsumption: function (component) {
        let that = this;
        that.showSpinner(component);

        let consumption = component.get('v.consumption');
        let consumptionId = consumption ? consumption.Id : null;


        component.find('apexService').builder()
            .setMethod("createConsumption")
            .setInput({
                caseId: that.getCaseId(component),
                contractId: component.get('v.contractId'),
                consumptionId: consumptionId
            })
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (!response.error) {
                    component.set("v.consumption", response.consumption);
                    component.set("v.servicePointCode", response.servicePointCode);
                    component.set("v.caseRecord", response.caseRecord);
                    that.changeStep(component, that, 6);
                    that.buildConsumptionFields(component, response);
                }

            })
            .setReject(function (error) {
                that.showError(component, error);
                that.hideSpinner(component);
            })
            .executeAction();

    },
    setChannelAndOrigin: function (component) {
        component.find('apexService').builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: component.get("v.opportunityId"),
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
            })
            .executeAction();
    },
    checkStartDate: function (component, helper, callback) {
        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("CheckStartDate")
            .setInput({
                opportunityId: component.get("v.opportunityId"),
                startDate: component.get("v.contractStartDate"),
            })
            .setResolve(function (response) {
              if (callback && typeof callback === "function") {
                    callback(response);
                }

                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
            })
            .executeAction();
    },
    updateContractInformation: function (component, helper) {
        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("updateContractInformation")
            .setInput({
                opportunityId: component.get("v.opportunityId"),
                supplyId: component.get("v.supplyId"),
                contractType: component.get("v.contractType"),
                contractName: component.get("v.contractName"),
                companySignedId: component.get("v.companySignedId"),
                salesman: component.get("v.salesman"),
                salesChannel: component.get("v.channelSelected"),
                contractStartDate: component.get("v.contractStartDate"),
                contractTerm: component.get("v.contractTerm"),
                salesSupportUser: component.get("v.salesSupportUser")
            })
            .setResolve(function (response) {
                helper.setContractFields(component, null, response.contract);
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
            })
            .executeAction();
    },

    handleBillingProfileSelection: function (component, callback) {
        let that = this;
        that.showSpinner(component);

        component.find('apexService').builder()
            .setMethod('handleBillingProfileSelection')
            .setInput({
                caseId: that.getCaseId(component),
                billingProfileId: component.get("v.billingProfileId")
            })
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (response.error) {
                    that.showError(component, response.errorMsg);
                    return;
                }
                component.set('v.billingProfileValid', true);
                if (callback && typeof callback === "function") {
                    callback(component, that);
                }
            })
            .setReject(function (response) {
                that.showError(component, JSON.stringify(response.getError()));
            })
            .executeAction();
    },

    performSaveProcess: function(component, helper, isDraft){
        let dossierId = component.get("v.dossierId");
        let salesSupportUser = component.get("v.salesSupportUser");

        component.find('apexService').builder()
            .setMethod("saveProcess")
            .setInput({
                opportunityId: component.get("v.opportunityId"),
                dossierId: dossierId,
                caseId: helper.getCaseId(component),
                isDraft: isDraft,
                salesSupportUser: salesSupportUser
            })
            .setResolve(function (response) {
                if (response.error) {
                    helper.showError(component, response.errorMsg);
                    return;
                }

                const pageReference = {
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: dossierId,
                        objectApiName: 'c__Dossier',
                        actionName: 'view'
                    }
                };
                helper.redirect(component, pageReference);
            })
            .setReject(function (response) {
                const errors = response.getError();
                helper.showError(component, errors[0].message);
            })
            .executeAction();
    },

    handleSaveDraft: function(component, helper){
        helper.performSaveProcess(component, helper, true);
    },

    handleSave: function(component, helper){
        helper.updateCaseByProsumerPrice(component);
        helper.performSaveProcess(component, helper, false);
    },

    setStep: function (component, response) {

        let step = 8;
        if (!response.dossier.SendingChannel__c) {
            step = 7;
        }

        if (response.caseRecord && !response.caseRecord.AvailablePower__c || !response.consumption) {
            step = 5;
        }

        if (!response.billingProfileId) {
            step = 3;
        }

        if (!response.contract) {
            step = 2;
        }
        if (!response.caseRecord) {
            step = 1;
        }
        if (!response.dossier.Channel__c || !response.dossier.Origin__c) {
            step = 0;
        }

        this.changeStep(component, this, step);
    },

    setContractFields: function (component, opportunity, contract) {
        if (opportunity) {
            if (opportunity.Origin__c) {
                component.set("v.originSelected", opportunity.Origin__c);
            }
            if (opportunity.Channel__c) {
                component.set("v.channelSelected", opportunity.Channel__c);
            }
        }
        let contractName = 'Prosumers';
        if(contract){
            component.set("v.contractId", contract.Id);
            if (contract.Name) {
                contractName = contract.Name;
            }
            if (contract.ContractType__c) {
                component.set("v.contractType", contract.ContractType__c);
            }
            if (contract.CompanySignedId) {
                component.set("v.companySignedId", contract.CompanySignedId);
            }
            if (contract.Salesman__c) {
                component.set("v.salesman", contract.Salesman__c);
            }
            if (contract.StartDate) {
                component.set("v.contractStartDate", contract.StartDate);
            }
            if (contract.ContractTerm) {
                component.set("v.contractTerm", contract.ContractTerm);
            }
        }
        component.set("v.contractName", contractName);
    },

    checkSelectedContract: function (component, contractData) {
        let channelSelected = component.get("v.channelSelected");
        return contractData.type &&
            (channelSelected !== "Indirect Sales" || contractData.salesman) &&
            (channelSelected !== "Direct Sales (KAM)" || contractData.companySignedId);
    },
    
    getCaseId: function(component){
        let caseRecord = component.get('v.caseRecord');
        return caseRecord ? caseRecord.Id : null;
    },

    updateUrl: function (component, accountId, opportunityId, dossierId) {
        let that = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ProsumersWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId,
                "c__dossierId": dossierId
            }
        };
        that.redirect(component, pageReference, true);
    },

    validateCaseForm: function(component){
        let error = 'Fill in required fields';
        let caseRecord = component.get('v.caseRecord');
        let caseCerIssuingDate = component.find('caseCerIssuingDate').get('v.value'),
            caseAvailablePower = component.find('caseAvailablePower').get('v.value'),
            caseConnectionCerNumber = component.find('caseConnectionCerNumber').get('v.value'),
            caseBatteries = component.find('caseBatteries').get('v.value');

        if(typeof caseAvailablePower === 'undefined'){
            caseAvailablePower = caseRecord.AvailablePower__c;
        }

        let valid = caseAvailablePower &&
            (caseConnectionCerNumber || typeof caseConnectionCerNumber === 'undefined' && caseRecord.ConnectionCertificateNumber__c) &&
            (caseBatteries || typeof caseBatteries === 'undefined' && caseRecord.Batteries__c) &&
            (caseCerIssuingDate || typeof caseCerIssuingDate === 'undefined' && caseRecord.CerIssuingDate__c);

        if(valid && (caseAvailablePower <= 0 || caseAvailablePower > 100)){
            valid = false;
            error = 'Installed Power must be greater than 0 and less than 100';
        }

        if(valid && new Date(caseCerIssuingDate).getTime() > new Date().getTime()){
            valid = false;
            error = 'Incorrect CER Issuing Date';
        }
        return {valid: valid, error: error};
    },

    changeStep: function (component, helper, stepNumber, isNext) {

        if (stepNumber === 2 && isNext) {
            let supplyProductionTechnology = component.find('supplyProductionTechnology').get('v.value');
            if(supplyProductionTechnology){
                component.find('supplyEditForm').submit();
            }else{
                helper.showError(component, 'Fill in required fields');
            }
            return;
        }
        if (stepNumber === 3 && isNext) {
            if(!helper.validateContractData(component)){
                return;
            }
            helper.updateContractInformation(component, helper);
            helper.checkStartDate(component, helper, (response)=>{
                if (response.error) {
                    helper.showError(component, response.errorMsg);
                    return;
                }
                helper.changeStep(component, helper, stepNumber);
            });
            return;
        }
        if (stepNumber === 4 && isNext) {
            stepNumber++;
        }
        if (stepNumber === 6 && isNext) {
            let caseFormValidation = helper.validateCaseForm(component);
            if(!caseFormValidation.valid){
                helper.showError(component, caseFormValidation.error);
            }else {
                component.find('caseEditForm').submit();
            }
            return;
        }
        if (stepNumber === 8 && isNext) {
            component.find('sendingChannelSelection').saveSendingChannel();
        }
        component.set('v.disableOriginChannel', stepNumber > 1);
        component.set("v.step", stepNumber);
    },

    validateContractData: function (component) {
        let startDate = component.get("v.contractStartDate");
        if (!startDate) {
            this.showError(component, 'Fill in required fields');
        } else if (new Date(startDate).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)) {
            this.showError(component, 'Contract Start Date cannot be in past');
        } else {
            return true;
        }
        return false;
    },

    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference, overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    redirectToDossier: function(component){
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        this.redirect(component, pageReference);
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let that = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 4) {//3
                        window.location = window.location + '#loaded';
                        that.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
    showError: function(component, message){
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        ntfSvc.error(ntfLib, message);
    },
    updateCaseByProsumerPrice: function (component) {
        let self = this;
        let caseId = self.getCaseId(component);
        component.find('apexService').builder()
            .setMethod("updateCaseByProsumerPrice")
            .setInput({
                caseId: caseId
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
            })
            .executeAction();
    }
})