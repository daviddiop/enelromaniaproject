/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.02.20.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;

        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        component.set("v.dossierId", dossierId);

        helper.initialize(component);
        helper.initConsoleNavigation(component, $A.get("$Label.c.Prosumers"));
    },

    handleSupplyResult: function (component, event, helper) {
        const supplyIds = event.getParam('selected');
        helper.createCase(component, supplyIds);
    },

    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('confirmStep', '')) + 1;
        helper.changeStep(component, helper, stepNumber, true);
    },

    editStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('editStep', ''));
        helper.changeStep(component, helper, stepNumber);
    },

    handleBillingProfileSelection: function (component, event, helper) {
        component.set('v.billingProfileId', event.getParam('billingProfileRecordId'));
        component.set('v.billingProfileValid', false);
        helper.handleBillingProfileSelection(component);
    },

    handleCaseSuccess: function(component, event, helper){
        helper.createConsumption(component);
    },

    handleCaseError: function(component, event, helper){
        console.log('case form error');
    },

    handleSupplySuccess: function(component, event, helper){
        helper.hideSpinner(component);
        helper.changeStep(component, helper, 2);
    },

    handleSupplyError: function(component, event, helper){
        helper.hideSpinner(component);
        console.log('supply form error');
    },

    handleOriginChannelSelection: function (component, event, helper) {
        if (component.get("v.step") > 0) {
            return;
        }
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            helper.setChannelAndOrigin(component);
            helper.changeStep(component, helper, 1);
        }
    },

    getContractData: function (component, event, helper) {
        let contractData = component.find('contractSelectionComponent').getNewContractDetails();

        component.set("v.contractName", contractData.name);
        component.set("v.contractType", contractData.type);
        component.set("v.companySignedId", contractData.companySignedId);
        component.set("v.salesman", contractData.salesman);
        component.set("v.contractStartDate", contractData.contractStartDate);
        component.set("v.contractTerm", contractData.contractTerm);

        component.set('v.contractDataIsValid', helper.checkSelectedContract(component, contractData));
    },

    handleCaseDelete: function (component, event, helper) {
        component.set('v.caseRecord', null);
    },

    getPrivacyId: function (component, event, helper) {
        if (!component.get("v.isSaving")) {
            return helper.handleSaveDraft(component, helper);
        }
        if (event.getParam('dontProcess')) {
            helper.showError(component, $A.get("$Label.c.DoNotProcess"));
            return;
        }

        helper.handleSave(component, helper);

    },

    cancel: function (component) {
        component.set('v.showCancelBox', true);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component);
    },
    onCloseCancelBox: function (component) {
        component.set('v.showCancelBox', false);
    },
    save: function (component, event, helper) {
        component.set('v.isSaving', true);
        if(component.find("privacyChange")){
            component.find("privacyChange").savePrivacyChange();
        }else{
            helper.handleSave(component, helper);
        }
    },
    saveDraft: function (component, event, helper) {
        component.set('v.isSaving', false);
        if(component.find("privacyChange")){
            component.find("privacyChange").savePrivacyChange();
        }else{
            helper.handleSaveDraft(component, helper);
        }
    },
});