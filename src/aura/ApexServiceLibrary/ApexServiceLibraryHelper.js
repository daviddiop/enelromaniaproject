({
    callServer: function (component, helper, method, input, resolve, reject, useSpinner) {
        var jsonInput = input ? JSON.stringify(input) : null;
        var serverCall = component.get("c.call");
        serverCall.setParams({
            className: component.get('v.controller'),
            methodName: method,
            jsonInput: jsonInput
        });
        serverCall.setCallback(component, function (response) {
            var error;
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if (result.error) {
                    error = result.error;
                } else {
                    if (resolve) {
                        resolve(result.data);
                    }
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        error = errors[0].message;
                    }
                } else {
                    error = $A.get('$Label.c.UnexpectedError');
                }
            } else {
                error = $A.get('$Label.c.RequestFailed');
            }
            if (error) {
                if (reject) {
                    reject(error);
                } else {
                    helper.showError(component, error);
                }
            }
        });
        $A.enqueueAction(serverCall);
    },

    showError: function (component, errorMessage) {
        component.find('notifySvc')
            .error(component.find('notifyLib'), errorMessage);
    }
})