({
    builder: function (component, event, helper) {
        return {
            method : null,
            input: null,
            resolve : null,
            reject : null,
            useSpinner : true,
            setMethod: function (_method) {
                this.method = _method;
                return this;
            },
            setInput: function (_input) {
                this.input = _input;
                return this;
            },
            setResolve: function (_resolve) {
                this.resolve = _resolve;
                return this;
            },
            setReject: function (_reject) {
                this.reject = _reject;
                return this;
            },
            setUseSpinner: function (_useSpinner) {
                this.useSpinner = _useSpinner;
                return this;
            },
            executeAction: $A.getCallback(function () {
                helper.callServer(component, helper, this.method, this.input, this.resolve, this.reject, this.useSpinner);
            })
        };
    }
})