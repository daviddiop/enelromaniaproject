/**
 * Created by l.centra on 20/01/2020.
 */

({
    init: function (component, event, helper) {
        console.log('init aura MRO Ctrl');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        component.set("v.isDraft", false);
        const today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set("v.inputValueDateDefault", today);
        helper.initialize(component, event, helper);
    },
    onRender: function (component) {
        console.log('onRender aura MRO Ctrl');
        const { getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel } = component.find("workspace");
        isConsoleNavigation()
            .then(function(response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function(response) {
                            let focusedTabId = response.tabId;
                            console.log(" response tab Info ", JSON.stringify(response));
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: $A.get("$Label.c.CompanyInsolvencyBankruptcy")
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: $A.get("$Label.c.CompanyInsolvencyBankruptcy")
                                });
                            }
                        });
                }
            });
    },
    handleOriginChannelSelection: function (component, event, helper) {
    let originSelected = event.getParam("selectedOrigin");
     let channelSelected = event.getParam("selectedChannel");
     if (originSelected && channelSelected) {
        component.set("v.originSelected", originSelected);
        component.set("v.channelSelected", channelSelected);
        component.set("v.step", 1);
        }else if (originSelected === 'undefined' || originSelected === ''
                    || originSelected === null   || channelSelected === ''
                    || channelSelected === null || channelSelected === 'undefined') {
                component.set("v.step", 0);
            }
    }
     ,
    getValueInputFields : function(component, event, helper) {
        let accountInputInsolvencyStatus = event.getParam('accountInputInsolvencyStatus');
        if(accountInputInsolvencyStatus === 'Insolvent') {
            component.set("v.inputValueInsolvencyEndDate",null);
            component.set("v.inputValueBankruptcyStartDate",null);
        } else if (accountInputInsolvencyStatus === 'Insolvency closed') {
            component.set("v.inputValueBankruptcyStartDate",null);
            component.set("v.inputValueInsolvencyEndDate",event.getParam('accountInputInsolvencyEndDate'));
        } else {
            component.set("v.inputValueInsolvencyEndDate",event.getParam('accountInputInsolvencyEndDate'));
            component.set("v.inputValueBankruptcyStartDate",event.getParam('accountInputBankruptcyStartDate'));
        }
        component.set("v.inputValueStatus", accountInputInsolvencyStatus);
        component.set("v.inputValueStartDate",event.getParam('accountInputInsolvencyStartDate'));
        component.set("v.inputValueJudicialAdministrator",event.getParam('accountInputJudicialAdministrator'));
        component.set("v.inputValueJudicialLiquidator",event.getParam('accountInputJudicialLiquidator'));
        component.set("v.step", event.getParam("step"));
    },
    getValueEditMode : function(component, event, helper) {
        component.set("v.step", event.getParam('stepEdit'));
    },
    cancel: function (component, event, helper) {
        //helper.handleCancel(component, helper);
        component.set("v.showCancel", true);
    },
    save: function (component, event, helper) {
        helper.createCase(component, helper);
    },
    saveDraft: function(component, event, helper) {
        component.set("v.isDraft", true);
        helper.createCase(component, event, helper);
    },
    closeCancelModal: function (component, event, helper) {
        component.set("v.showCancel", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        console.log("cancel========="+JSON.stringify(event));
        component.set("v.savingWizard", false);
        helper.redirectToDossier(component, helper);

    }
});