/**
 * Created by l.centra on 20/01/2020.
 */

({
    initialize: function (component, event, helper) {
        console.log('two');
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const fromDossier = myPageRef.state.c__editCase;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        if(fromDossier === 'edit') {
            component.set("v.editCase", true);
        }
        console.log(component.get("v.editCase"));
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        component.set("v.accountId", accountId);
        component.set("v.dossierId", dossierId);
        component.find("apexService").builder()
            .setMethod("InitializeCompanyInsolvency")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find("cookieSvc").getInteractionId(),
                "genericRequestId": genericRequestId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.account", response.account);
                //Open from Dossier
                if (response.accountId && !accountId) {
                    component.set("v.accountId", response.accountId);
                }
                component.set("v.isPersonAccount", response.isPersonAccount);
                component.set("v.recordTypeInsolvencyBankruptcy", response.recordTypeInsolvencyBankruptcy);
                if (response.companyDivisionId) {
                    component.set("v.companyDivisionId", response.companyDivisionId);
                }
                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId);
                }
                //Open from Dossier
                if (response.accountId && !accountId) {
                    self.updateUrl(component, response.accountId, dossierId);
                }
                if (response.companyDivisionName) {
                    component.set("v.companyDivisionName", response.companyDivisionName);
                    component.set("v.companyDivisionId", response.companyDivisionId);
                }
                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === "New" || dossierVal.Status__c === "Canceled") {
                        component.set("v.isClosed", true);
                    }
                }
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
                self.hideSpinner(component);
            })
            .executeAction();
    },

    showSpinner: function (component) {
        $A.util.removeClass(component.find("spinnerSection"), "slds-hide");
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find("spinnerSection"), "slds-hide");
    },
    redirectToDossier: function (component, helper) {
        const self = this;
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        self.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },


    updateUrl: function (component, accountId, dossierId) {
        const navService = component.find("navService");
        const pageReference = {
            type: "standard__component",
            attributes: {
                componentName: "c__MRO_LCP_CompanyInsolvencyWizard"
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference, true);
    },
    handleCancel: function (component, helper) {
        const self = this;
        const dossierId = component.get("v.dossierId");
        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId
            }).setResolve(function (response) {
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    createCase: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const companyDivision = component.get("v.companyDivisionId");
        const editCase = component.get("v.editCase");
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');
        let isDraft = component.get("v.isDraft");
        /**field Value from component mroInsolvencyStatusEdit**/
        let inputValueStatus = component.get("v.inputValueStatus");
        let inputValueStartDate = component.get("v.inputValueStartDate") ? component.get("v.inputValueStartDate") : '' ;
        let inputValueJudicialAdministrator = component.get("v.inputValueJudicialAdministrator");
        let inputValueInsolvencyEndDate  = component.get("v.inputValueInsolvencyEndDate") ? component.get("v.inputValueInsolvencyEndDate") : '';
        let inputValueBankruptcyStartDate = component.get("v.inputValueBankruptcyStartDate") ? component.get("v.inputValueBankruptcyStartDate") : '';
        let inputValueJudicialLiquidator = component.get("v.inputValueJudicialLiquidator");
        let inputdescription= component.get("v.description");
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("insertCase")
            .setInput({
                companyDivisionId: companyDivision,
                accountId: accountId,
                dossierId: dossierId,
                recordTypeId: component.get("v.recordTypeInsolvencyBankruptcy"),
                channelSelected: channelSelected,
                originSelected: originSelected,
                inputValueStatus: inputValueStatus,
                inputValueStartDate: JSON.stringify(inputValueStartDate),
                inputValueJudicialAdministrator: inputValueJudicialAdministrator,
                inputValueInsolvencyEndDate: JSON.stringify(inputValueInsolvencyEndDate),
                inputValueBankruptcyStartDate: JSON.stringify(inputValueBankruptcyStartDate),
                inputValueJudicialLiquidator: inputValueJudicialLiquidator,
                inputdescription: inputdescription,
                isDraft: isDraft,
                fromDossier: editCase
            }).setResolve(function (response) {
            if (!response.error) {
                self.redirectToDossier(component, helper);
                self.hideSpinner(component);
            } else {
                ntfSvc.error(ntfLib, response.errorMsg);
                return;
            }
        })
        .setReject(function (response) {
            self.hideSpinner(component, 'spinnerSection');
            const errors = response.getError();
            ntfSvc.error(ntfLib, errors[0].message);
            return;
        })
        .executeAction();
    }
});