({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.originSelected", "");
        component.set("v.channelSelected", "");
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initConsoleNavigation(component, $A.get('$Label.c.TerminationWizard'));
        helper.initialize(component, event, helper);
        helper.setCommodityFilter(component);
    },

    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected){
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step",1);
            component.set('v.disableAgree', false);
            let dossier = component.get('v.dossier');
            if (dossier && dossier.Origin__c) {
                component.set("v.step", 2);
            }
            if(component.get('v.originSelected') && component.get('v.channelSelected')) {

                helper.setChannelAndOrigin(component);
            }
        }
        if(!originSelected){
            component.set('v.disableAgree', true);
            component.set("v.step",0);
        }
    },
    searchSelectionHandler: function (component, event, helper) {
        console.log('MRO_LCP_TerminationWizard.searchSelectionHandler - Enter');
        const supplyIds = event.getParam('selected');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if(!supplyIds){
            ntfSvc.error(ntfLib, $A.get("$Label.c.InexistantSupply"));
            return;
        }
        let origin = component.get("v.originSelected");
        console.log('MRO_LCP_TerminationWizard.searchSelectionHandler - origin', origin);
        component.find('apexService').builder()
            .setMethod("getSupplyRecord")
            .setInput({
                supplyId: supplyIds,
                origin: origin
            })
            .setResolve(function (response) {

                if (!response.error) {
                   component.set("v.disconnectionCauseLabel", response.customReasons);
                   component.set("v.disconnectionCausePicklistValues", response.customReasons);
                   if(component.get("v.caseId")){
                       component.set("v.caseId", '');
                   }
                   console.log('MRO_LCP_TerminationWizard.searchSelectionHandler.setResolve - response.supplies', response.supplies);
                   component.set("v.listSupplyValue", response.supplies);
                   const caseList = component.get("v.caseTile");
                   console.log('MRO_LCP_TerminationWizard.searchSelectionHandler.setResolve - caseList', caseList);
                   if(caseList && caseList.length > 0) {
                       component.set("v.supplyCompanyDivisionId", caseList[0].CompanyDivision__c);
                   } else if (response.supplies.length > 0 && component.get("v.supplyCompanyDivisionId") == undefined) {
                       component.set("v.supplyCompanyDivisionId", response.supplies[0].CompanyDivision__c);
                   } else {
                       component.set("v.supplyCompanyDivisionId", undefined);
                   }
                   helper.handleSupplyResult(component, event, helper);
                } else {
                    console.log(response.errorMsg, response.errorTrace);
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {

//                console.log('MRO_LCP_TerminationWizard.searchSelectionHandler.setReject - Enter');
                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleCreateCase: function (component, event, helper) {
        let caseId = component.get("v.caseId");
        if(caseId){
            helper.updateCase(component, event, helper);
        } else {
           if(helper.validateFields(component, event)) {
               helper.createCase(component, event, helper);
           }
        }
    },
    handleSuccessDossier: function (component,event, helper){

        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (caseList.length !== 0) {
           for(let e in caseList){
               if(caseList.length > 1
                   && caseList[0].Reason__c !== caseList[e].Reason__c ||
                   caseList[0].EffectiveDate__c !== caseList[e].EffectiveDate__c){
                    ntfSvc.warn(ntfLib,$A.get("$Label.c.IncorrectDataToSaveWizard"));
                    return;
               }
           }
           helper.showSpinner(component, 'spinnerSectionModal');
           helper.saveWizard(component, event, helper);
        } else {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
        }
     },
     previousStep: function (component, event, helper) {
         event.preventDefault();
         const buttonPressed = event.getSource().getLocalId();
         if(buttonPressed === "returnStep2"){
            component.set("v.step", 2);
         }
     },
    handleSuccessCase: function(component, event, helper){
        helper.showSpinner(component, 'spinnerSectionModal');
        component.set("v.caseTile", event.getParam('caseTile'));
        component.set("v.showNewCase", false);
        component.set("v.osiTableView", component.get("v.caseTile").length > component.get("v.tileNumber"));
        helper.hideSpinner(component, 'spinnerSectionModal');
        component.set('v.disableOriginChannel', true);
    },
    handleEditCase: function(component, event, helper){
        component.set("v.caseId", event.getParam('recordCaseEdit'));

        let casesTile = component.get("v.caseTile");
        for(let key in casesTile){
            if(!casesTile.hasOwnProperty(key)) continue;
            if(casesTile[key].Id === event.getParam('recordCaseEdit')){
                component.set('v.disconnectionCause',casesTile[key].Reason__c);
            }
        }
        component.set("v.showNewCase", true);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    closeModel: function(component, event, helper) {
          component.set("v.isModalOpen", false);
      },
    save: function (component, event, helper) {
       component.find('sendingChannel').saveSendingChannel();
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            case 'confirmStep2':
                component.set("v.step", 3);
                component.find('sendingChannel').disableInputField(false);
                break;
            default:
                break;
        }
    },
    removeError: function (component, event, helper) {
        const labelFieldList = [];
        let valEffectiveDate = component.find("Field_EffectiveDate__c");
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }
        let valReason = component.find("Field_Reason__c");
        if (!Array.isArray(valReason)) {
            valReason = [valReason];
        }
        tmp = valReason[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('Field_Reason__c');
        }
        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
    },
    onSaveCancelReason: function (component, event, helper) {
        component.set("v.cancellationReason",event.getParam("cancelReason"));
        component.set("v.detailReason",event.getParam("detailsReason"));
        helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    },
})