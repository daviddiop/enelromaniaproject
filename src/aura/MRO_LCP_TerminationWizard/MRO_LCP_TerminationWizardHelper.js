({
    initialize: function (component, event, helper) {

        component.set('v.disableOriginChannel', false);
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const defaultDate= component.get("v.dateTermination");
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }
        component.find('apexService').builder()
            .setMethod("initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "defaultDateTermination": defaultDate,
                "genericRequestId": genericRequestId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    console.log('dossier', JSON.stringify(response.dossier));
                    component.set("v.templateId", response.templateId);
                     if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                        }
                    if (response.companyDivisionId) {
                        component.set("v.companyDivisionId", response.companyDivisionId);
                    }
                    if(!response.dossier.CustomerInteraction__c){
                        component.set("v.selectedOrigin",'Internal');
                        component.set("v.disableOrigin",true);
                        if(component.find('wizardHeader')) component.find('wizardHeader').disableOriginOption();
                    }
                    let step = 0;
                    if (response.caseTile && response.caseTile.length !== 0) {
                        let caseId = event.getParam('recordCaseEdit');
                        component.set("v.disconnectionCause", response.caseTile[0].Reason__c);
                        component.set("v.channelSelected", response.caseTile[0].Channel__c);
                        component.set("v.originSelected", response.caseTile[0].Origin);
                        step = 2;
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                        component.set('v.disableOriginChannel', true);
                    }

                    let dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }
                    let today = $A.localizationService.formatDate(response.defaultDateTermination, "YYYY-MM-DD");
                    let effectiveDate = $A.localizationService.formatDate(response.effectiveDate, "YYYY-MM-DD");


                    component.set("v.dateTermination", today );
                    component.set("v.disconnectionDate",effectiveDate);

                    component.set("v.dateCheckPentecost", response.dateCheckPentecost);
                    component.set("v.dateCheckEaster", response.dateCheckEaster);

                    component.set("v.step", step);
                    component.set("v.account", response.account);
                    component.set("v.accountPersonRT", response.accountPersonRT);
                    component.set("v.accountPersonProspectRT", response.accountPersonProspectRT);
                    component.set("v.accountBusinessRT", response.accountBusinessRT);
                    component.set("v.accountBusinessProspectRT", response.accountBusinessProspectRT);
                    component.set("v.disconnectionCauseLabel", response.customReasons);
                    component.set("v.disconnectionDateLabel", response.disconnectionDateLabel);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    component.set("v.terminationEle", response.terminationELE);
                    component.set("v.terminationGas", response.terminationGAS);
                    component.set("v.templateId", response.templateId);
                    if(component.get("v.disconnectionCausePicklistValues").length === 0){
                      component.set("v.disconnectionCausePicklistValues", response.customReasons);
                    }


                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    if(response.accessDenied){
                        self.goToAccount(component, event, helper);
                    }
                    return;
                }
            })
            .setReject(function (error) {
            })
            .executeAction()
    },
    setCommodityFilter: function (component) {
        let commodityEle = 'Electric';
        let commodityGas = 'Gas';
        let commodityConditionEle ="RecordType.DeveloperName='"+commodityEle+"'";
        let commodityConditionGas ="RecordType.DeveloperName='"+commodityGas+"'";
        let orConditions = [];
        orConditions.push(commodityConditionEle);
        orConditions.push(commodityConditionGas);
        component.set("v.orConditions",orConditions);
    },
    handleSupplyResult: function (component, event, helper) {
        component.set('v.disconnectionCause', '');

        const companyDivisionId = component.get("v.supplyCompanyDivisionId");
        console.log('MRO_LCP_TerminatinWizard.handleSupplyResult companyDivisionId', companyDivisionId);
        const searchedSupplyFieldsList = component.get("v.listSupplyValue");
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        // [START FIX -- 19.11.2020 salvatore.pignanelli@webresults.it - Retrieve origin selected - ENLCRO-1792 ]
        let originSelected = component.get('v.originSelected');
        // [END FIX -- 19.11.2020 salvatore.pignanelli@webresults.it - Retrieve origin selected - ENLCRO-1792 ]

        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let errSupplyNonDisconnectable = [];
        let errDifferentCompanyDivision = [];
        let validSupplies = [];
        for (let i = 0; i < searchedSupplyFieldsList.length; i++) {

            let validSupply = true;
            let oneSupply = searchedSupplyFieldsList[i];
            if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                continue;
            }
            // [START FIX -- 19.11.2020 salvatore.pignanelli@webresults.it - If the request origin != Internal, we skip
            //the check on oneSupply.NonDisconnectable__c - ENLCRO-1792 ]
            if (oneSupply.NonDisconnectable__c === true && originSelected == 'Internal') {
                errSupplyNonDisconnectable.push(oneSupply.Name);
                continue;
            }
            // [END FIX -- 19.11.2020 salvatore.pignanelli@webresults.it - If the request origin != Internal, we skip
            //the check on oneSupply.NonDisconnectable__c - ENLCRO-1792 ]
            console.log('MRO_LCP_TerminatinWizard.handleSupplyResult current supply companyDivisionId',
                    oneSupply.CompanyDivision__c);
            if (companyDivisionId !== undefined && oneSupply.CompanyDivision__c != companyDivisionId) {
                errDifferentCompanyDivision.push(oneSupply.Name);
                continue;
            }
            for (let j = 0; j < caseList.length; j++) {
                if (oneSupply.Id === caseList[j].Supply__c) {
                    errSupplyAlreadySelected.push(oneSupply.Name);
                    validSupply = false;
                    break;
                }
            }
            if (validSupply) {
                validSupplies.push(oneSupply);
            }
        }
        if (errSupplyAlreadySelected.length !== 0) {
            let messages = errSupplyAlreadySelected.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
        }
        if (errCurrentSupplyIsNotValid.length !== 0) {
            let messages = errCurrentSupplyIsNotValid.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
        }
        if (errSupplyNonDisconnectable.length !== 0) {
            let messages = errSupplyNonDisconnectable.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.NondisconnectableSupply") + ' - ' + messages);
        }
        if (errDifferentCompanyDivision.length !== 0) {
            let messages = errDifferentCompanyDivision.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyWithDifferentCompany") + ' - ' + messages);
        }

        console.log('MRO_LCP_TerminatinWizard.handleSupplyResult validSupplies', validSupplies.length,
                JSON.stringify(validSupplies));

        component.set("v.searchedSupplyFields", validSupplies);

        component.set("v.showNewCase", (validSupplies.length !== 0));


    },
    createCase: function (component, event, helper) {
        const self = this;
        const disconnectionCause = component.get("v.disconnectionCause");
        const disconnectionDate = component.get("v.disconnectionDate");
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const originSelected = component.get("v.originSelected");
        const channelSelected = component.get("v.channelSelected");
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((disconnectionCause.length === 0) || (disconnectionDate == null)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
       self.showSpinner(component, 'spinnerSectionModal');
       component.find('apexService').builder()
           .setMethod("createCase")
           .setInput({
               disconnectionCause: disconnectionCause,
               disconnectionDate: JSON.stringify(disconnectionDate),
               searchedSupplyFieldsList: JSON.stringify(searchedSupplyFieldsList),
               caseList: JSON.stringify(caseList),
               accountId: accountId,
               dossierId: dossierId,
               originSelected: originSelected,
               channelSelected: channelSelected
           }).setResolve(function (response) {
           self.hideSpinner(component, 'spinnerSectionModal');
           if (!response.error) {

               if (response.caseTile.length !== 0) {
                   component.set('v.disableOriginChannel', true);
                   component.set("v.companyDivisionId", response.caseTile[0].CompanyDivision__c);
                   component.set("v.step", 2);
                   component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                   component.set("v.showNewCase", false);
                   helper.initialize(component, event, helper);
                   component.set("v.caseTile", response.caseTile);
            }
           } else {
               component.set("v.showNewCase", false);
               ntfSvc.error(ntfLib, response.errorMsg);
           }
       })
           .setReject(function (error) {
           })
           .executeAction();
    },
    resetCase: function (component, event, helper){
        component.find("tile");
    },
    updateCase: function (component, event, helper) {
        const self = this;

        const caseList = component.get("v.caseTile");
        let caseId = component.get("v.caseId");
        const disconnectionCause = component.get("v.disconnectionCause");
        const disconnectionDate = component.get("v.disconnectionDate");

        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const items = [];

        component.find('apexService').builder()
            .setMethod("updateCase")
            .setInput({
                caseId: caseId,
                 caseList: JSON.stringify(caseList),
                 disconnectionDate: JSON.stringify(disconnectionDate),
                 disconnectionCause : disconnectionCause
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSectionModal');
                if (!response.error) {

                   component.set("v.caseTile", items);
                   component.set("v.showNewCase", false);
                   component.set("v.caseTile", response.updatedCaseTile);
                   component.set("v.caseId", '');
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

            })
            .setReject(function (error) {
                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    handleSuccessCase: function (component, event, helper) {
        helper.initialize(component, event, helper);
       // let messageSusscess =event.getParam('message');
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const cancelReason = component.get("v.cancellationReason");
        const detailReason = component.get("v.detailReason");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                cancelReason: cancelReason,
                detailReason: detailReason
            }).setResolve(function (response) {
            self.hideSpinner(component, 'spinnerSection');
            if (!response.error) {
                component.find('cookieSvc').clearInteractionId();
                self.redirectToDossier(component, helper);
            } else {
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_TerminationWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {

        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        if (items.length === 0) {
            component.set('v.disableOriginChannel', false);
            component.set("v.step", 2);
        }
        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));

    },
    redirectToDossier: function (component, helper) {
        component.find('cookieSvc').clearInteractionId();
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    validateFields: function (component, event) {
        const labelFieldList = new Array();
        let valEffectiveDate = component.find("Field_EffectiveDate__c");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }
        let valReason = component.find("Field_Reason__c");
        if (!Array.isArray(valReason)) {
            valReason = [valReason];
        }
        tmp = valReason[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_Reason__c');
        }

        if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService")
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        component.find('cookieSvc').clearInteractionId();
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                    helper.hideSpinner(component, 'spinnerSectionModal');
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = "Lightning Experience | Salesforce";
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    },
    disableCardWizard: function (component, disabled) {
        component.set('v.disableWizard', disabled);
        if (disabled === true) {
            $A.util.addClass(component.find('boxWizard'), 'slds-theme_shade');
            $A.util.addClass(component.find('boxTemplateContainer'), 'slds-theme_shade');
            $A.util.addClass(component.find('boxWizard'), 'wr-pointer-event_none');
        }
    },
    saveWizard: function (component, event, helper) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');
        const caseList = component.get("v.caseTile");

        let companyDivisionId = component.get("v.supplyCompanyDivisionId");
        if(companyDivisionId == undefined && caseList && caseList.length > 0) {
            companyDivisionId = caseList[0].CompanyDivision__c;
        }
        console.log('MRO_LCP_TerminationWizard.saveWizard companyDivisionId', companyDivisionId);

        component.find('apexService').builder()
            .setMethod("SaveWizard")
            .setInput({
                dossierId: dossierId,
                caseList: JSON.stringify(caseList),
                channelSelected: channelSelected,
                originSelected: originSelected,
                companyDivisionId: companyDivisionId
            }).setResolve(function (response) {
            if (!response.error) {
                component.find('cookieSvc').clearInteractionId();
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
             self.hideSpinner(component, 'spinnerSection');
    },
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
});