/**
 * Created by Vlad Mocanu on 20/01/2020.
 */

({
    init: function (component, event, helper) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("getListOfMostUsedLinkIds")
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                } else {
                    component.set('v.mostUsedLinkIds', response.idList)
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },
    handleServiceLinkClickLtgEvent: function (component, event) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let linkId = event.getParam("linkId");
        let recordId = event.getParam("recordId");

        component.find('apexService').builder()
            .setMethod("upsertMostUsedLink")
            .setInput({
                'linkId': linkId,
                'recordId': recordId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                } else {
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    }
});