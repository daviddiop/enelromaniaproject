/**
 * Created by goudiaby on 01/10/2019.
 */

({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;

        component.set("v.accountId", accountId);
        component.set("v.dossierId", dossierId);

        helper.initConsoleNavigation(component, $A.get("$Label.c.ContractMerge"));
        helper.initialize(component);
    },
    save: function (component, event, helper) {
        helper.updateCases(component, helper);
    },
    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    nextStep: function (component, event,helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        if (buttonPressed === "confirmStep1") {
            if (component.get("v.caseTile").length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredSupply"));
                return;
            }

            let companyDivisionId = component.get("v.caseTile")[0].CompanyDivision__c;
            let mixedCompanyFound = false;
            component.get("v.caseTile").forEach(myCase => {
                if (myCase.CompanyDivision__c !== companyDivisionId) {
                    mixedCompanyFound = true;
                    component.set("v.step", 1);
                    component.set("v.divisionId", "");
                    ntfSvc.error(ntfLib, $A.get('$Label.c.AllTheSuppliesShouldBeInSameCompany'));
                }
            });
            if (!mixedCompanyFound) {
                let cases = component.get("v.caseTile");
                component.set("v.step", 2);
                helper.reloadAll(component, cases);
            }
        } else if (buttonPressed === "confirmStep2") {
            /*
            if ((!component.get("v.contractId")) && (component.get("v.customerSignedDate") === null)) {
                ntfSvc.error(ntfLib,"Customer signed date is required");
                return;
            }
            */
            component.set("v.step", 3);
            //helper.updateContractAndContractSignedDateOnOpportunity(component);

        } else if (buttonPressed === 'confirmStep3') {
            if (!component.get("v.contractAccountId")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountRequired"));
            }
        }

    },
    editStep: function (component, event) {
        const buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep0") {
            component.set("v.step", 0);
        } else if (buttonPressed === "returnStep1") {
            component.set("v.step", 1);
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 2);
        }
    },
    getContractAccountId: function (component, event) {
        let contractAccountSelected = event.getParam("contractAccountRecordId");
        component.set("v.contractAccountId", contractAccountSelected);
    },
    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        component.set("v.contractId", contractSelected);

        /*
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        component.set("v.customerSignedDate", customerSignedDate);
        */
    },
    handleSupplyResult: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        let suppliesValues = event.getParam("supplyFieldsValue");
        let caseList = component.get("v.caseTile");
        for (let i = 0; i < suppliesValues.length; i++) {
            for (let j = 0; j < caseList.length; j++) {
                if (suppliesValues[i].Id === caseList[j].Supply__c) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SupplyAlreadySelected"));
                    return;
                }
            }
        }
        helper.createCase(component, event, helper, suppliesValues);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    }
});