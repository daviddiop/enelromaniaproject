/**
 * Created by goudiaby on 01/10/2019.
 */

({
    initialize: function (component) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod('Initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            })
            .setResolve(function (response) {
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.caseTile", response.cases);
                component.set("v.contractId", response.contractIdFromOpp);
                //component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.isClosed", false);

                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId);
                }

                let step = 0;
                if (response.cases.length !== 0) {
                    step = 1;
                    self.reloadAll(component, response.cases);
                }

                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                    }
                }

                component.set("v.accountId", accountId);
                component.set("v.step", step);
                if (response.cases) {
                    component.set("v.osiTableView", response.cases.length > component.get("v.tileNumber"));
                }

                let topElement = component.find("topElement").getElement();
                topElement.scrollIntoView({behavior: "instant", block: "end"});
                self.hideSpinner(component);
                self.resetSupplyForm(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateCases: function (component, helper) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const dossierId = component.get("v.dossierId");
        const casesCreated = component.get("v.caseTile");
        const contractId = component.get("v.contractId");

        const contractAccountId = component.get("v.contractAccountId");
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("UpdateCases")
            .setInput({
                caseList: JSON.stringify(casesCreated),
                dossierId: dossierId,
                contractId: contractId,
                contractAccountId: contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createCase: function (component, event, helper, suppliesFromSearch) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        let supplies = suppliesFromSearch;
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("CreateCase")
            .setInput({
                supplies: JSON.stringify(supplies),
                caseList: JSON.stringify(caseList),
                accountId: accountId,
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    if (response.cases.length !== 0) {
                        component.set("v.step", 1);
                        component.set("v.osiTableView", response.cases.length > component.get("v.tileNumber"));
                    }
                    self.resetSupplyForm(component);
                    component.set("v.caseTile", response.cases);
                    //self.reloadAll(response.cases);
                } else {
                    ntfSvc.error(ntfLib, response["errorMsg"]);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("CancelProcess")
            .setInput({
                caseList: JSON.stringify(caseList),
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        component.set("v.caseTile", items);
        if (items && items.length === 0){
            component.set("v.divisionId", "");
            helper.reloadAll(component,items);
        }
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    updateUrl: function (component, accountId, dossierId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__ContractMergeWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference, true);
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    /*if (window.localStorage && component.get("v.step") === 3 ) {
                        if (!window.localStorage['loaded']) {
                            window.localStorage['loaded'] = true;
                            helper.refreshFocusedTab(component);
                        } else {
                            window.localStorage.removeItem('loaded');
                        }
                    }*/
                    if (!window.location.hash && component.get("v.step") === 3) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce");
    },
    closeFocusedTab: function (component) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const {getFocusedTabInfo, refreshTab} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    }
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reload();
            } else {
                contractAccountComponent.reload();
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent) {
            if (supplySearchComponent instanceof Array) {
                let supplyComponentToObj = Object.assign({}, supplySearchComponent);
                supplyComponentToObj[0].resetForm();
            } else {
                supplySearchComponent.resetForm();
            }
        }
    },
    reloadAll: function (component, cases) {
        let self = this;
        if (cases){
            const myCase = cases[0];
            if (myCase) {
                component.set("v.divisionId", myCase.CompanyDivision__c);
            }
        }
            self.resetSupplyForm(component);
            self.reloadContractAddition(component);
            self.reloadContractAccount(component);
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    }
    /*,
    updateContractAndContractSignedDateOnDossier: function (component) {
        let dossierId = component.get("v.dossierId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let customerSignedDate = component.get("v.customerSignedDate");
        component
          .find("apexService")
          .builder()
          .setMethod("updateContractAndContractSignedDateOnDossier")
          .setInput({
              dossierId: dossierId,
              contractId: contractId,
              customerSignedDate: customerSignedDate
          })
          .setResolve(function (response) {
              //self.hideSpinner(component);
          })
          .setReject(function (error) {
              ntfSvc.error(ntfLib, error);
          })
          .executeAction();
    }*/
});