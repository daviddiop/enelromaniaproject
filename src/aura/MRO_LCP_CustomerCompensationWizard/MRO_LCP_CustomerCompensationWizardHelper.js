/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 09.04.20.
 */
({
    initialize: function (component) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId,
            })
            .setResolve(function (response) {
                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId);
                    return;
                }

                component.set('v.requestTypeOptions', response.requestTypeOptions.map(function (v) {
                    return {label: v.key, value: v.value};
                }));

                if(response.caseRecord) {
                    component.set("v.caseRecord", response.caseRecord);
                    component.set("v.caseId", response.caseRecord.Id);
                    component.set('v.supplyFromId', response.caseRecord.Supply__c);
                    component.set('v.contractAccountFromId', response.caseRecord.ContractAccount__c);
                    component.set('v.contractAccountToId', response.caseRecord.ContractAccount2__c);
                    self.setRequestType(component, response.caseRecord.SubProcess__c);
                }

                self.setStep(component, response);

                component.set("v.startDate", response.startDate);
                component.set("v.endDate", response.endDate);
                component.set("v.isClosed", response.isClosed);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    setChannelAndOrigin: function (component) {
        component.find('apexService').builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
            })
            .executeAction();
    },

    upsertCase: function (component, callback) {
        let that = this;
        that.showSpinner(component);

        let input = {
            dossierId: component.get("v.dossierId"),
            originSelected: component.get("v.originSelected"),
            channelSelected: component.get("v.channelSelected"),
            requestType: component.get("v.requestType"),
            supplyFromId: component.get("v.supplyFromId"),
            supplyToId: component.get("v.supplyToId"),
        };
        let selectedInvoices = component.get('v.selectedInvoiceList');
        if(selectedInvoices){
            selectedInvoices = selectedInvoices.map(function(v){
                return v.invoiceSerialNumber + v.invoiceNumber;
            });
            input.selectedInvoices = JSON.stringify(selectedInvoices).replace(/[\[\]"'"]+/g, '' );
        }
        let caseId = component.get('v.caseId');
        if(caseId){
            input.caseId = caseId;
        }
        component.find('apexService').builder()
            .setMethod("upsertCase")
            .setInput(input)
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (!response.error) {
                    component.set('v.contractAccountFromId', response.caseRecord.ContractAccount__c);
                    component.set('v.contractAccountToId', response.caseRecord.ContractAccount2__c);
                    component.set('v.supplyFromId', response.caseRecord.Supply__c);
                    component.set("v.caseRecord", response.caseRecord);
                    component.set("v.caseId", response.caseRecord.Id);
                    if(callback){
                        callback();
                    }
                }

            })
            .setReject(function (error) {
                that.hideSpinner(component);
            })
            .executeAction();
    },

    redirectToDossier: function (component) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        this.redirect(component, pageReference);
    },

    performSaveProcess: function(component, helper, isDraft){
        let dossierId = component.get("v.dossierId");

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("saveProcess")
            .setInput({
                dossierId: dossierId,
                caseId: component.get("v.caseId"),
                supplyId: component.get("v.supplyFromId"),
                isDraft: isDraft
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                helper.redirectToDossier(component);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },

    handleSave: function(component, helper){
        helper.performSaveProcess(component, helper, false);
    },

    handleSaveDraft: function(component, helper){
        helper.performSaveProcess(component, helper, true);
    },

    setRequestType: function (component, requestType) {
        if(!requestType){
            requestType = component.get('v.requestType');
        }else{
            component.set('v.requestType', requestType);
        }

        let requestTypeOptions = component.get('v.requestTypeOptions');
        let showContractAccountToSelection = requestType === requestTypeOptions[1].value;
        component.set('v.showContractAccountFromSelection', !!requestType);
        component.set('v.showContractAccountToSelection', showContractAccountToSelection);
    },

    setInvoicesTitle: function(component){
        let showContractAccountToSelection = component.get('v.showContractAccountToSelection');
        let caseRecord = component.get('v.caseRecord');

        if(!showContractAccountToSelection && caseRecord && caseRecord.ContractAccount__r){
            component.set('v.invoicesTitle', 'Invoices Inquiry for ' + caseRecord.ContractAccount__r.Name);
        }

        if(showContractAccountToSelection && caseRecord && caseRecord.ContractAccount2__r){
            component.set('v.invoicesTitle', 'Invoices Inquiry for ' + caseRecord.ContractAccount2__r.Name);
        }
    },

    setStep: function (component, response) {
        let step = 8;
        if (!response.dossier || !response.dossier.SendingChannel__c) {
            step = 7;
        }
        if (!response.caseRecord || !response.caseRecord.Amount__c) {
            step = 6;
        }
        if (!response.caseRecord || !response.caseRecord.MarkedInvoices__c) {
            step = 5;
        }
        if (!response.caseRecord || component.get('v.showContractAccountToSelection') && !response.caseRecord.ContractAccount2__c){
            step = 3;
        }
        if (!response.caseRecord || !response.caseRecord.ContractAccount__c){
            step = 2;
        }
        if (!response.dossier || !response.dossier.Channel__c || !response.dossier.Origin__c || !response.caseRecord || !response.caseRecord.SubProcess__c) {
            step = 1;
        }
        this.changeStep(component, this, step);
    },
    updateUrl: function (component, accountId, dossierId) {
        let self = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_CustomerCompensationWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        self.redirect(component, pageReference, true);
    },

    changeStep: function (component, helper, stepNumber, isNext) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.set('v.disableOriginChannel', stepNumber > 1);

        if(stepNumber > 1 && stepNumber < 5 && isNext){
            helper.showSpinner(component);
            helper.upsertCase(component, function(){
                helper.setRequestType(component);
                if(!component.get('v.showContractAccountToSelection') && stepNumber === 3){
                    stepNumber++;
                }
                helper.changeStep(component, helper, stepNumber);
            });
            return;
        }

        if(stepNumber === 5 && isNext){
            helper.setInvoicesTitle(component);
            if(component.find("periodSelection")) component.find("periodSelection").onConfirmSelection();
        }

        if(stepNumber === 6 && isNext){
            component.find('invoiceInquiry').getSelectedInvoices();
        }

        if (stepNumber === 7 && isNext) {
            let caseDescriptionFieldValue = component.find('caseDescriptionField').get('v.value');
            if (!caseDescriptionFieldValue || (isNaN(caseDescriptionFieldValue) && caseDescriptionFieldValue.trim() === "")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                return;
            }
            component.find('caseEditForm').submit();
            return;
        }

        if (stepNumber === 8 && isNext) {
            component.find('sendingChannelSelection').saveSendingChannel();
            return;
        }

        component.set("v.step", stepNumber);
    },

    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference, overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },

    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
})