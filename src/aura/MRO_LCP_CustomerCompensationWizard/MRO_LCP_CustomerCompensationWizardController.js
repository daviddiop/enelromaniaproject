/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 09.04.20.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        helper.initialize(component);
        helper.initConsoleNavigation(component, 'Customer Compensation');
    },

    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            helper.setChannelAndOrigin(component);
            if (component.get("v.step") === 0) {
                helper.changeStep(component, helper, 1);
            }
        }
    },

    handleRequestTypeChange: function (component, event, helper) {
        component.set('v.requestType', event.getParam("value"));
    },

    handleSendingChannelSelection: function (component, event, helper) {
        helper.changeStep(component, helper, 8);
    },

    handleSupplyFromResult: function (component, event, helper) {
        const selectedSupply = event.getParam("selected");
        if(!component.get('v.showContractAccountToSelection')) {
            const selectedSupplyObj = event.getParam("supplies");
            if (selectedSupplyObj && selectedSupplyObj[0] && selectedSupplyObj[0].ContractAccount__r && selectedSupplyObj[0].ContractAccount__r.IntegrationKey__c) {
                component.set('v.billingAccountNumber', selectedSupplyObj[0].ContractAccount__r.IntegrationKey__c);
            }
        }
        component.set("v.supplyFromId", selectedSupply[0]);
    },

    handleSelectedRange: function (component, event) {
        component.set("v.startDate", event.getParam("startDate"));
        component.set("v.endDate", event.getParam("endDate"));
    },

    handleSupplyToResult: function (component, event, helper) {
        const selectedSupply = event.getParam("selected");

        const selectedSupplyObj = event.getParam("supplies");
        if(selectedSupplyObj && selectedSupplyObj[0] && selectedSupplyObj[0].ContractAccount__r && selectedSupplyObj[0].ContractAccount__r.IntegrationKey__c) {
            component.set('v.billingAccountNumber', selectedSupplyObj[0].ContractAccount__r.IntegrationKey__c);
        }
        component.set("v.supplyToId", selectedSupply[0]);
    },

    handleSupplyFromDelete: function (component) {
        component.set('v.supplyFromId', null);
    },

    handleSupplyToDelete: function (component) {
        component.set('v.supplyToId', null);
    },

    handleInvoiceSelect: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedInvoiceList = event.getParam("selectedInvoiceList");
        if (selectedInvoiceList.length === 0) {
            ntfSvc.warn(ntfLib, $A.get("$Label.c.UnmarkedInvoice"));
            return;
        }
        component.set('v.selectedInvoiceList', selectedInvoiceList);
        helper.showSpinner(component);
        helper.upsertCase(component, function () {
            helper.changeStep(component, helper, 6);
        });
    },

    handleCaseSuccess: function(component, event, helper){
        helper.changeStep(component, helper, 7);
    },

    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().get('v.name');
        let stepNumber = +(buttonId.replace('confirmStep', '')) + 1;
        helper.changeStep(component, helper, stepNumber, true);
    },

    editStep: function (component, event, helper) {
        let buttonId = event.getSource().get('v.name');
        let stepNumber = +(buttonId.replace('editStep', ''));
        helper.changeStep(component, helper, stepNumber);
    },

    cancel: function (component, event, helper) {
        component.set('v.showCancelBox', true);
    },
    onCloseCancelBox: function(component){
        component.set('v.showCancelBox', false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component);
    },

    save: function (component, event, helper) {
        helper.handleSave(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.handleSaveDraft(component, helper);
    },


})