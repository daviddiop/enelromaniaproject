/**
 * Created by goudiaby on 28/05/2019.
 */
({
    init: function (component, event, helper) {

        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const opportunityId = myPageRef.state.c__opportunityId;
        helper.hideSpinner(component);
        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        helper.initConsoleNavigation(component, $A.get('$Label.c.ProductChange'));
        helper.initialize(component);
    },
    cancel: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty);
    },
    save: function (component, event, helper) {
        component.set("v.isSaving", true);
        helper.createPrivacyChangeRecord(component);
    },
    saveDraft: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.createPrivacyChangeRecord(component);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if (!component.get("v.opportunityServiceItems") || component.get("v.opportunityServiceItems").length === 0) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
                    return;
                }
                component.set("v.step", 2);
                break;
            case 'confirmStep2':
                let prdList = component.get("v.opportunityLineItems");
                if (!prdList && prdList.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct"));
                    return;
                }
                const utilityPrds = prdList.filter(oli => oli.Product2.RecordType.DeveloperName === 'Utility');
                if (!utilityPrds || utilityPrds.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct"));
                    return;
                }
                let oli = utilityPrds[0];
                helper.linkOliToOsi(component, oli);
                component.set("v.step", 3);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            default:
                break;
        }
    },
    handleSupplyResult: function (component, event, helper) {
        helper.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let selectedSupply = event.getParam("selectedSupply");
        let selectedSupplyId = selectedSupply.Id;
        let servicePointCode = selectedSupply.ServicePoint__r.Code__c;
        let servicePointCurrentSupply = selectedSupply.ServicePoint__r.CurrentSupply__c;
        let servicePointId = selectedSupply.ServicePoint__c;
        let market = selectedSupply.Market__c;
        let serviceSite = selectedSupply.ServiceSite__c;
        let contract = selectedSupply.Contract__c;
        let contractAccount = selectedSupply.ContractAccount__c;
        let product = selectedSupply.Product__c;

        let osiList = component.get("v.opportunityServiceItems");

        for (let j = 0; j < osiList.length; j++) {
            if (servicePointCode === osiList[j].ServicePointCode__c) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, $A.get("$Label.c.SupplyAlreadySelected"));
                return;
            }
        }

        if (servicePointCurrentSupply !== selectedSupplyId || !servicePointCurrentSupply) {
            helper.hideSpinner(component);
            ntfSvc.error(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid"));
            return;
        }

        if (serviceSite == null) {
            helper.hideSpinner(component);
            ntfSvc.error(ntfLib, $A.get("$Label.c.ServiceSiteNotExist"));
            return;
        }
        if (product == null) {
            helper.hideSpinner(component);
            ntfSvc.error(ntfLib, $A.get("$Label.c.ProductNotExist"));
            return;
        }
        if (contract == null) {
            helper.hideSpinner(component);
            ntfSvc.error(ntfLib, $A.get("$Label.c.ContractNotExist"));
            return;
        }

        if (contractAccount == null) {
            helper.hideSpinner(component);
            ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountNotExist"));
            return;
        }

        component.set("v.searchedPointId", servicePointId);
        component.set("v.searchedPointCode", servicePointCode);
        component.set("v.market", market);
        component.set("v.serviceSite", serviceSite);
        component.set("v.product", product);
        component.set("v.contract", contract);
        component.set("v.contractAccount", contractAccount);
        component.set("v.showNewOsi", true);

    },
    handleSupplySearch: function (component, event, helper) {
        //helper.hideSpinner(component);
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    saveOsiList: function (component, event, helper) {
        helper.saveOpportunity(component, "Qualification");
        helper.setPercentage(component, "Qualification");
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedPointId", "");
        component.set("v.searchedPointCode", "");
        helper.resetSupplyForm(component);
    },
    handleNewOsi: function (component, event, helper) {

        let osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
        helper.hideSpinner(component);

    },
    handleProductConfigClick: function (component, event, helper) {

        helper.saveOpportunity(component, "Proposal/Price Quote", helper.openProductSelection);
        helper.setPercentage(component, "Proposal/Price Quote");
    },
    handleOsiDelete: function (component, event, helper) {
        const osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        const items = [];
        for (let i = 0; i < osiList.length; i++) {
            if (osiList[i].Id !== osiId) {
                items.push(osiList[i]);
            }
        }

        component.set("v.opportunityServiceItems", items);

        if (items.length === 0) {
            component.set("v.step", 0);
            helper.resetSupplyForm(component);
        }
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
        component.set("v.dontProcess", event.getParam('dontProcess'));
        if (!component.get("v.isSaving")) {
            helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToOppty);
        }
        if (component.get("v.isSaving")) {
            if (component.get("v.dontProcess")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
                $A.get('e.force:refreshView').fire();
                return;
            }
            helper.saveOpportunity(component, "Closed Won", helper.redirectToOppty);
        }
    }
})