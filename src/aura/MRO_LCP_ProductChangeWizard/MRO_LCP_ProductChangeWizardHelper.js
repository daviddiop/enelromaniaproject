({
    initialize: function (component) {
        const self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        let activityId = myPageRef.state.c__activityId;
        const templateId = myPageRef.state.c__templateId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        //self.showSpinner(component);
        component.set("v.opportunityId", "");
        component.set("v.accountId", accountId);
        component.set('v.interactionId', component.find('cookieSvc').getInteractionId());
        component.set("v.contractId", "");
        component.set("v.originSelected","");
        component.set("v.channelSelected","");
        component.set("v.subProcess","");
        component.set("v.isMarketChange", false);
        component.set("v.companyDivisionId", "");
        component.set("v.market", "");
        component.set("v.andConditions", []);
        component.set("v.selectedCommodity","");
        component.set("v.inEnelArea",false);
        component.set("v.contractType", "");
        component.set("v.contractAccountMarket","");

        component.set("v.metersJson","");
        component.set("v.metersInfoMap",{});
        component.set("v.metersInfo",[]);
        component.set("v.metersCodes",[]);
        component.set("v.isEnelDistributor",false);
        //component.set("v.consumptionType",""); //AS: new version
        component.set("v.productRateType","");
        //component.set('v.consumptionConventionsDisabled', false); //AS: new version
        //component.set('v.updateConsumptions', false); //AS: new version

        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "activityId": activityId,
                "templateId": templateId,
                "interactionId": component.get('v.interactionId'),
                "genericRequestId" :genericRequestId
            })
            .setResolve(function (response) {
                //AS: aggiunto da transfer, verificare
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                if (response.opportunityId && !opportunityId) {
                    // reload the page with opportunityId param and reRun init method
                    self.updateUrl(component, accountId, response.opportunityId, response.dossierId, activityId, response.templateId);
                    return; //AS: aggiunto da transfer e swin
                }

                component.set("v.accountId",response.accountId);
                component.set("v.account", response.account);
                component.set('v.isBusinessClient', response.isBusiness);
                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunity", response.opportunity);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.templateId", response.templateId);
                component.set("v.productTypeSingleRate", response.productTypeSingleRate);
                component.set("v.productTypeDualRate", response.productTypeDualRate);
                let singleRate = component.get("v.productTypeSingleRate");
                let dualRate = component.get("v.productTypeDualRate");
                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                component.set('v.salesSupportUser', response.salesSupportUser);
                component.set("v.salesUnit", response.salesUnit);
                component.set("v.salesUnitId", response.salesUnit);
                component.set("v.userDepartment", response.userDepartment);
                component.set("v.companySignedId", response.companySignedId);
                component.set("v.salesman", response.salesman);
                component.set("v.user", response.user);
                component.set("v.isPvcActivity", response.isPvcActivity);

                if(component.get('v.opportunity') && component.get('v.opportunity').RequestedStartDate__c){
                    component.set('v.requestedStartDate',component.get('v.opportunity').RequestedStartDate__c);
                }
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                    component.set("v.originContractId", response.originContractId);
                    let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                    component.set('v.streetName',pointStreetId);
                    if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                        let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                        component.set("v.vatNumber", vatNumber)
                        let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                        component.set("v.selfReadingEnabled", selfReadingEnabled)
                    }
                    if(component.get("v.opportunityServiceItems")[0].ServicePoint__c) {
                        let inEnelArea = response.opportunityServiceItems[0].ServicePoint__r.CurrentSupply__r.InENELArea__c;
                        component.set("v.inEnelArea", inEnelArea);
                    }
                }
                let opportunityVal = response.opportunity;
                if (opportunityVal) {
                    if (opportunityVal.StageName === 'Closed Lost' || opportunityVal.StageName === 'Quoted') {
                        component.set("v.isClosed", true);
                    }
                    if (opportunityVal.Origin__c) {
                        component.set("v.originSelected", opportunityVal.Origin__c);
                    }
                    if (opportunityVal.Channel__c) {
                        component.set("v.channelSelected", opportunityVal.Channel__c);
                    }
                    if (opportunityVal.ContractName__c) {
                        component.set("v.contractName", opportunityVal.ContractName__c);
                    }
                    if (opportunityVal.ContractType__c) {
                        component.set("v.contractType", opportunityVal.ContractType__c);
                    }else {
                        component.set("v.contractType", response.defaultContractType);
                    }
                }

                if(component.get("v.userDepartment") && component.get("v.channelSelected") === "Direct Sales (KAM)"){
                    component.set("v.salesUnitId", component.get("v.userDepartment"));
                    component.set("v.salesUnit", null);
                    component.set("v.showUserDepartment", true);
                }
                if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                    component.set("v.userDepartment", '');
                }
                if(response.salesUnitAccount && !component.get("v.salesUnit")){
                    component.set("v.salesUnit", response.salesUnitAccount);
                    if(component.get("v.salesUnit").SalesDepartment__c === component.get("v.channelSelected")) {
                        let salesUnitAccountId = component.get("v.salesUnit").Id;
                        component.set("v.salesUnitId", salesUnitAccountId);
                    }
                    else{ //user with salesUnitId but SalesDepartment not matching with selected channel
                        component.set("v.salesUnit", null);
                    }
                }
                if(component.get("v.channelSelected") !== "Indirect Sales" && !component.get("v.companySignedId")){
                    if(response.salesUnitAccount && !component.get("v.salesUnit")){//user with salesUnitId but SalesDepartment not matching with selected channel
                        component.set("v.companySignedId", "");
                    }
                    else if(component.get("v.channelSelected") === "Direct Sales (KAM)" && !component.get("v.salesUnit") && !component.get("v.userDepartment")){//channel direct sales and user without salesUnitId and not in KamUserGroup
                        component.set("v.companySignedId", "");
                    }
                    else if(component.get("v.channelSelected") === "Back Office Sales" || component.get("v.channelSelected") === "Back Office Customer Care"){
                        component.set("v.companySignedId", "");
                    }
                    else{
                        let userId = component.get("v.user").Id;
                        component.set("v.companySignedId", userId);
                    }
                }

                component.set("v.expirationDate", response.expirationDate);
                component.set("v.selectedCommodity", response.commodity);
                component.set("v.commodityGas", response.commodityGas);
                component.set("v.commodityElectric", response.commodityElectric);
                component.set("v.companyDivisionId", response.companyDivisionId);
                component.set('v.pointAddressProvince',response.companyDivisionId);
                if (response.market) {
                    component.set('v.market', response.market);
                }

                component.set("v.subProcess", response.subProcess);
                if(component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated' || component.get("v.subProcess") === 'ChangeMarketFromRegulatedToFree'){ //Market Change
                    component.set("v.isMarketChange", true);
                    //only New contract option is shown
                    component.set("v.contractId", '');
                    component.set('v.hideNewContract', false);
                    if(component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated'){
                        component.set("v.market", "Free");
                        component.set("v.andConditions", "Market__c='Free'");
                        component.set('v.contractAccountMarket', 'Regulated');
                        component.set('v.hideContractList', true);
                    }
                    else if(component.get("v.subProcess") === 'ChangeMarketFromRegulatedToFree'){
                        component.set("v.market", "Regulated");
                        component.set("v.andConditions", "Market__c='Regulated'");
                        component.set('v.contractAccountMarket', 'Free');
                        component.set('v.hideContractList', false);
                    }
                }
                else if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket' || component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract'){ //NO Market Change
                    if(response.opportunityServiceItems.length !== 0){
                        let market = component.get("v.market");
                        if (response.opportunityServiceItems[0].Market__c) {
                            market = response.opportunityServiceItems[0].Market__c;
                            component.set("v.market", market);
                        }
                        component.set("v.contractAccountMarket", market);
                        component.set("v.andConditions", 'Market__c = \'' + market + '\'');
                    }
                    if (response.opportunityLineItems.length !== 0) {
                        let contractAccountList = [];
                        contractAccountList.push(response.opportunityServiceItems[0].ContractAccount__c);
                        component.set('v.contractAccountIdList', contractAccountList);
                        if(component.get("v.opportunityServiceItems")[0].ServicePoint__c) {
                            let inEnelArea = response.opportunityServiceItems[0].ServicePoint__r.CurrentSupply__r.InENELArea__c;
                            component.set("v.inEnelArea", inEnelArea);
                        }
                        //self.reloadContractAccount(component);
                        component.set("v.showContractAccountSelection", true);
                    }
                    //Residential customer && NO Market Change: NO New contract option, only the existing contract is shown (selected by default)
                    if(!component.get('v.isBusinessClient')){
                        if (response.contractIdFromOpp && response.contractIdFromOpp !== '') {
                            component.set("v.contractId", response.contractIdFromOpp);
                        }
                        else if(response.opportunityServiceItems.length !== 0){
                            component.set("v.contractId", response.opportunityServiceItems[0].Contract__c);
                        }
                        component.set('v.hideNewContract', true);
                        component.set('v.hideContractList', false);
                    }//Business customer && NO Market Change
                    else{
                        if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket'){ //NO New contract option, only the existing contract is shown (selected by default)
                            if(response.contractIdFromOpp && response.contractIdFromOpp !== '' && response.opportunityServiceItems.length !== 0 && response.contractIdsList.length === 1){
                                component.set("v.contractId", response.contractIdFromOpp);
                            }
                            else if(response.opportunityServiceItems.length !== 0 && response.contractIdsList.length === 1) {
                                component.set("v.contractId", response.opportunityServiceItems[0].Contract__c);
                            }
                            component.set('v.hideNewContract', true);
                            component.set('v.hideContractList', false);
                        }
                        else{ //subProcess == 'ChangeProductWithinTheSameMarketPlusNewContract'
                            component.set("v.contractId", '');
                            component.set('v.hideNewContract', false);
                            component.set('v.hideContractList', false);
                        }
                    }
                }

                //component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.useBit2WinCart", response.useBit2WinCart);
                component.set("v.consumptionList", response.consumptionList);//AS: new version ok
                self.consumptionListRefactor(component);

                if (response.commodity === component.get("v.commodityGas")) { //AS: new version ok
                    component.set('v.consumptionConventions', true);
                    component.set('v.consumptionConventionsDisabled', true);
                    component.set('v.consumptionType', singleRate);
                }
                if (response.consumptionConventions) { //AS: new version ok
                    component.set('v.consumptionConventions', response.consumptionConventions);
                    if (response.consumptionConventions === true) {
                        component.set('v.consumptionConventionsDisabled', true);
                        component.set('v.consumptionType', singleRate);
                    }
                }
                if (response.consumptionList && response.consumptionList.length !== 0 && response.consumptionList.length === 1) {//AS: new version
                    component.set('v.consumptionType', response.consumptionList[0].Type__c);
                    component.set("v.consumptionConventions", true);
                    component.set("v.consumptionConventionsDisabled", true);
                } else if (response.consumptionList && response.consumptionList.length !== 0 && response.consumptionList.length === 2) {//AS: new version
                    component.set('v.consumptionType', dualRate);
                }
                let consumptionTypeFromCmp = component.get('v.consumptionType');  //AS: new version
                if (!consumptionTypeFromCmp) {
                    component.set('v.consumptionType', 'None');
                }
                self.updateConsumptionConventionsCheckbox(component);

                if(response.dossier.SelfReadingProvided__c){
                    component.set("v.metersActivated", response.dossier.SelfReadingProvided__c);
                }else {
                    component.set("v.metersActivated", false);
                }

                let metersActivated = component.get("v.metersActivated");
                if(!metersActivated){
                    //component.set("v.metersValidated", true);
                    component.set('v.skipMetersValidation',true);
                } else {
                    //component.set("v.metersValidated", false);
                    component.set('v.skipMetersValidation',false);
                }

                let contractSelected = component.get('v.contractId');
                if(contractSelected !== '' && contractSelected !== 'new' && contractSelected !== undefined){
                    component.set("v.consumptionConventions", false);
                    component.set("v.consumptionConventionsDisabled", true);
                    if((component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket')){
                        component.set("v.disableMeterReading",true);
                        component.set("v.metersActivated",false);
                        component.set("v.metersValidated",true);
                        component.set("v.skipMetersValidation",true);
                    }
                } else {
                    //component.set("v.disableMeterReading",false);
                    component.set("v.consumptionConventions", true);
                    component.set("v.consumptionConventionsDisabled", false);
                }

                let step = 0;

                if(response.opportunityServiceItems.length !== 0){
                    component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));

                    if (response.consumptionList && response.consumptionList.length !== 0 && response.consumptionList.length > 0) { // AS: ok new version

                        let isEnelDistributor = response.opportunityServiceItems[0].Distributor__r.IsDisCoENEL__c;
                        //let selectedCommodity = component.get("v.opportunityServiceItems")[0].RecordType.DeveloperName;
                        let selectedCommodity = component.get("v.selectedCommodity");
                        //AS: ???
                        if(isEnelDistributor && isEnelDistributor === true){
                            isEnelDistributor=true;
                        }else {
                            isEnelDistributor=false;
                        }
                        component.set("v.isEnelDistributor", isEnelDistributor);
                        if (!isEnelDistributor || selectedCommodity === component.get("v.commodityGas")){
                            component.set("v.metersValidated", true);
                        }

                        component.set('v.disableWizard', false);
                        component.set('v.disableOriginChannel', true);
                        component.set('v.showContractAddition', true);
                        component.set("v.contractAccountId", response.opportunityServiceItems[0].ContractAccount__c);
                        component.set('v.showContractAccountSelection', true);

                        let osis=component.get("v.opportunityServiceItems");
                        let allLinked=true;
                        osis.forEach(osi => {
                            if(!osi.Product__c ){
                                allLinked=false;
                            }
                        });
                        if(allLinked) {
                            let osi=osis[0];
                            let rateType=osi.Product__r.CommercialProduct__r.RateType__c;
                            component.set("v.consumptionType", rateType);
                            if(rateType){
                                component.set("v.productRateType", rateType);
                            }
                        }


                        step = 10;
                    }
                    else if (response.opportunityLineItems.length !== 0) {
                        component.set('v.disableWizard', false);
                        component.set('v.disableOriginChannel', true);
                        component.set('v.showContractAddition', true);
                        component.set("v.contractAccountId", response.opportunityServiceItems[0].ContractAccount__c);
                        component.set('v.showContractAccountSelection', true);
                        step = 8;
                    }else if ((response.contractIdFromOpp && response.contractIdFromOpp != '') || (response.contractType && response.contractType != '')) {
                        component.set('v.showContractAddition',true);
                        component.set('v.disableWizard',false);
                        component.set('v.disableOriginChannel',true);
                        step = 6;
                    }else {
                        component.set('v.disableWizard',false);
                        component.set('v.disableOriginChannel',true);
                        step = 5;
                    }
                }
                else if (response.opportunity && component.get('v.originSelected') && component.get('v.channelSelected') && component.get('v.subProcess') && response.commodity) {
                    if((response.commodity === component.get("v.commodityGas")) && (component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated')){
                        component.set("v.selectedCommodity", '');
                        ntfSvc.error(ntfLib, $A.get("$Label.c.RegulatedMarketIsNotAvailableForGasCommodity"));
                        step = 2;
                    }
                    else {
                        step = 4;
                    }
                    component.set('v.disableWizard',true);
                    component.set('v.disableOriginChannel',false);
                }
                else if (component.get("v.opportunityId") != "" && component.get('v.originSelected') != "" && component.get('v.channelSelected') != "" && component.get('v.subProcess') != "") {
                    step = 2;
                    component.set('v.disableWizard',true);
                    component.set('v.disableOriginChannel',false);
                }

                if(step === 10){
                    let metersJson= response.metersJson;
                    if(metersJson){
                        if(component.get("v.metersActivated")){
                            component.set("v.metersValidated", false);
                        } else {
                            component.set("v.metersValidated", true);
                        }
                    }
                }

                component.set("v.sameContract", component.get("v.contractId") === component.get("v.originContractId"));

                component.set("v.step", step);
                self.hideSpinner(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    isBusinessClient: function (component) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let accountId = component.get("v.accountId");
        component.find('apexService').builder()
            .setMethod('isBusinessClient')
            .setInput({
                "accountId": accountId
            })
            .setResolve(function (response) {
                component.set('v.isBusinessClient', response.isBusinessClient);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    setSubProcessAndExpirationDate : function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let self = this;
        let clear = 'false';
        let spOnOpportunity = component.get("v.opportunity").SubProcess__c;
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        let subProcess = component.get('v.subProcess');
        let expirationDate = component.get('v.expirationDate');
        let opportunityServiceItems = component.get("v.opportunityServiceItems");
        let activityId = component.get('v.activityId');
        if(opportunityServiceItems.length > 0 && !activityId && subProcess != null && subProcess != spOnOpportunity){
            clear = 'true';
        }
        let opportunityLineItems = component.get("v.opportunityLineItems");
        component.find("apexService").builder()
            .setMethod("setSubProcessAndExpirationDate")
            .setInput({
                opportunityId: opportunityId,
                dossierId: dossierId,
                subProcess: subProcess,
                expirationDate:JSON.stringify(expirationDate),
                clear: clear
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(response.isCleared){
                    component.set("v.contractId", '');
                    self.clearOsisRelatedData(component);
                    if(opportunityLineItems.length > 0){
                        self.deleteOlis(component);
                    }
                }
                // Do preliminary data check
                component.find('preliminaryDataCheck').doPreliminaryDataCheck();
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    commodityChangeActions: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let self = this;
        let commodity = component.get("v.selectedCommodity");
        let dossierId = component.get("v.dossierId");
        let opportunityId = component.get("v.opportunityId");
        let clear = 'false';
        let commodityOnDossier = component.get("v.dossier").Commodity__c;
        let opportunityServiceItems = component.get("v.opportunityServiceItems");
        if(opportunityServiceItems.length > 0 && commodity != null && commodity != commodityOnDossier){
            clear = 'true';
        }
        let opportunityLineItems = component.get("v.opportunityLineItems");
        self.showSpinner(component);
        component
            .find("apexService")
            .builder()
            .setMethod("commodityChangeActions")
            .setInput({
                dossierId: dossierId,
                commodity: commodity,
                opportunityId: opportunityId,
                clear: clear
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(response.isCleared){
                    self.clearOsisRelatedData(component);
                    if(opportunityLineItems.length > 0){
                        self.deleteOlis(component);
                    }
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    deleteOsiActions: function (component, osiId) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let dossierId= component.get("v.dossierId");
        self.showSpinner(component);
        component
            .find("apexService")
            .builder()
            .setMethod("deleteOsi")
            .setInput({
                opportunityId: opportunityId,
                dossierId: dossierId,
                osiId: osiId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(response.clear){
                    self.clearOsisRelatedData(component);
                    self.deleteOlis(component);
                }
                else{
                    //component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                    let osis = [];
                    let osiList = response.opportunityServiceItems;
                    osiList.forEach(osi => {
                        osis.push(osi);
                    });
                    component.set("v.opportunityServiceItems", osis);
                    component.set("v.reloadMeterReading", true);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    clearOsisRelatedData: function(component){ //AS: copiato da transfer
        component.set("v.opportunityServiceItems",[]);
        component.set("v.opportunityLineItems",[]);
        component.set("v.companyDivisionId","");
        component.set("v.consumptionType","");
        component.set("v.consumptionList",[]);
        component.set("v.metersJson","");
        component.set("v.isEnelDistributor",false);
        component.set("v.metersInfoMap",{});
        component.set("v.metersInfo",[]);
        component.set("v.metersCodes",[]);
        component.set("v.requestedStartDate",null);
        /*aggiunti da me*/
        component.set("v.inEnelArea", false);
        component.set("v.contractAccountId", '');
        component.set("v.contractId", '');
    },
    resetSupplyForm: function (component) { //AS: non dovrebbe più servire
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    checkSelectedSupplies: function (component, event, helper, selectedSupplyIds, multi) {
        const self = this;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems");
        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];
        component
            .find("apexService")
            .builder()
            .setMethod('getSupplies')
            .setInput({
                "selectedSupplyIds": JSON.stringify(selectedSupplyIds)
            })
            .setResolve(function (response) {
                const selectedSuppliesList =  response.supplies;

                for (let i = 0; i < selectedSuppliesList.length; i++) {
                    let validSupply = true;
                    let supply = selectedSuppliesList[i];

                    //check main fields on each selected supply  //AS: verificare se serve inserire all'errore validSupply = false;
                    if ((!supply.Market__c) || (supply.Market__c === null)) {
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get('$Label.c.SupplyMarketShouldNotBeEmpty'));
                        return;
                    }
                    if ((!supply.CompanyDivision__c) || (supply.CompanyDivision__c === null)) {
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get('$Label.c.CompanyOfTheSuppliesShouldNotBeEmpty'));
                        return;
                    }
                    if ((!supply.ServiceSite__c) || (supply.ServiceSite__c === null)) {
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ServiceSiteNotExist"));
                        return;
                    }
                    if ((!supply.Product__c) || (supply.Product__c === null)) {
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ProductNotExist"));
                        return;
                    }
                    if ((!supply.Contract__c) || (supply.Contract__c === null)) {
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ContractNotExist"));
                        return;
                    }
                    if ((!supply.ContractAccount__c) || (supply.ContractAccount__c === null)) {
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountNotExist"));
                        return;
                    }
                    if ((!supply.ServicePoint__r.Distributor__c) || (supply.ServicePoint__r.Distributor__c === null)) {
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ServicePointDistributorEmpty"));
                        return;
                    }
                    if(!supply.ServicePoint__r.PointAddressNormalized__c){
                        validSupply = false;
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ServicePointAddressNotNormalized"));
                        return;
                    }
                    if (supply.ServicePoint__r && (supply.Id !== supply.ServicePoint__r.CurrentSupply__c)) {
                        validSupply = false;
                        errCurrentSupplyIsNotValid.push(supply.Name);
                        continue;
                    }

                    if(validSupply){
                        //compare and check each selected supply data with existing osi data
                        if(osiList.length > 0){
                            if(supply.CompanyDivision__c != osiList[0].ServicePoint__r.CurrentSupply__r.CompanyDivision__c){
                                //supplies must have the same company division
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get("$Label.c.AllTheSuppliesShouldBeInSameCompany"));
                                break;
                            }
                            else if(supply.ServicePoint__r.Distributor__r.IsDisCoENEL__c !== osiList[0].Distributor__r.IsDisCoENEL__c){
                                //supplies must have the same value for IsDisCoENEL__c on Distributor
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get('$Label.c.AllDistributersShouldHaveTheSameDiscoEnel'));
                                break;
                            }
                            else if(supply.InENELArea__c !== osiList[0].ServicePoint__r.CurrentSupply__r.InENELArea__c){
                                //supplies must have the same value for InENELArea
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get('$Label.c.SelectedSuppliesMustHaveSameInENELArea'));
                                break;
                            }
                            else if(supply.Market__c != osiList[0].Market__c){
                                //supplies can't have different market
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get("$Label.c.MixSuppliesFromDifferentMarket"));
                                break;
                            }
                            else if(!component.get('v.isMarketChange') && component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket' && supply.Contract__c != osiList[0].Contract__c){
                                //NO Market Change: supplies can't have different contract
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectedSuppliesMustHaveSameContract"));
                                break;
                            }
                            for (let j = 0; j < osiList.length; j++) {
                                if ((supply.ServicePoint__c === osiList[j].ServicePoint__c) || (supply.ServicePoint__r.Name === osiList[j].ServicePointCode__c)) {
                                    validSupply = false;
                                    errSupplyAlreadySelected.push(supply.Name);
                                    break;
                                }
                            }
                        }
                        if(multi && i != 0){ // else compare and check selected supplies between them
                            let companyDivision = selectedSuppliesList[0].CompanyDivision__c;
                            let inENELArea = selectedSuppliesList[0].InENELArea__c;
                            let market = selectedSuppliesList[0].Market__c;
                            let contract = selectedSuppliesList[0].Contract__c;
                            let isDisCoEnel = selectedSuppliesList[0].ServicePoint__r.Distributor__r.IsDisCoENEL__c;
                            if(supply.CompanyDivision__c !== companyDivision){
                                //supplies must have the same company division
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get("$Label.c.AllTheSuppliesShouldBeInSameCompany"));
                                return;
                            }
                            else if(supply.ServicePoint__r.Distributor__r.IsDisCoENEL__c !== isDisCoEnel){
                                //supplies must have the same value for IsDisCoENEL__c on Distributor
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get('$Label.c.AllDistributersShouldHaveTheSameDiscoEnel'));
                                return;
                            }
                            else if(supply.InENELArea__c !== inENELArea){
                                //supplies must have the same value for InENELArea
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get('$Label.c.SelectedSuppliesMustHaveSameInENELArea'));
                                return;
                            }
                            else if(supply.Market__c !== market){
                                //supplies can't have different market
                                //in case of ChangeProductWithinTheSameMarket the supplies search cannot be filtered by market a priori
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get('$Label.c.SelectedSuppliesMustHaveSameMarket'));
                                return;
                            }
                            else if(!component.get('v.isMarketChange') && component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket' && supply.Contract__c != contract){
                                //NO Market Change: supplies can't have different contract
                                validSupply = false;
                                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectedSuppliesMustHaveSameContract"));
                                return;
                            }
                        }
                    }

                    if (validSupply){
                        validSupplies.push(supply);
                    }
                }

                if (errSupplyAlreadySelected.length !== 0){
                    let messages = errSupplyAlreadySelected.join(' ');
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' +messages);
                }
                if (errCurrentSupplyIsNotValid.length !== 0){
                    let messages = errCurrentSupplyIsNotValid.join(' ');
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' +messages);
                }

                if(validSupplies.length > 0){
                    component.set("v.originContractId", validSupplies[0].Contract__c);
                    self.createOsis(component, event, helper, validSupplies);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    createOsis: function (component, event, helper, validSupplies) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let opportunityId = component.get("v.opportunityId");
        let osiList = component.get("v.opportunityServiceItems");
        let channel = component.get("v.channelSelected");
        let commodity = component.get("v.selectedCommodity");
        let subProcess = component.get("v.subProcess");
        let isMarketChange = component.get('v.isMarketChange');
        self.showSpinner(component);
        component
            .find("apexService")
            .builder()
            .setMethod("createOsis")
            .setInput({
                supplies: JSON.stringify(validSupplies),
                opportunityId: opportunityId,
                channel: channel,
                commodity: commodity,
                subProcess: subProcess
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                else if (response.newOsis && response.newOsis.length !== 0) {
                    let newOsiList = response.newOsis;
                    if(osiList.length > 0){
                        newOsiList.forEach(osi => {
                            osiList.push(osi);
                        });
                        component.set("v.opportunityServiceItems", osiList);
                    }
                    else {
                        component.set("v.opportunityServiceItems", newOsiList);
                    }

                    let supplyContract = [];
                    supplyContract.push(response.contract);

                    if(supplyContract.length > 0) {
                        if(!isMarketChange && subProcess == 'ChangeProductWithinTheSameMarket'){
                            component.set("v.contractId", supplyContract[0]);
                        }
                        else{
                            component.set("v.contractId", '');
                        }
                    }

                    component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                    component.set("v.companyDivisionId", response.companyDivisionId);
                    component.set("v.market", response.market);
                    component.set('v.requestedStartDate',response.defaultStartDate);
                    component.set('v.validationRequestedStartDate',response.validationStartDate);
                    component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                    component.set("v.step", 5);
                }
            })
            .setReject(function (errorMsg) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
    checkOsisList: function (component) {
        let self = this;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let opportunityId = component.get("v.opportunityId");
        let subProcess = component.get('v.subProcess');
        let isPvcActivity = component.get('v.isPvcActivity');
        let isMarketChange = component.get('v.isMarketChange');
        self.showSpinner(component);
        component
            .find('apexService')
            .builder()
            .setMethod('checkOsisList')
            .setInput({
                opportunityId: opportunityId,
                subProcess: subProcess,
                isPvcActivity: isPvcActivity
            })
            .setResolve(function (response) {
                if (response.error) {
                    self.hideSpinner(component);
                    component.set("v.step", 5);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                let osis = [];
                let osiList = response.opportunityServiceItems;
                osiList.forEach(osi => {
                    osis.push(osi);
                });
                component.set("v.opportunityServiceItems", osis);
                component.set("v.market", response.market);
                if (isMarketChange) {
                    if (response.market === 'Free') {
                        component.set("v.contractAccountMarket", 'Regulated');
                    } else {
                        component.set("v.contractAccountMarket", 'Free');
                    }
                } else {
                    component.set("v.contractAccountMarket", response.market);
                }

                let supplyContracts = [];
                let contractsList = response.contractIdList;
                contractsList.forEach(contract => {
                    supplyContracts.push(contract);
                });

                if(supplyContracts.length > 0){
                    component.set("v.contractType", response.contractType);

                    if(component.get('v.requestedStartDate')){
                        self.goToContractSelection(component);
                    }else{
                        self.validateStartDateField(component);
                        self.hideSpinner(component);
                        ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                        return;
                    }
                    //}
                }
                self.hideSpinner(component);
            })
            .setReject(function (errorMsg) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    saveCompanyDivision: function (component){
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        let opportunityServiceItems = component.get("v.opportunityServiceItems");
        let companyDivisionId = component.get("v.companyDivisionId") ? component.get("v.companyDivisionId") : opportunityServiceItems[0].ServicePoint__r.CurrentSupply__r.CompanyDivision__c;
        if(!component.get("v.companyDivisionId")){
            component.set("v.companyDivisionId", companyDivisionId);
        }
        component
            .find("apexService")
            .builder()
            .setMethod("saveCompanyDivision")
            .setInput({
                opportunityId: opportunityId,
                companyDivisionId: companyDivisionId,
                dossierId: dossierId //added from trasfer
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        let self = this;
        self.showSpinner(component);
        let oppId = component.get("v.opportunityId");
        let osiList = component.get("v.opportunityServiceItems");
        let contractAccountId = component.get("v.contractAccountId");
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityId": oppId,
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    validateStartDateField: function (component, event) {
        const labelFieldList = [];
        let valEffectiveDate = component.find("RequestedStartDate__c");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('RequestedStartDate__c');
        }
        if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    },
    setRequestedStartDate: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunity = component.get('v.opportunity');
        let requestedStartDate = component.get('v.requestedStartDate');

        component.find("apexService").builder()
            .setMethod("setRequestedStartDate")
            .setInput({
                opportunityId: opportunity.Id,
                requestedStartDate: JSON.stringify(requestedStartDate)
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    goToContractSelection: function (component) {
        let self = this;
        //save companydivision on dossier and opportunity
        self.saveCompanyDivision(component);
        //check and save requested start date on opportunity
        self.setRequestedStartDate(component);
        let excludeContractIds = [];
        //NO Market Change
        if(!component.get('v.isMarketChange')){
            //Residential customer && NO Market Change: NO New contract option, only the existing contract is shown (selected by default)
            if(!component.get('v.isBusinessClient')){
                component.set('v.hideNewContract', true);
                component.set('v.hideContractList', false);
            }
            //Business customer && NO Market Change
            else{
                if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket'){ //selected supplies are from one contract, only existing contract option is allowed
                    component.set('v.hideNewContract', true);
                    component.set('v.hideContractList', false);
                }
                else{
                    component.set("v.contractId", '');
                    component.set('v.hideNewContract', false);
                    component.set('v.hideContractList', false);
                }
            }
            if (component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract') {
                excludeContractIds.push(component.get("v.originContractId"));
            }
        }
        else{ //Market Change: only New contract option is shown
            component.set("v.contractId", '');
            component.set('v.hideNewContract', false);
            component.set('v.hideContractList', component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated');
        }
        component.set("v.step", 6);
        component.set("v.showContractAddition", true);
        component.set("v.excludeContractIds", excludeContractIds);
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    checkSelectedContract: function(component) {
        let isValid = true;
        let isNotKamUser = (!component.get("v.userDepartment") || component.get("v.userDepartment") === undefined);
        if (component.get("v.contractId") === 'new') {
            let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
            //console.log('New Contract details:', JSON.stringify(newContractDetails));
            //component.set("v.contractType",newContractDetails.type);
            if (!newContractDetails.type) {
                isValid = false;
            }
            if (component.get("v.channelSelected") === "Indirect Sales" && (!newContractDetails.salesman || !newContractDetails.salesUnitId)) {
                isValid = false;
            }
            else if ((component.get("v.channelSelected") === "Magazin Enel" || component.get("v.channelSelected") === "Magazin Enel Partner"
                || component.get("v.channelSelected") === "Call Center" || component.get("v.channelSelected") === "Info Kiosk")
                && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
                isValid = false;
            }
            //else if (component.get("v.channelSelected") === "Direct Sales (KAM)" && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
            else if (component.get("v.channelSelected") === "Direct Sales (KAM)"){
                if(!newContractDetails.companySignedId){
                    isValid = false;
                }
                else if(!newContractDetails.salesUnitId && isNotKamUser && component.get("v.user").SalesUnitID__c !== null && component.get("v.user").SalesUnitID__c !== undefined){
                    isValid = false;
                }
            }
            if (isValid) {
                component.set("v.contractType", newContractDetails.type);
            }
        }
        return isValid;
    },
    updateContractInformationOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        //start transfer version
        const self = this;
        let contractId = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined) ? '' : component.get("v.contractId");
        let commodity = component.get("v.selectedCommodity");
        let osiList = component.get("v.opportunityServiceItems");
        let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
        let salesChannel = component.get('v.channelSelected');
        let salesSupportUser = component.get("v.salesSupportUser");
        if(contractId && osiList){
            component.set("v.contractType", osiList[0].Contract__r.ContractType__c);
        }
        else {
            component.set("v.contractType", newContractDetails.type);
        }
        //let customerSignedDate = component.get("v.customerSignedDate");
        component
            .find("apexService")
            .builder()
            .setMethod("updateContractInformationOnOpportunity")
            .setInput({
                opportunityId: opportunityId,
                commodity: commodity, //added from trasfer
                contractId: contractId,
                //customerSignedDate: newContractDetails.customerSignedDate !== null ? JSON.stringify(newContractDetails.customerSignedDate) : null,
                contractType: component.get("v.contractType"),
                contractName: newContractDetails.name,
                salesUnitId: newContractDetails.salesUnitId,
                companySignedId: newContractDetails.companySignedId,
                salesman: newContractDetails.salesman,
                salesChannel: salesChannel,
                opportunityServiceItems: osiList,  //added from transfer, verificare la classe
                salesSupportUser: salesSupportUser
            })
            .setResolve(function (response) {
                console.log("updateContractInformationOnOpportunity response", JSON.parse(JSON.stringify(response)));
                self.hideSpinner(component);

                //start transfer version
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (response.purgedOLIs) {
                    component.set("v.opportunityLineItems", []);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        let accountId = component.get("v.accountId");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accountId);
            } else {
                contractAccountComponent.reset(accountId);
            }
        }
    },
    saveOpportunity: function (component, stage, callback, cancelReason, detailsReason) {
        var self = this;
        self.showSpinner(component);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId");
        let dossierId = component.get("v.dossierId");
        let skipMetersValidation = component.get('v.skipMetersValidation');
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "dossierId": dossierId,
                "stage": stage,
                "cancelReason": cancelReason,
                "detailsReason": detailsReason,
                "isSelfReading": !skipMetersValidation
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.enableSaveButton", true);
                    component.set("v.isFirstSave", true);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        let pageReference;

        if (useBit2WinCart) {
            let osiList = component.get("v.opportunityServiceItems");
            var isGas = false;
            var isElectric = false;
            for (var osi of osiList) {
                if (osi.RecordType.DeveloperName === 'Gas') {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === 'Electric') {
                    isElectric = true;
                }
            }
            var productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';

            }
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__productType": productType,
                    "c__commodity": component.get ("v.selectedCommodity"),
                    "c__contractType": component.get ("v.contractType"),
                    "c__dossierId": component.get("v.dossierId")
                }
            };
        }
        else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, pageReference,false);
    },
    linkOliToOsi: function (component, oli, callback) {
        let self = this;
        self.showSpinner(component);
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems");
        component.find('apexService').builder()
            .setMethod('linkOliToOsi')
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                let rateType=response.rateType;
                if(rateType){
                    component.set("v.consumptionType", rateType);
                    component.set("v.productRateType", rateType);
                    //component.find("consumptionTable").validateConsumptionsData();
                }else {
                    let commodity = component.get("v.selectedCommodity");
                    if(commodity === component.get("v.commodityElectric")) {
                        ntfSvc.error(ntfLib, $A.get('$Label.c.RateTypeIsNotDefinedOnTheProduct'));
                        return;
                    } else {
                        let singleRate= component.get("v.productTypeSingleRate");
                        component.set('v.consumptionType', singleRate);
                    }
                }
                component.set("v.step", 9);

                if (callback && typeof callback === "function") {
                    callback(component, self);
                } else {
                    //ntfSvc.success(ntfLib, '');
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    deleteOlis: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOlis")
            .setInput({
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.isDeleted){
                    component.set("v.opportunityLineItems",[]);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createConsumptions: function (component, consumptions, callback) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let accountId = component.get('v.accountId');
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let consumptionConventions = component.get('v.consumptionConventions');
        consumptions.forEach(consumption => {
            consumption.Customer__c = accountId;
        });
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("insertConsumptionList")
            .setInput({
                'consumptionList': consumptions,
                'contractId': contractId,
                'consumptionConventions': consumptionConventions
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.consumptionList', response.consumptionList);
                self.consumptionListRefactor(component);
                if (callback) {
                    callback();
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateIndexValues: function (component, callback) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let accountId = component.get("v.accountId");
        let meterInfoMap = component.get("v.metersInfoMap");
        let metersInfo = component.get("v.metersInfo");
        let metersCodes = component.get("v.metersCodes");
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        let skipMetersValidation = component.get("v.skipMetersValidation");
        let selfReadingProvided= skipMetersValidation ? true : component.get("v.metersActivated");
        let osis= component.get("v.opportunityServiceItems");
        osis.forEach( osi=> {
            let meterCode=osi.ServicePointCode__c;
            metersInfo.push(meterInfoMap[meterCode]);
            metersCodes.push(meterCode);
        });

        for(let i = 0; i < metersInfo.length; i++){
            if(!metersInfo[i]){
                metersInfo.splice(i,1);
            }
        }
        component.find('apexService').builder()
            .setMethod("createIndex")
            .setInput({
                //'caseList': JSON.stringify(caseList),
                'opportunityId': opportunityId,
                'accountId': accountId,
                'metersCodes': metersCodes,
                'metersInfo' : metersInfo,
                'selfReadingProvided': selfReadingProvided,
                'dossierId' :dossierId,
                'skipMetersValidation' : skipMetersValidation
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(callback){
                    callback();
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    getMeters: function (component,step) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        const meterList = component.find('meterList');
        //const selectedSupply = component.get("v.searchedSupplyFields");
        let opportunityId = component.get("v.opportunityId");
        component.find('apexService').builder()
            .setMethod("getMeters")
            .setInput({
                'opportunityId': opportunityId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                let meters = [];
                let metersMap = response.metersJson;
                for ( let key in metersMap ) {
                    meters.push({value:metersMap[key], key:key});
                    //component.set("v.metersJsonNew",JSON.parse(metersMap[key]));
                }
                component.set("v.metersJson", meters);

                let osis= component.get("v.opportunityServiceItems");
                let count=0;
                osis.forEach( osi => {
                        let key=osi.ServicePoint__r.CurrentSupply__c;
                        let meterLists = component.find("meterList");
                        let meterList;
                        if(meterLists){
                            if(meterLists instanceof Array){
                                meterList=meterLists[count];
                            } else {
                                meterList=meterLists;
                            }
                            meterList.setMeterList(JSON.parse(metersMap[key]));
                        }
                        count++;
                    }
                );
                if(step){
                    component.set("v.step", step);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    reloadSendingChannel: function (component) {
        let contractSelectionComponent = component.find("sendingChannelSelection");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadAddressList();
            } else {
                contractSelectionComponent.reloadAddressList();
            }
        }
    },
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent instanceof Array) {
            let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
            privacyChangeComponentToObj[0].savePrivacyChange();
        } else {
            privacyChangeComponent.savePrivacyChange();
        }
        component.set("v.enableSaveButton", true);
    },
    updateUrl: function (component, accountId, opportunityId, dossierId, activityId,templateId) {
        let self = this;
        let state = {};
        state["c__accountId"] = accountId;
        state["c__opportunityId"] = opportunityId;
        state["c__dossierId"] = dossierId;
        if (activityId) {
            state["c__activityId"] = activityId;
        }
        state["c__templateId"] = templateId;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ProductChangeWizard',
            },
            state: state
        };
        self.redirect(component, pageReference, true);
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                        if (!window.location.hash && component.get("v.step") === 3) {
                            window.location = window.location + '#loaded';
                            self.refreshFocusedTab(component);
                        }
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference,overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    redirectToDossier: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'Dossier__c',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirectToOppty: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect_old: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response === true) {
                workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                    workspaceAPI.openSubtab({
                        pageReference: pageReference,
                        focus: true
                    }).then(function (subtabId) {
                        workspaceAPI.closeTab({
                            tabId: enclosingTabId
                        });
                    }).catch(function (errorMsg) {
                        ntfSvc.error(ntfLib, errorMsg);
                    });
                });
            } else {
                const navService = component.find("navService");
                navService.navigate(pageReference);
            }
        }).catch(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        });
    },
    goToAccount: function (component) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    getMetersDynamic: function (component, osi, metersInfo, step,opportunityId,dossierId,selfReadingProvided,skipMetersValidation,accountId,rearrangedMeterCodes,i) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        if (metersInfo){
            for(let i = 0; i < metersInfo.length;){
                if(!metersInfo[i]){
                    metersInfo.splice(i,1);
                    i = i > 0 ? i-- : i === 0 ? 0 : i;
                }
                if(metersInfo.length === 1 && i === 0){
                    continue;
                } else {
                    i++;
                }
            }
        } else {
            metersInfo = [];
        }
        //console.log("component.get(v.metersInfo=="+JSON.stringify(component.get("v.metersInfo")));
        component.find('apexService').builder()
            .setMethod("getMetersDynamic")
            .setInput({
                //'caseList': JSON.stringify(caseList),
                'opportunityId': opportunityId,
                'accountId': accountId,
                'metersCodes': Object.values(rearrangedMeterCodes),
                'metersInfo' : metersInfo,
                'selfReadingProvided': selfReadingProvided,
                'dossierId' :dossierId,
                'skipMetersValidation' : skipMetersValidation,
                'osi': osi
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                //console.log("metersJson==="+ JSON.stringify(response.metersJson));

                let meters = [];
                let metersMap = response.metersJson;
                for ( let key in metersMap ) {
                    meters.push({value:metersMap[key], key:key});
                    //component.set("v.metersJsonNew",JSON.parse(metersMap[key])); //the error is here

                }
                component.set("v.metersJson", meters);

                /*let osis= component.get("v.opportunityServiceItems");

                osis.forEach( osi => {*/

                let key=osi.ServicePoint__r.CurrentSupply__c;
                let meterLists = component.find("meterList");
                let meterList;
                if(meterLists){
                    if(meterLists instanceof Array){
                        meterList=meterLists[i];
                    } else {
                        meterList=meterLists;
                    }
                    console.log("metersMap[key] +++++++++++++++++++++++++++++> "+ JSON.stringify(metersMap[key]));
                    meterList.setMeterList(JSON.parse(metersMap[key]));
                }
                /*       count++;
                   }
               );*/
                if(step){
                    component.set("v.step",step);
                }
                component.set("v.metersInfo", []);
                component.set("v.metersInfoMap", {});
                component.set("v.metersCodes", []);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    showSpinner: function (component) {
        component.set("v.showSpinner", true);
        //$A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        component.set("v.showSpinner", false);
        //$A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    reloadAll: function (component) {
        let self = this;
        self.reloadContractAddition(component);
        //self.reloadContractAccount(component);
    },

    updateConsumptionConventionsOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let consumptionConventions = component.get("v.consumptionConventions");
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateConsumptionConventionsOnOpportunity")
            .setInput({
                consumptionConventions: consumptionConventions,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    loadOsi: function (component, osiId, callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems") || [];
        let newOsiList=[];
        //component.set("v.opportunityServiceItems",newOsiList);
        component.find('apexService').builder()
            .setMethod('loadOsi')
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {

                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);

                let updatedOsi = response.opportunityServiceItem;
                osiList.forEach(osi => {
                    if(osi.Id === updatedOsi .Id){
                        newOsiList.push(updatedOsi);
                    }else{
                        newOsiList.push(osi);
                    }
                });
                component.set("v.opportunityServiceItems",newOsiList);
                if(callback){
                    callback(updatedOsi);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    deleteOlisByContractAccountChange: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractAccountId = component.get("v.contractAccountId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOlisByContractAccountChange")
            .setInput({
                opportunityId: opportunityId,
                contractAccountId: contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.isDeleted){
                    component.set("v.opportunityLineItems",[]);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateConsumptionConventionsCheckbox: function(component){
        let isNewContract = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined || component.get("v.contractId") === '');
        let commodity = component.get("v.selectedCommodity");
        if(isNewContract){
            if(commodity === 'Electric'){
                component.set("v.consumptionConventionsDisabled", false);
            }
        }
        else{
            component.set("v.consumptionConventionsDisabled", true);
        }
    },
    updateRegulatedTariffs: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get('v.opportunityId');
        let commodity = component.get('v.selectedCommodity');
        component.find('apexService').builder()
            .setMethod("updateRegulatedTariffs")
            .setInput({
                "opportunityId": opportunityId,
                "commodity": commodity
            })
            .setResolve(function (response) {
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    getMetersOneAtTime: function (component,step) {
        let osis= component.get("v.opportunityServiceItems");
        let self = this;
        const accountId = component.get("v.accountId");
        let meterInfoMap = component.get("v.metersInfoMap");
        let metersInfo = component.get("v.metersInfo");
        let metersCodes = component.get("v.metersCodes");
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        let selfReadingProvided=component.get("v.metersActivated");
        let skipMetersValidation = component.get("v.skipMetersValidation");
        let counter = 0;
        let rearrangedMeterCodes = {};

        if(metersCodes && metersCodes.length > 0) {
            metersCodes.forEach(function(element){
                rearrangedMeterCodes[element] = element;
            });
            console.log('rearrangedMeterCodes *************************************> ' + JSON.stringify(rearrangedMeterCodes));
        }
        self.showSpinner(component);
        osis.forEach( (osi, i) => {
            counter++;
            let meterCode=osi.ServicePointCode__c;
            metersInfo.push(meterInfoMap[meterCode]);
            metersCodes.push(meterCode);
            self.getMetersDynamic(component, osi, metersInfo, step, opportunityId,dossierId,selfReadingProvided,skipMetersValidation,accountId,rearrangedMeterCodes,i);
            if(counter === osis.length){
                if(step) {
                    component.set("v.step", step);
                }
                self.hideSpinner(component);
            }
        });
    },
    consumptionListRefactor: function(component){
        let consumptions = component.get("v.consumptionList");
        if (consumptions){
            consumptions.forEach(function(element){
                element.QuantityJanuary__c = element.QuantityJanuary__c.toFixed(3);
                element.QuantityFebruary__c = element.QuantityFebruary__c.toFixed(3);
                element.QuantityMarch__c = element.QuantityMarch__c.toFixed(3);
                element.QuantityApril__c = element.QuantityApril__c.toFixed(3);
                element.QuantityMay__c = element.QuantityMay__c.toFixed(3);
                element.QuantityJune__c = element.QuantityJune__c.toFixed(3);
                element.QuantityJuly__c = element.QuantityJuly__c.toFixed(3);
                element.QuantityAugust__c = element.QuantityAugust__c.toFixed(3);
                element.QuantitySeptember__c = element.QuantitySeptember__c.toFixed(3);
                element.QuantityOctober__c = element.QuantityOctober__c.toFixed(3);
                element.QuantityNovember__c = element.QuantityNovember__c.toFixed(3);
                element.QuantityDecember__c = element.QuantityDecember__c.toFixed(3);
            });
            component.set("v.consumptionList", consumptions);
        }
    },
    validateOpportunity: function (component,callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod('validateOpportunity')
            .setInput({
                "dossierId": component.get("v.dossierId"),
                "opportunityId": component.get("v.opportunityId")
            })
            .setResolve(function (response) {
                let validOsi = false;
                let validDossier = false;

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.enableSaveButton", true);
                } else {
                    validOsi = response.validOsi;
                    validDossier = response.validDossier;
                }

                if(validOsi === false){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ThisOpportunityDoesntHaveRelatedOsi"));
                    component.set("v.enableSaveButton", true);
                }
                if(validDossier === false){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ThisDossierHasARelatedCase"));
                    component.set("v.enableSaveButton", true);
                }
                if (callback) {
                    callback(validOsi, validDossier);
                }
                if (!valid) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    createOLIsForContractAddition: function(component, callback) {
         let ntfSvc = component.find('notify');
         let ntfLib = component.find('notifLib');
         let opportunityId = component.get("v.opportunityId");
         let contractId = component.get("v.contractId");
         let self = this;
         component.find('apexService').builder()
            .setMethod('createOLIsForContractAddition')
            .setInput({
                "opportunityId": opportunityId,
                "contractId": contractId
            })
            .setResolve(function (response) {
                console.log('createOLIsForContractAddition response', JSON.parse(JSON.stringify(response)));
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    console.log(response.errorMsg, response.errorTrace);
                    return;
                } else {
                    component.set("v.opportunityLineItems", response.opportunityLineItems);
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    }
})