({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        const activityId = myPageRef.state.c__activityId;
        const templateId = myPageRef.state.c__templateId;
        helper.hideSpinner(component);
        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        component.set("v.dossierId", dossierId);
        component.set("v.activityId", activityId);
        component.set("v.templateId", templateId);
        helper.initConsoleNavigation(component, $A.get('$Label.c.ProductChange'));
        helper.isBusinessClient(component);
        helper.initialize(component);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        let expirationDate = component.get("v.expirationDate");
        let expirationDateDate =  new Date(component.get('v.expirationDate'));
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        let todayDate = new Date(dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate());
        let osiList = component.get("v.opportunityServiceItems");

        switch (buttonPressed) {
            case 'confirmStep1': //Script Management
                if(!origin || origin === "undefined"){
                    component.set('v.originSelected', component.get('v.opportunity').Origin__c);
                }
                if(!channel || channel === "undefined"){
                    component.set('v.channelSelected', component.get('v.opportunity').Channel__c);
                }

                if(origin && channel) {
                    component.set('v.disableOriginChannel',true);
                    component.set('v.disableWizard',false);
                    //save channel and origin on opportunity and dossier
                    helper.setChannelAndOrigin(component);
                    component.set("v.step", 2);
                }else {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectOriginAndChannel"));
                    return;
                }
                break;
            case 'confirmStep2': //RequestClassification
                if(expirationDate === null){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }
                if(expirationDate !== null && expirationDateDate < todayDate){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.EnteredDateShouldBeGreaterOrEqualThan") + " "+ today);
                    return;
                }
                if(expirationDate && component.get("v.subProcess")){
                    const opportunityServiceItems = component.get("v.opportunityServiceItems");
                    const activityId = component.get("v.activityId");
                    if (activityId && opportunityServiceItems && opportunityServiceItems.length > 0) {
                        const market = component.get("v.market");
                        const subProcess = component.get("v.subProcess");
                        if ((subProcess === 'ChangeMarketFromFreeToRegulated' && market === 'Regulated') || (subProcess === 'ChangeMarketFromRegulatedToFree' && market === 'Free')) {
                            ntfSvc.error(ntfLib, $A.get("$Label.c.IncompatibleMarket"));
                            return;
                        }
                    }
                    //set the market by subProcess and add it to andConditions filter for supplies search
                    let marketCondition = "";
                    let andConditions = component.get("v.andConditions");
                    andConditions.splice(0, andConditions.length);

                    if(component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated'){
                        component.set("v.market", "Free");
                        component.set("v.contractAccountMarket", "Regulated");
                        marketCondition = "Market__c='Free'";
                        component.set("v.contractType", "");
                    }
                    else if(component.get("v.subProcess") === 'ChangeMarketFromRegulatedToFree'){
                        component.set("v.market", "Regulated");
                        component.set("v.contractAccountMarket", "Free");
                        marketCondition = "Market__c='Regulated'";
                        component.set("v.contractType", "");
                    }
                    else if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket' || component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract'){
                        component.set("v.market", "");
                    }
                    if(component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated' || component.get("v.subProcess") === 'ChangeMarketFromRegulatedToFree'){
                        andConditions.push(marketCondition);
                        component.set("v.andConditions", andConditions);
                    }
                    //save subProcess and expirationDate on opportunity and dossier
                    helper.setSubProcessAndExpirationDate(component);
                    component.set("v.step", 3);
                }else{
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }
                break;
            case 'confirmStep3': //Commodity Selection
                if((component.get("v.selectedCommodity") === component.get("v.commodityGas")) && (component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated')){
                    component.set("v.selectedCommodity", '');
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RegulatedMarketIsNotAvailableForGasCommodity"));
                    return;
                }
                //save commodity on dossier (if changed, delete Osis and Olis when present)
                helper.commodityChangeActions(component);
                if (!osiList || osiList.length === 0) {
                    component.set("v.step", 4);
                }
                else {
                    component.set("v.step", 5);
                }
                break;
            case 'confirmStep5': //SelectSupply
                helper.checkOsisList(component);
                if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                    component.set("v.userDepartment", '');
                }
                if(!component.get("v.userDepartment")){
                    if(component.get("v.channelSelected") === "Indirect Sales"){
                        component.set("v.salesUnit", null);
                    }
                    component.set("v.salesUnitId", '');
                    component.set("v.salesman", '');
                    component.set("v.companySignedId", '');

                    if(component.get("v.opportunity") && component.get("v.opportunity").SalesUnit__c){
                        component.set("v.salesUnit", component.get("v.opportunity").SalesUnit__c);
                        component.set("v.salesUnitId", component.get("v.opportunity").SalesUnit__c);
                    }
                    if(component.get("v.opportunity") && component.get("v.opportunity").CompanySignedBy__c){
                        component.set("v.companySignedId", component.get("v.opportunity").CompanySignedBy__c);
                    }
                    if(component.get("v.opportunity") && component.get("v.opportunity").Salesman__c){
                        component.set("v.salesman", component.get("v.opportunity").Salesman__c);
                    }
                    console.log('opp: '+JSON.stringify(component.get("v.opportunity")));
                    if(component.get('v.channelSelected') !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").SalesUnit__c){
                        if(component.get("v.salesUnit") && component.get("v.salesUnit").SalesDepartment__c === component.get('v.channelSelected')){
                            let salesAccountId = component.get("v.salesUnit").Id;
                            component.set("v.salesUnitId", salesAccountId);
                        }
                    }
                    if(component.get("v.channelSelected") !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").CompanySignedBy__c){
                        if(!component.get("v.salesUnit")){
                            component.set("v.companySignedId", "");
                        }
                        else{
                            if(component.get("v.channelSelected") !== "Back Office Sales" && component.get("v.channelSelected") !== "Back Office Customer Care"){
                                let userId = component.get("v.user").Id;
                                component.set("v.companySignedId", userId);
                            }
                        }
                    }
                }
                if (component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract') {
                    component.set("v.contractId", "");
                }
                break;
            case 'confirmStep6':
                component.set("v.contractId", component.find("contractSelectionComponent").getContract());
                let selectedContract = component.get('v.contractId');
                let originContract = component.get('v.originContractId');
                if (!selectedContract) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectContract"));
                    return;
                }
                if (component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract' && selectedContract === originContract) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectDifferentContract"));
                    return;
                }
                if (!helper.checkSelectedContract(component)) {
                    ntfSvc.error(ntfLib,$A.get("$Label.c.RequiredFields"));
                    return;
                }
                if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                    let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                    component.set('v.streetName',pointStreetId);
                    if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                        let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                        component.set("v.vatNumber", vatNumber)
                        let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                        component.set("v.selfReadingEnabled", selfReadingEnabled)
                    }
                    //component.log('FF companyDivisionId: '+ component.get("v.companyDivisionId"));
                    if(component.get("v.companyDivisionId")){
                        let companyDivisionId = component.get("v.companyDivisionId");
                        component.set('v.pointAddressProvince',companyDivisionId);
                    }
                }
                //save contract data on opportunity
                helper.updateContractInformationOnOpportunity(component); //AS: è meglio qui all'inizio, altrimenti non vengono valorizzati correttamente
                // i parametri da passare al componente ContractAccount

                if(component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated'){
                    component.set('v.contractAccountMarket', 'Regulated');
                }
                else if(component.get("v.subProcess") === 'ChangeMarketFromRegulatedToFree'){
                    component.set('v.contractAccountMarket', 'Free');
                }
                else if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket' || component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract'){
                    let market = component.get('v.market');
                    component.set('v.contractAccountMarket', market);
                }

                //retrieve the available contract accounts
                let supplyContractAccounts = [];
                if(selectedContract === '' || selectedContract === 'new' || selectedContract === undefined || selectedContract !== originContract){
                    component.set('v.contractAccountIdList', []);

                    component.set('v.hideButtons', false);
                }
                else if(selectedContract != '' && selectedContract != undefined && selectedContract != null && selectedContract != 'new'){
                    //retrieve the contract accounts of the selected supply/supplies
                    for (let j = 0; j < osiList.length; j++) {
                        if(osiList[j].Contract__c === selectedContract){
                            if(!supplyContractAccounts.includes(osiList[j].ServicePoint__r.CurrentSupply__r.ContractAccount__c)){
                                supplyContractAccounts.push(osiList[j].ServicePoint__r.CurrentSupply__r.ContractAccount__c);
                            }
                        }
                    }
                    component.set('v.contractAccountIdList', supplyContractAccounts);
                    component.set('v.hideButtons', true);
                }

                let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
                component.set("v.contractType", newContractDetails.type);
                let inEnelArea = osiList[0].ServicePoint__r.CurrentSupply__r.InENELArea__c;
                component.set("v.inEnelArea", inEnelArea);
                component.set("v.step", 7);
                component.set("v.showContractAccountSelection", true);
                component.find("contractAccountSelectionComponent").reload();
                break;
            case 'confirmStep7': //SelectContractAccount
                if (!component.get("v.contractAccountId")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectContractAccount"));
                    return;
                }
                helper.deleteOlisByContractAccountChange(component);
                //save contract account on all Osis
                helper.updateOsi(component);
                component.set("v.step", 8);
                break;
            case 'confirmStep8'://SelectProductSupply
                let prdList = component.get("v.opportunityLineItems");
                if (!prdList && prdList.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct"));
                    return;
                }
                const utilityPrds = prdList.filter(oli => oli.Product2.RecordType.DeveloperName === 'Utility');
                if (!utilityPrds || utilityPrds.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct"));
                    return;
                }
                let oli = utilityPrds[0];
                component.find("consumptionTable").update();
                //save selected product on osi
                helper.linkOliToOsi(component, oli);
                helper.updateConsumptionConventionsCheckbox(component);
                helper.updateRegulatedTariffs(component);
                break;
            case 'confirmStep9'://Consumption Details
                let contractSelected = component.get('v.contractId');
                if(contractSelected !== '' && contractSelected !== 'new' && contractSelected !== undefined){
                    component.set("v.consumptionConventions", false);
                    component.set("v.consumptionConventionsDisabled", true);
                } else {
                    component.set("v.consumptionConventions", true);
                    component.set("v.consumptionConventionsDisabled", false);
                }
                let consumptionCmp = component.find("consumptionTable");
                let consumptionList = consumptionCmp.getAllConsumptionObjects();

                helper.createConsumptions(component, consumptionList, function () {
                    let isEnelDistributor = osiList[0].Distributor__r.IsDisCoENEL__c;
                    let selectedCommodity = component.get("v.selectedCommodity");
                    component.set("v.isEnelDistributor", isEnelDistributor);

                    if (!isEnelDistributor || selectedCommodity === component.get("v.commodityGas")){
                        component.set("v.metersValidated", true);
                    }
                    if(selectedCommodity === component.get("v.commodityElectric") && component.get("v.reloadMeterReading")){
                        component.set("v.retrieveSapIndex", false);
                        helper.getMetersOneAtTime(component, 10);
                    }
                    component.set("v.step", 10);
                });

                helper.updateConsumptionConventionsOnOpportunity(component);

                if (!component.get("v.metersActivated")){
                    component.set("v.metersValidated", true);
                }


                break;
            case 'confirmStep10': //SelfReading
                //let showReading = component.get("v.showMeterReading");
                let selectContract= component.get('v.contractId');
                if(selectContract !== '' && selectContract !== 'new' && selectContract !== undefined){
                    component.set("v.metersActivated",false);
                    if((component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket')){
                        component.set("v.disableMeterReading",true);
                    }
                } else {
                    component.set("v.disableMeterReading",false);
                    component.set("v.metersActivated",true);
                }
                let isEnelDistributor = JSON.parse(component.get("v.isEnelDistributor"));
                //let selectedCommodity = component.get("v.opportunityServiceItems")[0].RecordType.DeveloperName;
                let selectedCommodity = component.get("v.selectedCommodity");
                //if(showReading){
                if(component.get("v.skipMetersValidation") && selectedCommodity === 'Electric' && isEnelDistributor === true){
                    let meterListCmp = component.find('meterList');
                    if(meterListCmp){
                        if(Array.isArray(meterListCmp)){
                            meterListCmp.forEach(function(element){
                                element.validateMeterInfo();
                            });
                        } else {
                            meterListCmp.validateMeterInfo();
                        }
                    } else {
                        ntfSvc.error(ntfLib, 'No meter list available to validate');
                        return;
                    }
                }
                if(selectedCommodity === component.get("v.commodityElectric") && isEnelDistributor) {
                    helper.updateIndexValues(component, function () {
                        helper.getMetersOneAtTime(component,11);
                    });
                }
                if(component.get('v.contractAccountId')){
                    helper.reloadSendingChannel(component);
                }
                component.set("v.step", 11);
                break;
            case 'confirmStep11'://Sending Channel Selection
                helper.saveSendingChannel(component);
                component.set("v.step", 12);
                component.set("v.enableSaveButton", true);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1': //Script Management
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel',false);
                component.set('v.disableWizardHeader',true);
                component.set("v.step", 1);
                break;
            case 'returnStep2': //RequestClassification
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.showContractAddition",false);
                component.set("v.showContractAccountSelection", false);
                component.set("v.step", 2);
                break;
            case 'returnStep3': //Commodity Selection
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.showContractAddition",false);
                component.set("v.showContractAccountSelection", false);
                component.set("v.step", 3);
                break;
            case 'returnStep5': //SelectSupply
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.showContractAddition",false);
                component.set("v.showContractAccountSelection", false);
                component.set("v.step", 5);
                break;
            case 'returnStep6': //ContractAddition
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                //component.set("v.contractId","");
                component.set("v.contractAccountIdList", []);
                component.set('v.showContractAddition',true);
                component.set("v.showContractAccountSelection", false);
                component.set("v.step", 6);
                //let isEmptyContractList = component.find("contractSelectionComponent").isEmptyContractList();
                /*if(!component.get("v.isEmptyContractList")){
                    helper.reloadContractAddition(component);
                }*/
                break;
            case 'returnStep7': //SelectContractAccount
                /*helper.deleteOlis(component);*/
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.showContractAccountSelection", true);
                component.set("v.step", 7);
                break;
            case 'returnStep8': //SelectProductSupply
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.step", 8);
                break;
            case 'returnStep9': //Consumption Details
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.step", 9);
                break;
            case 'returnStep10': //SelfReading
                if(component.get("v.metersActivated")){
                    component.set("v.metersValidated", false);
                    component.set('v.skipMetersValidation',false);
                }
                if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket'){
                    component.set("v.metersValidated", true);
                    component.set('v.skipMetersValidation',true);
                }
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.metersValidated", false);
                //let enableSelfReading = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                let isEnelDistributor = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                //let selectedCommodity = component.get("v.opportunityServiceItems")[0].RecordType.DeveloperName;
                let selectedCommodity = component.get("v.selectedCommodity");

                if (!isEnelDistributor || selectedCommodity === component.get("v.commodityGas")){
                    component.set("v.metersValidated", true);
                }
                component.set("v.metersInfoMap",{});
                component.set("v.metersInfo",[]);
                component.set("v.metersCodes",[]);
                component.set("v.step", 10);
                break;
            case 'returnStep11': //Sending Channel Selection
                component.set('v.disableWizard', true);
                component.set('v.disableOriginChannel', false);
                component.set("v.step", 11);
                break;
            default:
                break;
        }
    },
    handleOriginChannelSelection: function (component, event, helper) {
        if(component.get('v.disableWizard')){
            let originSelected = event.getParam('selectedOrigin');
            let channelSelected = event.getParam('selectedChannel');
            if(originSelected !== undefined && channelSelected !== null){
                component.set('v.originSelected', originSelected);
                component.set('v.channelSelected', channelSelected);
            }
            component.set("v.step", 1);
        }
    },
    handleSubProcess: function (component, event) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let subProcess= event.getParam("subProcess");
        component.set("v.subProcess", subProcess);
        /*
        if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract' && !component.get('v.isBusinessClient')){
            component.set("v.subProcess", '');
            ntfSvc.error(ntfLib, $A.get("$Label.c.OptionAvailableOnlyForBusinessCustomers"));
            return;
        }
        */
        if(component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated' || component.get("v.subProcess") === 'ChangeMarketFromRegulatedToFree'){
            component.set("v.isMarketChange", true);
        }
        else if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket' || component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract'){
            component.set("v.isMarketChange", false);
        }
        let excludeContractIds = [];
        //NO Market Change
        if(!component.get('v.isMarketChange')){
            //Residential customer && NO Market Change: NO New contract option, only the existing contract is showed (selected by default)
            if(!component.get('v.isBusinessClient')){
                component.set('v.hideNewContract', true);
                component.set('v.hideContractList', false);
            }
            //Business customer && NO Market Change
            else{
                if(component.get("v.subProcess") === 'ChangeProductWithinTheSameMarket'){
                    component.set('v.hideNewContract', true);
                    component.set('v.hideContractList', false);
                    component.set("v.contractId", component.get("v.originContractId"));
                }
                else{
                    component.set("v.contractId", '');
                    component.set('v.hideNewContract', false);
                    component.set('v.hideContractList', false);
                }
            }
            if (component.get("v.subProcess") === 'ChangeProductWithinTheSameMarketPlusNewContract') {
                excludeContractIds.push(component.get("v.originContractId"));
            }
        }
        else{ //Market Change: only New contract option is showed
            component.set("v.contractId", '');
            component.set('v.hideNewContract', false);
            component.set('v.hideContractList', component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated');
        }
        component.set("v.excludeContractIds", excludeContractIds);
    },
    handleExpirationDate: function (component, event) {
        let expirationDate = event.getParam("expirationDate");
        component.set("v.expirationDate", expirationDate);
    },
    handleSelectCommodity: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let selectedCommodity = event.getParam("selectedCommodity");

        if(selectedCommodity === component.get("v.commodityGas") && component.get("v.subProcess") === 'ChangeMarketFromFreeToRegulated'){
            component.set("v.selectedCommodity", '');
            ntfSvc.error(ntfLib, $A.get("$Label.c.RegulatedMarketIsNotAvailableForGasCommodity"));
            return;
        }

        //AS: from transfer
        let singleRate = component.get("v.productTypeSingleRate");
        if (selectedCommodity === component.get("v.commodityGas")) {
            component.set('v.consumptionConventions', true);
            component.set('v.consumptionConventionsDisabled', true);
            component.set('v.consumptionType', singleRate); //AS: new version
        }else if(selectedCommodity === component.get("v.commodityElectric")) {
            component.set('v.consumptionConventions', false);
            component.set('v.consumptionConventionsDisabled', false);
            component.set('v.consumptionType', 'None'); //AS: new version
        }

        component.set('v.selectedCommodity', selectedCommodity);
    },
    handleSupplySearch: function (component, event, helper) {
        //helper.hideSpinner(component);
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    getSelectedSupplies: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems");
        let selectedSupplyIds = event.getParam("selected");
        let multi = event.getParam("selected").length > 1; //check if it is a multiselection
        helper.checkSelectedSupplies(component, event, helper, selectedSupplyIds, multi);
    },
    handleOsiDelete: function (component, event, helper) {
        let osiId = event.getParam("recordId");
        helper.deleteOsiActions(component,osiId);
        const osiList = component.get("v.opportunityServiceItems");

        if (osiList.length === 0) {
            component.set("v.showContractAddition",false);
            component.set("v.showContractAccountSelection",false);
            component.set('v.requestedStartDate',null);
            component.set("v.step", 3);
            //helper.reloadContractAccount(component);
        }
        else {
            component.set("v.step", 4);
        }
        component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
    },
    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        let consumptionConventions = event.getParam("consumptionConventions");//AS: new version
        component.set("v.contractName", event.getParam("newContractName"));
        component.set("v.contractType", event.getParam("newContractType"));
        component.set("v.salesUnitId", event.getParam("newSalesUnitId"));
        component.set("v.companySignedId", event.getParam("newCompanySignedId"));
        component.set("v.salesman", event.getParam("newSalesman"));
        component.set("v.contractId", contractSelected);
        component.set("v.sameContract", contractSelected === component.get("v.originContractId"));
        if (consumptionConventions !== null) {
            component.set("v.consumptionConventions", consumptionConventions);
        }
    },
    getContractAccountRecord: function (component, event, helper) {
        component.set("v.contractAccountId", event.getParam("contractAccountRecordId"));
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(component, "Proposal/Price Quote", helper.openProductSelection);
        //helper.setPercentage(component, "Proposal/Price Quote"); //TODO: questo c'è su transfer e non da noi, neanche sullo swin
        //helper.deleteOlis(component);  //AS: non serve più
        //component.set("v.showContractAddition",false); //AS: non serve più
    },
    changeConsumptionType: function (component, event, helper) {
        component.set('v.consumptionType', component.find('changeConsumptionType').get('v.value'));
    },
    changeConsumptionConventions: function (component, event, helper) {
        let consumptionConventions = component.get('v.consumptionConventions');
        let singleRate = component.get("v.productTypeSingleRate");
        if(!component.get("v.productRateType")){
            if (consumptionConventions) {
                //component.set('v.consumptionType', 'Single Rate');
                component.set('v.consumptionType', singleRate);
            }
            if (!consumptionConventions) {
                component.set('v.consumptionType', 'None');
            }
        }
    },
    handleMetersData: function (component, event, helper){
        helper.getMetersOneAtTime(component, null);
    },
    handleMeterEdited: function (component, event, helper){
        component.set("v.metersValidated", false);
        let metersInfoMap=component.get("v.metersInfoMap");
        let meterCode= event.getParam('meterCode');
        delete metersInfoMap[meterCode];
    },
    handleMeterListValidated: function (component, event, helper){
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let meterList = component.find("meterList");
        let meterLists = [];
        if(meterList instanceof Array) {
            meterLists= meterList;
        } else{
            meterLists.push(meterList);
        }
        let metersInfoMap=component.get("v.metersInfoMap");
        let isMeterValidated=event.getParam('isMeterValidated');
        let eventMeterInfo= event.getParam('meterInfo');
        let meterCode= event.getParam('meterCode');
        let valid=isMeterValidated;
        if(isMeterValidated && isMeterValidated === true){
            if(component.get("v.skipMetersValidation") === false) {
                ntfSvc.success(ntfLib, $A.get('$Label.c.AllIndexValuesAreSuccessfullyValidated').replace('{0}', meterCode));
            }
            let meterInfo =[];
            eventMeterInfo.forEach(meterEle => {
                Object.values(meterEle).forEach(meterRow => {
                    meterInfo.push(meterRow);
                });

            });
            metersInfoMap[meterCode]= meterInfo;

            let osis= component.get("v.opportunityServiceItems");
            let meterSize = Object.keys(metersInfoMap).length;
            if(osis.length == meterSize){
                valid=true;
            }else {
                valid= false;
            }
        }else {
            ntfSvc.error(ntfLib, $A.get('$Label.c.AllIndexValuesAreNotValidated').replace('{0}', meterCode));
            delete metersInfoMap[meterCode];
            valid=false;
        }
        component.set("v.metersValidated", valid);
    },
    handleSavedSendingChannel: function (component, event, helper) {
        component.set("v.step", 12);
    },
    getPrivacyId: function (component, event, helper) {
        let isFirstSave = component.get("v.isFirstSave");

        if(isFirstSave === true) {
            component.set("v.isFirstSave", false);
            let ntfLib = component.find('notifLib');
            let ntfSvc = component.find('notify');

            let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange"); //non c'è in transfer
            if (!doNotCreatePrivacyChange) { //non c'è in transfer
                component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
                component.set("v.dontProcess", event.getParam('dontProcess'));
            }
            if (!component.get("v.isSaving")) {
                helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToDossier);
                //helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToOppty);
                component.set("v.isFirstSave", true);
            }
            if (component.get("v.isSaving")) {
                if (component.get("v.dontProcess")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
                    $A.get('e.force:refreshView').fire();
                    component.set("v.isFirstSave", true);
                    return;
                }
                helper.saveOpportunity(component, "Quoted", helper.redirectToDossier);
                //helper.saveOpportunity(component, "Quoted", helper.redirectToOppty);
            }
        }
    },
    removeError: function (component, event, helper) {
        const labelFieldList = [];
        let valEffectiveDate = component.find("RequestedStartDate__c");
        valEffectiveDate.blur();
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }

        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('RequestedStartDate__c');
        }
        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
    },
    save: function (component, event, helper) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.set("v.enableSaveButton", false);
        component.set("v.isSaving", true);
        component.set("v.isSaved", true);
        if(component.get("v.subProcess") !== 'ChangeProductWithinTheSameMarket'){
            helper.validateOpportunity(component,function (validOsi, validDossier) {
                if(validOsi && validDossier){
                    helper.createPrivacyChangeRecord(component);
                }
            });
        } else {
            if (component.get("v.dontProcess")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
                $A.get('e.force:refreshView').fire();
                return;
            }
            helper.saveOpportunity(component, "Quoted", helper.redirectToDossier);
            //helper.saveOpportunity(component, "Quoted", helper.redirectToOppty);
        }
    },
    saveDraft: function (component, event, helper) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.set("v.isSaving", false);
        component.set("v.isSaved", false);
        if(component.get("v.subProcess") !== 'ChangeProductWithinTheSameMarket'){
            helper.createPrivacyChangeRecord(component);
        } else {
            helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToDossier);
            //helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToOppty);
        }
    },
    onSaveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        component.set("v.isSaved", false);
        if (cancelReason) {
            component.set("v.isSaving", false);
            helper.redirectToDossier(component, helper);
            //helper.saveOpportunity(component, "Closed Lost", helper.redirectToDossier, cancelReason, detailsReason);
        }
    },
    handleValidateMeters: function (component, event, helper){
        let isMetersActivatedChecked = component.get('v.metersActivated');
        if(!isMetersActivatedChecked){
            component.set('v.metersValidated', true);
            component.set('v.skipMetersValidation',true);
        } else{
            component.set('v.metersValidated', false);
            component.set('v.skipMetersValidation',false);
        }
    },
    validateSendingChannelFields: function(component, event, helper){
        let areFieldsValidated = event.getParam('areFieldsValidated');
        component.set("v.enableSendingChannel",areFieldsValidated);
    },

    handleUpdateOsi:function(component, event, helper) {
        let osiId= event.getParam("osiId");
        helper.loadOsi(component,osiId,function (updatedOsi) {
            //console.log("new estimated==="+updatedOsi.EstimatedConsumption__c);
            let consumptionCmp = component.find("consumptionTable");
            if(consumptionCmp){
                consumptionCmp.update(updatedOsi.Id);
            }
        });
    },

    handleUpdateEstimatedConsumption: function (component, event, helper) {
        //console.log("event consump==="+JSON.stringify(event));
        let consumptionDetails=event.getParam("consumptionDetails");
        let osiId= consumptionDetails.OpportunityServiceItem__c;
        //console.log("event osiId==="+osiId);
        let osiTiles=component.find('osiTile');
        let osiTile;
        if (osiTiles instanceof Array) {
            osiTiles.forEach(osiTileCmp => {
                if(osiTileCmp.getRecordId() === osiId){
                    osiTile=osiTileCmp;
                }
            });
        }else {
            osiTile=osiTiles;

        }
        //if(osiTile){
        helper.loadOsi(component,osiId,function (osi) {
            //console.log("new estimated==="+osi.EstimatedConsumption__c);
        });

        //}

    },

    createOLIsForContractAddition: function(component, event, helper) {
        helper.showSpinner(component);
        helper.createOLIsForContractAddition(component);
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.hasBlockingErrors', true);
    }
})