/**
 * Created by BADJI on 23/07/2019.
 */
({
    init: function (component, event, helper) {
        const pageRef = component.get("v.pageReference");
        let recordIdParam = pageRef.state.c__opportunityId;
        component.set("v.opportunityId", recordIdParam);
        helper.reloadProductSelection(component);
    },
    handleNavToWizard: function (component, event, helper) {
        let wizardName = event.getParam("wizardType");
        let accountId = event.getParam("accountId");
        let opportunityId = event.getParam("opportunityId");

        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: wizardName,
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId
            }
        };
        //component.set("v.opportunityId", '');
        helper.redirect(component, pageReference);
    }
});