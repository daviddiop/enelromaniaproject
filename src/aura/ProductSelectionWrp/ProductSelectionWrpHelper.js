/**
 * Created by goudiaby on 30/07/2019.
 */

({
    redirect: function (component, pageReference) {
        const ntfSvc = component.find('ntfSvc');
        const ntfLib = component.find('ntfLib');
        const workspaceAPI = component.find("workspace");

        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response === true) {
                workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                    workspaceAPI.openSubtab({
                        pageReference: pageReference,
                        focus: true
                    }).then(function () {
                        workspaceAPI.closeTab({
                            tabId: enclosingTabId
                        });
                    }).catch(function (error) {
                    });
                });
            } else {
                const navService = component.find("navService");
                navService.navigate(pageReference);
            }
        }).catch(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        });
    },
    reloadProductSelection: function (component)  {
        let productSelectionComponent = component.find("productSelection");
        if (productSelectionComponent) {
            if (productSelectionComponent instanceof Array) {
                let productSelectionComponentToObj = Object.assign({}, productSelectionComponent);
                productSelectionComponentToObj[0].reload(component.get("v.opportunityId"));
            } else {
                productSelectionComponent.reload(component.get("v.opportunityId"));
            }
        }
    }
});