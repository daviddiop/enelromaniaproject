({
    initialize: function (component){
        const self = this;
        self.showSpinner(component);

        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const genericRequestDossierId = myPageRef.state.c__genericRequestDossierId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
        component.set("v.genericRequestDossierId", genericRequestDossierId);

        component.find('apexService').builder()
            .setMethod("init")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "genericRequestDossierId": genericRequestDossierId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            }).setResolve(function (response) {
            self.hideSpinner(component);
            component.set("v.dossierId", response.dossier.Id);
            component.set("v.informationRequestRecordTypeId", response.informationRequestRecordTypeId);

            if (response.dossier.Id && !dossierId) {
                self.updateUrl(component, accountId, response.dossier.Id, genericRequestDossierId);
                return;
            }

            const dossierVal = response.dossier;
            if (dossierVal) {
                if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                    component.set("v.isClosed", true);
                }
            }
            if (response.case) {
                component.set("v.caseTile", response.case);
            }
            self.setIRStep(component, response);
        }).setReject(function (error) {
            ntfSvc.error(ntfLib, error);
        }).executeAction();
    },

    updateDossierAndCaseStatus: function (component, helper) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("UpdateDossierAndCaseStatus")
            .setInput({
                dossierId: component.get("v.dossierId"),
                channelSelected: component.get('v.channelSelected'),
                originSelected: component.get('v.originSelected')
            }).setResolve(function (response) {
            self.hideSpinner(component);
            self.redirectToDossier(component, helper);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        }).executeAction();
    },

    updateUrl: function (component, accountId, dossierId, genericRequestDossierId) {
        const navService = component.find("navService");
        let state = {
            "c__accountId": accountId,
            "c__dossierId": dossierId
        };

        if (genericRequestDossierId){
            state["c__genericRequestDossierId"] = genericRequestDossierId;
        }
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_InformationRequestWizard',
            },
            state: state
        };
        navService.navigate(pageReference,true);
    },

    handleCancel: function (component, helper, cancelReason, detailsReason) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("CancelProcess")
            .setInput({
                dossierId: component.get("v.dossierId"),
                cancelReason: cancelReason,
                detailsReason: detailsReason
            }).setResolve(function (response) {
            self.hideSpinner(component);
            self.redirectToDossier(component, helper);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        })
            .executeAction();
    },

    handleIRSaveDraft: function(component, helper) {
        let self = this;
        let dossierId = component.get('v.dossierId');
        let caseList = component.get("v.caseTile");
        let originSelected = component.get('v.originSelected');
        let channelSelected = component.get('v.channelSelected');
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        component.find("apexService").builder()
            .setMethod("SaveDraftDossierAndCase")
            .setInput({
                dossierId: dossierId,
                caseList: JSON.stringify(caseList),
                originSelected: originSelected,
                channelSelected: channelSelected
            })
            .setResolve(function(response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function(response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },

    validateIRFields: function(component,fields) {
        let REQUIRED_FIELDS = ['Origin', 'Channel__c', 'SupplyType__c', 'Type', 'SubType__c', 'CustomerNotes__c'];
        if (component.get('v.showReasonsList')) {
            REQUIRED_FIELDS.push('Reason__c')
        }
        let listFieldInError = [];
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let self = this;
        for (let i in fields) {
            if ((REQUIRED_FIELDS.includes(i) && (fields[i] == null || fields[i] === ''))) {
                listFieldInError.push(i);
            }
        }
        if (listFieldInError.length && listFieldInError) {
            for(let e in listFieldInError) {
                let fieldName = listFieldInError[e];
                if (component.find(fieldName)) {
                    let regularField = component.find(fieldName);
                    let fixField = self.getFixField(component, fieldName);

                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    if (!Array.isArray(regularField)) {
                        regularField = [regularField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                    $A.util.addClass(regularField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        } else {
            return true;
        }
    },

    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id != deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        if (items.length === 0) {
            helper.changeIRStep(component, helper, 0);
        }

        component.set("v.caseTile", items);
    },

    removeFieldErrorIR: function (component, event, helper) {
        let fieldName  = event.getSource().get("v.fieldName");

        if (component.find(fieldName)) {
            let fixField = component.find(fieldName);
            if (Array.isArray(fixField)) {
                fixField = fixField[0];
            }
            $A.util.removeClass(fixField, 'slds-has-error');
        }
    },

    removeCustomFieldErrorIR: function (component, event) {
        let fieldName = event.getSource().get('v.name');
        let self = this;

        if (component.find(fieldName)) {
            let fixField = self.getFixField(component, fieldName);
            $A.util.removeClass(fixField, 'slds-has-error');
        }
    },

    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },

    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },

    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },

    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },

    supplyList: function (component, event, helper,selectedSupplyIds) {
        const self = this;
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let validSupplies = [];

        component.find('apexService').builder()
            .setMethod('CheckSelectedSupplies')
            .setInput({
                "selectedSupplyIds": JSON.stringify(selectedSupplyIds)
            })
            .setResolve(function (response) {
                const searchedSupplyFieldsList =  response.supplies;
                component.set("v.isENELDistributor", response.IsDisCoENEL__c);
                self.setDistributorProvince(component, response.DistributorProvince);
                for (let i = 0; i < searchedSupplyFieldsList.length; i++) {
                    let validSupply = true;
                    let oneSupply = searchedSupplyFieldsList[i];

                    self.setIRCaseSupplyType(component, oneSupply.RecordType.DeveloperName);
                    //self.updateBillingAdjustmentType(component,helper, oneSupply.RecordType.DeveloperName);
                    if (oneSupply.NonDisconnectable__c) {
                        ntfSvc.error(ntfLib, $A.get('$Label.c.NonDisconnectablePod'));
                        return;
                    }
                    for (let j = 0; j < caseList.length; j++) {
                        if (oneSupply.Id === caseList[j].Supply__c) {
                            errSupplyAlreadySelected.push(oneSupply.Name);
                            validSupply = false;
                            break;
                        }
                    }
                    if (validSupply){
                        validSupplies.push(oneSupply);
                    }
                }

                component.set("v.searchedSupplyFields", validSupplies);
                self.createIRCase(component, event, helper, validSupplies);
                component.set("v.isSupplySelected", true);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    getSubTypeDependencies : function(component, informationRequestType) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("GetAvailableSubTypes")
            .setInput({
                informationRequestType: informationRequestType,
                supplyType: component.get("v.selectedSupplyType"),
            }).setResolve(function (response) {
            component.set("v.subTypeDependencyMetadata", response.availableSubTypes);
            self.generateSubTypeList(component, response.availableSubTypes);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        }).executeAction();
    },

    configureSubTypeList: function (component, informationRequestType){
        const self = this;
        switch (informationRequestType) {
            case "For network activity":{
                self.getSubTypeDependencies(component, informationRequestType);
                component.set("v.showSubTypeFilteredList", true);
                let fixField = component.find('SubType__c');
                $A.util.addClass(fixField, 'slds-hide');
                break;
            }
            case "For commercial activity":{
                self.getSubTypeDependencies(component, informationRequestType);
                component.set("v.showSubTypeFilteredList", true);
                let fixField = component.find('SubType__c');
                $A.util.addClass(fixField, 'slds-hide');
                break;
            }
            default :{
                component.set("v.showSubTypeFilteredList", false);
                let fixField = component.find('SubType__c');
                $A.util.removeClass(fixField, 'slds-hide');
            }
        }
    },

    generateSubTypeList : function(component, availableSubTypes) {
        let subTypeOptions = [];
        for(let i=0; i< availableSubTypes.length; i++){
            subTypeOptions.push({'label': availableSubTypes[i].Label__c, 'value': availableSubTypes[i].Label__c});
        }
        component.set("v.availableSubTypes", subTypeOptions);
    },

    getAvailableReasons : function(component, selectedSubType) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("GetAvailableReasons")
            .setInput({
                caseSubType: selectedSubType,
            }).setResolve(function (response) {
            self.generateReasonList(component, response.availableReasons);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        }).executeAction();
    },

    generateReasonList : function(component, availableReasons) {
        let self = this;
        let availableReasonsList = [];

        for(let i=0; i< availableReasons.length; i++){
            availableReasonsList.push({'label': availableReasons[i].Label__c, 'value': availableReasons[i].Label__c});
        }

        if(availableReasonsList.length > 0) {
            component.set("v.availableReasons", availableReasonsList);
            let fixField = component.find('Reason__c');
            $A.util.addClass(fixField, 'slds-hide');
            component.set("v.showReasonsList", true);
        }
        else {
            self.resetReasonField(component)
        }
    },

    setIRCaseSupplyType: function (component, supplyRecordType){
        let supplyType;
        switch (supplyRecordType) {
            case "Electric":{
                supplyType = 'Electricity';
                break;
            }
            case "Gas":{
                supplyType = 'Gas';
                break;
            }
            case "Service":{
                supplyType = 'VAS';
                break;
            }
        }
        component.set("v.selectedSupplyType", supplyType);
    },

    setDistributorProvince: function (component, distributorProvince){
        if(distributorProvince !== undefined)
        {
            component.set("v.distributorProvince", distributorProvince);
            component.set("v.isProvinceDisabled", true);
        }
    },

    createIRCase: function (component, event, helper, validSupplies) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const channelSelected = component.get("v.channelSelected");
        const originSelected = component.get("v.originSelected");

        component.find('apexService').builder()
            .setMethod("CreateCase")
            .setInput({
                'supplies': JSON.stringify(validSupplies),
                'caseList': JSON.stringify(caseList),
                'accountId': accountId,
                'dossierId': dossierId,
                'channelSelected':channelSelected,
                'originSelected':originSelected
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, result.errorMsg);
                    return;
                }
                if (response.cases.length !== 0) {
                    helper.changeIRStep(component, helper, 1);
                    component.set("v.companyDivisionId", response.cases[0].CompanyDivision__c);
                }
                component.set("v.caseTile", response.cases);
                component.set("v.selectedSupplyId", response.cases[0].Supply__c);
                component.set("v.osiTableView", response.cases.length > component.get("v.tileNumber"));
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    setSupplyBillingProfile: function (component, selectedSupplyIds) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("SetSupplyBillingProfile")
            .setInput({
                'selectedSupplyIds': JSON.stringify(selectedSupplyIds)
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, result.errorMsg);
                    return;
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    updateIRCase : function (component, caseId, description, subType) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("UpdateCase")
            .setInput({
                caseId: caseId,
                notes: description,
                subType: subType
            }).setResolve(function (response) {
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        }).executeAction();
    },

    setIRStep: function(component, response) {
        let step = 1;

        if (!response.dossier ||
            !response.dossier.Channel__c ||
            !response.dossier.Origin__c ||
            !response.case) {
            step = 0;
        }
        this.changeIRStep(component, this, step);
    },

    changeIRStep: function(component, helper, stepNumber, isNext) {
        let self = this;
        component.set('v.disableOriginChannel', stepNumber > 0)

        if (stepNumber === 1 && isNext) {
            helper.warnIfNoSupplySelected(component)
            self.changeIRStep(component, helper, 1);
            return;
        }

        component.set("v.step", stepNumber);
    },

    setIRChannelAndOrigin: function (component) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("SetChannelAndOrigin")
            .setInput({
                dossierId: component.get("v.dossierId"),
                selectedOrigin: component.get("v.originSelected"),
                selectedChannel: component.get("v.channelSelected"),
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },

    warnIfNoSupplySelected: function (component) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseList = component.get("v.caseTile");

        if(caseList.length === 0){
            ntfSvc.warn(ntfLib, $A.get("$Label.c.NoSupplySelected"));
        }
    },

    resetMandatoryFields: function (component) {
        let self = this;

        component.find('caseTypeInput').set('v.value', '');
        component.set('v.subTypeValue', '');
        self.resetReasonField(component);
    },

    resetReasonField: function (component) {
        component.set('v.reasonValue', '')
        component.set("v.showReasonsList", false);
        $A.util.removeClass(component.find('Reason__c'), 'slds-hide');
    },

    getFixField: function(component, oldFieldName) {
        let newFieldName;
        switch (oldFieldName) {
            case "Type": {
                newFieldName = 'caseTypeDiv';
                break;
            }
            case "SubType__c": {
                newFieldName = 'subTypeCbxDiv';
                break;
            }
            case "Reason__c": {
                newFieldName = 'reasonCbxDiv';
                break;
            }
        }
        return component.find(newFieldName);
    },
    initConsoleNavigation: function (component, wizardLabel) {
            let self = this;
            const workspaceAPI = component.find("workspace");
            if (workspaceAPI) {
                workspaceAPI.isConsoleNavigation()
                    .then(function (response) {
                        if (response === true) {
                            workspaceAPI.getFocusedTabInfo()
                                .then(function (response) {
                                    let focusedTabId = response.tabId;
                                    let isSubTab = response.isSubtab;
                                    if (isSubTab) {
                                        workspaceAPI.setTabLabel({
                                            tabId: focusedTabId,
                                            label: wizardLabel
                                        });
                                        workspaceAPI.setTabIcon({
                                            tabId: focusedTabId,
                                            icon: "utility:case",
                                            iconAlt: wizardLabel
                                        });
                                    }
                                });
                        }
                    }).catch(function (error) {
                    console.log(error);
                });
                document.title = $A.get("$Label.c.LightningExperienceSalesforce");
            }
        }
});