/**
 * Created by Octavian on 4/9/2020.
 */

({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        if (dossierId){
        helper.initConsoleNavigation(component,$A.get("$Label.c.InformationRequest"));
        }
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initialize(component, event, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');

        if (originSelected && channelSelected) {
            component.set("v.originSelected", originSelected);
            component.set("v.channelSelected", channelSelected);
            helper.setIRChannelAndOrigin(component);
            component.set("v.isClosed", false);
        } else {
            component.set("v.isClosed", true);
        }
    },
    handleSubmit : function(component, event, helper) {
        event.preventDefault();
        let recordEditForm = component.find('caseEditForm');
        let fields = event.getParam("fields");
        let description = component.find('Description').get('v.value')
        let subType = component.find('subTypeCbx').get('v.value')

        if (helper.validateIRFields(component, fields)) {
            fields.Dossier__c = component.get('v.dossierId');
            let caseTiles = component.get('v.caseTile');
            if(caseTiles.length > 0) {
                recordEditForm.set("v.recordId", caseTiles[0].Id);
            }
            recordEditForm.submit(fields);
            helper.updateIRCase(component, caseTiles[0].Id, description, subType)
        }
    },
    handleFormError: function (component, event) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let errorMessage = event.getParam('message');

        ntfSvc.error(ntfLib, errorMessage);
    },
    handleFieldError: function (component, event, helper) {
        helper.removeFieldErrorIR(component, event, helper);
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
    },
    saveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");

        helper.handleCancel(component, helper, cancelReason, detailsReason);
    },
    saveIRDraft: function (component, event, helper) {
        helper.handleIRSaveDraft(component, helper);
    },
    handleSuccess: function (component, event, helper) {
        console.log('Case successfully updated!');
        if (event) {
            console.log('caseId: ' + JSON.stringify(event.getParam('response').id));
            helper.updateDossierAndCaseStatus(component, helper);
        }
    },
    getSupplies: function (component, event, helper) {
        let selectedSupplyIds = event.getParam("selected");
        helper.setSupplyBillingProfile(component, selectedSupplyIds);
        helper.supplyList(component, event, helper, selectedSupplyIds);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    handleInformationRequestTypeChanged: function (component, event, helper) {
        let fieldName = event.getSource().get("v.fieldName");
        let informationRequestType = event.getSource().get("v.value");
        if(fieldName === 'Type' && informationRequestType !== ""){
            helper.configureSubTypeList(component, informationRequestType);
        }
        helper.resetReasonField(component);
        helper.removeFieldErrorIR(component, event, helper);
    },
    handleSupplyTypeChanged: function (component, event, helper) {
        let fieldName = event.getSource().get("v.fieldName");
        let supplyType = event.getSource().get("v.value");
        if(fieldName === 'SupplyType__c' && supplyType !== "") {
            component.set("v.selectedSupplyType", supplyType);
            component.set("v.isSupplySelected", true);
        } else {
            component.set("v.isSupplySelected", false);
        }

        helper.resetMandatoryFields(component);
        helper.removeFieldErrorIR(component, event, helper);
    },
    handleSubTypeOptionsChange: function (component, event, helper){
        let selectedOptionValue = event.getParam("value");
        component.set("v.subTypeValue", selectedOptionValue);
        helper.getAvailableReasons(component, selectedOptionValue);
        helper.removeCustomFieldErrorIR(component, event, helper);
    },
    handleReasonOptionsChange: function (component, event, helper){
        let selectedOptionValue = event.getParam("value");
        component.set("v.reasonValue", selectedOptionValue);
        helper.removeCustomFieldErrorIR(component, event, helper);
    },
    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = parseInt(buttonId.replace('confirmStep', '')) + 1 ;
        helper.changeIRStep(component, helper, stepNumber, true);
    },
    editStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber =  parseInt(buttonId.replace('editStep', ''));
        helper.changeIRStep(component, helper, stepNumber);
    },
   /* onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo().then(function (response) {
                        let focusedTabId = response.tabId;
                        let isSubTab = response.isSubtab;
                        if (isSubTab) {
                            setTabLabel({
                                tabId: focusedTabId,
                                label: $A.get("$Label.c.InformationRequest")
                            });
                            setTabIcon({
                                tabId: focusedTabId,
                                icon: "utility:case",
                                iconAlt: $A.get("$Label.c.InformationRequest")
                            });
                        }
                    });
                }
            });
    }*/
});