/**
 * Created by Vlad Mocanu on 12/11/2019.
 */

({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        component.set("v.accountId", accountId);
        component.set("v.dossierId", dossierId);
        helper.setDates(component);
        helper.initConsoleNavigation(component, $A.get('$Label.c.RepaymentSchedule'));
        helper.initialize(component, helper);
    },
    nextStep: function (component, event, helper) {
        let buttonPressed = event.getSource().getLocalId();
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        if (buttonPressed === "confirmStep") {
            component.set("v.step", 2);

        } else if (buttonPressed === "confirmStep1") {
            helper.updateDossierCompanyDivision(component);
            component.set("v.step", 3);

        } else if (buttonPressed === 'confirmStep2') {
            component.find("periodSelection").onConfirmSelection();
            helper.handleInvoiceInquiryStartDateChange(component, event, helper);
        } else if (buttonPressed === 'confirmStep3') {
            let selectedFisaIds = component.get("v.selectedFisaIds");
            if (selectedFisaIds.length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.NoInvoicesConfirmed"));
            } else {
                component.set("v.step", 5);
            }
        } else if (buttonPressed === 'confirmStep4') {
            let rateFrequency = component.get("v.rateFrequency");
            let totalAmount = component.get("v.totalAmount");
            let numberOfInstallments = component.get("v.numberOfInstallments");
            let dueAmountPercentage = component.get("v.dueAmountPercentageValue");
            let staggerStartDate = component.get("v.staggerStartDate");
            let ntfLib = component.find("notifLib");
            let ntfSvc = component.find("notify");

            if (!rateFrequency) {
               component.find("rateFrequency").set("v.value", $A.get("$Label.c.DailyFrequency"));
            }

            if (!numberOfInstallments || numberOfInstallments < 2) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.MoreThanOneInstallment"));
                component.find("numberOfInstallments").set("v.value", "");
                return;
            }
            let staggerDateField = component.find("staggerStartDate").get("v.value");
            if(staggerDateField === '' || staggerDateField === null){
                ntfSvc.error(ntfLib, $A.get("$Label.c.invalidDataFields"));
                return;
            }

            let showDemonstratedPayment = component.get("v.showDemonstratedPaymentField");
            let isCreditManagement = component.get("v.isCreditManagement");

            if (isCreditManagement) {
                component.set("v.isAutomatedRequest", true);
            } else if ((totalAmount > 0 && totalAmount < 5000) && (numberOfInstallments <= 6) && (showDemonstratedPayment && dueAmountPercentage >= 50)) {
                component.set("v.isAutomatedRequest", true);
            }
            else if ((totalAmount > 0 && totalAmount < 5000) && (numberOfInstallments <= 6) && !showDemonstratedPayment) {
            	component.set("v.isAutomatedRequest", true);
            }
             else {
                 component.set("v.isAutomatedRequest", false);
            }
            component.set("v.step", 6);
            console.log('Controller - frequancy: '+component.get("v.rateFrequency"));
        }
    },
    editStep: function (component, event) {
        let buttonPressed = event.getSource().getLocalId();

        if (buttonPressed === "returnStep0") {
            component.set("v.step", 0);
        }else if (buttonPressed === "returnStep1") {
            component.set("v.step", 2);
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 3);
        } else if (buttonPressed === "returnStep3") {
            component.set("v.step", 4);
        } else if (buttonPressed === "returnStep4") {
            component.set("v.step", 5);
        }
    },
    handleStaggerDateChanged: function (component) {
        let staggerStartDate = component.find("staggerStartDate").get("v.value");
        let dateOfToday = new Date();
        let monthDigit = dateOfToday.getMonth() + 1;
        let dayDigit = dateOfToday.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        let today = dateOfToday.getFullYear() + "-" + monthDigit + "-" + dayDigit;
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        if (today > staggerStartDate) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.StaggerDateBeforeCurrentDate"));
            component.find("staggerStartDate").set("v.value", "");
            component.set("v.staggerStartDate", "");
        } else {
            component.set("v.staggerStartDate", staggerStartDate);
        }
    },
    handleNumberOfInstallmentsChanged: function (component) {
        component.set("v.numberOfInstallments", component.find("numberOfInstallments").get("v.value"));
    },
    handleTotalAmountChanged: function (component) {
        component.set("v.totalAmount", component.find("totalAmount").get("v.value"));
    },
    checkFisaList: function (component, event) {
        let isEmptyFisaList = event.getParam("isEmptyFisaList");
        component.set('v.isEmptyFisaList', isEmptyFisaList);

    },
     handleSubProcessChange: function (component, event, helper) {
        let subProcessSelected = event.getParam('subProcessSelected');
        component.set("v.subProcessSelected", subProcessSelected);

    },
    getFisaSelect: function (component, event, helper) {
        let selectedFisaIds = event.getParam("fisaIds");
        let fisaData = event.getParam("fisaData");
        let totalAmount = event.getParam("totalAmount");
        component.set('v.selectedFisaIds', selectedFisaIds);
        component.set('v.fisaData', fisaData);
       let ntfLib = component.find('notifLib');
       let ntfSvc = component.find('notify');
       component.set("v.totalAmount", totalAmount);
       if(fisaData){
           let paymentCode = fisaData[0].paymentCode;
           console.log('###fisaData: '+JSON.stringify(fisaData));
           console.log('###paymentCode: '+paymentCode);
           component.set('v.paymentCode',paymentCode);
            //helper.getCompanyDivisionByPaymentCode(component, event, helper);
       }
    },
    handleDemonstratedAmountChanged: function (component) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let demonstratedAmountField = component.find("demonstratedAmount");
        let demonstratedAmount = demonstratedAmountField.get("v.value");
        let percentagePayment = 0;
        let totalAmount = component.get("v.totalAmount");
        if (!demonstratedAmountField.checkValidity()) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.invalidDataFields"));
            return;
        }
        if (demonstratedAmount && demonstratedAmount >= 0 && totalAmount > 0) {
            percentagePayment = (demonstratedAmount / totalAmount) * 100;
            component.set("v.dueAmountPercentageValue", Math.round(percentagePayment *100)  / 100);
        }
    },
    sendRepaymentDetailsToSAP: function (component, event) {
        let repaymentStartDate = component.get("v.staggerStartDate");
        let totalAmount = component.get("v.totalAmount");
        let numberOfInstallments = component.get("v.numberOfInstallments");
        let listOfInvoices = component.get("v.invoicesSelection");
        let fileNetId = '000001';
        return fileNetId;
    },
    save: function (component, event, helper) {
        helper.handleSave(component, helper)
    },
    cancel: function (component, event, helper) {
        component.set("v.showCancelBox", true);

    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    saveDraft: function (component, event, helper) {
        helper.handleSaveDraft(component, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam("selectedOrigin");
        let channelSelected = event.getParam("selectedChannel");
        if (originSelected && channelSelected) {
            component.set("v.originSelected", originSelected);
            component.set("v.channelSelected", channelSelected);
            component.set("v.step", 1);
            let dossier = component.get('v.dossier');
            if (dossier && dossier.Origin__c) {
                //component.set("v.step", 2);
                if (window.localStorage) {
                    localStorage.setItem(dossier.Id + 'origin', originSelected);
                    localStorage.setItem(dossier.Id + 'channel', channelSelected);
                }
            }
        } /*else {
            component.set("v.step", 0);
        }*/
    },

    handleSelectedRange: function (component, event) {
        component.set("v.invoiceStartDate", event.getParam("startDate"));
        component.set("v.invoiceEndDate", event.getParam("endDate"));
    },
    handleClickPenalties: function (component) {
        let state = component.get('v.penaltiesState');
        component.set('v.penaltiesState', !state);
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        if (event.getParam("divisionId")) {
            let selectedCompanyDivision = event.getParam("divisionId");
            component.set("v.companyDivisionId", selectedCompanyDivision);
        }
    }

});