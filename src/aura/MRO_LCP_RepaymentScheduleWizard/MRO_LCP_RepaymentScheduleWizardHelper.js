/**
 * Created by Vlad Mocanu on 12/11/2019.
 */

({
    initialize: function(component,helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        component.set("v.accountId", accountId);
        if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }
        this.hideSpinner(component);

        component.find("apexService").builder()
            .setMethod("initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "templateId": templateId,
                "genericRequestId": genericRequestId
            })
            .setResolve(function(response) {
                if (!response.error) {
                    component.set("v.accountId", response.accountId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    component.set("v.templateId", response.templateId);
                    component.set("v.repaymentRequest", response.repaymentRequest);
                    component.set('v.originSelected', response.dossier.Origin__c);
                    component.set('v.channelSelected', response.dossier.Channel__c);
                    component.set("v.companyDivisionId", response.companyDivisionId);
                    component.set("v.staggerStartDate", response.staggerStartDate);
                    if(response.listSupplies.length > 0){
                        component.set("v.showDemonstratedPaymentField", true);
                    }
                    let creditManagement = response.creditManagementId;

                    /*/if (response.dossierId && response.dossierId !== myPageRef.state.c__dossierId) {
                        self.updateUrl(component, accountId, response.dossierId,response.templateId);
                    }*/
                    if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                    }
                    const dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === "New" || dossierVal.Status__c === "Canceled") {
                            component.set('v.disableOriginChannel',true);
                            component.set("v.isClosed", true);
                        }
                        if(dossierVal.Channel__c && dossierVal.Status__c === "Draft"){

                            component.set("v.step",1)
                        }

                        if (dossierVal.Origin__c && dossierVal.Channel__c){
                            component.set("v.originSelected", dossierVal.Origin__c);
                            component.set("v.channelSelected", dossierVal.Channel__c);

                        }else {
                            if(window.localStorage && localStorage.getItem(dossierVal.Id+'origin') !== null){
                                component.set("v.originSelected",JSON.stringify(localStorage.getItem(dossierVal.Id+'origin')));
                                component.set("v.channelSelected",JSON.stringify(localStorage.getItem(dossierVal.Id+'channel')));
                            }
                        }
                        if (window.localStorage){
                            if(localStorage.getItem(dossierVal.Id)  !== null){
                                component.set("v.invoiceStartDateDefault",localStorage.getItem(dossierVal.Id));
                            }
                            if(localStorage.getItem(dossierVal.Id+'demonstratedPayment')  !== null){
                                component.set("v.demonstratedAmount",localStorage.getItem(dossierVal.Id+'demonstratedPayment'));
                            }
                            if(localStorage.getItem(dossierVal.Id+'workingDays')  !== null){
                                component.set("v.SLAWorkingDays",localStorage.getItem(dossierVal.Id+'workingDays'));
                            }
                        }
                    }
                    if (response.caseRecord) {
                        component.set("v.caseRecord", response.caseRecord);
                        component.set("v.caseId", response.caseRecord.Id);

                        component.set("v.notes", component.get("v.caseRecord").CustomerNotes__c);
                        component.set("v.rateFrequency", component.get("v.caseRecord").Frequency__c);
                        component.set("v.selectedFisaIds", component.get("v.caseRecord").MarkedInvoices__c);
                        if (response.caseRecord.StartDate__c) {
                            component.set("v.staggerStartDate", response.caseRecord.StartDate__c);
                        }
                        if (component.get("v.caseRecord").Count__c) {
                            component.set("v.numberOfInstallments", component.get("v.caseRecord").Count__c);
                        }
                        if (component.get("v.caseRecord").Amount__c) {
                            component.set("v.totalAmount", component.get("v.caseRecord").Amount__c);
                        }
                    }
                    //if (dossierVal["OwnerId"] === creditManagement){
                    if (response.caseRecord && response.caseRecord.OwnerId === creditManagement){
                        component.set("v.isAutomatedRequest", true);
                        component.set("v.isCreditManagement", true);
                        component.set("v.step", 6);
                    }
                    self.hideSpinner(component);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    self.hideSpinner(component);
                    if (response.redirectToDossier){
                        self.redirectToDossier(component, helper);
                    }
                }
            })
            .setReject(function(error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    initConsoleNavigation: function(component, wizardLabel) {
        let self = this;
        const { getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel } = component.find("workspace");
        isConsoleNavigation()
            .then(function(response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function(response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 1) {
                        window.location = window.location + "#loaded";
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function(error) {
        });
        document.title = "Lightning Experience | Salesforce";
    },
    showSpinner: function(component) {
        $A.util.removeClass(component.find("spinnerSection"), "slds-hide");
        $A.util.addClass(component.find("spinnerSection"), "slds-show");
    },
    hideSpinner: function(component) {
        $A.util.addClass(component.find("spinnerSection"), "slds-hide");
        $A.util.removeClass(component.find("spinnerSection"), "slds-show");
    },
    redirect: function(component, pageReference) {
        const { closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab } = component.find("workspace");
        isConsoleNavigation()
            .then(function(response) {
                if (response === true) {
                    getEnclosingTabId().then(function(enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function() {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function(error) {
                            console.log("error on Console Navigation ", error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function(error) {
            console.log(error);
        });
    },
    updateUrl: function(component, accountId, genericRequestId,  dossierId,templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: "standard__component",
            attributes: {
                componentName: "c__MRO_LCP_RepaymentScheduleWizard"
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    handleCancel: function(component, helper) {
        let self = this;
        let dossierId = component.get("v.dossierId");
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        component.find("apexService").builder()
            .setMethod("cancelRepayment")
            .setInput({
                "dossierId": dossierId,
                "caseId": component.get("v.caseId")
            })
            .setResolve(function(response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function(response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },
    getCompanyDivisionByPaymentCode: function(component, event, helper) {
        let self = this;
        let paymentCode = component.get("v.paymentCode");
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        component.find("apexService").builder()
            .setMethod("getCompanyDivisionByPaymentCode")
            .setInput({
                "paymentCode": paymentCode
            })
            .setResolve(function(response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.companyDivisionId', response.companyDivisionId);
                console.log('###companyDivisionId: '+response.companyDivisionId);
            })
            .setReject(function(response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },
    handleSaveDraft: function(component, helper) {
        let self = this;
        let workingDays = component.get("v.SLAWorkingDays");
        let totalAmount = component.get("v.totalAmount");
        let staggerStartDate = component.get("v.staggerStartDate");
        let numberOfInstallments = component.get("v.numberOfInstallments");
        let selectedFisaIds = component.get("v.selectedFisaIds");
        if (component.get("v.selectedFisaIds")) {
            if (component.get("v.selectedFisaIds").length > 0) {
                selectedFisaIds = component.get("v.selectedFisaIds").toString();
            }
        }
        let accountId = component.get("v.accountId");
        let dossierId = component.get("v.dossierId");
        let selectedChannel = component.get("v.channelSelected");
        let selectedOrigin = component.get("v.originSelected");
        let caseRecord = component.get("v.caseId");
        let isAutomatedRequest = component.get("v.isAutomatedRequest");
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let notes = component.get("v.notes");
        let rateFrequency = component.get("v.rateFrequency");
        let companyDivisionId = component.get("v.companyDivisionId");
        if (component.get("v.demonstratedAmount") && component.get("v.demonstratedAmount") > 0){
            let demonstrated = component.get("v.demonstratedAmount");
        }
        component.find("apexService").builder()
            .setMethod("createDraftCase")
            .setInput({
                "totalAmount": totalAmount,
                "staggerStartDate": staggerStartDate,
                "numberOfInstallments": numberOfInstallments,
                "listOfInvoices": selectedFisaIds,
                "accountId": accountId,
                "dossierId": dossierId,
                "selectedChannel": selectedChannel,
                "selectedOrigin": selectedOrigin,
                "caseId": caseRecord,
                "isAutomatedRequest": isAutomatedRequest,
                "SLAWorkingDays" :workingDays,
                "notes" : notes,
                "rateFrequency": rateFrequency,
                "companyDivisionId": companyDivisionId
            })
            .setResolve(function(response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (window.localStorage) {
                    localStorage.removeItem(dossierId+'origin');
                    localStorage.removeItem(dossierId+'channel');
                    localStorage.setItem(dossierId,component.get("v.invoiceStartDate"));
                    localStorage.setItem(dossierId+'workingDays',component.get("v.SLAWorkingDays"));
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function(response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },
    handleSave: function(component, helper) {
        let self = this;
        let workingDays = component.get("v.SLAWorkingDays");
        let totalAmount = component.get("v.totalAmount");
        let staggerStartDate = component.get("v.staggerStartDate");
        let numberOfInstallments = component.get("v.numberOfInstallments");
        let selectedFisaIds = component.get("v.selectedFisaIds").toString();
        //let fisaList = component.get("v.fisaList");
        let accountId = component.get("v.accountId");
        let dossierId = component.get("v.dossierId");
        let selectedChannel = component.get("v.channelSelected");
        let selectedOrigin = component.get("v.originSelected");
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let isAutomatedRequest = component.get("v.isAutomatedRequest");
        let caseRecord = component.get("v.caseId");
        let notes = component.get("v.notes");
        let fisaData = component.get("v.fisaData");
        let rateFrequency = component.get("v.rateFrequency");
        let companyDivisionId = component.get("v.companyDivisionId");
        if (component.get("v.demonstratedAmount") && component.get("v.demonstratedAmount") > 0){
            let demonstrated = component.get("v.demonstratedAmount");
        }
        if (window.localStorage) {
            localStorage.setItem(dossierId+'demonstratedPayment',component.get("v.demonstratedAmount"));
        }

        let isCreditManagement = component.get("v.isCreditManagement");
        if (isCreditManagement){
            isAutomatedRequest = true;
        }

        if (totalAmount === 0 || staggerStartDate === "" || numberOfInstallments <= 1 || selectedFisaIds === "" ||
            accountId === "" || dossierId === "") {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
        } else {
            helper.showSpinner(component);
            component.find("apexService").builder()
                .setMethod("createCase")
                .setInput({
                    "totalAmount": totalAmount,
                    "staggerStartDate": staggerStartDate,
                    "numberOfInstallments": numberOfInstallments,
                    "listOfInvoices": selectedFisaIds,
                    "accountId": accountId,
                    "dossierId": dossierId,
                    "selectedChannel": selectedChannel,
                    "selectedOrigin": selectedOrigin,
                    "caseId": caseRecord,
                    "isAutomatedRequest": isAutomatedRequest,
                    "SLAWorkingDays" :workingDays,
                    "notes" : notes,
                    "rateFrequency": rateFrequency,
                    "selectedfisaData": JSON.stringify(fisaData),
                    "companyDivisionId": companyDivisionId
                })
                .setResolve(function(response) {
                    helper.hideSpinner(component);
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    if (window.localStorage){
                        localStorage.removeItem(dossierId+'origin');
                        localStorage.removeItem(dossierId+'channel');
                    }
                    if (isAutomatedRequest) {
                        ntfSvc.success(ntfLib, $A.get("$Label.c.SuccessAutomatedRequest"));
                        localStorage.clear();
                    } else {
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.SuccessManualRequest"));
                    }

                    self.redirectToDossier(component, helper);
                })
                .setReject(function(response) {
                    helper.hideSpinner(component);
                    const errors = response.getError();
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                    return;
                })
                .executeAction();
        }
    },
    redirectToDossier: function(component, helper) {
        const pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: "c__Dossier",
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    handleInvoiceInquiryStartDateChange: function(component, event, helper) {
        let inputStartDate = new Date(component.get("v.invoiceStartDate"));
        let invoiceEndDate = new Date(component.get("v.invoiceEndDate"));
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        if (invoiceEndDate) {
            let parsedInputStartDate = Date.parse(component.get("v.invoiceStartDate"));
            let parsedInvoiceEndDate = Date.parse(component.get("v.invoiceEndDate"));
            let years = invoiceEndDate.getFullYear() - inputStartDate.getFullYear();
            let numberOfMonths = (years * 12) + (invoiceEndDate.getMonth() - inputStartDate.getMonth());

            console.log('### inputStartDate.getDate() '+inputStartDate.getDate());
            if (numberOfMonths > 36 || (numberOfMonths === 36 && inputStartDate.getDate() < invoiceEndDate.getDate())) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.maxNumberOfMonth"));
                return;
            }

            if (parsedInputStartDate > parsedInvoiceEndDate) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.StartDateAfterEndDate"));
                return;
            } else {
                component.set("v.step", 4);
            }
        }

        component.set("v.invoiceStartDate", inputStartDate);
    },
    setDates: function (component) {
        let todayDate = new Date();
        let todayDate1 = new Date();
        let todayDate2 = new Date();
        let monthDigit = todayDate.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        let threeMonthsAgo = new Date(todayDate1.setMonth(todayDate1.getMonth() - 12));
        let minDate = new Date(todayDate2.setMonth(todayDate2.getMonth() - 36));
        component.set("v.invoiceEndDate", todayDate.getFullYear() + "-" + monthDigit + "-" + todayDate.getDate());
        component.set("v.invoiceStartDateDefault", threeMonthsAgo.getFullYear() + "-" + (threeMonthsAgo.getMonth() + 1) + "-" + threeMonthsAgo.getDate());
        component.set("v.invoiceMinStartDate", minDate.getFullYear() + "-" + (minDate.getMonth() + 1) + "-" + minDate.getDate());
        component.set("v.staggerStartDate", todayDate.getFullYear() + "-" + monthDigit + "-" + todayDate.getDate());
    },
    updateDossierCompanyDivision: function (component) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component);
        let dossierId = component.get('v.dossierId');
        let companyDivisionId = component.get('v.companyDivisionId');
        component.find('apexService').builder()
            .setMethod("updateDossierCompanyDivision")
            .setInput({
                dossierId: dossierId,
                companyDivisionId: companyDivisionId,
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                //self.initialize(component, event, helper);
            }
        })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
});