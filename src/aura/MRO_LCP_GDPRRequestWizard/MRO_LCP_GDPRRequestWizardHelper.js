/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 27.03.20.
 */
({
    initialize: function (component) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        let caseId = component.get('v.caseId');
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId,
            })
            .setResolve(function (response) {
                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId);
                    return;
                }
                component.set("v.isClosed", response.isClosed);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.caseId", response.caseId);
                component.set("v.account", response.account);
                component.set("v.requestTypeToShowPrivacyChange", response.requestTypeToShowPrivacyChange);
                component.set("v.requestTypeToShowCustomerNeeds", response.requestTypeToShowCustomerNeeds);
                component.set("v.showSendingChannel", !response.dossier.Parent__c || !response.dossier.SendingChannel__c);

                let selectedCustomerNeeds = response.case && response.case.CustomerNeeds__c ? response.case.CustomerNeeds__c.split(';') : [];
                component.set('v.selectedCustomerNeeds', selectedCustomerNeeds);

                component.set('v.processingItems', response.customerNeedsOptions.map(function (v) {
                    return {label: v.key, value: v.value, checked: selectedCustomerNeeds.includes(v.value)};
                }));
                component.set('v.requestTypeValues', response.requestTypeOptions.map(function (v) {
                    return {label: v.key, value: v.value};
                }));

                if (!caseId && response.case) {
                    self.setCaseFields(component, response.case);
                }

                self.setStep(component, response);
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    setChannelAndOrigin: function (component) {
        component.find('apexService').builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
            })
            .executeAction();
    },

    upsertCase: function (component) {
        let helper = this;
        helper.showSpinner(component);
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("upsertCase")
            .setInput({
                caseId: component.get("v.caseId"),
                dossierId: component.get("v.dossierId"),
                requestType: component.get("v.requestType"),
                customerNeeds: component.get('v.selectedCustomerNeeds').join(';'),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
                accountId: component.get("v.accountId")
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
                if (!response.error && response.caseId) {
                    component.set('v.caseId', response.caseId);
                    helper.setCaseFields(component, response.case);
                }else{
                    ntfSvc.error(ntfLib, response.error);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
                helper.hideSpinner(component);
            })
            .executeAction();
    },
    setCaseFields: function (component, caseRecord) {
        if (!caseRecord) {
            return;
        }
        this.setRequestDetailsFields(component, caseRecord.ReferenceDate__c, caseRecord.SLAExpirationDate__c);
        component.set('v.requestType', caseRecord.SubProcess__c);
    },

    setRequestDetailsFields: function (component, referenceDate, slaExpirationDate) {
        function formatDate(date) {
            return date.getFullYear() + '-'
                + (date.getMonth() + 1) + '-'
                + date.getDate();
        }
        let today = new Date();
        let requestDate = referenceDate ? new Date(referenceDate) : today;
        if(requestDate.getTime() > today.getTime()){
            requestDate = today;
        }
        let expirationDate = slaExpirationDate ? new Date(slaExpirationDate) : new Date(requestDate.getTime() + (30 * 24 * 60 * 60 * 1000));
        setTimeout(function(){
            component.find('caseReferenceDate').set('v.value', formatDate(requestDate));
        }, 10);
        component.find('caseSlaExpirationDate').set('v.value', formatDate(expirationDate));
    },

    redirectToDossier: function(component){
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        this.redirect(component, pageReference);
    },

    performSaveProcess: function (component, helper, isDraft) {
        let dossierId = component.get("v.dossierId");

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("saveProcess")
            .setInput({
                dossierId: dossierId,
                caseId: component.get("v.caseId"),
                isDraft: isDraft
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                helper.redirectToDossier(component);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },

    handleSave: function (component, helper) {
        helper.performSaveProcess(component, helper, false);
    },

    handleSaveDraft: function (component, helper) {
        helper.performSaveProcess(component, helper, true);
    },

    getStepFromRequestType: function (component, requestType) {
        let showPrivacyChangeComponent = component.get("v.requestTypeToShowPrivacyChange").includes(requestType),
            showProcessingBlock = component.get("v.requestTypeToShowCustomerNeeds").includes(requestType),
            nextStep = showPrivacyChangeComponent || showProcessingBlock ? 2 : 3;

        component.set('v.requestType', requestType);
        component.set('v.showPrivacyChangeComponent', showPrivacyChangeComponent);
        component.set('v.showProcessingBlock', showProcessingBlock);
        return nextStep;
    },

    setStep: function (component, response) {
        let step = 0;
        if(response.dossier.Channel__c && response.dossier.Origin__c && response.case && response.case.SubProcess__c){
            step = 1;
        }

        if(response.case && response.case.SubProcess__c && !response.case.ReferenceDate__c){
            step = this.getStepFromRequestType(component, response.case.SubProcess__c); // 2 or 3
        }

        if(response.case && response.case.ReferenceDate__c){
            step = 4;

            if (!component.get('v.showSendingChannel') || response.dossier && response.dossier.SendingChannel__c) {
                step = 5;
            }
        }

        this.changeStep(component, this, step);
    },
    updateUrl: function (component, accountId, dossierId) {
        let self = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_GDPRRequestWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        self.redirect(component, pageReference, true);
    },

    changeStep: function (component, helper, stepNumber, isNext) {
        component.set('v.disableOriginChannel', stepNumber > 1);

        if (stepNumber === 3 && isNext) {
            if(component.get('v.showProcessingBlock')){
                helper.upsertCase(component);
            }
            if(component.get('v.showPrivacyChangeComponent')){
                component.find('privacyChange').savePrivacyChange();
            }
        }

        if (stepNumber === 4 && isNext) {
            component.find('requestDetailsForm').submit();
            return;
        }

        if (stepNumber === 5 && isNext) {
            component.find('sendingChannelSelection').saveSendingChannel();
            return;
        }

        component.set("v.step", stepNumber);
    },

    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference, overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },

    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },

})