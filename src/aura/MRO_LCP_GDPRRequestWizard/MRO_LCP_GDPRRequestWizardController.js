/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 27.03.20.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        helper.initialize(component);
        helper.initConsoleNavigation(component, $A.get("$Label.c.GDPRRequest"));
    },

    handleOriginChannelSelection: function (component, event, helper) {
        if (component.get("v.step") > 0) {
            return;
        }
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            helper.setChannelAndOrigin(component);
            helper.changeStep(component, helper, 1);
        }
    },

    handleRequestDetailsFormSuccess: function(component, event, helper){
        helper.changeStep(component, helper, component.get('v.showSendingChannel') ? 4 : 5);
    },

    handleSendingChannelSelection: function (component, event, helper) {
        helper.changeStep(component, helper, 5);
    },

    handleRequestTypeChange: function (component, event, helper) {
        helper.upsertCase(component);
        helper.changeStep(component, helper, helper.getStepFromRequestType(component, event.getParam("value")), true);
    },

    handleCheckCustomerNeeds: function (component, event, helper) {
        let value = event.getSource().get('v.name');
        let selectedNeeds = component.get('v.selectedCustomerNeeds');
        if(selectedNeeds.includes(value)){
            selectedNeeds = selectedNeeds.filter(function(v){return v !== value});
        }else{
            selectedNeeds.push(value);
        }
        component.set('v.selectedCustomerNeeds', selectedNeeds);
    },

    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().get('v.name');
        let stepNumber = +(buttonId.replace('confirmStep', '')) + 1;
        helper.changeStep(component, helper, stepNumber, true);
    },

    editStep: function (component, event, helper) {
        let buttonId = event.getSource().get('v.name');
        let stepNumber = +(buttonId.replace('editStep', ''));
        helper.changeStep(component, helper, stepNumber);
    },

    cancel: function (component, event, helper) {
        component.set('v.showCancelBox', true);
    },
    onCloseCancelBox: function(component){
        component.set('v.showCancelBox', false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component);
    },

    save: function (component, event, helper) {
        helper.handleSave(component, helper);
    },

    saveDraft: function (component, event, helper) {
        helper.handleSaveDraft(component, helper);
    },

    navigateToAccount: function (component, event, helper) {
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": component.get('v.accountId'),
                "objectApiName": "Account",
                "actionName": "view"
            }
        };
        helper.redirect(component, pageReference, true);
    },

    handleChangeReferenceDate: function (component, event, helper) {
        helper.setRequestDetailsFields(component, event.getSource().get('v.value'));
    }
})