({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const companyDivComponent = component.find("companyDivision");

        helper.initConsoleNavigation(component,$A.get('$Label.c.SwitchOut'));
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);

        /*if (companyDivComponent && !dossierId) {
            companyDivComponent.reload();
        } else if (dossierId) {
            component.set("v.hasCompanyDivisionFilled", true);

        }*/
        helper.initialize(component, event, helper);
    },
    getBillingProfileRecordId: function (component, event, helper) {
        component.set("v.billingProfileId", event.getParam("billingProfileRecordId"));
        if (component.get("v.billingProfileId")) {
            component.set("v.step", 3)
        }
    },
    handleSupplyResult: function (component, event, helper) {
        component.set('v.effectiveDate', '');
        component.set('v.trader', '');
        const searchedSupplyFieldsList =  event.getParam("supplyFieldsValue");
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let errSupplyAlreadySelected =[];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];

        for (let i = 0; i < searchedSupplyFieldsList.length; i++) {
            let validSupply = true;
            let oneSupply = searchedSupplyFieldsList[i];
            if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                continue;
            }
            for (let j = 0; j < caseList.length; j++) {
                if (oneSupply.Id === caseList[j].Supply__c) {
                    errSupplyAlreadySelected.push(oneSupply.Name);
                    validSupply = false;
                    break;
                }
            }
            if (validSupply){
                validSupplies.push(oneSupply);
            }
        }
        if (errSupplyAlreadySelected.length !== 0){
            let messages = errSupplyAlreadySelected.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' +messages);
        }
        if (errCurrentSupplyIsNotValid.length !== 0){
            let messages = errCurrentSupplyIsNotValid.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' +messages);
        }

        component.set("v.searchedSupplyFields", validSupplies);
        component.set("v.showNewCase", (validSupplies.length !== 0));
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleCreateCase: function (component, event, helper) {
        event.preventDefault();
        if (helper.validateFields(component, event)) {
            helper.showSpinner(component,'spinnerSectionModal');
            helper.createCase(component, event, helper);
        }
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((caseList.length === 0) || (!billingProfileId)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.updateCase(component, helper);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if (!component.get("v.caseTile") || component.get("v.caseTile").length === 0) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
                    return;
                }
                /*const step1 = document.getElementById("step-2");
                step1.scrollIntoView(({ block: "end",behavior: "smooth"}));*/
                helper.resetSupplyForm(component);
                if (component.get("v.billingProfileId")) {
                    component.set("v.step", 3)
                }else{
                    component.set("v.step", 2);
                }
                helper.resetSupplyForm(component);

                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                /*let step1 = document.getElementById("step-1");
                step1.scrollIntoView(({ behavior: "smooth"}));*/
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                /*let step2 = document.getElementById("step-2");
                step2.scrollIntoView(({ behavior: "smooth"}));*/
                break;
            default:
                break;
        }
    },
    removeError: function (component, event, helper) {
        const labelFieldList = [];
        let valEffectiveDate = component.find("Field_EffectiveDate__c");
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }
        let valTrader = component.find("Field_Trader__c");
        if (!Array.isArray(valTrader)) {
            valTrader = [valTrader];
        }
        tmp = valTrader[0].get('v.value');
        if ((tmp != null) && (tmp.trim() != '')) {
            labelFieldList.push('Field_Trader__c');
        }
        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },
    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftBillingProfile(component, helper);
    }
})