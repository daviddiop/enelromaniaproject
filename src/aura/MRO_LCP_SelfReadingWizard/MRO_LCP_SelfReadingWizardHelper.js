({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        //const dossierId = myPageRef.state.c__dossierId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;

        console.log('dossierId='+dossierId);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);

        component.set("v.showCancelReasonModal", false);
        component.find('api').builder()
            .setMethod("initialize")
            .setInput({
                'accountId': accountId,
                'dossierId': dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if(response.originSelected && response.originSelected){
                    component.set('v.disableOriginChannel',true);
                }else{
                    component.set('v.disableOriginChannel',false);
                }
                component.set("v.caseTile", response.caseTile);
                component.set("v.accountId", response.accountId);
                component.set("v.originSelected", response.originSelected);
                component.set("v.channelSelected", response.channelSelected);
                component.set("v.templateId", response.templateId);
                //component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.showMeters", false);
                self.getSavedMeters(component);

                if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                }
                if (response.companyDivisionId) {
                    component.set("v.companyDivisionId", response.companyDivisionId);
                }

                let step = 0;
                if(response.originSelected && response.originSelected){
                    step = 1;
                }

                if (response.caseTile && response.caseTile.length !== 0) {
                    step = 2;
                    console.log("Called getMeters");
                    //helper.getMeters(component);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));

                    if(response.caseTile[0].Supply__r){
                        component.set("v.searchedSupplyFields", response.caseTile[0].Supply__r);
                        component.set("v.recordTypeName", response.caseTile[0].Supply__r.RecordType.DeveloperName);
                    }
                }
               /* if (response.metersJson && response.metersJson.length !== 0) {
                    step = 3;
                }*/
                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                        component.set('v.disableOriginChannel', true);
                    }
                }

                component.set("v.step", step);
                //component.set("v.account", response.account);
                console.log("Step = " + step);
            })
            .setReject(function (errorMsg) {
                self.hideSpinner(component, 'spinnerSection');
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    supplyList: function (component, event, helper,selectedSupplyIds) {
        const self = this;
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let errSupplyAlreadySelected =[];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];
        //let accountId = component.get("v.accountId");
        //console.log('isBusinessClientAccount: '+ accountId);
        component.find('api').builder()
            .setMethod('checkSelectedSupplies')
            .setInput({
                "selectedSupplyIds": JSON.stringify(selectedSupplyIds)
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                const searchedSupplyFieldsList =  response.supplies;

                for (let i = 0; i < searchedSupplyFieldsList.length; i++) {
                    let validSupply = true;
                    let oneSupply = searchedSupplyFieldsList[i];
                    /*if (oneSupply.NonDisconnectable__c) {
                        ntfSvc.error(ntfLib, $A.get('$Label.c.NonDisconnectablePod'));
                        return;
                    }*/
                    if ((!oneSupply.ContractAccount__c) || (oneSupply.ContractAccount__c === null)) {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountNotExist"));
                        return;
                    }
                    if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                        errCurrentSupplyIsNotValid.push(oneSupply.Name);
                        continue;
                    }
                    if(caseList){
                        for (let j = 0; j < caseList.length; j++) {
                            if (oneSupply.Id === caseList[j].Supply__c) {
                                errSupplyAlreadySelected.push(oneSupply.Name);
                                validSupply = false;
                                break;
                            }
                        }
                    }

                    if (validSupply){
                        validSupplies.push(oneSupply);
                    }
                }
                if (errSupplyAlreadySelected.length !== 0){
                    let messages = errSupplyAlreadySelected.join(' ');
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' +messages);
                }
                if (errCurrentSupplyIsNotValid.length !== 0){
                    let messages = errCurrentSupplyIsNotValid.join(' ');
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' +messages);
                }

                if(validSupplies.length !== 0){
                    component.set("v.recordTypeName", validSupplies[0].RecordType.Name);
                    component.set("v.searchedSupplyFields", validSupplies);
                    self.createCase(component, event, helper, validSupplies);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    checkSelfReadingInterval: function (component, helper) {
        const self= this;
        self.showSpinner(component, 'spinnerSection');
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
       // const selectedSupply = component.get("v.searchedSupplyFields");
        component.find('api').builder()
            .setMethod("getSelfReadingInterval")
            .setInput({
                'dossierId': dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (!response.isInsideInterval) {
                    component.set('v.step', 2);
                    ntfSvc.error(ntfLib, $A.get('$Label.c.SelfReadingIntervalExceeded'));
                    return;
                }
                component.set("v.showMeters", true);
                component.set("v.step", 3);
                //self.getSavedMeters(component);
                if(!component.get('v.hasSavedIndexes')){
                    self.getMeters(component);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },

    getMeters: function (component, helper) {
        const meterList = component.find('meterList');
        const selectedSupply = component.get("v.searchedSupplyFields");
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        component.find('api').builder()
            .setMethod("getMeters")
            .setInput({
                'selectedSupply': JSON.stringify(selectedSupply)
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set("v.showMeters", true);
                console.log("response.metersJson2: " + JSON.stringify(response.metersJson));
                component.set("v.metersJson", JSON.parse(response.metersJson));
                meterList.setMeterList(component.get("v.metersJson"));
                component.set("v.step", 3);

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },

    getSavedMeters: function (component) {
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        const dossierId = component.get("v.dossierId");
        const meterList = component.find('meterList');
        const self = this;
        console.log("dossierId meter:   " + dossierId);
        if(dossierId){
            component.find('api').builder()
                .setMethod("getSavedMeters")
                .setInput({
                    'dossierId': dossierId
                })
                .setResolve(function (response) {
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    console.log("response.metersJson length: " + JSON.stringify(response.metersJson));
                    if(JSON.stringify(response.metersJson) !== '{}'){
                        component.set("v.showMeters", true);
                        let savedMetersJson = JSON.parse(Object.values(response.metersJson)[0].toString());
                        component.set("v.metersJson", savedMetersJson);
                        console.log("response.metersJson: " + JSON.stringify(savedMetersJson));
                        meterList.setMeterList(component.get("v.metersJson"));

                        //component.set('v.hasSavedIndexes',true);
                        component.set("v.step", 3);
                    }

                })
                .setReject(function (errorMsg) {
                    ntfSvc.error(ntfLib, errorMsg);
                }).executeAction()
        }
    },

    updateIndexValues: function (component, meterInfo) {
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        const selectedSupply = component.get("v.searchedSupplyFields");
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId")
        console.log("updateIndexValues:: meterInfo " + JSON.stringify(meterInfo));
        let meterInfoElems = Object.values(meterInfo);
        console.log("object values = " + meterInfoElems)

        for (let i = 0; i < meterInfoElems.length; i++) {
            component.find('api').builder()
                .setMethod("createIndex")
                .setInput({
                    'caseList': JSON.stringify(caseList),
                    'selectedSupply': JSON.stringify(selectedSupply),
                    'accountId': accountId,
                    'meterInfo': JSON.stringify(meterInfoElems[i])
                })
                .setResolve(function (response) {
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                })
                .setReject(function (errorMsg) {
                    ntfSvc.error(ntfLib, errorMsg);
                }).executeAction()
        }
    },

    saveDraft: function (component, helper) {
        const self = this;
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("SaveDraft")
            .setInput({
                'oldCaseList': caseList,
                'dossierId': dossierId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },

    createCase: function (component, event, helper,validSupplies) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseList = component.get("v.caseTile");
        console.log('Caselist: ' + caseList );
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
       // const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const channelSelected = component.get("v.channelSelected");
        const originSelected = component.get("v.originSelected");

        component.find('api').builder()
            .setMethod("createCase")
            .setInput({
                'supplies': JSON.stringify(validSupplies),
                'caseList': caseList ? JSON.stringify(caseList) : null,
                'accountId': accountId,
                'dossierId': dossierId,
                'channelSelected':channelSelected,
                'originSelected':originSelected
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, result.errorMsg);
                    return;
                }
                if (response.cases && response.cases.length !== 0) {
                    component.set("v.step", 2);
                    component.set("v.companyDivisionId", response.cases[0].CompanyDivision__c);
                }
                component.set("v.showNewCase", false);
                component.set("v.caseTile", response.cases);
                component.set("v.osiTableView", response.cases.length > component.get("v.tileNumber"));
               // helper.resetSupplyForm(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
    updateCase: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("updateCaseList")
            .setInput({
                'oldCaseList': JSON.stringify(caseList),
                'dossierId': dossierId,
                'searchedSupplyFieldsList': JSON.stringify(searchedSupplyFieldsList)
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },

    handleCancel: function (component, helper) {
        const self = this;
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("cancelProcess")
            .setInput({
                'oldCaseList': caseList,
                'dossierId': dossierId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_SelfReadingWizard',
            },
            state: {

                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const self = this;
        const deleteRecordId = event.getParam("deleteRecord");
        const meterList = component.find('meterList');

        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id != deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        if (items.length === 0) {
            component.set("v.step", 2);
            component.set("v.showMeters", false);
            component.set("v.hasSavedIndexes",false);
        }

        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                        workspaceAPI.openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            workspaceAPI.closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = "Lightning Experience | Salesforce";
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    reloadIndexes: function (component) {
        let meterListComponent = component.find("meterList");
        if (meterListComponent instanceof Array) {
            let meterListComponentToObj = Object.assign({}, meterListComponent);
            meterListComponentToObj[0].setMeterList(component.get("v.metersJson"));
        } else {
            meterListComponent.setMeterList(component.get("v.metersJson"));
        }
    },
    updateOriginAndChannelOnDossier: function (component, event, helper,origin,channel) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('api').builder()
            .setMethod("updateOriginAndChannelOnDossier")
            .setInput({
                dossierId: dossierId,
                channelSelected: channel,
                originSelected: origin
            }).setResolve(function (response) {
            if (!response.error) {
                //self.initialize(component, event, helper);
            }
        })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
    updateCompanyDivisionAndCommodityOnDossier: function (component, event, helper,companyDivisionId,caseRecordTypeId) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component, 'spinnerSection');
        let dossierId = component.get('v.dossierId');
        component.find('api').builder()
            .setMethod("updateCompanyDivisionAndCommodityOnDossier")
            .setInput({
                dossierId: dossierId,
                companyDivisionId: companyDivisionId,
                caseRecordTypeId: caseRecordTypeId
            }).setResolve(function (response) {
            self.hideSpinner(component, 'spinnerSection');
            if (!response.error) {
                //self.initialize(component, event, helper);
            }
        })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    },
    setCommodityFilter: function (component) {
        let commodityEle = 'Electric';
        let commodityGas = 'Gas';
        let commodityConditionEle ="RecordType.DeveloperName='"+commodityEle+"'";
        let commodityConditionGas ="RecordType.DeveloperName='"+commodityGas+"'";
        let orConditions = [];
        orConditions.push(commodityConditionEle);
        orConditions.push(commodityConditionGas);
        component.set("v.orConditions",orConditions);
    },

})