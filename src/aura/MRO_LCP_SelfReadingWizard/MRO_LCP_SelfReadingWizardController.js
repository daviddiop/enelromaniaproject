({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;


        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);
        helper.setCommodityFilter(component);
        helper.initConsoleNavigation(component,$A.get('$Label.c.SelfReading'));
        helper.initialize(component, event, helper);
    },

    handleMeterListValidated: function (component, event, helper){
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        let meterInfo = event.getParam("meterInfo");
        if(!event.getParam("isMeterValidated")){
            component.set('v.step', 3)
            ntfSvc.error(ntfLib, "All Index Values Are Not Validated");
            return;
        }
        if(event.getParam("isMeterValidated"))
        {
            component.set('v.hasSavedIndexes', true);
            helper.updateIndexValues(component, meterInfo);
            ntfSvc.success(ntfLib, "All Index Values Are Validated");
            component.set('v.step', 4);
        }

    },

    getSupplies: function (component, event, helper) {

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let caseList = component.get('v.caseTile');
        if (caseList && Array.isArray(caseList) && caseList.length > 0) {
            ntfSvc.error(ntfLib, $A.get('$Label.c.OnlyOneSupplySelectedAtTme'));
            return;
        }
        let selectedSupplyIds = event.getParam("selected");
        helper.supplyList(component, event, helper, selectedSupplyIds);
        //helper.supplyValidityCheck(component, event, helper);
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (!caseList) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.updateCase(component, helper);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        let wizardHeader = component.find("wizardHeader");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':

                if(wizardHeader){
                    component.set('v.disableOriginChannel',true);
                    //wizardHeader.disableOriginChannelComponent();
                    component.set("v.step", 2);
                }
                helper.updateOriginAndChannelOnDossier(component, event, helper, component.get("v.originSelected"), component.get("v.channelSelected"));
                break;
            case 'confirmStep2':
                if (!component.get("v.caseTile")) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
                    return;
                }
                if(component.get("v.caseTile")){
                    helper.updateCompanyDivisionAndCommodityOnDossier(component, event, helper, component.get("v.caseTile")[0].CompanyDivision__c,component.get("v.caseTile")[0].RecordTypeId);
                }
                helper.checkSelfReadingInterval(component);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        let wizardHeader = component.find("wizardHeader");
        if(buttonPressed === "returnStep1"){
            if(wizardHeader){
                component.set('v.disableOriginChannel',false);
                component.set("v.step", 1);
            }
            //component.set('v.disableOriginChannel',true);
            //component.set("v.step", 1);
        }else if (buttonPressed === "returnStep2") {
            component.set("v.step", 2);
        }
    },
    cancel: function (component, event, helper) {
        component.set("v.showCancelReasonModal", true);
        //component.find('cancelReasonSelection').open();
        //helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraft(component, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected){
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            //component.set('v.disableAgree', false);
            component.set("v.step",1);
            const caseTiles = component.get("v.caseTile");
            if (caseTiles && Array.isArray(caseTiles) && caseTiles.length !==0){
                component.set("v.step",2);
            }
        }else {
            //component.set('v.disableAgree', true);
            component.set("v.step",0);
        }
    },
    closeCancelReasonModal: function (component) {
        component.set("v.showCancelReasonModal", false);
    },
    saveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
        //helper.handleCancel(component, helper);
    }
})