({
    isCancellation: function(component){
        return component.get("v.currentFlow") === component.get("v.flows").CANCELLATION;
    },

    initialize: function (component, helper) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let templateId = myPageRef.state.c__templateId;
        let currentStepTitle = myPageRef.state.c__currentStepTitle;
        let dossierId = myPageRef.state.c__dossierId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let companyDivComponent = component.find("companyDivision");
        let genericRequestId = myPageRef.state.c__genericRequestDossierId;

        component.set("v.companyDivisionId", "");
        component.set("v.contractId", "");
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);

        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
            })
            .setResolve(function (response) {
                if (response.opportunityId && !opportunityId) {
                    // reload the page with opportunityId param and reRun init method
                    helper.cleanPersistentAttributes();
                    self.updateUrl(component, accountId, response.opportunityId, response.dossierId, templateId);
                    return;
                }
                component.set("v.account", response.account);
                if (!response.account.IsPersonAccount) {
                    self.hideSpinner(component);
                    ntfSvc.error(ntfLib, $A.get('$Label.c.PhotovoltaicPanelOnlyForPersonAccount'));
                    return;
                }
                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunity", response.opportunity);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.contractId", response.contractIdFromOpp);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
                component.set("v.useBit2WinCart", response.useBit2WinCart);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.caseRecord", response.caseRecord);
                component.set("v.contractName", response.eneltel);
                component.set("v.contractType", 'Residential');
                component.set("v.user", response.user);
                component.set("v.companySignedId", response.user.Id);

                helper.setCurrentFlow(component, helper, response.currentFlow);

                if (!helper.isNullOrEmpty(response.opportunityServiceItems)) {
                    component.set("v.opportunityServiceItemId", response.opportunityServiceItems[0].Id);
                }
                if (!helper.isNullOrEmpty(response.currentSupply)) {
                    component.set("v.selectedToActivateSupplyId", response.currentSupply.Id);
                    component.set("v.selectedCommodity", response.currentSupply.RecordType.DeveloperName);
                }
                if (response.opportunityCompanyDivisionId) {
                    component.set("v.companyDivisionId", response.opportunityCompanyDivisionId);
                }
                if (companyDivComponent) {
                    companyDivComponent.setCompanyDivision();
                }

                helper.fillStepIndexes(component, helper);
                let opportunityVal = response.opportunity;
                if (opportunityVal) {
                    if (opportunityVal.StageName === 'Closed Lost' || opportunityVal.StageName === 'Closed Won') {
                        component.set("v.isClosed", true);
                    }
                }
                component.set("v.accountId", response.accountId);
                helper.setFiltrationParams(component, helper);
                helper.updateSearchConditions(component, helper);
                helper.restoreCurrentStep(component, helper);
                helper.restoreLocalState(component, helper);

                self.hideSpinner(component);

                setTimeout($A.getCallback(function() {
                    let currentStep = component.get('v.step');
                    if (currentStep) {
                        let stepBlock = helper.getAuraComponent(component, currentStep);
                        if (stepBlock && stepBlock.getElement()) {
                            stepBlock.getElement().scrollIntoView({behavior: "smooth", block: "end"});
                        }
                    }
                }), 1500);

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    restoreCurrentStep: function (component, helper) {
        let supplyToActivateId = component.get("v.selectedToActivateSupplyId");
        let contractId = component.get("v.contractId");
        let oliList = component.get("v.opportunityLineItems");
        let osiList = component.get("v.opportunityServiceItems");
        let currentFlow = component.get("v.currentFlow");
        let caseRecord = component.get("v.caseRecord");

        let currentStepTitle = component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title;

        let originSelected = component.get('v.originSelected');
        let channelSelected = component.get('v.channelSelected');

        if (!helper.isNullOrEmpty(oliList)) {
            currentStepTitle = component.get("v.steps").SET_PRODUCTS.Title;
        }
            // else if (!helper.isNullOrEmpty(contractId)) {
            //     currentStepTitle = component.get("v.steps").SET_CONTRACT.Title;
        // }
        else if (!helper.isNullOrEmpty(supplyToActivateId) || (osiList && osiList.length) || caseRecord) {
            currentStepTitle = component.get("v.steps").SET_SUPPLY_TO_ACTIVATE.Title;
        } else if (!helper.isNullOrEmpty(currentFlow)){
            currentStepTitle = component.get("v.steps").SHOW_SCRIPT_MANAGEMENT.Title;
        } else if (originSelected && channelSelected) {
            currentStepTitle = component.get("v.steps").SET_TYPE_OF_WORK.Title;
        }

        component.set("v.step", currentStepTitle);
        // if (currentStepTitle !== component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title) {
        //     helper.goToNextStep(component, helper);
        // }
    },
    updateSearchConditions: function(component, helper) {
        let currentFlow = component.get("v.currentFlow");
        let photovoltaicProductCategory = $A.get('$Label.c.PhotovoltaicProductCategory');

        let supplyToActivateSearchConditions = [];
        supplyToActivateSearchConditions.push(`(Account__c = '${component.get('v.accountId')}')`);
        if (currentFlow === component.get("v.flows").CREATION) {
            supplyToActivateSearchConditions.push(`(RecordType.DeveloperName = 'Electric' OR RecordType.DeveloperName = 'Gas')`);
        } else if (currentFlow === component.get("v.flows").CANCELLATION) {
            supplyToActivateSearchConditions.push(`(RecordType.DeveloperName = 'Service')`);
            supplyToActivateSearchConditions.push('(Product__c != null)');
            supplyToActivateSearchConditions.push('(Product__r.CommercialProduct__c != null)');
            supplyToActivateSearchConditions.push('(Product__r.CommercialProduct__r.ProductCategory__c = \'' + photovoltaicProductCategory + '\')');
        }
        component.set("v.supplyToActivateSearchConditions", supplyToActivateSearchConditions);
    },
    updateCurrentFlow: function (component, helper) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        const accountId = component.get("v.accountId");
        const opportunityId = component.get("v.opportunityId");
        const currentFlow = component.get("v.currentFlow");

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateCurrentFlow')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "currentFlow": currentFlow,
                "dossierId": component.get('v.dossierId')
            })
            .setResolve(function (response) {
                if (response.hasOpenCases && response.hasOpenCases === true) {
                    ntfSvc.warn(ntfLib, $A.get('$Label.c.PhotovoltaicPanelOpenAcquisitionProcess'));
                }
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateDates: function (component, helper, activationDate, terminationDate) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const osiId = component.get("v.opportunityServiceItemId");

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateDates')
            .setInput({
                "osiId": osiId,
                "activationDate": activationDate,
                "terminationDate": terminationDate
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    fillStepIndexes: function (component, helper) {
        let stepIndexes = {};

        let i = 0;
        for (const stepName in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(stepName)) {
                stepIndexes[stepName] = i;
                i++;
            }
        }

        component.set("v.stepIndexesMap", stepIndexes);
    },
    setFiltrationParams: function (component, helper) {
        let newSupplyColumns = [];

        let existingSupplyColumns = [
            {key: 'RecordType.Name', label: 'Type'},
            {key: 'Name', label: 'Supply'},
            {key: 'Status__c', label: 'Status'},
            {key: 'ContractAccount__r.Name', label: 'Contract Account'},
            {key: 'Contract__r.ContractNumber', label: 'Contract'},
            {key: 'ActivationDate__c', label: 'Activation Date'},
            {key: 'TerminationDate__c', label: 'Termination Date'},
            {key: 'Product__r.Name', label: 'Product'}
        ];

        // component.set("v.newSupplyColumns", newSupplyColumns);
        // component.set("v.existingSupplyColumns", existingSupplyColumns);
    },
    setCurrentFlow: function (component, helper, currentFlow) {
        let typeOfWork = $A.get('$Label.c.SelectTypeOfWork');
        component.set("v.currentFlow", currentFlow);

        switch (currentFlow) {
            case component.get("v.flows").CREATION:
                typeOfWork = $A.get('$Label.c.PhotovoltaicPanelsCreation');
                break;
            case component.get("v.flows").CANCELLATION:
                typeOfWork = $A.get('$Label.c.PhotovoltaicPanelCancellation');
                break;
            default:
                component.set("v.currentFlow", '');
        }
        component.find("typeOfWork").set("v.value", typeOfWork);
        helper.setSupplyStatus(component);
    },
    getStepFromIndex: function (component, stepIndex) {
        let index = 0;
        for (const stepTitle in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(stepTitle)) {
                if (index === stepIndex) {
                    return component.get("v.steps")[stepTitle];
                }
                index++;
            }
        }
        return null;
    },
    getIndexFromStepTitle: function (component, stepTitle) {
        let stepIndex = 0;
        for (const step in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(step)) {
                if (step === stepTitle) {
                    return stepIndex;
                }
                stepIndex++;
            }
        }
        return null;
    },
    getNextStep: function (component, helper, stepValue) {
        if (stepValue === null) {
            stepValue = 0;
        }
        let stepIndex = this.getIndexFromStepTitle(component, stepValue);
        let nextStep;
        let stepsCount = Object.keys(component.get("v.steps")).length;
        if ((stepIndex >= stepsCount - 1)) {
            return this.getStepFromIndex(component, stepsCount - 1);
        }
        for (let i = stepIndex + 1; i <= stepsCount; i++) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (helper.representedInFlow(component, helper, currentStep, component.get("v.currentFlow"))) {
                    nextStep = currentStep;
                    break;
                }
            }
        }
        return nextStep;
    },
    getStepFromTitle: function (component, helper, title) {
        let step = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (currentStep.Title === title) {
                    step = currentStep;
                    break;
                }
            }
        }
        return step;
    },
    getCurrentStep: function (component, helper) {
        let currentStepTitle = component.get("v.step");
        if (helper.isNullOrEmpty(currentStepTitle)){
            return ;
        }
        return helper.getStepFromTitle(component, helper, currentStepTitle);
    },
    getFirstStep: function (component, helper) {
        let firstStep = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (helper.representedInFlow(component, helper, currentStep, component.get("v.currentFlow"))) {
                    firstStep = currentStep;
                    break;
                }
            }
        }
        return firstStep;
    },
    getPreviousStep: function (component, helper, stepTitle) {
        let stepIndex = this.getIndexFromStepTitle(component, stepTitle);
        let previousStep;

        if ((stepIndex === 0) || (stepIndex - 1) === 0) {
            return this.getStepFromIndex(component, 0);
        }

        for (let i = stepIndex - 1; i >= 0; i--) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (helper.representedInFlow(component, helper, currentStep, component.get("v.currentFlow"))) {
                    previousStep = currentStep;
                    break;
                }
            }
        }

        return previousStep;
    },
    representedInFlow: function (component, helper, step, flowTitle){
        let result = false;
        for (let i = 0; i <= Object.keys(step.Flows).length; i++) {
            let flowName = step.Flows[i];
            if (flowName === 'ALL'){
                result = true;
            } else if (component.get("v.flows").hasOwnProperty(flowName)) {
                let stepFlowTitle = component.get("v.flows")[flowName];
                if (stepFlowTitle === flowTitle){
                    result = true;
                }
            }
        }
        return result;
    },
    setCompanyDivisionFromSupply: function (component, helper, companyDivisionId) {
        let companyDivisionComponent = component.find("companyDivision");
        let availableCompanyDivisions = companyDivisionComponent.getCompanyDivisions();
        let result = false;

        availableCompanyDivisions.forEach(companyDivision => {
            if (companyDivision.value === companyDivisionId) {
                component.set("v.companyDivisionId", companyDivision.value);
                companyDivisionComponent.setCompanyDivision();
                result = true;
            }
        });
        return result;
    },
    isNullOrEmpty: function (variable) {
        return (variable === undefined || variable === null || variable.length === 0);
    },
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent instanceof Array) {
            let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
            privacyChangeComponentToObj[0].savePrivacyChange();
        } else {
            privacyChangeComponent.savePrivacyChange();
        }
    },
    goToNextStep: function (component, helper) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let currentStepTitle = component.get("v.step");
        let nextStep = helper.getNextStep(component, helper, currentStepTitle);

        switch (currentStepTitle) {
            case component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title:
                break;
            case component.get("v.steps").SET_COMPANY_DIVISION.Title:
                break;
            case component.get("v.steps").SET_TYPE_OF_WORK.Title:
                if (component.get("v.currentFlow") === '') {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SetTypeOfWorkErrorMsg"));
                    return;
                }
                helper.updateCurrentFlow(component, helper);
                helper.updateSearchConditions(component, helper);
                helper.setSupplyStatus(component);

                nextStep = helper.getNextStep(component, helper, nextStep.Title);
                break;
            case component.get("v.steps").SET_SUPPLY_TO_ACTIVATE.Title:
                nextStep = helper.getNextStep(component, helper, nextStep.Title);
                break;
            case component.get("v.steps").SET_CONTRACT.Title:
                let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
                component.set("v.customerSignedDate", newContractDetails.customerSignedDate);
                // component.set("v.contractType", newContractDetails.type);

                if (helper.isNullOrEmpty(newContractDetails.type)){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.InvalidContractType"));
                    return;
                }
                helper.updateContractAndContractSignedDateOnOpportunity(component);
                break;
            case component.get("v.steps").SET_PRODUCTS.Title:
                let oliList = component.get("v.opportunityLineItems");
                let opportunityId = component.get("v.opportunityId");
                if (!oliList && oliList.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct"));
                    return;
                }

                let utilityProductOlis = oliList.filter(
                    oli => oli.Product2.RecordType.DeveloperName === "Utility"
                );

                if (!utilityProductOlis || utilityProductOlis.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct"));
                    return;
                }

                let oli = utilityProductOlis[0];

                if (component.get("v.isProductBundle") === true) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.BundleProductErrorMsg"));
                    return;
                }

                if (oli.Quantity > 1) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.PhotovoltaicMaxQtyLimit"));
                    return;
                }

                helper.linkOliToOsi(component, oli);
                helper.createAsset(component, opportunityId)
                break;
            case component.get("v.steps").BENEFICIARY_INFORMATION.Title:
                let beneficiaryInformationElement = component.find("beneficiaryInformationElement");
                if (!beneficiaryInformationElement){
                    break;
                }

                if (beneficiaryInformationElement.isEmpty()){
                    beneficiaryInformationElement.copyBeneficiaryInformation(component.get("v.accountId"));
                    break;
                }

                let validationResult = beneficiaryInformationElement.validateFields();
                if (validationResult === false) {
                    return;
                }

                if (validationResult === true){
                    beneficiaryInformationElement.saveBeneficiaryInformation();
                }
                break;
            case component.get("v.steps").SET_SENDING_CHANNEL.Title:
                helper.saveSendingChannel(component);
                break;
        }

        component.set("v.step", nextStep.Title);

        // to order the steps
        currentStepTitle = component.get("v.step");
        let currentStepIndex = helper.getIndexFromStepTitle(component, currentStepTitle);

        let previousStepTitle = helper.getPreviousStep(component, helper, currentStepTitle).Title;
        let previousStepIndex = helper.getIndexFromStepTitle(component, previousStepTitle);

        component.set("v.currentStepIndex", currentStepIndex);
        component.set("v.currentStepIndex", previousStepIndex);
    },
    setSupplyStatus: function (component) {
        const currentFlow = component.get('v.currentFlow');
        if (currentFlow === component.get("v.flows").CREATION) {
            component.set('v.supplyStatus', ['Active']);
        } else if (currentFlow === component.get("v.flows").CANCELLATION) {
            component.set('v.supplyStatus', ['Active', 'Activating']);
        }

    },
    getAuraComponent: function (component, fieldId) {
        return Array.isArray(component.find(fieldId)) ? component.find(fieldId)[0] : component.find(fieldId);
    },
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunityId = component.get('v.opportunityId');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');

        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: opportunityId,
                dossierId: dossierId,
                origin: origin,
                channel: channel
            })
            .setResolve(function (response) {
                // console.log(JSON.parse(JSON.stringify(response)))
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    saveOpportunity: function (component, helper, stage, callback, cancelReason, detailsReason) {
        let self = this;
        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");

        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let dossierId = component.get("v.dossierId");
        let assetId = component.get("v.assetId");
        let supplyToActivateActivationDate = "";
        let supplyToActivateTerminationDate = "";
        let selectedCommodity = "";

        if (!helper.isNullOrEmpty(component.get("v.selectedToActivateSupplyStringified"))){
            let selectedToActivateSupply = JSON.parse(component.get("v.selectedToActivateSupplyStringified"));
            selectedCommodity = selectedToActivateSupply.RecordType.Name;
        }

        let productDetail = {Price:'', Name:''};
        let oliList = component.get("v.opportunityLineItems");
        if (!helper.isNullOrEmpty(oliList)) {
            productDetail.Price = oliList[0].TotalPrice;
            productDetail.Name = oliList[0].Product2.Name;
        }

        const currentFlow = component.get("v.currentFlow");
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "contractId": contractId,
                "currentFlow": currentFlow,
                "dossierId": dossierId,
                "assetId": assetId,
                "supplyToActivateActivationDate": supplyToActivateActivationDate,
                "supplyToActivateTerminationDate": supplyToActivateTerminationDate,
                "selectedCommodity": selectedCommodity,
                "stage": stage,
                "cancelReason": cancelReason,
                "detailsReason": detailsReason,
                "productName": productDetail.Name,
                "productPrice": productDetail.Price
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateSupply: function (component, supplyId, status, callback) {
        let self = this;
        self.showSpinner(component);
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateSupply')
            .setInput({
                "supplyId": supplyId,
                "status": status,
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (callback && typeof callback === "function") {
                    callback(response);
                }
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let contractAccountId = component.get("v.contractAccountId");
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, opportunityId, dossierId, templateId) {
        let self = this;
        const {isConsoleNavigation} = component.find("workspace");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_PhotovoltaicPanelsWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };

        self.redirect(component, pageReference, true);
    },
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        let pageReference;

        if (useBit2WinCart) {
            let osiList = component.get("v.opportunityServiceItems");
            let isGas = false;
            let isElectric = false;
            let isService = false;
            for (let osi of osiList) {
                if (osi.RecordType.DeveloperName === 'Gas') {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === 'Service') {
                    isService = true;
                } else if (osi.RecordType.DeveloperName === 'Electric') {
                    isElectric = true;
                }
            }
            var productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';
            }
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__currentFlow": component.get("v.currentFlow"),
                    "c__currentStepTitle": component.get("v.step"),
                    "c__templateId": component.get("v.templateId"),
                    "c__productType": productType
                }
            };
        } else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, pageReference, false);
    },
    redirectToDossier: function (component, helper) {
        helper.cleanPersistentAttributes();

        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'Dossier__c',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference, false);
    },
    redirectToOppty: function (component, helper) {
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference, false);
    },
    resetData: function (component, helper) {
        component.set("v.opportunityServiceItems", []);
        component.set("v.opportunityLineItems", []);
    },
    resetSelectedToTerminateSupplyData: function (component, helper) {
        component.set("v.opportunityServiceItems", []);
        component.set("v.opportunityLineItems", []);
    },
    checkTheProductName: function (component, helper, product) {
        // reset the previously chosen product data
        component.set("v.isProductBundle", false);
        component.set("v.isExtendedProduct", false);

        // check if the product is set and whether it can be a bundle or need additional information fields
        if (product === undefined){
            return;
        }
        const productName = product.Name;
        if (productName.length < 5){
            return;
        }

        // determine whether the product is a bundle or not
        const isBundleProduct = productName.includes('Bundle');
        component.set("v.isProductBundle", isBundleProduct);

        // determine whether the product can have additional information or not
        const extendedProduct = (productName.substr(productName.length - 4).toUpperCase() === "PLUS" || productName.substr(productName.length - 1) === "+");
        component.set("v.isExtendedProduct", extendedProduct);
    },
    validateOsi: function (component, osiId) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        var self = this;
        component.find('apexService').builder()
            .setMethod("checkOsi")
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;

                osiList.push(newOsi);
                if (osiList.length === 1) {
                    self.updateCompanyDivisionOnOpportunity(component);
                }

                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                component.set("v.opportunityServiceItemId", osiId);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    linkOliToOsi: function (component, oli, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod("linkOliToOsi")
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction()
    },
    createAsset: function (component, oppId) {
        let self = this;
        self.showSpinner(component);
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let assetId = component.get("v.assetId");

        component.find('apexService').builder()
            .setMethod("createAsset")
            .setInput({
                "opportunityId": oppId,
                "assetId": assetId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.assetId', response.assetId);
                console.log('assetId->' + response.assetId);
                helper.savePersistentAttribute(component, helper, "assetId", response.assetId);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction()
    },
    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference, overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 3) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    closeFocusedTab: function (component, event, helper) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const {getFocusedTabInfo, refreshTab} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
    updateCompanyDivisionOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCompanyDivisionInOpportunity")
            .setInput({
                opportunityId: opportunityId,
                companyDivisionId: component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateContractAndContractSignedDateOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateContractAndContractSignedDateOnOpportunity")
            .setInput({
                opportunityId: opportunityId,
                contractId: contractId,
                customerSignedDate: component.get("v.customerSignedDate"),
                contractType: component.get('v.contractType')
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createCase: function (component, supplyId) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let that = this;
        that.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("createCase")
            .setInput({
                supplyId: supplyId,
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (!response.error) {
                    component.set('v.caseRecord', response.caseRecord);
                }

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
                that.hideSpinner(component);
            })
            .executeAction();
    },
    checkSelectedSupply: function (component) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let that = this;
        that.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("checkSelectedSupply")
            .setInput({
                supplyId: component.get("v.selectedToActivateSupplyId"),
                opportunityId: component.get('v.opportunityId')
            })
            .setResolve(function (response) {
                that.hideSpinner(component);
                component.set("v.contractName", response.eneltel);
                component.set("v.showNewOsi", true);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
                that.hideSpinner(component);
            })
            .executeAction();
    },
    savePersistentAttribute: function (component, helper, name, value) {
        // This is a workaround for automatic cleaning of attributes on page refresh
        let attrName = "v." + name;

        if (helper.isNullOrEmpty(value)){
            return;
        }

        component.set(attrName, value);
        window.sessionStorage.setItem(name, JSON.stringify(value));
    },
    loadPersistentAttribute: function (component, helper, name) {
        // This is a workaround for automatic cleaning of attributes on page refresh
        let returnValue = "";
        let attrName = "v." + name;

        // Give priority to session storage values when loading
        let valueInSessionStorage = window.sessionStorage.getItem(name);
        if (!helper.isNullOrEmpty(valueInSessionStorage)){
            returnValue = JSON.parse(valueInSessionStorage);
            component.set(attrName, returnValue);
            return returnValue;
        }

        let valueInComponentStorage = component.get(attrName);
        if (!helper.isNullOrEmpty(valueInComponentStorage)) {
            returnValue = valueInComponentStorage;
            return returnValue;
        }

        return returnValue;
    },
    cleanPersistentAttributes: function (){
        let n = window.sessionStorage.length;
        while(n--) {
            let key = window.sessionStorage.key(n);
            window.sessionStorage.removeItem(key);
        }
    },
    restoreLocalState: function (component, helper){
        helper.loadPersistentAttribute(component, helper, "assetId");
    }
})