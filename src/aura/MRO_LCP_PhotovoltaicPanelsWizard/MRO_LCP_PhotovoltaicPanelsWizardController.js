({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;

        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        component.set("v.dossierId", dossierId);

        helper.initialize(component, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.PhotovoltaicPanels"));
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
    },
    saveDraft: function (component, event, helper) {
        component.set("v.savingWizard", false);
        helper.saveOpportunity(component, helper, "Negotiation/Review", helper.redirectToDossier);
        helper.createPrivacyChangeRecord(component);
    },
    save: function (component, event, helper) {
        component.set("v.savingWizard", true);
        if (helper.isCancellation(component)){
            helper.saveOpportunity(component, helper, "Closed Won", helper.redirectToDossier);
        }else{
            helper.saveOpportunity(component, helper, "Closed Won", helper.redirectToDossier);
        }
    },
    onSaveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        if (cancelReason) {
            component.set("v.savingWizard", false);
            helper.saveOpportunity(component, helper, "Closed Lost", helper.redirectToOppty, cancelReason, detailsReason);
        }
    },
    nextStep: function (component, event, helper) {
        helper.goToNextStep(component, helper);
    },
    editStep: function (component, event, helper) {
        // this is the same as the step for this setting
        let stepToEditTitle = event.getSource().getLocalId();
        let previousStep = helper.getPreviousStep(component, helper, stepToEditTitle);

        component.set("v.step", stepToEditTitle);
        switch (stepToEditTitle) {
            case component.get("v.steps").SET_TYPE_OF_WORK.Title:
                break;
            case component.get("v.steps").FINISH.Title:
                break;
        }
    },
    handleTypeOfWorkSelection: function (component, event, helper) {
        let currentFlow = component.find('typeOfWork').get('v.value');
        helper.setCurrentFlow(component, helper, currentFlow);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let newOriginSelected = event.getParam('selectedOrigin');
        let newChannelSelected = event.getParam('selectedChannel');

        component.set('v.originSelected', newOriginSelected);
        component.set('v.channelSelected', newChannelSelected);

        if (newChannelSelected === null || newOriginSelected === null){
            return;
        }
        helper.setChannelAndOrigin(component);
        let channelStep = component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title;
        let currentStep = component.get("v.step");
        if (channelStep === currentStep) {
            component.set("v.step", helper.getFirstStep(component, helper).Title);
            helper.goToNextStep(component, helper);
        }
    },
    handleSupplyToActivateSelection: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedSupply = event.getParam("selectedObject");
        if (!selectedSupply) {
            ntfSvc.error(ntfLib, $A.get('$Label.c.SetSupplyErrorMsg'));
            return;
        }
        let selectedToActivateSupplyId = selectedSupply.Id;
        let companyDivisionId = selectedSupply.CompanyDivision__r.Id;
        component.set("v.selectedToActivateSupplyId", selectedToActivateSupplyId);

        let selectedToActivateSupplyStringified = JSON.stringify(selectedSupply);
        component.set("v.selectedToActivateSupplyStringified", selectedToActivateSupplyStringified);
        if (!helper.isNullOrEmpty(selectedSupply)){
            component.set("v.selectedCommodity", selectedSupply.RecordType.Name);
        }

        let setCompanyDivisionResult = helper.setCompanyDivisionFromSupply(component, helper, companyDivisionId);
        if (!setCompanyDivisionResult) {
            // The company division of the chosen supply isn't the same as of the current user
            // console.error('wrong company division');
            ntfSvc.error(ntfLib, $A.get('$Label.c.AllTheSuppliesShouldBeInSameCompany'));
            return;
        }

        if (helper.isCancellation(component)) {
            helper.createCase(component, selectedToActivateSupplyId);
        } else {
            helper.checkSelectedSupply(component);
        }
    },
    handleNewOsi: function (component, event, helper) {
        const osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
    },
    handleOsiDelete: function (component, event, helper) {
        let osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        let items = [];
        for (let i = 0; i < osiList.length; i++) {
            if (osiList[i].Id !== osiId) {
                items.push(osiList[i]);
            }
        }
        component.set("v.opportunityServiceItems", items);
        component.set(
            "v.osiTableView",
            items.length > component.get("v.tileNumber")
        );
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedPointId", "");
        component.set("v.searchedPointCode", "");
        //component.find("pointSelection").resetBox();
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(component, helper, "Proposal/Price Quote", helper.openProductSelection, "");
    },
    handleSavedSendingChannelSelected: function(component, event, helper) {
        console.log('sending channel is selected');
    },
    handleSavedSendingChannel: function(component, event, helper) {
        console.log('handleSavedSendingChannel');
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            if (event.getParam("divisionId")) {
                component.set("v.companyDivisionId", event.getParam("divisionId"));
                component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));
            }
        } catch (err) {
            console.error('Error on aura:PartnerServiceWizard :', err);
        }
    },
    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        let newContractType = event.getParam("newContractType");
        component.set("v.contractId", contractSelected);
        component.set("v.customerSignedDate", customerSignedDate);
        component.set("v.contractType", newContractType);
    },
    closeCancelReasonModal: function (component) {
        component.set("v.showCancelReasonModal", false);
    },
})