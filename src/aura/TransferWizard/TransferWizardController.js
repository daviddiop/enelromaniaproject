/**
 * Created by goudiaby on 28/05/2019.
 */
({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const opportunityId = myPageRef.state.c__opportunityId;
        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        helper.initConsoleNavigation(component, $A.get('$Label.c.Transfer'));
        helper.initialize(component);
    },
    getContractAccountRecordId: function (component, event, helper) {
        component.set(
            "v.contractAccountId",
            event.getParam("contractAccountRecordId")
        );
    },
    cancel: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty);
    },
    save: function (component, event, helper) {
        component.set("v.isSaving", true);
        helper.createPrivacyChangeRecord(component);
    },
    saveDraft: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.createPrivacyChangeRecord(component);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if (!component.get("v.opportunityServiceItems") || component.get("v.opportunityServiceItems").length === 0) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
                    return;
                }
                //check if supplies are in same compamy division
                let companyDivisionId = component.get("v.opportunityServiceItems")[0].ServicePoint__r.CurrentSupply__r.CompanyDivision__c;
                let mixedCompanyFound = false;
                component.get("v.opportunityServiceItems").forEach(osi => {
                    if (osi.ServicePoint__r.CurrentSupply__r.CompanyDivision__c !== companyDivisionId) {
                        mixedCompanyFound = true;
                        ntfSvc.error(ntfLib, $A.get('$Label.c.AllTheSuppliesShouldBeInSameCompany'));
                        return;
                    }
                });

                if (!mixedCompanyFound) {
                    component.set('v.companyDivisionId', companyDivisionId);
                    component.set("v.step", 2);
                    let contractAccountDivComponent = component.find("contractAccountSelectionComponent");
                    if (contractAccountDivComponent) {
                        contractAccountDivComponent.reload();
                    }
                }
                break;
            case 'confirmStep2':

                if ((!component.get("v.contractId")) && (component.get("v.customerSignedDate") === null)) {
                    ntfSvc.error(ntfLib,$A.get("$Label.c.CustomerSignedDateIsRequired"));
                    return;
                }

                if (component.get("v.isVisibleContractAddition")) {
                    component.set("v.step", 3);
                }

                helper.updateContractAndContractSignedDateOnOpportunity(component);
                break;
            case 'confirmStep3':
                if (!component.get("v.contractAccountId")) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.Required'));
                    return;
                }
                helper.updateOsi(component);
                component.set("v.step", 4);
                break;
            case 'confirmStep4':
                let prdList = component.get("v.opportunityLineItems");
                if (!prdList && prdList.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct"));
                    return;
                }
                const utilityPrds = prdList.filter(oli => oli.Product2.RecordType.DeveloperName === 'Utility');
                if (!utilityPrds || utilityPrds.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct"));
                    return;
                }
                let oli = utilityPrds[0];
                helper.linkOliToOsi(component, oli);
                component.set("v.step", 5);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                component.set("v.step", 4);
                break;
            default:
                break;
        }
    },
    handleSupplyResult: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let selectedSupplyId = event.getParam("selected")[0];
        var action = component.get("c.checkSelectedSypply");

        action.setParams({
            supplyId: selectedSupplyId,
            accountId: component.get('v.accountId'),
            opportunityId: component.get('v.opportunityId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if(!result.isValid) {
                    ntfSvc.error(ntfLib, result.message);
                    return;
                }
                component.set("v.searchedSupplyId", selectedSupplyId);
                component.set("v.showNewOsi", true);
            } else if (component.isValid() && state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                } else {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.UnexpectedError'));
                }
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));

        /*var contractAccountDivComponent = component.find("contractAccountSelectionComponent");
        if (contractAccountDivComponent) {
            contractAccountDivComponent.reload();
        }*/

        // Apply filter on Contract Addition
        helper.reloadContractAddition(component);

    },
    saveBillingSection: function (component, event, helper) {
        //const billingProfileId = component.get("v.billingProfileId");
        const contractAccountId = component.get("v.contractAccountId");
        helper.updateOsi(component);
        helper.saveOpportunity(component, "Value Proposition");
        helper.setPercentage(component, "Value Proposition");
    },
    saveOsiList: function (component, event, helper) {
        helper.saveOpportunity(component, "Qualification");
        helper.setPercentage(component, "Qualification");
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedSupplyId", "");
        //helper.resetSupplyForm(component);
    },
    handleNewOsi: function (component, event, helper) {
        let osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(component, "Proposal/Price Quote", helper.openProductSelection);
        helper.setPercentage(component, "Proposal/Price Quote");
    },
    handleOsiDelete: function (component, event, helper) {
        const osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        const items = [];
        for (let i = 0; i < osiList.length; i++) {
            if (osiList[i].Id !== osiId) {
                items.push(osiList[i]);
            }
        }

        component.set("v.opportunityServiceItems", items);

        if (items.length === 0) {
            component.set("v.companyDivisionId", "");
            component.set("v.step", 0);
            //helper.resetSupplyForm(component);
        }
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
        component.set("v.dontProcess", event.getParam('dontProcess'));
        if (!component.get("v.isSaving")) {
            helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToOppty);
        }
        if (component.get("v.isSaving")) {
            if (component.get("v.dontProcess")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
                $A.get('e.force:refreshView').fire();
                return;
            }
            helper.saveOpportunity(component, "Closed Won", helper.redirectToOppty);
        }
    },
    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        component.set("v.contractId", contractSelected);
        component.set("v.customerSignedDate", customerSignedDate);
    },
})