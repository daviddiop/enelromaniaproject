/**
 * Created by goudiaby on 28/05/2019.
 */
({
    initialize: function (component) {
        const self = this;
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const opportunityId = myPageRef.state.c__opportunityId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.opportunityId", '');
        component.set("v.contractId", '');
        component.set("v.accountId", accountId);
        component.set('v.interactionId', component.find('cookieSvc').getInteractionId());
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "interactionId": component.get('v.interactionId')
            }).setResolve(function (response) {
                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunity", response.opportunity);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.contractId", response.contractIdFromOpp);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.billingProfileId", response.billingProfileId);
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
                if (response.opportunityId && !opportunityId) {
                    self.updateUrl(component, accountId, response.opportunityId);
                }

                let step = 0;

                if (response.opportunityLineItems.length !== 0) {
                    step = 4;
                } else if (response.contractAccountId) {
                    step = 4;
                } else if (response.opportunityServiceItems.length !== 0) {
                    step = 2;
                }
                if (step >= 2) {
                    let path = 'ServicePoint__r.CurrentSupply__r.CompanyDivision__c'.split('.');
                    let companyDivision = path.reduce((obj, key) => ((obj && obj[key] !== 'undefined') ? obj[key] : undefined), response.opportunityServiceItems[0]);
                    component.set("v.companyDivisionId", companyDivision);
                }

                var opportunityVal = response.opportunity;
                if (opportunityVal) {
                    if (opportunityVal.StageName === 'Closed Lost' || opportunityVal.StageName === 'Closed Won') {
                        component.set("v.isClosed", true);
                    }
                }

                component.set("v.step", step);
                component.set("v.account", response.account);
                //self.resetSupplyForm(component);
                // component.set('v.disabledCompaniesDivision',response.user.CompanyDivisionEnforced__c);
                if (component.find("billingProfileSelection")) {
                    component.find("billingProfileSelection").reset(component.get("v.accountId"));
                }
                self.reloadContractAddition(component);
                //self.setPercentage(component, response.stage);
                self.hideSpinner(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    validateOsi: function (component, osiId) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('checkOsi')
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;
                osiList.push(newOsi);
                component.set("v.step", 1);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.showNewOsi", false);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                //self.resetSupplyForm(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    saveOpportunity: function (component, stage, callback) {
        const self = this;
        self.showSpinner(component);
        self.setPercentage(component, stage);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let companyDivision;
        if (component.get("v.opportunityServiceItems").length !== 0) {
            companyDivision = component.get("v.opportunityServiceItems")[0].ServicePoint__r.CurrentSupply__r.CompanyDivision__c;
            if (companyDivision) {
                let contractAccountDivComponent = component.find("contractAccountSelectionComponent");
                if (contractAccountDivComponent) {
                    contractAccountDivComponent.reload();
                }
            }
        }
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "stage": stage,
                "companyDivision": companyDivision
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        var self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        //let billingProfileId = component.get("v.billingProfileId");
        let contractAccountId = component.get("v.contractAccountId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
                //"billingProfileId": billingProfileId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, opportunityId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__TransferWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId
            }
        };
        navService.navigate(pageReference, true);
    },
    linkOliToOsi: function (component, oli, callback) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems");
        component.find('apexService').builder()
            .setMethod('linkOliToOsi')
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                } else {
                    //ntfSvc.success(ntfLib, '');
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    openProductSelection: function (component, helper) {
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__ProductSelectionWrp',
            },
            state: {
                "c__opportunityId": component.get("v.opportunityId")
            }
        };
        helper.redirect(component, pageReference);
    },
    redirectToOppty: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect_old: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response === true) {
                workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                    workspaceAPI.openSubtab({
                        pageReference: pageReference,
                        focus: true
                    }).then(function (subtabId) {
                        workspaceAPI.closeTab({
                            tabId: enclosingTabId
                        });
                    }).catch(function (errorMsg) {
                        ntfSvc.error(ntfLib, errorMsg);
                    });
                });
            } else {
                const navService = component.find("navService");
                navService.navigate(pageReference);
            }
        }).catch(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        });
    },
    setPercentage: function (component, stageName) {
        let perc = '';
        switch (stageName) {
            case 'Closed Won':
                perc = 100;
                break;
            case 'Closed Lost':
                perc = 100;
                break;
            case 'Negotiation/Review':
                perc = 90;
                break;
            case 'Proposal/Price Quote':
                perc = 75;
                break;
            case 'Value Proposition':
                perc = 50;
                break;
            case 'Qualification':
                perc = 20;
                break;
            case 'Prospecting':
                perc = 10;
                break;
            default:
                perc = 0;
        }
        component.set("v.percentage", perc);
    },
    goToAccount: function (component) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                        workspaceAPI.openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            workspaceAPI.closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                        /*if (window.localStorage && component.get("v.step") === 3 ) {
                            if (!window.localStorage['loaded']) {
                                window.localStorage['loaded'] = true;
                                self.refreshFocusedTab(component);
                            } else {
                                window.localStorage.removeItem('loaded');
                            }
                        }*/
                        if (!window.location.hash && component.get("v.step") === 3) {
                            window.location = window.location + '#loaded';
                            self.refreshFocusedTab(component);
                        }
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent instanceof Array) {
            let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
            privacyChangeComponentToObj[0].savePrivacyChange();
        } else {
            privacyChangeComponent.savePrivacyChange();
        }
    },
    resetSupplyForm: function (component) {
        // let supplySearchComponent = component.find("supplySelection");
        // if (supplySearchComponent instanceof Array) {
        //     let supplyComponentToObj = Object.assign({}, supplySearchComponent);
        //     supplyComponentToObj[0].resetForm();
        // } else {
        //     supplySearchComponent.resetForm();
        // }
    },
    resetBillingProfile: function (component) {
        let billingProfileComponent = component.find("billingProfileSelection");
        if (billingProfileComponent instanceof Array) {
            let billingProfileComponentToObj = Object.assign({}, billingProfileComponent);
            billingProfileComponentToObj[0].reset(component.get("v.accountId"));
        } else {
            billingProfileComponent.reset(component.get("v.accountId"));
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    updateContractAndContractSignedDateOnOpportunity: function (component) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
         let opportunityId = component.get("v.opportunityId");
         let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
         let customerSignedDate = component.get("v.customerSignedDate");
         component
            .find("apexService")
            .builder()
            .setMethod("updateContractAndContractSignedDateOnOpportunity")
            .setInput({
                opportunityId: opportunityId,
                contractId: contractId,
                customerSignedDate: customerSignedDate
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
     }
})