/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 23.04.20.
 */
({
    initialize: function (component) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let parentCaseId = myPageRef.state.c__caseId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);

        if(!parentCaseId){
            self.hideSpinner(component);
            ntfSvc.error(ntfLib, 'No parent Case found');
            component.set('v.isClosed', true);
        }

        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "parentCaseId": parentCaseId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            })
            .setResolve(function (response) {
                if(response.isNotPerson){
                    component.set("v.isClosed", true);
                    component.set('v.showAlert', true);
                    self.hideSpinner(component);
                    return;
                }

                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId, parentCaseId);
                    return;
                }
                component.set("v.isClosed", response.isClosed);
                component.set("v.parentSupplyId", response.parentSupplyId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.caseRecord", response.caseRecord);
                component.set("v.parentCaseRecord", response.parentCaseRecord);
                component.set("v.billingProfileId", response.billingProfileId);
                component.set("v.hasJustifyingDocuments", response.dossier.HasJustifyingDocuments__c);

                if(response.caseRecord){
                    component.set("v.distributorResponseDate", response.distributorResponseDate);
                    component.set('v.caseFormValid', response.distributorResponseDate);
                    let evidenceDate = response.caseRecord.EvidenceDocumentsReceivedDate__c;
                    let distributorDate = response.caseRecord.DistributorResponseDate__c;
                    self.toggleHasJustifyingDocumentsField(component, evidenceDate, distributorDate);
                }

                self.setStep(component, response);
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    toggleHasJustifyingDocumentsField: function(component, evidenceDate, distributorDate){
        if(!evidenceDate || !distributorDate){
            return;
        }
        evidenceDate = new Date(evidenceDate);
        distributorDate = new Date(distributorDate);
        let diff = (evidenceDate.getTime() - distributorDate.getTime()) / (1000 * 60 * 60 * 24);
        component.set('v.showHasJustifyingDocumentsField', diff >= 60 && diff <= 90);
    },

    setChannelAndOrigin: function (component, helper) {
        component.find('apexService').builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {
                if(component.get('v.step') === 0) {
                    helper.changeStep(component, helper, 1);
                }
            })
            .setReject(function (error) {
            })
            .executeAction();
    },

    upsertCase: function (component, supplyIds, callback) {
        let that = this;
        that.showSpinner(component);
        let input = {
            dossierId: component.get("v.dossierId"),
            billingProfileId: component.get("v.billingProfileId"),
            originSelected: component.get("v.originSelected"),
            channelSelected: component.get("v.channelSelected"),
        };

        if(supplyIds){
            input.supplyId = supplyIds[0];
        }

        let caseRecord = component.get('v.caseRecord');
        if(caseRecord){
            input.caseId = caseRecord.Id;
        }

        component.find('apexService').builder()
            .setMethod("upsertCase")
            .setInput(input)
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (!response.error) {
                    component.set('v.caseRecord', response.caseRecord);
                    if(callback){
                        callback();
                    }
                }
            })
            .setReject(function (error) {
                that.hideSpinner(component);
            })
            .executeAction();

    },

    updateHasJustifyingDocuments: function (component, hasJustifyingDocuments) {
        component.find('apexService').builder()
            .setMethod("updateHasJustifyingDocuments")
            .setInput({
                dossierId: component.get("v.dossierId"),
                hasJustifyingDocuments: hasJustifyingDocuments
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {

            })
            .executeAction();
    },

    redirectToDossier: function (component) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        this.redirect(component, pageReference);
    },

    performSaveProcess: function (component, helper, isDraft) {
        helper.showSpinner(component);
        let dossierId = component.get("v.dossierId");

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseRecord = component.get('v.caseRecord');
        let caseId = '';
        let caseSupply = '';

        if (caseRecord) {
            caseId = caseRecord.Id;
            if (caseRecord.Supply__c) {
                caseSupply = caseRecord.Supply__c;
            }
        }

        component.find('apexService').builder()
            .setMethod("saveProcess")
            .setInput({
                dossierId: dossierId,
                caseId: caseId,
                isDraft: isDraft,
                caseSupply: caseSupply
            })
            .setResolve(function (response) {
                if (response.error) {
                    helper.hideSpinner(component);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                helper.redirectToDossier(component);
            })
            .setReject(function (response) {
                helper.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },

    handleSave: function (component, helper) {
        helper.performSaveProcess(component, helper, false);
    },

    handleSaveDraft: function (component, helper) {
        helper.performSaveProcess(component, helper, true);
    },

    setStep: function (component, response) {
        let step = 5;

        if (!response.dossier.SendingChannel__c) {
            step = 4;
        }

        if (!response.billingProfileId) {
            step = 3;
        }

        if (!response.caseRecord || !response.caseRecord.DistributorResponseDate__c) {
            step = 2;
        }

        if (!response.dossier || !response.dossier.Channel__c || !response.dossier.Origin__c || !response.caseRecord || !response.caseRecord.Supply__c) {
            step = 1;
        }
        this.changeStep(component, this, step);
    },
    updateUrl: function (component, accountId, dossierId, parentCaseId) {
        let self = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_CompensationForDamagesWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId,
                "c__caseId": parentCaseId,
            }
        };
        self.redirect(component, pageReference, true);
    },

    changeStep: function (component, helper, stepNumber, isNext) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set('v.disableOriginChannel', stepNumber > 1);

        if (stepNumber === 3 && isNext) {
            helper.showSpinner(component);
            component.find('caseAmountForm').submit();
            return;
        }

        if (stepNumber === 4 && isNext) {
            let refundMethodIsInvoice = component.get("v.refundMethodIsInvoice")
            if (refundMethodIsInvoice){
                ntfSvc.error(ntfLib, 'The refund method “Invoice” is not available for this process.');
                return;
            }
            helper.showSpinner(component);
            helper.upsertCase(component, null, function(){
                helper.hideSpinner(component);
                helper.changeStep(component, helper, 4);
            });
            return;
        }

        if (stepNumber === 5 && isNext) {
            helper.showSpinner(component);
            component.find('sendingChannelSelection').saveSendingChannel();
            return;
        }

        component.set("v.step", stepNumber);
    },

    refundMethodCheck: function (component, helper, billingProfileRecordId) {
        component.find('apexService').builder()
            .setMethod("refundMethodCheck")
            .setInput({
                billingProfileId: billingProfileRecordId
            })
            .setResolve(function (response) {
                component.set('v.refundMethodIsInvoice', response.isInvoice);
            })
            .setReject(function (error) {

            })
            .executeAction();
    },

    checkEvidenceDocumentsReceivedDateFieldValidity: function(component, evidenceDocumentsReceivedDate) {
        let today = new Date();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (evidenceDocumentsReceivedDate > today) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.EvidenceDocumentsReceivedDateCannotBeInFuture"));
            return false
        } else {
            return true
        }
    },

    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference, overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },

    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
})