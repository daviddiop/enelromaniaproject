/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 23.04.20.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        helper.initialize(component);
        helper.initConsoleNavigation(component, $A.get("$Label.c.CompensationDamages"));
    },

    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            helper.setChannelAndOrigin(component, helper);
        }
    },

    handleSupplyResult: function (component, event, helper) {
        const supplyIds = event.getParam('selected');
        helper.upsertCase(component, supplyIds);
    },

    handleDeleteCaseTile: function (component, event, helper) {
        component.set('v.caseRecord', null);
    },

    handleCaseFieldsChange: function(component, event, helper){
        let caseDistributorResponseDateField = component.find('caseDistributorResponseDateField').get('v.value');
        let localId = event.getSource().getLocalId();
        component.set('v.caseFormValid', caseDistributorResponseDateField);

        if(localId === 'caseEvidenceDocumentsReceivedDateField' || localId === 'caseDistributorResponseDateField') {
            if (localId === 'caseEvidenceDocumentsReceivedDateField') {
                let caseEvidenceDocumentsReceivedDate = new Date(component.find('caseEvidenceDocumentsReceivedDateField').get('v.value'));
                let isEvidenceDocumentsReceivedDateFieldValid = helper.checkEvidenceDocumentsReceivedDateFieldValidity(component, caseEvidenceDocumentsReceivedDate);

                if (!isEvidenceDocumentsReceivedDateFieldValid) {
                    component.find('caseEvidenceDocumentsReceivedDateField').set('v.value', "");
                }
            }
            helper.toggleHasJustifyingDocumentsField(component,
                component.find('caseEvidenceDocumentsReceivedDateField').get('v.value'),
                component.find('caseDistributorResponseDateField').get('v.value'));
        }
    },

    handleCaseFormSuccess: function(component, event, helper){
        helper.hideSpinner(component);
        if(component.get('v.showHasJustifyingDocumentsField')) {
            let hasJustifyingDocuments = component.find('hasJustifyingDocumentsField').get('v.checked');
            component.set("v.hasJustifyingDocuments", hasJustifyingDocuments);
            helper.updateHasJustifyingDocuments(component, hasJustifyingDocuments);
        }
        helper.changeStep(component, helper, 3);
    },

    handleCaseFormError: function(component, event, helper){
        helper.hideSpinner(component);
        console.log('######## Case form error');
    },

    handleBillingProfileSelection: function(component, event, helper){
        let billingProfileRecordId = event.getParam('billingProfileRecordId');
        component.set('v.billingProfileId', billingProfileRecordId);
        helper.refundMethodCheck(component, helper, billingProfileRecordId);
    },

    handleSendingChannelSelection: function(component, event, helper){
        helper.hideSpinner(component);
        helper.changeStep(component, helper, 5);
    },

    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('confirmStep', '')) + 1;
        helper.changeStep(component, helper, stepNumber, true);
    },

    editStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('editStep', ''));
        helper.changeStep(component, helper, stepNumber);
    },

    cancel: function (component, event, helper) {
        component.set('v.showCancelBox', true);
    },
    onCloseCancelBox: function(component){
        component.set('v.showCancelBox', false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component);
    },

    save: function (component, event, helper) {
        helper.handleSave(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.handleSaveDraft(component, helper);
    },
    closeAlertBox: function (component, event, helper) {
        component.set('v.showAlert', false);
    },
})