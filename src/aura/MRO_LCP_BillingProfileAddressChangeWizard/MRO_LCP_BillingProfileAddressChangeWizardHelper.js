/**
 * Created by bouba on 03/10/2019.
 */
({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const gDPRParentRequestId = myPageRef.state.c__gDPRParentDossierId;
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod("init")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "companyDivisionId": '',
                "genericRequestId": genericRequestId,
                "gDPRParentRequestId": gDPRParentRequestId
            }).setResolve(function (response) {

                if (!response.error) {
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    component.set('v.originSelected', response.dossier.Origin__c);
                    component.set('v.channelSelected', response.dossier.Channel__c);
                    component.set("v.billingProfileAddressChangeRecordType", response.billingProfileAddressChangeRecordType);
                    component.set("v.templateId", response.templateId);

                    if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                    }
                    const dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }

                    let step = -1;
                    if(dossierVal.Origin__c && dossierVal.Channel__c){
                        step = 0;
                    }
                    if (response.caseTile && response.caseTile.length !== 0) {
                        step = 2;
                    }

                    component.set("v.step", step);
                    if (response.caseTile) {
                        component.set("v.tableView", response.caseTile.length > component.get("v.tileNumber"));
                    }
                } else {
                    console.log(response.errorMsg, response.errorTrace);
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
                self.hideSpinner(component);
            }).setReject(function (error) {

                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveChain: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");

        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            }).setResolve(function (response) {

                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {

                    console.log(response.errorMsg, response.errorTrace);
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
                self.hideSpinner(component);
            })
            .setReject(function (error) {

                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");

        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            }).setResolve(function (response) {

                if (!response.error) {

                    self.redirectToDossier(component, helper);
                } else {

                    console.log(response.errorMsg, response.errorTrace);
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
                self.hideSpinner(component);
            })

            .setReject(function (error) {

                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId,templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_BillingProfileAddressChangeWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 0);
            component.set("v.disableOriginChannel", false);
        }
        component.set("v.caseTile", items);
        component.set("v.tableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    saveDraft: function (component, event, helper) {

        const self = this;
        const dossierId = component.get("v.dossierId");
        const origin = component.get("v.originSelected");
        const channel = component.get("v.channelSelected");

        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("UpdateDossier")
            .setInput({
                dossierId: dossierId,
                origin: origin,
                channel: channel
            }).setResolve(function (response) {
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {

                     console.log(response.errorMsg, response.errorTrace);
                     ntfSvc.error(ntfLib, response.errorMsg);
                 }
            })
            .setReject(function (error) {

                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    updateDossier : function(component,event, helper){
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");
        const origin = component.get("v.originSelected");
        const channel = component.get("v.channelSelected");

        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("updateDossier")
            .setInput({
                dossierId: dossierId,
                origin: origin,
                channel: channel
            }).setResolve(function (response) {
                //if (callback) {
            if (!response.error) {
                self.initialize(component, event, helper)
                component.set("v.disableOriginChannel", true);
                //callback();
            }
            }).setReject(function () {

                console.log(error);
                ntfSvc.error(ntfLib, error);
                self.hideSpinner(component);
            })
            .executeAction();
    },

    retrieveSupply : function(component, event, helper){
        var contractAccountId = component.get("v.contractAccountId");
        component.find('apexService').builder()
            .setMethod("getSupply")
            .setInput({
                "contractAccountId": contractAccountId
            }).setResolve(function (response) {
                if (!response.error) {
                    let supplyObj = JSON.parse(JSON.stringify(response.supplyId));      //START alessio.murru@webresults.it -- 25/11/2020 -- ENLCRO-1971
                    component.set("v.supplyId", supplyObj.Id);
                } else {
                    console.log(response.errorMsg, response.errorTrace);
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            }).setReject(function (error) {

                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    cloneBillingProfile: function(component, event, helper){
        let caseId = event.getParam('newCaseId');

        component.find('apexService').builder()
            .setMethod('cloneBillingProfileForCase')
            .setInput({
                "caseId":caseId,
            }).setResolve(function(response){
                if (!response.error) {
                    console.log('Case\'s billing profiles have been cloned');
                } else {
                    console.log(response.errorMsg, response.errorTrace);
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            }).setReject(function(error){
                console.log(error);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    }
});