({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;

        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);
        helper.initialize(component, event, helper);
    },
    nextStep: function (component, event, helper) {
        let buttonPressed = event.getSource().getLocalId();

        if (buttonPressed === 'confirmStep0') {
            component.set("v.step", 1);
        } else if (buttonPressed === 'confirmStep1') {
            component.set("v.step", 2);
            component.set("v.showNewCase", true);
        } else if (buttonPressed === 'confirmStep2') {
            component.set("v.step", 3);
        } else if (buttonPressed === 'confirmStep3') {
            component.find('sendingChannelSelection').saveSendingChannel();
            component.set("v.step", 4);
        }
    },
    editStep: function (component, event) {
        const buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep0") {
            component.set("v.step", 0);
        } else if (buttonPressed === "returnStep1") {
            component.set("v.step", 1);
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 2);
        } else if (buttonPressed === "returnStep3") {
            component.set("v.step", 3);
        }
    },
    //START alessio.murru@webresults.it -- 25/11/2020 -- ENLCRO-1827
    getContactAccountRecordId: function (component, event, helper) {
        component.set("v.contractAccountId", event.getParam("contractAccountRecordId"));
        component.set("v.billingProfileId", event.getParam("selectedContractAccount").BillingProfile__c);
        helper.retrieveSupply(component, event, helper);
    },
    //END alessio.murru@webresults.it -- 25/11/2020 -- ENLCRO-1827
    handleOriginChannelSelection: function (component, event) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step", 0);
        } else {
            component.set("v.step", -1);
        }
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleSuccessCase: function (component, event, helper) {
        helper.cloneBillingProfile(component, event, helper);
        helper.updateDossier(component,event, helper );
        /*function (component, )
        {
            helper.initialize(component, event, helper);
        })z;*/
        //component.set("v.disableOriginChannel", true);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        if ((caseList.length === 0)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.saveChain(component, helper);
    },
    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraft(component, event, helper);
    }
});