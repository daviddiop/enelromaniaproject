({
    init: function (component, event, helper) {
        const eventType = event.getType();
        console.log('event type', eventType);
        const pageRef = component.get("v.pageReference");
        const recordId = component.get("v.recordId");
        console.log('pageReference', JSON.parse(JSON.stringify(pageRef)));
        console.log('recordId', recordId);

        if (pageRef) {
            let recordIdParam = pageRef.state.c__recordId;
            component.set("v.recordId", recordIdParam);
            if (eventType === 'aura:valueChange') {
                let listComponent = component.find("list");
                if (listComponent) {
                    listComponent.loadValidatableDocumentsData();
                }
            }
        }
        else if (recordId) {
            component.set("v.recordId", recordId);
        }
    }
});