/**
 * Created by Boubacar Sow on 31/10/2019.
 */

({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;

        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);

        helper.setSubTypeSelectionOptions(component);
        helper.setPeriodSelectionDate(component);
        helper.initialize(component, event, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected){
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step",1);
            let dossier = component.get('v.dossier');
            if (dossier && dossier.Origin__c) {
                component.set("v.step", 1);
            }
        }else {
            component.set("v.step",0);
        }
    },
    searchSelectionHandler: function (component, event, helper) {
        const supplyIds = event.getParam('selected');
        if (supplyIds){
            helper.searchSupplySelected(component,supplyIds);
        }

    },
    onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            console.log(' response tab Info ', JSON.stringify(response));
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: $A.get("$Label.c.MeterCheck")
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: $A.get("$Label.c.MeterCheck")
                                });
                            }
                        });
                }
            });
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
        if (!component.get("v.findButtonEvt")) {
            component.set("v.step", 0);
        }
    },

    removeContracthandler: function (component, event, helper) {
        component.set('v.supplyContractAccountId', null);
        component.set('v.disableOriginChannel',false);
    },

    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },

    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let subType = component.get("v.subType");

        switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            case 'confirmStep2':
                let contractAccount = component.get("v.supplyContractAccountId");
                console.log('### contractAccount '+contractAccount);
                if (!contractAccount){
                    component.set("v.step", 2);
                }else{
                    component.set("v.step", 3);
                }
                break;
            case 'confirmStep3':
                console.log('### subType in step '+subType);
                if (!subType){
                    component.set("v.step", 3);
                }else {
                    if (subType === 'SealUnseal'){
                        component.set("v.step", 6);
                        return;
                    }
                    component.set("v.step", 4);
                }
                break;
            case 'confirmStep4':
                component.find('periodSelection').onConfirmSelection();
                let startDate = component.get("v.startDate");
                console.log('### startDate in Step '+startDate);
                let endDate = component.get("v.endDate");
                console.log('### endDate in Step '+endDate);
                if (!startDate || !endDate){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.StartDateEndDateRange"));
                    component.set("v.step", 4);
                } else {
                    component.set("v.step", 5);
                }
                break;
            case 'confirmStep5':
                component.find('invoiceInquiry').getSelectedInvoices();
                let invoiceIds = component.get("v.invoiceIds");
                if (!invoiceIds) {
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.UnmarkedInvoice"));
                }
                component.set("v.step", 6);
                break;
            case 'confirmStep6':
                let notes = component.get("v.discoNotes");
                if (!notes) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.FillDiscoNotes"));
                    return;
                }
                component.find('sendingChannel').disableInputField(false);
                component.set("v.step", 7);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                component.set("v.step", 4);
                break;
            case 'returnStep5':
                component.set("v.step", 5);
                break;
            case 'returnStep6':
                component.set("v.step", 6);
                component.find('sendingChannel').disableInputField(true);
                break;
            default:
                break;
        }
    },

    subTypeSelectionHandler: function (component, event, helper) {
        let subType = event.getParam("value");
        console.log('#### subType '+subType);
        if (subType){
            component.set("v.subType", subType);
        }
    },

    handleInvoiceSelect: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let invoiceList = event.getParam("selectedInvoiceList");
        let invoiceIds = [];
        Object.keys(invoiceList).forEach(function (key) {
            invoiceIds.push(invoiceList[key].invoiceSerialNumber + invoiceList[key].invoiceNumber);
        });

        console.log("###invoiceIds "+invoiceIds);
        if(invoiceIds.length !== 0){
            ntfSvc.success(ntfLib, $A.get("$Label.c.SelectedSucess"));
            component.set("v.invoiceIds", invoiceIds);
        }else {
            ntfSvc.warn(ntfLib, $A.get("$Label.c.UnmarkedInvoice"));
        }
    },


    handleSuccessDossier: function (component,event, helper){
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        const phone = event.getParam("phone");
        console.log('### selectedChannel '+selectedChannel);
        helper.createCase(component, helper);
    },


    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },

    handleSuccessCase: function (component, event, helper) {
        helper.initialize(component, event, helper);
        helper.resetSupplyForm(component, event, helper);
    },

    save: function (component, event, helper) {
        helper.showSpinner(component);
        component.find('sendingChannel').saveSendingChannel();
    },

    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },

    saveDraft: function (component, event, helper) {
        helper.saveDraftMeterCheck(component, event, helper);
    },

    selectedRangeHandler: function (component, event, helper) {
        let startDate = event.getParam('startDate');
        console.log('### startDate '+startDate);
        let endDate = event.getParam('endDate');
        console.log('### endDate '+endDate);
        if (startDate){
            component.set("v.startDate", startDate);
        }
        if (endDate){
            component.set("v.endDate", endDate);
        }
    },

    Validatefields:function (component, event, helper) {
        const validatefields = event.getParam("areFieldsValidated");
        if (validatefields ===false){
            helper.hideSpinner(component);
        }
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.hasBlockingErrors', true);
    }
});