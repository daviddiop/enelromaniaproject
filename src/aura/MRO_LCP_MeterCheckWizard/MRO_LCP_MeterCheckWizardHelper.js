/**
 * Created by Boubacar Sow on 31/10/2019.
 */

({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const genericRequestId = myPageRef.state.c__genericRequestId;

        component.set("v.accountId", accountId);
        /*if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }*/
        component.find('apexService').builder()
            .setMethod("InitializeMeterCheck")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId,
                "companyDivisionId": ''
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                component.set("v.caseTile", response.caseTile);
                component.set("v.accountId", response.accountId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.templateId", response.templateId);
                component.set("v.meterCheckRtEle", response.meterCheckRTEle);
                component.set("v.meterCheckRtGas", response.meterCheckRTGas);

                console.log('###response.genericRequestId '+response.genericRequestId)
                if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId) && (response.templateId && !templateId)) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                }
                if (response.companyDivisionName) {
                    component.set("v.companyDivisionName", response.companyDivisionName);
                    component.set("v.companyDivisionId", response.companyDivisionId);
                }
                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                    }
                }

                let step = 0;
                let origin  = component.get('v.originSelected');
                let channel  = component.get('v.channelSelected');
                if(origin && channel){
                    step = 1;
                }
                if (response.caseTile && response.caseTile.length !== 0) {
                    step = 1;
                    component.set('v.disableOriginChannel',true);
                }
                component.set("v.step", step);
                if(response.caseTile){
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                }
            }
        })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createCase: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        let invoiceIds = component.get("v.invoiceIds");
        const dossierId = component.get("v.dossierId");
        const accountId = component.get("v.accountId");
        const contractAccountId = component.get("v.supplyContractAccountId");
        const contractId = component.get("v.supplyContractId");
        const supply = component.get("v.searchedSupply");
        const origin = component.get("v.originSelected");
        const channel = component.get("v.channelSelected");
        const notes = component.get("v.discoNotes");
        const subType  = component.get("v.subType");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (!invoiceIds){
            invoiceIds = [];
        }
        console.log(JSON.stringify(invoiceIds))

        component.find('apexService').builder()
            .setMethod("CreateCase")
            .setInput({
                invoiceIds: JSON.stringify(invoiceIds).replace(/[\[\]"'"]+/g, '' ), //JSON.stringify(invoiceIds),
                dossierId: dossierId,
                accountId: accountId,
                contractAccountId: contractAccountId,
                contractId: contractId,
                origin: origin,
                channel: channel,
                notes: notes,
                subType: subType,
                supply: JSON.stringify(supply)
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
            else{
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_MeterCheckWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 0);
        }
        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    searchSupplySelected: function (component,supplyIds) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("getSupplyRecord")
            .setInput({
                supplyId: supplyIds[0]
            })
            .setResolve(function (response)
            {
                if (!response.error) {
                    let supplySelected;
                    if(response.supply){
                        supplySelected = response.supply;
                        component.set("v.searchedSupply", response.supply);
                        component.set("v.supplyId", response.supply.Id);
                        if (supplySelected.ServicePoint__r){
                            let servicePoint = supplySelected.ServicePoint__r;
                            console.log('### servicePoint '+servicePoint);
                            if (servicePoint && (supplySelected.Id !== servicePoint.CurrentSupply__c)) {
                                ntfSvc.error(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + supplySelected.Name);
                                component.set("v.showNewCase", false);
                                let supplyContractAccountId;
                                component.set("v.supplyContractAccountId",supplyContractAccountId);
                                component.set("v.step", 2);
                                return;
                            }

                        }

                        if (supplySelected.ContractAccount__c) {
                            component.set("v.supplyContractAccountId", supplySelected.ContractAccount__c);
                            component.set('v.billingAccountNumber', supplySelected.ContractAccount__r.IntegrationKey__c);
                            component.set('v.disableOriginChannel',true);
                        } else {
                            ntfSvc.error(ntfLib, $A.get("$Label.c.MissingContractAccount"));
                            component.set("v.supplyContractAccountId",null);
                            component.set("v.step", 2);
                            return;
                        }

                        if (supplySelected.Contract__r){
                            let contract = supplySelected.Contract__r;
                            console.log('### contract '+JSON.stringify(contract));
                            component.set("v.supplyContractId", contract.Id);
                            if(!contract.StartDate){
                                ntfSvc.error(ntfLib, $A.get("$Label.c.MissingStartDate"));
                                component.set("v.supplyContractAccountId",null);
                                component.set("v.step", 2);
                                return;
                            }
                            let contractStartDate = new Date(contract.StartDate);
                            let minDate = contractStartDate.getFullYear()+ "-" +(contractStartDate.getMonth() + 1) + "-" + contractStartDate.getDate();
                            console.log('### minDate '+minDate);
                            component.set("v.minDate", minDate);
                            let startDate = new Date(component.get("v.startDate"));
                            if(startDate < contractStartDate){
                                ntfSvc.error(ntfLib, $A.get("$Label.c.StartDateContractStartDate"));
                                component.set("v.supplyContractAccountId",null);
                                component.set("v.step", 2);
                                return;
                            }
                        } else {
                            ntfSvc.error(ntfLib, $A.get("$Label.c.MissingContract"));
                            component.set("v.supplyContractAccountId",null);
                            component.set("v.step", 2);
                            return;
                        }

                        if (supplySelected.RecordType["DeveloperName"] === "Electric") {
                            component.set("v.recordTypeMeterCheck", component.get("v.meterCheckRtEle"));
                            component.set("v.isElectricalRequest ", true);
                        } else if (supplySelected.RecordType["DeveloperName"] === "Gas") {
                            component.set("v.recordTypeMeterCheck", component.get("v.meterCheckRtGas"));
                            component.set("v.isElectricalRequest ", false);
                            component.set("v.subType", "MeterCheck");
                        }

                    }
                }
            })
            .setReject(function (error) {
            })
            .executeAction();

    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        component.find('cookieSvc').clearInteractionId();
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    saveDraftMeterCheck: function (component, event, helper) {
        const self = this;
        self.redirectToDossier(component, helper);
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },

    setPeriodSelectionDate : function (component) {
        const dateOfToday = new Date();
        let max = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        max.setMonth(dateOfToday.getMonth() - 3);
        //console.log('#### max '+ max);
        let maxDate = max.getFullYear()+ "-" +(max.getMonth() + 1) + "-" + max.getDate();
        //console.log('### maxDate '+maxDate);
        //console.log('### today '+today);
        component.set("v.startDate",maxDate);
        component.set("v.maxDate",maxDate);
        component.set("v.endDate",today);
    },

    setSubTypeSelectionOptions : function (component) {
        let subTypeOptions = [
            {value: 'MeterCheck', label: 'Meter Check'},
            {value: 'SealUnseal', label: 'Seal / Unseal'}
        ];
        component.set("v.subtypeOptions", subTypeOptions);
    }
});