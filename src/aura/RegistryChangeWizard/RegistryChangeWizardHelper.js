({
    initialize: function (component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var recordId = myPageRef.state.c__recordId;
        var addrForm1 = component.find('address');

        component.set('v.recordId', recordId);
        component.find('apexService').builder()
            .setMethod("getOptionList")
            .setInput({
                "accountId": recordId,
                "companyDivisionId": component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                component.set("v.personRecordTypeId", response.personRecordType.value);
                component.set("v.businessRecordTypeId", response.businessRecordType.value);

                helper.retrieveAccount(component, component.get('v.recordId'));
                helper.getCaseList(component, component.get('v.recordId'));
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    getAccountFields: function (component, newSlowcaseFields, fields) {
        var fastFields = {
            PersonMobilePhone: "AccountPhone__c",
            PersonEmail: "AccountEmail__c",
            Phone: "AccountPhone__c",
            Email__c: "AccountEmail__c"
        };
        var slowFields = {
            FirstName: "AccountFirstName__c",
            LastName: "AccountLastName__c",
            Name: "AccountName__c"
        };

        var slowcaseFieldsTemp = {};
        var fastcaseFields = {};
        var fastcaseFieldsTemp = {};
        var fieldChange = false;
        var account = component.get('v.account');
        for (var f in fields) {
            for (var cle in fastFields) {
                if (f.toString() == cle) {
                    if (account[f] != fields[f]) {
                        fastcaseFields[fastFields[cle]] = fields[f];
                        component.set('v.newFastcase', fastcaseFields);
                    } else {
                        fastcaseFieldsTemp[fastFields[cle]] = fields[f];
                        component.set('v.newFastcaseTemp', fastcaseFieldsTemp);
                    }
                }
            }
            for (var cle in slowFields) {
                if (f.toString() === cle) {
                    if (account[f] != fields[f]) {
                        if (f.toString() === 'FirstName' || f.toString() === 'LastName') {
                            fieldChange = true;
                        }
                        newSlowcaseFields[slowFields[cle]] = fields[f];
                        component.set('v.newSlowcase', newSlowcaseFields);
                    } else {
                        slowcaseFieldsTemp[slowFields[cle]] = fields[f];
                        component.set('v.newSlowcaseTemp', slowcaseFieldsTemp);
                    }
                }
            }
        }
        if (fieldChange) {
            this.getContactListRelated(component);
        } else {
            this.createNewRegistryChange(component);
        }
    },
    retrieveAccount: function (component, accountId) {
        component.find('apexService').builder()
            .setMethod("retrieveAccount").setInput(accountId)
            .setResolve(function (response) {
                if (response.IsPersonAccount === true) {
                    component.set('v.recordTypeId', component.get("v.personRecordTypeId"));
                } else {
                    component.set('v.recordTypeId', component.get("v.businessRecordTypeId"));
                }
                component.set('v.account', response);
                let address = {};
                address.streetNumber = response.ResidentialStreetNumber__c;
                address.streetName = response.ResidentialStreetName__c;
                address.streetType = response.ResidentialStreetType__c;
                address.apartment = response.ResidentialApartment__c;
                address.building = response.ResidentialBuilding__c;
                address.city = response.ResidentialCity__c;
                address.country = response.ResidentialCountry__c;
                address.floor = response.ResidentialFloor__c;
                address.locality = response.ResidentialLocality__c;
                address.postalCode = response.ResidentialPostalCode__c;
                address.province = response.ResidentialProvince__c;
                address.streetNumbersExtn = response.ResidentialStreetNumberExtn__c;
                component.set('v.address', address);
                component.set('v.addressForcedResidential',response.ResidentialAddressNormalized__c);
                if(response.ResidentialAddressNormalized__c){
                    component.set('v.readOnlySlow', true);
                }
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    getCaseList: function (component, accountId) {
        component.find('apexService').builder()
            .setMethod("getCaseList")
            .setInput(accountId)
            .setResolve(function (response) {
                if (response.fastCases.length !== 0) {
                    component.set('v.readOnlyFast', true);
                }
                if (response.slowCases.length !== 0) {
                    component.set('v.readOnlySlow', true);
                    component.set('v.readOnlySlowAccount', true);
                    component.set('v.isNewRecord', true);
                }
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    getContactListRelated: function (component) {
        component.find('apexService').builder()
        .setMethod("getContactList")
        .setInput(component.get('v.account'))
        .setResolve(function (response) {
            if (response.length !== 0) {
                component.set('v.isOpenModal', true);
                component.set('v.accountContactRelationList', response);
            } else {
                this.createNewRegistryChange(component);
            }
        })
        .setReject(function (error) {
            var ntfSvc = component.find('ntfSvc');
            var ntfLib = component.find('ntfLib');
            ntfSvc.error(ntfLib, error);
        })
        .executeAction();
    },
    createNewRegistryChange: function (component) {
        const self = this;
        const ntfSvc = component.find('ntfSvc');
        const ntfLib = component.find('ntfLib');
        let inputData = component.get('v.inputData');
        inputData.accountId = component.get('v.account').Id;
        inputData.companyDivisionId = component.get('v.companyDivisionId');
        inputData.residentialAddressForced = !component.get('v.addressForcedResidential');

        if (component.get('v.disableSave') === false) {
            if ((component.get('v.newSlowcase') == null) && (component.get('v.newFastcase') == null)) {
                ntfSvc.warn(ntfLib, $A.get("$Label.c.NoChanges"));
                return;
            }
        } else {
            let addrForm = component.find('address');
            if (!addrForm.isValid()) {
                ntfSvc.error(ntfLib, 'Address must be verified');
                return;
            }
        }

        if (component.get('v.newFastcase') != null) {
            let newFastCase = component.get('v.newFastcase');
            let newFastCaseTemp = component.get('v.newFastcaseTemp');
            for (let cle in newFastCase) {
                for (let field in newFastcaseTemp) {
                    if (field !== cle) {
                        newFastCase[field] = newFastCaseTemp[field];
                    }
                }
            }
            inputData.fastcase = component.get('v.newFastcase');
        }
        if (component.get('v.newSlowcase') != null) {
            let newSlowCase = component.get('v.newSlowcase');
            let newSlowCaseTemp = component.get('v.newSlowcaseTemp');
            for (let cle in newSlowCase) {
                for (let field in newSlowCaseTemp) {
                    if (field !== cle) {
                        newSlowCase[field] = newSlowCaseTemp[field];
                    }
                }
            }
            inputData.slowcase = component.get('v.newSlowcase');
        }
        component.find('apexService').builder()
            .setMethod("saveNewRegistry")
            .setInput(inputData)
            .setResolve(function (response) {
                if (response.error !== true) {
                    let dossierId = response.dossierId;
                    ntfSvc.success(ntfLib, $A.get("$Label.c.Success"));
                    let pageReference = {
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: dossierId,
                            objectApiName: 'Dossier__c',
                            actionName: 'view'
                        }
                    };
                    self.redirect(component, pageReference);
                }
            })
            .setReject(function (error) {
                var ntfSvc = component.find('ntfSvc');
                var ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    goToAccount: function (component, event, helper) {
        let accountId = component.get("v.recordId");
        let navService = component.find("navService");

        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    redirect: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                        workspaceAPI.openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            workspaceAPI.closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                workspaceAPI.setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                workspaceAPI.setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    }
})