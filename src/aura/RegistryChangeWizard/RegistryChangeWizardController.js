({
    doInit: function(component, event, helper) {
        helper.showSpinner(component);
        helper.initialize(component, event, helper);
        helper.initConsoleNavigation(component, $A.get('$Label.c.RegistryChange'));
    },
    onPageReferenceChanged: function() {
        $A.get('e.force:refreshView').fire();
    },
    registryChange: function(component, event, helper) {
        event.preventDefault();
        let fields = event.getParam("fields");
        let account = component.get('v.account');
        let addrForm = component.find('address').getValues();

        let newSlowCaseFields = {};
        for (let f in addrForm) {
            if (addrForm.hasOwnProperty(f)) {
                if (addrForm[f] !== '') {
                    let addrField = f.toString().replace("Residential", "Address");
                    if (account[f] !== addrForm[f]) {
                        newSlowCaseFields[addrField] = addrForm[f];
                        component.set('v.newSlowcase', newSlowCaseFields);
                    } else {
                        newSlowCaseFields[addrField] = addrForm[f];
                    }
                }
            }
        }
        helper.getAccountFields(component, newSlowCaseFields, fields);
    },

    getToggleButtonValue: function(component, event, helper) {
        let checkCmp = component.find("tglBtn").get("v.checked");
        component.set('v.chkBoxValue', checkCmp);
    },

    updateContact: function(component, event, helper) {
        let ntfSvc = component.find('ntfSvc');
        let ntfLib = component.find('ntfLib');
        component.set('v.isOpenModal', false);
        if (component.get('v.chkBoxValue')) {
            helper.createNewRegistryChange(component);
        } else {
            ntfSvc.warn(ntfLib, $A.get("$Label.c.UserInterruption"));
            return false;
        }
    },

    cancel: function(component, event, helper) {
        component.set('v.isOpenModalCancel', true);
    },

    continueModif: function(component, event, helper) {
        component.set('v.isOpenModalCancel', false);
    },

    cancelModif: function(component, event, helper) {
        component.set('v.isOpenModalCancel', false);
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get('v.account').Id,
                objectApiName: 'Account',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        if(event.getParam("divisionId")) {
            component.set("v.companyDivisionId", event.getParam("divisionId"));
        }
    },
    disabledSaveButton: function(component, event) {
        component.set('v.disableSave', event.getParam('disabledSave'));
    },
    updateAddressNormalized: function(component , event){
        component.set('v.addressNormalized', event.getParam('addressNormalized'));
        component.set('v.addressForcedResidential', event.getParam('addressForced'));
    },
})