/**
 * Created by BADJI on 12/11/2019.
 */
({
    initialize: function (component, event, helper) {
        const self = this;
        //self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const companyDivComponent = component.find("companyDivision");
        component.set("v.accountId", accountId);
        component.set("v.showCancelReasonModal", false);
       // component.set("v.companyDivisionId",null);
        component.set("v.companyDivisionId", '');
        if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }
        component.find('apexService').builder()
            .setMethod("Initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "templateId": templateId,
                "genericRequestId": genericRequestId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                let step = 0;
                if (response.originSelected && response.originSelected) {
                    component.set('v.disableOriginChannel', true);
                    step = 1;
                } else {
                    component.set('v.disableOriginChannel', false);
                }
                component.set("v.originSelected", response.originSelected);
                component.set("v.channelSelected", response.channelSelected);
                component.set("v.accountId", response.accountId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.templateId", response.templateId);
                component.set("v.companyDivisionId", response.companyDivisionId);
                //component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.accountSituation", response.accountSituation);
                //component.set("v.step",0);

                /*if ((response.dossierId && response.dossierId !== myPageRef.state.c__dossierId) ) {
                    self.updateUrl(component, accountId, response.dossierId, response.templateId);
                }*/
                if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                }

            if (companyDivComponent) {
                //companyDivComponent.wizardCompanyDivisionId = response.opportunityCompanyDivisionId;
                companyDivComponent.setCompanyDivision();
                //self.reloadCompanyDivision(component);
            }
                /*if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId);
                }*/
                 if (response.companyDivisionId) {
                    component.set("v.companyDivisionId", response.companyDivisionId);
                    component.set("v.companyDivisionCode", response.companyDivisionCode);
                }
                /*if (response.companyDivisionId) {
                    step = 3;
                }*/
                console.log('step: ' + step);
                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                        component.set('v.disableOriginChannel', true);
                        component.set("v.minDate", response.caseData[0].StartDate__c);
                        component.set("v.endDate", response.caseData[0].EndDate__c);
                        component.set('v.selectedFisaIds', response.caseData[0].MarkedInvoices__c);
                        component.set('v.contractAccountId', response.caseData[0].ContractAccount__c);
                        let outCome = response.caseData[0].Outcome__c;
                        if (outCome === $A.get("$Label.c.CNPCUI_Level")){
                            component.set('v.isCUICNP', true);
                            component.set('v.processName', 'AccountSituation_CC');
                        }else {
                            component.set('v.isCUICNP', false);
                            component.set('v.processName', 'AccountSituation_' + (outCome === $A.get("$Label.c.AccountLevel") ? 'AC' : 'CA'));
                        }
                    }
                    if(!component.get('v.outCome')){
                        step = 1;
                    }
                }
                component.set("v.step", step);

                component.set("v.companyDivisionIds", response.companyDivisionIds);
                if (!response.companyDivisionIds || !response.companyDivisionIds.length) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.CustomerWithoutSupply"));
                }
            } else {


                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_AccountSituationWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        let accountId = component.get("v.accountId");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accountId);
            } else {
                contractAccountComponent.reset(accountId);
            }
        }
    },
    retrieveCompanyDivisionCode: function (component){
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let companyDivisionId = component.get("v.companyDivisionId");
        console.log("retrieveCompanyDivisionCode > companyDivisionId: "+companyDivisionId);
        component.find('apexService').builder()
            .setMethod('retrieveCompanyDivisionCode')
            .setInput({
                "companyDivisionId": companyDivisionId
            })
            .setResolve(function (response) {
                console.log('retrieveCompanyDivisionCode response: '+ response.isValid);
                console.log('response.companyDivisionCode: '+ response.companyDivisionCode);
                component.set("v.companyDivisionCode", response.companyDivisionCode);
                console.log('retrieveCompanyDivisionCode > companyDivisionCode: '+ component.get("v.companyDivisionCode"));
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    createCase: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        /*const selectedFisa = JSON.stringify(component.get('v.selectedFisaIds')).replace(/[\[\]"'"]+/g, '' );
        console.log('**** '+selectedFisa);*/
        const startDate = component.get('v.startDate');
        const endDate = component.get('v.endDate');
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const companyDivision = component.get("v.companyDivisionId");
        const contractAccountId = component.get("v.contractAccountId");
        const subProcessSelected = component.get("v.subProcessSelected");
        const selectedChannel = component.get("v.channelSelected");
        const selectedOrigin = component.get("v.originSelected");
        const outCome = component.get("v.outCome");
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("insertCase")
            .setInput({
                /*selectedFisa: selectedFisa,*/
                companyDivisionId: companyDivision,
                subProcessSelected: subProcessSelected,
                accountId: accountId,
                dossierId: dossierId,
                recordTypeId: component.get("v.accountSituation"),
                startDate: JSON.stringify(startDate),
                contractAccountId: contractAccountId ? contractAccountId : null,
                endDate: JSON.stringify(endDate),
                selectedChannel:selectedChannel,
                selectedOrigin:selectedOrigin,
                outCome:outCome
            }).setResolve(function (response) {
            if (!response.error) {
                self.redirectToDossier(component, helper);
                self.hideSpinner(component);
            } else {
                ntfSvc.error(ntfLib, response.errorMsg);
                self.hideSpinner(component);
            }
        })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                console.log(JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    redirectToDossier: function (component) {
       const self = this;
       const pageReference = {
          type: 'standard__recordPage',
          attributes: {
              recordId: component.get("v.dossierId"),
              objectApiName: 'c__Dossier',
              actionName: 'view'
            }
       };
       self.redirect(component, pageReference);
    },
    initConsoleNavigation: function (component, wizardLabel) {
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = "Lightning Experience | Salesforce";
        }
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    handleCancel: function (component, helper) {
        const self = this;
        const dossierId = component.get("v.dossierId");
        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId
            }).setResolve(function (response) {
            if (!response.error) {
                 self.redirectToDossier(component, helper);
            }
            })
            .setReject(function (error) {
            })
            .executeAction();
    },
    setPeriodSelectionDate : function (component) {
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        let startDate = new Date(1900, 1, 1);
        let firstDay = startDate.getFullYear() + "-" + startDate.getMonth() + "-" + startDate.getDate();
        component.set("v.endDate", today);
        component.set("v.minDate", today);
        component.set("v.startDate", firstDay);
        component.set("v.defaultDate", firstDay);
    },
    handleDateCheck : function(component){
        let startDate =  new Date(component.get('v.startDate'));
        let defaultDate =  new Date(component.get('v.defaultDate'));
        return defaultDate <= startDate;
    },
    updateOriginAndChannelOnDossier: function (component, event, helper,origin,channel) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("updateOriginAndChannelOnDossier")
            .setInput({
                dossierId: dossierId,
                channelSelected: channel,
                originSelected: origin
            }).setResolve(function (response) {
            if (!response.error) {
                //self.initialize(component, event, helper);
            }
        })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
    updateDossierCompanyDivision: function (component) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component);
        let dossierId = component.get('v.dossierId');
        let companyDivisionId = component.get('v.companyDivisionId');
        component.find('apexService').builder()
            .setMethod("updateDossierCompanyDivision")
            .setInput({
                dossierId: dossierId,
                companyDivisionId: companyDivisionId,
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                //self.initialize(component, event, helper);
            }
        })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction()
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    }
});