/**
 * Created by BADJI on 12/11/2019.
 */
({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const templateId = myPageRef.state.c__templateId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);
        component.set("v.dossierId", dossierId);
        helper.setPeriodSelectionDate(component, event, helper);
        helper.initialize(component, event, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.AccountSituation"));
    },
    getFisaSelect: function (component, event) {
        let selectedFisaIds = event.getParam("fisaIds");
        component.set('v.selectedFisaIds', selectedFisaIds);
        if (selectedFisaIds) {
            component.set("v.step", 7);
        }

    },
    checkFisaList: function (component, event) {
        let isEmptyFisaList = event.getParam("isEmptyFisaList");
        component.set('v.isEmptyFisaList', isEmptyFisaList);
        if (isEmptyFisaList) {
            component.set("v.step", 6);
        } else {
            component.set("v.step", 7);
        }
    },

    getContractAccountRecordId: function (component, event) {
        let selectedContractAccount = event.getParam("selectedContractAccount");
        /* console.log('selectedContractAccount: '+selectedContractAccount);*/
        if (selectedContractAccount) {
            component.set("v.contractAccountId", selectedContractAccount.Id);
            component.set("v.billingAccountNumber", selectedContractAccount.BillingAccountNumber__c);
            //component.set("v.companyDivisionCode", selectedContractAccount.CompanyDivision__r.Code__c);
            /*console.log('getContractAccountRecordId > v.contractAccountId: '+component.get("v.contractAccountId"));
            console.log('getContractAccountRecordId > v.billingAccountNumber: '+component.get("v.billingAccountNumber"));
            console.log('getContractAccountRecordId > v.companyDivisionCode: '+component.get("v.companyDivisionCode"));*/
            /*component.set("v.companyDivisionId", selectedContractAccount.CompanyDivision__c);*/
        }
    },

    handleCUICNP: function (component, event, helper) {
        component.set("v.isCUICNP", component.find("inputToggle").get("v.checked"));
        console.log('#### isCUICNP '+component.get('v.isCUICNP'));
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let wizardHeader = component.find("wizardHeader");
        switch (buttonPressed) {
            case 'confirmStep1':
                if(wizardHeader){
                    component.set('v.disableOriginChannel',true);
                    component.set("v.step", 2);
                }
                helper.updateOriginAndChannelOnDossier(component, event, helper, component.get("v.originSelected"), component.get("v.channelSelected"));
                break;
            case 'confirmStep2':
                /*component.set("v.step", 3);
                if(component.get("v.companyDivisionId")) {
                    helper.updateDossierCompanyDivision(component);
                }*/
                let outCome = component.get('v.outCome');
                if (!outCome){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.OutComeMissing"));
                    return;
                }
                component.set("v.step", 3);
                break;
            case 'confirmStep3':
                let hideContractAcountSelection =  component.get('v.hideContractAcountSelection');
                /*component.set("v.step", 4);
                console.log("v.contractAccountId: "+component.get("v.contractAccountId"));
                //if(component.get("v.contractAccountId") == ""){
                    helper.retrieveCompanyDivisionCode(component);
                //}*/

                if (hideContractAcountSelection){
                    component.set("v.step", 5);

                }else {
                    component.set("v.step", 4);
                }
                if(component.get("v.companyDivisionId")) {
                    helper.updateDossierCompanyDivision(component);
                }

                break;
            case 'confirmStep4':
                 /*component.find("periodSelection").onConfirmSelection();
                if (!component.find("periodSelection").handleValidDate()) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RangeNotValid"));
                    component.set("v.step", 4);
                } else {
                    if(!helper.handleDateCheck(component,helper)){
                        ntfSvc.error(ntfLib, $A.get("$Label.c.RangeNotValid"));
                        component.set("v.step", 4);
                    }else {
                        component.set("v.step", 5);
                    }
                }*/
                let contractAccountId = component.get('v.contractAccountId');
                if(contractAccountId){
                    component.set("v.step", 5);
                }
                else{
                    component.set("v.step", 4);
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountMissing"));
                }

                console.log("v.contractAccountId: "+component.get("v.contractAccountId"));
                //if(component.get("v.contractAccountId") == ""){
                    helper.retrieveCompanyDivisionCode(component);
                //}

                break;
            case 'confirmStep5':
                /*let subProcessSelected = component.get("v.subProcessSelected");
                if (!subProcessSelected) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SubProcessMissing"));
                    return;
                }
                component.set("v.step", 6);*/
                    component.find("periodSelection").onConfirmSelection();
                   if (!component.find("periodSelection").handleValidDate()) {
                       ntfSvc.error(ntfLib, $A.get("$Label.c.RangeNotValid"));
                       component.set("v.step", 5);
                   } else {
                       if(!helper.handleDateCheck(component,helper)){
                           ntfSvc.error(ntfLib, $A.get("$Label.c.RangeNotValid"));
                           component.set("v.step", 5);
                       }else {
                           component.set("v.step", 6);
                       }
                   }
                break;
            case 'confirmStep6':
                let subProcessSelected = component.get("v.subProcessSelected");
                if (!subProcessSelected) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SubProcessMissing"));
                    return;
                }
                component.set("v.step", 7);
                break;
            default:
                break;
        }
    },
    editStep: function (component, event) {
        const buttonPressed = event.getSource().getLocalId();
        let wizardHeader = component.find("wizardHeader");
        if(buttonPressed === "returnStep1"){
            if(wizardHeader){
                component.set('v.disableOriginChannel',false);
                component.set("v.step", 1);
            }
            //component.set('v.disableOriginChannel',true);
            //component.set("v.step", 1);
        } else if (buttonPressed === "returnStep2") {
            /*if(component.get("v.contractAccountId")){
                component.set("v.contractAccountId", "");
            }
            if(component.get("v.companyDivisionCode")){
                component.set("v.companyDivisionCode", "");
            }*/
            component.set("v.step", 2);
        } else if (buttonPressed === "returnStep3") {
            if(component.get("v.contractAccountId")){
                component.set("v.contractAccountId", "");
            }
            if(component.get("v.companyDivisionCode")){
                component.set("v.companyDivisionCode", "");
            }
            component.set("v.step", 3);
        } else if (buttonPressed === "returnStep4") {
            component.set("v.step", 4);
        } else if (buttonPressed === "returnStep5") {
            component.set("v.step", 5);
        }else if (buttonPressed === "returnStep6") {
            component.set("v.step", 6);
        }
    },
    save: function (component, event, helper) {
        helper.createCase(component, helper);
    },
    cancel: function (component, event, helper) {
        component.set("v.showCancelReasonModal", true);
        //helper.handleCancel(component, helper);
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            if (event.getParam("divisionId")) {
                let selectedCompanyDivision = event.getParam("divisionId");
                let availableCompanyDivisionIds = component.get("v.companyDivisionIds");
                if (!availableCompanyDivisionIds || availableCompanyDivisionIds.indexOf(selectedCompanyDivision) === -1) {
                    const ntfLib = component.find('notifLib');
                    const ntfSvc = component.find('notify');
                    ntfSvc.error(ntfLib, $A.get("$Label.c.CompanyDivisionWrong"));
                    component.set("v.companyDivisionId", null);
                    return;
                }
                component.set("v.companyDivisionId", selectedCompanyDivision);
                /*console.log("handleCompanyDivisionChange > v.companyDivisionId: "+component.get("v.companyDivisionId"));*/
                /*component.set("v.companyDivisionCode", selectedCompanyDivision.Code__c);
                console.log("handleCompanyDivisionChange > v.companyDivisionCode: "+component.get("v.companyDivisionCode"));*/
                //component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));
                /*if ((component.get("v.step") === 2) && component.get("v.isCompanyDivisionEnforced")) {//0
                    component.set("v.step", 3);//1
                }*/

                // Apply filter on Contract Account
                if (component.find("contractAccountSelectionComponent")) {
                    helper.reloadContractAccount(component);
                }
            }
        } catch (err) {
            console.error('Error on aura: MRO_LCP_AccountSituationWizard :', err);
        }
    },

    handleSubProcessChange: function (component, event, helper) {
        let subProcessSelected = event.getParam('subProcessSelected');
        component.set("v.subProcessSelected", subProcessSelected);

        console.log('### subProcessSelected ' + component.get("v.subProcessSelected"));
    },

    handleOutComeChange: function (component, event, helper) {
        let outComeSelected = event.getParam('outComeSelected');
        component.set("v.outCome", outComeSelected);

        console.log('### outComeSelected ' + component.get("v.outCome"));
        if (outComeSelected === $A.get("$Label.c.AccountLevel") || outComeSelected === $A.get("$Label.c.CNPCUI_Level")) {
            component.set('v.hideContractAcountSelection', true);
            component.set('v.contractAccountId',null);
            component.set('v.processName', 'AccountSituation_' + (outComeSelected === $A.get("$Label.c.AccountLevel") ? 'AC' : 'CC'));
        }
        if (outComeSelected === $A.get("$Label.c.ContractAccountLevel")){
            component.set('v.hideContractAcountSelection', false);
            component.set('v.processName', 'AccountSituation_CA');
        }
    },

    handleSelectedRange: function (component, event) {
        component.set("v.startDate", event.getParam("startDate"));
        component.set("v.endDate", event.getParam("endDate"));
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected){
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            //component.set('v.disableAgree', false);
            component.set("v.step",1);
            if(component.get("v.companyDivisionId") && component.get('v.outCome')){
                component.set("v.step",3);
            }
        }else {
            //component.set('v.disableAgree', true);
            component.set("v.step",0);
        }
    },
    closeCancelReasonModal: function (component) {
        component.set("v.showCancelReasonModal", false);
    },
    saveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
        //helper.handleCancel(component, helper);
    }
});