/**
 * Created by goudiaby on 01/08/2019.
 */

({
    init: function (component, event, helper) {
        const pageRef = component.get("v.pageReference");
        const recordId = component.get("v.recordId");

        if (recordId) {
            component.set("v.recordId", recordId);
        } else {
            let recordIdParam = pageRef.state.c__recordId;
            component.set("v.recordId", recordIdParam);
        }
    }
});