({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;

        helper.initConsoleNavigation(component,$A.get('$Label.c.DecoReco'));
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);
        helper.initialize(component, event, helper);
    },
    searchSelectionHandler: function (component, event, helper){
        const supplyIds = event.getParam('selected');
        if (!supplyIds || !supplyIds.length) {
            component.set("v.showNewCase", false);
            return;
        }
        helper.getSupplyRecord(component, supplyIds);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleCreateCase: function (component, event, helper) {
        event.preventDefault();
            helper.showSpinner(component,'spinnerSectionModal');
            helper.createCase(component, event, helper);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (caseList.length === 0) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.updateCase(component, helper);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        //const ntfLib = component.find('notifLib');
        //const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                component.set("v.disableOriginChannel", true);
                helper.setChannelAndOrigin(component);
                break;
            case 'confirmStep2':
                //helper.updateCommodityToDossier(component);
                component.set("v.step", 3);
            default:
                break;
        }
    },

    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                component.set('v.disableOriginChannel',false);
                component.set('v.disableWizardHeader',true);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            default:
                break;
        }
    },

    openCancelModal: function( component, event,helper){
        let caseTile = component.get('v.caseTile');
        if(caseTile.length > 0)
            component.set('v.showDeleteModal', true);
        else {
            component.set('v.showDeleteModal', false);
            component.set("v.step", 2);
        }
    },
    cancelCaseTile: function (component,event,helper){
            const self = this;
            const caseList = component.get("v.caseTile");
            const ntfLib = component.find('notifLib');
            const ntfSvc = component.find('notify');

            component.find('api').builder()
                .setMethod("deleteCases")
                .setInput({
                    'oldCaseList': caseList,
                    'dossierId': ''
                })
                .setResolve(function (response) {
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    let emptyCaseTile = [];
                    component.set('v.caseTile', emptyCaseTile);
                    component.set('v.showDeleteModal', false);
                    component.set("v.step", 2);
                })
                .setReject(function (response) {
                    const errors = response.getError();
                    ntfSvc.error(ntfLib, errors[0].message);
                })
                .executeAction();
       /* var lwcElm = component.find('mroCaseTileLwcId');
        var caseTileList = [];
        caseTileList = component.get('v.caseTile');
        for(let i = 0; i < caseTileList.length; i++){
            lwcElm.caseId = caseTileList[i].Id;
            lwcElm.deleteCaseFromTile();
        }*/

    },
    closeCancelModal: function (component,event,helper){
        component.set('v.showDeleteModal', false);
    },

    checkModalFields: function (component, event, helper) {

    },

    cancel: function (component) {
        component.find('cancelReasonSelection').open();
    },

    saveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");

        helper.handleCancel(component, helper, cancelReason, detailsReason);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftBillingProfile(component, helper);
    },

    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            console.log('### Origine '+component.get('v.originSelected'));
            console.log('### Channel '+component.get('v.channelSelected'));
            component.set("v.step", 1);
            let dossier = component.get('v.dossier');
            if(dossier && dossier.Origin__c){
                component.set("v.step", 1);
            }

        } else {
            component.set("v.step", 0);
        }
    },

    decoRecoFieldsCheck : function(component, event, helper){
        let startDateValue = event.getParam('startDateValue') ? event.getParam('startDateValue') : '';
        let noteValue = event.getParam('noteValue') ? event.getParam('noteValue') : '';
        let reqTypeValue = event.getParam('requestTypeValue');
        let reqTypeLabel = reqTypeValue === 'DECO_C' ? 'Disconnection' : reqTypeValue === 'RECO_C' ? 'Reconnection' : '';
        let supplyStatus = reqTypeValue === 'DECO_C' ? ['Active'] : ['Active', 'Terminating'];
        let durationValue = reqTypeValue === 'DECO_C' ? event.getParam('durationValue') : '';
        let selectedCaseFields = reqTypeValue === 'DECO_C' ? ['StartDate__c','CustomerNotes__c','Duration__c'] : ['StartDate__c','CustomerNotes__c'] ;
        let connectionStatus = reqTypeValue === 'DECO_C' ? 'Connected' : 'Disconnected';
        let suspended = reqTypeValue === 'DECO_C' ? []: ['Customer Request','Late Payment'];
        let connectionStatusCondition = "ConnectionStatus__c = '"+connectionStatus+"'";
        let andConditions = component.get("v.andConditions");
        andConditions.splice(0, andConditions.length);
        andConditions.push(connectionStatusCondition);
        if(reqTypeValue === 'RECO_C'){
            let suspendedCondition = " Suspended__c IN ('Customer Request','Late Payment') " ;
            andConditions.push(suspendedCondition);
        }

        component.set('v.isDecoRecoDisabled', event.getParam('decoRecoDisabledValue'));
        component.set('v.selectCaseTileFields', selectedCaseFields);
        component.set('v.selectCaseFields', selectedCaseFields);
        component.set('v.durationValue', durationValue);
        component.set('v.supplyStatus', supplyStatus);
        component.set('v.startDateValue', startDateValue);
        component.set('v.requestTypeLabel', reqTypeLabel);
        component.set('v.noteValue', noteValue);
        component.set('v.suspended', suspended);
        component.set('v.connectionStatus', connectionStatus);
        component.set("v.andConditions",andConditions);

    },

    changeFieldToShow: function(component, event,helper){
        let caseList = [];
        let fieldToAdd = event.getParam('fieldToAdd');
        caseList = component.get('v.selectCaseFields');
        caseList.push(fieldToAdd);
        component.set('v.selectCaseFields', caseList);
    }
});