({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const parentDecoSupplyId = myPageRef.state.c__parentDecoSupplyId;
        const parentDecoCaseId = myPageRef.state.c__parentDecoCaseId;
        const parentDecoCaseOrigin = myPageRef.state.c__parentDecoCaseOrigin;
        const parentDecoCaseChannel = myPageRef.state.c__parentDecoCaseChannel;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        //const companyDivisionId = component.get("v.companyDivisionId");

        component.find('api').builder()
            .setMethod("initialize")
            .setInput({
                'accountId': accountId,
                'dossierId': dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                'genericRequestId': genericRequestId,
                'parentDecoCaseId': parentDecoCaseId,
                'parentDecoCaseOrigin': parentDecoCaseOrigin,
                'parentDecoCaseChannel': parentDecoCaseChannel
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error){
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    component.set("v.templateId", response.templateId);
                    component.set('v.originSelected', response.dossier.Origin__c);
                    component.set('v.channelSelected', response.dossier.Channel__c);

                    if(response.account){
                        if(response.account.Phone || response.account.PersonMobilePhone){
                            ntfSvc.warn(ntfLib, $A.get("$Label.c.DecoRecoRequiredPhoneNumberWarning"));
                        }else{
                            ntfSvc.error(ntfLib, $A.get("$Label.c.DecoRecoRequiredPhoneNumberError"));
                            component.set('v.isClosed', true);
                            return;
                        }
                    }

                    if ((response.dossierId && !dossierId) && (response.templateId && !templateId)) {
                        if (parentDecoSupplyId && parentDecoCaseId) {
                            component.set('v.requestTypeLabel', 'Reconnection')
                            component.set("v.isCanceledDeco", true);

                            self.updateUrl(component, accountId, response.dossierId, response.templateId, parentDecoCaseId, parentDecoSupplyId);
                            self.createRecoFromDecoCase(component, event, helper, response.accountId, response.dossierId, 'Reconnection', parentDecoSupplyId, parentDecoCaseId);
                        } else {
                            self.updateUrl(component, accountId, response.dossierId, response.templateId );
                        }
                    }

                    if (response.companyDivisionId) {
                        //component.set("v.companyDivisionName", response.companyDivisionName);
                        component.set("v.companyDivisionId", response.companyDivisionId);
                    }

                    let step = component.get("v.step") === -1 ? 0 : component.get("v.step");

                    if(response.dossier.Origin__c && response.dossier.Channel__c)
                        step = 1;

                    if (response.caseTile && response.caseTile.length !== 0) {
                        step = 3;
                        component.set('v.disableOriginChannel',true);
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));

                        let reqType = response.caseTile[0].Type === 'Disconnection' ? 'DECO_C' : 'RECO_C';
                        component.set('v.requestTypeValue', reqType);
                        component.set('v.startDateValue', response.caseTile[0].StartDate__c);
                        component.set('v.noteValue', response.caseTile[0].CustomerNotes__c);
                        if(reqType === 'DECO_C'){
                            component.set('v.durationValue', response.caseTile[0].Duration__c);
                        }
                    }
                    const dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        //if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        if ( dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }

                    component.set("v.step", step);
                    component.set("v.account", response.account);
                    component.set("v.accountPersonRT", response.accountPersonRT);
                    component.set("v.accountPersonProspectRT", response.accountPersonProspectRT);
                    component.set("v.accountBusinessRT", response.accountBusinessRT);
                    component.set("v.accountBusinessProspectRT", response.accountBusinessProspectRT);
                    component.set("v.traderPicklistValues", response.traderPicklistValues);
                    component.set("v.traderLabel", response.traderLabel);
                    component.set("v.decoRecoRecordType", response.decoRecoRecordType);
                    component.set("v.commodityPicklistValues", response.commodityPicklistValues);
                    component.set("v.commodityLabel", response.commodityLabel);
                    component.set("v.commodity", response.commodity);
                    component.set("v.parentDecoSupplyId", parentDecoSupplyId);
                    component.set("v.parentDecoCaseId", parentDecoCaseId);
                } else {
                    //console.log('MRO_LCP_DecoRecoWizardHelper initialize ERROR ********** ' + JSON.stringify(response));
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    saveDraftBillingProfile: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("saveDraftBP")
            .setInput({
                'oldCaseList': caseList
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    createCase: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSectionModal');
        //const trader = component.get("v.trader");
        //const reason = component.get("v.reason");
       // const effectiveDate = component.get("v.effectiveDate");
        //const referenceDate = component.get("v.referenceDate");
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const startDateValue = component.get("v.startDateValue");
        const durationValue = component.get("v.durationValue") ? component.get('v.durationValue') : null;
        const noteValue = component.get("v.noteValue");
        const requestTypeLabel = component.get("v.requestTypeLabel");
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');


        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
      /*  if ((trader == null) || (effectiveDate == null) || (referenceDate == null) || (reason == null)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            self.hideSpinner(component, 'spinnerSectionModal');
            return;
        }*/

        if (caseList.legend !== 0 ){
          /* let casePivot = caseList[0];
            if (casePivot){
                if ((casePivot.EffectiveDate__c !== effectiveDate) || (casePivot.Reason__c !== reason) ||
                    (casePivot.ReferenceDate__c !== referenceDate) || (casePivot.Trader__c !== trader) ){
                    //ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    ntfSvc.error(ntfLib,  $A.get("$Label.c.CaseSwitchOutData"));
                    self.hideSpinner(component, 'spinnerSectionModal');

                    return;
                }
            }*/
        }

       /* if (reason === 'Trader change'){
            let numberOfDay = self.getDayDateDiff(component);
            console.log('### numberOfDay '+numberOfDay);
            if (!isNaN(numberOfDay) && numberOfDay < 21 ){
                ntfSvc.warn(ntfLib, $A.get("$Label.c.SwitchOutDate21"));
                self.hideSpinner(component, 'spinnerSectionModal');
                return;
            }
        }*/

        component.find('api').builder()
            .setMethod("createCase")
            .setInput({
                //'trader': trader,
                //'reason': reason,
                //'effectiveDate': effectiveDate,
                //'referenceDate': referenceDate,
                'searchedSupplyFieldsList': searchedSupplyFieldsList,
                'caseList': caseList,
                'accountId': accountId,
                'dossierId': dossierId,
                'channelSelected': channelSelected,
                'originSelected': originSelected,
                'startDateValue' : startDateValue,
                'durationValue' : durationValue,
                'noteValue' : noteValue,
                'requestTypeLabel' : requestTypeLabel
            })
            .setResolve(function (response) {
                /* added by Giuseppe Mario Pastore 30-03-2020*/
                component.set('v.recordTypeIdDecoReco', response.caseTile[0].recordTypeId);
                /* added by Giuseppe Mario Pastore 30-03-2020*/
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.showNewCase", false);
                    return;
                }
                else{

                    if (response.caseTile.length !== 0) {
                        component.set("v.step", 3);
                        component.set("v.companyDivisionId",response.caseTile[0].CompanyDivision__c);
                    }

                    component.set("v.commodity", response.caseTile[0].Supply__r.RecordType.DeveloperName);
                    console.log("v.commodity -------------> " + component.get("v.commodity"));
                    component.set("v.showNewCase", false);
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    self.updateDossier(component, event, helper);
                    helper.updateCommodityToDossier(component);
                }
            })
            .setReject(function (response) {
                console.log('JSON.stringify(response) reject ********> ' + JSON.stringify(response));
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    createRecoFromDecoCase: function (component, event, helper, accountId, dossierId, requestTypeLabel, parentDecoSupplyId, parentDecoCaseId) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let customerNotes = 'Correction on Disconnection case';

        self.showSpinner(component, 'spinnerSectionModal');

        component.find('api').builder()
            .setMethod("CreateRecoCase")
            .setInput({
                'accountId': accountId,
                'dossierId': dossierId,
                'requestTypeLabel' : requestTypeLabel,
                'supplyId': parentDecoSupplyId,
                'parentCaseId': parentDecoCaseId,
                'customerNotes': customerNotes
            })
            .setResolve(function (response) {

                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.showNewCase", false);
                    return;
                }
                else {
                    component.set('v.recordTypeIdDecoReco', response.caseTile[0].recordTypeId);
                    if (response.caseTile.length !== 0) {
                        component.set("v.step", 3);
                        component.set("v.companyDivisionId",response.caseTile[0].CompanyDivision__c);
                    }
                    component.set('v.requestTypeValue', 'RECO_C')
                    component.set('v.startDateValue', response.caseTile[0].StartDate__c);
                    component.set('v.noteValue', response.caseTile[0].CustomerNotes__c);
                    component.set("v.commodity", response.caseTile[0].Supply__r.RecordType.DeveloperName);
                    component.set("v.showNewCase", false);
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    component.set('v.originSelected', response.caseTile[0].Origin);
                    component.set('v.channelSelected', response.caseTile[0].Channel__c);

                    self.updateDossier(component, event, helper);
                    helper.updateCommodityToDossier(component);
                    component.find
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    updateCase: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        //add by david
        //const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        //
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("updateCaseList")
            .setInput({
                'oldCaseList': caseList,
                'dossierId': dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },

    handleCancel: function (component, helper, cancelReason, detailsReason) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        self.showSpinner(component);
        component.find('api').builder()
            .setMethod("CancelProcess")
            .setInput({
                dossierId: component.get("v.dossierId"),
                cancelReason: cancelReason,
                detailsReason: detailsReason
            }).setResolve(function (response) {
            self.hideSpinner(component);
            self.redirectToDossier(component, helper);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        })
            .executeAction();
    },

    getSupplyRecord: function (component, supplyIds){
        component.set('v.effectiveDate', '');
        component.set('v.referenceDate', '');
        component.set('v.trader', '');
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let errSupplyAlreadySelected =[];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];

        component.find('api').builder()
            .setMethod("getSupplyRecord")
            .setInput({
                supplyIds: supplyIds,
                subProcess: component.get('v.requestTypeLabel'),
                accountId: component.get('v.accountId'),
                caseList: JSON.stringify(caseList)
            })
            .setResolve(function (response)
            {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMessage);
                }else{
                    for (let i = 0; i < response.supplies.length; i++) {
                        let validSupply = true;
                        let oneSupply = response.supplies[i];
                        if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                            errCurrentSupplyIsNotValid.push(oneSupply.Name);
                            continue;
                        }
                        if (caseList) {
                            for (let j = 0; j < caseList.length; j++) {
                                if (oneSupply.Id === caseList[j].Supply__c) {
                                    errSupplyAlreadySelected.push(oneSupply.Name);
                                    validSupply = false;
                                    break;
                                }
                            }
                        }

                        if (validSupply){
                            validSupplies.push(oneSupply);
                        }
                    }
                    if (errSupplyAlreadySelected.length !== 0){
                        let messages = errSupplyAlreadySelected.join(' ');
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' +messages);
                        component.set("v.showNewCase", false);
                        return ;
                    }
                    if (errCurrentSupplyIsNotValid.length !== 0){
                        let messages = errCurrentSupplyIsNotValid.join(' ');
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' +messages);
                        component.set("v.showNewCase", false);
                        return ;
                    }
                    component.set("v.searchedSupplyFields", validSupplies);
                    component.set("v.showNewCase", true);
                }

            })
            .setReject(function (error) {
            })
            .executeAction();

    },

    updateUrl: function (component, accountId, dossierId, templateId, parentDecoSupplyId, parentDecoCaseId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_DecoRecoWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId,
                "c__templateId": templateId,
                'c__parentDecoSupplyId': parentDecoSupplyId,
                'c__parentDecoCaseId': parentDecoCaseId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 3);
            component.set('v.disableOriginChannel',false);
        }

        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    validateFields: function (component, event) {
       /* const labelFieldList = new Array();
        //let valEffectiveDate = component.find("Field_EffectiveDate__c");
        //let valReferenceDate = component.find("Field_ReferenceDate__c");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }

        if (!Array.isArray(valReferenceDate)) {
            valReferenceDate = [valReferenceDate];
        }
        tmp = valReferenceDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_ReferenceDate__c');
        }
        */

        /*let valTrader = component.find("Field_Trader__c");
        if (!Array.isArray(valTrader)) {
            valTrader = [valTrader];
        }
        tmp = valTrader[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_Trader__c');
        }*/
       /* if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }*/
        return true;
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        //let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                        /*if (window.localStorage && component.get("v.step") === 3 ) {
                            if (!window.localStorage['loaded']) {
                                window.localStorage['loaded'] = true;
                                self.refreshFocusedTab(component);
                            } else {
                                window.localStorage.removeItem('loaded');
                            }
                        }*/
                        /*if (!window.location.hash && component.get("v.step") === 3) {
                            window.location = window.location + '#loaded';
                            self.refreshFocusedTab(component);
                        }*/
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    /*    resetSupplyForm: function (component) {
            let supplySearchComponent = component.find("supplySelection");
            if (supplySearchComponent instanceof Array) {
                let supplyComponentToObj = Object.assign({}, supplySearchComponent);
                supplyComponentToObj[0].resetForm();
            } else {
                supplySearchComponent.resetForm();
            }
        },*/
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    },
    updateDossier: function (component, event, helper) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');

        component.find('api').builder()
            .setMethod("UpdateDossier")
            .setInput({
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected
            }).setResolve(function (response) {
            if (!response.error) {
                self.initialize(component, event, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    updateCommodityToDossier: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        component
            .find("api")
            .builder()
            .setMethod("updateCommodityToDossier")
            .setInput({
                dossierId: dossierId,
                commodity: component.get("v.commodity")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        //let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("api").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                //opportunityId: opportunity.Id,
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    }

});