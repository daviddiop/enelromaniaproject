/**
 * Created by goudiaby on 16/01/2020.
 */

({
    initialize: function (component, objectId) {
        const self = this;
        self.showSpinner(component);
        const ntfSvc = component.find('ntfSvc');
        const ntfLib = component.find('ntfLib');
        component.find('apexService').builder()
            .setMethod('CheckConditions')
            .setInput({
                "objectId": objectId
            })
            .setResolve(function (response) {
                setTimeout(() => self.hideSpinner(component), 1000);
                let {isCancellable, messages} = response;
                component.set("v.renderCancellationModal", isCancellable);
                if (!isCancellable){
                    component.set("v.errorMessages", messages);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            }).executeAction();
    },
    redirect: function (component, pageReference) {
        const ntfSvc = component.find('ntfSvc');
        const ntfLib = component.find('ntfLib');
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");

        isConsoleNavigation().then(function (response) {
            if (response === true) {
                getEnclosingTabId().then(function (enclosingTabId) {
                    openSubtab({
                        pageReference: pageReference,
                        focus: true
                    }).then(function () {
                        closeTab({
                            tabId: enclosingTabId
                        });
                    }).catch(function (error) {
                    });
                });
            } else {
                const navService = component.find("navService");
                navService.navigate(pageReference);
            }
        }).catch(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        });
    },
    showSpinner: function (component) {
        component.set("v.showSpinner", true);
    },
    hideSpinner: function (component) {
        component.set("v.showSpinner", false);
    }
});