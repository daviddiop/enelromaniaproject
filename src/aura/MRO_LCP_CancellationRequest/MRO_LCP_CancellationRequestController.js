/**
 * Created by goudiaby on 15/01/2020.
 */

({
    init: function (component,event,helper) {
        const pageRef = component.get("v.pageReference");
        const recordIdParam = pageRef.state.c__recordId;
        let objectName = pageRef.state.c__objectName;
        const originPhase = pageRef.state.c__originPhase;
        const recordType = pageRef.state.c__recordType;
        component.set("v.recordId", recordIdParam);
        objectName = (objectName !== 'Case') ? objectName+'__c' : objectName;
        component.set("v.objectName", objectName);
        component.set("v.originPhase", originPhase);
        component.set("v.objectRT", recordType);
        component.set("v.renderCancellationModal",true);
        helper.initialize(component,recordIdParam);
    },
    navigateToRecord : function (component, event, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.recordId"),
                objectApiName: component.get("v.objectName"),
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    }
});