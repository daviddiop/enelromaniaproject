({
    manageLoading : function (component, event, loading) {
        if(loading	===	"start") {
            var spinner = component.find('spinner');
            $A.util.removeClass(spinner,'slds-hide');
        } else if(loading === "stop") {
            var spinner = component.find('spinner');
            $A.util.addClass(spinner,'slds-hide');
        }
    },
    handleNavToWizard: function (component, helper) {
        const requestType = component.get('v.requestType');
        const wizardType = requestType === 'Connection' ? 'Activation' : requestType;
        let wizardName = 'c__MRO_LCP_' + wizardType + 'Wizard';
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: wizardName,
            },
            state: {
                "c__currentStepTitle": component.get('v.currentStepTitle'),
                "c__accountId": component.get('v.accountId'),
                "c__opportunityId": component.get('v.opportunityId'),
                "c__dossierId": component.get('v.dossierId'),
                "c__templateId": component.get('v.templateId')
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const ntfSvc = component.find('ntfSvc');
        const ntfLib = component.find('ntfLib');
        const workspaceAPI = component.find("workspace");

        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response === true) {
                workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                    var self = this;
                    workspaceAPI.isSubtab({
                        tabId: enclosingTabId
                    })
                    .then(function(isSubtab) {
                        if (isSubtab) {
                            workspaceAPI.openSubtab({
                                pageReference: pageReference,
                                focus: true
                            }).then(function () {
                                workspaceAPI.closeTab({
                                    tabId: enclosingTabId
                                });
                            }).catch(function (error) {
                                console.error('Redirect error', error);
                            });
                        } else {
                            workspaceAPI.openTab({
                                pageReference: pageReference,
                                focus: true
                            }).then(function () {
                                console.log('enclosingTabId', self.enclosingTabId);
                                workspaceAPI.closeTab({
                                    tabId: self.enclosingTabId
                                });
                            }).catch(function (error) {
                                console.error('Redirect error', error);
                            });
                        }
                    });
                });
            } else {
                const navService = component.find("navService");
                navService.navigate(pageReference);
            }
        }).catch(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        });
    },
    buildQueryString: function(component, helper, parameters){
        let queryString = "";
        for (let key in parameters) {
            if (queryString !== "") {
                queryString += "&";
            }
            queryString += key + "=" + encodeURIComponent(parameters[key]);
        }
        return queryString;
    },
    parseQueryString: function(component, helper, queryString) {
        let query = {};
        let pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
        for (let i = 0; i < pairs.length; i++) {
            let pair = pairs[i].split('=');
            query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
        }
        return query;
    },
})
