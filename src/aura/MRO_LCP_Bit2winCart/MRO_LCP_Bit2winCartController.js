({
    onInit : function(component, event, helper) {
        const pageReference = component.get("v.pageReference");
        if (pageReference && pageReference.state && pageReference.state.c__opportunityId) {
            component.set("v.opportunityId", pageReference.state.c__opportunityId);
            component.set("v.opportunityName", pageReference.state.c__opportunityName);
            component.set("v.accountId", pageReference.state.c__accountId);
            component.set("v.currentFlow", pageReference.state.c__currentFlow);
            component.set("v.currentStepTitle", pageReference.state.c__currentStepTitle);
            component.set("v.templateId", pageReference.state.c__templateId);
            component.set("v.requestType", pageReference.state.c__requestType);
            component.set("v.dossierId", pageReference.state.c__dossierId);
            component.set("v.commodity", pageReference.state.c__commodity);
            component.set("v.contractType", pageReference.state.c__contractType);
            component.set("v.rendered", true);

            if (component.get("v.requestType") === 'PartnerService'){
                // This is to prevent duplicate parameters in query string when changing values
                let params = helper.parseQueryString(component, helper, component.get("v.parameters"));
                params["c__hasAsistenta"] = pageReference.state.c__hasAsistenta;

                let queryString = helper.buildQueryString(component, helper, params);
                component.set("v.parameters", queryString);
            }

            if(pageReference.state.c__addOsiProduct){
                component.set("v.parameters", component.get("v.parameters") + '&addOsiProduct=true');
            }
        }
    },
    showSpinner : function(component) {},
    hideSpinner : function(component){},
    manageLoadingEvent : function ( component , event, helper ) {
        var loading	= event.getParam('loading');
        helper.manageLoading(component, event, loading);
    },
    hideCartCatalogButtons : function(component,event) {
        var what = event.getParam('whatShow');
        if(what === 'configurator') {
            var openCart =	component.find('openCart');
            var openCatalog	= component.find('openCatalog');
            $A.util.addClass(openCatalog,'slds-hide');
            $A.util.addClass(openCart,'slds-hide');
        }
    },
    onPageReferenceChanged: function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    updateCartQty : function (component,event) {},
    changeCatalogView : function(component,event) {
        var itemView = event.getParam('view');
        var container = component.find('totalAppContainer');
        if(itemView === 'singleAdd') {
            $A.util.removeClass(container, 'max-width-app-horizontal');
            $A.util.addClass(container, 'max-width-app-vertical');
        } else if(itemView === 'multiAdd') {
            $A.util.addClass(container, 'max-width-app-horizontal');
            $A.util.removeClass(container, 'max-width-app-vertical');
        }
    },
    afterCheckout : function(component, event, helper) {
        var data = event.getParam("bitwinMap");
        var checkoutData = data["checkOutResponse"];
        if (checkoutData) {
            console.log("checkOutResponse: ",JSON.stringify(checkoutData));
            console.log("configuration: ",checkoutData.configuration.Id);
            let cartItemList = [];
            for (var cartItem of checkoutData.cart) {
                cartItemList.push({
                    'configurationItemId': cartItem.fields.ConfigurationItemId,
                    'productName': cartItem.fields.productname,
                    'productId': cartItem.fields.productid,
                    'salesType': cartItem.fields.Sales_Type,
                    'quantity': cartItem.fields.qty,
                    'price':  cartItem.fields.baseonetimefee,
                    'recurringPrice':  cartItem.fields.recurringchargediscounted
                })
            }
            component.find('apexService').builder()
                .setMethod('createOli')
                .setInput({
                    "opportunityId": component.get('v.opportunityId'),
                    "cartItemList": JSON.stringify(cartItemList)
                })
                .setResolve(function (result) {
                    helper.handleNavToWizard(component, helper);
                })
                .setReject(function (error) {
                    const ntfSvc = component.find('ntfSvc');
                    const ntfLib = component.find('ntfLib');
                    ntfSvc.error(ntfLib, error);
                })
                .executeAction();
        }
    },
    onBack: function(component, event, helper) {
       // $A.get('e.force:refreshView').fire();
        helper.handleNavToWizard(component, helper);
    }
})
