({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);

        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const complaintType = myPageRef.state.c__complaintType;
        const genericRequestId = myPageRef.state.c__genericRequestId;

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
        component.set("v.complaintType", complaintType);

        component.find('apexService').builder()
            .setMethod("init")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "genericRequestId": genericRequestId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            }).setResolve(function (response) {
            self.hideSpinner(component);

            component.set("v.dossierId", response.dossier.Id);
            component.set("v.dossier", response.dossier);
            component.set("v.complaintRecordTypeId", response.complaintRecordTypeId);
            component.set("v.isPersonAccount", response.isPersonAccount);

            if (response.dossier.Id && !dossierId) {
                self.updateUrl(component, accountId, response.dossier.Id, complaintType);
                return;
            }

            component.set("v.startDate", response.startDate);
            component.set("v.endDate", response.endDate);
            const dossierVal = response.dossier;
            if (dossierVal) {
                if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                    component.set("v.isClosed", true);
                }
            }
            if (response.caseList && response.caseList.length > 0) {
                if(response.caseList[0].Supply__r) {
                    component.set('v.supplyStatus', response.caseList[0].Supply__r.Status__c);
                    component.set('v.inEnelArea', response.caseList[0].Supply__r.InENELArea__c);
                    if (response.caseList[0].Supply__r.ServicePoint__r) {
                        component.set('v.servicePointCode', response.caseList[0].Supply__r.ServicePoint__r.Code__c);
                    }
                }
                component.set("v.caseTile", response.caseList);
                component.set("v.selectedSupplyId", response.caseList[0].Supply__c);
                component.set('v.billingAccountNumber', response.caseList[0].ContractAccount__r ? response.caseList[0].ContractAccount__r.IntegrationKey__c : '');
                if (response.supplyType) {
                    self.setCaseSupplyType(component, response.supplyType);
                    self.setDistributorProvince(component, response.DistributorProvince);
                    self.setServiceSiteProvince(component, response.ServiceSiteProvince);
                    component.set("v.isENELDistributor", response.IsDisCoENEL__c);
                    console.log('isDiscoEnel: ' + response.IsDisCoENEL__c);
                    console.log('ServiceSiteProvince ' + response.ServiceSiteProvince)
                }
                component.set("v.step", 1);
            }
            else {
                component.set("v.step", 0);
            }
            console.log("Init Step: " + component.get("v.step"));

        }).setReject(function (error) {
            ntfSvc.error(ntfLib, error);
        }).executeAction();
    },
    handleSave: function (component, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const self = this;
        const caseList = component.get("v.caseTile");
        let selectedInvoiceIds = component.get("v.selectedInvoiceIds");
        let markedInvoiceIds = component.get("v.markedInvoiceIds");

        let selectedSubtype = component.get('v.subTypeValue').split(' - ')[0];
        if(['E01', 'E02', 'GE01', 'GE02'].includes(selectedSubtype)){
            markedInvoiceIds = selectedInvoiceIds;
        }

        self.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("saveComplaint")
            .setInput({
                caseObject: JSON.stringify(component.get('v.caseObject')),
                caseList: JSON.stringify(caseList),
                dossierId: component.get("v.dossierId"),
                accountId: component.get("v.accountId"),
                markedInvoiceIds: JSON.stringify(markedInvoiceIds),
                selectedInvoiceIds: JSON.stringify(selectedInvoiceIds),
                billingAdjustmentType: component.get("v.billingAdjType"),
                correctionType: component.get("v.correctionType"),
                channelSelected: component.get("v.channelSelected"),
                originSelected: component.get("v.originSelected")
            }).setResolve(function (response) {
            self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
        }).setReject(function (error) {
            self.hideSpinner(component);
            ntfSvc.error(ntfLib, error);
        })
            .executeAction();
    },
    supplyList: function (component, event, helper, selectedSupplyIds) {
        const self = this;
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];
        component.set('v.incorrectSupply', false);

        component.find('apexService').builder()
            .setMethod('CheckSelectedSupplies')
            .setInput({
                "selectedSupplyIds": JSON.stringify(selectedSupplyIds)
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                const searchedSupplyFieldsList = response.supplies;
                component.set('v.supplyStatus', response.supplies[0].Status__c);
                component.set("v.isENELDistributor", response.IsDisCoENEL__c);
                self.setDistributorProvince(component, response.DistributorProvince);
                self.setServiceSiteProvince(component, response.ServiceSiteProvince);
                for (let i = 0; i < searchedSupplyFieldsList.length; i++) {
                    let validSupply = true;
                    let oneSupply = searchedSupplyFieldsList[i];
                    component.set('v.servicePointCode', oneSupply.ServicePoint__r ? oneSupply.ServicePoint__r.Code__c : '');

                    component.set('v.inEnelArea', oneSupply.InENELArea__c);

                    self.setCaseSupplyType(component, oneSupply.RecordType.DeveloperName);
                    //self.updateBillingAdjustmentType(component,helper, oneSupply.RecordType.DeveloperName);

                    for (let j = 0; j < caseList.length; j++) {
                        if (oneSupply.Id === caseList[j].Supply__c) {
                            errSupplyAlreadySelected.push(oneSupply.Name);
                            validSupply = false;
                            break;
                        }
                    }
                    component.set('v.billingAccountNumber', oneSupply.ContractAccount__r ? oneSupply.ContractAccount__r.IntegrationKey__c : '');
                    if (validSupply) {
                        validSupplies.push(oneSupply);
                    }
                }
                if (errSupplyAlreadySelected.length !== 0) {
                    let messages = errSupplyAlreadySelected.join(' ');
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
                }
                if (errCurrentSupplyIsNotValid.length !== 0) {
                    let messages = errCurrentSupplyIsNotValid.join(' ');
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
                }

                component.set("v.searchedSupplyFields", validSupplies);
                self.createCase(component, event, helper, validSupplies);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    createCase: function (component, event, helper, validSupplies) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const channelSelected = component.get("v.channelSelected");
        const originSelected = component.get("v.originSelected");

        component.find('apexService').builder()
            .setMethod("createCase")
            .setInput({
                'supplies': JSON.stringify(validSupplies),
                'caseList': JSON.stringify(caseList),
                'accountId': accountId,
                'dossierId': dossierId,
                'channelSelected': channelSelected,
                'originSelected': originSelected
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, result.errorMsg);
                    return;
                }
                if (response.cases.length !== 0) {
                    component.set("v.step", 1);
                    component.set("v.companyDivisionId", response.cases[0].CompanyDivision__c);
                }
                component.set("v.caseTile", response.cases);
                component.set("v.selectedSupplyId", response.cases[0].Supply__c);
                component.set("v.osiTableView", response.cases.length > component.get("v.tileNumber"));
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    getMeters: function (component, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedSupplyId = component.get("v.selectedSupplyId");
        component.find('apexService').builder()
            .setMethod("GetMeters")
            .setInput({
                'selectedSupplyId': selectedSupplyId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set("v.showMeterReading", true);
                component.set("v.metersJson", JSON.parse(response.metersJson));
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    checkInvoice: function (component, helper, markedInvoiceIds) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const self = this;
        self.showSpinner(component);
        console.log("CheckBill +")
        component.find('apexService').builder()
            .setMethod("CheckBill")
            .setInput({
                'markedInvoiceId': markedInvoiceIds[0]
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set("v.checkBillReturnCodes", response.returnCodes);
                console.log("Check Bill Return Codes: " + component.get("v.checkBillReturnCodes"));
                self.updateCorrectionType(component, helper);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateIndexValues: function (component, meterInfo) {
        const selectedSupply = component.get("v.searchedSupplyFields");
        const caseObject = component.get("v.caseObject");
        const accountId = component.get("v.accountId");
        const caseList = component.get("v.caseTile");

        let meterInfoElems = Object.values(meterInfo);
        console.log("object values = " + meterInfoElems)

        for (let i = 0; i < meterInfoElems.length; i++) {
            component.find('apexService').builder()
                .setMethod("CreateIndex")
                .setInput({
                    'caseList': JSON.stringify(caseList),
                    'accountId': accountId,
                    'meterInfo': JSON.stringify(meterInfoElems[i])
                })
                .setResolve(function (response) {
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }

                    component.set("v.isIndexValidated", true);
                })
                .setReject(function (response) {
                    const errors = response.getError();
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                })
                .executeAction();
        }
    },
    handleCancel: function (component, helper) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseList = component.get("v.caseTile");
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: component.get("v.dossierId"),
                caseList: JSON.stringify(caseList),
            }).setResolve(function (response) {
            self.hideSpinner(component);
            self.redirectToDossier(component, helper);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        })
            .executeAction();
    },
    updateCorrectionType: function (component, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let checkBillReturnCodes = component.get("v.checkBillReturnCodes");
        let billingAdjInfoCmp = component.find('billingAdjInfo');
        if (billingAdjInfoCmp != null) {
            billingAdjInfoCmp.setCheckBillReturnCodes(checkBillReturnCodes);
            billingAdjInfoCmp.setBillingAdjustmentType(component.get("v.billingAdjType"));
            billingAdjInfoCmp.setIsDiscoEnel(component.get("v.isENELDistributor"));
        }
        let cod00 = checkBillReturnCodes.includes("COD 00"),
            cod01 = checkBillReturnCodes.includes("COD 01"),
            cod02 = checkBillReturnCodes.includes("COD 02"),
            cod03 = checkBillReturnCodes.includes("COD 03"),
            cod04 = checkBillReturnCodes.includes("COD 04"),
            cod05 = checkBillReturnCodes.includes("COD 05"),
            cod06 = checkBillReturnCodes.includes("COD 06"),
            cod07 = checkBillReturnCodes.includes("COD 07"),
            cod08 = checkBillReturnCodes.includes("COD 08");

        if(cod00){
            component.set("v.selectedInvoiceIds", []);
            component.set("v.markedInvoiceIds", []);
            ntfSvc.error(ntfLib, 'Please select another invoice. The selected one is not a valid one!');
        }

        let selectedSubtype = component.get('v.subTypeValue').split(' - ')[0];
        if(!['A042', 'E05-ES', 'GA042', 'GE05-ES'].includes(selectedSubtype)){
            return;
        }
        let incompatibleCod07 = cod07 && (cod06 || cod05);

        component.set('v.disableCorrectionType', false);
        console.log('#### v.checkBillReturnCodes', checkBillReturnCodes);
        if(!component.get('v.inEnelArea')){
            helper.forceManual(component);
        }else{
            if(incompatibleCod07){
                helper.forceManual(component);
            }else if(cod05 && !cod08){
                if(['E05-ES', 'GE05-ES'].includes(selectedSubtype)){
                    component.set('v.invalidSubtype', true);
                    ntfSvc.error(ntfLib, 'The selected invoice does not match the selected Tipar! Please change the Tipar if you want to continue');
                    return;
                }
                if(cod04 && !(cod01 || cod02 || cod03 || cod08)){
                    component.set("v.correctionType", "AutomaticDR");
                    component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsDRAndManual"));
                }else if(cod01 || cod02 || cod03 || cod08){
                    helper.forceManual(component);
                }else{
                    component.set("v.billingAdjType", (selectedSubtype.substr(0, 1) === 'G' ? 'G' : '') + "A042");
                    helper.checkIndexCodes(component, helper);
                }
            }else if((cod06 || cod07) && !cod08){
                component.set("v.billingAdjType", (selectedSubtype.substr(0, 1) === 'G' ? 'G' : '') + "E05-ES");
                helper.checkIndexCodes(component, helper);
            }else{
                helper.forceManual(component);
            }
        }

        let a042Check = ['A042', 'GA042'].includes(selectedSubtype) && (cod06 || cod07);
        let e05esCheck = ['E05-ES', 'GE05-ES'].includes(selectedSubtype) && cod05;

        if((selectedSubtype !== component.get("v.billingAdjType") || a042Check || e05esCheck) && !incompatibleCod07){
            component.set('v.invalidSubtype', true);
            ntfSvc.error(ntfLib, 'The selected invoice does not match the selected Tipar! Please change the Tipar if you want to continue');
        }else{
            component.set('v.invalidSubtype', false);
        }

    },
    forceManual: function(component){
        component.set("v.correctionType", "Manual");
        component.set('v.correctionTypeManual', true);
        component.set('v.disableCorrectionType', true);
    },
    checkIndexCodes: function(component, helper){
        let checkIndexReturnCodesMap = component.get("v.meterCodesMap");
        if(!checkIndexReturnCodesMap || Object.keys(checkIndexReturnCodesMap).length === 0){
            return helper.getMeters(component, helper);
        }
        let checkIndexReturnCodes = Object.values(checkIndexReturnCodesMap);
        if(checkIndexReturnCodes.includes('00')){
            component.set("v.correctionType", "Automatic");
        }else if(checkIndexReturnCodes.includes('99')){
            component.set("v.correctionType", "AutomaticDR");
        }else if(checkIndexReturnCodes.includes('90') || checkIndexReturnCodes.includes('91')){
            console.log('DISCARD Management');
        }else /*if(+checkIndexReturnCodes[0] > 8 && +checkIndexReturnCodes[0] < 18)*/{
            component.set("v.correctionType", "Manual");
            component.set('v.correctionTypeManual', true);
            component.set('v.disableCorrectionType', true);
        }
    },

    updateBillingAdjustmentCorrectionTypes: function (component, selectedSubType) {
        console.log('updateBillingAdjustmentCorrectionTypes selectedSubType: ' + selectedSubType);
        const self = this;
        switch (selectedSubType) {
            case 'A042 - Read (Read error)' : {
                component.set("v.billingAdjType", "A042");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsA042"));
                component.set("v.correctionType", "AutomaticDR");
                console.log("a042");
                break;
            }
            case 'E05-ES - Estimated Billing / IVR / Site' : {
                component.set("v.billingAdjType", "E05-ES");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsE05ES"));
                component.set("v.correctionType", "Manual");
                component.set('v.correctionTypeManual', true);
                console.log("e05");
                break;
            }
            case 'GA042 - Read (Read error)' : {
                component.set("v.billingAdjType", "GA042");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsA042"));
                component.set("v.correctionType", "AutomaticDR");
                console.log("ga042");
                break;
            }
            case 'GE05-ES - Estimated Billing / IVR / Site' : {
                component.set("v.billingAdjType", "GE05-ES");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsE05ES"));
                component.set("v.correctionType", "Manual");
                component.set('v.correctionTypeManual', true);
                console.log("ge05");
                break;
            }
        }
        let billingAdjInfoCmp = component.find('billingAdjInfo');
        if (billingAdjInfoCmp != null) {
            billingAdjInfoCmp.setBillingAdjustmentType(component.get("v.billingAdjType"));
            billingAdjInfoCmp.setIsDiscoEnel(component.get("v.isENELDistributor"));
        }
        self.updateCorrectionType(component, self);
    },
    updateBillingAdjustmentType: function (component, helper, supplyType) {
        switch (supplyType) {
            case "Electric": {
                component.set("v.billingAdjType", "A042");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsA042"));
                component.set("v.correctionType", "AutomaticDR");
                break;
            }
            case "Electricity": {
                component.set("v.billingAdjType", "A042");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsA042"));
                component.set("v.correctionType", "AutomaticDR");
                break;
            }
            case "Gas": {
                component.set("v.billingAdjType", "E05-ES");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsE05ES"));
                component.set("v.correctionType", "Manual");
                component.set('v.correctionTypeManual', true);
                break;
            }
            case "Service": {
                component.set("v.billingAdjType", "E05-ES");
                component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsE05ES"));
                component.set("v.correctionType", "Manual");
                component.set('v.correctionTypeManual', true);
                break;
            }
        }
    },
    setCaseSupplyType: function (component, supplyRecordType) {
        switch (supplyRecordType) {
            case "Electric": {
                component.set("v.selectedSupplyType", "Electricity");
                break;
            }
            case "Gas": {
                component.set("v.selectedSupplyType", "Gas");
                break;
            }
            case "Service": {
                component.set("v.selectedSupplyType", "VAS");
                break;
            }
        }
        component.set("v.isSupplyTypeDisabled", true);
    },
    setDistributorProvince: function (component, distributorProvince) {
        if (distributorProvince != undefined) {
            component.set("v.distributorProvince", distributorProvince);
            component.set("v.isProvinceDisabled", true);
        }
    },
    setServiceSiteProvince: function (component, serviceSiteProvince) {
        if (serviceSiteProvince != undefined) {
            component.set("v.serviceSiteProvince", serviceSiteProvince);
            component.set("v.isProvinceDisabled", true);
        }
    },
    manageProvinceVisibility: function (component, complaintType) {
        console.log('manageProvinceVisibility complaintType= ' + complaintType);
        switch (complaintType) {
            case "For network activity": {
                component.set('v.showProvinceField', true);
                break;
            }
            default : {
                component.set('v.showProvinceField', false);
            }
        }
    },
    configureSubTypeList: function (component, complaintType) {
        const self = this;
        switch (complaintType) {
            case "For network activity": {
                self.getSubTypeDependencies(component, complaintType);
                component.set("v.showSubTypeFilteredList", true);
                component.set("v.isNetworkActivity", true);
                let fixField = component.find('SubType__c');
                $A.util.addClass(fixField, 'slds-hide');
                break;
            }
            case "For commercial activity": {
                self.getSubTypeDependencies(component, complaintType);
                component.set("v.showSubTypeFilteredList", true);
                component.set("v.isNetworkActivity", false);
                let fixField = component.find('SubType__c');
                $A.util.addClass(fixField, 'slds-hide');
                break;
            }
            case "ANRE provision": {
                self.getSubTypeDependencies(component, complaintType);
                component.set("v.showSubTypeFilteredList", true);
                component.set("v.isNetworkActivity", false);
                let fixField = component.find('SubType__c');
                $A.util.addClass(fixField, 'slds-hide');
                break;
            }
            default : {
                component.set("v.showSubTypeFilteredList", false);
                component.set("v.isNetworkActivity", false);
                let fixField = component.find('SubType__c');
                $A.util.removeClass(fixField, 'slds-hide');
            }
        }
    },
    enableIncidentField: function (component, selectedOptionValue) {
        if (component.get("v.isNetworkActivity") && selectedOptionValue === 'R_A011 - Damage to equipment') {
            component.set("v.showIncidentDate", true);//
        } else {
            component.set("v.showIncidentDate", false);
        }
    },
    getSubTypeDependencies: function (component, complaintType) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("GetAvailableSubTypes")
            .setInput({
                complaintType: complaintType,
                supplyType: component.get("v.selectedSupplyType"),
            }).setResolve(function (response) {
            component.set("v.subTypeDependencyMetadata", response.availableSubTypes);
            self.generateSubTypeList(component, response.availableSubTypes);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        }).executeAction();
    },
    generateSubTypeList: function (component, availableSubTypes) {
        let subTypeOptions = [];
        for (let i = 0; i < availableSubTypes.length; i++) {
            subTypeOptions.push({'label': availableSubTypes[i].Label__c, 'value': availableSubTypes[i].Label__c});
        }
        component.set("v.availableSubTypes", subTypeOptions);
    },
    getAvailableReasons: function (component, selectedSubType) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("GetAvailableReasons")
            .setInput({
                caseSubType: selectedSubType,
            }).setResolve(function (response) {
            self.generateReasonList(component, response.availableReasons);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        }).executeAction();
    },
    generateReasonList: function (component, availableReasons) {
        let availableReasonsList = [];
        for (let i = 0; i < availableReasons.length; i++) {
            availableReasonsList.push({'label': availableReasons[i].Label__c, 'value': availableReasons[i].Label__c});
        }
        if (availableReasonsList.length > 0) {
            component.set("v.availableReasons", availableReasonsList);
            let fixField = component.find('Reason__c');
            $A.util.addClass(fixField, 'slds-hide');
            component.set("v.showReasonsList", true);
        }
        else {
            component.set("v.showReasonsList", false);
            let fixField = component.find('Reason__c');
            $A.util.removeClass(fixField, 'slds-hide');
            component.set("v.reasonValue", "");
        }
    },
    enableAdditionalStepsBySubType: function (component, selectedSubType) {
        const self = this;
        let subTypeMetadataList = component.get("v.subTypeDependencyMetadata");
        let selectedSubTypeMetadata;
        for (let i = 0; i < subTypeMetadataList.length; i++) {
            if (subTypeMetadataList[i].Label__c === selectedSubType) {
                selectedSubTypeMetadata = subTypeMetadataList[i];
                break;
            }
        }
        if (selectedSubTypeMetadata) {
            const caseList = component.get("v.caseTile");
            component.set("v.showInvoiceSelection", selectedSubTypeMetadata.HasInvoiceSelection__c && caseList && caseList.length > 0);
            component.set("v.showBillingAdjustment", selectedSubTypeMetadata.HasBillingAdjustment__c);
            component.set("v.isInvoiceMarkingDisabled", selectedSubTypeMetadata.DisableInvoiceMarking__c);
            component.set("v.showParentCaseField", selectedSubTypeMetadata.HasParentCaseField__c);
            if (selectedSubTypeMetadata.HasBillingAdjustment__c === true) {
                self.updateBillingAdjustmentCorrectionTypes(component, selectedSubType);
            }
        }
        console.log("SelectedSubType: " + selectedSubType + " InvoiceSelection Enabled: " + selectedSubTypeMetadata.HasInvoiceSelection__c + " Billing Adjustment Enabled: " + selectedSubTypeMetadata.HasBillingAdjustment__c + " Disable Invoice Marking: " + selectedSubTypeMetadata.DisableInvoiceMarking__c);
    },
    warnIfNoSupplySelected: function (component, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseList = component.get("v.caseTile");
        if (caseList.length == 0) {
            ntfSvc.warn(ntfLib, $A.get("$Label.c.NoSupplySelected"));
        }
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id != deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        if (items.length === 0) {
            component.set("v.step", 0);
        }

        component.set("v.caseTile", items);
    },
    removeError: function (component, event, helper) {
        let fieldName = event.getSource().get("v.fieldName");
        let fieldValue = event.getSource().get("v.value");

        if(fieldName === 'IncidentDate__c' && fieldValue){
            try{
                let incidentTimestamp = new Date(fieldValue).getTime();
                let twoWeeksAgo = new Date().getTime() - 14 * 24 * 60 * 60 * 1000;
                if(twoWeeksAgo > incidentTimestamp){
                    const ntfLib = component.find('notifLib');
                    const ntfSvc = component.find('notify');
                    ntfSvc.warn(ntfLib, 'In case the customer does NOT have justificativ documents, please cancel the request and waits for the customer to bring them');
                }
            }catch (e) {

            }
        }

        if (component.find(fieldName)) {
            let fixField = component.find(fieldName);
            if (Array.isArray(fixField)) {
                fixField = fixField[0];
            }
            $A.util.removeClass(fixField, 'slds-has-error');
        }
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    updateUrl: function (component, accountId, dossierId, complaintType) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ComplaintsWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId,
                "c__complaintType": complaintType
            }
        };
        navService.navigate(pageReference,true);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
            let self = this;
            const workspaceAPI = component.find("workspace");
            if (workspaceAPI) {
                workspaceAPI.isConsoleNavigation()
                    .then(function (response) {
                        if (response === true) {
                            workspaceAPI.getFocusedTabInfo()
                                .then(function (response) {
                                    let focusedTabId = response.tabId;
                                    let isSubTab = response.isSubtab;
                                    if (isSubTab) {
                                        workspaceAPI.setTabLabel({
                                            tabId: focusedTabId,
                                            label: wizardLabel
                                        });
                                        workspaceAPI.setTabIcon({
                                            tabId: focusedTabId,
                                            icon: "utility:case",
                                            iconAlt: wizardLabel
                                        });
                                    }
                                });
                        }
                    }).catch(function (error) {
                    console.log(error);
                });
                document.title = $A.get("$Label.c.LightningExperienceSalesforce");
            }
        },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    validateFields: function (component, fields) {
        var REQUIRED_FIELDS = ['Origin', 'Channel__c', 'Type', 'SubType__c', 'CustomerNotes__c'];
        var listFieldInError = [];
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');
        for (var i in fields) {
            if ((REQUIRED_FIELDS.includes(i) && (fields[i] == null || fields[i] === ''))) {
                listFieldInError.push(i);
            }
        }
        if (listFieldInError.length && listFieldInError) {
            for (let e in listFieldInError) {
                var fieldName = listFieldInError[e];
                if (component.find(fieldName)) {
                    var fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    }
});