({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        if (dossierId){
        helper.initConsoleNavigation(component,$A.get("$Label.c.Complaints"));
        }
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initialize(component, event, helper);
    },
 /*   onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo().then(function (response) {
                        let focusedTabId = response.tabId;
                        let isSubTab = response.isSubtab;
                        if (isSubTab) {
                            setTabLabel({
                                tabId: focusedTabId,
                                label: $A.get("$Label.c.Complaints")
                            });
                            setTabIcon({
                                tabId: focusedTabId,
                                icon: "utility:case",
                                iconAlt: $A.get("$Label.c.Complaints")
                            });
                        }
                    });
                }
            });
    }, */
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'confirmStep0':
                component.set("v.step", 1);
                helper.warnIfNoSupplySelected(component, helper);
                break;
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep0':
                component.set("v.step", 0);
                break;
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            default:
                break;
        }
    },
    handleSupplyTypeChanged: function (component, event, helper) {
        let fieldName = event.getSource().get("v.fieldName");
        let supplyType = event.getSource().get("v.value");
        if (fieldName == 'SupplyType__c' && supplyType != "") {
            component.set("v.selectedSupplyType", supplyType);
        }
        //helper.updateBillingAdjustmentType(component,helper, supplyType);
        helper.removeError(component, event, helper)
    },
    handleComplaintTypeChanged: function (component, event, helper) {
        let fieldName = event.getSource().get("v.fieldName");
        let complaintType = event.getSource().get("v.value");
        if (fieldName == 'Type' && complaintType != "") {
            helper.configureSubTypeList(component, complaintType);
            helper.manageProvinceVisibility(component, complaintType);
        }
        helper.removeError(component, event, helper)
    },
    removeError: function (component, event, helper) {
        helper.removeError(component, event, helper);
    },
    handleSubmit: function (component, event, helper) {
        event.preventDefault();
        var fields = event.getParam("fields");
        if (helper.validateFields(component, fields)) {
            component.set('v.caseObject', fields);
            component.set("v.step", 2);
        }
    },
    invoiceSelectHandler: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let selectedInvoiceList = event.getParam("selectedInvoiceList");
        let markedInvoiceList = event.getParam("markedInvoiceList");
        console.log("Marked invoice List:  " + JSON.stringify(event.getParam("markedInvoiceList")));
        if (!selectedInvoiceList || !selectedInvoiceList.length) {
            component.set("v.selectedInvoiceIds", []);
            component.set("v.markedInvoiceIds", []);
            return;
        }

        let selectedSubtype = component.get('v.subTypeValue').split(' - ')[0];
        if(['A042', 'E05-ES', 'GA042', 'GE05-ES'].includes(selectedSubtype) && selectedInvoiceList.length > 1){
            ntfSvc.error(ntfLib, 'For this COD Tipar only one invoice can be selected');
            component.set("v.selectedInvoiceIds", []);
            component.set("v.markedInvoiceIds", []);
            return;
        }

        let metersParams = {
            'InvoiceNumber' : selectedInvoiceList[0].invoiceNumber,
            'InvoiceSerialNumber' : selectedInvoiceList[0].invoiceSerialNumber,
        };
        component.set('v.metersParamsJson', JSON.stringify(metersParams));
        let selectedInvoiceIds = selectedInvoiceList.reduce(function (invoiceIds, invoice) {
            invoiceIds.push(invoice.invoiceSerialNumber + ' ' + invoice.invoiceNumber);
            return invoiceIds;
        }, []);
        let markedInvoiceIds = markedInvoiceList.reduce(function (invoiceIds, invoice) {
            invoiceIds.push(invoice.invoiceSerialNumber + ' ' + invoice.invoiceNumber);
            return invoiceIds;
        }, []);
        component.set("v.selectedInvoiceIds", selectedInvoiceIds);
        if (!component.get("v.isInvoiceMarkingDisabled")) {
            component.set("v.markedInvoiceIds", markedInvoiceIds);
        }
        ntfSvc.success(ntfLib, 'Selected : ' + selectedInvoiceIds + ' Marked : ' + markedInvoiceIds);

        let complaintType = component.get("v.complaintType");
        console.log("Complaint type = " + complaintType);
        helper.checkInvoice(component, helper, markedInvoiceIds);
    },
    handleSubTypeOptionsChange: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedOptionValue = event.getParam("value");
        component.set("v.subTypeValue", selectedOptionValue);

        component.set("v.showMeterReading", false);
        component.set("v.checkBillReturnCodes", '');
        let selectedSubtype = selectedOptionValue.split(' - ')[0];
        let isA042orE05ES = ['A042', 'E05-ES'].includes(selectedSubtype);
        let isE01E02 = ['E01', 'E02', 'GE01', 'GE02'].includes(selectedSubtype);
        let isGA042orGE05ES = ['GA042', 'GE05-ES'].includes(selectedSubtype);

        component.set('v.invalidSubtype', false);
        if(selectedSubtype === 'R_A011' && !component.get('v.isPersonAccount')) {
            component.set('v.invalidSubtype', true);
            ntfSvc.error(ntfLib, 'Business Customers are not compatible with this request');
        }

        component.set("v.invoicesInquiryIsRequired", isA042orE05ES || isE01E02);
        if((isA042orE05ES || isGA042orGE05ES) && component.get('v.supplyStatus') !== 'Active'){
            component.set('v.incorrectSupply', true);
            ntfSvc.error(ntfLib, 'For this COD Tipar you must select an Active Supply');
        }else{
            component.set('v.incorrectSupply', false);
        }
        console.log(selectedOptionValue);
        console.log(component.get("v.invoicesInquiryIsRequired"));
        helper.enableAdditionalStepsBySubType(component, selectedOptionValue);
        helper.getAvailableReasons(component, selectedOptionValue);
        helper.enableIncidentField(component, selectedOptionValue);
    },
    handleReasonOptionsChange: function (component, event, helper) {
        let selectedOptionValue = event.getParam("value");
        component.set("v.reasonValue", selectedOptionValue);
    },
    handleMeterListValidated: function (component, event, helper) {
        let meterInfo = event.getParam("meterInfo");
        if (event.getParam("isMeterValidated")) {
            component.set("v.correctionType", "Automatic");
            helper.updateIndexValues(component, meterInfo);
            let billingAdjInfoCmp = component.find('billingAdjInfo');
            if (billingAdjInfoCmp != null) {
                billingAdjInfoCmp.setIsIndexValid(true);
            }
        }
        console.log("Was index validated: " + component.get("v.isIndexValidated"));
    },
    handleMeterListEdited: function (component, event, helper) {
        let returnCode = event.getParam("returnCode");
        if(returnCode !== '00'){
            component.set("v.isIndexValidated", false);
            let billingAdjInfoCmp = component.find('billingAdjInfo');
            if (billingAdjInfoCmp != null) {
                billingAdjInfoCmp.setIsIndexValid(false);
            }
        }
        let meterCode = event.getParam("meterCode");
        let meterCodesMap = component.get('v.meterCodesMap');
        meterCodesMap[meterCode] = returnCode;
        component.set('v.meterCodesMap', meterCodesMap);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set("v.originSelected", originSelected);
            component.set("v.channelSelected", channelSelected);
            component.set("v.isClosed", false);
        } else {
            component.set("v.isClosed", true);
        }
    },
    getSupplies: function (component, event, helper) {
        let selectedSupplyIds = event.getParam("selected");
        component.set('v.subTypeValue', '');
        component.set('v.meterCodesMap', {});
        helper.supplyList(component, event, helper, selectedSupplyIds);

    },
    handleBillingAdjOptionsChange: function (component, event, helper) {
        let selectedOptionValue = event.getParam("value");
        console.log("handleBillingAdjOptionsChange called : " + selectedOptionValue);
        if (selectedOptionValue == "A042") {
            component.set("v.billingAdjType", "A042");
            component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsA042"));
            component.set("v.correctionType", "AutomaticDR");
        }
        else {
            component.set("v.billingAdjType", "E05-ES");
            component.set("v.correctionTypeOptions", component.get("v.correctionTypeOptionsE05ES"));
            component.set("v.correctionType", "Manual");
            component.set('v.correctionTypeManual', true);
        }
    },
    handleCorrectionOptionsChange: function (component, event, helper) {
        let selectedOptionValue = event.getParam("value");
        //component.set("v.showMeterReading", selectedOptionValue === 'Automatic');
        component.set('v.correctionType', selectedOptionValue);
        component.set('v.correctionTypeManual', selectedOptionValue === 'Manual');
        console.log("handleCorrectionOptionsChange called : " + selectedOptionValue);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseObject = component.get("v.caseObject");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (!caseObject) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        const markedInvoiceIds = component.get("v.markedInvoiceIds");
        const selectedInvoiceIds = component.get("v.markedInvoiceIds");
        if (selectedInvoiceIds.legend > 0 && (!markedInvoiceIds || !markedInvoiceIds.length) && !component.get("v.isInvoiceMarkingDisabled")) {
            ntfSvc.error(ntfLib, $A.get("The selected invoice could not be marked"));
            return;
        }

        helper.handleSave(component, helper);
    },
    cancel: function (component, event, helper) {
        component.set('v.showCancelBox', true);
    },
    onCloseCancelBox: function (component, event, helper) {
        component.set('v.showCancelBox', false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    },
});