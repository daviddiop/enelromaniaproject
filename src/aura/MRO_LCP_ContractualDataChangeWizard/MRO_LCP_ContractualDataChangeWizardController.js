/**
 * Created by Stefano on 03/04/2020.
 */

({
    init: function(component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        component.set("v.openCase", '');
        component.set("v.isContractualDataChange", true);
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);
        component.set("v.commodities", ['Electric', 'Gas']); //Alessio 18/11/2020 -- ENLCRO-1783
        helper.initialize(component, event, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');

        component.set('v.originSelected', originSelected);
        component.set('v.channelSelected', channelSelected);
        let filled = channelSelected != null && originSelected != null;

        if(filled && component.get("v.step") == 0 && !component.get('v.wizardHeaderLoaded')){
            component.set('v.disableOriginChannel', filled);
        }
        component.set('v.wizardHeaderLoaded', true);
        if(!component.get('v.disableWizardHeader')) {
            if (component.get("v.step") <= 1) {
                if (channelSelected != null && originSelected != null) {
                    component.set("v.step", 1);
                } else {
                    component.set("v.step", 0);
                }

            } else if (filled) {
                component.set('v.disableOriginChannel', true);
            }

        }
    },
    closeCancelModal: function (component, event, helper) {
        component.set("v.showCancel", false);
    },
    cancel: function (component, event, helper) {
        component.set("v.showCancel", true);
    },
    save: function (component, event, helper) {
        let isSaving=component.get("v.isSaving");
        if(!isSaving) {
            component.set("v.isSaving", true);
            //helper.createPrivacyChangeRecord(component);
        }
        helper.saveSendingChannel(component);
        let areFieldsValidated = component.get("v.areFieldsValidated");
        if(areFieldsValidated === true){
            helper.saveContractualCasesChain(component, helper);
        }
    },
    saveDraft: function (component, event, helper) {
        component.set("v.isSaving", false);
        //helper.createPrivacyChangeRecord(component);
        helper.redirectToDossier(component, helper);
    },

    handleSavedSendingChannel: function (component, event, helper) {
        component.set("v.step", 5);
    },

    handleSubProcess: function (component, event) {
        let subProcess = event.getParam("subProcess");

        component.set("v.selectedSubProcess", subProcess);
    },
    onSaveCancelReason: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.redirectToDossier(component, helper);

    },

    nextStep: function (component, event, helper) {
        const self = this;
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if(component.get('v.originSelected') && component.get('v.channelSelected')) {
                    component.set('v.disableOriginChannel',true);
                    component.set('v.disableWizardHeader',false);

                    helper.setChannelAndOrigin(component);

                }else {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectOriginAndChannel"));
                    return;
                }

                break;
            case 'confirmStep2':
                let subprocess=component.get("v.selectedSubProcess");

                if(subprocess) {
                    if (component.get("v.selectedSubProcess") === 'None') {
                        component.set('v.hasSubProcessNone', true);
                    }
                    let isPersonalAccount = component.get("v.isPersonAccount");

                    if(!isPersonalAccount && subprocess === component.get("v.subProcessChangeContact")){
                        component.find('contactSelection').open();
                        return;
                    }
                    helper.validateSubProcessValue(component, true);
                    helper.updateSearchSupplyCondition(component);
                }else{
                    //ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    //component.set("v.step", 3);
                    return;
                }
                break;
            case 'confirmStep3':
                helper.reloadSendingChannel(component);
                component.set("v.step", 4);
                break;

            /*case 'confirmStep4':
                helper.saveSendingChannel(component);
                break;*/
        }
    },
    editStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                component.set('v.disableOriginChannel',false);
                component.set('v.disableWizardHeader',true);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                component.set("v.step", 4);
                break;
            case 'returnStep5':
                component.set("v.step", 5);
                break;
            case 'returnStep6':
                component.set("v.step", 6);
                break;
            case 'returnStep7':
                component.set("v.step", 7);
                break;
            case 'returnStep8':
                component.set("v.step", 8);
                break;
            case 'returnStep9':
                component.set("v.step", 9);
                break;
            case 'returnStep10':
                component.set("v.step", 10);
                break;
            default:
                break;
        }
    },

    handleSupplyResult: function (component,event,helper) {
        // HANDLE event after advanced search
        helper.checkSelectedSupply(component,event,helper);
        if(component.get("v.cases").length > 0){
            component.set("v.disableAdvancedSearch", true);
        }

    },
    handleNewCase : function(component, event, helper){
        let caseId = event.getParam("caseId");
        component.find('contractualDataEdit').close();
        helper.retrieveCase(component, caseId);
    },
    handleCaseDelete: function(component,event,helper){
        let caseId = event.getParam("recordId");
        helper.deleteCase(component,caseId);

    },
    validateSendingChannelFields: function(component, event, helper){
         let areFieldsValidated = event.getParam('areFieldsValidated');
         component.set("v.areFieldsValidated", areFieldsValidated);
    },
    closeContractualDataEditModal: function(component, event, helper){
        let disableAdvancedSearch = event.getParam('disableAdvancedSearch');
        component.set("v.disableAdvancedSearch", disableAdvancedSearch);
        component.find('contractualDataEdit').set('v.selfReadingPeriodEndList', []);
        component.find('contractualDataEdit').set('v.selfReadingPeriodEndVal', '');

    }
});