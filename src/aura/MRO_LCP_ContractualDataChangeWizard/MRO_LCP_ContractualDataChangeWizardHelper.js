/**
 * Created by Stefano on 03/04/2020.
 */

({
    initialize: function(component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const parentATRCaseId = myPageRef.state.c__parentATRCaseId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const gdprParentDossierId = myPageRef.state.c__gDPRParentDossierId;
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        component.set("v.accountDataChanged", false);
        component.set("v.isClosed", false);
        component.set("v.showCancel", false);
        component.set("v.isSaving", false);
        component.set("v.showNewContractualDataEdit", false);
        component.set('v.account','');
        component.set('v.selectedSubProcess','');
        component.set('v.validSubProcess','');
        component.set('v.originSelected','');
        component.set('v.channelSelected','');
        component.set('v.disableOriginChannel',true);
        component.set('v.disableWizardHeader',true);
        component.set('v.originSelected', '');
        component.set('v.channelSelected', '');
        component.set('v.step', 0);

        component.set("v.accountId", accountId);
        component.find("apexService").builder()
            .setMethod("initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "templateId": templateId,
                "interactionId": component.find("cookieSvc").getInteractionId(),
                "parentATRCaseId": parentATRCaseId,
                "genericRequestId": genericRequestId,
                "gdprParentDossierId": gdprParentDossierId
            }).setResolve(function(response) {
            self.hideSpinner(component);
            if (response.error) {
                ntfSvc.error(ntfLib, response.errorMsg);
                console.error("err==="+response.errorTrace);
                let openCase=response.openCase;
                if(openCase && openCase === true){
                    component.set("v.isClosed",true);
                    component.set("v.openCase", 'notEmpty');
                }
                return;
            }

            if (!response.error) {
                component.set("v.caseId", response.caseId);
                component.set("v.accountId", response.accountId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.templateId", response.templateId);
                component.set("v.account", response.account);
                component.set("v.isPersonAccount", response.isPersonAccount);
                component.set("v.recordTypeCustomerDataChange", response.recordTypeCustomerDataChange);
                component.set("v.recordType", response.recordType);

                if ((response.dossierId && !dossierId) && (response.templateId && !templateId)) {
                    self.updateUrl(component, accountId, response.dossierId, response.templateId, parentATRCaseId);
                }

                component.set("v.dossierId", dossierId);

                if (response.companyDivisionName) {
                    component.set("v.companyDivisionName", response.companyDivisionName);
                    component.set("v.companyDivisionId", response.companyDivisionId);
                }

                const dossierVal = component.get("v.dossier");

                if (dossierVal) {
                    if (dossierVal.Status__c === "New" || dossierVal.Status__c === "Canceled") {
                        component.set("v.isClosed", true);
                    }
                }

                let step = 0;

                if (dossierVal.Origin__c) {
                    component.set("v.originSelected", dossierVal.Origin__c);
                }

                if (dossierVal.Channel__c) {
                    component.set("v.channelSelected", dossierVal.Channel__c);
                    step = 2;
                }

                let subProcess=dossierVal.SubProcess__c;
                if (subProcess) {
                    component.set("v.selectedSubProcess", subProcess);
                    component.set("v.validSubProcess", subProcess);
                    step = 3;
                }

                component.set("v.cases", []);

                if(response.cases && response.cases.length > 0) {
                    component.set("v.cases", response.cases);
                    component.set("v.caseTableView", response.cases.length > component.get("v.tileNumber"));
                    step = 4;
                }

                let disableHeader=step > 1;
                component.set('v.disableWizardHeader',disableHeader);
                component.set('v.disableOriginChannel',disableHeader);
                component.set("v.step", step);
                if(subProcess){
                    let isPersonalAccount = component.get("v.isPersonAccount");
                    if(!isPersonalAccount && subProcess === component.get("v.subProcessChangeContact")){
                        return;
                    }
                    //helper.validateSubProcessValue(component, false);
                }
            }

        })
            .setReject(function(error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    validateSubProcessValue : function (component, showError) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        const self=this;
        let dossierId = component.get('v.dossierId');
        let subProcess = component.get('v.selectedSubProcess');
        let accountId= component.get('v.accountId');
        const caseList = component.get("v.cases");
        //component.set("v.accountDataChanged", false);
        component.find("apexService").builder()
            .setMethod("validateSubProcess")
            .setInput({
                dossierId: dossierId,
                subProcess:subProcess,
                accountId : accountId,
                caseList: JSON.stringify(caseList)
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                if(response.authorized){
                    component.set("v.validSubProcess",subProcess);
                    component.set("v.dossierStatus",response.dossierStatus);
                    component.set("v.casePhase",response.casePhase);
                    component.set("v.caseStatus",response.caseStatus);
                    if(response.warnMsg){
                        ntfSvc.warn(ntfLib, response.warnMsg);
                    }
                    if(response.clear || component.get("v.cases").length === 0){
                        component.set("v.cases",[]);
                        component.set("v.disableAdvancedSearch", false);
                    }
                    component.set("v.step", 3);

                } else {
                    component.set("v.validSubProcess",'');
                    if(showError) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                    }

                }
                //self.setSubProcessChanges(component);

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    showSpinner: function(component) {
        $A.util.removeClass(component.find("spinnerSection"), "slds-hide");
    },
    hideSpinner: function(component) {
        $A.util.addClass(component.find("spinnerSection"), "slds-hide");
    },
    saveContractualCasesChain: function(component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.cases");
        const dossierId = component.get("v.dossierId");
        const accountId = component.get('v.accountId');
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");

        component.find("apexService").builder()
            .setMethod("saveContractualDataChange")
            .setInput({
                caseList: JSON.stringify(caseList),
                accountId: accountId,
                dossierId: dossierId
            }).setResolve(function(response) {
            self.hideSpinner(component);
            if (!response.error) {
                let caseList = component.get("v.cases");
                self.createCaseComments(component,caseList);
            }else {
                ntfSvc.error(ntfLib, response.errorMsg);

            }
        })
            .setReject(function(error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    updateUrl: function(component, accountId, dossierId, templateId, parentATRCaseId) {
        const navService = component.find("navService");
        const pageReference = {
            type: "standard__component",
            attributes: {
                componentName: "c__MRO_LCP_ContractualDataChangeWizard"
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId,
                "c__templateId": templateId,
                "parentATRCaseId": parentATRCaseId
            }
        };
        navService.navigate(pageReference, true);
    },
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        //let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set("v.step", 2);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    retrieveCase: function (component, caseId) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const dossierId = component.get('v.dossierId');
        const commodity = component.get('v.selectedCommodity');
        component.find('apexService').builder()
            .setMethod('retrieveCase')
            .setInput({
                "caseId": caseId,
                "commodity": commodity,
                "dossierId": dossierId
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                let caseList = component.get("v.cases") || [];
                let newCase = response.case;
                caseList.push(newCase);
                //component.set("v.step", 1);
                component.set("v.caseId", newCase.Id);
                component.set("v.showNewContractualDataEdit", false);
                component.set("v.caseTableView", caseList.length > component.get("v.tileNumber"));
                component.set("v.cases", caseList);
                component.set("v.selectedCommodity", newCase.Supply__r.RecordType.Name);
                if(component.get("v.cases").length > 0){
                    component.set("v.disableAdvancedSearch", true);
                }
                //self.resetSupplyForm(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    deleteCase: function(component,caseId){

        component
            .find("apexService")
            .builder()
            .setMethod("deleteCase")
            .setInput({
                caseId: caseId
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                const cases = [];
                let casesList= component.get("v.cases");
                for (let i = 0; i < casesList.length; i++) {
                    if (casesList[i].Id !== caseId) {
                        cases.push(casesList[i]);
                    }
                }

                component.set("v.cases", cases);
                if(cases.length === 0){
                    component.set("v.disableAdvancedSearch", false);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    checkSelectedSupply: function(component,event,helper){
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedSupplyId = event.getParam("selected")[0];
        component.set('v.selectedSupplyId',selectedSupplyId);
        let dossierId = component.get('v.dossierId');
        let subProcess = component.get('v.selectedSubProcess');

        component.find("apexService").builder()
            .setMethod("checkSelectedSupply")
            .setInput({
                "supplyId": selectedSupplyId,
                "dossierId": dossierId,
                "subProcess":subProcess
            }).setResolve(function(response) {
            helper.hideSpinner(component);
            if (response.error || !response.isValid) {
                ntfSvc.error(ntfLib, response.errorMsg);
                console.error("err===" + response.errorTrace);
                return;
            }
            if (!response.error && response.isValid) {
                let supply = response.supply;
                component.set("v.selectedSupplyId", supply.Id);
                component.set("v.companyDivisionId", supply.CompanyDivision__c);
                component.set("v.selectedCommodity", supply.RecordType.Name);
                component.find('contractualDataEdit').open(); // ********** Contractual Data Edit starts here *********
            }
        }).setReject(function(error) {
            ntfSvc.error(ntfLib, error);
        })
            .executeAction();
    },
   /* createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent instanceof Array) {
            let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
            privacyChangeComponentToObj[0].savePrivacyChange();
        } else {
            privacyChangeComponent.savePrivacyChange();
        }
    },*/
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    redirectToDossier: function(component, helper) {
        const pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: "c__Dossier",
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function(component, pageReference) {
        const { closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab } = component.find("workspace");
        isConsoleNavigation()
            .then(function(response) {
                if (response === true) {
                    getEnclosingTabId().then(function(enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function() {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function(error) {
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function(error) {
            console.log(error);
        });
    },

    createCaseComments: function(component,cases){
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let selectedSubProcess = component.get('v.selectedSubProcess');

        component.find('apexService').builder()
            .setMethod('createCaseComment')
            .setInput({
                "cases": JSON.stringify(cases)
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                if (['Change Contracted Quantities', 'Change Consumption conventions'].includes(selectedSubProcess)) {
                    self.addSupplyTypeToCase(component, self);
                }

                self.redirectToDossier(component, self);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    addSupplyTypeToCase: function(component, self){
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let dossierId = component.get('v.dossierId');
        let selectedSupplyId = component.get('v.selectedSupplyId');

        component.find('apexService').builder()
            .setMethod('AddSupplyTypeToCase')
            .setInput({
                "dossierId": dossierId,
                "selectedSupplyId": selectedSupplyId
            })
            .setResolve(function (response) {
                if(response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, self);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    reloadSendingChannel: function (component) {
        let contractSelectionComponent = component.find("sendingChannelSelection");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadAddressList();
            } else {
                contractSelectionComponent.reloadAddressList();
            }
        }
    },
    updateSearchSupplyCondition: function(component){
        let andConditions = component.get("v.andConditions");
        andConditions.splice(0, andConditions.length);
        let accountId = component.get("v.accountId");
        let accountCondition = "Account__c ='" + accountId + "'";
        andConditions.push(accountCondition);

        component.set("v.andConditions",andConditions);
    }
});