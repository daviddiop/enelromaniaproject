({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        component.set("v.dossierId", dossierId);

        helper.initialize(component, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.ExternalProduct"));
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
    },
    saveDraft: function (component, event, helper) {
        component.set("v.savingWizard", false);
        helper.saveOpportunity(component, helper, "Negotiation/Review", helper.redirectToDossier);
    },
    save: function (component, event, helper) {
        component.set("v.savingWizard", true);
        helper.saveOpportunity(component, helper, "Quoted", helper.redirectToDossier);
    },
    onSaveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        if (cancelReason) {
            component.set("v.savingWizard", false);
            helper.cancelEverything(component, helper, cancelReason, detailsReason);
        }
    },
    nextStep: function (component, event, helper) {
        helper.btnNextClickHandler(component, helper);
    },
    editStep: function (component, event, helper) {
        // this is the same as the step for this setting

        let stepToEditTitle = event.getSource().getLocalId();
        let previousStep = helper.getPreviousStep(component, helper, stepToEditTitle);

        console.log(component.get("v.currentFlow"));
        console.log(stepToEditTitle);
        console.log(previousStep.Title);
        console.log(helper.getIndexFromStepTitle(component, stepToEditTitle));
        console.log(component.get("v.stepIndexesMap").SET_TYPE_OF_WORK);

        component.set("v.step", stepToEditTitle);
        switch (stepToEditTitle) {
            case component.get("v.steps").SET_TYPE_OF_WORK.Title:
                break;
            case component.get("v.steps").FINISH.Title:
                break;
        }
    },
    handleOliRemove: function (component, event, helper) {
        helper.deleteOlis(component);
    },
    handleSupplyToTerminateSelection: function (component, event, helper) {
        let selectedSupply = event.getParam("selectedObject");
        helper.handleVasSelection(component, helper, selectedSupply);
    },
    handleTypeOfWorkSelection: function (component, event, helper) {
        let currentFlow = component.find('typeOfWork').get('v.value');
        helper.setCurrentFlow(component, helper, currentFlow);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        // values from UI
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        helper.handleOriginChannelSelection(component, helper, originSelected, channelSelected);
    },
    validateDeliveryDate: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedDate = new Date(event.getParam('value'));
        selectedDate = selectedDate.setUTCHours(0,0,0,0);
        let today = new Date();
        today = today.setUTCHours(0,0,0,0);

        if (selectedDate < today){
            ntfSvc.error(ntfLib, $A.get('$Label.c.deliveryDateCannotBeInPast'));
            component.find("deliveryDate").set("v.value", "");
        }
    },
    handleSupplyToActivateSelection: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedCommoditySupply = event.getParam("selectedObject");
        helper.checkCommoditySupply(component, helper, selectedCommoditySupply.Id);
    },
    handleNewOsi: function (component, event, helper) {
        const osiId = event.getParam("opportunityServiceItemId");
        helper.afterNewOsi(component, helper, osiId);
    },
    handleOsiDelete: function (component, event, helper) {
        let osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        let items = [];
        for (let i = 0; i < osiList.length; i++) {
            if (osiList[i].Id !== osiId) {
                items.push(osiList[i]);
            }
        }
        component.set("v.opportunityServiceItems", items);
        component.set(
            "v.osiTableView",
            items.length > component.get("v.tileNumber")
        );
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedPointId", "");
        component.set("v.searchedPointCode", "");
        //component.find("pointSelection").resetBox();
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(component, helper, "Proposal/Price Quote", helper.openProductSelection, "");
    },
    handleSavedSendingChannelSelected: function(component, event, helper) {
        console.log('sending channel is selected');
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
        component.set("v.dontProcess", event.getParam("dontProcess"));
        if (!component.get("v.savingWizard")) {
            helper.saveOpportunity(component, helper, "Negotiation/Review", helper.redirectToDossier);
        }
        if (component.get("v.savingWizard")) {
            if (component.get("v.dontProcess")) {
                ntfSvc.error(ntfLib, "Do Not Process");
                $A.get("e.force:refreshView").fire();
                return;
            }
            helper.saveOpportunity(component, helper, "Closed Won", helper.redirectToDossier);
        }
    },
    handleSavedSendingChannel: function(component, event, helper) {
        console.log('handleSavedSendingChannel');
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            if (event.getParam("divisionId")) {
                component.set("v.companyDivisionId", event.getParam("divisionId"));
                component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));
            }
        } catch (err) {
            console.error('Error on aura:PartnerServiceWizard :', err);
        }
    }
})