({
    // Start of the steps api methods.
    stepsInit: function (component, helper) {
        // Fills the Step Indexes Map
        let stepIndexes = {};

        let i = 0;
        for (const stepName in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(stepName)) {
                stepIndexes[stepName] = i;
                i++;
            }
        }

        component.set("v.stepIndexesMap", stepIndexes);
    },
    isPartOfCurrentFlow: function (component, helper, step) {
        // Checks whether a given step is a part of the active flow

        let flowTitle = component.get("v.currentFlow");

        // If there is only one default flow, every step is included
        if (flowTitle === undefined) {
            return true;
        }

        let result = false;
        for (let i = 0; i <= Object.keys(step.Flows).length; i++) {
            let iFlowName = step.Flows[i];
            if (iFlowName === 'ALL') {
                result = true;
            } else if (component.get("v.flows").hasOwnProperty(iFlowName)) {
                let stepFlowTitle = component.get("v.flows")[iFlowName];
                if (stepFlowTitle === flowTitle) {
                    result = true;
                }
            }
        }
        return result;
    },
    setCurrentStep: function (component, helper, stepTitle) {
        // Set the current step
        let currentStepIndex = helper.getIndexFromStepTitle(component, stepTitle);
        component.set("v.step", stepTitle);
        component.set("v.currentStepIndex", currentStepIndex);

        // Set the previous step
        let previousStepTitle = helper.getPreviousStep(component, helper, stepTitle).Title;
        let previousStepIndex = helper.getIndexFromStepTitle(component, previousStepTitle);
        component.set("v.previousStepIndex", previousStepIndex);
    },
    getStepFromIndex: function (component, stepIndex) {
        let index = 0;
        for (const iStepTitle in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(iStepTitle)) {
                if (index === stepIndex) {
                    return component.get("v.steps")[iStepTitle];
                }
                index++;
            }
        }
        return null;
    },
    getIndexFromStepTitle: function (component, stepTitle) {
        // Returns the index of a step by its title
        let stepIndex = 0;
        for (const iStepTitle in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(iStepTitle)) {
                if (iStepTitle === stepTitle) {
                    return stepIndex;
                }
                stepIndex++;
            }
        }
        return null;
    },
    getNextStep: function (component, helper, stepTitle) {
        // If the current step is null or empty, use the first step instead of it, if needed
        let currentStepTitle = component.get("v.step");
        if (helper.isNullOrEmpty(currentStepTitle)) {
            currentStepTitle = helper.getFirstStep(component, helper).Title;
        }

        // Start searching for the next, with the given or current step
        if (helper.isNullOrEmpty(stepTitle)) {
            stepTitle = currentStepTitle;
            console.warn('next step based on the current');
        }

        let stepIndex = this.getIndexFromStepTitle(component, stepTitle);
        let nextStep;
        let stepsCount = Object.keys(component.get("v.steps")).length;
        if ((stepIndex >= stepsCount - 1)) {
            return this.getStepFromIndex(component, stepsCount - 1);
        }
        for (let i = stepIndex + 1; i <= stepsCount; i++) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (helper.isPartOfCurrentFlow(component, helper, iStep)) {
                    nextStep = iStep;
                    break;
                }
            }
        }
        return nextStep;
    },
    getStepFromTitle: function (component, helper, stepTitle) {
        // Returns the step by its title
        let step = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (iStep.Title === stepTitle) {
                    step = iStep;
                    break;
                }
            }
        }
        return step;
    },
    getCurrentStep: function (component, helper) {
        // Returns the current step
        let currentStepTitle = component.get("v.step");
        if (helper.isNullOrEmpty(currentStepTitle)) {
            console.error('current step is not set');
            return;
        }
        return helper.getStepFromTitle(component, helper, currentStepTitle);
    },
    getFirstStep: function (component, helper) {
        // Returns the first step
        let firstStep = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (helper.isPartOfCurrentFlow(component, helper, iStep)) {
                    firstStep = iStep;
                    break;
                }
            }
        }
        return firstStep;
    },
    getPreviousStep: function (component, helper, stepTitle) {
        // Returns the previous step
        let stepIndex = this.getIndexFromStepTitle(component, stepTitle);
        let previousStep;

        if ((stepIndex === 0) || (stepIndex - 1) === 0) {
            return this.getStepFromIndex(component, 0);
        }

        for (let i = stepIndex - 1; i >= 0; i--) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (helper.isPartOfCurrentFlow(component, helper, iStep)) {
                    previousStep = iStep;
                    break;
                }
            }
        }
        return previousStep;
    },
    goToNextStep: function (component, helper) {
        // Activates the next step
        let currentStepTitle = component.get("v.step");
        let nextStep = helper.getNextStep(component, helper, currentStepTitle);

        helper.setCurrentStep(component, helper, nextStep.Title);
        helper.onNewStep(component, helper);
    },
    // End of the steps api methods.
    onNewStep: function (component, helper) {
        // An event after a step has been selected, a nice place to do after selection checks, jumps etc
        let currentStepTitle = component.get("v.step");
        switch (currentStepTitle) {
            case component.get("v.steps").SET_COMPANY_DIVISION.Title:
            case component.get("v.steps").SHOW_SCRIPT_MANAGEMENT.Title:
                helper.btnNextClickHandler(component, helper);
                break;
        }
    },
    isInstantDelivery: function (component) {
        return component.get("v.currentFlow") === component.get("v.flows").INSTANT_DELIVERY;
    },
    isDispatchedWithInstallation: function (component) {
        return component.get("v.currentFlow") === component.get("v.flows").DISPATCHED_WITH_INSTALLATION;
    },
    isDispatchedWithoutInstallation: function (component) {
        return component.get("v.currentFlow") === component.get("v.flows").DISPATCHED_WITHOUT_INSTALLATION;
    },
    isCancellation: function (component) {
        return component.get("v.currentFlow") === component.get("v.flows").CANCELLATION;
    },
    initialize: function (component, helper) {
        helper.stepsInit(component, helper);

        let self = this;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        let myPageRef = component.get("v.pageReference");
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let accountId = myPageRef.state.c__accountId;
        let templateId = myPageRef.state.c__templateId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        let caseToTerminateId = myPageRef.state.c__caseId;

        helper.savePersistentAttribute(component, helper,"genericRequestId", genericRequestId);
        helper.savePersistentAttribute(component, helper,"templateId", templateId);
        helper.savePersistentAttribute(component, helper,"caseToTerminateId", caseToTerminateId);

        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId,
                "caseToTerminateId": caseToTerminateId,
            })
            .setResolve(function (response) {

                if (response.opportunityId && !opportunityId) {
                    // First start of the wizard
                    // Refresh with all the necessary params
                    helper.cleanPersistentAttributes();
                    self.updateUrl(component, accountId, response.opportunityId, response.dossierId, templateId, caseToTerminateId);

                    return;
                }

                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunity", response.opportunity);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
                component.set("v.useBit2WinCart", response.useBit2WinCart);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.existingVasStringified", response.existingVas);
                component.set('v.originSelected', response.opportunity.Origin__c);
                component.set('v.channelSelected', response.opportunity.Channel__c);
                component.set("v.commodity", response.commodity);
                component.set("v.commodityProductStringified", response.commodityProduct);
                component.set("v.account", response.account);
                component.set("v.accountId", response.accountId);
                component.set("v.numberOfInstallmentsOptions", response.numberOfInstallmentsOptions);
                component.set("v.salesman", response.salesman);
                component.set("v.salesUnit", response.salesUnit);
                component.set("v.salesUnitId", response.salesUnit);
                component.set("v.user", response.user);
                component.set("v.userDepartment", response.userDepartment);
                if(component.get("v.userDepartment")){
                    component.set("v.salesUnitId", component.get("v.userDepartment"));
                    component.set("v.salesUnit", component.get("v.userDepartment"));
                    component.set("v.showUserDepartment", true);
                }

                helper.setCurrentFlow(component, helper, response.currentFlow);
                helper.checkTheCommodityProductName(component, helper, response.commodityProduct);
                helper.selectCompanyDivision(component, helper, response.opportunityCompanyDivisionId);

                // This is to determine whether the selected product is a bundle and check the selected product exceeded the thresholds
                if (!helper.isNullOrEmpty(response.opportunityLineItems) && (helper.isInstantDelivery(component) || helper.isDispatchedWithoutInstallation(component) || helper.isDispatchedWithInstallation(component))) {
                    helper.checkTheProductName(component, helper, response.opportunityLineItems[0]);
                    helper.checkOliThresholds(component, helper, response.opportunityLineItems[0], response.dossierId, response.opportunityId);

                    // Get total quantity of selected products
                    let totalQuantity = 0;
                    for (let i = 0; i< response.opportunityLineItems.length; i++){
                        let oli = response.opportunityLineItems[i];
                        if (oli.Quantity){
                            totalQuantity += oli.Quantity;
                        }
                    }

                    if (totalQuantity === 0){
                        totalQuantity = 1;
                    }

                    // Set the quantity to Number of Pieces field
                    component.find("numberOfPieces").set("v.value", totalQuantity);
                }

                // Set contract fields
                if (!helper.isNullOrEmpty(response.contract)) {
                    component.set("v.contractId", response.contract.Id);
                    component.set("v.contractType", response.contract.ContractType__c);
                    component.set("v.assetId", response.assetId);
                }

                if(response.salesUnitAccount && !component.get("v.salesUnit")){
                    component.set("v.salesUnit", response.salesUnitAccount);
                    if(component.get("v.salesUnit").SalesDepartment__c === component.get("v.channelSelected")) {
                        let salesUnitAccountId = component.get("v.salesUnit").Id;
                        component.set("v.salesUnitId", salesUnitAccountId);
                    }
                }

                if(component.get("v.channelSelected") !== "Indirect Sales" && !component.get("v.companySignedId")){
                    if(component.get("v.channelSelected") === "Direct Sales (KAM)" && !component.get("v.salesUnit")){
                        component.set("v.companySignedId", "");
                    }
                    else{
                        let userId = component.get("v.user").Id;
                        component.set("v.companySignedId", userId);
                    }
                }

                if (!helper.isNullOrEmpty(response.caseToTerminate)){
                    helper.initializeCaseTerminationSubprocess(component, helper, response.caseToTerminate);
                }

                helper.setFiltrationParams(component, helper);
                helper.updateSearchConditions(component, helper);
                helper.restoreCurrentStep(component, helper);
                helper.restoreLocalState(component, helper);

                if (component.find("deliveryDate")){
                    component.find("deliveryDate").set("v.value", (new Date()).toISOString());
                }

                // Loading finished
                helper.hideSpinner(component);

                const htmlElement = helper.getComponent(component, helper, component.get("v.step"));
                if (htmlElement) {
                    setTimeout(()=>{
                        htmlElement.getElement().scrollIntoView();
                    }, 3500);
                }
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    initializeCaseTerminationSubprocess: function(component, helper, caseToTerminate) {
        // Process has been started from Cancellation button on a Case
        // and has to be redirected to appropriate opportunity and account
        // /lightning/cmp/c__MRO_LCP_ExternalProductWizard?c__accountId={!Case.AccountId}&c__templateId=a0w0Q0000013mv4QAA&c__caseId={!Case.Id}&c__currentStepTitle=SET_SUPPLY_TO_TERMINATE

        let serviceSupplyId = caseToTerminate.Supply__c;
        let accountId = caseToTerminate.AccountId;
        let dossierId = caseToTerminate.Dossier__c;
        let opportunityId = caseToTerminate.Dossier__r.Opportunity__c;
        let serviceSupply = helper.toPlainObject(caseToTerminate.Supply__r);

        helper.savePersistentAttribute(component, helper, "selectedToTerminateSupplyId", serviceSupplyId);
        helper.savePersistentAttribute(component, helper, "selectedToTerminateSupplyStringified", JSON.stringify(serviceSupply));

        const flows = helper.toPlainObject(component.get("v.flows"));
        helper.setCurrentFlow(component, helper, flows.CANCELLATION);
        helper.handleVasSelection(component, helper, serviceSupply);
    },
    restoreCurrentStep: function (component, helper) {
        let supplyToActivateId = component.get("v.commoditySupplyId");
        let contractId = component.get("v.contractId");
        let oliList = component.get("v.opportunityLineItems");
        let currentFlow = component.get("v.currentFlow");

        let currentStepTitle = component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title;

        if (!helper.isNullOrEmpty(oliList)) {
            currentStepTitle = component.get("v.steps").SET_PRODUCTS.Title;
        } else if (!helper.isNullOrEmpty(supplyToActivateId)) {
            currentStepTitle = component.get("v.steps").SET_SUPPLY_TO_ACTIVATE.Title;
        } else if (!helper.isNullOrEmpty(currentFlow)) {
            currentStepTitle = component.get("v.steps").SET_TYPE_OF_WORK.Title;
        } else if (component.get("v.originSelected") && component.get("v.channelSelected")) {
            currentStepTitle = component.get("v.steps").SET_TYPE_OF_WORK.Title;
        }

        // Save the current step
        helper.setCurrentStep(component, helper, currentStepTitle);

        // Check whether to remain on current step
        if (helper.isNullOrEmpty(currentFlow) ||
            currentStepTitle === helper.getFirstStep(component, helper).Title ||
            currentStepTitle === component.get("v.steps").SET_PRODUCTS.Title) {
            return;
        }

        // Go to the next step
        helper.goToNextStep(component, helper);
    },
    selectCompanyDivision: function (component, helper, companyDivisionId) {
        if (component.get("v.currentFlow") === "") {
            return;
        }

        // Check the input
        if (!companyDivisionId) {
            console.log("no companyDivisionId has been provided");
            return;
        }

        // Set the company division id
        component.set("v.companyDivisionId", companyDivisionId);

        // Select the company division in its UI
        let companyDivisionComponent = component.find("companyDivision");
        if (!companyDivisionComponent) {
            console.error("couldn't find company division component");
            return;
        }
        companyDivisionComponent.setCompanyDivision();
    },
    checkCommoditySupply: function (component, helper, commoditySupplyId) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        const dossierId = component.get('v.dossierId');
        const contractId = helper.loadPersistentAttribute(component, helper, 'contractId');
        const currentFlow = component.get('v.currentFlow');
        const opportunityId = component.get('v.opportunityId');

        helper.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("checkCommoditySupply")
            .setInput({
                supplyId: commoditySupplyId,
                dossierId: dossierId,
                contractId: contractId,
                currentFlow: currentFlow,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                // Set component's attributes
                let commoditySupply = response.commoditySupply;

                helper.savePersistentAttribute(component, helper, "commoditySupplyId", commoditySupply.Id);
                helper.savePersistentAttribute(component, helper, "commodity", commoditySupply.RecordType.Name);
                helper.savePersistentAttribute(component, helper, "contractType", response.contractType);
                helper.savePersistentAttribute(component, helper, "commodityProductStringified", response.commodityProduct);

                // Check if a bundle has been selected
                helper.checkTheCommodityProductName(component, helper, response.commodityProduct);

                // Set current company division on UI element
                helper.selectCompanyDivision(component, helper, commoditySupply.CompanyDivision__r.Id);

                component.set("v.showNewOsi", true);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    afterNewOsi: function (component, helper, osiId) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("afterNewOsi")
            .setInput({
                "osiId": osiId,
                "opportunityId": component.get("v.opportunityId"),
                "accountId": component.get("v.accountId"),
                "commoditySupplyId": component.get("v.commoditySupplyId")
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;

                osiList.push(newOsi);
                if (osiList.length === 1) {
                    helper.updateCompanyDivisionOnOpportunity(component, helper);
                }

                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                component.set("v.opportunityServiceItemId", osiId);
            })
            .setReject(function (response) {
                helper.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateSearchConditions: function (component, helper) {
        let supplyToActivateSearchConditions = [];
        supplyToActivateSearchConditions.push(`(Account__c='${component.get('v.accountId')}')`);
        supplyToActivateSearchConditions.push(`(RecordType.DeveloperName='Electric' OR RecordType.DeveloperName='Gas')`);
        component.set("v.supplyToActivateSearchConditions", supplyToActivateSearchConditions);

        let supplyToTerminateSearchConditions = [];
        supplyToTerminateSearchConditions.push(`(Account__c='${component.get('v.accountId')}')`);
        supplyToTerminateSearchConditions.push(`(RecordType.DeveloperName='Service')`);
        component.set("v.existingSupplySearchConditions", supplyToTerminateSearchConditions);
    },
    updateCurrentFlow: function (component, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        const opportunityId = component.get("v.opportunityId");
        const currentFlow = component.get("v.currentFlow");
        const dossierId = component.get("v.dossierId");

        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateCurrentFlow')
            .setInput({
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "currentFlow": currentFlow,
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

                if (!helper.isNullOrEmpty(response.contractId)){
                    helper.savePersistentAttribute(component, helper, "contractId", response.contractId);
                    helper.savePersistentAttribute(component, helper, "contractName", response.contractName);
                    helper.savePersistentAttribute(component, helper, "assetId", response.assetId);
                    helper.setContractName(component, helper);
                }

                // Reload with clean cache on first start
                $A.get('e.force:refreshView').fire();
            })
            .setReject(function (error) {
                helper.hideSpinner(component);

                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateDates: function (component, helper, activationDate, terminationDate) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const osiId = component.get("v.opportunityServiceItemId");

        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateDates')
            .setInput({
                "osiId": osiId,
                "activationDate": activationDate,
                "terminationDate": terminationDate
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    fillStepIndexes: function (component, helper) {
        let stepIndexes = {};

        let i = 0;
        for (const stepName in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(stepName)) {
                stepIndexes[stepName] = i;
                i++;
            }
        }

        component.set("v.stepIndexesMap", stepIndexes);
    },
    setFiltrationParams: function (component, helper) {
        let existingSupplyColumns = [
            {key: 'RecordType.Name', label: 'Type'},
            {key: 'Name', label: 'Supply'},
            {key: 'Status__c', label: 'Status'},
            {key: 'ContractAccount__r.Name', label: 'Contract Account'},
            {key: 'Contract__r.ContractNumber', label: 'Contract'},
            {key: 'ActivationDate__c', label: 'Activation Date'},
            {key: 'TerminationDate__c', label: 'Termination Date'},
            {key: 'Product__r.Name', label: 'Product'}
        ];

        component.set("v.existingSupplyColumns", existingSupplyColumns);
    },
    setCurrentFlow: function (component, helper, currentFlow) {
        let typeOfWork = $A.get('$Label.c.SelectTypeOfWork');
        component.set("v.currentFlow", currentFlow);
        let flows = helper.toPlainObject(component.get("v.flows"));
        switch (currentFlow) {
            case flows.INSTANT_DELIVERY:
                typeOfWork = $A.get('$Label.c.HardVasInstantDelivery');
                break;
            case flows.DISPATCHED_WITH_INSTALLATION:
                typeOfWork = $A.get('$Label.c.HardVasDispatchedWithoutInstallation');
                break;
            case flows.DISPATCHED_WITHOUT_INSTALLATION:
                typeOfWork = $A.get('$Label.c.HardVasDispatchedWithInstallation');
                break;
            case flows.CANCELLATION:
                typeOfWork = $A.get('$Label.c.HardVasCancellation');
                break;
            default:
                component.set("v.currentFlow", '');
        }
        component.find("typeOfWork").set("v.value", typeOfWork);
    },
    setCompanyDivisionFromSupply: function (component, helper, companyDivisionId) {
        let companyDivisionComponent = component.find("companyDivision");
        let availableCompanyDivisions = companyDivisionComponent.getCompanyDivisions();
        let result = false;

        availableCompanyDivisions.forEach(companyDivision => {
            if (companyDivision.value === companyDivisionId) {
                component.set("v.companyDivisionId", companyDivision.value);
                companyDivisionComponent.setCompanyDivision();
                result = true;
            }
        });
        return result;
    },
    isNullOrEmpty: function (variable) {
        return (variable === undefined || variable === null || variable.length === 0);
    },
    btnNextClickHandler: function (component, helper) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let currentStepTitle = component.get("v.step");
        let nextStep = helper.getNextStep(component, helper, currentStepTitle);

        switch (currentStepTitle) {
            case component.get("v.steps").SET_HARD_VAS_DETAILS.Title:
                const numberOfInstallments = component.find("numberOfInstallments").get("v.value");
                if (helper.isNullOrEmpty(numberOfInstallments)){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectNumberOfInstallments"));
                    return;
                } else {
                    let threshold = parseInt(component.get('v.threshold'));
                    let valueToCompare = parseInt(component.find("numberOfPieces").get("v.value")) + parseInt(component.get('v.unpaidSupplyListCount'));
                    console.log('valueToCompare-> ' + valueToCompare);
                    console.log('threshold-> ' + threshold);
                    if (valueToCompare > threshold){
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ErrorTheProductThresholdIsExceeded"));
                        return;
                    }
                }

                break;
            case component.get("v.steps").BENEFICIARY_INFORMATION.Title:
                let beneficiaryInformationElement = component.find("beneficiaryInformationElement");
                if (!beneficiaryInformationElement){
                    break;
                }

                if (beneficiaryInformationElement.isEmpty()){
                    beneficiaryInformationElement.copyBeneficiaryInformation(component.get("v.accountId"));
                    break;
                }

                let validationResult = beneficiaryInformationElement.validateFields();
                if (validationResult === false) {
                    return;
                }

                if (validationResult === true){
                    beneficiaryInformationElement.saveBeneficiaryInformation();
                }
                break;
            case component.get("v.steps").SET_COMPANY_DIVISION.Title:
                break;
            case component.get("v.steps").SET_TYPE_OF_WORK.Title:
                if (component.get("v.currentFlow") === '') {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SetTypeOfWorkErrorMsg"));
                    return;
                }

                helper.updateCurrentFlow(component, helper);
                helper.updateSearchConditions(component, helper);
                break;
            case component.get("v.steps").SET_PRODUCTS.Title:
                let prdList = component.get("v.opportunityLineItems");
                if (helper.isNullOrEmpty(prdList)) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct"));
                    return;
                }
                let oli = prdList[0];

                if (component.get("v.isProductBundle") === true) {
                    console.error("The product is a bundle");
                    ntfSvc.error(ntfLib, $A.get("$Label.c.BundleProductErrorMsg"));
                    return;
                }

                if (!helper.isNullOrEmpty(component.get("v.existingVasStringified"))) {
                    console.error("There is already a service for such commodity and product");
                    ntfSvc.error(ntfLib, `${$A.get("$Label.c.ErrorTheProductIsAlreadyActivated")} - Supply: ${component.get("v.existingVasStringified").Name}`);
                    return;
                }

                let numberOfInstallmentsOptions = component.get("v.numberOfInstallmentsOptions");
                if (helper.isNullOrEmpty(numberOfInstallmentsOptions)){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.NoInstallmentOptionsForSelectedProduct"));
                    return;
                }

                if (component.get("v.isExceedThreshold") === true) {
                    console.error("The order limit for this product has been exceeded.");
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ErrorTheProductThresholdIsExceeded"));
                    return;
                }

                helper.processSelectedOli(component, oli);
                break;
            case component.get("v.steps").SET_CONTRACT.Title:
                if (!helper.updateContractInfo(component, helper)){
                    return;
                }
                break;
            case component.get("v.steps").SET_SENDING_CHANNEL.Title:
                helper.saveSendingChannel(component, helper);
                break;
            case component.get("v.steps").SET_TERMINATION_DETAILS.Title:
                let terminationReason = component.find("terminationReason").get("v.value");
                if (helper.isNullOrEmpty(terminationReason)) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectTerminationReason"));
                    return;
                }
                break;
        }

        // Set up new state
        helper.goToNextStep(component, helper);
    },
    setContractName: function (component, helper) {
        let contractSelectionElement = component.find("contractSelectionComponent");
        if (!contractSelectionElement){
            return null;
        }
        let contractName = helper.loadPersistentAttribute(component, helper,"contractName");
        //contractSelectionElement.setContractName(contractName);
    },
    getContractFields: function (component) {
        let contractSelectionElement = component.find("contractSelectionComponent");
        if (!contractSelectionElement){
            return null;
        }
        return JSON.parse(JSON.stringify(contractSelectionElement.getNewContractDetailsWithIsoDates()));
    },
    getComponent: function (component, helper, inputName) {
        return Array.isArray(component.find(inputName)) ? component.find(inputName)[0] : component.find(inputName);
    },
    getInputFieldValue: function (component, helper, fieldId) {
        return Array.isArray(component.find(fieldId)) ? component.find(fieldId)[0].get("v.value") : component.find(fieldId).get("v.value");
    },
    cancelEverything: function (component, helper, cancelReason, detailsReason) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let dossierId = component.get("v.dossierId");
        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId");
        let assetId = component.get("v.assetId");

        helper.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId,
                opportunityId: opportunityId,
                contractId: contractId,
                assetId: assetId,
                cancelReason: cancelReason,
                detailsReason: detailsReason
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
                helper.redirectToDossier(component, helper);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleOriginChannelSelection: function(component, helper, originSelected, channelSelected){
        if (!channelSelected || !originSelected) {
            return;
        }

        // activate type of work workaround
        let currentStepIndex = component.get("v.currentStepIndex");
        let setTypeOfWorkStepIndex = component.get("v.stepIndexesMap").SET_TYPE_OF_WORK;

        if (currentStepIndex < setTypeOfWorkStepIndex){
            helper.goToNextStep(component, helper);
        }

        // current values from opportunity
        let currentOrigin = component.get('v.originSelected');
        let currentChannel = component.get('v.channelSelected');

        if (channelSelected === currentChannel && originSelected === currentOrigin) {
            // no changes
            helper.restoreCurrentStep(component, helper);
        } else {
            // one of the values has been changed
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);

            helper.setChannelAndOrigin(component, helper);
        }

        if (!currentChannel || !currentOrigin) {
            // setting for the first time
            component.set("v.step", helper.getFirstStep(component, helper).Title);
            helper.btnNextClickHandler(component, helper);
        }
    },
    setChannelAndOrigin: function (component, helper) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');

        if (!opportunity || !origin || !channel) {
            console.log(`prevented illegal call to setChannelAndOrigin`);
            return;
        }

        helper.showSpinner(component);

        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId: dossierId,
                origin: origin,
                channel: channel
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component, helper) {
        const mroSendingChannelSelection = helper.getComponent(component, helper, "sendingChannelSelection");
        mroSendingChannelSelection.saveSendingChannel();
    },
    saveOpportunity: function (component, helper, stage, callback, cancelReason, detailsReason) {
        let self = this;
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let commodity = component.get("v.commodity");
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let dossierId = component.get("v.dossierId");
        let supplyToActivateActivationDate = "";
        let supplyToActivateTerminationDate = "";
        let terminationReason = "";
        let numberOfInstallments;
        let numberOfPieces;
        let deliveryDate;
        let productId = null;
        let salesUnitId = helper.isNullOrEmpty(component.get("v.salesUnitId")) ? component.get("v.salesUnitId") : null;
        let companySignedBy = helper.isNullOrEmpty(component.get("v.companySignedId")) ? component.get("v.companySignedId") : null;

        let selectedToTerminateSupplyId = component.get("v.selectedToTerminateSupplyId");

        if (helper.isCancellation(component)) {
            terminationReason = component.find("terminationReason").get("v.value");
        }
        if (helper.isDispatchedWithInstallation(component) || helper.isDispatchedWithoutInstallation(component)) {
            let contractFields = helper.getContractFields(component);
            // Calculate the End Date
            supplyToActivateActivationDate = new Date(contractFields.customerSignedDate);
            supplyToActivateTerminationDate = new Date(contractFields.customerSignedDate);
            let activationMonth = Number.parseInt(supplyToActivateActivationDate.getMonth());
            let terminatesIn = Number.parseInt(contractFields.contractTerm);
            supplyToActivateTerminationDate = new Date(supplyToActivateTerminationDate.setMonth(activationMonth + terminatesIn));
        }

        if (!helper.isCancellation(component)) {
            numberOfInstallments = component.find("numberOfInstallments").get("v.value");
            numberOfPieces = component.find("numberOfPieces").get("v.value");
            deliveryDate = component.find("deliveryDate").get("v.value");

            let oliList = component.get("v.opportunityLineItems");
            if (!helper.isNullOrEmpty(oliList)) {
                productId = oliList[0].Product2.Id;
            }
        }

        if (!helper.isNullOrEmpty(component.get("v.commoditySupplyStringified"))) {
            let selectedToActivateSupply = JSON.parse(component.get("v.commoditySupplyStringified"));
            commodity = selectedToActivateSupply.RecordType.Name;
        }

        const currentFlow = component.get("v.currentFlow");
        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "currentFlow": currentFlow,
                "dossierId": dossierId,
                "supplyToActivateActivationDate": supplyToActivateActivationDate,
                "supplyToActivateTerminationDate": supplyToActivateTerminationDate,
                "selectedCommodity": commodity,
                "stage": stage,
                "numberOfInstallments": numberOfInstallments,
                "salesUnitId": salesUnitId,
                "companySignedBy": companySignedBy,
                "terminationReason": terminationReason,
                "selectedToTerminateSupplyId": selectedToTerminateSupplyId,
                "numberOfPieces": numberOfPieces,
                "deliveryDate": deliveryDate,
                "productId": productId,
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                if (response.warning) {
                    ntfSvc.warn(ntfLib, response.warningMsg);
                }

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                helper.hideSpinner(component);

                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateSupply: function (component, supplyId, status, callback) {
        let self = this;
        self.showSpinner(component);
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateSupply')
            .setInput({
                "supplyId": supplyId,
                "status": status,
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (callback && typeof callback === "function") {
                    callback(response);
                }
            })
            .setReject(function (error) {
                self.hideSpinner(component);

                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, opportunityId, dossierId, templateId, caseToTerminateId) {
        let self = this;
        const {isConsoleNavigation} = component.find("workspace");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ExternalProductWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId,
                "c__dossierId": dossierId,
                "c__templateId": templateId,
                "c__caseId": caseToTerminateId
            }
        };

        let redirector = self.getRedirector(component, self);
        redirector(component, self, pageReference);
    },
    getRedirector: function(component, helper) {
            const caseToTerminateId = helper.loadPersistentAttribute(component, helper, "caseToTerminateId");
            if (!helper.isNullOrEmpty(caseToTerminateId)){
                return helper.customRedirect;
            }else{
                return helper.redirect;
            }
    },
    handleVasSelection: function(component, helper, selectedSupply){
        let selectedToTerminateSupplyId = selectedSupply.Id;
        component.set("v.companyDivisionId", selectedSupply.CompanyDivision__c);
        component.set("v.selectedToTerminateSupplyId", selectedToTerminateSupplyId);
        component.set("v.selectedToTerminateSupplyStringified", JSON.stringify(selectedSupply));
        component.set("v.commodityProductStringified", "");

        if (helper.isCancellation(component)){
            helper.checkTheProductName(component, helper, selectedSupply.Product__r);
        }

        helper.updateCompanyDivisionOnOpportunity(component, helper);
    },
    deleteOlis: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOlis")
            .setInput({
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.isDeleted) {
                    component.set("v.opportunityLineItems", []);
                }
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        let pageReference;

        if (useBit2WinCart) {
            let osiList = component.get("v.opportunityServiceItems");
            let isGas = false;
            let isElectric = false;
            let isService = false;
            for (let osi of osiList) {
                if (osi.RecordType.DeveloperName === 'Gas') {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === 'Service') {
                    isService = true;
                } else if (osi.RecordType.DeveloperName === 'Electric') {
                    isElectric = true;
                }
            }
            let productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';
            }
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__currentFlow": component.get("v.currentFlow"),
                    "c__currentStepTitle": component.get("v.step"),
                    "c__templateId": component.get("v.templateId"),
                    "c__productType": productType
                }
            };
        } else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, helper, pageReference);
    },
    redirectToDossier: function (component, helper) {
        helper.cleanPersistentAttributes();
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'Dossier__c',
                actionName: 'view'
            }
        };

        let redirector = helper.getRedirector(component, helper);
        redirector(component, helper, pageReference);
    },
    checkTheCommodityProductName: function (component, helper, product) {
        // reset the previously chosen product data
        component.set("v.isCommodityProductBundle", false);

        // check if the product is set and whether it can be a bundle or need additional information fields
        if (product === undefined) {
            return;
        }
        const productName = product.Name;
        if (productName.length < 5) {
            return;
        }

        // determine whether the product is a bundle or not
        const isBundleProduct = productName.includes('Bundle');
        component.set("v.isCommodityProductBundle", isBundleProduct);
    },
    checkTheProductName: function (component, helper, product) {
        // reset the previously chosen product data
        component.set("v.isProductBundle", false);
        component.set("v.isExtendedProduct", false);

        // check if the product is set and whether it can be a bundle or need additional information fields
        if (product === undefined) {
            return;
        }
        const productName = product.Name;
        if (productName.length < 5) {
            return;
        }

        // determine whether the product is a bundle or not
        const isBundleProduct = productName.includes('Bundle');
        component.set("v.isProductBundle", isBundleProduct);

        // determine whether the product can have additional information or not
        const extendedProduct = (productName.substr(productName.length - 4).toUpperCase() === "PLUS" || productName.substr(productName.length - 1) === "+");
        component.set("v.isExtendedProduct", extendedProduct);
    },
    checkOliThresholds: function (component, helper, oli, dossierId, opportunityId) {
        component.set("v.isExceedThreshold", false);
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        if (oli === undefined) {
            return;
        }
        component.find('apexService').builder()
            .setMethod("checkOliThresholds")
            .setInput({
                "opportunityLineItemId": oli.Id,
                "dossierId": dossierId,
                'opportunityId': opportunityId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.isExceedThreshold', response.isExceedThreshold);
                component.set('v.threshold', response.threshold);
                console.log('threshold-> ' + response.threshold);
                console.log('unpaidSupplyListCount-> ' + response.unpaidSupplyListCount);
                component.set('v.unpaidSupplyListCount', response.unpaidSupplyListCount);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction()
    },
    processSelectedOli: function (component, oli, callback) {
        let self = this;
        self.showSpinner(component);
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod("processSelectedOli")
            .setInput({
                "opportunityLineItemId": oli.Id,
                "opportunityId": component.get("v.opportunityId"),
                "dossierId": component.get("v.dossierId")
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component);

                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction()
    },
    customRedirect: function (component, helper, pageReference) {
        const navService = component.find("navService");
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubTab, openTab} = component.find("workspace");

        isConsoleNavigation()
          .then(function (response) {
              if (response === true) {
                  getEnclosingTabId().then(function (enclosingTabId) {
                      openTab({
                          pageReference: pageReference,
                          focus: true
                      }).then(function () {
                          closeTab({
                              tabId: enclosingTabId
                          });
                      }).catch(function (error) {
                          console.error(error);
                      });
                  });
              } else {
                  let navPromise = navService.navigate(pageReference, true);
                  navPromise && navPromise.then(() => {});
              }
          }).catch(function (error) {
            console.error(error);
        });
    },
    redirect: function (component, helper, pageReference) {
        const navService = component.find("navService");
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.error(error);
                        });
                    });
                } else {
                    let navPromise = navService.navigate(pageReference, true);
                    navPromise && navPromise.then(() => {});
                }
            }).catch(function (error) {
            console.error(error);
        });
    },
    toPlainObject: function (input) {
        if (typeof input !== 'object' || input === null) {
            console.error("the input is not an object!");
            return null;
        }
        return JSON.parse(JSON.stringify(input));
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 3) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    closeFocusedTab: function (component, event, helper) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const {getFocusedTabInfo, refreshTab} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
    updateCompanyDivisionOnOpportunity: function (component, helper) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        helper.showSpinner(component);
        component
            .find("apexService")
            .builder()
            .setMethod("updateCompanyDivisionInOpportunity")
            .setInput({
                opportunityId: opportunityId,
                dossierId: dossierId,
                companyDivisionId: component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateContractInfo: function (component, helper) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId");

        let contractFields = helper.getContractFields(component);
        if (contractFields === null){
            ntfSvc.error(ntfLib, $A.get('$Label.c.ContractFieldsAreEmpty'));
            return false;
        }

        if (component.get("v.channelSelected") === "Indirect Sales"){
            let contractSelectionComponent = helper.getComponent(component, helper, "contractSelectionComponent");
            if (!contractSelectionComponent.validate()){
                ntfSvc.error(ntfLib, $A.get('$Label.c.ContractFieldsValidationFailed'));
                return false;
            }
        }

        component
            .find("apexService")
            .builder()
            .setMethod("updateContractInfo")
            .setInput({
                opportunityId: opportunityId,
                contractFields: JSON.stringify(contractFields),
                contractId: contractId
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();

        return true;
    },
    savePersistentAttribute: function (component, helper, name, value) {
        // This is a workaround for automatic cleaning of attributes on page refresh
        let attrName = "v." + name;

        if (helper.isNullOrEmpty(value)){
            return;
        }

        component.set(attrName, value);
        window.sessionStorage.setItem(name, JSON.stringify(value));
    },
    loadPersistentAttribute: function (component, helper, name) {
        // This is a workaround for automatic cleaning of attributes on page refresh
        let returnValue = "";
        let attrName = "v." + name;

        // Give priority to session storage values when loading
        let valueInSessionStorage = window.sessionStorage.getItem(name);
        if (!helper.isNullOrEmpty(valueInSessionStorage)){
            returnValue = JSON.parse(valueInSessionStorage);
            component.set(attrName, returnValue);
            return returnValue;
        }

        let valueInComponentStorage = component.get(attrName);
        if (!helper.isNullOrEmpty(valueInComponentStorage)) {
            returnValue = valueInComponentStorage;
            return returnValue;
        }

        return returnValue;
    },
    cleanPersistentAttributes: function (){
        let n = window.sessionStorage.length;
        while(n--) {
            let key = window.sessionStorage.key(n);
            window.sessionStorage.removeItem(key);
        }
    },
    restoreLocalState: function (component, helper){
        helper.loadPersistentAttribute(component, helper, "commoditySupplyId");
        helper.loadPersistentAttribute(component, helper, "commodity");
        helper.loadPersistentAttribute(component, helper, "contractType");
        helper.loadPersistentAttribute(component, helper, "commodityProductStringified");
        helper.loadPersistentAttribute(component, helper, "contractName");
        helper.loadPersistentAttribute(component, helper, "assetId");
        helper.setContractName(component, helper);
    }
})