/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.04.20.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        helper.initialize(component);
        helper.initConsoleNavigation(component, $A.get("$Label.c.ATRExtension"));
    },

    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            helper.setChannelAndOrigin(component, helper);
        }
    },

    handleSupplyResult: function (component, event, helper) {
        const supplyIds = event.getParam('selected');
        helper.upsertCase(component, supplyIds);
    },

    handleDeleteCaseTile: function (component, event, helper) {
        component.set('v.caseRecord', null);
    },

    handleCaseFieldsChange: function(component, event, helper){

    },

    handleCaseFormSuccess: function(component, event, helper){
        helper.changeStep(component, helper, 3);
    },

    handleCaseFormError: function(component, event, helper){
        console.log('######## Case form error');
    },

    handleSendingChannelSelection: function(component, event, helper){
        helper.changeStep(component, helper, 5);
    },

    handleSuccessContact: function (component, event, helper) {
        let contactRecord = event.getParam('contactRecord');
        if (contactRecord) {
            localStorage.clear();
            localStorage.setItem(component.get('v.dossierId'), JSON.stringify(event.getParam('contactRecord')));
            component.set('v.representativeContactId', contactRecord.Id);
            if (component.get('v.isBusinessAccount')) {
                component.set('v.disableForBusiness', false);
            }
        } else {
            localStorage.clear();
            component.set('v.representativeContactId', null);
            if (component.get('v.isBusinessAccount')) {
                component.set('v.disableForBusiness', true);
            }
        }
    },

    handleSelectBilling: function (component, event, helper) {
        let billingId = event.getParam("billingProfileRecordId");
        component.set('v.billingProfileId', billingId);
        helper.getBillingProfile(component, helper, billingId);
    },

    getPrivacyId: function (component, event, helper) {
        helper.changeStep(component, helper, 6);
    },

    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('confirmStep', '')) + 1;
        helper.changeStep(component, helper, stepNumber, true);
    },

    editStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('editStep', ''));
        helper.changeStep(component, helper, stepNumber);
    },

    cancel: function (component, event, helper) {
        component.set('v.showCancelBox', true);
    },
    onCloseCancelBox: function(component){
        component.set('v.showCancelBox', false);
    },
    closeAlertBox: function(component){
        component.set('v.showAlert', false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component);
    },

    save: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        helper.handleSave(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.handleSaveDraft(component, helper);
    },
})