/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.04.20.
 */
({
    initialize: function (component) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);

        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId" : genericRequestId
            })
            .setResolve(function (response) {
                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId);
                    return;
                }
                component.set("v.isClosed", response.isClosed);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.caseRecord", response.caseRecord);
                component.set("v.accountRecord", response.account);

                if(localStorage.getItem(response.dossierId) !== null){
                    component.set("v.contactRecord", JSON.parse(localStorage.getItem(dossierId)));
                    component.set("v.representativeContactId", component.get("v.contactRecord").Id)
                }

                self.setDisabledStepIfBusiness(component, response.account.RecordType.DeveloperName);
                self.updateSearchConditions(component);
                self.setStep(component, response);
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    setChannelAndOrigin: function (component, helper) {
        component.find('apexService').builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {
                if(component.get('v.step') === 0) {
                    helper.changeStep(component, helper, 1);
                }
            })
            .setReject(function (error) {
            })
            .executeAction();
    },

    upsertCase: function (component, supplyIds, callback) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let that = this;
        that.showSpinner(component);
        let input = {
            dossierId: component.get("v.dossierId"),
            originSelected: component.get("v.originSelected"),
            channelSelected: component.get("v.channelSelected"),
        };

        if(supplyIds){
            input.supplyId = supplyIds[0];
        }

        let caseRecord = component.get('v.caseRecord');
        if(caseRecord){
            input.caseId = caseRecord.Id;
        }

        component.find('apexService').builder()
            .setMethod("upsertCase")
            .setInput(input)
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (!response.error) {
                    component.set('v.caseRecord', response.caseRecord);

                    if(response.showAlert){
                        component.set('v.showAlert', true);
                    }

                    if(callback){
                        callback();
                    }
                }
            })
            .setReject(function (error) {
                that.hideSpinner(component);
                const errors = error.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();

    },

    redirectToDossier: function (component) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        this.redirect(component, pageReference);
    },

    performSaveProcess: function (component, helper, isDraft) {
        let input = {
            dossierId: component.get("v.dossierId"),
            isDraft: isDraft,
            contactId: component.get("v.representativeContactId"),
            billingProfileId: component.get("v.billingProfileId")
        };
        let caseRecord = component.get("v.caseRecord");
        if(caseRecord){
            input.caseId = caseRecord.Id;
            input.companyDivisionId = caseRecord.CompanyDivision__c;
            input.supplyId = caseRecord.Supply__c;
        }

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("saveProcess")
            .setInput(input)
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                helper.redirectToDossier(component);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },

    handleSave: function (component, helper) {
        helper.performSaveProcess(component, helper, false);
    },

    handleSaveDraft: function (component, helper) {
        helper.performSaveProcess(component, helper, true);
    },

    setStep: function (component, response) {
        let step = 4;

        if (!response.dossier.SendingChannel__c) {
            step = 3;
        }

        if (!response.caseRecord || !response.caseRecord.Description) {
            step = 2;
        }

        if (!response.dossier || !response.dossier.Channel__c || !response.dossier.Origin__c || !response.caseRecord) {
            step = 1;
        }
        this.changeStep(component, this, step);
    },

    updateUrl: function (component, accountId, dossierId) {
        let self = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ATRExtensionWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        self.redirect(component, pageReference, true);
    },

    updateSearchConditions: function (component, helper) {
        let today = this.getWorkingDays(5).toJSON().slice(0,10);
        let supplySearchConditions = [];

        supplySearchConditions.push(`(ServicePoint__r.ATRExpirationDate__c > `  + today + `)`);
        supplySearchConditions.push(`(ServiceSite__c != null AND RecordType.DeveloperName != 'Service' AND ServicePoint__r.SolutionType__c = 'Temporary')`);
        component.set("v.supplySearchConditions", supplySearchConditions);
    },

    changeStep: function (component, helper, stepNumber, isNext) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set('v.disableOriginChannel', stepNumber > 1);

        if (stepNumber === 3 && isNext) {
            component.find('caseDetailsForm').submit();
            return;
        }

        if (stepNumber === 4 && isNext) {
            let isBusinessAccount = component.get('v.isBusinessAccount');
            let refundIBANIsNull = component.get('v.refundIBANIsNull');
            let billingAddressIsNull = component.get('v.billingAddressIsNull');

            if (billingAddressIsNull){
                ntfSvc.error(ntfLib, $A.get("$Label.c.ATRMissingAddressError"));
                return;
            }
            if (isBusinessAccount && refundIBANIsNull){
                ntfSvc.error(ntfLib, $A.get("$Label.c.ATRProvideIBANError"));
                return;
            }
        }

        if (stepNumber === 5 && isNext) {
            component.find('sendingChannelSelection').saveSendingChannel();
            return;
        }

        if (stepNumber === 6 && isNext) {
            try{
                component.find("privacyChange").savePrivacyChange();
                return;
            } catch (ex) {}
        }

        component.set("v.step", stepNumber);
    },

    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference, overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },

    getBillingProfile: function (component, helper, billingId) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod('getBillingProfile')
            .setInput({
                billingProfileId: billingId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                let refundIBANIsNull = response.billingProfile.RefundIBAN__c == null ? true : false;
                let billingAddressIsNull = response.billingProfile.BillingAddress__c == null ? true : false;
                component.set('v.refundIBANIsNull', refundIBANIsNull)
                component.set('v.billingAddressIsNull', billingAddressIsNull)
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },

    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },

    getWorkingDays: function(workingDaysBack) {
        let today = new Date();

        while (workingDaysBack > 0) {
            if (![6, 0].includes(today.getDay())) {
                workingDaysBack--;
            }
            today = new Date(today.setDate(today.getDate() - 1));
        }

        return today;
    },

    setDisabledStepIfBusiness: function (component, accountRecordType) {
        if (accountRecordType === 'Business') {
            component.set('v.isBusinessAccount', true);
            if (component.get('v.representativeContactId')) {
                component.set('v.disableForBusiness', false);
            } else {
                component.set('v.disableForBusiness', true);
            }
        }
    },

    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },

    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
})