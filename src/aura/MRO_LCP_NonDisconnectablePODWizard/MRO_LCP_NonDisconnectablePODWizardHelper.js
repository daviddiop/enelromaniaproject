/**
 * Created by david on 29.10.2019.
 */
({
    initialize: function (component, event, helper) {
        const self = this;
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component);

        component.set("v.accountId", accountId);
        if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }
        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod("Initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "genericRequestId": genericRequestId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            }).setResolve(function (response) {
            if (!response.error) {
                component.set("v.caseTile", response.caseTile);
                component.set("v.accountId", response.accountId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.nonDisconnectablePODEle", response.NonDisconnectablePODEle);
                component.set("v.nonDisconnectablePODGas", response.NonDisconnectablePODGas);
                component.set("v.reason", response.reason);
                component.set("v.templateId", response.templateId);

                if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                }
                if (response.caseTile) {
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                }
                if(response.dossier.Status__c === 'Canceled'){
                    component.set('v.isClosed',true);
                }
                let step = 0;
                if(component.get('v.originSelected') && component.get('v.channelSelected')){
                    step = 1;
                }
                if (response.caseTile && response.caseTile.length !== 0) {
                    step = 2;
                }

                component.set("v.step", step);
            } else {
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
        let topElement = component.find("topElement").getElement();
        if (topElement) {
            topElement.scrollIntoView({behavior: "instant", block: "end"});
        }
        self.hideSpinner(component);
    },
    handleCancel: function (component, helper) {
        const self = this;
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                caseList: caseList,
                dossierId: dossierId
            }).setResolve(function (response) {
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_NonDisconnectablePODWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    /*
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = "Lightning Experience | Salesforce";
        }
    }/*
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        component.set("v.caseTile", items);
        component.set("v.step", 2);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    saveChain: function (component, helper) {
        const self = this;
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const origin = component.get('v.originSelected');
        const channel = component.get('v.channelSelected');
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                origin : origin,
                channel :  channel
            }).setResolve(function (response) {
            if (!response.error) {
                self.redirectToDossier(component, helper);
                self.hideSpinner(component);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    createCase: function (component, event, helper) {
        $A.util.removeClass(component.find('spinnerSectionModal'), 'slds-hide');
        const self = this;
        const nonDisconnectable = component.get("v.nonDisconnectable");
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const origin = component.get('v.originSelected');
        const channel = component.get('v.channelSelected');
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const commodity = component.get("v.commodity");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let reason;
        if (nonDisconnectable) {
            reason = component.find("reason").get("v.value");
            if (!reason) {
                $A.util.addClass(component.find("reason"), "slds-has-error");
                $A.util.addClass(component.find('spinnerSectionModal'), 'slds-hide');
                return;
            }
        } else {
            reason = "";
        }

        component.find('apexService').builder()
            .setMethod("createOldCase")
            .setInput({
                'nonDisconnectable': nonDisconnectable,
                'reason': reason,
                'searchedSupplyFieldsList': searchedSupplyFieldsList,
                'caseTile': caseList,
                'accountId': accountId,
                'dossierId': dossierId,
                'origin' : origin,
                'channel' :  channel,
                'commodity' :  commodity
            })
            .setResolve(function (response) {
                if (response.error) {
                    component.set("v.showNewCase", false);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                } else {
                    component.set('v.showNewCase',false);
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    $A.util.addClass(component.find('spinnerSectionModal'), 'slds-hide');
                    self.initialize(component, event, helper);
                }


                /*if (response.caseTile.length !== 0) {
                    component.set("v.step", 1);
                }*/


                //self.resetSupplyForm(component);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    setCommodityFilter: function (component) {
        let commodityEle = 'Electric';
        let commodityGas = 'Gas';
        let commodityConditionEle ="RecordType.DeveloperName='"+commodityEle+"'";
        let commodityConditionGas ="RecordType.DeveloperName='"+commodityGas+"'";
        let orConditions = [];
        orConditions.push(commodityConditionEle);
        orConditions.push(commodityConditionGas);
        component.set("v.orConditions",orConditions);
    },
});