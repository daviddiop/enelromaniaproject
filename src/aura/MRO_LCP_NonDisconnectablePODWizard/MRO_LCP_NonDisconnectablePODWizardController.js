/**
 * Created by david on 29.10.2019.
 */

({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        component.set("v.accountId", accountId);
        helper.setCommodityFilter(component);
        helper.initConsoleNavigation(component, $A.get('$Label.c.NonDisconnectablePod'));
        helper.initialize(component, event, helper);
    },
    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },
     handleOriginChannelSelection: function (component, event, helper) {
         let originSelected = event.getParam('selectedOrigin');
         let channelSelected = event.getParam('selectedChannel');
         if (originSelected && channelSelected){
             component.set('v.originSelected', originSelected);
             component.set('v.channelSelected', channelSelected);
             component.set("v.step",1);
         }else {
             component.set("v.step",0);
         }
     },
/*    handleSupplyResult: function (component, event, helper) {
        const searchedSupplyFieldsList = event.getParam("supplyFieldsValue");
        console.log('searchedSupplyFieldsList' + JSON.stringify(searchedSupplyFieldsList));
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];

        for (let i = 0; i < searchedSupplyFieldsList.length; i++) {
            let validSupply = true;
            let oneSupply = searchedSupplyFieldsList[i];
            if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                continue;
            }
            if (caseList) {
                for (let j = 0; j < caseList.length; j++) {
                    if (oneSupply.Id === caseList[j].Supply__c) {
                        errSupplyAlreadySelected.push(oneSupply.Name);
                        validSupply = false;
                        break;
                    }
                }
            }
            if (validSupply) {
                validSupplies.push(oneSupply);
            }
        }
        if (errSupplyAlreadySelected.length !== 0) {
            let messages = errSupplyAlreadySelected.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
        }
        if (errCurrentSupplyIsNotValid.length !== 0) {
            let messages = errCurrentSupplyIsNotValid.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
        }
        component.set("v.searchedSupplyFields", validSupplies);
        component.set("v.disabledSaveButton", false)
        component.set("v.nonDisconnectable", false);
        component.set("v.nonDisconnectableReason", '');
        if (searchedSupplyFieldsList.length === 1) {
            component.set("v.nonDisconnectable", searchedSupplyFieldsList[0].NonDisconnectable__c);
            if(!component.get('v.nonDisconnectable')){
                component.set("v.disabledSaveButton", false)
            }
            component.set("v.nonDisconnectableReason", searchedSupplyFieldsList[0].NonDisconnectableReason__c);
            if (component.get('v.nonDisconnectableReason')) {
                component.set("v.disabledSaveButton", false)
            }
        }
        component.set("v.showNewCase", (validSupplies.length !== 0));

    },


    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },*/

    searchSelectionHandler: function (component, event, helper){
        const supplyIds = event.getParam('selected');
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (!supplyIds || !supplyIds.length) {
            return;
        }
        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];
        let isSameNonDisconnectableAndReason = true ;
        let nonDisconnectableReason;
        let commodity = '';

        component.find('apexService').builder()
            .setMethod("getSupplyRecord")
            .setInput({
                supplyIds: supplyIds
            })
            .setResolve(function (response)
                {
                    if (!response.error) {
                        for (let i = 0; i < response.supplies.length; i++) {
                            let validSupply = true;
                            let oneSupply = response.supplies[i];
                            if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                                continue;
                            }
                            if (caseList) {
                                for (let j = 0; j < caseList.length; j++) {
                                    if (oneSupply.Id === caseList[j].Supply__c) {
                                        errSupplyAlreadySelected.push(oneSupply.Name);
                                        validSupply = false;
                                        break;
                                    }
                                }
                            }
                            if (validSupply) {
                                validSupplies.push(oneSupply);
                            }
                            for (let j = 0; j < response.supplies.length; j++) {
                                if (oneSupply.NonDisconnectable__c !== response.supplies[j].NonDisconnectable__c) {
                                    isSameNonDisconnectableAndReason = false;
                                    break;
                                }else {
                                    if (oneSupply.NonDisconnectableReason__c === response.supplies[j].NonDisconnectableReason__c){
                                        nonDisconnectableReason = response.supplies[j].NonDisconnectableReason__c;
                                    }else{
                                        nonDisconnectableReason = undefined;
                                        break;
                                    }
                                }
                            }
                            for (let j = 0; j < response.supplies.length; j++) {
                                if (oneSupply.RecordType["DeveloperName"] === 'Electric' ){
                                    if (oneSupply.RecordType["DeveloperName"] === response.supplies[j].RecordType["DeveloperName"]){
                                        if (commodity === 'Gas'){
                                            commodity = '';
                                            break ;
                                        }else {
                                            commodity = 'Electric';
                                        }
                                    }
                                }
                            }
                            for (let j = 0; j < response.supplies.length; j++) {
                                if (oneSupply.RecordType["DeveloperName"] === 'Gas' ){
                                    if (oneSupply.RecordType["DeveloperName"] === response.supplies[j].RecordType["DeveloperName"]){
                                        if (commodity === 'Electric'){
                                            commodity = '';
                                            break ;
                                        }else {
                                            commodity = 'Gas';
                                        }
                                    }
                                }
                            }
                        }
                        console.log('### commodity '+commodity);
                        console.log('### isSameNonDisconnectableAndReason '+isSameNonDisconnectableAndReason);
                        component.set('v.commodity', commodity);

                        if (errSupplyAlreadySelected.length !== 0) {
                            let messages = errSupplyAlreadySelected.join(' ');
                            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
                        }
                        if (errCurrentSupplyIsNotValid.length !== 0) {
                            let messages = errCurrentSupplyIsNotValid.join(' ');
                            ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
                        }
                        component.set("v.searchedSupplyFields", validSupplies);
                        component.set("v.disabledSaveButton", false)
                        component.set("v.nonDisconnectable", false);
                        component.set("v.nonDisconnectableReason", '');
                        if (response.supplies.length === 1) {
                            component.set("v.nonDisconnectable", response.supplies[0].NonDisconnectable__c);
                            if(!component.get('v.nonDisconnectable')){
                                component.set("v.disabledSaveButton", false)
                            }
                            if(component.get('v.nonDisconnectable')){
                                ntfSvc.warn(ntfLib, $A.get("$Label.c.PodIsAlreadyMarked"));
                            }
                            component.set("v.nonDisconnectableReason", response.supplies[0].NonDisconnectableReason__c);
                            if (component.get('v.nonDisconnectableReason')) {
                                component.set("v.disabledSaveButton", false)
                            }
                        }
                        if (isSameNonDisconnectableAndReason){
                            if(response.supplies.length > 1 ){
                                component.set("v.nonDisconnectable", response.supplies[0].NonDisconnectable__c);
                                if(!component.get('v.nonDisconnectable')){
                                    component.set("v.disabledSaveButton", false)
                                }
                                if(component.get('v.nonDisconnectable')){
                                    ntfSvc.warn(ntfLib, $A.get("$Label.c.PodIsAlreadyMarked"));
                                }
                                if (nonDisconnectableReason){
                                    component.set("v.nonDisconnectableReason", response.supplies[0].NonDisconnectableReason__c);
                                }
                                if (component.get('v.nonDisconnectableReason')) {
                                    component.set("v.disabledSaveButton", false)
                                }
                            }
                        }
                        component.set("v.showNewCase", (validSupplies.length !== 0));
                    }
                })
            .setReject(function (error) {
            })
            .executeAction();
    },

    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            default:
                break;
        }
    },

    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleSuccessCase: function (component, event, helper) {
        component.set("v.showNewCase", false);
        helper.initialize(component, event, helper);
        //helper.resetSupplyForm(component, event, helper);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    save: function (component, event, helper) {
        helper.saveChain(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftNonDisconnectablePOD(component, event, helper);
    },
    handleNonDisconnectable: function (component, event, helper) {
        component.set("v.nonDisconnectable", component.find("inputToggle").get("v.checked"));
        component.set("v.disabledSaveButton", component.find("inputToggle").get("v.checked"));
        if(component.get("v.nonDisconnectableReason")){
            component.set("v.disabledSaveButton", false);
        }
    },
    handleReason: function (component, event, helper) {
        if (component.find("reason").get("v.value")) {
            component.set("v.disabledSaveButton", false);
        } else {
            component.set("v.disabledSaveButton", true);
        }
    },

    handleCreateCase: function (component, event, helper) {
        helper.createCase(component, event, helper);
    },
});