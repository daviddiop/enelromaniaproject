({
    initialize: function (component, event, helper) {

        console.log('step', component.get("v.step"));
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        /*if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }*/
        component.find('api').builder()
            .setMethod("initialize")
            .setInput({
                'accountId': accountId,
                'dossierId': dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
                // 'companyDivisionId': ''
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    component.set("v.templateId", response.templateId);
                    component.set('v.originSelected', response.dossier.Origin__c);
                    component.set('v.channelSelected', response.dossier.Channel__c);


                    /*if ((response.dossierId && response.dossierId !== myPageRef.state.c__dossierId)) {
                        self.updateUrl(component, accountId, response.dossierId, response.templateId);
                    }*/
                    if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                    }
                    if (response.companyDivisionId) {
                        //component.set("v.companyDivisionName", response.companyDivisionName);
                        component.set("v.companyDivisionId", response.companyDivisionId);
                    }

                    let step = component.get("v.step") === -1 ? 0 : component.get("v.step");
                    if (response.caseTile && response.caseTile.length !== 0) {
                        step = 3;
                        component.set('v.disableOriginChannel', true);
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    }
                    const dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }

                    component.set("v.step", step);
                    component.set("v.account", response.account);
                    console.log('account' + JSON.stringify(response.account));
                    component.set("v.accountPersonRT", response.accountPersonRT);
                    component.set("v.accountPersonProspectRT", response.accountPersonProspectRT);
                    component.set("v.accountBusinessRT", response.accountBusinessRT);
                    component.set("v.accountBusinessProspectRT", response.accountBusinessProspectRT);
                    component.set("v.traderPicklistValues", response.traderPicklistValues);
                    component.set("v.traderLabel", response.traderLabel);
                    component.set("v.effectiveDateLabel", response.effectiveDateLabel);
                    component.set("v.referenceDateLabel", response.referenceDateLabel);
                    component.set("v.defaultFirstDate", response.effectiveDate);
                    component.set("v.switchOutRecordType", response.switchOutRecordType);
                    component.set("v.effectiveDate", response.effectiveDate);
                    component.set("v.defaultEffectiveDate", response.effectiveDate);
                    component.set("v.hasRetroactiveSwitchOutPermission", response.hasRetroactiveSwitchOutPermission);

                    component.set("v.commodityPicklistValues", response.commodityPicklistValues);
                    component.set("v.commodityLabel", response.commodityLabel);
                    component.set("v.commodity", response.commodity);

                    component.set("v.caseTile", response.caseTile);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    saveDraftBillingProfile: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("saveDraftBP")
            .setInput({
                'oldCaseList': caseList
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    createCase: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, "spinnerSectionModal");
        const trader = component.get("v.trader");
        const reason = component.get("v.reason");
        const effectiveDate = component.get("v.effectiveDate");
        const referenceDate = component.get("v.referenceDate");
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const caseId = component.get("v.caseId");
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((trader == null) || (effectiveDate == null) || (referenceDate == null) || (reason == null)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            self.hideSpinner(component, 'spinnerSectionModal');
            return;
        }

        let hasRetroactiveSwitchOutPermission = component.get("v.hasRetroactiveSwitchOutPermission");
        if (!hasRetroactiveSwitchOutPermission && effectiveDate < component.get('v.defaultEffectiveDate')) {
            self.hideSpinner(component, "spinnerSectionModal");
            ntfSvc.error(ntfLib, $A.get("$Label.c.SwitchOutDate5days"));
            return;
        }
        if (caseList.length !== 0) {
            let casePivot = caseList[0];
            if (casePivot) {
                if ((casePivot.EffectiveDate__c !== effectiveDate) || (casePivot.Reason__c !== reason) ||
                    (casePivot.ReferenceDate__c !== referenceDate) || (casePivot.Trader__c !== trader)) {
                    //ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    ntfSvc.error(ntfLib, $A.get("$Label.c.CaseSwitchOutData"));
                    self.hideSpinner(component, "spinnerSectionModal");
                    return;
                }
            }
        }

        if (reason === "Trader change") {
            let numberOfDay = self.getDayDateDiff(component);
            console.log("### numberOfDay " + numberOfDay);
            if (!isNaN(numberOfDay) && numberOfDay < 21) {
                ntfSvc.warn(ntfLib, $A.get("$Label.c.SwitchOutDate21"));
                self.hideSpinner(component, "spinnerSectionModal");
            }
        }
        //self.validDate(component, helper)
        self.updateList(component, helper);
    },

    validDate: function (component, helper) {
        const self = this;
        const effectiveDate = component.get("v.effectiveDate");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("getValidDate")
            .setInput({
                'effectiveDate': JSON.stringify(effectiveDate)
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.showNewCase", false);
                } else {
                    if (!response.effectiveValid) {
                        self.updateList(component, helper);
                    } else {
                        self.hideSpinner(component, "spinnerSectionModal");
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.notWorkingDay"));
                    }
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateList: function (component, helper) {
        const self = this;
        const trader = component.get("v.trader");
        const reason = component.get("v.reason");
        const referenceDate = component.get("v.referenceDate");
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const caseId = component.get("v.caseId");
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        let channelSelected = component.get("v.channelSelected");
        let originSelected = component.get("v.originSelected");
        const effectiveDate = component.get("v.effectiveDate");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find("api").builder()
            .setMethod("createCase")
            .setInput({
                "trader": trader,
                "reason": reason,
                "effectiveDate": effectiveDate,
                "referenceDate": referenceDate,
                "searchedSupplyFieldsList": searchedSupplyFieldsList,
                "caseList": caseList,
                "accountId": accountId,
                "caseId": caseId,
                "dossierId": dossierId,
                "channelSelected": channelSelected,
                "originSelected": originSelected
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.showNewCase", false);
                } else {
                    if (response.caseTile.length !== 0) {
                        component.set("v.step", 3);
                        component.set("v.companyDivisionId", response.caseTile[0].CompanyDivision__c);
                    }
                    component.set("v.showNewCase", false);
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    self.updateDossier(component, helper);
                }
                self.hideSpinner(component, "spinnerSection");
            })
            .setReject(function (response) {
                self.hideSpinner(component, "spinnerSectionModal");
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateCase: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        //add by david
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        //
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("updateCaseList")
            .setInput({
                'oldCaseList': caseList,
                'dossierId': dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },

    checkSupplyRecord: function (component, supplies) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];
        let oneTrader;
        let oneDistributor;

        for (let i = 0; i < supplies.length; i++) {
            let validSupply = true;
            let oneSupply = supplies[i];
            if (!oneSupply.ServicePoint__c) {
                ntfSvc.error(ntfLib, 'Service Point cannot be empty');
                return;
            }

            if (!oneTrader) {
                oneTrader = oneSupply["ServicePoint__r"]["Trader__c"];
            }
            if (!oneDistributor) {
                oneDistributor = oneSupply["ServicePoint__r"]["Distributor__c"];
            }

            if (oneTrader !== oneSupply["ServicePoint__r"]["Trader__c"] || oneDistributor !== oneSupply["ServicePoint__r"]["Distributor__c"]) {
                ntfSvc.error(ntfLib, 'On the same request,you cannot have 2 different Traders and/or 2 different distributors');
                return;
            }

            if (oneSupply.ServicePoint__c && (oneSupply.Id !== oneSupply["ServicePoint__r"]["CurrentSupply__c"])) {
                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                continue;
            }
            if (caseList) {
                for (let j = 0; j < caseList.length; j++) {
                    if (oneSupply.Id === caseList[j].Supply__c) {
                        errSupplyAlreadySelected.push(oneSupply.Name);
                        validSupply = false;
                        break;
                    }
                }
            }
            if (validSupply) {
                validSupplies.push(oneSupply);
            }
        }
        if (errSupplyAlreadySelected.length !== 0) {
            let messages = errSupplyAlreadySelected.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
            component.set("v.showNewCase", false);
            return;
        }
        if (errCurrentSupplyIsNotValid.length !== 0) {
            let messages = errCurrentSupplyIsNotValid.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
            component.set("v.showNewCase", false);
            return;
        }

        component.set("v.searchedSupplyFields", validSupplies);
        component.set("v.showNewCase", true);
    },

    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_SwitchOutWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 3);
            component.set('v.disableOriginChannel', false);
        }

        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    validateFields: function (component, event) {
        const labelFieldList = new Array();
        let valEffectiveDate = component.find("Field_EffectiveDate__c");
        let valReferenceDate = component.find("Field_ReferenceDate__c");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }

        if (!Array.isArray(valReferenceDate)) {
            valReferenceDate = [valReferenceDate];
        }
        tmp = valReferenceDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_ReferenceDate__c');
        }

        tmp = component.get('v.trader');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_Trader__c');
        }
        if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    },
    updateDossier: function (component, helper) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');

        component.find('api').builder()
            .setMethod("UpdateDossier")
            .setInput({
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected
            }).setResolve(function (response) {
            if (!response.error) {
                self.initialize(component, event, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    updateCommodityToDossier: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        component
            .find("api")
            .builder()
            .setMethod("updateCommodityToDossier")
            .setInput({
                dossierId: dossierId,
                commodity: component.get("v.commodity")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    getDayDateDiff: function (component) {
        const millesecondsPerDay = 86400000;
        let diff = Date.parse(component.get("v.effectiveDate")) - Date.parse(component.get("v.referenceDate"));
        return isNaN(diff) ? NaN : Math.floor(diff / millesecondsPerDay);
    },
    removeDateRestriction: function(component){
        let defaultFirstDate = component.get("v.defaultFirstDate");
         let reason = component.find("Field_Reason__c");
         if (!Array.isArray(reason)) {
             reason = [reason];
         }
         let tmp = reason[0].get('v.value');
        if(tmp && tmp == $A.get("$Label.c.Fraud")){
            component.set("v.defaultEffectiveDate", '');
            $A.util.removeClass(component.find("Field_EffectiveDate__c"), 'slds-has-error');
        } else {
            component.set("v.defaultEffectiveDate",defaultFirstDate);
        }
    }
});