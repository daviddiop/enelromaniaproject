({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const companyDivComponent = component.find("companyDivision");

        helper.initConsoleNavigation(component,$A.get('$Label.c.SwitchOut'));
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        component.set('v.reason','Trader change');

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        today =  yyyy + '-' + mm  + '-' + dd;
        component.set('v.referenceDate',today);
        helper.initialize(component, event, helper);
    },
    searchSelectionHandler: function (component, event, helper){
        const supplyIds = event.getParam('selected');
        if (!supplyIds || !supplyIds.length) {
            component.set("v.showNewCase", false);
            return;
        }
        const supplyValues = event.getParam('supplies');
        if (supplyIds) {
            //console.log(JSON.stringify(supplyValues));
            helper.checkSupplyRecord(component, supplyValues);
        }
    },
    selectTrader:function (component, event, helper) {
        let traderId = event.getParam('Id');
        console.log('traderId', traderId);
        component.set('v.trader', traderId);
    },
    unSelectTrader:function (component, event, helper) {
        component.set('v.trader', '');
    },
    closeCaseModal: function (component, event, helper) {
        component.set('v.caseId','');
        component.set("v.showNewCase", false);
    },
    handleCreateCase: function (component, event, helper) {
        event.preventDefault();
        let fields = event.getParam("fields");
        component.set('v.reason',fields.Reason__c);
        if (helper.validateFields(component, event)) {
            helper.showSpinner(component,'spinnerSectionModal');
            helper.createCase(component, event, helper);
        }
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (caseList.length === 0) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.updateCase(component, helper);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            case 'confirmStep9':
                    helper.updateCommodityToDossier(component);
                    component.set("v.step", 3);
            default:
                break;
        }
    },
   checkReason:function (component, event, helper){
       helper.removeDateRestriction(component);
   },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep9':
                component.set("v.step", 2);
                /*let step1 = document.getElementById("step-1");
                step1.scrollIntoView(({ behavior: "smooth"}));*/
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                /*let step2 = document.getElementById("step-2");
                step2.scrollIntoView(({ behavior: "smooth"}));*/
                break;
            default:
                break;
        }
    },
    removeError: function (component, event, helper) {
        const labelFieldList = [];
        helper.removeDateRestriction(component);
        let valEffectiveDate = component.find("Field_EffectiveDate__c");
        let valReferenceDate = component.find("Field_ReferenceDate__c");
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }

        if (!Array.isArray(valReferenceDate)) {
            valReferenceDate = [valReferenceDate];
        }
        tmp = valReferenceDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_ReferenceDate__c');
        }

        let valTrader = component.find("Field_Trader__c");
        if (!Array.isArray(valTrader)) {
            valTrader = [valTrader];
        }
        tmp = valTrader[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('Field_Trader__c');
        }
        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },
    cancel: function (component, event, helper) {
        component.set("v.showCancelBox", true);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        component.set("v.showCancelBox", false);
        helper.redirectToDossier(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftBillingProfile(component, helper);
    },

    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            console.log('### Origin '+component.get('v.originSelected'));
            console.log('### Channel '+component.get('v.channelSelected'));
            component.set("v.step", 1);
            let dossier = component.get('v.dossier');
        }
    },
});