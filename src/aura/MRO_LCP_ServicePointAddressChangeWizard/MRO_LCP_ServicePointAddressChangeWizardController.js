({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;

        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initialize(component, event, helper);
    },
    nextStep: function (component, event, helper) {
        let buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === 'confirmStep1') {
            component.set("v.step", 1);
        }
        if (buttonPressed === 'confirmStep2') {
            component.set("v.step", 2);
            let sendingChannel = component.find('sendingChannel');
            if (component.get('v.caseTile')) {
                sendingChannel.disableInputField(false);
            }
        }
    },
    editStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep2") {
            let sendingChannel = component.find('sendingChannel');
            sendingChannel.disableInputField(true);
            component.set("v.step", 1);
        }
    },
    handleSupplyResult: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        let selected = event.getParam("selected");
        let supply = event.getParam("selectedObject");
        let selectedSupply = selected[0];
        component.set("v.supplyId", selectedSupply.Id);
        console.log(" Supply ", supply);
        if (supply){
            if (!supply.ServiceSite__c) {
                ntfSvc.warn(ntfLib, $A.get("$Label.c.MissingServiceSite"));
            }
            if (!supply.ServicePoint__c) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.MissingServicePoint"));
            }else {
                if(supply.ServicePoint__r.Distributor__c){
                    component.set("v.distributor",supply.ServicePoint__r.Distributor__c);
                }
                component.set("v.supplyId", supply.Id);
                component.set("v.searchedPointId", supply.ServicePoint__c);
                component.set("v.searchedPointCode", supply["ServicePoint__r"]["Name"]);
                component.set("v.supplyCompanyDivisionId", supply.CompanyDivision__c);
                component.set("v.showNewCase", true);
                if (!supply.ServiceSite__c) {
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.MissingServiceSite"));
                }else {
                    console.log('###supplyId: '+component.get('v.supplyId'));
                    helper.getOtherSupplyRelatedToServiceSite(component,supply.ServiceSite__c);
                }
                //helper.getServicePointData(component,supply.ServicePoint__c);
            }
        }
    },
    handleOriginChannelSelection: function (component, event) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step", 0);
        } else {
            component.set("v.step", -1);
        }
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleSuccessCase: function (component, event, helper) {
        helper.updateDossier(component, event, helper);
        helper.initialize(component, event, helper);
        helper.resetSupplyForm(component, event, helper);
        component.set("v.disableOriginChannel", true);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        if ((caseList.length === 0)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        console.log('* saving chain *')
        let sendingCha = component.find('sendingChannel');
        sendingCha.saveSendingChannel();
        //helper.showSpinner(component);
        //helper.saveChain(component, helper);
    },
    handleSuccessDossier: function (component,event, helper){
        event.preventDefault();
        helper.saveChain(component, helper);
    },
    cancel: function (component, event, helper) {
        component.set("v.showCancelBox", true);
        //helper.handleCancel(component, helper);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        component.set("v.showCancelBox", false);
        helper.clearDataForm(component);
        helper.redirectToDossier(component, helper);
        component.set('v.dossierId', "");
        //helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    }
});