({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod("InitializeServicePointAddressChange")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "companyDivisionId": '',
                "genericRequestId": genericRequestId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                component.set("v.caseTile", response.caseTile);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set('v.originSelected', response.dossier.Origin__c);
                component.set('v.channelSelected', response.dossier.Channel__c);
                component.set("v.recordTypeServicePointAddressChange", response.recordTypeServicePointAddressChange);
                component.set("v.templateId", response.templateId);

                /*if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId, response.templateId);
                }*/
                if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                }

                if (response.companyDivisionName) {
                    component.set("v.companyDivisionName", response.companyDivisionName);
                    component.set("v.companyDivisionId", response.companyDivisionId);
                }
                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                    }
                }

                let step = -1;
                if(dossierVal.Origin__c && dossierVal.Channel__c){
                    step = 0;
                }
                if (response.caseTile && response.caseTile.length !== 0) {
                    step = 1;
                    component.set("v.disableOriginChannel", true);
                }

                component.set("v.step", step);
                if (response.caseTile) {
                    component.set("v.tableView", response.caseTile.length > component.get("v.tileNumber"));
                }
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveChain: function (component, helper) {

        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const caseList = component.get("v.caseTile");
        const otherSupplies = component.get("v.otherSupplies");
        const dossierId = component.get("v.dossierId");
//        console.log('caseList', JSON.stringify(caseList));
        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                otherSupplies: JSON.stringify(otherSupplies),
                dossierId: dossierId
            }).setResolve(function (response) {
            if (!response.error) {
                helper.redirectToDossier(component, helper);
            } else {
                console.log(response.errorMsg, response.errorTrace);
                ntfSvc.error(ntfLib, response.errorMsg);
            }
            self.hideSpinner(component);
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    getServicePointData: function (component, servicePointId) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("getServicePoint")
            .setInput({
                servicePointId: servicePointId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                component.set("v.servicePointAddress", response["servicePointAddress"]);
                component.set("v.showNewCase", true);
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    getOtherSupplyRelatedToServiceSite: function (component, serviceSiteId) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const supplyId = component.get('v.supplyId');
        component.find('apexService').builder()
            .setMethod("OtherSupplyRelatedToServiceSite")
            .setInput({
                supplyId: supplyId,
                serviceSiteId: serviceSiteId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                if (response.supplies){
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.OtherSuppliesRelatedToServiceSite"));
                    console.log('###Supplies: '+JSON.stringify(response.supplies));
                    component.set('v.otherSupplies',response.supplies);
                }
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId,templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ServicePointAddressChangeWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 1);
            component.set("v.disableOriginChannel", false);
        }
        component.set("v.caseTile", items);
        component.set("v.tableView", items.length > component.get("v.tileNumber"));
    },
    updateDossier : function(component){
        const self = this;
        const dossierId = component.get("v.dossierId");
        const origin = component.get("v.originSelected");
        const channel = component.get("v.channelSelected");
        component.find('apexService').builder()
            .setMethod("updateDossier")
            .setInput({
                dossierId: dossierId,
                origin: origin,
                channel: channel
            }).setResolve()
            .setReject(function () {
                self.hideSpinner(component);
            })
            .executeAction();
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    saveDraftServicePointAddressChange: function (component, event, helper) {
        const self = this;
        self.redirectToDossier(component, helper);
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetBox();
        } else {
            supplySearchComponent.resetBox();
        }
    },
    clearDataForm: function (component) {
        component.set("v.caseTile", []);
        component.set('v.originSelected', "");
        component.set('v.channelSelected', "");
        component.set("v.templateId", "");
        component.find('cookieSvc').clearInteractionId();
    }
    ,
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    }
});