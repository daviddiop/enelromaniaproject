({
    // Start of the steps api methods.
    stepsInit: function (component, helper) {
        // Fills the Step Indexes Map
        let stepIndexes = {};

        let i = 0;
        for (const stepName in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(stepName)) {
                stepIndexes[stepName] = i;
                i++;
            }
        }

        component.set("v.stepIndexesMap", stepIndexes);
    },
    isPartOfCurrentFlow: function (component, helper, step) {
        // Checks whether a given step is a part of the active flow

        let flowTitle = component.get("v.currentFlow");

        // If there is only one default flow, every step is included
        if (flowTitle === undefined) {
            return true;
        }

        let result = false;
        for (let i = 0; i <= Object.keys(step.Flows).length; i++) {
            let iFlowName = step.Flows[i];
            if (iFlowName === 'ALL') {
                result = true;
            } else if (component.get("v.flows").hasOwnProperty(iFlowName)) {
                let stepFlowTitle = component.get("v.flows")[iFlowName];
                if (stepFlowTitle === flowTitle) {
                    result = true;
                }
            }
        }
        return result;
    },
    setCurrentStep: function (component, helper, stepTitle) {
        // Set the current step
        let currentStepIndex = helper.getIndexFromStepTitle(component, stepTitle);
        component.set("v.step", stepTitle);
        component.set("v.currentStepIndex", currentStepIndex);

        // Set the previous step
        let previousStepTitle = helper.getPreviousStep(component, helper, stepTitle).Title;
        let previousStepIndex = helper.getIndexFromStepTitle(component, previousStepTitle);
        component.set("v.previousStepIndex", previousStepIndex);
    },
    getStepFromIndex: function (component, stepIndex) {
        let index = 0;
        for (const iStepTitle in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(iStepTitle)) {
                if (index === stepIndex) {
                    return component.get("v.steps")[iStepTitle];
                }
                index++;
            }
        }
        return null;
    },
    getIndexFromStepTitle: function (component, stepTitle) {
        // Returns the index of a step by its title
        let stepIndex = 0;
        for (const iStepTitle in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(iStepTitle)) {
                if (iStepTitle === stepTitle) {
                    return stepIndex;
                }
                stepIndex++;
            }
        }
        return null;
    },
    getNextStep: function (component, helper, stepTitle) {
        // If the current step is null or empty, use the first step instead of it, if needed
        let currentStepTitle = component.get("v.step");
        if (helper.isNullOrEmpty(currentStepTitle)) {
            currentStepTitle = helper.getFirstStep(component, helper).Title;
        }

        // Start searching for the next, with the given or current step
        if (helper.isNullOrEmpty(stepTitle)) {
            stepTitle = currentStepTitle;
            console.warn('next step based on the current');
        }

        let stepIndex = this.getIndexFromStepTitle(component, stepTitle);
        let nextStep;
        let stepsCount = Object.keys(component.get("v.steps")).length;
        if ((stepIndex >= stepsCount - 1)) {
            return this.getStepFromIndex(component, stepsCount - 1);
        }
        for (let i = stepIndex + 1; i <= stepsCount; i++) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (helper.isPartOfCurrentFlow(component, helper, iStep)) {
                    if (iStep.Title === component.get("v.steps").SET_ADDITIONAL_INFO.Title && !component.get("v.isExtendedProduct")) {
                        continue;
                    }
                    nextStep = iStep;
                    break;
                }
            }
        }
        return nextStep;
    },
    getStepFromTitle: function (component, helper, stepTitle) {
        // Returns the step by its title
        let step = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (iStep.Title === stepTitle) {
                    step = iStep;
                    break;
                }
            }
        }
        return step;
    },
    getCurrentStep: function (component, helper) {
        // Returns the current step
        let currentStepTitle = component.get("v.step");
        if (helper.isNullOrEmpty(currentStepTitle)) {
            console.error('current step is not set');
            return;
        }
        return helper.getStepFromTitle(component, helper, currentStepTitle);
    },
    getFirstStep: function (component, helper) {
        // Returns the first step
        let firstStep = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (helper.isPartOfCurrentFlow(component, helper, iStep)) {
                    firstStep = iStep;
                    break;
                }
            }
        }
        return firstStep;
    },
    getPreviousStep: function (component, helper, stepTitle) {
        // Returns the previous step
        let stepIndex = this.getIndexFromStepTitle(component, stepTitle);
        let previousStep;

        if ((stepIndex === 0) || (stepIndex - 1) === 0) {
            return this.getStepFromIndex(component, 0);
        }

        for (let i = stepIndex - 1; i >= 0; i--) {
            let iStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(iStep.Title)) {
                if (helper.isPartOfCurrentFlow(component, helper, iStep)) {
                    previousStep = iStep;
                    break;
                }
            }
        }
        return previousStep;
    },
    goToNextStep: function (component, helper) {
        // Activates the next step

        let currentStepTitle = component.get("v.step");
        let nextStep = helper.getNextStep(component, helper, currentStepTitle);

        helper.setCurrentStep(component, helper, nextStep.Title);
        helper.onNewStep(component, helper);
    },
    onNewStep: function (component, helper) {
        // An event after a step has been selected, a nice place to do after selection checks, jumps etc
        let currentStepTitle = component.get("v.step");
        switch (currentStepTitle) {
            case component.get("v.steps").SET_COMPANY_DIVISION.Title:
            case component.get("v.steps").SHOW_SCRIPT_MANAGEMENT.Title:
            case component.get("v.steps").CHECK_PENALTIES.Title:
                helper.btnNextClickHandler(component, helper);
                break;
        }
    },
    // End of the steps api methods.
    initialize: function (component, helper) {
        helper.stepsInit(component, helper);

        let self = this;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        let myPageRef = component.get("v.pageReference");
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let accountId = myPageRef.state.c__accountId;
        let templateId = myPageRef.state.c__templateId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;

        component.set("v.genericRequestId", genericRequestId);
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);

        helper.showSpinner(component);

        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
            })
            .setResolve(function (response) {
                if (response.opportunityId && !opportunityId) {
                    // when wizard is first opened, clean cache from previous runs
                    helper.cleanPersistentAttributes();
                    helper.resetServiceSupply(component, helper);

                    // Navigate to full url with dossier, opportunity and co
                    self.updateUrl(component, accountId, response.opportunityId, response.dossierId, templateId);
                    return;
                }

                component.set("v.opportunity", response.opportunity);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
                component.set("v.useBit2WinCart", response.useBit2WinCart);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.existingVasStringified", response.existingVas);
                component.set('v.originSelected', response.opportunity.Origin__c);
                component.set('v.channelSelected', response.opportunity.Channel__c);
                component.set("v.commodity", response.commodity);
                component.set("v.commodityProductStringified", response.commodityProduct);
                component.set("v.account", response.account);
                component.set("v.accountId", response.accountId);
                component.set("v.isGdprReadOnly", component.get("v.account").IsPersonAccount === false);
                component.set("v.startedBy", response.startedBy);
                component.set("v.salesman", response.salesman);
                component.set("v.salesUnit", response.salesUnit);
                component.set("v.salesUnitId", response.salesUnit);
                component.set("v.user", response.user);
                component.set("v.userDepartment", response.userDepartment);
                if(component.get("v.userDepartment")){
                    component.set("v.salesUnitId", component.get("v.userDepartment"));
                    component.set("v.salesUnit", component.get("v.userDepartment"));
                    component.set("v.showUserDepartment", true);
                }

                helper.setCurrentFlow(component, helper, response.currentFlow);
                helper.checkTheCommodityProductName(component, helper, response.commodityProduct);
                helper.selectInitiativeType(component, helper);
                helper.selectCompanyDivision(component, helper, response.opportunityCompanyDivisionId);

                // This is to determine whether the selected product is a bundle
                if (!helper.isNullOrEmpty(response.opportunityLineItems) && (helper.isActivation(component) || helper.isSubstitution(component))) {
                    helper.checkTheProductName(component, helper, response.opportunityLineItems[0]);
                }

                // Set contract fields
                if (!helper.isNullOrEmpty(response.contract)) {
                    component.set("v.contractId", response.contract.Id);
                    component.set("v.contractType", response.contract.ContractType__c);
                    component.set("v.assetId", response.assetId);
                }

                if(response.salesUnitAccount && !component.get("v.salesUnit")){
                    component.set("v.salesUnit", response.salesUnitAccount);
                    if(component.get("v.salesUnit").SalesDepartment__c === component.get("v.channelSelected")) {
                        let salesUnitAccountId = component.get("v.salesUnit").Id;
                        component.set("v.salesUnitId", salesUnitAccountId);
                    }
                }

                if(component.get("v.channelSelected") !== "Indirect Sales" && !component.get("v.companySignedId")){
                    if(component.get("v.channelSelected") === "Direct Sales (KAM)" && !component.get("v.salesUnit")){
                        component.set("v.companySignedId", "");
                    }
                    else{
                        let userId = component.get("v.user").Id;
                        component.set("v.companySignedId", userId);
                    }
                }

                helper.setFiltrationParams(component, helper);
                helper.updateSearchConditions(component, helper);
                helper.restoreCurrentStep(component, helper);
                helper.restoreLocalState(component, helper);

                // Scroll to the top
                let topElement = component.find("topElement").getElement();
                if (topElement) {
                    topElement.scrollIntoView({behavior: "instant", block: "end"});
                }

                // Loading finished
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);

                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    monthsBetweenDates: function (d1, d2) {
        let months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    },
    restoreCurrentStep: function (component, helper) {
        const commoditySupplyId = component.get("v.commoditySupplyId");
        const contractId = component.get("v.contractId");
        const oliList = component.get("v.opportunityLineItems");
        const currentFlow = component.get("v.currentFlow");
        const initiativeType = component.get("v.startedBy");

        let currentStepTitle = component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title;

        if (!helper.isNullOrEmpty(oliList)) {
            currentStepTitle = component.get("v.steps").SET_PRODUCTS.Title;

        } else if (!helper.isNullOrEmpty(commoditySupplyId)) {
            currentStepTitle = component.get("v.steps").SET_SUPPLY_TO_ACTIVATE.Title;

        } else if (!helper.isNullOrEmpty(initiativeType)) {
            currentStepTitle = component.get("v.steps").SET_STARTED_BY.Title;

        } else if (!helper.isNullOrEmpty(currentFlow)) {
            currentStepTitle = component.get("v.steps").SET_TYPE_OF_WORK.Title;

        } else if (component.get("v.originSelected") && component.get("v.channelSelected")) {
            currentStepTitle = component.get("v.steps").SET_TYPE_OF_WORK.Title;
        }

        // Save the current step
        helper.setCurrentStep(component, helper, currentStepTitle);

        // Check whether to remain on current step
        if (helper.isNullOrEmpty(currentFlow) ||
            currentStepTitle === helper.getFirstStep(component, helper).Title ||
            currentStepTitle === component.get("v.steps").SET_PRODUCTS.Title) {
            return;
        }

        // Go to the next step
        helper.goToNextStep(component, helper);
    },
    cancelEverything: function (component, helper, cancelReason, detailsReason) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let dossierId = component.get("v.dossierId");
        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId");
        let assetId = component.get("v.assetId");

        helper.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId,
                opportunityId: opportunityId,
                contractId: contractId,
                assetId: assetId,
                cancelReason: cancelReason,
                detailsReason: detailsReason
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
                helper.redirectToDossier(component, helper);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateCurrentFlow: function (component, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        const opportunityId = component.get("v.opportunityId");
        const currentFlow = component.get("v.currentFlow");
        const dossierId = component.get("v.dossierId");

        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateCurrentFlow')
            .setInput({
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "currentFlow": currentFlow,
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                if (!helper.isNullOrEmpty(response.contractId)){
                    helper.savePersistentAttribute(component, helper, "contractId", response.contractId);
                }

                if (!helper.isNullOrEmpty(response.assetId)){
                    helper.savePersistentAttribute(component, helper, "assetId", response.assetId);
                }

                // If current flow has been changed, previously chosen supply's information has to be removed
                helper.resetServiceSupply(component, helper);

                // Reload with clean cache on first start
                $A.get('e.force:refreshView').fire();
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    setFiltrationParams: function (component, helper) {
        let newSupplyColumns = [];

        let existingSupplyColumns = [
            {key: 'RecordType.Name', label: 'Type'},
            {key: 'Name', label: 'Supply'},
            {key: 'Status__c', label: 'Status'},
            {key: 'ContractAccount__r.Name', label: 'Contract Account'},
            {key: 'Contract__r.ContractNumber', label: 'Contract'},
            {key: 'ActivationDate__c', label: 'Activation Date'},
            {key: 'TerminationDate__c', label: 'Termination Date'},
            {key: 'Product__r.Name', label: 'Product'}
        ];

        component.set("v.newSupplyColumns", newSupplyColumns);
        component.set("v.existingSupplyColumns", existingSupplyColumns);
    },
    setCurrentFlow: function (component, helper, currentFlow) {
        let typeOfWork = $A.get('$Label.c.SelectTypeOfWork');
        component.set("v.currentFlow", currentFlow);
        let flows = JSON.parse(JSON.stringify(component.get("v.flows")));
        switch (currentFlow) {
            case flows.ACTIVATION:
                typeOfWork = $A.get('$Label.c.ActivationOfNewVas');
                break;
            case flows.TERMINATION:
                typeOfWork = $A.get('$Label.c.TerminationOfAnExistingSoftVas');
                break;
            case flows.SUBSTITUTION:
                typeOfWork = $A.get('$Label.c.ActivationOfNewVasInSubstitution');
                break;
            default:
                component.set("v.currentFlow", '');
        }
        component.find("typeOfWork").set("v.value", typeOfWork);
    },
    isTermination: function (component) {
        return component.get("v.currentFlow") === component.get("v.flows").TERMINATION;
    },
    isActivation: function (component) {
        return component.get("v.currentFlow") === component.get("v.flows").ACTIVATION;
    },
    isSubstitution: function (component) {
        return component.get("v.currentFlow") === component.get("v.flows").SUBSTITUTION;
    },
    checkCommoditySupply: function (component, helper, commoditySupplyId) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        const dossierId = component.get('v.dossierId');
        const contractId = helper.loadPersistentAttribute(component, helper, 'contractId');
        const currentFlow = component.get('v.currentFlow');
        const opportunityId = component.get('v.opportunityId');

        helper.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("checkCommoditySupply")
            .setInput({
                supplyId: commoditySupplyId,
                dossierId: dossierId,
                contractId: contractId,
                currentFlow: currentFlow,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                // Set component's attributes
                let commoditySupply = response.commoditySupply;

                helper.savePersistentAttribute(component, helper, "commoditySupplyId", commoditySupply.Id);
                helper.savePersistentAttribute(component, helper, "commodity", commoditySupply.RecordType.Name);

                helper.savePersistentAttribute(component, helper, "contractType", response.contractType);
                helper.savePersistentAttribute(component, helper, "commodityProductStringified", response.commodityProduct);
                helper.savePersistentAttribute(component, helper, "hasAsistenta", response.hasAsistenta);

                // Check if a bundle has been selected
                helper.checkTheCommodityProductName(component, helper, response.commodityProduct);

                // Set current company division on UI element
                helper.selectCompanyDivision(component, helper, commoditySupply.CompanyDivision__r.Id);

                // component.set("v.searchedSupplyId", selectedSupplyId);
                component.set("v.showNewOsi", true);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    setInitiativeType: function (component, helper, startedBy) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        helper.showSpinner(component);

        component.find("apexService").builder()
            .setMethod("setInitiativeType")
            .setInput({
                dossierId: component.get('v.dossierId'),
                startedBy: startedBy,
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
                // Set component's attributes
                component.set("v.startedBy", startedBy);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    selectInitiativeType: function (component, helper) {
        let startedBy = component.get("v.startedBy");

        if (helper.isNullOrEmpty(startedBy)) {
            return;
        }

        // Select the UI element
        let startedBySelector = component.find("startedBySelector");
        if (!startedBySelector) {
            console.error("couldn't find startedBySelector component");
            return;
        }

        startedBySelector.set("v.value", startedBy);
    },
    selectCompanyDivision: function (component, helper, companyDivisionId) {
        if (component.get("v.currentFlow") === "") {
            return;
        }

        // Check the input
        if (!companyDivisionId) {
            console.log("no companyDivisionId has been provided");
            return;
        }

        // Set the company division id
        component.set("v.companyDivisionId", companyDivisionId);

        // Select the company division in its UI
        let companyDivisionComponent = component.find("companyDivision");
        if (!companyDivisionComponent) {
            console.error("couldn't find company division component");
            return;
        }
        companyDivisionComponent.setCompanyDivision();
    },
    renderExistingData: function (component, helper, oliList, osiList, product) {
        component.set("v.opportunityLineItems", oliList);
        component.set("v.opportunityServiceItems", osiList);
        if (helper.isNullOrEmpty(product)) {
            return;
        }
        let productProcessed = JSON.stringify(product);
        component.set("v.selectedProductStringified", productProcessed);
    },
    isNullOrEmpty: function (variable) {
        return (variable === undefined || variable === null || variable.length === 0);
    },
    updateSearchConditions: function (component, helper) {
        let supplyToTerminateSearchConditions = [];
        let supplyToActivateSearchConditions = [];
        supplyToTerminateSearchConditions.push(`(Account__c='${component.get('v.accountId')}')`);
        supplyToActivateSearchConditions.push(`(Account__c='${component.get('v.accountId')}')`);
        supplyToTerminateSearchConditions.push(`(RecordType.DeveloperName='Service')`);
        supplyToActivateSearchConditions.push(`(RecordType.DeveloperName='Electric' OR RecordType.DeveloperName='Gas')`);

        if (helper.isSubstitution(component) && !helper.isNullOrEmpty(component.get("v.selectedToTerminateSupplyStringified"))) {
            let supplyToTerminate = JSON.parse(component.get("v.selectedToTerminateSupplyStringified"));
            if (supplyToTerminate.ServiceSite__r) {
                supplyToActivateSearchConditions.push(`(ServiceSite__c='${supplyToTerminate.ServiceSite__r.Id}')`);
            }
        }

        component.set("v.existingSupplySearchConditions", supplyToTerminateSearchConditions);
        component.set("v.newSupplySearchConditions", supplyToActivateSearchConditions);
    },
    btnNextClickHandler: function (component, helper) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let currentStepTitle = component.get("v.step");

        switch (currentStepTitle) {
            case component.get("v.steps").SET_COMPANY_DIVISION.Title:
                break;
            case component.get("v.steps").SET_TYPE_OF_WORK.Title:
                if (component.get("v.currentFlow") === '') {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SetTypeOfWorkErrorMsg"));
                    return;
                }
                helper.updateCurrentFlow(component, helper);
                helper.updateSearchConditions(component, helper);
                break;
            case component.get("v.steps").SET_SUPPLY_TO_TERMINATE.Title:
                if (helper.isTermination(component) && component.get("v.isProductBundle") === true) {
                    console.error("The product is a bundle");
                    ntfSvc.error(ntfLib, $A.get("$Label.c.BundleProductErrorMsg"));
                    return;
                }
                if (component.get("v.selectedToTerminateSupplyId") === '') {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredSupply"));
                    return;
                }
                if (helper.isSubstitution(component)) {
                    helper.updateSearchConditions(component, helper);
                }
                break;
            case component.get("v.steps").SET_SUPPLY_TO_ACTIVATE.Title:
                if (helper.isNullOrEmpty(component.get("v.opportunityServiceItems"))) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredSupply"));
                    return;
                }

                const isCommodityProductBundle = component.get("v.isCommodityProductBundle");
                // Check if the commodity's product is a bundle and warn about it if it does
                if (isCommodityProductBundle) {
                    const commodityProductStringified = component.get("v.commodityProductStringified");
                    if (!commodityProductStringified) {
                        console.warn(`isCommodityProductBundle is true, but commodityProduct is null`);
                        break;
                    }
                    const commodityProduct = JSON.parse(JSON.stringify(commodityProductStringified));
                    const warningMessage = $A.get("$Label.c.WarningCommodityProductIsBundle").replace("{!PRODUCT}", commodityProduct.Name);
                    ntfSvc.warn(ntfLib, warningMessage);
                }
                break;
            case component.get("v.steps").SET_PRODUCTS.Title:
                let prdList = component.get("v.opportunityLineItems");
                if (!prdList && prdList.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct"));
                    return;
                }

                let utilityPrds = prdList.filter(
                    oli => oli.Product2.RecordType.DeveloperName === "Utility"
                );
                if (!utilityPrds || utilityPrds.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct"));
                    return;
                }

                let oli = utilityPrds[0];

                if (component.get("v.isProductBundle") === true) {
                    console.error("The product is a bundle");
                    ntfSvc.error(ntfLib, $A.get("$Label.c.CommodityHasBundleProduct"));
                    return;
                }

                if (!helper.isNullOrEmpty(component.get("v.existingVasStringified"))) {
                    console.error("There is already a service for such commodity and product");
                    ntfSvc.error(ntfLib, `${$A.get("$Label.c.ErrorTheProductIsAlreadyActivated")} - Supply: ${component.get("v.existingVasStringified").Name}`);
                    return;
                }

                helper.processSelectedOli(component, helper, oli);
                break;
            case component.get("v.steps").SET_SENDING_CHANNEL.Title:
                helper.saveSendingChannel(component);
                break;
            case component.get("v.steps").SET_CONTRACT.Title:
                let contractFields = helper.getContractFields(component);
                if (helper.isNullOrEmpty(contractFields.type)) {
                    console.warn($A.get("$Label.c.InvalidContractType"));
                }

                if (!helper.updateContractInfo(component, helper)){
                    return;
                }
                break;
            case component.get("v.steps").SET_ADDITIONAL_INFO.Title:
                let infoFieldDate = "";
                let infoField1 = "";

                let showCarDetailsFields = component.get("v.showCarDetailsFields");

                infoFieldDate = helper.getInputFieldValue(component, helper, 'ManufacturingDate__c');
                infoField1 = helper.getInputFieldValue(component, helper, 'NE__Text_Extension_1__c');

                if (showCarDetailsFields){
                    if (helper.isNullOrEmpty(infoFieldDate) || helper.isNullOrEmpty(infoField1)) {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.FillAdditionalInfoFields"));
                        return;
                    }

                    // Checks if the selected manufacturing date is older than 10 years
                    const maxDifferenceInMonths = 12 * 10;
                    const actualDifferenceInMonths = helper.monthsBetweenDates(new Date(infoFieldDate), new Date());
                    if (actualDifferenceInMonths > maxDifferenceInMonths) {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ManufacturingDateIsTooOld"));
                        return;
                    }
                }

                if (!helper.isNullOrEmpty(infoFieldDate) && (new Date(infoFieldDate) > new Date())) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ManufacturingDateIsInFuture"));
                    return;
                }

                if (!helper.isNullOrEmpty(infoField1) && (infoField1.length !== 17)) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ChassisNumberMustBe17Symbols"));
                    return;
                }

                let infoField2 = helper.getInputFieldValue(component, helper, 'NE__Text_Extension_2__c');
                let infoField3 = helper.getInputFieldValue(component, helper, 'NE__Text_Extension_3__c');
                let infoField4 = helper.getInputFieldValue(component, helper, 'NE__Text_Extension_4__c');

                component.set("v.additionalInfoMap", {
                    'infoField1': infoField1,
                    'infoField2': infoField2,
                    'infoField3': infoField3,
                    'infoField4': infoField4,
                    'infoFieldDate': infoFieldDate
                });
                break;
            case component.get("v.steps").SET_TERMINATION_DETAILS.Title:
                const noPenaltyOptions = [$A.get("$Label.c.UpgradeOrDowngrade"), $A.get("$Label.c.SwitchToBundleProduct"), $A.get("$Label.c.DoesNotRecognizeTheContract")];
                let terminationReason = component.find("terminationReason").get("v.value");
                if (helper.isNullOrEmpty(terminationReason)) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectTerminationReason"));
                    return;
                }
                if (noPenaltyOptions.includes(terminationReason)){
                    component.set("v.appliedPenalties", false);
                    component.set("v.expiresIn", null);
                } else {
                    helper.checkForAppliedPenalties(component, helper);
                }
                break;
            case component.get("v.steps").SELECT_INVOICES.Title:
                component.find("invoicesInquiryComponent").getSelectedInvoices();

                let markedInvoiceIds = component.get("v.markedInvoiceIds");
                let selectedInvoiceIds = component.get("v.selectedInvoiceIds");
                break;
            case component.get("v.steps").SET_STARTED_BY.Title:
                let startedBy = component.find('startedBySelector').get('v.value');

                if (startedBy === '') {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.SelectInitiativeType'));
                    return;
                }

                helper.setInitiativeType(component, helper, startedBy);
                break;
        }

        // Set up new state
        helper.goToNextStep(component, helper);
    },
    getInputFieldValue: function (component, helper, fieldId) {
        return Array.isArray(component.find(fieldId)) ? component.find(fieldId)[0].get("v.value") : component.find(fieldId).get("v.value");
    },
    setChannelAndOrigin: function (component, helper) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');

        if (!opportunity || !origin || !channel) {
            console.log(`prevented illegal call to setChannelAndOrigin`);
            return;
        }

        helper.showSpinner(component);

        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId: dossierId,
                origin: origin,
                channel: channel
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
                console.info("set channel and origin request has successfully completed");
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    saveOpportunity: function (component, helper, stage, callback) {
        let self = this;
        let opportunityId = component.get("v.opportunity").Id;
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");

        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let dossierId = component.get("v.dossierId");
        let terminationReason = "";
        let commodity = "";
        let appliedPenalties = component.get("v.appliedPenalties");
        let salesUnitId = helper.isNullOrEmpty(component.get("v.salesUnitId")) ? component.get("v.salesUnitId") : null;
        let companySignedBy = helper.isNullOrEmpty(component.get("v.companySignedId")) ? component.get("v.companySignedId") : null;

        if (helper.isTermination(component)) {
            terminationReason = component.find("terminationReason").get("v.value");
        }

        let selectedToTerminateSupplyId = component.get("v.selectedToTerminateSupplyId");
        const additionalInfo = (helper.isActivation(component) || helper.isSubstitution(component)) ? JSON.stringify(component.get("v.additionalInfoMap")) : null;

        const currentFlow = component.get("v.currentFlow");
        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "currentFlow": currentFlow,
                "dossierId": dossierId,
                "selectedToTerminateSupplyId": selectedToTerminateSupplyId,
                "additionalInfo": additionalInfo,
                "terminationReason": terminationReason,
                "salesUnitId": salesUnitId,
                "companySignedBy": companySignedBy,
                "commodity": component.get("v.commodity"),
                "stage": stage,
                "appliedPenalties": appliedPenalties
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                if (response.warning) {
                    ntfSvc.warn(ntfLib, response.warningMsg);
                }

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    getContractFields: function (component) {
        return JSON.parse(JSON.stringify(component.find("contractSelectionComponent").getNewContractDetails()));
    },
    getComponent: function (component, helper, inputName) {
        return Array.isArray(component.find(inputName)) ? component.find(inputName)[0] : component.find(inputName);
    },
    setServiceSupply: function (component, helper, selectedSupply){
        const vasContractType = selectedSupply.Contract__r.ContractType__c;
        if (vasContractType){
            component.set("v.contractType", vasContractType);
            helper.savePersistentAttribute(component, helper, "contractType", vasContractType);
        }
        helper.savePersistentAttribute(component, helper, "selectedToTerminateSupplyId", selectedSupply.Id);
        helper.savePersistentAttribute(component, helper, "selectedToTerminateSupplyStringified", JSON.stringify(selectedSupply));
        helper.savePersistentAttribute(component, helper, "selectedProductStringified", "");
        helper.savePersistentAttribute(component, helper, "companyDivisionId", selectedSupply.CompanyDivision__c);
    },
    handleServiceSupplySelection: function (component, helper, serviceSupplyId) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        let contractId = component.get("v.contractId");
        helper.showSpinner(component);

        component
            .find("apexService")
            .builder()
            .setMethod("handleServiceSupplySelection")
            .setInput({
                opportunityId: opportunityId,
                contractId: contractId,
                serviceSupplyId: serviceSupplyId
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateContractInfo: function (component, helper) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId");

        let contractFields = helper.getContractFields(component);
        if (contractFields === null){
            ntfSvc.error(ntfLib, $A.get('$Label.c.ContractFieldsAreEmpty'));
            return false;
        }

        if (component.get("v.channelSelected") === "Indirect Sales"){
            let contractSelectionComponent = helper.getComponent(component, helper, "contractSelectionComponent");
            if (!contractSelectionComponent.validate()){
                ntfSvc.error(ntfLib, $A.get('$Label.c.ContractFieldsValidationFailed'));
                return false;
            }
        }

        helper.showSpinner(component);
        component
            .find("apexService")
            .builder()
            .setMethod("updateContractInfo")
            .setInput({
                opportunityId: opportunityId,
                contractFields: JSON.stringify(contractFields),
                contractId: contractId
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();

        return true;
    },
    updateUrl: function (component, accountId, opportunityId, dossierId, templateId) {
        let self = this;
        const {isConsoleNavigation} = component.find("workspace");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_PartnerServiceWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };

        self.redirect(component, pageReference, true);
    },
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        let pageReference;

        if (useBit2WinCart) {
            let osiList = component.get("v.opportunityServiceItems");
            let isGas = false;
            let isElectric = false;
            let isService = false;
            for (let osi of osiList) {
                if (osi.RecordType.DeveloperName === 'Gas') {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === 'Service') {
                    isService = true;
                } else if (osi.RecordType.DeveloperName === 'Electric') {
                    isElectric = true;
                }
            }
            let productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';
            }

            const hasAsistenta = helper.loadPersistentAttribute(component, helper, "hasAsistenta");

            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__currentFlow": component.get("v.currentFlow"),
                    "c__currentStepTitle": component.get("v.step"),
                    "c__templateId": component.get("v.templateId"),
                    "c__productType": productType,
                    "c__hasAsistenta": hasAsistenta,
                }
            };
        } else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, pageReference, false);
    },
    redirectToDossier: function (component, helper) {
        helper.cleanPersistentAttributes();

        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'Dossier__c',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference, false);
    },
    redirectToOppty: function (component, helper) {
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference, false);
    },
    checkTheCommodityProductName: function (component, helper, product) {
        // reset the previously chosen product data
        component.set("v.isCommodityProductBundle", false);

        // check if the product is set and whether it can be a bundle or need additional information fields
        if (product === undefined) {
            return;
        }
        const productName = product.Name;
        if (productName.length < 5) {
            return;
        }

        // determine whether the product is a bundle or not
        const isBundleProduct = productName.toLowerCase().includes('bundle');
        component.set("v.isCommodityProductBundle", isBundleProduct);
    },
    checkTheProductName: function (component, helper, product) {
        // reset the previously chosen product data
        component.set("v.isProductBundle", false);
        component.set("v.isExtendedProduct", false);

        // check if the product is set and whether it can be a bundle or need additional information fields
        if (product === undefined) {
            return;
        }
        const productName = product.Name;
        if (productName.length < 5) {
            return;
        }

        // determine whether the product is a bundle or not
        const isBundleProduct = productName.toLowerCase().includes('bundle');
        component.set("v.isProductBundle", isBundleProduct);

        // determine whether the product can have additional information or not
        //const extendedProduct = (productName.substr(productName.length - 4).toLowerCase() === "plus" || productName.substr(productName.length - 1) === "+");
        const extendedProduct = !helper.isNullOrEmpty(product.Product2.CommercialProduct__r.AdditionalServiceDetails__c);
        component.set("v.isExtendedProduct", extendedProduct);
    },
    checkForAppliedPenalties: function (component, helper) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let self = this;

        let dossierId = component.get("v.dossierId");
        let supplyToTerminateId = component.get("v.selectedToTerminateSupplyId")
        let currentFlow = component.get("v.currentFlow");

        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("appliedPenalties")
            .setInput({
                "supplyToTerminateId": supplyToTerminateId,
                "dossierId": dossierId,
                "currentFlow": currentFlow
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
                let appliedPenalties = response.appliedPenalties;
                let monthsWithPenalties = response.monthsWithPenalties;

                component.set("v.appliedPenalties", appliedPenalties);
                component.set("v.expiresIn", monthsWithPenalties);
            })
            .setReject(function (response) {
                helper.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateCompanyDivisionOnOpportunity: function (component, helper) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        helper.showSpinner(component);
        component
            .find("apexService")
            .builder()
            .setMethod("updateCompanyDivisionInOpportunity")
            .setInput({
                opportunityId: opportunityId,
                dossierId: dossierId,
                companyDivisionId: component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
            })
            .setReject(function (error) {
                helper.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    afterNewOsi: function (component, helper, osiId) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let self = this;
        helper.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("afterNewOsi")
            .setInput({
                "osiId": osiId,
                "opportunityId": component.get("v.opportunityId"),
                "accountId": component.get("v.accountId"),
                "commoditySupplyId": component.get("v.commoditySupplyId")
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;

                osiList.push(newOsi);
                if (osiList.length === 1) {
                    self.updateCompanyDivisionOnOpportunity(component, helper);
                }

                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                component.set("v.opportunityServiceItemId", osiId);
            })
            .setReject(function (response) {
                helper.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    checkStrings: function (str1, str2){
        let str1ToCompare = str1.toLowerCase();
        str1ToCompare = str1ToCompare.replaceAll(' ', '');

        let str2ToCompare = str2.toLowerCase();
        str2ToCompare = str2ToCompare.replaceAll(' ', '');

        return (str1ToCompare === str2ToCompare);
    },
    processSelectedOli: function (component, helper, oli, callback) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        const carServiceProductName = 'Enel Asistenta+';
        if (!oli || !oli.Product2 || !oli.Product2.Name){
            ntfSvc.error(ntfLib, $A.get("$Label.c.MissingProductInformation"));
        }
        //else if (helper.checkStrings(oli.Product2.Name, carServiceProductName)){
        else if (!helper.isNullOrEmpty(oli.Product2.CommercialProduct__r.AdditionalServiceDetails__c) && helper.checkStrings(oli.Product2.CommercialProduct__r.AdditionalServiceDetails__c, 'Car')){
            component.set("v.showCarDetailsFields", true);
        }

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("processSelectedOli")
            .setInput({
                "opportunityLineItemId": oli.Id,
                "opportunityId": component.get("v.opportunityId"),
                "dossierId": component.get("v.dossierId")
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component);

                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction()
    },
    /**
     * Console Navigation and Redirection Utilities
     * @author Baba Goudiaby
     */
    redirect: function (component, pageReference, overrideUrl) {
        const navService = component.find("navService");
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.error(JSON.parse(JSON.stringify(error)));
                        });
                    });
                } else {
                    // Clean cache on first start
                    let navPromise = navService.navigate(pageReference, true);
                    navPromise && navPromise.then();
                }
            }).catch(function (error) {
            console.error(JSON.parse(JSON.stringify(error)));
        });
    },
    toPlainObject: function (input) {
        if (typeof input !== 'object' || input === null) {
            console.error("the input is not an object!");
            return null;
        }
        return JSON.parse(JSON.stringify(input));
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 3) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    closeFocusedTab: function (component, event, helper) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const {getFocusedTabInfo, refreshTab} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
    savePersistentAttribute: function (component, helper, name, value) {
        // This is a workaround for automatic cleaning of attributes on page refresh
        let attrName = "v." + name;

        if (helper.isNullOrEmpty(value)){
            return;
        }

        component.set(attrName, value);
        window.sessionStorage.setItem(name, JSON.stringify(value));
    },
    loadPersistentAttribute: function (component, helper, name) {
        // This is a workaround for automatic cleaning of attributes on page refresh
        let returnValue = "";
        let attrName = "v." + name;

        // Give priority to session storage values when loading
        let valueInSessionStorage = window.sessionStorage.getItem(name);
        if (!helper.isNullOrEmpty(valueInSessionStorage)){
            returnValue = JSON.parse(valueInSessionStorage);
            component.set(attrName, returnValue);
            return returnValue;
        }

        let valueInComponentStorage = component.get(attrName);
        if (!helper.isNullOrEmpty(valueInComponentStorage)) {
            returnValue = valueInComponentStorage;
            return returnValue;
        }

        return returnValue;
    },
    cleanPersistentAttributes: function (){
        let n = window.sessionStorage.length;
        while(n--) {
            let key = window.sessionStorage.key(n);
            window.sessionStorage.removeItem(key);
        }
    },
    deletePersistentAttribute: function (component, helper, name) {
        // This is a workaround for automatic cleaning of attributes on page refresh
        let attrName = "v." + name;

        component.set(attrName, "");
        window.sessionStorage.removeItem(name);
    },
    restoreLocalState: function (component, helper){
        helper.loadPersistentAttribute(component, helper, "commoditySupplyId");
        helper.loadPersistentAttribute(component, helper, "commodity");
        helper.loadPersistentAttribute(component, helper, "assetId");
        helper.loadPersistentAttribute(component, helper, "selectedToTerminateSupplyId");
        helper.loadPersistentAttribute(component, helper, "selectedToTerminateSupplyStringified");
        helper.loadPersistentAttribute(component, helper, "selectedProductStringified");
        helper.loadPersistentAttribute(component, helper, "companyDivisionId");
        helper.loadPersistentAttribute(component, helper, "contractType");
        helper.loadPersistentAttribute(component, helper, "commodityProductStringified");
        helper.loadPersistentAttribute(component, helper, "hasAsistenta");
    },
    resetServiceSupply: function (component, helper){
        helper.deletePersistentAttribute(component, helper, "contractType");
        helper.deletePersistentAttribute(component, helper, "selectedToTerminateSupplyId");
        helper.deletePersistentAttribute(component, helper, "selectedToTerminateSupplyStringified");
        helper.deletePersistentAttribute(component, helper, "selectedProductStringified");
        helper.deletePersistentAttribute(component, helper, "companyDivisionId");
    }
});