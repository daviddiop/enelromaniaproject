({
    handleTypeOfWorkSelection: function (component, event, helper) {
        let currentFlow = component.find('typeOfWork').get('v.value');
        helper.setCurrentFlow(component, helper, currentFlow);
    },
    handleStartedBySelection: function (component, event, helper) {

    },
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;

        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        component.set("v.dossierId", dossierId);

        helper.initialize(component, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.PartnerService"));
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
    },
    saveDraft: function (component, event, helper) {
        component.set("v.savingWizard", false);
        helper.saveOpportunity(component, helper, "Qualification", helper.redirectToOppty);
    },
    save: function (component, event, helper) {
        component.set("v.savingWizard", true);
        helper.saveOpportunity(component, helper, "Quoted", helper.redirectToDossier);
    },
    onSaveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        if (cancelReason) {
            component.set("v.savingWizard", false);
            helper.cancelEverything(component, helper, cancelReason, detailsReason);
        }
    },
    nextStep: function (component, event, helper) {
        helper.btnNextClickHandler(component, helper);
    },
    editStep: function (component, event, helper) {
        // this is the same as the step for this setting
        let stepToEditTitle = event.getSource().getLocalId();

        let previousStep = helper.getPreviousStep(component, helper, stepToEditTitle);
        let stepToEditIndex = helper.getIndexFromStepTitle(component, stepToEditTitle);

        component.set("v.step", stepToEditTitle);
        component.set("v.currentStepIndex", stepToEditIndex);

        let stepIndexes = JSON.parse(JSON.stringify(component.get("v.stepIndexesMap")));

        switch (stepToEditTitle) {
            case component.get("v.steps").FINISH.Title:
                break;
            default:
                break;
        }
    },
    handleOriginChannelSelection: function (component, event, helper) {
        // values from UI
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');

        if (!channelSelected || !originSelected) {
            return;
        }

        // activate type of work workaround
        let currentStepIndex = component.get("v.currentStepIndex");
        let setTypeOfWorkStepIndex = component.get("v.stepIndexesMap").SET_TYPE_OF_WORK;

        if (currentStepIndex < setTypeOfWorkStepIndex){
            helper.goToNextStep(component, helper);
        }

        // current values from opportunity
        let currentOrigin = component.get('v.originSelected');
        let currentChannel = component.get('v.channelSelected');

        if (channelSelected === currentChannel && originSelected === currentOrigin) {
            // no changes
            helper.restoreCurrentStep(component, helper);
        } else {
            // one of the values has been changed
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);

            helper.setChannelAndOrigin(component, helper);
        }

        if (!currentChannel || !currentOrigin) {
            // setting for the first time
            component.set("v.step", helper.getFirstStep(component, helper).Title);
            helper.btnNextClickHandler(component, helper);
        }
    },
    handleNewOsi: function (component, event, helper) {
        const osiId = event.getParam("opportunityServiceItemId");
        helper.afterNewOsi(component, helper, osiId);
    },
    handleOsiDelete: function (component, event, helper) {
        let osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        let items = [];
        for (let i = 0; i < osiList.length; i++) {
            if (osiList[i].Id !== osiId) {
                items.push(osiList[i]);
            }
        }
        component.set("v.opportunityServiceItems", items);
        component.set(
            "v.osiTableView",
            items.length > component.get("v.tileNumber")
        );
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedPointId", "");
        component.set("v.searchedPointCode", "");
        //component.find("pointSelection").resetBox();
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(component, helper, "Qualification", helper.openProductSelection);
    },
    handleSavedSendingChannelSelected: function (component, event, helper) {
        console.log('sending channel is selected');
    },
    handleSavedSendingChannel: function (component, event, helper) {
        console.log('handleSavedSendingChannel');
    },
    handleSupplyToActivateSelection: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedCommoditySupply = event.getParam("selectedObject");
        helper.checkCommoditySupply(component, helper, selectedCommoditySupply.Id);
    },
    handleSupplyToTerminateSelection: function (component, event, helper) {
        const selectedSupply = event.getParam("selectedObject");

        helper.setServiceSupply(component, helper, selectedSupply);

        if (helper.isSubstitution(component)) {
            helper.handleServiceSupplySelection(component, helper, selectedSupply.Id);
        }

        helper.updateCompanyDivisionOnOpportunity(component, helper);
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            if (event.getParam("divisionId")) {
                component.set("v.companyDivisionId", event.getParam("divisionId"));
                component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));
            }
        } catch (err) {
            console.error('Error on aura:PartnerServiceWizard :', err);
        }
    },
    handleInvoiceSelect: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let markedInvoiceList = event.getParam("markedInvoiceList");
        let selectedInvoiceList = event.getParam("selectedInvoiceList");

        let selectedInvoiceIds = [];
        Object.keys(selectedInvoiceList).forEach(function (key) {
            selectedInvoiceIds.push(selectedInvoiceList[key].invoiceId);
        });

        let markedInvoiceIds = [];
        Object.keys(markedInvoiceIds).forEach(function (key) {
            markedInvoiceIds.push(markedInvoiceList[key].invoiceId);
        });

        if (markedInvoiceIds.length !== 0) {
            component.set("v.markedInvoiceIds", markedInvoiceIds);
        }

        if (selectedInvoiceIds.length !== 0) {
            component.set("v.selectedInvoiceIds", selectedInvoiceIds);
        }
    },
});