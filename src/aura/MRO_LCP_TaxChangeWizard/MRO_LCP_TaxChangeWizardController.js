/**
 * Created by David  on 07.11.2019.
 */

({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        component.set("v.accountId", accountId);
        helper.showSpinner(component);
        helper.initialize(component, event, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.TaxChange"));
        /*if(component.get('v.requestType') === 'select request type'){
            component.set('v.showSelectRequest',true);
        }*/
    /*|| (v.requestType == $Label.c.UpdateRequest )*/
    },
    handleChangeRequestType: function (component, event, helper ){
        let requestValue = event.getSource().get("v.value");
        component.set('v.isDisabledFields',false);
        helper.changeRequestType(component,event,helper,requestValue);
    },
    validRequest: function (component, event, helper){
        component.set('v.showSelectRequest',false);
        helper.showSpinner(component);
        helper.initialize(component, event, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.TaxChange"));
    },

    searchSelectionHandler: function (component, event){

        const supplyIds = event.getParam('selected');
        const taxSubsidy = component.get('v.taxSubsidyTile');

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let errSupplyAlreadySelected = [];
        let errSupplyDifferentMarket= [];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];
        let invalidSuppliesSelected = [];


        component.find('apexService').builder()
            .setMethod("getSupplyRecord")
            .setInput({
                supplyIds: supplyIds
            })
            .setResolve(function (response)
            {
                if (!response.error) {
                    for (let i = 0; i < response.supplies.length; i++) {
                        let validSupply = true;
                        let oneSupply = response.supplies[i];
                        if (response.supplies[i].ServicePoint__r) {
                            if (oneSupply.Id !== response.supplies[i].ServicePoint__r.CurrentSupply__c) {
                                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                                continue;
                            }
                        }
                        if (taxSubsidy) {
                            for (let j = 0; j < taxSubsidy.length; j++) {
                                if (oneSupply.Id === taxSubsidy[j].Supply__c) {
                                    errSupplyAlreadySelected.push(oneSupply.Name);
                                    validSupply = false;
                                    break;
                                }
                            }
                        }
                        if (validSupply) {
                            validSupplies.push(oneSupply);
                        }
                    }
                    if(response.supplies.length > 1){
                        let oneSupply = response.supplies[0];
                        for (let i = 1; i < response.supplies.length; i++) {
                            if(oneSupply.Market__c !== response.supplies[i].Market__c){
                                errSupplyDifferentMarket.push(response.supplies[i].Name);
                                let validSupply = false;
                                validSupplies = [];
                            }
                            if(oneSupply.ServiceSite__c === response.supplies[i].ServiceSite__c){
                                invalidSuppliesSelected.push(response.supplies[i].Name);
                            }
                        }
                    }
                    if (errSupplyAlreadySelected.length !== 0) {
                        let messages = errSupplyAlreadySelected.join(' ');
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
                    }
                    if (errCurrentSupplyIsNotValid.length !== 0) {
                        let messages = errCurrentSupplyIsNotValid.join(' ');
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
                    }
                    if (errSupplyDifferentMarket.length !== 0) {
                        let messages = errSupplyDifferentMarket.join(' ');
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.MixSuppliesFromDifferentMarket") + ' - ' + messages);
                        return;
                    }
                    if (invalidSuppliesSelected.length !== 0) {
                        let messages = invalidSuppliesSelected.join(' ');
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.InvalidSupplyPerServiceSite"));
                        return;
                    }
                    if(response.existeCasePerSupplies){
                        ntfSvc.warn(ntfLib,$A.get("$Label.c.NotExecutedCaseAlreadyExiste"));
                        return;
                    }
                    component.set("v.supply", response.supplies[0]);
                    component.set("v.supplyId", supplyIds[0]);
                    component.set("v.supplyList", response.supplies);
                    component.set("v.showNewCase",(validSupplies.length !== 0));
                }

            })
            .setReject(function (error) {
            })
            .executeAction();
    },
    
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },

    handleClose: function (component, event, helper) {
        //component.set('v.caseId', '');
        component.set('v.taxSubsidyId', '');
        component.set("v.showNewCase", false);
        component.set('v.isDisabledFields',false);
    },
    handleSaveSubvention: function (component, event, helper) {
        component.set("v.showNewCase", false);
        component.set('v.isDisabledFields', false);
        if (component.get('v.requestType') === $A.get("$Label.c.UpdateRequest")) {
            helper.updateTaxSubsidyStatut(component, event, helper);
        } else {
            component.set('v.taxSubsidyId', '');
            helper.initialize(component, event, helper);
        }
        /*if(component.get('v.requestType') === $A.get("$Label.c.UpdateRequest")){
            component.set('v.isNotValid',true);
        }*/
    },
    handleTaxSubsidyDelete: function (component, event, helper) {
        component.set('v.isDisabledFields', false);
        let parentCaseId = event.getParam("parentCaseId");
        console.log('### parentCaseId ' + parentCaseId);
        component.set('v.parentCaseId', parentCaseId);
        helper.updateOldTaxSubsidyStatut(component, event, helper);
        helper.removeTaxSubsidy(component, event, helper);
    },
    handleEditTaxSubsidy: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        component.set('v.taxSubsidyId', event.getParam("recordSubsidyEdit"));
        if(component.get('v.requestType') === $A.get("$Label.c.UpdateRequest")) {
            for(let i = 0; i< component.get('v.taxSubsidyTile').length; i++) {
                if (component.get('v.taxSubsidyTile')[i].Id === event.getParam("recordSubsidyEdit") && component.get('v.taxSubsidyTile')[i].RecordTypeId === component.get('v.heatingSubventionRecordType')) {
                    ntfSvc.warn(ntfLib, $A.get("$Label.c.IntroduceOnlytheFirstMonthOfThatPeriod"));
                }
            }
            for (let i = 0; i < component.get('v.taxSubsidyTilesUpdate').length; i++) {
                if (component.get('v.taxSubsidyTilesUpdate')[i].Id === event.getParam("recordSubsidyEdit")) {
                    component.set('v.parentCaseId', component.get('v.taxSubsidyTilesUpdate')[i].Case__c);
                }
            }
            console.log('###taxSubsidyTilesToUpdate ' + JSON.stringify(component.get('v.taxSubsidyTilesToUpdate')));
            console.log('###recordSubsidyEdit ' + event.getParam("recordSubsidyEdit"));
            for (let i = 0; i < component.get('v.taxSubsidyTilesToUpdate').length; i++) {
                if (component.get('v.taxSubsidyTilesToUpdate')[i].Id === event.getParam("recordSubsidyEdit")) {
                    component.set('v.isTaxToUpdate', true);
                }
            }
            component.set('v.isDisabledFields', true);
        }
        component.set('v.showNewCase', true);
    },

    save: function (component, event, helper) {
        helper.createPrivacyChangeRecord(component);
        //component.find('sendingChannel').saveSendingChannel();
        //helper.saveChain(component, helper);
    },
    saveList: function (component, event, helper) {
        helper.createPrivacyChangeRecord(component);
        //component.find('sendingChannel').saveSendingChannel();
        //helper.updateCaseList(component, helper);
    },

    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");

        let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
        if (!doNotCreatePrivacyChange) {
            component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
            component.set("v.dontProcess", event.getParam("dontProcess"));
        }
        if (component.get("v.requestType") === $A.get("$Label.c.UpdateRequest")) {
            helper.saveChain(component, helper);
        }
        if (component.get("v.requestType") === $A.get("$Label.c.NewRequest")) {
            helper.updateCaseList(component, helper);
        }
    },
    /*handleSuccessDossier: function (component,event,helper){
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        if(selectedChannel ){
            if (component.get("v.requestType") === $A.get("$Label.c.NewRequest")) {
                component.set("v.step", 5);
                let isCreditManagement = component.get('v.isCreditManagement');
                if (isCreditManagement) {
                    component.set("v.step", 6);
                }
            }
            if(component.get("v.requestType") === $A.get("$Label.c.UpdateRequest")){
                component.set("v.step", 6);
            }
        }
    },*/
    cancel: function (component, event, helper) {
       const ntfLib = component.find("notifLib");
       const ntfSvc = component.find("notify");
       if (component.get("v.requestType") === $A.get("$Label.c.NewRequest") && component.get('v.taxSubsidyTile').length > 0) {
                  ntfSvc.warn(ntfLib, $A.get("$Label.c.CancelProcessIsNotYetPossible"));
                  component.set("v.step", 2);
              }
        else if (component.get("v.requestType") === $A.get("$Label.c.UpdateRequest") && component.get('v.taxSubsidyTilesToUpdate').length > 0) {
                        ntfSvc.warn(ntfLib, $A.get("$Label.c.CancelProcessIsNotYetPossible"));
                        component.set("v.step", 3);
              }
        else {
           component.set("v.showCancelBox", true);
           //helper.handleCancel(component, helper);
       }
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
        //helper.handleCancel(component, helper);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'confirmStep0':
                if(!component.get('v.requestType')){
                    return;
                }
                else {
                    component.set("v.step",2);
                }
                break;
            case "confirmStep":
                component.set("v.step", 3);
                break;
            case "confirmStep2":
                if (component.get('v.taxSubsidyTile').length !== 0) {
                    //component.set("v.step", 5);
                    //component.find('sendingChannel').disableInputField(false);
                    if (component.get("v.requestType") === $A.get("$Label.c.NewRequest")) {
                        component.set("v.step", 5);
                        let isCreditManagement = component.get('v.isCreditManagement');
                        if (isCreditManagement) {
                            component.set("v.step", 6);
                        }
                    }
                    if(component.get("v.requestType") === $A.get("$Label.c.UpdateRequest")){
                        component.set("v.step", 6);
                    }
                } else {
                    component.set("v.step", 3);
                }
                break;
                case "confirmStep5":
                component.set("v.step", 6);
                break;
            default:
                break;
        }
    },
    editStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep0") {
            component.set("v.step", 1);
        }
        if (buttonPressed === "returnStep2") {
            component.set("v.step", 3);
        }
        if (buttonPressed === "returnStep5") {
            component.set("v.step", 5);
        }
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            helper.updateDossierAndCase(component,event,helper);
            component.set("v.step", 1);
        } else {
            component.set("v.step", 0);
        }
    },
    handleRetroActiveDate: function (component, event, helper) {
        component.set("v.retroDate", component.find("inputToggle").get("v.checked"));
    },
});