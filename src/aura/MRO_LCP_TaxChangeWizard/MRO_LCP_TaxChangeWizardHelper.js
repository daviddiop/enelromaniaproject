/**
 * Created by David on 07.11.2019.
 */

({
    initialize: function (component, event, helper) {
        const self = this;
        let topElement = component.find("topElement").getElement();
        if(topElement){
            topElement.scrollIntoView({ behavior: "instant",block: "end"});
        }
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        //component.set("v.dossierId", dossierId);
        //const caseId = myPageRef.state.c__caseId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component);

        component.set("v.accountId", accountId);
        if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }
        component.find('apexService').builder()
            .setMethod("initializeTaxChange")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                /*"caseId": caseId,*/
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                'genericRequestId': genericRequestId
            }).setResolve(function (response) {
            if (!response.error) {
                component.set("v.accountId", response.accountId);
                component.set("v.account", response.account);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.taxSubsidyTile",response.subsidyTiles);
                component.set("v.defaultTaxSubsidyTile",response.subsidyTiles);
                component.set("v.taxSubsidyTilesUpdate",response.subsidyTilesUpdate);
                component.set("v.taxSubsidyTilesToUpdate",response.subsidyTilesToUpdate);
                component.set("v.heatingSubventionRecordType",response.heatingSubventionRecordType);
                component.set("v.cases",response.cases);
                //alert(JSON.stringify(response.subsidyTilesUpdate));

                //component.set("v.caseId",response.caseId);
                component.set("v.templateId", response.templateId);
                let creditManagement = response.creditManagementId;

                if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                    self.hideSpinner(component);
                }
                if(response.dossier.Status__c === 'Canceled' || response.dossier.Status__c === 'New'){
                    component.set('v.isClosed',true);
                }
                //let step = 0;
                if(response.subsidyTiles.length !== 0){
                    //step = 1 ;
                    component.set("v.osiTableView", response.subsidyTiles.length > component.get("v.tileNumber"));
                }
                const caseVal = component.get("v.cases");
                if (caseVal["OwnerId"] === creditManagement) {
                    component.set("v.isCreditManagement", true);
                }

                //component.set("v.step", step);

                if(localStorage.getItem(dossierId)  !== null){
                    component.set("v.requestType",localStorage.getItem(dossierId));
                    self.changeRequestType(component,event,helper,component.get('v.requestType'));
                }
                if(response.subsidyTiles.length !== 0) {
                    let caseRequest = response.subsidyTiles[0].Case__r.SubProcess__c;
                    if(caseRequest === 'New Tax'){
                        component.set("v.requestType",$A.get("$Label.c.NewRequest"));
                        self.changeRequestType(component,event,helper,component.get('v.requestType'));
                    } else if(caseRequest === 'Tax Change'){
                        component.set("v.requestType",$A.get("$Label.c.UpdateRequest"));
                        self.changeRequestType(component,event,helper,component.get('v.requestType'));
                    }
                }
                if (response.subsidyTiles.length !== 0) {
                    const items = [];
                    for (let i = 0; i < response.subsidyTiles.length; i++) {
                        if (localStorage.getItem(response.subsidyTiles[i].Id) !== null) {
                            items.push({
                                label: response.subsidyTiles[i].Id,
                                value: response.subsidyTiles[i].Name + ' / ' + response.subsidyTiles[i].Supply__r.Name + ' /' + localStorage.getItem(response.subsidyTiles[i].Id)
                            });
                        }
                    }
                    component.set('v.retroActiveDate', items);
                }
            } else {
                ntfSvc.error(ntfLib, response.errorMsg);
                self.hideSpinner(component);
                if (response.redirectToDossier){
                    component.set("v.dossierId", response.dossierId);
                    self.redirectToDossier(component, helper);
                }
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
        self.hideSpinner(component);
    },
    changeRequestType: function(component,event,helper,requestType){
        component.set('v.requestType',requestType);
        component.set('v.showSelectRequest',false);
        component.set('v.isNotValid',false);
        component.set('v.disableOriginChannel',false);
        component.set('v.disabledSelectRequest',false);
        //component.find('sendingChannel').disableInputField(true);
        //component.set('v.step',1);
        window.localStorage;
        localStorage.setItem(component.get('v.dossierId'),requestType);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let originSelected = component.get('v.originSelected');
        let channelSelected = component.get('v.channelSelected');
        if (originSelected && channelSelected) {
            component.set('v.step',1);
            if (requestType === $A.get("$Label.c.NewRequest")) {
                component.set('v.step',2);
                component.set('v.taxSubsidyTile', component.get('v.defaultTaxSubsidyTile'));
                if (component.get('v.defaultTaxSubsidyTile').length !== 0) {
                    component.set('v.disabledSelectRequest',true);
                    component.set('v.step', 3);
                    component.set("v.osiTableView", component.get('v.defaultTaxSubsidyTile').length > component.get("v.tileNumber"));
                    component.set('v.disableOriginChannel',true);
                    //component.find('sendingChannel').disableInputField(false);
                }
            }
            if (requestType === $A.get("$Label.c.UpdateRequest")) {
                component.set('v.step',2);
                component.set('v.taxSubsidyTile', component.get('v.taxSubsidyTilesUpdate'));
                if (component.get("v.taxSubsidyTilesUpdate").length !== 0) {
                    component.set("v.osiTableView", component.get("v.taxSubsidyTilesUpdate").length > component.get("v.tileNumber"));
                    if (component.get("v.taxSubsidyTilesToUpdate").length !== 0) {
                        component.set("v.disabledSelectRequest", true);
                        component.set("v.step", 3);
                        component.set("v.disableOriginChannel", true);
                        component.set("v.taxTableView", component.get("v.taxSubsidyTilesToUpdate").length > component.get("v.tileNumber"));
                        //component.find('sendingChannel').disableInputField(false);
                    }
                }
            }

            if (component.get('v.requestType') === $A.get("$Label.c.UpdateRequest") && component.get('v.taxSubsidyTilesUpdate').length !== 0) {
                const items = [];
                let subventionType = component.get('v.taxSubsidyTilesUpdate')[0].RecordType.Name;
                if (component.get('v.taxSubsidyTilesUpdate').length > 1) {
                    for (let i = 1; i < component.get('v.taxSubsidyTilesUpdate').length; i++) {
                        if (component.get('v.taxSubsidyTilesUpdate')[i].RecordType.Name !== subventionType) {
                            items.push(component.get('v.taxSubsidyTilesUpdate')[i]);
                        }
                    }
                }
                component.set('v.isEmptyList',false);
                if(component.get('v.requestType') === $A.get("$Label.c.UpdateRequest") && component.get('v.taxSubsidyTilesToUpdate').length ===0 ){
                    component.set('v.isEmptyList',true);
                }
                if (component.get('v.requestType') === $A.get("$Label.c.UpdateRequest") && component.get('v.taxSubsidyTilesToUpdate').length !== 0) {
                    //component.set('v.isNotValid', true);
                }
            }
        }
    },
    updateDossierAndCase: function(component,event,helper){
        const self = this;
        const dossierId = component.get("v.dossierId");
        const origin = component.get("v.originSelected");
        const channel = component.get("v.channelSelected");
        component.find('apexService').builder()
          .setMethod("updateDossier")
          .setInput({
              dossierId: dossierId,
              origin: origin,
              channel: channel
          }).setResolve(function (response) {
            if (!response.error) {
                self.initialize(component, event, helper);
            }
        })
          .setReject(function (error) {
              self.hideSpinner(component);
          })
          .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId,templateId) {
        const navService = component.find("navService");

        /*"c__caseId": caseId,*/
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_TaxChangeWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    removeTaxSubsidy: function (component, event, helper) {
        const self = this;
        const deleteRecordId = event.getParam("deleteRecord");
        const taxSubsidyTile = component.get("v.taxSubsidyTile");
        const items = [];
        const defaultTax = [];
        const updateTax = [];
        const taxToUpdate = [];
        for (let i = 0; i < taxSubsidyTile.length; i++) {
            if (taxSubsidyTile[i].Id !== deleteRecordId) {
                items.push(taxSubsidyTile[i]);
            }
        }
        component.set("v.taxSubsidyTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
        if (component.get("v.defaultTaxSubsidyTile").length !== 0) {
            for (let i = 0; i < component.get('v.defaultTaxSubsidyTile').length; i++) {
                if (component.get("v.defaultTaxSubsidyTile")[i].Id !== deleteRecordId) {
                    defaultTax.push(component.get("v.defaultTaxSubsidyTile")[i]);
                }
            }
        }
        if (component.get("v.taxSubsidyTilesUpdate").length !== 0) {
            for (let i = 0; i < component.get('v.taxSubsidyTilesUpdate').length; i++) {
                if (component.get("v.taxSubsidyTilesUpdate")[i].Id !== deleteRecordId) {
                    updateTax.push(component.get("v.taxSubsidyTilesUpdate")[i]);
                }
            }
        }
        if (component.get("v.requestType") === $A.get("$Label.c.UpdateRequest")) {
            if (component.get("v.taxSubsidyTilesToUpdate").length !== 0) {
                for (let i = 0; i < component.get('v.taxSubsidyTilesToUpdate').length; i++) {
                    if (component.get("v.taxSubsidyTilesToUpdate")[i].Id !== deleteRecordId) {
                        taxToUpdate.push(component.get("v.taxSubsidyTilesToUpdate")[i]);
                    }
                }
            }
        }
        component.set("v.defaultTaxSubsidyTile", defaultTax);
        component.set("v.taxSubsidyTilesUpdate", updateTax);
        component.set("v.taxSubsidyTilesToUpdate", taxToUpdate);
        self.initialize(component, event, helper);
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    saveChain: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");
        //const caseId = component.get("v.caseId");
        let supplyId = component.get('v.taxSubsidyTile')[0].Supply__c;
        let subventionType = component.get("v.taxSubsidyTile")[0].RecordType.Name;
        let companyDivisionId = component.get("v.taxSubsidyTile")[0].Supply__r.CompanyDivision__c;
        let taxSubsidiesList = JSON.stringify(component.get('v.taxSubsidyTile'));
        let taxSubsidiesListToUpdate = JSON.stringify(component.get("v.taxSubsidyTilesUpdate"));


        if (component.get("v.requestType") === $A.get("$Label.c.UpdateRequest")) {
            supplyId = component.get("v.taxSubsidyTilesToUpdate")[0].Supply__c;
            subventionType = component.get("v.taxSubsidyTilesToUpdate")[0].RecordType.Name;
            companyDivisionId = component.get("v.taxSubsidyTilesToUpdate")[0].Supply__r.CompanyDivision__c;
            taxSubsidiesList = JSON.stringify(component.get("v.taxSubsidyTilesToUpdate"));
        }
            component.find("apexService").builder()
              .setMethod("updateCaseListAndTax")
              .setInput({
                  //caseRecord: caseId,
                  dossierId: dossierId,
                  supplyId: supplyId,
                  companyDivisionId: companyDivisionId,
                  subventionType: subventionType,
                  requestType: component.get("v.requestType"),
                  taxSubsidiesList: taxSubsidiesList,
                  taxSubsidiesListToUpdate: taxSubsidiesListToUpdate,
                  origin: component.get("v.originSelected"),
                  channel: component.get("v.channelSelected")
              }).setResolve(function(response) {
                if (!response.error) {
                    self.hideSpinner(component);
                    self.redirectToDossier(component, helper);
                }
            })
              .setReject(function(error) {
                  self.hideSpinner(component);
              })
              .executeAction();

    },
    updateCaseList: function(component, helper) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");
        //const caseId = component.get("v.caseId");
        let supplyId = component.get("v.taxSubsidyTile")[0].Supply__c;
        let subventionType = component.get("v.taxSubsidyTile")[0].RecordType.Name;
        let companyDivisionId = component.get("v.taxSubsidyTile")[0].Supply__r.CompanyDivision__c;
        let taxSubsidiesList = JSON.stringify(component.get("v.taxSubsidyTile"));
        let isRetroActiveDate = false;
        let notes = component.get('v.notes');
        let retroActiveDate = component.get('v.retroActiveDate');
        let isCreditManagement = component.get('v.isCreditManagement');
        let distributorNotes = component.get('v.notes');
        for (let i = 0; i < retroActiveDate.length; i++) {
            notes = notes + '(' + retroActiveDate[i].value + ')';
        }
        console.log('notes' + notes);
        /*if(retroActiveDate){*/
        if (retroActiveDate.length !== 0  && !isCreditManagement) {
            isRetroActiveDate = true;
        }
        /*}*/
        component.find("apexService").builder()
            .setMethod("updateCaseAndDossier")
            .setInput({
                //caseRecord: caseId,
                dossierId: dossierId,
                supplyId: supplyId,
                companyDivisionId: companyDivisionId,
                subventionType: subventionType,
                requestType: component.get("v.requestType"),
                taxSubsidiesList: taxSubsidiesList,
                origin: component.get("v.originSelected"),
                channel: component.get("v.channelSelected"),
                isRetroActiveDate: isRetroActiveDate,
                notes: notes,
                distributorNotes: distributorNotes
            }).setResolve(function (response) {
            if (!response.error) {
                self.hideSpinner(component);
                self.redirectToDossier(component, helper);
            }
        })
          .setReject(function(error) {
              self.hideSpinner(component);
          })
          .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        let taxSubsidiesList = JSON.stringify(component.get('v.taxSubsidyTile'));
        if (component.get("v.requestType") === $A.get("$Label.c.UpdateRequest")) {
           taxSubsidiesList = JSON.stringify(component.get("v.taxSubsidyTilesToUpdate"));
         }
        const caseId = component.get("v.caseId");
        const dossierId = component.get("v.dossierId");
        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                caseRecord:caseId ,
                dossierId: dossierId,
                taxSubsidiesList: taxSubsidiesList
            }).setResolve(function (response) {
            if (!response.error) {
                self.hideSpinner(component);
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
                self.hideSpinner(component);
            })
            .executeAction();
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = "Lightning Experience | Salesforce";
        }
    },

    createPrivacyChangeRecord: function (component) {
        let self = this;
        let privacyChangeComponent = component.find("privacyChange");

        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }

     //   self.changeStatusAndExecuteConfirmTransition(component);
    },

   /* changeStatusAndExecuteConfirmTransition: function (component) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("ChangeStatusAndExecuteTransition")
            .setInput({
                dossierId: component.get('v.dossierId')
            }).setResolve(function (response) {
            if (response.error) {
                ntfSvc.error(ntfLib, response.errorMsg);
                self.hideSpinner(component);
            }
        })
            .setReject(function (error) {
                self.hideSpinner(component);
            })
            .executeAction();
    }, */

    /**
     * @author Boubacar Sow
     * @description: [ENLCRO-1010] Tax changes - Process Changes
     * @date 26/06/2020
     * @param component
     * @param helper
     * @param event
     */
    updateTaxSubsidyStatut: function (component, helper, event) {
        const self = this;
        self.showSpinner(component);
        const taxSubsidyId = component.get("v.taxSubsidyId");
        component.find('apexService').builder()
            .setMethod("updateTaxSubsidyStatut")
            .setInput({
                taxSubsidyId: taxSubsidyId
            }).setResolve(function (response) {
            if (!response.error) {
                component.set('v.taxSubsidyId', '');
                self.initialize(component, event, helper);
            }
        })
            .setReject(function (error) {
                self.hideSpinner(component);
            })
            .executeAction();
    },

    /**
     * @author Boubacar Sow
     * @description: [ENLCRO-1010] Tax changes - Process Changes
     * @date 26/06/2020
     * @param component
     * @param helper
     * @param event
     */
    updateOldTaxSubsidyStatut: function (component, helper, event) {
        const self = this;
        self.showSpinner(component);
        const parentCaseId = component.get('v.parentCaseId');

        component.find('apexService').builder()
            .setMethod("updateOldTaxSubsidyStatut")
            .setInput({
                parentCaseId: parentCaseId
            }).setResolve(function (response) {
            if (!response.error) {
                //component.set('v.taxSubsidyId','');
                //self.initialize(component, event, helper);
            }
        })
            .setReject(function (error) {
                self.hideSpinner(component);
            })
            .executeAction();
    },

});