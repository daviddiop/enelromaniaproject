/**
 * Created by BADJI on 20/05/2019.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        const connectionDossierId = myPageRef.state.c__connectionDossierId;
        let disabledOpportunityServiceFields ={
            distributor:"disabled",
            availablePower:"disabled",
            contractualPower:"disabled",
            powerPhase:"disabled",
            pressure:"disabled",
            pressureLevel:"disabled",
            voltageSetting:"disabled",
            voltageLevel:"disabled",
            startDate:"disabled",
            flatRate:"disabled"
        };
        component.set("v.disabledOpportunityServiceFields",disabledOpportunityServiceFields);
        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        component.set("v.dossierId", dossierId);
        component.set("v.connectionDossierId", connectionDossierId);
        console.log('v.step' + component.get('v.step'));
        //For Person Account the default Contract Type = "Residential"
        helper.isBusinessClient(component);
        //For Person Account the default Contract Type = "Residential"
        helper.initialize(component);
        helper.initConsoleNavigation(component,$A.get("$Label.c.Activation"));
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
       /* component.set("v.isSaving", false);
        helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty);*/
    },
    onSaveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        component.set("v.isSaved", false);
        if (cancelReason) {
            component.set("v.savingWizard", false);
            helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty, cancelReason, detailsReason);
        }
    },
    saveDraft: function (component, event, helper) {
        component.set("v.isSaving", false);
        component.set("v.isSaved", false);
        helper.createPrivacyChangeRecord(component);
    },
    save: function (component, event, helper) {
        component.set("v.enableSaveButton", false);
        component.set("v.isSaving", true);
        component.set("v.isSaved", true);
        helper.validateOpportunity(component,function (validOsi, validDossier) {
            if(validOsi && validDossier){
                helper.createPrivacyChangeRecord(component);
            }
        });
    },

    nextStep: function (component, event, helper) {
        let buttonPressed = event.getSource().getLocalId();
        let ntfLib = component.find("ntfLib");
        let ntfSvc = component.find("ntfSvc");

        let expirationDateDate =  new Date(component.get('v.expirationDate'));
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        let todayDate = new Date(dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate());

        //if (buttonPressed === "confirmStep9") {
        if (buttonPressed === "confirmStep-1") {

            if(component.get('v.expirationDate') === null){
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                return;
            }
            if((component.get('v.expirationDate') !== null) && (expirationDateDate < todayDate)){
                ntfSvc.error(ntfLib, $A.get("$Label.c.EnteredDateShouldBeGreaterOrEqualThan") + " "+ today);
                return;
            }

            if(!component.get('v.originSelected') || (component.get('v.originSelected') === "undefined")){
                component.set('v.originSelected', component.get('v.opportunity').Origin__c);
            }
            if(!component.get('v.channelSelected') || (component.get('v.channelSelected') === "undefined")){
                component.set('v.channelSelected', component.get('v.opportunity').Channel__c);
            }
            //FF 29/02

            if(component.get('v.originSelected') && component.get('v.channelSelected')) {
                component.set('v.disableOriginChannel',true);
                component.set('v.disableWizard',false);

                // FF Activation/SwitchIn - Interface Check - Pack2
                helper.setChannelAndOrigin(component);

                if(component.get('v.expirationDate') !== null){
                    helper.setSubProcessAndExpirationDate(component);
                }else{
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }
                // FF Activation/SwitchIn - Interface Check - Pack2
                component.set("v.step", -1);
            }else {
                // FF Activation/SwitchIn - Interface Check - Pack2
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectOriginAndChannel"));
                // FF Activation/SwitchIn - Interface Check - Pack2
                return;
            }

        } else if (buttonPressed === "confirmStep0") {//confirmStep9
            let commodity = component.get("v.commodity");
            if(component.get('v.useMassiveHelper')) {
                helper.setMassiveHelperParams(component, commodity);
            }
            if (commodity === 'Gas') {
                component.set('v.consumptionConventions', true);
                component.set('v.consumptionConventionsDisabled', true);
                component.set('v.consumptionType', 'Single Rate');
            } else {
                component.set('v.consumptionConventions', false);
                component.set('v.consumptionConventionsDisabled', false);
                component.set('v.consumptionType', 'None');
            }

            helper.updateCommodityToDossier(component);
            //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
            helper.deleteOsisAndOlisByCommodityChange(component);
            //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
            //component.set("v.step", 1);

        }
        else if(buttonPressed === "confirmStep1") {
            if(component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                for (let osi of component.get("v.opportunityServiceItems")) {
                    if (!osi.EstimatedConsumption__c) {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.FillEstimatedConsumption"));
                        return;
                    }
                }

                if(component.get('v.requestedStartDate')){

                    helper.isNotWorkingDate(component);

                }else{
                    helper.validateFields(component);
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }
            }
        }
        else if (buttonPressed === "confirmStep2") {

            if(component.get("v.commodity") === "Gas"){
                component.set("v.market","Free");
            }

            if(!component.get('v.contractType')){
                if (component.get("v.isBusinessClient")) {
                    component.set("v.contractType", 'Business');
                } else{
                    component.set("v.contractType", 'Residential');
                }
            }
            //For Person Account the default Contract Type = "Residential"
            let pointSelectionCmp = component.find("pointSelection");
            if (pointSelectionCmp) {
                pointSelectionCmp.resetBox();
            }
            if(component.get("v.companyDivisionId")){
                helper.updateCompanyDivisionOnOpportunity(component);
                //helper.updateTraderToOsi(component);
            } else {
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                return;
            }
            if (!component.get("v.opportunityServiceItems") || component.get("v.opportunityServiceItems").length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredSupply"));
                return;
            }
            if (component.get("v.isVisibleContractAddition")) {
                if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                    component.set("v.userDepartment", '');
                }
                if(!component.get("v.userDepartment")){
                    if(component.get("v.channelSelected") === "Indirect Sales"){
                        component.set("v.salesUnit", null);
                    }
                    component.set("v.salesUnitId", '');
                    component.set("v.salesman", '');
                    component.set("v.companySignedId", '');

                    if(component.get("v.opportunity") && component.get("v.opportunity").SalesUnit__c){
                        component.set("v.salesUnit", component.get("v.opportunity").SalesUnit__c);
                        component.set("v.salesUnitId", component.get("v.opportunity").SalesUnit__c);
                    }
                    if(component.get("v.opportunity") && component.get("v.opportunity").CompanySignedBy__c){
                        component.set("v.companySignedId", component.get("v.opportunity").CompanySignedBy__c);
                    }
                    if(component.get("v.opportunity") && component.get("v.opportunity").Salesman__c){
                        component.set("v.salesman", component.get("v.opportunity").Salesman__c);
                    }
                    console.log('opp: '+JSON.stringify(component.get("v.opportunity")));
                    if(component.get('v.channelSelected') !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").SalesUnit__c){
                        if(component.get("v.salesUnit") && component.get("v.salesUnit").SalesDepartment__c === component.get('v.channelSelected')){
                            let salesAccountId = component.get("v.salesUnit").Id;
                            component.set("v.salesUnitId", salesAccountId);
                        }
                    }
                    if(component.get("v.channelSelected") !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").CompanySignedBy__c){
                        if(!component.get("v.salesUnit")){
                            component.set("v.companySignedId", "");
                        }
                        else{
                            if(component.get("v.channelSelected") !== "Back Office Sales" && component.get("v.channelSelected") !== "Back Office Customer Care"){
                                let userId = component.get("v.user").Id;
                                component.set("v.companySignedId", userId);
                            }
                        }
                    }
                }
                component.set("v.step", 3);
            } else {
                component.set("v.step", 4);//3
                helper.saveOpportunity(component,"Qualification");
            }
        } else if (buttonPressed === "confirmStep3") {
            if(component.get("v.companyDivisionId")){
                let companyDivisionId = component.get("v.companyDivisionId");
                component.set('v.pointAddressProvince',companyDivisionId);
            }
            if (!helper.checkSelectedContract(component)) {
                ntfSvc.error(ntfLib,$A.get("$Label.c.RequiredFields"));
                return;
            }
            component.set("v.step", 4);
            if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                component.set('v.streetName',pointStreetId);

                if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                    let isDiscoEnel = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                    component.set("v.isDiscoEnel", isDiscoEnel);
                    let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                    component.set("v.vatNumber", vatNumber)
                    let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                    component.set("v.selfReadingEnabled", selfReadingEnabled);
                }
            }

            helper.updateContractAndContractSignedDateOnOpportunity(component);
            //helper.saveOpportunity(component,"Qualification");
            helper.reloadContractAccount(component);
        } else if (buttonPressed === 'confirmStep4') {
            if (!component.get("v.contractAccountId")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountRequired")); //$A.get('$Label.c.BillingProfileRequired')
                return;
            }
            helper.deleteOlisByContractAccountChange(component);
            helper.updateOsi(component);
            component.set("v.step", 5);
        } else if (buttonPressed === "confirmStep5") {
            let prdList = component.get("v.opportunityLineItems");
            if (!prdList && prdList.length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct")); //$A.get('$Label.c.OnlyOneProductRequired')); //almeno un PRODOTTO
                return;
            }

            var utilityPrds = prdList.filter(
                oli => oli.Product2.RecordType.DeveloperName === "Utility"
            );
            if (!utilityPrds || utilityPrds.length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct")); //almeno 1 UTILITY
                return;
            }
            let oli = utilityPrds[0];
            component.find("consumptionTable").update();
            helper.linkOliToOsi(component, oli);
            helper.updateConsumptionConventionsCheckbox(component);
            helper.updateRegulatedTariffs(component);
        }else if (buttonPressed === "confirmStep6") {//confirmStep5
            let consumptionList = component.find("consumptionTable").getAllConsumptionObjects();
            helper.createConsumptions(component, consumptionList, function () {
                component.set("v.step", 7);//6
            });
            if(component.get('v.contractAccountId')){
                helper.reloadSendingChannel(component);
            }
            helper.updateConsumptionConventionsOnOpportunity(component);
        } else if (buttonPressed === "confirmStep7") {//confirmStep5
            helper.saveSendingChannel(component);
            component.set("v.enableSaveButton", true);
        }
        console.log('step finale: ' +  component.get("v.step"));
    },
    editStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep-1") {
            component.set("v.step", -2);
            component.set('v.disableWizard', true);
            component.set('v.disableOriginChannel', false);
        } else if (buttonPressed === "returnStep0") {
            component.set("v.step", -1);
        } else if (buttonPressed === "returnStep1") {
            component.set("v.step", 1);//1
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 2);//2
        } else if (buttonPressed === "returnStep3") {
            component.set("v.step", 3);//3
        } else if (buttonPressed === "returnStep4") {
            component.set("v.step", 4);//4
        } else if (buttonPressed === "returnStep5") {
            component.set("v.step", 5);
        } else if (buttonPressed === "returnStep6") {
            component.set("v.step", 6);
        } else if (buttonPressed === "returnStep7") {
            component.set("v.step", 7);
        }
    },

    handlePointResult: function (component, event, helper) {
        var ntfLib = component.find("ntfLib");
        var ntfSvc = component.find("ntfSvc");
        //var gmPatt = /^(\d{14})$/i;
        //var eePatt = /^(\w{2}\d{3}\w{1}\d{8})$/i;

        let servicePointId = event.getParam("servicePointId");
        let servicePointCode = event.getParam("servicePointCode");
        let servicePoint = event.getParam("servicePoint");
        let suppliesByServicePointCode = event.getParam("suppliesByServicePointCode");
        let osiList = component.get("v.opportunityServiceItems");
        let commodity = component.get("v.commodity");

        if (servicePoint) {
            if ((servicePoint.RecordType.DeveloperName === 'Gas' && commodity !== 'Gas')
                || (servicePoint.RecordType.DeveloperName === 'Electric' && commodity !== 'Electric')) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.IncoherentCommodity"));
                return;
            }
        }

        if(osiList){
            for (var j = 0; j < osiList.length; j++) {
                if (servicePointCode === osiList[j].ServicePointCode__c) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ServicePointAlreadySelected"));
                    return;
                }
            }
        }


        console.log('FFF suppliesByServicePointCode: ' + JSON.stringify(suppliesByServicePointCode));
        if (suppliesByServicePointCode.length > 0) {
            for (let i = 0; i < suppliesByServicePointCode.length; i++) {
                if (suppliesByServicePointCode[i].ServicePoint__r.CurrentSupply__c && (suppliesByServicePointCode[i].ServicePoint__r.CurrentSupply__r.Status__c !== 'Not Active')) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ServicePointHasRelatedSupply"));
                    return;
                }
            }
        }

        component.set("v.searchedPointId", servicePointId);
        component.set("v.searchedPointCode", servicePointCode);
        component.set("v.showNewOsi", true);
    },
    handleNewOsi: function (component, event, helper) {
        let osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
    },
    handleUploadFinished: function (component, event, helper) {
        helper.createPointsFromOpportunity(component);
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedPointId", "");
        component.set("v.searchedPointCode", "");
        //component.find("pointSelection").resetBox();
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(
            component,
            "Qualification",
            helper.openProductSelection,
            ""
        );
        //helper.setPercentage(component, "Proposal/Price Quote");
    },
    handleOsiDelete: function (component, event, helper) {
        let osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        let companyDivComponent = component.find("companyDivision");
        let items = [];
        for (let i = 0; i < osiList.length; i++) {
             if (osiList[i].Id !== osiId) {
                 items.push(osiList[i]);
             }
         }
        if (items.length == 0) {
            component.set("v.step", 0);
            component.set('v.requestedStartDate',null);
            component.set('v.companyDivisionId',null);
            if (companyDivComponent) {
                companyDivComponent.setCompanyDivision();
            }
        }
        component.set("v.opportunityServiceItems", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    getPrivacyId: function (component, event, helper) {
        let isFirstSave = component.get("v.isFirstSave");

        if(isFirstSave === true) {
            component.set("v.isFirstSave", false);
            const ntfLib = component.find('ntfLib');
            const ntfSvc = component.find('ntfSvc');

            let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
            if (!doNotCreatePrivacyChange) {
                component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
                component.set("v.dontProcess", event.getParam('dontProcess'));
            }
            if (!component.get("v.isSaving")) {
                helper.saveOpportunity(component, "Qualification", helper.redirectToOppty);
                component.set("v.isFirstSave", true);
            }
            if (component.get("v.isSaving")) {
                if (component.get("v.dontProcess")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
                    $A.get('e.force:refreshView').fire();
                    component.set("v.isFirstSave", true);
                    return;
                }
                helper.saveOpportunity(component, "Quoted", helper.redirectToOppty);
            }
        }
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            if(event.getParam("divisionId")) {
                component.set("v.companyDivisionId", event.getParam("divisionId"));
                component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));

                if ((component.get("v.step") === 2) && component.get("v.isCompanyDivisionEnforced") ) {//0
                    component.set("v.step", 3);//1
                }

                // Apply filter on Contract Account
                if (component.find("contractAccountSelectionComponent")) {
                    helper.reloadContractAccount(component);
                }
                // Apply filter on Contract Addition
                if (component.get("v.isVisibleContractAddition")) {
                    helper.reloadContractAddition(component);
                }
            }
        } catch (err) {
            console.error('Error on aura:ActivationWizard :', err);
        }
    },
    getContractAccountRecordId: function (component, event, helper) {
        component.set(
            "v.contractAccountId",
            event.getParam("contractAccountRecordId")
        );
    },

    handleExpirationDate: function (component, event) {
        let expirationDate = event.getParam("expirationDate");
        component.set("v.expirationDate", expirationDate);
        console.log('444 expirationDate :', component.get("v.expirationDate"));
    },

    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        let consumptionConventions = event.getParam("consumptionConventions");
        component.set("v.contractId", contractSelected);
        component.set("v.customerSignedDate", customerSignedDate);
        component.set("v.contractName", event.getParam("newContractName"));
        component.set("v.contractType", event.getParam("newContractType"));
        component.set("v.companySignedId", event.getParam("newCompanySignedId"));
        component.set("v.salesman", event.getParam("newSalesman"));
        component.set("v.salesUnitId", event.getParam("newSalesUnitId"));
        if (consumptionConventions !== null) {
            component.set("v.consumptionConventions", consumptionConventions);
        }
        component.set("v.market", event.getParam("newMarket"));
    },

    //[ENLCRO-659] Activation - Interface Check - Pack3
    removeError: function (component) {
        const labelFieldList = [];
        let valEffectiveDate = component.find("RequestedStartDate__c");
        //FF 29/02
        valEffectiveDate.blur();
        //FF 29/02
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('RequestedStartDate__c');
        }

        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },
    //[ENLCRO-659] Activation - Interface Check - Pack3

    changeConsumptionType: function (component, event, helper) {
        component.set('v.consumptionType', component.find('changeConsumptionType').get('v.value'));
    },
    changeConsumptionConventions: function (component, event, helper) {
        let consumptionConventions = component.get('v.consumptionConventions');
        if(!component.get("v.productRateType")) {
            if (consumptionConventions === true) {
                component.set('v.consumptionType', 'Single Rate');
            }
            if (consumptionConventions === false) {
                component.set('v.consumptionType', 'None');
            }
        }
    },

    handleOriginChannelSelection: function (component, event, helper) {
        if(component.get('v.disableWizard')){
            let originSelected = event.getParam('selectedOrigin');
            let channelSelected = event.getParam('selectedChannel');
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            if( component.get("v.step") <= -2) {
                if(channelSelected !=null && originSelected !=null){
                    component.set("v.step", -2);
                }else {
                    component.set("v.step", -3);
                }
            } else if(channelSelected !=null || originSelected !=null){
                component.set('v.disableOriginChannel',true);
            }
        }
    },

    handleSavedSendingChannel: function (component, event, helper) {
        component.set("v.step", 8);
    },

    handleUpdateEstimatedConsumption: function (component, event, helper) {
        console.log("event consump==="+JSON.stringify(event));
        let consumptionDetails=event.getParam("consumptionDetails");
        let osiId= consumptionDetails.OpportunityServiceItem__c;
        console.log("event osiId==="+osiId);
        let osiTiles=component.find('osiTile');
        let osiTile;
        if (osiTiles instanceof Array) {
            osiTiles.forEach(osiTileCmp => {
                if(osiTileCmp.getRecordId() === osiId){
                    osiTile=osiTileCmp;
                }
            });
        }else {
            osiTile=osiTiles;

        }
        //if(osiTile){
        helper.loadOsi(component,osiId,function (osi) {
            console.log("new estimated==="+osi.EstimatedConsumption__c);
        });

        //}

    },

    handleUpdateOsi:function(component, event, helper) {
        let osiId= event.getParam("osiId");
        helper.loadOsi(component,osiId,function (updatedOsi) {
            console.log("new estimated==="+updatedOsi.EstimatedConsumption__c);
            let consumptionCmp = component.find("consumptionTable");
            if(consumptionCmp){
                consumptionCmp.update(updatedOsi.Id);
            }
        });
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.hasBlockingErrors', true);
    },

    createOLIsForContractAddition: function(component, event, helper) {
        helper.showSpinner(component);
        helper.createOLIsForContractAddition(component);
    },

    handleOsiDeleteAll: function(component, event, helper){
        helper.showSpinner(component);
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let osiList = component.get("v.opportunityServiceItems");
        let oppId = component.get("v.opportunityId");
        if(osiList.length > 0){
            //oppId = osiList[0].Opportunity__c;
            component.find('apexService').builder()
            .setMethod('deleteAllOsi')
            .setInput({
                "opportunityId": oppId
            }).setResolve(function (response) {
                console.log('success', 'OsiDeleteAll');
                let companyDivComponent = component.find("companyDivision");
                let items = [];
                if (items.length == 0) {
                    component.set("v.step", 0);
                    component.set('v.requestedStartDate',null);
                    component.set('v.companyDivisionId',null);
                    if (companyDivComponent) {
                        companyDivComponent.setCompanyDivision();
                    }
                }
                component.set("v.opportunityServiceItems", items);
                component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
                helper.hideSpinner(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
                helper.hideSpinner(component);
            }).executeAction();
        }
        component.set('v.isDeleteAllOsi', false);
    },
    closeModal: function(component, event, helper){
        component.set('v.isDeleteAllOsi', false);
    },
    openModal: function(component, event, helper){
        component.set('v.isDeleteAllOsi', true);
    }
});