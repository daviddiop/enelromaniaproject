({
    initialize: function (component) {
        var self = this;
        var myPageRef = component.get("v.pageReference");
        var accountId = myPageRef.state.c__accountId;
        var opportunityId = myPageRef.state.c__opportunityId;
        var dossierId = myPageRef.state.c__dossierId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        var ntfLib = component.find('ntfLib');
        var ntfSvc = component.find('ntfSvc');
        var companyDivComponent = component.find("companyDivision");
        component.set("v.opportunityId", '');
        component.set("v.accountId", accountId);
        component.set('v.interactionId', component.find('cookieSvc').getInteractionId());

        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "interactionId": component.get('v.interactionId'),
                "connectionDossierId": component.get("v.connectionDossierId"),
                "genericRequestId" :genericRequestId
            })
            .setResolve(function (response) {

                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunity", response.opportunity);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                console.log('response.dossierId: ' + response.dossierId);
                console.log('response.dossier: ' + response.dossier);
                component.set("v.commodity", response.commodity);
                component.set('v.subProcess', response.subProcess);
                component.set('v.expirationDate', response.expirationDate);
                console.log('response.expirationDate: ' + response.expirationDate);
                console.log('response.subProcess: ' + response.subProcess);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.contractId", response.contractIdFromOpp);
                component.set("v.commodityPicklistValues", response.commodityPicklistValues);
                component.set("v.commodityLabel", response.commodityLabel);
                console.log('commodity: ' + response.commodity);
                console.log('commodityPicklistValues: ' + response.commodityPicklistValues);
                console.log('commodityLabel: ' + response.commodityLabel);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.salesUnit", response.salesUnit);
                component.set("v.salesUnitId", response.salesUnit);
                component.set("v.userDepartment", response.userDepartment);
                component.set("v.companySignedId", response.companySignedId);
                component.set("v.salesman", response.salesman);
                component.set("v.user", response.user);
                component.set("v.canCreatePlaceholder", response.canCreatePlaceholder);
                 component.set("v.useMassiveHelper", response.useMassiveHelper);
                 component.set("v.commodityToFtMap", response.commodityToFtMap);
                 component.set("v.massiveHelperParams", JSON.stringify({opportunityId : response.opportunityId}));
                 if(component.get('v.useMassiveHelper')) {
                     self.setMassiveHelperParams(component, response.commodity);
                 }
                 self.consumptionListRefactor(component);

                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                if(component.get('v.opportunity') && component.get('v.opportunity').RequestedStartDate__c){
                    component.set('v.requestedStartDate',component.get('v.opportunity').RequestedStartDate__c);
                }
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
                if (response.opportunityCompanyDivisionId) {
                    component.set("v.companyDivisionId", response.opportunityCompanyDivisionId);
                    component.set('v.pointAddressProvince',response.opportunityCompanyDivisionId);
                }
                component.set("v.useBit2WinCart", response.useBit2WinCart);
                component.set("v.isConnectionOffering", response.isConnectionOffering);

                component.set("v.consumptionList", response.consumptionList);
                self.consumptionListRefactor(component);

                let contractId = component.get("v.contractId") ;
                let market =  "";
                component.set("v.market",market);

                if (response.commodity === 'Gas') {
                    component.set('v.consumptionConventions', true);
                    component.set('v.consumptionConventionsDisabled', true);
                    component.set('v.consumptionType', 'Single Rate');
                }
                if (response.consumptionConventions) {
                    component.set('v.consumptionConventions', response.consumptionConventions);
                    if (response.consumptionConventions === true) {
                        component.set('v.consumptionConventionsDisabled', true);
                        component.set('v.consumptionType', 'Single Rate');
                    }
                }
                if (response.consumptionList && response.consumptionList.length && response.consumptionList.length === 1) {
                    component.set('v.consumptionType', response.consumptionList[0].Type__c);
                    component.set("v.consumptionConventions", true);
                    component.set("v.consumptionConventionsDisabled", true);
                } else if (response.consumptionList && response.consumptionList.length && response.consumptionList.length === 2) {
                    component.set('v.consumptionType', 'Dual Rate');
                }
                let consumptionTypeFromCmp = component.get('v.consumptionType');
                if (!consumptionTypeFromCmp) {
                    component.set('v.consumptionType', 'None');
                }
                self.updateConsumptionConventionsCheckbox(component);

                if(response.commodity ==='Gas'){
                    component.set("v.market",'Free')
                }
                //if(response.opportunityCompanyDivisionId){
                if (companyDivComponent) {
                    //companyDivComponent.wizardCompanyDivisionId = response.opportunityCompanyDivisionId;
                    companyDivComponent.setCompanyDivision();
                    //self.reloadCompanyDivision(component);
                }
                //}
                if (response.opportunityId && !opportunityId) {
                    self.updateUrl(component, accountId, response.opportunityId, response.dossierId);
                }
                //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                if(component.get("v.isConnectionOffering")){
                    component.set("v.isNewConnectionFlagReadOnly",true);
                }
                //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                    let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                    component.set('v.streetName',pointStreetId);

                    if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                        let isDiscoEnel = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                        component.set("v.isDiscoEnel", isDiscoEnel);
                        let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                        component.set("v.vatNumber", vatNumber);
                        let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                        component.set("v.selfReadingEnabled", selfReadingEnabled);
                    }
                }
                self.reloadContractAddition(component);

                let step = -3;
                if ((response.opportunityLineItems.length !== 0) && (response.contractAccountId) && (response.opportunityServiceItems.length !== 0)) {
                    step = 5;//4
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if ((response.contractAccountId) && (response.opportunityServiceItems.length !== 0)) {
                    step = 5;//4
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                    //For Person Account the default Contract Type = "Residential"
                } else if (response.opportunity && component.get("v.companyDivisionId") && response.opportunityServiceItems.length !== 0 && response.opportunity.ContractType__c) {
                    step = 4;
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                    //For Person Account the default Contract Type = "Residential"
                } else if ((component.get("v.companyDivisionId") && response.opportunityServiceItems.length !== 0) || component.get("v.isCompanyDivisionEnforced")) {
                    step = 3;
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if (response.opportunityServiceItems.length !== 0) {
                    step = 1;
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if (response.commodity && response.opportunity && response.expirationDate) {
                    step = 0;//1
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if (response.opportunity  && response.opportunity.Origin__c && response.opportunity.Channel__c) {
                    step = -2;//0
                    component.set('v.disableWizard',true);
                    component.set('v.disableOriginChannel',false);
                }

                var opportunityVal = response.opportunity;
                if (opportunityVal) {
                    if (opportunityVal.StageName === 'Closed Lost' || opportunityVal.StageName === 'Quoted') {
                        component.set("v.isClosed", true);
                    }
                    if (opportunityVal.Origin__c) {
                        component.set("v.originSelected", opportunityVal.Origin__c);
                    }
                    if (opportunityVal.Channel__c) {
                        component.set("v.channelSelected", opportunityVal.Channel__c);
                    }
                    if (opportunityVal.ContractName__c) {
                        component.set("v.contractName", opportunityVal.ContractName__c);
                    }
                    if (opportunityVal.ContractType__c) {
                        component.set("v.contractType", opportunityVal.ContractType__c);
                    }
                }
                if(component.get("v.userDepartment") && component.get("v.channelSelected") === "Direct Sales (KAM)"){
                    component.set("v.salesUnitId", component.get("v.userDepartment"));
                    component.set("v.salesUnit", null);
                    component.set("v.showUserDepartment", true);
                }
                if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                    component.set("v.userDepartment", '');
                }
                if(response.salesUnitAccount && !component.get("v.salesUnit")){
                    component.set("v.salesUnit", response.salesUnitAccount);
                    if(component.get("v.salesUnit").SalesDepartment__c === component.get("v.channelSelected")) {
                        let salesUnitAccountId = component.get("v.salesUnit").Id;
                        component.set("v.salesUnitId", salesUnitAccountId);
                    }
                    else{ //user with salesUnitId but SalesDepartment not matching with selected channel
                        component.set("v.salesUnit", null);
                    }
                }
                if(component.get("v.channelSelected") !== "Indirect Sales" && !component.get("v.companySignedId")){
                    if(response.salesUnitAccount && !component.get("v.salesUnit")){//user with salesUnitId but SalesDepartment not matching with selected channel
                        component.set("v.companySignedId", "");
                    }
                    else if(component.get("v.channelSelected") === "Direct Sales (KAM)" && !component.get("v.salesUnit") && !component.get("v.userDepartment")){//channel direct sales and user without salesUnitId and not in KamUserGroup
                        component.set("v.companySignedId", "");
                    }
                    else if(component.get("v.channelSelected") === "Back Office Sales" || component.get("v.channelSelected") === "Back Office Customer Care"){
                        component.set("v.companySignedId", "");
                    }
                    else{
                        let userId = component.get("v.user").Id;
                        component.set("v.companySignedId", userId);
                    }
                }

                component.set("v.step", step);
                console.log('initialize step: ' + step);
                component.set("v.accountId",response.accountId);

                //component.set("v.account",response.account);
                if (component.find("contractAccountSelectionComponent")) {
                    self.reloadContractAccount(component);
                }
                self.hideSpinner(component);
                //self.setPercentage(component,response.stage);
                //$A.get('e.force:refreshView').fire();

                component.set("v.salesSupportUser", response.salesSupportUser);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error, true);
                component.set("v.disableWizard", true);
                self.hideSpinner(component);
            })
            .executeAction();
    },
    validateOsi: function (component, osiId) {
        let self = this;
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let channel = component.get('v.channelSelected');
        component.find('apexService').builder()
            .setMethod('checkOsi')
            .setInput({
                "osiId": osiId,
                "channel":channel
            })
            .setResolve(function (response) {
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;
                osiList.push(newOsi);
                /*if (osiList.length == 1) {
                    self.updateCompanyDivisionOnOpportunity(component);
                }*/
                component.set("v.step", 1);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                component.find("pointSelection").resetBox();
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            }).executeAction();
    },
    saveOpportunity: function (component, stage, callback,cancelReason, detailsReason) {
        let self = this;
        self.showSpinner(component);
        //self.setPercentage(component, stage);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId");
        let dossierId = component.get("v.dossierId");
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "dossierId": dossierId,
                "stage": stage,
                "cancelReason": cancelReason,
                "detailsReason": detailsReason
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.enableSaveButton", true);
                    component.set("v.isFirstSave", true);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        let self = this;
        self.showSpinner(component);
        let oppId = component.get("v.opportunityId");
        let osiList = component.get("v.opportunityServiceItems");
        let contractAccountId = component.get("v.contractAccountId");
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityId": oppId,
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateTraderToOsi: function (component, callback) {
            let self = this;
            self.showSpinner(component);
            let osiList = component.get("v.opportunityServiceItems");
            let companyDivisionId = component.get("v.companyDivisionId")
            let ntfSvc = component.find('ntfSvc');
            let ntfLib = component.find('ntfLib');

            component.find('apexService').builder()
                .setMethod('updateTraderToOsiList')
                .setInput({
                    "opportunityServiceItems": osiList,
                    "companyDivisionId": companyDivisionId
                })
                .setResolve(function (response) {
                    self.hideSpinner(component);
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    if (callback && typeof callback === "function") {
                        callback(component, self);
                    }
                })
                .setReject(function (response) {
                    const errors = response.getError();
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                })
                .executeAction();
        },
    updateUrl: function (component, accountId, opportunityId,dossierId) {
        let navService = component.find("navService");
        const {isConsoleNavigation} = component.find("workspace");
        let self = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ActivationWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId,
                "c__dossierId": dossierId
            }
        };
        self.redirect(component, pageReference, true);
    },
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        console.log('useBit2WinCart',useBit2WinCart);
        //console.log('accountId', component.get("v.accountId"));
        let pageReference;

        if (useBit2WinCart) {
            let osiList = component.get("v.opportunityServiceItems");
            var isGas = false;
            var isElectric = false;
            for (var osi of osiList) {
                if (osi.RecordType.DeveloperName === 'Gas') {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === 'Electric') {
                    isElectric = true;
                }
            }
            var productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';

            }
            console.log('RequestType__c');
            console.log(component.get('v.opportunity').RequestType__c);
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__productType": productType,
                    "c__dossierId": component.get("v.dossierId")
                }
            };
        }
        else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, pageReference,false);
    },
    redirectToOppty: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference,false);
    },
    linkOliToOsi: function (component, oli, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let commodity =  component.get("v.commodity");

        component.find('apexService').builder()
            .setMethod('linkOliToOsi')
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (response.error) {
                ntfSvc.error(ntfLib,response.errorMsg);
                return;
            }

            if(response.rateType){

                component.set("v.consumptionType", response.rateType);
                component.set("v.productRateType", response.rateType);
                //component.find("consumptionTable").validateConsumptionsData();

            }else {
                if(commodity === 'Electric') {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RateTypeIsNotDefinedOnTheProduct"));
                    return;
                } else {
                    component.set('v.consumptionType', "Single Rate");
                }
            }

            component.set("v.step", 6);//5
            //component.set("v.step", component.get("v.isConnectionOffering") ? 7 : 6);
            if (callback && typeof callback === "function") {
                callback(component, self);
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference,overrideUrl) {
        console.log('pageReference', pageReference);
        console.log('overrideUrl', overrideUrl);
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    workspaceAPI.getEnclosingTabId()
                    .then(function (enclosingTabId) {
                        console.log('enclosingTabId', enclosingTabId);
                        var self = this;
                        workspaceAPI.isSubtab({
                            tabId: enclosingTabId
                        })
                        .then(function(isSubtab) {
                            if (isSubtab) {
                                workspaceAPI.openSubtab({
                                    pageReference: pageReference,
                                    focus: true
                                }).then(function () {
                                    workspaceAPI.closeTab({
                                        tabId: self.enclosingTabId
                                    });
                                }).catch(function (error) {
                                    console.error('Redirect error', error);
                                });
                            } else {
                                console.log('Pagereference', pageReference);
                                workspaceAPI.openTab({
                                    pageReference: pageReference,
                                    focus: true
                                }).then(function () {
                                    console.log('enclosingTabId', self.enclosingTabId);
                                    workspaceAPI.closeTab({
                                        tabId: self.enclosingTabId
                                    });
                                }).catch(function (error) {
                                    console.error('Redirect error', error);
                                });
                            }
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference,overrideUrl);
                }
            }).catch(function (error) {
                console.log(error);
            });
    },
    initConsoleNavigation: function (component,wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                        /*if (window.localStorage && component.get("v.step") === 3 ) {
                            if (!window.localStorage['loaded']) {
                                window.localStorage['loaded'] = true;
                                helper.refreshFocusedTab(component);
                            } else {
                                window.localStorage.removeItem('loaded');
                            }
                        }*/

                        if (!window.location.hash && component.get("v.step") === 4) {//3  AS:verificare
                            window.location = window.location + '#loaded';
                            self.refreshFocusedTab(component);
                        }
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce")
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    }
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
        component.set("v.enableSaveButton", true);
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    reloadCompanyDivision: function (component) {
        let companyDivisionComponent = component.find("companyDivision");
        if (companyDivisionComponent) {
            if (companyDivisionComponent instanceof Array) {
                let companyDivisionComponentToObj = Object.assign({}, companyDivisionComponent);
                companyDivisionComponentToObj[0].reload();
            } else {
                companyDivisionComponent.reload();
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    reloadContractAccount: function (component) {
            let contractAccountComponent = component.find("contractAccountSelectionComponent");
            let accountId = component.get("v.accountId");
            if (contractAccountComponent) {
                if (contractAccountComponent instanceof Array) {
                    let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                    contractAccountComponentToObj[0].reset(accountId);
                } else {
                    contractAccountComponent.reset(accountId);
                }
            }
        },
    updateCompanyDivisionOnOpportunity: function (component) {
        let ntfSvc = component.find('ntfSvc');
        let ntfLib = component.find('ntfLib');
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCompanyDivisionInOpportunity")
            .setInput({
                opportunityId: opportunityId,
                companyDivisionId: component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateCommodityToDossier: function (component) {
        let ntfSvc = component.find('ntfSvc');
        let ntfLib = component.find('ntfLib');
        let dossierId = component.get("v.dossierId");
        let commodity = component.get("v.commodity");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCommodityToDossier")
            .setInput({
                dossierId: dossierId,
                commodity: commodity
            })
            .setResolve(function (response) {
                component.set("v.step", 0);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateContractAndContractSignedDateOnOpportunity: function (component) { /* diffe - questo metodo non c'era ma viene chiamato nel controller, aggiunto */
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let opportunityId = component.get("v.opportunityId");
        let contractId = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined) ? '' : component.get("v.contractId");
        let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
        let salesChannel = component.get('v.channelSelected');

        /* DEFECT MROCRMDEF-1029 --- Need to have 'market' = selectedContract.MarketName
        let market =  (!contractId || contractId === 'new') ? "" : "Free"; */

        let market = (!contractId || contractId == 'new') ? "" : component.get("v.market");
        let salesSupportUser = component.get("v.salesSupportUser");
        component.set("v.market",market);
        component
            .find("apexService")
            .builder()
            .setMethod("updateContractAndContractSignedDateOnOpportunity")
            .setInput({
                opportunityId: opportunityId,
                contractId: contractId,
                contractType: newContractDetails.type,
                contractName: newContractDetails.name,
                companySignedId: newContractDetails.companySignedId,
                salesUnitId: newContractDetails.salesUnitId,
                salesman: newContractDetails.salesman,
                salesChannel: salesChannel,
                salesSupportUser: salesSupportUser
            })
            .setResolve(function (response) {
                console.log("updateContractAndContractSignedDateOnOpportunity response", JSON.parse(JSON.stringify(response)));
                //self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, errorMsg);
                    return;
                } else {
                    if (response.purgedOLIs) {
                        component.set("v.opportunityLineItems", []);
                    }
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    reloadSendingChannel: function (component) {
        let contractSelectionComponent = component.find("sendingChannelSelection");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadAddressList();
            } else {
                contractSelectionComponent.reloadAddressList();
            }
        }
    },
    createConsumptions: function (component, consumptions, callback) {
        let self = this;
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let accountId = component.get('v.accountId');
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let consumptionConventions = component.get('v.consumptionConventions');
        consumptions.forEach(consumption => {
            consumption.Customer__c = accountId;
        });
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("insertConsumptionList")
            .setInput({
                'consumptionList': consumptions,
                'contractId': contractId,
                'consumptionConventions': consumptionConventions
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                component.set('v.consumptionList', response.consumptionList);
                self.consumptionListRefactor(component);
                self.calculateTotalExpectedRevenue(component, callback);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    // FF Activation/SwitchIn - Interface Check - Pack2
    setChannelAndOrigin: function (component) {
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    setSubProcessAndExpirationDate : function (component) {
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');

        let opportunity = component.get('v.opportunity');
        let subProcess = component.get('v.subProcess');
        let expirationDate = component.get('v.expirationDate');
        component.find("apexService").builder()
            .setMethod("setSubProcessAndExpirationDate")
            .setInput({
                opportunityId: opportunity.Id,
                subProcess:subProcess,
                expirationDate:JSON.stringify(expirationDate)
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    // [ENLCRO-659] Activation - Interface Check - Pack3
    setRequestedStartDate: function (component) {
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let opportunity = component.get('v.opportunity');
        let requestedStartDate = component.get('v.requestedStartDate');

        component.find("apexService").builder()
            .setMethod("setRequestedStartDate")
            .setInput({
                opportunityId: opportunity.Id,
                requestedStartDate: JSON.stringify(requestedStartDate),
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    //[ENLCRO-659] Activation - Interface Check - Pack3

    //FF OSI Edit - Pack2 - Interface Check
    deleteOsisAndOlisByCommodityChange: function (component) {
        let self = this;
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let opportunityId = component.get("v.opportunityId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOsisAndOlisByCommodityChange")
            .setInput({
                commodity: component.get("v.commodity"),
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.isDeleted){
                    component.set("v.opportunityServiceItems",[]);
                    component.set("v.opportunityLineItems",[]);
                    component.set("v.requestedStartDate",null);

                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    //FF OSI Edit - Pack2 - Interface Check

    checkSelectedContract: function(component) {
         let isValid = true;
        let isNotKamUser = (!component.get("v.userDepartment") || component.get("v.userDepartment") === undefined);
         if (!component.get("v.contractId")) {
             let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
             console.log('New Contract details:', JSON.stringify(newContractDetails));
            // Customer Signed Date should be taken out; START DATE - to be decided how it is going to be calculated
             //if (!newContractDetails.customerSignedDate || !newContractDetails.type) {
             //Customer Signed Date should be taken out; START DATE - to be decided how it is going to be calculated
             if (!newContractDetails.type) {
                 isValid = false;
             }
             if (component.get("v.channelSelected") === "Indirect Sales" && (!newContractDetails.salesman || !newContractDetails.salesUnitId)) {
                 isValid = false;
             }
             else if ((component.get("v.channelSelected") === "Magazin Enel" || component.get("v.channelSelected") === "Magazin Enel Partner"
                 || component.get("v.channelSelected") === "Call Center" || component.get("v.channelSelected") === "Info Kiosk")
                 && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
                 isValid = false;
             }
             //else if (component.get("v.channelSelected") === "Direct Sales (KAM)" && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
             else if (component.get("v.channelSelected") === "Direct Sales (KAM)"){
                 if(!newContractDetails.companySignedId){
                     isValid = false;
                 }
                 else if(!newContractDetails.salesUnitId && isNotKamUser && component.get("v.user").SalesUnitID__c !== null && component.get("v.user").SalesUnitID__c !== undefined){
                     isValid = false;
                 }
             }
             if (isValid) {
                 component.set("v.contractType", newContractDetails.type);
             }
         }
         return isValid;
    },
    // FF Activation/SwitchIn - Interface Check - Pack2


    //[ENLCRO-659] Activation - Interface Check - Pack3
    validateFields: function (component, event) {
        const labelFieldList = new Array();
        let valEffectiveDate = component.find("RequestedStartDate__c");
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');

        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('RequestedStartDate__c');
        }
        if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    },
    //[ENLCRO-659] Activation - Interface Check - Pack3
    //For Person Account the default Contract Type = "Residential"
    isBusinessClient: function (component) {//AS: added
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let accountId = component.get("v.accountId");
        //console.log('isBusinessClientAccount: '+ accountId);
        component.find('apexService').builder()
            .setMethod('isBusinessClient')
            .setInput({
                "accountId": accountId
            })
            .setResolve(function (response) {
                component.set('v.isBusinessClient', response.isBusinessClient);
                console.log('isBusinessClient: '+ response.isBusinessClient);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    //For Person Account the default Contract Type = "Residential"


    isNotWorkingDate: function (component) {
        let self = this;
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let requestedStartDate = component.get('v.requestedStartDate');
        let osiList = component.get("v.opportunityServiceItems");
        let companyDivComponent = component.find("companyDivision");
        let distributorVatNumber = osiList[0].Distributor__r.VATNumber__c;
        let opportunityId = component.get('v.opportunityId');

        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("isNotWorkingDate")
            .setInput({
                requestedStartDate: JSON.stringify(requestedStartDate),
                distributorVatNumber: distributorVatNumber,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (osiList && osiList.length !== 0){
                    component.set('v.validationRequestedStartDate', response.validationStartDate);
                    component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);

                    let requestedStartDateAsDate =  new Date(requestedStartDate);
                    let validationRequestedStartDate =  new Date(response.validationStartDate);

                    if (validationRequestedStartDate > requestedStartDateAsDate){
                        ntfSvc.error(ntfLib, $A.get("$Label.c.EnteredDateShouldBeGreaterOrEqualThan") + " "+ component.get('v.validationRequestedStartDate'));
                        return;
                    }
                }

                component.set('v.isNotWorkingDate',response.isNotWorkingDate);
                if (response.isNotWorkingDate && response.isValidationStartDateWorkingDay && response.isValidationStartDateWorkingDay === true){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.IsNotWorkingDay"));
                    return;
                }else{
                    if(!component.get("v.companyDivisionId") && osiList && osiList.length !== 0){
                        component.set("v.companyDivisionId",response.defaultCompanyDivisionId);
                        if (companyDivComponent) {
                            companyDivComponent.setCompanyDivision();
                        }
                    }

                    self.setRequestedStartDate(component);
                    component.set("v.step", 2);
                }

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    updateConsumptionConventionsOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let consumptionConventions = component.get("v.consumptionConventions");
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateConsumptionConventionsOnOpportunity")
            .setInput({
                consumptionConventions: consumptionConventions,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    loadOsi: function (component, osiId, callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems") || [];
        let newOsiList=[];
        //component.set("v.opportunityServiceItems",newOsiList);
        component.find('apexService').builder()
            .setMethod('loadOsi')
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {

                let updatedOsi = response.opportunityServiceItem;
                osiList.forEach(osi => {
                    if(osi.Id === updatedOsi .Id){
                        newOsiList.push(updatedOsi);
                    }else{
                        newOsiList.push(osi);
                    }
                });
                component.set("v.opportunityServiceItems",newOsiList);
                component.set('v.requestedStartDate', response.defaultStartDate);
                component.set('v.validationRequestedStartDate', response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                if(callback){
                    callback(updatedOsi);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    deleteOlisByContractAccountChange: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractAccountId = component.get("v.contractAccountId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOlisByContractAccountChange")
            .setInput({
                opportunityId: opportunityId,
                contractAccountId: contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.isDeleted){
                    component.set("v.opportunityLineItems",[]);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createPointsFromOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get('v.opportunityId');
        component.find('apexService').builder()
            .setMethod("createPointsFromOpportunity")
            .setInput({
                "opportunityId": opportunityId
            })
            .setResolve(function (response) {
                let osiList = response.opportunityServiceItems;
                console.log(JSON.stringify(osiList));

                component.set("v.step", 1);//0
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                if(component.find("pointSelection")) {
                    component.find("pointSelection").resetBox();
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateConsumptionConventionsCheckbox: function(component){
        let isNewContract = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined || component.get("v.contractId") === '');
        let commodity = component.get("v.commodity");
        if(isNewContract){
            if(commodity === 'Electric'){
                component.set("v.consumptionConventionsDisabled", false);
            }
        }
        else{
            component.set("v.consumptionConventionsDisabled", true);
        }
    },
    updateRegulatedTariffs: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get('v.opportunityId');
        let commodity = component.get('v.commodity');
        component.find('apexService').builder()
            .setMethod("updateRegulatedTariffs")
            .setInput({
                "opportunityId": opportunityId,
                "commodity": commodity
            })
            .setResolve(function (response) {
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    createOLIsForContractAddition: function(component, callback) {
         let ntfSvc = component.find('notify');
         let ntfLib = component.find('notifLib');
         let opportunityId = component.get("v.opportunityId");
         let contractId = component.get("v.contractId");
         let self = this;
         component.find('apexService').builder()
            .setMethod('createOLIsForContractAddition')
            .setInput({
                "opportunityId": opportunityId,
                "contractId": contractId
            })
            .setResolve(function (response) {
                console.log('createOLIsForContractAddition response', JSON.parse(JSON.stringify(response)));
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    console.log(response.errorMsg, response.errorTrace);
                    return;
                } else {
                    component.set("v.opportunityLineItems", response.opportunityLineItems);
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    consumptionListRefactor: function(component){
        let consumptions = component.get("v.consumptionList");
        if (consumptions){
            consumptions.forEach(function(element){
                element.QuantityJanuary__c = element.QuantityJanuary__c.toFixed(3);
                element.QuantityFebruary__c = element.QuantityFebruary__c.toFixed(3);
                element.QuantityMarch__c = element.QuantityMarch__c.toFixed(3);
                element.QuantityApril__c = element.QuantityApril__c.toFixed(3);
                element.QuantityMay__c = element.QuantityMay__c.toFixed(3);
                element.QuantityJune__c = element.QuantityJune__c.toFixed(3);
                element.QuantityJuly__c = element.QuantityJuly__c.toFixed(3);
                element.QuantityAugust__c = element.QuantityAugust__c.toFixed(3);
                element.QuantitySeptember__c = element.QuantitySeptember__c.toFixed(3);
                element.QuantityOctober__c = element.QuantityOctober__c.toFixed(3);
                element.QuantityNovember__c = element.QuantityNovember__c.toFixed(3);
                element.QuantityDecember__c = element.QuantityDecember__c.toFixed(3);
            });
            component.set("v.consumptionList", consumptions);
        }
    },
    validateOpportunity: function (component,callback) {
        const self = this;
        const ntfLib = component.find('ntfLib');
        const ntfSvc = component.find('ntfSvc');

        component.find('apexService').builder()
            .setMethod('validateOpportunity')
            .setInput({
                "dossierId": component.get("v.dossierId"),
                "opportunityId": component.get("v.opportunityId")
            })
            .setResolve(function (response) {
                let validOsi = false;
                let validDossier = false;

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.enableSaveButton", true);
                } else {
                    validOsi = response.validOsi;
                    validDossier = response.validDossier;
                }

                if(validOsi === false){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ThisOpportunityDoesntHaveRelatedOsi"));
                    component.set("v.enableSaveButton", true);
                }
                if(validDossier === false){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ThisDossierHasARelatedCase"));
                    component.set("v.enableSaveButton", true);
                }
                if (callback) {
                    callback(validOsi, validDossier);
                }
                if (!valid) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    setMassiveHelperParams: function (component, commodity) {
        let commodityToFtMap = component.get('v.commodityToFtMap');
        let massiveHelperParamsString = component.get('v.massiveHelperParams');
        if (component.get('v.useMassiveHelper') && commodityToFtMap && massiveHelperParamsString && commodity) {
            component.set("v.templateId", commodityToFtMap[commodity]);
            let massiveHelperParams = JSON.parse(massiveHelperParamsString);
            massiveHelperParams.commodity = commodity;
            component.set("v.massiveHelperParams", JSON.stringify(massiveHelperParams));
        }
    },
     calculateTotalExpectedRevenue: function (component, callback) {

         let self = this;
         const ntfLib = component.find('ntfLib');
         const ntfSvc = component.find('ntfSvc');
         let opportunityId = component.get('v.opportunityId');
         self.showSpinner(component);
         component.find("apexService").builder()
             .setMethod("calculateTotalExpectedRevenue")
             .setInput({
                 'opportunityId': opportunityId
             })
             .setResolve(function (response) {

                if(response.error == false) {
                    console.log('calculateTotalExpectedRevenue.setResolve totalExpectedRevenue',
                        response.totalExpectedRevenue);
                    if (callback) {
                        callback();
                    }
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    console.log(response.errorMsg, response.errorTrace);
                }
                 self.hideSpinner(component);
             })
             .setReject(function (error) {

                 console.log('calculateTotalExpectedRevenue.setReject');
                 ntfSvc.error(ntfLib, error);
                 self.hideSpinner(component);
             })
             .executeAction();
     }
})