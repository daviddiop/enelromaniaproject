/**
 * Created by Vlad Mocanu on 24/03/2020.
 */

({
    initialize: function (component) {
        let self = this;
        self.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let genericRequestDossierId = myPageRef.state.c__genericRequestDossierId;
        let interactionId = component.find('cookieSvc').getInteractionId();


        component.set("v.accountId", accountId);
        component.set("v.dossierId", dossierId);
        component.set("v.genericRequestDossierId", genericRequestDossierId);

        component.find('apexService').builder()
            .setMethod("initialize")
            .setInput({
                'accountId': accountId,
                'dossierId': dossierId,
                'genericRequestDossierId' : genericRequestDossierId,
                "interactionId": interactionId
            })
            .setResolve(function (response) {
                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId, response.parentDossierId);
                }

                if (response.supplyId) {
                    component.set('v.supplyId', response.supplyId);
                    component.set('v.supplyStatus', response.supplyStatus);
                    component.set('v.supplyType', response.supplyType);

                    if (response.supplyStatus !== 'Active') {
                        component.set('v.isBillingProfileRequired', true)
                    } else {
                        component.set('v.isBillingProfileRequired', false)
                    }
                }

                if (response.parentDossierId) {
                    component.set('v.parentDossierId', response.parentDossierId);
                }

                if (response.cases) {
                    component.set('v.caseTile', response.cases);
                }

                component.set('v.accountId', response.accountId);
                component.set('v.dossierId', response.dossierId);
                component.set("v.billingProfileId", response.billingProfileId);

                self.setAuthorityStep(component, response);
                self.hideSpinner(component);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    setChannelAndOrigin: function (component, helper) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("SetChannelAndOrigin")
            .setInput({
                dossierId: component.get("v.dossierId"),
                selectedOrigin: component.get("v.selectedOrigin"),
                selectedChannel: component.get("v.selectedChannel"),
            })
            .setResolve(function (response) {
                if(component.get('v.step') === 0) {
                    helper.changeAuthorityStep(component, helper, 1);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    createCase: function (component, dossierId) {
        let self = this;
        let supplyId = component.get('v.supplyId');
        let accountId = component.get('v.accountId');
        let selectedOrigin = component.get('v.selectedOrigin');
        let selectedChannel = component.get('v.selectedChannel');
        let caseList = component.get('v.caseTile');
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        self.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("createCase")
            .setInput({
                'dossierId': dossierId,
                'accountId': accountId,
                'supplyId': supplyId,
                'selectedOrigin': selectedOrigin,
                'selectedChannel': selectedChannel,
                'caseList': JSON.stringify(caseList)
            })
            .setResolve(function (response) {
                self.hideSpinner(component)
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                let responseCases = response.cases;

                component.set('v.caseTile', responseCases);
                component.set('v.caseId', responseCases[0].Id);
                self.showSpinner(component);
                })
            .setReject(function (response) {
                self.hideSpinner(component)
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    deleteCase: function (component, event) {
        let deleteRecordId = event.getParam("deleteRecord");
        let caseList = component.get("v.caseTile");
        let items = [];

        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        component.set("v.caseTile", items);
    },

    deleteCanceledCase: function (component, deleteRecordId) {
        let self = this;
        let caseList = component.get("v.caseTile");
        let items = [];

        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        component.set("v.caseTile", items);

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("DeleteCanceledCase")
            .setInput({
                caseId: deleteRecordId
            }).setResolve(function (response) {
            self.hideSpinner(component);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        })
            .executeAction();
    },

    handleAuthorityCancel: function (component, helper, cancelReason, detailsReason) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("CancelProcess")
            .setInput({
                dossierId: component.get("v.dossierId"),
                cancelReason: cancelReason,
                detailsReason: detailsReason
            }).setResolve(function (response) {
            self.hideSpinner(component);
            self.redirectToDossier(component, helper);
        }).setReject(function (response) {
            const errors = response.getError();
            ntfSvc.error(ntfLib, JSON.stringify(errors));
        })
            .executeAction();
    },

    handleAuthoritySaveDraft: function(component, helper) {
        let self = this;
        let dossierId = component.get('v.dossierId');
        let caseList = component.get("v.caseTile");
        let billingProfileId = component.get("v.billingProfileId");
        let selectedChannel = component.get("v.selectedChannel");
        let selectedOrigin = component.get("v.selectedOrigin");
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        component.find("apexService").builder()
            .setMethod("SaveDraftCase")
            .setInput({
                "dossierId": dossierId,
                "caseList": JSON.stringify(caseList),
                "billingProfileId": billingProfileId,
                'selectedOrigin': selectedOrigin,
                'selectedChannel': selectedChannel
            })
            .setResolve(function(response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.redirectToDossier(component, helper);
            })
            .setReject(function (response) {
                ntfSvc.error(ntfLib, response);
            })
            .executeAction();
    },

    handleAuthoritySave: function (component, helper) {
        let self = this;
        let dossierId = component.get('v.dossierId');
        let caseList = component.get("v.caseTile");
        let billingProfileId = component.get("v.billingProfileId");
        let selectedChannel = component.get("v.selectedChannel");
        let selectedOrigin = component.get("v.selectedOrigin");
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        if (dossierId === "") {
            ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredFields'));
            component.set("v.step", 0)
        } else {
            helper.showSpinner(component);
            component.find('apexService').builder()
                .setMethod("SaveCase")
                .setInput({
                    "dossierId": dossierId,
                    "caseList": JSON.stringify(caseList),
                    "billingProfileId": billingProfileId,
                    'selectedOrigin': selectedOrigin,
                    'selectedChannel': selectedChannel
                })
                .setResolve(function (response) {
                    helper.hideSpinner(component);
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    self.redirectToDossier(component, helper);
                })
                .setReject(function (response) {
                    helper.hideSpinner(component);
                    const errors = response.getError();
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                    return;
                })
                .executeAction();
        }
    },

    getSupplyStatus: function (component, selectedSupplyId) {
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("getSupplyStatusById")
            .setInput({
                'supplyId': selectedSupplyId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                component.set('v.supplyStatus', response.supplyStatus);
                component.set('v.supplyType', response.supplyType);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },

    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = "ANRECompensation | Salesforce";
                    if (!window.location.hash && component.get("v.step") === 1) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
        });
        document.title = "ANRECompensation | Salesforce";
    },

    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error on Console Navigation ", error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },

    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },

    updateUrl: function (component, accountId, dossierId, parentDossierId) {
        let navService = component.find("navService");
        let state = {
            "c__accountId": accountId,
            "c__dossierId": dossierId
        };

        if (parentDossierId){
            state["c__parentDossierId"] = parentDossierId;
        }

        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ANRECompensationWizard',
            },
            state: state
        };
        navService.navigate(pageReference, true);
    },

    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent) {
            if (supplySearchComponent instanceof Array) {
                let supplyComponentToObj = Object.assign({}, supplySearchComponent);
                supplyComponentToObj[0].resetBox();
            } else {
                supplySearchComponent.resetBox();
            }
        }
    },

    setHideNewBillingButton: function(component) {
        let numberOfBillingProfiles =  component.find('billingProfileSelection').countBillingProfiles();

        if (numberOfBillingProfiles === 0) {
            component.set("v.hideProfileNewButton", false);
        } else {
            component.set("v.hideProfileNewButton", true);
        }
    },

    setAuthorityStep: function (component, response) {
        let step = 2;

        if (!response.dossier ||
            !response.dossier.Channel__c ||
            !response.dossier.Origin__c ||
            !response.cases) {
            step = 1;
        }
        this.changeAuthorityStep(component, this, step);
    },

    changeAuthorityStep: function (component, helper, stepNumber, isNext) {
        let self = this;
        component.set('v.disableOriginChannel', stepNumber > 1);

        if (stepNumber === 2 && isNext) {
            let caseList = component.get('v.caseTile');
            for (let i = 0; i < caseList.length; i++) {
                if (caseList[i].CompensationType__c === undefined) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.CompensationTypeNotSelected"));
                    self.changeAuthorityStep(component, helper, 1);
                    return;
                }
            }
            helper.setHideNewBillingButton(component);
            self.changeAuthorityStep(component, helper, 2);
            return;
        }

        component.set("v.step", stepNumber);
    },

    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },

    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    }
});