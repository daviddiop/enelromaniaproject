/**
 * Created by Vlad Mocanu on 24/03/2020.
 */

({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        helper.initConsoleNavigation(component, $A.get('$Label.c.ANRECompensation'));
        helper.initialize(component);
    },

    handleOriginChannelSelection: function (component, event, helper) {
        let selectedOrigin = event.getParam("selectedOrigin");
        let selectedChannel = event.getParam("selectedChannel");

        if (selectedOrigin && selectedChannel) {
            component.set('v.selectedOrigin', selectedOrigin);
            component.set('v.selectedChannel', selectedChannel);
            helper.setChannelAndOrigin(component, helper);
            component.set('v.originAndChannelSelected', true)
        }
    },

    handleSupplyResult: function (component, event, helper) {
        let selectedSupplyId = String(event.getParam("selected"));
        let dossierId = component.get('v.dossierId');

        component.set('v.supplyId', selectedSupplyId);

        if (selectedSupplyId) {
            helper.getSupplyStatus(component, selectedSupplyId);
        }

        component.set('v.isFirstEdit', true);
        component.set('v.showNewCase', true);
        helper.createCase(component, dossierId);
    },

    handleDeleteCaseTile: function (component, event, helper) {
        helper.deleteCase(component, event);
    },

    handleSuccessCase: function (component, event, helper) {
        component.set("v.showNewCase", false);
        helper.initialize(component);
        helper.hideSpinner(component);
    },

    handleEditCaseSuccess: function (component, event, helper) {
        helper.initialize(component);
    },

    getBillingProfileRecordId: function (component, event, helper) {
        component.set('v.billingProfileId', event.getParam('billingProfileRecordId'));
    },

    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = parseInt(buttonId.replace('confirmStep', '')) + 1 ;
        helper.changeAuthorityStep(component, helper, stepNumber, true);
    },

    editStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber =  parseInt(buttonId.replace('editStep', ''));
        helper.changeAuthorityStep(component, helper, stepNumber);
    },

    closeCaseModal: function (component, event, helper) {
        let isFirstEdit = component.get('v.isFirstEdit');
        if(isFirstEdit) {
            let deleteRecordId = event.getParam('caseId')
            helper.deleteCanceledCase(component, deleteRecordId);
            component.set('v.isFirstEdit', false)
        }
        component.set('v.showNewCase', false);
        helper.hideSpinner(component);
    },

    cancel: function (component) {
        component.find('cancelReasonSelection').open();
    },

    saveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");

        helper.handleAuthorityCancel(component, helper, cancelReason, detailsReason);
    },

    saveDraft: function (component, event, helper) {
        helper.handleAuthoritySaveDraft(component, helper);
    },

    save: function (component, event, helper) {
        helper.handleAuthoritySave(component, helper)
    }
});