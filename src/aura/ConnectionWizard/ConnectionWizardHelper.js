({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);

        component.find('apexService').builder()
            .setMethod("initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {

                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.billingProfileId", response.billingProfileId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    if (response.dossierId && !dossierId) {
                        self.updateUrl(component, accountId, response.dossierId);
                    }

                    var step = 0;
                    if (response.billingProfileId) {
                        step = 3;
                    } else if (response.caseTile) {
                        if(response.caseTile.length !== 0){
                            step = 1;
                        }
                    }

                    var dossierVal = component.get("v.dossier")

                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }

                    if (response.caseTile) {
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    }
                    if (component.find("billingProfileSelection")) {
                        component.find("billingProfileSelection").reset(component.get("v.accountId"));
                    }
                    component.set("v.step", step);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction()
    },
    saveDraftBillingProfile: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("saveDraftBP")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                billingProfileId: billingProfileId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateCase: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");

        const dossierId = component.get("v.dossierId");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                billingProfileId: billingProfileId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, dossierId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__ConnectionWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 0);
        }

        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService")
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component,wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                workspaceAPI.setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                workspaceAPI.setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    }
})