({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);

        helper.initialize(component, event, helper);
        helper.hideSpinner(component, 'spinnerSection');
        helper.initConsoleNavigation(component, $A.get('$Label.c.Connection'));
    },
    getBillingProfileRecordId: function (component, event, helper) {
        component.set("v.billingProfileId", event.getParam("billingProfileRecordId"));
        if (component.get("v.billingProfileId")) {
            component.set("v.step", 3)
        }
    },
    handleCase: function (component, event, helper) {
        component.set("v.showNewCase", true);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
     handleSuccessCase: function (component, event, helper) {
        helper.initialize(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((caseList.length === 0) || (!billingProfileId)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.updateCase(component, helper);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if (!component.get("v.caseTile") || component.get("v.caseTile").length === 0) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
                    return;
                }
                if (component.get("v.billingProfileId")) {
                    component.set("v.step", 3)
                } else {
                    component.set("v.step", 2);
                }

                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            default:
                break;
        }
    },
    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftBillingProfile(component, helper);
    }
})