/**
 * Created by BADJI on 20/05/2019.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const opportunityId = myPageRef.state.c__opportunityId;

        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);

        helper.initialize(component);
        helper.initConsoleNavigation(component,$A.get("$Label.c.Activation"));
    },
    cancel: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty);
    },
    save: function (component, event, helper) {
        component.set("v.isSaving", true);
        helper.createPrivacyChangeRecord(component);
    },
    saveDraft: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.createPrivacyChangeRecord(component);
    },
    nextStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        var ntfLib = component.find("ntfLib");
        var ntfSvc = component.find("ntfSvc");
        if (buttonPressed === "confirmStep") {
            component.set("v.step", 1);
        } else if (buttonPressed === "confirmStep1") {
            component.find("pointSelection").resetBox();
            if (
                !component.get("v.opportunityServiceItems") ||
                component.get("v.opportunityServiceItems").length === 0
            ) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredSupply"));
                return;
            }
            if (component.get("v.isVisibleContractAddition")) {
                component.set("v.step", 2);
            } else {
                component.set("v.step", 3);
                helper.saveOpportunity(component,"Prospecting");
            }
        } else if (buttonPressed === "confirmStep2") {

            if ((!component.get("v.contractId")) && (component.get("v.customerSignedDate") === null)) {
                ntfSvc.error(ntfLib,"Customer signed date is required");
                return;
            }

            component.set("v.step", 3);
            helper.updateContractAndContractSignedDateOnOpportunity(component);
            helper.saveOpportunity(component,"Prospecting");
        } else if (buttonPressed === 'confirmStep3') {
            if (!component.get("v.contractAccountId")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountRequired")); //$A.get('$Label.c.BillingProfileRequired')
                return;
            }
            helper.updateOsi(component);
            component.set("v.step", 4);
        } else if (buttonPressed === "confirmStep4") {
            let prdList = component.get("v.opportunityLineItems");
            if (!prdList && prdList.length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct")); //$A.get('$Label.c.OnlyOneProductRequired')); //almeno un PRODOTTO
                return;
            }

            var utilityPrds = prdList.filter(
                oli => oli.Product2.RecordType.DeveloperName === "Utility"
            );
            if (!utilityPrds || utilityPrds.length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct")); //almeno 1 UTILITY
                return;
            }
            let oli = utilityPrds[0];
            helper.linkOliToOsi(component, oli);
            component.set("v.step", 5);
        }
    },
    editStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep0") {
            component.set("v.step", 0);
        } else if (buttonPressed === "returnStep1") {
            component.set("v.step", 1);
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 2);
        } else if (buttonPressed === "returnStep3") {
            component.set("v.step", 3);
        } else if (buttonPressed === "returnStep4") {
            component.set("v.step", 4);
        }
    },
    /*
    getBillingProfileRecordId: function (component, event, helper) {
        component.set(
            "v.billingProfileId",
            event.getParam("billingProfileRecordId")
        );
    },
    saveBillingSection: function (component, event, helper) {
        const billingProfileId = component.get("v.billingProfileId");
        helper.updateOsi(component);
        helper.saveOpportunity(component, "Value Proposition");
        helper.setPercentage(component, "Value Proposition");
    },
    saveOsiList: function (component, event, helper) {
        helper.saveOpportunity(component, "Qualification");
        helper.setPercentage(component, "Qualification");
    },*/
    handlePointResult: function (component, event, helper) {
        const ntfLib = component.find("ntfLib");
        const ntfSvc = component.find("ntfSvc");
        let servicePointId = event.getParam("servicePointId");
        let servicePointCode = event.getParam("servicePointCode");
        let suppliesByServicePointCode = event.getParam("suppliesByServicePointCode");

        if (suppliesByServicePointCode.length > 0) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.ServicePointHasRelatedSupply"));
            return;
        }

        component.set("v.searchedPointId", servicePointId);
        component.set("v.searchedPointCode", servicePointCode);
        component.set("v.showNewOsi", true);
    },
    handleNewOsi: function (component, event, helper) {
        let osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedPointId", "");
        component.set("v.searchedPointCode", "");
        //component.find("pointSelection").resetBox();
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(
            component,
            "Proposal/Price Quote",
            helper.openProductSelection
        );
        helper.setPercentage(component, "Proposal/Price Quote");
    },
    handleOsiDelete: function (component, event, helper) {
        const osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        const items = [];
        for (let i = 0; i < osiList.length; i++) {
            if (osiList[i].Id != osiId) {
                items.push(osiList[i]);
            }
        }
        if (items.length == 0) {
            component.set("v.step", 0);
        }
        component.set("v.opportunityServiceItems", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },

    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find('ntfLib');
        const ntfSvc = component.find('ntfSvc');
        component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
        component.set("v.dontProcess", event.getParam('dontProcess'));
        if (!component.get("v.isSaving")) {
            helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToOppty);
        }
        if (component.get("v.isSaving")) {
            if (component.get("v.dontProcess")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
                $A.get('e.force:refreshView').fire();
                return;
            }
            helper.saveOpportunity(component, "Closed Won", helper.redirectToOppty);
        }
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        if(event.getParam("divisionId")) {
            component.set("v.companyDivisionId", event.getParam("divisionId"));
            component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));

            if ( (component.get("v.step") === 0) && component.get("v.isCompanyDivisionEnforced") ) {
                component.set("v.step", 1);
            }

            // Apply filter on Contract Account
            if (component.find("contractAccountSelectionComponent")) {
                helper.reloadContractAccount(component);
            }
            // Apply filter on Contract Addition
            if (component.get("v.isVisibleContractAddition")){
                helper.reloadContractAddition(component);
            }
        }
    },
    getContractAccountRecordId: function (component, event, helper) {
        component.set(
            "v.contractAccountId",
            event.getParam("contractAccountRecordId")
        );
    },

    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        component.set("v.contractId", contractSelected);
        component.set("v.customerSignedDate", customerSignedDate);
    },
});