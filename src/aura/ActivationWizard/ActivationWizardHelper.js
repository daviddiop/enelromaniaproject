/**
 * Created by BADJI on 20/05/2019.
 */
({
    initialize: function (component) {
        var self = this;
        var myPageRef = component.get("v.pageReference");
        var accountId = myPageRef.state.c__accountId;
        var opportunityId = myPageRef.state.c__opportunityId;
        var ntfLib = component.find('ntfLib');
        var ntfSvc = component.find('ntfSvc');
        var companyDivComponent = component.find("companyDivision");
        component.set("v.opportunityId", '');
        component.set("v.accountId", accountId);
        component.set('v.interactionId', component.find('cookieSvc').getInteractionId());

        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "interactionId": component.get('v.interactionId')
            })
            .setResolve(function (response) {
                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunity", response.opportunity);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.contractId", response.contractIdFromOpp);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
                component.set("v.companyDivisionId", response.opportunityCompanyDivisionId);
                //if(response.opportunityCompanyDivisionId){
                    if (companyDivComponent) {
                        //companyDivComponent.wizardCompanyDivisionId = response.opportunityCompanyDivisionId;
                        companyDivComponent.setCompanyDivision();
                        //self.reloadCompanyDivision(component);
                    }
                //}
                if (response.opportunityId && !opportunityId) {
                    self.updateUrl(component, accountId, response.opportunityId);
                }

                self.reloadContractAddition(component);

                var step = 0;
                if ((response.opportunityLineItems.length !== 0) && (response.contractAccountId) && (response.opportunityServiceItems.length !== 0)) {
                    step = 4;
                } else if ((response.contractAccountId) && (response.opportunityServiceItems.length !== 0)) {
                    step = 4;
                } else if (response.opportunityServiceItems.length !== 0) {
                    step = 2;
                } else if (component.get("v.isCompanyDivisionEnforced") ) {
                    step = 1;
                }

                var opportunityVal =response.opportunity;
                if (opportunityVal) {
                    if (opportunityVal.StageName === 'Closed Lost' || opportunityVal.StageName === 'Closed Won') {
                        component.set("v.isClosed", true);
                    }
                }
                component.set("v.step", step);
                component.set("v.accountId",response.accountId);
                component.set("v.account",response.account);
                if (component.find("contractAccountSelectionComponent")) {
                    self.reloadContractAccount(component, component.get("v.accountId"));
                }
                self.hideSpinner(component);
                //self.setPercentage(component,response.stage);
                //$A.get('e.force:refreshView').fire();

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    validateOsi: function (component, osiId) {
        var self = this;
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('checkOsi')
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi =response.opportunityServiceItem;
                osiList.push(newOsi);
                if (osiList.length == 1) {
                    self.updateCompanyDivisionOnOpportunity(component);
                }
                component.set("v.step", 1);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                component.find("pointSelection").resetBox();
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            }).executeAction();
    },
    saveOpportunity: function (component, stage, callback) {
        let self = this;
        self.showSpinner(component);
        //self.setPercentage(component, stage);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "stage": stage
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        var self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let contractAccountId = component.get("v.contractAccountId");
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, opportunityId) {
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__ActivationWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId
            }
        };
        navService.navigate(pageReference);
    },
    openProductSelection: function (component, helper) {
        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__ProductSelectionWrp',
            },
            state: {
                "c__opportunityId": component.get("v.opportunityId")
            }
        };
        helper.redirect(component, pageReference);
    },
    redirectToOppty: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    linkOliToOsi: function (component, oli, callback) {
        var self = this;
        self.showSpinner(component);
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems");
        component.find('apexService').builder()
            .setMethod('linkOliToOsi')
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (response.error) {
                ntfSvc.error(ntfLib,response.errorMsg);
                return;
            }
            if (callback && typeof callback === "function") {
                callback(component, self);
            } else {
                //ntfSvc.success(ntfLib, '');
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    goToAccount: function (component) {
        let accountId = component.get("v.accountId");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                        workspaceAPI.openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            workspaceAPI.closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error on Console Navigation ", error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component,wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                        /*if (window.localStorage && component.get("v.step") === 3 ) {
                            if (!window.localStorage['loaded']) {
                                window.localStorage['loaded'] = true;
                                helper.refreshFocusedTab(component);
                            } else {
                                window.localStorage.removeItem('loaded');
                            }
                        }*/
                        if (!window.location.hash && component.get("v.step") === 3) {
                            window.location = window.location + '#loaded';
                            self.refreshFocusedTab(component);
                        }
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = "Lightning Experience | Salesforce";
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    }
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    reloadCompanyDivision: function (component) {
        let companyDivisionComponent = component.find("companyDivision");
        if (companyDivisionComponent) {
            if (companyDivisionComponent instanceof Array) {
                let companyDivisionComponentToObj = Object.assign({}, companyDivisionComponent);
                companyDivisionComponentToObj[0].reload();
            } else {
                companyDivisionComponent.reload();
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    reloadContractAccount: function (component, accVal) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accVal);
            } else {
                contractAccountComponent.reset(accVal);
            }
        }
    },
    updateCompanyDivisionOnOpportunity: function (component) {
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCompanyDivisionInOpportunity")
            .setInput({
                opportunityId: opportunityId,
                companyDivisionId: component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    }
})