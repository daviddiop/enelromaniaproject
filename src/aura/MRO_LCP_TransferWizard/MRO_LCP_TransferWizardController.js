/**
 * Created by goudiaby on 28/05/2019.
 */
({
    init: function (component, event, helper) {
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        component.set('v.wizardHeaderLoaded', false);
        let disabledOpportunityServiceFields={
            distributor: true,
            availablePower:true,
            contractualPower:true,
            powerPhase:true,
            pressure:true,
            pressureLevel:true,
            consumptionCategory: true,
            voltageSetting:true,
            voltageLevel:true,
            startDate:true,
            flatRate:true,
            nonDisconnectable: true,
            supplyOperation: true
        };
        let hiddenOpportunityServiceFields={
            isNewConnection: true
        };

        component.set("v.disabledOpportunityServiceFields",disabledOpportunityServiceFields);
        component.set("v.hiddenOpportunityServiceFields",hiddenOpportunityServiceFields);
        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);

        component.set("v.dossierId", dossierId);
        helper.initialize(component);
        helper.initConsoleNavigation(component, $A.get('$Label.c.Transfer'));

    },
    getContractAccountRecordId: function (component, event, helper) {
        component.set(
            "v.contractAccountId",
            event.getParam("contractAccountRecordId")
        );
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
        //component.set("v.isSaving", false);
        //helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty);
    },
    onSaveCancelReason: function (component, event, helper) {
        console.log("cancel========="+JSON.stringify(event));
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        component.set("v.isSaved", false);
        if (cancelReason) {
            component.set("v.savingWizard", false);
            helper.redirectToDossier(component, helper);
            //helper.saveOpportunity(component, "Closed Lost", helper.redirectToDossier, cancelReason, detailsReason);
        }
    },
    save: function (component, event, helper) {
        component.set("v.isSaving", true);
        component.set("v.enableSaveButton", false);
        //component.set("v.savingWizard", true);
        component.set("v.isSaved", true);
        helper.validateOpportunity(component,function (valid) {
            if(valid){
                helper.createPrivacyChangeRecord(component);
                helper.updateDossierStatus(component);
            }else {
                component.set("v.enableSaveButton", true);
                component.set("v.isSaving", false);
            }
        });


    },
    saveDraft: function (component, event, helper) {
        component.set("v.isSaving", false);
        component.set("v.isSaved", false);
        helper.createPrivacyChangeRecord(component);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if(component.get('v.originSelected') && component.get('v.channelSelected')) {
                    component.set('v.disableOriginChannel',true);
                    component.set('v.disableWizardHeader',false);

                    helper.setChannelAndOrigin(component);
                    component.set("v.step", 2);
                    console.log('stepdentro' +  component.get("v.step"));

                }else {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.SelectOriginAndChannel'));
                    return;
                }

                break;
            case 'confirmStep2':
                helper.setExpirationDate(component);
                component.set("v.step", 3);
                break;
            case 'confirmStep3':
                helper.commodityChangeActions(component);
                component.set("v.step", 4);
                break;
            case 'confirmStep4':
                if(!component.get('v.requestedStartDate')){
                    helper.validateStartDateField(component);
                    //ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }
                helper.validateAllOsis(component,true,function (validOsis) {
                    if(validOsis) {
                        helper.validateOsis(component, function () {
                            helper.updateTraderToOsi(component);
                            component.set("v.step", 5);
                            let contractAccountDivComponent = component.find("contractAccountSelectionComponent");
                            if (contractAccountDivComponent) {
                                contractAccountDivComponent.reload();
                            }
                            let contractSelectionComponent = component.find("contractSelectionComponent");
                            if (contractSelectionComponent) {
                                contractSelectionComponent.reloadContractList();
                            }
                        });
                    }
                });
                if (component.get("v.isVisibleContractAddition")) {
                    if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                        component.set("v.userDepartment", '');
                    }
                    if(!component.get("v.userDepartment")){
                        if(component.get("v.channelSelected") === "Indirect Sales"){
                            component.set("v.salesUnit", null);
                        }
                        component.set("v.salesUnitId", '');
                        component.set("v.salesman", '');
                        component.set("v.companySignedId", '');

                        if(component.get("v.opportunity") && component.get("v.opportunity").SalesUnit__c){
                            component.set("v.salesUnit", component.get("v.opportunity").SalesUnit__c);
                            component.set("v.salesUnitId", component.get("v.opportunity").SalesUnit__c);
                        }
                        if(component.get("v.opportunity") && component.get("v.opportunity").CompanySignedBy__c){
                            component.set("v.companySignedId", component.get("v.opportunity").CompanySignedBy__c);
                        }
                        if(component.get("v.opportunity") && component.get("v.opportunity").Salesman__c){
                            component.set("v.salesman", component.get("v.opportunity").Salesman__c);
                        }
                        console.log('opp: '+JSON.stringify(component.get("v.opportunity")));
                        if(component.get('v.channelSelected') !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").SalesUnit__c){
                            if(component.get("v.salesUnit") && component.get("v.salesUnit").SalesDepartment__c === component.get('v.channelSelected')){
                                let salesAccountId = component.get("v.salesUnit").Id;
                                component.set("v.salesUnitId", salesAccountId);
                            }
                        }
                        if(component.get("v.channelSelected") !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").CompanySignedBy__c){
                            if(!component.get("v.salesUnit")){
                                component.set("v.companySignedId", "");
                            }
                            else{
                                if(component.get("v.channelSelected") !== "Back Office Sales" && component.get("v.channelSelected") !== "Back Office Customer Care"){
                                    let userId = component.get("v.user").Id;
                                    component.set("v.companySignedId", userId);
                                }
                            }
                        }
                    }
                }
                break;
            case 'confirmStep5':

                if (!helper.checkSelectedContract(component)) {
                    ntfSvc.error(ntfLib,$A.get("$Label.c.RequiredFields"));
                    return;
                }
                if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                    let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                    component.set('v.streetName',pointStreetId);
                    if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                        let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                        component.set("v.vatNumber", vatNumber)
                        let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                        component.set("v.selfReadingEnabled", selfReadingEnabled)
                    }

                    if(component.get("v.companyDivisionId")){
                        let companyDivisionId = component.get("v.companyDivisionId");
                        component.set('v.pointAddressProvince',companyDivisionId);
                    }
                }
                helper.updateContractAndContractSignedDateOnOpportunity(component);
                break;
            case 'confirmStep6':
                if (!component.get("v.contractAccountId")) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.SelectContractAccount'));
                    return;
                }

                /*
                let contractAccountComponent = document.getElementById('contractAccountCmp');
                let contractsInputs=contractAccountComponent.querySelectorAll("input");
                let selected=false;
                contractsInputs.forEach(contractAccount => {
                    if(contractAccount.checked === true){
                        selected=true;
                    }

                });
                if(selected === false){
                    ntfSvc.error(ntfLib, $A.get('$Label.c.SelectContractAccount'));
                    return;
                }

                 */
                helper.deleteOlisByContractAccountChange(component);
                helper.updateOsi(component);
                component.set("v.step", 7);
                //$A.get('e.force:refreshView').fire();
                break;

            case 'confirmStep7':
                let prdList = component.get("v.opportunityLineItems");
                if (!prdList && prdList.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct"));
                    return;
                }
                const utilityPrds = prdList.filter(oli => oli.Product2.RecordType.DeveloperName === 'Utility');
                if (!utilityPrds || utilityPrds.length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct"));
                    return;
                }
                let oli = utilityPrds[0];
                helper.linkOliToOsi(component, oli);
                helper.updateConsumptionConventionsCheckbox(component);
                helper.updateRegulatedTariffs(component);
                break;
            case 'confirmStep8':

                let consumptionCmp = component.find("consumptionTable");
                let consumptionList = consumptionCmp.getAllConsumptionObjects();
                console.log("consumptionList=="+consumptionList.length);
                let osis = component.get("v.opportunityServiceItems");
                let osisTotalEstimated = [];
                let osisIds = [];
                osis.forEach(osi=>{
                    osisTotalEstimated.push(consumptionCmp.calculateTotalEstiamted(osi.Id));
                    osisIds.push(osi.Id);
                });
                helper.createConsumptions(component, consumptionList,osisIds, osisTotalEstimated, function () {
                    let isEnelDistributor = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                    let selectedCommodity = component.get("v.opportunityServiceItems")[0].RecordType.DeveloperName;

                    component.set("v.step", 9);
                    component.set("v.isEnelDistributor", isEnelDistributor);

                    if (!isEnelDistributor ||  selectedCommodity === 'Gas'){
                        component.set("v.metersValidated", true);
                    }

                    if(selectedCommodity === 'Electric' && component.get("v.reloadMeterReading")){
                        component.set("v.retrieveSapIndex", false);
                        helper.getMetersOneAtTime(component, 9);
                    }

                });

                helper.updateConsumptionConventionsOnOpportunity(component);

                if (!component.get("v.metersActivated")){
                    component.set("v.metersValidated", true);
                }

                break;
            case 'confirmStep9':
                let isEnelDistributor = JSON.parse(component.get("v.isEnelDistributor"));
                let selectedCommodity = component.get("v.opportunityServiceItems")[0].RecordType.DeveloperName;

                if(component.get("v.skipMetersValidation") && selectedCommodity === 'Electric' && isEnelDistributor === true){
                    let meterListCmp = component.find('meterList');
                    if(meterListCmp){
                        if(Array.isArray(meterListCmp)){
                            meterListCmp.forEach(function(element){
                                element.validateMeterInfo();
                            });
                        } else {
                            meterListCmp.validateMeterInfo();
                        }
                    } else {
                        ntfSvc.error(ntfLib, 'No meter list available to validate');
                        return;
                    }
                }

                if(isEnelDistributor && selectedCommodity === 'Electric') {
                    helper.updateIndexValues(component, function () {
                      helper.getMetersOneAtTime(component,10);
                    });
                }

                if(component.get('v.contractAccountId')){
                   helper.reloadSendingChannel(component);
                }
                
                component.set("v.step", 10);


                break;
            case 'confirmStep10':
                helper.saveSendingChannel(component);
                component.set("v.step", 11);
                component.set("v.enableSaveButton", true);
                break;

            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                component.set('v.disableOriginChannel',false);
                component.set('v.disableWizardHeader',true);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                component.set("v.step", 4);
                break;
            case 'returnStep5':
                component.set("v.step", 5);
                break;
            case 'returnStep6':
                component.set("v.step", 6);
                break;
            case 'returnStep7':
                component.set("v.step", 7);
                break;
            case 'returnStep8':
                component.set("v.step", 8);
                /*added by Giuseppe Mario Pastore on 15-06-2020 - resetting these values, when the operator will try
                * to modify again in the same sessione the index values, he will be able to do it*/
                component.set("v.metersInfoMap",{});
                component.set("v.metersInfo",[]);
                component.set("v.metersCodes",[]);
                /*added by Giuseppe Mario Pastore on 15-06-2020 */
                break;
            case 'returnStep9':
                component.set("v.step", 9);
                if(component.get("v.metersActivated")){
                    component.set("v.metersValidated", false);
                }

                /*added by Giuseppe Mario Pastore on 15-06-2020 - resetting these values, when the operator will try
                * to modify again in the same sessione the index values, he will be able to do it*/
                let isEnelDistributor = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                let selectedCommodity = component.get("v.opportunityServiceItems")[0].RecordType.DeveloperName;

                if (!isEnelDistributor ||  selectedCommodity === 'Gas'){
                    component.set("v.metersValidated", true);
                }
                component.set("v.metersInfoMap",{});
                component.set("v.metersInfo",[]);
                component.set("v.metersCodes",[]);
                /*added by Giuseppe Mario Pastore on 15-06-2020 */
                break;
            case 'returnStep10':
                component.set("v.step", 10);
                break;
            default:
                break;
        }
    },

    handleSelectCommodity: function (component, event, helper) {
        component.set("v.step", 3);
        let selectedCommodity = event.getParam("selectedCommodity");
        if (selectedCommodity === component.get("v.commodityGas")) {
            component.set('v.consumptionConventions', true);
            component.set('v.consumptionConventionsDisabled', true);
        }else if(selectedCommodity === component.get("v.commodityElectric")) {
            component.set('v.consumptionConventions', false);
            component.set('v.consumptionConventionsDisabled', false);
        }
        helper.updateSearchSupplyCondition(component,selectedCommodity);


    },
    handleSupplyResult: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedSupplyId = event.getParam("selected")[0];

        console.log("selected supplyy==="+event.getParam("selected").length);

        let multi= event.getParam("selected").length > 1;
        console.log("selected supplyy==="+multi);
        let selectedIds= event.getParam("selected");

        var action = component.get("c.checkSelectedSupply");
        let startDate=component.get('v.requestedStartDate');
        let hasDefaultStartDate=false;
        if(startDate && startDate !== null && startDate !== ''){
            hasDefaultStartDate= true;
        }
        if(multi) {
            let channel= component.get("v.channelSelected");
            action = component.get("c.checkSelectedSupplies");
            action.setParams({
                supplyIds: event.getParam("selected"),
                accountId: component.get('v.accountId'),
                opportunityId: component.get('v.opportunityId'),
                hasDefaultStartDate: hasDefaultStartDate,
                channel:channel
            });
        } else {
            action.setParams({
                supplyId: selectedSupplyId,
                accountId: component.get('v.accountId'),
                opportunityId: component.get('v.opportunityId')
            });
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("resp supp---"+JSON.stringify(response.getReturnValue()));
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if(!result.isValid) {
                    ntfSvc.error(ntfLib, result.message);
                    return;
                }
                if(result.error){
                    ntfSvc.error(ntfLib, result.errorMsg);
                    return;
                }
                if(multi){

                    let osiList = component.get("v.opportunityServiceItems") || [];
                    let newOsis = result.opportunityServiceItems;
                    newOsis.forEach(osi => {
                        osiList.push(osi)
                    });
                    component.set("v.step", 4);
                    component.set("v.opportunityServiceItems", osiList);
                    component.set("v.showNewOsi", false);
                    component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                }else {
                    component.set("v.searchedSupplyId", selectedSupplyId);
                    let supply = result.supply;
                    if(supply) {
                        component.set("v.supply", supply);
                    }
                    component.set("v.searchedPointId", supply.ServicePoint__c);
                    component.set("v.searchedPointCode", supply.ServicePoint__r.Code__c);
                    component.set("v.searchedSiteId", supply.ServiceSite__c);
                    component.set("v.showNewOsi", true);

                }

            } else if (component.isValid() && state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                } else {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.UnexpectedError'));
                }
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));

        /*var contractAccountDivComponent = component.find("contractAccountSelectionComponent");
        if (contractAccountDivComponent) {
            contractAccountDivComponent.reload();
        }*/

        // Apply filter on Contract Addition
        helper.reloadContractAddition(component);

    },
    saveBillingSection: function (component, event, helper) {
        //const billingProfileId = component.get("v.billingProfileId");
        const contractAccountId = component.get("v.contractAccountId");
        helper.updateOsi(component);
        helper.saveOpportunity(component, "Value Proposition");
        helper.setPercentage(component, "Value Proposition");
    },
    saveOsiList: function (component, event, helper) {
        helper.saveOpportunity(component, "Qualification");
        helper.setPercentage(component, "Qualification");
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedSupplyId", "");
        //helper.resetSupplyForm(component);
    },
    handleNewOsi: function (component, event, helper) {
        let osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(component, "Proposal/Price Quote", helper.openProductSelection);
        helper.setPercentage(component, "Proposal/Price Quote");
    },
    handleOsiDelete: function (component, event, helper) {
        const osiList = component.get("v.opportunityServiceItems");
        let osiId = event.getParam("recordId");
        const items = [];
        let serviceSiteToEditDescription;
        for (let i = 0; i < osiList.length; i++) {
            if (osiList[i].Id !== osiId) {
                items.push(osiList[i]);
            }
            if(osiList[i].Id === osiId){
                serviceSiteToEditDescription = osiList[i].ServiceSite__c ? osiList[i].ServiceSite__c : null;
                component.set("v.serviceSiteToEditDescription", serviceSiteToEditDescription);
            }
        }

        component.set("v.opportunityServiceItems", items);

        if (items.length === 0) {
            component.set("v.companyDivisionId", "");
            component.set("v.step", 4);
            //helper.resetSupplyForm(component);
        }
        helper.deleteOsiActions(component,osiId);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    getPrivacyId: function (component, event, helper) {
        let isFirstSave = component.get("v.isFirstSave");

        if(isFirstSave === true){
            component.set("v.isFirstSave", false);
            const ntfLib = component.find('notifLib');
            const ntfSvc = component.find('notify');
            component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
            component.set("v.dontProcess", event.getParam('dontProcess'));
            if (!component.get("v.isSaving")) {
                let stage= component.get("v.opportunityDraftStage");
                helper.saveOpportunity(component, stage, helper.redirectToDossier);
                component.set("v.isFirstSave", true);
            }
            if (component.get("v.isSaving")) {
                if (component.get("v.dontProcess")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
                    $A.get('e.force:refreshView').fire();
                    component.set("v.isFirstSave", true);
                    return;
                }
                let stage= component.get("v.opportunitySaveStage");
                helper.saveOpportunity(component, stage, helper.redirectToDossier);
            }
        }
    },
    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        let consumptionConventions = event.getParam("consumptionConventions");
        component.set("v.contractName", event.getParam("newContractName"));
        component.set("v.contractType", event.getParam("newContractType"));
        component.set("v.salesUnitId", event.getParam("newSalesUnitId"));
        component.set("v.companySignedId", event.getParam("newCompanySignedId"));
        component.set("v.salesman", event.getParam("newSalesman"));
        component.set("v.contractId", contractSelected);
        console.log('getContractData contractType: '+component.get("v.contractType"));
        console.log('contractSelected *******************> '+ contractSelected);
        //component.set("v.customerSignedDate", customerSignedDate);
        if (consumptionConventions !== null) {
            component.set("v.consumptionConventions", consumptionConventions);
        }
    },
    handleOriginChannelSelection: function (component, event, helper) {
        console.log('stevent===' + event);

        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        console.log('originSelected' + originSelected);
        console.log('channelSelected' + channelSelected);

        let savedOrigin=component.get("v.originSelected");
        let savedChannel=component.get("v.channelSelected");

        let filled= savedChannel !== null && savedChannel !== '' && savedOrigin != null && savedOrigin !== '';

        if(filled && component.get("v.step") == 0 && !component.get('v.wizardHeaderLoaded')){
            component.set('v.disableOriginChannel', filled);
            component.set("v.step", 1);
        }
        component.set('v.wizardHeaderLoaded', true);
        component.set('v.originSelected', originSelected);
        component.set('v.channelSelected', channelSelected);
        if(!component.get('v.disableWizardHeader')) {
            if (component.get("v.step") <= 1) {
                if (channelSelected != null && originSelected != null) {
                    component.set("v.step", 1);
                    component.set('v.disableOriginChannel', false);
                } else {
                    component.set("v.step", 0);
                }

            } else if (filled) {
                component.set('v.disableOriginChannel', false);
            }

        }
    },
    handleExpirationDate: function (component, event) {
        let expirationDate = event.getParam("expirationDate");
        component.set("v.expirationDate", expirationDate);
        //component.set("v.step", 2);
        console.log('444 expirationDate :', component.get("v.expirationDate"));
    },
    changeConsumptionType: function (component, event, helper) {
        component.set('v.consumptionType', component.find('changeConsumptionType').get('v.value'));
    },
    changeConsumptionConventions: function (component, event, helper) {
        let consumptionConventions = component.get('v.consumptionConventions');
        if(! component.get("v.productRateType")){
            if (consumptionConventions === true) {
                component.set('v.consumptionType', 'Single Rate');
            }
            if (consumptionConventions === false) {
                component.set('v.consumptionType', 'None');
            }
        }

    },

    handleMetersData: function (component, event, helper){
        helper.getMetersOneAtTime(component, null);
    },
    handleMeterEdited: function (component, event, helper){
        component.set("v.metersValidated", false);
        let metersInfoMap=component.get("v.metersInfoMap");
        let meterCode= event.getParam('meterCode');
        delete metersInfoMap[meterCode];
    },
    handleMeterListValidated: function (component, event, helper){
        console.log("validate meters:"+JSON.stringify(event));
        console.log("isMeterValidated:"+JSON.stringify(event.getParam('isMeterValidated')));
        console.log("meterInfo:"+JSON.stringify(event.getParam('meterInfo')));
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let meterList = component.find("meterList");
        let meterLists = [];
        if(meterList instanceof Array) {
            meterLists= meterList;
        } else{
            meterLists.push(meterList);
        }
        let metersInfoMap=component.get("v.metersInfoMap");
        let isMeterValidated=event.getParam('isMeterValidated');
        let eventMeterInfo= event.getParam('meterInfo');
        let meterCode= event.getParam('meterCode');
        let valid=isMeterValidated;
        if(isMeterValidated && isMeterValidated === true){
            if(component.get("v.skipMetersValidation") === false) {
                ntfSvc.success(ntfLib, $A.get('$Label.c.AllIndexValuesAreSuccessfullyValidated').replace('{0}', meterCode));
            }
            let meterInfo =[];
            eventMeterInfo.forEach(meterEle => {
                Object.values(meterEle).forEach(meterRow => {
                    meterInfo.push(meterRow);
                });

            });
            metersInfoMap[meterCode]= meterInfo;

            let osis= component.get("v.opportunityServiceItems");
            let meterSize = Object.keys(metersInfoMap).length;
            if(osis.length == meterSize){
                valid=true;
            }else {
                valid= false;
            }

        }else {
            //ntfSvc.error(ntfLib, $A.get('$Label.c.AllIndexValuesAreNotValidated').replace('{0}', meterCode)); //TO REMOVE
            delete metersInfoMap[meterCode];
            valid=false;

        }

        component.set("v.metersValidated", valid);

    },

    handleMeterAllListValidated: function (component, event, helper){
        console.log("validate meters");
        let meterList = component.find("meterList");
        let meterLists = [];
        if(meterList instanceof Array) {
            meterLists= meterList;
        } else{
            meterLists.push(meterList);
        }
        let metersInfo=[];
        let metersCodes=[];
        let valid=true;

        meterLists.forEach( meterList=> {
            if(valid){
                let validationData= meterList.validateMeterInfo();
                if(validationData.isMeterValidated){
                    let meterInfo =[];
                    validationData.meterInfo.forEach(meterEle => {
                        Object.values(meterEle).forEach(meterRow => {
                            meterInfo.push(meterRow);
                        });

                    });
                    metersInfo.push(meterInfo);
                    metersCodes.push(validationData.meterCode);
                }else {
                    valid=false;
                }
            }

        });
        component.set("v.metersValidated", valid);
        if(valid){
            component.set("v.metersInfo", metersInfo);
            component.set("v.metersCodes", metersCodes);
        }
    },
    handleUpdateOsi:function(component, event, helper) {
        let osiId= event.getParam("osiId");
        helper.loadOsi(component,osiId,function (updatedOsi) {
            console.log("new estimated==="+updatedOsi.EstimatedConsumption__c);
            let consumptionCmp = component.find("consumptionTable");
            if(consumptionCmp){
                consumptionCmp.update(updatedOsi.Id);
            }
        });
    },
    handleConsumptionValidation:function(component, event, helper) {
        let validation=event.getParam("validation");
        let step=component.get("v.step");
        console.log("validation=="+JSON.stringify(validation) + "   step=="+step);
        if(step > 8 && (!validation.saved || !validation.valid) ){
            component.set("v.step",8);
        }

    },
    handleUpdateEstimatedConsumption: function (component, event, helper) {
        console.log("event consump==="+JSON.stringify(event));
        let consumptionDetails=event.getParam("consumptionDetails");
        let osiId= consumptionDetails.OpportunityServiceItem__c;
        console.log("event osiId==="+osiId);
        let osiTiles=component.find('osiTile');
        let osiTile;
        if (osiTiles instanceof Array) {
            osiTiles.forEach(osiTileCmp => {
                if(osiTileCmp.getRecordId() === osiId){
                    osiTile=osiTileCmp;
                }
            });
        }else {
            osiTile=osiTiles;

        }
        //if(osiTile){
            helper.loadOsi(component,osiId,function (osi) {
                console.log("new estimated==="+osi.EstimatedConsumption__c);
            });

        //}

    },
    handleSavedSendingChannel: function (component, event, helper) {
        component.set("v.step", 11);
    },
    removeError: function (component, event, helper) {
        const labelFieldList = [];

        let valEffectiveDate = component.find("RequestedStartDate__c");
        valEffectiveDate.blur();
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('RequestedStartDate__c');
        }

        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },

    handleValidateMeters: function (component, event, helper){
        let isMetersActivatedChecked = component.get('v.metersActivated');
        if(!isMetersActivatedChecked){
            component.set('v.metersValidated', true);
            component.set('v.skipMetersValidation',true);
        } else{
            component.set('v.metersValidated', false);
            component.set('v.skipMetersValidation',false);
        }
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.hasBlockingErrors', true);
    },

    createOLIsForContractAddition: function(component, event, helper) {
        helper.showSpinner(component);
        helper.createOLIsForContractAddition(component);
    }
})