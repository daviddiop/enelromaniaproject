/**
 * Created by goudiaby on 28/05/2019.
 */
({
    initialize: function (component) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        //var companyDivComponent = component.find("companyDivision");
        component.set("v.companyDivisionId", "");
        component.set("v.selectedCommodity",'');
        component.set("v.contractMarket", "");
        component.set("v.contractType", "");
        component.set("v.contractAccountMarket", "");
        component.set("v.metersJson","");
        component.set("v.metersInfoMap",{});
        component.set("v.metersInfo",[]);
        component.set("v.metersCodes",[]);
        component.set("v.isEnelDistributor",false);
        component.set("v.account","");
        component.set('v.disableOriginChannel',true);
        component.set('v.disableWizardHeader',true);
        component.set("v.opportunityId", '');
        component.set("v.contractId", '');
        component.set("v.accountId", accountId);
        component.set('v.originSelected','');
        component.set('v.channelSelected','');
        component.set("v.inEnelArea",'');
        component.set("v.consumptionType",'');
        component.set("v.productRateType",'');
        component.set('v.consumptionConventionsDisabled', false);

        //component.set('v.interactionId', component.find('cookieSvc').getInteractionId());
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId" :genericRequestId
                //"interactionId": component.get('v.interactionId') 
            }).setResolve(function (response) {
            if(response.error){
                ntfSvc.error(ntfLib, response.errorMsg);
                return;
            }

           if (response.opportunityId && !opportunityId) {
                // reload the page with opportunityId param and reRun init method
                self.updateUrl(component, accountId, response.opportunityId,response.dossierId,response.templateId);

                return;
            }

            let step = 0;

            component.set("v.opportunityId", response.opportunityId);
            component.set("v.opportunity", response.opportunity);
            component.set("v.contractMarket", response.contractMarket);

            component.set('v.requestedStartDate',response.defaultStartDate);
            component.set('v.validationRequestedStartDate',response.validationStartDate);
            component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
            if(component.get('v.opportunity') && component.get('v.opportunity').RequestedStartDate__c){
                component.set('v.requestedStartDate',component.get('v.opportunity').RequestedStartDate__c);
            }

            if(response.opportunity.CompanyDivision__c){
                component.set("v.companyDivisionId",response.opportunity.CompanyDivision__c);
                component.set('v.pointAddressProvince',response.opportunity.CompanyDivision__c);
            }
            component.set("v.dossierId", response.dossierId);
            component.set("v.dossier", response.dossier);
            component.set("v.dossierCommodityEditStatus", response.dossierCommodityEditStatus);
            console.log('response.dossierId: ' + response.dossierId);
            console.log('response.dossier: ' + JSON.stringify(response.dossier));
            console.log('response.dossier status: ' + response.dossier.Status__c);
            console.log('response.dossierCommodityEditStatus: ' + response.dossierCommodityEditStatus);
            component.set("v.selectedCommodity", response.dossier.Commodity__c);
            console.log("response.dossier.SelfReadingProvided__c=="+response.dossier.SelfReadingProvided__c);
            if(response.dossier.SelfReadingProvided__c){
                component.set("v.metersActivated", response.dossier.SelfReadingProvided__c);
            }else {
                component.set("v.metersActivated", false);
            }
            let metersActivated = component.get("v.metersActivated");
            if(!metersActivated){
                component.set("v.metersValidated", true);
                component.set('v.skipMetersValidation',true);
            }
            component.set("v.opportunityLineItems", response.opportunityLineItems);
            component.set("v.opportunityServiceItems", response.opportunityServiceItems);
            component.set("v.contractAccountId", response.contractAccountId);
            component.set("v.contractId", response.contractIdFromOpp);
            component.set("v.customerSignedDate", response.customerSignedDate);
            component.set("v.billingProfileId", response.billingProfileId);
            component.set("v.useBit2WinCart", response.useBit2WinCart);
            component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
            component.set('v.expirationDate', response.opportunity.ExpirationDate__c);
            component.set("v.consumptionList", response.consumptionList);
            self.consumptionListRefactor(component);
            component.set("v.commodityGas", response.commodityGas);
            component.set("v.commodityElectric", response.commodityElectric);
            component.set("v.productTypeSingleRate", response.productTypeSingleRate);
            component.set("v.productTypeDualRate", response.productTypeDualRate);
            component.set("v.opportunitySaveStage", response.opportunitySaveStage);
            component.set("v.opportunityDraftStage", response.opportunityDraftStage);
            component.set("v.account", response.account);
            component.set("v.templateId", response.templateId);
            component.set("v.salesSupportUser", response.salesSupportUser);
            component.set("v.salesUnit", response.salesUnit);
            component.set("v.salesUnitId", response.salesUnit);
            component.set("v.userDepartment", response.userDepartment);
            component.set("v.companySignedId", response.companySignedId);
            component.set("v.salesman", response.salesman);
            component.set("v.user", response.user);

            let dualRate= component.get("v.productTypeDualRate");
            let singleRate= component.get("v.productTypeSingleRate");
            let contractId = component.get("v.contractId") ;
            if(!contractId || contractId === ''){
                component.set("v.contractAccountMarket","");
            }else {
                let market= component.get("v.contractMarket");
                component.set("v.contractAccountMarket",market);
            }
            if (response.dossier.Commodity__c) {
                self.updateSearchSupplyCondition(component, response.dossier.Commodity__c);
                step=4;

            }
            if (response.opportunityLineItems.length !== 0) {
                step = 7;

            } else if (response.contractAccountId) {
                step = 7;

            } else if (response.opportunityServiceItems.length !== 0) {
                step = 5;
            }
            if (response.dossier.Commodity__c === component.get("v.commodityGas")) {
                component.set('v.consumptionConventions', true);
                console.log('consumptionConvention Gas: '+ component.get('v.consumptionConventions'));
                component.set('v.consumptionConventionsDisabled', true);
                component.set('v.consumptionType', singleRate);
            }
            if (response.consumptionConventions) {
                component.set('v.consumptionConventions', response.consumptionConventions);
                if (response.consumptionConventions === true) {
                    component.set('v.consumptionConventionsDisabled', true);
                    component.set('v.consumptionType', singleRate);
                }
            }
            self.updateConsumptionConventionsCheckbox(component);
            let osisLength=0;
            if(response.opportunityServiceItems.length !== 0){
                osisLength=response.opportunityServiceItems.length;
            }
            let consumptionType=component.get("v.consumptionType");

            if (response.consumptionList && response.consumptionList.length && response.consumptionList.length > 0) {
                //component.set('v.consumptionType', response.consumptionList[0].Type__c);

                if(response.consumptionList[0].Type__c === singleRate){
                    component.set('v.consumptionType', singleRate);
                    component.set("v.consumptionConventions", true);
                    component.set("v.consumptionConventionsDisabled", true);
                }else {
                    component.set('v.consumptionType', dualRate);
                }

                step=9;
            }

            if(step === 8){
                if( ( !consumptionType || consumptionType === 'None')){
                    step=7;
                }
            }

            let consumptionTypeFromCmp = component.get('v.consumptionType');
            if (!consumptionTypeFromCmp) {
                component.set('v.consumptionType', 'None');
            }

            if(step == 9){
                let metersJson= response.metersJson;
                if(metersJson){
                    if(component.get("v.metersActivated")){
                        component.set("v.metersValidated", false);
                    } else {
                        component.set("v.metersValidated", true);
                    }
                    step= 10;
                }
            }
            if(step > 7) {
                if(component.get("v.opportunityServiceItems").length > 0){
                    let osis=component.get("v.opportunityServiceItems");
                    let allLinked=true;
                    osis.forEach(osi => {
                        if(!osi.Product__c ){
                            allLinked=false;
                        }
                    });
                    if(allLinked) {
                        let osi=osis[0];
                        let rateType=osi.Product__r.CommercialProduct__r.RateType__c;
                        component.set("v.consumptionType", rateType);
                        if(rateType){
                            component.set("v.productRateType", rateType);
                        }
                        console.log(response.dossier.Commodity__c+"linkedProduct==="+rateType);
                        if(step < 8){
                            step = 8;
                        }
                    } else {
                        step =7;
                    }
                }
            }


            if (step >= 5) {
                let path = 'ServicePoint__r.CurrentSupply__r.CompanyDivision__c'.split('.');
                //let companyDivision = path.reduce((obj, key) => ((obj && obj[key] !== 'undefined') ? obj[key] : undefined), response.opportunityServiceItems[0]);
                self.validateAllOsis(component, false,function (validOsis) {

                    if(validOsis ){
                        self.validateOsis(component, function () {
                           /* if(companyDivision && step > 4){
                                component.set("v.companyDivisionId", companyDivision);
                            }*/
                            if (step > 8) {
                                //let isDisCoENEL = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                                let isEnelDistributor = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                                let selectedCommodity = component.get("v.opportunityServiceItems")[0].RecordType.DeveloperName;


                                if(isEnelDistributor && isEnelDistributor === true){
                                    isEnelDistributor=true;
                                }else {
                                    isEnelDistributor=false;
                                }

                                component.set("v.isEnelDistributor", isEnelDistributor);
                                component.set("v.selectedCommodity", selectedCommodity);
                                if (!isEnelDistributor ||  selectedCommodity === 'Gas'){
                                    component.set("v.metersValidated", true);
                                }
                                if(isEnelDistributor  && step == 9 ){
                                    step=10;
                                }
                            }
                            if(step >= 8){
                                component.find("consumptionTable").validateConsumptionsData();

                            }


                        });

                    } else {
                        step = 4;
                        component.set("v.step",step);
                    }


                });


            }
            let disableHeader=step > 3;
            if(component.get('v.wizardHeaderLoaded') && step == 0 && component.get("v.step") ==3){
                step=3;
            }
            console.log( component.get('v.wizardHeaderLoaded')+"step==========="+step);
            component.set('v.disableWizardHeader',disableHeader);
            component.set('v.disableOriginChannel',disableHeader);

            let opportunityVal = response.opportunity;
            if (opportunityVal) {
                if (opportunityVal.StageName === 'Closed Lost' || opportunityVal.StageName === 'Quoted') {
                    component.set("v.isClosed", true);
                }
                if (opportunityVal.Origin__c) {
                    component.set("v.originSelected", opportunityVal.Origin__c);
                }
                if (opportunityVal.Channel__c) {
                   component.set("v.channelSelected", opportunityVal.Channel__c);
                }
                if (opportunityVal.ContractName__c) {
                   component.set("v.contractName", opportunityVal.ContractName__c);
                 }
                if (opportunityVal.ContractType__c) {
                    component.set("v.contractType", opportunityVal.ContractType__c);
                }else {
                    component.set("v.contractType", response.defaultContractType);
                }
            }

            if(component.get("v.userDepartment") && component.get("v.channelSelected") === "Direct Sales (KAM)"){
                component.set("v.salesUnitId", component.get("v.userDepartment"));
                component.set("v.salesUnit", null);
                component.set("v.showUserDepartment", true);
            }
            if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                component.set("v.userDepartment", '');
            }
            if(response.salesUnitAccount && !component.get("v.salesUnit")){
                component.set("v.salesUnit", response.salesUnitAccount);
                if(component.get("v.salesUnit").SalesDepartment__c === component.get("v.channelSelected")) {
                    let salesUnitAccountId = component.get("v.salesUnit").Id;
                    component.set("v.salesUnitId", salesUnitAccountId);
                }
                else{ //user with salesUnitId but SalesDepartment not matching with selected channel
                    component.set("v.salesUnit", null);
                }
            }
            if(component.get("v.channelSelected") !== "Indirect Sales" && !component.get("v.companySignedId")){
                if(response.salesUnitAccount && !component.get("v.salesUnit")){//user with salesUnitId but SalesDepartment not matching with selected channel
                    component.set("v.companySignedId", "");
                }
                else if(component.get("v.channelSelected") === "Direct Sales (KAM)" && !component.get("v.salesUnit") && !component.get("v.userDepartment")){//channel direct sales and user without salesUnitId and not in KamUserGroup
                    component.set("v.companySignedId", "");
                }
                else if(component.get("v.channelSelected") === "Back Office Sales" || component.get("v.channelSelected") === "Back Office Customer Care"){
                    component.set("v.companySignedId", "");
                }
                else{
                    let userId = component.get("v.user").Id;
                    component.set("v.companySignedId", userId);
                }
            }

            component.set("v.step", step);

            if (component.find("billingProfileSelection")) {
                component.find("billingProfileSelection").reset(component.get("v.accountId"));
            }
            if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                component.set('v.streetName',pointStreetId);
                if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                    let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                    component.set("v.vatNumber", vatNumber);
                    let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                    component.set("v.selfReadingEnabled", selfReadingEnabled)
                }
            }
            self.reloadContractAddition(component);
            //self.setPercentage(component, response.stage);
            self.hideSpinner(component);
        })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    updateSearchSupplyCondition: function(component, selectedCommodity){
        let commodityCondition="RecordType.name='"+selectedCommodity+"'";
        let andConditions=component.get("v.andConditions");
        andConditions.splice(0, andConditions.length);
        andConditions.push(commodityCondition);
        let accountId=component.get("v.accountId");
        let accountCondition="Account__c !='"+accountId+"'";
        andConditions.push(accountCondition);
        component.set("v.selectedCommodity",selectedCommodity);
        component.set("v.andConditions",andConditions);

    },
    loadOsi: function (component, osiId, callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems") || [];
        let newOsiList=[];
        //component.set("v.opportunityServiceItems",newOsiList);
        component.find('apexService').builder()
            .setMethod('loadOsi')
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {

                let updatedOsi = response.opportunityServiceItem;
                osiList.forEach(osi => {
                   if(osi.Id === updatedOsi .Id){
                       newOsiList.push(updatedOsi);
                   }else{
                       newOsiList.push(osi);
                   }
                });
                component.set("v.opportunityServiceItems",newOsiList);
                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                if(callback){
                    callback(updatedOsi);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    validateOsi: function (component, osiId) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        //let startDate=component.get('v.opportunity').RequestedStartDate__c;
        console.log("check osiiiiiiiiiiii");
        let startDate=component.get('v.requestedStartDate');
        let hasDefaultStartDate=false;

        if(startDate && startDate !== null && startDate !== ''){
            hasDefaultStartDate= true;
        }
        let channel= component.get("v.channelSelected");
        component.find('apexService').builder()
            .setMethod('checkOsi')
            .setInput({
                "osiId": osiId,
                "hasDefaultStartDate":hasDefaultStartDate,
                "channel": channel
            })
            .setResolve(function (response) {
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;
                osiList.push(newOsi);
                component.set("v.step", 4);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.showNewOsi", false);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));

                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);

                //self.resetSupplyForm(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    validateOsis: function (component, callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('checkOsis')
            .setInput({
                "opportunityId" : component.get("v.opportunityId")
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(callback){
                    callback();
                }
            })
            .setReject(function (errorMsg) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    validateAllOsis: function (component, showErrorMsg ,callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if(showErrorMsg) {
            self.showSpinner(component);
        }
        let validOsis = true;
        let companyDivisionId ;
        if (!component.get("v.opportunityServiceItems") || component.get("v.opportunityServiceItems").length === 0) {
            if(showErrorMsg) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
            }
            validOsis = false;
        }
        if (validOsis) {

            //check if supplies are in same compamy division
            companyDivisionId = component.get("v.opportunityServiceItems")[0].ServicePoint__r.CurrentSupply__r.CompanyDivision__c;
            //let companyDivisionId = component.get("v.opportunityServiceItems")[0].Opportunity__r.CompanyDivision__c;

            let isDisCoENEL = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;

            let mixedCompanyFound = false;
            let mixedEnelFound = false;
            component.get("v.opportunityServiceItems").forEach(osi => {
                if (osi.ServicePoint__r.CurrentSupply__r.CompanyDivision__c !== companyDivisionId) {
                    mixedCompanyFound = true;
                    if (showErrorMsg) {
                        self.hideSpinner(component);
                        ntfSvc.error(ntfLib, $A.get('$Label.c.AllTheSuppliesShouldBeInSameCompany'));
                    }
                    validOsis = false
                }
                if (validOsis) {
                    if (osi.Distributor__r.IsDisCoENEL__c !== isDisCoENEL) {
                        mixedEnelFound = true;
                        if (showErrorMsg) {
                            self.hideSpinner(component);
                            ntfSvc.error(ntfLib, $A.get('$Label.c.AllDistributersShouldHaveTheSameDiscoEnel'));
                        }
                        validOsis = false;
                    }
                }

            });

            if (validOsis && !mixedCompanyFound && !mixedEnelFound) {
                if (!companyDivisionId) {
                    if (showErrorMsg) {
                        self.hideSpinner(component);
                        ntfSvc.error(ntfLib, $A.get('$Label.c.CompanyOfTheSuppliesShouldNotBeEmpty'));
                    }
                    validOsis = false;
                }
            }

        }
        if(validOsis) {
            let requestedStartDate = component.get('v.requestedStartDate');
            component.find('apexService').builder()
            .setMethod('validateAllOsi')
            .setInput({
                "commodity": component.get("v.selectedCommodity"),
                "opportunityId": component.get("v.opportunityId"),
                "requestedStartDate": JSON.stringify(requestedStartDate)
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                let valid = false;
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);

                } else {
                    valid = response.valid;
                    if (valid) {
                        component.set('v.companyDivisionId', companyDivisionId);
                        let osi= component.get("v.opportunityServiceItems")[0];
                        let inEnelArea=osi.ServicePoint__r.CurrentSupply__r.InENELArea__c;
                        component.set("v.inEnelArea",inEnelArea);
                        let contractAccountDivComponent = component.find("contractAccountSelectionComponent");
                        contractAccountDivComponent.reload();
                    }
                }

                if (callback) {
                    callback(valid);
                }
                if (!valid) {
                    if (showErrorMsg) {
                        if(response.errorMsg){
                            ntfSvc.error(ntfLib, response.errorMsg);
                        }
                        let servicePoints = '';
                        let separator = ', ';
                        let count = 0;
                        response.serviceSites.forEach(serviceSite => {
                            /*if(count > 0){
                                serviceSites = serviceSites + separator;
                            }
                            serviceSites = serviceSites + serviceSite;
                            count++;*/

                            ntfSvc.error(ntfLib, $A.get("$Label.c.ServiceSiteSelectedSupplies") + ' ' + serviceSite);
                        });
                        if (response.servicePoints.length > 0) {
                            response.servicePoints.forEach(servicePoint => {
                                if (count > 0) {
                                    servicePoints = servicePoints + separator;
                                }
                                servicePoints = servicePoints + servicePoint;
                                count++;

                            });

                            ntfSvc.error(ntfLib, $A.get("$Label.c.InvalidVoltageValueForServicePoints") + ' ' + servicePoints);
                        }

                    }
                    return;
                }
                //self.resetSupplyForm(component);
            })
            .setReject(function (errorMsg) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    } else if (callback) {
            self.hideSpinner(component);
            callback(validOsis);

        }
    },
    validateOpportunity: function (component,callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let requestedStartDate = component.get('v.requestedStartDate');
        component.find('apexService').builder()
            .setMethod('validateOpportunity')
            .setInput({
                "opportunityId": component.get("v.opportunityId"),
                "requestedStartDate": JSON.stringify(requestedStartDate)
            })
            .setResolve(function (response) {
                let valid = false;
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);

                } else {
                    valid = response.valid;

                }
                if (callback) {
                    callback(valid);
                }
                if (!valid) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();

    },
    saveOpportunity: function (component, stage, callback, cancelReason, detailsReason) {
        let self = this;
        self.showSpinner(component);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId");
        let dossierId = component.get("v.dossierId");
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "dossierId": dossierId,
                "stage": stage,
                "cancelReason": cancelReason,
                "detailsReason": detailsReason
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.enableSaveButton", true);
                    component.set("v.isFirstSave", true);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        var self = this;
        self.showSpinner(component);
        let oppId = component.get("v.opportunityId");
        let osiList = component.get("v.opportunityServiceItems");
        //let billingProfileId = component.get("v.billingProfileId");
        let contractAccountId = component.get("v.contractAccountId");
        console.log("contract=========="+contractAccountId);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityId": oppId,
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
                //"billingProfileId": billingProfileId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, opportunityId,dossierId,templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_TransferWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId,
                "c__dossierId": dossierId,
                "c__templateId":templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    linkOliToOsi: function (component, oli, callback) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems");
        component.find('apexService').builder()
            .setMethod('linkOliToOsi')
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                let rateType=response.rateType;
                console.log("linkedProduct==="+response.rateType);
                if(rateType){
                    component.set("v.step", 8);
                    component.set("v.consumptionType", rateType);
                    component.set("v.productRateType", rateType);
                    //component.find("consumptionTable").validateConsumptionsData();
                }else {
                    let commodity=component.get("v.selectedCommodity");
                        if(commodity === component.get("v.commodityElectric")) {
                            ntfSvc.error(ntfLib, $A.get('$Label.c.RateTypeIsNotDefinedOnTheProduct'));
                            return;
                        } else {
                            let singleRate= component.get("v.productTypeSingleRate");
                            component.set("v.step", 8);
                            component.set('v.consumptionType', singleRate);
                        }

                }
                if(component.get("v.contractId") !== 'new' && component.get("v.contractId") !== undefined ) {
                    component.set("v.consumptionConventions", false);
                    component.set("v.consumptionConventionsDisabled", true);
                }
                component.set("v.step", 8);

                if (callback && typeof callback === "function") {
                    callback(component, self);
                } else {
                    //ntfSvc.success(ntfLib, '');
                }

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        let pageReference;

        if (useBit2WinCart) {
            let osiList = component.get("v.opportunityServiceItems");
            var isGas = false;
            var isElectric = false;
            for (var osi of osiList) {
                if (osi.RecordType.DeveloperName === component.get("v.commodityGas")) {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === component.get("v.commodityElectric")) {
                    isElectric = true;
                }
            }
            var productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';

            }
            console.log('RequestType__c');
            console.log(component.get('v.opportunity').RequestType__c);
            let contractId = component.get("v.contractId");
            let existingContract = false;
            if(contractId && contractId !== 'new'){
                existingContract = true;
            }
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__productType": productType,
                    "c__commodity": component.get ("v.selectedCommodity"),
                    "c__contractType": component.get ("v.contractType"),
                    "c__dossierId": component.get("v.dossierId"),
                    "c__addOsiProduct": existingContract

                }
            };
        }
        else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, pageReference,false);
    },
    redirectToOppty: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirectToDossier: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'Dossier__c',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect_old: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response === true) {
                workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                    workspaceAPI.openSubtab({
                        pageReference: pageReference,
                        focus: true
                    }).then(function (subtabId) {
                        workspaceAPI.closeTab({
                            tabId: enclosingTabId
                        });
                    }).catch(function (errorMsg) {
                        ntfSvc.error(ntfLib, errorMsg);
                    });
                });
            } else {
                const navService = component.find("navService");
                navService.navigate(pageReference);
            }
        }).catch(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        });
    },
    setPercentage: function (component, stageName) {
        let perc = '';
        switch (stageName) {
            case 'Closed Won':
                perc = 100;
                break;
            case 'Closed Lost':
                perc = 100;
                break;
            case 'Negotiation/Review':
                perc = 90;
                break;
            case 'Proposal/Price Quote':
                perc = 75;
                break;
            case 'Value Proposition':
                perc = 50;
                break;
            case 'Qualification':
                perc = 20;
                break;
            case 'Prospecting':
                perc = 10;
                break;
            default:
                perc = 0;
        }
        component.set("v.percentage", perc);
    },
    goToAccount: function (component) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                        workspaceAPI.openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            workspaceAPI.closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                        /*if (window.localStorage && component.get("v.step") === 3 ) {
                            if (!window.localStorage['loaded']) {
                                window.localStorage['loaded'] = true;
                                self.refreshFocusedTab(component);
                            } else {
                                window.localStorage.removeItem('loaded');
                            }
                        }*/
                        if (!window.location.hash && component.get("v.step") === 4) {
                            window.location = window.location + '#loaded';
                            self.refreshFocusedTab(component);
                        }
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        let accountId = component.get("v.accountId");
        if (contractAccountComponent) {
            console.log('preif');
            if (contractAccountComponent instanceof Array) {
                console.log('parte if');
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accountId);
            } else {
                console.log('parte else');
                contractAccountComponent.reset(accountId);
            }
        }
    },
    reloadSendingChannel: function (component) {
        let contractSelectionComponent = component.find("sendingChannelSelection");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadAddressList();
            } else {
                contractSelectionComponent.reloadAddressList();
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent instanceof Array) {
            let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
            privacyChangeComponentToObj[0].savePrivacyChange();
        } else {
            privacyChangeComponent.savePrivacyChange();
        }
        component.set("v.enableSaveButton", true);
    },
    resetSupplyForm: function (component) {
        // let supplySearchComponent = component.find("supplySelection");
        // if (supplySearchComponent instanceof Array) {
        //     let supplyComponentToObj = Object.assign({}, supplySearchComponent);
        //     supplyComponentToObj[0].resetForm();
        // } else {
        //     supplySearchComponent.resetForm();
        // }
    },
    resetBillingProfile: function (component) {
        let billingProfileComponent = component.find("billingProfileSelection");
        if (billingProfileComponent instanceof Array) {
            let billingProfileComponentToObj = Object.assign({}, billingProfileComponent);
            billingProfileComponentToObj[0].reset(component.get("v.accountId"));
        } else {
            billingProfileComponent.reset(component.get("v.accountId"));
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    commodityChangeActions: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        let self = this;
        /*
        component.set("v.contractAccountId", response.contractAccountId);
        component.set("v.contractId", response.contractIdFromOpp);
        component.set("v.customerSignedDate", response.customerSignedDate);
        component.set("v.billingProfileId", response.billingProfileId);*/
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("commodityChangeActions")
            .setInput({
                dossierId: dossierId,
                commodity: component.get("v.selectedCommodity"),
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(response.clear){
                    self.clearOsisRelatedData(component);

                }
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    deleteOsiActions: function (component, osiId) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let dossierId= component.get("v.dossierId");
        let isTransfer = component.get("v.isTransferOrProductChange");
        let serviceSiteToEditDescription = component.get("v.serviceSiteToEditDescription");
        component
            .find("apexService")
            .builder()
            .setMethod("deleteOsi")
            .setInput({
                opportunityId: opportunityId,
                dossierId: dossierId,
                osiId: osiId,
                isTransfer : isTransfer,
                serviceSiteToEditDescription : serviceSiteToEditDescription
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(response.clear){
                    self.clearOsisRelatedData(component);
                } else {
                    component.set("v.reloadMeterReading", true);
                }

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    clearOsisRelatedData: function(component){
        component.set("v.opportunityServiceItems",[]);
        component.set("v.opportunityLineItems",[]);
        component.set("v.companyDivisionId","");
        component.set("v.consumptionType","");
        component.set("v.consumptionList",[]);
        component.set("v.metersJson","");
        component.set("v.isEnelDistributor",false);
        component.set("v.metersInfoMap",{});
        component.set("v.metersInfo",[]);
        component.set("v.metersCodes",[]);
        component.set("v.requestedStartDate","");
    },
    updateTraderToOsi: function (component, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let companyDivisionId = component.get("v.companyDivisionId");
        let requestedStartDate = component.get('v.requestedStartDate');

        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod('updateTraderToOsiList')
            .setInput({
                "opportunityServiceItems": osiList,
                "companyDivisionId": companyDivisionId,
                "opportunityId" : component.get("v.opportunityId"),
                "requestedStartDate": requestedStartDate,
                "dossierId" : component.get("v.dossierId")


            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateDossierStatus: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateDossierStatus")
            .setInput({
                dossierId: dossierId
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(!response.error){
                    component.get("v.dossier").Status__c=response.status;
                }

                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    updateContractAndContractSignedDateOnOpportunity: function (component) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const self=this;
        self.showSpinner(component);
        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let commodity = component.get("v.selectedCommodity");
        let commodityGas = component.get("v.commodityGas");
        let market;
        let salesSupportUser = component.get("v.salesSupportUser");
        if(commodity === commodityGas){
            market= component.get("v.contractMarket");
        } else if(!contractId || contractId === ''){
            market = "";
        }else {
            market= component.get("v.contractMarket");
        }
        component.set("v.contractAccountMarket",market);
        console.log("contract Acc market==="+component.get("v.contractAccountMarket") +  '   contractId==='+contractId);
        let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
        let salesChannel = component.get('v.channelSelected');
        let osiList = component.get("v.opportunityServiceItems");
        component
            .find("apexService")
            .builder()
            .setMethod("updateContractAndContractSignedDateOnOpportunity")
            .setInput({
                opportunityId: opportunityId,
                commodity: commodity,
                contractId: contractId,
                //customerSignedDate: newContractDetails.customerSignedDate,
                contractType: newContractDetails.type,
                contractName: newContractDetails.name,
                companySignedId: newContractDetails.companySignedId,
                salesUnitId: newContractDetails.salesUnitId,
                salesman: newContractDetails.salesman,
                salesChannel: salesChannel,
                opportunityServiceItems: osiList,
                salesSupportUser: salesSupportUser
            })
            .setResolve(function (response) {
                console.log("updateContractAndContractSignedDateOnOpportunity response", JSON.parse(JSON.stringify(response)));
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (response.purgedOLIs) {
                    component.set("v.opportunityLineItems", []);
                }
                console.log('reload start');
                self.reloadContractAccount(component);
                console.log('reload finished');
                if (component.get("v.isVisibleContractAddition")) {

                    component.set("v.step", 6);
                }

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    checkSelectedContract: function(component) {
        let isValid = true;
        let isNotKamUser = (!component.get("v.userDepartment") || component.get("v.userDepartment") === undefined);
        if (!component.get("v.contractId") || component.get("v.contractId") === 'new') {
            let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
            console.log('New Contract details:', JSON.stringify(newContractDetails));
            component.set("v.contractType",newContractDetails.type);
            if (!newContractDetails.type) {
                isValid = false;
             }
            if (component.get("v.channelSelected") === "Indirect Sales" && (!newContractDetails.salesman || !newContractDetails.salesUnitId)) {
                isValid = false;
            }
            else if ((component.get("v.channelSelected") === "Magazin Enel" || component.get("v.channelSelected") === "Magazin Enel Partner"
                || component.get("v.channelSelected") === "Call Center" || component.get("v.channelSelected") === "Info Kiosk")
                && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
                isValid = false;
            }
            //else if (component.get("v.channelSelected") === "Direct Sales (KAM)" && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
            else if (component.get("v.channelSelected") === "Direct Sales (KAM)"){
                if(!newContractDetails.companySignedId){
                    isValid = false;
                }
                else if(!newContractDetails.salesUnitId && isNotKamUser && component.get("v.user").SalesUnitID__c !== null && component.get("v.user").SalesUnitID__c !== undefined){
                    isValid = false;
                }
            }
        }
        return isValid;
    },
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    setExpirationDate : function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunity = component.get('v.opportunity');
        let expirationDate = component.get('v.expirationDate');
        component.find("apexService").builder()
            .setMethod("setExpirationDate")
            .setInput({
                opportunityId: opportunity.Id,
                expirationDate:JSON.stringify(expirationDate)
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createConsumptions: function (component, consumptions, osisIds, osisTotalEstimated, callback) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let self = this;

        let accountId = component.get('v.accountId');
        let contractId = component.get('v.contractId');
        console.log("consumptions==="+JSON.stringify(consumptions));

        let consumptionConventions = component.get('v.consumptionConventions');
        consumptions.forEach(consumption => {
            consumption.Customer__c = accountId;
        });

        component.find("apexService").builder()
            .setMethod("insertConsumptionList")
            .setInput({
                'consumptionList': consumptions,
                'contractId': contractId,
                'consumptionConventions': consumptionConventions,
                'osisTotalEstimated': osisTotalEstimated,
                'osisIds' : osisIds
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.consumptionList', response.consumptionList);
                self.consumptionListRefactor(component);
                if (callback) {
                    callback();
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    getMeters: function (component,step) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        const meterList = component.find('meterList');
        //const selectedSupply = component.get("v.searchedSupplyFields");
        let opportunityId = component.get("v.opportunityId");
        console.log("get meter data"+opportunityId );
        component.find('apexService').builder()
            .setMethod("getMeters")
            .setInput({
                'opportunityId': opportunityId
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                //console.log("metersJson==="+ JSON.stringify(response.metersJson));

                let meters = [];
                let metersMap = response.metersJson;
                for ( let key in metersMap ) {
                   meters.push({value:metersMap[key], key:key});
                    //component.set("v.metersJsonNew",JSON.parse(metersMap[key])); //the error is here

                }
                component.set("v.metersJson", meters);

                let osis= component.get("v.opportunityServiceItems");
                let count=0;
                osis.forEach( osi => {
                        let key=osi.ServicePoint__r.CurrentSupply__c;
                        let meterLists = component.find("meterList");
                        let meterList;
                       if(meterLists){
                            if(meterLists instanceof Array){
                                meterList=meterLists[count];
                            } else {
                                meterList=meterLists;
                            }
                            meterList.setMeterList(JSON.parse(metersMap[key]));
                        }
                        count++;
                    }
                );
                if(step){
                    component.set("v.step",step);
                }

                //component.set("v.step", 2);

            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    validateStartDateField: function (component, event) {
        const labelFieldList = [];
        let valEffectiveDate = component.find("RequestedStartDate__c");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('RequestedStartDate__c');
        }
        if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    },
    updateIndexValues: function (component, callback) {

        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        const accountId = component.get("v.accountId");
        //console.log("updateIndexValues:: meterInfo " + JSON.stringify(meterInfo));
        let meterInfoMap = component.get("v.metersInfoMap");
        let metersInfo = component.get("v.metersInfo");
        let metersCodes = component.get("v.metersCodes");
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        let selfReadingProvided=component.get("v.metersActivated");
        let skipMetersValidation = component.get("v.skipMetersValidation");
       let osis= component.get("v.opportunityServiceItems");
        osis.forEach( osi=> {
            let meterCode=osi.ServicePointCode__c;
            metersInfo.push(meterInfoMap[meterCode]);
            metersCodes.push(meterCode);
        });

        for(let i = 0; i < metersInfo.length; i++){
            if(!metersInfo[i]){
                metersInfo.splice(i,1);
            }
        }
        console.log("component.get(v.metersInfo=="+JSON.stringify(component.get("v.metersInfo")));
        component.find('apexService').builder()
                .setMethod("createIndex")
                .setInput({
                    //'caseList': JSON.stringify(caseList),
                    'opportunityId': opportunityId,
                    'accountId': accountId,
                    'metersCodes': metersCodes,
                    'metersInfo' : metersInfo,
                    'selfReadingProvided': selfReadingProvided,
                    'dossierId' :dossierId,
                    'skipMetersValidation' : skipMetersValidation
                })
                .setResolve(function (response) {
                    if (response.error) {
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    if(callback){
                        callback();
                    }
                })
                .setReject(function (response) {
                    const errors = response.getError();
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                })
                .executeAction();
    },

    getMetersDynamic: function (component, osi, metersInfo, step,opportunityId,dossierId,selfReadingProvided,skipMetersValidation,accountId,rearrangedMeterCodes,i) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        if (metersInfo){
            for(let i = 0; i < metersInfo.length;){
                if(!metersInfo[i]){
                    metersInfo.splice(i,1);
                    i = i > 0 ? i-- : i === 0 ? 0 : i;
                }
                if(metersInfo.length === 1 && i === 0){
                    continue;
                } else {
                    i++;
                }
            }
        } else {
            metersInfo = [];
        }
        console.log("component.get(v.metersInfo=="+JSON.stringify(component.get("v.metersInfo")));
        component.find('apexService').builder()
            .setMethod("getMetersDynamic")
            .setInput({
                //'caseList': JSON.stringify(caseList),
                'opportunityId': opportunityId,
                'accountId': accountId,
                'metersCodes': Object.values(rearrangedMeterCodes),
                'metersInfo' : metersInfo,
                'selfReadingProvided': selfReadingProvided,
                'dossierId' :dossierId,
                'skipMetersValidation' : skipMetersValidation,
                'osi': osi
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                //console.log("metersJson==="+ JSON.stringify(response.metersJson));

                let meters = [];
                let metersMap = response.metersJson;
                for ( let key in metersMap ) {
                    meters.push({value:metersMap[key], key:key});
                    //component.set("v.metersJsonNew",JSON.parse(metersMap[key])); //the error is here

                }
                component.set("v.metersJson", meters);

                /*let osis= component.get("v.opportunityServiceItems");

                osis.forEach( osi => {*/

                        let key=osi.ServicePoint__r.CurrentSupply__c;
                        let meterLists = component.find("meterList");
                        let meterList;
                        if(meterLists){
                            if(meterLists instanceof Array){
                                meterList=meterLists[i];
                            } else {
                                meterList=meterLists;
                            }
                            console.log("metersMap[key] +++++++++++++++++++++++++++++> "+ JSON.stringify(metersMap[key]));
                            meterList.setMeterList(JSON.parse(metersMap[key]));
                        }
                 /*       count++;
                    }
                );*/
                if(step){
                    component.set("v.step",step);
                }
                component.set("v.metersInfo", []);
                component.set("v.metersInfoMap", {});
                component.set("v.metersCodes", []);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },

    updateConsumptionConventionsOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let consumptionConventions = component.get("v.consumptionConventions");
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateConsumptionConventionsOnOpportunity")
            .setInput({
                consumptionConventions: consumptionConventions,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    deleteOlisByContractAccountChange: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractAccountId = component.get("v.contractAccountId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOlisByContractAccountChange")
            .setInput({
                opportunityId: opportunityId,
                contractAccountId: contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.isDeleted){
                    component.set("v.opportunityLineItems",[]);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateConsumptionConventionsCheckbox: function(component){
        let isNewContract = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined || component.get("v.contractId") === '');
        let commodity = component.get("v.selectedCommodity");
        if(isNewContract){
            if(commodity === 'Electric'){
                component.set("v.consumptionConventionsDisabled", false);
            }
        }
        else{
            component.set("v.consumptionConventionsDisabled", true);
        }
    },
    updateRegulatedTariffs: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get('v.opportunityId');
        let commodity = component.get('v.selectedCommodity');
        component.find('apexService').builder()
            .setMethod("updateRegulatedTariffs")
            .setInput({
                "opportunityId": opportunityId,
                "commodity": commodity
            })
            .setResolve(function (response) {
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    getMetersOneAtTime: function (component,step) {
        let osis= component.get("v.opportunityServiceItems");
        let self = this;
        const accountId = component.get("v.accountId");
        let meterInfoMap = component.get("v.metersInfoMap");
        let metersInfo = component.get("v.metersInfo");
        let metersCodes = component.get("v.metersCodes");
        let opportunityId = component.get("v.opportunityId");
        let dossierId = component.get("v.dossierId");
        let selfReadingProvided=component.get("v.metersActivated");
        let skipMetersValidation = component.get("v.skipMetersValidation");
        let counter = 0;
        let rearrangedMeterCodes = {};

        if(metersCodes && metersCodes.length > 0) {
            metersCodes.forEach(function(element){
                rearrangedMeterCodes[element] = element;
            });
            console.log('rearrangedMeterCodes *************************************> ' + JSON.stringify(rearrangedMeterCodes));
        }
        self.showSpinner(component);
        osis.forEach( (osi, i) => {
            counter++;
            let meterCode=osi.ServicePointCode__c;
            metersInfo.push(meterInfoMap[meterCode]);
            metersCodes.push(meterCode);
            self.getMetersDynamic(component, osi, metersInfo, step, opportunityId,dossierId,selfReadingProvided,skipMetersValidation,accountId,rearrangedMeterCodes,i);
            if(counter === osis.length){
                if(step) {
                    component.set("v.step", step);
                }
                self.hideSpinner(component);
            }
        });
    },
    createOLIsForContractAddition: function(component, callback) {
         let ntfSvc = component.find('notify');
         let ntfLib = component.find('notifLib');
         let opportunityId = component.get("v.opportunityId");
         let contractId = component.get("v.contractId");
         let self = this;
         component.find('apexService').builder()
            .setMethod('createOLIsForContractAddition')
            .setInput({
                "opportunityId": opportunityId,
                "contractId": contractId
            })
            .setResolve(function (response) {
                console.log('createOLIsForContractAddition response', JSON.parse(JSON.stringify(response)));
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    console.log(response.errorMsg, response.errorTrace);
                    return;
                } else {
                    component.set("v.opportunityLineItems", response.opportunityLineItems);
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    consumptionListRefactor: function(component){
        let consumptions = component.get("v.consumptionList");
        if (consumptions){
            consumptions.forEach(function(element){
                element.QuantityJanuary__c = element.QuantityJanuary__c.toFixed(3);
                element.QuantityFebruary__c = element.QuantityFebruary__c.toFixed(3);
                element.QuantityMarch__c = element.QuantityMarch__c.toFixed(3);
                element.QuantityApril__c = element.QuantityApril__c.toFixed(3);
                element.QuantityMay__c = element.QuantityMay__c.toFixed(3);
                element.QuantityJune__c = element.QuantityJune__c.toFixed(3);
                element.QuantityJuly__c = element.QuantityJuly__c.toFixed(3);
                element.QuantityAugust__c = element.QuantityAugust__c.toFixed(3);
                element.QuantitySeptember__c = element.QuantitySeptember__c.toFixed(3);
                element.QuantityOctober__c = element.QuantityOctober__c.toFixed(3);
                element.QuantityNovember__c = element.QuantityNovember__c.toFixed(3);
                element.QuantityDecember__c = element.QuantityDecember__c.toFixed(3);
            });
            component.set("v.consumptionList", consumptions);
        }
    }
})