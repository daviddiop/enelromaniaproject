/**
 * Created by vincenzo.scolavino on 11/03/2020.
 */

({
    init: function (component, event, helper) {
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        component.set('v.wizardHeaderLoaded', false);
        let disabledOpportunityServiceFields={
            distributor: true,
            availablePower:true,
            contractualPower:true,
            powerPhase:true,
            pressure:true,
            pressureLevel:true,
            consumptionCategory: true,
            voltageSetting:true,
            voltageLevel:true,
            startDate:true,
            flatRate:true
        };

        component.set("v.disabledOpportunityServiceFields",disabledOpportunityServiceFields);
        component.set("v.accountId", accountId);

        component.set("v.dossierId", dossierId);
        helper.initialize(component,helper);
        helper.initConsoleNavigation(component, $A.get('$Label.c.NLCCLCPODNumberChange'));

    },
    getContractAccountRecordId: function (component, event, helper) {
        component.set(
            "v.contractAccountId",
            event.getParam("contractAccountRecordId")
        );
    },

    cancel: function (component, event, helper) {
        component.set("v.showCancel", true);
    },

    onSaveCancelReason: function (component, event, helper) {
        console.log("cancel========="+JSON.stringify(event));
        component.set("v.savingWizard", false);
        helper.redirectToDossier(component, helper);

    },

    closeCancelModal: function (component, event, helper) {
        component.set("v.showCancel", false);
    },

    save: function (component, event, helper) {
        component.set("v.isSaving", true);
        component.set("v.step",6);
        //helper.showSpinner();
        helper.saveDataChange(component, helper, false);
        //helper.createPrivacyChangeRecord(component);
    },

    saveDraft: function (component, event, helper) {
        component.set("v.isSaving", false);
        helper.saveDataChange(component, helper, true);
        //helper.createPrivacyChangeRecord(component);
    },

    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if(component.get('v.originSelected') && component.get('v.channelSelected')) {
                    component.set('v.disableOriginChannel',true);
                    component.set('v.disableWizardHeader',false);

                    helper.setChannelAndOrigin(component);
                    component.set("v.step", 2);
                    console.log('stepdentro' +  component.get("v.step"));

                }else {
                    ntfSvc.error(ntfLib, "Select origin and channel");
                    return;
                }

                break;

            case 'confirmStep2':
                let cases = component.get("v.cases");
                if(cases.length > 0 && cases) {
                    helper.deleteCases(component);
                    component.set("v.cases", []);
                    component.set("v.step", 3);
                }
                component.set("v.step", 3);
                helper.commodityChangeActions(component);
                console.log(component.get("v.selectedCommodity"));
                break;

            case 'confirmStep3':
                component.set("v.step", 4);
                let dossierId = component.get("v.dossierId");
                let companyDivisionId = component.get("v.companyDivisionId");
                if(dossierId && companyDivisionId)
                    helper.updateDossierCompanyDivisionId(component,helper,dossierId,companyDivisionId);
                break;
            case 'confirmStep4':
                component.set("v.step", 5);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                component.set('v.disableOriginChannel',false);
                component.set('v.disableWizardHeader',true);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                component.set("v.step", 4);
                break;
            default:
                break;
        }
    },

    handleSelectCommodity: function (component, event, helper) {
        component.set("v.step", 2);
        let selectedCommodity = event.getParam("selectedCommodity");
        component.set("v.selectedCommodity",selectedCommodity);
        helper.updateSearchSupplyCondition(component,selectedCommodity);


    },
    handleSupplyResult: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let selectedSupplyId = event.getParam("selected")[0];
        component.set('v.selectedSupplyId',selectedSupplyId);
        let dossierId = component.get('v.dossierId');

        var action = component.get("c.checkSelectedSupply");
        action.setParams({
            supplyId: selectedSupplyId,
            dossierId: dossierId,
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("resp supp---"+JSON.stringify(response.getReturnValue()));
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if(!result.isValid) {
                    ntfSvc.error(ntfLib, result.message);
                    return;
                }
                if(result.error){
                    ntfSvc.error(ntfLib, result.errorMsg);
                    return;
                }
                var supply = result.supply;
                component.set("v.searchedSupplyId", supply.Id);
                component.set("v.selectedCompanyDivisionId", supply.CompanyDivision__c);
                component.set("v.companyDivisionId", supply.CompanyDivision__c);
                component.set("v.showNewCase", true);
            } else if (component.isValid() && state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                } else {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.UnexpectedError'));
                }
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));

        /*var contractAccountDivComponent = component.find("contractAccountSelectionComponent");
        if (contractAccountDivComponent) {
            contractAccountDivComponent.reload();
        }*/

        // Apply filter on Contract Addition
        helper.reloadContractAddition(component);

    },
    saveBillingSection: function (component, event, helper) {
        //const billingProfileId = component.get("v.billingProfileId");
        const contractAccountId = component.get("v.contractAccountId");
        helper.updateOsi(component);
        helper.saveOpportunity(component, "Value Proposition");
        helper.setPercentage(component, "Value Proposition");
    },
    saveOsiList: function (component, event, helper) {
        helper.saveOpportunity(component, "Qualification");
        helper.setPercentage(component, "Qualification");
    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
        component.set("v.searchedSupplyId", "");
        //helper.resetSupplyForm(component);
    },
    handleNewOsi: function (component, event, helper) {
        let osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(component, "Proposal/Price Quote", helper.openProductSelection);
        helper.setPercentage(component, "Proposal/Price Quote");
    },

    /*getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
        component.set("v.dontProcess", event.getParam('dontProcess'));
        let isSaving = component.get("v.isSaving");
        if (component.get("v.dontProcess")) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
            $A.get('e.force:refreshView').fire();
            return;
        }

        if(isSaving && event.getParam('doNotCreatePrivacyChange')){
            helper.saveDataChange(component,helper);
        }
        else{
            helper.setPrivacyChangeDossier(component,helper);
        }

    },*/
    getContractData: function (component, event) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        component.set("v.contractName", event.getParam("newContractName"));
        component.set("v.contractType", event.getParam("newContractType"));
        component.set("v.companySignedId", event.getParam("newCompanySignedId"));
        component.set("v.salesman", event.getParam("newSalesman"));
        component.set("v.contractId", contractSelected);
        component.set("v.customerSignedDate", customerSignedDate);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        console.log('stevent===' + event);

        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        console.log('originSelected' + originSelected);
        console.log('channelSelected' + channelSelected);
        component.set('v.originSelected', originSelected);
        component.set('v.channelSelected', channelSelected);
        let filled=channelSelected != null && originSelected != null;

        if(filled && component.get("v.step") == 0 && !component.get('v.wizardHeaderLoaded')){
            component.set('v.disableOriginChannel', filled);
            component.set("v.step", 3);
        }
        component.set('v.wizardHeaderLoaded', true);
        if(!component.get('v.disableWizardHeader')) {
            if (component.get("v.step") <= 1) {
                if (channelSelected != null && originSelected != null) {
                    component.set("v.step", 1);
                } else {
                    component.set("v.step", 0);
                }

            } else if (filled) {
                component.set('v.disableOriginChannel', true);
            }

        }
    },
    handleExpirationDate: function (component, event) {
        let expirationDate = event.getParam("expirationDate");
        component.set("v.expirationDate", expirationDate);
        //component.set("v.step", 2);
        console.log('444 expirationDate :', component.get("v.expirationDate"));
    },
    changeConsumptionType: function (component, event, helper) {
        component.set('v.consumptionType', component.find('changeConsumptionType').get('v.value'));
    },
    changeConsumptionConventions: function (component, event, helper) {
        let consumptionConventions = component.get('v.consumptionConventions');
        if (consumptionConventions === true) {
            component.set('v.consumptionType', 'Single Rate');
        }
        if (consumptionConventions === false) {
            component.set('v.consumptionType', 'None');
        }
    },
    activateMeter: function(component, event, helper){
        console.log("event=="+JSON.stringify(event.target) +"  event=="+JSON.stringify(event.detail));
        component.set("v.metersActivated",event.detail.checked);


    },

    handleSavedSendingChannel: function (component, event, helper) {
        component.set("v.step", 5);
    },

    handleNewCase : function(component, event, helper){
        helper.retrieveCaseList(component);
    },

    handleCaseDelete: function(component,event,helper){
        helper.deleteSelectedCase(component,event,helper);

    },

})