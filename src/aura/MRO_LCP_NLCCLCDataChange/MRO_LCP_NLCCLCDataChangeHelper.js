/**
 * Created by vincenzo.scolavino on 11/03/2020.
 */

({
    initialize: function (component,helper) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;

        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.set("v.companyDivisionId", "");
        component.set("v.account","");
        component.set('v.disableOriginChannel',true);
        component.set('v.disableWizardHeader',true);
        component.set("v.opportunityId", '');
        component.set("v.contractId", '');
        component.set("v.accountId", accountId);
        component.set('v.originSelected','');
        component.set('v.channelSelected','');
        component.set("v.showCancel", false);

        //component.set('v.interactionId', component.find('cookieSvc').getInteractionId());
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                //"opportunityId": opportunityId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "templateId": templateId,
                "genericRequestId": genericRequestId
                //"interactionId": component.get('v.interactionId')
            }).setResolve(function (response) {
            if(response.error){
                ntfSvc.error(ntfLib, response.errorMsg);
                return;
            }

            let step = 0;
            component.set("v.dossierId", response.dossierId);
            component.set("v.dossier", response.dossier);
            component.set("v.dossierCommodityEditStatus", response.dossierCommodityEditStatus);
            console.log('response.dossierId: ' + response.dossierId);
            console.log('response.dossier: ' + JSON.stringify(response.dossier));
            console.log('response.dossierCommodityEditStatus: ' + response.dossierCommodityEditStatus);

            if(response.companyDivisionId)
                component.set("v.companyDivisionId", response.companyDivisionId);

            if(response.dossier)
                component.set("v.selectedCommodity", response.dossier.Commodity__c);

             if (response.dossierId && !dossierId) {
                 self.updateUrl(component, accountId, response.dossierId,response.templateId);
             }

             if(response.dossier.Origin__c && response.dossier.Channel__c)
                 step=1;

             if(response.dossier.Commodity__c) {
                 component.set("v.selectedCommodity",response.dossier.Commodity__c);
                 helper.updateSearchSupplyCondition(component,response.dossier.Commodity__c);
                 step = 3;
             }

             component.set("v.cases", []);
             if(response.cases && response.cases.length > 0) {
                 component.set("v.cases", response.cases);
                 component.set("v.caseTableView", response.cases.length > component.get("v.tileNumber"));
                 let companyDivisionId = response.cases[0].CompanyDivision__c;
                 component.set("v.companyDivisionId",companyDivisionId);
                 step= response.dossier.CompanyDivision__c ? 4 : 3;
             }

            let disableHeader=step > 3;
            if(component.get('v.wizardHeaderLoaded') && step == 0 && component.get("v.step") ==3){
                step=3;
            }

            console.log( component.get('v.wizardHeaderLoaded')+"step==========="+step);
            component.set('v.disableWizardHeader',disableHeader);
            component.set('v.disableOriginChannel',disableHeader);

            component.set("v.step", step);
            component.set("v.account", response.account);
            component.set("v.templateId", response.templateId);

            if (component.find("billingProfileSelection")) {
                component.find("billingProfileSelection").reset(component.get("v.accountId"));
            }
            self.reloadContractAddition(component);
            //self.setPercentage(component, response.stage);
            self.hideSpinner(component);
        })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },

    updateSearchSupplyCondition: function(component, selectedCommodity){
        let commodityCondition="RecordType.name='"+selectedCommodity+"'";
        let andConditions=component.get("v.andConditions");
        andConditions.splice(0, andConditions.length);
        andConditions.push(commodityCondition);
        let accountId=component.get("v.accountId");
        let accountCondition="Account__c ='"+accountId+"'";
        andConditions.push(accountCondition);
        let distributorCondition = "ServicePoint__r.Distributor__r.IsDisCoENEL__c = false";
        andConditions.push(distributorCondition);
        component.set("v.selectedCommodity",selectedCommodity);
        component.set("v.andConditions",andConditions);
        /* if (selectedCommodity === 'Gas') {
             component.set('v.consumptionConventions', true);
             component.set('v.consumptionConventionsDisabled', true);
             component.set('v.consumptionType', 'Single Rate');
         } else {
             component.set('v.consumptionConventions', false);
             component.set('v.consumptionConventionsDisabled', false);
             component.set('v.consumptionType', 'None');
         } */
    },
    validateOsi: function (component, osiId) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('checkOsi')
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;
                osiList.push(newOsi);
                component.set("v.step", 4);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.showNewOsi", false);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));

                //self.resetSupplyForm(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    validateOsis: function (component, callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('checkOsis')
            .setInput({
                "opportunityId" : component.get("v.opportunityId")
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                /*let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsis = response.opportunityServiceItems;
                newOsis.forEach(osi => {
                    osiList.push(osi)
                });
                component.set("v.step", 4);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.showNewOsi", false);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));*/

                //self.resetSupplyForm(component);
                if(callback){
                    callback();
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    validateAllOsis: function (component, showErrorMsg ,callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod('validateAllOsi')
            .setInput({
                "commodity" : component.get("v.selectedCommodity"),
                "opportunityId" : component.get("v.opportunityId")
            })
            .setResolve(function (response) {
                let valid= false;
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);

                }else{
                    valid=response.valid;
                }

                if (callback) {
                    callback(valid);
                }
                if(!valid){
                    if(showErrorMsg) {
                        let servicePoints='';
                        let separator=', ';
                        let count=0;
                        response.serviceSites.forEach(serviceSite => {
                            /*if(count > 0){
                                serviceSites = serviceSites + separator;
                            }
                            serviceSites = serviceSites + serviceSite;
                            count++;*/

                            ntfSvc.error(ntfLib, $A.get("$Label.c.ServiceSiteSelectedSupplies")+' ' + serviceSite);
                        });
                        if(response.serviceSites.length == 0) {
                            response.servicePoints.forEach(servicePoint => {
                                if (count > 0) {
                                    servicePoints = servicePoints + separator;
                                }
                                servicePoints = servicePoints + servicePoint;
                                count++;

                            });

                            ntfSvc.error(ntfLib, $A.get("$Label.c.InvalidVoltageValueForServicePoints")+' ' + servicePoints);
                        }

                    }
                    return;
                }
                //self.resetSupplyForm(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    saveOpportunity: function (component, stage, callback, cancelReason, detailsReason) {
        let self = this;
        self.showSpinner(component);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId");
        let dossierId = component.get("v.dossierId");
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "dossierId": dossierId,
                "stage": stage,
                "cancelReason": cancelReason,
                "detailsReason": detailsReason
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        var self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        //let billingProfileId = component.get("v.billingProfileId");
        let contractAccountId = component.get("v.contractAccountId");
        console.log("contract=========="+contractAccountId);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
                //"billingProfileId": billingProfileId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_NLCCLCDataChange',
            },
            state: {
                "c__accountId": accountId,
                //"c__opportunityId": opportunityId,
                "c__dossierId": dossierId,
                "c__templateId":templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    linkOliToOsi: function (component, oli, callback) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems");
        component.find('apexService').builder()
            .setMethod('linkOliToOsi')
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                } else {
                    //ntfSvc.success(ntfLib, '');
                }

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            })
            .executeAction();
    },
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        let pageReference;

        if (useBit2WinCart) {
            let osiList = component.get("v.opportunityServiceItems");
            var isGas = false;
            var isElectric = false;
            for (var osi of osiList) {
                if (osi.RecordType.DeveloperName === 'Gas') {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === 'Electric') {
                    isElectric = true;
                }
            }
            var productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';

            }
            console.log('RequestType__c');
            console.log(component.get('v.opportunity').RequestType__c);
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__productType": productType,
                    "c__commodity": component.get ("v.commodity"),
                    "c__contractType": component.get ("v.contractType"),
                    "c__dossierId": component.get("v.dossierId")
                }
            };
        }
        else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, pageReference,false);
    },
    redirectToOppty: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirectToDossier: function (component, helper) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'Dossier__c',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect_old: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response === true) {
                workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                    workspaceAPI.openSubtab({
                        pageReference: pageReference,
                        focus: true
                    }).then(function (subtabId) {
                        workspaceAPI.closeTab({
                            tabId: enclosingTabId
                        });
                    }).catch(function (errorMsg) {
                        ntfSvc.error(ntfLib, errorMsg);
                    });
                });
            } else {
                const navService = component.find("navService");
                navService.navigate(pageReference);
            }
        }).catch(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        });
    },
    setPercentage: function (component, stageName) {
        let perc = '';
        switch (stageName) {
            case 'Closed Won':
                perc = 100;
                break;
            case 'Closed Lost':
                perc = 100;
                break;
            case 'Negotiation/Review':
                perc = 90;
                break;
            case 'Proposal/Price Quote':
                perc = 75;
                break;
            case 'Value Proposition':
                perc = 50;
                break;
            case 'Qualification':
                perc = 20;
                break;
            case 'Prospecting':
                perc = 10;
                break;
            default:
                perc = 0;
        }
        component.set("v.percentage", perc);
    },
    goToAccount: function (component) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    workspaceAPI.getEnclosingTabId().then(function (enclosingTabId) {
                        workspaceAPI.openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            workspaceAPI.closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                        /*if (window.localStorage && component.get("v.step") === 3 ) {
                            if (!window.localStorage['loaded']) {
                                window.localStorage['loaded'] = true;
                                self.refreshFocusedTab(component);
                            } else {
                                window.localStorage.removeItem('loaded');
                            }
                        }*/
                        if (!window.location.hash && component.get("v.step") === 4) {
                            window.location = window.location + '#loaded';
                            self.refreshFocusedTab(component);
                        }
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        let accountId = component.get("v.accountId");
        if (contractAccountComponent) {
            console.log('preif');
            if (contractAccountComponent instanceof Array) {
                console.log('parte if');
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accountId);
            } else {
                console.log('parte else');
                contractAccountComponent.reset(accountId);
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent instanceof Array) {
            let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
            privacyChangeComponentToObj[0].savePrivacyChange();
        } else {
            privacyChangeComponent.savePrivacyChange();
        }
    },

    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },

    /*updateDossierStatus: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateDossierStatus")
            .setInput({
                dossierId: dossierId
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(!response.error){
                    component.get("v.dossier").Status__c=response.status;
                }

                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },*/

    updateDossierCompanyDivisionId: function (component,helper,dossierId,companyDivisionId) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component
            .find("apexService")
            .builder()
            .setMethod("updateDossierCompanyDivision")
            .setInput({
                dossierId: dossierId,
                companyDivisionId: companyDivisionId
            })
            .setResolve(function (response) {
                if(response.error){
                    component.set("v.step", 3);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                //component.set("v.step", 4);
            })
            .setReject(function (error) {
                component.set("v.step", 3);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    /*updateCasesStatus: function(component,helper){
        //this.showSpinner();
        let caseList = component.get('v.cases');
        var action = component.get("c.updateCasesStatus");
        action.setParams({
            inputList: caseList
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("resp supp---"+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.error){
                    ntfSvc.error(ntfLib, result.errorMsg);
                    //this.hideSpinner(component)
                    return;
                }

                component.set("v.cases", result.cases);
                helper.redirectToDossier(component,helper);

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                } else {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.UnexpectedError'));
                }
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },*/

    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        //let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                //opportunityId: opportunity.Id,
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },

    commodityChangeActions: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        let self = this;
        component
            .find("apexService")
            .builder()
            .setMethod("commodityChangeActions")
            .setInput({
                dossierId: dossierId,
                commodity: component.get("v.selectedCommodity"),
            })
            .setResolve(function (response) {
                if(response.error){
                    component.set("v.step", 2);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                component.set("v.step", 2);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    retrieveCaseList: function(component){
        let oldCaseList = component.get('v.cases');
        let dossierId = component.get('v.dossierId');
        const self = this;
        self.showSpinner(component);
        component
            .find("apexService")
            .builder()
            .setMethod("getUpdatedCaseList")
            .setInput({
                dossierId: dossierId
            })
            .setResolve(function (response) {
                if(response.error){
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.cases",oldCaseList);
                    return;
                }
                let newCaseList = response.cases;
                if(newCaseList.length===0)
                    component.set("v.companyDivisionId", "");
                component.set("v.caseTableView", newCaseList.length > component.get("v.tileNumber"));
                component.set("v.showNewCase", false);
                component.set("v.cases",newCaseList);
                if(newCaseList.length === 0){
                    component.set('v.step',3);
                } else {
                    component.set("v.step", 4);
                }
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    deleteSelectedCase: function(component,event,helper){
        let recordId = event.getParam("recordId");
        const self = this;
            component
                .find("apexService")
                .builder()
                .setMethod("deleteSelectedCase")
                .setInput({
                    recordId: recordId
                })
                .setResolve(function (response) {
                    if(response.error){
                        ntfSvc.error(ntfLib, response.errorMsg);
                        return;
                    }
                    helper.retrieveCaseList(component);
                })
                .setReject(function (error) {
                    ntfSvc.error(ntfLib, error);
                })
                .executeAction();
    },

    setPrivacyChangeDossier: function(component,helper){
        let privacyChangeId = component.get("v.privacyChangeId");
        let dossierId = component.get("v.dossierId");
        let self = this;
        component
            .find("apexService")
            .builder()
            .setMethod("updatePrivacyChangeDossier")
            .setInput({
                dossierId : dossierId,
                privacyChangeId: privacyChangeId
            })
            .setResolve(function (response) {
                if(response.error){
                    //self.hideSpinner();
                    component.set("v.step",5);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                let isSaving = component.get("v.isSaving");
                if(isSaving)
                    helper.saveDataChange(component,helper);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    saveDataChange: function(component,helper,isDraft){
        let self = this;
        self.showSpinner(component);
        let caseList = component.get('v.cases');
        let dossierId = component.get("v.dossierId");
        var action = component.get("c.saveNlcClcDataChange");
        action.setParams({
            dossierId : dossierId,
            inputList: caseList,
            isDraft : isDraft
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("resp supp---"+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.error){
                    ntfSvc.error(ntfLib, result.errorMsg);
                    //self.hideSpinner(component);
                    component.set("v.step",5);
                    return;
                }
                self.hideSpinner(component);
                helper.redirectToDossier(component,helper);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                } else {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.UnexpectedError'));
                }
            }
        });
        $A.enqueueAction(action);
    },

    deleteCases: function(component){
        let self = this;
        //self.showSpinner(component);
        let caseList = component.get('v.cases');
        let dossierId = component.get("v.dossierId");
        var action = component.get("c.deleteCases");
        action.setParams({
            dossierId : dossierId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Deletecases ok--->"+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                console.log('DELETECASE SUCCESS');
                if(result.error){
                    ntfSvc.error(ntfLib, result.errorMsg);
                    return;
                }
                //self.hideSpinner(component);
            } else if (state === "ERROR") {
                console.log('DELETECASE ERROR');
                var errors = response.getError();
                if (errors) {
                    ntfSvc.error(ntfLib, JSON.stringify(errors));
                } else {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.UnexpectedError'));
                }
                //self.hideSpinner(component);
            }
        });
        $A.enqueueAction(action);
    }

})