/**
 * Created by goudiaby on 15/05/2020.
 */

({
    generateInteraction: function (component) {
        let self = this;
        let genesysId = component.get('v.genesysId');
        let enelTel = component.get('v.enelTel');
        component.find('apexService').builder()
            .setMethod('generateCTIInteraction')
            .setInput({
                'genesysId': genesysId,
                'enelTel': enelTel
            })
            .setResolve(function (result) {
                self.popUpDispatcher(component, result);
            }).setReject(function (errorMsg) {
            console.log(errorMsg);
        }).executeAction();
    },
    popUpDispatcher: function (component, result) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let enelTel = component.get('v.enelTel');
        let blockCode = component.get('v.blockCode');
        let errorMsg = '';

        if (enelTel) {
            if (!result.servicePointId) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.NoInfoFoundEnelTel"));
                self.navigateToInteractionPage(component, result.interactionId);
                return;
            }
            let ivrBlock = blockCode ? blockCode.substr(blockCode.indexOf("E")+1,1) : '';
            if (ivrBlock === '1') {
                //1 .Popup for Contract Account Page
                let contractAccountId = result.contractAccountId;
                if (!contractAccountId) {
                    errorMsg = $A.get("$Label.c.NoContractAccountFoundEnelTel");
                } else {
                    self.redirectToPopUp(component, contractAccountId);
                }
            } else if (ivrBlock === '2' || ivrBlock === '4') {
                //2 - 3 .Popup for Supply Page
                let supplyId = result.supplyId;
                if (!supplyId) {
                    errorMsg = $A.get("$Label.c.NoSupplyFoundEnelTel");
                } else {
                    self.redirectToPopUp(component, supplyId);
                }
            } else if (ivrBlock === '3') {
                //4 .Popup for Service Point Page
                let servicePointId = result.servicePointId;
                if (!servicePointId) {
                } else {
                    self.redirectToPopUp(component, servicePointId);
                }
            }
            if (result.interactionId) {
                component.find('cookieSvc').clearInteractionId();
                component.find('cookieSvc').setInteractionId(result.interactionId);
            }
            if (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
                self.navigateToInteractionPage(component, result.interactionId);
            }
        }else {
            self.navigateToInteractionPage(component, result.interactionId);
        }
    },
    navigateToInteractionPage: function (component, interactionId) {
        const self = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_InteractionEdit',
            },
            state: {
                "c__id": interactionId
            }
        };
        self.redirect(component, pageReference);
    },
    redirectToPopUp: function (component, recordId) {
        const self = this;
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                actionName: 'view'
            }
        };
        self.redirect(component, pageReference);
    },
    /*
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab, isSubtab, openTab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                    if (response === true) {
                        getEnclosingTabId().then(function (enclosingTabId) {
                            isSubtab({
                                tabId: enclosingTabId
                            }).then(function (isSubtab) {
                                if (isSubtab) {
                                    openSubtab({
                                        pageReference: pageReference,
                                        focus: true
                                    }).then(function () {
                                        closeTab({
                                            tabId: enclosingTabId
                                        });
                                    }).catch(function (errorMsg) {
                                        ntfSvc.error(ntfLib, errorMsg);
                                    });
                                } else {
                                    openTab({
                                        pageReference: pageReference,
                                        focus: true
                                    }).then(function () {
                                        closeTab({
                                            tabId: enclosingTabId
                                        });
                                    }).catch(function (errorMsg) {
                                        ntfSvc.error(ntfLib, errorMsg);
                                    });
                                }
                            });
                        });
                    } else {
                        let navService = component.find("navService");
                        navService.navigate(pageReference).then();
                    }
                }
            ).catch(function (error) {
            console.log(error);
        });
    }
});