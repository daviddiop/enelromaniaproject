/**
 * Created by goudiaby on 15/05/2020.
 */

({
    init: function (component, event, helper) {
        let thisPageReference = component.get("v.pageReference");
        let enelTelParam = thisPageReference.state.c__enelTel;
        let blockCodeParam = thisPageReference.state.c__blockCode;
        component.set("v.enelTel", enelTelParam);
        component.set("v.blockCode", blockCodeParam);
        let genesysId = component.get("v.genesysId");
        if (genesysId) {
            component.set("v.genesysId", genesysId);
        } else {
            let connIdParam = thisPageReference.state.c__connId;
            component.set("v.genesysId", connIdParam);
        }
        helper.generateInteraction(component);
    }
});