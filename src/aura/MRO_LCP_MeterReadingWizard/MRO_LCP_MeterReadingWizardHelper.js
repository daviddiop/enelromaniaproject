/**
 * Created by BADJI on 31/10/2019.
 */
({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        /*if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }*/
        component.find('apexService').builder()
            .setMethod("Initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                component.set("v.phoneNumber", response.phoneCustomer);

                component.set("v.step",1);
                if(response.caseTile){
                    component.set("v.step",2);
                   component.set("v.caseTile", response.caseTile);
                   component.set("v.dateProposedForExecution", response.caseTile.EffectiveDate__c);
                   component.set("v.originSelected", response.caseTile.Origin);
                   component.set("v.channelSelected", response.caseTile.Channel__c);
                   component.set("v.discoNotes", response.caseTile.DiscoNote__c);

                }
                if(response.originSelected){
                    component.set("v.originSelected", response.originSelected);
                }
                if(response.channelSelected){
                    component.set("v.channelSelected", response.channelSelected);
                }

                component.set("v.dossier", response.dossier);
                component.set("v.templateId", response.templateId);
                component.set("v.accountId", response.accountId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.meterReadingEle", response.meterReadingEle);
                component.set("v.meterReadingGas", response.meterReadingGas);
                if (response.dossierId !== dossierId || response.genericRequestId !== genericRequestId) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId);
                }
                if (response.companyDivisionId) {
                    component.set("v.companyDivisionId", response.companyDivisionId);
                }
                const dossierVal = component.get("v.dossier");
                if (response.dossier) {
                    if (response.dossier.Status__c === 'New' || response.dossier.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                        component.set('v.disableOriginChannel', true);
                    }
                }
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
     updateCase: function (component, helper) {
         const self = this;
         self.showSpinner(component, 'spinnerSection');
         let caseList = component.get("v.caseTile");

         let channelSelected = component.get('v.channelSelected');
         let originSelected = component.get('v.originSelected');

         let sendingChannel = component.find('sendingChannel');
          if(sendingChannel){
              sendingChannel.saveSendingChannel();
          }

         const dossierId = component.get("v.dossierId");
         const ntfLib = component.find('notifLib');
         const ntfSvc = component.find('notify');
         component.find('apexService').builder()
             .setMethod("updateCaseList")
             .setInput({
                 oldCaseList: JSON.stringify(caseList),
                 dossierId: dossierId,
             })
             .setResolve(function (response) {
                 self.hideSpinner(component, 'spinnerSection');
                 if (!response.error) {
                     self.redirectToDossier(component, helper);
                 } else {
                     ntfSvc.error(ntfLib, response.errorMsg);
                 }

             })
             .setReject(function (error) {
                 ntfSvc.error(ntfLib, error);
             })
             .executeAction();
     },
    initConsoleNavigation: function (component, wizardLabel) {
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
            });
            document.title = "Lightning Experience | Salesforce";
        }
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const channelSelected = component.get('v.channelSelected');
        const originSelected = component.get('v.originSelected');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                channel: channelSelected,
                origin: originSelected
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    redirectToDossier: function (component, helper) {
        const self = this;
       const pageReference = {
          type: 'standard__recordPage',
          attributes: {
              recordId: component.get("v.dossierId"),
              objectApiName: 'c__Dossier',
              actionName: 'view'
            }
       };
       self.redirect(component, pageReference);
    },
    fillNoteForRequest: function(component, event, helper){
         component.set("v.showNewCase", (component.get("v.searchedSupply").length !== 0));
     },
     handleCreateCase: function(component, event, helper){
         if(helper.handleSupplyResult(component,event,helper)){
             helper.createCase(component,event,helper);
         }
     },
    handleSupplyResult: function (component, event, helper) {
        const searchedSupplyFieldsList = component.get("v.searchedSupply");
        const self = this;
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let validSupplies = [];
        for (let i = 0; i < searchedSupplyFieldsList.length; i++) {

            let validSupply = true;
            let oneSupply = searchedSupplyFieldsList[i];

            if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                continue;
            }

            if(caseList){
                for (let j = 0; j < caseList.length; j++) {
                    if (oneSupply.Id === caseList[j].Supply__c) {
                        errSupplyAlreadySelected.push(oneSupply.Name);
                        validSupply = false;
                        break;
                    }
                }
            }
            if (validSupply) {
                validSupplies.push(oneSupply);
            }
        }
        if (errSupplyAlreadySelected.length !== 0) {
            let messages = errSupplyAlreadySelected.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
        }
        if (errCurrentSupplyIsNotValid.length !== 0) {
            let messages = errCurrentSupplyIsNotValid.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
        }
        if(validSupplies.length === 0){
            return false;
        }
        component.set("v.searchedSupply", validSupplies);
        return true;

    } ,
    createCase: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const supply = component.get("v.searchedSupply");
        const channelSelected = component.get('v.channelSelected');
        const originSelected = component.get('v.originSelected');
        component.find('apexService').builder()
            .setMethod("InsertCase")
            .setInput({
                supply: JSON.stringify(supply),
                accountId: accountId,
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected
            }).setResolve(function (response) {
                self.hideSpinner(component);
            if (!response.error) {

                if (response.caseTile.length !== 0) {
                    component.set("v.companyDivisionId",response.caseTile.CompanyDivision__c);
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.dateProposedForExecution", response.caseTile.EffectiveDate__c);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                }


            } else {
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    removeCase: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        if (items.length === 0) {
            component.set('v.disableOriginChannel', false);
            let wizardHeader = component.find("wizardHeader");
            if(wizardHeader){
                wizardHeader.enableOriginChannelComponent();
            }
            component.set("v.step", 1);
        }
        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
        self.hideSpinner(component);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
        });
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_MeterReadingWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference, true);
    },
    updateDossier: function (component, event, helper) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        let caseList = component.get("v.caseTile");

        let note =component.get("v.discoNotes");
        let phone =component.get("v.phoneNumber");
        let dateProposedForExecution =component.get("v.dateProposedForExecution");

        let sendingChannel = component.find('sendingChannel');
        if(sendingChannel){
            sendingChannel.saveSendingChannel();
        }
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');
        component.find('apexService').builder()
            .setMethod("UpdateDossier")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected,
                note: note,
                phone: phone,
                dateProposedForExecution: JSON.stringify(dateProposedForExecution)
            }).setResolve(function (response) {

                self.hideSpinner(component, 'spinnerSection');
                 if (!response.error) {
                     self.redirectToDossier(component, helper);

                 } else {

                     ntfSvc.error(ntfLib, response.errorMsg);
                 }
                  self.hideSpinner(component);

        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    setCommodityFilter: function (component) {
        let commodityEle = 'Electric';
        let commodityGas = 'Gas';
        let commodityConditionEle ="RecordType.DeveloperName='"+commodityEle+"'";
        let commodityConditionGas ="RecordType.DeveloperName='"+commodityGas+"'";
        let orConditions = [];
        orConditions.push(commodityConditionEle);
        orConditions.push(commodityConditionGas);
        component.set("v.orConditions",orConditions);
    }
})