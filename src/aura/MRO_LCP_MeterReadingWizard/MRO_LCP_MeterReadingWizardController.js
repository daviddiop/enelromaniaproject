/**
/**
 * Created by BADJI on 31/10/2019.
 */
({
     init: function (component, event, helper) {

        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        component.set("v.accountId", accountId);
        helper.setCommodityFilter(component);
        helper.initialize(component, event, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.MeterReading"));
     },
      handleCreateCase: function (component, event, helper) {
              component.set("v.showNewCase",false);
              helper.initialize(component, event, helper);
              component.find('sendingChannel').disableInputField(true);
      },
      closeCaseModal: function (component, event, helper) {
          component.set("v.showNewCase", false);
      },
     searchSelectionHandler: function (component, event, helper) {
         const ntfLib = component.find('notifLib');
         const ntfSvc = component.find('notify');
         const supplyIds = event.getParam('selected');
         if (!supplyIds || !supplyIds.length) {
             return;
         };
         if(component.get("v.caseTile").length > 0){
              ntfSvc.error(ntfLib, $A.get("$Label.c.caseAlreadySelected"));
              return;
          }
         component.find('apexService').builder()
             .setMethod("getSupplyRecord")
             .setInput({
                 supplyId: supplyIds[0]
             })
             .setResolve(function (response)
             {
             if (!response.error) {
                component.set("v.searchedSupply", response.supply);
                if(!response.supply.ContractAccount__c){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.MissingContractAccount"));
                    return;
                }
                component.set("v.contractAccountId", response.ContractAccount__c);
                component.set("v.supplyId", response.supply.Id);
                component.set("v.companyDivisionId", response.supply.CompanyDivision__c);
                if (response.supply.RecordType["DeveloperName"] === "Electric") {
                    component.set("v.selectedRecordType", component.get("v.meterReadingEle"));
                } else if (response.supply.RecordType["DeveloperName"] === "Gas") {
                    component.set("v.selectedRecordType", component.get("v.meterReadingGas"));
                }
                //helper.fillNoteForRequest(component, event, helper);
                helper.handleCreateCase(component, event, helper);
             }
             })
             .setReject(function (error) {
             })
             .executeAction();
     },
     toggleSection : function(component, event, helper) {
         var sectionAuraId = event.target.getAttribute("data-auraId");
         var sectionDiv = component.find(sectionAuraId).getElement();
         var sectionState = sectionDiv.getAttribute('class').search('slds-is-open');
         if(sectionState == -1){
             sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
         }else{
             sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
         }
     },
      handleSuccessDossier: function (component,event, helper){

          event.preventDefault();
          const ntfLib = component.find('notifLib');
          const ntfSvc = component.find('notify');
          const selectedChannel = event.getParam("sendingChannel");
          if(selectedChannel ){
              helper.showSpinner(component);
              helper.updateDossier(component, event, helper);
          }
     },
     nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
         switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            case 'confirmStep2':
                let wizardHeader = component.find("wizardHeader");
                if(wizardHeader){
                    wizardHeader.disableOriginChannelComponent();
                }
                component.set("v.step", 3);

                break;
            case 'confirmStep3':
                    let notes =component.get("v.discoNotes");
                    if(!notes){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.errorEmptyNotes"));
                        return;
                    }
                component.set("v.step", 5);
                component.find('sendingChannel').disableInputField(false);
                 break;
            default:
                break;
         }
     },

     previousStep: function (component, event, helper) {
         event.preventDefault();
         const buttonPressed = event.getSource().getLocalId();
         if(buttonPressed === "returnStep1"){
            component.set("v.step", 1);
         }else if (buttonPressed === "returnStep2") {
              component.set("v.step", 2);
              component.set("v.disableOriginChannel", false);
         }else if (buttonPressed === "returnStep3") {
             component.set("v.step", 3);
             component.find('sendingChannel').disableInputField(true);
          }
     },
     handleOriginChannelSelection: function (component, event, helper) {
         let originSelected = event.getParam('selectedOrigin');
         let channelSelected = event.getParam('selectedChannel');
         if (originSelected && channelSelected){
             component.set('v.originSelected', originSelected);
             component.set('v.channelSelected', channelSelected);
             component.set('v.disableAgree', false);
             component.set("v.step",1);
         }else {
             component.set('v.disableAgree', true);
             component.set("v.step",0);
         }
     },
     handleCaseDelete: function (component, event, helper) {
         helper.removeCase(component, event, helper);
     },
     handleSupplyResult: function (component, event, helper) {
         let selectedSupply = event.getParam("selectedSupply");
         component.set("v.searchedSupply", selectedSupply);
         component.set("v.supplyId", selectedSupply.Id);
         const searchedSearchedSupplyList = component.get("v.searchedSupply");
         const ntfLib = component.find('notifLib');
         const ntfSvc = component.find('notify');
         component.set('v.contractAccountId', selectedSupply.ContractAccount__c);
         component.set('v.companyDivisionId', selectedSupply.CompanyDivision__c);
         if (selectedSupply.ServicePoint__r && (selectedSupply.Id !== selectedSupply.ServicePoint__r.CurrentSupply__c)) {
             ntfSvc.error(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + selectedSupply.Name);
             return;
         }

         if(!selectedSupply[0].ContractAccount__c ){
            ntfSvc.error(ntfLib, $A.get("$Label.c.MissingContractAccount"));
            return
         }
         component.set('v.step', 2);
     },
      handleSupplySearch: function (component, event, helper) {
         component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
         component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
         if (!component.get("v.findButtonEvt")){
             component.set("v.step", 0);
         }
     },
     save: function (component, event, helper) {
        component.find('sendingChannel').saveSendingChannel();

     },
     onRender: function (component) {
         const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
         isConsoleNavigation()
             .then(function (response) {
                 if (response === true) {
                     getFocusedTabInfo()
                         .then(function (response) {
                             let focusedTabId = response.tabId;
                             let isSubTab = response.isSubtab;
                             if (isSubTab) {
                                 setTabLabel({
                                     tabId: focusedTabId,
                                     label: $A.get("$Label.c.MeterReading")
                                 });
                                 setTabIcon({
                                     tabId: focusedTabId,
                                     icon: "utility:case",
                                     iconAlt: $A.get("$Label.c.MeterReading")
                                 });
                             }
                         });
                 }
             });
     },
     cancel: function (component, event, helper) {
         helper.handleCancel(component, helper);
     }

})