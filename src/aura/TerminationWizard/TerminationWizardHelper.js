({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod("initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.billingProfileId", response.billingProfileId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    if (response.dossierId && !dossierId) {
                        self.updateUrl(component, accountId, response.dossierId);
                    }
                    if (response.companyDivisionId) {
                        component.set("v.companyDivisionId", response.companyDivisionId);
                    }
                    var step = 0;
                    if (response.billingProfileId) {
                        step = 3;
                    } else if (response.caseTile.length !== 0) {
                        step = 2;
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    }
                    var dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }
                    component.set("v.step", step);
                    component.set("v.account", response.account);
                    component.set("v.accountPersonRT", response.accountPersonRT);
                    component.set("v.accountPersonProspectRT", response.accountPersonProspectRT);
                    component.set("v.accountBusinessRT", response.accountBusinessRT);
                    component.set("v.accountBusinessProspectRT", response.accountBusinessProspectRT);
                    component.set("v.disconnectionCausePicklistValues", response.disconnectionCausePicklistValues);
                    component.set("v.disconnectionCauseLabel", response.disconnectionCauseLabel);
                    component.set("v.disconnectionDateLabel", response.disconnectionDateLabel);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    self.resetSupplyForm(component);
                    if (component.find("billingProfileSelection")) {
                        component.find("billingProfileSelection").reset(component.get("v.accountId"));
                    }
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
        .executeAction()
    },
    createCase: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSectionModal');
        const disconnectionCause = component.get("v.disconnectionCause");
        const disconnectionDate = component.get("v.disconnectionDate");
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((disconnectionCause.length === 0) || (disconnectionDate == null)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        component.find('apexService').builder()
            .setMethod("createCase")
            .setInput({
                disconnectionCause: disconnectionCause,
                disconnectionDate: JSON.stringify(disconnectionDate),
                searchedSupplyFieldsList: JSON.stringify(searchedSupplyFieldsList),
                caseList: JSON.stringify(caseList),
                accountId: accountId,
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSectionModal');
                if (!response.error) {
                    if (response.caseTile.length !== 0) {
                        component.set("v.companyDivisionId",response.caseTile[0].CompanyDivision__c);
                        component.set("v.step", 1);
                    }

                    component.set("v.showNewCase", false);
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    helper.resetSupplyForm(component);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveDraftBillingProfile: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("saveDraftBP")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                billingProfileId: billingProfileId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateCase: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");

        const dossierId = component.get("v.dossierId");
        const billingProfileId = component.get("v.billingProfileId");
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                billingProfileId: billingProfileId,
                searchedSupplyFieldsList: JSON.stringify(searchedSupplyFieldsList)
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, dossierId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__TerminationWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        if (items.length === 0) {
            component.set("v.step", 0);
        }

        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    validateFields: function (component, event) {
        const labelFieldList = new Array();
        let valEffectiveDate = component.find("Field_EffectiveDate__c");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }
        let valReason = component.find("Field_Reason__c");
        if (!Array.isArray(valReason)) {
            valReason = [valReason];
        }
        tmp = valReason[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('Field_Reason__c');
        }
        if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService")
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component,wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = "Lightning Experience | Salesforce";
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    }
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    }
})