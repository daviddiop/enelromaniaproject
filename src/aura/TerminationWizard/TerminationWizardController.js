({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initConsoleNavigation(component, $A.get('$Label.c.TerminationWizard'));
        helper.initialize(component, event, helper);
    },
    getBillingProfileRecordId: function (component, event, helper) {
        component.set("v.billingProfileId", event.getParam("billingProfileRecordId"));
        if (component.get("v.billingProfileId")) {
            component.set("v.step", 3)
        }
    },
    handleSupplyResult: function (component, event, helper) {
        component.set('v.disconnectionDate', '');
        component.set('v.disconnectionCause', '');
        const searchedSupplyFieldsList = event.getParam("supplyFieldsValue");
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let errSupplyAlreadySelected = [];
        let errCurrentSupplyIsNotValid = [];
        let errSupplyNonDisconnectable = [];
        let validSupplies = [];

        for (let i = 0; i < searchedSupplyFieldsList.length; i++) {
            let validSupply = true;
            let oneSupply = searchedSupplyFieldsList[i];
            if (oneSupply.ServicePoint__r && (oneSupply.Id !== oneSupply.ServicePoint__r.CurrentSupply__c)) {
                errCurrentSupplyIsNotValid.push(oneSupply.Name);
                continue;
            }
            if (oneSupply.NonDisconnectable__c === true) {
                errSupplyNonDisconnectable.push(oneSupply.Name);
                continue;
            }
            for (let j = 0; j < caseList.length; j++) {
                if (oneSupply.Id === caseList[j].Supply__c) {
                    errSupplyAlreadySelected.push(oneSupply.Name);
                    validSupply = false;
                    break;
                }
            }
            if (validSupply) {
                validSupplies.push(oneSupply);
            }
        }
        if (errSupplyAlreadySelected.length !== 0) {
            let messages = errSupplyAlreadySelected.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.SupplyAlreadySelected") + ' - ' + messages);
        }
        if (errCurrentSupplyIsNotValid.length !== 0) {
            let messages = errCurrentSupplyIsNotValid.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + messages);
        }
        if (errSupplyNonDisconnectable.length !== 0) {
            let messages = errSupplyNonDisconnectable.join(' ');
            ntfSvc.warn(ntfLib, $A.get("$Label.c.NondisconnectableSupply") + ' - ' + messages);
        }

        component.set("v.searchedSupplyFields", validSupplies);
        component.set("v.showNewCase", (validSupplies.length !== 0));
    }/*,
    handleSupplyResult: function (component, event, helper) {
        component.set('v.disconnectionDate', '');
        component.set('v.disconnectionCause', '');
        component.set("v.searchedSupplyFields", event.getParam("supplyFieldsValue"));
        const searchedSupplyFieldsList = component.get("v.searchedSupplyFields");
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        for (let i = 0; i < searchedSupplyFieldsList.length; i++) {
            if (searchedSupplyFieldsList[i].NonDisconnectable__c === true) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.NondisconnectableSupply"));
                return;
            }

            for (let j = 0; j < caseList.length; j++) {
                if (searchedSupplyFieldsList[i].Id === caseList[j].Supply__c) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SupplyAlreadySelected"));
                    component.set("v.showNewCase", false);
                    return;
                }
            }
        }

        if (servicePointCurrentSupply !== selectedSupplyId || !servicePointCurrentSupply) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid"));
            return;
        }
        component.set("v.showNewCase", true);
    }*/,
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleCreateCase: function (component, event, helper) {
        if (helper.validateFields(component, event)) {
            helper.createCase(component, event, helper);
        }
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((caseList.length === 0) || (!billingProfileId)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.updateCase(component, helper);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                if (!component.get("v.caseTile") || component.get("v.caseTile").length === 0) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
                    return;
                }

                if (component.get("v.billingProfileId")) {
                    component.set("v.step", 3)
                } else {
                    component.set("v.step", 2);
                }
                helper.resetSupplyForm(component);
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            default:
                break;
        }
    },
    removeError: function (component, event, helper) {
        const labelFieldList = [];
        let valEffectiveDate = component.find("Field_EffectiveDate__c");
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('Field_EffectiveDate__c');
        }
        let valReason = component.find("Field_Reason__c");
        if (!Array.isArray(valReason)) {
            valReason = [valReason];
        }
        tmp = valReason[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('Field_Reason__c');
        }
        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },
    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftBillingProfile(component, helper);
    },
    handleCloseCompanyDivision: function (component, event, helper) {
        const companyDivisionIsCompleted = event.getParam('isCompleted');
        if (!companyDivisionIsCompleted) {
            helper.goToAccount(component, event, helper);
            helper.closeFocusedTab(component, event, helper);
        }
    }
})