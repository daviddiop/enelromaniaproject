({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let templateId = myPageRef.state.c__templateId;

        component.set("v.accountId", accountId);
        component.set("v.dossierId", dossierId);
        component.set("v.templateId", templateId);

        helper.initConsoleNavigation(component, $A.get("$Label.c.ContractMerge"));
        helper.initialize(component, helper);
    },
    handleSubProcess: function (component, event) {
        let subProcess= event.getParam("subProcess");
        // component.set("v.subProcess", subProcess);
    },
    handleTypeOfWorkSelection: function (component, event, helper) {
        let currentFlow = component.find('typeOfWork').get('v.value');
        helper.setCurrentFlow(component, helper, currentFlow);
    },
    nextStep: function (component, event,helper) {
        helper.goToNextStep(component, helper);
    },
    editStep: function (component, event) {
        // this is the same as the step for this setting
        let stepToEditTitle = event.getSource().getLocalId();
        //let previousStep = helper.getPreviousStep(component, helper, stepToEditTitle);

        component.set("v.step", stepToEditTitle);
        switch (stepToEditTitle) {
            case component.get("v.steps").REQUEST_CLASSIFICATION.Title:
                break;
            case component.get("v.steps").FINISH.Title:
                break;
        }
    },
    getContractAccountId: function (component, event) {
        let contractAccountSelected = event.getParam("contractAccountRecordId");
        component.set("v.contractAccountId", contractAccountSelected);
    },
    getContractData: function (component, event, helper) {
        if (component.get("v.step") != component.get("v.steps").CONTRACT.Title) {
            return;
        }
        let contractSelected = event.getParam("selectedContract");
        if (contractSelected) {
            component.set('v.contractId', contractSelected);
            component.set("v.contractName", event.getParam("newContractName"));
            component.set("v.contractType", event.getParam("newContractType"));
            component.set("v.companySignedId", event.getParam("newCompanySignedId"));
            component.set("v.salesman", event.getParam("newSalesman"));
            component.set("v.selectedMarket", event.getParam("newMarket"));
        }

        /*
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        component.set("v.customerSignedDate", customerSignedDate);
        */
    },
    handleSelectCommodity: function (component, event, helper) {
        let selectedCommodity = event.getParam("selectedCommodity");
        component.set('v.commodity', selectedCommodity);
        helper.updateAdvancedSearchCondition(component);
    },
    handleSupplyResult: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");

        let supplyIds = event.getParam("selected");
        if (!supplyIds) {
            return;
        }
        component.set("v.supplyIds", supplyIds);
        helper.createCase(component, event, helper, supplyIds);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    updateLocation: function(component, event, helper) {
        let index = event.currentTarget.dataset.index;
        let text = index > -1? component.find("text")[index].getElement(): null;
        if(text) {
            text.style.left = (event.clientX+20)+'px';
            text.style.top = (event.clientY-20)+'px';
        }
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let newOriginSelected = event.getParam('selectedOrigin');
        let newChannelSelected = event.getParam('selectedChannel');

        component.set('v.originSelected', newOriginSelected);
        component.set('v.channelSelected', newChannelSelected);

        if (newChannelSelected === null || newOriginSelected === null){
            return;
        }

        let channelStep = component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title;
        let currentStep = component.get("v.step");
        if (channelStep === currentStep) {
            component.set("v.step", helper.getFirstStep(component, helper).Title);
            helper.goToNextStep(component, helper);
        }
    },
    handleSavedSendingChannel: function (component, event, helper) {

    },
    save: function (component, event, helper) {
        component.set("v.savingWizard", true);
        if (component.get("v.currentFlow") === component.get("v.flows").CONTRACT_MERGE) {
            helper.createPrivacyChangeRecord(component);
        } else {
            if (!component.get("v.savingWizard")) {
                helper.updateCases(component, helper);
            }
            if (component.get("v.savingWizard")) {
                if (component.get("v.dontProcess")) {
                    ntfSvc.error(ntfLib, "Do Not Process");
                    $A.get("e.force:refreshView").fire();
                    return;
                }
                helper.updateCases(component, helper);
            }
        }

    },
    saveDraft: function (component, event, helper) {
        component.set("v.savingWizard", false);
        helper.createPrivacyChangeRecord(component);
    },
    cancel: function (component, event, helper) {
        component.set("v.showCancelReasonModal", true);
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");

        let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
        if (!doNotCreatePrivacyChange) {
            component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
            component.set("v.dontProcess", event.getParam("dontProcess"));
        }

        if (!component.get("v.savingWizard")) {
            helper.updateCases(component, helper);
        }
        if (component.get("v.savingWizard")) {
            if (component.get("v.dontProcess")) {
                ntfSvc.error(ntfLib, "Do Not Process");
                $A.get("e.force:refreshView").fire();
                return;
            }
            helper.updateCases(component, helper);
        }
    },
    onSaveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        if (cancelReason) {
            component.set("v.savingWizard", false);
            helper.handleCancel(component, helper, cancelReason, detailsReason);
        }
    },
    handleMeterListEdited: function(component, event, helper){
        component.set('v.metersValidated', helper.validateAllMeters(component).valid);
    },

    closeCancelReasonModal: function (component) {
        component.set("v.showCancelReasonModal", false);
    },
    saveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    }
});