({
    initialize: function (component, helper) {
        let self = this;

        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;

        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod('Initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "genericRequestId": genericRequestId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            })
            .setResolve(function (response) {
                console.log(JSON.stringify(response));
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.caseTile", response.cases);
                component.set("v.divisionId", response.companyDivisionId);
                component.set("v.commodity", response.commodity);
                component.set("v.contractId", response.contractId);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.effectiveDate", response.effectiveDate);
                // component.set("v.tariffType", response.tariffType);
                component.set("v.isInEnelArea", response.inEnelArea);
                component.set("v.selectedMarket", response.market);
                component.set("v.selectedContractType", response.contractType);
                component.set("v.supplyIds", response.supplyIds);
                component.set("v.supplyContractAccountIds", response.supplyContractAccountIds);
                component.set("v.supplyContractIds", response.supplyContractIds);
                component.set("v.contractId", response.supplyContractIds ? response.supplyContractIds[0] : null);
                component.set("v.isGdprReadOnly", response.account.IsPersonAccount === false);

                if (response.supplyIds && response.supplyIds.length) {
                    component.set("v.supplyId", response.supplyIds[0]);
                }

                helper.setCurrentFlow(component, helper, response.subProcess);

                component.set("v.isClosed", false);

                if(response.contractAccountId && response.commodity !== 'Gas'){
                    component.set("v.showMeters", true);
                    self.getMeters(component);
                }

                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId, templateId);
                }

                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                    }
                    if (dossierVal.Origin__c) {
                        component.set("v.originSelected", dossierVal.Origin__c);
                    }
                    if (dossierVal.Channel__c) {
                        component.set("v.channelSelected", dossierVal.Channel__c);
                    }
                    component.set('v.metersValidated', dossierVal.SelfReadingProvided__c);
                }

                component.set("v.accountId", accountId);
                if (response.cases) {
                    component.set("v.osiTableView", response.cases.length > component.get("v.tileNumber"));
                }

                self.updateAdvancedSearchCondition(component);

                helper.fillStepIndexes(component, helper);
                let topElement = component.find("topElement").getElement();
                if (topElement) {
                    topElement.scrollIntoView({behavior: "instant", block: "end"});
                }
                self.resetSupplyForm(component);
                helper.restoreCurrentStep(component, helper);
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateCases: function (component, helper) {
        const self = this;

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        const dossierId = component.get("v.dossierId");
        const casesCreated = component.get("v.caseTile");
        const contractId = component.get("v.contractId");
        const contractAccountId = component.get("v.contractAccountId");
        const savingWizard = component.get("v.savingWizard");
        const currentFlow = component.get("v.currentFlow");

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("UpdateCases")
            .setInput({
                caseList: JSON.stringify(casesCreated),
                dossierId: dossierId,
                contractId: contractId,
                contractAccountId: contractAccountId,
                isDraft: !savingWizard,
                subProcess: currentFlow
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    setEffectiveDate: function (component, helper) {
        const self = this;

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        const casesCreated = component.get("v.caseTile");
        const effectiveDate = component.find('effectiveDate').get("v.value");

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("setEffectiveDate")
            .setInput({
                caseList: JSON.stringify(casesCreated),
                effectiveDate: JSON.stringify(effectiveDate),
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
                self.hideSpinner(component);
            })
            .executeAction();
    },
    setContractAndContractAccount: function (component, helper) {
        const self = this;

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("setContractAndContractAccount")
            .setInput({
                caseList: JSON.stringify(component.get("v.caseTile")),
                contractId: component.get("v.contractId"),
                contractAccountId: component.get("v.contractAccountId")
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
                self.hideSpinner(component);
            })
            .executeAction();
    },
    createCase: function (component, event, helper, supplyIds) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");
        const dossierId = component.get("v.dossierId");

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("CreateCase")
            .setInput({
                supplyIds: JSON.stringify(supplyIds),
                caseList: JSON.stringify(caseList),
                accountId: accountId,
                dossierId: dossierId,
                commodity: component.get("v.commodity"),
                channel: component.get("v.channelSelected"),
                origin: component.get("v.originSelected")
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (response.cases.length !== 0) {
                    component.set("v.osiTableView", response.cases.length > component.get("v.tileNumber"));
                }
                self.resetSupplyForm(component);
                component.set("v.caseTile", response.cases);
                // component.set("v.tariffType", response.tariffType);
                component.set("v.effectiveDate", response.effectiveDate);
                component.set("v.isInEnelArea", response.inEnelArea);
                component.set("v.selectedMarket", response.market);
                component.set("v.selectedContractType", response.contractType);
                component.set("v.supplyContractAccountIds", response.supplyContractAccountIds);
                component.set("v.supplyContractIds", response.supplyContractIds);
                component.set("v.contractId", response.supplyContractIds ? response.supplyContractIds[0] : null);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper, cancelReason, detailsReason) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("CancelProcess")
            .setInput({
                'caseList': JSON.stringify(caseList),
                'dossierId': dossierId,
                'cancelReason': cancelReason,
                'detailsReason': detailsReason
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }
        component.set("v.caseTile", items);
        if (items && items.length === 0){
            component.set("v.divisionId", "");
            helper.reloadAll(component,items);
        }
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    updateUrl: function (component, accountId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ContractMergeWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    checkCasesToSupplies: function (component, helper, callback){
        let cases = component.get("v.caseTile");
        let dossierId = component.get("v.dossierId");
        let commodity = component.get("v.commodity");

        helper.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("CheckCasesEqualToSupplies")
            .setInput({
                cases: JSON.stringify(cases),
                dossierId: dossierId,
                commodity: commodity
            })
            .setResolve(function (response) {
                helper.hideSpinner(component);

                if (callback){
                    callback(response);
                }
            })
            .setReject(function (error) {
                helper.hideSpinner(component);

                if (callback){
                    callback(error);
                }
            })
            .executeAction();
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    /*if (window.localStorage && component.get("v.step") === 3 ) {
                        if (!window.localStorage['loaded']) {
                            window.localStorage['loaded'] = true;
                            helper.refreshFocusedTab(component);
                        } else {
                            window.localStorage.removeItem('loaded');
                        }
                    }*/
                    if (!window.location.hash && component.get("v.step") === 3) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce");
    },
    closeFocusedTab: function (component) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const {getFocusedTabInfo, refreshTab} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    }
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reload();
            } else {
                contractAccountComponent.reload();
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent) {
            if (supplySearchComponent instanceof Array) {
                let supplyComponentToObj = Object.assign({}, supplySearchComponent);
                supplyComponentToObj[0].resetBox();
            } else {
                supplySearchComponent.resetBox();
            }
        }
    },
    reloadAll: function (component, cases) {
        let self = this;
        // if (cases){
        //     const myCase = cases[0];
        //     if (myCase) {
        //         component.set("v.divisionId", myCase.CompanyDivision__c);
        //     }
        // }
        self.resetSupplyForm(component);
        self.reloadContractAddition(component);
        self.reloadContractAccount(component);
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
    updateCommodityToDossier: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        component.find("apexService").builder()
            .setMethod("updateCommodityToDossier")
            .setInput({
                dossierId: dossierId,
                commodity: component.get("v.commodity")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateAdvancedSearchCondition: function(component){
        let commodityCondition = "RecordType.Name = '" + component.get('v.commodity') + "'";
        // let marketCondition = "Market__c = 'Free'";

        let andConditions = component.get("v.andConditions");
        andConditions.splice(0, andConditions.length);
        andConditions.push(commodityCondition);
        // andConditions.push(marketCondition);

        component.set("v.andConditions", andConditions);
    },
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');

        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId: dossierId,
                origin: origin,
                channel: channel
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    },

    getMeters: function (component, nextStep) {
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        const meterList = component.find('meterList');
        const caseList = component.get("v.caseTile");
        let counter = 0;
        let that = this;
        this.showSpinner(component);
        let allHasNonActiveEnergy = true;
        caseList.forEach((caseRecord, i) => {
            this.getMetersForCase(component, caseRecord, (meterList instanceof Array) ? meterList[i] : meterList, function(hasNonActiveEnergy){
                allHasNonActiveEnergy = allHasNonActiveEnergy && hasNonActiveEnergy;
                caseList[i].hasNonActiveEnergy = hasNonActiveEnergy;
                counter++;
                if(counter === caseList.length){
                    if(nextStep || allHasNonActiveEnergy) {
                        if(allHasNonActiveEnergy) ntfSvc.error(ntfLib, 'The consumption place has Reactive/inductive energy. The process continues with Distributor reading');
                        component.set("v.step", component.get("v.steps").SENDING_CHANNEL.Title);
                    }
                    component.set("v.caseTile", caseList);
                    component.set('v.hasNonActiveEnergy', allHasNonActiveEnergy);
                    that.hideSpinner(component);
                }
            });
        });
    },

    getMetersForCase: function (component, caseRecord, meterList, callback){
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        let that = this;
        component.find('apexService').builder()
            .setMethod("getMeters")
            .setInput({
                'eneltel': caseRecord.Supply__r.ServicePoint__r.ENELTEL__c,
                'caseId' : caseRecord.Id
            })
            .setResolve(function (response) {
                if (response.error) {
                    that.hideSpinner(component);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if(callback){
                    callback(response.hasNonActiveEnergy);
                }
                component.set("v.showMeters", true);
                meterList.setMeterList(JSON.parse(response.metersJson));
            })
            .setReject(function (errorMsg) {
                that.hideSpinner(component);
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    updateIndexValues: function (component, meterInfo) {
         let that = this;
         this.showSpinner(component);
        const ntfSvc = component.find('notify');
        const ntfLib = component.find('notifLib');
        const caseList = component.get("v.caseTile");
        const accountId = component.get("v.accountId");

        let meterMap = {};
        caseList.forEach((caseRecord, index) => {
            meterMap[caseRecord.Id] = meterInfo[index];
        });

        component.find('apexService').builder()
            .setMethod("createIndex")
            .setInput({
                'caseList': JSON.stringify(caseList),
                'accountId': accountId,
                'meterInfo': JSON.stringify(meterMap)
            })
            .setResolve(function (response) {
                if (response.error) {
                    that.hideSpinner(component);
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                ntfSvc.success(ntfLib, "All Index Values Are Validated");
                that.getMeters(component, true);
            })
            .setReject(function (errorMsg) {
                that.hideSpinner(component);
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    setCurrentFlow: function (component, helper, currentFlow) {
        if (!currentFlow) {
            currentFlow = component.get("v.flows").CONTRACT_ACCOUNT_SPLIT;
        }
        let typeOfWork = $A.get('$Label.c.SelectTypeOfWork');
        component.set("v.currentFlow", currentFlow);

        switch (currentFlow) {
            case component.get("v.flows").CONTRACT_MERGE:
                typeOfWork = $A.get('$Label.c.ContractMerge');
                break;
            case component.get("v.flows").CONTRACT_ACCOUNT_SPLIT:
                typeOfWork = $A.get('$Label.c.ContractAccountSplit');
                break;
            default:
                component.set("v.currentFlow", '');
        }
    },
    representedInFlow: function (component, helper, step, flowTitle){
        let result = false;
        for (let i = 0; i <= Object.keys(step.Flows).length; i++) {
            let flowName = step.Flows[i];
            if (flowName === 'ALL'){
                result = true;
            } else if (component.get("v.flows").hasOwnProperty(flowName)) {
                let stepFlowTitle = component.get("v.flows")[flowName];
                if (stepFlowTitle === flowTitle){
                    result = true;
                }
            }
        }
        return result;
    },
    restoreCurrentStep: function (component, helper) {
        let contractAccountId = component.get("v.contractAccountId");
        let contractId = component.get("v.contractId");
        let caseList = component.get("v.caseTile");
        let commodity = component.get("v.commodity");
        let currentFlow = component.get("v.currentFlow");
        let origin = component.get("v.originSelected");
        let channel = component.get("v.channelSelected");
        let showMeters = component.get("v.showMeters");
        let steps = component.get("v.steps");
        let metersValidated = component.get("v.metersValidated");

        let currentStepTitle = steps.SET_CHANNEL_AND_ORIGIN.Title;

        if (showMeters) {
            currentStepTitle = steps.METER_LIST.Title;
        } else if (contractAccountId) {
            currentStepTitle = steps.CONTRACT_ACCOUNT.Title;
        } else if (contractId) {
            currentStepTitle = steps.CONTRACT.Title;
        } else if (caseList && caseList.length) {
            currentStepTitle = steps.ADVANCED_SEARCH.Title;
        } else if (commodity) {
            currentStepTitle = steps.COMMODITY.Title;
        //} else if (currentFlow){
        //    currentStepTitle = steps.SCRIPT_MANAGEMENT.Title;
        }

        let stepIndexes = component.get("v.stepIndexesMap");
        component.set("v.currentStepIndex", stepIndexes[currentStepTitle]);
        component.set("v.previousStepIndex", stepIndexes[currentStepTitle] - 1);

        component.set("v.step", currentStepTitle);
    },
    fillStepIndexes: function (component, helper) {
        let stepIndexes = {};

        let i = 0;
        for (const stepName in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(stepName)) {
                stepIndexes[stepName] = i;
                i++;
            }
        }

        component.set("v.stepIndexesMap", stepIndexes);
    },
    getStepFromIndex: function (component, stepIndex) {
        let index = 0;
        for (const stepTitle in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(stepTitle)) {
                if (index === stepIndex) {
                    return component.get("v.steps")[stepTitle];
                }
                index++;
            }
        }
        return null;
    },
    getIndexFromStepTitle: function (component, stepTitle) {
        let stepIndex = 0;
        for (const step in component.get("v.steps")) {
            if (component.get("v.steps").hasOwnProperty(step)) {
                if (step === stepTitle) {
                    return stepIndex;
                }
                stepIndex++;
            }
        }
        return null;
    },
    getNextStep: function (component, helper, stepValue) {
        if (stepValue === null) {
            stepValue = 0;
        }
        let stepIndex = this.getIndexFromStepTitle(component, stepValue);
        let nextStep;
        let stepsCount = Object.keys(component.get("v.steps")).length;
        if ((stepIndex >= stepsCount - 1)) {
            return this.getStepFromIndex(component, stepsCount - 1);
        }
        for (let i = stepIndex + 1; i <= stepsCount; i++) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (helper.representedInFlow(component, helper, currentStep, component.get("v.currentFlow"))) {
                    nextStep = currentStep;
                    break;
                }
            }
        }
        return nextStep;
    },
    getStepFromTitle: function (component, helper, title) {
        let step = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (currentStep.Title === title) {
                    step = currentStep;
                    break;
                }
            }
        }
        return step;
    },
    getCurrentStep: function (component, helper) {
        let currentStepTitle = component.get("v.step");
        if (helper.isNullOrEmpty(currentStepTitle)){
            return ;
        }
        return helper.getStepFromTitle(component, helper, currentStepTitle);
    },
    getFirstStep: function (component, helper) {
        let firstStep = null;
        for (let i = 0; i <= Object.keys(component.get("v.steps")).length; i++) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (helper.representedInFlow(component, helper, currentStep, component.get("v.currentFlow"))) {
                    firstStep = currentStep;
                    break;
                }
            }
        }
        return firstStep;
    },
    getPreviousStep: function (component, helper, stepTitle) {
        let stepIndex = this.getIndexFromStepTitle(component, stepTitle);
        let previousStep;

        if ((stepIndex === 0) || (stepIndex - 1) === 0) {
            return this.getStepFromIndex(component, 0);
        }

        for (let i = stepIndex - 1; i >= 0; i--) {
            let currentStep = this.getStepFromIndex(component, i);
            if (component.get("v.steps").hasOwnProperty(currentStep.Title)) {
                if (helper.representedInFlow(component, helper, currentStep, component.get("v.currentFlow"))) {
                    previousStep = currentStep;
                    break;
                }
            }
        }

        return previousStep;
    },
    getInput: function (component, helper, inputName) {
        return Array.isArray(component.find(inputName)) ? component.find(inputName)[0] : component.find(inputName);
    },
    goToNextStep: function (component, helper) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let currentStepTitle = component.get("v.step");
        const isContractMerge = component.get("v.currentFlow") === component.get("v.flows").CONTRACT_MERGE;

        switch (currentStepTitle) {
            case component.get("v.steps").SET_CHANNEL_AND_ORIGIN.Title:
                if (!component.get('v.originSelected') || (component.get('v.originSelected') === "undefined")){
                    component.set('v.originSelected', component.get('v.dossier').Origin__c);
                }
                if (!component.get('v.channelSelected') || (component.get('v.channelSelected') === "undefined")){
                    component.set('v.channelSelected', component.get('v.dossier').Channel__c);
                }
                if(component.get('v.originSelected') && component.get('v.channelSelected')) {
                    component.set('v.disableOriginChannel',true);
                    helper.setChannelAndOrigin(component);
                } else {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.SelectOriginAndChannel"));
                }
                break;
            case component.get("v.steps").COMMODITY.Title:
                helper.updateCommodityToDossier(component);

                const contractSelectionComponent = helper.getInput(component, helper, "contractSelectionComponent");
                if (contractSelectionComponent) {
                    contractSelectionComponent.getContracts();
                }

                break;
            case component.get("v.steps").ADVANCED_SEARCH.Title:
                if (component.get("v.caseTile").length === 0) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredSupply"));
                    return;
                }

                let companyDivisionId = component.get("v.caseTile")[0].CompanyDivision__c;
                component.set('v.divisionId', companyDivisionId);

                let mixedCompanyFound = false;
                component.get("v.caseTile").forEach(myCase => {
                    if (myCase.CompanyDivision__c !== companyDivisionId) {
                        mixedCompanyFound = true;
                        component.set("v.divisionId", "");
                        ntfSvc.error(ntfLib, $A.get('$Label.c.AllTheSuppliesShouldBeInSameCompany'));
                    }
                });

                if (!mixedCompanyFound) {
                    if (isContractMerge) {
                        helper.setEffectiveDate(component, helper);
                    }
                    let cases = component.get("v.caseTile");
                    helper.reloadAll(component, cases);
                }

                helper.checkCasesToSupplies(component, helper, (result) => {
                    if (result.error){
                        ntfSvc.error(ntfLib, result.errorMsg);
                        return;
                    }

                    helper.toNextStep(component, helper);
                });
                return;
            case component.get("v.steps").CONTRACT.Title:
                let contractId = component.get("v.contractId");
                if (!contractId || contractId === "new") {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAdditionMergeDescription"));
                    return;
                }
                helper.setContractAndContractAccount(component, helper);
                helper.reloadContractAccount(component);
                //helper.updateContractAndContractSignedDateOnOpportunity(component);
                break;
            case component.get("v.steps").CONTRACT_ACCOUNT.Title:
                if (!isContractMerge) {
                    if (!component.get("v.contractAccountId")) {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountRequired"));
                        return;
                    }
                    helper.setContractAndContractAccount(component, helper);
                }
                let commodity = component.get('v.commodity');
                let isInEnelArea = component.get('v.isInEnelArea');
                if (commodity === 'Gas') {
                    helper.toNextStep(component, helper);
                    break;
                }

                component.set("v.showMeters", true);
                helper.getMeters(component);
                break;
            case component.get("v.steps").METER_LIST.Title:
                helper.showSpinner(component);
                setTimeout(function () {
                    helper.meterListNext(component, helper);
                }, 1000);
                return;
            case component.get("v.steps").SENDING_CHANNEL.Title:
                helper.saveSendingChannel(component);
                break;
        }

        helper.toNextStep(component, helper);
    },
    meterListNext: function (component, helper) {
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");
        let validationInfo = helper.validateAllMeters(component);

        if (!validationInfo.valid) {
            helper.hideSpinner(component);
            ntfSvc.error(ntfLib, "All Index Values Are Not Validated");
            return;
        }
        helper.updateIndexValues(component, validationInfo.meterInfo);
    },
    validateAllMeters: function(component){
        let meterListComponents = component.find("meterList");
        if(!meterListComponents){
            return {valid: false};
        }
        if(!(meterListComponents instanceof Array)){
            meterListComponents = [meterListComponents];
        }
        let valid = true;
        let meterInfo = [];
        meterListComponents.forEach(meterListComponent => {
            let validationData = meterListComponent.validateMeterInfo();
            if(validationData) {
                valid = valid && validationData.isMeterValidated;
                meterInfo.push(validationData.meterInfo);
            }
        });
        return {valid: valid, meterInfo : meterInfo};
    },
    toNextStep: function (component, helper) {
        let currentStepTitle = component.get("v.step");
        let nextStep = helper.getNextStep(component, helper, currentStepTitle);
        component.set("v.step", nextStep.Title);

        let currentStepIndex = helper.getIndexFromStepTitle(component, nextStep.Title);
        let previousStepTitle = helper.getPreviousStep(component, helper, nextStep.Title).Title;
        let previousStepIndex = helper.getIndexFromStepTitle(component, previousStepTitle);
        component.set("v.currentStepIndex", currentStepIndex);
        component.set("v.previousStepIndex", previousStepIndex);
    },
    updateCurrentFlow: function (component, helper) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('updateCurrentFlow')
            .setInput({
                "currentFlow": component.get("v.currentFlow"),
                "dossierId": component.get('v.dossierId')
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    /*,
    updateContractAndContractSignedDateOnDossier: function (component) {
        let dossierId = component.get("v.dossierId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let customerSignedDate = component.get("v.customerSignedDate");
        component
          .find("apexService")
          .builder()
          .setMethod("updateContractAndContractSignedDateOnDossier")
          .setInput({
              dossierId: dossierId,
              contractId: contractId,
              customerSignedDate: customerSignedDate
          })
          .setResolve(function (response) {
              //self.hideSpinner(component);
          })
          .setReject(function (error) {
              ntfSvc.error(ntfLib, error);
          })
          .executeAction();
    }*/
});