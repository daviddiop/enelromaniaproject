({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod("InitializeTechnicalDataChange")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "companyDivisionId": ''// Add by bsow
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    component.set("v.technicalDataRtEle", response.technicalDataChangeRTEle);
                    component.set("v.technicalDataRtGas", response.technicalDataChangeRTGas);
                    self.resetSupplyForm(component);
                    if (response.dossierId && !dossierId) {
                        self.updateUrl(component, accountId, response.dossierId);
                    }
                    if (response.companyDivisionName) {
                        component.set("v.companyDivisionName", response.companyDivisionName);
                        component.set("v.companyDivisionId", response.companyDivisionId);
                    }
                    const dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }

                    let step = 0;
                    if (response.caseTile) {
                        step = 1;
                    }

                    component.set("v.step", step);
                    if (response.caseTile) {
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    }
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveChain: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, dossierId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__TechnicalDataChangeWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 0);
        }
        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    generateCaseMapFieldValues: function (component, selectedSupply) {
        let fieldsGas = ['AvailablePower__c', 'ContractualPower__c', 'EstimatedConsumption__c', 'ConversionFactor__c', 'PressureLevel__c', 'Pressure__c'];
        let fieldsEle = ['AvailablePower__c', 'ContractualPower__c', 'EstimatedConsumption__c', 'PowerPhase__c', 'VoltageLevel__c', 'Voltage__c'];
        let servicePointValues = selectedSupply.ServicePoint__r;
        let mapDataFieldsValues = {};
        component.set("v.recordTypeTechnicalData", '');
        if (selectedSupply.RecordType["DeveloperName"] === "Electric") {
            component.set("v.recordTypeTechnicalData", component.get("v.technicalDataRtEle"));
            for (const fieldsEleItem of fieldsEle) {
                let value = servicePointValues[fieldsEleItem];
                let newFieldValue = {[fieldsEleItem]: value};
                if (!Object.keys(mapDataFieldsValues).length) {
                    mapDataFieldsValues = newFieldValue;
                    continue;
                }
                mapDataFieldsValues = Object.assign(mapDataFieldsValues, newFieldValue);
            }
        } else if (selectedSupply.RecordType["DeveloperName"] === "Gas") {
            component.set("v.recordTypeTechnicalData", component.get("v.technicalDataRtGas"));
            for (const fieldsGasItem of fieldsGas) {
                let value = servicePointValues[fieldsGasItem];
                let newFieldValue = {[fieldsGasItem]: value};
                if (!Object.keys(mapDataFieldsValues).length) {
                    mapDataFieldsValues = newFieldValue;
                    continue;
                }
                mapDataFieldsValues = Object.assign(mapDataFieldsValues, newFieldValue);
            }
        }
        component.set("v.selectCaseFieldsValues", mapDataFieldsValues);
    },
    saveDraftTechnicalData: function (component, event, helper) {
        const self = this;
        self.redirectToDossier(component, helper);
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    }
});