({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;

        let defaultSelectFields = component.get("v.selectFields");
        let serviceFieldsTechnicalData = ['ServicePoint__r.AvailablePower__c', 'ServicePoint__r.ConversionFactor__c', 'ServicePoint__r.ContractualPower__c', 'ServicePoint__r.EstimatedConsumption__c', 'ServicePoint__r.PowerPhase__c', 'ServicePoint__r.Pressure__c', 'ServicePoint__r.PressureLevel__c', 'ServicePoint__r.Voltage__c', 'ServicePoint__r.VoltageLevel__c'];
        let allSelectFields = defaultSelectFields.concat(serviceFieldsTechnicalData);
        component.set("v.selectFields", allSelectFields);
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initialize(component, event, helper);
    },
    onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: $A.get("$Label.c.TechnicalData")
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: $A.get("$Label.c.TechnicalData")
                                });
                            }
                        });
                }
            });
    },
    handleSupplyResult: function (component, event, helper) {
        let selectedSupply = event.getParam("selectedSupply");
        component.set("v.searchedSupply", selectedSupply);
        component.set("v.supplyId", selectedSupply.Id);
        component.set("v.supplyCompanyDivisionId", selectedSupply.CompanyDivision__c);

        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (selectedSupply.ServicePoint__r && (selectedSupply.Id !== selectedSupply.ServicePoint__r.CurrentSupply__c)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + selectedSupply.Name);
            component.set("v.showNewCase", false);
            return;
        }

        for (let j = 0; j < caseList.length; j++) {
            if (selectedSupply.Id === caseList[j].Supply__c) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SupplyAlreadySelected"));
                component.set("v.showNewCase", false);
                return;
            }
        }

        helper.generateCaseMapFieldValues(component, selectedSupply);
        component.set("v.showNewCase", true);
    },
    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    handleCompanyDivisionChanged: function (component, event, helper) {
        event.stopPropagation();
        const divisionId = event.getParam("divisionId");
        const divisionName = event.getParam("divisionName");
        const isCompanyDivisionEnforced = event.getParam("isCompanyDivisionEnforced");
        component.set("v.isCompanyDivisionEnforced", isCompanyDivisionEnforced);
        const myPageRef = component.get("v.pageReference");
        const dossierId = myPageRef.state.c__dossierId;
        if (divisionId && isCompanyDivisionEnforced) {
            component.set("v.hasCompanyDivisionFilled", true);
            component.set("v.companyDivisionId", divisionId);
            component.set("v.companyDivisionName", divisionName);
        } else {
            component.set("v.companyDivisionId", '');
        }
        if (!dossierId) {
            helper.initialize(component, event, helper);
        }

        helper.hideSpinner(component, "spinnerSection");
    },
    handleSuccessCompanyDivision: function (component, event, helper) {
        const companyDivisionIsCompleted = event.getParam('isCompleted');
        const companyDivisionId = event.getParam('companyDivisionId');
        const companyDivisionName = event.getParam('companyDivisionName');
        component.set("v.hasCompanyDivisionFilled", companyDivisionIsCompleted);
        component.set("v.companyDivisionId", companyDivisionId);
        component.set("v.companyDivisionName", companyDivisionName);

        if (companyDivisionIsCompleted) {
            helper.initialize(component, event, helper);
        }
    },
    handleCloseCompanyDivision: function (component, event, helper) {
        const companyDivisionIsCompleted = event.getParam('isCompleted');
        if (!companyDivisionIsCompleted) {
            helper.goToAccount(component, event, helper);
            helper.closeFocusedTab(component, event, helper);
        }
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleSuccessCase: function (component, event, helper) {
        helper.initialize(component, event, helper);
        helper.resetSupplyForm(component, event, helper);
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((caseList.length === 0)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.saveChain(component, helper);
    },
    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftTechnicalData(component, event, helper);
    }
});