/**
 * Created by  bouba on 03/10/2019.
 */

({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initialize(component, event, helper);
    },
    onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            console.log(' response tab Info ', JSON.stringify(response));
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: $A.get("$Label.c.MeterChange")
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: $A.get("$Label.c.MeterChange")
                                });
                            }
                        });
                }
            });
    },

    searchSelectionHandler: function (component, event, helper) {

        const supplyIds = event.getParam('selected');
        if (!supplyIds || !supplyIds.length) {
            return;
        }
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("getSupplyRecord")
            .setInput({
                supplyId: supplyIds[0]
            })
            .setResolve(function (response) {
                if (!response.error) {
                    component.set("v.supplyId", response.supply.Id);
                    component.set("v.supplyCompanyDivisionId", response.supply.CompanyDivision__c);
                    if(response.supply){
                        if (response.supply.ServicePoint__r && (response.supply.Id !== response.supply.ServicePoint__r.CurrentSupply__c)) {
                            ntfSvc.error(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + response.supply.Name);
                            component.set("v.showNewCase", false);
                            return;
                        }
                         if (response.supply.ServicePoint__r.Distributor__r){
                            component.set('v.isEnelDistributor', response.supply.ServicePoint__r.Distributor__r.IsDisCoENEL__c);
                        }
                    }
                    if (caseList) {
                        for (let j = 0; j < caseList.length; j++) {
                            if (response.supply.Id === caseList[j].Supply__c) {
                                ntfSvc.error(ntfLib, $A.get("$Label.c.SupplyAlreadySelected"));
                                component.set("v.showNewCase", false);
                                return;
                            }
                        }
                    }
                    console.log('### response.supply.ServicePoint__r.Distributor__r '+JSON.stringify(response.supply.ServicePoint__r.Distributor__r));
                    if (response.supply.ServicePoint__r.Distributor__r){
                        component.set('v.isEnelDistributor', response.supply.ServicePoint__r.Distributor__r.IsDisCoENEL__c);
                    }
                    console.log('### isEnelDistributor' +component.get('v.isEnelDistributor'));

                    if (response.supply.RecordType["DeveloperName"] === "Electric") {
                        component.set("v.recordTypeMeterChange", component.get("v.meterChangeRtEle"));
                    } else if (response.supply.RecordType["DeveloperName"] === "Gas") {
                        component.set("v.recordTypeMeterChange", component.get("v.meterChangeRtGas"));
                    }
                    component.set("v.showNewCase", true);
                }
            })
            .setReject(function (errorMsg) {
            })
            .executeAction();
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            case 'confirmStep2':
                helper.updateCommodityToDossier(component);
                let commodity = component.get("v.commodity");
                console.log('### commodity '+commodity);
                if (!commodity){
                    return;
                }
                component.set("v.step", 3);
                break;
            case 'confirmStep3':
                let sendingChannel = component.find('sendingChannel');
                sendingChannel.disableInputField(false);
                component.set("v.step", 4);
                break;
            case 'confirmStep4':
                let sendingCha = component.find('sendingChannel');
                sendingCha.saveSendingChannel();
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                component.set("v.step", 4);
                let sendingChannel = component.find('sendingChannel');
                sendingChannel.disableInputField(false);
                break;
            case 'returnStep5':
                component.set("v.step", 5);
                break;
            default:
                break;
        }
    },

    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleSuccessCase: function (component, event, helper) {
        helper.updateDossier(component, event, helper);
    },

    handleSuccessDossier: function (component,event, helper){
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        let sendingChannel = component.find('sendingChannel');
        let billingProfile = component.get('v.billingProfile');
        let account = component.get('v.account');
        if (!sendingChannel){
            return;
        }

        sendingChannel.disableInputField(true);
        component.set("v.step", 5);
    },

    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((caseList.length === 0)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        helper.saveChain(component, helper);
    },

    cancel: function (component, event, helper) {
        component.set("v.showCancelBox", true);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    },

    saveDraft: function (component, event, helper) {
        helper.saveDraftMeterChange(component, event, helper);
    },

    handleOriginChannelSelection: function (component, event, helper) {
        debugger;
        console.log(event);
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        console.log('channelSelected'+originSelected);
        console.log('channelSelected'+channelSelected);
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step", 1);
            let dossier = component.get('v.dossier');
            if (dossier && dossier.Origin__c) {
                component.set("v.step", 1);
            }
        } else {
            component.set("v.step", 0);
        }
    },
    handleSuccessContact: function(component,event,helper){
        let contactRecord = event.getParam('contactRecord');
        if(contactRecord){
            localStorage.clear();
            localStorage.setItem(component.get('v.dossierId'),JSON.stringify(event.getParam('contactRecord')));
            component.set('v.contactId',contactRecord.Id);
        }
        else {
            localStorage.clear();
            component.set('v.contactId',null);
        }
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
        if (!doNotCreatePrivacyChange) {
            component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
            component.set("v.dontProcess", event.getParam("dontProcess"));
        }
        helper.saveChain(component, helper);
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.isClosed', true);
        component.set('v.hasBlockingErrors', true);
    },
});