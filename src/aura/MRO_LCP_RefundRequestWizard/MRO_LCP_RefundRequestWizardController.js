/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 16.03.20.
 */
({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        helper.initialize(component);
        helper.initConsoleNavigation(component, $A.get("$Label.c.RefundRequest"));
    },

    handleSupplyResult: function (component, event, helper) {
        const supplyIds = event.getParam('selected');
        helper.createCase(component, supplyIds);
    },

    handleCaseDelete: function (component, event, helper) {
        component.set('v.caseTile', []);
        component.set('v.caseId', "");
    },

    handleOriginChannelSelection: function (component, event, helper) {
        if (component.get("v.step") > 0) {
            return;
        }
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            helper.setChannelAndOrigin(component);
            helper.changeStep(component, helper, 1);
        }
    },

    handleChangeCaseNotes: function (component, event, helper) {
        component.set('v.caseNotes', event.getSource().get("v.value"));
    },

    handleNotesSuccess: function (component, event, helper) {
        helper.hideSpinner(component);
        if(component.get('v.isSavingDraft')){
            helper.handleSaveDraft(component, helper);
        }
        helper.changeStep(component, helper, 2);
    },

    handleNotesError: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        ntfSvc.error(ntfLib, 'Unexpected error');
        helper.hideSpinner(component);
    },

    handleBillingProfileSelection: function (component, event, helper) {
        helper.handleBillingProfileSelection(component, event.getParam('billingProfileRecordId'));
    },

    handleSendingChannelSelection: function (component, event, helper) {
        helper.performSaveProcess(component, helper, false);
    },

    nextStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('confirmStep', '')) + 1;
        helper.changeStep(component, helper, stepNumber, true);
    },

    editStep: function (component, event, helper) {
        let buttonId = event.getSource().getLocalId();
        let stepNumber = +(buttonId.replace('editStep', ''));
        helper.changeStep(component, helper, stepNumber);
    },

    cancel: function (component) {
        component.set('v.showCancelBox', true);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component);
    },
    onCloseCancelBox: function (component) {
        component.set('v.showCancelBox', false);
    },

    save: function (component, event, helper) {
        helper.handleSave(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.handleSaveDraft(component, helper);
    },


})