/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 16.03.20.
 */
({
    initialize: function (component) {
        let self = this;
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId,
            })
            .setResolve(function (response) {
                if (response.dossierId && !dossierId) {
                    self.updateUrl(component, accountId, response.dossierId);
                    return;
                }
                self.setStep(component, response);

                component.set("v.isClosed", response.isClosed);
                component.set("v.billingProfileId", response.billingProfileId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                self.setCaseTile(component, response);
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    setChannelAndOrigin: function (component) {
        component.find('apexService').builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
            })
            .executeAction();
    },

    createCase: function (component, supplyIds) {
        let that = this;
        that.showSpinner(component);

        component.find('apexService').builder()
            .setMethod("createCase")
            .setInput({
                supplyId: supplyIds[0],
                dossierId: component.get("v.dossierId"),
                originSelected: component.get("v.originSelected"),
                channelSelected: component.get("v.channelSelected"),
            })
            .setResolve(function (response) {
                that.hideSpinner(component);
                if (!response.error) {
                    that.setCaseTile(component, response);
                }

            })
            .setReject(function (error) {
                that.hideSpinner(component);
            })
            .executeAction();
    },

    setCaseTile: function(component, response){
        component.set('v.caseTile', response.caseTile);
        if(response.caseTile && response.caseTile.length > 0){
            component.set('v.caseId', response.caseTile[0].Id);
            component.set('v.caseNotes', response.caseTile[0].Description);
        }
    },

    handleBillingProfileSelection: function (component, billingProfileId) {
        let self = this;
        self.showSpinner(component);
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod('handleBillingProfileSelection')
            .setInput({
                caseId: component.get("v.caseId"),
                billingProfileId: billingProfileId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.billingProfileId', billingProfileId);
                component.set('v.refundMethodIsInvoice', response.isInvoice);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },

    redirectToDossier: function(component){
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        this.redirect(component, pageReference);
    },

    performSaveProcess: function(component, helper, isDraft){
        component.set('v.isSavingDraft', isDraft);
        if(isDraft && component.get('v.step') === 1 && component.get('v.caseNotes')){
            return helper.changeStep(component, helper, 2, true);
        }

        let dossierId = component.get("v.dossierId");

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("saveProcess")
            .setInput({
                dossierId: dossierId,
                caseId: component.get("v.caseId"),
                isDraft: isDraft
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                const pageReference = {
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: dossierId,
                        objectApiName: 'c__Dossier',
                        actionName: 'view'
                    }
                };
                helper.redirect(component, pageReference);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, errors[0].message);
            })
            .executeAction();
    },

    handleSave: function(component, helper){
        component.find('sendingChannelSelection').saveSendingChannel();
    },

    handleSaveDraft: function(component, helper){
        helper.performSaveProcess(component, helper, true);
    },

    setStep: function (component, response) {
        let step = 4;
        if (!response.dossier || !response.dossier.SendingChannel__c) {
            step = 3;
        }
        if (!response.billingProfileId) {
            step = 2;
        }
        if (response.caseTile.length === 0) {
            step = 1;
        }
        if (!response.dossier.Channel__c || !response.dossier.Origin__c) {
            step = 0;
        }
        this.changeStep(component, this, step);
    },
    updateUrl: function (component, accountId, dossierId) {
        let self = this;
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_RefundRequestWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__dossierId": dossierId
            }
        };
        self.redirect(component, pageReference, true);
    },

    changeStep: function (component, helper, stepNumber, isNext) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.set('v.disableOriginChannel', stepNumber > 1);
        if(stepNumber === 2 && isNext){
            helper.showSpinner(component);
            component.find('caseNotesForm').submit();
            return;
        }

        if (stepNumber === 3 && isNext) {
            let refundMethodIsInvoice = component.get("v.refundMethodIsInvoice")
            if (refundMethodIsInvoice) {
                ntfSvc.error(ntfLib, 'The refund method “Invoice” is not available for this process.');
                return;
            }
        }

        component.set("v.step", stepNumber);
    },

    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference, overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },

    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
})