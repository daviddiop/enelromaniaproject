/**
 * Created by David DIOP on 03.02.2020.
 */

({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const companyDivComponent = component.find("companyDivision");

        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initialize(component, event, helper);
        helper.initConsoleNavigation(component, $A.get("$Label.c.LocationApproval"));
    },
    handleSelectOption: function (component, event, helper) {
        let serviceSelected = event.target.value;
        component.set('v.serviceSiteId', serviceSelected);
        let newServiceSiteMap = component.get('v.newServiceSiteMap');
        if (newServiceSiteMap){
            let isNew = newServiceSiteMap.get(serviceSelected) === 'isNew';
            console.log('###isNew: '+isNew);
            if (isNew){
                component.set('v.isNewServiceSite',isNew);
            }else {
                component.set('v.isNewServiceSite',false);
            }
        }

        if (serviceSelected){
            helper.getDistributorByServiceSiteAddres(component,serviceSelected, helper);
        }
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            console.log('###divisionId '+event.getParam("divisionId"));
            if (event.getParam("divisionId")) {
                let selectedCompanyDivision = event.getParam("divisionId");
               // let availableCompanyDivisionIds = component.get("v.companyDivisionIds");
                /*if (!availableCompanyDivisionIds || availableCompanyDivisionIds.indexOf(selectedCompanyDivision) === -1) {
                    const ntfLib = component.find('notifLib');
                    const ntfSvc = component.find('notify');
                    component.set("v.companyDivisionId", null);
                    return;
                }*/
                component.set("v.companyDivisionId", selectedCompanyDivision);
                component.set('v.saveStepValue',8);

                // Apply filter on Contract Account
                /*if (component.find("contractAccountSelectionComponent")) {
                    helper.reloadContractAccount(component);
                }*/
            }
        } catch (err) {
            //console.error('Error on aura: MRO_LCP_AccountSituationWizard :', err);
        }
    },
    handleNewServiceSite: function (component, event, helper) {
        if(component.get('v.account').IsPersonAccount){

        }
        console.log(JSON.stringify(component.get('v.address')));
        component.set('v.showNewService', true);
    },

    handleEditServiceSite: function (component, event, helper) {
        if(component.get('v.account').IsPersonAccount){

        }
        console.log(JSON.stringify(component.get('v.address')));
        let serviceSiteId = component.get('v.serviceSiteId');
        if (serviceSiteId){
            helper.getServiceSiteAddress(component, helper);
        }
    },
    handleSelectBilling: function (component, event, helper) {
        let billingId = event.getParam("billingProfileRecordId");
        component.set('v.billingProfileId',billingId);
        helper.getRefundMethod(component, helper, billingId);
        //component.find('sendingChannel').disableInputField(false);
    },

    closeCaseModal: function (component, event, helper) {
        component.set('v.showNewService', false);
    },
    closeEditServiceSiteModal: function (component, event, helper) {
        component.set('v.showEditService', false);
    },
    disabledForm: function (component, event, helper) {
        let fields = event.getParam("fields");
        component.set('v.fields',fields);
        component.set('v.disabledForm', true);
        component.find('classification').disableInputField(true);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep':
                component.set("v.step", 2);
                component.set('v.disabledForm', false);
                //helper.getDistributorByUserStateProvince(component,helper);
                component.find('classification').disableInputField(false);
                break;
                case 'confirmStep1':
                let validField = component.find('classification').getValidFields();
                if(!validField){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }
                let serviceSites = component.get('v.serviceSite');
                if (serviceSites.length > 0){
                    helper.getNewServiceSites(component, helper);
                }
                component.set("v.step", 3);
                break;
            case 'confirmStep2':
                component.find('classification').handleConfirm();
                let fields = component.get('v.fields');
                if (!fields){
                    component.set("v.step", 3);
                    component.set('v.disabledForm', false);
                    component.find('classification').disableInputField(false);
                    return;
                }

                if (component.get('v.serviceSiteId')) {
                    component.set("v.step", 4);
                } else {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    component.set("v.step", 3);
                }
                break;
            case 'confirmStep4':
                if (component.get('v.billingProfileId')) {
                    let isBusinessAccount = component.get('v.isBusinessAccount');
                    let refundMethod = component.get('v.refundMethod');
                    if (isBusinessAccount && refundMethod != 'Bank Transfer') {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.WrongRefundMethod"));
                        component.set("v.step", 4);
                        return;
                    }
                    component.set("v.step", 5);
                    let sendingChannel = component.find('sendingChannel');
                    sendingChannel.reloadAddressList();
                    sendingChannel.disableInputField(false);
                } else {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.BillingProfileRequired"));
                    component.set("v.step", 4);
                }
                break
            case 'confirmStep5':
                component.find('sendingChannel').saveSendingChannel();
                break
            case 'confirmStep6':
                if(component.get("v.isBusinessAccount") && !component.get('v.contactId')){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RepresentativeSelectionRequired"));
                    return;
                }

                else component.set("v.step", 7);

                break;
            case 'confirmStep7':
                    component.set("v.step", 8);
                let companyDivisionId = component.get('v.companyDivisionId');
                if (!companyDivisionId){
                        component.set('v.saveStepValue',7);
                }
                break;
            default:
                break;
        }
    },
    editStep: function (component, event, helper) {
        let buttonPressed = event.getSource().getLocalId();
        component.find('sendingChannel').disableInputField(true);
        if (buttonPressed === "returnStep1") {
            component.set("v.step", 2);
            component.set('v.disabledForm', false);
            component.find('classification').disableInputField(false);
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 3);
            component.set('v.saveStepValue',7);
            component.set('v.companyDivisionIsEmpty',false);
        } else if (buttonPressed === "returnStep4") {
            component.find('sendingChannel').disableInputField(true);
            component.set("v.step", 4);
        }else if (buttonPressed === "returnStep5") {
            component.set("v.step", 5);
            component.find('sendingChannel').disableInputField(false);
        }else if (buttonPressed === "returnStep6") {
            component.set("v.step", 6);
        }else if (buttonPressed === "returnStep7") {
            component.set("v.step", 7);
        }
    },
    handleCreateService: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let addrFormValues = component.find('address').getValues();
        let serviceSite = component.get('v.serviceSiteObject');
        let addrForm = component.find('address');

        let fields = event.getParam("fields");
        if (!addrForm.isValid()) {
            ntfSvc.error(ntfLib, 'Address must be verified');
            return;
        }
        for (let f in addrFormValues) {
            serviceSite[f] = addrFormValues[f];
        }
        serviceSite.Account__c = component.get('v.accountId');
        component.set('v.serviceSiteObject', serviceSite);
        let serviceInformation = {};
        serviceInformation.serviceSite = component.get('v.serviceSiteObject');
        component.set('v.serviceInformation', serviceInformation);
        component.find('api').builder()
            .setMethod("createServiceSite")
            .setInput({
                'serviceSite': JSON.stringify(component.get('v.serviceInformation'))
            })
            .setResolve(function (response) {
                if (!response.error) {
                    component.set('v.serviceSiteId',response.serviceRecord.Id);
                    component.set('v.showNewService', false);
                    let serviceSiteList = component.get('v.serviceSite');
                    serviceSiteList.push(response.serviceRecord);
                    component.set('v.serviceSite',serviceSiteList);
                    component.set('v.isNewServiceSite',true);
                    component.set('v.refreshNewServiceSite',true);
                    if (component.get('v.serviceSiteId')){
                        helper.getDistributorByServiceSiteAddres(component, component.get('v.serviceSiteId'), helper);
                    }
                    helper.initialize(component, event, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (response) {
            })
            .executeAction();
    },

    handleEditService: function (component, event, helper) {
        if (component.get('v.serviceSiteId')){
            helper.updateServiceSiteAddress(component, event, helper);
        }
    },
    cancel: function (component, event, helper) {
        //helper.handleCancel(component, helper);
        component.set("v.showCancelBox", true);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
        //helper.handleCancel(component, helper);
    },
    handleSuccessDossier: function (component,event,helper){
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        if(selectedChannel ){
            //helper.createCase(component, helper);
            component.set('v.step',6);
        }
    },
    saveDraft: function (component, event, helper) {
        helper.saveDrafLocationApproval(component, event, helper);
    },
    save: function (component, event, helper) {
        let isBusinessAccount = component.get('v.isBusinessAccount');
        if (component.get('v.companyDivisionIsEmpty')){
            //helper.createPrivacyChangeRecord(component);
            if(component.get('v.contactId') && !isBusinessAccount){
                helper.createPrivacyChangeRecord(component);
            } else {
                helper.createCase(component, helper);
            }
        } else {
            if (component.get('v.contactId') && !isBusinessAccount ) {
                helper.createPrivacyChangeRecord(component);
            } else {
                helper.createCase(component, helper);
            }
        }
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
        if (!doNotCreatePrivacyChange) {
            component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
            component.set("v.dontProcess", event.getParam("dontProcess"));
        }
        helper.createCase(component, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            //component.set('v.disabledForm', false);
            //component.find('classification').disableInputField(false);
            
            component.set("v.step", 1);
            const dossierVal = component.get("v.dossier");
            if (dossierVal) {
                if (dossierVal.Status__c === 'Canceled' || dossierVal.Status__c === 'New') {
                    component.set('v.disabledForm', true);
                    component.find('classification').disableInputField(true);
                }

            }
            //component.find('classification').disableInputField(false);
            //component.set('v.disabledForm',false);
        } else {
            component.set("v.step", 0);
        }
    },
    searchHandler: function (component, event, helper) {
        component.set('v.interlocutorFields', event.getParam('searchFields'));
        component.set('v.interlocutorList', event.getParam('individualList'));
        component.set('v.showListBox', true);
    },
    onCloseListBox: function (component, event, helper){
        component.set('v.showListBox', false);
    },
    redirectHandler: function (component, event, helper) {
        let page = event.getParam('page');
        if (page === 'individualSearch') {
            component.set('v.showListBox', false);
        }
        if(page === 'individualEdit'){
        }
    },
    onContactSelect: function (component, event, helper) {
        let contactRecord = event.getParam('contact');
        component.set('v.contactRecord', contactRecord);
        component.set('v.showListBox', false);
    },
    handleSuccessContact: function(component,event,helper){
        //let contactId = event.getParam('contactId');
        //component.set('v.contactId',contactId);
        let contactRecord = event.getParam('contactRecord');
        if(contactRecord){
            localStorage.clear();
            localStorage.setItem(component.get('v.dossierId'),JSON.stringify(event.getParam('contactRecord')));
            component.set('v.contactId',contactRecord.Id);
        }
        else {
            localStorage.clear();
            component.set('v.contactId',null);
        }
    }
});