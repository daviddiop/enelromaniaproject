/**
 * Created by terangacloud on 03.02.2020.
 */

({
    initialize: function (component, event, helper) {
        const self = this;


        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.set("v.accountId", accountId);
      /*  if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        } */

        component.find('api').builder()
            .setMethod("initialize")
            .setInput({
                'accountId': accountId,
                'dossierId': dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
                // 'companyDivisionId': ''
            })
            .setResolve(function (response) {
                if (component.find("classification")) {
                    self.hideSpinner(component, 'spinnerSection');
                }

                if (!response.error) {
                    component.set("v.accountId", response.accountId);
                    component.set("v.account", response.account);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    component.set("v.serviceSite", response.serviceSite);
                    component.set("v.caseRecordType", response.caseRecordType);
                    component.set("v.caseRecord", response.caseRecord);
                    component.set("v.templateId", response.templateId);
                    component.set("v.contactRecord", response.contactRecord);
                    if(component.get("v.contactRecord")){
                        component.set("v.contactId",component.get("v.contactRecord").Id)
                    }

                    if (response.account.RecordType.Name === 'Business') {
                        component.set('v.isBusinessAccount', true);
                    }

                    if(localStorage.getItem(dossierId)  !== null){
                        component.set("v.contactRecord",JSON.parse(localStorage.getItem(dossierId)));
                        component.set("v.contactId",component.get("v.contactRecord").Id)
                    }

                    let addr = {};
                    addr = {
                        'streetNumber': null,
                        'streetNumberExtn': null,
                        'streetName': null,
                        'streetType': null,
                        'apartment': null,
                        'building': null,
                        'city': null,
                        'country': null,
                        'floor': null,
                        'locality': null,
                        'postalCode': null,
                        'province': null
                    };
                    if (response.account.IsPersonAccount) {
                        if(response.account.ResidentialStreetNumber__c){
                        addr.streetNumber =  response.account.ResidentialStreetNumber__c;
                        }
                        if(response.account.ResidentialStreetNumberExtn__c){
                            addr.streetNumberExtn =  response.account.ResidentialStreetNumberExtn__c;
                        }
                        if(response.account.ResidentialStreetName__c){
                            addr.streetName =  response.account.ResidentialStreetName__c;
                        }
                        if(response.account.ResidentialStreetType__c){
                            addr.streetType =  response.account.ResidentialStreetType__c;
                        }
                        if(response.account.ResidentialApartment__c){
                            addr.apartment =  response.account.ResidentialApartment__c;
                        }
                        if(response.account.ResidentialBuilding__c){
                            addr.building =  response.account.ResidentialBuilding__c;
                        }
                        if(response.account.ResidentialCity__c){
                            addr.city =  response.account.ResidentialCity__c;
                        }
                        if(response.account.ResidentialCountry__c){
                            addr.country =  response.account.ResidentialCountry__c;
                        }
                        if(response.account.ResidentialFloor__c){
                            addr.floor =  response.account.ResidentialFloor__c;
                        }
                        if(response.account.ResidentialLocality__c){
                            addr.locality =  response.account.ResidentialLocality__c;
                        }
                        if(response.account.ResidentialPostalCode__c){
                            addr.postalCode =  response.account.ResidentialPostalCode__c;
                        }
                        if(response.account.ResidentialProvince__c){
                            addr.province =  response.account.ResidentialProvince__c;
                        }
                    }
                    component.set('v.address', addr);
                    if (component.get('v.caseRecord') && component.get('v.caseRecord').length !== 0) {
                        component.set("v.caseId", component.get('v.caseRecord')[0].Id);
                        component.find('classification').changeCaseId(component.get('v.caseRecord')[0].Id);
                        if (component.get('v.caseRecord')[0].Service_Site__c) {
                            component.set('v.step', 3);
                            component.set('v.serviceSiteId', component.get('v.caseRecord')[0].Service_Site__c);
                        }
                        if (component.get('v.caseRecord')[0].BillingProfile__c) {
                            component.set('v.billingProfileId', component.get('v.caseRecord')[0].BillingProfile__c);
                            component.set('v.step', 4);
                            component.find('sendingChannel').disableInputField(false);
                        }
                    }

                    const dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'Canceled' || dossierVal.Status__c === 'New') {
                            component.set("v.isClosed", true);
                            component.set('v.disableOriginChannel', true);
                            component.set('v.disabledForm', true);
                            component.find('classification').disableInputField(true);
                        }

                    }

                    let step = component.get('v.step');
                    if (step === 0 && component.get('v.isClosed') === false) {
                        component.set('v.disabledForm', true);
                        component.find('classification').disableInputField(true);
                    }
                         if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                    }
                     if (component.get('v.refreshNewServiceSite')){
                         let serviceSites = component.get('v.serviceSite');
                         if (serviceSites.length > 0){
                             self.getNewServiceSites(component, helper);
                         }
                     }
                    let  account = component.get('v.account');
                    console.log('###Accoutn: '+JSON.stringify(account));
                    if (account){
                        if (account.IsPersonAccount){
                            if (!account.PersonMobilePhone && !account.Phone){
                                ntfSvc.error(ntfLib, $A.get("$Label.c.PhoneNumberAccountMissing "));
                                component.set('v.disableOriginChannel',true);
                                component.set('v.isDisabled',true);
                                component.set('v.step', 0);
                                return;
                            }
                        }
                        if (!account.IsPersonAccount){
                            if (account.PrimaryContact__c ){
                                if (!account.PrimaryContact__r.MobilePhone && !account.PrimaryContact__r.Phone ){
                                    ntfSvc.error(ntfLib, $A.get("$Label.c.PhoneNumberAccountMissing "));
                                    component.set('v.disableOriginChannel',true);
                                    component.set('v.isDisabled',true);
                                    component.set('v.step', 0);
                                    return;
                                }
                            }else{
                                ntfSvc.error(ntfLib, $A.get("$Label.c.PhoneNumberAccountMissing "));
                                component.set('v.disableOriginChannel',true);
                                component.set('v.isDisabled',true);
                                component.set('v.step', 0);
                                return;
                            }
                        }
                    }

                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    },
    updateUrl: function (component, accountId,genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_LocationApprovalWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },

    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");
        const caseId = component.get('v.caseId');

        component.find('api').builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId,
                caseId: caseId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },

    getServiceSiteAddress: function (component, helper) {
        const self = this;
        self.showSpinner(component,'spinnerSection');
        const serviceSiteId = component.get('v.serviceSiteId');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("getServiceSiteAddressField")
            .setInput({
                serviceSiteId: serviceSiteId
            }).setResolve(function (response) {
            self.hideSpinner(component,'spinnerSection');
            if (!response.error) {
                console.log('####serviceSiteAddress '+JSON.stringify(response.serviceSiteAddress));
                if (response.serviceSiteAddress){
                    component.set('v.address',response.serviceSiteAddress);
                    component.set('v.showEditService', true);
                }
            }else {
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },

    getDistributorByServiceSiteAddres: function (component, serviceSiteId, helper) {
        const self = this;
        self.showSpinner(component,'spinnerSection');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('api').builder()
            .setMethod("getDistributorByServiceSiteAddres")
            .setInput({
                serviceSiteId:serviceSiteId
            }).setResolve(function (response) {
            self.hideSpinner(component,'spinnerSection');
            if (!response.error) {
                if (response.distributor){
                    component.set('v.distributor',response.distributor);
                    component.set('v.companyDivisionId',response.companyDivisionId);
                    console.log('### distributor '+JSON.stringify(component.get('v.distributor')));
                    console.log('### companyDivisionId '+component.get('v.companyDivisionId'));
                }
            }else {
                component.set('v.distributor',null);
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },

    getNewServiceSites: function (component, helper) {
        const self = this;
        self.showSpinner(component,'spinnerSection');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        let serviceSite = component.get('v.serviceSite');
        //console.log('###Taille: '+serviceSite.length);
        let serviceSitesIds = [];
        serviceSite.forEach(function(item){
            serviceSitesIds.push(item.value)
        });
        //console.log('###serviceSitesIds: '+JSON.stringify(serviceSitesIds));

        component.find('api').builder()
            .setMethod("getNewServiceSite")
            .setInput({
                serviceSitesIds: JSON.stringify(serviceSitesIds)
            }).setResolve(function (response) {
            self.hideSpinner(component,'spinnerSection');
            if (!response.error) {
                //console.log('###newServiceSiteMap '+JSON.stringify(response.newServiceSite));
                let newServiceSite = response.newServiceSite;
                let newServiceSiteMap =  new Map();
                newServiceSite.forEach(function(item){
                    newServiceSiteMap.set(item,'isNew');
                });
                component.set('v.newServiceSiteMap',newServiceSiteMap);
                component.set('v.refreshNewServiceSite',false);
                /*for (const [key, value] of newServiceSiteMap.entries()) {
                    console.log(`["${key}", "${value}"]`);
                }*/
            }else {
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },

    updateServiceSiteAddress: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');

        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let addrFormValues = component.find('address').getValues();
        let serviceSite = component.get('v.serviceSiteObject');
        let addrForm = component.find('address');

        let fields = event.getParam("fields");
        if (!addrForm.isValid()) {
            ntfSvc.error(ntfLib, 'Address must be verified');
            return;
        }
        for (let f in addrFormValues) {
            serviceSite[f] = addrFormValues[f];
        }
        serviceSite.Account__c = component.get('v.accountId');
        serviceSite.Id = component.get('v.serviceSiteId');
        component.set('v.serviceSiteObject', serviceSite);
        let serviceInformation = {};
        serviceInformation.serviceSite = component.get('v.serviceSiteObject');
        component.set('v.serviceInformation', serviceInformation);
        component.find('api').builder()
            .setMethod("updateServiceSite")
            .setInput({
                'serviceSite': JSON.stringify(component.get('v.serviceInformation'))
            })
            .setResolve(function (response) {
                self.hideSpinner(component,'spinnerSection');
                if (!response.error) {
                    component.set('v.serviceSiteId',response.serviceSiteRecord.Id);
                    component.set('v.showEditService', false);
                    if (component.get('v.serviceSiteId')){
                        helper.getDistributorByServiceSiteAddres(component, component.get('v.serviceSiteId'), helper);
                    }
                    helper.initialize(component, event, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (response) {
            })
            .executeAction();

    },

    saveDrafLocationApproval: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        let fields = component.get('v.fields');
        let caseRecord = component.get('v.caseObject');
        if (fields) {
            for (let f in fields) {
                caseRecord[f] = fields[f];
            }
        }
        if (component.get('v.caseId')) {
            fields.Id = component.get('v.caseId');
        }
        if(component.get('v.contactId')){
            fields.contactId = component.get('v.contactId');
        }
        component.set('v.caseObject', caseRecord);
        let caseInformation = {};
        caseInformation.caseRecord = component.get('v.caseObject');
        component.set('v.caseInformation', caseInformation);

        let serviceSiteId = component.get('v.serviceSiteId');
        let billingId = component.get('v.billingProfileId');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('api').builder()
            .setMethod("saveDraftRequest")
            .setInput({
                'caseFields': JSON.stringify(fields),
                'serviceSiteId': serviceSiteId,
                'billingId': billingId,
                'dossierId': component.get('v.dossierId'),
                'accountId': component.get('v.accountId'),
                'origin': component.get('v.originSelected'),
                'channel': component.get('v.channelSelected')
            })
            .setResolve(function (response) {
                if (!response.error) {
                    self.redirectToDossier(component);
                } else {
                    self.hideSpinner(component, 'spinnerSection');
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (response) {
            })
            .executeAction();
    },
    createCase: function (component) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        let fields = component.get('v.fields');
        let caseRecord = component.get('v.caseObject');
        let fieldString ;

        if (component.get('v.caseId')) {
            fields.Id = component.get('v.caseId');
        }
        if(component.get('v.contactId')){
            fields.contactId = component.get('v.contactId');
        }

        if(fields){
            for (let f in fields) {
                caseRecord[f] = fields[f];
            }
            fieldString = JSON.stringify(fields);
        }

        component.set('v.caseObject', caseRecord);
        let caseInformation = {};
        caseInformation.caseRecord = component.get('v.caseObject');
        component.set('v.caseInformation', caseInformation);

        let serviceSiteId = component.get('v.serviceSiteId');
        let billingId = component.get('v.billingProfileId');
        let companyDivisionId = component.get('v.companyDivisionId');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let input = {
            'caseFields': fieldString,
            'serviceSiteId': serviceSiteId,
            'billingId': billingId,
            'dossierId': component.get('v.dossierId'),
            'accountId': component.get('v.accountId'),
            'origin': component.get('v.originSelected'),
            'channel': component.get('v.channelSelected')
        };
        if (companyDivisionId){
            input["companyDivisionId"] = companyDivisionId;
        }else {
            self.hideSpinner(component, 'spinnerSection');
            ntfSvc.error(ntfLib, $A.get("$Label.c.NoCompanyDivisionForUser"));
            return;
        }
        console.log('###companyDivisionId: '+companyDivisionId);
        component.find('api').builder()
            .setMethod("createCase")
            .setInput(input)
            .setResolve(function (response) {
                if (!response.error) {
                    self.redirectToDossier(component);
                } else {
                    self.hideSpinner(component, 'spinnerSection');
                }
            })
            .setReject(function (response) {
            })
            .executeAction();
    },
    redirectToDossier: function (component) {
        const self = this;
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        self.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 4) {//3
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    },
    getRefundMethod: function (component, helper, billingId) {
        let self = this;
        self.showSpinner(component);
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('api').builder()
            .setMethod('getRefundMethod')
            .setInput({
                billingProfileId: billingId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.refundMethod', response.refundMethod);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    }
});