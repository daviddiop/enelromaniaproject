({
    initialize: function (component) {
        var self = this;
        var myPageRef = component.get("v.pageReference");
        var accountId = myPageRef.state.c__accountId;
        var opportunityId = myPageRef.state.c__opportunityId;
        var dossierId = myPageRef.state.c__dossierId;
        let genericRequestId = myPageRef.state.c__genericRequestId;
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');
        var companyDivComponent = component.find("companyDivision");
        component.set("v.companyDivisionId", "");
        component.set("v.contractId", "");
        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId" :genericRequestId
            })
            .setResolve(function (response) {
                if (response.opportunityId && !opportunityId) {
                    // reload the page with opportunityId param and reRun init method
                    self.updateUrl(component, accountId, response.opportunityId, response.dossierId);
                    return;
                }

                component.set("v.salesSupportUser", response.salesSupportUser);
                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunity", response.opportunity);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.commodity", response.commodity);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.contractId", response.contractIdFromOpp);
                component.set("v.commodityPicklistValues", response.commodityPicklistValues);
                component.set("v.commodityLabel", response.commodityLabel);
                component.set("v.salesUnit", response.salesUnit);
                component.set("v.salesUnitId", response.salesUnit);
                component.set("v.userDepartment", response.userDepartment);
                component.set("v.canCreatePlaceholder", response.canCreatePlaceholder);
                if(response.dossier){
                    component.set("v.isDual",response.dossier.IsDual__c);
                    component.set("v.relatedDossierId",response.dossier.RelatedDossier__c);
                    if(response.dossier.RelatedDossier__c){
                        if(response.dossier.RelatedDossier__r.Commodity__c === 'Gas'){
                            component.set("v.commodity",'Electric');
                        }else{
                            component.set("v.commodity",'Gas');
                        }

                    }
                    if(!response.dossier.IsDual__c){
                        component.set("v.relatedDossierId",'')
                    }
                }
                component.set("v.companySignedId", response.companySignedId);
                component.set("v.salesman", response.salesman);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                if(component.get('v.opportunity') && component.get('v.opportunity').RequestedStartDate__c){
                    component.set('v.requestedStartDate',component.get('v.opportunity').RequestedStartDate__c);
                }
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));
                component.set("v.useBit2WinCart", response.useBit2WinCart);
                component.set('v.subProcess', response.subProcess);
                component.set('v.expirationDate', response.expirationDate);
                component.set("v.user", response.user);
                component.set("v.consumptionList", response.consumptionList);
                component.set("v.useMassiveHelper", response.useMassiveHelper);
                component.set("v.commodityToFtMap", response.commodityToFtMap);
                component.set("v.massiveHelperParams", JSON.stringify({opportunityId : response.opportunityId}));
                if(component.get('v.useMassiveHelper')) {
                    self.setMassiveHelperParams(component, response.commodity);
                }
                self.consumptionListRefactor(component);

                let contractId = component.get("v.contractId") ;
                let market =  "";
                component.set("v.market",market);

                if (response.commodity === 'Gas') {
                    component.set('v.consumptionConventions', true);
                    component.set('v.consumptionConventionsDisabled', true);
                    component.set('v.consumptionType', 'Single Rate');
                }
                if (response.consumptionConventions) {
                    component.set('v.consumptionConventions', response.consumptionConventions);
                    if (response.consumptionConventions === true) {
                        component.set('v.consumptionConventionsDisabled', true);
                        component.set('v.consumptionType', 'Single Rate');
                    }
                }
                if (response.consumptionList && response.consumptionList.length && response.consumptionList.length === 1) {
                    component.set('v.consumptionType', response.consumptionList[0].Type__c);
                    component.set("v.consumptionConventions", true);
                    component.set("v.consumptionConventionsDisabled", true);
                } else if (response.consumptionList && response.consumptionList.length && response.consumptionList.length === 2) {
                    component.set('v.consumptionType', 'Dual Rate');
                }
                let consumptionTypeFromCmp = component.get('v.consumptionType');
                if (!consumptionTypeFromCmp) {
                    component.set('v.consumptionType', 'None');
                }
                self.updateConsumptionConventionsCheckbox(component);
                if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                    let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                    component.set('v.streetName',pointStreetId);

                    if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                        let isDiscoEnel = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                        component.set("v.isDiscoEnel", isDiscoEnel);
                        let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                        component.set("v.vatNumber", vatNumber);
                        let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                        component.set("v.selfReadingEnabled", selfReadingEnabled);
                    }
                }
                //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                if(component.get('v.opportunity').SubProcess__c === 'None'){
                    component.set('v.hasSubProcessNone',true);
                }
                //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                //For Sub-Process Public Tender the contract type should be always business  and markt = Free
                if(component.get('v.opportunity').SubProcess__c === 'PublicTender'){
                    component.set('v.hasSubProcessPublicTender',true);
                }
                if(component.get('v.opportunity').SubProcess__c === 'LastInstanceSupplier'){
                    component.set('v.hasLastInstanceSupplier',true);
                    component.set("v.market",'Regulated')
                }
                if(response.commodity ==='Gas'){
                    component.set("v.market",'Free')
                }

                //For Sub-Process Public Tender the contract type should be always business  and markt = Free
                if (response.opportunityCompanyDivisionId) {
                    component.set("v.companyDivisionId", response.opportunityCompanyDivisionId);
                    component.set('v.pointAddressProvince',response.opportunityCompanyDivisionId);
                }
                if (companyDivComponent) {
                    companyDivComponent.setCompanyDivision();
                }

                let step = -4;//-1
                if ((response.opportunityLineItems.length !== 0) && (response.contractAccountId) && (response.opportunityServiceItems) && (response.opportunityServiceItems.length !== 0)) {
                    step = 6;//4
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if ((response.contractAccountId) && (response.opportunityServiceItems.length !== 0)) {
                    step = 6;//4
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                    //For Person Account the default Contract Type = "Residential"
                } else if (response.opportunity && response.opportunity.Channel__c !== 'Indirect Sales' && response.opportunity.Channel__c !== 'Direct Sales (KAM)' && component.get("v.companyDivisionId") && response.opportunityServiceItems.length !== 0 && response.opportunity.ContractType__c) {
                    step = 4;
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                    //For Person Account the default Contract Type = "Residential"
                } else if ((component.get("v.companyDivisionId") && response.opportunityServiceItems.length !== 0) || component.get("v.isCompanyDivisionEnforced")) {
                    step = 3;
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if (response.opportunityServiceItems.length !== 0) {
                    step = 1;
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if (response.commodity && response.opportunity && response.expirationDate) {
                    step = 0;//1
                    component.set('v.disableWizard',false);
                    component.set('v.disableOriginChannel',true);
                } else if (response.opportunity && response.opportunity.Origin__c && response.opportunity.Channel__c) {
                    step = -3;//0
                    component.set('v.disableWizard',true);
                    component.set('v.disableOriginChannel',false);
                }


                let opportunityVal = response.opportunity;
                if (opportunityVal) {
                    if (opportunityVal.StageName === 'Quoted' || opportunityVal.StageName === 'Closed Lost') {
                        component.set("v.isClosed", true);
                    }
                    if (opportunityVal.Origin__c) {
                        component.set("v.originSelected", opportunityVal.Origin__c);
                    }
                    if (opportunityVal.Channel__c) {
                        component.set("v.channelSelected", opportunityVal.Channel__c);
                    }
                    if (opportunityVal.ContractName__c) {
                        component.set("v.contractName", opportunityVal.ContractName__c);
                    }
                    if (opportunityVal.ContractType__c) {
                        component.set("v.contractType", opportunityVal.ContractType__c);
                    }
                }
                if(component.get("v.userDepartment") && component.get("v.channelSelected") === "Direct Sales (KAM)"){
                    component.set("v.salesUnitId", component.get("v.userDepartment"));
                    component.set("v.salesUnit", null);
                    component.set("v.showUserDepartment", true);
                }
                if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                    component.set("v.userDepartment", '');
                }
                if(response.salesUnitAccount && !component.get("v.salesUnit")){
                    component.set("v.salesUnit", response.salesUnitAccount);
                    if(component.get("v.salesUnit").SalesDepartment__c === component.get("v.channelSelected")) {
                        let salesUnitAccountId = component.get("v.salesUnit").Id;
                        component.set("v.salesUnitId", salesUnitAccountId);
                    }
                    else{ //user with salesUnitId but SalesDepartment not matching with selected channel
                        component.set("v.salesUnit", null);
                    }
                }

                if(opportunityVal && opportunityVal.SalesUnit__c !== component.get("v.salesUnit") && opportunityVal.ContractType__c
                    && (!response.opportunityLineItems && response.opportunityLineItems.length === 0)
                    && (response.opportunityServiceItems && response.opportunityServiceItems.length !== 0)
                    && (!response.contractAccountId)){
                    step = 3;
                }

                /*if(opportunityVal && !opportunityVal.SalesUnit__c && !opportunityVal.CompanySignedBy__c && !opportunityVal.Salesman__c && opportunityVal.ContractType__c
                    && (response.opportunityServiceItems && response.opportunityServiceItems.length !== 0)
                    && (!response.contractAccountId)){
                    step = -1;
                }*/

                if(component.get("v.channelSelected") !== "Indirect Sales" && !component.get("v.companySignedId")){
                    if(response.salesUnitAccount && !component.get("v.salesUnit")){//user with salesUnitId but SalesDepartment not matching with selected channel
                        component.set("v.companySignedId", "");
                    }
                    else if(component.get("v.channelSelected") === "Direct Sales (KAM)" && !component.get("v.salesUnit") && !component.get("v.userDepartment")){//channel direct sales and user without salesUnitId and not in KamUserGroup
                        component.set("v.companySignedId", "");
                    }
                    else if(component.get("v.channelSelected") === "Back Office Sales" || component.get("v.channelSelected") === "Back Office Customer Care"){
                        component.set("v.companySignedId", "");
                    }
                    else{
                        let userId = component.get("v.user").Id;
                        component.set("v.companySignedId", userId);
                    }
                }

                component.set("v.step", step);
                component.set("v.accountId", response.accountId);

                console.log('initialize salesUnit: '+component.get("v.salesUnit"));
                console.log('initialize salesUnitId: '+component.get("v.salesUnitId"));
                console.log('initialize companySignedId: '+component.get("v.companySignedId"));

                if (component.find("contractAccountSelectionComponent")) {
                    self.reloadContractAccount(component);
                }
                let topElement = component.find("topElement").getElement();
                if (topElement) {
                    topElement.scrollIntoView({behavior: "instant", block: "end"});
                }
                // component.find("consumptionTable").update();
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveOpportunity: function (component, stage, callback, cancelReason, detailsReason) {
        let self = this;
        self.showSpinner(component);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId");
        let dossierId = component.get("v.dossierId");
        let relatedDossierId = component.get('v.relatedDossierId'); //simon.matei@accenture.com 03122020
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "dossierId": dossierId,
                "relatedDossierId": relatedDossierId, //simon.matei@accenutre.com 03122020
                "stage": stage,
                "cancelReason": cancelReason,
                "detailsReason": detailsReason
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    console.log(response.errorMsg, response.errorTrace);
                    component.set("v.enableSaveButton", true);
                    component.set("v.isFirstSave", true);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        let self = this;
        self.showSpinner(component);
        let oppId = component.get("v.opportunityId");
        let osiList = component.get("v.opportunityServiceItems");
        let contractAccountId = component.get("v.contractAccountId");
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityId": oppId,
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateTraderToOsi: function (component, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let companyDivisionId = component.get("v.companyDivisionId")
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod('updateTraderToOsiList')
            .setInput({
                "opportunityServiceItems": osiList,
                "companyDivisionId": companyDivisionId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, opportunityId,dossierId) {
        let self = this;
        const {isConsoleNavigation} = component.find("workspace");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_SwitchInWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId,
                "c__dossierId": dossierId
            }
        };
        self.redirect(component, pageReference, true);
    },/*
    setContract: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            let contractId = '';
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractId = contractSelectionComponentToObj[0].getSelectedContract();
            } else {
                contractId = contractSelectionComponent.getSelectedContract();
            }
            component.set("v.contractId", contractId);
        }
    },*/
    openProductSelection: function (component, helper) {
        let useBit2WinCart = component.get("v.useBit2WinCart");
        let pageReference;
        let osiList = component.get("v.opportunityServiceItems");
        if (useBit2WinCart && osiList) {
            var isGas = false;
            var isElectric = false;
            for (var osi of osiList) {
                if (osi.RecordType.DeveloperName === 'Gas') {
                    isGas = true;
                } else if (osi.RecordType.DeveloperName === 'Electric') {
                    isElectric = true;
                }
            }
            var productType = '';
            if (isGas && isElectric) {
                productType = 'Gaz + Energie';
            } else if (isGas) {
                productType = 'Gaz';
            } else if (isElectric) {
                productType = 'Energie';

            }

            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__MRO_LCP_Bit2winCart'
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId"),
                    "c__opportunityName": component.get("v.opportunity").Name,
                    "c__accountId": component.get("v.accountId"),
                    "c__requestType": component.get('v.opportunity').RequestType__c,
                    "c__productType": productType,
                    "c__dossierId": component.get("v.dossierId")
                }
            };
        }
        else {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ProductSelectionWrp',
                },
                state: {
                    "c__opportunityId": component.get("v.opportunityId")
                }
            };
        }

        helper.redirect(component, pageReference,false);
    },
    redirectToOppty: function (component, helper) {
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference,false);
    },
    validateOsi: function (component, osiId) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let channel = component.get('v.channelSelected');
        var self = this;
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("checkOsi")
            .setInput({
                "osiId": osiId,
                "channel":channel

            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;
                osiList.push(newOsi);
                component.set("v.step", 1);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                if(component.find("pointSelection")) {
                    component.find("pointSelection").resetBox();
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    linkOliToOsi: function (component, oli, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let commodity =  component.get("v.commodity");

        component.find('apexService').builder()
            .setMethod("linkOliToOsi")
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }

                if(response.rateType){

                    component.set("v.consumptionType", response.rateType);
                    component.set("v.productRateType", response.rateType);
                    //component.find("consumptionTable").validateConsumptionsData();

                }else {
                    if(commodity === 'Electric') {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.RateTypeIsNotDefinedOnTheProduct"));
                        return;
                    } else {
                        component.set('v.consumptionType', "Single Rate");
                    }
                }
                component.set("v.step", 7);//5
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction()
    },/*
    goToAccount: function (component, event, helper) {
        let accountId = component.get("v.accountId");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference, true);
    },*/
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference,overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    /*if (window.localStorage && component.get("v.step") === 3 ) {
                        if (!window.localStorage['loaded']) {
                            window.localStorage['loaded'] = true;
                            helper.refreshFocusedTab(component);
                        } else {
                            window.localStorage.removeItem('loaded');
                        }
                    }*/
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 4) {//3
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    closeFocusedTab: function (component, event, helper) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const {getFocusedTabInfo, refreshTab} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    }
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
        component.set("v.enableSaveButton", true);
    },
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        let accountId = component.get("v.accountId");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accountId);
            } else {
                contractAccountComponent.reset(accountId);
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    reloadCompanyDivision: function (component) {
        let companyDivisionComponent = component.find("companyDivision");
        if (companyDivisionComponent) {
            if (companyDivisionComponent instanceof Array) {
                let companyDivisionComponentToObj = Object.assign({}, companyDivisionComponent);
                companyDivisionComponentToObj[0].reload();
            } else {
                companyDivisionComponent.reload();
            }
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
    updateCompanyDivisionOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCompanyDivisionInOpportunity")
            .setInput({
                opportunityId: opportunityId,
                companyDivisionId: component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateCommodityToDossier: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let dossierId = component.get("v.dossierId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCommodityToDossier")
            .setInput({
                dossierId: dossierId,
                commodity: component.get("v.commodity")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
                component.set("v.step", 1);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    reloadSendingChannel: function (component) {
        let contractSelectionComponent = component.find("sendingChannelSelection");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadAddressList();
            } else {
                contractSelectionComponent.reloadAddressList();
            }
        }
    },
    updateContractAndContractSignedDateOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractId = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined) ? '' : component.get("v.contractId");
        let salesChannel = component.get('v.channelSelected');
        let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
        console.log('updateContractAndContractSignedDateOnOpportunity newContractDetails: '+JSON.stringify(newContractDetails));
        /* DEFECT MROCRMDEF-1029 --- Need to have 'market' = selectedContract.MarketName
        let market =  (!contractId || contractId === 'new') ? "" : "Free"; */

        let market = (!contractId || contractId == 'new') ? "" : component.get("v.market");
        let salesSupportUser = component.get("v.salesSupportUser");
        component.set("v.market",market);

        if(component.get('v.hasLastInstanceSupplier')){
            component.set("v.market",'Regulated');
        }

        component
            .find("apexService")
            .builder()
            .setMethod("updateContractAndContractSignedDateOnOpportunity")
            .setInput({
                opportunityId: opportunityId,
                contractId: contractId,
                contractType: newContractDetails.type,
                contractName: newContractDetails.name,
                companySignedId: newContractDetails.companySignedId,
                salesUnitId: newContractDetails.salesUnitId,
                salesman: newContractDetails.salesman,
                salesChannel: salesChannel,
                salesSupportUser: salesSupportUser
            })
            .setResolve(function (response) {
                console.log("updateContractAndContractSignedDateOnOpportunity response", JSON.parse(JSON.stringify(response)));
                //self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, errorMsg);
                    return;
                } else {
                    if (response.purgedOLIs) {
                        component.set("v.opportunityLineItems", []);
                    }
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createConsumptions: function (component, consumptions, callback) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let accountId = component.get('v.accountId');
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let consumptionConventions = component.get('v.consumptionConventions');
        consumptions.forEach(consumption => {
            consumption.Customer__c = accountId;
        });
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("insertConsumptionList")
            .setInput({
                'consumptionList': consumptions,
                'contractId': contractId,
                'consumptionConventions': consumptionConventions
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                component.set('v.consumptionList', response.consumptionList);
                self.consumptionListRefactor(component);
                self.calculateTotalExpectedRevenue(component, callback);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveSendingChannel: function (component) {
        component.find("sendingChannelSelection").saveSendingChannel();
    },
    // FF Activation/SwitchIn - Interface Check - Pack2
    setChannelAndOrigin: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let origin = component.get('v.originSelected');
        let channel = component.get('v.channelSelected');
        component.find("apexService").builder()
            .setMethod("setChannelAndOrigin")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId:dossierId,
                origin:origin,
                channel:channel
            })
            .setResolve(function (response) {
                //$A.get("e.force:refreshView").fire();
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    setIsDualAndRelatedDossier: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunity = component.get('v.opportunity');
        let dossierId = component.get('v.dossierId');
        let isDual = component.get('v.isDual');
        let relatedDossierId = component.get('v.relatedDossierId')
        component.find("apexService").builder()
            .setMethod("setIsDualAndRelatedDossier")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId:dossierId,
                isDual:isDual,
                relatedDossierId:relatedDossierId
            })
            .setResolve(function (response) {
                if(component.get("v.relatedDossierCommodity")){
                    if(component.get("v.relatedDossierCommodity") === 'Gas'){
                        component.set("v.commodity",'Electric');
                    }else{
                        component.set("v.commodity",'Gas');
                    }
                }else {
                    component.set("v.commodity",'');
                }
                if(!component.get('v.relatedDossierId')){
                    component.set("v.relatedDossierCommodity",'');
                    component.set("v.commodity",'');
                }

                component.set("v.step", 0);
                //$A.get("e.force:refreshView").fire();
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    setSubProcessAndExpirationDate : function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        let opportunity = component.get('v.opportunity');
        let dossierId = component.get("v.dossierId");
        let subProcess = component.get('v.subProcess');
        let expirationDate = component.get('v.expirationDate');
        component.find("apexService").builder()
            .setMethod("setSubProcessAndExpirationDate")
            .setInput({
                opportunityId: opportunity.Id,
                dossierId:dossierId,
                subProcess:subProcess,
                expirationDate:JSON.stringify(expirationDate)
            })
            .setResolve(function (response) {
                //$A.get("e.force:refreshView").fire();
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
   // FF Activation/SwitchIn - Interface Check - Pack2
//[ENLCRO-646] Switch-In - Check Interface - Pack3
    setRequestedStartDate: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunity = component.get('v.opportunity');
        let requestedStartDate = component.get('v.requestedStartDate');

        component.find("apexService").builder()
            .setMethod("setRequestedStartDate")
            .setInput({
                opportunityId: opportunity.Id,
                requestedStartDate: JSON.stringify(requestedStartDate),
            })
            .setResolve(function (response) {

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    //[ENLCRO-646] Switch-In - Check Interface - Pack3
    //FF OSI Edit - Pack2 - Interface Check

    deleteOsisAndOlisByCommodityChange: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOsisAndOlisByCommodityChange")
            .setInput({
                commodity: component.get("v.commodity"),
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.isDeleted){
                    component.set("v.opportunityServiceItems",[]);
                    component.set("v.opportunityLineItems",[]);
                    component.set("v.requestedStartDate",null);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    //[ENLCRO-646] Switch-In - Check Interface - Pack3
    validateFields: function (component, event) {
        const labelFieldList = new Array();
        let valEffectiveDate = component.find("RequestedStartDate__c");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp == null || tmp.trim() === '')) {
            labelFieldList.push('RequestedStartDate__c');
        }
        if (labelFieldList.length !== 0) {
            for (let e in labelFieldList) {
                const fieldName = labelFieldList[e];
                if (component.find(fieldName)) {
                    let fixField = component.find(fieldName);
                    if (!Array.isArray(fixField)) {
                        fixField = [fixField];
                    }
                    $A.util.addClass(fixField[0], 'slds-has-error');
                }
            }
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return false;
        }
        return true;
    },
    //[ENLCRO-646] Switch-In - Check Interface - Pack3
    //FF OSI Edit - Pack2 - Interface Check

    checkSelectedContract: function(component) {
        let isValid = true;
        let contractId = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined) ? '' : component.get("v.contractId");
        let isNotKamUser = (!component.get("v.userDepartment") || component.get("v.userDepartment") === undefined);
        if (!contractId) {
            let newContractDetails = component.find("contractSelectionComponent").getNewContractDetails();
            console.log('checkSelectedContract: '+JSON.stringify(newContractDetails));
            if (!newContractDetails.type) {
                isValid = false;
            }
            if (component.get("v.channelSelected") === "Indirect Sales" && (!newContractDetails.salesman || !newContractDetails.salesUnitId)) {
                isValid = false;
            }
            else if ((component.get("v.channelSelected") === "Magazin Enel" || component.get("v.channelSelected") === "Magazin Enel Partner"
                || component.get("v.channelSelected") === "Call Center" || component.get("v.channelSelected") === "Info Kiosk")
                && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
                isValid = false;
            }
            //else if (component.get("v.channelSelected") === "Direct Sales (KAM)" && (!newContractDetails.companySignedId || !newContractDetails.salesUnitId)) {
            else if (component.get("v.channelSelected") === "Direct Sales (KAM)"){
                if(!newContractDetails.companySignedId){
                    isValid = false;
                }
                else if(!newContractDetails.salesUnitId && isNotKamUser && component.get("v.user").SalesUnitID__c !== null && component.get("v.user").SalesUnitID__c !== undefined){
                    isValid = false;
                }
            }
            if (isValid) {
                component.set("v.contractType", newContractDetails.type);
            }
        }
        return isValid;
    },
    //For Person Account the default Contract Type = "Residential"
    isBusinessClient: function (component) {//AS: added
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let accountId = component.get("v.accountId");
        //console.log('isBusinessClientAccount: '+ accountId);
        component.find('apexService').builder()
            .setMethod('isBusinessClient')
            .setInput({
                "accountId": accountId
            })
            .setResolve(function (response) {
                component.set('v.isBusinessClient', response.isBusinessClient);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },
    //For Person Account the default Contract Type = "Residential"

    isNotWorkingDate: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let requestedStartDate = component.get('v.requestedStartDate');
        let osiList = component.get("v.opportunityServiceItems");
        let companyDivComponent = component.find("companyDivision");
        let distributorVatNumber = osiList[0].Distributor__r.VATNumber__c;
        let opportunityId = component.get('v.opportunityId');
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("isNotWorkingDate")
            .setInput({
                requestedStartDate: JSON.stringify(requestedStartDate),
                distributorVatNumber: distributorVatNumber,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (osiList && osiList.length !== 0){
                    component.set('v.validationRequestedStartDate', response.validationStartDate);
                    component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);

                    let requestedStartDateAsDate =  new Date(requestedStartDate);
                    let validationRequestedStartDate =  new Date(response.validationStartDate);

                    if (validationRequestedStartDate > requestedStartDateAsDate){
                        ntfSvc.error(ntfLib, $A.get("$Label.c.EnteredDateShouldBeGreaterOrEqualThan") + " "+ component.get('v.validationRequestedStartDate'));
                        return;
                    }
                }

                component.set('v.isNotWorkingDate',response.isNotWorkingDate);
                if (response.isNotWorkingDate && response.isValidationStartDateWorkingDay && response.isValidationStartDateWorkingDay === true){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.IsNotWorkingDay"));
                    return;
                } else {
                    if(!component.get("v.companyDivisionId") && osiList && osiList.length !== 0){
                        component.set("v.companyDivisionId",response.defaultCompanyDivisionId);
                        if (companyDivComponent) {
                            companyDivComponent.setCompanyDivision();
                        }
                    }

                    self.setRequestedStartDate(component);
                    component.set("v.step", 3);
                }

            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    updateConsumptionConventionsOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let consumptionConventions = component.get("v.consumptionConventions");
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateConsumptionConventionsOnOpportunity")
            .setInput({
                consumptionConventions: consumptionConventions,
                opportunityId: opportunityId
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    loadOsi: function (component, osiId, callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let osiList = component.get("v.opportunityServiceItems") || [];
        let newOsiList=[];
        //component.set("v.opportunityServiceItems",newOsiList);
        component.find('apexService').builder()
            .setMethod('loadOsi')
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {

                let updatedOsi = response.opportunityServiceItem;
                osiList.forEach(osi => {
                    if(osi.Id === updatedOsi.Id){
                        newOsiList.push(updatedOsi);
                    }else{
                        newOsiList.push(osi);
                    }
                });
                component.set("v.opportunityServiceItems",newOsiList);
                component.set('v.requestedStartDate',response.defaultStartDate);
                component.set('v.validationRequestedStartDate',response.validationStartDate);
                component.set('v.isValidationStartDateWorkingDay', response.isValidationStartDateWorkingDay);
                if(callback){
                    console.log("Sono nel callback if");
                    callback(updatedOsi);
                }
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    deleteOlisByContractAccountChange: function (component) {
        let self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractAccountId = component.get("v.contractAccountId");
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("deleteOlisByContractAccountChange")
            .setInput({
                opportunityId: opportunityId,
                contractAccountId: contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if(response.isDeleted){
                    component.set("v.opportunityLineItems",[]);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    createPointsFromOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get('v.opportunityId');
        component.find('apexService').builder()
            .setMethod("createPointsFromOpportunity")
            .setInput({
                "opportunityId": opportunityId
            })
            .setResolve(function (response) {
                let osiList = response.opportunityServiceItems;
                console.log(JSON.stringify(osiList));

                component.set("v.step", 1);//0
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                if(component.find("pointSelection")) {
                    component.find("pointSelection").resetBox();
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateConsumptionConventionsCheckbox: function(component){
        let isNewContract = (component.get("v.contractId") === 'new' || component.get("v.contractId") === undefined || component.get("v.contractId") === '');
        let commodity = component.get("v.commodity");
        if(isNewContract){
            if(commodity === 'Electric'){
                component.set("v.consumptionConventionsDisabled", false);
            }
        }
        else{
            component.set("v.consumptionConventionsDisabled", true);
        }
    },
    updateRegulatedTariffs: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get('v.opportunityId');
        let commodity = component.get('v.commodity');
        component.find('apexService').builder()
            .setMethod("updateRegulatedTariffs")
            .setInput({
                "opportunityId": opportunityId,
                "commodity": commodity
            })
            .setResolve(function (response) {
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    consumptionListRefactor: function(component){
        let consumptions = component.get("v.consumptionList");
        if (consumptions){
            consumptions.forEach(function(element){
                element.QuantityJanuary__c = element.QuantityJanuary__c.toFixed(3);
                element.QuantityFebruary__c = element.QuantityFebruary__c.toFixed(3);
                element.QuantityMarch__c = element.QuantityMarch__c.toFixed(3);
                element.QuantityApril__c = element.QuantityApril__c.toFixed(3);
                element.QuantityMay__c = element.QuantityMay__c.toFixed(3);
                element.QuantityJune__c = element.QuantityJune__c.toFixed(3);
                element.QuantityJuly__c = element.QuantityJuly__c.toFixed(3);
                element.QuantityAugust__c = element.QuantityAugust__c.toFixed(3);
                element.QuantitySeptember__c = element.QuantitySeptember__c.toFixed(3);
                element.QuantityOctober__c = element.QuantityOctober__c.toFixed(3);
                element.QuantityNovember__c = element.QuantityNovember__c.toFixed(3);
                element.QuantityDecember__c = element.QuantityDecember__c.toFixed(3);
            });
            component.set("v.consumptionList", consumptions);
        }
    },
    createOLIsForContractAddition: function(component, callback) {
         let ntfSvc = component.find('notify');
         let ntfLib = component.find('notifLib');
         let opportunityId = component.get("v.opportunityId");
         let contractId = component.get("v.contractId");
         let self = this;
         component.find('apexService').builder()
            .setMethod('createOLIsForContractAddition')
            .setInput({
                "opportunityId": opportunityId,
                "contractId": contractId
            })
            .setResolve(function (response) {
                console.log('createOLIsForContractAddition response', JSON.parse(JSON.stringify(response)));
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    console.log(response.errorMsg, response.errorTrace);
                    return;
                } else {
                    component.set("v.opportunityLineItems", response.opportunityLineItems);
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    getCompanyDivisionByTrader: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let traderId = component.get('v.traderId');
        let companyDivComponent = component.find("companyDivision");
        component.find('apexService').builder()
            .setMethod("getCompanyDivisionByTrader")
            .setInput({
                "traderId": traderId
            })
            .setResolve(function (response) {
                let companyDivisionId = response.companyDivisionId;
                component.set('v.companyDivisionId',companyDivisionId);
                if (companyDivComponent) {
                    companyDivComponent.setCompanyDivision();
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    validateOpportunity: function (component,callback) {
        const self = this;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod('validateOpportunity')
            .setInput({
                "dossierId": component.get("v.dossierId"),
                "opportunityId": component.get("v.opportunityId")
            })
            .setResolve(function (response) {
                let validOsi = false;
                let validDossier = false;

                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    component.set("v.enableSaveButton", true);
                } else {
                    validOsi = response.validOsi;
                    validDossier = response.validDossier;
                }

                if(validOsi === false){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ThisOpportunityDoesntHaveRelatedOsi"));
                    component.set("v.enableSaveButton", true);
                }
                if(validDossier === false){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ThisDossierHasARelatedCase"));
                    component.set("v.enableSaveButton", true);
                }
                if (callback) {
                    callback(validOsi, validDossier);
                }
                if (!valid) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }

            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    },

    setMassiveHelperParams: function (component, commodity) {
        let commodityToFtMap = component.get('v.commodityToFtMap');
        let massiveHelperParamsString = component.get('v.massiveHelperParams');
        if (component.get('v.useMassiveHelper') && commodityToFtMap && massiveHelperParamsString && commodity) {
            component.set("v.templateId", commodityToFtMap[commodity]);
            let massiveHelperParams = JSON.parse(massiveHelperParamsString);
            massiveHelperParams.commodity = commodity;
            component.set("v.massiveHelperParams", JSON.stringify(massiveHelperParams));
        }
    },

    calculateTotalExpectedRevenue: function (component, callback) {

        let self = this;
        let opportunityId = component.get('v.opportunityId');
        self.showSpinner(component);
        component.find("apexService").builder()
            .setMethod("calculateTotalExpectedRevenue")
            .setInput({
                'opportunityId': opportunityId
            })
            .setResolve(function (response) {

                if(response.error == false) {
                    console.log('calculateTotalExpectedRevenue.setResolve totalExpectedRevenue',
                        response.totalExpectedRevenue);
                    if (callback) {
                        callback();
                    }
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    console.log(response.errorMsg, response.errorTrace);
                }
                self.hideSpinner(component);
            })
            .setReject(function (error) {

                console.log('calculateTotalExpectedRevenue.setReject');
                ntfSvc.error(ntfLib, error);
                self.hideSpinner(component);
            })
            .executeAction();
    },
    //START simon.matei@accenture.com 02122020
    linkDossier: function(component){
        const self = this;
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
        .setMethod('linkDossier')
        .setInput({
            "dossierId": component.get("v.dossierId"),
            "relatedDossierId": component.get("v.relatedDossierId")
        }).setResolve(function (response) {
           console.log('success', 'linkDossierAndContract' );

        })
        .setReject(function (errorMsg) {
            ntfSvc.error(ntfLib, errorMsg);
        }).executeAction();

    },
    //END simon.matei@accenture.com 02122020
    checkOsisList: function(component) {

        console.log('MRO_LCP_SwitchInWizard.checkOsisList - Enter');
        let self = this;
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        let opportunityId = component.get("v.opportunityId");
        self.showSpinner(component);
        component
            .find('apexService')
            .builder()
            .setMethod('checkOsisList')
            .setInput({
                opportunityId: opportunityId
            })
            .setResolve(function (response) {

                if (response.error) {

                    ntfSvc.error(ntfLib, response.errorMsg);
                } else {

                    if(component.find("pointSelection")) {
                        component.find("pointSelection").resetBox();
                    }
                    if(component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                        if(component.get("v.opportunityServiceItems")[0].ServicePoint__c && component.get("v.opportunityServiceItems")[0].ServicePoint__r.CurrentSupply__c){
                            if(component.get("v.opportunityServiceItems")[0].ServicePoint__r.CurrentSupply__r.Status__c ==='Active'){
                                let traderId = component.get("v.opportunityServiceItems")[0].Trader__c;
                                component.set('v.traderId',traderId);
                                self.getCompanyDivisionByTrader(component);
                            }
                        }

                        if(component.get('v.requestedStartDate')){

                            self.isNotWorkingDate(component);
                        }else{

                            self.validateFields(component);
                            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                        }
                    }
                }
                self.hideSpinner(component);
            })
            .setReject(function (errorMsg) {

                self.hideSpinner(component);
                ntfSvc.error(ntfLib, errorMsg);
            }).executeAction();
    }
})