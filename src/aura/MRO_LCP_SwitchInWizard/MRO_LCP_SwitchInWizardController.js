({
    init: function (component, event, helper) {
        helper.showSpinner(component);
        let myPageRef = component.get("v.pageReference");
        let accountId = myPageRef.state.c__accountId;
        let opportunityId = myPageRef.state.c__opportunityId;
        let dossierId = myPageRef.state.c__dossierId;
        let disabledOpportunityServiceFields ={
            distributor:"disabled",
            availablePower:"disabled",
            contractualPower:"disabled",
            powerPhase:"disabled",
            pressure:"disabled",
            pressureLevel:"disabled",
            voltageSetting:"disabled",
            voltageLevel:"disabled",
            startDate:"disabled",
            flatRate:"disabled"
        };
        component.set("v.disabledOpportunityServiceFields",disabledOpportunityServiceFields);
        component.set("v.accountId", accountId);
        component.set("v.opportunityId", opportunityId);
        component.set("v.dossierId", dossierId);
        //For Person Account the default Contract Type = "Residential"
        helper.isBusinessClient(component);
        //For Person Account the default Contract Type = "Residential"
        helper.initialize(component);
        helper.initConsoleNavigation(component, $A.get("$Label.c.SwitchIn"));
    },
    cancel: function (component, event, helper) {
        component.find('cancelReasonSelection').open();
        // Moved to onSaveCancelReason
        // component.set("v.savingWizard", false);
        // helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty);
    },
    onSaveCancelReason: function (component, event, helper) {
        let cancelReason = event.getParam("cancelReason");
        let detailsReason = event.getParam("detailsReason");
        component.set("v.isSaved", false);
        if (cancelReason) {
            component.set("v.savingWizard", false);
            helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty, cancelReason, detailsReason);
        }
    },
    saveDraft: function (component, event, helper) {
        component.set("v.savingWizard", false);
        component.set("v.isSaved", false);
        //helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToOppty);
        //this commented function are moved in controller.getPrivacyId()
        helper.createPrivacyChangeRecord(component);
    },
    save: function (component, event, helper) {
        component.set("v.enableSaveButton", false);
        component.set("v.savingWizard", true);
        component.set("v.isSaved", true);
        //helper.saveOpportunity(component, "Closed Won", helper.redirectToOppty);
        //this commented function are moved in controller.getPrivacyId()
        helper.validateOpportunity(component,function (validOsi, validDossier) {
            if(validOsi && validDossier){
                helper.createPrivacyChangeRecord(component);
            }
        });
        // START simon.matei@accenture.com 02122020
        //if(component.get('v.relatedDossierCommodity')){
            helper.linkDossier(component);
        //}
        //END simon.matei@accenture.com 02122020
    },

    checkDual: function (component, event, helper) {
        let isDual    = component.get('v.isDual');
        console.log('isDual: '+ isDual);
        if(!isDual){
            component.set('v.relatedDossierId', '');
            component.set("v.relatedDossierCommodity",'');
            component.set("v.commodity",'');
        }
        component.set('v.isDual', isDual);
    },

    selectRelatedDossier:function (component, event, helper) {
        let relatedDossierId = event.getParam('Id');
        let relatedDossierCommodity = event.getParam('Commodity__c');
        let relatedDossierDivision = event.getParam('CompanyDivision__c');
        component.set('v.relatedDossierId', relatedDossierId);
        component.set('v.relatedDossierCommodity', relatedDossierCommodity);
        component.set('v.relatedDossierDivision', relatedDossierDivision);

    },
    unSelectRelatedDossier:function (component, event, helper) {
        component.set('v.relatedDossierId', '');
    },
    nextStep: function (component, event, helper) {

        let buttonPressed = event.getSource().getLocalId();
        let ntfLib = component.find("notifLib");
        let ntfSvc = component.find("notify");

        let expirationDateDate =  new Date(component.get('v.expirationDate'));
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        let todayDate = new Date(dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate());

        if (buttonPressed === "confirmStep-2") {
            //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
            component.set('v.hasSubProcessNone',false);
            component.set('v.hasSubProcessPublicTender',false);
            component.set('v.hasLastInstanceSupplier',false);
            //For Person Account the default Contract Type = "Residential"

            if(component.get('v.expirationDate') === null){
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                return;
            }

            if((component.get('v.expirationDate') !== null) && (expirationDateDate < todayDate)){
                ntfSvc.error(ntfLib, $A.get("$Label.c.EnteredDateShouldBeGreaterOrEqualThan") + " "+ today);
                return;
            }

            if(!component.get('v.originSelected') || (component.get('v.originSelected') === "undefined")){
                component.set('v.originSelected', component.get('v.opportunity').Origin__c);
            }
            if(!component.get('v.channelSelected') || (component.get('v.channelSelected') === "undefined")){
                component.set('v.channelSelected', component.get('v.opportunity').Channel__c);
            }
            //FF 29/02


            if(component.get('v.originSelected') && component.get('v.channelSelected')) {
                component.set('v.disableOriginChannel',true);
                component.set('v.disableWizard',false);
                // FF Activation/SwitchIn - Interface Check - Pack2
                helper.setChannelAndOrigin(component);
                if(component.get('v.expirationDate') !== null && component.get("v.subProcess")){
                    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                    if(component.get("v.subProcess") === 'None'){
                        component.set('v.hasSubProcessNone',true);
                    }
                    //For Sub-Process Public Tender the contract type should be always business  and markt = Free
                    if(component.get("v.subProcess") === 'PublicTender'){
                        component.set('v.hasSubProcessPublicTender',true);
                    }

                    if(component.get("v.subProcess") === 'LastInstanceSupplier'){
                        component.set('v.hasLastInstanceSupplier',true);
                    }
                    //For Sub-Process Public Tender the contract type should be always business  and markt = Free
                    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                    helper.setSubProcessAndExpirationDate(component);
                }else{
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }

                if(component.get('v.hasLastInstanceSupplier')){
                    component.set("v.commodity",'Electric');
                }
                if(component.get('v.hasLastInstanceSupplier')){
                    component.set("v.commodity",'Electric');
                }
                if(component.get('v.hasLastInstanceSupplier')){
                    component.set("v.step", 0);
                } else {
                    component.set("v.step", -2);
                }
            }else {
                // FF Activation/SwitchIn - Interface Check - Pack2
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectOriginAndChannel"));
                // FF Activation/SwitchIn - Interface Check - Pack2
                return;
            }
        } else if (buttonPressed === "confirmStep-1") {
            helper.setIsDualAndRelatedDossier(component);

        } else if (buttonPressed === "confirmStep0") {//confirmStep9

            console.log('relatedDossier: ' + component.get('v.relatedDossierId'));
            let commodity = component.get("v.commodity");

            if(component.get('v.useMassiveHelper')) {
                helper.setMassiveHelperParams(component, commodity);
            }

            console.log('commodity: ' + commodity);
            if (commodity === 'Gas') {
                component.set('v.consumptionConventions', true);
                component.set('v.consumptionConventionsDisabled', true);
                component.set('v.consumptionType', 'Single Rate');
            } else {
                component.set('v.consumptionConventions', false);
                component.set('v.consumptionConventionsDisabled', false);
                component.set('v.consumptionType', 'None');
            }
            helper.updateCommodityToDossier(component);
            //FF OSI Edit - Pack2 - Interface Check
            helper.deleteOsisAndOlisByCommodityChange(component);
            //FF OSI Edit - Pack2 - Interface Check
            //component.set("v.step", 1);
        }
        else if(buttonPressed === "confirmStep1") {

            helper.checkOsisList(component);
        }
        else if (buttonPressed === "confirmStep2") {
            //For Person Account the default Contract Type = "Residential"

            if(component.get("v.hasLastInstanceSupplier")){
                component.set("v.market",'Regulated');
            }

            if(component.get("v.commodity") === "Gas"){
                component.set("v.market","Free");
            }

            if(component.get("v.hasSubProcessPublicTender")){
                component.set("v.contractType", 'Business');
            }else{
                if(!component.get('v.contractType')){
                    if (component.get("v.isBusinessClient")) {
                        component.set("v.contractType", 'Business');
                    } else{
                        component.set("v.contractType", 'Residential');
                    }
                }
            }

            //For Person Account the default Contract Type = "Residential"

            if (component.get("v.companyDivisionId")) {
                helper.updateCompanyDivisionOnOpportunity(component);
                //helper.updateTraderToOsi(component);
            } else {
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                return;
            }
            if (!component.get("v.opportunityServiceItems") || component.get("v.opportunityServiceItems").length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredSupply"));
                return;
            }
            if (component.get("v.isVisibleContractAddition")) {
                if(component.get("v.userDepartment") && component.get("v.channelSelected") !== "Direct Sales (KAM)"){
                    component.set("v.userDepartment", '');
                }
                if(!component.get("v.userDepartment")){
                    if(component.get("v.channelSelected") === "Indirect Sales"){
                        component.set("v.salesUnit", null);
                    }
                    component.set("v.salesUnitId", '');
                    component.set("v.salesman", '');
                    component.set("v.companySignedId", '');

                    if(component.get("v.opportunity") && component.get("v.opportunity").SalesUnit__c){
                        component.set("v.salesUnit", component.get("v.opportunity").SalesUnit__c);
                        component.set("v.salesUnitId", component.get("v.opportunity").SalesUnit__c);
                    }
                    if(component.get("v.opportunity") && component.get("v.opportunity").CompanySignedBy__c){
                        component.set("v.companySignedId", component.get("v.opportunity").CompanySignedBy__c);
                    }
                    if(component.get("v.opportunity") && component.get("v.opportunity").Salesman__c){
                        component.set("v.salesman", component.get("v.opportunity").Salesman__c);
                    }
                    console.log('opp: '+JSON.stringify(component.get("v.opportunity")));
                    if(component.get('v.channelSelected') !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").SalesUnit__c){
                        if(component.get("v.salesUnit") && component.get("v.salesUnit").SalesDepartment__c === component.get('v.channelSelected')){
                            let salesAccountId = component.get("v.salesUnit").Id;
                            component.set("v.salesUnitId", salesAccountId);
                        }
                    }
                    if(component.get("v.channelSelected") !== "Indirect Sales" && component.get("v.opportunity") && !component.get("v.opportunity").CompanySignedBy__c){
                        if(!component.get("v.salesUnit")){
                            component.set("v.companySignedId", "");
                        }
                        else{
                            if(component.get("v.channelSelected") !== "Back Office Sales" && component.get("v.channelSelected") !== "Back Office Customer Care"){
                                let userId = component.get("v.user").Id;
                                component.set("v.companySignedId", userId);
                            }
                        }
                    }
                }


                component.set("v.step", 4);
            } else {
                component.set("v.step", 5);
                helper.saveOpportunity(component, "Prospecting");
            }
        } else if (buttonPressed === "confirmStep3") {
            if(component.get("v.companyDivisionId")){
                let companyDivisionId = component.get("v.companyDivisionId");
                component.set('v.pointAddressProvince',companyDivisionId);
            }

            if (!helper.checkSelectedContract(component)) {
                ntfSvc.error(ntfLib,$A.get("$Label.c.RequiredFields"));
                return;
            }
            component.set("v.step", 5);

            if (component.get("v.opportunityServiceItems") && component.get("v.opportunityServiceItems").length !== 0){
                let pointStreetId = component.get("v.opportunityServiceItems")[0].PointStreetId__c;
                component.set('v.streetName',pointStreetId);

                if(component.get("v.opportunityServiceItems")[0].Distributor__c) {
                    let isDiscoEnel = component.get("v.opportunityServiceItems")[0].Distributor__r.IsDisCoENEL__c;
                    component.set("v.isDiscoEnel", isDiscoEnel);
                    let vatNumber = component.get("v.opportunityServiceItems")[0].Distributor__r.VATNumber__c;
                    component.set("v.vatNumber", vatNumber);
                    let selfReadingEnabled = component.get("v.opportunityServiceItems")[0].Distributor__r.SelfReadingEnabled__c;
                    component.set("v.selfReadingEnabled", selfReadingEnabled);
                }
            }

            helper.updateContractAndContractSignedDateOnOpportunity(component);
            //helper.saveOpportunity(component, "Qualification");
            helper.reloadContractAccount(component);
        } else if (buttonPressed === 'confirmStep4') {
            if (!component.get("v.contractAccountId")) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccountRequired")); //$A.get('$Label.c.BillingProfileRequired')
                return;
            }

            helper.deleteOlisByContractAccountChange(component);
            helper.updateOsi(component);
            component.set("v.step", 6);

        } else if (buttonPressed === "confirmStep5") {
            let prdList = component.get("v.opportunityLineItems");
            if (!prdList && prdList.length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectProduct")); //$A.get('$Label.c.OnlyOneProductRequired')); //almeno un PRODOTTO
                return;
            }

            var utilityPrds = prdList.filter(
                oli => oli.Product2.RecordType.DeveloperName === "Utility"
            );
            if (!utilityPrds || utilityPrds.length === 0) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SelectUtilityProduct")); //almeno 1 UTILITY
                return;
            }
            let oli = utilityPrds[0];
            component.find("consumptionTable").update();
            helper.linkOliToOsi(component, oli);
            helper.updateConsumptionConventionsCheckbox(component);
            helper.updateRegulatedTariffs(component);
        } else if (buttonPressed === "confirmStep6") {
            let consumptionList = component.find("consumptionTable").getAllConsumptionObjects();
            helper.createConsumptions(component, consumptionList, function () {
                component.set("v.step", 8);
            });
            if(component.get('v.contractAccountId')){
                helper.reloadSendingChannel(component);
            }
            helper.updateConsumptionConventionsOnOpportunity(component);
        } else if (buttonPressed === "confirmStep7") {
            helper.saveSendingChannel(component);
            component.set("v.enableSaveButton", true);
        }
        /*console.log('step finale: ' +  component.get("v.step"));*/
    },
    editStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep-2") {
            component.set("v.step", -3);
            component.set('v.disableWizard', true);
            component.set('v.disableOriginChannel', false);
        } else if (buttonPressed === "returnStep-1") {
            component.set("v.step", -2);
        }else if (buttonPressed === "returnStep0") {
            component.set("v.step", 0);
        } else if (buttonPressed === "returnStep1") {
            component.set('v.traderId',"");
            component.set("v.step", 1);
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 3);
        } else if (buttonPressed === "returnStep3") {
            component.set("v.step", 4);
        } else if (buttonPressed === "returnStep4") {
            component.set("v.step", 5);
        } else if (buttonPressed === "returnStep5") {
            component.set("v.step", 6);
        } else if (buttonPressed === "returnStep6") {
            component.set("v.step", 7);
        } else if (buttonPressed === "returnStep7") {
            component.set("v.step", 8);
        }
    },
    getContractAccountRecordId: function (component, event, helper) {
        component.set(
            "v.contractAccountId",
            event.getParam("contractAccountRecordId")
        );
    },
    handleSubProcess: function (component, event) {
        let subProcess= event.getParam("subProcess");
        component.set("v.subProcess", subProcess);
    },
    handleExpirationDate: function (component, event) {
        let expirationDate = event.getParam("expirationDate");
        component.set("v.expirationDate", expirationDate);
    },

    getContractData: function (component, event, helper) {
        let contractSelected = event.getParam("selectedContract");
        let customerSignedDate = event.getParam("newCustomerSignedDate");
        let consumptionConventions = event.getParam("consumptionConventions");
        component.set("v.contractId", contractSelected);
        component.set("v.customerSignedDate", customerSignedDate);
        //For Sub-Process Public Tender the contract type should be always business  and markt = Free
        if(contractSelected === 'new' && component.get("v.hasSubProcessPublicTender")) {
            component.set("v.contractType", 'Business');
        }else {
            component.set("v.contractType", event.getParam("newContractType"));
        }
        //For Sub-Process Public Tender the contract type should be always business  and markt = Free
        component.set("v.contractName", event.getParam("newContractName"));
        component.set("v.salesUnitId", event.getParam("newSalesUnitId"));
        component.set("v.companySignedId", event.getParam("newCompanySignedId"));
        component.set("v.salesman", event.getParam("newSalesman"));
        if (consumptionConventions !== null) {
            component.set("v.consumptionConventions", consumptionConventions);
        }
        component.set("v.market", event.getParam("newMarket"));
    },
    handlePointResult: function (component, event, helper) {
        var ntfLib = component.find("notifLib");
        var ntfSvc = component.find("notify");
        //var gmPatt = /^(\d{14})$/i;
        //var eePatt = /^(\w{2}\d{3}\w{1}\d{8})$/i;

        let servicePointId = event.getParam("servicePointId");
        let servicePointCode = event.getParam("servicePointCode");
        let servicePoint = event.getParam("servicePoint");
        let osiList = component.get("v.opportunityServiceItems");
        let commodity = component.get("v.commodity");

        if (servicePoint) {
            if ((servicePoint.RecordType.DeveloperName === 'Gas' && commodity !== 'Gas')
                || (servicePoint.RecordType.DeveloperName === 'Electric' && commodity !== 'Electric')) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.IncoherentCommodity"));
                return;
            }
        }
        if(osiList){
            for (var j = 0; j < osiList.length; j++) {
                if (servicePointCode === osiList[j].ServicePointCode__c) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ServicePointAlreadySelected"));
                    return;
                }
            }
        }


        if (servicePointId) {
            let servicePoint = event.getParam("servicePoint");
            if (servicePoint.CurrentSupply__c != null) {
                let labels = component.find("labels");
                let errorLabel = $A.get("$Label.c.IncompatibleSupplyStatus");
                if (servicePoint.CurrentSupply__r.Status__c === 'Activating'){
                    //ntfSvc.error(ntfLib, 'The POD ' + servicePoint.Code__c + ' is ' + servicePoint.CurrentSupply__r.Status__c + ' with Case ' + servicePoint.CurrentSupply__r.Activator__r.CaseNumber);
                    ntfSvc.error(ntfLib, labels.formatLabel(errorLabel, servicePoint.Code__c, servicePoint.CurrentSupply__r.Status__c, servicePoint.CurrentSupply__r.Activator__r.CaseNumber));
                    return;
                }

                if (servicePoint.CurrentSupply__r.Status__c === 'Terminating') {
                    //ntfSvc.error(ntfLib, 'The POD ' + servicePoint.Code__c + ' is ' + servicePoint.CurrentSupply__r.Status__c + ' with Case ' + servicePoint.CurrentSupply__r.Terminator__r.CaseNumber);
                    ntfSvc.error(ntfLib, labels.formatLabel(errorLabel, servicePoint.Code__c, servicePoint.CurrentSupply__r.Status__c, servicePoint.CurrentSupply__r.Terminator__r.CaseNumber));
                    return;
                }

                if (servicePoint.CurrentSupply__r.CompanyDivision__c === component.get("v.companyDivisionId")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ServicePointNotValid"));
                    return;
                }
            }
        }

        component.set("v.searchedPointId", servicePointId);
        component.set("v.searchedPointCode", servicePointCode);
        component.set("v.showNewOsi", true);
    },
    handleNewOsi: function (component, event, helper) {
        let osiId = event.getParam("opportunityServiceItemId");
        helper.validateOsi(component, osiId);
    },
    handleUploadFinished: function (component, event, helper) {
        helper.createPointsFromOpportunity(component);
    },
    handleOsiDelete: function (component, event, helper) {
        let osiList = component.get("v.opportunityServiceItems");
        let companyDivComponent = component.find("companyDivision");
        let osiId = event.getParam("recordId");
        if(osiList){
            let items = [];
            for (let i = 0; i < osiList.length; i++) {
                if (osiList[i].Id !== osiId) {
                    items.push(osiList[i]);
                }
            }
            if (items.length == 0) {
                component.set("v.step", 0);
                component.set('v.requestedStartDate',null);
                component.set('v.companyDivisionId',null);
                if (companyDivComponent) {
                    companyDivComponent.setCompanyDivision();
                }
            }

            component.set("v.opportunityServiceItems", items);
            component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
        }

    },
    closeOsiModal: function (component, event, helper) {
        component.set("v.showNewOsi", false);
        component.set("v.searchedPointId", "");
        component.set("v.searchedPointCode", "");
        //component.find("pointSelection").resetBox();
    },
    handleProductConfigClick: function (component, event, helper) {
        helper.saveOpportunity(
            component,
            "Qualification",
            helper.openProductSelection,
            ""
        );
    },
    getPrivacyId: function (component, event, helper) {
        let isFirstSave = component.get("v.isFirstSave");

        if(isFirstSave === true){
            component.set("v.isFirstSave", false);
            const ntfLib = component.find("notifLib");
            const ntfSvc = component.find("notify");

            let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
            if (!doNotCreatePrivacyChange) {
                component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
                component.set("v.dontProcess", event.getParam("dontProcess"));
            }

            if (!component.get("v.savingWizard")) {
                helper.saveOpportunity(
                    component,
                    "Qualification",
                    helper.redirectToOppty
                );
                component.set("v.isFirstSave", true);
            }
            if (component.get("v.savingWizard")) {
                if (component.get("v.dontProcess")) {
                    ntfSvc.error(ntfLib, "Do Not Process");
                    $A.get("e.force:refreshView").fire();
                    component.set("v.isFirstSave", true);
                    return;
                }
                helper.saveOpportunity(component, "Quoted", helper.redirectToOppty);
            }
        }
    },
    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            if (event.getParam("divisionId")) {

                component.set("v.companyDivisionId", event.getParam("divisionId"));
                component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));

                if ((component.get("v.step") === 2) && component.get("v.isCompanyDivisionEnforced")) {//0
                    component.set("v.step", 3);//1
                }

                // Apply filter on Contract Account
                if (component.find("contractAccountSelectionComponent")) {
                    helper.reloadContractAccount(component);
                }
                // Apply filter on Contract Addition
                if (component.get("v.isVisibleContractAddition")) {
                    helper.reloadContractAddition(component);
                }
            }
        } catch (err) {
            console.error('Error on aura:SwitchInWizard :', err);
        }
    },
    //[ENLCRO-646] Switch-In - Check Interface - Pack3
    removeError: function (component, event, helper) {
        const labelFieldList = [];

        let valEffectiveDate = component.find("RequestedStartDate__c");
        valEffectiveDate.blur();
        if (!Array.isArray(valEffectiveDate)) {
            valEffectiveDate = [valEffectiveDate];
        }
        let tmp = valEffectiveDate[0].get('v.value');
        if ((tmp != null) && (tmp.trim() !== '')) {
            labelFieldList.push('RequestedStartDate__c');
        }

        for (let e in labelFieldList) {
            const fieldName = labelFieldList[e];
            if (component.find(fieldName)) {
                let fixField = component.find(fieldName);
                if (!Array.isArray(fixField)) {
                    fixField = [fixField];
                }
                $A.util.removeClass(fixField[0], 'slds-has-error');
            }
        }
    },
    //[ENLCRO-646] Switch-In - Check Interface - Pack3
    changeConsumptionType: function (component, event, helper) {
        component.set('v.consumptionType', component.find('changeConsumptionType').get('v.value'));
    },
    changeConsumptionConventions: function (component, event, helper) {
        let consumptionConventions = component.get('v.consumptionConventions');
        if(!component.get("v.productRateType")) {
            if (consumptionConventions === true) {
                component.set('v.consumptionType', 'Single Rate');
            }
            if (consumptionConventions === false) {
                component.set('v.consumptionType', 'None');
            }
        }
    },
    handleOriginChannelSelection: function (component, event, helper) {
        if(component.get('v.disableWizard')){
            let originSelected = event.getParam('selectedOrigin');
            let channelSelected = event.getParam('selectedChannel');
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            if( component.get("v.step") <= -3) {
                if(channelSelected !=null && originSelected !=null){
                    component.set("v.step", -3);
                }else {
                    component.set("v.step", -4);
                }
            } else if(channelSelected !=null || originSelected !=null){
                component.set('v.disableOriginChannel',true);
            }
        }
    },
    handleSavedSendingChannel: function (component, event, helper) {
        component.set("v.step", 9);
    },

    handleUpdateEstimatedConsumption: function (component, event, helper) {
        console.log("event consump==="+JSON.stringify(event));
        let consumptionDetails=event.getParam("consumptionDetails");
        let osiId= consumptionDetails.OpportunityServiceItem__c;
        console.log("event osiId==="+osiId);
        let osiTiles=component.find('osiTile');
        let osiTile;
        if (osiTiles instanceof Array) {
            osiTiles.forEach(osiTileCmp => {
                if(osiTileCmp.getRecordId() === osiId){
                    osiTile=osiTileCmp;
                }
            });
        }else {
            osiTile=osiTiles;

        }
        //if(osiTile){
        helper.loadOsi(component,osiId,function (osi) {
            console.log("new estimated==="+osi.EstimatedConsumption__c);
        });

        //}

    },

    handleUpdateOsi:function(component, event, helper) {
        let osiId= event.getParam("osiId");
        helper.loadOsi(component,osiId,function (updatedOsi) {
            console.log("new estimated==="+updatedOsi.EstimatedConsumption__c);
            let consumptionCmp = component.find("consumptionTable");
            if(consumptionCmp){
                consumptionCmp.update(updatedOsi.Id);
            }
        });
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.hasBlockingErrors', true);
    },

    createOLIsForContractAddition: function(component, event, helper) {
        helper.showSpinner(component);
        helper.createOLIsForContractAddition(component);
    },

    handleOsiDeleteAll: function(component, event, helper){
        helper.showSpinner(component);
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let osiList = component.get("v.opportunityServiceItems");
        let oppId = component.get("v.opportunityId");
        if(osiList.length > 0){
            //oppId = osiList[0].Opportunity__c;
            component.find('apexService').builder()
            .setMethod('deleteAllOsi')
            .setInput({
                "opportunityId": oppId
            }).setResolve(function (response) {
                console.log('success', 'OsiDeleteAll');
                let companyDivComponent = component.find("companyDivision");
                let items = [];
                if (items.length == 0) {
                    component.set("v.step", 0);
                    component.set('v.requestedStartDate',null);
                    component.set('v.companyDivisionId',null);
                    if (companyDivComponent) {
                        companyDivComponent.setCompanyDivision();
                    }
                }
                component.set("v.opportunityServiceItems", items);
                component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
                helper.hideSpinner(component);
            })
            .setReject(function (errorMsg) {
                ntfSvc.error(ntfLib, errorMsg);
                helper.hideSpinner(component);
            }).executeAction();
        }
        component.set('v.isDeleteAllOsi', false);
    },
    closeModal: function(component, event, helper){
        component.set('v.isDeleteAllOsi', false);
    },
    openModal: function(component, event, helper){
        component.set('v.isDeleteAllOsi', true);
    }
});