({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);

        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }

        component.find('apexService').builder()
            .setMethod("init")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "templateId": templateId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
            }).setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error){
                    component.set("v.dossier", response.dossier);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.account", response.account);
                    component.set("v.templateId", response.templateId);
                    component.set("v.channelList", response.channelOptionList);

                    if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                    }

                    const dossierVal = response.dossier;
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                            component.set('v.disableOriginChannel',true);
                        }
                    }
                    let step = 0
;
                    if(component.get('v.originSelected') && component.get('v.channelSelected')){
                        step = 1
                    }
                    component.set("v.step", step);

                }

            }).setReject(function (error) {
				self.hideSpinner(component);
                ntfSvc.error(ntfLib, error);
            }).executeAction();
    },
    listChannel: function() {
        return [
            { apiName: null, label: '' },
            { apiName: "Post mail", label: 'Post mail' },
            { apiName: "Email", label: 'Email' }
        ]
    },
	handleSave: function(component, helper) {
		const self = this;
		helper.showSpinner(component);

		const dossierId = component.get("v.dossierId");
		const accountId = component.get("v.accountId");
		const contractAccountId = component.get("v.contractAccountId");
		const selectedInvoices = component.get("v.selectedInvoices");
		const fileNetIds = component.get("v.fileNetIds");
		const channelSelected = component.get("v.channelSelected");
        const originSelected = component.get("v.originSelected");
        const addressEmail = component.get("v.accountEmail");
        const sendingChannel = component.get('v.selectedChannel');
        const companyDivisionId = component.get("v.companyDivisionId");
        const invoiceList = component.get("v.invoiceList");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

		component.find("apexService").builder()
			.setMethod("saveCase")
			.setInput({
				dossierId: dossierId,
				accountId: accountId,
				contractAccountId: contractAccountId,
				channelSelected: channelSelected,
				originSelected: originSelected,
				addressEmail: addressEmail,
                sendingChannel: sendingChannel,
				invoiceIds: JSON.stringify(selectedInvoices),
				fileNetIds:  JSON.stringify(fileNetIds),
				companyDivisionId: companyDivisionId,
				invoiceList: JSON.stringify(invoiceList)
			}).setResolve(function(response) {
			helper.hideSpinner(component);
			//self.updateDossier(component, helper);
			helper.redirectToDossier(component, helper);

		})
			.setReject(function(error) {
				helper.hideSpinner(component);
				ntfSvc.error(ntfLib, error);
			}).executeAction();
	},
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");
        const channelSelected = component.get("v.channelSelected");
        const originSelected = component.get("v.originSelected");
        const companyDivisionId = component.get("v.companyDivisionId");

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected,
                companyDivisionId: companyDivisionId
            }).setResolve(function (response) {
                self.hideSpinner(component);
                self.redirectToDossier(component, helper);
            }).executeAction();
    },
    redirectToDossier: function (component, helper) {
        const self = this;
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        self.redirect(component, pageReference);
    },
    updateUrl: function (component, accountId,genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_InvoiceDuplicateWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    redirect: function (component, pageReference) {
		component.find('cookieSvc').clearInteractionId();
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    setPeriodSelectionDate : function (component) {
        const dateOfToday = new Date();
        let max = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        max.setMonth(dateOfToday.getMonth() - 3);
        let maxDate = max.getFullYear()+ "-" +(max.getMonth() + 1) + "-" + max.getDate();
        component.set("v.startDate",maxDate);
        component.set("v.endDate",today);
    },

    updateDossier: function (component, helper) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');
        let sendingChannel = component.get('v.selectedChannel');

        component.find('apexService').builder()
            .setMethod("UpdateDossier")
            .setInput({
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected,
                sendingChannel: sendingChannel
            }).setResolve(function (response) {
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
     createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    }
});