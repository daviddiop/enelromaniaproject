({
    init: function (component, event, helper) {

        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        helper.setPeriodSelectionDate(component);

        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);

        helper.initialize(component, event, helper);
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
        if (!doNotCreatePrivacyChange) {
            component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
            component.set("v.dontProcess", event.getParam("dontProcess"));
        }
        helper.handleSave(component, helper);
    },
    onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo().then(function (response) {
                        let focusedTabId = response.tabId;
                        let isSubTab = response.isSubtab;
                        if (isSubTab) {
                            setTabLabel({
                                tabId: focusedTabId,
                                label: handleSelectedRange$A.get("$Label.c.MeterChange")
                            });
                            setTabIcon({
                                tabId: focusedTabId,
                                icon: "utility:case",
                                iconAlt: $A.get("$Label.c.MeterChange")
                            });
                        }
                    });
                }
            });
    },
    searchSelectionHandler: function (component, event, helper) {
        const supplyIds = event.getParam('selected');
        if (!supplyIds || !supplyIds.length) {
            return;
        }

        helper.showSpinner(component);
        component.find('apexService').builder()
            .setMethod("findContractAccount")
            .setInput({
                supplyId: supplyIds[0]
            }).setResolve(function (response) {
                helper.hideSpinner(component);
                component.set('v.contractAccountId', response.contractAccountId);
                component.set('v.contractAccount', response.contractAccount);
                component.set('v.billingProfile', response.billingProfile);
                //helper.updateDossier(component,event,helper);
            }).setReject(function (error) {
                helper.hideSpinner(component);
                const ntfLib = component.find('notifLib');
                const ntfSvc = component.find('notify');
                ntfSvc.error(ntfLib, error);
            }).executeAction();
    },
    removeContracthandler: function (component, event, helper) {
        component.set('v.contractAccountId', null);
        component.set('v.disableOriginChannel',false);
    },
    invoiceSelectHandler: function (component, event, helper) {
        const invoiceList = event.getParam('selectedInvoiceList');
        if (!invoiceList || !invoiceList.length) {
            return;
        }
        component.set('v.invoiceList', invoiceList);
        console.log('**** selectedInvoiceList ***** '+JSON.stringify(invoiceList));
        let fileNetIds = invoiceList.reduce(function (fileNetIds, invoice) {
            if(invoice.fileNetId){
                fileNetIds.push(invoice.fileNetId);
            }
           return fileNetIds;
        }, []);
        component.set('v.fileNetIds', fileNetIds);
        let invoiceIds = invoiceList.reduce(function (invoiceIds, invoice) {
            invoiceIds.push(invoice.invoiceId);
            return invoiceIds;
        }, []);
        component.set('v.selectedInvoices', invoiceIds);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        ntfSvc.success(ntfLib, $A.get("$Label.c.Selected") + ' : ' + invoiceIds);
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case "confirmStep1":
                component.set("v.step", 2);
                break;
            case 'confirmStep2':
                let sendingChannel = component.find('sendingChannel');
                sendingChannel.reloadAddressList();
                component.set("v.step", 3);
                break;
            case "confirmStep3":
                component.find("periodSelection").onConfirmSelection();
                if (component.get('v.isinvalidRange')) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.maxNumberOfMonthIs12"));
                    return;
                }
                if (!component.get("v.contractAccountId")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.ContractAccount") + " " + $A.get("$Label.c.Mandatory"));
                    component.set("v.step", 2);
                    return;
                }
                if (!component.get("v.startDate")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.StartDate") + " - " + $A.get("$Label.c.Mandatory"));
                    component.set("v.step", 2);
                    return;
                }
                if (!component.get("v.endDate")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.EndDate") + " - " + $A.get("$Label.c.Mandatory"));
                    component.set("v.step", 2);
                    return;
                }
                let startDate = new Date(component.get("v.startDate"));
                let endDate = new Date(component.get("v.endDate"));
                if(startDate > endDate){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RangeNotValid"));
                    return;
                }
                component.set("v.step", 4);
                break;
            case "confirmStep4":
                const selectedInvoices = component.get("v.selectedInvoices");
                if (!selectedInvoices || !selectedInvoices.length) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                }
                component.find('sendingChannel').disableInputField(false);
                component.set("v.step", 5);
                break;
            //case "confirmStep5":
                //component.find('sendingChannel').saveSendingChannel();
               // component.set("v.step", 6);
                //break;
            default:
                break;
        }
    },

    editStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep1") {
            component.set("v.step", 1);
        }
        if (buttonPressed === "returnStep2") {
            component.set("v.step", 2);
        }
        if (buttonPressed === "returnStep3") {
            component.set("v.step", 3);
            component.find('sendingChannel').disableInputField(true);
            /*component.find('sendingChannel').disableEditButton(false);*/
        }
        if (buttonPressed === "returnStep4") {

            component.set("v.step", 4);
        }
        if (buttonPressed === "returnStep5") {
            component.set("v.step", 5);
            component.find('sendingChannel').disableInputField(false);
        }

    },
    getContractAccountRecordId: function(component, event, helper) {
        let selectedContractAccount = event.getParam("selectedContractAccount");
        if (selectedContractAccount) {
            component.set("v.contractAccountId", selectedContractAccount.Id);
            component.set("v.companyDivisionId",selectedContractAccount.CompanyDivision__c);
            component.set("v.contractStartDate",selectedContractAccount.StartDate__c);
            if (selectedContractAccount.BillingProfile__r){
                component.set('v.billingProfile',selectedContractAccount.BillingProfile__r);
            }
        }
    },
    save: function (component, event, helper) {
        component.find('sendingChannel').saveSendingChannel();
        //helper.createPrivacyChangeRecord(component);
    },
    closeCaseModal:function(component,event,helper){
      component.set('v.showNewCase',false);
    },
    cancel: function (component) {
        component.set("v.showCancelBox", true);
        //helper.handleCancel(component, helper);
        //component.find('cancelReasonSelection').open();
    },

    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
        //helper.handleCancel(component, helper);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step", 1);
        } else {
            component.set("v.step", 0);
        }

    },
    handleCreateCase: function (component, event, helper) {
        event.preventDefault();
        let fields = event.getParam("fields");
        component.set('v.accountEmail',fields.AccountEmail__c);
        component.set('v.showNewCase',false);
        if(component.get('v.accountEmail')){
            component.set('v.step',3);
            component.find('sendingChannel').disableInputField(true);
        }
    },
    handleSuccessDossier: function (component,event, helper){
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        let sendingChannel = component.find('sendingChannel');
        component.set('v.selectedChannel', selectedChannel);
        let billingProfile = component.get('v.billingProfile');
        let account = component.get('v.account');
        if (selectedChannel === 'Mail') {
            if (billingProfile && billingProfile.Email__c) {
                component.set('v.accountEmail',billingProfile.Email__c);
                //sendingChannel.disableInputField(true);
            } else {
                ntfSvc.error(ntfLib, $A.get('$Label.c.BillingAddressNotValued'));
                sendingChannel.disableInputField(false);
                return;
            }
        }
        if (selectedChannel === 'Email') {
            if(billingProfile && billingProfile.Email__c){
                component.set('v.accountEmail',billingProfile.Email__c);
                //sendingChannel.disableInputField(true);
            } else if(account && account.Email__c){
                component.set('v.accountEmail',account.Email__c);
                //sendingChannel.disableInputField(true);
            } else if(account && account.PersonEmail){
                component.set('v.accountEmail',account.PersonEmail);
                //sendingChannel.disableInputField(true);
            } else {
                const email = event.getParam("email");
                if (!email){
                    return;
                }
                component.set('v.accountEmail',email);
            }
        }
        if(selectedChannel === 'Fax'){
            //component.set("v.step", 4);
        }
        let addressEmail = component.get("v.accountEmail");
        console.log('####addressEmail '+addressEmail);
        if (!selectedChannel){
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            sendingChannel.disableInputField(false);
            return;
        }
        helper.handleSave(component, helper);
    },
  handleSelectedRange: function(component,event,helper){
      const ntfLib = component.find('notifLib');
      const ntfSvc = component.find('notify');
      let invalidNumberOfMonth ;
      component.set('v.startDate',event.getParam("startDate"));
      component.set('v.endDate',event.getParam("endDate"));
      let startDate = new Date(component.get('v.startDate'));
      let endDate = new Date(component.get('v.endDate'));
      let years = endDate.getFullYear() - startDate.getFullYear();
      let numberOfMonths = (years * 12) + (endDate.getMonth() - startDate.getMonth());

      if(numberOfMonths> 12){
          invalidNumberOfMonth = true;
      }
      if(numberOfMonths === 12 && startDate.getDate() <= endDate.getDate()){
          invalidNumberOfMonth = true;
      }
      component.set('v.isinvalidRange', invalidNumberOfMonth);

  }
});