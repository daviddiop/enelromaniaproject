/**
 * Created by Boubacar Sow on 18/12/2019.
 */

({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        let today = new Date();
        let defaultDate = today.getFullYear() + "-" + (today.getMonth()+1) + "-" + (today.getDate());
        console.log('### defaultDate'+defaultDate);
        component.set("v.defaultEffDate",defaultDate)
        helper.initialize(component, event, helper);
    },
    onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: $A.get("$Label.c.TechnicalDataChange")
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: $A.get("$Label.c.TechnicalDataChange")
                                });
                            }
                        });
                }
            });
    },

    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let sendingChannel = component.find('sendingChannel');

        switch (buttonPressed) {
            case 'confirmStep1':
                component.set("v.step", 2);
                break;
            case 'confirmStep2':
                helper.updateCommodityToDossier(component);
                let commodity = component.get("v.commodity");
                if (!commodity){
                    return;
                }
                component.set("v.step", 3);
                break;
            case 'confirmStep3':
                if (!sendingChannel){
                    return;
                }
                sendingChannel.disableInputField(false);
                component.set("v.step", 4);
                break;
            case 'confirmStep4':
                if (!sendingChannel){
                    return;
                }
                sendingChannel.saveSendingChannel();
                break;
            default:
                break;
        }
    },

    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        let sendingChannel = component.find('sendingChannel');

        switch (buttonPressed) {
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                if (!sendingChannel){
                    return;
                }
                sendingChannel.disableInputField(true);
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                if (!sendingChannel){
                    return;
                }
                sendingChannel.disableInputField(false);
                component.set("v.step", 4);
                break;
            default:
                break;
        }
    },


    handleSupplyResult: function (component, event, helper) {
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        let selectedSupplyId = event.getParam("selected");
        component.set("v.supplyId", selectedSupplyId[0]);


        if (selectedSupplyId) {
            helper.getSelectedSypply(component);
        }

    },

    handleSupplySearch: function (component, event, helper) {
        component.set("v.selectedRecordType", event.getParam("selectedRecordType"));
        component.set("v.findButtonEvt", event.getParam("findButtonEvt"));
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleSuccessCase: function (component, event, helper) {
        helper.updateDossier(component, event, helper);
    },

    handleSuccessDossier: function (component,event, helper){
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        let sendingChannel = component.find('sendingChannel');
        let account = component.get('v.account');
        if (!sendingChannel){
            return;
        }
        sendingChannel.disableInputField(true);
        component.set("v.step", 5);

    },

    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((caseList.length === 0)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        if(component.get('v.representativeContactId')){
            helper.createPrivacyChangeRecord(component);
        }
        else {
            helper.saveChain(component, helper);
        }
    },

    cancel: function (component, event, helper) {
        component.set("v.showCancelBox", true);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    },

    saveDraft: function (component, event, helper) {
        helper.saveDraftTechnicalDataChange(component, event, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step", 1);
            let dossier = component.get('v.dossier');
            if (dossier && dossier.Origin__c) {
                component.set("v.step", 2);
            }
        } else {
            component.set("v.step", 0);
        }
    },
    handleSuccessContact: function (component, event, helper) {
        let contactRecord = event.getParam('contactRecord');
        if (contactRecord) {
            localStorage.clear();
            localStorage.setItem(component.get('v.dossierId'), JSON.stringify(event.getParam('contactRecord')));
            component.set('v.representativeContactId', contactRecord.Id);
        } else {
            localStorage.clear();
            component.set('v.representativeContactId', null);
        }
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
        if (!doNotCreatePrivacyChange) {
            component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
            component.set("v.dontProcess", event.getParam("dontProcess"));
        }
        helper.saveChain(component, helper);
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.isClosed', true);
        component.set('v.hasBlockingErrors', true);
    },
});