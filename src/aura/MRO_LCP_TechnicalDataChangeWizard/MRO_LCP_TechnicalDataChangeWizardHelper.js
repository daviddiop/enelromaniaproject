/**
 * Created by Boubacar Sow on 18/12/2019.
 */

({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const genericRequestId = myPageRef.state.c__genericRequestId;

        component.set("v.accountId", accountId);
        /*if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        }*/
        component.find('apexService').builder()
            .setMethod("InitializeTechnicalDataChange")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "templateId": templateId,
                "genericRequestId": genericRequestId,
                "companyDivisionId": ''
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.accountId", response.accountId);
                    component.set("v.account", response.account);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.templateId", response.templateId);
                    component.set("v.dossier", response.dossier);
                    component.set("v.technicalDataChangeRtEle", response.technicalDataChangeRTEle);
                    component.set("v.technicalDataChangeRtGas", response.technicalDataChangeRTGas);
                    component.set("v.commodityPicklistValues", response.commodityPicklistValues);
                    component.set("v.commodityLabel", response.commodityLabel);
                    component.set("v.commodity", response.commodity);

                    component.set("v.contactRecord",response.contactRecord);

                    if(component.get("v.contactRecord")){
                        component.set("v.representativeContactId",component.get("v.contactRecord").Id)
                    }

                    if(localStorage.getItem(dossierId)  !== null){
                        component.set("v.contactRecord",JSON.parse(localStorage.getItem(dossierId)));
                        component.set("v.representativeContactId",component.get("v.contactRecord").Id)
                    }

                    //self.resetSupplyForm(component);
                    console.log('###response.genericRequestId '+response.genericRequestId);
                    if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                    }
                    if (response.companyDivisionName) {
                        component.set("v.companyDivisionName", response.companyDivisionName);
                        component.set("v.companyDivisionId", response.companyDivisionId);
                    }
                    const dossierVal = component.get("v.dossier");
                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }

                    let step = component.get("v.step") == -1 ? 0 : component.get("v.step");
                    if (response.caseTile) {
                        if (response.caseTile.length > 0){
                            step = 3;
                            component.set('v.disableOriginChannel',true);
                        }
                    }


                    component.set("v.step", step);
                    if (response.caseTile) {
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    }
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    getSelectedSypply: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const supplyId = component.get("v.supplyId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("getSelectedSypply")
            .setInput({
                supplyId: supplyId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                console.log('#### response'+JSON.stringify(response));
                if (!response.error) {
                    const caseList = component.get("v.caseTile");
                    let selectedSupply = response.selectedSupply;

                    if (selectedSupply.Account__r.RecordTypeDeveloperName__c == 'Business') {
                        if (selectedSupply.BillingProfile__r.Bank__c === undefined || selectedSupply.BillingProfile__r.IBAN__c === undefined) {
                            ntfSvc.error(ntfLib, 'You cannot proceed further with this process until the Bank and the IBAN are not completed. Please access first \n' +
                                'the Customer Data Change process and then continue with this one.');
                            return;
                        }
                    }

                    component.set("v.selectedSupply", selectedSupply);
                    console.log('#### selectedSupply'+JSON.stringify(selectedSupply.ServicePoint__r));

                    component.set("v.supplyCompanyDivisionId", selectedSupply.CompanyDivision__c);
                    if (selectedSupply.ServicePoint__r && (selectedSupply.Id !== selectedSupply.ServicePoint__r.CurrentSupply__c)) {
                        ntfSvc.error(ntfLib, $A.get("$Label.c.CurrentSupplyIsNotValid") + ' - ' + selectedSupply.Name);
                        component.set("v.showNewCase", false);
                        return;
                    }

                    for (let j = 0; j < caseList.length; j++) {
                        if (selectedSupply.Id === caseList[j].Supply__c) {
                            ntfSvc.error(ntfLib, $A.get("$Label.c.SupplyAlreadySelected"));
                            component.set("v.showNewCase", false);
                            return;
                        }
                    }

                    if(caseList.length > 0 && caseList[0].Supply__c != null){
                        if(selectedSupply.InENELArea__c !== caseList[0].Supply__r.InENELArea__c && selectedSupply.Contract__c !== caseList[0].Supply__r.Contract__c){
                            ntfSvc.error(ntfLib, $A.get('$Label.c.SelectedSuppliesMustHaveSameContractAndSameInENELArea'));
                            component.set("v.showNewCase", false);
                            return;
                        }
                        else if(selectedSupply.Contract__c !== caseList[0].Supply__r.Contract__c){
                            ntfSvc.error(ntfLib, $A.get("$Label.c.SelectedSuppliesMustHaveSameContract"));
                            component.set("v.showNewCase", false);
                            return;
                        }
                        else if(selectedSupply.InENELArea__c !== caseList[0].Supply__r.InENELArea__c){
                            ntfSvc.error(ntfLib, $A.get('$Label.c.SelectedSuppliesMustHaveSameInENELArea'));
                            component.set("v.showNewCase", false);
                            return;
                        }
                    }

                    /*if (selectedSupply.RecordType["DeveloperName"] === "Electric") {
                        component.set("v.recordTypeTechnicalDataChange", component.get("v.technicalDataChangeRtEle"));
                    } else if (selectedSupply.RecordType["DeveloperName"] === "Gas") {
                        component.set("v.recordTypeTechnicalDataChange", component.get("v.technicalDataChangeRtGas"));
                    }*/

                    self.generateCaseMapFieldValues(component, selectedSupply);
                    component.set("v.showNewCase", true);


                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    saveChain: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const contactId = component.get("v.representativeContactId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                contactId : contactId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const caseList = component.get("v.caseTile");
        const dossierId = component.get("v.dossierId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_TechnicalDataChangeWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set('v.disableOriginChannel',false);
            component.set("v.step", 2);
        }
        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    generateCaseMapFieldValues: function (component, selectedSupply) {
        //let fieldsGas = ['AvailablePower__c', 'ContractualPower__c', 'EstimatedConsumption__c', 'ConversionFactor__c', 'PressureLevel__c', 'Pressure__c','FirePlacesCount__c'];
        let fieldsGas = ['EstimatedConsumption__c', 'PressureLevel__c', 'Pressure__c','FirePlacesCount__c','ConsumptionCategory__c','FlowRate__c'];
        let fieldsEle = ['AvailablePower__c', 'ContractualPower__c', 'EstimatedConsumption__c', 'PowerPhase__c', 'VoltageLevel__c', 'Voltage__c','VoltageSetting__c'];
        let servicePointValues = selectedSupply.ServicePoint__r;
        let mapDataFieldsValues = {};
        component.set("v.recordTypeTechnicalDataChange", '');
        if(!servicePointValues){
            let ntfSvc = component.find('notify');
            let ntfLib = component.find('notifLib');
            ntfSvc.warn(ntfLib, '$Label.c.ServicePointMissing');
            servicePointValues = {};
        }
        if (servicePointValues.Distributor__r){
            component.set('v.isEnelDistributor', servicePointValues.Distributor__r.IsDisCoENEL__c);
            component.set('v.distributorId', servicePointValues.Distributor__r.Id);
        }
        console.log('### isEnelDistributor '+component.get('v.isEnelDistributor'));
        if (selectedSupply.RecordType["DeveloperName"] === "Electric") {
            component.set("v.recordTypeTechnicalDataChange", component.get("v.technicalDataChangeRtEle"));

            for (const fieldsEleItem of fieldsEle) {
                let value = servicePointValues[fieldsEleItem];
                let newFieldValue = {[fieldsEleItem]: value};
                if (!Object.keys(mapDataFieldsValues).length) {
                    mapDataFieldsValues = newFieldValue;
                    continue;
                }
                mapDataFieldsValues = Object.assign(mapDataFieldsValues, newFieldValue);
            }
        } else if (selectedSupply.RecordType["DeveloperName"] === "Gas") {
            component.set("v.recordTypeTechnicalDataChange", component.get("v.technicalDataChangeRtGas"));
            for (const fieldsGasItem of fieldsGas) {
                let value = servicePointValues[fieldsGasItem];
                let newFieldValue = {[fieldsGasItem]: value};
                if (!Object.keys(mapDataFieldsValues).length) {
                    mapDataFieldsValues = newFieldValue;
                    continue;
                }
                mapDataFieldsValues = Object.assign(mapDataFieldsValues, newFieldValue);
            }
        }
        component.set("v.selectCaseFieldsValues", mapDataFieldsValues);
        component.set("v.selectCaseFieldsExistingValues", mapDataFieldsValues);
        },
    saveDraftTechnicalDataChange: function (component, event, helper) {
        const self = this;
        self.redirectToDossier(component, helper);
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
    updateDossier: function (component, event, helper) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');

        component.find('apexService').builder()
            .setMethod("UpdateDossier")
            .setInput({
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected
            }).setResolve(function (response) {
            if (!response.error) {
                component.set('v.disableOriginChannel',true);
                self.initialize(component, event, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },

    updateCommodityToDossier: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        const dossierId = component.get("v.dossierId");
        const commodity = component.get("v.commodity");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCommodityToDossier")
            .setInput({
                dossierId: dossierId,
                commodity: commodity
            })
            .setResolve(function (response) {
                if (response.error){
                    return;
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    },

});