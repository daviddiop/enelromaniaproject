/**
 * Created by  bouba on 03/10/2019.
 */

({
    init: function(component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const templateId = myPageRef.state.c__templateId;
        let subProcessChanges={
            isChangeAccountName: false,
            isChangeCompanyName: false,
            isChangeContactData: false,
            isChangeCNP: false,
            isChangeCUI: false,
            isChangeONRC: false,
            isChangeCAEN: false,
            isChangeResidentialAddress:false,
            isChangeOtherFields:false
        };
        if(genericRequestId){
            component.set("v.genericRequestId",genericRequestId);
        }
        component.set("v.openCase", '');
        component.set("v.subProcessChanges", subProcessChanges);
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        component.set("v.templateId", templateId);
        helper.initialize(component, event, helper);
    },
    onRender: function(component) {
        const { getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel } = component.find("workspace");
        isConsoleNavigation()
            .then(function(response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function(response) {
                            let focusedTabId = response.tabId;
                            console.log(" response tab Info ", JSON.stringify(response));
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: $A.get("$Label.c.MeterChange")
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: $A.get("$Label.c.MeterChange")
                                });
                            }
                        });
                }
            });
    },
    handelContactSelection: function(component, event, helper) {
        let isBusinessAccount = event.getParam('isBusinessAccount');
        component.set("v.isBusinessAccountToEdit",isBusinessAccount);
        //console.log('####isBusinessAccount On Wizard: '+isBusinessAccount);
        if (component.get('v.isBusinessAccountToEdit')){
            let account = event.getParam("selected");
            //console.log('####Account: '+JSON.stringify(account));
            component.set("v.account",account);
            component.set("v.accountId",account.Id);
            let subprocess= component.get("v.subProcess");
            component.set("v.validSubProcess", subprocess);
            helper.validateSubProcessValue(component);

        }else {
            let contact=event.getParam("selected");
            component.set("v.contact",contact);

            //let accountChange = component.find("accountChange");
            let subprocess= component.get("v.subProcess");
            component.set("v.validSubProcess", subprocess);
            helper.validateSubProcessValue(component);
        }
    },

  handleSuccessCase: function(component, event, helper) {
    component.set("v.caseId", event.getParam("id"));
    component.set("v.changedMobile", event.getParam("mobile"));
    component.set("v.changedEmail", event.getParam("email"));
    component.set("v.changedPhone", event.getParam("phone"));
    const caseId = component.get("v.caseId");
    const ntfLib = component.find("notifLib");
    const ntfSvc = component.find("notify");
    console.log("caseId==="+caseId);
    console.log("contact==="+JSON.stringify(component.get("v.contact")));

    if ((!caseId)) {
      ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
      return;
    }
    helper.saveChain(component, helper);
   // helper.createPrivacyChangeRecord(component);
    //helper.saveChain(component, helper);
  },
  cancel: function(component, event, helper) {
   component.find('cancellationRequest').open();

  },
  saveDraft: function(component, event, helper) {
    helper.saveDraftServicePointAddressChange(component, event, helper);
  },
  handleOriginChannelSelection: function (component, event, helper) {
    console.log(event);
    let originSelected = event.getParam('selectedOrigin');
    let channelSelected = event.getParam('selectedChannel');
    console.log('originSelected' + originSelected);
    console.log('channelSelected' + channelSelected);
    component.set('v.originSelected', originSelected);
    component.set('v.channelSelected', channelSelected);
    let filled=channelSelected != null && originSelected != null;

    if(filled && component.get("v.step") == 0 && !component.get('v.wizardHeaderLoaded')){
      //component.set('v.disableOriginChannel', filled);
      //component.set("v.step", 3);
    }
    component.set('v.wizardHeaderLoaded', true);
    if(!component.get('v.disableWizardHeader')) {
      if (component.get("v.step") <= 1) {
        if (channelSelected != null && originSelected != null) {
          component.set("v.step", 1);
        } else {
          component.set("v.step", 0);
        }

      } else if (filled) {
        component.set('v.disableOriginChannel', true);
      }

    }
  },
  nextStep: function (component, event, helper) {
    const self = this;
    const buttonPressed = event.getSource().getLocalId();
    const ntfLib = component.find('notifLib');
    const ntfSvc = component.find('notify');
    switch (buttonPressed) {
      case 'confirmStep1':
        if(component.get('v.originSelected') && component.get('v.channelSelected')) {
          component.set('v.disableOriginChannel',true);
          component.set('v.disableWizardHeader',false);

          helper.setChannelAndOrigin(component);
          component.set("v.step", 2);

        }else {
          ntfSvc.error(ntfLib, $A.get("$Label.c.SelectOriginAndChannel"));
          return;
        }

        break;
      case 'confirmStep2':
        let subprocess=component.get("v.subProcess");

        if(subprocess) {
          if (component.get("v.subProcess") === 'None') {
            component.set('v.hasSubProcessNone', true);
          }
          let isPersonalAccount = component.get("v.isPersonAccount");
          if(subprocess === component.get("v.subProcessChangeContact")){
            helper.getOldData(component,'Contact');
          }
          if (subprocess === component.get("v.subProcessChangeOtherFields")) {
            helper.getOldData(component,'Account');
          }
          if(!isPersonalAccount && subprocess === component.get("v.subProcessChangeContact")){
            component.find('contactSelection').open();
            return;
          }
          console.log('***subProcess***** '+component.get('v.subProcess'));
          console.log('***isPersonAccount***** '+component.get('v.isPersonAccount'));
          console.log('***subProcessChangeContact***** '+component.get('v.subProcessChangeContact'));
          console.log('***subProcessChangeOtherFields***** '+component.get('v.subProcessChangeOtherFields'));
          console.log('***subProcessChangeContact***** '+component.get('v.subProcessChangeContact'));
          helper.validateSubProcessValue(component, true);
        }else{
          ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
          return;
        }
        break;
      case 'confirmStep3':
        let accountChange = component.find("accountChange");
        let valid =accountChange.validateFields();
        if(valid){
          let subProcessChanges=component.get("v.subProcessChanges");
          console.log("subProcessChanges.isChangeCNP==="+subProcessChanges.isChangeCNP);
          if(subProcessChanges.isChangeCNP || subProcessChanges.isChangeCUI){
            let key= accountChange.validateKey();
            helper.validateKey(component,key, function () {
              component.set("v.step", 4);
            });
          } /*else if(subProcessChanges.isChangeCAEN){
            let naceCode= accountChange.getNaceCode();
            helper.validateNace(component,naceCode, function () {
              component.set("v.step", 4);
            });
          }*/ else {
            component.set("v.step", 4);
          }

        }
        break;
      case 'confirmStep4':
       // helper.saveSendingChannel(component);
       // component.set("v.step", 5);
        break;

      default:
        break;
    }
  },

  editStep: function (component, event, helper) {
    const buttonPressed = event.getSource().getLocalId();
    switch (buttonPressed) {
      case 'returnStep1':
        component.set("v.step", 1);
        component.set('v.disableOriginChannel',false);
        component.set('v.disableWizardHeader',true);
        break;
      case 'returnStep2':
        component.set("v.step", 2);
        break;
      case 'returnStep3':
        component.set("v.step", 3);
        break;
      case 'returnStep4':
        component.set("v.step", 4);
        break;
      case 'returnStep5':
        component.set("v.step", 5);
        break;
      case 'returnStep6':
        component.set("v.step", 6);
        break;
      case 'returnStep7':
        component.set("v.step", 7);
        break;
      case 'returnStep8':
        component.set("v.step", 8);
        break;
      case 'returnStep9':
        component.set("v.step", 9);
        break;
      case 'returnStep10':
        component.set("v.step", 10);
        break;
      default:
        break;
    }
  },

  handleSubProcess: function (component, event) {
    let subProcess= event.getParam("subProcess");
    component.set("v.subProcess", subProcess);


  },
  /*  getPrivacyId: function (component, event, helper) {
      const ntfLib = component.find('notifLib');
      const ntfSvc = component.find('notify');
      console.log("get privacy Id");
      component.set("v.privacyChangeId", event.getParam('privacyChangeId'));
      component.set("v.dontProcess", event.getParam('dontProcess'));
      console.log("is svaing==="+component.get("v.isSaving"));
      if (!component.get("v.isSaving")) {
        helper.redirectToDossier(component, helper);
        //helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToDossier);
      }
      if (component.get("v.isSaving")) {
        if (component.get("v.dontProcess")) {
          ntfSvc.error(ntfLib, $A.get("$Label.c.DoNotProcess"));
          $A.get('e.force:refreshView').fire();
          return;
        }
        helper.saveChain(component, helper);
        //helper.saveOpportunity(component, "Closed Won", helper.redirectToDossier);
      }
},*/
  closeCancelModal: function (component, event, helper) {
    component.set("v.showCancel", false);
    //helper.resetSupplyForm(component);
  },
  cancel: function (component, event, helper) {
    //component.find('cancellationRequest').open();
    component.set("v.showCancel", true);

    //component.set("v.isSaving", false);
    //helper.saveOpportunity(component, "Closed Lost", helper.redirectToOppty);
  },
  onSaveCancelReason: function (component, event, helper) {
    console.log("cancel========="+JSON.stringify(event));
    helper.redirectToDossier(component, helper);

  },
  save: function (component, event, helper) {
    helper.saveSendingChannel(component);
    let areFieldsValidated = component.get("v.areFieldsValidated");
    if(areFieldsValidated === true) {
      let isSaving=component.get("v.isSaving");
      if(!isSaving){
        component.set("v.isSaving", true);
        let accountChange = component.find("accountChange");
        accountChange.submit();
      }
      //helper.saveChain(component, helper);
    }

    //helper.createPrivacyChangeRecord(component);
   // helper.updateDossierStatus(component);

  },
  saveDraft: function (component, event, helper) {
    component.set("v.isSaving", false);
    //helper.createPrivacyChangeRecord(component);
    if (!component.get("v.isSaving")) {
      helper.redirectToDossier(component, helper);
      //helper.saveOpportunity(component, "Negotiation/Review", helper.redirectToDossier);
    }
  },
  handleEditAccountData: function (component, event, helper) {

   let change= event.getParam("change");

   component.set("v.accountDataChanged", change);

  },
  handleSavedSendingChannel: function (component, event, helper) {
    component.set("v.step", 5);
  },
  validateSendingChannelFields: function(component, event, helper){
    let areFieldsValidated = event.getParam('areFieldsValidated');
    component.set("v.areFieldsValidated", areFieldsValidated);
  }


});