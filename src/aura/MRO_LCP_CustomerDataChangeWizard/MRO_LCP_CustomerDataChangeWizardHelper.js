/**
 * Created by bouba on 03/10/2019.
 */
({
  initialize: function(component, event, helper) {
    const self = this;
    const myPageRef = component.get("v.pageReference");
    const accountId = myPageRef.state.c__accountId;
    const dossierId = myPageRef.state.c__dossierId;
    const genericRequestId = myPageRef.state.c__genericRequestId;
    const gDPRParentDossierId = myPageRef.state.c__gDPRParentDossierId;
    const templateId = myPageRef.state.c__templateId;
    const ntfLib = component.find("notifLib");
    const ntfSvc = component.find("notify");

    self.showSpinner(component);
    component.set("v.accountDataChanged", false);
    component.set("v.isClosed", false);
    component.set("v.showCancel", false);
    component.set("v.isSaving", false);
    component.set('v.account','');
    component.set('v.subProcess','');
    component.set('v.validSubProcess','');
    component.set('v.originSelected','');
    component.set('v.channelSelected','');
    component.set('v.disableOriginChannel',true);
    component.set('v.disableWizardHeader',true);
    component.set('v.originSelected', '');
    component.set('v.channelSelected', '');
    component.set('v.step', 0);

    component.set("v.accountId", accountId);
    component.find("apexService").builder()
      .setMethod("initialize")
      .setInput({
        "accountId": accountId,
        "dossierId": dossierId,
        "interactionId": component.find("cookieSvc").getInteractionId(),
        "genericRequestId": genericRequestId,
        "templateId": templateId,
        "gDPRParentDossierId": gDPRParentDossierId
      }).setResolve(function(response) {
      self.hideSpinner(component);

      if(response.error) {
        let openCase = response.openCase;

        ntfSvc.error(ntfLib, response.errorMsg);
        if(openCase && openCase === true) {
          component.set("v.isClosed",true);
          component.set("v.openCase", 'notEmpty');
        }
        return;
      }

      if (!response.error) {
        component.set("v.caseId", response.caseId);
        component.set("v.accountId", response.accountId);
        component.set("v.dossierId", response.dossierId);
        component.set("v.dossier", response.dossier);
        component.set("v.account", response.account);
        component.set("v.templateId", response.templateId);
        component.set("v.isPersonAccount", response.isPersonAccount);
        component.set("v.recordTypeCustomerDataChange", response.recordTypeCustomerDataChange);

        //set sub process
        component.set("v.subProcessChangeAccName", response.subProcessChangeAccName);
        component.set("v.subProcessChangeCompanyName", response.subProcessChangeCompanyName);
        component.set("v.subProcessChangeContact", response.subProcessChangeContact);
        component.set("v.subProcessChangeCNP", response.subProcessChangeCNP);
        component.set("v.subProcessChangeCUI", response.subProcessChangeCUI);
        component.set("v.subProcessChangeCAEN", response.subProcessChangeCAEN);
        component.set("v.subProcessChangeONRC", response.subProcessChangeONRC);
        component.set("v.subProcessChangeResidentialAddress", response.subProcessChangeResidentialAddress);
        component.set("v.subProcessChangeOtherFields",response.subProcessChangeOtherFields);
        component.set("v.contactId", response.contactId);

        if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
            self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
        }

        component.set("v.dossierId", dossierId);

        if (response.companyDivisionName) {
          component.set("v.companyDivisionName", response.companyDivisionName);
          component.set("v.companyDivisionId", response.companyDivisionId);
        }

        const dossierVal = component.get("v.dossier");

        if (dossierVal) {
          if (dossierVal.Status__c === "New" || dossierVal.Status__c === "Canceled") {
            component.set("v.isClosed", true);
          }
        }

        let step = 0;

        if (dossierVal.Origin__c) {
          component.set("v.originSelected", dossierVal.Origin__c);
        }

        if (dossierVal.Channel__c) {
          component.set("v.channelSelected", dossierVal.Channel__c);
          step = 1;
        }

        let subProcess=dossierVal.SubProcess__c;

        if (subProcess) {
          component.set("v.subProcess", subProcess);
          component.set("v.validSubProcess", subProcess);
          step = 2;
        }

        let disableHeader = step > 1;

        component.set('v.disableWizardHeader', disableHeader);
        component.set('v.disableOriginChannel', disableHeader);
        component.set("v.step", step);

        if(subProcess) {
          let isPersonalAccount = component.get("v.isPersonAccount");

          if(!isPersonalAccount && subProcess === component.get("v.subProcessChangeContact")){
            return;
          }
          helper.validateSubProcessValue(component, false);
        }
      }
    })
      .setReject(function(error) {
        ntfSvc.error(ntfLib, error);
      })
      .executeAction();
  },
  saveChain: function(component, helper) {
    const self = this;
    helper.showSpinner(component);
    const caseList = [{ Id: component.get("v.caseId")}];
    const dossierId = component.get("v.dossierId");
    const accountId = component.get('v.accountId');
    const dossierStatus =  component.get('v.dossierStatus');
    const changedMobile =  component.get('v.changedMobile');
    const changedEmail =  component.get('v.changedEmail');
    const changedPhone =  component.get('v.changedPhone');
    const ntfLib = component.find("notifLib");
    const ntfSvc = component.find("notify");

    component.find("apexService").builder()
      .setMethod("updateCaseList")
      .setInput({
        oldCaseList: JSON.stringify(caseList),
        accountId: accountId,
        dossierStatus: dossierStatus,
        dossierId: dossierId,
        changedPhone: changedPhone,
        changedEmail: changedEmail,
        changedMobile: changedMobile
      }).setResolve(function(response) {
      self.hideSpinner(component);
      if (!response.error) {
        helper.createCaseComments(component,caseList);
      }
    })
      .setReject(function(error) {
        ntfSvc.error(ntfLib, error);
      })
      .executeAction();
  },
  updateUrl: function(component, accountId,genericRequestId, dossierId, templateId) {
    const navService = component.find("navService");
    const pageReference = {
      type: "standard__component",
      attributes: {
        componentName: "c__MRO_LCP_CustomerDataChangeWizard"
      },
      state: {
        "c__accountId": accountId,
        "c__genericRequestId": genericRequestId,
        "c__dossierId": dossierId,
        "c__templateId": templateId
      }
    };
    if(genericRequestId){
      pageReference.state["c__genericRequestId"] = genericRequestId;
    }
    navService.navigate(pageReference, true);
  },

  redirectToDossier: function(component, helper) {
    const pageReference = {
      type: "standard__recordPage",
      attributes: {
        recordId: component.get("v.dossierId"),
        objectApiName: "c__Dossier",
        actionName: "view"
      }
    };
    helper.redirect(component, pageReference);
  },
  goToAccount: function(component, event, helper) {
    const accountId = component.get("v.accountId");
    const pageReference = {
      type: "standard__recordPage",
      attributes: {
        recordId: accountId,
        objectApiName: "Account",
        actionName: "view"
      }
    };
    helper.redirect(component, pageReference);
  },
  redirect: function(component, pageReference) {
    const { closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab } = component.find("workspace");
    isConsoleNavigation()
      .then(function(response) {
        if (response === true) {
          getEnclosingTabId().then(function(enclosingTabId) {
            openSubtab({
              pageReference: pageReference,
              focus: true
            }).then(function() {
              closeTab({
                tabId: enclosingTabId
              });
            }).catch(function(error) {
            });
          });
        } else {
          const navService = component.find("navService");
          navService.navigate(pageReference);
        }
      }).catch(function(error) {
      console.log(error);
    });
  },
  closeFocusedTab: function(component, event, helper) {
    const workspaceAPI = component.find("workspace");
    workspaceAPI.getFocusedTabInfo().then(function(response) {
      let focusedTabId = response.tabId;
      workspaceAPI.closeTab({ tabId: focusedTabId });
    }).catch(function(error) {
      console.log(error);
    });
  },

  showSpinner: function(component) {
    $A.util.removeClass(component.find("spinnerSection"), "slds-hide");
  },
  hideSpinner: function(component) {
    $A.util.addClass(component.find("spinnerSection"), "slds-hide");
  },
  setChannelAndOrigin: function (component) {
    let ntfSvc = component.find('notify');
    let ntfLib = component.find('notifLib');
    //let opportunity = component.get('v.opportunity');
    let dossierId = component.get('v.dossierId');
    let origin = component.get('v.originSelected');
    let channel = component.get('v.channelSelected');
    component.find("apexService").builder()
        .setMethod("setChannelAndOrigin")
        .setInput({
          dossierId:dossierId,
          origin:origin,
          channel:channel
        })
        .setResolve(function (response) {
          if(response.error){
            ntfSvc.error(ntfLib, response.errorMsg);
            return;
          }
        })
        .setReject(function (error) {
          ntfSvc.error(ntfLib, error);
        })
        .executeAction();
  },
  setSubProcessChanges: function (component){

    let subProcessChanges=component.get("v.subProcessChanges");
    let subprocess= component.get("v.validSubProcess");
    subProcessChanges.isChangeAccountName=subprocess === component.get("v.subProcessChangeAccName");
    subProcessChanges.isChangeCompanyName=subprocess === component.get("v.subProcessChangeCompanyName");
    subProcessChanges.isChangeContactData=subprocess === component.get("v.subProcessChangeContact");
    subProcessChanges.isChangeCNP=subprocess === component.get("v.subProcessChangeCNP");
    subProcessChanges.isChangeCUI=subprocess === component.get("v.subProcessChangeCUI");
    subProcessChanges.isChangeONRC=subprocess === component.get("v.subProcessChangeONRC");
    subProcessChanges.isChangeCAEN=subprocess === component.get("v.subProcessChangeCAEN");
    subProcessChanges.isChangeOtherFields=subprocess === component.get("v.subProcessChangeOtherFields");
    subProcessChanges.isChangeResidentialAddress=subprocess === component.get("v.subProcessChangeResidentialAddress");

    //component.set("v.subProcessChanges",subProcessChanges);
    if(subprocess !== '' ){
      component.set("v.step", 3);
    }
    let accountChange = component.find("accountChange");
    accountChange.updateSubProcess();
  },

  validateSubProcessValue : function (component, showError) {
    let ntfSvc = component.find('notify');
    let ntfLib = component.find('notifLib');
    const self=this;
    let dossierId = component.get('v.dossierId');
    let subProcess = component.get('v.subProcess');
    let accountId= component.get('v.accountId');
    component.set("v.accountDataChanged", false);
    component.find("apexService").builder()
        .setMethod("validateSubProcess")
        .setInput({
          dossierId: dossierId,
          subProcess:subProcess,
          accountId : accountId
        })
        .setResolve(function (response) {
          if (response.error) {
            ntfSvc.error(ntfLib, response.errorMsg);
            return;
          }

          if(response.authorized){
            component.set("v.validSubProcess",subProcess);
            component.set("v.dossierStatus",response.dossierStatus);
            component.set("v.casePhase",response.casePhase);
            component.set("v.caseStatus",response.caseStatus);
            if(response.warnMsg){
              ntfSvc.warn(ntfLib, response.warnMsg);
            }

          } else {
            component.set("v.validSubProcess",'');
            if(showError) {
              ntfSvc.error(ntfLib, response.errorMsg);
            }

          }
          self.setSubProcessChanges(component);

        })
        .setReject(function (error) {
          ntfSvc.error(ntfLib, error);
        })
        .executeAction();
  },

  validateKey : function (component, key, callback) {
    let ntfSvc = component.find('notify');
    let ntfLib = component.find('notifLib');

    let accountId = component.get('v.accountId');
    let subProcess= component.get('v.subProcess');
    component.find("apexService").builder()
        .setMethod("validateKey")
        .setInput({
          accountId: accountId,
          subProcess: subProcess,
          key:key
        })
        .setResolve(function (response) {
          if (response.error) {
            ntfSvc.error(ntfLib, response.errorMsg);
            return;
          }
          if (!response.valid) {
            let subProcessChanges = component.get("v.subProcessChanges");
            if (!subProcessChanges.isChangeCUI){
              ntfSvc.error(ntfLib, response.errorMsg);
              return;
            }
          }

          if(callback){
            callback();
          }

        })
        .setReject(function (error) {
          ntfSvc.error(ntfLib, error);
        })
        .executeAction();
  },
  /*validateNace : function (component, naceCode, callback) {
    let ntfSvc = component.find('notify');
    let ntfLib = component.find('notifLib');

    let accountId = component.get('v.accountId');
    component.find("apexService").builder()
        .setMethod("validateNaceCode")
        .setInput({
          naceCode:naceCode
        })
        .setResolve(function (response) {
          if (response.error) {
            ntfSvc.error(ntfLib, response.errorMsg);
            return;
          }
          if (!response.valid) {
            ntfSvc.error(ntfLib, response.errorMsg);
            return;
          }
          component.set("v.naceId", response.naceId);
          if(callback){
            callback();
          }

        })
        .setReject(function (error) {
          ntfSvc.error(ntfLib, error);
        })
        .executeAction();
  },*/
  /*createPrivacyChangeRecord: function (component) {
    let privacyChangeComponent = component.find("privacyChange");
    if (privacyChangeComponent instanceof Array) {
      let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
      privacyChangeComponentToObj[0].savePrivacyChange();
    } else {
      privacyChangeComponent.savePrivacyChange();
    }
  },*/
  saveSendingChannel: function (component) {
    component.find("sendingChannelSelection").saveSendingChannel();
  },

  createCaseComments: function(component,cases){
    const self = this;
    const ntfLib = component.find('notifLib');
    const ntfSvc = component.find('notify');
    component.find('apexService').builder()
        .setMethod('createCaseComment')
        .setInput({
          "cases": JSON.stringify(cases),
          "contactPhone" : component.get("v.oldContactPhone"),
          "contactMobile" : component.get("v.oldContactMobile"),
          "contactEmail" : component.get("v.oldContactEmail"),
          "accountWebsite": component.get("v.oldAccountWebsite"),
          "accountNumbOfEmployees": component.get("v.oldAccountNumbOfEmployees"),
          "accountAnnualRevenue": component.get("v.oldAccountAnnualRevenue"),
          "accountAnnualRevenueYear": component.get("v.oldAccountAnnualRevenueYear")
        })
        .setResolve(function (response) {
          if(response.error){
            ntfSvc.error(ntfLib, response.errorMsg);
            return;
          }
          self.redirectToDossier(component, self);
        })
        .setReject(function (errorMsg) {
          ntfSvc.error(ntfLib, errorMsg);
        }).executeAction();
  },

  getOldData: function(component,objectName){
    const self = this;
    const ntfLib = component.find('notifLib');
    const ntfSvc = component.find('notify');
    let id;
    if(objectName==='Account'){
      id = component.get("v.accountId");
    }else{
      id= component.get("v.contactId");
    }
    component.find('apexService').builder()
        .setMethod('getOldData')
        .setInput({
          "objectId": id,
          "objectApiName":objectName
        })
        .setResolve(function (response) {
          if(response.error){
            ntfSvc.error(ntfLib, response.errorMsg);
            return;
          }
          if(objectName==='Contact'){
            component.set("v.oldContactPhone",response.contactPhone);
            component.set("v.oldContactMobile",response.contactMobile);
            component.set("v.oldContactEmail",response.contactEmail);
          }else{
            component.set("v.oldAccountWebsite",response.accountWebsite);
            component.set("v.oldAccountAnnualRevenue",response.accountAnnualRevenue);
            component.set("v.oldAccountAnnualRevenueYear",response.accountAnnualRevenueYear);
            component.set("v.oldAccountNumbOfEmployees",response.accountNumbOfEmployees);
          }

        })
        .setReject(function (errorMsg) {
          ntfSvc.error(ntfLib, errorMsg);
        }).executeAction();
  },


});