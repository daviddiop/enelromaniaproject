({
    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);

        helper.initialize(component, event, helper);
        helper.hideSpinner(component, 'spinnerSection');
        helper.initConsoleNavigation(component, $A.get('$Label.c.Connection'));
    },

    handleCompanyDivisionChange: function (component, event, helper) {
        try {
            if (event.getParam("divisionId")) {
                component.set("v.companyDivisionId", event.getParam("divisionId"));
                component.set("v.isCompanyDivisionEnforced", event.getParam("isCompanyDivisionEnforced"));
                helper.updateDossierCompanyDivision(component, event, helper);

            }
        } catch (err) {
            console.error('Error on aura:ConnectionWizard :', err);
        }
    },

    getBillingProfileRecordId: function (component, event, helper) {
        component.set("v.billingProfileId", event.getParam("billingProfileRecordId"));
        if (component.get("v.billingProfileId")) {

        }
    },
    handleCase: function (component, event, helper) {
        console.log('handleCase - origin: ', component.get("v.originSelected"));
        console.log('handleCase - channel: ', component.get("v.channelSelected"));
        component.set("v.showNewCase", true);
        let addr = {};
        addr = {
            'streetNumber': null,
            'streetNumberExtn': null,
            'streetName': null,
            'streetType': null,
            'apartment': null,
            'building': null,
            'city': null,
            'country': null,
            'floor': null,
            'locality': null,
            'postalCode': null,
            'province': null
        };
        component.set('v.address', addr);
    },
    closeCaseModal: function (component, event, helper) {
        component.set("v.showNewCase", false);
    },
    handleCaseDelete: function (component, event, helper) {
        helper.removeCase(component, event, helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {

        console.log('handleOriginChannelSelection - ENTER');
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set("v.originSelected", originSelected);
            component.set("v.channelSelected", channelSelected);
            // Retrieve value from component to make it available (DO NOT REMOVE CONSOLE LOG)
            console.log("Selection worked " + component.get("v.originSelected") + " " + component.get("v.channelSelected"));
            component.set("v.step", 0);
            let dossier = component.get('v.dossier');
            if (dossier && dossier.Origin__c) {
                const caseList = component.get("v.caseTile");
                if (caseList && caseList.length > 0) {
                    component.set("v.step", 2);
                }
            }
            if (component.get("v.dossierId")){
                helper.updateConnectionType(component, event, helper);
            }
            component.set("v.disableWizard", false);
        } else {
            component.set("v.disableWizard", true);
        }
        console.log('handleOriginChannelSelection - EXIT');
    },
    handleCloned: function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
        component.set("v.step", 1);
    },
    handleSuccessCase: function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
        const caseList = component.get("v.caseTile");
        if (caseList && caseList.length > 0) {
            component.set('v.disableOriginChannel',true);
        }
    },
    handleConnectionTypeChange: function (component, event, helper) {
        console.log("handleConnectionTypeChange - changed to " + event.getParam("connectionTypeId"));
        console.log('handleConnectionTypeChange - origin: ', component.get("v.originSelected"));
        console.log('handleConnectionTypeChange - channel: ', component.get("v.channelSelected"));

        const connectionType = event.getParam("connectionTypeId");
        component.set("v.connectionType", connectionType);
        if (event.getParam("isInitialValue")) {
            component.set("v.step", 1);
        }
        if (component.get("v.dossierId")){
            helper.updateConnectionType(component, event, helper);
        }
    },
    save: function (component, event, helper) {
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        if ((caseList.length === 0) || (!billingProfileId)) {
            ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
            return;
        }
        component.find('sendingChannel').saveSendingChannel();

    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep0':
            let connectionType = component.get("v.connectionType");
            let casesData =  component.get("v.caseTile");
                if (connectionType) {
                    if(casesData && casesData.length !== 0){
                        if(casesData[0] && casesData[0].SubProcess__c !== connectionType){
                            casesData[0].SubProcess__c = connectionType;
                            ntfSvc.warn(ntfLib, $A.get('$Label.c.connectionType'));
                        }
                    }
                    component.set("v.step", 1)
                } else {
                    ntfSvc.error(ntfLib, 'Select an one connection type');
                    return;
                }
                break;
            case 'confirmStep1':
                if (component.get("v.companyDivisionId")) {
                    component.set("v.step", 2)
                } else {
                    ntfSvc.error(ntfLib, 'Select a Company Division');
                    return;
                }
                break;
            case 'confirmStep2':
                if (!component.get("v.caseTile") || component.get("v.caseTile").length === 0) {
                    ntfSvc.error(ntfLib, $A.get('$Label.c.RequiredSupply'));
                    return;
                }
                helper.checkCases(
                    component,
                    function(response) {
                        helper.hideSpinner(component, 'spinnerSection');
                        if (response.error) {
                            ntfSvc.error(ntfLib, response.errorMsg);
                        } else {
                            component.set("v.step", 3);
                        }
                    },
                    function(error) {
                        ntfSvc.error(ntfLib, error);
                        helper.hideSpinner(component, 'spinnerSection');
                    }
                );
                break;
            case 'confirmStep3':
                if (!component.get("v.billingProfileId")) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.BillingProfileRequired")); //$A.get('$Label.c.BillingProfileRequired')
                    return;
                }
                component.find('sendingChannel').reloadAddressList();

                component.set("v.step", 4);
                break;
            case 'confirmStep4':
                component.find('sendingChannel').saveSendingChannel();
                break;
            case 'confirmStep5':
                if (component.get('v.representativeContactId')) {
                    component.set("v.step", 6);
                } else {
                    component.set("v.step", 5);
                }
                break;
            default:
                break;
        }
    },
    previousStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep0':
                component.set("v.step", 0);
                break;
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            case 'returnStep2':
                component.set("v.step", 2);
                break;
            case 'returnStep3':
                component.set("v.step", 3);
                break;
            case 'returnStep4':
                component.set("v.step", 4);
                break;
            case 'returnStep5':
                component.set("v.step", 5);
                break;
            default:
                break;
        }
    },

    cancel: function (component, event, helper) {
        component.set("v.showCancelBox", true);
    },
    onCloseCancelBox: function (component) {
        component.set("v.showCancelBox", false);
    },
    onSaveCancelReason: function (component, event, helper) {
        helper.redirectToDossier(component, helper);
    },
    saveDraft: function (component, event, helper) {
        helper.saveDraftBillingProfile(component, helper);
    },
    handleSuccessDossier: function (component, event, helper) {
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        if (selectedChannel) {
            if(component.get('v.representativeContactId')){
                helper.createPrivacyChangeRecord(component);
            }
            else {
                helper.updateCase(component,helper);
            }
        }
    },
    handleSuccessContact: function (component, event, helper) {
        let contactRecord = event.getParam('contactRecord');
        if (contactRecord) {
            localStorage.clear();
            localStorage.setItem(component.get('v.dossierId'), JSON.stringify(event.getParam('contactRecord')));
            component.set('v.representativeContactId', contactRecord.Id);
        } else {
            localStorage.clear();
            component.set('v.representativeContactId', null);
        }
    },
    getPrivacyId: function (component, event, helper) {
        const ntfLib = component.find("notifLib");
        const ntfSvc = component.find("notify");
        let doNotCreatePrivacyChange = event.getParam("doNotCreatePrivacyChange");
        if (!doNotCreatePrivacyChange) {
            component.set("v.privacyChangeId", event.getParam("privacyChangeId"));
            component.set("v.dontProcess", event.getParam("dontProcess"));
        }
        helper.updateCase(component, helper);
    },

    handleBlockingErrors: function(component, event, helper) {

        component.set('v.isClosed', true);
        component.set('v.hasBlockingErrors', true);
    },
})