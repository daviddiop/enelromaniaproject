({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const genericRequestId = myPageRef.state.c__genericRequestId;
        component.set("v.accountId", accountId);

        console.log('InteractionId', component.find('cookieSvc').getInteractionId());

        component.find('apexService').builder()
            .setMethod("initialize")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "genericRequestId": genericRequestId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    component.set("v.caseTile", response.caseTile);
                    component.set("v.account", response.account);
                    component.set("v.accountId", response.accountId);
                    component.set("v.billingProfileId", response.billingProfileId);
                    component.set("v.dossierId", response.dossierId);
                    component.set("v.dossier", response.dossier);
                    component.set('v.companyDivisionId', response.dossier.CompanyDivision__c);
                    component.set('v.originSelected', response.dossier.Origin__c);
                    component.set('v.channelSelected', response.dossier.Channel__c);
                    component.set("v.contactRecord", response.contactRecord);
                    if(component.get("v.contactRecord")){
                        component.set("v.contactId",component.get("v.contactRecord").Id)
                    }
                    let step = 0;
                    if(localStorage.getItem(dossierId)  !== null){
                        component.set("v.contactRecord",JSON.parse(localStorage.getItem(dossierId)));
                        component.set("v.representativeContactId",component.get("v.contactRecord").Id)
                    }
                    if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                        self.updateUrl(component, accountId, response.genericRequestId, response.dossierId);
                    }
                    let sendingChannel = component.find('sendingChannel');
                    sendingChannel.disableInputField(true);
                    let dossierVal = component.get("v.dossier");

                    /*if(response.account){
                        if((response.account.IsPersonAccount && !response.account.PersonMobilePhone && !response.account.Phone) ||
                            (!response.account.IsPersonAccount &&
                                (!response.account.PrimaryContact__r || !response.account.PrimaryContact__r.Phone && !response.account.PrimaryContact__r.MobilePhone))){
                            ntfSvc.warn(ntfLib, $A.get("$Label.c.ProcessRequeresPhoneMobile"));
                        }
                    }*/


                    if (!component.get("v.companyDivisionId")) {
                        if (!component.get("v.connectionType")) {
                            step = 0;
                            return;
                        }
                        step = 1;
                    }
                    if (response.billingProfileId) {
                        step = 4;
                        sendingChannel.disableInputField(false);
                    } else if (response.caseTile) {
                        if (response.caseTile.length !== 0) {
                            step = 2;
                            component.set("v.disableWizard", false);
                            component.set('v.disableOriginChannel', true);
                        }
                    }

                    if (dossierVal) {
                        if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                            component.set("v.isClosed", true);
                        }
                    }
                    if (dossierVal["SubProcess__c"]) {
                        component.set("v.connectionType", dossierVal.SubProcess__c);
                    } else {
                        step = 0;
                        component.set('v.disableOriginChannel', false);
                    }

                    if (response.caseTile) {
                        component.set("v.osiTableView", response.caseTile.length > component.get("v.tileNumber"));
                    }
                    if (component.find("billingProfileSelection")) {
                        component.find("billingProfileSelection").reset(component.get("v.accountId"));
                    }
                    component.set("v.step", step);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();

    },
    saveDraftBillingProfile: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const billingProfileId = component.get("v.billingProfileId");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
         let connectionType = component.get("v.connectionType");
          if(!connectionType){
             ntfSvc.error(ntfLib, $A.get("$Label.c.SubProcessCheck"));
             self.hideSpinner(component, 'spinnerSection');
             return;
         }

        component.find('apexService').builder()
            .setMethod("saveDraftBP")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                billingProfileId: billingProfileId,
                connectionType: connectionType
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateDossier: function (component, event, helper) {
        const self = this;
        let dossierId = component.get('v.dossierId');
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');

        component.find('apexService').builder()
            .setMethod("UpdateDossier")
            .setInput({
                dossierId: dossierId,
                channelSelected: channelSelected,
                originSelected: originSelected
            }).setResolve(function (response) {
            if (!response.error) {
                self.initialize(component, event, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    updateCase: function (component, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const caseList = component.get("v.caseTile");
        const ntfLib = component.find('notifLib');
         const ntfSvc = component.find('notify');
        const dossierId = component.get("v.dossierId");
        const billingProfileId = component.get("v.billingProfileId");
        const contactId = component.get("v.representativeContactId");
        let connectionType = component.get("v.connectionType");
        const accountId = component.get("v.accountId");
        if(!connectionType){
            ntfSvc.error(ntfLib, $A.get("$Label.c.SubProcessCheck"));
            self.hideSpinner(component, 'spinnerSection');
            return;
        }

        component.find('apexService').builder()
            .setMethod("updateCaseList")
            .setInput({
                oldCaseList: JSON.stringify(caseList),
                dossierId: dossierId,
                billingProfileId: billingProfileId,
                contactId : contactId,
                accountId : accountId,
                connectionType: connectionType
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    if(response.phoneError){
                        ntfSvc.warn(ntfLib, response.phoneError);
                        setTimeout(function(){
                            self.redirectToDossier(component, helper);
                        }, 2000);
                    }else {
                        self.redirectToDossier(component, helper);
                    }
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateConnectionType: function (component, event, helper) {
        const self = this;
        const dossierId = component.get("v.dossierId");
        let connectionType = component.get('v.connectionType');
        let channelSelected = component.get('v.channelSelected');
        let originSelected = component.get('v.originSelected');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod("updateDossierConnectionTypeAndChannelOrigin")
            .setInput({
                dossierId: dossierId,
                connectionType: connectionType,
                channelSelected: channelSelected,
                originSelected: originSelected
            })
            .setResolve(function () {
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    updateDossierCompanyDivision: function (component, event, helper) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const dossierId = component.get("v.dossierId");
        let companyDivisionId = component.get('v.companyDivisionId');
        component.find('apexService').builder()
            .setMethod("updateDossierCompanyDivision")
            .setInput({
                dossierId: dossierId,
                companyDivisionId: companyDivisionId
            })
            .setResolve(function (response) {
                self.hideSpinner(component, 'spinnerSection');
                if (!response.error) {
                    component.set("v.step", 2);
                } else {
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_ConnectionWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference, true);
    },
    removeCase: function (component, event, helper) {
        const deleteRecordId = event.getParam("deleteRecord");
        const caseList = component.get("v.caseTile");
        const items = [];
        for (let i = 0; i < caseList.length; i++) {
            if (caseList[i].Id !== deleteRecordId) {
                items.push(caseList[i]);
            }
        }

        if (items.length === 0) {
            component.set("v.step", 1);
            component.set('v.disableOriginChannel',false);
        }

        component.set("v.caseTile", items);
        component.set("v.osiTableView", items.length > component.get("v.tileNumber"));
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const navService = component.find("navService")
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    redirect: function (component, pageReference) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (errorMsg) {
                            ntfSvc.error(ntfLib, errorMsg);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            ntfSvc.error(ntfLib, error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const workspaceAPI = component.find("workspace");
        if (workspaceAPI) {
            workspaceAPI.isConsoleNavigation()
                .then(function (response) {
                    if (response === true) {
                        workspaceAPI.getFocusedTabInfo()
                            .then(function (response) {
                                let focusedTabId = response.tabId;
                                let isSubTab = response.isSubtab;
                                if (isSubTab) {
                                    workspaceAPI.setTabLabel({
                                        tabId: focusedTabId,
                                        label: wizardLabel
                                    });
                                    workspaceAPI.setTabIcon({
                                        tabId: focusedTabId,
                                        icon: "utility:case",
                                        iconAlt: wizardLabel
                                    });
                                }
                            });
                    }
                }).catch(function (error) {
                console.log(error);
            });
            document.title = $A.get("$Label.c.LightningExperienceSalesforce");
        }
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    },
    resetSupplyForm: function (component) {
        let supplySearchComponent = component.find("supplySelection");
        if (supplySearchComponent instanceof Array) {
            let supplyComponentToObj = Object.assign({}, supplySearchComponent);
            supplyComponentToObj[0].resetForm();
        } else {
            supplySearchComponent.resetForm();
        }
    },
    checkCases: function(component, resolveCallback, rejectCallback) {
        const self = this;
        self.showSpinner(component, 'spinnerSection');
        component.find('apexService').builder()
            .setMethod("checkCases")
            .setInput({
                dossierId: component.get("v.dossierId")
            })
            .setResolve(resolveCallback)
            .setReject(rejectCallback)
            .executeAction();
    },
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    },
    showSpinner: function (component, idSpinner) {
        $A.util.removeClass(component.find(idSpinner), 'slds-hide');
    },
    hideSpinner: function (component, idSpinner) {
        $A.util.addClass(component.find(idSpinner), 'slds-hide');
    }
})