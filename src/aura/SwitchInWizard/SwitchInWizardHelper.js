({
    initialize: function (component) {
        var self = this;
        var myPageRef = component.get("v.pageReference");
        var accountId = myPageRef.state.c__accountId;
        var opportunityId = myPageRef.state.c__opportunityId;
        var ntfLib = component.find('notifLib');
        var ntfSvc = component.find('notify');
        var companyDivComponent = component.find("companyDivision");
        component.set("v.companyDivisionId", "");
        component.set("v.contractId", "");
        component.set("v.accountId", accountId);
        component.find('apexService').builder()
            .setMethod('initialize')
            .setInput({
                "accountId": accountId,
                "opportunityId": opportunityId,
                "interactionId": component.find('cookieSvc').getInteractionId()
            })
            .setResolve(function (response) {
                if (response.opportunityId && !opportunityId) {
                    // reload the page with opportunityId param and reRun init method
                    self.updateUrl(component, accountId, response.opportunityId);
                    return;
                }
                component.set("v.opportunityId", response.opportunityId);
                component.set("v.opportunityLineItems", response.opportunityLineItems);
                component.set("v.opportunityServiceItems", response.opportunityServiceItems);
                component.set("v.contractAccountId", response.contractAccountId);
                component.set("v.contractId", response.contractIdFromOpp);
                component.set("v.customerSignedDate", response.customerSignedDate);
                component.set("v.osiTableView", response.opportunityServiceItems.length > component.get("v.tileNumber"));

                if (response.opportunityCompanyDivisionId) {
                    component.set("v.companyDivisionId", response.opportunityCompanyDivisionId);
                }
                if (companyDivComponent) {
                    companyDivComponent.setCompanyDivision();
                }


                let step = 0;
                if ((response.opportunityLineItems.length !== 0) && (response.contractAccountId) && (response.opportunityServiceItems.length !== 0)) {
                    step = 4;
                } else if ((response.contractAccountId) && (response.opportunityServiceItems.length !== 0)) {
                    step = 4;
                } else if (response.opportunityServiceItems.length !== 0) {
                    step = 2;
                } else if ((component.get("v.companyDivisionId") && response.opportunityServiceItems.length !== 0) ||
                    component.get("v.isCompanyDivisionEnforced")) {
                    step = 1;
                }

                let opportunityVal = response.opportunity;
                if (opportunityVal) {
                    if (opportunityVal.StageName === 'Closed Lost' || opportunityVal.StageName === 'Closed Won') {
                        component.set("v.isClosed", true);
                    }
                }

                component.set("v.step", step);
                component.set("v.accountId", response.accountId);


                if (component.find("contractAccountSelectionComponent")) {
                    self.reloadContractAccount(component);
                }
                let topElement = component.find("topElement").getElement();
                if (topElement) {
                    topElement.scrollIntoView({behavior: "instant", block: "end"});
                }
                self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    saveOpportunity: function (component, stage, callback) {
        let self = this;
        self.showSpinner(component);
        let opportunityId = component.get("v.opportunityId");
        let privacyChangeId = component.get("v.privacyChangeId");
        let contractId = component.get("v.contractId");
        let ntfLib = component.find('notifLib');
        let ntfSvc = component.find('notify');
        component.find('apexService').builder()
            .setMethod('updateOpportunity')
            .setInput({
                "opportunityId": opportunityId,
                "privacyChangeId": privacyChangeId,
                "contractId": contractId,
                "stage": stage
            })
            .setResolve(function (response) {
                self.hideSpinner(component);

                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateOsi: function (component, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let contractAccountId = component.get("v.contractAccountId");
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod('updateOsiList')
            .setInput({
                "opportunityServiceItems": osiList,
                "contractAccountId": contractAccountId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, opportunityId) {
        let self = this;
        const {isConsoleNavigation} = component.find("workspace");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__SwitchInWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__opportunityId": opportunityId
            }
        };
        self.redirect(component, pageReference, true);
    },/*
    setContract: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            let contractId = '';
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractId = contractSelectionComponentToObj[0].getSelectedContract();
            } else {
                contractId = contractSelectionComponent.getSelectedContract();
            }
            component.set("v.contractId", contractId);
        }
    },*/
    openProductSelection: function (component, helper) {
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__ProductSelectionWrp',
            },
            state: {
                "c__opportunityId": component.get("v.opportunityId")
            }
        };
        helper.redirect(component, pageReference,false);
    },
    redirectToOppty: function (component, helper) {
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.opportunityId"),
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference,false);
    },
    validateOsi: function (component, osiId) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        var self = this;
        component.find('apexService').builder()
            .setMethod("checkOsi")
            .setInput({
                "osiId": osiId
            })
            .setResolve(function (response) {
                let osiList = component.get("v.opportunityServiceItems") || [];
                let newOsi = response.opportunityServiceItem;
                osiList.push(newOsi);
                if (osiList.length == 1) {
                    self.updateCompanyDivisionOnOpportunity(component);
                }
                component.set("v.step", 1);
                component.set("v.opportunityServiceItems", osiList);
                component.set("v.osiTableView", osiList.length > component.get("v.tileNumber"));
                component.set("v.showNewOsi", false);
                component.set("v.searchedPointCode", "");
                component.find("pointSelection").resetBox();
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction();
    },
    linkOliToOsi: function (component, oli, callback) {
        let self = this;
        self.showSpinner(component);
        let osiList = component.get("v.opportunityServiceItems");
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');

        component.find('apexService').builder()
            .setMethod("linkOliToOsi")
            .setInput({
                "opportunityServiceItems": osiList,
                "oli": oli
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                if (callback && typeof callback === "function") {
                    callback(component, self);
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
            })
            .executeAction()
    },/*
    goToAccount: function (component, event, helper) {
        let accountId = component.get("v.accountId");
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        navService.navigate(pageReference, true);
    },*/
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /Start
     * @author Baba Goudiaby
     * ****************************************************
     */
    redirect: function (component, pageReference, overrideUrl) {
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    });
                } else {
                    let navService = component.find("navService");
                    navService.navigate(pageReference,overrideUrl);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    initConsoleNavigation: function (component, wizardLabel) {
        let self = this;
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: wizardLabel
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: wizardLabel
                                });
                            }
                        });
                    /*if (window.localStorage && component.get("v.step") === 3 ) {
                        if (!window.localStorage['loaded']) {
                            window.localStorage['loaded'] = true;
                            helper.refreshFocusedTab(component);
                        } else {
                            window.localStorage.removeItem('loaded');
                        }
                    }*/
                    document.title = $A.get("$Label.c.LightningExperienceSalesforce");
                    if (!window.location.hash && component.get("v.step") === 3) {
                        window.location = window.location + '#loaded';
                        self.refreshFocusedTab(component);
                    }
                }
            }).catch(function (error) {
            console.log(error);
        });
        document.title = $A.get("$Label.c.LightningExperienceSalesforce")
    },
    closeFocusedTab: function (component, event, helper) {
        const {closeTab, getFocusedTabInfo} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    refreshFocusedTab: function (component) {
        const {getFocusedTabInfo, refreshTab} = component.find("workspace");
        getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            refreshTab({
                tabId: focusedTabId
            });
        }).catch(function (error) {
        });
    }
    /**\
     * ****************************************************
     * Console Navigation and Redirection Utility  /End
     * @author Baba Goudiaby
     * ****************************************************
     */,
    createPrivacyChangeRecord: function (component) {
        let privacyChangeComponent = component.find("privacyChange");
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    },
    reloadContractAccount: function (component) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        let accountId = component.get("v.accountId");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accountId);
            } else {
                contractAccountComponent.reset(accountId);
            }
        }
    },
    reloadContractAddition: function (component) {
        let contractSelectionComponent = component.find("contractSelectionComponent");
        if (contractSelectionComponent) {
            if (contractSelectionComponent instanceof Array) {
                let contractSelectionComponentToObj = Object.assign({}, contractSelectionComponent);
                contractSelectionComponentToObj[0].reloadContractList();
            } else {
                contractSelectionComponent.reloadContractList();
            }
        }
    },
    reloadCompanyDivision: function (component) {
        let companyDivisionComponent = component.find("companyDivision");
        if (companyDivisionComponent) {
            if (companyDivisionComponent instanceof Array) {
                let companyDivisionComponentToObj = Object.assign({}, companyDivisionComponent);
                companyDivisionComponentToObj[0].reload();
            } else {
                companyDivisionComponent.reload();
            }
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.addClass(component.find('spinnerSection'), 'slds-show');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
        $A.util.removeClass(component.find('spinnerSection'), 'slds-show');
    },
    updateCompanyDivisionOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        component
            .find("apexService")
            .builder()
            .setMethod("updateCompanyDivisionInOpportunity")
            .setInput({
                opportunityId: opportunityId,
                companyDivisionId: component.get("v.companyDivisionId")
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    updateContractAndContractSignedDateOnOpportunity: function (component) {
        let ntfSvc = component.find('notify');
        let ntfLib = component.find('notifLib');
        let opportunityId = component.get("v.opportunityId");
        let contractId = component.get("v.contractId") === 'new' ? '' : component.get("v.contractId");
        let customerSignedDate = component.get("v.customerSignedDate");
        component
            .find("apexService")
            .builder()
            .setMethod("updateContractAndContractSignedDateOnOpportunity")
            .setInput({
                opportunityId: opportunityId,
                contractId: contractId,
                customerSignedDate: customerSignedDate
            })
            .setResolve(function (response) {
                //self.hideSpinner(component);
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    }
})