({
    onInit: function (component, event, helper) {
        let thisPageReference = component.get("v.pageReference");
        let interactionId = component.get("v.recordId") || thisPageReference.state.c__id;
        let dossierId = thisPageReference.state.c__dossierId;
        if(dossierId != null) {
            component.set('v.genericRequestDossierId', dossierId);
            component.set('v.step', 1);
        }
        if (interactionId) {
            helper.findById(component, interactionId, function (interaction) {
                let workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function (response) {
                    let focusedTabId = response.tabId;
                    workspaceAPI.setTabLabel({
                        tabId: focusedTabId,
                        label: interaction.name
                    });
                });
                component.set('v.interaction', interaction);
                if (interaction.origin) {
                    component.set('v.isEditOriginChannel', true);
                    component.set('v.interactionOrigin', interaction.origin);
                    component.set('v.interactionChannel', interaction.channel);
                    helper.isGenericRequest(component, interaction.origin);
                }
                if(interaction.interlocutorType) {
                    component.set('v.interlocutorType', interaction.interlocutorType);
                    helper.applyInterlocutorTypeConstratints(component, interaction.interlocutorType);
                }
                if(interaction.institutionId) {
                    component.set('v.institutionId', interaction.institutionId);
                }
                component.set('v.isPumaInstitution', interaction.institutionName === 'PUMA');

                let disableOriginChannel = !(interaction.origin && interaction.channel);
                helper.disableCardWizard(component, disableOriginChannel);
                if (!interaction.isClosed) {
                    component.find('cookieSvc').setInteractionId(interaction.id);
                } else {
                    component.find('cookieSvc').clearInteractionId();
                }
                helper.hideSpinner(component);
                helper.getGenericRequestInformation(component, helper, dossierId);
            });
        } else {
            helper.insertInteraction(component);
            helper.disableCardWizard(component, true);
        }
    },
    onPageReferenceChanged: function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    onInterlocutorSelect: function (component, event, helper) {
        let interlocutor = event.getParam('individual');
        let interaction = component.get('v.interaction');
        interaction.interlocutorId = interlocutor.Id;
        helper.upsertInteraction(component, interaction, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            if (updatedInteraction.customerInteractionIds && updatedInteraction.customerInteractionIds.length) {
                helper.closeModal(component);
            } else {
                component.set('v.currentComponent', 'individualRelations');
            }
        });
    },
    onInterlocutorSearch: function (component, event, helper) {
        component.set('v.interlocutorFields', event.getParam('searchFields'));
        component.set('v.interlocutorList', event.getParam('individualList'));
        component.set('v.currentComponent', 'individualList');
    },
    interlocutorRemoveHandler: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        if (interaction && !interaction.isClosed) {
            let thisPageReference = component.get("v.pageReference");
            let interactionId = component.get("v.recordId") || thisPageReference.state.c__id;
            helper.removeInterlocutor(component, interactionId, function (interaction) {
                component.set('v.interaction', interaction);
                component.set('v.institutionId', null);
                component.set('v.currentComponent', 'individualSearch');
                helper.closeModal(component);
            });
        }
    },
    searchHandler: function (component, event, helper) {
        console.log('searchHandler' + JSON.stringify(event.getParam('accountList')));
        console.log('searchHandler searchType' + JSON.stringify(event.getParam('searchType')));
        component.set('v.customerFields', event.getParam('account'));
        component.set('v.customerList', event.getParam('accountList'));
        if(event.getParam('searchType') == 'Institution'){
            component.set('v.blockCustomerInteractionSaveOnAccountSearch', true);
        }
        console.log('blockCustomerInteractionSaveOnAccountSearch : ' + component.get('v.blockCustomerInteractionSaveOnAccountSearch'));
        component.set('v.currentComponent', 'accountList');
        console.log('searchHandler--');
    },
    handleAccountSelection: function (component, event, helper) {
        let accountId = event.getParam('accountId');
        let customerInteractionId = event.getParam('customerInteractionId');
        let interlocutorId = event.getParam('interlocutorId');
        let institutionId = component.get('v.institutionId');
        let interlocutorType = component.get('v.interlocutorType');

        component.set('v.customerInteractionId', customerInteractionId);
        if(customerInteractionId) {
            component.find('preliminaryDataCheck').doPreliminaryDataCheck();
        }

        if (accountId) {
            if (interlocutorId || institutionId || interlocutorType == 'Anonymous') {
                let interaction = component.get('v.interaction');
                helper.findById(component, interaction.id, function (updatedInteraction) {
                    component.set('v.interaction', updatedInteraction);
                    helper.closeModal(component);
                });
            } else {
                if(customerInteractionId) {
                    component.set('v.data', {
                        'customerId': accountId,
                        'customerInteractionId': customerInteractionId
                    });
                    component.set('v.currentComponent', 'accountRelations');
                } else {
                    helper.saveInteractionInstitution(accountId, component, helper);
                    helper.closeModal(component);
                }
            }
        }
    },
    onRelationForIndividualSelect: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        helper.findById(component, interaction.id, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.set('v.currentComponent', 'individualSearch');
            helper.closeModal(component);
        });
    },
    onRelationForAccountSelect: function (component, event, helper) {
        component.set('v.customerFields', null);
        helper.refreshAccountForm(component, helper);
    },
    refreshAndCloseModal: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        helper.findById(component, interaction.id, function (interaction) {
            component.set('v.interaction', interaction);
            helper.closeModal(component);
        });
    },
    refresh: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        helper.findById(component, interaction.id);
    },
    goToNewAccount: function (component, event, helper) {
        component.set('v.currentComponent', 'accountEdit');
    },
    onEditComment: function (component, event, helper) {
        component.set('v.isEdit', true);
    },
    onSaveComment: function (component, event, helper) {
        component.find('interactionEditForm').submit();
    },
    /**
     * Handle Event from Origin Channel Component
     *
     * @author BG
     * @description [ENLCRO-325] Channel and Origin Selection - Implementation
     * @param component
     * @param event
     * @param helper
     */
    onSelectionOriginChannel: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        console.log('onSelectionOriginChannel selectedOrigin: ' + originSelected + ' channelSelected: '+ channelSelected);
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);

            helper.disableCardWizard(component, false);
            if (!component.get('v.isEditOriginChannel')) {
                helper.saveInteraction(component, event);
            }
            component.set('v.isEditOriginChannel', false);
            helper.isGenericRequest(component, originSelected);
        } else {
            helper.disableCardWizard(component, true);
        }
    },
    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep0':
                if(helper.validateFields(component)){
                    helper.createGenericRequestDossier(component);
                    component.set('v.step', 1);
                }
                break;
            case 'confirmStep1':
                component.set('v.step', 2);
                helper.enableSendingChannelSelection(component, helper);
                break;
            default:
                break;
        }
        console.log("Step : " + component.get('v.step'));
    },
    previousStep: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'returnStep0':
                component.set("v.step", 0);
                break;
            case 'returnStep1':
                component.set("v.step", 1);
                break;
            default:
                break;
        }
    },
    handleSavedSendingChannel: function (component, event, helper) {
        let sendingChannel = event.getParam('sendingChannel');
        console.log("Sending Channel Saved " + sendingChannel);
        helper.updateGenericRequestDossier(component, helper, component.get("v.selectedQueue"));
    },
    saveGenericRequestDossier: function (component, event, helper) {
        var buttonPressed = event.getSource().getLocalId();
        switch (buttonPressed) {
            case 'assignCustomerCare':
                console.log("Assign to Customer Care");
                component.set("v.selectedQueue", 'CustomerCare');
                break;
            case 'assignBackOffice':
                console.log("Assign to Back Office");
                component.set("v.selectedQueue", 'BackOffice');
                break;
            default:
                break;
        }
        helper.saveSendingChannelSelection(component, helper);
    },
    onCancelCommentEdit: function (component, event, helper) {
        component.set('v.isEdit', false);
        let interaction = component.get('v.interaction');
        component.set('v.interaction', interaction);
    },
    onCloseInteraction: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        interaction.status = 'Closed';
        helper.upsertInteraction(component, interaction, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.find('cookieSvc').clearInteractionId();
            helper.goToInteractionView(component);
        });
    },
    onCancelInteraction: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        interaction.status = 'Canceled';
        helper.upsertInteraction(component, interaction, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.find('cookieSvc').clearInteractionId();
            helper.goToInteractionView(component);
        });
    },
    handleSuccess: function (component, event, helper) {
        let payload = event.detail;
        component.set('v.isEdit', false);
    },
    handleError: function (component, event) {
        let errors = event.getParams();
        let ntfSvc = component.find('ntfSvc');
        let ntfLib = component.find('ntfLib');
        ntfSvc.error(ntfLib, errors);
    },
    upgradeInterlocutor: function (component, event, helper) {
        component.set('v.currentComponent', 'individualEdit');
        let interaction = component.get('v.interaction');
        let searchFields = {
            FirstName: interaction.interlocutorFirstname,
            LastName: interaction.interlocutorLastname,
            NationalIdentityNumber: interaction.interlocutorNationalId,
            Email: interaction.interlocutorEmail,
            Phone: interaction.interlocutorPhone
        };
        component.set('v.interlocutorFields', searchFields);
        component.set('v.individualEditBtnHide', true);
        helper.openModal(component);
    },
    onInterlocutorTypeChange: function (component, event, helper) {
        helper.applyInterlocutorTypeConstratints(component, component.get('v.interlocutorType'));

        let interaction = component.get('v.interaction');
        if (interaction && !interaction.isClosed) {
            let thisPageReference = component.get("v.pageReference");
            let interactionId = component.get("v.recordId") || thisPageReference.state.c__id;
            helper.removeInterlocutor(component, interactionId, function (interaction) {
                component.set('v.interaction', interaction);
                component.set('v.institutionId', null);
                helper.setInterlocutorSearchType(component, component.get('v.interlocutorType'));
                helper.closeModal(component);
            });
            helper.saveInterlocutorType(component, helper);
        }
    },
    openInterlocutorModal: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        if (interaction && !interaction.isClosed) {
            helper.setInterlocutorSearchType(component, component.get('v.interlocutorType'));
            helper.switchOffIndividualBackBtn(component);
            helper.openModal(component);
        }
    },
    openAccountModal: function (component, event, helper) {
        console.log('openAcc Modal');
        event.stopPropagation();
        let interaction = component.get('v.interaction');
        if (interaction && !interaction.isClosed) {
            component.set("v.accountSearchType", "Customer");
            component.set('v.blockCustomerInteractionSaveOnAccountSearch', false);
            component.set('v.currentComponent', 'accountSearch');
            helper.switchOffIndividualBackBtn(component);
            helper.openModal(component);
        }
    },
    redirectHandler: function (component, event, helper) {
        let page = event.getParam('page');
        if (page) {
            component.set('v.currentComponent', page);
        }
    },
    close: function (component, event, helper) {
        let interaction = component.get('v.interaction');
        helper.findById(component, interaction.id);
        helper.closeModal(component);
    }
})