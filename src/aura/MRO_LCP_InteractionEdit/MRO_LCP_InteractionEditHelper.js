({
    getIndexInList: function (accountList, customerInteraction) {
        return accountList.findIndex(function (account) {
            if (account.id === customerInteraction.id) {
                return account;
            }
        });
    },
    insertInteraction: function (component, callback) {
        let self = this;
        component.find('apexService').builder()
            .setMethod('insertInteraction')
            .setResolve(function (interaction) {
                if (interaction) {
                    if (callback) {
                        callback(interaction);
                    } else {
                        component.set('v.interaction', interaction);
                        component.set('v.recordId', interaction.id);
                        if (interaction.id) {
                            component.find('cookieSvc').setInteractionId(interaction.id);
                            self.updateUrl(component, interaction.id);
                        }
                    }
                }
            })
            .executeAction();
    },
    upsertInteraction: function (component, interaction, callback) {
        component.find('apexService').builder()
            .setMethod('upsertInteraction')
            .setInput(interaction)
            .setResolve(function (updatedInteraction) {
                if (callback) {
                    callback(updatedInteraction);
                } else {
                    component.set('v.interaction', updatedInteraction);
                }
            })
            .executeAction();
    },
    saveInteractionInstitution: function (accountId, component, helper) {
        let interaction = component.get('v.interaction');
        console.log('saveInteractionInstitution accountId: ' + accountId + ' interactionID: ' + interaction.id);
        component.find('apexService').builder()
            .setMethod('saveInstitutionInterlocutor')
            .setInput({
                'interactionId': interaction.id,
                'institutionAccountId': accountId
            })
            .setResolve(function (response) {
                component.set('v.institutionId', response.institutionId);
                console.log("institutionId " + response.institutionId);
                component.set('v.isPumaInstitution', response.institution && response.institution.Name === 'PUMA');
            })
            .setReject(function (error) {
                let ntfSvc = component.find('ntfSvc');
                let ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    removeInterlocutor: function (component, interactionId, callback) {
        component.find('apexService').builder()
            .setMethod('removeInterlocutor')
            .setInput({
                'interactionId': interactionId
            })
            .setResolve(function (interaction) {
                if (callback) {
                    callback(interaction);
                } else {
                    component.set('v.interaction', interaction);
                }
            })
            .setReject(function (error) {
                let ntfSvc = component.find('ntfSvc');
                let ntfLib = component.find('ntfLib');
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },
    findById: function (component, interactionId, callback) {
        component.find('apexService').builder()
            .setMethod('findById')
            .setInput({
                'interactionId': interactionId
            })
            .setResolve(function (interaction) {
                if (interaction) {
                    if (callback) {
                        console.log('interaction on callback findBy*** ', JSON.stringify(interaction));
                        callback(interaction);
                    } else {
                        component.set('v.interaction', interaction);
                    }
                }
            })
            .executeAction();
    },
    refreshAccountForm: function (component, helper) {
        let interaction = component.get('v.interaction');
        this.findById(component, interaction.id, function (updatedInteraction) {
            component.set('v.interaction', updatedInteraction);
            component.set('v.rendered', false);
        });
    },
    getGenericRequestInformation: function (component, helper, dossierId) {
        console.log("getGenericRequestInformation + ")
        let self = this;
        const ntfLib = component.find('ntfLib');
        const ntfSvc = component.find('ntfSvc');
        let interactionId = component.get('v.interaction').id;
        self.showSpinner(component);
        component.find('apexService').builder()
            .setMethod('getGenericRequestInformation')
            .setInput({
                'dossierId': interactionId,
                'interactionId': interactionId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.genericRequestRecordTypeId', response.genericRequestRecordTypeId);
                component.set('v.hasKAM', response.hasKAM);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    createGenericRequestDossier: function (component, origin) {
        console.log("createGenericRequestDossier + ")
        let self = this;
        const ntfLib = component.find('ntfLib');
        const ntfSvc = component.find('ntfSvc');
        self.showSpinner(component);
        let interactionId = component.get('v.interaction').id;
        component.find('apexService').builder()
            .setMethod('createGenericRequestDossier')
            .setInput({
                'interactionId': interactionId
            })
            .setResolve(function (response) {
                self.hideSpinner(component);
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                component.set('v.genericRequestDossierId', response.genericRequestDossierId);
                self.updateUrlWithDossierId(component, interactionId, response.genericRequestDossierId);
            })
            .setReject(function (response) {
                self.hideSpinner(component);
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    updateGenericRequestDossier: function (component, helper, queue) {
        let self = this;
        const ntfLib = component.find('ntfLib');
        const ntfSvc = component.find('ntfSvc');
        let dossierId = component.get('v.genericRequestDossierId');
        let commentsField = component.find('Comments__c').get("v.value");
        let SLAExpirationDateCmp = component.find('SLAExpirationDate__c');
        let assignmentProvince = component.find('AssignmentProvince__c').get("v.value");

        let input = {
            'dossierId': dossierId,
            'comments': commentsField,
            'assignmentProvince': assignmentProvince,
            'queue' : queue
        };

        if(SLAExpirationDateCmp != null){
            let SLAExpirationDate = SLAExpirationDateCmp.get("v.value");
            if (new Date(SLAExpirationDate).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)) {
                ntfSvc.error(ntfLib, $A.get("$Label.c.SLAExpirationDateCannotBeInPast"));
                return;
            }

            input.SLAExpirationDate = SLAExpirationDate;
        }

        if(component.get('v.isPumaInstitution')){
            input.fileNumber = component.find('FileNumber__c').get("v.value");
            input.enelRegistrationDateAndNumber = component.find('EnelRegistrationDateAndNumber__c').get("v.value");
            input.pumaUrgency = component.find('PumaUrgency__c').get("v.value");
        }
        component.find('apexService').builder()
            .setMethod('updateGenericRequestDossier')
            .setInput(input)
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
                self.goToDossierView(component, response.genericRequestDossierId);
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    isGenericRequest: function (component, origin) {
        if(origin == "Fax" ||origin == "Email" ||origin == "Mail"){
            component.set('v.isGenericRequest', true);
            $A.util.removeClass(component.find('customerSession'), 'customer-list');
            $A.util.addClass(component.find('customerSession'), 'slds-theme_default');

        }
        else {
            component.set('v.isGenericRequest', false);
            $A.util.addClass(component.find('customerSession'), component.get('v.classCustomerCard'));
        }
    },
    validateFields: function (component) {
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let customerInteractionList =  component.find('customerInteractionList').getElement();
        if(customerInteractionList && customerInteractionList.isNotEmpty) {
            return true;
        }
        ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
        return false;
    },
    enableSendingChannelSelection: function (component, helper) {
        let sendingChannelSelectionCmp = component.find('sendingChannelSelection');
        if(sendingChannelSelectionCmp){
            sendingChannelSelectionCmp.disableInputField(false);
        }
    },
    saveSendingChannelSelection: function (component, helper) {
        let sendingChannelSelectionCmp = component.find('sendingChannelSelection');
        if(sendingChannelSelectionCmp){
            sendingChannelSelectionCmp.saveSendingChannel();
        }
    },
    saveInterlocutorType : function (component, helper) {
        let ntfLib = component.find('ntfLib');
        let ntfSvc = component.find('ntfSvc');
        let interactionId = component.get('v.interaction').id;
        let interlocutorType = component.get('v.interlocutorType');
        component.find('apexService').builder()
            .setMethod('saveInterlocutorType')
            .setInput({
                'interactionId': interactionId,
                'interlocutorType': interlocutorType,
            })
            .setResolve(function (response) {
                if (response.error) {
                    ntfSvc.error(ntfLib, response.errorMsg);
                    return;
                }
            })
            .setReject(function (response) {
                const errors = response.getError();
                ntfSvc.error(ntfLib, JSON.stringify(errors));
                return;
            })
            .executeAction();
    },
    applyInterlocutorTypeConstratints: function (component, interlocutorType) {
        switch (interlocutorType) {
            case 'Anonymous' : {
                component.set('v.hideInterlocutorSearch', true);
                component.set('v.isInstitutionInterlocutor', false);
                break;
            }
            case 'Person' : {
                component.set('v.hideInterlocutorSearch', false);
                component.set('v.isInstitutionInterlocutor', false);
                break;
            }
            case 'Institution' : {
                component.set('v.hideInterlocutorSearch', false);
                component.set('v.isInstitutionInterlocutor', true);
                break;
            }
        }
    },
    setInterlocutorSearchType: function (component, interlocutorType) {
        switch (interlocutorType) {
            case 'Person' : {
                component.set('v.currentComponent', 'individualSearch');
                break;
            }
            case 'Institution' : {
                component.set('v.currentComponent', 'accountSearch');
                component.set("v.accountSearchType", "Institution");
                break;
            }
        }
        console.log("AccountSearchType = " + component.get("v.accountSearchType"));
    },

    /**
     * Save Origin - Channel to the interaction Object
     *
     * @author BG
     * @description [ENLCRO-325] Channel and Origin Selection - Implementation
     * @param component
     * @param event
     */
    saveInteraction: function (component, event) {
        event.preventDefault();
        let fields = {};

        if (component.get('v.originSelected')) {
            fields['Origin__c'] = component.get('v.originSelected');
        }
        if (component.get('v.channelSelected')) {
            fields['Channel__c'] = component.get('v.channelSelected');
        }
        component.find('interactionEditForm').submit(fields);
        let interaction = component.get('v.interaction');
        interaction.channel = fields['Channel__c'];
        interaction.origin = fields['Origin__c'];
    },
    /**
     * Disable wizard Section
     *
     * @author BG
     * @description [ENLCRO-325] Channel and Origin Selection - Implementation
     * @param component
     * @param {Boolean} disabled if set to True all wizard section should be disabled
     */
    disableCardWizard: function (component, disabled) {
        component.set('v.disableWizard', disabled);
        if (disabled === true) {
            $A.util.addClass(component.find('boxCardInterlocutor'), 'slds-theme_shade');
            $A.util.addClass(component.find('boxCardInterlocutor'), 'wr-pointer-event_none');
            $A.util.addClass(component.find('hrefInterlocutor'), 'slds-has-blur-focus');
        } else {
            $A.util.removeClass(component.find('boxCardInterlocutor'), 'slds-theme_shade');
            $A.util.removeClass(component.find('boxCardInterlocutor'), 'wr-pointer-event_none');
            $A.util.removeClass(component.find('hrefInterlocutor'), 'slds-has-blur-focus');
        }
    },
    goToInteractionView: function (component) {
        let thisPageReference = component.get("v.pageReference");
        let interactionId = component.get("v.recordId") || thisPageReference.state.c__id;
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: interactionId,
                objectApiName: 'Interaction__c',
                actionName: "view"
            }
        };
        navService.navigate(pageReference);
    },
    updateUrl: function (component, interactionId) {
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_InteractionEdit',
            },
            state: {
                "c__id": interactionId
            }
        };
        navService.navigate(pageReference, true);
    },
    updateUrlWithDossierId: function (component, interactionId, dossierId) {
        let navService = component.find("navService");
        let pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_InteractionEdit',
            },
            state: {
                "c__id": interactionId,
                "c__dossierId": dossierId
            }
        };
        navService.navigate(pageReference);
    },
    goToDossierView: function (component, dossierId) {
        let navService = component.find("navService");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: dossierId,
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        navService.navigate(pageReference);
    },
    openModal: function (component) {
        component.set('v.rendered', true);
    },
    closeModal: function (component) {
        component.set('v.interlocutorFields', null);
        component.set('v.customerFields', null);
        component.set('v.rendered', false);
    },
    switchOffIndividualBackBtn: function (component) {
        let isHide = component.get('v.individualEditBtnHide');
        if (isHide === true) {
            component.set('v.individualEditBtnHide', false);
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },
})