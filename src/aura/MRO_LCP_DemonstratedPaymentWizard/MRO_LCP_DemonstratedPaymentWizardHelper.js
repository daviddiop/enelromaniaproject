/**
 * Created by Boubacar Sow on 08/11/2019.
 */

({
    initialize: function (component, event, helper) {
        const self = this;
        self.showSpinner(component);
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        let dossierId = myPageRef.state.c__dossierId;
        const templateId = myPageRef.state.c__templateId;
        const genericRequestId = myPageRef.state.c__genericRequestId;
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const companyDivComponent = component.find("companyDivision");
        //component.find('sendingChannel').disableInputField(true);

        component.set("v.accountId", accountId);
       /* if(component.get('v.dossierId')){
            dossierId = component.get('v.dossierId');
        } */
        component.find('apexService').builder()
            .setMethod("InitializeDemonstratedPayment")
            .setInput({
                "accountId": accountId,
                "dossierId": dossierId,
                "genericRequestId": genericRequestId,
                "interactionId": component.find('cookieSvc').getInteractionId(),
                "templateId": templateId,
                "companyDivisionId": ''
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                component.set("v.accountId", response.accountId);
                component.set("v.dossierId", response.dossierId);
                component.set("v.dossier", response.dossier);
                component.set("v.templateId", response.templateId);

                if ((response.dossierId !== dossierId || response.genericRequestId !== genericRequestId)) {
                    self.updateUrl(component, accountId, response.genericRequestId, response.dossierId, response.templateId);
                }
                if (response.companyDivisionName) {
                    component.set("v.companyDivisionName", response.companyDivisionName);
                    component.set("v.companyDivisionId", response.companyDivisionId);

                }
                const dossierVal = component.get("v.dossier");
                if (dossierVal) {
                    if (dossierVal.Status__c === 'New' || dossierVal.Status__c === 'Canceled') {
                        component.set("v.isClosed", true);
                    }
                }

                let step = 0;
                if (component.find("contractAccountSelectionComponent")) {
                    self.reloadContractAccount(component, component.get("v.accountId"));
                }
                component.set("v.step", step);
            } else {
                self.hideSpinner(component);
                ntfSvc.error(ntfLib, response.errorMsg);
            }
        })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    createCase: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const invoiceIds = component.get("v.invoiceIds");
        const dossierId = component.get("v.dossierId");
        const accountId = component.get("v.accountId");
        const companyDivisionId = component.get("v.companyDivisionId");
        const contractAccountId = component.get("v.contractAccountId");
        const paymentInformation = component.get("v.paymentInformation");
        const originSeleted = component.get("v.originSelected");
        const channelSelected = component.get("v.channelSelected");
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');

        console.log(JSON.stringify(invoiceIds));

        component.find('apexService').builder()
            .setMethod("CreateCase")
            .setInput({
                invoiceIds: JSON.stringify(invoiceIds).replace(/[\[\]"'"]+/g, '' ),
                dossierId: dossierId,
                accountId: accountId,
                companyDivisionId: companyDivisionId,
                contractAccountId: contractAccountId,
                paymentInformation: JSON.stringify(paymentInformation),
                origin: originSeleted,
                channel: channelSelected
            }).setResolve(function (response) {
                self.hideSpinner(component);
                if (!response.error) {
                    self.redirectToDossier(component, helper);
                } else {
                    self.hideSpinner(component);
                    ntfSvc.error(ntfLib, response.errorMsg);
                }
            })
            .setReject(function (error) {
                ntfSvc.error(ntfLib, error);
            })
            .executeAction();
    },

    handleCancel: function (component, helper) {
        const self = this;
        self.showSpinner(component);
        const dossierId = component.get("v.dossierId");

        component.find('apexService').builder()
            .setMethod("cancelProcess")
            .setInput({
                dossierId: dossierId
            }).setResolve(function (response) {
            self.hideSpinner(component);
            if (!response.error) {
                self.redirectToDossier(component, helper);
            }
        })
            .setReject(function (error) {
            })
            .executeAction();
    },
    updateUrl: function (component, accountId, genericRequestId, dossierId, templateId) {
        const navService = component.find("navService");
        const pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MRO_LCP_DemonstratedPaymentWizard',
            },
            state: {
                "c__accountId": accountId,
                "c__genericRequestId": genericRequestId,
                "c__dossierId": dossierId,
                "c__templateId": templateId
            }
        };
        navService.navigate(pageReference, true);
    },
    redirectToDossier: function (component, helper) {
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get("v.dossierId"),
                objectApiName: 'c__Dossier',
                actionName: 'view'
            }
        };
        helper.redirect(component, pageReference);
    },
    goToAccount: function (component, event, helper) {
        const accountId = component.get("v.accountId");
        const pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accountId,
                objectApiName: 'Account',
                actionName: "view"
            }
        };
        helper.redirect(component, pageReference);
    },
    redirect: function (component, pageReference) {
        component.find('cookieSvc').clearInteractionId();
        const {closeTab, getEnclosingTabId, isConsoleNavigation, openSubtab} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getEnclosingTabId().then(function (enclosingTabId) {
                        openSubtab({
                            pageReference: pageReference,
                            focus: true
                        }).then(function () {
                            closeTab({
                                tabId: enclosingTabId
                            });
                        }).catch(function (error) {
                            console.log("error");
                        });
                    });
                } else {
                    const navService = component.find("navService");
                    navService.navigate(pageReference);
                }
            }).catch(function (error) {
            console.log(error);
        });
    },
    closeFocusedTab: function (component, event, helper) {
        const workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            let focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        }).catch(function (error) {
            console.log(error);
        });
    },
    saveDraftDemonstratedPayment: function (component, event, helper) {
        const self = this;
        self.redirectToDossier(component, helper);
    },
    reloadContractAccount: function (component, accVal) {
        let contractAccountComponent = component.find("contractAccountSelectionComponent");
        if (contractAccountComponent) {
            if (contractAccountComponent instanceof Array) {
                let contractAccountComponentToObj = Object.assign({}, contractAccountComponent);
                contractAccountComponentToObj[0].reset(accVal);
            } else {
                contractAccountComponent.reset(accVal);
            }
        }
    },
    showSpinner: function (component) {
        $A.util.removeClass(component.find('spinnerSection'), 'slds-hide');
    },
    hideSpinner: function (component) {
        $A.util.addClass(component.find('spinnerSection'), 'slds-hide');
    },

    /*setPeriodSelectionDate : function (component) {
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        let minDate = dateOfToday.getFullYear()+ "-" +((dateOfToday.getMonth() + 1)-3) + "-" + dateOfToday.getDate();
        component.set("v.endDate",today);
        component.set("v.minDate",minDate);
    },*/
   /* setPeriodSelectionDate : function (component) {
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        let minDate = dateOfToday.getFullYear()+ "-" +((dateOfToday.getMonth() + 1)-3) + "-" + dateOfToday.getDate();
//        console.log('### minDate '+minDate);
        component.set("v.startDate",today);
        component.set("v.endDate",today);
    },*/
    setPeriodSelectionDate : function (component) {
        const dateOfToday = new Date();
        let max = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        max.setMonth(dateOfToday.getMonth() - 3);
        //console.log('#### max '+ max);
        let maxDate = max.getFullYear()+ "-" +(max.getMonth() + 1) + "-" + max.getDate();
        //console.log('### maxDate '+maxDate);
        //console.log('### today '+today);
        component.set("v.startDate",maxDate);
        component.set("v.endDate",today);
    },
    validateInputData: function (component,event, helper) {
        let valide = false ;
        if (!component.find('EffectiveDate').checkValidity()) {
            valide = true;
            $A.util.addClass(component.find('EffectiveDate'), 'slds-has-error');
        }else{
            $A.util.removeClass(component.find('EffectiveDate'), 'slds-has-error');
        }
        return valide;
    }


});