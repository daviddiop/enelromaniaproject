/**
 * Created by Boubacar Sow on 08/11/2019.
 */

({

    init: function (component, event, helper) {
        const myPageRef = component.get("v.pageReference");
        const accountId = myPageRef.state.c__accountId;
        const dossierId = myPageRef.state.c__dossierId;
        helper.setPeriodSelectionDate(component);
        component.set("v.dossierId", dossierId);
        component.set("v.accountId", accountId);
        helper.initialize(component, event, helper);
    },

    onRender: function (component) {
        const {getFocusedTabInfo, isConsoleNavigation, setTabIcon, setTabLabel} = component.find("workspace");
        isConsoleNavigation()
            .then(function (response) {
                if (response === true) {
                    getFocusedTabInfo()
                        .then(function (response) {
                            let focusedTabId = response.tabId;
                            console.log(' response tab Info ', JSON.stringify(response));
                            let isSubTab = response.isSubtab;
                            if (isSubTab) {
                                setTabLabel({
                                    tabId: focusedTabId,
                                    label: $A.get("$Label.c.DemonstratedPayment")
                                });
                                setTabIcon({
                                    tabId: focusedTabId,
                                    icon: "utility:case",
                                    iconAlt: $A.get("$Label.c.DemonstratedPayment")
                                });
                            }
                        });
                }
            });
    },

    getContractAccountRecord: function (component, event, helper) {
        let selectedContractAccount = event.getParam("selectedContractAccount");

        console.log("### selectedContractAccount "+selectedContractAccount);
        if (selectedContractAccount){
            component.set("v.contractAccountId",selectedContractAccount.Id);
            component.set("v.companyDivisionId",selectedContractAccount.CompanyDivision__c);
            component.set("v.billingAccountNumber",selectedContractAccount.IntegrationKey__c);
            component.set("v.step", 2);

        }
    },

    handleInvoiceSelect: function(component, event, helper){
        let invoiceList = event.getParam("selectedInvoiceList");

        let invoiceIds = [];
        Object.keys(invoiceList).forEach(function (key) {
            invoiceIds.push(invoiceList[key].invoiceSerialNumber + invoiceList[key].invoiceNumber);
        });

        console.log("###invoiceIds "+invoiceIds);
        if(invoiceIds){
            const ntfLib = component.find('notifLib');
            const ntfSvc = component.find('notify');
            ntfSvc.success(ntfLib, $A.get("$Label.c.SelectedSucess"));
            component.set("v.invoiceIds", invoiceIds);
            component.set("v.step", 5);
        }
    },

    nextStep: function (component, event, helper) {
        const buttonPressed = event.getSource().getLocalId();
        component.find('sendingChannel').disableInputField(true);
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        switch (buttonPressed) {
            case 'confirmStep0':
                component.set("v.step", 2);
                break;
            case 'confirmStep1':
                if(component.get('v.contractAccountId')){
                    component.set("v.step", 3);
                    let sendingChannel = component.find('sendingChannel');
                    sendingChannel.reloadAddressList();
                }
                break;
            case 'confirmStep2':
                component.find("periodSelection").onConfirmSelection();
                let validDate = component.find("periodSelection").handleValidDate();
               if(validDate){
                   component.set("v.step", 4);
               } else {
                   ntfSvc.error(ntfLib, $A.get("$Label.c.RangeNotValid"));
                   component.set("v.step", 3);
               }
                /*if (!component.find('startDate').checkValidity()) {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RangeNotValid"));
                    component.set("v.step", 3);
                } else {
                    component.set("v.step", 4);
                }*/
                //component.set("v.step", 4);
                break;
            case "confirmStep3":
                if (component.get("v.invoiceIds")) {
                    component.set("v.step", 5);
                }
                else {
                    ntfSvc.error(ntfLib, $A.get("$Label.c.EmptyInvoices"));
                    component.set("v.step", 4);
                }
                break;
            case "confirmStep5":
                let notes = component.find('Notes').get('v.value');
                if (helper.validateInputData(component)){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    return;
                } else if(!notes || notes === ''){
                    ntfSvc.error(ntfLib, $A.get("$Label.c.RequiredFields"));
                    $A.util.addClass(component.find('Notes'), 'slds-has-error');
                    return;
                }
                else {
                    component.set("v.step", 6);
                    component.find('sendingChannel').disableInputField(false);
                }
                break;
            default:
                break;
        }
    },

    editStep: function (component, event, helper) {
        component.find('sendingChannel').disableInputField(true);
        var buttonPressed = event.getSource().getLocalId();
        if (buttonPressed === "returnStep1") {
            component.set("v.step", 2);
        } else if (buttonPressed === "returnStep2") {
            component.set("v.step", 3);
        } else if (buttonPressed === "returnStep3") {
            if (component.get('v.invoiceIds')){
                component.set("v.step", 5);
            }else{
                component.set("v.step", 4);
            }
        }
        else if (buttonPressed === "returnStep5") {
                component.set("v.step", 5);
            component.find('sendingChannel').disableInputField(false);
        }
    },

    saveChain: function (component, event, helper){
        component.find('sendingChannel').saveSendingChannel();
    },
    handleSuccessDossier: function (component,event,helper){
        event.preventDefault();
        const ntfLib = component.find('notifLib');
        const ntfSvc = component.find('notify');
        const selectedChannel = event.getParam("sendingChannel");
        if(selectedChannel ){
            const amount = component.get("v.amount");
            const effectiveDate = component.get("v.effectiveDate");
            const notes = component.get("v.notes");
            let paymentInformation = {};
            paymentInformation.amount = amount;
            paymentInformation.effectiveDate = effectiveDate;
            paymentInformation.note = notes;
//        console.log('#### paymentInformation: '+JSON.stringify(paymentInformation));
            component.set('v.paymentInformation',paymentInformation);

            helper.createCase(component, helper);
        }
    },

    cancel: function (component, event, helper) {
        helper.handleCancel(component, helper);
    },

    saveDraft: function (component, event, helper) {
        helper.saveDraftDemonstratedPayment(component,event,helper);
    },
    handleOriginChannelSelection: function (component, event, helper) {
        let originSelected = event.getParam('selectedOrigin');
        let channelSelected = event.getParam('selectedChannel');
        if (originSelected && channelSelected) {
            component.set('v.originSelected', originSelected);
            component.set('v.channelSelected', channelSelected);
            component.set("v.step", 1);
            let dossier = component.get('v.dossier');
            if(dossier && dossier.Origin__c){
                component.set("v.step", 2);
            }
        } else {
            component.set("v.step", 0);
        }
    },

    handleSelectedRange: function(component,event,helper){
        component.set('v.startDate',event.getParam("startDate"));
        component.set('v.endDate',event.getParam("endDate"));
    }


});