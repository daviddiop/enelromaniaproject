/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 18, 2020
 * @desc    
 * @history 
 */

trigger MRO_CaseGroupTrg on CaseGroup__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_CaseGroupHandler());
}