/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   ott 13, 2020
 * @desc    
 * @history 
 */

trigger MRO_AccountContactRelationTrg on AccountContactRelation (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_AccountContactRelationHandler());
}