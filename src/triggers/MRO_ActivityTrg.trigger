/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   set 24, 2019
 * @desc    Apex Trigger for the wrts_prcgvr__Activity__c object
 * @history 
 */

trigger MRO_ActivityTrg on wrts_prcgvr__Activity__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_ActivityHandler());
}