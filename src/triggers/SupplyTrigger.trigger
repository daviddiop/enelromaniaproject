trigger SupplyTrigger on Supply__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_SupplyHandler());
}