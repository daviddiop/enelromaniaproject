/**
 * Created by Roberto Tomasoni on 21/03/2019.
 * modified by Bouba on 10/10/2019.
 */

trigger CaseTrigger on Case (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if (!FeatureManagement.checkPermission('MRO_SkipTriggerCase')){

        TriggerManager.handle(new MRO_TR_CaseHandler());
    }
}