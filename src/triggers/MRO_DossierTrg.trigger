/**
 * @author   Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since    Sep 24, 2019
 * @desc     Apex Trigger for the Dossier__c object
 * modified by Bouba on 10/10/2019.
 */

trigger MRO_DossierTrg on Dossier__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if (!FeatureManagement.checkPermission('MRO_SkipTriggerDossier')){

        TriggerManager.handle(new MRO_TR_DossierHandler());
    }
}