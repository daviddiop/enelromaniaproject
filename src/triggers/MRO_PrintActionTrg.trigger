/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   nov 14, 2019
 * @desc    
 * @history 
 */

trigger MRO_PrintActionTrg on PrintAction__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_PrintActionHandler());
}