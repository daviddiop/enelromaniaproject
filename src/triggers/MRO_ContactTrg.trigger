/**
 * Created by napoli on 14/11/2019.
 */
/**
 * Created by napoli on 14/11/2019.
 */
/**
 * @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
 * @since   nov 14, 2019
 * @desc
 * @history
 */
trigger MRO_ContactTrg on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_ContactHandler());
}