/**
 * Created by Stefano on 09/09/2019.
 */

trigger MRO_CityTrg on City__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_City());
}