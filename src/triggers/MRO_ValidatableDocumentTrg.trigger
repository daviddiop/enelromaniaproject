/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 24, 2020
 * @desc    
 * @history 
 */

trigger MRO_ValidatableDocumentTrg on ValidatableDocument__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_ValidatableDocumentHandler());
}