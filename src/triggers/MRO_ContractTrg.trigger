trigger MRO_ContractTrg on Contract (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_ContractHandler());
}