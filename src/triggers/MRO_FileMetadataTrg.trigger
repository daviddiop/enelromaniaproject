/**
 * @author  Stefano Porcari
 * @since   Dec 23, 2019
 * @desc    Apex Trigger for the FileMetadata__c object
 * @history 
 */
trigger MRO_FileMetadataTrg on FileMetadata__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_FileMetadataHandler());
}