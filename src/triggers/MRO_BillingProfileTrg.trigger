/**
 * Created by ferhati on 10/02/2020.
 */

trigger MRO_BillingProfileTrg on BillingProfile__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_BillingProfileHandler());
}