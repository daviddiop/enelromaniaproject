/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   13/01/2020
* @desc
* @history
*/
trigger MRO_ExecutionLogTrg on ExecutionLog__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_ExecutionLogHandler());
}