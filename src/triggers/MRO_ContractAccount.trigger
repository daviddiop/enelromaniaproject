/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 13, 2020
 * @desc    
 * @history 
 */

trigger MRO_ContractAccount on ContractAccount__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_ContractAccountHandler());
}