/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 26, 2020
 * @desc    
 * @history 
 */

trigger MRO_OpportunityTrg on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_OpportunityHandler());
}