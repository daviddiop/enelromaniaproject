/**
 * Created by napoli on 26/09/2019.
 */
/**
 * @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
 * @since   set 26, 2019
 * @desc
 * @history
 */
trigger MRO_IndividualTrg on Individual (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerManager.handle(new MRO_TR_IndividualHandler());
}