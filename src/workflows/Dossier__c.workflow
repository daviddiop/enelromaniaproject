<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetDossierReferenceDate</fullName>
        <field>ReferenceDate__c</field>
        <formula>IF(ISBLANK(Parent__c), DATEVALUE(CreatedDate), Parent__r.ReferenceDate__c)</formula>
        <name>Set Dossier Reference Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>DossierReferenceDate</fullName>
        <actions>
            <name>SetDossierReferenceDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
  ISBLANK(ReferenceDate__c),
  ISCHANGED(Parent__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
