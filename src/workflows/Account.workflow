<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Fill_Key_field</fullName>
        <description>Fill the Key field with the National Identity Number (for PersonAccounts) or the VAT Number (for business Accounts).</description>
        <field>Key__c</field>
        <formula>IF(
  IsPersonAccount,
  NationalIdentityNumber__pc,
  IF(
    NOT(ISBLANK(VATNumber__c)),
    CASE(
      RecordType.DeveloperName,
      &apos;Distributor&apos;, &apos;D_&apos;,
      &apos;Trader&apos;, &apos;T_&apos;,
      &apos;Institution&apos;, &apos;I_&apos;,
      &apos;Performer&apos;, &apos;P_&apos;,
      &apos;SalesUnit&apos;, &apos;S_&apos;,
      &apos;&apos;
    ) &amp;  
    IF(
      ForeignCompany__c,
      VATNumber__c,
      IF(
        LEFT(VATNumber__c, 2) = &apos;RO&apos;,
        MID(VATNumber__c, 3, LEN(VATNumber__c)),
        VATNumber__c
      )
    ),
    &apos;&apos;
  )
)</formula>
        <name>Fill Key field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Key field update needed</fullName>
        <actions>
            <name>Fill_Key_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Key field needs to be filled or updated for new Account records or if National Identity Number / VAT Number are changed.</description>
        <formula>ISBLANK(Key__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
