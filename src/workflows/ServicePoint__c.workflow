<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetServicePointMeterReadingControl</fullName>
        <field>MeterReadingControl__c</field>
        <literalValue>002</literalValue>
        <name>Set ServicePoint Meter Reading Control</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Service Point Meter Reading Control without IVR</fullName>
        <actions>
            <name>SetServicePointMeterReadingControl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(
  ISPICKVAL(MeterReadingControl__c, &apos;&apos;),
  NOT(Distributor__r.SelfReadingEnabled__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
