<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetSupplyIntegrationKey</fullName>
        <field>IntegrationKey__c</field>
        <formula>Name</formula>
        <name>Set Supply Integration Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Supply_SAPContractCode</fullName>
        <field>SAPContractCode__c</field>
        <formula>IF(
  RecordType.DeveloperName = &apos;Service&apos;,
  RIGHT(Name, LEN(Name)-4)&amp;&apos;-V&apos;,
  ServiceSiteKey__c
)</formula>
        <name>Set Supply SAPContractCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set Supply Integration Key</fullName>
        <actions>
            <name>SetSupplyIntegrationKey</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Supply__c.IntegrationKey__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Supply SAPContractCode</fullName>
        <actions>
            <name>Set_Supply_SAPContractCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   ISBLANK(SAPContractCode__c),   OR(     ISNEW(),     ISCHANGED(ServiceSite__c)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
