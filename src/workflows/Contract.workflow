<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Fill_Contract_Name</fullName>
        <field>Name</field>
        <formula>ContractNumber</formula>
        <name>Fill Contract Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetContractIntegrationKey</fullName>
        <field>IntegrationKey__c</field>
        <formula>ContractNumber</formula>
        <name>Set Contract Integration Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Empy Contract Name</fullName>
        <actions>
            <name>Fill_Contract_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Name</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Contract Integration Key</fullName>
        <actions>
            <name>SetContractIntegrationKey</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(IntegrationKey__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
