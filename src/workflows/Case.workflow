<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_SLA_Due_Date_is_close</fullName>
        <description>Case SLA Due Date is close</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>pierandrea.pes@wr.enelro</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORT_Case_SLA_Due_Date_notification_SAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>CopyCaseNumber</fullName>
        <description>Copies the the CaseNumber to the Integration Key</description>
        <field>IntegrationKey__c</field>
        <formula>CaseNumber</formula>
        <name>CopyCaseNumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SendSLADueNotification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>GDPRRequest</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>With 7 calendar days before the SLA Due Date, the system should issue a notification to the Case owner that the due date is close.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_SLA_Due_Date_is_close</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.SLAExpirationDate__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Integration Key</fullName>
        <actions>
            <name>CopyCaseNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (   ISCHANGED ( Phase__c ),   ISBLANK( IntegrationKey__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
