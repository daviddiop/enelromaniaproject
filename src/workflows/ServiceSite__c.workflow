<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_ServiceSite_IntegrationKeyELE</fullName>
        <field>IntegrationKeyELE__c</field>
        <formula>IF(
  ISBLANK(IntegrationKeyELE__c),
  MID(Name, 4, LEN(Name)-3) &amp; &apos;-E&apos;,
  IntegrationKeyELE__c
)</formula>
        <name>Set ServiceSite IntegrationKeyELE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ServiceSite_IntegrationKeyGAS</fullName>
        <field>IntegrationKeyGAS__c</field>
        <formula>IF(
  ISBLANK(IntegrationKeyGAS__c),
  MID(Name, 4, LEN(Name)-3) &amp; &apos;-G&apos;,
  IntegrationKeyGAS__c
)</formula>
        <name>Set ServiceSite IntegrationKeyGAS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ServiceSite_IntegrationKeyVAS</fullName>
        <field>IntegrationKeyVAS__c</field>
        <formula>IF(
  ISBLANK(IntegrationKeyVAS__c),
  MID(Name, 4, LEN(Name)-3) &amp; &apos;-V&apos;,
  IntegrationKeyVAS__c
)</formula>
        <name>Set ServiceSite IntegrationKeyVAS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Service Site Integration Keys</fullName>
        <actions>
            <name>Set_ServiceSite_IntegrationKeyELE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_ServiceSite_IntegrationKeyGAS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_ServiceSite_IntegrationKeyVAS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
   ISBLANK(IntegrationKeyELE__c),
   ISBLANK(IntegrationKeyGAS__c),
   ISBLANK(IntegrationKeyVAS__c)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
