/**
 * Created by Boubacar Sow  on 02/10/2020.
 */

import {LightningElement, track, api} from "lwc";
import {NavigationMixin} from "lightning/navigation";
import {labels} from "c/mroLabels";

export default class MroOutComeSelection extends  NavigationMixin(LightningElement) {
    @api caseId;
    @api defaultValue = '';
    @api
    get recordTypeId() {
        return this._recordTypeId;
    }

    set recordTypeId(value) {
        console.log('**** Record type Id ', value);
        this._recordTypeId = value;
    }

    @api disabled = false;

    @track outComeSelected = "";
    @track _recordTypeId;

    labels = labels;

    handleChange(event) {
        this.outComeSelected = event.target.value;
        this.sendEvent();
    }

    sendEvent() {
        console.log("outComeSelected = " + this.outComeSelected);
        const changedEvent = new CustomEvent("changed", {
            detail: {
                outComeSelected: this.outComeSelected,
            }
        });
        this.dispatchEvent(changedEvent);
    }

}