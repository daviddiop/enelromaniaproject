import { LightningElement, api } from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import { NavigationMixin } from 'lightning/navigation';
import { labels } from 'c/labels';

export default class SupplyListItem extends NavigationMixin(LightningElement) {
    @api supply;
    labels = labels;

    get isGas() {
        return this.supply.RecordType.DeveloperName === 'Gas';
    }

    get isVas() {
        return this.supply.RecordType.DeveloperName === 'Service';
    }

    get isElectric() {
        return this.supply.RecordType.DeveloperName === 'Electric';
    }

    get svgURLEle() {
        return SVG_URL + '/images/Electric.svg#electric';
    }
    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }
    get svgURLVas() {
        return SVG_URL + '/images/Service.svg#service';
    }

    navigateToRecordViewPage(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.target.name,

                actionName: 'view'
            }
        });
    }
}