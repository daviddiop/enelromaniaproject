/**
 * Created by Bouba on 03/10/2019.
 */

import {api, LightningElement, track} from 'lwc';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error} from 'c/notificationSvc';
import {labels} from 'c/mroLabels';
import {checkServicePointEleCode, checkServicePointGasCode} from "c/mroValidations";

export default class MroSupplySearch extends LightningElement {
    labels = labels;
    @api accountId;
    @api status;
    @api showHeader = false;
    @api enableEmptySearch = false;
    @api contractId;
    @api checkOnlyOneSupply = false;
    @api notOwnAccount = false;
    @api disabled = false;
    @api selectFields = [];
    @api companyDivisionId;
    @api showCompanyDivisionAsFilter = false;
    @api commodity;

    @track viewFields = [];
    @track showSuppliesList;
    @track emptySuppliesList = true;
    @track servicePointCode = '';
    @track contractCode = '';
    @track contractCustomerCode = '';
    @track selectedRecordType = '';
    @track servicePointRecordTypeVal;
    @track contractRecordTypeVal;
    @track recordTypeOptions = [];
    @track supplyFieldsList = [];
    @track selectedSupplyList = [];
    @track supplyFieldsListClone = [];
    @track filterSupply = [];
    @track findButtonEvt = 'find';
    @track selectedSupply;
    @track objNam = 'Supply__c';
    @track supplyLookupFields = [];

    @api
    disabledSupply(valueForDisable) {
        this.disabled = valueForDisable;
        if (this.disabled) {
            return "slds-m-top_x-small slds-scrollable_y listView slds-theme_shade";
        }
        return "slds-m-top_x-small slds-scrollable_y listView";
    }

    filteredSupplyList(event) {
        if (this.checkOnlyOneSupply) {
            this.selectOnlyOneSupply(event);
        }

        if (event.target.checked) {
            let filteredSupplyList = this.supplyFieldsList.filter(function (supplyCT) {
                return supplyCT.Id === event.target.value;
            });
            this.selectedSupplyList = this.selectedSupplyList.concat(filteredSupplyList);
        } else {
            if ((this.selectedSupplyList.length) > 0) {
                this.selectedSupplyList = this.selectedSupplyList.filter(function (el) {
                    return el.Id !== event.target.value;
                });
            }
        }
    }

    @api
    resetForm() {
        this.newSearch();
    }

    saveToSupplyListEvent() {
        const selectedEvent = new CustomEvent('selected', {
            detail: {
                supplyFieldsValue: this.selectedSupplyList
            }
        });
        this.dispatchEvent(selectedEvent);
    }

    selectOnlyOneSupply(event) {
        this.selectedSupply = this.supplyFieldsList.find(function (supply) {
            event.target.checked = true;
            return supply.Id === event.target.value;
        });

        let supplyInputs = this.template.querySelectorAll('input');
        supplyInputs.forEach(inputElement => {
            if (inputElement.value !== event.target.value) {
                inputElement.checked = false;
            }
        });

        if (!this.selectedSupply.ServicePoint__c) {
            error(this, this.labels.noRelatedServicePoint);
            return;
        }

        const selectedEvent = new CustomEvent('checkonesupply', {
            detail: {
                selectedSupply: this.selectedSupply
            }
        });
        this.dispatchEvent(selectedEvent);
    }

    newSearch() {
        this.showSuppliesList = false;
        this.emptySuppliesList = true;
        this.selectedSupplyList = [];
        this.contractCode = '';
        this.contractCustomerCode = '';
        this.servicePointCode = '';
        this.selectedRecordType = this.servicePointRecordTypeVal;

        const selectedEvent = new CustomEvent('checked', {
            detail: {
                selectedRecordType: this.selectedRecordType,
            }
        });
        this.dispatchEvent(selectedEvent);
    }

    handleCompanyDivisionChanged(event) {
        this.companyDivisionEnforced = event.detail.isCompanyDivisionEnforced;
        if (this.showCompanyDivisionAsFilter || this.companyDivisionEnforced) {
            this.companyDivisionId = event.detail.divisionId;
        }
        let inputCmp = this.template.querySelector(
            '[data-id="searchBoxServicePoint"]'
        );
        if (inputCmp) {
            inputCmp.value = "";
        }
        if (!this.companyDivisionEnforced && this.showCompanyDivisionAsFilter) {
            this.resetForm();
        }
    }

    handleChangeServicePoint(event) {
        event.target.classList.remove('slds-has-error');
        this.servicePointCode = event.target.value;
        this.removeError();
    }

    handleChangeContractCode(event) {
        event.target.classList.remove('slds-has-error');
        this.contractCode = event.target.value.trim();
        this.removeError();
    }

    handleChangeContractCustomerCode(event) {
        event.target.classList.remove('slds-has-error');
        this.contractCustomerCode = event.target.value.trim();
        this.removeError();
    }

    selectedSearchType(event) {
        this.selectedRecordType = event.target.value;

        const selectedEvent = new CustomEvent('checked', {
            detail: {
                selectedRecordType: this.selectedRecordType
            }
        });
        this.dispatchEvent(selectedEvent);

        this.contractCode = '';
        this.contractCustomerCode = '';
    }

    renderedCallback() {
        this.getLookupFields(this.objNam);
    }

    getLookupFields(objName) {
        let inputs = {objName: objName};
        notCacheableCall({
            className: 'MRO_LC_Supply',
            methodName: 'getLookupFields',
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }

                this.supplyLookupFields = response.data.lookupFields;
            })
            .catch(err => {
                error(this, JSON.stringify(err));
            });
    }

    filterSearchResults(event) {
        var temp = this.supplyFieldsListClone;
        if (event.target.value) {
            let self = this;
            let value = event.target.value.toString().toLowerCase();
            this.supplyFieldsList = temp.filter(function (val) {
                for (var property in val) {
                    if (val.hasOwnProperty(property)) {
                        if (val[property] instanceof Object) {
                            for (var subproperty in val[property]) {
                                if (val[property].hasOwnProperty(subproperty)) {
                                    if ((subproperty !== 'Id') && self.selectFields.toString().includes(subproperty.toString()) && val[property][subproperty].toString().toLowerCase().includes(value)) {
                                        return true;
                                    }
                                }
                            }
                        } else {
                            let valProperty = val[property].toString();
                            if ((property !== 'Id') && (!self.supplyLookupFields.toString().includes(property.toString())) && self.selectFields.toString().includes(property.toString()) && (valProperty.toLowerCase().includes(value))) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            });
        } else if (!event.target.value) {
            this.supplyFieldsList = temp;
        }
    }

    get addClass() {
        if (this.enableEmptySearch) {
            return '';
        }
        return 'wr-required';
    }

    get disableSelectButton() {
        return (this.selectedSupplyList.length === 0);
    }

    get disableContractCustomerCode() {
        return (this.contractCode.length >= 1);
    }

    get disableContractCode() {
        return (this.contractCustomerCode.length >= 1);
    }

    get isServicePoint() {
        return ((this.selectedRecordType === '') || (this.selectedRecordType === this.servicePointRecordTypeVal));
    }

    get isContract() {
        return (this.selectedRecordType === this.contractRecordTypeVal);
    }

    get isBlank() {
        return (this.servicePointCode === '' && (this.contractCode === '' && this.contractCustomerCode === ''));
    }

    /*
    get recordTypeOptions() {
        return this.recordTypeOptions;
    }
    */
    /*get recordTypeId() {
        return this.recordTypeId;
    }*/

    connectedCallback() {
        if ((this.selectFields.length > 0)) {
            this.viewFields = this.selectFields.filter(function (el) {
                return (!(el.includes('.')));
            });
        }
        if (!this.companyDivisionEnforced && !this.showCompanyDivisionAsFilter) {
            this.companyDivisionId = "";
        }
        this.getRecordTypes();
    }


    getRecordTypes() {
        let inputs = {};
        cacheableCall({
            className: 'MRO_SRV_Supply',
            methodName: 'getRecordTypes',
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }
                this.servicePointRecordTypeVal = response.data.servicePointRecordType.value;
                this.contractRecordTypeVal = response.data.contractRecordType.value;
                this.recordTypeOptions.push(response.data.servicePointRecordType);
                this.recordTypeOptions.push(response.data.contractRecordType);
            })
            .catch(err => {
                error(this, JSON.stringify(err));
            });

    }


    findSupplyByServicePointSearch() {

        if (!this.enableEmptySearch) {
            let inputCmp = this.template.querySelector('lightning-input');
            let value = inputCmp.value;

            if (this.servicePointCode.length === 0) {
                error(this, this.labels.requiredFields);
                this.validateFields();
                return;
            }
            /*
            else if (this.servicePointCode.length < 14) {
                error(this, this.labels.shortEntry);
                this.validateFields();
                return;
            }
             */


            if (!this.commodity && !checkServicePointGasCode(value) && !checkServicePointEleCode(value)) {
                error(this, this.labels.invalidCode);
                inputCmp.classList.add('slds-has-error');
                return;
            } else if (this.commodity === 'Electric' && !checkServicePointEleCode(value)) {
                error(this, this.labels.invalidCode);
                inputCmp.classList.add('slds-has-error');
                return;
            } else if (this.commodity === 'Gas' && !checkServicePointGasCode(value)) {
                error(this, this.labels.invalidCode);
                inputCmp.classList.add('slds-has-error');
                return;
            } else {
                inputCmp.setCustomValidity("");
            }
            inputCmp.reportValidity();
            if (!inputCmp.reportValidity()) {
                return;
            }

            if (!this.servicePointCode || this.servicePointCode === "") {
                error(this, this.labels.noDataEntered);
                return;
            }

            inputCmp.setCustomValidity("");
        }

        this.findSuppliesByServicePoint();
    }

    findSuppliesByServicePoint() {

        let stringSelectFields = JSON.stringify(this.selectFields);
        let stringNotOwnAccount = JSON.stringify(this.notOwnAccount);
        let inputs = {
            servicePointCode: this.servicePointCode,
            accountId: this.accountId,
            status: this.status,
            selectFields: stringSelectFields,
            contractId: this.contractId,
            notOwnAccount: stringNotOwnAccount,
            companyDivisionId: this.companyDivisionId
        };

        notCacheableCall({
            className: 'MRO_LC_Supply',
            methodName: 'getSuppliesByServicePoint',
            input: inputs
        })
            .then(result => {
                if (result.data.error) {
                    error(this, result.data.errorMsg);
                    return;
                }

                this.supplyFieldsList = result.data.supplyFieldsSP;
                this.supplyFieldsListClone = result.data.supplyFieldsSP;
                this.showSuppliesList = result.data.supplyFieldsSP.length > 0;
                this.emptySuppliesList = result.data.supplyFieldsSP.length === 0;
                const selectedEvent = new CustomEvent('checked', {
                    detail: {
                        findButtonEvt: this.findButtonEvt,
                    }
                });
                this.dispatchEvent(selectedEvent);
            })
            .catch(err => {
                error(this, JSON.stringify(err));
            });
    }

    findSupplyByContract() {

        if (!this.enableEmptySearch) {
            if (this.validateFields() === false) {
                error(this, this.labels.requiredFields);
                return;
            }
        }

        this.findSuppliesByContract();
    }

    findSuppliesByContract() {
        let stringSelectFields = JSON.stringify(this.selectFields);
        let stringNotOwnAccount = JSON.stringify(this.notOwnAccount);
        let inputs = {
            contractCode: this.contractCode,
            contractCustomerCode: this.contractCustomerCode,
            accountId: this.accountId,
            status: this.status,
            selectFields: stringSelectFields,
            contractId: this.contractId,
            notOwnAccount: stringNotOwnAccount,
            companyDivisionId: this.companyDivisionId
        };

        notCacheableCall({
            className: 'MRO_LC_Supply',
            methodName: 'getSuppliesByContract',
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }
                this.supplyFieldsList = response.data.supplyFieldsCT;
                this.supplyFieldsListClone = response.data.supplyFieldsCT;
                this.showSuppliesList = response.data.supplyFieldsCT.length > 0;
                this.emptySuppliesList = response.data.supplyFieldsCT.length === 0;

                const selectedEvent = new CustomEvent('checked', {
                    detail: {
                        findButtonEvt: this.findButtonEvt
                    }
                });
                this.dispatchEvent(selectedEvent);
            })
            .catch(err => {
                error(this, JSON.stringify(err));
            });
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (!this.disableContractCode && !this.disableContractCustomerCode) {
                if (valueInput === null || valueInput.trim() === '') {
                    inputRequiredCmp.classList.add('slds-has-error');
                    areValid = false;
                }
            }
            if (this.servicePointCode && (!this.disableContractCode && !this.disableContractCustomerCode)) {
                if (valueInput == null || valueInput.length < 14 || valueInput.trim() === '') {
                    inputRequiredCmp.classList.add('slds-has-error');
                    areValid = false;
                }
            }
        });
        return areValid;
    };

    removeError = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.remove('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }

}