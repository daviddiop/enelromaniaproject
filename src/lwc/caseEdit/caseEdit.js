import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import {error, success} from 'c/notificationSvc';
import fieldStyle from '@salesforce/resourceUrl/EnergyAppResources';
import {loadStyle} from 'lightning/platformResourceLoader';
import {labels} from 'c/labels';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';

export default class CaseEdit extends NavigationMixin(LightningElement) {
    // Added for compatibility with previous version - deprecated  
    @api isConnectionWizard;
    @api hideCommodityButton;
    //--

    @api caseId;
    @api isCaseModalByAddressForm = false;
    @api dossierId;
    @api supplyId;
    @api accountId;
    @api companyId;
    @api showHeader;
    @api closeModal = false;
    @api selectCaseFields = [];
    @api selectCaseFieldsValues;
    @api readOnlyAddress = false;
    @api addressForced = false;
    @api recordTypeCase;
    @api caseAddress;
    @api hideCommodity = false;
    @api title;
    @track defaultTitle;


    @track isNormalize;
    @track caseFields;
    @track showLoadingSpinner = true;
    @track recordTypePicklist = [];
    @track selectedOption;

    defaultCaseOrigin;
    defaultCaseStatus;
    labels = labels;

    connectedCallback() {
        Promise.all([
            loadStyle(this, fieldStyle + '/style.css'),
        ])
            .then(() => {
            })
            .catch(err => {
                error(this, err.body.message);
            });
        if (this.selectCaseFieldsValues && Object.keys(this.selectCaseFieldsValues).length) {
            this.caseFields = Object.keys(this.selectCaseFieldsValues);
        } else if (this.selectCaseFields.length) {
            this.caseFields = this.selectCaseFields;
        }
        this.getConstants();

        if (this.isCaseModalByAddressForm) {
            this.getRecordTypeConnection();
        }


        this.defaultTitle = this.title ?  this.title : this.labels.createCase;

    }

    getConstants() {
        cacheableCall({
            className: "Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.defaultCaseOrigin = apexResponse.data.CASE_DEFAULT_CASE_ORIGIN;
                        this.defaultCaseStatus = apexResponse.data.CASE_STATUS_DRAFT;
                    }
                }
            });
    }


    getRecordTypeConnection() {
        cacheableCall({
            className: "CaseCnt",
            methodName: "getCaseDescribe"
        })
            .then(apexResponse => {
                if (apexResponse.isError) {
                    error(this, JSON.stringify(apexResponse.error));
                }
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.recordTypePicklist = apexResponse.data.recordTypePicklistValues;
                    }
                }
            });
    }

    handleLoad() {
        if (this.selectCaseFieldsValues) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = this.selectCaseFieldsValues[element.fieldName];
            });
        }
        this.showLoadingSpinner = false;
    }

    handleSubmit(event) {
        this.showLoadingSpinner = true;
        event.preventDefault();
        event.stopPropagation();

        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });

        if (this.isCaseModalByAddressForm) {
            this.showLoadingSpinner = true;

            let addrForm = this.template.querySelector('c-address-form');
            if (!addrForm.isValid()) {
                error(this, this.labels.addressMustBeVerified);
                this.showLoadingSpinner = false;
                return;
            }

            let addrFields = addrForm.getValues();
            for (let f in addrFields) {
                if (addrFields.hasOwnProperty(f)) {
                    fields[f] = addrFields[f];
                }
            }

            fields.AddressNormalized__c = this.addressForced === false;
        }
        if (this.accountId) {
            fields.AccountId = this.accountId;
        }

        if (this.dossierId) {
            fields.Dossier__c = this.dossierId;
        }
        if (this.supplyId) {
            fields.Supply__c = this.supplyId;
        }
        if (this.companyId) {
            fields.CompanyDivision__c = this.companyId;
        }

        if (this.isCaseModalByAddressForm) {
            fields.RecordTypeId = this.selectedOption;
        } else {
            if (this.recordTypeCase) {
                fields.RecordTypeId = this.recordTypeCase;
            }
        }

        fields.Origin = this.defaultCaseOrigin;
        fields.Status = this.defaultCaseStatus;

        if (this.validateFields()) {
            this.showLoadingSpinner = true;
            this.template.querySelector('lightning-record-edit-form').submit(fields);
        } else {
            error(this, this.labels.requiredFields);
            return;
        }
    }

    handleSuccess(event) {
        this.showLoadingSpinner = false;
        event.preventDefault();
        event.stopPropagation()
        success(this, this.labels.caseSuccessfullyUpdated);
        const successEvent = new CustomEvent('casesuccess');
        this.dispatchEvent(successEvent);
        this.addClosingEvent();

    }
    handleError(event) {
        error(this, event.detail.message);
        this.showLoadingSpinner = false;
    }
    handleCancel() {
        this.addClosingEvent();
    }

    removeError(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
    }

    addClosingEvent() {
        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }

    validateFields() {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        let requireFieldsCombobox = Array.from(this.template.querySelectorAll('lightning-combobox.wr-required'));
        requireFields = requireFields.concat(requireFieldsCombobox);
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (!valueInput || (isNaN(valueInput) && valueInput.trim() === '')) {
                inputRequiredCmp.classList.add('slds-has-error');
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        return areValid;
    }

    disabledSaveButton(event) {
        this.isNormalize = event.detail.isnormalize;
    }

    disabledSaveButtonCaseAddress(event) {
        this.isNormalize = event.detail.isnormalize;
        this.addressForced = event.detail.addressForced;
    }

    handleSelectOption(event) {
        this.selectedOption = event.target.value;
    }

}