import { LightningElement, api, track } from 'lwc';
import {labels} from 'c/labels';


export default class ProductDetail extends LightningElement {

    @api product
    @track rendered
    labels = labels;

    @api
    open() {
        this.rendered = true;
    }

    @api
    close() {
        this.rendered = false;
    }

    onAddClick() {
        this.rendered = false;
        const selectedEvent = new CustomEvent('selectproduct', {
            detail: {
                action: 'add',
                product: this.product
            }
        });
        const selectedEventLWC = new CustomEvent('selectproduct', {
            detail: {
                action: 'add',
                product: this.product
            }
        });
        this.dispatchEvent(selectedEvent);
        this.dispatchEvent(selectedEventLWC);
    }
}