import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';

export default class AddressInput extends NavigationMixin(LightningElement) {

    @api values;
    @api element;
    @api fieldName;
    @api addressPrefix
    @api required;
    @api placeholder = '';
    @api readOnly;

    @track value = '';
    @track label = '';

    @api
    getValue() {
        return this.value;
    }

    @api
    setValue(value) {
        this.value = value;
    }

    // @api
    // setValues(values) {
    //     this.values = values;
    // }

    @api
    setLabel(label) {
        this.label = label;
    }

    handleChange(event) {
        this.value = event.detail.value;
        this.removeError(event);
        const valueChange = new CustomEvent('valuechange', {
            bubbles: true,
            detail: {
                value: this.value,
                guid: this.element,
                field: this.fieldName
            }
        });
        this.dispatchEvent(valueChange);
    }

    removeError(event) {
        event.target.classList.remove('slds-has-error');
    }

    connectedCallback() {
        this.fieldName =  this.addressPrefix + this.element.charAt(0).toUpperCase() + this.element.slice(1) + '__c';
        const itemregister = new CustomEvent('privateitemregister', {
            bubbles: true,
            detail: {
                callbacks: {
                    registerDisconnectCallback: this.registerDisconnectCallback,
                    setValue: this.setValue
                },
                guid: this.element,
            }
        });
        this.dispatchEvent(itemregister);
    }


    registerDisconnectCallback(callback) {
        this.disconnectFromParent = callback;
    }

    disconnectFromParent;

    disconnectedCallback() {
        //this.disconnectFromParent(this.element);
    }
}