import { LightningElement, api } from 'lwc';
import {labels} from 'c/labels';

export default class ProductTile extends LightningElement {
    @api button
    @api product
    labels = labels;


    get isAddDetails() {
        return this.button === 'Add Details';
    }

    get isToggle() {
        return this.button === 'Togle';
    }


    get isUtility() {
        return this.product.recordTypeName === 'Utility';
    }

    get isGood() {
        return this.product.recordTypeName === 'Good';
    }

    get isService() {
        return this.product.recordTypeName === 'Service';
    }

    onDetailClick() {
        const selectedEvent = new CustomEvent('selectproduct', {
            detail: {
                action: 'details',
                product: this.product
            }
        });
        const selectedEventLWC = new CustomEvent('selectproduct', {
            detail: {
                action: 'details',
                product: this.product
            }
        });
        this.dispatchEvent(selectedEvent);
        this.dispatchEvent(selectedEventLWC);
    }

    onAddClick() {
        const selectedEvent = new CustomEvent('selectproduct', {
            detail: {
                action: 'add',
                product: this.product
            }
        });
        const selectedEventLWC = new CustomEvent('selectproduct', {
            detail: {
                action: 'add',
                product: this.product
            }
        });
        this.dispatchEvent(selectedEvent);
        this.dispatchEvent(selectedEventLWC);
    }
}