import {LightningElement, api} from 'lwc';

export default class MroWarrantiesNotification extends LightningElement {
    @api recordId;
}