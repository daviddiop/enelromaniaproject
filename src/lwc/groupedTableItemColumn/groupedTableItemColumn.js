import { LightningElement, api } from 'lwc';

export default class GroupedTableItemColumn extends LightningElement {
    @api object
    @api columnKey

    get value() {
        let path = this.columnKey.split('.');
        return path.reduce((obj, key) => ((obj && obj[key] !== 'undefined') ? obj[key] : undefined), this.object);
    }
}