import { LightningElement, api, wire } from "lwc";
import { NavigationMixin } from 'lightning/navigation';
import { labels } from "c/labels";
//import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
//import { error } from 'c/notificationSvc';
import { getRecord } from 'lightning/uiRecordApi';

export default class WizardHeader extends NavigationMixin(LightningElement) {
    label = labels;
    @api accountId ; 
    @api opportunityId;
    @api companyDivisionId; 
    @api dossierId ; 
    @api wizardLabel ;
    @api hideCompanyDivision=false;
    
    //@track account = {};
    //@track opportunity = {};
    //@track dossier = {};
    //@track companyDivisionName;
    //@track isPerson = false;
    //@track isBusiness = false;
    //@track hasRendered = false;

    @wire(getRecord, { recordId: '$accountId', fields: ['Account.Name','Account.IsPersonAccount'] })
    account;

    @wire(getRecord, { recordId: '$opportunityId', fields: ['Opportunity.Name','Opportunity.CompanyDivision__c'] })
    opportunity;

    @wire(getRecord, { recordId: '$dossierId', fields: ['Dossier__c.Name','Dossier__c.CompanyDivision__c'] })
    dossier;

    @wire(getRecord, { recordId: '$companyDivisionId', fields: ['CompanyDivision__c.Name'] })
    companyDivision;

    get companyDivisionName() {
        if(this.companyDivision.data) {
            return this.companyDivision.data.fields.Name.value;
        } else if(this.opportunity.data) {
            return this.opportunity.data.fields.CompanyDivision__c.value;
        } else if(this.dossier.data) {
            return this.dossier.data.fields.CompanyDivision__c.value;
        }
        return null;
    }

    get opportunityName() {
        if(!this.opportunity.data) return null;
        return this.opportunity.data.fields.Name.value;
    }

    get dossierName() {
        if(!this.dossier.data) return null;
        return this.dossier.data.fields.Name.value;
    }

    get accountName() {
        if(!this.account.data) return null;
        return this.account.data.fields.Name.value;
    }

    get isPerson() {
        if(!this.account.data) return false;
        return this.account.data.fields.IsPersonAccount.value || false;
    }

    // connectedCallback() {
    //     this.initialize();
    // }

    // initialize() {
    //     let inputs = {
    //         accountId: this.accountId,
    //         opportunityId: this.opportunityId,
    //         companyDivisionId: this.companyDivisionId,
    //         dossierId: this.dossierId
    //     };

    //     notCacheableCall({
    //         className: "WizardHeaderCnt",
    //         methodName: "initialize",
    //         input: inputs
    //     })
    //         .then(response => {
    //             if (response.isError) {
    //                 error(this, JSON.stringify(response.error));
    //             }
    //             //this.account = response.data.response.account;
    //             //this.opportunity = response.data.response.opportunity;
    //             //this.dossier = response.data.response.dossier;
    //             //this.companyDivisionName = response.data.response.companyDivisionName;
    //             // if (response.data.response.accountPersonRT === this.account.RecordTypeId || response.data.response.accountPersonProspectRT === this.account.RecordTypeId) {
    //             //     this.isPerson = true;
    //             // }
    //             // if (response.data.response.accountBusinessRT === this.account.RecordTypeId || response.data.response.accountBusinessProspectRT === this.account.RecordTypeId) {
    //             //     this.isBusiness = true;
    //             // }
    //         }).catch((errorMsg) => {
    //             error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
    //         });
    // }

    navigateToAccount() {
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.accountId,
                "objectApiName": "Account",
                "actionName": "view"
            }
        });
    }
}