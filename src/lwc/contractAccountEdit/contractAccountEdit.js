/**
 * Created by tomasoni on 08/08/2019.
 */
import {LightningElement, api, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import {NavigationMixin} from 'lightning/navigation';
import {error} from 'c/notificationSvc';
import {labels} from 'c/labels';

export default class contractAccountEdit extends NavigationMixin(LightningElement) {
    contractAccount;
    labels = labels;
    @api contractAccountRecordId;
    @api accountId;
    @api buttonType;
    @api showHeader = false;
    @track showModalContract = false;
    @api readOnly = false;
    @track billingProfileId = '';
    @track disabled = true;
    @track show = false;
    @api companyDivisionId;
    @track continueHandleSuccess;
    @track showLoadingSpinner = true;

    connectedCallback() {
        this.showLoadingSpinner = false;
        if (this.buttonType === 'new') {
            this.contractAccountRecordId = '';
            this.billingProfileId = '';
        }
        this.show = true;
    }

    handleLoadCA() {
        if ((this.buttonType !== 'new')&&  (this.billingProfileId == '')) {
            let inputCmp = this.template.querySelector('[data-id="myBillingProfileId"]');
            this.billingProfileId = inputCmp.value;
            this.show = true;
        }
    }

    get hasBillingProfileIdLoaded() {
        return (this.buttonType === 'new') || ((this.buttonType !== 'new') && (this.billingProfileId !== ''));
    }

    get hasAccountId() {
        return (this.accountId != null);
    }

    handleCancel() {
        this.dispatchEvent(new CustomEvent('contractaccounteditclose'));
    }
    handleErrorCA(event) {
        error(this, event.detail.message);
        this.showLoadingSpinner = false;
    }
    handleSuccessCA(event) {
        event.preventDefault();
        event.stopPropagation();
        let payload = event.detail;
        const selectedEvent = new CustomEvent('contractaccounteditsave', {
            detail: {
                contractAccountEditId: payload.id,
                showModalContAcc: this.showModalContract

            }
        });
        this.dispatchEvent(selectedEvent);


        this.showLoadingSpinner = false;

        this.showModalContract = false;
    }

    handleSubmitCA(event) {

        event.preventDefault();
        event.stopPropagation();
        this.showLoadingSpinner = true;
        const fields = event.detail.fields;
        if (this.validateFields()) {
            if (!this.billingProfileId) {
                error(this, 'Billing Profile is not exist');
                this.showLoadingSpinner = false;
                return;
            }
            fields.BillingProfile__c = this.billingProfileId;
            fields.CompanyDivision__c = this.companyDivisionId;

            if (this.buttonType === 'clone') {  //Should be EDIT BG
                this.contractAccountRecordId = '';
                fields.Account__c = this.accountId;
                this.template.querySelector('[data-id="contractAccountEditForm"]').recordId = this.contractAccountRecordId;
            }
            this.template.querySelector('[data-id="contractAccountEditForm"]').submit(fields);
            this.showModalContract = true;

        }else{

            this.showLoadingSpinner = false;
        }

    }

    getBillingProfileRecordId(event) {
        event.stopPropagation();
        this.showModalContract = event.detail.showModalContract;
        this.billingProfileId = event.detail.billingProfileRecordId;
    }

    getEditButton (event) {
      event.stopPropagation();
      this.showModalContract = event.detail.showModalContract;
    }

    removeError(event) {
        let inputCmp = event.target;

        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        return areValid;
    };
}