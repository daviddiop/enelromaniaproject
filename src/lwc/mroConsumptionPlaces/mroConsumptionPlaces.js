/**
 * Created by Boubacar Sow on 16/10/2020.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';
import {NavigationMixin} from "lightning/navigation";
import {getRecord} from 'lightning/uiRecordApi';

export default class MroConsumptionPlaces extends LightningElement {

    @api username ;
    @api hideCheckBox = false;
    @api hideRetrieveConsumptionPlaces = false;

    @track consumptionPlacesList;
    @track showLoadingSpinner = false;
    @track showIllustrationEmptyStat = false;

    labels = labels;
    consumptionPlacesColumns = [
        {label: this.labels.clientCode, fieldName: 'clientCode', type: 'text', sortable: true, fixedWidth:200},
        {label: this.labels.paymentCode, fieldName: 'paymentCode', type: 'text', sortable: true, fixedWidth:200},
        {label: this.labels.enelTel, fieldName: 'enelTel', type: 'text', sortable: true, fixedWidth:200},
        {label: this.labels.address, fieldName: 'address', type: 'text', sortable: true, wrapText:true}
    ];

    connectedCallback() {
        this.hideCheckBox = true;
        this.handleRetreiveConsumptionPlaces();
    }

    handleRetreiveConsumptionPlaces(){
        this.showLoadingSpinner = true;
        let input = {
            'username': this.username
        }
        notCacheableCall({
            className: "MRO_LC_ConsumptionPlaces",
            methodName: "listConsumptionPlaces",
            input: input
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, this.labels.unexpectedError);
            }
            console.log('#### ConsumptionPlaces data '+JSON.stringify(apexResponse.data));
            if (apexResponse.data.error) {
                error(this, apexResponse.data.errorMsg);
            }
            if (!apexResponse.data.error && apexResponse.data.consumptionPlaces){
                this.consumptionPlacesList = JSON.parse(JSON.stringify(apexResponse.data.consumptionPlaces));
            }
            this.showIllustrationEmptyStat = !this.consumptionPlacesList || this.consumptionPlacesList.length === 0;
        }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg));
        });

    }

}