import {LightningElement, track, api} from 'lwc';
import {labels} from 'c/mroLabels';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error} from 'c/notificationSvc';
import {checkServicePointEleCode, checkServicePointGasCode} from "c/mroValidations";

export default class MroServicePointSearch extends LightningElement {
    label = labels;
    @track servicePointCode = '';
    @api companyDivisionId;
    @api disabled = false;
    @api accountId;
    @api commodity;

    @api
    resetBox() {
        let inputCmp = this.template.querySelector('[data-id="searchBox"]');
        inputCmp.value = '';
        this.removeError();

    }

    handleChange(event) {
        event.target.classList.remove('slds-has-error');
        this.servicePointCode = event.target.value;
        this.removeError();
    }

    findServicePoint() {
        let inputCmp = this.template.querySelector('lightning-input');
        let value = inputCmp.value;

        if (this.servicePointCode.length === 0) {
            error(this, this.label.requiredFields);
            return;
        }
        /*
        else if (this.servicePointCode.length < 14) {
            error(this, this.label.shortEntry);
            this.validateServicePoint();
            return;
        }
        */

        if (!this.commodity && !checkServicePointGasCode(value) && !checkServicePointEleCode(value)) {
            error(this, this.label.invalidCode);
            inputCmp.classList.add('slds-has-error');
            return;
        } else if (this.commodity === 'Electric' && !checkServicePointEleCode(value)) {
            error(this, this.label.invalidCode);
            inputCmp.classList.add('slds-has-error');
            return;
        } else if (this.commodity === 'Gas' && !checkServicePointGasCode(value)) {
            error(this, this.label.invalidCode);
            inputCmp.classList.add('slds-has-error');
            return;
        } else {
            inputCmp.setCustomValidity("");
        }
        //inputCmp.setCustomValidity("");
        inputCmp.reportValidity();
        if (!inputCmp.reportValidity()) {
            return;
        }

        this.searchPoint();
    }

    searchPoint = () => {
        let inputs = {pointCode: this.servicePointCode, accountId: this.accountId};

        notCacheableCall({
            className: 'MRO_LC_ServicePointSearch',
            methodName: 'findCode',
            input: inputs
        })
            .then((response) => {
                const selectedEvent = new CustomEvent(
                    "selected",
                    {
                        detail: {
                            servicePointCode: this.servicePointCode,
                            servicePointId: response.data.servicePointId,
                            servicePoint: response.data.servicePoint,
                            suppliesByServicePointCode: response.data.suppliesByServicePointCode
                        }
                    }
                );
                this.servicePointCode = '';
                this.dispatchEvent(selectedEvent);
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    };

    removeError = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.remove('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };
}