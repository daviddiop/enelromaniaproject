import { LightningElement, api } from 'lwc';
import { labels } from 'c/labels';

export default class IndividualListv extends LightningElement {
    @api searchFields;
    @api interlocutorList;
    @api interactionId;

    labels = labels;

    onNew() {
        this.dispatchEvent(new CustomEvent('redirect', {
            detail: {
                'page': 'individualEdit',
                'searchFields': this.searchFields
        }}));
    }
    onBack() {
        this.dispatchEvent(new CustomEvent('redirect', {
            detail: {
                'page': 'individualSearch',
                'searchFields': this.searchFields
        }}));
    }
    onSelect(event) {
        this.dispatchEvent(new CustomEvent('select', {
            detail: {
                'individual': event.detail.individual
            }
        }));
    }
}