import {LightningElement, api} from 'lwc';
import {labels} from 'c/labels';

export default class MroOpportunityLineItemTile extends LightningElement {
    @api recordId;
    labels = labels;
    @api disabled = false;
    @api selectable = false;

    @api getSelectedProducts() {
        let products = this.template.querySelectorAll(`input[name="products"]:checked`);
        let selectedProducts = [];
        products.forEach((element) => {
            selectedProducts.push(element.id);
        });
        console.log(selectedProducts);
        return selectedProducts;
    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }
    removeOppLineItem(event) {
        const removeEvent = new CustomEvent('remove', {
            detail: {
                id: this.recordId
            }
        });
        this.dispatchEvent(removeEvent);
    }

    handleOliCheck(event) {
        this.getSelectedProducts();
    }
}
