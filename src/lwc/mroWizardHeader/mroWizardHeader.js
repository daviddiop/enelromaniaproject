import {LightningElement, api, track} from "lwc";
import {NavigationMixin} from 'lightning/navigation';
import {labels} from "c/labels";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {error} from 'c/notificationSvc';

export default class MroWizardHeader extends NavigationMixin(LightningElement) {
    label = labels;
    @api accountId;
    @api opportunityId;
    @api companyDivisionId;
    @api dossierId;
    @api wizardLabel;
    @api hideCompanyDivision = false;

    @api
    get selectedOrigin() {
        return this._selectedOrigin;
    }

    set selectedOrigin(value) {
        this._selectedOrigin = value;
    }

    @api
    get selectedChannel() {
        return this._selectedChannel;
    }

    set selectedChannel(value) {
        this._selectedChannel = value;
    }

    @api
    get disableOriginChannel() {
        return this._disableOriginChannel;
    }

    set disableOriginChannel(value) {
        this._disableOriginChannel = value;
    }

    @track account = {};
    @track opportunity = {};
    @track dossier = {};
    @track originList;
    @track channelList;
    @api disabledOrigin;
    @api disabledChannel;
    @track companyDivisionName;
    @track isPerson = false;
    @track isBusiness = false;
    @track hasRendered = false;
    @track _selectedOrigin = '';
    @track _selectedChannel = '';
    @track _disableOriginChannel;
    @track originFromInteraction;

    @api
    disableOriginChannelComponent() {
        if (this.template.querySelector('c-mro-origin-channel-selection')) {
            this.template.querySelector('c-mro-origin-channel-selection').disableOriginChannel();
        }
    }

    @api
    enableChannelComponent() {
        if (this.template.querySelector('c-mro-origin-channel-selection')) {
            this.template.querySelector('c-mro-origin-channel-selection').enableChannel();
        }
    }

    @api
    enableOriginChannelComponent() {
        if (this.template.querySelector('c-mro-origin-channel-selection')) {
            this.template.querySelector('c-mro-origin-channel-selection').enableOriginChannel(this.originFromInteraction);
        }
    }

    @api
    disableOriginOption() {
        console.log('MroWizardHeader.disableOriginOption - Enter');
        if (this.template.querySelector('c-mro-origin-channel-selection')) {
            this.template.querySelector('c-mro-origin-channel-selection').disableOrigin();
        }
    }


    connectedCallback() {
        this.initialize();
    }

    renderedCallback() {
        console.log('MroWizardHeader.renderedCallback - _disableOriginChannel', this._disableOriginChannel);
        if (this._disableOriginChannel) {
            this.disableOriginChannelComponent();
        } else if (this.disabledOrigin) {
            this.disableOriginOption();
            if(!this.disabledChannel) {

                this.enableChannelComponent();
            }
        } else {
            this.enableOriginChannelComponent();
        }
    }

    initialize() {
        let inputs = {
            accountId: this.accountId,
            opportunityId: this.opportunityId,
            companyDivisionId: this.companyDivisionId,
            dossierId: this.dossierId
        };

        notCacheableCall({
            className: "MRO_LC_WizardHeader",
            methodName: "initialize",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse.isError) {
                    error(this, JSON.stringify(apexResponse.error));
                }
                //Spring 20 Apex response non-extensible with additional parameters.
                let result = Object.assign({}, apexResponse.data);
                this.account = result.response.account;
                this.opportunity = result.response.opportunity;
                this.dossier = result.response.dossier;
                this.companyDivisionName = result.response.companyDivisionName;
                let originSelected = result.response.originSelected;
                let channelSelected = result.response.channelSelected;

                if (originSelected && !this._selectedOrigin) {
                    this._selectedOrigin = originSelected;
                }
                if (channelSelected && !this._selectedChannel) {
                    this._selectedChannel = channelSelected;
                }
                if (originSelected && channelSelected) {
                    this.addOriginChannelSelectionEvt(originSelected, channelSelected);
                }
                this.originFromInteraction = result.response.originFromInteraction;

                if (result.response.accountPersonRT === this.account.RecordTypeId || result.response.accountPersonProspectRT === this.account.RecordTypeId) {
                    this.isPerson = true;
                }
                if (result.response.accountBusinessRT === this.account.RecordTypeId || result.response.accountBusinessProspectRT === this.account.RecordTypeId) {
                    this.isBusiness = true;
                }
            }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg));
        });
    }

    handleSelectionOriginChannel(event) {
        this.addOriginChannelSelectionEvt(event.detail.selectedOrigin, event.detail.selectedChannel);
    }

    addOriginChannelSelectionEvt(originSelected, channelSelected) {
        const originChannelEvt = new CustomEvent('onselectoriginchannel', {
            detail: {
                'selectedOrigin': originSelected,
                'selectedChannel': channelSelected
            }
        });
        this.dispatchEvent(originChannelEvt);
    }

    navigateToAccount() {
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.accountId,
                "objectApiName": "Account",
                "actionName": "view"
            }
        });
    }
}
