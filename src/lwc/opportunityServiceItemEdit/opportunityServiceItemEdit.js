import {LightningElement, api, track} from 'lwc';
import getConstants from '@salesforce/apex/Constants.getAllConstants';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import init from '@salesforce/apex/OpportunityServiceItemCnt.initialize';
import getAvailableAddresses from '@salesforce/apex/AddressService.getAvailableAddresses';
import {labels} from 'c/labels';
import {error} from 'c/notificationSvc';

export default class OpportunityServiceItemEdit extends LightningElement {
    label = labels;

    REQUIRED_FIELDS_EE = ['ServicePointCode__c', 'Distributor__c', 'Market__c', 'ContractualPower__c', 'EstimatedConsumption__c', 'Trader__c', 'AvailablePower__c', 'Distributor__c', 'PowerPhase__c', 'Voltage__c', 'VoltageLevel__c'];
    REQUIRED_FIELDS_GM = ['ServicePointCode__c', 'Distributor__c', 'Market__c', 'ContractualPower__c', 'EstimatedConsumption__c', 'Trader__c', 'AvailablePower__c', 'Distributor__c', 'ConversionFactor__c', 'Pressure__c', 'PressureLevel__c'];
    
    @api supplyId;
    @api servicePointId;
    @api servicePointCode;
    
    @api opportunityId;
    @api accountId;

    @api recordId;
    @api market;
    @api serviceSite;
    @api contract;
    @api product;
    @api contractAccount;
    @api contractId;

    @api readOnlyAddress = false;
    @api readOnlySiteAddress = false;
    @api autoSave = false;
    @api addressForced = false;
    @api addressForcedSite = false;
    @api title;

    @track isNormalize;
    @track isSavedSS = false;
    @track defaultTitle;


    @track recordTypeId;
    electricRecordTypeId;
    gasRecordTypeId;

    //@track show
    @track spinner;
    @track loads = true;
    @track show = false;

    @track isGas = false;
    @track isElectric = false;

    @track address = [];
    @track siteAddressPickList = [];
    @track serviceSiteAddress = [];
    @track serviceSiteAddressChange = [];
    @track selectedOption;
    @track showNewServiceSite;
    @track objName;
    @track checkedSite = false;
    @track serviceSiteField;
    @track siteAddressKeyField;
    @track isSelected;
    @track allSS = {};
    @track allOSI = {};
    @track selectedServiceSite = false;
    @track isCheckedPA = false;
    @track isCheckedSS = false;
    @track submitErrorMessage;
    @track availableAddresses;

    servicePoint;
    firstTimeEntry = true;
    @track successBoolean = true;

    tmpOsi;

    connectedCallback() {
        //this.spinner = true;
        this.defaultTitle = this.title ?  this.title : this.label.createOsi;
        this.isNormalize = true;

        if (this.recordId) {
            this.initOsiAddress();
        } else if(this.supplyId) {
            this.initialize();
        }

        if (this.servicePointId) {
            this.initServicePoint();
        }

        if (this.accountId) {
            this.initSiteAddress();
            getAvailableAddresses({
                accountId: this.accountId
            }).then((response)=>{
                this.availableAddresses = response;
            }).catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
        }
    }

    handleError(event) {
        error(this, event.detail.message);
        this.spinner = false;

    }

    initialize(){
        init({
            supplyId : this.supplyId,
            servicePointId : this.servicePointId,
            servicePointCode : this.servicePointCode,
            opportunityId : this.opportunityId,
            accountId : this.accountId 
        }).then((response) => {
            this.tmpOsi = response.osi;
            this.recordTypeId = this.tmpOsi.RecordTypeId;
            this.serviceSite = this.tmpOsi.ServiceSite__c;
            this.selectedOption = this.serviceSite;
            this.isSelected = this.serviceSite;

            if(response.recordType === 'Gas'){
                this.isGas = true;
            } else if (response.recordType === 'Electric') {
                this.isElectric = true;
            }
            this.address = response.osiAddress;
            this.show = true;
        }).catch((errorMsg) => {
            error(this, errorMsg.body.message);
        });
    }

    initOsiAddress() {
        let inputs = {osiId: this.recordId};
        notCacheableCall({
            className: 'OpportunityServiceItemCnt',
            methodName: 'getOsiAddressField',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            let addr = {};
            addr = response.data.osiAddress;
            this.address = addr;
            this.show = true;
            // start add by david
            if (addr.addressNormalized) {
                this.readOnlyAddress = true;
                this.addressForced = false;
            }
            //end add by david
            this.spinner = false;
        })
        .catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            this.spinner = false;
        });
    }

    initSiteAddress() {
        let inputs = {
            accountId: this.accountId,
            recordId: this.recordId
        };
        notCacheableCall({
            className: 'OpportunityServiceItemCnt',
            methodName: 'getListOSIServiceSite',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }

            let addr = [];
            addr = response.data.serviceSitePicklistValues;
            this.siteAddressPickList = addr;
            this.allOSI = response.data.allOSI;
            this.allSS = response.data.allSS;
            this.serviceSiteField = response.data.serviceSiteField;
            this.siteAddressKeyField = response.data.siteAddressKeyField;
            this.selectedServiceSite = false;
            this.spinner = false;
        }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            this.spinner = false;
        });
    }

    initServicePoint() {
        let inputs = {servicePointId: this.servicePointId};
        notCacheableCall({
            className: 'OpportunityServiceItemCnt',
            methodName: 'getPointCodeFields',
            input: inputs
        }).then((response) => {
            this.servicePoint = response.data.servicePoint;
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            let addr = {};
            addr = response.data.servicePointAddress;
            this.address = addr;
            //start add by david
            if (addr.addressNormalized) {
                this.readOnlyAddress = true;
                this.addressForced = false;
            }
            //end add by david
            this.show = true;
            this.spinner = false;
        })
        .catch((errorMsg) => {
            this.spinner = false;
            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));

        });
    }

    getObjectName(objId) {
        let inputs = {objId: objId};
        notCacheableCall({
            className: 'OpportunityServiceItemCnt',
            methodName: 'objectAPIName',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            this.objName = response.data.objName;
            this.spinner = false;
        }).catch((errorMsg) => {
            this.spinner = false;
            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));

        });
    }

    renderedCallback() {
        //this.isSavedSS = false;
        //if (this.recordId) {

            this.selectedServiceSite = true;
            if ((this.selectedOption === 'new') && (this.isSavedSS)) {
                this.isSelected = 'new';
            } else if ((this.siteAddressKeyField) && (this.siteAddressKeyField !== this.recordId)) {
                this.isSelected = this.siteAddressKeyField;

            } else if ((this.serviceSiteField) && (this.serviceSiteField !== this.recordId)) {
                this.isSelected = this.serviceSiteField;
            } // else if ((this.recordId === this.siteAddressKeyField)) {
            //     this.isSelected = 'new';
            // }

            let inputs = this.template.querySelectorAll('input');
            inputs.forEach(inputElement => {
                inputElement.checked = ((inputElement.value === this.isSelected));
            });
        //}
        getConstants().then(response => {
            this.electricRecordTypeId = response.OSIRecordTypes.Electric;
            this.gasRecordTypeId = response.OSIRecordTypes.Gas;
            let gmPatt = /^(\d{14})$/i;
            let eePatt = /^(\w{2}\d{3}\w{1}\d{8})$/i;
            if (gmPatt.test(this.servicePointCode)) {
                this.isGas = true;
                this.recordTypeId = this.gasRecordTypeId;
            } else if (eePatt.test(this.servicePointCode)) {
                this.isElectric = true;
                this.recordTypeId = this.electricRecordTypeId;
            }
            this.show = true;
        })
    }

    handleLoad() {
        if (this.loads) {
            if (!this.servicePointCode) {
                let inputCmp = this.template.querySelector('[data-id="myServicePointCode"]');
                this.servicePointCode = inputCmp.value;
                let gmPatt = /^(\d{14})$/i;
                let eePatt = /^(\w{2}\d{3}\w{1}\d{8})$/i;

                if (gmPatt.test(this.servicePointCode)) {
                    this.isGas = true;
                } else if (eePatt.test(this.servicePointCode)) {
                    this.isElectric = true;
                }
            }

            if(this.tmpOsi){
                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    element.value = this.tmpOsi[element.fieldName] ? this.tmpOsi[element.fieldName] : undefined;
                });
            } else if(this.servicePoint){
                let fieldsList = Array.from(this.template.querySelectorAll('lightning-input-field'));
                fieldsList.forEach(element => {
                    let str = element.fieldName;
                    element.value = this.servicePoint[str.slice(0, str.length-3)] ? this.servicePoint[str.slice(0, str.length-3)] : undefined;
                });
            } else {
                this.template.querySelector('[data-id="myServicePointCode"]').value = this.servicePointCode;
            }

            if ((this.autoSave) && (this.firstTimeEntry)) {
                this.spinner = false;
                let fieldList = {};

                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    fieldList[element.fieldName] = element.value;
                });

                fieldList.Account__c = this.accountId;
                fieldList.Opportunity__c = this.opportunityId;
                fieldList.ServicePoint__c = this.servicePointId;
                fieldList.Market__c = this.market;
                fieldList.ServiceSite__c = this.serviceSite;
                fieldList.ContractAccount__c = this.contractAccount;
                fieldList.Product__c = this.product;
                fieldList.PointAddressNormalized__c = true;

                let addrForm = this.template.querySelector('div.pointAddressData c-address-form');
                let addrFields = addrForm.getValues();
                for (let f in addrFields) {
                    if (addrFields.hasOwnProperty(f)) {
                        fieldList[f] = addrFields[f];
                    }
                }

                if (this.successBoolean) {
                    if (fieldList.Distributor__c != null && fieldList.Trader__c != null) {
                        this.template.querySelector('lightning-record-edit-form').submit(fieldList);
                        this.firstTimeEntry = false;
                    }
                }
            }
        }
    }

    handleSuccess(event) {
        //this.isSavedSS = false;
        if (this.successBoolean) {
            event.preventDefault();
            this.spinner = true;
            let payload = event.detail;
            this.dispatchEvent(new CustomEvent('save', {
                detail: {
                    'opportunityServiceItemId': payload.id,
                    'opportunityServiceItem': payload,
                    'contractId': this.contractId
                }

            }, {bubbles: true}));

            if (this.selectedOption === 'new') {
                let inputs = {osiId: payload.id};
                notCacheableCall({
                    className: 'OpportunityServiceItemCnt',
                    methodName: 'setOsiIdToSiteAddressKey',
                    input: inputs
                }).then((response) => {
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    }
                    //this.spinner = false;
                }).catch((errorMsg) => {
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                    this.spinner = false;
                });
            }

        }
        if (this.autoSave) {
            this.successBoolean = false;
        }
    }

    getAddressFields() {
        this.selectedServiceSite = true;
        let addrForm = this.template.querySelector('div.serviceSiteAddressData c-address-form');
        if (!addrForm.isValid()) {
            error(this, this.label.validateAddress); //'Address must be verified');
            return;
        }

        let fields = {};
        let addrFields = addrForm.getValues();
        for (let f in addrFields) {
            if (addrFields.hasOwnProperty(f)) {
                fields[f] = addrFields[f];
            }
        }
        this.serviceSiteAddress = fields;

        let list = [];
        let valueSetValues = this.serviceSiteAddress;
        Object.keys(valueSetValues).forEach(function (val) {
            list.push(valueSetValues[val])
        });

        this.siteAddressPickList[0].label = list.join(' ');
        fields.SiteAddressNormalized__c = this.addressForcedSite === false;
        this.showNewServiceSite = false;
        this.isSavedSS = true;
    }

    handleSelectOption(event) {
        this.isCheckedSS = false;
        let supplyInputs = this.template.querySelectorAll('input');
        supplyInputs.forEach(inputElement => {
            inputElement.checked = ((inputElement.value === event.target.value));
        });
        this.selectedOption = event.target.value;

        if (this.selectedOption && (this.selectedOption !== 'new')) {
            //this.readOnlyAddress = false;
            this.selectedServiceSite = true;
            this.getObjectName(this.selectedOption);
        }

        if (this.serviceSiteAddress.SiteAddressNormalized__c) {
            this.readOnlySiteAddress = true;
            this.addressForcedSite = false;
        } else {
            this.readOnlySiteAddress = false;
        }

        let list = [];
        let valueSetValues = this.serviceSiteAddress;

        Object.keys(valueSetValues).forEach(function (val) {
            list.push('"' +
                val.replace("__c", "").substring(4).charAt(0).toLowerCase() +
                val.replace("__c", "").substring(4).slice(1) +
                '":' +
                '"' +
                valueSetValues[val] +
                '"'
            );
        });

        this.serviceSiteAddressChange = JSON.parse('{' + list + '}');
        this.showNewServiceSite = (this.selectedOption === 'new');
        if (this.selectedOption === 'new') {
            if ((this.recordId) && (this.recordId === this.siteAddressKeyField)) {
                let inputs = {osiId: this.recordId};
                notCacheableCall({
                    className: 'OpportunityServiceItemCnt',
                    methodName: 'getServiceSiteAddressField',
                    input: inputs
                }).then((response) => {
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    }
                    let addr = {};
                    addr = response.data.serviceSiteAddress;
                    this.serviceSiteAddressChange = addr;
                    if (addr.addressNormalized) {
                        this.readOnlySiteAddress = true;
                        this.addressForcedSite = false;
                    }

                    this.spinner = false;
                    this.isSavedSS = false;
                }).catch((errorMsg) => {
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                    this.spinner = false;
                });
            } else if ((JSON.stringify(this.serviceSiteAddressChange) === "{}") && (this.address)) {
                this.isSavedSS = false;
                this.serviceSiteAddressChange = this.address;
                if (this.address.addressNormalized) {
                    this.readOnlySiteAddress = true;
                    this.addressForcedSite = false;
                }
            }
        }
        this.loads = false;
    }

    closeModal() {
        let serviceSiteInputs = this.template.querySelectorAll('input');
        serviceSiteInputs.forEach(inputElement => {
            inputElement.checked = ((this.isSavedSS) && (inputElement.value === this.selectedOption));
        });

        this.showNewServiceSite = false;
    }

    handleCancelServiceSite() {
        let serviceSiteInputs = this.template.querySelectorAll('input');
        serviceSiteInputs.forEach(inputElement => {
            inputElement.checked = ((this.isSavedSS) && (inputElement.value === this.selectedOption));
        });

        this.showNewServiceSite = false;

    }

    handleSubmit(event) {
        this.spinner = true;
        this.submitErrorMessage = '';

        event.preventDefault();
        let fieldList = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fieldList[element.fieldName] = element.value;
        });

        let addrForm = this.template.querySelector('div.pointAddressData c-address-form');

        if (!addrForm.isValid()) {
            this.isCheckedPA = true;
            this.submitErrorMessage = this.submitErrorMessage + this.label.addressMustBeVerified;
            this.spinner = false;
            this.loads = false;
        }

        let addrFields = addrForm.getValues();
        for (let f in addrFields) {
            if (addrFields.hasOwnProperty(f)) {
                fieldList[f] = addrFields[f];
            }
        }

        let serviceSiteFields = this.serviceSiteAddress;
        if (this.selectedOption === 'new') {
            if (serviceSiteFields) {
                for (let ss in serviceSiteFields) {
                    if (serviceSiteFields.hasOwnProperty(ss)) {
                        fieldList[ss] = serviceSiteFields[ss];
                    }
                }
            }
        }

        if (this.selectedOption && (this.selectedOption !== 'new')) {
            if (this.objName === 'ServiceSite__c') {
                if (this.allSS) {
                    if (this.allSS.hasOwnProperty(this.selectedOption)) {
                        for (let SS in this.allSS[this.selectedOption]) {
                            if (this.allSS[this.selectedOption].hasOwnProperty(SS)) {
                                fieldList[SS] = this.allSS[this.selectedOption][SS];
                            }
                        }
                    }
                }
                fieldList.SiteAddressKey__c = '';
                fieldList.ServiceSite__c = this.selectedOption;

            }
            if (this.objName === 'OpportunityServiceItem__c') {
                if (this.allOSI) {
                    if (this.allOSI.hasOwnProperty(this.selectedOption)) {
                        for (let osi in this.allOSI[this.selectedOption]) {
                            if (this.allOSI[this.selectedOption].hasOwnProperty(osi)) {
                                fieldList[osi] = this.allOSI[this.selectedOption][osi];
                            }
                        }
                    }
                }
                fieldList.SiteAddressKey__c = this.selectedOption;
                fieldList.ServiceSite__c = null;
            }
        }

        fieldList.Account__c = this.accountId;
        fieldList.Opportunity__c = this.opportunityId;
        fieldList.ServicePoint__c = this.servicePointId;
        if(this.tmpOsi) {
            fieldList.ServicePoint__c = this.tmpOsi.ServicePoint__c;
        }
        //start add by David
        fieldList.PointAddressNormalized__c = this.addressForced === false;
        // end add by David

        if (!this._validateFields(fieldList)) {
            this.spinner = false;
            if (this.submitErrorMessage !== '') {
                this.submitErrorMessage = this.submitErrorMessage + ' - ';
            }
            
            this.submitErrorMessage = this.submitErrorMessage + this.label.requiredFields;
            this.loads = false;
        }

        if (this.submitErrorMessage !== '') {
            this.spinner = false;
            error(this, this.submitErrorMessage);
            return;
        }
        this.template.querySelector('lightning-record-edit-form').submit(fieldList);
    }

    get addClassAutoSave() {
        if (this.autoSave) {
            return 'slds-hide';
        }
        return '';
    }

    _validateFields(fieldList) {
        let labelFieldList = [];
        for (let i in fieldList) {
            if (fieldList.hasOwnProperty(i)) {
                if (this.isElectric) {
                    if (this.REQUIRED_FIELDS_EE.includes(i) && (
                        !fieldList[i] ||
                        (
                            (typeof fieldList[i] === 'string' || fieldList[i] instanceof String) &&
                            fieldList[i].trim() === ''
                        )
                    )
                    ) {
                        labelFieldList.push(i);
                    }
                }
                if (this.isGas) {
                    if (this.REQUIRED_FIELDS_GM.includes(i) && (
                        !fieldList[i] ||
                        (
                            (typeof fieldList[i] === 'string' || fieldList[i] instanceof String) &&
                            fieldList[i].trim() === ''
                        )
                    )
                    ) {
                        labelFieldList.push(i);
                    }
                }
            }
        }
        if (!this.selectedServiceSite) {
            this.isCheckedSS = true;
            //this.isCheckedPA = false;
            if (this.submitErrorMessage !== '') {
                this.submitErrorMessage = this.submitErrorMessage + ' - '
            }
            this.submitErrorMessage = this.submitErrorMessage + this.label.requiredServiceSite;
        }
        if (labelFieldList.length !== 0) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (labelFieldList.includes(element.fieldName)) {
                    element.classList.add('slds-has-error');
                }
            });
            return false;
        }
        return true;
    }

    removeError(event) {
        let inputField = event.target;
        inputField.classList.remove('slds-has-error');
    }

    handleCancel() {
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    disabledSaveButton(event) {
        this.isNormalize = event.detail.isnormalize;
        this.isCheckedPA = false;
    }

    disabledSaveButtonPointAddress(event) {
        this.addressForced = event.detail.addressForced;
        this.isCheckedPA = false;
        let addrForm = this.template.querySelector('div.pointAddressData c-address-form');

        let list = [];
        let valueSetValues = addrForm.getValues();

        Object.keys(valueSetValues).forEach(function (val) {
            list.push('"' +
                val.replace("__c", "").substring(5).charAt(0).toLowerCase() +
                val.replace("__c", "").substring(5).slice(1) +
                '":' +
                '"' +
                valueSetValues[val] +
                '"'
            );
        });

        this.address = JSON.parse('{' + list + '}');

        this.address.addressNormalized = event.detail.isnormalize;
    }

    disabledSaveButtonSiteAddress(event) {
        this.isNormalize = event.detail.isnormalize;
        this.isCheckedSS = false;
        this.addressForcedSite = event.detail.addressForced;
    }

    get addClassSS() {
        if (this.isCheckedSS) {
            return 'slds-box slds-grid slds-col slds-wrap slds-m-right_small slds-m-top_small wr-boxBorder';
        }
        return 'slds-box slds-grid slds-col slds-wrap slds-m-right_small slds-m-top_small';
    }

    get addClassPA() {
        if (this.isCheckedPA) {
            return 'slds-box slds-grid slds-wrap slds-m-left_small slds-m-top_small slds-size_4-of-7 wr-boxBorder';
        }
        return 'slds-box slds-grid slds-wrap slds-m-left_small slds-m-top_small slds-size_4-of-7';
    }
}