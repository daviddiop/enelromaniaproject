import { LightningElement, api, track } from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { labels } from 'c/mroLabels';
import { error } from 'c/notificationSvc';

export default class MroAccountSearch extends LightningElement {
    @api interactionId;
    @api searchFields;
    @api accountSearchType = 'Customer';

    @track spinner = false;

    @track showLoadingSpinner = true;
    labels = labels;
    @track vatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;

    onLoad(){
        if (this.searchFields) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = this.searchFields[element.fieldName];
            });
            if (this.searchFields.eneltel) {
                this.template.querySelector('[data-id="ENELTEL__c"]').value = this.searchFields.eneltel;
            }
            if (this.searchFields.servicePointCode) {
                this.template.querySelector('[data-id="Code__c"]').value = this.searchFields.servicePointCode;
            }
            if (this.searchFields.billingAccountNumber) {
                this.template.querySelector('[data-id="BillingAccountNumber__c"]').value = this.searchFields.billingAccountNumber;
            }
        } else {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = '';
            });
        }
        this.showLoadingSpinner = false;
    }
    handleError(event) {
        error(this, event.detail.message);
        this.showLoadingSpinner = false;
    }
    handleSubmit(event) {
        this.spinner = true;
        event.preventDefault();

        let accountObj = {
            'Name': this.template.querySelector('[data-id="Name"]').value,
            'Email__c': this.template.querySelector('[data-id="Email__c"]').value,
            'Key__c': this.template.querySelector('[data-id="Key__c"]').value,
            'Phone': this.template.querySelector('[data-id="Phone"]').value,
            'AccountNumber': this.template.querySelector('[data-id="AccountNumber"]').value,
            'eneltel': this.template.querySelector('[data-id="ENELTEL__c"]').value,
            'servicePointCode': this.template.querySelector('[data-id="Code__c"]').value,
            'billingAccountNumber': this.template.querySelector('[data-id="BillingAccountNumber__c"]').value
        };
        if (this.removeExtraFieldsAndCheckRequired(accountObj)) {
            this.showLoadingSpinner = false;
            this.spinner = false;
            error(this, labels.noDataEntered);
            return;
        }
        if(this.accountSearchType == 'Institution'){
            accountObj['RecordTypeDeveloperName'] = 'Institution'
        }
        notCacheableCall({
            className: 'MRO_LC_Account',
            methodName: 'searchAccount',
            input: accountObj
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            } else {
                this.dispatchEvent(new CustomEvent('search', {
                    detail: {
                        'accountList': response.data,
                        'account': accountObj,
                        'searchType': this.accountSearchType
                    }
                }));
            }
        }).catch((errorMsg) => {
            error(this, errorMsg.body.output.errors[0].message);
            this.spinner = false;
        });
    }
    removeExtraFieldsAndCheckRequired(obj) {
        let fields = this.listAccountField();
        for (let prop in obj) {
            if (fields.indexOf(prop) === -1) {
                obj[prop] = undefined;
            } else {
                let value = obj[prop];
                if (prop.endsWith('__c')) {
                    let newFieldName = prop.replace('__c', '');
                    obj[newFieldName] = value;
                }
            }
        }
        for (let field of fields) {
            if (obj.hasOwnProperty(field) && obj[field]) {
                let value = obj[field];
                if (typeof value === 'string' && value.trim()) {
                    return false;
                } else if (typeof value !== 'string') {
                    return false;
                }
            }
        }
        return true;
    }
    listAccountField () {
        return ["Name", "Email__c", "Key__c", "Phone", "AccountNumber", "eneltel", "servicePointCode", "billingAccountNumber"];
    }
    doNothing(event) {
        event.preventDefault();
        event.stopPropagation();
    }
}