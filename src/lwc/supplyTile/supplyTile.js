import { LightningElement, api, wire, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord } from 'lightning/uiRecordApi';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import { error, success } from 'c/notificationSvc';
import {labels} from 'c/labels';

export default class SupplyTile extends NavigationMixin(LightningElement) {
    labels = labels;
    @api supplyId;
    @api isRow = false;
    @api address;
    @api billingProfileInformation;
    @api selectFields = [];
    @api disabled = false;


    @track openConfirmModal;
    @track recordId;
    @track deletedRecordId;
    @track listBillingProfile = [];


    @track isGas
    @track isElectric
    @track isService
    @wire(getRecord, { recordId: '$supplyId', fields: [
                                                          'Supply__c.RecordType.DeveloperName',
                                                          'Supply__c.BillingProfile__c',
                                                          'Supply__c.BillingProfile__r.BillingStreetType__c',
                                                          'Supply__c.BillingProfile__r.BillingStreetName__c',
                                                          'Supply__c.BillingProfile__r.BillingStreetNumber__c',
                                                          'Supply__c.BillingProfile__r.BillingStreetNumberExtn__c',
                                                          'Supply__c.BillingProfile__r.BillingCity__c',
                                                          'Supply__c.BillingProfile__r.BillingPostalCode__c',
                                                          'Supply__c.BillingProfile__r.IBAN__c',
                                                          'Supply__c.BillingProfile__r.RecordType.Name',
                                                          'Supply__c.BillingProfile__r.DeliveryChannel__c',
                                                          'Supply__c.BillingProfile__r.Email__c',
                                                          'Supply__c.BillingProfile__r.Fax__c',
                                                        ] })
    wiredRecord({ error, data }) {
        if (data) {
            this.isElectric = data.fields.RecordType.value.fields.DeveloperName.value === 'Electric';
            this.isGas = data.fields.RecordType.value.fields.DeveloperName.value === 'Gas';
            this.isService = data.fields.RecordType.value.fields.DeveloperName.value === 'Service';
            this.error = undefined;
            this.address = data.fields.BillingProfile__r.value.fields.BillingStreetType__c.value + ' ' +
                data.fields.BillingProfile__r.value.fields.BillingStreetName__c.value + ' ' +
                data.fields.BillingProfile__r.value.fields.BillingStreetNumber__c.value + '' +
                ((data.fields.BillingProfile__r.value.fields.BillingStreetNumberExtn__c.value === null) ? '' : data.fields.BillingProfile__r.value.fields.BillingStreetNumberExtn__c.value) + ', ' +
                data.fields.BillingProfile__r.value.fields.BillingCity__c.value + ' ' +
                data.fields.BillingProfile__r.value.fields.BillingPostalCode__c.value;

            this.billingProfileInformation = data.fields.BillingProfile__r.value.fields.RecordType.value.fields.Name.value;
            if (data.fields.BillingProfile__r.value.fields.RecordType.value.fields.Name.value === 'Giro') {

                this.billingProfileInformation = this.billingProfileInformation + ' - ' + 'IBAN : ' + data.fields.BillingProfile__r.value.fields.IBAN__c.value;
            }

            this.billingProfileInformation = this.billingProfileInformation + ' - ' + data.fields.BillingProfile__r.value.fields.DeliveryChannel__c.value;
            switch (data.fields.BillingProfile__r.value.fields.DeliveryChannel__c.value) {
                case 'Email':
                    this.billingProfileInformation = this.billingProfileInformation + ' : ' + data.fields.BillingProfile__r.value.fields.Email__c.value;
                    break;
                case 'Mail':
                    break;
                case 'Fax':
                    this.billingProfileInformation = this.billingProfileInformation + ' : ' + data.fields.BillingProfile__r.value.fields.Fax__c.value;
                    break;

                default:
                    break;
            }
            this.billingProfileInformation = this.billingProfileInformation + ' - ' + this.address;


/*

            if (data.fields.BillingProfile__r.value.fields.RecordType.value.fields.Name.value === 'Giro') {
                this.billingProfileInformation =  data.fields.BillingProfile__r.value.fields.RecordType.value.fields.Name.value +
                ' - ' + data.fields.BillingProfile__r.value.fields.IBAN__c.value + this.address;
            } else if (data.fields.BillingProfile__r.value.fields.RecordType.value.fields.Name.value === 'Postal Order') {
                this.billingProfileInformation =  data.fields.BillingProfile__r.value.fields.RecordType.value.fields.Name.value +
                ' - ' + data.fields.BillingProfile__r.value.fields.DeliveryChannel__c.value + ': ' +
                data.fields.BillingProfile__r.value.fields.Email__c.value + this.address ;
            }
            */

        } else if (error) {
            this.error = error;
            this.record = undefined;
        }
    }

    get svgURLEle() {
        this.recordId = this.supplyId;
        return SVG_URL + '/images/Electric.svg#electric';
    }

    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }

    get svgURLService() {
        return SVG_URL + '/images/Service.svg#service';
    }

    closeModal(){
        this.openEditModal=false;
        this.openConfirmModal=false;
    }

    deleteSupplyFromTile () {
        this.openConfirmModal = false;
        success(this, this.labels.success);
        const deleteEvent = new CustomEvent('delete', {
             detail: {
                deletedRecordId: this.deletedRecordId
             }
        });
        this.dispatchEvent(deleteEvent);
    }

    deleteCaseIcon (event){
        this.deletedRecordId = event.target.value;
        this.openConfirmModal=true;
    }
}