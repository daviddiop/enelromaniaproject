import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import {labels} from 'c/labels';

export default class CustomerInteractionListItem extends NavigationMixin(LightningElement) {
    @api customerInteraction;
    @api rolesList;
    @api isInteractionClosed;
    @track address;

    @track isRoleEdit = false;
    @track role;

    labels = labels;
    @track vatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;

    get hasRelatedContact() {
        return !!this.customerInteraction.contactId;
    }

    get isRoleEditDisabled() {
        return this.customerInteraction.isCustomerAndInterlocutor || this.isInteractionClosed
    }

    @api
    get roleValue() {
        return this.customerInteraction.isCustomerAndInterlocutor ? this.labels.customer : this.customerInteraction.role;
    }

    get editRoleButtonIcon() {
        return this.isRoleEdit ? 'utility:save' : 'utility:edit';
    }

    get editRoleButtonAltText() {
        return this.isRoleEdit ? this.labels.save : this.labels.edit;
    }

    connectedCallback() {

        this.role = this.customerInteraction.role;
        this.address = this.customerInteraction.address;
    }

    onEditClick() {
        if (this.isRoleEdit) {
            this.dispatchEvent(new CustomEvent('customerinteractionevent', {
                detail: {
                    'type': 'edit',
                    'customerInteraction': this.customerInteraction,
                    'newRole': this.role,
                },
                bubbles: true
            }));
        }
        this.isRoleEdit = !this.isRoleEdit;
    }

    handleRoleChange(event) {
        this.role = event.detail.value;
    }

    onDeleteClick() {
        this.dispatchEvent(new CustomEvent('customerinteractionevent', {
            detail: {
                'type': 'delete',
                'customerInteraction': this.customerInteraction
            },
            bubbles: true
        }));
    }

    navigateToAccount() {
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.customerInteraction.customerId,
                "objectApiName": "Account",
                "actionName": "view"
            }
        });
    }

    navigateToContact() {
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.customerInteraction.contactId,
                "objectApiName": "Contact",
                "actionName": "view"
            }
        });
    }
}