import { LightningElement, api } from 'lwc';

export default class GroupedTableItem extends LightningElement {
    @api row
    @api columns
    @api multiselection

    get isGroup() {
        return this.row.isGroup;
    }

    get colspan(){
        return this.columns.length + 1;
    }

    handleGroupSelection(evt) {
        if(this.row.isGroup){
            this.dispatchEvent(new CustomEvent('togglegroup', { 
                detail: {
                    groupKey: this.row.key, 
                    children: [],
                    selected: evt.detail.checked,
                    isGroup : this.row.isGroup
                }
            }));
        }
    }

    handleItemSelection(evt) {
        if(!this.row.isGroup) { 
            this.dispatchEvent(new CustomEvent('toggleitem', {
                detail: {
                    groupKey: this.row.key, 
                    key: this.row.key, 
                    selected: evt.detail.checked,
                    isGroup : this.isGroup
                }
            }));
        }
    }

    propagateEvent(evt) {
        evt.detail.groupKey = this.row.key;
        const event = new CustomEvent('toggleitem', {detail: evt.detail});
        this.dispatchEvent(event);
    }

}