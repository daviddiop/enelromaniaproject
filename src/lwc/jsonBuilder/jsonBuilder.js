import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";

export function jsonBuilder(objectJsonBuilder, recordId) {
    let jsonObject;
    let error = null;
    let inputs = {
        objectJsonBuilder: objectJsonBuilder,
        recordId: recordId
    };
    if (objectJsonBuilder) {
        jsonObject = notCacheableCall({
            className: "PickJsonBuilder",
            methodName: "getJsonByObjectNameAndId",
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error = JSON.stringify(response.error);
                return error;
            }
            return response.data;
        }).catch((errorMsg) => {
            error = errorMsg.body.output.errors[0].message;
        });
    }
    return jsonObject;
}