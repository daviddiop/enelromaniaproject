/**
 * Created by l.centra on 21/01/2020.
 */
import {LightningElement, api, track} from 'lwc';
import {labels} from 'c/mroLabels';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import getPickListValues from '@salesforce/apex/MRO_UTL_Utils.getPickListValues';
import {error, success} from 'c/notificationSvc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class MroInsolvencyStatusEdit extends LightningElement {
    labels = labels;
    @track options;
    @track fieldName = 'InsolvencyBankruptcyStatus__c';
    @api objectName = 'Account';
    @track accountInsolvencyStatus;
    @track firstScenario;
    @track secondScenario;
    @track thirdScenario;
    @track accountJudicialAdministrator;
    @track accountJudicialLiquidator;
    @track accountInsolvencyStartDate;
    @track accountInsolvencyEndDate;
    @track accountBankruptcyStartDate;
    @track isEdit = false;
    @track disableNextStep = false;
    @track hideStartDate = false;
    @track hideEndDate = false;
    @track hideJudicialAdministrator = false;
    @track isStartDateValueEmpty = false;
    @track disableEndDate = false;

    @api objectId;
    @api accountId;
    @api dossierId;
    @api account;
    @api step;
    @api recordTypeCase;
    @api inputValueDateDefault;

    @api
    submit() {
        this.handleSubmit();
    }

    handleSubmit(event) {
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
    }

    connectedCallback() {
        if (this.accountId) {
            this.retrieveAccount();
        }
        getPickListValues({objectApiName: this.objectName, fieldApiName: this.fieldName})
            .then(data => {
                console.log(data.valueOf());
                this.options = data;
            })
            .catch(error => {
                error(this, 'Unexpected Error');
            });
    }
    //Open From Dossier
    retrieveCase() {

    }

    retrieveAccount() {
        console.log('retrieveAccount');
        let inputs = {accountId: this.accountId};
        notCacheableCall({
            className: "MRO_LC_AccountEdit",
            methodName: "retrieveAccount",
            input: inputs
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }
            this.account = apexResponse.data;
            this.accountInsolvencyStatus = apexResponse.data.InsolvencyBankruptcyStatus__c;
            if (typeof this.accountInsolvencyStatus === 'undefined') {
                this.firstScenario = true;
            } else if (this.accountInsolvencyStatus === 'Insolvent') {
                this.secondScenario = true;
            } else if (this.accountInsolvencyStatus === 'Insolvency closed') {
                this.thirdScenario = true;
            }
            //default today
            this.accountInsolvencyStartDate = apexResponse.data.InsolvencyStartDate__c;
            if (!this.accountInsolvencyStartDate){
                this.accountInsolvencyStartDate = this.inputValueDateDefault;
                this.isStartDateValueEmpty = true;
            }
            this.accountJudicialAdministrator = apexResponse.data.JudicialAdministrator__c;
            if(apexResponse.data.InsolvencyEndDate__c) {
                this.accountInsolvencyEndDate = apexResponse.data.InsolvencyEndDate__c;
            }else this.accountInsolvencyEndDate =  this.inputValueDateDefault;
            if(apexResponse.data.BankruptcyDate__c) {
                this.accountBankruptcyStartDate = apexResponse.data.BankruptcyDate__c;
            }else this.accountBankruptcyStartDate = this.inputValueDateDefault;
            this.accountJudicialLiquidator = apexResponse.data.JudicialLiquidator__c;

        });
    }

    handleNext() {
        let accountInputInsolvencyStatus = this.accountInsolvencyStatus;
        let accountInputInsolvencyStartDate = this.accountInsolvencyStartDate;
        let accountInputJudicialAdministrator = this.accountJudicialAdministrator;
        let accountInputInsolvencyEndDate = this.accountInsolvencyEndDate;
        let accountInputBankruptcyStartDate = this.accountBankruptcyStartDate;
        let accountInputJudicialLiquidator = this.accountJudicialLiquidator;
        this.step = 3;
        this.isEdit = false;
        let step = this.step;
        const valueInputFieldsEvent = new CustomEvent("valueinputfields", {
            detail: {
                accountInputInsolvencyStatus,
                accountInputInsolvencyStartDate,
                accountInputJudicialAdministrator,
                accountInputInsolvencyEndDate,
                accountInputBankruptcyStartDate,
                accountInputJudicialLiquidator,
                step

            }
        });
        // Fire the custom event
        this.dispatchEvent(valueInputFieldsEvent);
    }
    handleEdit(event) {
        this.isEdit = true;
        this.step = 2;
        let stepEdit =  this.step;
        const valueEditModeEvent = new CustomEvent("valueeditmode", {
            detail: {
                stepEdit
            }
        });
        // Fire the custom event
        this.dispatchEvent(valueEditModeEvent);
    }
    /**Field Case SubProcess__c**/
    handleChangeStatus(event) {
        this.accountInsolvencyStatus = event.detail.value;
        this.disableNextStep = false;
        //Insolvency Closed value scenario
        if (this.accountInsolvencyStatus === 'Insolvency closed') {
            if(this.isStartDateValueEmpty){
                this.accountInsolvencyStartDate = null;
            }
            if (!this.isInsolventClosed) {
                let title = this.thirdScenario ? 'Insolvency already closed' : 'Account not Insolvent';
                let message = this.thirdScenario ? 'The Insolvency for this Account has already been closed' : 'The Insolvency/Bankruptcy status of this Account is not "Insolvent". Cannot close the Insolvency of a non-Insolvent Account';
                this.disableNextStep = true;
                this.showErrorToast(title, message, 'error');
            } else if (!this.accountInsolvencyEndDate) {
                if (!this.accountInsolvencyStartDate) {
                    let title = 'Missing Start Date';
                    let message = 'The Start Date field for this Account is missing';
                    this.showErrorToast(title, message,'warning');
                }
                this.disableNextStep = true;
            } else {
                if (!this.accountInsolvencyStartDate) {
                    let title = 'Missing Start Date';
                    let message = 'The Start Date field for this Account is missing';
                    this.showErrorToast(title, message,'warning');
                }
                this.disableNextStep = false;
            }
        }

        //Insolvent value scenario
        if (this.accountInsolvencyStatus === 'Insolvent') {
            if(this.firstScenario){
                this.accountInsolvencyStartDate = this.inputValueDateDefault;
            }
            if (this.secondScenario) {
                let title = 'Account already Insolvent';
                let message = 'The selected Account is already Insolvent';
                this.disableNextStep = true;
                this.showErrorToast(title, message,'error');
            } else if (!this.accountInsolvencyStartDate || !this.accountJudicialAdministrator) {
                this.disableNextStep = true;
            } else {
                this.disableNextStep = false;
            }
        }

        //Bankrupt value scenario showing/hinding fields basing on [ENLCRO-1143]
        if (this.accountInsolvencyStatus === 'Bankrupt') {
            if(!this.firstScenario){
                if (this.isStartDateValueEmpty) {
                    this.accountInsolvencyStartDate = null;
                }
                if(this.secondScenario){
                    this.disableEndDate = true;
                } else {
                    this.disableEndDate = false;
                    this.hideEndDate = this.accountInsolvencyEndDate ? false : true;
                }
                this.hideStartDate = this.accountInsolvencyStartDate ? false : true;
                this.hideJudicialAdministrator = this.accountJudicialAdministrator ? false : true;
                if (!this.accountInsolvencyStartDate || !this.accountJudicialAdministrator) {
                    let title = 'Field(s) Missing';
                    let message = !this.accountInsolvencyStartDate && !this.accountJudicialAdministrator ?
                        'The Start Date and Judicial Administrator field for this Account are missing' :
                        this.accountInsolvencyStartDate ? 'The Judicial Administrator field for this Account is missing' :
                            'The Start Date field for this Account is missing';
                    this.showErrorToast(title, message,'warning');
                }
                if (!this.accountJudicialLiquidator || !this.accountInsolvencyEndDate || !this.accountBankruptcyStartDate) {
                    this.disableNextStep = true;
                } else {
                    this.step = 2;
                    this.disableNextStep = false;
                }
            } else {
                this.disableEndDate = false;
                this.accountInsolvencyStartDate = this.inputValueDateDefault;
                this.hideJudicialAdministrator = this.accountJudicialAdministrator ? false : true;
                this.hideEndDate = true;
                this.hideStartDate = true;
                if (!this.accountJudicialLiquidator || !this.accountBankruptcyStartDate) {
                    this.disableNextStep = true;
                } else {
                    this.step = 2;
                    this.disableNextStep = false;
                }
            }
        }

        if(this.isInsolventClosed) {
            this.step = 2;
        }
    }
    /**Field Case StartDate__c**/
    handleChangeStartDate(event) {
        this.accountInsolvencyStartDate = event.detail.value;
        this.validateFields();
        if(this.fillCheckScenario1()) {
            this.step = 2;
        }else this.step = 1;
    }
    /**Field Case InsolvencyManager__c**/
    handleChangeJudicialAdministrator(event) {
        this.accountJudicialAdministrator = event.target.value;
        this.validateFields();
        if(this.fillCheckScenario1()) {
            this.step = 2;
        }else this.step = 1;
    }
    /**Field Case EndDate__c**/
    handleChangeInsolvencyEndDate(event) {
        this.accountInsolvencyEndDate = event.target.value;
        this.validateFields();
        if(this.isBankrupt) {
            if(this.fillCheckScenario2()) {
                this.step = 2;
            }else this.step = 1;
        }else if(this.isInsolventClosed) {
            if(this.fillCheckScenario3()) {
                this.step = 2;
            }else this.step = 1;
        }
    }
    /**Field Case EndDate__c**/
    handleChangeBankruptcyStartDate(event) {
        this.accountBankruptcyStartDate = event.target.value;
        this.validateFields();
        if(this.fillCheckScenario2()) {
            this.step = 2;
        }else this.step = 1;

    }
    /**Field Case InsolvencyManager__c**/
    handleChangeJudicialLiquidator(event) {
        this.accountJudicialLiquidator = event.target.value;
        this.validateFields();
        if(this.fillCheckScenario2()) {
            this.step = 2;
        }else this.step = 1;
    }
    get isInsolvent() {
        return (this.accountInsolvencyStatus === 'Insolvent' && this.firstScenario) ||
            (this.accountInsolvencyStatus === 'Insolvent' && this.thirdScenario);
    }
    get isInsolventClosed() {
        return this.accountInsolvencyStatus === 'Insolvency closed' && this.secondScenario;
    }
    get isBankrupt() {
        return (this.accountInsolvencyStatus === 'Bankrupt' && ( this.firstScenario || this.secondScenario)) ||
            (this.accountInsolvencyStatus === 'Bankrupt' && this.thirdScenario);
    }
    get disableStatus() {
        return this.step === 0 || this.step === 3;
    }
    //able button set step 2
    get disableNext() {
        return (this.step === 0 || this.step === 1 || this.step === 3) && !this.isEdit ||
            (this.accountInsolvencyStatus === 'Insolvency closed' && !this.isInsolventClosed) ||
            (this.accountInsolvencyStatus === 'Insolvent' && this.secondScenario && this.disableNextStep) ||
            (this.accountInsolvencyStatus === 'Insolvent' && this.disableNextStep) ||
            (this.accountInsolvencyStatus === 'Bankrupt' && this.disableNextStep);
    }
    get disableInput() {
        return (this.step === 0 || this.accountInsolvencyStatus === 'undefined' || this.step === 3) && !this.isEdit;
    }
    get disableEdit() {
        return this.step === 0 || this.step === 1 || this.step === 2;
    }
    fillCheckScenario1() {
        return (this.accountInsolvencyStartDate && this.accountJudicialAdministrator);
    }
    fillCheckScenario2() {
        return (this.accountInsolvencyEndDate && this.accountBankruptcyStartDate && this.accountJudicialLiquidator );
    }
    fillCheckScenario3() {
        return this.accountInsolvencyEndDate;
    }

    showErrorToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    validateFields(){
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.required && element.value) {
                this.disableNextStep = false;
            } else {
                this.disableNextStep = true;
                return;
            }
        });
    }
}