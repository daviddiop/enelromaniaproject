/**
 * Created by David diop on 29.04.2020.
 */

import {LightningElement, api, track} from 'lwc';
import {error, success, warn} from 'c/notificationSvc';
import {labels} from "c/mroLabels";
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class MroRepresentativeSelection extends LightningElement {
    labels = labels;
    @api contactList;
    @api contactField;
    @api contactRecord;
    @api accountRecord;
    @api disabledForm = false;
    @track showListBox = false;
    @track showBox = false;
    @track showEditBox = false;
    @track contactToUpdate;
    @track isConnectionRepresentant = true;

    searchHandler(event) {
        this.contactList = event.detail.individualList;
        this.contactField = event.detail.searchFields;
        this.showBox = true;
        this.showListBox = true;
        this.showEditBox = false;
    }

    onContactSelect(event) {
        if (event.detail.contact.AccountId) {
            if (event.detail.contact.AccountId && event.detail.contact.AccountId !== this.accountRecord.Id) {
                this.showBox = false;
                this.contactToUpdate = event.detail.contact;
                this.updateContact();
            } else {
                let contactRecord = event.detail.contact;
                this.contactRecord = contactRecord;
                this.showBox = false;
                const successEvent = new CustomEvent("success", {
                    detail: {
                        contactRecord: contactRecord
                    }
                });
                this.dispatchEvent(successEvent);
            }
        } else {
            this.showBox = false;
            this.contactToUpdate = event.detail.contact;
            this.updateContact();
        }
    }

    updateContact() {
        if (!this.contactToUpdate.NationalIdentityNumber__c) {
            error(this, this.labels.nationalIdentityNumber + '-' + this.labels.required);
            return;
        }
        this.contactRecord = this.contactToUpdate;
        const successEvent = new CustomEvent("success", {
            detail: {
                contactRecord: this.contactToUpdate
            }
        });
        this.dispatchEvent(successEvent);
    }

    redirectHandler(event) {
        let page = event.detail.page;
        if (page === 'individualSearch') {
            this.showBox = false;
        }
        if (page === 'individualEdit') {
            this.showBox = false;
            this.showListBox = false;
            this.showEditBox = true;
        }
    }

    handleCancel(event) {
        this.showBox = true;
        this.showListBox = true;
        this.showEditBox = false;
    }

    contactRemoveHandler() {
        this.contactRecord = null;
        const successEvent = new CustomEvent("success", {
            detail: {
                contactRecord: null
            }
        });
        this.dispatchEvent(successEvent);
    }

    handleSave(event) {
        this.showBox = false;
        this.showListBox = false;
        this.showEditBox = false;
        this.contactRecord = event.detail.contactRecord;
        const successEvent = new CustomEvent("success", {
            detail: {
                contactRecord: this.contactRecord
            }
        });
        this.dispatchEvent(successEvent);
    }
}