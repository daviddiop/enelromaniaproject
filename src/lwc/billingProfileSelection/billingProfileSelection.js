import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import {error} from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/labels';

export default class BillingProfilesSelection extends NavigationMixin(LightningElement) {
    labels = labels;
    @api showEditButton = false;
    @api showHeader = false;
    @api accountId;
    @api disabled = false;
    @api billingProfileRecordId = '';
    @track listBillingProfile = [];
    @track enableEditButtonBP = true;
    @api showModal = false;
    @track buttonType;
    @track enableButtons = true;
    @track recordList;
    @track showModalContract = true;

    @api
    reset(accId) {
        this.accountId = accId;
        this.reloadBillingProfileRecords();
    }

    connectedCallback() {
        if(this.accountId){
            this.reloadBillingProfileRecords();
        }
    }

    reloadBillingProfileRecords() {
        let inputs = {accountId: this.accountId};

        notCacheableCall({
            className: 'BillingProfileCnt',
            methodName: 'getBillingProfileRecords',
            input: inputs
        })
            .then((response) => {
                this.listBillingProfile = response.data.listBillingProfile;
                if(this.listBillingProfile){
                    this.reloadRecordList();
                }


            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    reloadRecordList() {
        let records = [];
        if (this.listBillingProfile) {
            for (let el of this.listBillingProfile) {
                let address = el.BillingStreetType__c + ' ' + el.BillingStreetName__c + ' ' + el.BillingStreetNumber__c + ((typeof el.BillingStreetNumberExtn__c === "undefined" || !el.BillingStreetNumberExtn__c) ? '' : el.BillingStreetNumberExtn__c) + ', ' + el.BillingCity__c + ' ' + el.BillingPostalCode__c;
                let record = {};
                record.Id = el.Id;
                record.value = el.PaymentMethod__c;
                if (el.PaymentMethod__c === 'Giro') {
                    record.value = record.value + ' - '+ ' IBAN : ' + el.IBAN__c;
                }
                record.value = record.value + ' - ' + el.DeliveryChannel__c;
                switch (el.DeliveryChannel__c) {
                    case 'Email':
                        record.value = record.value + ' : ' + el.Email__c;
                        break;
                    case 'Mail':
                        break;
                    case 'Fax':
                        record.value = record.value + ' : ' + el.Fax__c;
                        break;

                    default:
                        break;
                }
                record.value = (((typeof el.BillingStreetName__c === "undefined") || (!el.BillingStreetName__c))
                                || ((typeof el.BillingStreetType__c === "undefined") || (!el.BillingStreetType__c))
                                || ((typeof el.BillingStreetNumber__c === "undefined") || (!el.BillingStreetNumber__c))
                                || ((typeof el.BillingCity__c === "undefined") || (!el.BillingCity__c))
                                || ((typeof el.BillingPostalCode__c === "undefined") || (!el.BillingPostalCode__c))) ? record.value : record.value + ' - ' + address;

                record.selected = ((el.Id) === (this.billingProfileRecordId));
                records.push(record);
            }
        }
        this.recordList = records;
    }

    getActivatedSupplies() {
        let inputs = {billingProfileRecordId: this.billingProfileRecordId};

        notCacheableCall({
            className: 'BillingProfileCnt',
            methodName: 'getActiveSupplies',
            input: inputs
        })
            .then((response) => {
                this.enableEditButtonBP = !response.data.enableEditButtonBP;
                this.enableButtons = false;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    saveBillingProfileEdit(event) {
        if (event.detail.billingProfileEditId) {
             if (this.buttonType === 'edit'){
                 event.stopPropagation();
                 const selectedEvent = new CustomEvent('edit', {
                     detail: {
                        showModalContract: this.showModalContract
                     }
                 });
                 this.dispatchEvent(selectedEvent);
             }

            if ((this.buttonType === 'new') || (this.buttonType === 'clone')) {
                event.stopPropagation();
                let newBPid = event.detail.billingProfileEditId;
                this.billingProfileRecordId = newBPid;
                this.enableEditButtonBP = false;
                const selectedEvent = new CustomEvent('selected', {
                    detail: {
                        billingProfileRecordId: this.billingProfileRecordId,
                        showModalContract: this.showModalContract,
                    }
                });
                this.dispatchEvent(selectedEvent);
            }

            this.reloadBillingProfileRecords();
            this.showModal = false;
            this.enableButtons = false;
        }
    }

    handleStatusRadio(event) {
        this.billingProfileRecordId = event.target.value;
        if(this.billingProfileRecordId){
            event.stopPropagation();
            const selectedEvent = new CustomEvent('selected', {
                detail: {
                    billingProfileRecordId: this.billingProfileRecordId,

                }
            });
            this.dispatchEvent(selectedEvent);

            this.getActivatedSupplies();
             this.reloadRecordList()
            //this.reloadBillingProfileRecords();
        }

    }


    get addClass() {
        if (this.disabled) {
            return 'slds-card_boundary slds-form-element slds-size_11-of-12 slds-scrollable_y slds-class slds-theme_shade';
        }
        return 'slds-card_boundary slds-form-element slds-size_11-of-12 slds-scrollable_y';
    }

    get addButtonClass() {
        if (this.showEditButton) {
            return 'slds-align_absolute-center slds-p-bottom_small';
        }
        return 'slds-align_absolute-center slds-p-bottom_small slds-hide';
    }

    newBillingProfile(event) {
        event.preventDefault();
        this.showModal = true;
        this.buttonType = 'new';
    }

    editBillingProfile(event) {
        event.preventDefault();
        this.showModal = true;
        this.buttonType = 'edit';
    }

    cloneBillingProfile(event) {
        event.preventDefault();
        this.showModal = true;
        this.buttonType = 'clone';
    }

    closeModal() {
        this.showModal = false;

    }

    closeHandle(event) {
        this.showModal = event.target.showModal;

    }

}