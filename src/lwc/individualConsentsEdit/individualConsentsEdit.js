import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { labels } from 'c/labels';
import {error} from 'c/notificationSvc';

export default class IndividualConsentsEdit extends NavigationMixin(LightningElement) {
    labels = labels;

    handleError(event) {
        error(this, event.detail.message);
    }

}