import { LightningElement,api,track } from 'lwc';
import {labels} from 'c/labels';

export default class DocumentTypeItem extends LightningElement {
    @api documentType;
    @api documentBundleItem
    @api sectionOpened = false;
    @track isValidated = false;
    @track isOpen = false;
    labels = labels;
    
    // Handles click on the 'Show/hide content' button
    handleCheckClick() {
        let section = this.template.querySelector('.section-document');
        if (section){
            section.classList.remove('slds-hide');
            this.isOpen = this.sectionOpened = !this.isOpen;
            this.dispatchEvent(new CustomEvent("changesection"));
            if (this.documentType.documentItems === null && this.documentType.documentBundleItem) {
                this.documentBundleItem = this.documentType.documentBundleItem;
                this.isValidated = true;
                this.dispatchEvent(new CustomEvent("validate"));
            }
        }
    }

    handleCancelClick() {
        let section = this.template.querySelector('.section-document');
        section.classList.add('slds-hide');
        this.isOpen = this.sectionOpened = false;
        this.dispatchEvent(new CustomEvent("changesection"));
    }

    handleValidateClick() {
        let showErrPanel = false;
        let activeSectionCheckboxes = Array.from(this.template.querySelectorAll('[data-id="allCheckboxes"]'));
        activeSectionCheckboxes.forEach((checkbox) => {
            if(!checkbox.checked){
                checkbox.classList.add('slds-has-error');
                showErrPanel = true;
            }
        });
        if (showErrPanel){
            this.template.querySelector('.error-panel').classList.replace('slds-hide','slds-show');
        }else{
            if (this.documentType.documentBundleItem){
                this.documentBundleItem = this.documentType.documentBundleItem;
                this.isValidated = true;
                this.dispatchEvent(new CustomEvent("validate"));
            }
        }
    }

    removeError(event){
        event.target.classList.remove('slds-has-error');
        let errPanel = this.template.querySelector('.error-panel');
        if (errPanel.classList.contains('slds-show')){
            errPanel.classList.replace('slds-show', 'slds-hide');
        }  
    }
}