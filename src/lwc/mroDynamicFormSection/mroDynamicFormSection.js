/**
 * Created by pes on 06/05/2020.
 */

import {LightningElement, api, track, wire} from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import parseLabels from '@salesforce/apex/MRO_LC_DynamicForm.parseLabels';
import {error} from "c/notificationSvc";

const labelRegex = /^{!\$Label\.\w+}$/i;

export default class MroDynamicFormSection extends LightningElement {
    @api
    get section() {
        return this._section;
    }
    set section(value) {
        this._section = JSON.parse(JSON.stringify(value));
        this._section.colSize = 12 / this._section.columns;
        this.title = this._section.title;
        if (this.title && this.isCustomLabel(this.title)) {
            this.labels[this.title.slice(9,-1)] = "";
        }
        this.fields = this._section.fields;
        this.fields.forEach(field => {
            field.rendered = !field.inheritsFromFieldId;
            if (field.label && this.isCustomLabel(field.label)) {
                this.labels[field.label.slice(9,-1)] = "";
            }
        });
        //console.log('Labels before', this.labels);
        this.parseCustomLabels();
    }
    _section;
    @api objectType;
    @track title;
    @track fields;
    @track labels = {};

    @wire(getObjectInfo, { objectApiName: '$objectType' })
    objectInfo({error, data}) {
        if (data) {
            console.log('ObjectInfo:', JSON.parse(JSON.stringify(data)));
            this._objectInfo = data;
            this.fields.forEach(field => {
                if (!field.label) {
                    field.label = data.fields[field.fieldName].label;
                }
            });
        }
        else if (error) {
            console.error(error);
        }
    };
    _objectInfo;

    parseCustomLabels() {
        if (this.labels) {
            let emptyLabels = true;
            for (let key in this.labels) {
                if (this.labels.hasOwnProperty(key)) {
                    emptyLabels = false;
                    break;
                }
            }
            if (!emptyLabels) {
                console.log('Parsing labels for '+this._section.title, JSON.stringify(this.labels));

                parseLabels({
                    labels: this.labels
                })
                .then(response => {
                    console.log('Parsing labels response for '+this._section.title, JSON.stringify(response.labels));
                    this.labels = JSON.parse(JSON.stringify(response.labels));
                    //console.log('Labels after', this.labels);
                    if (this.title) {
                        this.title = this.getCustomLabel(this.title);
                    }
                    this.fields.forEach(field => {
                        field.label = this.getCustomLabel(field.label);
                    });
                }).catch(errorMsg => {
                    console.error('Parse labels error', errorMsg);
                    error(this, errorMsg);
                });
                /*
                if (this.title) {
                    this.title = this.getCustomLabel(this.title);
                }
                this.fields.forEach(field => {
                    field.label = this.getCustomLabel(field.label);
                });
                 */
            }
        }
    }

    isCustomLabel(label) {
        return labelRegex.test(label);
    }

    getCustomLabel(label) {
        if (this.isCustomLabel(label)) {
            return this.labels[label.slice(9,-1)];
        }
        else {
            return label;
        }
    }

    @api
    getSectionData() {
        const record = {
            Id: this._section.recordId
        };
        this.template.querySelectorAll('lightning-input-field').forEach(inputField => {
            record[inputField.fieldName] = inputField.value;
        });
        return record;
    }
}