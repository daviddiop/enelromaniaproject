import {LightningElement, track, api} from 'lwc';
import {labels} from 'c/labels';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error} from 'c/notificationSvc';

export default class ServicePointSearch extends LightningElement {
    label = labels;
    @track servicePointCode = '';
    @api companyDivisionId;
    @api disabled = false;
    @api accountId;

    @api
    resetBox() {

        let inputCmp = this.template.querySelector('[data-id="searchBox"]');
        inputCmp.value = '';
        this.removeError();

    }

    handleChange(event) {
        event.target.classList.remove('slds-has-error');
        this.servicePointCode = event.target.value;
        this.removeError();
    }

    findServicePoint() {
        if (this.servicePointCode.length === 0) {
            error(this, this.label.requiredFields);
            this.validateServicePoint();
            return;
        } else if (this.servicePointCode.length < 14) {
            error(this, this.label.shortEntry);
            this.validateServicePoint();
            return;
        }
        let inputCmp = this.template.querySelector('lightning-input');
        let value = inputCmp.value;

        let gmPatt = /^(\d{14})$/i;
        let eePatt = /^(\w{2}\d{3}\w{1}\d{8})$/i;
        if (!gmPatt.test(value) && !eePatt.test(value)) {
            error(this, this.label.invalidCode);
            inputCmp.classList.add('slds-has-error');
            return;
        }
        inputCmp.setCustomValidity("");
        inputCmp.reportValidity();
        if (!inputCmp.reportValidity()) {
            return;
        }

        if (!this.servicePointCode || this.servicePointCode === "") {
            error(this, this.label.noDataEntered);
            return;
        }
        this.searchPoint();
    }

    searchPoint = () => {
        let inputs = {pointCode: this.servicePointCode, accountId: this.accountId};

        notCacheableCall({
            className: 'ServicePointCnt',
            methodName: 'findCode',
            input: inputs
        })
            .then((response) => {
                const selectedEvent = new CustomEvent(
                    "selected",
                    {
                        detail: {
                            servicePointCode: this.servicePointCode,
                            servicePointId: response.data.servicePointId,
                            servicePoint: response.data.servicePoint,
                            suppliesByServicePointCode: response.data.suppliesByServicePointCode

                        }
                    }
                );
                this.servicePointCode = '';
                this.dispatchEvent(selectedEvent);
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    };

    validateServicePoint = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.length < 14 || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };



    removeError = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.remove('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };
}