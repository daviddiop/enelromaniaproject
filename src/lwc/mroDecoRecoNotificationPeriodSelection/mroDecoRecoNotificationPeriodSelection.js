/**
 * Created by giupastore on 01/09/2020.
 */

import {CurrentPageReference} from 'lightning/navigation';
import {LightningElement, track, wire, api} from 'lwc';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';


export default class MroDecoRecoNotificationPeriodSelection extends LightningElement {

    @api recordId;
    @api objectType = 'Account';
    @track notificationStartDateDefault;
    @track notificationStartDate;
    @track notificationEndDate;

    labels = labels;

    connectedCallback() {
        this.notificationStartDateDefault = this.setNotificationStartDateDefault(new Date());
        this.notificationStartDate = this.notificationStartDateDefault;
        this.notificationEndDate = this.setNotificationEndDate(new Date())
    }

    setNotificationStartDateDefault(currentDay) {
        let threeMonthsAgo = new Date(currentDay.setMonth(currentDay.getMonth() - 2));
        let threeMonthsAgoDigit = threeMonthsAgo.getMonth();

        if (threeMonthsAgoDigit <= 9) {
            threeMonthsAgoDigit = '0' + threeMonthsAgoDigit;
        }

        return threeMonthsAgo.getFullYear() + "-" + threeMonthsAgoDigit + "-" + threeMonthsAgo.getDate();
    }

    setNotificationEndDate(currentDay) {
        let monthDigit = currentDay.getMonth() + 1;

        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }

        return currentDay.getFullYear() + "-" + monthDigit + "-" + currentDay.getDate();
    }

    onHandleNotificationStartDateChange(event) {
        console.log(this.recordId);
        let inputStartDate = event.target.value;

        if (this.notificationEndDate) {
            let parsedInputStartDate = Date.parse(inputStartDate);
            let parsednotificationEndDate = Date.parse(this.notificationEndDate);

            if (parsedInputStartDate >= parsednotificationEndDate) {
                error(this, labels.startDateAfterEndDate);
                inputStartDate = "";
                event.target.value = inputStartDate;
            }
        }
        this.notificationStartDate = inputStartDate;
    }

    get supplyId(){
        return this.objectType === 'Supply' ? this.recordId : null;
    }


    get customerCode(){
        return this.objectType === 'Account' ? this.recordId : null;
    }

}