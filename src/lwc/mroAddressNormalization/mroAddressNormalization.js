/**
 * Created by tommasobolis on 22/10/2020.
 */

import { api, track, wire, LightningElement } from 'lwc';
import { labels } from 'c/mroLabels';
import { error } from 'c/notificationSvc';
import { updateRecord } from 'lightning/uiRecordApi';
import initialize from '@salesforce/apex/MRO_LC_AddressNormalization.initialize';
import updateAddress from '@salesforce/apex/MRO_LC_AddressNormalization.updateAddress';

export default class MroAddressNormalization extends LightningElement {

    labels = labels;

    @api showSpinner=false;
    @api recordId;
    @api readOnly = false;

    @track isRequiredAddressField = true;
    @track disableNormalize = false;
    @track isNormalize;
    @track addressForced = true;
    @track activity;
    @track address;
    @track availableAddresses;

    get isSaveDisabled() {

        let isSaveDisabled = this.activity && this.address && this.address.addressNormalized;
        return !isSaveDisabled;
    }

    get showModal() {

        let showModal = this.address && this.activity;
        return showModal;
    }

    connectedCallback() {

        this.showSpinner = true;
        this.readOnly = false;
        this.disableNormalize = false;
        this.isNormalize = false;
        this.address = {"country":"ROMANIA"};
        this.initialize();
    }

    initialize() {
        console.log('MroAddressNormalization.initialize - Enter');
        console.log('MroAddressNormalization.initialize - recordId', this.recordId);
        initialize({recordId: this.recordId})
            .then((response) => {
                console.log('MroAddressNormalization.initialize - response: ' + JSON.stringify(response));
                if(response.error == false){
                    this.address = response.forcedAddress;
                    this.activity = response.activity;
                    this.isnormalize = this.activity.SendingAddressNormalized__c;
                    this.readOnly = this.activity.SendingAddressNormalized__c;
                    this.addressForced = !this.activity.SendingAddressNormalized__c;
                } else {
                    console.log('MroAddressNormalization.initialize - error', resposne.errorMessage, response.errorStackTrace);
                }
                this.showSpinner = false;
            }).catch((errorMsg) => {
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output));
                    console.log(JSON.stringify(errorMsg.body.output))
                } else {
                    error(this,errorMsg);
                }
                this.showSpinner = false;
            });
    }

    handleAddressNormalized(event) {

        this.isNormalize = event.detail.isnormalize;
        this.addressForced = event.detail.addressForced;
        if(event.detail.addressNormalized) {
            this.address = event.detail.address;
            this.address.addressNormalized = this.isNormalize;
            console.log('MroAddressNormalization.handleAddressNormalized - address', JSON.stringify(this.address));
        }
    }

    updateAddress() {
        this.showSpinner = true;
        updateAddress({
            recordId: this.recordId,
            address: this.address
        })
            .then((response) => {

                if(!response.error) {

                    this.updateRecordView(this.recordId);
                    this.dispatchEvent(new CustomEvent("close"));
                } else {

                    error(this, JSON.stringify(response.errorMessage));
                    console.log('MroAddressNormalization.updateAddress - error', response.errorMessage, response.errorStackTrace);
                }
                this.showSpinner = false;
            }).catch((errorMsg) => {
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.showSpinner = false;
            });
    }

    updateRecordView(recordId) {
       updateRecord({fields: { Id: recordId }});
    }

    handleCancel() {
        this.dispatchEvent(new CustomEvent("close"));
    }

    handleSave() {
        this.updateAddress();
    }
}