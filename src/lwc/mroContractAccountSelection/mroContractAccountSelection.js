import {LightningElement, api, track} from "lwc";
import {error} from "c/notificationSvc";
import {NavigationMixin} from "lightning/navigation";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from "c/mroLabels";

export default class ContractAccountSelection extends NavigationMixin(LightningElement) {
    labels = labels;
    @api showEditButton = false;
    @api showHeader = false;
    @api hideButtons = false;
    @api accountId;
    @api companyDivisionId;
    @api disabled = false;
    @api contractAccountRecordId = '';
    @api filter = [];
    @api showDifferentiateDetails = false;
    @api commodity;
    @api market;
    @api inEnelArea = false;
    @api selfReadingEnabled = false;
    @api isAccountSituationWizard = false;
    @api isDemonstratedPaymentWizard = false;
    @api isInvoiceDuplicateWizard = false;
    @api isBillingProfileAddressChangeWizard = false;
    @api contractAccountIdList;
    @api excludeContractAccountIds;
    @api refundFilter;
    @api vatNumber;
    @api reuseBillingAccountNumber = false;
    @api opportunityServiceItems = [];

    @api showRefundFields = false;

    // [ENLCRO-714]
    @api hideSelfReadingPeriodEndField = false;

    //FF  [ENLCRO-669] Contract Account Selection - Check Interface
    @api contractType;
    @api supply;
    @api contractId;

    //FF  [ENLCRO-669] Contract Account Selection - Check Interface

    //For Sub-Process Public Tender the contract type should be always business  and markt = Free
    @api hasSubProcessPublicTender = false;
    // For Sub-Process Public Tender the contract type should be always business  and markt = Free

    @api streetName;
    @api pointAddressProvince;
    @api hasDirectDebitInBillingProfile = false;

    @track _contractAccountRecordId;
    @track listContractAccounts = [];
    @track enableEditButtonCA = true;
    @track showModalContract = false;
    @track buttonType;
    @track enableButtons = true;
    @track recordList;
    @track objectId;
    @track popoverPosition;
    @track yPos;
    @track xPos;
    @track hasNotContent = false;
    @track latePaymentPenalty;
    @track paymentTerms;

    @api
    reset(accId) {
        this.accountId = accId;
        this.reloadContractAccountRecords();
    }

    @api
    reload() {
        this.reloadContractAccountRecords();
    }

    connectedCallback() {
        this.reloadContractAccountRecords();
        //this.filter;
        //console.log('this.contractType222 ' + JSON.stringify(this.contractType));
    }

    reloadContractAccountRecords() {
        if (!this.accountId) {
            error(this, this.labels.account+' - '+this.labels.missingId);
            return;
        }
        console.log("contract market==="+this.market);
        if(!this.market){
            this.market='';
        }
        let supplyIds = [];
        if (this.supply) {
            if (this.supply instanceof Array) {
                supplyIds = this.supply;
            } else {
                supplyIds.push(this.supply);
            }
        }
        let inputs = {
            accountId: this.accountId,
            companyDivisionId: this.companyDivisionId,
            contractId: this.contractId,
            commodity: this.commodity,
            market: this.market,
            isAccountSituationWizard: JSON.stringify(this.isAccountSituationWizard),
            isDemonstratedPaymentWizard: JSON.stringify(this.isDemonstratedPaymentWizard),
            isInvoiceDuplicateWizard: JSON.stringify(this.isInvoiceDuplicateWizard),
            isBillingProfileAddressChangeWizard: JSON.stringify(this.isBillingProfileAddressChangeWizard),
            filter: this.filter.length != 0 ? JSON.stringify(this.filter) : '',
            contractType: this.contractType,
            "supplyIds": JSON.stringify(supplyIds),
            contractAccountIdList: this.contractAccountIdList ? JSON.stringify(this.contractAccountIdList) : null,
            excludeContractAccountIds: this.excludeContractAccountIds && this.excludeContractAccountIds.length ? JSON.stringify(this.excludeContractAccountIds) : null
        };
        console.log('getContractAccountRecords inputs: '+JSON.stringify(inputs));
        notCacheableCall({
            className: "MRO_LC_ContractAccount",
            methodName: "getContractAccountRecords",
            input: inputs
        })
            .then(response => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                } else {
                    this.listContractAccounts = response.data.listContractAccount;
                    this.latePaymentPenalty = response.data.latePaymentPenalty;
                    this.paymentTerms = response.data.paymentTerms;
                    console.log("reloadRecordList===");

                    this.reloadRecordList();
                    console.log("done reloadRecordList===");

                }
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    getActivatedSupplies() {
        let inputs = {contractAccountId: this.contractAccountRecordId};

        notCacheableCall({
            className: "MRO_LC_ContractAccount",
            methodName: "getActiveSupplies",
            input: inputs
        })
            .then(response => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                } else {
                    this.enableEditButtonCA = !response.data.enableEditButtonCA;
                    this.enableButtons = false;
                }
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    saveContractAccountEdit(event) {
        event.preventDefault();
        event.stopPropagation();
       /* console.log('event.detail: ' + JSON.stringify(event.detail));
        console.log('event.detail.contractAccountEdit: ' + JSON.stringify(event.detail.contractAccountEditId));*/
        if (event.detail.contractAccountEditId) {
            //console.log('event.detail.showModalContAcc: ' + JSON.stringify(event.detail.showModalContAcc));
            if ((this.buttonType === "new") || (this.buttonType === "clone")) {
                //let newCAid = event.detail.contractAccountEditId;
                this.contractAccountRecordId = event.detail.contractAccountEditId;
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                let contractTypeVal = event.detail.contrAccountEditContractType;
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                /*console.log('this.contractAccountRecordId: ' + event.detail.contractAccountEditId);
                console.log('event.detail.contrAccountEditContractType: ' + event.detail.contrAccountEditContractType);*/
                this.enableEditButtonCA = false;

                const selectedEvent = new CustomEvent("contractaccountselected", {
                    detail: {
                        contractAccountRecordId: this.contractAccountRecordId,
                        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                        contrAccountContractType: contractTypeVal
                        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                    }
                });
                this.dispatchEvent(selectedEvent);

            }
            //console.log('showModalContract1: ' + this.showModalContract);
            this.reloadContractAccountRecords();
            if (event.detail.showModalContAcc) {
                console.log('this.showModalContract1: ' + JSON.stringify(event.detail.showModalContAcc));
                this.showModalContract = false;
                console.log('this.showModalContract2: ' + JSON.stringify(this.showModalContract));
            } else {
                console.log('this.showModalContract3: ' + JSON.stringify(event.detail.showModalContAcc));
                this.showModalContract = true;
                console.log('this.showModalContract4: ' + JSON.stringify(event.detail.showModalContAcc));
            }
            this.enableButtons = false;


        }
    }

    handleStatusRadio(event) {
        let contractAccountRecord = this.listContractAccounts.find(function (contractAccount) {
            event.target.checked = true;
            return contractAccount.Id === event.target.value;
        });
        console.log(' contractAccountRecord: '+ JSON.stringify(contractAccountRecord))
        this.contractAccountRecordId = contractAccountRecord.Id;
        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
        let contractTypeVal = contractAccountRecord.ContractType__c;
        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
        console.log('contractTypeVal: ' + contractTypeVal);

        const selectedEvent = new CustomEvent("contractaccountselected", {
            detail: {
                selectedContractAccount: contractAccountRecord,
                contractAccountRecordId: this.contractAccountRecordId,
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                contrAccountContractType: contractTypeVal
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface
            }
        });
        this.dispatchEvent(selectedEvent);

        this.getActivatedSupplies();
        this.reloadRecordList();
    }

    reloadRecordList() {
        let records = [];
        if (this.listContractAccounts) {
            for (let el of this.listContractAccounts) {
                let record = {};
                if (el.BillingProfile__r) {
                    let address =
                        el.BillingProfile__r.BillingStreetType__c +
                        " " +
                        el.BillingProfile__r.BillingStreetName__c +
                        " " +
                        el.BillingProfile__r.BillingStreetNumber__c +
                        (typeof el.BillingProfile__r.BillingStreetNumberExtn__c ===
                        "undefined" || !el.BillingProfile__r.BillingStreetNumberExtn__c
                            ? ""
                            : el.BillingProfile__r.BillingStreetNumberExtn__c) +
                        ", " +
                        el.BillingProfile__r.BillingCity__c +
                        " " +
                        el.BillingProfile__r.BillingPostalCode__c;
                    record.Id = el.Id;
                    record.name = el.Name;
                    //Add Market in CA table
                    record.market = el.Market__c;
                    // Add Market in CA table
                    if(this.showDifferentiateDetails){
                        record.commodity = el.Commodity__c;
                        record.activeSuppliesCount =  el.ActiveSuppliesCount__c;
                    }else{
                        record.paymentMethod = el.BillingProfile__r.PaymentMethod__c;
                        //[ENLCRO-658] Billing Profile - Interface Check - Pack2
                        //if (el.BillingProfile__r.PaymentMethod__c === "Giro") {
                            record.Iban = el.BillingProfile__r.IBAN__c;
                        //}
                        record.refundMethod = el.BillingProfile__r.RefundMethod__c;
                        record.refundIban = el.BillingProfile__r.RefundIBAN__c;

                        record.deliveryChannelType = el.BillingProfile__r.DeliveryChannel__c;
                        switch (el.BillingProfile__r.DeliveryChannel__c) {
                            case "Email":
                                record.deliveryChannel = el.BillingProfile__r.Email__c;
                                break;
                            case "Mail":
                                break;
                            /*case "Fax":
                                record.deliveryChannel = el.BillingProfile__r.Fax__c;
                                break;*/

                            default:
                                break;
                        }
                        //[ENLCRO-658] Billing Profile - Interface Check - Pack2
                        record.commodity =  el.Commodity__c;
                        record.billingFrequency = el.BillingFrequency__c;
                        record.paymentTerms = el.PaymentTerms__c;
                        record.billingAddress = (((typeof el.BillingProfile__r.BillingStreetName__c === "undefined") || (!el.BillingProfile__r.BillingStreetName__c))
                            || ((typeof el.BillingProfile__r.BillingStreetType__c === "undefined") || (!el.BillingProfile__r.BillingStreetType__c))
                            || ((typeof el.BillingProfile__r.BillingStreetNumber__c === "undefined") || (!el.BillingProfile__r.BillingStreetNumber__c))
                            || ((typeof el.BillingProfile__r.BillingCity__c === "undefined") || (!el.BillingProfile__r.BillingCity__c))
                            || ((typeof el.BillingProfile__r.BillingPostalCode__c === "undefined") || (!el.BillingProfile__r.BillingPostalCode__c))) ? '' : address;
                    }
                    record.selected = (el.Id === this.contractAccountRecordId);
                    if (el.CompanyDivision__r) {
                        record.companyDivision =  el.CompanyDivision__r.Name;
                    }
                    record.billingProfileName =  el.BillingProfile__r.Name;
                    record.billingProfileId =  el.BillingProfile__r.Id;

                    records.push(record);
                }
            }
        }
        this.recordList = records;
        if (this.isAccountSituationWizard) {
            if (records.length === 0) {
                this.hasNotContent = true;
            }
            else {
                   this.hasNotContent = false;
                 }
        }
    }

    get addClass() {
        if (this.disabled) {
            return "slds-card_boundary slds-form-element slds-size_11-of-12 slds-class slds-theme_shade";
        }
        return "slds-card_boundary slds-form-element slds-size_11-of-12";
    }

    showPop(event){
        this.objectId = event.currentTarget.dataset.value;
        this.popoverPosition = event.currentTarget.dataset.position;
        let anchorRect = event.currentTarget.getBoundingClientRect();
        let anchorRectTop = anchorRect.top;
        let anchorRectLeft = anchorRect.left;
        let anchorRectRight = anchorRect.right;
        let popoverRect = this.template.querySelector('div[class="popover"]').getBoundingClientRect();
        let popoverRectTop = popoverRect.top;
        let popoverRectLeft = popoverRect.left;
        let popoverRectRight = popoverRect.right;
        this.yPos = Math.abs(anchorRectTop - popoverRectTop);
        if(this.popoverPosition == "radio-right"){
            this.xPos = Math.abs(anchorRectRight - popoverRectLeft + 15); //15px space added for popover nubbin
        }
        else if(this.popoverPosition == "left"){
            this.xPos = Math.abs(anchorRectLeft - popoverRectRight - 15); //15px space subtracted for popover nubbin
        }
        this.template.querySelector('c-mro-compact-layout-popover').showPopover(event);
    }
    closePop(event){
        this.objectId = event.currentTarget.dataset.value;
        this.template.querySelector('c-mro-compact-layout-popover').closePopoverOnMouseOver();
    }

    get addButtonClass() {
        if (this.showEditButton) {
            return "slds-align_absolute-center slds-p-bottom_small";
        }
        return "slds-align_absolute-center slds-p-bottom_small slds-hide";
    }

    newContractAccount(event) {
        event.preventDefault();
        this.showModalContract = true;
        this.buttonType = "new";
    }

    editContractAccount(event) {
        event.preventDefault();
        this.showModalContract = true;
        this.buttonType = "edit";
    }

    cloneContractAccount(event) {
        event.preventDefault();
        this.showModalContract = true;
        this.buttonType = "clone";
    }

    closeModal() {
        this.showModalContract = false;
    }

    closeHandle(event) {
        this.showModalContract = event.target.showModalContract;
    }
}