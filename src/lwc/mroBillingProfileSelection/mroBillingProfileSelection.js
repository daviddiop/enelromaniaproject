import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import {error} from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/mroLabels';

export default class BillingProfilesSelection extends NavigationMixin(LightningElement) {
    labels = labels;
    @api showEditButton = false;
    @api hideButtons = false;
    @api showHeader = false;
    @api accountId;
   // [ENLCRO-658] Billing Profile - Interface Check - Pack2
    @api forceGiroIfBusiness = false;
    @api showAllBilling = false;
    //[ENLCRO-658] Billing Profile - Interface Check - Pack2
    @api disabled = false;
    @api billingProfileRecordId = '';
    @api hideNewButton = false;
    @api isRefund = false;
    @api isCustomerRefundRequest = false;
    @api isProsumers = false;
    @api refundFilter;
    @api showRefundColumns = false;
    @api refundOnClone = false;
    @api addressOnClone = false;

    @track listBillingProfile = [];
    @track enableEditButtonBP = true;
    @api showModal = false;
    @track buttonType;
    @track enableButtons = true;
    @track recordList;
    @track showModalContract = true;
    @track billingPaymentMethod = '';
    @track isBusinessCustomer = false;
    //Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
    @api contractType;
    //Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
    @api hasDirectDebitInBillingProfile = false;

    @api
    reset(accId) {
        this.accountId = accId;
        this.reloadBillingProfileRecords();
    }

    @api countBillingProfiles() {
        return this.listBillingProfile.length;
    }

    connectedCallback() {
        if(this.accountId){
            this.isBusinessClient();
        }
    }

    reloadBillingProfileRecords() {
        let inputs = {accountId: this.accountId};

        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'getBillingProfileRecords',
            input: inputs
        })
            .then((response) => {
                this.listBillingProfile = response.data.listBillingProfile;
                if(this.refundFilter){
                    this.listBillingProfile = this.listBillingProfile.filter(v => {
                        return v.RefundMethod__c === this.refundFilter;
                    });
                }
                if(this.isBusinessCustomer && this.forceGiroIfBusiness && !this.showAllBilling){
                    this.listBillingProfile = this.listBillingProfile.filter(element => {
                        return element.IBAN__c;
                    });
                }

                if(this.listBillingProfile){
                    this.reloadRecordList();
                }

            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    reloadRecordList() {
        let records = [];
        if (this.listBillingProfile) {
            for (let el of this.listBillingProfile) {
                let address = el.BillingStreetType__c + ' ' + el.BillingStreetName__c + ' ' + el.BillingStreetNumber__c + ((typeof el.BillingStreetNumberExtn__c === "undefined" || !el.BillingStreetNumberExtn__c) ? '' : el.BillingStreetNumberExtn__c) + ', ' + el.BillingCity__c + ' ' + el.BillingPostalCode__c;
                let record = {};
                record.Id = el.Id;
                record.paymentMethod = el.PaymentMethod__c;
                record.refundMethod = el.RefundMethod__c;
                record.refundIban = el.RefundIBAN__c;
                //[ENLCRO-658] Billing Profile - Interface Check - Pack2
                //if (el.PaymentMethod__c === 'Giro') {
                    record.Iban = el.IBAN__c;
               // }

                record.Id = el.Id;
                record.name = el.Name;
                record.deliveryChannelType = el.DeliveryChannel__c;
                switch (el.DeliveryChannel__c) {
                    case 'Email':
                        record.deliveryChannel = el.Email__c;
                        break;
                    case 'Mail':
                        break;
                    case 'SMS':
                        record.deliveryChannel = el.MobilePhone__c;
                        break;

                    default:
                        break;
                }
                //[ENLCRO-658] Billing Profile - Interface Check - Pack2
                record.billingAddress = (((typeof el.BillingStreetName__c === "undefined") || (!el.BillingStreetName__c))
                                || ((typeof el.BillingStreetType__c === "undefined") || (!el.BillingStreetType__c))
                                || ((typeof el.BillingStreetNumber__c === "undefined") || (!el.BillingStreetNumber__c))
                                || ((typeof el.BillingCity__c === "undefined") || (!el.BillingCity__c))
                                || ((typeof el.BillingPostalCode__c === "undefined") || (!el.BillingPostalCode__c))) ? '' : address;

                record.selected = ((el.Id) === (this.billingProfileRecordId));
                records.push(record);
            }
        }
        this.recordList = records;
    }

    getActivatedSupplies() {
        let inputs = {billingProfileRecordId: this.billingProfileRecordId};

        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'getActiveSupplies',
            input: inputs
        })
            .then((response) => {
                this.enableEditButtonBP = !response.data.enableEditButtonBP;
                this.enableButtons = false;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    isBusinessClient() {
        let inputs = {accountId: this.accountId};
        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'isBusinessClient',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                else if (response.data.isBusinessClient) {
                    this.isBusinessCustomer = true;
                }
                this.reloadBillingProfileRecords();
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    saveBillingProfileEdit(event) {
        if (event.detail.billingProfileEditId) {
             if (this.buttonType === 'edit'){
                 event.stopPropagation();
                 const selectedEvent = new CustomEvent('edit', {
                     detail: {
                        showModalContract: this.showModalContract
                     }
                 });
                 this.dispatchEvent(selectedEvent);
             }

            if ((this.buttonType === 'new') || (this.buttonType === 'clone')) {
                event.stopPropagation();
                let newBPid = event.detail.billingProfileEditId;
                this.billingProfileRecordId = newBPid;
                this.enableEditButtonBP = false;
                const selectedEvent = new CustomEvent('selected', {
                    detail: {
                        billingProfileRecordId: this.billingProfileRecordId,
                        showModalContract: this.showModalContract,
                    }
                });
                this.dispatchEvent(selectedEvent);
            }

            this.reloadBillingProfileRecords();
            this.showModal = false;
            this.enableButtons = false;
        }
    }

    handleStatusRadio(event) {
        this.billingProfileRecordId = event.target.value;
        let paymentMethod = this.getRecordPaymentMethod(this.billingProfileRecordId)[0].paymentMethod;
        if(this.billingProfileRecordId){
            event.stopPropagation();
            const selectedEvent = new CustomEvent('selected', {
                detail: {
                    billingProfileRecordId: this.billingProfileRecordId,
                    billingProfilePaymentMethod: paymentMethod
                }
            });
            this.dispatchEvent(selectedEvent);

            this.getActivatedSupplies();
            this.reloadRecordList()
            //this.reloadBillingProfileRecords();
        }

    }

    get displayRefundColumns(){
        return this.isRefund || this.showRefundColumns;
    }


    get addClass() {
        if (this.disabled) {
            return 'slds-card_boundary slds-form-element slds-size_11-of-12 slds-class slds-theme_shade';
        }
        return 'slds-card_boundary slds-form-element slds-size_11-of-12';
    }

    get addButtonClass() {
        if (this.showEditButton) {
            return 'slds-align_absolute-center slds-p-bottom_small';
        }
        return 'slds-align_absolute-center slds-p-bottom_small slds-hide';
    }

    newBillingProfile(event) {
        event.preventDefault();
        this.showModal = true;
        this.buttonType = 'new';
    }

    editBillingProfile(event) {
        event.preventDefault();
        this.showModal = true;
        this.buttonType = 'edit';
    }

    cloneBillingProfile(event) {
        event.preventDefault();
        this.showModal = true;
        this.buttonType = 'clone';
    }

    closeModal() {
        this.showModal = false;

    }

    closeHandle(event) {
        this.showModal = event.target.showModal;
    }
/*
    showPop(event){
        let ancoraId = event.currentTarget.dataset.value;
        this.template.querySelector(`a[data-value = ${ancoraId}] + c-mro-compact-layout-popover`).showPopover(event);
    }

    closePop(event){
        let ancoraId = event.currentTarget.dataset.value;
        this.template.querySelector(`a[data-value = ${ancoraId}] + c-mro-compact-layout-popover`).closePopover();
    }
*/

    getRecordPaymentMethod(billingProfileRecordId){
        let billingProfileRecord = this.recordList.filter(element => {
            return element.Id === billingProfileRecordId;
        });
        return billingProfileRecord;
    }
}