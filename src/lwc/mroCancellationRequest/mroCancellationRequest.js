/**
 * Created by goudiaby on 23/08/2019.
 */

import {LightningElement, api, track} from 'lwc';
import {error, success, warn} from 'c/notificationSvc';
import {labels} from 'c/mroLabels';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';

export default class cancellationRequest extends LightningElement {
    @api objectId;
    @api objectName = "";
    @api objectRecordType = "";
    @api originPhase = "";
    @track isRendered= false;
    @api isModal = false;
    @track listCancellationReasons;
    @track disabledConfirmButton = false;
    @track fields;
    @track _objectId;
    @track showLoadingSpinner;
    @track showCancellationDetails = false;
    @track cancellationDetailsField = "";

    labels = labels;
    selectedCancellationReason = "";

    connectedCallback() {
        this.isRendered=true;
        this._objectId = this.objectId;
        this.getFieldSetFields();
        this.getCancellationReasonsPicklist();
    }

    handleConfirm() {
        this.updateSobjectCancellationCase();
    }

    handleCancel() {
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    getCancellationReasonsPicklist() {
        let inputs = {objectName: this.objectName, recordType: this.objectRecordType, originPhase: this.originPhase};

        notCacheableCall({
            className: 'MRO_LC_CancellationRequest',
            methodName: 'GetCancellationReasons',
            input: inputs
        })
            .then((response) => {
                if (response) {
                    let result = Object.assign({}, response.data);
                    if (response.isError) {
                        let errorMsg = response.error;
                        if (errorMsg.includes('no rows')) {
                            warn(this, this.labels.emptyCancellationReasons);
                        } else {
                            console.error(this, response.error);
                        }
                    } else {
                        if (result.warn) {
                            warn(this, result.warn);
                        }
                        this.listCancellationReasons = result.listCancellationReasons;
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    get showFields() {
        return this.fields && this.fields.length > 0;
    }

    getFieldSetFields() {
        this.showLoadingSpinner = true;
        let inputs = {objectName: this.objectName};

        cacheableCall({
            className: 'MRO_LC_CancellationRequest',
            methodName: 'GetFieldset',
            input: inputs
        })
            .then((response) => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        let result = Object.assign({}, response.data);
                        this.fields = result.fields;
                    }
                }
                this.showLoadingSpinner = false;
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    updateSobjectCancellationCase() {
        this.showLoadingSpinner = true;
        let inputs = {
            objectName: this.objectName,
            objectId: this.objectId,
            cancellationReason: this.selectedCancellationReason,
            cancellationDetails: this.cancellationDetailsField
        };

        notCacheableCall({
            className: 'MRO_LC_CancellationRequest',
            methodName: 'UpdateSobjectCancellation',
            input: inputs
        })
            .then((response) => {
                this.showLoadingSpinner = false;
                if (response) {
                    if (response.error) {
                        error(this, response.error);
                    }
                    let result = Object.assign({}, response.data);
                    if (result.isSuccess) {
                        success(this, this.labels.cancelledSuccess);
                        this.dispatchEvent(new CustomEvent('success'));
                    } else {
                        if (result.error) {
                            error(this, result.error);
                        }
                        if (result.message) {
                            error(this, result.message);
                        }
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    handleSelectReason(event) {
        this.selectedCancellationReason = event.detail.value;
        this.showCancellationDetails = (this.selectedCancellationReason && this.selectedCancellationReason.includes('Other'));
    }

    handleCancellationDetails(event){
        this.cancellationDetailsField = event.detail.value;
    }

    @api open() {
        this.isRendered = true;
    }
}