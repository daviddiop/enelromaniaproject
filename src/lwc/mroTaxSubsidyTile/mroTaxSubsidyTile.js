/**
 * Created by David Diop on 18.11.2019.
 */

import { api, LightningElement, track } from "lwc";
import SVG_URL from "@salesforce/resourceUrl/EnergyAppResources";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { error, success } from "c/notificationSvc";
import { labels } from "c/labels";

export default class MroTaxSubsidyTile extends LightningElement {
  RECORD_TYPE_EE = ["Termination_ELE", "SwitchOut_ELE", "TechnicalDataChangeEle", "MeterChangeEle", "Connection_ELE"];
  RECORD_TYPE_GAS = ["Termination_GAS", "SwitchOut_GAS", "TechnicalDataChangeGas", "MeterChangeGas", "Connection_GAS"];
  RECORD_TYPE_SERVICE = ["Termination_Service"];
  RECORD_TYPE_BP = ["BillingProfileChange"];
  RECORD_TYPE_MERGE = ["ContractMerge"];

  labels = labels;
  @api taxSubsidyId;
  @api isConnectionWizard = false;
  @api isNonDisconnectableWizard = false;
  @api isTaxChangeWizard = false;
  @api hideEditIcon = false;
  @api isRow = false;
  @api disabledButton = false;
  @api selectCaseFields = [];
  @api selectCaseTileFields = [];
  @api readOnlyAddress = false;
  @api addressForced = false;
  @api recordTypeId;
  @api isUpdateRequestType = false;
  @api showDesactive = false;
  @track caseFields = [];
  @track openConfirmModal;
  @track openEditModal = false;
  @track listRecordType = [];
  @track isGas;
  @track isElectric;
  @track isService;
  @track isBillingProfile;
  @track isContractMerge;
  @track caseAddress;


  connectedCallback() {
    if (this.isUpdateRequestType) {
      //alert();
    }
  }


  get svgURLEle() {
    return SVG_URL + "/images/Electric.svg#electric";
  }

  get svgURLGas() {
    return SVG_URL + "/images/Gas.svg#gas";
  }


  closeModal() {
    this.openEditModal = false;
    this.openConfirmModal = false;
  }

  deleteTaxFromTile() {
    let inputs = { recordId: this.taxSubsidyId };
    notCacheableCall({
      className: "MRO_LC_TaxSubsidyEdit",
      methodName: "deleteTaxSubsidy",
      input: inputs
    })
      .then(result => {
        if (result.data.error) {
          error(this, result.data.errorMsg);
          return;
        }
        this.openConfirmModal = false;
        success(this, this.labels.success);
        const deleteEvent = new CustomEvent("delete", {
          detail: {
            deleteRecord: this.taxSubsidyId,
            parentCaseId: result.data.parentCase
          }
        });
        this.dispatchEvent(deleteEvent);
      })
      .catch(errorMsg => {
        error(this, JSON.stringify(errorMsg));
      });
  }

  desactiveTaxSubsidy() {
    let inputs = { recordId: this.taxSubsidyId };
    notCacheableCall({
      className: "MRO_LC_TaxSubsidyEdit",
      methodName: "notActiveTaxSubsidy",
      input: inputs
    })
      .then(result => {
        if (result.data.error) {
          error(this, result.data.errorMsg);
          return;
        }
        this.openConfirmModal = false;
        success(this, this.labels.success);
        const deleteEvent = new CustomEvent("delete", {
          detail: {
            deleteRecord: this.taxSubsidyId
          }
        });
        this.dispatchEvent(deleteEvent);
      })
      .catch(errorMsg => {
        error(this, JSON.stringify(errorMsg));
      });
  }

  editTaxSubsidyIcon() {
    //start  add by david
    const editEvent = new CustomEvent("edit", {
      detail: {
        recordSubsidyEdit: this.taxSubsidyId
      }
    });
    this.dispatchEvent(editEvent);
    // end add by david
  }

  deleteTaxSubsidyIcon() {
    this.openConfirmModal = true;
  }

  handleClose(event) {
    this.openEditModal = event.target.closeModal;
  }


  get cardClass() {
    if (this.disabledButton && !this.isUpdateRequestType) {
      return "slds-class slds-theme_shade";
    }
    return "slds-class";
  }

}