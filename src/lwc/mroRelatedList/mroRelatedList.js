/* eslint-disable no-console */
/* eslint-disable eqeqeq */
import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import {labels} from 'c/mroLabels';
import initialize from '@salesforce/apex/MRO_LC_RelatedListCnt.initialize';
import createRelatedList from '@salesforce/apex/MRO_LC_RelatedListCnt.createRelatedList';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class relatedList extends NavigationMixin(LightningElement) {
    @api title; //String which contains the component's title
    @api icon; //String which contains the component's icon
    @api sObjectType //String which contains the sObject API Name of the records have to be listed
    @api relationshipFieldAPIName; //string which contains the API Name of the field which contains the Id of the relationship SObject
    @api fieldSet; // String which contains the name of the FieldSet where is stored the relationship Object's API Fields Name to retrieve
    @api enableNavigateToSObject; //boolean which indicates whether is allowed Navigate to sObject (enable/disable the 'View' button)
    @api recordId = '$recordId';
    @api listType;

    labels = labels;

    /** This attribute contains the records to show */
    @track relatedList = [];
    @track label = new Map();

    /** This attribute is used for show the spinner */
    @track showSpinner = false;
    @track sortDirection = 'ASC';

   //Pagination attributes
   @track currentPageNumber=1;
   @track numOfPages=1;
   @track recordsForPage=5;
   @track numOfRecords;
   @track shownPageRecordsList=[];

   @track showPagination=false;

    connectedCallback() {
        this.showSpinner = true;
        this.init();

    }

     init() {

        initialize( {recordId: this.recordId,
                     title: this.title,
                     sObjectType: this.sObjectType,
                     relationshipFieldAPIName: this.relationshipFieldAPIName,
                     fieldSet: this.fieldSet})

        .then(result => {
            if (result.error == false) {

                    this.label = result.label;
                    this.numOfRecords = result.relatedList.records.length;
                    this.relatedList = result.relatedList;
                    this.title = result.label.title;

                    this.numOfPages=Math.ceil(this.numOfRecords /this.recordsForPage);
                    if(this.numOfRecords>this.recordsForPage){
                        this.showPagination=true;
                    }
                    this.showPage(this.currentPageNumber);
            }else {

                this.showError(result.errorMessage);
                console.log('MRO_LC_RelatedListCnt.initialize (Callback) errorMessage', result.errorMessage);
                console.log('MRO_LC_RelatedListCnt.initialize (Callback) errorStackTraceString', result.errorStackTraceString);
            }
            this.showSpinner = false;
        })
    }
    //records for page
    showPage(pageNum){
        let allRecordsList = this.relatedList.records;
        if(allRecordsList !== undefined){
           let recordsForPage = this.recordsForPage;
           let shownPageRecordsList = [];
           let leftBracket = (pageNum - 1) * recordsForPage;
           let rightBracket = leftBracket + recordsForPage;
           if (rightBracket > allRecordsList.length) rightBracket = allRecordsList.length;
           for (let i = leftBracket; i < rightBracket; i++) {
                    shownPageRecordsList.push(allRecordsList[i]);
           }
           this.shownPageRecordsList=shownPageRecordsList;
        }
        this.showSpinner=false;
    }
    //event handler
    buttonPageClickHandler(event){
        this.showSpinner=true;
        let newPage = event.detail.pageNumber;
        this.currentPageNumber=newPage;
        this.showPage(newPage);
    }

    // Navigate to view Record Page
    navigateToViewRecordPage(event) {
        //var recordId = event.target.name;
        var recordId = event.currentTarget.name;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": recordId,
                "objectApiName": this.sObjectType,
                "actionName": "view"
            },
        });
    }

    sortRelatedList(event){
        if(this.sortDirection == 'ASC'){
            this.sortDirection = 'DESC';
        } else if(this.sortDirection == 'DESC'){
            this.sortDirection = 'ASC';
        }
        var columnToSort = event.currentTarget.name;
        var columnToSortIndex = event.currentTarget.dataset.index;

        createRelatedList( {recordId: this.recordId,
                            sObjectType: this.sObjectType,
                            fieldSet: this.fieldSet,
                            relationshipFieldAPIName: this.relationshipFieldAPIName,
                            orderByColumnIndex: columnToSortIndex,
                            orderByDirection: this.sortDirection})

            .then(result => {
                if (result.error == false) {

                    this.numOfRecords = result.relatedList.records.length;
                    this.relatedList = result.relatedList;
                    this.numOfPages=Math.ceil(this.numOfRecords /this.recordsForPage);
                    if(this.numOfRecords>this.recordsForPage){
                        this.showPagination=true;
                    }
                    this.showPage(this.currentPageNumber);

                    var arrowIconList = this.template.querySelectorAll("lightning-icon");
                    arrowIconList.forEach(function(element){

                        if(element.dataset.name == 'sortRow' && element.dataset.index == columnToSortIndex){
                            element.classList.remove('slds-hide');
                            element.classList.add('slds-show');
                        }else if(element.dataset.name == 'sortRow' && element.dataset.index != columnToSortIndex){
                            element.classList.remove('slds-show');
                            element.classList.add('slds-hide');
                        }
                    },this);

                }else {
                    this.showError(result.errorMessage);
                    console.log('MRO_LC_RelatedListCnt.createRelatedList (Callback) errorMessage', result.errorMessage);
                    console.log('MRO_LC_RelatedListCnt.createRelatedList (Callback) errorStackTraceString', result.errorStackTraceString);
                }
                this.showSpinner = false;
            })
    }

    get IsSortDirectionEqualsASC(){
        return this.sortDirection == 'ASC' ? true : false;
    }

    get IsSortDirectionEqualsDESC(){
        return this.sortDirection == 'DESC' ? true : false;
    }

    showSuccess(message) {

       let toastTitle = this.labels.success;
       let toastMsg = message;
       let toastVariant = 'success';

       this.dispatchEvent(
           new ShowToastEvent({
               title: toastTitle,
               message: toastMsg,
               variant: toastVariant
           })
       );
    }

    showError(message) {

       let toastTitle = this.labels.error;
       let toastMsg = message;
       let toastVariant = 'error';

       this.dispatchEvent(
           new ShowToastEvent({
               title: toastTitle,
               message: toastMsg,
               variant: toastVariant
           })
       );
    }
 }