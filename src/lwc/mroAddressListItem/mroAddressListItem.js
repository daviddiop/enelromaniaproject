import { LightningElement, api } from 'lwc';

export default class MroAddressListItem extends LightningElement {

    @api
    address;

    handleSelection() {
        this.dispatchEvent(new CustomEvent('select', { bubbles: false, detail: { selected: this.address } }));
    }
}