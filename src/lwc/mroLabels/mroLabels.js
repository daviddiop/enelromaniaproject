import phoneNumbers from '@salesforce/label/c.PhoneNumbers';
import servicePoint from '@salesforce/label/c.ServicePoint';
import find from '@salesforce/label/c.Find';
import pointSelection from '@salesforce/label/c.PointSelection';
import success from '@salesforce/label/c.Success';
import warning from '@salesforce/label/c.Warning';
import error from '@salesforce/label/c.Error';
import noDataEntered from '@salesforce/label/c.NoDataEntered';
import requiredFields from '@salesforce/label/c.RequiredFields';
import New from '@salesforce/label/c.New';
import Status from '@salesforce/label/c.Status';
import roles from '@salesforce/label/c.Roles';
import relatedContactList from '@salesforce/label/c.RelatedContactList';
import relatedAccountList from '@salesforce/label/c.RelatedAccountList';
import notFound from '@salesforce/label/c.NotFound';
import customerSearchResults from '@salesforce/label/c.CustomerSearchResults';
import VATNumber from '@salesforce/label/c.VATNumber';
import nationalIdentityNumber from '@salesforce/label/c.NationalIdentityNumber';
import BsnType from '@salesforce/label/c.BusinessType';
import active from '@salesforce/label/c.Active';
import phone from '@salesforce/label/c.Phone';
import phoneOnly from '@salesforce/label/c.PhoneOnly';
import email from '@salesforce/label/c.Email';
import accountServices from '@salesforce/label/c.AccountServices';
import search from '@salesforce/label/c.Search';
import supplySelectionLabel from '@salesforce/label/c.SupplySelection';
import selectSupply from '@salesforce/label/c.SelectSupply';
import filter from '@salesforce/label/c.Filter';
import servicePointSearchLabel from '@salesforce/label/c.ServicePointSearch';
import invalidCode from '@salesforce/label/c.InvalidCode';
import code from '@salesforce/label/c.Code';
import individualEditForm from '@salesforce/label/c.IndividualEditForm';
import newSearch from '@salesforce/label/c.NewSearch';
import newContract from '@salesforce/label/c.NewContract';
import select from '@salesforce/label/c.Select';
import contractEndDate from '@salesforce/label/c.ContractEndDate';
import customerSignedDate from '@salesforce/label/c.ContractCustomerSignedDate';
import contractName from '@salesforce/label/c.ContractName';
import contractType from '@salesforce/label/c.ContractType';
import customerCode from '@salesforce/label/c.CustomerCode';
import caseEdit from '@salesforce/label/c.CaseEdit';
import save from '@salesforce/label/c.Save';
import cancel from '@salesforce/label/c.Cancel';
import cancellationRequest from '@salesforce/label/c.CancellationRequest';
import cancellationReason from '@salesforce/label/c.CancellationReason';
import no from '@salesforce/label/c.No';
import yes from '@salesforce/label/c.Yes';
import informationRequired from '@salesforce/label/c.InformationRequired';
import documentChecking from '@salesforce/label/c.DocumentChecking';
import fileCheckActivity from '@salesforce/label/c.FileCheckActivity';
import fileMetadata from '@salesforce/label/c.FileMetadata';
import createdDate from '@salesforce/label/c.CreatedDate';
import companyDivision from '@salesforce/label/c.CompanyDivision';
import selectDivision from '@salesforce/label/c.SelectDivision';
import dossier from '@salesforce/label/c.Dossier';
import validatableDocuments from '@salesforce/label/c.ValidatableDocuments';
import edit from '@salesforce/label/c.Edit';
import enforced from '@salesforce/label/c.Enforced';
import clone from '@salesforce/label/c.Clone';
import billingProfile from '@salesforce/label/c.BillingProfile';
import billingInformation from '@salesforce/label/c.BillingInformation';
import billingProfileEdit from '@salesforce/label/c.BillingProfileEdit';
import paymentMethod from '@salesforce/label/c.PaymentMethod';
import refundMethod from '@salesforce/label/c.RefundMethod';
import billingAddress from '@salesforce/label/c.BillingAddress';
import addressValidated from '@salesforce/label/c.AddressValidated';
import confirmationQuestion from '@salesforce/label/c.ConfirmationQuestion';
import confirmation from '@salesforce/label/c.Confirmation';
import confirm from '@salesforce/label/c.Confirm';
import addressMustBeVerified from '@salesforce/label/c.AddressMustBeVerified';
import close from '@salesforce/label/c.Close';
import duplicatedRecord from '@salesforce/label/c.DuplicatedRecord';
import customer from '@salesforce/label/c.Customer';
import pleaseSelect from '@salesforce/label/c.PleaseSelect';
import contactDetails from '@salesforce/label/c.ContactDetails';
import fistnameOrLastnameRequired from '@salesforce/label/c.FistnameOrLastnameRequired';
import individualCreated from '@salesforce/label/c.IndividualCreated';
import individualNotCreated from '@salesforce/label/c.IndividualNotCreated';
import documentValidated from '@salesforce/label/c.DocumentValidated';
import account from '@salesforce/label/c.Account';
import accountEmptyContact from '@salesforce/label/c.AccountEmptyContact';
import addressForced from '@salesforce/label/c.AddressForced';
import selectFamilyProduct from '@salesforce/label/c.SelectFamilyProduct';
import selectAddress from '@salesforce/label/c.SelectAddress';
import invalidFields from '@salesforce/label/c.InvalidFields';
import invalidDistributorProvince from '@salesforce/label/c.InvalidDistributorProvince';
import validateAddress from '@salesforce/label/c.ValidateAddress';
import validated from '@salesforce/label/c.Validated';
import mandatory from '@salesforce/label/c.Mandatory';
import requiredCheckDocument from '@salesforce/label/c.RequiredCheckDocument';
import validate from '@salesforce/label/c.Validate';
import update from '@salesforce/label/c.Update';
import opportunityServiceItem from '@salesforce/label/c.OpportunityServiceItem';
import add from '@salesforce/label/c.Add';
import productImage from '@salesforce/label/c.ProductImage';
import noImagePresent from '@salesforce/label/c.NoImagePresent';
import productName from '@salesforce/label/c.ProductName';
import recordType from '@salesforce/label/c.RecordType';
import productDescription from '@salesforce/label/c.ProductDescription';
import product from '@salesforce/label/c.Product';
import cart from '@salesforce/label/c.Cart';
import noCart from '@salesforce/label/c.NoCart';
import checkout from '@salesforce/label/c.Checkout';
import check from '@salesforce/label/c.Check';
import done from '@salesforce/label/c.Done';
import notDone from '@salesforce/label/c.NotDone';
import doneSuccessfully from '@salesforce/label/c.DossierActivitiesDoneSuccessfully';
import details from '@salesforce/label/c.Details';
import noDocument from '@salesforce/label/c.NoDocument';
import shortEntry from '@salesforce/label/c.ShortEntry';
import selectAccountType from '@salesforce/label/c.SelectAccountType';
import allSupplies from '@salesforce/label/c.AllSupplies';
import chooseDocument from '@salesforce/label/c.ChooseDocument';
import documentUpload from '@salesforce/label/c.DocumentUpload';
import recentUploadedDocuments from '@salesforce/label/c.RecentUploadedDocuments';
import documentUploaded from '@salesforce/label/c.DocumentUploaded';
import checkDocument from '@salesforce/label/c.CheckDocument';
import checkDocumentCompleted from '@salesforce/label/c.CheckDocumentCompleted';
import copyFromOtherAddresses from '@salesforce/label/c.CopyFromOtherAddresses';
import copyFromResidentialAddress from '@salesforce/label/c.CopyFromResidentialAddress';
import back from '@salesforce/label/c.Back';
import businessAccount from '@salesforce/label/c.PersonAccount';
import personAccount from '@salesforce/label/c.PersonAccount';
import quantity from '@salesforce/label/c.Quantity';
import totalPrice from '@salesforce/label/c.TotalPrice';
import unitPrice from '@salesforce/label/c.UnitPrice';
import siteAddress from '@salesforce/label/c.SiteAddress';
import noDescriptionProvided from '@salesforce/label/c.NoDescriptionProvided';
import total from '@salesforce/label/c.Total';
import shareTheKnowledge from '@salesforce/label/c.ShareTheKnowledge';
import noRelatedServicePoint from '@salesforce/label/c.NoRelatedServicePoint';
import validateMetadata from '@salesforce/label/c.ValidateMetadata';
import caseNumber from '@salesforce/label/c.CaseNumber';
import supplyCode from '@salesforce/label/c.SupplyCode';
import catalog from '@salesforce/label/c.Catalog';
import productOptionConfiguration from '@salesforce/label/c.ProductOptionConfiguration';
import defaultValue from '@salesforce/label/c.DefaultValue';
import value from '@salesforce/label/c.Value';
import key from '@salesforce/label/c.Key';
import min from '@salesforce/label/c.Min';
import max from '@salesforce/label/c.Max';
import selectProductOptionType from '@salesforce/label/c.SelectProductOptionType';
import noProductConfigurator from '@salesforce/label/c.NoProductConfigurator';
import addDetails from '@salesforce/label/c.AddDetails';
import addressInformations from '@salesforce/label/c.AddressInformations';
import remove from '@salesforce/label/c.Remove';
import none from '@salesforce/label/c.None';
import createNewRelation from '@salesforce/label/c.CreateNewRelation';
import customerNotInTheList from '@salesforce/label/c.CustomerNotInTheList';
import filters from '@salesforce/label/c.Filters';
import priceBook from '@salesforce/label/c.PriceBook';
import next from '@salesforce/label/c.Next';
import family from '@salesforce/label/c.Family';
import productType from '@salesforce/label/c.ProductType';
import noFilters from '@salesforce/label/c.NoFilters';
import noProducts from '@salesforce/label/c.NoProducts';
import emptyCompanyDivision from '@salesforce/label/c.EmptyCompanyDivision';
import selectionsOutOfRange from '@salesforce/label/c.SelectionsOutOfRange';
import interlocutorSearchResults from '@salesforce/label/c.InterlocutorSearchResults';
import notInterlocutorInTheList from '@salesforce/label/c.NotInterlocutorInTheList';
import searchForInterlocutor from '@salesforce/label/c.SearchForInterlocutor';
import searchForCustomer from '@salesforce/label/c.SearchForCustomer';
import noCustomerSelected from '@salesforce/label/c.NoCustomerSelected';
import addAnotherCustomer from '@salesforce/label/c.AddAnotherCustomer';
import addressNotValid from '@salesforce/label/c.AddressNotValid';
import typeText from '@salesforce/label/c.TypeText';
import typeDouble from '@salesforce/label/c.TypeDouble';
import typeHere from '@salesforce/label/c.TypeHere';
import switchIn from '@salesforce/label/c.SwitchIn';
import requiredServiceSite from '@salesforce/label/c.RequiredServiceSite';
import pointAddress from '@salesforce/label/c.PointAddress';
import serviceSite from '@salesforce/label/c.ServiceSite';
import duplicatedServiceSite from '@salesforce/label/c.DuplicatedServiceSite';
import contractAccountEdit from '@salesforce/label/c.ContractAccountEdit';
import commodity from '@salesforce/label/c.Commodity';
import commoditySelection from '@salesforce/label/c.CommoditySelection';
import privacyRecap from '@salesforce/label/c.PrivacyRecap';
import reactivation from '@salesforce/label/c.Reactivation';
import createCase from '@salesforce/label/c.CreateCase';
import caseSuccessfullyUpdated from '@salesforce/label/c.CaseSuccessfullyUpdated';
import createOsi from '@salesforce/label/c.CreateOsi';
import osiEdit from '@salesforce/label/c.OsiEdit';
import invalidCnp from '@salesforce/label/c.InvalidCnp';
import invalidCUI from '@salesforce/label/c.InvalidCUI';
import editCUI from '@salesforce/label/c.EditCUI';
import invalidONRC from '@salesforce/label/c.InvalidONRC';
import invoiceDueDate from '@salesforce/label/c.InvoiceDueDate';

import repliesMeterSerie from '@salesforce/label/c.RepliesMeterSerie';
import lastSelfReadingIndex from '@salesforce/label/c.lastSelfReadingIndex';
import lastSelfReadingDate from '@salesforce/label/c.LastSelfReadingDate';
import meterDetailsQuery from '@salesforce/label/c.MeterDetailsQuery';
import meterDetails from '@salesforce/label/c.MeterDetails';
import sapQueryIndex from '@salesforce/label/c.SAPQueryIndex';
import sapQueryIVRGas from '@salesforce/label/c.SAPQueryIVRGas';

import invoiceId from '@salesforce/label/c.InvoiceId';
import totalAmount from '@salesforce/label/c.TotalAmount';
import nonDisconnectable from '@salesforce/label/c.NonDisconnectable';
import subventionType from '@salesforce/label/c.SubventionType';
import taxChangeEdit from '@salesforce/label/c.TaxChangeEdit';
import accountSupplyNotValid from '@salesforce/label/c.AccountSupplyNotValid';
import supplyNotValid from '@salesforce/label/c.SupplyNotValid';
import rangeNotValid from '@salesforce/label/c.RangeNotValid';
import contractAccount from '@salesforce/label/c.ContractAccount';
import companyInsolvencyBankruptcy from '@salesforce/label/c.CompanyInsolvencyBankruptcy';
import insolvencyBankruptcyStatusNew from '@salesforce/label/c.InsolvencyBankruptcyStatusNew';
import Notes_or_Other_Information from '@salesforce/label/c.Notes_or_Other_Information';
import periodSelection from '@salesforce/label/c.PeriodSelection';
import invoices from '@salesforce/label/c.Invoices';
import invoicesInTimeFrame from '@salesforce/label/c.InvoicesInTimeFrame';
import startDateAfterEndDate from '@salesforce/label/c.StartDateAfterEndDate';
import origin from '@salesforce/label/c.Origin';
import channel from '@salesforce/label/c.Channel';
import emptyCancellationReasons from '@salesforce/label/c.EmptyCancellationReasons';
import cancelledSuccess from '@salesforce/label/c.CancelledSuccessfully';
import activityCancellationConfirmMessage from '@salesforce/label/c.ActivityCancellationConfirmMessage';
import january from '@salesforce/label/c.January';
import february from '@salesforce/label/c.February';
import march from '@salesforce/label/c.March';
import april from '@salesforce/label/c.April';
import may from '@salesforce/label/c.May';
import june from '@salesforce/label/c.June';
import july from '@salesforce/label/c.July';
import august from '@salesforce/label/c.August';
import september from '@salesforce/label/c.September';
import october from '@salesforce/label/c.October';
import november from '@salesforce/label/c.November';
import december from '@salesforce/label/c.December';
import type from '@salesforce/label/c.Type';
import action from '@salesforce/label/c.Action';
import ContractualAvailablePowerValidation from '@salesforce/label/c.ContractualAvailablePowerValidation';
import ContractualPowerValidation from '@salesforce/label/c.ContractualPowerValidation';
import startDate from '@salesforce/label/c.StartDate';
import endDate from '@salesforce/label/c.EndDate';
import notWorkingDay from '@salesforce/label/c.notWorkingDay';
import notes from '@salesforce/label/c.notes';
import requestPurpose from '@salesforce/label/c.requestPurpose';
import FieldChangeMessage from '@salesforce/label/c.FieldChangeMessage';
import nonePicklist from '@salesforce/label/c.NonePicklist';
import name from '@salesforce/label/c.Name';
import iban from '@salesforce/label/c.IBAN';
import refundIban from '@salesforce/label/c.RefundIBAN';
import deliveryChannel from '@salesforce/label/c.DeliveryChannel';
import address from '@salesforce/label/c.Address';
import numberEmail from '@salesforce/label/c.NumberEmail';
import market from '@salesforce/label/c.Market';
import activeSuppliesCount from '@salesforce/label/c.ActiveSuppliesCount';
import billingFrequency from '@salesforce/label/c.BillingFrequency';
import paymentTerms from '@salesforce/label/c.PaymentTerms';
import incorrectIBANNumber from '@salesforce/label/c.IncorrectIBANNumber';
import incorrectIBANLength from '@salesforce/label/c.IncorrectIBANLength';
import billingProfileNotExist from '@salesforce/label/c.BillingProfileNotExist';
import activationOfNewVas from '@salesforce/label/c.ActivationOfNewVas';
import activationOfNewVasInSubstitution from '@salesforce/label/c.ActivationOfNewVasInSubstitution';
import terminationOfAnExistingSoftVas from '@salesforce/label/c.TerminationOfAnExistingSoftVas';
import hardVas from '@salesforce/label/c.HardVas';
import softVas from '@salesforce/label/c.SoftVas';
import selectCommodity from '@salesforce/label/c.SelectCommodity';
import terminationDate from '@salesforce/label/c.TerminationDate';
import activationDate from '@salesforce/label/c.ActivationDate';
import supplyId from '@salesforce/label/c.SupplyId';
import productId from '@salesforce/label/c.ProductId';
import selectedSupplyInfo from '@salesforce/label/c.SelectedSupplyInfo';
import SwitchOutDate from '@salesforce/label/c.SwitchOutDate';
import NewTrader from '@salesforce/label/c.NewTrader';
import compensationType from '@salesforce/label/c.CompensationType';
import compensationCode from '@salesforce/label/c.CompensationCode';
import reason from '@salesforce/label/c.Reason';
import findingDate from '@salesforce/label/c.FindingDate';
import requestDate from '@salesforce/label/c.RequestDate';
import amount from '@salesforce/label/c.Amount';
import meterSerialNumber from '@salesforce/label/c.MeterSerialNumber';
//FF Customer Creation - Pack1/2 - Interface Check
import mobile from '@salesforce/label/c.Mobile';
import contactChannel from '@salesforce/label/c.ContactChannel';
//FF Customer Creation - Pack1/2 - Interface Check
import marketingContact from '@salesforce/label/c.MarketingContact';

import ministryEnergyQuota from '@salesforce/label/c.MinistryEnergyQuota';
import vat from '@salesforce/label/c.VAT';
import greenCertificates from '@salesforce/label/c.GreenCertificates';
import danubeDelta from '@salesforce/label/c.DanubeDelta';
import heatingSubvention from '@salesforce/label/c.HeatingSubvention';
import selfProducers from '@salesforce/label/c.SelfProducers';
import payingSupplyExcise from '@salesforce/label/c.PayingSupplyExcise';
import miners from '@salesforce/label/c.Miners';
//FF OSI Edit - Pack2 - Interface Check
import estimatedConsumption from '@salesforce/label/c.EstimatedConsumption';
import estimatedConsumptionKWh from '@salesforce/label/c.EstimatedConsumptionKWh';
//FF OSI Edit - Pack2 - Interface Check
import servicePointCode from '@salesforce/label/c.ServicePointCode';
import monthsUntilExpiration from '@salesforce/label/c.MonthsUntilExpiration';
import monthS from '@salesforce/label/c.Months';
import checkSAPInterrogation  from '@salesforce/label/c.checkSAPInterrogation';
import additionalInformation from '@salesforce/label/c.AdditionalInformation';
import cancelReasonTitle from '@salesforce/label/c.CancelReasonTitle';
import cancellationConfirmation from '@salesforce/label/c.cancellationConfirmation';
import switchOutDate21 from '@salesforce/label/c.SwitchOutDate21';
import marketingPhone from '@salesforce/label/c.MarketingPhone';
import marketingMobilePhone from '@salesforce/label/c.MarketingMobilePhone';
import marketingEmail from '@salesforce/label/c.MarketingEmail';
import required from '@salesforce/label/c.Required';
import connectionDescriptionLabel from '@salesforce/label/c.ConnectionCaseDescriptionLabel';
import single from '@salesforce/label/c.Single';
import bundle from '@salesforce/label/c.Bundle';
import typeOfPenalty from '@salesforce/label/c.TypeOfPenalty';
import valueOfPenalty from '@salesforce/label/c.ValueOfPenalty';
import caseSwitchOutData from '@salesforce/label/c.CaseSwitchOutData';
import privacyConsents from '@salesforce/label/c.PrivacyConsents';
import cannotCreatePrivacyChange from '@salesforce/label/c.CannotCreatePrivacyChange';
import residentialAddress from '@salesforce/label/c.ResidentialAddress';
import cancellationDetails from '@salesforce/label/c.CancellationDetails';
import switchOutDate5days from '@salesforce/label/c.SwitchOutDate5days';
import incorrectDataToSaveWizard from '@salesforce/label/c.IncorrectDataToSaveWizard';
import invalidDate from '@salesforce/label/c.InvalidDate';
import retentionAccepted from '@salesforce/label/c.RetentionAccepted';
import parentActivityMissing from '@salesforce/label/c.ParentActivityMissing';
import retentionRefusalTitle from '@salesforce/label/c.RetentionRefusalTitle';
import retentionRefusalConfirmation from '@salesforce/label/c.RetentionRefusalConfirmation';
import actions from '@salesforce/label/c.Actions';
import refuse from '@salesforce/label/c.Refuse';
import accept from '@salesforce/label/c.Accept';
import selectAll from '@salesforce/label/c.SelectAll';
import enelTel from '@salesforce/label/c.EnelTel';
import readingRoute from '@salesforce/label/c.ReadingRoute';
import supplyName from '@salesforce/label/c.SupplyName';
import serialNumber from '@salesforce/label/c.SerialNumber';
import smartMeter from '@salesforce/label/c.SmartMeter';
import lastInvoiceDate from '@salesforce/label/c.LastInvoiceDate';
import lastInvoiceStatus from '@salesforce/label/c.LastInvoiceStatus';
import lastIndexInvoiced from '@salesforce/label/c.LastIndexInvoiced';
import lastInvoiceReading from '@salesforce/label/c.LastInvoiceReading';
import lastInvoicedIndexDate from '@salesforce/label/c.LastInvoicedIndexDate';
import lastInvoiceReadingDate from '@salesforce/label/c.LastInvoiceReadingDate';
import readingDate from '@salesforce/label/c.ReadingDate';
import readingType from '@salesforce/label/c.ReadingType';
import dial from '@salesforce/label/c.Dial';
import nrIntreg from '@salesforce/label/c.NrIntreg';
import index from '@salesforce/label/c.Index';
import invoiced from '@salesforce/label/c.Invoiced';
import linkToImages from '@salesforce/label/c.LinkToImages';
import effectiveDate from '@salesforce/label/c.EffectiveDate';
import subventionTypes from '@salesforce/label/c.SubventionTypes';
import invalidRangeDate from '@salesforce/label/c.InvalidRangeDate';
import subventionAssigned from '@salesforce/label/c.SubventionAssigned';
import primary from '@salesforce/label/c.Primary';
import setPrimary from '@salesforce/label/c.SetPrimary';
import subventionAlreadyEdit from '@salesforce/label/c.SubventionAlreadyEdit';
import maxNumberOfMonth from '@salesforce/label/c.maxNumberOfMonth';
import missingDistributorOrServicePoint from '@salesforce/label/c.MissingDistributorOrServicePoint';


import danubeDeltaInstitutions from '@salesforce/label/c.DanubeDeltaInstitutions';
import minersInstitutions from '@salesforce/label/c.MinersInstitutions';
import ministryEnergyQuotaInstitution from '@salesforce/label/c.MinistryEnergyQuotaInstitution';
import heatingSubventionInstitutions from '@salesforce/label/c.HeatingSubventionInstitutions';

import paymentCode from '@salesforce/label/c.PaymentCode';
import invoiceNumber from '@salesforce/label/c.InvoiceNumber';
import invoiceDate from '@salesforce/label/c.InvoiceDate';
import dueDate from '@salesforce/label/c.DueDate';
import invoiceValue from '@salesforce/label/c.InvoiceValue';
import debtorTurnover from '@salesforce/label/c.DebtorTurnover';
import creditorTurnover from '@salesforce/label/c.CreditorTurnover';
import hip from '@salesforce/label/c.Hip';
import nrCollectionDocument from '@salesforce/label/c.NrCollectionDocument';
import collectionDocument from '@salesforce/label/c.CollectionDocument';
import dateOfReceiptDocument from '@salesforce/label/c.DateOfReceiptDocument';
import receiptDocumentValue from '@salesforce/label/c.ReceiptDocumentValue';
import collectionPartner from '@salesforce/label/c.CollectionPartner';
import collectionOperationDate from '@salesforce/label/c.CollectionOperationDate';

import missingPaymentAgency from '@salesforce/label/c.MissingPaymentAgency';
import caseType from '@salesforce/label/c.caseType';
import caseSubType from '@salesforce/label/c.caseSubType';
import distance from '@salesforce/label/c.distance';
import externalRequestId from '@salesforce/label/c.externalRequestId';
import referenceDate from '@salesforce/label/c.referenceDate';
import subject from '@salesforce/label/c.subject';
import description from '@salesforce/label/c.description';
import retrieveWarranties from '@salesforce/label/c.RetrieveWarranties';
import supply from '@salesforce/label/c.Supply';
import parentCase from '@salesforce/label/c.ParentCase';
import deliveryAddress from '@salesforce/label/c.DeliveryAddress';
import consumptionDetails from '@salesforce/label/c.ConsumptionDetails';
import invalidMobile from '@salesforce/label/c.InvalidMobile';
import contractTerm from '@salesforce/label/c.ContractTerm';
import allDistributersShouldHaveTheSameDiscoEnel  from '@salesforce/label/c.AllDistributersShouldHaveTheSameDiscoEnel';
import allOSIShouldHaveTheSameApplicantNotHolder  from '@salesforce/label/c.AllOSIShouldHaveTheSameApplicantNotHolder';
import imputernicit from '@salesforce/label/c.Imputernicit';
import connectionRepresentant from '@salesforce/label/c.ConnectionRepresentant';
import suppliesFromRegulatedMarket from '@salesforce/label/c.SuppliesFromRegulatedMarket';
import amountMaxValue from '@salesforce/label/c.AmountMaxValue';
import amountGreaterThanQuota from '@salesforce/label/c.AmountGreaterThanQuota';
import agence from '@salesforce/label/c.Agence';
import province from '@salesforce/label/c.Province';
import institution from '@salesforce/label/c.Institution';
import dataEntry from '@salesforce/label/c.DataEntry';
import manageDiscard from '@salesforce/label/c.ManageDiscard';
import requestCorrected from '@salesforce/label/c.RequestCorrected';
import editRequest from '@salesforce/label/c.EditRequest';
import requestUpdated from '@salesforce/label/c.RequestUpdated';
import enterValueInTheSearch from '@salesforce/label/c.EnterValueInTheSearch';
import contactEditForm  from '@salesforce/label/c.ContactEditForm';
import contactSearchResults  from '@salesforce/label/c.ContactSearchResults';
import noContactIntheList  from '@salesforce/label/c.NoContactIntheList';
import searchForRepresentative  from '@salesforce/label/c.SearchForRepresentative';
import updateContactToAccount  from '@salesforce/label/c.UpdateContactToAccount';
import enteredDateShouldBeGreaterOrEqualThan  from '@salesforce/label/c.EnteredDateShouldBeGreaterOrEqualThan';
import preCheck from '@salesforce/label/c.PreCheck';
import archiveOnDisCo from '@salesforce/label/c.ArchiveOnDisCo';
import archivedOnDisCo from '@salesforce/label/c.ArchivedOnDisCo';
import multipleArchiveOnDiscoFound from '@salesforce/label/c.MultipleArchiveOnDiscoFound';
import amountkWh from '@salesforce/label/c.AmountkWh';
import amountPercent from '@salesforce/label/c.AmountPercent';
import amountCurrency from '@salesforce/label/c.AmountCurrency';
import constant from '@salesforce/label/c.Constant';
import meterType from '@salesforce/label/c.MeterType';
import invoiceIssueDate from '@salesforce/label/c.InvoiceIssueDate';
import penalties from '@salesforce/label/c.Penalties';
import ACTRecup from '@salesforce/label/c.ACTRecup';
import partnerCompany from '@salesforce/label/c.PartnerCompany';
import acceptToShareDataWithPartner from '@salesforce/label/c.AcceptToShareDataWithPartner';
import beneficiaryName from '@salesforce/label/c.BeneficiaryName';
import beneficiaryPhoneNumber from '@salesforce/label/c.BeneficiaryPhoneNumber';
import beneficiaryEmail from '@salesforce/label/c.BeneficiaryEmail';
import beneficiaryInformation from '@salesforce/label/c.BeneficiaryInformation';
import nameAndPhoneAreMandatory from '@salesforce/label/c.NameAndPhoneAreMandatory';
import invalidEmail from '@salesforce/label/c.InvalidEmail';
import Voltage from '@salesforce/label/c.Voltage';
import Distributor from '@salesforce/label/c.Distributor';
import AbsorbablePower from '@salesforce/label/c.AbsorbablePower';
import salesman from '@salesforce/label/c.Salesman';
import salesUnit from '@salesforce/label/c.SalesUnit';
import RequestDateContraint from '@salesforce/label/c.RequestDateContraint';
import companySignedByID from '@salesforce/label/c.CompanySignedByID';
import MunicipalNetwork from '@salesforce/label/c.MunicipalNetwork';
import Correction from '@salesforce/label/c.Correction';
import NewPermitRequest from '@salesforce/label/c.NewPermitRequest';
import PlacementPermitExtension from '@salesforce/label/c.PlacementPermitExtension';
import TransEuropeanTransportInfrastructure from '@salesforce/label/c.TransEuropeanTransportInfrastructure';
import OpticalFiber from '@salesforce/label/c.OpticalFiber';
import RequiredForConnection from '@salesforce/label/c.RequiredForConnection';
import supplyType from '@salesforce/label/c.SupplyType';
import noContractAccountFound from '@salesforce/label/c.NoContractAccountFound';
import requestStartDateInFuture from '@salesforce/label/c.RequestStartDateShouldBeInTheFuture';
import allOSIShouldHaveTheSameTrader from '@salesforce/label/c.AllOSIShouldHaveTheSameTrader';
import theFieldLengthShouldBeLessOrEqualThen10Characters from '@salesforce/label/c.TheFieldLengthShouldBeLessOrEqualThen10Characters';
import notRegularWorkingDay from '@salesforce/label/c.notRegularWorkingDay';

import firstName from '@salesforce/label/c.FirstName';
import lastName from '@salesforce/label/c.LastName';
import unique from '@salesforce/label/c.Unique';
import master from '@salesforce/label/c.Master';
import lastLogin from '@salesforce/label/c.LastLogin';
import emptyList from '@salesforce/label/c.EmptyList';
import accountId from '@salesforce/label/c.AccountId';
import retrieveAssociatedAccount from '@salesforce/label/c.RetrieveAssociatedAccount';
import descriptionColumn from '@salesforce/label/c.DescriptionColumn';

import lastDayOfAMonth from '@salesforce/label/c.LastDayOfAMonth';
import startDayOfAMonth from '@salesforce/label/c.StartDayOfAMonth';

import sms from '@salesforce/label/c.Sms';
import push from '@salesforce/label/c.Push';
import indexLowerSAPIndex from '@salesforce/label/c.IndexLowerSAPIndex';
import retrieveNotification from '@salesforce/label/c.RetrieveNotification';
import clientCode from '@salesforce/label/c.ClientCode';
import correctPhoneNumber from '@salesforce/label/c.CorrectPhoneNumber';
import correctMobileNumber from '@salesforce/label/c.CorrectMobileNumber';
import retrieveConsumptionPlaces from '@salesforce/label/c.RetrieveConsumptionPlaces';
import dataPreliminaryCheck from '@salesforce/label/c.DataPreliminaryCheck';
import blocking from '@salesforce/label/c.Blocking';
import continueLabel from '@salesforce/label/c.Continue';
import normalizeAddressButton from '@salesforce/label/c.NormalizeAddressButton';
import pleaseSelectAMarketingChannel from '@salesforce/label/c.PleaseSelectAMarketingChannel';
import allOSIShouldHaveTheSameSelfReading from '@salesforce/label/c.AllOSIShouldHaveTheSameSelfReading';
import unexpectedError from '@salesforce/label/c.UnexpectedError';
import greenCerticatesForGasSupplies from '@salesforce/label/c.GreenCerticatesForGasSupplies';
import contractFieldsAreEmpty from '@salesforce/label/c.ContractFieldsAreEmpty';
import contractFieldsValidationFailed from '@salesforce/label/c.ContractFieldsValidationFailed';
import acceptWithProductChange from '@salesforce/label/c.AcceptWithProductChange';
import acceptWithoutProductChange from '@salesforce/label/c.AcceptWithoutProductChange';
import notAvailableToSelectADirectDebitPayment from '@salesforce/label/c.NotAvailableToSelectADirectDebitPayment';
import inEnelArea from '@salesforce/label/c.InEnelArea';
import startDateForUpdateTax  from '@salesforce/label/c.StartDateForUpdateTax';
import EndDateForUpdateTax from '@salesforce/label/c.EndDateForUpdateTax';
import noParameterChanged from '@salesforce/label/c.NoParameterChanged';
import productChangeActivityButton from '@salesforce/label/c.ProductChangeActivityButton';

const labels = {
    enterValueInTheSearch,
    institution,
    ministryEnergyQuota,
    miners,
    payingSupplyExcise,
    selfProducers,
    heatingSubvention,
    danubeDelta,
    greenCertificates,
    vat,

    lastDayOfAMonth,
    startDayOfAMonth,
    startDateForUpdateTax,
    EndDateForUpdateTax,
    greenCerticatesForGasSupplies,
    province,
    agence,
    missingPaymentAgency,
    amountGreaterThanQuota,
    amountMaxValue,
    suppliesFromRegulatedMarket,
    connectionRepresentant,
    imputernicit,
    invalidMobile,
    danubeDeltaInstitutions,
    minersInstitutions,
    ministryEnergyQuotaInstitution,
    heatingSubventionInstitutions,

    noParameterChanged,
    primary,
    setPrimary,
    description,
    subject,
    referenceDate,
    externalRequestId,
    distance,
    caseSubType,
     checkSAPInterrogation,
    caseType,
    maxNumberOfMonth,
    subventionAlreadyEdit,
    subventionAssigned,
    invalidRangeDate,
    subventionTypes,
    effectiveDate,
    retentionAccepted,
    parentActivityMissing,
    retentionRefusalTitle,
    retentionRefusalConfirmation,
    actions,
    refuse,
    accept,
	residentialAddress,
    caseSwitchOutData,
	switchOutDate21,
	cancellationConfirmation,
    nonePicklist,
    invalidDate,
    cancellationDetails,
    cancelledSuccess,
    activityCancellationConfirmMessage,
    emptyCancellationReasons,
    switchOutDate5days,
    incorrectDataToSaveWizard,
    requestPurpose,
    missingDistributorOrServicePoint,
    notes,
    origin,
    channel,
    startDate,
    meterDetailsQuery,
    repliesMeterSerie,
    lastSelfReadingIndex,
    lastSelfReadingDate,
    meterDetails,
    sapQueryIVRGas,
    sapQueryIndex,
    endDate,
    periodSelection,
    notWorkingDay,
    switchIn,
    selectionsOutOfRange,
    noProductConfigurator,
    none,
    filters,
    priceBook,
    confirm,
    next,
    value,
    family,
    typeText,
    typeDouble,
    typeHere,
    selectFamilyProduct,
    productType,
    noFilters,
    noProducts,
    key,
    min,
    max,
    addDetails,
    contractEndDate,
    customerSignedDate,
    noCustomerSelected,
    addAnotherCustomer,
    productOptionConfiguration,
    defaultValue,
    selectProductOptionType,
    supplyCode,
    filter,
    selectSupply,
    allSupplies,
    caseNumber,
    validateMetadata,
    noRelatedServicePoint,
    shareTheKnowledge,
    total,
    noDescriptionProvided,
    code,
    siteAddress,
    unitPrice,
    totalPrice,
    quantity,
    product,
    businessAccount,
    personAccount,
    selectAccountType,
    shortEntry,
    noDocument,
    billingInformation,
    details,
    checkout,
    check,
    done,
    notDone,
    doneSuccessfully,
    noCart,
    cart,
    productName,
    noImagePresent,
    productImage,
    add,
    opportunityServiceItem,
    update,
    validate,
    requiredCheckDocument,
    mandatory,
    confirmation,
    confirmationQuestion,
    servicePoint,
    companyDivision,
    selectDivision,
    pointSelection,
    find,
    success,
    warning,
    error,
    noDataEntered,
    requiredFields,
    New,
    Status,
    roles,
    relatedContactList,
    relatedAccountList,
    notFound,
    customerSearchResults,
    VATNumber,
    nationalIdentityNumber,
    BsnType,
    active,
    phone,
	phoneOnly,
    email,
    accountServices,
    search,
    supplySelectionLabel,
    servicePointSearchLabel,
    invalidCode,
    individualEditForm,
    newSearch,
    newContract,
    select,
    contractName,
    contractType,
    customerCode,
    caseEdit,
    save,
    cancel,
    cancellationRequest,
    cancellationReason,
    no,
    yes,
    informationRequired,
    documentChecking,
    fileCheckActivity,
    fileMetadata,
    createdDate,
    dossier,
    validatableDocuments,
    billingProfile,
    edit,
    enforced,
    clone,
    billingProfileEdit,
    paymentMethod,
    billingAddress,
    addressValidated,
    addressMustBeVerified,
    close,
    duplicatedRecord,
    customer,
    pleaseSelect,
    contactDetails,
    fistnameOrLastnameRequired,
    individualCreated,
    individualNotCreated,
    documentValidated,
    account,
    accountEmptyContact,
    addressForced,
    selectAddress,
    invalidFields,
    invalidDistributorProvince,
    validateAddress,
    validated,
    chooseDocument,
    documentUpload,
    recentUploadedDocuments,
    documentUploaded,
    checkDocument,
    checkDocumentCompleted,
    copyFromResidentialAddress,
    copyFromOtherAddresses,
    back,
    catalog,
    addressInformations,
    remove,
    createNewRelation,
    customerNotInTheList,
    emptyCompanyDivision,
    interlocutorSearchResults, notInterlocutorInTheList, searchForInterlocutor,
    searchForCustomer,
    addressNotValid,
    requiredServiceSite,
    pointAddress,
    serviceSite,
    duplicatedServiceSite,
    contractAccountEdit,
    commodity,
    commoditySelection,
    privacyRecap,
    reactivation,
    createCase,
    caseSuccessfullyUpdated,
    createOsi,
    osiEdit,
    invalidCnp,
	invalidCUI,
    editCUI,
    invalidONRC,
    invoiceDueDate,
    invoiceId,
    totalAmount,
    nonDisconnectable,
    subventionType,
    taxChangeEdit,
    accountSupplyNotValid,
    supplyNotValid,
    rangeNotValid,
	invoices,
	invoicesInTimeFrame,
	startDateAfterEndDate,
    contractAccount,
    phoneNumbers,
    companyInsolvencyBankruptcy,
    insolvencyBankruptcyStatusNew,
	retrieveWarranties,
    january,
    february,
    march,
    april,
    may,
    june,
    july,
    august,
    september,
    october,
    november,
    december,
    type,
    action,
	ContractualAvailablePowerValidation,
	ContractualPowerValidation,
    FieldChangeMessage,
    name,
    iban,
    deliveryChannel,
    address,
    numberEmail,
    market,
    activeSuppliesCount,
    billingFrequency,
    paymentTerms,
    refundMethod,
    incorrectIBANNumber,
    incorrectIBANLength,
    billingProfileNotExist,
    SwitchOutDate,
	NewTrader,
    activationOfNewVas,
    activationOfNewVasInSubstitution,
    terminationOfAnExistingSoftVas,
    hardVas,
    softVas,
    selectCommodity,
    terminationDate,
    activationDate,
    supplyId,
    productId,
    selectedSupplyInfo,
    cancelReasonTitle,
//FF Customer Creation - Pack1/2 - Interface Check
    contactChannel,
    mobile,
    marketingContact,
    marketingPhone,
    marketingMobilePhone,
    marketingEmail,
    required,
    single,
    bundle,
    typeOfPenalty,
    valueOfPenalty,
    servicePointCode,
    monthsUntilExpiration,
    monthS,
    additionalInformation,
    privacyConsents,
    cannotCreatePrivacyChange,
    selectAll,
    enelTel,
    readingRoute,
    supplyName,
    serialNumber,
    smartMeter,
    lastInvoiceDate,
    lastInvoiceStatus,
    lastIndexInvoiced,
    lastInvoiceReading,
    lastInvoicedIndexDate,
    lastInvoiceReadingDate,
    readingDate,
    readingType,
    dial,
    nrIntreg,
    index,
    invoiced,
    linkToImages,
    paymentCode,
    invoiceNumber,
    invoiceDate,
    dueDate,
    invoiceValue,
    debtorTurnover,
    creditorTurnover,
    hip,
    nrCollectionDocument,
    collectionDocument,
    dateOfReceiptDocument,
    receiptDocumentValue,
    collectionPartner,
    collectionOperationDate,

//FF Customer Creation - Pack1/2 - Interface Check
    //FF OSI Edit - Pack2 - Interface Check
    estimatedConsumption,
    estimatedConsumptionKWh,
    //FF OSI Edit - Pack2 - Interface Check
    connectionDescriptionLabel,
    Notes_or_Other_Information,
    compensationType,
    compensationCode,
    reason,
    findingDate,
    requestDate,
    amount,
    supply,
    refundIban,
    parentCase,
    deliveryAddress,
    contractTerm,
    allDistributersShouldHaveTheSameDiscoEnel,
    allOSIShouldHaveTheSameApplicantNotHolder,
    dataEntry,
    manageDiscard,
    requestCorrected,
    editRequest,
    requestUpdated,
    updateContactToAccount,
    searchForRepresentative,
    noContactIntheList,
    contactSearchResults,
    contactEditForm,
    consumptionDetails,
    enteredDateShouldBeGreaterOrEqualThan,
    preCheck,
    archiveOnDisCo,
    archivedOnDisCo,
    multipleArchiveOnDiscoFound,
    amountkWh,
    amountPercent,
    amountCurrency,
    constant,
    meterType,
    invoiceIssueDate,
    penalties,
    ACTRecup,
    acceptToShareDataWithPartner,
    beneficiaryName,
    beneficiaryPhoneNumber,
    beneficiaryEmail,
    beneficiaryInformation,
    nameAndPhoneAreMandatory,
    invalidEmail,
    partnerCompany,
    Voltage,
    Distributor,
    AbsorbablePower,
    salesman,
    salesUnit,
    companySignedByID,
    MunicipalNetwork,
    Correction,
    NewPermitRequest,
    PlacementPermitExtension,
    TransEuropeanTransportInfrastructure,
    OpticalFiber,
    RequiredForConnection,
    RequestDateContraint,
    firstName,
    lastName,
    unique,
    master,
    lastLogin,
    emptyList,
    accountId,
    retrieveAssociatedAccount,
    descriptionColumn,
    sms,
    indexLowerSAPIndex,
    push,
    retrieveNotification,
    clientCode,
    correctPhoneNumber,
    correctMobileNumber,
    retrieveConsumptionPlaces,
    supplyType,
    noContractAccountFound,
    requestStartDateInFuture,
    allOSIShouldHaveTheSameTrader,
    theFieldLengthShouldBeLessOrEqualThen10Characters,
    notRegularWorkingDay,
    dataPreliminaryCheck,
    blocking,
    continueLabel,
    normalizeAddressButton,
    pleaseSelectAMarketingChannel,
    allOSIShouldHaveTheSameSelfReading,
    unexpectedError,
    contractFieldsAreEmpty,
    contractFieldsValidationFailed,
    acceptWithProductChange,
    acceptWithoutProductChange,
    notAvailableToSelectADirectDebitPayment,
    inEnelArea,
    productChangeActivityButton,
    meterSerialNumber
};

const formatLabel = (labelToFormat, ...formattingArguments) => {
    if (typeof labelToFormat !== 'string') throw new Error('\'stringToFormat\' must be a String');
    return labelToFormat.replace(/{(\d+)}/gm, (match, index) =>
            (formattingArguments[index] === undefined ? '' : `${formattingArguments[index]}`));
}

export {labels, formatLabel};