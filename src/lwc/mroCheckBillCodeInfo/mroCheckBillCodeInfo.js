/**
 * Created by Octavian on 6/16/2020.
 */

import {api, track, LightningElement} from 'lwc';

export default class MroCheckBillCodeInfo extends LightningElement {
    @api returnCode;
    @api billingAdjustmentType;

    @track returnCodeHelpText = '';

    connectedCallback() {
        switch (this.returnCode) {
            case 'COD 00' : {
                this.returnCodeHelpText = 'Invoice does not exists, is cancelled or it is of Storno Type';
                break;
            }
            case 'COD 01' : {
                this.returnCodeHelpText = 'The selected invoice is not the last invoice';
                break;
            }
            case 'COD 02' : {
                this.returnCodeHelpText = 'Centralized Invoice (Factura Centralizata)';
                break;
            }
            case 'COD 03' : {
                this.returnCodeHelpText = 'Invoice contains a technical index (FACTURA CONTINE INDEX TEHNIC)';
                break;
            }
            case 'COD 04' : {
                this.returnCodeHelpText = 'Presence of reactive energy';
                break;
            }
            case 'COD 05' : {
                this.returnCodeHelpText = 'The selected invoice is a settlement one (reading has been made from the distributor)';
                break;
            }
            case 'COD 06' : {
                this.returnCodeHelpText = 'The selected invoice is estimate from an estimated reading';
                break;
            }
            case 'COD 07' : {
                this.returnCodeHelpText = 'The selected invoice is estimate from a self-reading';
                break;
            }
            case 'COD 08' : {
                this.returnCodeHelpText = 'There is a further reading for the claimed invoice (EXISTA CITIRE ULTERIOARA FACTURII RECLAMATE)';
                break;
            }
            case 'COD 18' : {
                this.returnCodeHelpText = 'The invoice contains components for contract termination (FACTURA CONTINE CALCUL DE REZILIERE CONTRACT)';
                break;
            }
            case 'COD 19' : {
                this.returnCodeHelpText = 'In the invoiced period there is a meter with reactive energy ( IN PERIOADA FACTURII EXISTA CONTOR MONTAT CU REACTIVA)';
                break;
            }

        }
    }

    get isSuccess () {
        console.log('isSuccess returnCode: ' + this.returnCode + ' BAType: ' + this.billingAdjustmentType);
        if(this.returnCode == 'COD 01' || this.returnCode == 'COD 00') {
            return false;
        }
        console.log('isSuccess isE05 :' + (this.billingAdjustmentType == 'E05-ES'));
        console.log('isSuccess isValidCode :' + (this.billingAdjustmentType == 'E05-ES'));
        if((this.billingAdjustmentType == 'E05-ES' || this.billingAdjustmentType == 'GE05-ES') && this.returnCode != 'COD 06' && this.returnCode != 'COD 07') {

            return false;
        }
        if((this.billingAdjustmentType == 'A042' || this.billingAdjustmentType == 'GA042') && (this.returnCode == 'COD 06' || this.returnCode == 'COD 07')) {
            return false;
        }
        console.log('isSuccess return true');
        return true;
    }

    get isError () {
        if(this.returnCode == 'COD 01') {
            return false;
        }
        if((this.billingAdjustmentType == 'E05-ES' || this.billingAdjustmentType == 'GE05-ES') && (this.returnCode == 'COD 06' || this.returnCode == 'COD 07')) {
            return false;
        }
        if((this.billingAdjustmentType == 'A042' || this.billingAdjustmentType == 'GA042') && this.returnCode != 'COD 06' && this.returnCode != 'COD 07') {
            return false;
        }
        return true;
    }

    get isWarning () {
        if(this.returnCode != 'COD 01' || this.returnCode == 'COD 00') {
            return false;
        }
        return  true;
    }

}