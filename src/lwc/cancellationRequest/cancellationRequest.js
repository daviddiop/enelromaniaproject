/**
 * Created by goudiaby on 23/08/2019.
 */

import {LightningElement, api, track} from 'lwc';
import {error, success} from 'c/notificationSvc';
import { labels } from 'c/labels';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class cancellationRequest extends LightningElement {
    @api objectId;
    @api objectName = "Case";
    @api recordType = "SwitchIn";
    @api originPhase = "RE010";
    @track listCancellationReasons;
    @track disabledConfirmButton = true;
    @track fields;
    labels = labels;
    selectedCancellationReason = "";

    connectedCallback() {
        this.getFieldSetFields();
        this.fetchCancellationReasonsPicklist();
    }

    handleConfirm(){
        this.updateSobjectCancellationCause();
    }

    handleCancel(){
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    fetchCancellationReasonsPicklist() {
        let inputs = {objectName: this.objectName, recordType: this.recordType, originPhase: this.originPhase};

        notCacheableCall({
            className: 'CancellationRequestCnt',
            methodName: 'getCancellationReasons',
            input: inputs
        })
            .then((response) => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        this.listCancellationReasons = response.data.listCancellationReasons;
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    getFieldSetFields(){
        let inputs = {objectName: this.objectName};

        notCacheableCall({
            className: 'CancellationRequestCnt',
            methodName: 'getFieldset',
            input: inputs
        })
            .then((response) => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        this.fields = response.data.fields;
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    updateSobjectCancellationCause() {
        let inputs = {objectName: this.objectName, objectId: this.objectId, cancellationReason : this.selectedCancellationReason};

        notCacheableCall({
            className: 'CancellationRequestCnt',
            methodName: 'updateSobjectToCancelled',
            input: inputs
        })
            .then((response) => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        success(this, 'Cancellation request has successfully done');
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    handleSelectReason(event) {
        this.selectedCancellationReason = event.detail.value;
        this.disabledConfirmButton = !this.selectedCancellationReason;
    }

}