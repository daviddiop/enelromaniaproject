import { LightningElement, api, track } from "lwc";
import { labels } from "c/labels";

export default class MroAccountContactRelationsListItem extends LightningElement {
    labels = labels;


    @track vatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;
    @api from;
    @api accountContactRelation;

    get isPersonAccount() {
        /*
        if (this.accountContactRelation && this.accountContactRelation.Account ) {
            return this.accountContactRelation.Account.IsPersonAccount;
        }
        return true;
         */
        return this.accountContactRelation.isPersonAccount;
    }

    selectContact() {
        this.dispatchEvent(new CustomEvent("select"));
    }

    get fromAccount() {
        return this.from === 'Account';
    }
}