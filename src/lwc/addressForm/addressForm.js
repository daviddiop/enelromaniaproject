import { LightningElement, api, track } from 'lwc';
import normalizeLabel from '@salesforce/label/c.NormalizeAddress';
import forceAddressLabel from '@salesforce/label/c.ForceAddress';
//import cacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.cacheableCall";
import getStructure from "@salesforce/apex/AddressService.getAddressStructure";
import { error } from "c/notificationSvc";
import {labels} from 'c/labels';

export default class AddressForm extends LightningElement {
    //**Start Deprecated */
    @api residentialAddress;
    @api otherAddresses;
    @api isFacultative = false;
    //**Start Deprecated */

    label = {
        normalizeLabel,
        forceAddressLabel
    };

    @api objectApiName;
    @api addressPrefix = '';
    @api address = {};
    @api availableAddresses = [];
    @api addressLayoutApiName;

    @api readOnly ;
    @api disableSave ;
    @api showMessage = false ;
    @api disableNormalize = false;
    @api readOnlyButton = false;
    @api addressNormalized = false;
    @api isNormalize = false;
    @api isNewRecord ;
    @api addressForced = false;

    labels = labels;


    @track addressParts =[];

    initialized = false;

    connectedCallback() {
        if(!this.addressLayoutApiName) {
            this.addressLayoutApiName = 'DefaultAddressLayout'
        }
        getStructure({
            addressStructureApiName: this.addressLayoutApiName
        }).then(response => {
            if (response) {
                let result = response.slice(0);
                let parts = [];
                for(let item of result) {
                    let pickValues = [];
                    for(let value of item.AddressPicklistValues__r || []) {
                        pickValues.push({
                            key: value.QualifiedApiName,
                            value: value.MasterLabel
                        });
                    }
                    parts.push({
                        item: item,
                        values : pickValues
                    });
                }
                this.addressParts = parts;
            }
        });
    }

    handleCityChange(event) {
        this.enableNormalize();
        this.removeError(event);
    }

    handleCountryChange(event) {
        this.enableNormalize();
        this.removeError(event);
    }

    renderedCallback() {
        if (this.initialized) {
            return;
        }
        if(this.readOnly){
            this.addressNormalized = true;
            this.isNormalize = true ;
        }
        this.initialized = true;
    }

    @api
    getValues() {
        let addrForm = this.template.querySelector('c-address-edit-form');
        return addrForm.getAddressFields();
    }

    @api
    isValid() {
        //let addrForm = this.template.querySelector('c-address-edit-form');
        //return addrForm.isValid();
        if(this.readOnly){
           this.isNormalize = true;
        }
        return this.isNormalize;
    }

    forceAddress() {
        let addrForm = this.template.querySelector('c-address-edit-form');
        addrForm.forceAddress();
        this.showMessage = addrForm.forceAddress();
        if(!this.showMessage){
            this.disabledButton();
        }
    }

    normalize() {
        let addrForm = this.template.querySelector('c-address-edit-form');
        addrForm.normalize();
        this.showMessage = addrForm.normalize();
        if(!this.showMessage){
            this.disabledButton();
        }
    }
    disabledButton(){
        this.disabledSave  = false;
        this.dispatchEvent(new CustomEvent('changesavebutton', {detail: { 'disabledSave': this.disabledSave}}));
    }

    handleNameChange(event) {
        event.stopPropagation();
    }
    enableNormalize(){
        if(this.disableNormalize){
            this.disableNormalize = false;
            this.disabledSave  = true;
            this.isNormalize = false;
            this.dispatchEvent(new CustomEvent('changesavebutton', {
                detail: {
                    'disabledSave': this.disabledSave,
                    'isnormalize' : this.isNormalize
                }})
            );
        }
    }

    handleChange(event) {
        this.enableNormalize();
        this.removeError(event);
    }

    handleChangeValue(){
        this.enableNormalize();
    }

    removeError(event) {
        event.target.classList.remove('slds-has-error');
    }

    handleIsReadOnly(event){
        this.addressNormalized = true;
        this.readOnly = event.detail.readOnly;
        this.addressForced  =  event.detail.addressForced;
        if(event.detail.readOnly){
            this.isNormalize = true;
            this.dispatchEvent(new CustomEvent('updateaddressnormalized', {
                detail: {
                    'addressNormalized': true,
                    'isnormalize' : true,
                    'addressForced':event.detail.addressForced
                } }));
        } else {
            error(this, this.labels.addressNotValid);
        }
    }

    editAddress(){
        this.readOnly = false;
        this.addressNormalized = false;
        this.disableNormalize = true;
        this.isNormalize = true;
        this.dispatchEvent(new CustomEvent('updateaddressnormalized', {
            detail: {
                'addressNormalized': false,
                'isnormalize' : this.isNormalize,
                'addressForced':this.addressForced
            } })
        );
    }
}