/**
 * Created by BADJI on 14/05/2020.
 */

import { LightningElement } from 'lwc';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';

export default class MroSapInterrogationIndex extends LightningElement {

    value = 'SAP Query Index';
    labels = labels;
    get options() {
        return [
            { label: 'SAP Query Index', value: 'SAP Query Index' },
            { label: 'SAP Query IVR Gas', value: 'SAP Query IVR Gas' },
        ];
    }

    handleChange(event) {
        this.value = event.detail.value;
        this.template.querySelectorAll('[data-id="meterDetails"]').indexInterogate = this.value;
        this.dispatchEvent(new CustomEvent('selectedvalue', {
                    detail: {
                        'selectedValue': this.value
                    }
                }));
    }
}