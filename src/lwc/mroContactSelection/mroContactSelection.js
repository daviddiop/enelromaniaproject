/**
 * Created by Luca Ravicing on Mar 19, 2020.
 */
import { LightningElement, track,api} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from "c/mroLabels";
import {error} from 'c/notificationSvc';

export default class MroContactSelection extends LightningElement {
    labels = labels;
    @api accountId;
    @api isRendered;
    @api accountName;

    @track contactMap;
    @track contacts;
    @track account;
    @track isBusinessAccount = false;
    @track selectedContact;
    @track disabledConfirm;

    connectedCallback() {

    }

    initialize() {
        this.disabledConfirm=true;
        this.contactMap= new Map();
        notCacheableCall({
            className: 'MRO_LC_ContactSelection',
            methodName: 'getContacts',
            input: {
                'accountId': this.accountId,
            },
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    if (response.data.account){
                        this.account = response.data.account;
                        this.contactMap.set(this.account.Id,this.account);
                    }
                    if (response.data.contacts) {
                        this.contacts = response.data.contacts;
                        let primaryContact=response.data.primaryContact;
                        let length=this.contacts.length;
                        if(length <= 1){
                            if(length == 1){
                                this.selectedContact=this.contacts[0];
                                this.fireSelectionEvent();
                            }
                            else {
                                error(this,this.labels.accountEmptyContact);
                            }
                            if (this.account && this.account.RecordTypeDeveloperName__c
                                && this.account.RecordTypeDeveloperName__c === 'Business'){
                                this.isBusinessAccount = true;
                                this.selectedContact = this.account;
                            }
                            this.fireSelectionEvent();
                        }
                        if(response.data.contacts.length > 0){
                            this.isRendered = true;
                            response.data.contacts.forEach(contact => {
                                //contact=JSON.stringify(contact);
                                contact['isPrimary']= contact.Id === primaryContact;
                                this.contactMap.set(contact.Id,contact);
                            });
                        }



                    }
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    // handle the selected value
    handleChange(event) {
        let selectedValue = event.target.value;
        this.selectedContact=this.contactMap.get(selectedValue);
        console.log("selected======"+JSON.stringify(this.selectedContact));
        if(this.selectedContact){
            this.disabledConfirm=false;
            this.isBusinessAccount = false;
        }

    }
    handleAccountChange(event) {
        let selectedValue = event.target.value;
        this.selectedContact=this.contactMap.get(selectedValue);
        this.isBusinessAccount = true;
        if(this.selectedContact){
            this.disabledConfirm=false;
        }

    }

    @api open() {
        this.initialize();

    }

    handleCancel() {
        this.isRendered = false;
    }
    handleConfirm(event) {
        this.isRendered = false;
        event.preventDefault();
        this.fireSelectionEvent();

    }

    fireSelectionEvent(){
        let detail = {
            selected :this.selectedContact,
            isBusinessAccount: this.isBusinessAccount
        };
        /*if (this.isBusinessAccount){
            detail["isBusinessAccount"] = this.isBusinessAccount;
        }*/
        this.dispatchEvent(new CustomEvent('selected', { detail: detail }));
    }

}