/**
 * Created by Teranga Cloud on 13/05/2020.
 */

import {LightningElement, track, api} from "lwc";
import {NavigationMixin} from "lightning/navigation";
import {labels} from "c/mroLabels";

export default class MroSubProcessSelection extends NavigationMixin(LightningElement) {
    @api caseId;
    @api defaultValue = '';
    @api
    get recordTypeId() {
        return this._recordTypeId;
    }

    set recordTypeId(value) {
        console.log('**** Record type Id ', value);
        this._recordTypeId = value;
    }

    @api disabled = false;

    @track subProcessSelected = "";
    @track _recordTypeId;

    labels = labels;

    handleChange(event) {
        this.subProcessSelected = event.target.value;
        this.sendEvent();
    }

    sendEvent() {
        console.log("subProcessSelected = " + this.subProcessSelected);
        const changedEvent = new CustomEvent("changed", {
            detail: {
                subProcessSelected: this.subProcessSelected,
            }
        });
        this.dispatchEvent(changedEvent);
    }
}