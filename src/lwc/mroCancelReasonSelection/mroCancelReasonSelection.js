import {LightningElement, api, track} from 'lwc';
import {labels} from "c/mroLabels";
import {error, success, warn} from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
export default class MroCancelReasonSelection extends LightningElement {

    @api opportunityId;
    @api dossierId;
    @api isRendered;

    @api open() {
        this.isRendered = true
    }

    @track showReasonDetails = false;
    @track disabledConfirmButton = true;
    @track showLoadingSpinner = true;
    @track cancelReason;
    @track detailsReason;

    labels = labels;

    handleLoad() {
        if (!this.isRendered){
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = '';
            });
        }
        this.cancelReason = '';
        this.detailsReason = '';
        this.showLoadingSpinner = false;
    }
    handleConfirm(event) {
        event.preventDefault();
        if (this.opportunityId) {
            this.template.querySelector("lightning-record-edit-form").submit();
        } else if (this.dossierId) {
            this.fireEvent();
        }
    }

    handleSuccess(event) {
        event.preventDefault();
        event.stopPropagation();
        this.updateCancellationDetails();
        this.updateCancellationReason();
         //this.fireEvent();


    }

    updateCancellationDetails(){
        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });
        this.cancelReason=fields["LostReason__c"];
        this.detailsReason=fields["ClosingDetails__c"];
    }
    fireEvent() {
        if(!this.cancelReason){
            this.updateCancellationDetails();
        }
        const selectedEvent = new CustomEvent('savereason', {
            detail: {
                cancelReason: this.cancelReason,
                detailsReason: this.detailsReason
            }
        });
        this.dispatchEvent(selectedEvent);

        this.isRendered = false;
    }

    updateCancellationReason() {
        this.showLoadingSpinner = true;
        let inputs = {
            opportunityId: this.opportunityId,
            cancellationReason: this.cancelReason,
            cancellationDetails: this.detailsReason
        };

        notCacheableCall({
            className: 'MRO_LC_CancellationRequest',
            methodName: 'updateCancellationReasons',
            input: inputs
        })
            .then((response) => {
                this.showLoadingSpinner = false;
                if (response) {
                    if (response.error) {
                        error(this, response.error);
                        return;
                    }
                    success(this, this.labels.cancelledSuccess);
                    this.fireEvent();
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    handleReasonChange(event) {
        let reasonField = event.target;
        this.showReasonDetails = false;
        if (reasonField.value && reasonField.value !== '') {
            if (reasonField.value === 'Other reason') {
                this.showReasonDetails = true;
            } else {
                this.disabledConfirmButton = false;
            }
        } else {
            this.disabledConfirmButton = true;
        }
    }

    handleDetailsReasonChange(event) {
        let detailsReason = event.target;
        this.disabledConfirmButton = !(detailsReason.value && detailsReason.value !== '');
    }

    handleCancel() {
        this.isRendered = false;
    }
}