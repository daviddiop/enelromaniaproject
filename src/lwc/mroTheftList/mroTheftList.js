/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 23.09.20.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
//import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
import { NavigationMixin } from 'lightning/navigation';

export default class MroTheftList extends NavigationMixin(LightningElement) {

    channelName = '/event/DocumentManagementSystemEvent__e';
    subscription = {};

    @api servicePointCode;
    @api servicePointId;
    @api startDate;
    @api endDate;
    @api displayedDocumentColumns = ['processRef', 'processId', 'status', 'distributorId', 'requestType'];
    @api displayedProcessColumns = ['processRef', 'processId', 'status', 'distributorId', 'requestType', 'executionDate', 'capTriplePrice'];
    @api retrieveName = 'Retrieve Theft';
    @api disabled = false;

    @track documentTableColumns;
    @track documentDetailColumns;
    @track processTableColumns;
    @track processDetailColumns;

    @track documentList;
    @track processList;

    @track showDetails;
    @track detailType;
    @track detailsList;

    @track showLoadingSpinner;

    //TODO: create labels
    labels = {
        close: 'Close',
        theftProcessRef: 'ProcessRef',
        theftProcessId: 'ProcessId',
        theftStatus: 'Status',
        theftDistributorId: 'DistributorId',
        theftServicePointCode: 'ServicePointCode',
        theftRequestType: 'RequestType',
        theftDocumentId: 'DocumentId',
        theftDocumentType: 'DocumentType',
        theftObjectType: 'ObjectType',
        theftObjectKey: 'ObjectKey',
        theftOdlType: 'OdlType',
        theftExecutionDate: 'ExecutionDate',
        theftTypeRecoveral: 'TypeRecoveral',
        theftCaseNumber: 'CaseNumber',
        theftDacFrom: 'DacFrom',
        theftDacTo: 'DacTo',
        theftAlteCor: 'AlteCor',
        theftActiveEnergyF2: 'ActiveEnergyF2',
        theftActiveEnergyF3: 'ActiveEnergyF3',
        theftIndReactiveEnergy: 'IndReactiveEnergy',
        theftIndTriplePrice: 'IndTriplePrice',
        theftCapReactiveEnergy: 'CapReactiveEnergy',
        theftCapTriplePrice: 'CapTriplePrice',
    };

    documentColumns = [
        {label: this.labels.theftProcessRef, fieldName: 'processRef', type: 'text'},
        {label: this.labels.theftProcessId, fieldName: 'processId', type: 'text'},
        {label: this.labels.theftStatus, fieldName: 'status', type: 'text'},
        {label: this.labels.theftDistributorId, fieldName: 'distributorId', type: 'text'},
        {label: this.labels.theftServicePointCode, fieldName: 'servicePointCode', type: 'text'},
        {label: this.labels.theftRequestType, fieldName: 'requestType', type: 'text'},
        {label: this.labels.theftDocumentId, fieldName: 'documentId', type: 'text'},
        {label: this.labels.theftDocumentType, fieldName: 'documentType', type: 'text'},
        {label: this.labels.theftObjectType, fieldName: 'objectType', type: 'text'},
        {label: this.labels.theftObjectKey, fieldName: 'objectKey', type: 'text'},
        {
            type: 'button', typeAttributes: {
                label: 'Details',
                name: 'Details',
                title: 'Details',
                disabled: false,
                value: 'details',
                iconPosition: 'left'
            }
        }
    ];

    processColumns = [
        {label: this.labels.theftProcessRef, fieldName: 'processRef', type: 'text'},
        {label: this.labels.theftProcessId, fieldName: 'processId', type: 'text'},
        {label: this.labels.theftStatus, fieldName: 'status', type: 'text'},
        {label: this.labels.theftDistributorId, fieldName: 'distributorId', type: 'text'},
        {label: this.labels.theftServicePointCode, fieldName: 'servicePointCode', type: 'text'},
        {label: this.labels.theftRequestType, fieldName: 'requestType', type: 'text'},
        {label: this.labels.theftOdlType, fieldName: 'odlType', type: 'text'},
        {label: this.labels.theftExecutionDate, fieldName: 'executionDate', type: 'date'},
        {label: this.labels.theftTypeRecoveral, fieldName: 'typeRecoveral', type: 'text'},
        {label: this.labels.theftCaseNumber, fieldName: 'caseNumber', type: 'text'},
        {label: this.labels.theftDacFrom, fieldName: 'dacFrom', type: 'date'},
        {label: this.labels.theftDacTo, fieldName: 'dacTo', type: 'date'},
        {label: this.labels.theftAlteCor, fieldName: 'alteCor', type: 'text'},
        {label: this.labels.theftActiveEnergyF2, fieldName: 'activeEnergyF2', type: 'number'},
        {label: this.labels.theftActiveEnergyF3, fieldName: 'activeEnergyF3', type: 'number'},
        {label: this.labels.theftIndReactiveEnergy, fieldName: 'indReactiveEnergy', type: 'number'},
        {label: this.labels.theftIndTriplePrice, fieldName: 'indTriplePrice', type: 'number'},
        {label: this.labels.theftCapReactiveEnergy, fieldName: 'capReactiveEnergy', type: 'number'},
        {label: this.labels.theftCapTriplePrice, fieldName: 'capTriplePrice', type: 'number'},
        {
            type: 'button', typeAttributes: {
                label: 'Details',
                name: 'Details',
                title: 'Details',
                disabled: false,
                value: 'details',
                iconPosition: 'left'
            }
        }
    ];

    connectedCallback() {
        this.documentTableColumns = this.documentColumns.filter(column => {
            return column.type === 'button' || this.displayedDocumentColumns.includes(column.fieldName);
        });
        this.documentDetailColumns = this.documentColumns.filter(column => {
            return column.type !== 'button' && !this.displayedDocumentColumns.includes(column.fieldName);
        });
        this.processTableColumns = this.processColumns.filter(column => {
            return column.type === 'button' || this.displayedProcessColumns.includes(column.fieldName);
        });
        this.processDetailColumns = this.processColumns.filter(column => {
            return column.type !== 'button' && !this.displayedProcessColumns.includes(column.fieldName);
        });
    }

    handleRetrieveTheft() {
        this.showLoadingSpinner = true;
        notCacheableCall({
            className: 'MRO_LC_TheftQuery',
            methodName: 'listTheft',
            input: {
                'servicePointCode': this.servicePointCode,
                'servicePointId': this.servicePointId,
                'startDate': this.startDate,
                'endDate': this.endDate,
            }
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, 'Unexpected Error');
                return;
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
                return;
            }
            this.documentList = apexResponse.data.documents;
            this.documentList[0].documentId = 'F20RF0004739';
            this.documentList[0].objectKey = '05530207';
            this.processList = apexResponse.data.processes;
        });
    }

    handleDocumentRowAction(event) {
        this.detailType = 'document';
        const rowId =  event.detail.row.processId;
        this.detailsList = this.documentList.filter(document => {
            return document.processId === rowId;
        });
        this.detailsList = this.documentList.map(document => {
            document.columns = [];
            this.documentDetailColumns.forEach(col => {
                document.columns.push(document[col.fieldName]);
            });
            document.showDownload = !!document.contentDocumentId;
            document.downloadhref = '/sfc/servlet.shepherd/document/download' + '/' + document.contentDocumentId;
            document.showSpinner = false;
            document.showSync = !document.contentDocumentId;
            return document;
        });

        this.showDetails = true;
    }

    subscribeDMSEvent() {
        const messageCallback = (response) => {
            console.log('New message received : ', JSON.stringify(response));
            this.payload = JSON.stringify(response);
            this.handleRetrieveTheft();
        };
        subscribe(this.channelName, -1, messageCallback).then(response => {
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
            this.subscription = response;
        });
    }

    handleFilePreviewClick(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state: {
                selectedRecordId: event.currentTarget.name
            }
        })
    }

    handleSync (event) {
        let documentId = event.target.dataset.id;
        let objectKey = event.target.dataset.value;

        this.getFileFromDMS(documentId, objectKey);
    }

    getFileFromDMS(documentId, objectKey, fileMetadataId){
        console.log(documentId, objectKey, fileMetadataId);
        let input = {
            documentId: documentId,
            objectKey: objectKey,
            fileMetadataId: fileMetadataId
        };
        notCacheableCall({
            className: "MRO_LC_TheftQuery",
            methodName: "getFileFromDMS",
            input: input
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }

            if(apexResponse.data.fileMetadataId && !fileMetadataId){
                return this.getFileFromDMS(documentId, objectKey, apexResponse.data.fileMetadataId);
            }
            console.log(JSON.stringify(this.detailsList));
            this.detailsList = this.detailsList.map(document => {
                if(document.documentId === documentId) document.showSpinner = true;
                return document;
            });
            console.log(JSON.stringify(this.detailsList));
            this.subscribeDMSEvent();

        });
    }

    handleProcessRowAction(event) {
        this.detailType = 'process';
        const rowId =  event.detail.row.processId;
        this.detailsList = this.processList.filter(process => {
            return process.processId === rowId;
        });
        this.showDetails = true;
    }

    closeDetailList() {
        this.showDetails = false;
    }

    get detailColumns() {
        return this.detailType === 'document' ? this.documentDetailColumns : this.processDetailColumns;
    }

    get isDocumentDetail() {
        return this.detailType === 'document';
    }
}