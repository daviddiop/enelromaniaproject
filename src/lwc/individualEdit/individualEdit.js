import { LightningElement, api, track } from 'lwc';
import { labels } from 'c/labels';
import { NavigationMixin } from 'lightning/navigation';
import { error, success, warn } from 'c/notificationSvc';
import INDIVIDUAL_OBJECT from '@salesforce/schema/Individual';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class individualEdit extends NavigationMixin(LightningElement) {
    @api recordId;
    @api interactionId;
    @api searchFields;
    @api isRedirectAllowed;
    @api disableFilled;
    @api hideCancel;
    @api hideBack = false;

    @track interlocutor = {
        firstName: '',
        lastName: '',
        nationalId: '',
        phone: '',
        email: '',
        toString: function () {
            return JSON.stringify(this);
        }
    };

    labels = labels;
    interlocutorObject = INDIVIDUAL_OBJECT;

    get cancelRendered() {
        if (this.hideCancel === 'true') {
            return false;
        }
        return true;
    }

    get firstName() {
        if (this.searchFields && this.searchFields.FirstName) {
            /*
            let nameParts = this.searchFields.FirstName.split(' ');
            if (nameParts.length > 1) {
                return nameParts[0];
            }
            */
            return this.searchFields.FirstName;
        }
        return '';
    }
    get lastName() {
        /*
        if (this.searchFields && this.searchFields.FirstName) {
            let nameParts = this.searchFields.FirstName.split(' ');
            if (nameParts.length > 1) {
                return nameParts[1];
            }
            return '';
        }
        */
        if (this.searchFields && this.searchFields.LastName) {
            return this.searchFields.LastName;
        }
        return '';
    }
    get nationalId() {
        if (this.searchFields && this.searchFields.NationalIdentityNumber) {
            return this.searchFields.NationalIdentityNumber;
        }
        return '';
    }
    get email() {
        if (this.searchFields && this.searchFields.Email) {
            return this.searchFields.Email;
        }
        return '';
    }
    get phone() {
        if (this.searchFields && this.searchFields.Phone) {
            return this.searchFields.Phone;
        }
        return '';
    }

    get disabled() {
        let disabled = {};
        disabled.firstName = !!(this.searchFields && this.searchFields.FirstName && this.disableFilled);
        disabled.lastName = !!(this.searchFields && this.searchFields.LastName && this.disableFilled);
        disabled.nationalId = !!(this.searchFields && this.searchFields.NationalIdentityNumber && this.disableFilled);
        disabled.email = !!(this.searchFields && this.searchFields.Email && this.disableFilled);
        disabled.phone = !!(this.searchFields && this.searchFields.Phone && this.disableFilled);
        return disabled;
    }

    connectedCallback() {
        notCacheableCall({
            className: 'IndividualCnt',
            methodName: 'initData'
        }).then((responseData) => {
            if (responseData.isError) {
                error(this, JSON.stringify(responseData.error));
            } else {
                this.isSaveUnIdentifiedInterlocutorAllowed = responseData.data.isSaveUnIdentifiedInterlocutorAllowed;
            }
        }).catch((errorMsg) => {
            error(this, errorMsg.body.output.errors[0].message);
        });
    }

    /**
     * @description Method to make call server for saving new Individual
     * @param {*} event 
     */
    handleError(event) {
         error(this, event.detail.message);
    }
    handleSave() {
        if (this.validateFields() === false) {
            let nationalId = this.template.querySelector('[data-id="NationalIdentityNumber__c"]').value;
            if (this.interactionId && this.isSaveUnIdentifiedInterlocutorAllowed && !nationalId && !this.disableFilled) {
                error(this, this.labels.fistnameOrLastnameRequired);
            } else {
                error(this, this.labels.requiredFields);
            }
        } else {
            this.populateInterlocutor();
            let inputs = { interactionId: this.interactionId, interlocutorDTO: this.interlocutor.toString() };
            notCacheableCall({
                className: 'IndividualCnt',
                methodName: 'saveIndividual',
                input: inputs
            }).then((response) => {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    if (response.data.duplicateIndividuals) {
                        warn(this, this.labels.duplicatedRecord);
                        this.dispatchEvent(new CustomEvent('duplicatefound', { detail: {
                            'type': 'search',
                            'individualList': response.data.duplicateIndividuals,
                            'searchFields': this.searchFields
                        }}, {bubbles: true}));
                    } else {
                        const individualId = response.data.individualId;
                        if (individualId) {
                            success(this, `${this.interlocutor.firstName} ${this.interlocutor.lastName} ` + this.labels.individualCreated);
                            if (this.isRedirectAllowed !== 'false') {
                                this.addEventListener('success', this.handleNavigateToRecord(individualId));
                            }
                            this.dispatchEvent(new CustomEvent('success', {
                                detail: {
                                    'individualId': individualId
                                }
                            }, {bubbles: true}));
                        } else if (!individualId && this.interactionId && this.isSaveUnIdentifiedInterlocutorAllowed && !this.disableFilled) {
                            this.dispatchEvent(new CustomEvent('success', {
                                detail: {
                                    'individualId': null
                                }
                            }, {bubbles: true}));
                        }
                    }
                }
            }).catch((errorMsg) => {
                error(this, errorMsg.body.output.errors[0].message);
            });
        }
    }

    handleCancel() {
        if(this.isRedirectAllowed !== 'false'){
            this.addEventListener('cancel', this.handleNavigateToRecord(this.recordId));
        }
        this.dispatchEvent(new CustomEvent('cancel', {
            detail: {
                'searchFields': this.searchFields
            }
        }));
    }

    handleBack() {
        this.dispatchEvent(new CustomEvent('back', {
            detail: {
                'page': 'individualList',
                'searchFields': this.searchFields
            }
        }));
    }

    /**
     * @description Method to make call server for updating an existing Individual
     * @param {*} event 
     */
    handleUpdate() {
        if (this.validateFields() === false) {
            error(this, this.labels.requiredFields);
        } else {
            this.interlocutor.id = this.recordId;
            this.populateInterlocutor();
            let inputs = { interactionId: this.interactionId, interlocutorDTO: this.interlocutor.toString() };
            notCacheableCall({
                className: 'IndividualCnt',
                methodName: 'updateIndividual',
                input: inputs
            })
                .then((response) => {
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    } else {
                        const individualId = response.data.individualId;
                        if (individualId) {
                            success(this, `Individual ${this.interlocutor.firstName}  ${this.interlocutor.lastName}  was updated!`);
                            if(this.isRedirectAllowed !== 'false'){
                                this.addEventListener('success', this.handleNavigateToRecord(individualId));
                            }
                            this.dispatchEvent(new CustomEvent('success', { bubbles: true }));
                        }
                    }
                })
                .catch((errorMsg) => {
                    error(this, errorMsg.body.output.errors[0].message);
                });
        }
    }

    /**
     * @description Handle change input
     *              Remove error class on input field if required and not filled
     * @param {*} event 
     */
    handleChangeInput(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
        if (inputCmp.fieldName === 'FirstName') {
            this.interlocutor.firstName = inputCmp.value;
        } else if (inputCmp.fieldName === 'LastName') {
            this.interlocutor.lastName = inputCmp.value;
        } else if (inputCmp.fieldName === 'NationalIdentityNumber__c') {
            this.interlocutor.nationalId = inputCmp.value;
        }

        if (inputCmp.name === 'email') {
            this.interlocutor.email = inputCmp.value;
        } else if (inputCmp.name === 'tel') {
            this.interlocutor.phone = inputCmp.value;
        }
    }

    /**
     * @description Method to check all input fields ,with custom class wr-required, if are filled or not
     * @returns true if all fields are valid otherwise false
     */
    validateFields = () => {
        let areValid = true;
        let nationalId = this.template.querySelector('[data-id="NationalIdentityNumber__c"]').value;
        if (this.interactionId && this.isSaveUnIdentifiedInterlocutorAllowed && !nationalId && !this.disableFilled) {
            let firstname = this.template.querySelector('[data-id="FirstName"]').value;
            let lastname = this.template.querySelector('[data-id="LastName"]').value;
            if (!firstname && !lastname) {
                return false;
            }
            return true;
        }
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

    populateInterlocutor() {
        this.interlocutor.firstName = this.template.querySelector('[data-id="FirstName"]').value;
        this.interlocutor.lastName = this.template.querySelector('[data-id="LastName"]').value;
        this.interlocutor.nationalId = this.template.querySelector('[data-id="NationalIdentityNumber__c"]').value;
        this.interlocutor.email = this.template.querySelector('[data-id="Email"]').value;
        this.interlocutor.phone = this.template.querySelector('[data-id="Phone"]').value;
    }

    /**
     * Navigates to new individual record page 
     *
     * @param String individualId new Individual Id
     */
    handleNavigateToRecord(individualId) {
        if (individualId) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: individualId,
                    objectApiName: INDIVIDUAL_OBJECT,
                    actionName: 'view'
                }
            });
        } else {
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'Individual',
                    actionName: 'list'
                }
            });
        }
    }
}