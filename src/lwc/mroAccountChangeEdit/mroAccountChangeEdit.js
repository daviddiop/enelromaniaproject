import { LightningElement, api, track } from "lwc";
import { NavigationMixin } from "lightning/navigation";
import { error, success } from "c/notificationSvc";
import fieldStyle from "@salesforce/resourceUrl/EnergyAppResources";
import { loadStyle } from "lightning/platformResourceLoader";
import { labels } from "c/mroLabels";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import cacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.cacheableCall";
import {checkCNP, checkCUI, checkMail, checkONRC} from "c/mroValidations";


export default class mroAccountChangeEdit extends NavigationMixin(LightningElement) {

  @api recordId;
  @api recordTypeCase;

    // @api isCaseModalByAddressForm = false;
    @api dossierId;
    @api isPersonAccount;
    @api isBusinessAccountToEdit = false;
    @api accountId;
    @api showHeader;
    @api closeModal = false;
    @api disabled=false;
    @api title;
    @api subProcess;
    @api contact;
    @api origin;
    @api channel;
    @api subProcessChanges;
    @api casePhase;
    @api caseStatus;

  REQUIRED_CLASS='wr-required';
  ERROR_CLASS='slds-has-error';

  @track defaultTitle;
  // @track selectCaseFields = ["AccountName__c", "AccountEmail__c"];
  // @track accountCaseId = this.accountId;
  @track caseId;
  @track contactId;

  @track account;
  @track address;

  @track accountFormId="editAccountForm";
  @track caseFormId="editCaseForm";
  @track contactFormId="editContactForm";
  @track dataIdContactMobile="contactMobile";
  @track dataIdContactPhone="contactPhone";
  @track dataIdContactMail="contactMail";
  @track dataIdOldContactMobile="oldContactMobile";
  @track dataIdOldContactPhone="oldContactPhone";
  @track dataIdOldContactMail="oldContactMail";
  @track oldContactMobile;
  @track oldContactPhone;
  @track oldContactMail;

  @track showLoadingSpinner = true;

  @track readOnlyAddress=false;
  @track contactEmail;
  @track contactPhone;
  @track contactMobile;
  @track accountEmail;
  @track accountPhone;
  @track accountMobile;
  @track accountNaceCode;
  @track accountNaceCodeRef;
  @track accountOnrcCode;
  @track accountNationalIdentityNumber;
  @track accountLastName;
  @track accountFirstName;
  @track accountVATNumber;
  @track accountVATExempted;
  @track accountName;
  @track accountKey;
  @track accountCompany;
  @track accountForeignCompany;
  @track accountForeignCitizen;
  @track accountWebsite;
  @track accountAnnualRevenue;
  @track accountNumberOfEmployees;
  @track isChangeAccountName= false;
  @track isChangeCompanyName= false;
  @track isChangeContactData= false;
  @track isChangeCNP= false;
  @track isChangeCUI= false;
  @track isChangeONRC= false;
  @track isChangeCAEN= false;
  @track isChangeOtherFields= false;
  @track isChangeResidentialAddress=false;

   @track accountModifiedMobile;
   @track accountModifiedPhone;
   @track accountModifiedEmail;


  @track isNormalize;
  @track addressForced;

  @track showFirstName;
  @track showLastName;
  @track showAccountName;
  @track showCompanyName;
  @track showVatNumber;
  @track showVATExempted;
  @track showContactMobile;
  @track showContactPhone;
  @track showContactEmail;
  @track showCNP;
  @track showForeignCitizen;
  @track showResidentialAddress;
  @track showForeignCompany;
  @track showOnrcCode;
  @track showNaceCode;
  @track showAddressForm;
  @track showWebsite;
  @track showNumbOfEmployee;
  @track showAnnualRevenue;
  @ track caseCnp;
  @ track caseCnpField;
  @ track caseCui;
  @ track caseCuiField;
  @ track caseOnrc;
  @ track caseOnrcField;
  @ track caseNaceCode;
  @ track isPrimaryContact=false;
  @track primaryContact;
  @track newAnnualRevenue;
  @track newWebsite;
  @track newNumbOfEmployees;

  @track vatPrefix;

  // @track isBusinessProspect;
  // @track isPersonProspect;

  // accountRecordType='BusinessProspect';
  //
  // get isBusinessProspect(){
  //   return this.accountRecordType==='Business'
  // }
  //
  // get isPersonProspect(){
  //   return this.accountRecordType==='BusinessProspect'
  // }
  //
  // testUrl = new URL(window.location.href).searchParams;
  // caseId = this.testUrl.get('c__caseId');
  // accountId = this.testUrl.get('c__accountId');

  defaultCaseOrigin;
  defaultCaseStatus;
  labels = labels;

  connectedCallback() {
    Promise.all([
      loadStyle(this, fieldStyle + "/style.css")
    ])
      .then(() => {
      })
      .catch(err => {
        error(this, err.body.message);
      });
    // if (this.selectCaseFieldsValues && Object.keys(this.selectCaseFieldsValues).length) {
    //   this.caseFields = Object.keys(this.selectCaseFieldsValues);
    // } else if (this.selectCaseFields.length) {
    //   this.caseFields = this.selectCaseFields;
    // }
    //this.getConstants();

    // if (this.isCaseModalByAddressForm) {
    //   this.getRecordTypeConnection();
    // }

    this.spinner = true;
    this.isNormalize = true;
    if (this.accountId) {
      // this.initSiteAddress();
      this.initAccountRecord();
    }
    // if (this.recordId) {
    //   this.initCaseAddress();
    // }
    // if (this.servicePointId) {
    //   this.initServicePoint();
    // }
    this.showHeader = true;
    this.defaultTitle = this.title ? this.title : "Custumer Data Change";
    /*let contactPhoneField = this.template.querySelector(this.getDataIdString(this.dataIdContactMobile));
    let contactMobileField = this.template.querySelector(this.getDataIdString(this.dataIdContactMobile));
    let contactMailField = this.template.querySelector(this.getDataIdString(this.dataIdContactMail));
    this.oldContactPhone =  contactPhoneField.value;
    this.oldContactMobile =  contactMobileField.value;
    this.oldContactMail =  contactMailField.value;*/

    }
    @api
    updateSubProcess(){
        console.log("case==========="+this.caseStatus + '   '+this.casePhase);
        if(this.contact){
            this.contactId=this.contact.Id;
            this.updateContact();
        }
        console.log('###isBusinessAccountToEdit: '+this.isBusinessAccountToEdit)
        if (this.isBusinessAccountToEdit){
            console.log('###contact: '+JSON.stringify(this.contact))
            console.log('###accountId: '+this.account.Id)
            this.contact = null;
            this.initAccountRecord();
        }

        this.isChangeAccountName =this.subProcessChanges.isChangeAccountName;
        this.isChangeCompanyName=this.subProcessChanges.isChangeCompanyName;
        this.isChangeContactData=this.subProcessChanges.isChangeContactData;
        this.isChangeCNP=this.subProcessChanges.isChangeCNP;
        this.isChangeCUI=this.subProcessChanges.isChangeCUI;
        this.isChangeONRC=this.subProcessChanges.isChangeONRC;
        this.isChangeCAEN=this.subProcessChanges.isChangeCAEN;
        this.isChangeOtherFields=this.subProcessChanges.isChangeOtherFields;
        this.isChangeResidentialAddress=this.subProcessChanges.isChangeResidentialAddress;

    this.showAccountName=false;
    this.showFirstName= false;
    this.showLastName= false;
    this.showCompanyName= false;
    this.showVatNumber= false;
    this.showVATExempted=false;
    this.showContactMobile= false;
    this.showContactPhone= false;
    this.showContactEmail= false;
    this.showCNP= false;
    this.showForeignCitizen= false;
    this.showResidentialAddress= false;
    this.showForeignCompany= false;
    this.showOnrcCode= false;
    this.showNaceCode= false;
    this.showAddressForm=false;
    this.showWebsite=false;
    this.showNumbOfEmployee=false;
    this.showAnnualRevenue=false;

    if(this.isChangeAccountName) {
      this.showFirstName= true;
      this.showLastName= true;
    } else if (this.isChangeCompanyName){
        this.showAccountName= true;
        this.showVatNumber= true;
        this.showOnrcCode= true;
    }else if (this.isChangeContactData){
        this.showContactEmail= true;
        this.showContactMobile= true;
        this.showContactPhone= true;
    }else if (this.isChangeCNP){
        this.showCNP= true;
        this.showAccountName= true;
        this.showForeignCitizen=true;
    }else if (this.isChangeCUI){
        this.showVatNumber=true;
        this.showAccountName=true;
        this.showResidentialAddress=true;
        this.showForeignCompany=true;
        this.showVATExempted =true;

    }else if (this.isChangeONRC){
      this.showOnrcCode=true;
      this.showAccountName= true;
      this.showResidentialAddress=true;
    }else if (this.isChangeCAEN){
      this.showNaceCode=true;
    }else if(this.isChangeResidentialAddress){
      this.showAddressForm=true;
    }else if(this.isChangeOtherFields){
      console.log('isChangeOtherfields');
      this.showWebsite=true;
      this.showNumbOfEmployee=true;
      this.showAnnualRevenue=true;
    }
console.log("this.isChangeResidentialAddress=="+this.isChangeResidentialAddress);
  }


  // initServicePoint() {
  //   let inputs = { servicePointId: this.servicePointId };
  //
  //   notCacheableCall({
  //     className: "MRO_LC_OpportunityServiceItem",
  //     methodName: "getPointCodeFields",
  //     input: inputs
  //   })
  //     .then((apexResponse) => {
  //
  //       if (apexResponse.isError) {
  //         return error(this, JSON.stringify(apexResponse.error));
  //       }
  //
  //       this.setCaseAddress(apexResponse.data.servicePointAddress);
  //
  //       this.spinner = false;
  //     })
  //     .catch((errorMsg) => {
  //       this.spinner = false;
  //       error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
  //
  //     });
  // }

  initAccountRecord() {
    console.log("init account");
    let inputs = { accountId: this.accountId, dossierId: this.dossierId };
    notCacheableCall({
      className: "MRO_LC_AccountChangeEdit",
      methodName: "initialize",
      input: inputs
    })
      .then((response) => {
        let result=response.data;
        if (result.error) {
          return error(this, result.errorMsg);
        }
        this.account = result.account;
        this.vatPrefix=result.vatPrefix;
        this.defaultCaseOrigin = result.defaultCaseOrigin;
        this.defaultCaseStatus = result.defaultCaseStatus;
        this.primaryContact = result.account.PrimaryContact__c;
        if(!this.contact && this.account !== null){
            this.accountMobile = this.account.MobilePhone;
            this.accountPhone = this.account.Phone;
            this.accountEmail = this.account.Email;
        }
        this.caseId = result.caseId;
        this.initCaseFields(this.account);
        this.initAddress();
      });
  }


  initAddress(){
    let address = {};


    address.streetId = this.account.ResidentialStreetId__c;
    address.streetNumber = this.account.ResidentialStreetNumber__c;
    address.streetName = this.account.ResidentialStreetName__c;
    address.streetNumberExtn = this.account.ResidentialStreetNumberExtn__c;
    address.streetType = this.account.ResidentialStreetType__c;
    address.apartment = this.account.ResidentialApartment__c;
    address.building = this.account.ResidentialBuilding__c;
    address.block=this.account.ResidentialBlock__c;
    address.floor = this.account.ResidentialFloor__c;
    address.locality = this.account.ResidentialLocality__c;
    address.postalCode = this.account.ResidentialPostalCode__c;
    address.province = this.account.ResidentialProvince__c;
    address.city = this.account.ResidentialCity__c;
    address.country = this.account.ResidentialCountry__c;
    address.addressKey = this.account.ResidentialAddressKey__c;
    this.address = address;
    if (this.account.ResidentialAddressNormalized__c) {
      //this.readOnly = true;
    }
    console.log('address:  '+JSON.stringify(address));
  }

  initCaseFields(account) {
    this.accountVATNumber = account.VATNumber__c;
    if(this.accountVATNumber == null){
      this.accountVATNumber='';
    }
    this.accountVATExempted=account.VATExempted__c;
    this.accountKey=account.Key__c;
    this.accountName = account.Name;
    this.accountCompany=account.Company__c;
    this.accountFirstName = account.FirstName;
    this.accountLastName = account.LastName;
    this.accountNationalIdentityNumber = account.NationalIdentityNumber__pc;
    this.accountOnrcCode = account.ONRCCode__c;
    this.accountNaceCode = account.NACECode__c;
    this.accountNaceCodeRef = account.NACEReference__c;
    this.accountForeignCompany = account.ForeignCompany__c;
    this.accountForeignCitizen = account.ForeignCitizen__pc;

    this.contactMobile = this.isPersonAccount ? account.PersonMobilePhone : '';
    this.contactPhone = this.isPersonAccount ? account.Phone : '';
    this.contactEmail = this.isPersonAccount ? account.PersonEmail : '';
    this.accountMobile=this.contactMobile;
    this.accountPhone=this.contactPhone;
    this.accountEmail= this.contactEmail;
    this.accountWebsite= account.Website;
    this.accountNumberOfEmployees= account.accountNumberOfEmployees;
    this.accountAnnualRevenue = account.AnnualRevenueYear__c;
  }
  handleChangeVatNumber(event){
    this.caseCuiField= this.template.querySelector('[data-id="CUI"]');
    this.removeErrorStyle(this.caseCuiField);
    let checked=event.target.value;
    let value= this.accountKey;
    if(value != null && value !== ''){
      let index=value.indexOf("_");
      if(index != -1) {
        value=value.substring(0,index);
      }
      if(!checked){
        if(!value.startsWith(this.vatPrefix)){
          value= this.vatPrefix+ value;
        }
      }
      this.caseCuiField.value=value;
      this.caseCui = this.caseCuiField.value;
      let change = this.caseCui !== this.accountVATNumber;
      this.fireChangeEvent(change);
    }

  }

  handleChangePrimaryContact(event) {
    this.isPrimaryContact = event.target.checked;
    let change= false;
    if(!this.primaryContact){
      change=this.isPrimaryContact;
    } else if(this.isPrimaryContact){
      change=(this.contactId !== this.primaryContact);
    }
    if(!change ){
      change= this.validateChangeContact();
    }

    this.fireChangeEvent(change);
  }
/*
  getConstants() {
    cacheableCall({
      className: "Utils",
      methodName: "getAllConstants"
    })
      .then(apexResponse => {
        if (apexResponse) {
          if (apexResponse.isError) {
            error(this, apexResponse.error);
          } else {
            this.defaultCaseOrigin = apexResponse.data.CASE_DEFAULT_CASE_ORIGIN;
            this.defaultCaseStatus = apexResponse.data.CASE_STATUS_DRAFT;
          }
        }
      });
  }*/


  // getRecordTypeConnection() {
  //   cacheableCall({
  //     className: "CaseCnt",
  //     methodName: "getCaseDescribe"
  //   })
  //     .then(apexResponse => {
  //       if (apexResponse.isError) {
  //         error(this, JSON.stringify(apexResponse.error));
  //       }
  //       if (apexResponse) {
  //         if (apexResponse.isError) {
  //           error(this, apexResponse.error);
  //         } else {
  //           this.recordTypePicklist = apexResponse.data.recordTypePicklistValues;
  //         }
  //       }
  //     });
  // }

  handleLoad() {
    if (this.selectCaseFieldsValues) {
      this.template.querySelectorAll("lightning-input-field").forEach(element => {
        element.value = this.selectCaseFieldsValues[element.fieldName];
      });
    }
    this.showLoadingSpinner = false;
  }

  @api
  submit() {
    this.handleSubmit();
  }

  handleSubmit(event) {
    this.showLoadingSpinner = true;
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    let fields = {};

    let form = this.template.querySelector(this.getFormString(this.caseFormId));
    this.template.querySelectorAll(this.getFormInputFieldsString(this.caseFormId)).forEach(element => {
      fields[element.fieldName] = element.value;
    });

    this.showLoadingSpinner = true;
    if(this.isChangeResidentialAddress){
      let addrForm = this.template.querySelector('c-mro-address-form');
      if (!addrForm.isValid()) {
        error(this, this.labels.addressMustBeVerified);
        this.showLoadingSpinner = false;
        return;
      }

      let addrFields = addrForm.getValues();
      for (let f in addrFields) {
        if (addrFields.hasOwnProperty(f)) {
          fields[f] = addrFields[f];
        }
      }

      fields.AddressAddressNormalized__c = this.addressForced === false;

    }

    if (this.accountId) {
      fields.AccountId = this.accountId;
    }
    if(this.caseStatus){
      fields.Status=this.caseStatus;
    }
    if(this.casePhase){
      fields.Phase__c=this.casePhase;
    }else {
      fields.Status = this.defaultCaseStatus;
    }

    if (this.dossierId) {
      fields.Dossier__c = this.dossierId;
    }

    if (this.recordTypeCase) {
      fields.RecordTypeId = this.recordTypeCase;
    }

    fields.Origin = this.defaultCaseOrigin;
    if(this.origin){
      fields.Origin = this.origin;
    }
    if(this.channel){
      fields.Channel__c= this.channel;
    }
    if(this.subProcess){
      fields.SubProcess__c= this.subProcess;
    }
    /*if(this.isChangeCAEN){
      fields.AccountNACEReference__c=this.naceId;
    }*/
    if(this.isChangeContactData && !this.isPersonAccount){
        if(this.contact){
            fields.ContactId=this.contact.Id;
        }

    }
    if(this.accountModifiedPhone){
        fields.AccountPhone__c = this.accountModifiedPhone;
    }
    if(this.accountModifiedEmail){
        fields.AccountEmail__c = this.accountModifiedEmail;
    }
    if(this.accountModifiedMobile){
        fields.SuppliedPhone = this.accountModifiedMobile
    }

    if (this.validateFields()) {
      this.showLoadingSpinner = true;
      form.submit(fields);
    } else {
      error(this, this.labels.requiredFields);
      return;
    }
  }

  getFormInputFieldsString(formDataId){
   return  'lightning-record-edit-form[data-id="'+formDataId+'"] lightning-input-field';
  }

  getFormString(formDataId){
    return 'lightning-record-edit-form[data-id="'+formDataId+'"]';
  }

  saveAccount() {
    let fields = {};
    this.template.querySelectorAll(this.getFormInputFieldsString(this.accountFormId)).forEach(element => {
      fields[element.fieldName] = element.value;
    });
    if(this.isChangeContactData && !this.isPersonAccount && this.isPrimaryContact){
      fields.PrimaryContact__c = this.contactId;
    }
    this.template.querySelector(this.getFormString(this.accountFormId)).submit(fields);
  }

  saveContact() {
    let fields = {};
    this.template.querySelectorAll(this.getFormInputFieldsString(this.contactFormId)).forEach(element => {
      fields[element.fieldName] = element.value;
    });
    this.template.querySelector(this.getFormString(this.contactFormId)).submit(fields);
  }

  handleSuccess(event) {
    event.preventDefault();
    event.stopPropagation();
    let apiName=event.detail.apiName;
    console.log("sucess apiName==========="+apiName);
    if (apiName === "Case") {
      this.caseId=event.detail.id;
      /*if(this.isChangeContactData){
        if(this.isPersonAccount){
          this.saveAccount();
        }else {

          this.saveContact();
        }
        return;
      }else */if (this.isChangeOtherFields) {
        this.saveAccount();
        return;
      }
    } else if(apiName === "Contact") {
      if(this.isPrimaryContact && this.isPrimaryContact){
       // this.saveAccount();
        return;
      }
    }

    this.showLoadingSpinner = false;
    success(this, labels.caseSuccessfullyUpdated);

    const successEvent = new CustomEvent("casesuccess", {
         detail: {
         id: this.caseId,
         mobile: this.accountMobile,
         phone: this.accountPhone,
         email: this.accountEmail
          }
       });
    this.dispatchEvent(successEvent);
    // this.addClosingEvent();

  }

  handleError(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    console.error("form error=="+JSON.stringify(event.detail));
    error(this, event.detail.message);
  }
  handleCancel() {
    this.addClosingEvent();
  }

  removeError(event) {
    let inputCmp = event.target;
    this.removeErrorStyle(inputCmp);
    inputCmp.value=inputCmp.value.trim();
    let change= true;
    if(this.isChangeCNP){
      this.caseCnpField =  this.template.querySelector('[data-id="CNP"]');
      let nationalIdentityNumber =  this.caseCnpField.value;
      this.caseCnp= nationalIdentityNumber.trim();
      change = this.caseCnp !== this.accountNationalIdentityNumber;
    } else if(this.isChangeAccountName){
      let firstNameField =  this.template.querySelector('[data-id="firstName"]');
      let lastNameField =  this.template.querySelector('[data-id="lastName"]');
      change= firstNameField.value != this.accountFirstName || lastNameField.value != this.accountLastName;

    } else if(this.isChangeCompanyName){
      let accountNameField= this.template.querySelector('[data-id="accountName"]');
      change = accountNameField.value !== this.accountName;

    } else if(this.isChangeONRC){
      this.caseOnrcField= this.template.querySelector('[data-id="ONRC"]');
      this.caseOnrc=this.caseOnrcField.value.trim();
      change = this.caseOnrc !== this.accountOnrcCode;
    }else if(this.isChangeCAEN){
      let caenField= this.template.querySelector('[data-id="CAEN"]');
      this.caseNaceCode=caenField.value.trim();
      change = this.caseNaceCode !== this.accountNaceCodeRef;

    }else if(this.isChangeCUI){
      this.caseCuiField= this.template.querySelector('[data-id="CUI"]');
      this.caseCui=this.caseCuiField.value.trim();
      let oldVat=this.accountVATNumber;
      if(oldVat != null && oldVat != ''){
        oldVat=oldVat.toLowerCase();
      }else if (this.isChangeOtherFields){
        this.newAnnualRevenue=this.template.querySelector('[data-id="newAnnualRevenue"]');
        this.newNumbOfEmployees=this.template.querySelector('[data-id="newNumbOfEmployees"]');
        this.newWebsite=this.template.querySelector('[data-id="newWebsite"]');
        change = this.newWebsite !== this.accountWebsite || this.newNumbOfEmployees !== this.accountNumberOfEmployees ||
            this.newAnnualRevenue !== this.accountAnnualRevenue;
      }
      change = this.caseCui.toLowerCase() !== oldVat.toLowerCase();

    } else if(this.isChangeContactData){
      change= this.validateChangeContact();
    }
    this.fireChangeEvent(change);
  }

  removeErrorStyle(inputCmp){
    if (inputCmp.fieldName) {
      inputCmp.classList.remove(this.ERROR_CLASS);
    }
  }

  fireChangeEvent(change){
    const editEvent =  new CustomEvent('edit', { detail: { change: change } });
    this.dispatchEvent(editEvent);
  }

  addClosingEvent() {
    const closeEvent = new CustomEvent("close");
    this.dispatchEvent(closeEvent);
  }
@api
  validateFields() {
    let valid = true;
    let requireFields = Array.from(this.template.querySelectorAll("lightning-input-field."+this.REQUIRED_CLASS));
    //let requireFieldsCombobox = Array.from(this.template.querySelectorAll("lightning-combobox.wr-required"));
    //requireFields = requireFields.concat(requireFieldsCombobox);
    valid =this.validate();
    if(valid){
      if(requireFields && requireFields.length) {
          requireFields.forEach((inputRequiredCmp) => {
              let valueInput = inputRequiredCmp.value;
              if (!valueInput || (isNaN(valueInput) && valueInput.trim() === "")) {
                  inputRequiredCmp.classList.add(this.ERROR_CLASS);
                  error(this, this.labels.requiredFields);
                  valid = false;
              }
          });
      }

      let phoneField =  this.template.querySelector(this.getDataIdString(this.dataIdContactPhone));
      if(phoneField) {
          let phone = phoneField.value;
          if (phone.length !== 10) {
              error(this, this.labels.correctPhoneNumber);
              valid = false;
          }
      }

      let mobileField =  this.template.querySelector(this.getDataIdString(this.dataIdContactMobile));
      if(mobileField) {
          let mobile = mobileField.value;
          if (mobile.length !== 10) {
              error(this, this.labels.correctMobileNumber);
              valid = false;
          }
      }

    }

    return valid;
  }

  validate(){
    if(this.isChangeCNP){
      if(this.accountForeignCitizen !== true) {
        let check = checkCNP(this.caseCnp);

        if (!check.outCome) {
          error(this, this.labels.invalidCnp.replace('{0}', check.errorCode).replace('{1}', this.caseCnp));
          this.caseCnpField.classList.add(this.ERROR_CLASS);
          this.showLoadingSpinner = false;
          return false;
        }
      }
    } else if(this.isChangeCUI) {
      //if(this.accountForeignCompany !== true) {
        let prefix= this.vatPrefix.toLowerCase();
        let validatedAccountVatNumber= this.accountVATNumber;
        let valid = true;
        if(validatedAccountVatNumber != null && validatedAccountVatNumber !== '') {
          validatedAccountVatNumber=validatedAccountVatNumber.toLowerCase();
          if (validatedAccountVatNumber.startsWith(prefix)) {
            valid = this.caseCui.toLowerCase().trim() == validatedAccountVatNumber.substr(prefix.length);
          } else {
            valid = this.caseCui.toLowerCase().trim() == (prefix + validatedAccountVatNumber);

          }
        }else {
          let check = checkCUI(this.caseCui);

          if (!check.outCome) {
            error(this, this.labels.invalidCUI.replace('{0}', check.errorCode).replace('{1}', this.caseCui));
            this.caseCnpField.classList.add(this.ERROR_CLASS);
            this.showLoadingSpinner = false;
            return false;
          }
        }
        if(!valid){
          error(this,this.labels.editCUI.replace('{0}', this.vatPrefix));
          this.caseCuiField.classList.add(this.ERROR_CLASS);
          this.showLoadingSpinner = false;
          return false;
        }
     // }
    } else if(this.isChangeONRC){

      let check = checkONRC(this.caseOnrc);
      if (!check.outCome) {
        error(this, this.labels.invalidONRC.replace('{0}', this.caseOnrc));
        this.caseOnrcField.classList.add(this.ERROR_CLASS);
        this.showLoadingSpinner = false;
        return false;
      }

    } else if(this.isChangeContactData){
      return this.validateChangeContact();
    }
    return true;
  }

  getDataIdString(dataId){
    return '[data-id='+'"'+dataId+'"'+']';
  }

    validateChangeContact(){

        let phoneField =  this.template.querySelector(this.getDataIdString(this.dataIdContactPhone));
        let phone=phoneField.value;

        let mobileField =  this.template.querySelector(this.getDataIdString(this.dataIdContactMobile));
        let mobile=mobileField.value;
        let mailField =  this.template.querySelector(this.getDataIdString(this.dataIdContactMail));
        let mail=mailField.value;
        console.log('721 - ' + phoneField.value + ' ' + mobileField.value + ' ' + mailField.value);
        this.accountModifiedMobile = mobileField.value;
        this.accountModifiedPhone = phoneField.value;
        this.accountModifiedEmail = mailField.value;
        if (this.isBusinessAccountToEdit){
            this.accountPhone = phoneField.value;
            this.accountEmail = mailField.value;
        }
        if(phone != null) {
            phone=phone.trim();
        } else {
            phone='';
        }
        if(mail != null){
            mail= mail.trim();
        }else {
            mail='';
        }
        if(mobile !=null){
            mobile=mobile.trim();
        }else {
            mobile='';
        }
        if(phone == '' && mail =='' && mobile == '' ){
            return false;
        }
        if(mail != '' ) {
            let check = checkMail(mail);
            let valid= check.outCome;
            if(!valid){
                mailField.classList.add(this.ERROR_CLASS);
                return false;
            }
        }
        let change=false;
        if(!this.isPersonAccount ){
            //console.log('***** is checking this.isPersonAccount enter*** '+this.isPersonAccount);

            if(this.isPrimaryContact){
                change=(this.contactId !== this.primaryContact);
            }
        }

    if(!change){
      if(this.isPersonAccount){
        change=mobile !== this.accountMobile || phone !== this.accountPhone || mail !== this.accountEmail;
      }else {
          if(this.contact){
              change=mobile !== this.contact.MobilePhone || phone !== this.contact.Phone || mail !== this.contact.Email;
          } else{
              if(mobile || phone || mail){
                  change= true;
              }
          }

      }

    }
    return change;
  }
  disabledSaveButton(event) {
    this.isNormalize = event.detail.isnormalize;
    const isNormalizeEvent = new CustomEvent("isnormalize", { detail: { isNormalize: event.detail.isnormalize } });
    this.dispatchEvent(isNormalizeEvent);
  }

  disabledSaveButtonCaseAddress(event) {
    this.isNormalize = event.detail.isnormalize;
    this.addressForced = event.detail.addressForced;
    this.fireChangeEvent(this.isNormalize)

  }

  @api validateKey(){
    if(this.isChangeCNP){
      return this.caseCnp;
    } else if (this.isChangeCUI){
      return this.caseCui;
    }else{
      return '';
    }
  }

  @api getNaceCode (){
    return this.caseNaceCode;
  }
  updateContact(){
    //this.contact=contact;
    this.contactMobile = this.contact.MobilePhone;
    this.contactPhone = this.contact.Phone;
    this.contactEmail = this.contact.Email;
    this.isPrimaryContact= this.primaryContact == this.contactId;
  }

  @api getOldPhoneValue(){
    return this.oldContactPhone;
  }

  @api getOldMobileValue(){
    return this.oldContactMobile;
  }

  @api getOldMailValue(){
    return this.oldContactMail;
  }

  /*handleContactLoad(){
    let contactPhoneField = this.template.querySelector(this.getDataIdString(this.dataIdContactPhone));
    let contactMobileField = this.template.querySelector(this.getDataIdString(this.dataIdContactMobile));
    let contactMailField = this.template.querySelector(this.getDataIdString(this.dataIdContactMail));
    this.oldContactPhone =  contactPhoneField.value;
    this.oldContactMobile =  contactMobileField.value;
    this.oldContactMail =  contactMailField.value;
  }*/

}