import { LightningElement, api, track } from 'lwc';
import { decorateTemplate } from 'c/templateEngineDom';

export default class ScriptElement extends LightningElement {
    /*
    * ScriptElement__c representing the template element to be rendered
     */
    @api element;
    /*
    * JSON element containing the values to be merged with the template
     */
    @api inputData;
    /*
    * Used to specify the subsection of the JSON in which the desired values are located
     */
    @track jsonSubsection = "";
    /*
    * Result of the merge between the HTMLContent template and the values in the JSON
     */
    @track richText = "";

    @api
    setInputData(value) {
        this.inputData = value;
        this.richText = decorateTemplate(this.element.HTMLContent__c, JSON.stringify(this.inputData), this.jsonSubsection);
        this.template.querySelector('div').innerHTML = this.richText;
    }

    connectedCallback() {
        let param = JSON.stringify(this.inputData);
        if (this.element.Variable__c) {
            this.jsonSubsection = this.element.Variable__c;
        }

        this.richText = decorateTemplate(this.element.HTMLContent__c, param, this.jsonSubsection);
    }

    renderedCallback() {
        this.template.querySelector('div').innerHTML = this.richText;
        //this.template.querySelector('div').appendChild(document.createRange().createContextualFragment(this.richText));
        //this.template.querySelector('div').appendChild(this.richText);
    }

    @api
    getRichText(){
        return this.richText;
    }
}