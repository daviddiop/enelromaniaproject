/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 10.07.20.
 */

import {LightningElement, api, track} from 'lwc';
import {labels} from 'c/labels';

export default class MroSupplyTile extends LightningElement {
    labels = labels;

    @api disabled = false;
    @api supplyFields = [];
    @api supplyId;

    @track openConfirmModal = false;

    deleteSupply(){
        this.openConfirmModal = false;
        const deleteEvent = new CustomEvent('delete', {
            detail: {
                deleteRecord: this.supplyId
            }
        });
        this.dispatchEvent(deleteEvent);
    }

    showDeleteConfirmation(){
        this.openConfirmModal = true;
    }

    closeModal(){
        this.openConfirmModal = false;
    }

    get cardClass() {
        return 'slds-class' + (this.disabled ? ' slds-theme_shade' : '');
    }
}