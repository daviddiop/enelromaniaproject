/**
 * Created by vincenzo.scolavino on 19/03/2020.
 */

import {api, LightningElement, track} from 'lwc';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {checkServicePointEleCode, checkServicePointGasCode} from "c/mroValidations";

export default class MroNlcclcCaseEdit extends LightningElement {
    @api label = labels;
    @api title;
    @api show;
    @api showNotes;
    @api accountId;
    @api dossierId;
    @api supplyId;
    @api companyDivisionId;
    @api serviceSiteId;
    @api editMode;
    @api incomingNlcClcCode;
    @api incomingCustomerNotes;
    @api caseId;
    @api showModal;
    @api incomingServicePointCode;

    @track defaultTitle;
    @track typeLabel;
    @track spinner;
    @track nlcClcCode;
    @track newNlcClcCode;
    @track servicePointCode;
    @track saveButtonDisabled;
    @track customerNotes;
    @track newServicePointCode;
    @track isMainDistributor = false;
    @track isDelgazDistributor = false;
    @track isDistrEnerOltDistributor = false;

    connectedCallback() {
        this.saveButtonDisabled = true;
        this.spinner = true;
        this.getNlcClcCodeFromSupply();
        this.checkIfMainDistributor();
        if(this.editMode) {
            this.newNlcClcCode = this.incomingNlcClcCode;
            this.newServicePointCode = this.incomingServicePointCode;
            this.customerNotes = this.incomingCustomerNotes;
        }
    }

    getNlcClcCodeFromSupply(){
        let inputs = {recordId : this.supplyId};
        notCacheableCall({
            className: 'MRO_LC_NLCCLCEdit',
            methodName: 'getSupplyById',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            let resultData = response.data.supply;
            this.show = true;
            this.spinner = false;
            this.defaultTitle = this.title;
            let isGas = resultData.RecordType.DeveloperName === 'Gas';
            this.typeLabel       = isGas ? 'CLC' : 'NLC';
            this.nlcClcCode      = isGas ? resultData.ServiceSite__r.CLC__c : resultData.ServiceSite__r.NLC__c;
            this.servicePointCode = resultData.ServicePoint__r.Code__c;
            this.serviceSiteId = resultData.ServiceSite__c;
        })
            .catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.addressLoaded = true;
                this.spinner = false;
            });
    }


    handleNlcClcChange(evt){
        let typedValue = evt.target.value;
        this.newNlcClcCode = typedValue;
        this.saveButtonDisabled = (!this.newServicePointCode || this.newServicePointCode.length === 0) ||
                                  (!this.newNlcClcCode || this.newNlcClcCode.length === 0) ;
    }

    handlePODChange(evt){
        let typedValue = evt.target.value;
        this.newServicePointCode = typedValue;
        this.saveButtonDisabled = (!this.newServicePointCode || this.newServicePointCode.length === 0) ||
                                  (!this.newNlcClcCode || this.newNlcClcCode.length === 0) ;
    }

    handleNotesChange(evt){
        let typedValue = evt.target.value;
        this.customerNotes = typedValue;
        //this.saveButtonDisabled = this.newNlcClcCode.length === 0 || this.customerNotes.length === 0;
    }

    save(){
        this.spinner = true;
        if(this.checkInputValidity(this.newServicePointCode)){
            if(!this.editMode) {
                this.createCase();
            } else
                this.updateCase();
        } else {
            this.spinner = false;
        }
    }

    createCase(){
        let inputs = {
            accountId         : this.accountId,
            dossierId         : this.dossierId,
            supplyId          : this.supplyId,
            serviceSiteId     : this.serviceSiteId,
            companyDivisionId : this.companyDivisionId,
            nlcClcCode        : this.newNlcClcCode,
            oldServicePointCode  : this.servicePointCode,
            customerNotes     : this.customerNotes,
            typeLabel         : this.typeLabel,
            oldNlcClc         : this.nlcClcCode,
            servicePointCode  : this.newServicePointCode};

        notCacheableCall({
            className: 'MRO_LC_NLCCLCEdit',
            methodName: 'createNewCase',
            input: inputs
        }).then((response) => {

            if (response.data.error) {
                error(this, JSON.stringify(response.data.errorMsg));
                this.spinner = false;
            }
            else {
                //let caseCreated = response.data.case;
                this.dispatchEvent(new CustomEvent('casecreated'));
                this.spinner = false;
                this.show = false;
                this.resetBox();
                return;
            }
        })
            .catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));

                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.addressLoaded = true;
                this.spinner = false;
            });
    }

    updateCase() {
        let inputs = { recordId          : this.caseId,
                       nlcClcCode        : this.newNlcClcCode,
                       servicePointCode  : this.newServicePointCode,
                       customerNotes     : this.customerNotes,
                       typeLabel         : this.typeLabel,
                       oldNlcClc         : this.nlcClcCode,
                       oldServicePointCode: this.servicePointCode};

        notCacheableCall({
            className: 'MRO_LC_NLCCLCEdit',
            methodName: 'UpdateCaseNlcClc',
            input: inputs
        }).then((response) => {

            if (response.data.error) {
                error(this, JSON.stringify(response.data.errorMsg));
                this.spinner = false;
            }
            else {
                //let caseCreated = response.data.case;
                this.dispatchEvent(new CustomEvent('nlcclcedited', {
                    detail: {
                        'updatedNlcClc': this.newNlcClcCode,
                        'updateServiceCointCode' : this.newServicePointCode
                    }
                }));
                this.spinner = false;
                this.show = false;
                this.resetBox();
                return;
            }
        })
            .catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));

                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.addressLoaded = true;
                this.spinner = false;
            });
    }

    resetBox() {
        this.typeLabel = '';
        this.nlcClcCode = '';
        this.accountId = '';
        this.dossierId = '';
        this.supplyId = '';
    }

    closeModal(){
        this.dispatchEvent(new CustomEvent('casecreated'));
        this.spinner = false;
        this.show = false;
        this.showModal = false;
        this.resetBox();
    }

    checkIfMainDistributor() {
        let input = {supplyId : this.supplyId};
        let mainDistributorVATs = ['RO10976687','14491102','14506181','14476722','14493260'];
        notCacheableCall({
            className: 'MRO_LC_NLCCLCEdit',
            methodName: 'GetServicePointDistributor',
            input: input
        }).then((response) => {
            if (response.data.error) {
                error(this, JSON.stringify(response.data.errorMsg));
                this.spinner = false;
            }
            else {
                let distributorVATNumber = response.data.distributor.VATNumber__c;
                if (mainDistributorVATs.includes(distributorVATNumber)) {
                    if (distributorVATNumber === 'RO10976687') {
                        this.isDelgazDistributor = true;
                    } else if (distributorVATNumber === '14491102') {
                        this.isDistrEnerOltDistributor = true;
                    }
                    this.isMainDistributor = true;
                } else {
                    this.isMainDistributor = false;
                }
            }
        }).catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
            });
    }

    checkInputValidity(typedValue) {
        let isMainDistributorPODValid = this.checkMainDistributorValidity(typedValue);
        let regularChecksValidity = this.regularChecks(typedValue);

        return this.finalInputChecks(regularChecksValidity, isMainDistributorPODValid);
    }

    finalInputChecks(regularChecksValidity, isMainDistributorPODValid) {
        let finalInputChecksValidity = false

        if (regularChecksValidity) {
            if (this.isMainDistributor) {
                if (isMainDistributorPODValid) {
                    finalInputChecksValidity = true;
                }
            } else {
                finalInputChecksValidity = true;
            }
        }

        return finalInputChecksValidity;
    }

    regularChecks(typedValue) {
        let regularChecks = false;

        if (this.typeLabel === 'CLC') {
            if (typedValue === this.newNlcClcCode) {
                if (!checkServicePointGasCode(typedValue)) {
                    error(this, this.label.invalidCode + ' for POD');
                } else if (!this.checkNewCLCValidity()) {
                    error(this, this.label.invalidCode + ' for CLC');
                } else {
                    regularChecks = true;
                }
            } else {
                error(this, 'CLC/NLC code and POD must be the same');
            }
        } else {
            if (!checkServicePointEleCode(typedValue)) {
                error(this, this.label.invalidCode + ' for POD');
            } else {
                regularChecks = true;
            }
        }

        return regularChecks;
    }

    checkMainDistributorValidity(typedValue) {
        let isMainDistributorValid = false;

        if (this.isMainDistributor) {
            if (this.isDelgazDistributor) {
                if (this.checkDelgazPODValidity(typedValue)) {
                    isMainDistributorValid = true;
                }
            } else if (this.checkOtherMainDistributorsPODValidity(typedValue)) {
                isMainDistributorValid = true;
            }
        }
        return isMainDistributorValid;
    }

    checkNewCLCValidity() {
        let newClcFirstThree = this.newNlcClcCode.slice(0, 3);
        let oldClcFirstThree = this.nlcClcCode.slice(0, 3);

        return newClcFirstThree === oldClcFirstThree;
    }

    checkOtherMainDistributorsPODValidity(newPODNumber) {

        if (this.validatePODFirstThreeDigits(newPODNumber) &&
            this.validatePOD45Digits(newPODNumber) &&
            this.validatePOD67Digits(newPODNumber) &&
            this.validateLastThreeDigits(newPODNumber) &&
            this.validateNumberOfChars(newPODNumber)
        ) {
            return true
        } else {
            error(this, this.label.invalidCode + ' for POD');
            return false
        }
    }

    checkDelgazPODValidity(newPODNumber) {
        if (this.validatePODFirstThreeDigits(newPODNumber) &&
            this.validateLastThreeDigits(newPODNumber) &&
            this.validateNumberOfChars(newPODNumber)
        ) {
            return true
        } else {
            error(this, this.label.invalidCode + ' for POD');
            return false
        }
    }

    validatePODFirstThreeDigits(newPODNumber) {
        let newPODFirstThree = newPODNumber.slice(0, 3);
        let oldPODFirstThree = this.servicePointCode.slice(0, 3);

        return newPODFirstThree === oldPODFirstThree;
    }

    validatePOD45Digits(newPODNumber) {
        let newPOD45Digits = newPODNumber.slice(3, 5);

        return ['01', '02', '03', '04'].includes(newPOD45Digits);
    }

    validatePOD67Digits(newPODNumber) {
        let are67DigitsValid = false;
        let newPOD67Digits = newPODNumber.slice(5, 7);
        let countyCodeDistrEnerOltenia = ['01', '02', '03', '04', '05', '06', '07'];
        let countyCodeOtherDistributors = ['01', '02', '03', '04', '05', '06'];

        if (this.isDistrEnerOltDistributor) {
            are67DigitsValid = countyCodeDistrEnerOltenia.includes(newPOD67Digits);
        } else {
            are67DigitsValid = countyCodeOtherDistributors.includes(newPOD67Digits);
        }
        console.log(are67DigitsValid);
        return are67DigitsValid;
    }

    validateLastThreeDigits(newPODNumber) {
        let newPODLastThree = parseInt(newPODNumber.slice(-3));

        return newPODLastThree > 0;
    }

    validateNumberOfChars(newPODNumber) {
        let newPODCharNumber = newPODNumber.length;
        let isCharNumberValid = false;

        if (this.isDelgazDistributor) {
            if (newPODCharNumber === 10) {
                isCharNumberValid = true;
            }
        } else if (this.isDistrEnerOltDistributor) {
            if (newPODCharNumber === 26) {
                isCharNumberValid = true;
            }
        } else if (newPODCharNumber === 18) {
            isCharNumberValid = true;
        }

        return isCharNumberValid;
    }

}