import { LightningElement, api, track } from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import { NavigationMixin } from 'lightning/navigation';
import getAvailableAddressesCountryRestricted from '@salesforce/apex/MRO_SRV_Address.getAvailableAddressesCountryRestricted';
import { error } from 'c/notificationSvc';
import { labels } from 'c/mroLabels';
import {checkPhone} from "c/mroValidations";

const methodPostalOrder = 'Postal Order';
const methodBankTransfer = 'Bank Transfer';

export default class BillingProfileEdit extends NavigationMixin(LightningElement) {
    billingProfile;
    labels = labels;
    @api billingProfileRecordId;
    @api accountId = '';
    @api forceGiroIfBusiness=false;
    @api buttonType = '';
    @api showHeader = false;
    @api showModal = false;
    @api readOnly = false;
    @api addressForcedBilling = false;
   /* Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts*/
    @api contractType;
    /*Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts*/
    @api isCustomerRefundRequest = false;
    @api isProsumers = false;
    @api isRefund = false;
    @api refundFilter;
    @api refundOnClone = false;
    @api addressOnClone = false;
    @api hasDirectDebitInBillingProfile = false;

    @track disabled = true;
    @track bank='';
    //FF Customer Creation - Pack1/2 - Interface Check
    @track refoundBank='';
    //FF Customer Creation - Pack1/2 - Interface Check
    @track selectedRecordType = '';
    @track address;
    @track show = true;
    // [ENLCRO-658] Billing Profile - Interface Check - Pack2
    //@track isFax = '';
    @track isMobile = '';
    // [ENLCRO-658] Billing Profile - Interface Check - Pack2
    @track isIban = '';
    //FF Customer Creation - Pack1/2 - Interface Check
    @track isRefundIban = '';
    //FF Customer Creation - Pack1/2 - Interface Check
    @track isEmail = '';
    @track isNormalize;
    @track isRequiredAddressField = true;
    @track showLoadingSpinner = true;
    @track disableNormalize = false;
    @track mapBankNameById;
    @track availableAddresses;

    @track refundMethod;
    @track deliveryChannelBusinessList;
    @track deliveryChannelBusinessVal;
    @track isInvoiceRefundMethod = false;
    @track paymentMethodValue = '';

    connectedCallback() {
        this.showLoadingSpinner = false;
        this.isNormalize = true;

        if (this.isNewView) {
            this.billingProfileRecordId = '';
            this.address = {"country":"ROMANIA"};
            this.show = true;
        } else {
            this.fillAddressFields();
        }

        if (this.accountId) {
            getAvailableAddressesCountryRestricted({
                accountId: this.accountId,
                restrictToCountry: "ROMANIA"
            }).then((response) => {
                this.availableAddresses = response;
            }).catch((errorMsg) => {
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.spinner = false;
            });
        }
        this.mapBankNameWithId();

    }

    makeRefundRequestFieldsOptional() {
        let paymentMethod = this.template.querySelector('[data-id="paymentMethodVal"]');
        let deliveryChannel = this.template.querySelector('[data-id="myDeliveryChannel"]');

        if (paymentMethod.classList.contains('wr-required')) {
            paymentMethod.classList.remove('wr-required');
        }

        if (deliveryChannel.classList.contains('wr-required')) {
            deliveryChannel.classList.remove('wr-required');
        }
    }

    hideRefundFieldsForProsumers() {
        let refundIban = this.template.querySelector('[data-id="refundIban"]');
        let refundBank = this.template.querySelector('[data-id="refundBank"]');

        refundIban.classList.add('slds-hide');
        refundBank.classList.add('slds-hide');
    }

    showRefundFieldsForProsumers() {
        let refundIban = this.template.querySelector('[data-id="refundIban"]');
        let refundBank = this.template.querySelector('[data-id="refundBank"]');

        refundIban.classList.remove('slds-hide');
        refundBank.classList.remove('slds-hide');
    }

    handleLoadBP() {
        if(this.hasDirectDebit){
            this.setPaymentMethodOption();
        }
        this.recordTypeEditId();

        if ((this.isCustomerRefundRequest || this.isProsumers) && (this.buttonType === 'edit' || this.buttonType === 'new')) {
            this.makeRefundRequestFieldsOptional();
        }

        //Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
        if ((this.buttonType === 'new') && (this.contractType) && (this.contractType === 'Business')) {
            let paymentMethod = this.template.querySelector('[data-id="paymentMethodVal"]');
            let refundMethod = this.template.querySelector('[data-id="refundMethodVal"]');
            if(!paymentMethod.value){
                paymentMethod.value = 'Bank Transfer';
            }
            if(!refundMethod.value){
                refundMethod.value = 'Invoice';
            }
            this.isInvoiceRefundMethod = true;
            this.getListDeliveryChannelBusiness();
        }

        if ((this.buttonType === 'edit' || this.buttonType === 'clone') && (this.contractType) && (this.contractType === 'Business')) {
            let refundMethod = this.template.querySelector('[data-id="refundMethodVal"]');

            if (refundMethod.value === 'Invoice') {
                let myDeliveryChannel = this.template.querySelector('[data-id="myDeliveryChannel"]');
                if(myDeliveryChannel && myDeliveryChannel.value){
                    this.deliveryChannelBusinessVal = myDeliveryChannel.value;
                    this.isInvoiceRefundMethod = true;
                    this.getListDeliveryChannelBusiness();
                }

            }
        }
        if(this.hasDirectDebit){
            this.template.querySelector('[data-id="paymentMethodVal"]').value = this.paymentMethodValue;
        }
        //Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
    }

    renderedCallback() {
        if (!this.billingProfileRecordId) {
            this.show = true;
            this.showLoadingSpinner = false;
        }
        //Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
        if (!this.contractType){
            this.isBusinessClient();
        }
        //Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
    }

    isBusinessClient() {
        let inputs = {accountId: this.accountId};
        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'isBusinessClient',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                else if (response.data.isBusinessClient) {
                    let paymentMethod = this.template.querySelector('[data-id="paymentMethodVal"]');
                    let refundMethod = this.template.querySelector('[data-id="refundMethodVal"]');
                    if (this.forceGiroIfBusiness) {
                        if(!paymentMethod.value){
                            paymentMethod.value = 'Bank Transfer';
                        }
                        if(!refundMethod.value){
                            refundMethod.value = 'Invoice';
                        }
                        this.isIban = 'wr-required';
                       // Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
                    }else if ((this.buttonType === 'new') && (!this.contractType)) {
                        if(!paymentMethod.value){
                            paymentMethod.value = 'Bank Transfer';
                        }
                        if(!refundMethod.value){
                            refundMethod.value = 'Invoice';
                        }
                    }
                    //Refund Method default with "Invoice"; Payment Method = "Bank Transfer" for Business Accounts
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    getListDeliveryChannelBusiness(){
        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'getListDeliveryChannelBusiness',
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                console.log('FF response.data.deliveryChannelBusinesses: ' + response.data.deliveryChannelBusinesses);
                this.deliveryChannelBusinessList = response.data.deliveryChannelBusinesses;
                if ((this.buttonType === 'new') && (this.contractType) && (this.contractType === 'Business')) {
                    this.deliveryChannelBusinessVal = this.deliveryChannelBusinessList[0].value;
                }
                this.isInvoiceRefundMethod = true;

            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            })
    }

    mapBankNameWithId() {
        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'getMapBankNameWithId'
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let mapBankNameWithId = {};
                mapBankNameWithId = response.data.mapBankId;
                this.mapBankNameById = mapBankNameWithId;
                console.log('mapBankNameById: ' + JSON.stringify(this.mapBankNameById));
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    fillAddressFields() {
        let inputs = {billingProfileRecordId: this.billingProfileRecordId};
        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'getAddressFields',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let addr = {};
                addr = response.data.billingAddress;
                this.address = addr;
                this.show = true;
                if(addr.addressNormalized){
                    this.readOnly = true;
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }
// [ENLCRO-658] Billing Profile - Interface Check - Pack2
    recordTypeEditId() {
        if (this.buttonType !== 'new') {
            let deliveryChannelCmp = this.template.querySelector('[data-id="myDeliveryChannel"]');
            let paymentMethod = this.template.querySelector('[data-id="paymentMethodVal"]');
            let refundMethod = this.template.querySelector('[data-id="refundMethodVal"]');

            if(this.isRefund && refundMethod){
                refundMethod.value = this.template.querySelector('[data-id="refundMethodValHidden"]').value;
            }

            if (deliveryChannelCmp) {
              /*  if (deliveryChannelCmp.value === 'Fax') {
                    this.isRequiredAddressField = true;
                    this.disableNormalize = true;
                    this.isNormalize = true;
                    this.isFax = 'wr-required';
                    this.isMobile = '';
                    this.isEmail = '';
                } else */

                if (deliveryChannelCmp.value === 'Email') {
                    this.isRequiredAddressField = true;
                    this.disableNormalize = true;
                    this.isNormalize = true;
                    this.isEmail = 'wr-required';
                    this.isMobile = '';
                   // this.isFax = '';

                } else if (deliveryChannelCmp.value === 'SMS') {
                    this.isRequiredAddressField = true;
                    this.disableNormalize = true;
                    this.isNormalize = true;
                    this.isMobile = 'wr-required';
                    this.isEmail = '';
                    //this.isFax = '';

                } else {
                    this.disableNormalize = false;
                    this.isRequiredAddressField = false;
                    this.isNormalize = false;
                    this.isEmail = '';
                    //this.isFax = '';
                    this.isMobile = '';
                }
            }

            if (paymentMethod){
                if (paymentMethod.value === 'Direct Debit') {
                    this.isIban = 'wr-required';
                } else {
                    this.isIban = '';
                }
            }
            //FF Customer Creation - Pack1/2 - Interface Check
            if (refundMethod){
                this.refundMethod = refundMethod.value;
                if (refundMethod.value === methodBankTransfer) {
                    this.isRefundIban = 'wr-required';
                } else {
                    this.isRefundIban = '';
                }
            }
            //FF Customer Creation - Pack1/2 - Interface Check
        }
    }
    // [ENLCRO-658] Billing Profile - Interface Check - Pack2
    get isNewView() {
        return this.buttonType === 'new';

    }
    handleCancel() {
        this.dispatchEvent(new CustomEvent('close'));

    }
    handleErrorBP(event) {
        //error(this, event.detail.message);
        console.log('eventdeta: ' + JSON.stringify(event.detail));
        this.showLoadingSpinner = false;
    }
    handleSuccessBP(event) {
        event.preventDefault();
        event.stopPropagation();
        let payload = event.detail;
        const selectedEvent = new CustomEvent('save', {
            detail: {
                billingProfileEditId: payload.id
            }
        });
        this.dispatchEvent(selectedEvent);
        this.showLoadingSpinner = false;
    }

    handleSubmitBP(event) {
        event.preventDefault();
        event.stopPropagation();
        this.showLoadingSpinner = true;

        const fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });

        if(this.refundFilter && this.refundMethod && this.refundFilter !== this.refundMethod){
            error(this, 'Only ' + this.refundFilter + ' refund method allowed');
            this.showLoadingSpinner = false;
            return;
        }

        if(this.showAddressBlock) {
            let addrForm = this.template.querySelector('c-mro-address-form');

            if (!addrForm.isValid()) {
                error(this, this.labels.addressMustBeVerified);
                this.showLoadingSpinner = false;
                return;
            }

            let addrFields = addrForm.getValues();
            for (let f in addrFields) {
                if (addrFields.hasOwnProperty(f)) {
                    fields[f] = addrFields[f];
                }
            }
        }
        //FF Customer Creation - Pack1/2 - Interface Check
        if (this.validateFields() && this.checkIbanValidation() && this.checkRefundIbanValidation()) {
            //FF Customer Creation - Pack1/2 - Interface Check

            if (this.buttonType === 'clone') {
                this.billingProfileRecordId = '';
                fields.Account__c = this.accountId;
                this.template.querySelector('lightning-record-edit-form').recordId = this.billingProfileRecordId;
            }

            if(this.isRefund){
                fields.RefundMethod__c = this.refundMethod;
            }

            fields.BillingAddressNormalized__c = !this.addressForcedBilling;
            this.template.querySelector('lightning-record-edit-form').submit(fields);


        }else {
            this.showLoadingSpinner = false;
            if(this.template.querySelector('[data-id="paymentMethodVal"].slds-has-error')){
                this.template.querySelector('[data-id="paymentMethodValComboBox"]').classList.add('slds-has-error');
            }
        }
    }
    //FF Customer Creation - Pack1/2 - Interface Check
    setRefundBank(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName === 'RefundIBAN__c') {
            let refundBankCmp = this.template.querySelector('[data-id="refundBank"]');
            if(inputCmp.value.length > 7){

                console.log('inputCmp.value.length: ' + inputCmp.value.length);
                console.log('inputCmp.value.substring(4,8): ' + inputCmp.value.substring(4,8));
                Object.keys(this.mapBankNameById).forEach((key) => { //loop through keys array
                    if (inputCmp.value.substring(4,8) == key) {
                        refundBankCmp.value = this.mapBankNameById[key];
                    }
                });
            }else{
                refundBankCmp.value = '';
            }
        }
        this.removeError(event);
    }

    setBank(event) {
        let inputIbanCmp = event.target;
        if (inputIbanCmp.fieldName === 'IBAN__c') {
            let bankCmp = this.template.querySelector('[data-id="bank"]');
            if(inputIbanCmp.value.length > 7){
                Object.keys(this.mapBankNameById).forEach((key) => { //loop through keys array
                    if (inputIbanCmp.value.substring(4,8) == key) {
                        bankCmp.value = this.mapBankNameById[key];
                    }
                });
            }else{
                bankCmp.value = '';
            }
        }
        this.removeError(event);
    }
    //FF Customer Creation - Pack1/2 - Interface Check
    removeError(event) {
        let inputCmp = event.target;

        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }

    }

    paymentMethodChange(event) {
        let inputCmp = event.target;
        if (this.hasDirectDebit || inputCmp.fieldName === 'PaymentMethod__c') {

            let paymentValue = inputCmp.value;
            if(this.hasDirectDebit){
                this.template.querySelector('[data-id="paymentMethodVal"]').value = paymentValue;
            }

            if (inputCmp.value === 'Direct Debit') {
                this.isIban = 'wr-required';
            } else {
                this.isIban = '';
            }
        }
        this.removeError(event);
    }
    //FF Customer Creation - Pack1/2 - Interface Check
    refundMethodChange(event) {
        let inputCmp = event.target;

        if (this.isRefund || inputCmp.fieldName === 'RefundMethod__c') {
            this.refundMethod = inputCmp.value;
            if (this.isProsumers) {
                if (this.refundMethod !== methodBankTransfer) {
                    this.hideRefundFieldsForProsumers();
                } else {
                    this.showRefundFieldsForProsumers();
                }
            }

            if(this.isRefund){
                this.template.querySelector('[data-id="refundMethodValHidden"]').value = this.refundMethod;
            }
            if (inputCmp.value === methodBankTransfer) {
                this.isInvoiceRefundMethod = false;
                this.isRefundIban = 'wr-required';
            } else {
                this.isRefundIban = '';
            }
            if ((this.contractType) && (this.contractType === 'Business')) {

                if ((this.buttonType === 'edit' || this.buttonType === 'clone')) {
                    let refundFunction = this.template.querySelector('[data-id="refundMethodVal"]');
                    if (refundFunction.value === 'Invoice') {
                        let myDeliveryChannel = this.template.querySelector('[data-id="myDeliveryChannel"]');
                        if(myDeliveryChannel && myDeliveryChannel.value){
                            this.deliveryChannelBusinessVal = myDeliveryChannel.value;
                            this.isInvoiceRefundMethod = true;
                            this.getListDeliveryChannelBusiness();
                        }

                    }
                }

                if (inputCmp.value === methodBankTransfer || inputCmp.value === methodPostalOrder) {
                    this.isInvoiceRefundMethod = false;
                } else {
                    this.isInvoiceRefundMethod = true;
                }
            }

        }
        this.removeError(event);
    }
    //FF Customer Creation - Pack1/2 - Interface Check
    // [ENLCRO-658] Billing Profile - Interface Check - Pack2
    deliveryChannelChange(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName === 'DeliveryChannel__c') {

           /* if (inputCmp.value === 'Fax') {
                this.isRequiredAddressField = true;
                this.disableNormalize = true;
                this.isNormalize = true;
                this.isFax = 'wr-required';
                this.isMobile = '';
                this.isEmail = '';

            } else */
            if (inputCmp.value === 'Email') {
                this.isRequiredAddressField = false;
                this.disableNormalize = false;
                this.isNormalize = false;
                this.isEmail = 'wr-required';
                this.isMobile = '';
                //this.isFax = '';

            } else if (inputCmp.value === 'SMS') {
                this.isRequiredAddressField = false;
                this.disableNormalize = false;
                this.isNormalize = false;
                this.isMobile = 'wr-required';
                this.isEmail = '';
                //this.isFax = '';

            } else {
                this.disableNormalize = false;
                this.isRequiredAddressField = false;
                this.isNormalize = false;
                this.isEmail = '';
                //this.isFax = '';
                this.isMobile = '';
            }
        }
        this.removeError(event);
    }

    deliveryChannelBusinessChange(event) {
        let inputValue = event.detail.value;
        let myDeliveryChannel = this.template.querySelector('[data-id="myDeliveryChannel"]');

            if (inputValue === 'Email') {
                this.isRequiredAddressField = false;
                this.disableNormalize = false;
                this.isNormalize = false;
                this.isEmail = 'wr-required';
                this.isMobile = '';
                //this.isFax = '';
            } else {
                this.disableNormalize = false;
                this.isRequiredAddressField = false;
                this.isNormalize = false;
                this.isEmail = '';
                //this.isFax = '';
                this.isMobile = '';
            }

        myDeliveryChannel.value = inputValue;
        this.removeError(event);
    }


    // [ENLCRO-658] Billing Profile - Interface Check - Pack2

    disabledSaveButton(event){
        this.isNormalize = event.detail.isnormalize;
    }
    disabledSaveButtonBillingAddress(event){
        this.isNormalize = event.detail.isnormalize;
        this.addressForcedBilling = event.detail.addressForced;
    }
    //FF Customer Creation - Pack1/2 - Interface Check
    mod97(digits) {
        let checksum = digits.slice(0, 2), fragment;
        for (let offset = 2; offset < digits.length; offset += 7) {
            fragment = String(checksum) + digits.substring(offset, offset + 7);
            checksum = parseInt(fragment, 10) % 97;
        }
        return checksum;
    }

    getIBANSize(input){
        let iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''); // keep only alphanumeric characters
        if(iban.length < 5) return 100;
        let code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/);
        if(code === null) return 100;
        let CODE_LENGTHS = {
            AL: 28,
            AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
            CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
            FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
            HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
            LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
            MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
            RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
        };

        let size = CODE_LENGTHS[code[1]] || 100;

        return size;
    }

    convertToInteger(aString) {
        let result = "";
        for (let i = 0; i < aString.length; i++) {
            let ch = aString[i];
            if (isNaN(ch) ){
                result += ch.charCodeAt(0) - 55;
            } else{
                result += ch.toString();
            }
        }
        return result;
    }

    getKeyByValue(map,val) {
        return Object.keys(map).find(key => map[key] === val);
    }

    checkIbanValidation= () => {
        if(!this.showPaymentBlock){
            return true;
        }
        let inputCmp = this.template.querySelector('[data-id="iban"]');

        let input = inputCmp.value;
        if(!input){input='';}
        let iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
            code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
            digits;
        if((inputCmp.classList.contains('wr-required'))  && ((!code) || (iban.length !== this.getIBANSize(input)))) {
            inputCmp.classList.add('slds-has-error');
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN008').replace('{1}', iban));
            //error(this, this.labels.incorrectIBANLength);
            return false;
        }

        if((!inputCmp.classList.contains('wr-required') && (input.length !== 0))  && ((!code) || (iban.length !== this.getIBANSize(input)))) {
            inputCmp.classList.add('slds-has-error');
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN008').replace('{1}', iban));
            return false;
        }
        // rearrange country code and check digits, and convert chars to ints
        let reArranged = String(iban).substring(4) + String(iban).substring(0, 4);
        let integerDigits = this.convertToInteger(reArranged);
        // final check
        if ((input.length !== 0)  && (this.mod97(integerDigits) !== 1)) {
            inputCmp.classList.add('slds-has-error');
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN002').replace('{1}', iban));
            return false
        }

        return (input.length !== 0)  ? this.mod97(integerDigits) === 1 : true;
    }

    checkRefundIbanValidation= () => {
        if(!this.showRefundIbanBlock){
            return true;
        }
        let inputCmp = this.template.querySelector('[data-id="refundIban"]');
        let input = inputCmp.value;
        if(!input){input='';}
        let iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
            code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
            digits;
        if((inputCmp.classList.contains('wr-required'))  && ((!code) || (iban.length !== this.getIBANSize(input)))) {
            inputCmp.classList.add('slds-has-error');
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN008').replace('{1}', iban));
            //error(this, this.labels.incorrectIBANLength);
            return false;
        }

        if((!inputCmp.classList.contains('wr-required') && (input.length !== 0))  && ((!code) || (iban.length !== this.getIBANSize(input)))) {
            inputCmp.classList.add('slds-has-error');
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN008').replace('{1}', iban));
            return false;
        }
        // rearrange country code and check digits, and convert chars to ints
        let reArranged = String(iban).substring(4) + String(iban).substring(0, 4);
        let integerDigits = this.convertToInteger(reArranged);
        // final check
        if ((input.length !== 0)  && (this.mod97(integerDigits) !== 1)) {
            inputCmp.classList.add('slds-has-error');
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN002').replace('{1}', iban));
            return false
        }

        return (input.length !== 0)  ? this.mod97(integerDigits) === 1 : true;
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        if(areValid){
            let inputMobileCmp = this.template.querySelector('[data-id="mobilePhone"]');
            if(inputMobileCmp){
                let mobilePhone = inputMobileCmp.value;
                if(mobilePhone != null && mobilePhone != ''){
                    let check=checkPhone(mobilePhone);
                    let valid= check.outCome;
                    if(!valid) {
                        areValid = false;
                        error(this, this.labels.invalidMobile.replace('{0}', mobilePhone));
                    }
                }
            }
        }
        return areValid;
    };
    //FF Customer Creation - Pack1/2 - Interface Check

    get refundMethodOptions(){
        return [
            {label: methodPostalOrder, value: methodPostalOrder},
            {label: methodBankTransfer, value: methodBankTransfer},
        ];
    }

    get addressOnCloneMode(){
        return this.buttonType === 'clone' && this.addressOnClone;
    }

    get refundOnCloneMode(){
        return this.buttonType === 'clone' && this.refundOnClone;
    }

    get showPaymentBlock(){
        return !this.refundOnCloneMode && (!this.isRefund || this.isNewView);
    }

    get showDeliveryBlock(){
        return !this.refundOnCloneMode && (!this.isRefund || this.isNewView);
    }

    get showAddressBlock(){
        return (!this.refundOnCloneMode || this.addressOnCloneMode) && (!this.isRefund || this.refundMethod === methodPostalOrder);
    }

    get showRefundIbanBlock(){
        return !this.isRefund || this.refundMethod === methodBankTransfer;
    }

    get showDeliveryChannel (){
        if(this.isInvoiceRefundMethod){
            return 'slds-col slds-size_1-of-2';
        }
        return 'slds-hide';
    }
    get hideDeliveryChannel (){
        if(this.isInvoiceRefundMethod){
            return 'slds-hide';
        }
        return 'slds-col slds-size_1-of-2';
    }

    get paymentMethodOptions(){
        return [
            {label: '--None--', value: ''},
            {label: methodPostalOrder, value: methodPostalOrder},
            {label: methodBankTransfer, value: methodBankTransfer}
        ];
    }

    get hasDirectDebit(){
        return this.hasDirectDebitInBillingProfile;
    }

    get showDirectDebit (){
        if(this.hasDirectDebitInBillingProfile){
            return 'slds-col slds-size_1-of-2';
        }
        return 'slds-hide';
    }

    get hideDirectDebit (){
        if(this.hasDirectDebitInBillingProfile){
            return 'slds-hide';
        }
        return 'slds-col slds-size_1-of-2';
    }

    setPaymentMethodOption(){
       if (this.buttonType === 'new' || this.template.querySelector('[data-id="paymentMethodVal"]').value === 'Direct Debit'){
            this.template.querySelector('[data-id="paymentMethodVal"]').value = this.paymentMethodValue;
        }
        else {
            this.paymentMethodValue = this.template.querySelector('[data-id="paymentMethodVal"]').value;
        }
    }
}