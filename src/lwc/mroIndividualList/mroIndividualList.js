/**
 * Created by David diop on 28.04.2020.
 */

import {LightningElement,api,track} from 'lwc';
import { labels } from 'c/labels';

export default class MroIndividualList extends LightningElement {
    @api searchFields;
    @api interlocutorList;
    @api interactionId;
    @api formTitle = labels.interlocutorSearchResults;
    @api notInterlocutorOrContact = labels.notInterlocutorInTheList;
    @api connectionRepresentant =  false;

    labels = labels;

    onNew() {
        this.dispatchEvent(new CustomEvent('redirect', {
            detail: {
                'page': 'individualEdit',
                'searchFields': this.searchFields
            }}));
    }
    onBack() {
        this.dispatchEvent(new CustomEvent('redirect', {
            detail: {
                'page': 'individualSearch',
                'searchFields': this.searchFields
            }}));
    }

    onSelect(event) {
        if (this.connectionRepresentant) {
            this.dispatchEvent(new CustomEvent('select', {
                detail: {
                    'contact': event.detail.contactSelected
                }
            }));
        } else {
            this.dispatchEvent(new CustomEvent('select', {
                detail: {
                    'individual': event.detail.individual
                }
            }));
        }
    }
}