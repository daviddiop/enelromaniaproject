import {LightningElement, track, api} from 'lwc';
import {labels} from "c/mroLabels";

export default class MroProductShortInfo extends LightningElement {
    @track product;
    labels = labels;

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    @api get productJson() {
        return this.product
    }
    set productJson(e) {
        if (!this.IsJsonString(e)){
            return;
        }
        this.product = JSON.parse(e);
    }
}
