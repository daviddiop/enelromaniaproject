/**
 * Created by goudiaby on 03/03/2020.
 */

import {api, track, wire, LightningElement} from 'lwc';
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';
import {NavigationMixin} from 'lightning/navigation';
import {getRecord, updateRecord} from 'lightning/uiRecordApi';

import updateChildActivitiesRefusalCase from "@salesforce/apex/MRO_SRV_Activity.updateChildActivitiesCloseStatusAndReason";
import refuseRetentionActivity from "@salesforce/apex/MRO_LC_ActivityActions.refuseRetentionActivity";
import acceptRetentionActivity from "@salesforce/apex/MRO_LC_ActivityActions.acceptRetentionActivity";
import cancelActivity from "@salesforce/apex/MRO_LC_ActivityActions.cancelActivity";
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

import ACTIVITY_ISCLOSED_FIELD from '@salesforce/schema/wrts_prcgvr__Activity__c.wrts_prcgvr__IsClosed__c';
import ACTIVITY_STATUS_FIELD from '@salesforce/schema/wrts_prcgvr__Activity__c.wrts_prcgvr__Status__c';
import ACTIVITY_TYPE_FIELD from '@salesforce/schema/wrts_prcgvr__Activity__c.Type__c';
import ACTIVITY_FIELDSTEMPLATE_FIELD from '@salesforce/schema/wrts_prcgvr__Activity__c.FieldsTemplate__c';
import ACTIVITY_OBJECTID_FIELD from '@salesforce/schema/wrts_prcgvr__Activity__c.wrts_prcgvr__ObjectId__c';
import ACTIVITY_ACCOUNTID_FIELD from '@salesforce/schema/wrts_prcgvr__Activity__c.Account__c';
import ACTIVITY_NOTCANCELLABLE_FIELD from '@salesforce/schema/wrts_prcgvr__Activity__c.IsNotCancellable__c';

export default class MroActivityActions extends NavigationMixin(LightningElement) {

    @api recordId;
    @track renderForm = false;
    @track showSpinner = false;
    parentActivity = '';
    firstEntry = true;
    labels = labels;
    @track isClassifiedDiscard = false;
    @track isGenericDiscard = false;
    @track isDataEntry = false;
    @track isRetention = false;
    @track isPowerVoltageChange = false;
    @track isCancellable = true;
    @track renderDiscardManagementForm = false;
    parentRecordId;
    accountId;

    @track isAddressVerification = false;
    @track renderAddressNormalizationForm = false;
    @track showCancellationConfirmDialog = false;

    @wire(getRecord, { recordId: '$recordId',
    fields:
        [ACTIVITY_ISCLOSED_FIELD,
        ACTIVITY_STATUS_FIELD,
        ACTIVITY_TYPE_FIELD,
        ACTIVITY_FIELDSTEMPLATE_FIELD,
        ACTIVITY_OBJECTID_FIELD,
        ACTIVITY_ACCOUNTID_FIELD,
        ACTIVITY_NOTCANCELLABLE_FIELD]
    })
    activity({error, data}) {
        if (error) {
            console.error(error);
        }
        else if (data) {
            console.log('Record data', JSON.parse(JSON.stringify(data)));
            this.parentRecordId = data.fields.wrts_prcgvr__ObjectId__c.value;
            this.accountId = data.fields.Account__c.value;
            if (!data.fields.wrts_prcgvr__IsClosed__c.value) {
                const type = data.fields.Type__c.value;
                if (type === 'Discard') {
                    const fieldsTemplateId = data.fields.FieldsTemplate__c.value;
                    this.isGenericDiscard = !fieldsTemplateId;
                    this.isClassifiedDiscard = !this.isGenericDiscard;
                } else if (type === 'DataEntry') {
                    this.isDataEntry = true;
                } else if (type === 'Address Verification') {
                    this.isAddressVerification = true;
                }
                this.isRetention = type.includes('Retention');
                this.isPowerVoltageChange = type.includes('Product Change');
                this.isCancellable = !data.fields.IsNotCancellable__c.value;
            }
        }
    }

    handleLoad(event) {
        event.preventDefault;
        event.stopPropagation();
        this.showSpinner = true;
        let fieldList = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fieldList[element.fieldName] = element.value;
        });

        if (this.firstEntry) {
            if (!fieldList['Parent__c']) {
                error(this, this.labels.parentActivityMissing);
                this.showSpinner = false;
                this.renderForm = false;
                return;
            }
            this.parentActivity = fieldList['Parent__c'];
            fieldList.wrts_prcgvr__Status__c = 'In working';

            this.template.querySelector('lightning-record-edit-form').submit(fieldList);
            this.firstEntry = false;
        }
        this.showSpinner = false;
        this.renderForm = false;
    }

    handleSuccess(event) {
        event.preventDefault();
        this.showSpinner = true;
        success(this, this.labels.retentionAccepted);
        setTimeout(() => this.showSpinner = false, 500);
        //this.handleNavigateToRecord(this.parentActivity);
    }

    handleRefusalSuccess(event) {
        let fields = event.detail;
        let parentId = fields.parentId;
        let reason = fields.refusalReason;
        let reasonDetails = fields.detailsReason;
        this.updateChildActivities(parentId,reason,reasonDetails);
    }

    updateChildActivities(parentId, refusalReason, refusalReasonDetails){
        updateChildActivitiesRefusalCase({
            parentId : parentId,
            refusalReason : refusalReason,
            refusalReasonDetails : refusalReasonDetails
        }).then((response) => {
            if (response){
                console.log(response);
            }
        }).catch((errorMsg) => {
            this.spinner = false;
            error(this, errorMsg.body.message);
        });
    }

    handleAcceptWithProductChange() {

        console.log('handleAcceptWithProductChange');
        this[NavigationMixin.Navigate]({
            type: 'standard__component',
            attributes: {
                componentName: "c__MRO_LCP_ProductChangeWizard"
            },
            state: {
                c__accountId: this.accountId,
                c__activityId: this.recordId
            }
        });
    }

    handleAcceptWithoutProductChange() {

        console.log('handleAcceptWithoutProductChange');
        this.showSpinner = true;
        acceptRetentionActivity({
            activityId : this.recordId
        }).then(result => {

            console.log(result);
            if(result.error == true) {

                error(this, result.errorMsg + ' ' + result.errorTrace);
            } else {

                updateRecord({ fields: { Id: this.recordId } });
            }
            this.showSpinner = false;
        })
        .catch(error => {

            console.log(error);
            this.showSpinner = false;
        });
    }

    handleRefuse() {

        console.log('handleRefuse');
        this.showSpinner = true;
        refuseRetentionActivity({
            activityId : this.recordId
        }).then(result => {

            console.log(result);
            if(result.error == true) {

                error(this, result.errorMsg + ' ' + result.errorTrace);
            } else {

                updateRecord({ fields: { Id: this.recordId } });
            }
            this.showSpinner = false;
        })
        .catch(error => {

            console.log(error);
            this.showSpinner = false;
        });
    }

    handleDiscardManagement(event) {
        this.renderDiscardManagementForm = true;
    }

    handleCloseDiscardManagement(event) {
        this.renderDiscardManagementForm = false;
        if (event.detail.saved) {
            updateRecord({ fields: { Id: this.recordId } });
        }
    }

    handleRequestCorrected(event) {
        this.showSpinner = true;
        notCacheableCall({
            className: 'MRO_LC_DiscardManagement',
            methodName: 'closeActivity',
            input: {
                "activityId": this.recordId
            }
        }).then((response) => {
            if (response.isError) {
                this.showSpinner = false;
                error(this, response.error);
            } else if (response.data.result === 'OK') {
                this.showSpinner = false;
                success(this, this.labels.requestUpdated);
                this.handleNavigateToRecord(this.parentRecordId);
            }
        }).catch((err) => {
            //console.log(JSON.parse(JSON.stringify(err)));
            this.showSpinner = false;
            error(this, err);
        });
    }

    showCancelConfirmation() {
        this.showCancellationConfirmDialog = true;
    }

    handleCancelConfirmation(event) {
        if (event.target.name === 'cancellationConfirmDialog') {
            console.log('handleCancel');
            let displayMessage = 'Status: ' + event.detail.status + '. Event detail: ' + JSON.stringify(event.detail.originalMessage) + '.';
            if(event.detail.status === 'confirm') {
                this.showSpinner = true;
                cancelActivity({
                    activityId: this.recordId
                }).then(result => {

                    console.log(result);
                    if (result.error == true) {

                        error(this, result.errorMsg + ' ' + result.errorTrace);
                    } else {

                        updateRecord({fields: {Id: this.recordId}});
                    }
                    this.showSpinner = false;
                })
                    .catch(error => {

                        console.log(error);
                        this.showSpinner = false;
                    });
            } else if(event.detail.status === 'cancel') {
                this.showCancellationConfirmDialog = false;
            }
        }
    }

    /**
     * Navigates to new individual record page
     *
     * @param parentId object Parent
     */
    handleNavigateToRecord(parentId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: parentId,
                //objectApiName: 'wrts_prcgvr__Activity__c',
                actionName: 'view'
            }
        });
    }

    handleNormalizeAddress(event) {

        console.log('MroActivityActions.handleNormalizeAddress');
        this.renderAddressNormalizationForm = true;
    }

    handleCloseAddressNormalization() {

        console.log('MroActivityActions.handleCloseAddressVerification');
        this.renderAddressNormalizationForm = false;
    }
}