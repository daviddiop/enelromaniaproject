import { LightningElement, api } from "lwc";

export default class mroInputAutocomplete extends LightningElement {
  @api values;
  @api label = "";
  @api name = "";
  @api value = "";
  @api required;
  @api placeholder = "";
  @api readOnly;
  @api hidden;

  initialized = false;

  get inputType() {
    return this.hidden ? "hidden" : "text";
  }

  get _value() {
    return this.value === void 0 ? "" : this.value;
  }

  renderedCallback() {
    if (this.initialized) {
      return;
    }
    this.initialized = true;
    let listId = this.template.querySelector("datalist").id;
    this.template.querySelector("input").setAttribute("list", listId);
  }

  handleChange(evt) {
    this.value = evt.target.value;
    this.dispatchEvent(new CustomEvent("change", {
      bubbles: false,
      detail: { value: evt.target.value, target: this.name }
    }));
  }

  get hasValues() {
    return (this.values && this.values.length);
  }

}