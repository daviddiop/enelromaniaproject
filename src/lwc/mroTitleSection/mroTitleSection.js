/**
 * Created by Kimo CompuTeR on 2/18/2020.
 */

import {LightningElement,api} from 'lwc';

export default class MroTitleSection extends LightningElement {
    @api title;
    @api subtitle;
}