import { LightningElement, api, track } from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { labels } from 'c/labels';
import { error } from 'c/notificationSvc';
import { reduceErrors } from 'c/lwcUtils';

export default class IndividualSearchv extends LightningElement {
    @api interactionId;
    @api interlocutor = {};
    @api searchFields;
    @track spinner = false;
    @api formTitle = labels.searchForInterlocutor;
    @api connectionRepresentant = false;
    @api disabledForm = false;

    labels = labels;
    @track VatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;

    onLoad(){
        if (this.searchFields) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = this.searchFields[element.fieldName];
            });
        } else {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = '';
            });
        }
    }

    handleError(event) {
        error(this, event.detail.message);
        this.spinner = false;
    }

    handleSubmit(event) {
        this.spinner = true;
        event.preventDefault();
        let interactionObj = event.detail.fields;
        interactionObj.Id = this.interactionId;
        if (this.removeExtraFieldsAndCheckRequired(interactionObj)) {
            error(this, labels.noDataEntered);
            this.spinner = false;
            return;
        }
        let searchFields = {
            'InterlocutorFirstName__c': interactionObj.InterlocutorFirstName__c,
            'InterlocutorLastName__c': interactionObj.InterlocutorLastName__c,
            'InterlocutorNationalIdentityNumber__c': interactionObj.InterlocutorNationalIdentityNumber__c,
            'InterlocutorEmail__c': interactionObj.InterlocutorEmail__c,
            'InterlocutorPhone__c': interactionObj.InterlocutorPhone__c
        };
        let searchMethod = 'searchInterlocutor';
        if(this.connectionRepresentant){
            searchMethod = 'searchContact';
        }
        notCacheableCall({
            className: 'MRO_LC_Individual',
            methodName: searchMethod,
            input: interactionObj
        }).then((result) => {
            this.spinner = false;
            if (result.isError) {
                error(this, JSON.stringify(result.error));
            } else {
                if (result.data && result.data.length > 0) {
                    this.dispatchEvent(new CustomEvent('search', {
                        detail: {
                            'individualList': result.data,
                            'searchFields': searchFields
                        }
                    }));
                } else {
                    this.dispatchEvent(new CustomEvent('search', {
                        detail: {
                            'individualList': [],
                            'searchFields': searchFields
                        }
                    }));
                }

            }
        }).catch((errorMsg) => {
            error(this, reduceErrors(errorMsg));
            this.spinner = false;
        });
    }
    removeExtraFieldsAndCheckRequired(obj) {
        let fields = [
            "InterlocutorFirstName__c","InterlocutorLastName__c","InterlocutorNationalIdentityNumber__c","InterlocutorEmail__c","InterlocutorPhone__c"
        ];
        for (let prop in obj) {
            if (prop !== "Id" && prop !== "id" && fields.indexOf(prop) === -1) {
                obj[prop] = undefined;
            }
        }
        for (let field of fields) {
            if (obj.hasOwnProperty(field) && obj[field]) {
                let value = obj[field];
                if (typeof value === 'string' && value.trim()) {
                    return false;
                } else if (typeof value !== 'string') {
                    return false;
                }
            }
        }
        return true;
    }
}