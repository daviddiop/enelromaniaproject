import {CurrentPageReference} from 'lightning/navigation';
import {LightningElement, track, wire, api} from 'lwc';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';


export default class MroInvoiceInquiryPeriodSelection extends LightningElement {

  @api recordId;
  @track invoiceStartDateDefault;
  @track invoiceStartDate;
  @track invoiceEndDate;

  labels = labels;

  connectedCallback() {
    this.invoiceStartDateDefault = this.setInvoiceStartDateDefault(new Date());
    this.invoiceStartDate = this.invoiceStartDateDefault;
    this.invoiceEndDate = this.setInvoiceEndDate(new Date())
  }

  setInvoiceStartDateDefault(currentDay) {
    let threeMonthsAgo = new Date(currentDay.setMonth(currentDay.getMonth() - 2));
    let threeMonthsAgoDigit = threeMonthsAgo.getMonth();

    if (threeMonthsAgoDigit <= 9) {
      threeMonthsAgoDigit = '0' + threeMonthsAgoDigit;
    }

    return threeMonthsAgo.getFullYear() + "-" + threeMonthsAgoDigit + "-" + threeMonthsAgo.getDate();
  }

  setInvoiceEndDate(currentDay) {
    let monthDigit = currentDay.getMonth() + 1;

    if (monthDigit <= 9) {
      monthDigit = '0' + monthDigit;
    }

    return currentDay.getFullYear() + "-" + monthDigit + "-" + currentDay.getDate();
  }

  onHandleInvoiceInquiryStartDateChange(event) {
    let inputStartDate = event.target.value;

    if (this.invoiceEndDate) {
      let parsedInputStartDate = Date.parse(inputStartDate);
      let parsedInvoiceEndDate = Date.parse(this.invoiceEndDate);

      if (parsedInputStartDate >= parsedInvoiceEndDate) {
        error(this, labels.startDateAfterEndDate);
        inputStartDate = "";
        event.target.value = inputStartDate;
      }
    }
    this.invoiceStartDate = inputStartDate;
  }

}