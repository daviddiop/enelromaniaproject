import {LightningElement, api, track} from "lwc";
import {error} from "c/notificationSvc";
import {NavigationMixin} from "lightning/navigation";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from "c/labels";

export default class ContractAccountSelection extends NavigationMixin(
    LightningElement
) {
    labels = labels;
    @api showEditButton = false;
    @api showHeader = false;
    @api hideButtons = false;
    @api accountId;
    @api companyDivisionId;
    @api disabled = false;
    @api contractAccountRecordId = '';
    @track listContractAccounts = [];
    @track enableEditButtonCA = true;
    @track showModalContract = false;
    @track buttonType;
    @track enableButtons = true;
    @track recordList;

    @api
    reset(accId) {
        this.accountId = accId;
        this.reloadContractAccountRecords();
    }

    @api
    reload() {
        this.reloadContractAccountRecords();
    }

    connectedCallback() {
        this.reloadContractAccountRecords();
    }

    reloadContractAccountRecords() {
        if (!this.accountId || !this.companyDivisionId) {
            return;
        }
        let inputs = {
            accountId: this.accountId,
            companyDivisionId: this.companyDivisionId
        };
        notCacheableCall({
            className: "ContractAccountCnt",
            methodName: "getContractAccountRecords",
            input: inputs
        })
            .then(response => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                } else {
                    this.listContractAccounts = response.data.listContractAccount;
                    this.reloadRecordList();
                }
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    getActivatedSupplies() {
        let inputs = {contractAccountId: this.contractAccountRecordId};

        notCacheableCall({
            className: "ContractAccountCnt",
            methodName: "getActiveSupplies",
            input: inputs
        })
            .then(response => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                } else {
                    this.enableEditButtonCA = !response.data.enableEditButtonCA;
                    this.enableButtons = false;
                }
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    saveContractAccountEdit(event) {
        event.preventDefault();
        event.stopPropagation();
        if (event.detail.contractAccountEditId) {
            if ((this.buttonType === "new") || (this.buttonType === "clone")) {
                this.contractAccountRecordId = event.detail.contractAccountEditId;
                this.enableEditButtonCA = false;

                const selectedEvent = new CustomEvent("contractaccountselected", {
                    detail: {
                        contractAccountRecordId: this.contractAccountRecordId
                    }
                });
                this.dispatchEvent(selectedEvent);

            }
            this.reloadContractAccountRecords();
            if (event.detail.showModalContAcc) {
                this.showModalContract = false;
            } else {
                this.showModalContract = true;
            }
            this.enableButtons = false;


        }
    }

    handleStatusRadio(event) {
        this.contractAccountRecordId = event.target.value;

        const selectedEvent = new CustomEvent("contractaccountselected", {
            detail: {
                contractAccountRecordId: this.contractAccountRecordId
            }
        });
        this.dispatchEvent(selectedEvent);

        this.getActivatedSupplies();
        this.reloadRecordList();
    }

    reloadRecordList() {
        let records = [];
        if (this.listContractAccounts) {
            for (let el of this.listContractAccounts) {
                if (el.BillingProfile__r) {
                    let address =
                        el.BillingProfile__r.BillingStreetType__c +
                        " " +
                        el.BillingProfile__r.BillingStreetName__c +
                        " " +
                        el.BillingProfile__r.BillingStreetNumber__c +
                        (typeof el.BillingProfile__r.BillingStreetNumberExtn__c ===
                        "undefined" || !el.BillingProfile__r.BillingStreetNumberExtn__c
                            ? ""
                            : el.BillingProfile__r.BillingStreetNumberExtn__c) +
                        ", " +
                        el.BillingProfile__r.BillingCity__c +
                        " " +
                        el.BillingProfile__r.BillingPostalCode__c;
                    let record = {};
                    record.Id = el.Id;

                    record.value = el.BillingProfile__r.PaymentMethod__c;
                    if (el.BillingProfile__r.PaymentMethod__c === "Giro") {
                        record.value =
                            record.value + " - " + " IBAN : " + el.BillingProfile__r.IBAN__c;
                    }

                    record.value =
                        record.value + " - " + el.BillingProfile__r.DeliveryChannel__c;
                    switch (el.BillingProfile__r.DeliveryChannel__c) {
                        case "Email":
                            record.value = record.value + " : " + el.BillingProfile__r.Email__c;
                            break;
                        case "Mail":
                            break;
                        case "Fax":
                            record.value = record.value + " : " + el.BillingProfile__r.Fax__c;
                            break;

                        default:
                            break;
                    }

                    record.value = (((typeof el.BillingProfile__r.BillingStreetName__c === "undefined") || (!el.BillingProfile__r.BillingStreetName__c))
                                    || ((typeof el.BillingProfile__r.BillingStreetType__c === "undefined") || (!el.BillingProfile__r.BillingStreetType__c))
                                    || ((typeof el.BillingProfile__r.BillingStreetNumber__c === "undefined") || (!el.BillingProfile__r.BillingStreetNumber__c))
                                    || ((typeof el.BillingProfile__r.BillingCity__c === "undefined") || (!el.BillingProfile__r.BillingCity__c))
                                    || ((typeof el.BillingProfile__r.BillingPostalCode__c === "undefined") || (!el.BillingProfile__r.BillingPostalCode__c))) ? record.value + " (" + el.BillingFrequency__c + " - " + el.PaymentTerms__c + ")" : record.value + " - " + address + " (" + el.BillingFrequency__c + " - " + el.PaymentTerms__c + ")";
                    record.selected = (el.Id === this.contractAccountRecordId);
                    records.push(record);
                }

            }
        }
        this.recordList = records;
    }

    get addClass() {
        if (this.disabled) {
            return "slds-card_boundary slds-form-element slds-size_11-of-12 slds-scrollable_y slds-class slds-theme_shade";
        }
        return "slds-card_boundary slds-form-element slds-size_11-of-12 slds-scrollable_y";
    }

    get addButtonClass() {
        if (this.showEditButton) {
            return "slds-align_absolute-center slds-p-bottom_small";
        }
        return "slds-align_absolute-center slds-p-bottom_small slds-hide";
    }

    newContractAccount(event) {
        event.preventDefault();
        this.showModalContract = true;
        this.buttonType = "new";
    }

    editContractAccount(event) {
        event.preventDefault();
        this.showModalContract = true;
        this.buttonType = "edit";
    }

    cloneContractAccount(event) {
        event.preventDefault();
        this.showModalContract = true;
        this.buttonType = "clone";
    }

    closeModal() {
        this.showModalContract = false;
    }

    closeHandle(event) {
        this.showModalContract = event.target.showModalContract;
    }
}