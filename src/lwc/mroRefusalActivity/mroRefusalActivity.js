/**
 * Created by goudiaby on 03/03/2020.
 */

import {api, LightningElement, track} from 'lwc';
import {labels} from 'c/mroLabels';
import {success} from 'c/notificationSvc';

export default class MroRefusalActivity extends LightningElement {
    @api recordId;
    @track isRendered = false;
    @track showReasonDetails = false;
    @track disabledConfirmButton = true;
    @track showLoadingSpinner = true;

    labels = labels;

    @api open() {
        this.isRendered = true
    }

    handleLoad() {
        if (!this.isRendered) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = '';
            });
        }
        this.showLoadingSpinner = false;
    }

    handleConfirm(event) {
        event.preventDefault();
        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });
        fields["wrts_prcgvr__Status__c"] = 'Cancelled';
        fields["wrts_prcgvr__IsClosed__c"] = true;
        this.template.querySelector("lightning-record-edit-form").submit(fields);
    }

    handleSuccess(event) {
        event.preventDefault();
        event.stopPropagation();
        let payload = JSON.parse(JSON.stringify(event.detail));
        let fields = payload.fields;
        success(this, this.labels.cancelledSuccess);
        this.dispatchEvent(new CustomEvent('success', {
            detail: {
                parentId : payload.id,
                refusalReason: fields['CancellationReason__c'].value,
                detailsReason: fields['CancellationDetails__c'].value
            }
        }));
        this.isRendered = false;
    }

    handleReasonChange(event) {
        let reasonField = event.target;
        this.showReasonDetails = false;
        if (reasonField.value && reasonField.value !== '') {
            if (reasonField.value === 'Other reason') {
                this.showReasonDetails = true;
            } else {
                this.disabledConfirmButton = false;
            }
        } else {
            this.disabledConfirmButton = true;
        }
    }

    handleDetailsReasonChange(event) {
        let detailsReason = event.target;
        this.disabledConfirmButton = !(detailsReason.value && detailsReason.value !== '');
    }

    handleCancel() {
        this.isRendered = false;
    }
}