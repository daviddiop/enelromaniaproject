/**
 * Created by BADJI on 06/02/2020.
 */

import { LightningElement, api, track } from "lwc";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';

export default class MroPeriodSelections extends LightningElement {

    @api startDate;
    @api endDate;
    @api minDateOfStart;
    @api minDateOfEnd;
    @api maxDateOfStart;
    @api maxDateOfEnd;
    @track disabled =false;
    labels = labels;
    @track maxDateValue;
    @api isDisabledStart = false;
    @api isDisabledEnd = false;
    @track enableStartDate = false;
    @track enableEndDate = false;
    @api wizardType = '';
    @api disabledEditButton;
    @api disabledConfirmButton;
    @api showConfirmButton = false;
    @api showEditButton = false;
    @api hideLabel =false;

    @api
    onConfirmSelection() {
        let validDate = true;
        let selectedStartDate = this.template.querySelector('[data-id="startDate"]').value;
        let selectedEndDate = this.template.querySelector('[data-id="endDate"]').value;
        if(this.wizardType == 'Meter Reading'){
           if((selectedStartDate> selectedEndDate) || (selectedStartDate < this.startDate)){
               validDate = false;
                error(this, this.labels.rangeNotValid);

           } else {
               this.template.querySelector('[data-id="startDate"]').disabled=true;
           }

        }

        this.dispatchEvent(new CustomEvent('selectedrange', {
            detail: {
                'startDate': selectedStartDate,
                'endDate' : selectedEndDate,
                'isValidDate' : validDate
            }
        }));
    }


     @api
     onEdit(){
         this.dispatchEvent(new CustomEvent('editperiod'));
     }


    @api
    addStartError(){
        this.template.querySelector('[data-id="startDate"]').classList.add('slds-has-error');
    }

    @api
    addEndError(){
        this.template.querySelector('[data-id="endDate"]').classList.add('slds-has-error');
    }

    @api
    handleValidDate(){
        let selectedStartDate =  new Date(this.template.querySelector('[data-id="startDate"]').value);
        let selectedEndDate =  new Date(this.template.querySelector('[data-id="endDate"]').value);
        return selectedStartDate <= selectedEndDate;
    }

    @api
    handleDisabledSelection(showEditButton,showConfirmButton,isDisabledStart,isDisabledEnd){
        this.showEditButton = showEditButton;
        this.showConfirmButton = showConfirmButton;
        this.template.querySelector('[data-id="startDate"]').disabled = isDisabledStart;
        this.template.querySelector('[data-id="endDate"]').disabled = isDisabledEnd;
    }

}