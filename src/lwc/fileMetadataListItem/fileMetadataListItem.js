import { LightningElement, api, wire } from "lwc";
import { NavigationMixin } from 'lightning/navigation';
import { labels } from  'c/labels';
import { getRecord } from 'lightning/uiRecordApi';

export default class DocumentListItem extends NavigationMixin(LightningElement) {

    labels = labels;
    @api fileMetadataId;

    @wire(getRecord, { recordId: '$fileMetadataId', fields: ['FileMetadata__c.Name','FileMetadata__c.Title__c','FileMetadata__c.DocumentId__c','FileMetadata__c.ExternalId__c'] })
    fileMetadata;

    get documentId(){
        if(!this.fileMetadata.data) return null;
        return this.fileMetadata.data.fields.DocumentId__c.value;
    }

    handleCheckDocument() {
        this[NavigationMixin.Navigate]({
            type: "standard__component",
            attributes: {
                componentName: "c__validatableDocumentsListWrp"
            },
            state: {
                c__recordId: this.fileMetadataId
            }
        });
    }

    navigateToDocument() {
        this[NavigationMixin.Navigate]({
           type: "standard__recordPage",
           attributes: {
               "recordId": this.documentId,
               "objectApiName": "ContentDocument",
               "actionName": "view"
           }
       });
   }
}