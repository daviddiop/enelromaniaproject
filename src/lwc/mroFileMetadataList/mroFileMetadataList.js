import { LightningElement, api } from 'lwc';
import { labels } from  'c/labels';

export default class MroFileMetadataList extends LightningElement {
    @api fileMetadataList;
    @api disableCheckDocument = false;
    labels = labels;
}