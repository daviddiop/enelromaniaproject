import {LightningElement, api, track} from "lwc";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {error, success} from "c/notificationSvc";
//import {mroLabels} from "c/mroLabels";
import {labels} from "c/mroLabels";
import ACCOUNT_OBJECT from "@salesforce/schema/Account";
import getAvailableAddressesCountryRestricted from '@salesforce/apex/MRO_SRV_Address.getAvailableAddressesCountryRestricted';
import {checkCNP, checkCUI} from "c/mroValidations";

export default class MroAccountEdit extends LightningElement {
    @api recordId = "";
    @api account;
    @api interactionId;
    @api showRole = false;
    @api showCancel = false;
    @api isNewRecord = false;
    @api readOnly = false;
    @api addressForcedResidential = false;

    @track recordTypeId;
    @track accountObject = ACCOUNT_OBJECT;
    @track selectedRoles = [];
    @track address;
    @track showLoadingSpinner = false;
    @track isPerson = false;

    @track showInsolvencyStartDate = false;
    @track showInsolvencyEndDate = false;
    @track showBankruptcyDate = false;
    @track availableAddresses;

    businessRecordType;
    personRecordType;
    recordTypeOptions = [];
    rolesOptionList = [];
    isEdit = false;
    addressForm = false;

    labels = labels;
    @track VatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;
    @track acc = {
        Name: "",
        VATNumber__c: "",
        Phone: "",
        isPersonAccount: false,
        Email__c: "",
        FirstName: "",
        LastName: "",
        NationalIdentityNumber__pc: "",
        ForeignCitizen__pc: false,
        ForeignCompany__c: false,
        PersonMobilePhone: "",
        PersonEmail: "",
        NACEReference__c: "",
        ONRCCode__c: "",
        //FF Customer Creation - Pack1/2 - Interface Check
        IDDocumentNr__pc:"",
        IDDocumentSeries__pc:"",
        IDDocIssuer__pc:"",
        Website:"",
        Fax:"",
        VIP__c:false,
        VATExempted__c:false,
        BusinessType__c:"",
        NumberOfEmployees:"",
        AnnualRevenue:"",
        AnnualRevenueYear__c:"",
        ContactChannel__pc: "",
        InsolvencyBankruptcyStatus__c: "",
        InsolvencyStartDate__c: "",
        InsolvencyEndDate__c: "",
        BankruptcyDate__c: "",
        //FF Customer Creation - Pack1/2 - Interface Check
        toString: function () {
            return JSON.stringify(this);
        }
    };

    connectedCallback() {
        this.isEdit = this.recordId !== "";
        this.initialize();
    }

    initialize() {
        notCacheableCall({
            className: "MRO_LC_AccountEdit",
            methodName: "getOptionList"
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.addressForm = true;
                    this.personRecordType = response.data.personRecordType.value;
                    this.businessRecordType = response.data.businessRecordType.value;
                    this.recordTypeOptions.push(response.data.businessRecordType);
                    this.recordTypeOptions.push(response.data.personRecordType);
                    this.rolesOptionList = response.data.roleOptionList;
                    if (this.recordId) {
                        this.retrieveAccount(this.recordId);
                    } else {
                        this.isNewRecord = true;
                        this.recordTypeId = response.data.businessRecordType.value;
                        this.isBusiness = true;
                        this.isCreation = true;
                        this.isUpdate = false;
                        for (let field in this.account) {
                            if (field) {
                                this.acc[field] = this.account[field];
                                if (field === 'InsolvencyBankruptcyStatus__c') {
                                    const value = this.acc[field];
                                    this.showInsolvencyStartDate = (value === 'Insolvent' || value === 'Insolvency closed');
                                    this.showInsolvencyEndDate = (value === 'Insolvency closed');
                                    this.showBankruptcyDate = (value === 'Bankrupt');
                                }
                            }
                        }
                        let addr = {
                            'streetNumber': null,
                            'streetNumberExtn': null,
                            'streetName': null,
                            'streetType': null,
                            'apartment': null,
                            'building': null,
                            'city': null,
                            'country': 'ROMANIA',
                            'floor': null,
                            'locality': null,
                            'postalCode': null,
                            'province': null
                        };
                        this.address = addr;
                    }
                }
                getAvailableAddressesCountryRestricted({
                    accountId: this.accountId,
                    restrictToCountry: "ROMANIA"
                }).then((response) => {
                    this.availableAddresses = response;
                }).catch((errorMsg) => {
                    if(errorMsg.body){
                        error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                    } else {
                        error(this,errorMsg);
                    }
                    this.spinner = false;
                });
            }
        });
    }

    retrieveAccount(accountId) {
        let inputs = {accountId: accountId};
        notCacheableCall({
            className: "MRO_LC_AccountEdit",
            methodName: "retrieveAccount",
            input: inputs
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.acc = response.data;
                    this.isUpdate = true;
                    if (response.data.IsPersonAccount.value === true) {
                        this.isPerson = true;
                    }
                    this.addressForm = true;
                    let address = {};
                    address.streetNumber = response.data.ResidentialStreetNumber__c;
                    address.streetName = response.data.ResidentialStreetName__c;
                    address.streetType = response.data.ResidentialStreetType__c;
                    address.apartment = response.data.ResidentialApartment__c;
                    address.building = response.data.ResidentialBuilding__c;
                    address.city = response.data.ResidentialCity__c;
                    address.country = response.data.ResidentialCountry__c;
                    address.floor = response.data.ResidentialFloor__c;
                    address.locality = response.data.ResidentialLocality__c;
                    address.postalCode = response.data.ResidentialPostalCode__c;
                    address.province = response.data.ResidentialProvince__c;
                    address.streetNumbersExtn = response.data.AddressStreetNumberExtn__c;
                    this.address = address;
                    if (response.data.ResidentialAddressNormalized__c) {
                        this.readOnly = true;
                    }
                }
            }
        });
    }

    selectedAccountType(event) {
        this.recordTypeId = event.target.value;
        this.isPerson = this.recordTypeId === this.personRecordType;
    }

    handleSaveAccount(event) {
        event.preventDefault();
        this.showLoadingSpinner = true;
        const fields = event.detail.fields;
        let rolesList = [];
        this.selectedRoles.forEach(element => {
            rolesList.push(element.value);
        });
        let rolesListString = rolesList.toString();

        fields.IsPersonAccount = this.recordTypeId !== this.businessRecordType;
        if (this.validateFields()) {
            let addrForm = this.template.querySelector('c-mro-address-form');
            if (!addrForm.isValid()) {
                error(this, 'Address must be verified');
                this.showLoadingSpinner = false;
                return;
            }
            let addrFields = addrForm.getValues();
            for (let f in addrFields) {
                if (addrFields.hasOwnProperty(f)) {
                    fields[f] = addrFields[f];
                }
            }
            //start add by David
            if(this.addressForcedResidential == false){
                fields.ResidentialAddressNormalized__c = true;
            } else{
                fields.ResidentialAddressNormalized__c = false;
            }
            // end add by David

            if(fields.IsPersonAccount){
                let nationalIdentityNumberField =  this.template.querySelector('[data-id="NationalIdentityNumber__pc"]');
                let nationalIdentityNumber =  nationalIdentityNumberField.value;
                let foreignCitizenField =  this.template.querySelector('[data-id="ForeignCitizen__pc"]');
                let foreignCitizen =  foreignCitizenField.value;
                if(foreignCitizen !== true) {
                    let check = checkCNP(nationalIdentityNumber);
                    if (!check.outCome) {
                        error(this, this.labels.invalidCnp.replace('{0}', check.errorCode).replace('{1}', nationalIdentityNumber));
                        nationalIdentityNumberField.classList.add('slds-has-error');
                        this.showLoadingSpinner = false;
                        return;
                    }
                }
            } else {
                let VATNumberField =  this.template.querySelector('[data-id="VATNumber__c"]');
                let VATNumber =  VATNumberField.value;
                let foreignCompanyField =  this.template.querySelector('[data-id="ForeignCompany__c"]');
                let foreignCompany =  foreignCompanyField.value;
                if(foreignCompany !== true) {
                    let checkVatNumber = checkCUI(VATNumber);
                    if (!checkVatNumber.outCome) {
                        error(this, this.labels.invalidCUI.replace('{0}', checkVatNumber.errorCode).replace('{1}', VATNumber));
                        VATNumberField.classList.add('slds-has-error');
                        this.showLoadingSpinner = false;
                        return;
                    }
                }
            }

            let inputs = {
                account: JSON.stringify(fields),
                interactionRecord: this.interactionId,
                roles: rolesListString,
                recordTypeId: this.recordTypeId
            };

            notCacheableCall({
                className: 'MRO_LC_AccountEdit',
                methodName: 'saveAccount',
                input: inputs
            })
                .then((response) => {
                    if (response) {
                        if (response.isError) {
                            error(this, response.error);
                        } else {
                            const acct = response.data;
                            if (acct) {
                                success(this, this.labels.success);
                                this.dispatchEvent(new CustomEvent('success', {
                                    detail: {
                                        'type': 'new',
                                        'account': acct
                                    }
                                }));
                            }
                        }
                        this.showLoadingSpinner = false;
                    }
                })
                .catch((errorMsg) => {
                    this.showLoadingSpinner = false;
                    error(this, errorMsg);
                });
        }
    }

    updateAccount() {
        let addrForm = this.template.querySelector('c-address-form');
        if (!addrForm.isValid()) {
            error(this, 'Address must be verified');
            return;
        }

        let addrFields = addrForm.getValues();
        for (let f in addrFields) {
            if (addrFields.hasOwnProperty(f)) {
                this.acc[f] = addrFields[f];
            }
        }
        //start add by David
        if(this.addressForcedResidential == false){
            this.acc.ResidentialAddressNormalized__c = true;
        } else{
            this.acc.ResidentialAddressNormalized__c = false;
        }
        // end add by David
        let inputs = {account: JSON.stringify(this.acc)};
        notCacheableCall({
            className: 'MRO_LC_AccountEdit',
            methodName: 'updateAccount',
            input: inputs
        })
            .then((response) => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        const acct = response.data;
                        if (acct) {
                            success(this, this.labels.success);
                            this.dispatchEvent(new CustomEvent('success', {
                                detail: {
                                    'type': 'new',
                                    'account': acct
                                }
                            }));
                        }

                    }
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    handleChangeInput(event) {
        const fieldName = event.target.fieldName;
        this.acc[fieldName] = event.target.value;
        if (fieldName === 'InsolvencyBankruptcyStatus__c') {
            const value = this.acc[fieldName];
            this.showInsolvencyStartDate = (value === 'Insolvent' || value === 'Insolvency closed');
            this.showInsolvencyEndDate = (value === 'Insolvency closed');
            this.showBankruptcyDate = (value === 'Bankrupt');
        }
        event.target.classList.remove("slds-has-error");
    }

    handleOptionRolesChange(event) {
        let selectedRole = {value: event.detail.value, label: event.detail.value};
        this.selectedRoles.push(selectedRole);
        this.rolesOptionList = this.rolesOptionList.filter(roleOption => {
            return roleOption.value !== selectedRole.value;
        });
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput === null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

    cancel() {
        this.dispatchEvent(
            new CustomEvent("cancel", {
                detail: {
                    type: "cancel"
                }
            })
        );
    }

    clear(event) {
        let selectedPill = {value: event.detail.name, label: event.detail.name};
        this.rolesOptionList.push(selectedPill);
        this.selectedRoles = this.selectedRoles.filter(roleOption => {
            return roleOption.value !== selectedPill.value;
        });
    }

    onBack() {
        this.dispatchEvent(new CustomEvent('back', {
            detail: {
                'page': 'accountList'
            }
        }));
    }
    disabledSaveButtonResiAddress(event){
        this.isNormalize = event.detail.isnormalize;
        this.addressForcedResidential = event.detail.addressForced;
    }
}