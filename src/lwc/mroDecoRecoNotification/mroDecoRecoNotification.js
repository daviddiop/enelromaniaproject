/**
 * Created by giupastore on 31/08/2020.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';

const CLAIMED = 'Claimed';

export default class MroDecoRecoNotification extends LightningElement {

    @api startDate = 'start';
    @api endDate = 'end';
    @api customerCode;
    @api hideConfirmSelection;
    @api hideSelectionColumn = false;
    @api showNotificationsAlreadyMarked = false;
    @api isNotMandatory = false;
    @api disableSelectClaimedNotification = false;
    @api hideTVAColumn = false;
    @api supplyId;
    @api getSelectedNotification() {
        this.onConfirmSelection();
    }

    @track notificationList;
    @track selectAll;

    labels = labels;
    /*paymentColumns = [
        { label: 'Payment Date', fieldName: 'paymentDate', type: 'date', sortable: true },
        { label: 'Amount', fieldName: 'paymentValue', type: 'number', sortable: true },
        { label: 'Type', fieldName: 'paymentType', type: 'text', sortable: true },
        { label: 'Balance', fieldName: 'finalBalance', type: 'number', sortable: true }
    ];
    billDetailColumns = [
        { label: 'Item', fieldName: 'item', type: 'text' },
        { label: 'Quantity', fieldName: 'quantity', type: 'number' },
        { label: 'Value', fieldName: 'value', type: 'number' },
        { label: 'VAT', fieldName: 'vat', type: 'text' }
    ];
    meterDetailColumns = [
        { label: 'Serial Number', fieldName: 'serialNumber', type: 'text' },
        { label: 'Dial', fieldName: 'dial', type: 'text' },
        { label: 'New Reading', fieldName: 'newReading', type: 'number' },
        { label: 'Old Reading', fieldName: 'oldReading', type: 'number' },
        { label: 'Quantity', fieldName: 'quantity', type: 'number' },
        { label: 'Average Consumption', fieldName: 'averageConsumption', type: 'number' }
    ];*/

    renderedCallback(){
        if (this.showNotificationsAlreadyMarked || this.disableSelectClaimedNotification){
            if (this.notificationList){
                this.inhibitionRowsAlreadyMarked();
            }
        }
    }

    handleRetrieveNotifications() {
        notCacheableCall({
            className: "MRO_LC_DecoRecoNotification",
            methodName: "listDisconnectionNotification",
            input: {
                'startDate': this.startDate,
                'endDate': this.endDate,
                'customerCode': this.customerCode,
                'supplyId' : this.supplyId
            }
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }
            this.notificationList = JSON.parse(JSON.stringify(apexResponse.data));
        });
    }
    /*showInvoiceDetails(event) {
        var invoiceId = event.target.dataset.id;
        if (!invoiceId) {
            return;
        }
        notCacheableCall({
            className: "MRO_LC_InvoicesInquiry",
            methodName: "getInvoiceDetails",
            input: {
                'invoiceId': invoiceId,
            }
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }
            this.invoiceDetailList = apexResponse.data;
            this.detailListRendered = true;
        });
    }*/
   /* closeDetailList() {
        this.invoiceDetailList = [];
        this.detailListRendered = false;
    }*/
    onSelectAll(event) {
        let self = this;
        this.notificationListList.map(function(notification) {
            if (!self.disableSelectClaimedNotification|| notification.status !== CLAIMED){
                notification.selected = event.target.checked;
            }
            return notification
        });
    }
    onSelect(event) {
        let foundNotification = this.findNotification(event.target.dataset.id);
        if (!foundNotification) {
            return;
        }
        foundNotification.selected = event.target.checked;
    }
    findNotification(notificationId) {
        if (!notificationId) {
            return null;
        }
        return this.notificationList.find(function (notification) {
            return notification.notificationId === notificationId;
        });
    }
    /*findInvoiceDetail(invoiceDetailId) {
        if (!invoiceDetailId) {
            return null;
        }
        return this.invoiceDetailList.find(function (invoiceDetail) {
            return invoiceDetail.invoiceDetailId === invoiceDetailId;
        });
    }*/
    onConfirmSelection() {
        let self = this;
        if (!this.notificationList) {
            return;
        }
        let notificationList = this.notificationListList.reduce(function(myNotificationList, notification) {
            if (notification.selected === true) {
                let notificationId = {
                    'notificationId': notification.notificationId
                };
                if (!self.disableSelectClaimedNotification || notification.status !== CLAIMED){
                    myNotificationList.selectedNotificationList.push(notificationId);
                }
                if (!notification.isMarked) {
                    myNotificationList.markedNotificationList.push(notificationId);
                }
            }
            return myNotificationList;
        }, {selectedNotificationList: [], markedNotificationList: []});
        if (!notificationList.selectedNotificationList.length && !this.isNotMandatory) {
            error(this, 'Notifications didn\'t select.');
        } else {
            console.log(JSON.stringify(notificationList));
            this.dispatchEvent(new CustomEvent('notificationselect', {
                detail: {
                    'selectedNotificationList': notificationList.selectedNotificationList,
                    'markedNotificationList': notificationList.markedNotificationList
                }
            }));
        }
    }

    inhibitionRowsAlreadyMarked() {
        let rows = Array.from(this.template.querySelectorAll('tr.slds-hint-parent lightning-input'));
        rows.forEach(checkboxElement => {
            let notification = this.findNotification(checkboxElement.dataset.id);
            if (this.showNotificationsAlreadyMarked && notification && notification.isMarked){
                checkboxElement.disabled = true;
                checkboxElement.readOnly = true;
                checkboxElement.checked = true;
            }
            if (this.disableSelectClaimedNotification && notification && notification.status === CLAIMED){
                checkboxElement.disabled = true;
                checkboxElement.readOnly = true;
                checkboxElement.checked = false;
            }
        });
    }
}