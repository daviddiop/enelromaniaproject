import { LightningElement, api, track } from 'lwc';
import EAppStyle from '@salesforce/resourceUrl/EnergyAppResources';
import { loadStyle } from 'lightning/platformResourceLoader';
import { success,error,warn } from 'c/notificationSvc';
import { labels } from 'c/mroLabels';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";

export default class MroValidatableDocumentCheck extends LightningElement {
    @api documentType;
    @api fileMetadataId = '';
    @api sectionOpened = false;
    @api disableCheckButton = false;
    @track _isValidated = false;
    @track isOpen = false;
    @track idValidatableDoc;
    @track showConfirmationDialog = false;
    @track confirmationMessage;
    @track showSpinner = false;
    labels = labels;

    renderedCallback() {
        Promise.all([
            loadStyle(this, EAppStyle + '/style.css'),
        ])
            .then(() => {
            })
            .catch(errno => {
                error(this, errno.body.message);
            });

        if (this.isValidated) {
            this.activeSectionCheckboxes().forEach((checkbox) => {
                checkbox.checked = true;
                checkbox.classList.remove('slds-has-error');
            });
        }
    }

    get validatableDocId() {
        return this.documentType.validatableDocument;
    }

    get isValidated() {
        return this.documentType.isValidated || this._isValidated;
    }

    get checkButtonDisabled() {
        return this.disableCheckButton || this.sectionOpened;
    }

    get archivedBadgeClass() {
        return this.documentType.isArchivedOnDisCo ? 'slds-badge slds-badge_lightest' : 'slds-badge'
    }

    get archivedBadgeLabel() {
        return this.documentType.isArchivedOnDisCo ? labels.archivedOnDisCo : labels.archiveOnDisCo
    }

    /**
     * 
     * Handles click on the Check button for checking document type
     * 
     * */
    handleCheckClick() {
        let section = this.template.querySelector('.section-document');
        if (!this.documentType.documentItems) {
            this.handleSubmit();
            return;
        }
        if (section) {
            section.classList.remove('slds-hide');
            section.scrollIntoView({ behavior: "smooth"});
            this.isOpen = this.sectionOpened = !this.isOpen;
            this.dispatchEvent(new CustomEvent("changesection", {
                detail: {
                    isOpen: this.isOpen
                }
            }));
        }

    }

    handleCancelClick() {
        let section = this.template.querySelector('.section-document');
        section.classList.add('slds-hide');
        this.isOpen = this.sectionOpened = false;
        this.hideErrPanel();
        this.dispatchEvent(new CustomEvent("changesection", {
            detail: {
                isOpen: this.isOpen,
                isValidated: this._isValidated,
                archiveOnDisCo: this.documentType.archiveOnDisCo
            }
        }));
    }

    handleValidateClick() {
        let showErrPanel = !this.validateFields();
        this.activeSectionCheckboxes().forEach((checkbox) => {
            if (!checkbox.checked) {
                checkbox.classList.add('slds-has-error');
                showErrPanel = true;
            }
        });

        if (showErrPanel) {
            this.template.querySelector('.error-panel').classList.replace('slds-hide', 'slds-show');
        } else {
            if (this.documentType.documentBundleItem) {
                this.documentBundleItem = this.documentType.documentBundleItem;
                this.handleSubmit(false);
            }
        }
    }

    handleConfirm() {
        this.showConfirmationDialog = false;
        if (this.documentType.documentBundleItem) {
            this.documentBundleItem = this.documentType.documentBundleItem;
            this.handleSubmit(true);
        }
    }
    handleCancelConfirmation() {
        this.showConfirmationDialog = false;
    }

    getElement(self, selector) {
        return new Promise(function (resolve) {
            const element = self.querySelector(selector);
            if (element) {
                resolve(element);
            }
        });
    }

    handleSubmit(confirmed) {
        /*
        const selfTemplate = this.template;
        const self = this;
        this.getElement(selfTemplate, '[data-id="FileMetadata"]').then(function (inputElement) {
            inputElement.value = self.fileMetadataId;
            self.template.querySelector('lightning-record-edit-form').submit();
        }, self);
         */

        this.showSpinner = true;

        let inputs = {
            'validatableDocument': JSON.stringify(this.getValidatableDocument()),
            'fileMetadataId': this.fileMetadataId,
            'documentItemsDTO': JSON.stringify(this.documentType.documentItems),
            'confirmed': confirmed ? confirmed.toString() : 'false'
        };
        notCacheableCall({
            className: "MRO_LC_ValidatableDocumentsCnt",
            methodName: "validateDocument",
            input: inputs
        })
            .then(response => {
                this.showSpinner = false;
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                        return;
                    }
                    else {
                        if (response.data.result === 'OK') {
                            this.handleSuccess();
                            this.handleCancelClick();
                        }
                        else if (response.data.result === 'WARNING') {
                            console.log('warning');
                            this.showConfirmationDialog = true;
                            this.confirmationMessage = response.data.message;
                        }
                        else if (response.data.result === 'ERROR') {
                            error(this, response.data.message);
                        }
                    }
                }
            })
            .catch(errorMsg => {
                this.showSpinner = false;
                error(this, errorMsg);
            });
    }

    handleSuccess(){
        this._isValidated = true;
        success(this, labels.documentValidated);
    }
    handleError(event) {
        error(this, event.detail.message);
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

    removeError(event) {
        event.target.classList.remove('slds-has-error');
        this.hideErrPanel();
    }
    
    hideErrPanel = () =>{
        let errPanel = this.template.querySelector('.error-panel');
        if (errPanel.classList.contains('slds-show')) {
            errPanel.classList.replace('slds-show', 'slds-hide');
        }
    };

    activeSectionCheckboxes = () => {
        return Array.from(this.template.querySelectorAll('[data-id="allCheckboxes"]'));
    };

    getValidatableDocument() {
        let validatableDocument = {
            'Id' : this.documentType.validatableDocument
        };
        this.template.querySelectorAll('lightning-record-edit-form lightning-input-field').forEach(inputField => {
            validatableDocument[inputField.fieldName] = inputField.value;
        });
        return validatableDocument;
    }
}