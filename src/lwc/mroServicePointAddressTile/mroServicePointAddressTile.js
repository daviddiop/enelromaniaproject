import {api, LightningElement, track} from "lwc";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {error} from "c/notificationSvc";
import {labels} from "c/labels";

export default class MroServicePointAddressTile extends LightningElement {
    RECORD_TYPE_SERVICE_POINT_ADDRESS = ["ServicePointAddressChange"];

    labels = labels;
    @api
    get caseId() {
        return this._caseId;
    }
    set caseId(value) {
        this._caseId = value;
    }

    @api
    get caseList() {
        return this._caseList;
    }
    set caseList(value) {
        this._caseList = value;
    }
    @api accountId;
    @api isServicePointWizard;

    @api hideEditIcon = false;
    @api isRow = false;
    @api disabled = false;
    @api selectCaseFields = [];
    @api selectCaseTileFields = [];
    @api readOnlyAddress = false;
    @api addressForced = false;
    @api recordTypeCase;
    @api tileTitle;
    @track defaultTileTitle;

    @track caseFields = [];
    @track openConfirmModal;
    @track openEditModal = false;
    @track listRecordType = [];
    @track isServicePointAddressChange;
    @track isNormalize;

    connectedCallback() {
        this.defaultTileTitle = this.tileTitle ? this.tileTitle : "Service Point Address Edit";
        let inputs = {recordId: this.caseId};
        notCacheableCall({
            className: "MRO_LC_Supply",
            methodName: "getDeveloperName",
            input: inputs
        })
            .then(response => {
                let recordType = response.data.listRecordType[0].RecordType.DeveloperName;
                this.isServicePointAddressChange = this.RECORD_TYPE_SERVICE_POINT_ADDRESS.includes(recordType);

                if ((!this.isRow) && (this.selectCaseTileFields.length > 0)) {
                    this.selectCaseTileFields = this.selectCaseTileFields.filter(function (el) {
                        return el !== "Supply__c";
                    });
                }

                this.caseFields = this.selectCaseFields;
                this.error = undefined;
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    closeModal() {
        this.openEditModal = false;
        this.openConfirmModal = false;
    }

    deleteCaseFromTile() {
        let inputs = {recordId: this.caseId};
        notCacheableCall({
            className: "MRO_LC_Supply",
            methodName: "deleteCase",
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }
                this.openConfirmModal = false;
                this.openEditModal = false;
                const deleteEvent = new CustomEvent("delete", {
                    detail: {
                        deleteRecord: this.caseId
                    }
                });
                this.dispatchEvent(deleteEvent);
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    async editCaseIcon() {
        this.openEditModal = true;
    }

    deleteCaseIcon() {
        this.openConfirmModal = true;
    }

    handleClose(event) {
        this.openEditModal = event.target.closeModal;
    }

    get addClassHideEditIcon() {
        if (this.hideEditIcon) {
            return "slds-hide";
        }
        return "";
    }

    get cardClass() {
        if (this.disabled) {
            return "slds-class slds-theme_shade";
        }
        return "slds-class";
    }
}