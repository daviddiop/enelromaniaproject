import {LightningElement, api, track} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import {error, success, warn} from 'c/notificationSvc';
import fieldStyle from '@salesforce/resourceUrl/EnergyAppResources';
import {loadStyle} from 'lightning/platformResourceLoader';
import {labels} from 'c/mroLabels';
import getAvailableAddresses from '@salesforce/apex/MRO_SRV_Address.getAvailableAddresses';
import getAvailableAddressesCountryRestricted from '@salesforce/apex/MRO_SRV_Address.getAvailableAddressesCountryRestricted';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import hasRetroactiveSwitchOutPermission from '@salesforce/customPermission/MRO_RetroactiveSwitchOut';

export default class MroCaseEdit extends NavigationMixin(LightningElement) {
    // Added for compatibility with previous version - deprecated
    @api hideCommodityButton;
    //--

    @api caseId;
    @api isCaseModalByAddressForm = false;
    @api isConnectionWizard = false;
    @api connectionType = "";

    @api
    get caseList() {
        return this._caseList;
    }

    set caseList(value) {
        this._caseList = value;
    }

    @api dossierId;
    @api accountId;
    @api supplyId;
    @api supplyType;
    @api companyId;
    @api showHeader;
    @api closeModal = false;
    @api selectCaseFields = [];
    @api selectCaseFieldsValues;
    @api readOnlyAddress = false;
    @api addressForced = false;
    @api recordTypeCase;
    @api caseAddress;
    @api hideCommodity = false;
    @api title;
    @api isCloneCase = false;
    @api isEnelDistributor = false;
    @track defaultTitle;

    @track availableAddresses;
    @track isNormalize;
    @api readOnlyFields = false;
    @api caseEditTitle;

    @track caseFields;
    @track showLoadingSpinner = true;
    @track recordTypePicklist = [];
    @track selectedOption;
    /*add by david*/
    @api nonDisconnectable;
    @api isNonDisconnectableWizard = false;
    @api recordTypeId;
    @api originSelected;
    @api channelSelected;
    @api isMeterChange = false;
    @api isSwitchOut = false;
    @api isPowerVoltage = false;
    @api isPowerVoltageWizard = false;
    @api isExistingValues = false;
    @api defaultEffDate;
    /*end add by david*/
    defaultCaseOrigin;
    defaultCaseStatus;
    @api distributorId;
    labels = labels;
    /*add by Insa*/
    @api isTerminationWizard = false;
    @api isAuthorityCompensation = false;
    @api maxDate;
    @api disconnectionDate;
    @api terminationCause;
    @api terminationPicklistValues;
    @track reasonList = [];
    @track effectiveDate;
    @track caseTile = [];
    @track reason;
    @track isDefaultCaseEdit = true;
    @track isValidDate = false;
    @track hasRendered = false;
    @track spinnerStatus = false;

    selectedCancellationReason = "";
    @api listSupplies;
    @api caseTilesEdit = [];

    /*add by INSA*/

    /* added by Giuseppe Mario Pastore 31-01-2020*/
    @api isDecoReco = false;
    /* added by Giuseppe Mario Pastore 31-01-2020*/


    // Start add by Bouba
    @track fieldChanged = false;
    _caseList;
    // End add by Bouba

    /* added by Mocanu Vlad Octavian 01.04.2020 */
    @track disableAuthorityFields = true;
    @track compensationType;
    @track compensationCode;
    @track authorityReason;
    @track parentCaseId;
    @track showParentCaseId;
    @track manualChangeCompensationType = false;
    isNotSavedCase = true;
    /* End added by Mocanu Vlad Octavian 01.04.2020 */
    connectedCallback() {
        Promise.all([
            loadStyle(this, fieldStyle + '/style.css'),
        ])
            .then(() => {
            })
            .catch(err => {
                error(this, err.body.message);
            });

        if (this.selectCaseFieldsValues && Object.keys(this.selectCaseFieldsValues).length) {
            this.caseFields = Object.keys(this.selectCaseFieldsValues);
        }else if (this.selectCaseFields.length) {
            this.caseFields = this.selectCaseFields;
        }

        getAvailableAddressesCountryRestricted({
            accountId: this.accountId,
            restrictToCountry: "ROMANIA"
        }).then((response) => {
            this.availableAddresses = response;
            console.log('response'+JSON.stringify(response));
            if(this.isConnectionWizard && this.isCaseModalByAddressForm && !this.caseId){
                let defaultAddress = {};
                defaultAddress = {
                    'streetNumber': null,
                    'streetNumberExtn': null,
                    'streetName': null,
                    'streetType': 'STRADA',
                    'apartment': null,
                    'building': null,
                    'city': null,
                    'country': 'ROMANIA',
                    'floor': null,
                    'locality': null,
                    'postalCode': null,
                    'province': null
                };
                this.caseAddress = defaultAddress;
            }
        }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            this.spinner = false;
        });
        if (!this.caseId && this.isCaseModalByAddressForm && this.isConnectionWizard) {
            this.getConnectionAddress();
        }
        //add by david
        if(this.isNonDisconnectableWizard && this.caseId){
            this.getCaseRecord();
        }
        // add by INSA

        if(this.isPowerVoltage){
            let myCaseFields = [];
            for (let mapDataFields in this.caseFields) {
                if (this.caseFields[mapDataFields] !== 'Voltage__c') {
                    myCaseFields.push(this.caseFields[mapDataFields]);
                }
            }
            this.caseFields = myCaseFields;
        }
        if(this.isExistingValues){
            let myCaseFields = [];
            for (let mapDataFields in this.caseFields) {
                if (this.caseFields[mapDataFields] !== 'VoltageSetting__c') {
                    myCaseFields.push(this.caseFields[mapDataFields]);
                }
            }
            this.caseFields = myCaseFields;
        }

        if (this.isTerminationWizard || this.isNonDisconnectableWizard || this.isMeterChange || this.isSwitchOut || (this.isAuthorityCompensation && this.selectCaseFields.length === 0)) {
            if (this.terminationPicklistValues) {
                /*
                for (let i = 0; i < this.terminationPicklistValues.length; i++) {
                    this.reasonList = [...this.reasonList, {
                        label: this.terminationPicklistValues[i],
                        value: this.terminationPicklistValues[i]
                    }]
                }
                */
                this.reasonList = this.reasonList.concat(this.terminationPicklistValues);
            }
            this.isDefaultCaseEdit = false;
            if (this.disconnectionDate) {
                this.effectiveDate = this.disconnectionDate;
            }
        }
        if (this.isTerminationWizard && this.caseId) {
            //alert(JSON.stringify(this.terminationPicklistValues));
            this.getCaseRecordForUpdatingTermination();
            console.log('***** ' + this.caseFields);
        }
        if (this._caseList) {
            this.caseTile = [...this._caseList];
        }
        this.getConstants();

        if (this.isCaseModalByAddressForm) {
            this.getRecordTypeConnection();
        }

        this.defaultTitle = this.title ?  this.title : this.labels.createCase;
    }

    get readOnlyFieldsAddress() {
        if(this.isCaseModalByAddressForm){
            return "['country']";
        } else {
            return "[]";
        }
    }

    renderedCallback() {
        if ( this.isNotSavedCase && this.selectCaseFieldsValues && this.selectCaseFieldsValues['Voltage__c'] && Object.keys(this.selectCaseFieldsValues).length) {
            let voltage = (this.selectCaseFieldsValues['Voltage__c'] * 1000).toString();
            this.changeElementFieldValue('VoltageSetting__c', voltage);
        }

        if(this.isConnectionWizard)
        {
            if(this.connectionType !== "")
            {
                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    if(element.fieldName === "SubProcess__c")
                    {
                        element.value = this.connectionType;
                        element.disabled = true;
                    }
                });
           }else {
                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    if(element.fieldName === "SubProcess__c")
                    {
                        element.disabled = true;
                    }
                })
            }
        }
    }

    getConstants() {
        notCacheableCall({
            className: "Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.defaultCaseOrigin = apexResponse.data.CASE_DEFAULT_CASE_ORIGIN;
                        this.defaultCaseStatus = apexResponse.data.CASE_STATUS_DRAFT;
                    }
                }
            });
    }

    getCaseRecordForUpdatingTermination(){
         this.showLoadingSpinner = true;
         let inputs = {recordId: this.caseId};
         notCacheableCall({
             className: "MRO_LC_TerminationWizard",
             methodName: "getCase",
             input: inputs
         })
             .then(apexResponse => {
                 if (apexResponse) {
                     if (apexResponse.isError) {
                         error(this, apexResponse.error);
                     } else {
                         this.effectiveDate = apexResponse.data.caseRecord.EffectiveDate__c;
                         this.reason = apexResponse.data.caseRecord.Reason__c;
                         this.template.querySelector("lightning-combobox").value = this.reason;
                         this.template.querySelector("lightning-input").value =this.effectiveDate;
                         console.log('this.effectiveDate'+this.effectiveDate);
                         console.log('this.reason'+this.reason);
                     }
                 }
             });
         this.showLoadingSpinner = false;
    }

    handleSelectReason(event) {
        this.terminationCause = event.detail.value;
    }

    getCaseRecord(){
        this.showLoadingSpinner = true;
        let inputs = {recordId: this.caseId};
        notCacheableCall({
            className: "MRO_LC_NonDisconnectablePOD",
            methodName: "getCase",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.nonDisconnectable = apexResponse.data.caseRecord.NonDisconnectable__c;
                        this.effectiveDate = apexResponse.data.caseRecord.EffectiveDate__c;
                        this.reason = apexResponse.data.caseRecord.Reason__c;
                        console.log('this.effectiveDate'+this.effectiveDate);
                        console.log('this.reason'+this.reason);
                        console.log('this.nonDisconnectable   '+JSON.stringify(apexResponse.data.caseRecord));
                    }
                }
            });
        this.showLoadingSpinner = false;
    }

    getRecordTypeConnection() {
        notCacheableCall({
            className: "CaseCnt",
            methodName: "getCaseDescribe"
        })
            .then(apexResponse => {
                if (apexResponse.isError) {
                    error(this, JSON.stringify(apexResponse.error));
                }
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.recordTypePicklist = apexResponse.data.recordTypePicklistValues;
                        if(this.isConnectionWizard){
                            for(let i = 0 ; i < this.recordTypePicklist.length; i++) {
                                if(this.recordTypePicklist[i].label == "Electric") {
                                    this.selectedOption = this.recordTypePicklist[i].value;
                                }
                            }
                        }
                    }
                }
            });
    }

    getConstants() {
        notCacheableCall({
            className: "Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.defaultCaseOrigin = apexResponse.data.CASE_DEFAULT_CASE_ORIGIN;
                        this.defaultCaseStatus = apexResponse.data.CASE_STATUS_DRAFT;
                    }
                }
            });
    }

    handleLoad() {
        if (this.selectCaseFieldsValues) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (!element.value){
                    element.value = this.selectCaseFieldsValues[element.fieldName];
                }
                if (this.readOnlyFields){
                    this.readOnlyField(element.fieldName);
                }
                /*added by Giuseppe Mario Pastore 11-March-2020*/
                if (this.isDecoReco) {
                    if (element.fieldName === 'Description') {
                        console.log('Element classlist for Description field: ' + element.classList);
                        element.classList.remove('wr-required');
                        // element.classList.remove('slds-has-error');
                    }
                }
                /*added by Giuseppe Mario Pastore 11-March-2020*/
            });
        }
        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            /*added by Giuseppe Mario Pastore 11-March-2020*/
            if (this.isDecoReco) {
                if (element.fieldName === 'Description') {
                    console.log('Element classlist for Description field: ' + element.classList);
                    element.classList.remove('wr-required');
                    // element.classList.remove('slds-has-error');
                }
            } else if (this.isAuthorityCompensation) {
                if (element.fieldName === 'CompensationType__c') {
                    this.compensationType = element.value;
                    if (this.supplyType === 'Gas') {
                        element.value = 'Gaz';
                        this.compensationType = 'Gaz';
                        element.onchange = this.showHideAuthorityFields(this.compensationType);
                    }

                    if (this.compensationType) {
                        this.template.querySelectorAll('lightning-input-field').forEach(ele => {
                            if (ele.fieldName === 'CompensationCode__c') {
                                ele.disabled = false;
                            }
                        });
                        this.showParentCaseIdInput(this.compensationType);
                    } else {
                        this.template.querySelectorAll('lightning-input-field').forEach(ele => {
                            if (ele.fieldName === 'CompensationCode__c') {
                                ele.disabled = true;
                            }
                        });
                    }
                } else if (element.fieldName === 'Reason__c') {
                    element.disabled = true;
                    element.classList.remove('wr-required');
                    element.required = false;
                } else if (element.fieldName === 'StartDate__c') {
                    let eleValue = element.value;
                    if (eleValue === null) {
                        element.value = new Date().toISOString();
                    }
                }
            }
            /*added by Giuseppe Mario Pastore 11-March-2020*/
            fields[element.fieldName] = element.value;
        });

        if (fields.EstimatedConsumption__c){
            this.readOnlyField('EstimatedConsumption__c');
        }
        this.readOnlyField('ConsumptionCategory__c');
        this.isOptionalField('ConsumptionCategory__c');
        this.isOptionalField('FirePlacesCount__c');
        this.isOptionalField('FlowRate__c');

        if(!this.spinnerStatus){
            console.log('showLoadingSpinner1');
            this.showLoadingSpinner = false;
        }
    }

    handleSubmit(event) {
        this.showLoadingSpinner = true;
        event.preventDefault();
        event.stopPropagation();
        this.showLoadingSpinner = true;
        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });

        if (this.isCaseModalByAddressForm) {
            this.showLoadingSpinner = true;

            let addrForm = this.template.querySelector('c-mro-address-form');
            if (!addrForm.isValid()) {
                error(this, this.labels.addressMustBeVerified);
                this.showLoadingSpinner = false;
                return;
            }

            let addrFields = addrForm.getValues();
            for (let f in addrFields) {
                if (addrFields.hasOwnProperty(f)) {
                    fields[f] = addrFields[f];
                }
            }

            console.log('addressForced', this.addressForced);
            fields.AddressAddressNormalized__c = (this.addressForced !== true);
            console.log('addressNormalized', fields.AddressAddressNormalized__c);
        }
        if (this.accountId) {
            fields.AccountId = this.accountId;
        }

        /* start add by david */
        if(this.isNonDisconnectableWizard){
            if(this.nonDisconnectable && fields.Reason__c === "" ){
                this.querySelectorAll('lightning-input-field').forEach(element => {
                    if(element.required){
                        element.classList.add('slds-has-error');
                    }
                });
                this.showLoadingSpinner = false;
                return;
            }
        }

        if (this.nonDisconnectable === false) {
            fields.Reason__c = "";
        }
        fields.NonDisconnectable__c = this.nonDisconnectable;
        /* end add by david */

        //Added by Bouba
        if (parseFloat(fields.ContractualPower__c) > 11) {
           if(fields.PowerPhase__c && fields.PowerPhase__c == 'Single'){
           this.addError('ContractualPower__c');
           error(this, this.labels.ContractualPowerValidation);
           this.showLoadingSpinner = false;
           return;
           }
        }
        if (parseFloat(fields.AvailablePower__c) < parseFloat(fields.ContractualPower__c)){
            this.addError('ContractualPower__c');
            this.addError('AvailablePower__c');
            error(this, this.labels.ContractualAvailablePowerValidation);
            this.showLoadingSpinner = false;
            return;
        }
        if (this.selectCaseFieldsValues) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if(!isNaN(parseFloat(element.value))){
                    if(parseFloat(element.value)  !== parseFloat(this.selectCaseFieldsValues[element.fieldName])){
                        this.fieldChanged = true;
                    }
                }else {
                    if(element.value  !== this.selectCaseFieldsValues[element.fieldName]){
                        this.fieldChanged = true;
                    }
                }
            });
            if (!this.fieldChanged){
                error(this, this.labels.FieldChangeMessage);
                this.showLoadingSpinner = false;
                return;
            }
        }

        // End add by Bouba
        if (this.accountId) {
            fields.AccountId  = this.accountId;
        }
        if (this.dossierId) {
            fields.Dossier__c = this.dossierId;
        }
        if (this.supplyId) {
            fields.Supply__c = this.supplyId;
        }
        if (this.companyId){
            fields.CompanyDivision__c = this.companyId;
        }
        if (this.recordTypeCase) {
            fields.RecordTypeId = this.recordTypeCase;
        }
        // fields related to the OriginChannelSelection
        if (this.channelSelected) {
            fields.Channel__c = this.channelSelected;
        }
        if (this.originSelected) {
            fields.Origin = this.originSelected;
        } else {
            fields.Origin = this.defaultCaseOrigin;
        }

        if (this.isCaseModalByAddressForm) {
            fields.RecordTypeId = this.selectedOption;
        } else {
            if (this.recordTypeCase) {
                fields.RecordTypeId = this.recordTypeCase;
            }
        }

        fields.Status = this.defaultCaseStatus;

        if(this.isConnectionWizard)
        {
            if(fields.VoltageSetting__c) {
                fields.Voltage__c = fields.VoltageSetting__c / 1000;
            }
        }

        if(this.isCloneCase) {
            this.caseId = '';
            this.template.querySelector('lightning-record-edit-form').recordId = this.caseId;
        }

        if (this.isPowerVoltage) {
            fields.Distributor__c = this.distributorId;
            fields.EffectiveDate__c = this.defaultEffDate;
            fields.CustomerConfirmationDate__c = this.defaultEffDate;
            if (this.selectCaseFieldsValues) {
                if (fields.AvailablePower__c === this.selectCaseFieldsValues['AvailablePower__c'] &&
                    fields.PowerPhase__c === this.selectCaseFieldsValues['PowerPhase__c'] &&
                    fields.VoltageLevel__c === this.selectCaseFieldsValues['VoltageLevel__c'] &&
                    fields.ContractualPower__c === this.selectCaseFieldsValues['ContractualPower__c']
                ) {
                    error(this, this.labels.noParameterChanged);
                    return;
                }
            }
            if(fields.VoltageSetting__c) {
                            fields.Voltage__c = fields.VoltageSetting__c / 1000;
            }
            fields.CustomerConfirmationDate__c = this.defaultEffDate;
        }
        /*
        if (this.isMeterChange){
            fields.IsDisCoENEL__c = this.isEnelDistributor;
        }
        */

        /* start add by  INSA*/
        if (this.isTerminationWizard) {
            let inpDate = this.template.querySelector("lightning-input");
            fields.EffectiveDate__c = inpDate.value;
            let inpReason = this.template.querySelector("lightning-combobox");
            fields.Reason__c = inpReason.value;
            let effectiveDate = new Date(this.template.querySelector('[data-id="effectiveDate"]').value);
            let defaultDate = new Date(this.maxDate);
            if (effectiveDate && effectiveDate < defaultDate) {
                error(this, this.labels.invalidDate);
                this.showLoadingSpinner = false;
                return;
            }
            if (this._caseList.length !== 0) {
                if (((this._caseList.length >= 1 && !this.caseId) || (this._caseList.length > 1 && this.caseId))
                    && this._caseList[0].Reason__c !== fields.Reason__c ||
                    this._caseList[0].EffectiveDate__c !== fields.EffectiveDate__c) {
                    warn(this, this.labels.incorrectDataToSaveWizard);
                    this.showLoadingSpinner = false;
                    return;
                }
            }
            this.spinnerStatus = true;
            this.updateCaseTermination(fields);
        }
        else if(this.isSwitchOut){
            if (fields.Reason__c === 'Trader change') {
                let numberOfDay = this.getDayDateDiff(fields.EffectiveDate__c, fields.ReferenceDate__c);
                if (!isNaN(numberOfDay) && numberOfDay < 21) {
                    warn(this, this.labels.switchOutDate21);
                    return;
                }
            }
            if (!hasRetroactiveSwitchOutPermission && fields.EffectiveDate__c < this.defaultEffDate) {
                error(this, this.labels.switchOutDate5days);
                return;
            }
            if (this.validateFields()) {
                if (this._caseList.length > 1) {
                    let casePivot = this._caseList[0];
                    if (casePivot) {
                        if ((casePivot.Trader__c !== fields.Trader__c) ||
                            (casePivot.EffectiveDate__c !== fields.effectiveDate) ||
                            (casePivot.Reason__c !== fields.reason) ||
                            (casePivot.ReferenceDate__c !== fields.referenceDate)) {
                            error(this, this.labels.caseSwitchOutData);
                            this.addClosingEvent();
                            return;
                        } else {
                            this.updateCaseSwitchOut(fields);
                        }
                    }
                }
                this.updateCaseSwitchOut(fields);
                //this.template.querySelector('lightning-record-edit-form').submit(fields);
            }

        } else {
            //Added by Bouba
            if (this.validateFields()) {
                this.showLoadingSpinner = true;
                this.template.querySelector('lightning-record-edit-form').submit(fields);
                this.spinnerStatus = true;
                this.isNotSavedCase = false;
            } else {
                error(this, this.labels.requiredFields);
                this.isNotSavedCase = false;
                return;
            }
            //End add by Bouba

        }
        /* end add by  INSA*/
    }

    updateCaseSwitchOut(fields) {
        this.showLoadingSpinner = true;
        let inputs = { checkEffectiveDate: JSON.stringify(fields.EffectiveDate__c) };
        notCacheableCall({
            className: "MRO_UTL_Date",
            methodName: "getValidDate",
            input: inputs
        })
          .then(apexResponse => {
              if (apexResponse) {
                  if (apexResponse.isError) {
                      error(this, apexResponse.error);
                  } else {
                      this.showLoadingSpinner = false;
                      if (apexResponse.data.isInvalid) {
                          error(this, this.labels.notWorkingDay);
                          return;
                      } else {
                          this.template.querySelector("lightning-record-edit-form").submit(fields);
                      }
                  }
              }
          });
    }

    createCaseTermination(fields) {
        this.showLoadingSpinner = true;
        fields.listSupplies = this.listSupplies;
        console.log('channel ****** ' + JSON.stringify(fields));
        let inputs = {
            caseTile: JSON.stringify(this._caseList),
            channelSelected: fields.Channel__c,
            originSelected: fields.Origin,
            supplies: JSON.stringify(this.listSupplies),
            caseId: this.caseId,
            dossierId: this.dossierId,
            accountId: this.accountId,
            reason: fields.Reason__c,
            effectiveDate: fields.EffectiveDate__c
        };
        notCacheableCall({
            className: "MRO_LC_TerminationWizard",
            methodName: "createCase",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        console.log('**********save caase ' + JSON.stringify(apexResponse));
                        this.caseTile = apexResponse.data.caseTile;
                        this.handleSuccess();
                    }
                }
            })
            .catch(error => {
                error(this, JSON.stringify(error));
            });

    }

    updateCaseTermination(fields) {
        this.spinnerStatus = true;
        this.showLoadingSpinner = true;
        let inputs = {checkEffectiveDate: JSON.stringify(fields.EffectiveDate__c)};
        notCacheableCall({
            className: "MRO_UTL_Date",
            methodName: "getValidDate",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        if (apexResponse.data.isInvalid) {
                            error(this, this.labels.notWorkingDay);
                            this.showLoadingSpinner = false;
                            return;
                        } else {
                            if (this.caseId) {
                               this.spinnerStatus = true;
                                this.template.querySelector('lightning-record-edit-form').submit(fields);
                            } else {
                                this.spinnerStatus = true;
                                this.createCaseTermination(fields);
                            }
                        }
                    }
                }
            });
    }

    handleSuccess(event) {

        this.showLoadingSpinner = false;
        console.log('showLoadingSpinner2');
        success(this, 'Case successfully updated');
        let caseTile = JSON.parse(JSON.stringify(this.caseTile));
        if (event) {
            event.preventDefault();
            event.stopPropagation();
            let payload = JSON.parse(JSON.stringify(event.detail));
            let fields = payload.fields;
            caseTile.forEach((caseRecord, index) => {
                if (caseRecord.Id === payload.id) {
                    Object.keys(caseRecord).forEach(fieldName => {
                        if (fields.hasOwnProperty(fieldName)) {
                            caseRecord[fieldName] = fields[fieldName].value;
                        }
                    });
                    caseTile[index] = caseRecord;
                }
            });
        }
        console.log('** Case Tile after update ', caseTile);
        const successEvent = new CustomEvent('casesuccess', {
            detail: {
                caseTile : caseTile
            }
        });
        this.dispatchEvent(successEvent);
        this.addClosingEvent();
    }

    handleCancel() {
        this.addClosingEvent();
    }

    handleError(event) {
        let message = event.detail.detail ? event.detail.detail : event.detail.message;
        error(this, message);
        //error(this, JSON.stringify(event.detail.detail));
        this.showLoadingSpinner = false;
    }

    handleDefaultAuthorityCompensationFields(event) {
        let inputField = event.target;

        if (inputField.fieldName === 'CompensationType__c') {
            if (inputField.value) {
                this.compensationType = inputField.value;
                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    if (element.fieldName === 'CompensationCode__c') {
                        element.disabled = false;
                    }
                });
            } else {
                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    if (element.fieldName === 'CompensationCode__c') {
                        element.disabled = true;
                    }
                });
            }
        } else if (inputField.fieldName === 'CompensationCode__c') {
            inputField.onchange = this.handleCompensationCodeChange(event);
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (element.fieldName === 'Reason__c') {
                    element.value = this.authorityReason;
                }
            });
        }

        this.removeError(event);
    }

    handleCompensationTypeChange(event) {
        this.compensationType = event.target.value;
        if (this.isAuthorityCompensation) {
            this.showHideAuthorityFields(this.compensationType);
        }
    }

    showHideAuthorityFields(compensationType) {
            if (compensationType) {
                this.disableAuthorityFields = false;
                this.showParentCaseIdInput(this.compensationType);
            } else {
                this.disableAuthorityFields = true
            }
    }

    handleCompensationCodeChange(event) {
        this.compensationCode = event.target.value;

        if (this.isAuthorityCompensation) {
            switch (this.compensationType) {
                case 'Distributie':
                    switch (true) {
                        case this.compensationCode.includes('CC01'):
                            this.authorityReason = 'Conform Ord. ANRE nr. 11/2016, Anexa  4, poz. 1';
                            break;
                        case this.compensationCode.includes('CC02'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 2';
                            break;
                        case this.compensationCode.includes('CC03'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 3';
                            break;
                        case this.compensationCode.includes('CC04'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 4';
                            break;
                        case this.compensationCode.includes('CC05'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 5';
                            break;
                        case this.compensationCode.includes('CC06'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 6';
                            break;
                        case this.compensationCode.includes('CC07'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 7';
                            break;
                        case this.compensationCode.includes('CC08'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 8';
                            break;
                        case this.compensationCode.includes('CC09'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 9';
                            break;
                        case this.compensationCode.includes('CC10'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 10';
                            break;
                        case this.compensationCode.includes('CC11'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 11';
                            break;
                        case this.compensationCode.includes('CC12'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 12';
                            break;
                        case this.compensationCode.includes('CC13'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 13';
                            break;
                        case this.compensationCode.includes('CC14'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 14';
                            break;
                        case this.compensationCode.includes('CC15'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa 1, poz. 2';
                            break;
                        case this.compensationCode.includes('CC16'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa 1, poz. 1';
                            break;
                        case this.compensationCode.includes('CC17'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa 1, poz. 3-4';
                            break;
                        case this.compensationCode.includes('CC18'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa 1, poz. 5';
                            break;
                        case this.compensationCode.includes('CC19'):
                            this.authorityReason = 'Conform  Ord. ANRE nr. 11/2016, Anexa  4, poz. 15';
                            break;
                        case this.compensationCode === '':
                            this.authorityReason = '';
                            break;
                    }
                    break;
                case 'Furnizare':
                    this.authorityReason = '';
                    break;
                case 'Gaz':
                    this.authorityReason = '';
                    break;
            }
        }
    }

    handleParentCaseIdChange(event) {
        this.parentCaseId = event.target.value;
        if (this.parentCaseId) {
            this.setCaseDatesFromParent();
        } else {
            this.setCaseDates();
        }
    }

    setCaseDatesFromParent() {
        notCacheableCall({
            className: "MRO_LC_ANRECompensation",
            methodName: "SetCaseDatesFromParentCase",
            input: {
                "parentCaseId": this.parentCaseId
            }
        }).then(apexResponse => {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (!apexResponse.data.error) {
                    if (element.fieldName === 'ReferenceDate__c') {
                        let createdDate = apexResponse.data.createdDate;
                        if (createdDate) {
                            element.value = createdDate;
                        }
                    } else if (element.fieldName === 'StartDate__c') {
                        element.value = apexResponse.data.findingDate;
                    }
                }
            });
        });
    }

    setCaseDates() {
        let today = new Date().toISOString();

        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.fieldName === 'ReferenceDate__c') {
                element.value = today;
            } else if (element.fieldName === 'StartDate__c') {
                element.value = today;
            }
        })
    }

    removeError(event) {
        let inputCmp = event.target;
        /*Added by Giuseppe Mario Pastore on 11-March-2020*/
        //let fieldList = [];
        console.log('this.recordTypeCase *********> ' + this.recordTypeCase);
        if (inputCmp.fieldName === 'Type' && inputCmp.value === 'Disconnection') {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {

                if (element.fieldName === 'Duration__c') {
                    console.log('element.fieldName = Duration__c ****> ' + element.fieldName);
                    console.log(element.classList);
                    element.classList.remove('slds-has-error');
                    if (!element.classList.contains('slds-hide')) {
                        element.classList.add('slds-hide');
                    }
                    element.value = null;
                }

            });
        } else if (inputCmp.fieldName === 'Type' && inputCmp.value === 'Reconnection') {
            /*this.template.querySelectorAll('lightning-input-field').forEach(element =>{
                fieldList.push(element.fieldName);
            });*/
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (element.fieldName === 'Duration__c') {
                    console.log('Duration__c has slds-hide style attribute');
                    element.classList = element.classList.contains('slds-hide') ? element.classList.remove('slds-hide') : element.classList;
                    element.classList.add('slds-has-error');
                }

            });
        }
        /*Added by Giuseppe Mario Pastore on 11-March-2020*/
        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
        if(this.isPowerVoltage){
            this.updateVoltageSetting(inputCmp);
            this.setSinglePhaseValue(inputCmp);
        }
    }
    /*add by david*/
    handleNonDisconnectable(event){
        this.nonDisconnectable = false;
        let inputCmp = event.target.checked;
        if(inputCmp){
            this.nonDisconnectable = true;
        }
    }
    /*add by david*/

    /**
     * @author Boubacar Sow
     * @description set the voltage setting value by voltage level.
     *              [ENLCRO-577] Power/Voltage Change - Customer feedback to be corrected
     * @param fieldName
     */

    updateVoltageSetting(inputField) {
        if (inputField.fieldName === 'VoltageLevel__c' && !this.isConnectionWizard){
            if (inputField.value === 'LV'){
                this.changeElementFieldValue('VoltageSetting__c', '230');
            }
            if (inputField.value === 'MV'){
                this.changeElementFieldValue('VoltageSetting__c', '6000');
            }
            if (inputField.value === 'HV'){
                this.changeElementFieldValue('VoltageSetting__c', '110000');
            }
        }
    }

    setSinglePhaseValue(inputField) {
        if (inputField.fieldName === 'ContractualPower__c') {
            if((inputField.value.length !== 0) && (inputField.value < 11)){
                this.changeElementFieldValue('PowerPhase__c', 'Single');
            } else if((inputField.value.length !== 0) && (inputField.value >= 11)){
                this.changeElementFieldValue('PowerPhase__c', 'Three');
            }else if(inputField.value.length === 0){
                this.changeElementFieldValue('PowerPhase__c', '');
            }
        }
    }

    /**
     * @author Boubacar Sow
     * @description change element fields value .
     *              [ENLCRO-577] Power/Voltage Change - Customer feedback to be corrected
     * @param fieldName
     */
    changeElementFieldValue(fieldName, value){
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if(element.fieldName === fieldName){
                element.value = value;
            }
        });
    }


    /**
     * @author Boubacar Sow
     * @description remove wr-required class
     *              [ENLCRO-797] Power/Voltage Change - WP1_WP2_PR2_Delta_Changes
     * @param fieldName
     */
    isOptionalField(fieldName){
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if(element.fieldName === fieldName){
                element.classList.remove('wr-required');
            }
        });
    }

    /**
     * @author Boubacar Sow
     * @description add slds-has-error class
     *              [ENLCRO-353] Power Voltage Change  Implementation Wizard
     * @param fieldName
     */
    addError(fieldName){
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if(element.fieldName === fieldName){
                element.classList.add('slds-has-error');
            }
        });
    }

    /**
     * @author Boubacar Sow
     * @description make the field read only
     *              [ENLCRO-353] Power Voltage Change  Implementation Wizard
     * @param fieldName
     */
    readOnlyField(fieldName){
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if(element.fieldName === fieldName){
                console.log('#### element.fieldName'+element.fieldName);
                element.disabled = true;
            }
        });
    }

    addClosingEvent() {
        const closeEvent = new CustomEvent('close', {
            detail: {
                caseId: this.caseId
            }
        });
        this.dispatchEvent(closeEvent);
    }

    setSinglePhase(event) {

        let inputContractualPowerCmp = event.target;
        if (inputContractualPowerCmp.fieldName === 'ContractualPower__c') {
            let contractualPowerCmp = this.template.querySelector('[data-id="powerPhase"]');
            if((inputContractualPowerCmp.value.length !== 0) && (inputContractualPowerCmp.value < 11)){
                contractualPowerCmp.value = 'Single'
            } else if((inputContractualPowerCmp.value.length !== 0) && (inputContractualPowerCmp.value >= 11)){
                contractualPowerCmp.value = 'Three';
            }else if(inputContractualPowerCmp.value.length === 0){
                contractualPowerCmp.value = '';
            }
            console.log("PowerPhase = " + contractualPowerCmp.value);
        }


        this.removeError(event);
    }

    setVoltageSetting(event) {
        console.log("set voltage setting event");
        let inputVoltageLevelCmp = event.target;
        this.updateVoltageSetting(inputVoltageLevelCmp);
        this.removeError(event);
    }

    validateFields() {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        let requireFieldsCombobox = Array.from(this.template.querySelectorAll('lightning-combobox.wr-required'));
        requireFields = requireFields.concat(requireFieldsCombobox);
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (!valueInput || (isNaN(valueInput) && valueInput.trim() === '')) {
                inputRequiredCmp.classList.add('slds-has-error');
                this.showLoadingSpinner =false;
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        return areValid;
    }
    get showRequestPurpose(){
        return this.isExistingValues || this.isPowerVoltage;
    }

    get cardFooterClass() {
        if (this.readOnlyFields) {
            return "slds-card__footer slds-m-top_medium slds-align-right slds-grid slds-grid_align-end readOnlyFields";
        }
        return "slds-card__footer slds-m-top_medium slds-align-right slds-grid slds-grid_align-end";
    }

    getDayDateDiff(date1 , date2) {
        const millesecondsPerDay = 86400000;
        let diff = Date.parse(date1) - Date.parse(date2);
        return isNaN(diff) ? NaN : Math.floor(diff / millesecondsPerDay);
    }

    /*validateFields() {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        return areValid;
    }*/

    disabledSaveButton(event) {
        this.isNormalize = event.detail.isnormalize;
    }

    disabledSaveButtonCaseAddress(event) {
        this.isNormalize = event.detail.isnormalize;
        this.addressForced = event.detail.addressForced;
    }

    handleSelectOption(event) {
        this.selectedOption = event.target.value;
    }

    get spinnerClass() {
        if (this.showLoadingSpinner) {
            return 'slds-modal slds-fade-in-open slds-backdrop';
        }
        return 'slds-modal slds-fade-in-open slds-backdrop slds-hide';
    }

    showParentCaseIdInput(compensationType) {
        if (compensationType === 'Furnizare' || compensationType === 'Gaz') {
            this.showParentCaseId = true;
        } else {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (element.fieldName === 'ParentId') {
                    element.value = null;
                    this.parentCaseId = null;
                }
            })
            this.setCaseDates();
            this.showParentCaseId = false;
        }
    }

    getConnectionAddress() {
        this.spinnerStatus = true;
        this.showLoadingSpinner = true;
        notCacheableCall({
            className: "MRO_LC_ConnectionWizard",
            methodName: "getAddress"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        console.log('adddddd' + JSON.stringify(apexResponse.data.osiAddress));
                        this.showLoadingSpinner = false;
                    }
                }
            });
    }
}