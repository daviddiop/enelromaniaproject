/**
 * Created by Boubacar Sow on 15/10/2020.
 */

import {api, LightningElement, track, wire} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';
import {NavigationMixin} from "lightning/navigation";
import {getRecord} from 'lightning/uiRecordApi';

export default class MroNotifications extends LightningElement {

    @api username ;
    @api hideCheckBox = false;
    @api hideRetrieveNotification = false;

    @track notificationList;
    @track showLoadingSpinner = false;
    @track showIllustrationEmptyStat = false;

    labels = labels;
    notificationColumns = [
        {label: this.labels.type, fieldName: 'type', type: 'text', sortable: true},
        {label: this.labels.descriptionColumn, fieldName: 'description', type: 'text', sortable: true},
        {label: this.labels.email, fieldName: 'email', type: 'boolean', sortable: true, fixedWidth:60},
        {label: this.labels.sms, fieldName: 'sms', type: 'boolean', sortable: true, fixedWidth:60},
        {label: this.labels.push, fieldName: 'push', type: 'boolean', sortable: true, fixedWidth:60},
    ];

    connectedCallback() {
        this.hideCheckBox = true;
        this.handleRetreiveNotification();
    }

    handleRetreiveNotification(){
        this.showLoadingSpinner = true;
        let input = {
            'username': this.username
        }
        notCacheableCall({
            className: "MRO_LC_Notification",
            methodName: "listNotification",
            input: input
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, this.labels.unexpectedError);
            }
            console.log('#### notification data '+JSON.stringify(apexResponse.data));
            if (apexResponse.data.error) {
                error(this, apexResponse.data.errorMsg);
            }
            if (!apexResponse.data.error && apexResponse.data.notifications){
                this.notificationList = JSON.parse(JSON.stringify(apexResponse.data.notifications));
            }
            this.showIllustrationEmptyStat = !this.notificationList || this.notificationList.length === 0;

        }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg));
        });

    }

}