import { LightningElement, track, api } from 'lwc';
import Results from '@salesforce/label/c.Results';
import Pages from '@salesforce/label/c.Pages';
import TotalRecordNumberWarning from '@salesforce/label/c.TotalRecordNumberWarning';

export default class paginationControl extends LightningElement {

    @api paginationControlsId = 'paginationControls';
    @api showFirstLast;
    @api firstLabel = '|<';
    @api previousLabel = '<';
    @api currentPageNumber;
    //@track buttonList = [];
    @api totalPageNumber;
    @api nextLabel = ">";
    @api lastLabel = ">|";

    @api textPosition = 'bottom';

    /** result label dynamically retrieved from custom label
    */
    @track resultLabel = Results;

    /** total record number warning label dynamically retrieved from custom label
    */
    @track totalRecordNumberWarning = TotalRecordNumberWarning;

    /** pages label dynamically retrieved from custom label
    */
    @track pagesLabel = Pages;

    @api totalRecordNumber;
    @api showTotalRecordNumberWarning;

    /**
      * connectedCallback method that initialize showFirstLast and showTotalRecordNumberWarning attribute
      *     and invoke the generateButtonList method
    */
    connectedCallback () {
        this.showFirstLast = true;
        this.showTotalRecordNumberWarning = false;
        //this.generateButtonList();
    }

    /**
      * Method that handles the event on the pagination buttons and dispatch the buttonpageclick event
    */
    handleButtonClick (event) {

        event.preventDefault();
        let buttonName = event.target.name;
        let pageNumber =
            buttonName == "first" ? 1 :
            buttonName == "previous" ? this.currentPageNumber - 1 :
            buttonName == "next" ? this.currentPageNumber + 1 :
            buttonName == "last" ? this.totalPageNumber :
            parseInt(buttonName);

//        console.log('PaginationControl.handleButtonClick - buttonName: ', buttonName, ' - pageNumber: ', pageNumber);

        if (pageNumber != this.currentPageNumber) {
            this.currentPageNumber = pageNumber;
            //this.generateButtonList();
            let newEvent = new CustomEvent('buttonpageclick', {
                            detail: {pageNumber: pageNumber, paginationControlsId: this.paginationControlsId}
            });
            this.dispatchEvent(newEvent);
        }
    }

    /**
      * Method that generates the list of pagination buttons to display
    *
    generateButtonList () {

        let btnList = [];
        let startPageNumber = this.currentPageNumber < 3 ? 1 : this.currentPageNumber - 2;
        let endPageNumber = this.currentPageNumber > this.totalPageNumber - 3 ? this.totalPageNumber : this.currentPageNumber + 2;
//        console.log('PaginationControl.generateButtonList - ', 'startPageNumber: ', startPageNumber, 'endPageNumber: ', endPageNumber);

        for(let i = startPageNumber; i <= endPageNumber; i++) {
            btnList.push({
                label: String(i),
                pageNumber: i,
                variant: i == this.currentPageNumber ? 'brand' : 'neutral'
            });
        }
        this.buttonList = btnList;
    }
    */

    get buttonList() {
        let btnList = [];
        let startPageNumber = this.currentPageNumber < 3 ? 1 : this.currentPageNumber - 2;
        let endPageNumber = this.currentPageNumber > this.totalPageNumber - 3 ? this.totalPageNumber : this.currentPageNumber + 2;
//        console.log('PaginationControl.generateButtonList - ', 'startPageNumber: ', startPageNumber, 'endPageNumber: ', endPageNumber);

        for(let i = startPageNumber; i <= endPageNumber; i++) {
            btnList.push({
                label: String(i),
                pageNumber: i,
                variant: i == this.currentPageNumber ? 'brand' : 'neutral'
            });
        }
        return btnList;
    }

    /**
     * returns true if the first three dots are to be displayed
    */
    get showFirstThreeDots () {

        return (this.currentPageNumber>3) ? true : false;
    }

    /**
     * returns true if the last three dots are to be displayed
    */
    get showLastThreeDots  () {

        return ((this.totalPageNumber-2) > this.currentPageNumber) ? true : false;
    }

    /**
     * returns true if the page displayed is the first one
    */
    get onFirstPage () {

        return (this.currentPageNumber == 1) ? true : false;
    }

    /**
     * returns true if the page displayed is the first one
    */
    get onLastPage () {

        return (this.currentPageNumber == this.totalPageNumber) ? true: false;
    }

    /**
     * returns true if the total records are more than 0
    */
    get showPaginationButtons () {

        return this.totalRecordNumber > 0;
    }

    /**
     * returns true if the pagination text must to be displayed at the top of pagination buttons
    */
    get showTopText () {

        //this.generateButtonList();
        return this.textPosition == 'top';
    }

    /**
     * returns true if the pagination text must to be displayed at the bottom of pagination buttons
    */
    get showBottomText () {

        //this.generateButtonList();
        return this.textPosition == 'bottom';
    }

    /**
     * returns the pagination text to be displayed
    */
    get paginationLabelText () {

//        this.generateButtonList();
        return (this.totalRecordNumber + ' ' + this.resultLabel + ' - ' + this.currentPageNumber + ' / ' + this.totalPageNumber + ' ' + this.pagesLabel);
    }

}