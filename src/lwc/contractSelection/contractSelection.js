/**
 * Created by goudiaby on 11/09/2019.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/labels';

export default class ContractSelection extends LightningElement {
    @api recordId;
    @api showCustomerSignedDate = false;
    @api companyDivisionId;
    @api contractId;
    @api disabled =false;
    @api hideNew = false;
    @api contractSignedDate;

    @track contracts;
    @track customerSignedDate;
    @track isNew = false;
    @track selectedContract;
    @track hasRendered = false;
    @track disableInput = false;
    labels = labels;
    @track contractValue;
    @track contractSignedDateValue;
    @track isReloaded = false;

    getContract() {
       return this.isNew ? 'new' : this.selectedContract.Id;
    }

    @api
    reloadContractList() {
        this.contractAddition();
        this.isReloaded = true;
    }

    contractAddition (){
        this.customerSignedDate = new Date().toISOString().slice(0, 10);
        this.contractValue = this.contractId;
        this.contractSignedDateValue = this.contractSignedDate;
        if(this.contractValue){
            this.isNew = '';
        }
        if(!this.contractValue){
            let contractInputs = this.template.querySelectorAll('[data-id="checkBoxContract"]');
            contractInputs.forEach(inputElement => {
                inputElement.checked = false;
            });

            let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
            this.disableInput = false;
            newContractCheckbox.checked = true;
        }



        if((this.contractSignedDateValue) || (this.contractValue)){
            this.customerSignedDate = this.contractSignedDateValue;
        }

        this.getContracts();
    }

    connectedCallback() {

        this.customerSignedDate = new Date().toISOString().slice(0, 10);
        if((this.contractSignedDateValue) || (this.contractValue)){
            this.customerSignedDate = this.contractSignedDateValue;
        }
        this.getContracts();
        this.isNew = !this.hideNew;
    }

    renderedCallback() {
        if (this.contracts && !this.hasRendered) {
            if(!this.isReloaded){
                this.contractAddition();
            }

            this.hasRendered = true;
            this.getContractEvent();
            let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
            if (this.isNew) {
                 this.disableInput = false;
                newContractCheckbox.checked = true;
            }
            this.contractSelectionHelper(this.contractId);
        }


        if(this.contractValue){
            let contractInputs = this.template.querySelectorAll('[data-id="checkBoxContract"]');
            contractInputs.forEach(inputElement => {
                inputElement.checked = (inputElement.value === this.contractValue);
            });
        }

        if(this.disabled){
            this.disableInput = true;
        }else if(!this.selectedContract){
            if(!this.contractValue && !this.isNew){
                this.disableInput=true;
            }else if(this.isNew){
               this.disableInput = false;
            }

            this.contractValue='';
        }

    }

    getContracts() {
        let inputs = {accountId: this.recordId, companyDivisionId: this.companyDivisionId};
        notCacheableCall({
            className: 'ContractCnt',
            methodName: 'getContractsByAccount',
            input: inputs
        })
            .then((response) => {
                this.contracts = response.data.contracts;
            })
            .catch((err) => {
            });
    }

    handleContractSelection(event) {
        if (event.target.checked) {
            this.contractValue='';
            this.contractSelectionHelper(event.target.value);
            this.getContractEvent();
        }else{
            this.setNewContract();
            this.customerSignedDate = new Date().toISOString().slice(0, 10);
            const getCntrEvent = new CustomEvent('getcontract', {
                detail: {
                    newCustomerSignedDate: this.customerSignedDate
                }
            });
            this.dispatchEvent(getCntrEvent);
        }
    }

    contractSelectionHelper(contractValue) {
        //this.contractValue='';
        this.selectedContract = this.contracts.find(function (contract) {
            return (contract.Id === contractValue);
        });

        if (this.selectedContract) {
            this.customerSignedDate = this.selectedContract["CustomerSignedDate"] ? this.selectedContract["CustomerSignedDate"] : null;

            this.disableInput = true;
            this.isNew = false;
        } else {
            this.setNewContract();
        }

        this.checkSelectedContract(contractValue, this.isNew);
    }

    setNewContract(){

        if((this.contractSignedDateValue) || (this.contractValue)){
            this.customerSignedDate = this.contractSignedDateValue;
        }
        this.selectedContract = null;
        this.isNew = true;
        this.disableInput = false;
        let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
        newContractCheckbox.checked = this.isNew;
    }

    getContractEvent() {
        const getCntrEvent = new CustomEvent('getcontract', {
            detail: {
                selectedContract: this.getContract(),
                newCustomerSignedDate: this.customerSignedDate
            }
        });
        this.dispatchEvent(getCntrEvent);
    }

    checkSelectedContract(contractId, isNewContract) {
        let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
        newContractCheckbox.checked = isNewContract;

        let contractInputs = this.template.querySelectorAll('[data-id="checkBoxContract"]');
        contractInputs.forEach(inputElement => {
            inputElement.checked = (inputElement.value === contractId);
        });

    }

    get classContractList() {
        if (!this.contracts) {
            return 'slds-size_10-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class';
        }
        if (this.hideNew) {
            if (this.disabled) {
                return 'slds-size_11-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class slds-theme_shade';
            }
            return 'slds-size_11-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class';
        }
        if (this.disabled) {
            return 'slds-size_10-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class slds-theme_shade';
        }
        return 'slds-size_10-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class';
    }

    get classSelectedContractBox() {
        if (this.disabled) {
            return 'slds-grid slds-wrap slds-gutters_xx-small slds-align_absolute-center wr-pointer-event_none';
        }
        return 'slds-grid slds-wrap slds-gutters_xx-small slds-align_absolute-center';
    }

    get classGridNewContract() {
        if (this.hideNew) {
            return 'slds-hide slds-size_1-of-12 slds-m-horizontal_xx-small slds-align_absolute-center';
        }
        return 'slds-size_1-of-12 slds-m-horizontal_xx-small slds-align_absolute-center';
    }

    get classBoxCustomerSignDate(){
        if(this.showCustomerSignedDate){
            return 'slds-hide slds-m-top_medium'
        }else{
           return 'slds-m-top_medium';
        }
    }

    get newContractTitle() {
        return this.labels.newContract;
    }

    handleSignedContract(event){
        const getCntrEvent = new CustomEvent('getcontract', {
            detail: {
                newCustomerSignedDate: event.target.value
            }
        });
        this.dispatchEvent(getCntrEvent);
    }

     removeError = () => {
        let areValid = true;

        let requireFields = Array.from(this.template.querySelectorAll('[data-id="signedDateField"]'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput === null || valueInput.trim() === '') {
                inputRequiredCmp.classList.remove('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

}