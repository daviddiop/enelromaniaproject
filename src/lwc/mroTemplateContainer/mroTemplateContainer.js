import {api, LightningElement, track} from 'lwc';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import {jsonBuilder} from 'c/jsonBuilder';
import {error} from "c/notificationSvc";

export default class TemplateContainer extends LightningElement {
    @api templateId; //Id of the Script Template to render
    @api recordId;
    @api templateCode;
    @api objectJsonBuilder;
    @api inputJson; //JSON representation of the template inputs

    @api friendlyErrorMessage = 'Error retrieving data';

    @track templateItems = null;
    @track error = null;
    @track viewDetails = false;
    @track showLoadingSpinner = false;
    jsonPromise;
    resultingRichText = "";

    @api
    setParams(value) {
        this.inputJson = value;
        this.template.querySelectorAll('c-script-element').forEach(element => {
                element.setInputData(this.inputJson);
                //this.showLoadingSpinner = false;
            }
        );
    }

    connectedCallback() {
        this.showLoadingSpinner = true;
        cacheableCall({
            className: 'MRO_LC_ScriptTemplate',
            methodName: 'getScriptElements',
            input: {
                templateId: this.templateId,
                templateCode: this.templateCode
            }
        }).then(response => {
            if (response.isError) {
                this.error = JSON.stringify(response.error);
                this.showLoadingSpinner = false;
                return this.error;
            }
            this.templateItems = response.data.scriptElements;
            if (!this.inputJson) {
                this.jsonPromise = jsonBuilder(this.objectJsonBuilder, this.recordId);
                this.jsonPromise.then(response => {
                    if (response.isError) {
                        error(JSON.stringify(response.error));
                        this.showLoadingSpinner = false;
                    }
                    this.setParams(response);

                }).catch((errorMsg) => {
                    error(errorMsg.body);
                    this.showLoadingSpinner = false;
                });
                this.showLoadingSpinner = false;
            } else {
                this.inputJson = JSON.parse(this.inputJson);

                this.showLoadingSpinner = false;
            }
        }).catch((errorMsg) => {
            this.error = errorMsg.body.output.errors[0].message;
            this.showLoadingSpinner = false;
        });
    }

    @api
    getResultingRichText() {
        return this.resultingRichText;
    }

    get errorMessages() {
        return this.reduceErrors(this.error);
    }

    handleCheckboxChange(event) {
        this.viewDetails = event.target.checked;
    }

    reduceErrors(errors) {
        if (!Array.isArray(errors)) {
            errors = [errors];
        }

        return (
            errors
                .filter(error => !!error)
                .map(error => {
                    if (Array.isArray(error.body)) {
                        return error.body.map(e => e.message);
                    } else if (error.body && typeof error.body.message === 'string') {
                        return error.body.message;
                    } else if (typeof error.message === 'string') {
                        return error.message;
                    }
                    return error.statusText;
                })
                .reduce((prev, curr) => prev.concat(curr), [])
                .filter(message => !!message)
        );
    }
};