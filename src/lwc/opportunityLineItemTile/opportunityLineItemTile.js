import { LightningElement, api } from 'lwc';
import { labels } from 'c/labels';

export default class OpportunityLineItemTile extends LightningElement {
    @api recordId;
    labels = labels;
    @api disabled = false;

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }
}