/**
 * Created by Octavian on 11/20/2019.
 */

import {api, track, LightningElement} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
//FF Self Reading #WP3-P2
import { error } from 'c/notificationSvc';
import {labels} from 'c/mroLabels';
//FF Self Reading #WP3-P2

export default class MroMeterTile extends LightningElement {
    labels = labels;
    @api meterNumber; //Id of the Meter MeterNumber__c
    @track quadrants; //Json Array containing the list of available quadrants for the Meter
    @api validateReadings; //boolean value that enables/disables
    @api recordTypeName ; //recordtype of selected supply
    @api disabled= false;
    @api disallowEdit= false;
    @api tabelId;
    @api servicePointCode;
    @api processName;
    @api params;

    @track meterData;
    @track draftValues;
    @track errors;
    @track activeEnergyName;
    @track sapIndexValue;
    @track initValue = 0;
    @track indexLessThanSAPIndex = false;

    /*added by Giuseppe Mario Pastore 11-06-2020*/
    @api tableIdToEdit;
    @track meterDataToEdit;
    @track draftValuesToEdit;
    @api isUsedSelfReadingCmp = false;
    @api skipMetersValidation = false;
    @api isMeterAlreadyLoaded = false;
    @api isTransferOrProductChange = false;
    @api allowEditOnlyActiveEnergy = false;
    columnsToEdit;
    indexColumnToEdit;



    quadrantsArray = [];

    columns ;
    indexColumn;

    connectedCallback(){
        this.isMeterAlreadyLoaded = true;
        this.initColumns();
        if(this.quadrants){
            this.meterData.splice(0, this.meterData.length);
            this.meterData = JSON.parse(this.getMeterData(this.quadrants));
        }
        this.getConstants();
    }


    @api setQuadrantsInfo(quadrants){
        this.disableIndex();
        this.quadrants = JSON.parse(quadrants);
        let quadrantsReadOnly = [];
        let quadrantsToEdit = [];

        if(this.isUsedSelfReadingCmp === true) {
            this.quadrants.forEach(element => {
                if (!this.allowEditOnlyActiveEnergy || element["ID_CADRAN"].substring(0, 3) === '1.8' && element["ID_CADRAN"] !== '1.8.0N' || this.recordTypeName === 'Gas') {
                    quadrantsToEdit.push(element);
                } else {
                    quadrantsReadOnly.push(element);
                }
            });
            if (this.isTransferOrProductChange) {
                this.meterData = JSON.parse(this.getMeterDataIndexSAP(quadrantsReadOnly));
                this.meterDataToEdit = JSON.parse(this.getMeterDataIndexSAP(quadrantsToEdit));
            } else if(this.processName === 'ContractMerge'){
                this.meterData = this.getMeterDataNew(quadrantsReadOnly);
                this.meterDataToEdit = this.getMeterDataNew(quadrantsToEdit);
            } else {
                this.meterData = JSON.parse(this.getMeterData(quadrantsReadOnly));
                this.meterDataToEdit = JSON.parse(this.getMeterData(quadrantsToEdit));
            }
        }
        else{
            this.meterData = JSON.parse(this.getMeterData(this.quadrants));
        }

    }

    @api isMeterValidated(){
        let isMeterValidated = true;
        for(let i=0; i < this.quadrantsArray.length; i++){
            let quadrantJSON = JSON.parse(this.quadrantsArray[i]);
            if(quadrantJSON["VALIDATED_ICON"] != "utility:check"){
                isMeterValidated = false;
            }
        }
        return isMeterValidated;
    }

    @api getMeterInfo(){
        let meterMap = [];
        for(let i=0; i < this.quadrantsArray.length; i++){
            let quadrantJSON = JSON.parse(this.quadrantsArray[i]);
            if(quadrantJSON["VALIDATED_ICON"] == "utility:check" || this.skipMetersValidation){
                meterMap.push({
                    'meterNumber' : this.meterNumber,
                    'quadrantName': quadrantJSON["CADRAN"],
                    'index': quadrantJSON["INDEX"],
                    'idCadran': quadrantJSON["ID_CADRAN"]
                });
            }
        }
        console.log(JSON.stringify(meterMap));
        return meterMap;
    }

    @api getMeterInfoValidated(){
        let meterMap = [];
        for(let i=0; i < this.quadrantsArray.length; i++){
            let quadrantJSON = JSON.parse(this.quadrantsArray[i]);
            if(quadrantJSON["VALIDATED_ICON"] == "utility:check" || this.skipMetersValidation){
                meterMap.push({
                    'meterNumber' : this.meterNumber,
                    'quadrantName': quadrantJSON["CADRAN"],
                    'index': quadrantJSON["INDEX"],
                    "idCadran" : quadrantJSON["ID_CADRAN"],
                    "validated" : true
                });
            } else {
                meterMap.push({
                    'meterNumber' : this.meterNumber,
                    'quadrantName': quadrantJSON["CADRAN"],
                    'index': quadrantJSON["INDEX"],
                    "idCadran" : quadrantJSON["ID_CADRAN"],
                    "validated" : false
                });
            }
        }
        console.log(JSON.stringify(meterMap));
        return meterMap;
    }
    disableIndex(){
        this.initColumns();
    }
    handleSaveIndex(event) {



        if(this.isUsedSelfReadingCmp === true) {
            this.draftValuesToEdit = event.detail.draftValues;
            this.draftValuesToEdit[0]["INDEX"] = this.draftValuesToEdit[0]["INDEX"] ? this.draftValuesToEdit[0]["INDEX"] : 0;
            let quadrantDescription = this.draftValuesToEdit[0]["id"].split("-");
            let quadrantIndex = quadrantDescription[1];

            let quadrantJSON = JSON.parse(this.quadrantsArray[quadrantIndex]);
          //  if (quadrantJSON["ID_CADRAN"].substring(0, 3) === '1.8' && quadrantJSON["ID_CADRAN"] !== '1.8.0N') {
            let quadrantName=quadrantJSON["CADRAN"];
            let quadrantIdStartsWith = quadrantJSON["ID_CADRAN"].substring(0,3);
            let allowEdit= quadrantName === this.activeEnergyName;
            //if (allowEdit == true && quadrantIdStartsWith === '1.8') {
                this.updateMeterIndex(this.draftValuesToEdit[0]["INDEX"], quadrantIndex);
                if (this.validateReadings && !this.indexLessThanSAPIndex) {
                    this.validateIndexValue(this.draftValuesToEdit[0]["INDEX"], quadrantIndex);
                }
                 //}
            //}
        }else {
            this.draftValues = event.detail.draftValues;
            this.draftValues[0]["INDEX"] = this.draftValues[0]["INDEX"] ? this.draftValues[0]["INDEX"] : 0;
            let quadrantDescription = this.draftValues[0]["id"].split("-");
            let quadrantIndex = quadrantDescription[1];

            let quadrantJSON = JSON.parse(this.quadrantsArray[quadrantIndex]);
            if (quadrantJSON["ID_CADRAN"].substring(0, 3) === '1.8' && quadrantJSON["ID_CADRAN"] !== '1.8.0N') {
                this.updateMeterIndex(this.draftValues[0]["INDEX"], quadrantIndex);
                if (this.validateReadings) {
                    this.validateIndexValue(this.draftValues[0]["INDEX"], quadrantIndex);
                }
            }

        }
    }

    handleCancel(event)
    {
        this.draftValues = null;
        this.draftValuesToEdit = null;
    }

    validateIndexValue(newIndexValue, quadrantIndex){
        let quadrantJSON = JSON.parse(this.quadrantsArray[quadrantIndex]);
        notCacheableCall({
            className: "MRO_LC_MeterTile",
            methodName: "validateQuadrantIndex",
            input: {
                'ServicePointCode' : this.servicePointCode,
                'MeterSerialNumber': this.meterNumber,
                'DialId' : quadrantJSON["ID_CADRAN"],
                'QuadrantName': quadrantJSON["CADRAN"],
                'QuadrantIndex': newIndexValue,
                'params': this.params,
            }
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }
            if(!apexResponse.data.indexValidated){
                error(this, apexResponse.data.errorMessage);
            }
            this.updateValidationValue(apexResponse.data.indexValidated, quadrantIndex, apexResponse.data.errorMessage, apexResponse.data.returnCode);
        });
    }

// FF Self Reading #WP3-P2
    updateMeterIndex(newIndexValue, quadrantIndex){
        let quadrantJSON = JSON.parse(this.quadrantsArray[quadrantIndex]);
        if(this.isUsedSelfReadingCmp === true) {
            if (parseFloat(newIndexValue) < parseFloat(quadrantJSON["SAP_INDEX"])){
                error(this, this.labels.indexLowerSAPIndex);
                this.indexLessThanSAPIndex = true;
                return ;
            }
            else{
                this.indexLessThanSAPIndex = false;
            }
        }
        quadrantJSON["QUANTITY"] = (newIndexValue - quadrantJSON["SAP_INDEX"]).toString();
        quadrantJSON["INDEX"] = newIndexValue;
        this.quadrantsArray[quadrantIndex] = JSON.stringify(quadrantJSON);
        if(this.isUsedSelfReadingCmp === true) {
            this.meterDataToEdit = JSON.parse("[" + this.quadrantsArray.toString() + "]");
        } else {
            this.meterData = JSON.parse("[" + this.quadrantsArray.toString() + "]");
        }
        //this.template.queryse //--------> this line of code is written in this way, is that normal?
        //console.log("this.meterData==="+JSON.stringify(this.meterDataToEdit));
    }
// FF Self Reading #WP3-P2
    updateValidationValue(isValidated, quadrantIndex, errorMessage, returnCode){
        let quadrantJSON = JSON.parse(this.quadrantsArray[quadrantIndex]);
        if(isValidated){
            quadrantJSON["VALIDATED_ICON"] = "utility:check";
            this.errors = null;
            this.handleCancel();
        }
        else{
            quadrantJSON["VALIDATED_ICON"] = "utility:close";
            this.triggerError((parseInt(quadrantIndex)+1), errorMessage);
        }

        this.quadrantsArray[quadrantIndex] = JSON.stringify(quadrantJSON);
        if(this.isUsedSelfReadingCmp === true) {
            this.meterDataToEdit = JSON.parse("[" + this.quadrantsArray.toString() + "]");
        }
        else {
            this.meterData = JSON.parse("[" + this.quadrantsArray.toString() + "]");
        }

        this.dispatchEvent(new CustomEvent('edit', {
            detail: {
                'isValidated': isValidated,
                'code': returnCode,
            }
        }));
    }

    triggerError(row, errorMessage){
        console.log("trigger error row = " + row + " message = " + errorMessage)
        this.errors = {
            table: {
                title: 'You have invalid index values. Fix the errors and try again.',
                messages: [
                    'On row ' + row + ' : ' +errorMessage
                ]
            }
        };
    }
// FF Self Reading #WP3-P2
    initColumns(){
        /*edited by Giuseppe Mario Pastore 11-06-2020*/
        if(this.isUsedSelfReadingCmp === true) {
            this.indexColumn= {label: 'New Index', fieldName: 'INDEX', editable: false};
            this.columns = [
                {label: 'Quadrant Name', fieldName: 'CADRAN'},
                {label: 'Quadrant ID', fieldName: 'ID_CADRAN'},
                {label: 'Last Index from SAP', fieldName: 'SAP_INDEX'},
                this.indexColumn,
                {label: 'Quantity Consumed', fieldName: 'QUANTITY'},
                {
                    label: 'Validated', fieldName: 'VALIDATED', cellAttributes:
                        {iconName: {fieldName: 'VALIDATED_ICON'}, iconPosition: 'right'}
                }

            ];


            this.indexColumnToEdit = {
                label: 'New Index',
                fieldName: 'INDEX',
                editable: (!this.disallowEdit && !this.disabled)
            };
            this.columnsToEdit = [
                {label: 'Quadrant Name', fieldName: 'CADRAN'},
                {label: 'Quadrant ID', fieldName: 'ID_CADRAN'},
                {label: 'Last Index from SAP', fieldName: 'SAP_INDEX'},
                this.indexColumnToEdit,
                {label: 'Quantity Consumed', fieldName: 'QUANTITY'},
                {
                    label: 'Validated', fieldName: 'VALIDATED', cellAttributes:
                        {iconName: {fieldName: 'VALIDATED_ICON'}, iconPosition: 'right'}
                }

            ];
        }
        else {
            this.indexColumn= {label: 'New Index', fieldName: 'INDEX', editable: (!this.disallowEdit && !this.disabled)};
            this.columns = [
                {label: 'Quadrant Name', fieldName: 'CADRAN'},
                {label: 'Quadrant ID', fieldName: 'ID_CADRAN'},
                {label: 'Last Index from SAP', fieldName: 'SAP_INDEX'},
                 this.indexColumn,
                {label: 'Quantity Consumed', fieldName: 'QUANTITY'},
                {
                    label: 'Validated', fieldName: 'VALIDATED', cellAttributes:
                        {iconName: {fieldName: 'VALIDATED_ICON'}, iconPosition: 'right'}
                }


            ];
        }
        /*added by Giuseppe Mario Pastore 11-06-2020*/


    }
    getMeterData(quadrantsJson) {
        this.quadrantsArray.splice(0, this.quadrantsArray.length);

        for (let i = 0; i < Object.values(quadrantsJson).length; i++) {
            let quadrant = Object.values(quadrantsJson)[i];
            if(quadrant){
                 this.sapIndexValue =  quadrant["INDEX"];
            }
            if(this.recordTypeName === 'Gas'){
                if(quadrant["CADRAN"] !== 'Energie Reactiva Inductva'){
                    this.quadrantsArray.push(
                        "                {\n" +
                        "                    \"ID_CADRAN\": \"" + quadrant["ID_CADRAN"] + "\",\n" +
                        "                    \"CADRAN\": \"" + quadrant["CADRAN"] + "\",\n" +
                        //"                    \"INDEX\": \"0\",\n" +
                        "                    \"SAP_INDEX\": \""+ (( this.sapIndexValue == null) ? "0"  :   this.sapIndexValue )+ "\",\n" +
                        "                    \"INDEX\": \""+ (( this.initValue == null) ? "0"  :  this.initValue )+ "\",\n" +
                        "                    \"QUANTITY\": \""+ (( quadrant["INDEX"] === null || this.isMeterAlreadyLoaded) ? "0"  :   quadrant["INDEX"] )+ "\",\n" +
                        "                    \"VALIDATED\": \"\",\n" +
                        //"                    \"VALIDATED_ICON\": \"utility:close\"\n" +
                        "                    \"VALIDATED_ICON\": \""+ ((quadrant["VALIDATED"]=== true ) ? "utility:check" : "utility:close" ) +  "\"\n" +
                        "                }\n"
                    );
                }
            } else {
                this.quadrantsArray.push(
                        "                {\n" +
                        "                    \"ID_CADRAN\": \"" + quadrant["ID_CADRAN"] + "\",\n" +
                        "                    \"CADRAN\": \"" + quadrant["CADRAN"] + "\",\n" +
                        //"                    \"INDEX\": \"0\",\n" +
                        "                    \"SAP_INDEX\": \""+ (( this.sapIndexValue == null) ? "0"  :   this.sapIndexValue )+ "\",\n" +
                        "                    \"INDEX\": \""+ (( this.initValue == null) ? "0"  :  this.initValue )+ "\",\n" +
                        "                    \"QUANTITY\": \""+ ((this.initValue == null) ? "0"  :   this.initValue )+ "\",\n" +
                        "                    \"VALIDATED\": \"\",\n" +
                        //"                    \"VALIDATED_ICON\": \"utility:close\"\n" +
                        "                    \"VALIDATED_ICON\": \""+ ((quadrant["VALIDATED"]=== true ) ? "utility:check" : "utility:close" ) +  "\"\n" +
                        "                }\n"
                    );
                }
            }

        return "[" + this.quadrantsArray.toString() + "]";
    }
    getMeterDataIndexSAP(quadrantsJson) {
        this.quadrantsArray.splice(0, this.quadrantsArray.length);

        for (let i = 0; i < Object.values(quadrantsJson).length; i++) {
            let quadrant = Object.values(quadrantsJson)[i];
            if(quadrant){
                this.sapIndexValue =  quadrant["SAP_INDEX"] ? quadrant["SAP_INDEX"] : quadrant["INDEX"] ;
                this.initValue = quadrant["SAP_INDEX"] ? quadrant["INDEX"] : quadrant["SAP_INDEX"] ;
            }
            if(this.recordTypeName === 'Gas'){
                if(quadrant["CADRAN"] !== 'Energie Reactiva Inductva'){
                    this.quadrantsArray.push(
                        "                {\n" +
                        "                    \"ID_CADRAN\": \"" + quadrant["ID_CADRAN"] + "\",\n" +
                        "                    \"CADRAN\": \"" + quadrant["CADRAN"] + "\",\n" +
                        //"                    \"INDEX\": \"0\",\n" +
                        "                    \"SAP_INDEX\": \""+ (( this.sapIndexValue == null) ? "0"  :   this.sapIndexValue )+ "\",\n" +
                        "                    \"INDEX\": \""+ (( this.initValue == null) ? "0"  :  this.initValue )+ "\",\n" +
                        "                    \"QUANTITY\": \""+ (( quadrant["INDEX"] === null || this.isMeterAlreadyLoaded) ? "0"  :   quadrant["INDEX"] )+ "\",\n" +
                        "                    \"VALIDATED\": \"\",\n" +
                        //"                    \"VALIDATED_ICON\": \"utility:close\"\n" +
                        "                    \"VALIDATED_ICON\": \""+ ((quadrant["VALIDATED"]=== true ) ? "utility:check" : "utility:close" ) +  "\"\n" +
                        "                }\n"
                    );
                }
            } else {
                console.log('quadrant["VALIDATED"] °ç°ç°ç°ç°ç°ç°ç°ç°ç°ç°ç°ç°ç° ' + quadrant["VALIDATED"]);
                this.quadrantsArray.push(
                    "                {\n" +
                    "                    \"ID_CADRAN\": \"" + quadrant["ID_CADRAN"] + "\",\n" +
                    "                    \"CADRAN\": \"" + quadrant["CADRAN"] + "\",\n" +
                    //"                    \"INDEX\": \"0\",\n" +
                    "                    \"SAP_INDEX\": \""+ (( this.sapIndexValue == null) ? "0" : this.sapIndexValue )+ "\",\n" +
                    "                    \"INDEX\": \""+ (( this.initValue == null) ? "0" :  this.initValue )+ "\",\n" +
                    "                    \"QUANTITY\": \""+ ( (!quadrant["INDEX"] && quadrant["INDEX"] === 0 )  ? "0" : quadrant["SAP_INDEX"] ? quadrant["INDEX"] - quadrant["SAP_INDEX"] : "0" ) + "\",\n" +
                    "                    \"VALIDATED\": \"\",\n" +
                    //"                    \"VALIDATED_ICON\": \"utility:close\"\n" +
                    "                    \"VALIDATED_ICON\": \""+ ((quadrant["VALIDATED"]=== true ) ? "utility:check" : "utility:close" ) +  "\"\n" +
                    "                }\n"
                );
            }
        }

        return "[" + this.quadrantsArray.toString() + "]";
    }

    getMeterDataNew(quadrantsList) {
        this.quadrantsArray = quadrantsList.map(quadrant => {
            quadrant['VALIDATED_ICON'] = quadrant['VALIDATED'] ? 'utility:check' : 'utility:close';
            quadrant['VALIDATED'] = '';
            quadrant['SAP_INDEX'] = typeof quadrant['SAP_INDEX'] === 'number' ? quadrant['SAP_INDEX'] : 0;
            quadrant['INDEX'] = typeof quadrant['INDEX'] === 'number' ? quadrant['INDEX'] : 0;
            quadrant['QUANTITY'] = typeof quadrant['INDEX'] === 'number' && quadrant['INDEX'] > 0 ? quadrant['INDEX'] - quadrant['SAP_INDEX'] : 0;
            return JSON.stringify(quadrant);
        });
        return this.quadrantsArray.map(quadrant => {return JSON.parse(quadrant)});
    }

// FF Self Reading #WP3-P2

    getConstants() {
        cacheableCall({
            className: "MRO_UTL_Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {

                        this.activeEnergyName = apexResponse.data.ACTIVE_ENERGY;

                    }
                }
            });
    }
}