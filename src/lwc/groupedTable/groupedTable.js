import { LightningElement, api, track } from 'lwc';
import {labels} from 'c/labels';

export default class GroupedTable extends LightningElement {
    label = labels
    @api multiselection;
    @api columns = [];
    @api rows = [];

    @api showSpinner = false;
    @track currentPage = 0;
    @api pageSize = 20;
  
    @track queryTerm

    get showedPage() {
        return this.currentPage +1;
    }

    get total() {
        let groups = this.filteredRows.slice(0);
        let total = 0;
        for(let g of groups) {
            total += 1 + g.children.length;
        }
        return total;
    }

    get pagedItems() {
        let groups = this.filteredRows.slice(0);
        let pages = [];
        let count = 0;
        let tmpPage = [];
        for(let i=0; i< groups.length; i++) {
            if(count + 1 + groups[i].children.length < this.pageSize) {
                tmpPage.push(groups[i]);
                count += 1 + groups[i].children.length
                if(i === groups.length - 1) {
                    pages.push(tmpPage);
                }
            } else {
                let tmp = {...groups[i]};
                tmp.children  = groups[i].children.slice(0, this.pageSize - count -1);
                tmpPage.push(tmp);

                let newG = {...groups[i]};
                newG.children = groups[i].children.slice(this.pageSize - count - 1);
                if(newG.children.length !== 0) {
                    groups.splice(i + 1, 0, newG);
                }
                count = 0;
                pages.push(tmpPage);
                tmpPage = [];
            }
        }
        return pages;
    }

    get currentPageRows() {
        return this.pagedItems[this.currentPage] || [];
    }

    handleItem(evt) {
        const event = new CustomEvent('toggleitem', {
            detail: evt.detail
        });
        this.dispatchEvent(event);
    }

    handleGroup(evt) {
        evt.detail.children = this.filteredRows.filter(obj => {return obj.key === evt.detail.groupKey})[0].children.map(child => child.key);
        const event = new CustomEvent('togglegroup', {
            detail: evt.detail
        });
        this.dispatchEvent(event);
    }


    get isLastPage() {
        return this.currentPage >= this.pagedItems.length - 1;
    }

    get isFirstPage() {
        return this.currentPage === 0
    }

    get filteredRows() {
        let queryTerm = this.queryTerm;
        if(!queryTerm) {
            return this.rows;
        }
        let filteredRows = [];
        for(let r of this.rows) {
            let tmp = {...r};

            tmp.children = tmp.children.filter(function(item) {
                if(!queryTerm) return true;
                function searchTerm(obj, term){
                    for(let key in obj) {
                        if(!obj.hasOwnProperty(key)) continue;
                        if(typeof obj[key] === 'object') {
                            if(searchTerm(obj[key], term)) {
                                return true;
                            }
                        }
                        if (typeof obj[key] !== 'string' && !(obj[key] instanceof String)) continue;
                        if(obj[key].indexOf(term) !== -1) {
                            return true;
                        }
                    }
                    return false;
                }
                return searchTerm(item, queryTerm);
            });
            if(tmp.children.length > 0){
                filteredRows.push(tmp);
            }
        }
        return filteredRows;
    }

    handleKeyUp(evt) {
        const isEnterKey = evt.keyCode === 13;
        if (isEnterKey) {
            this.queryTerm = evt.target.value;
            this.currentPage = 0;
        }
    }

    @api
    resetPage(){
        this.currentPage = 0;
    }

    nextPage() {
        if(!this.isLastPage) {
            this.currentPage++
        }
    }

    previousPage() {
        if(!this.isFirstPage) {
            this.currentPage--
        }
    }

    goToFirstPage() {
        this.currentPage = 0
    }

    goToLastPage() {
        this.currentPage = this.pagedItems.length - 1;
    }
}