import {api, LightningElement, track} from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error, success} from 'c/notificationSvc';
import {labels} from 'c/labels';

export default class PrivacyRecap extends LightningElement {
    @api selectedIndividualFields;
    @track defaultIndividualFields = ['HasOptedOutSolicit', 'HasOptedOutProcessing','HasOptedOutProfiling','HasOptedOutTracking','SendIndividualData','ShouldForget'];
    @api recordId;
    @track individualId;
    @track hasIndividualId;
    @track label;

    labels = labels;


    connectedCallback () {
        this.getIndividualId();
    }

    getIndividualId() {
        let inputs = {
            accountId: this.recordId
        };

        notCacheableCall({
            className: 'PrivacyChangeCnt',
            methodName: 'getIndividualPrivacyRecap',
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }

                this.individualId = response.data.individualId;
                this.hasIndividualId = ((response.data.individualId) && (response.data.individualId != null));
                this.label = response.data.label;
            })
            .catch(err => {
                error(this, JSON.stringify(err));
            });
    }

    get viewIndividualFields (){

        if((!this.selectedIndividualFields)){
            return  this.defaultIndividualFields;
        }else{
            return this.selectedIndividualFields.split(',');
        }
    }
}