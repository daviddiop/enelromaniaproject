/**
 * Created by aspiga on 19/12/2019.
*/

import {LightningElement, api, track, wire} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import {getFieldValue, getRecordUi} from "lightning/uiRecordApi";
import {error} from 'c/notificationSvc';

export default class MroCompactLayoutPopover extends NavigationMixin(LightningElement) {
    @api recordId;
    @api customFields = [];
    @api xCoord;
    @api yCoord;

    @track objectApiName;
    @track isVisible = false;
    @track layoutFields = [];
    @track nameField = 'Name';
    @track iconUrl;
    @track iconStyle;
    @track top = 0;
    @track left = 0;
    @track changePositionPoint = Math.max(document.documentElement.clientHeight, window.innerHeight || 200) / 2;
    @api position = 'radio-right'; //possible values: auto/left/right/top/radio-right

    @api
    get showBox(){
        if(this.isVisible){
            return "showPopover";
        }
        return "hidePopover";
    }

    get positionStyle() {
        if(this.isVisible){
            if(this.position == 'right'){
                return (`z-index: 999; position: absolute; top:0px; left:${this.left - 640}px`);
            } else if(this.position == 'left'){
                return (`z-index: 999; position: absolute; top:${this.yCoord}px; right:${this.xCoord}px`);
            } else if(this.position == 'radio-right'){
                return (`z-index: 999; position: absolute; top:${this.yCoord}px; left:${this.xCoord}px`);
            } else if (this.position == 'right-center') {
                return (`z-index: 999; position: absolute; top:${this.top - 175}px; left:${this.left - 5}px`);
            } else {
                return 'z-index: 999; position: absolute; top:-197px; left:0';
            }
        }
        else return '';
    }

    get boxClass(){
        if(this.position == 'right'){
            return 'slds-popover slds-popover_panel slds-popover_medium slds-nubbin_left-top slds-text-body_regular';
        }
        else if(this.position == 'left'){
            return 'slds-popover slds-popover_panel slds-popover_medium slds-nubbin_right-top slds-text-body_regular';
        }
        else if(this.position == 'radio-right'){
            return 'slds-popover slds-popover_panel slds-popover_medium slds-nubbin_left-top slds-text-body_regular radioright';
        } else if (this.position == 'right-center') {
            return 'slds-popover slds-popover_panel slds-popover_medium slds-nubbin_left-top slds-text-body_regular';
        } else return 'slds-popover slds-popover_panel slds-popover_medium slds-nubbin_bottom-left slds-text-body_regular';
    }

    errorNtf = error;

    @wire(getRecordUi, { recordIds: '$recordId', layoutTypes: 'Compact', modes: 'View'})
    recordUi({ data, error }) {
        if (error) {
            console.error(error);
            this.errorNtf(error);
        }
        else if (data) {
            console.log(JSON.stringify(data));
            this.objectApiName = data.records[this.recordId].apiName;
            console.log('this.objectApiName: ' + this.objectApiName);
            switch (this.objectApiName) {
                case 'Case':
                    this.nameField = 'CaseNumber';
                    break;
                case 'Contract':
                    if (data.records[this.recordId].fields.Name === undefined) {
                        this.nameField = 'ContractNumber';
                    } else {
                        this.nameField = 'Name';
                    }
                    break;
                default:
                    this.nameField = 'Name';
                    break;
            }
            const objectInfo = data.objectInfos[this.objectApiName];
            const themeInfo = objectInfo.themeInfo;
            this.iconUrl = themeInfo.iconUrl;
            this.iconStyle = 'background-color: #' + themeInfo.color;
            const compactLayout = Object.values(data.layouts[this.objectApiName])[0].Compact;
            this.layoutFields = [];
            compactLayout.View.sections[0].layoutRows.forEach(row => {
                let apiName;
                if (row.layoutItems[0].layoutComponents.length > 1) {
                    const compoundFieldName = row.layoutItems[0].layoutComponents[0].apiName;
                    apiName = objectInfo.fields[compoundFieldName].compoundFieldName;
                }
                else {
                    apiName = row.layoutItems[0].layoutComponents[0].apiName;
                }
                if (apiName === this.nameField) {
                    return;
                }
                if (this.customFields.length === 0) {
                    this.layoutFields.push(apiName);
                } else {
                    this.layoutFields = this.customFields;
                }
            });
            console.log(JSON.stringify(this.layoutFields));
        }
    };

    @api
    showPopover(event) {
        this.isVisible = true;

        if(this.top == 0 && this.left == 0){
            this.top = event.clientY;
            this.left = event.clientX;
        }
        let rect = event.target.getBoundingClientRect();
        //console.log('rect', rect);
        if(this.position == 'auto'){
            if(rect != 'undefined' && this.changePositionPoint != 'undefined'){
                this.position = (rect.bottom >= this.changePositionPoint)? 'top' : 'radio-right';
            }
            else this.position = 'right';
        }
        /*console.log('this.recordId: ', this.recordId);
        console.log('document.documentElement.clientHeight', document.documentElement.clientHeight);
        console.log('window.innerHeight', window.innerHeight);
        console.log('changePositionPoint', this.changePositionPoint);
        console.log('position', this.position);
        console.log('rect.bottom', rect.bottom);
        console.log('top', this.top);
        console.log('left', this.left);*/
    }

    @api
    closePopover() {
        this.isVisible = false;
    }
    @api
    closePopoverOnMouseOver() {
        setTimeout(function(){
            this.isVisible = false;
        }, 1000);
    }

    navigateToObject() {
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.recordId,
                "objectApiName": this.objectApiName,
                "actionName": "view"
            }
        });
    }

}