import { LightningElement, api, track } from 'lwc';
import {labels} from "c/mroLabels";
import { NavigationMixin } from 'lightning/navigation';
import { error, success, warn } from 'c/notificationSvc';
import INDIVIDUAL_OBJECT from '@salesforce/schema/Individual';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {checkCNP} from "c/mroValidations";

export default class MroIndividualEdit extends NavigationMixin(LightningElement) {
    @api recordId;
    @api interactionId;
    @api searchFields;
    @api isRedirectAllowed;
    @api disableFilled;
    @api hideCancel;
    @api hideBack = false;
    @api isConnectionRepresentative = false;
    @api accountRecord;
    @api formTitle = labels.individualEditForm;
    @track selectedContactChannel = [];
    contactChannelOptionList = [];
    contactChannelListString;

    @track showLoadingSpinner = false;
    @track interlocutor = {
        firstName: '',
        lastName: '',
        nationalId: '',
        ForeignCitizen: false,
        phone: '',
        email: '',
        //FF Customer Creation - Pack1/2 - Interface Check
        mobile:'',
        contactChannel:'',
        //FF Customer Creation - Pack1/2 - Interface Check
        gender: '',
        birthDate: '',
        birthCity: '',
        toString: function () {
            return JSON.stringify(this);
        }
    };

    labels = labels;
    interlocutorObject = INDIVIDUAL_OBJECT;

    get cancelRendered() {
        if (this.hideCancel === 'true') {
            return false;
        }
        return true;
    }

    get firstName() {
        if (this.searchFields && this.searchFields.FirstName) {
            /*
            let nameParts = this.searchFields.FirstName.split(' ');
            if (nameParts.length > 1) {
                return nameParts[0];
            }
            */
            return this.searchFields.FirstName;
        }
        return '';
    }
    get lastName() {
        /*
        if (this.searchFields && this.searchFields.FirstName) {
            let nameParts = this.searchFields.FirstName.split(' ');
            if (nameParts.length > 1) {
                return nameParts[1];
            }
            return '';
        }
        */
        if (this.searchFields && this.searchFields.LastName) {
            return this.searchFields.LastName;
        }
        return '';
    }
    get nationalId() {
        if (this.searchFields && this.searchFields.NationalIdentityNumber) {
            return this.searchFields.NationalIdentityNumber;
        }
        return '';
    }
    get email() {
        if (this.searchFields && this.searchFields.Email) {
            return this.searchFields.Email;
        }
        return '';
    }
    get phone() {
        if (this.searchFields && this.searchFields.Phone) {
            return this.searchFields.Phone;
        }
        return '';
    }


    //FF Customer Creation - Pack1/2 - Interface Check
    clear(event) {
        let selectedPill = {value: event.detail.name, label: event.detail.name};
        this.contactChannelOptionList.push(selectedPill);
        this.selectedContactChannel = this.selectedContactChannel.filter(channelOption => {
            return channelOption.value !== selectedPill.value;
        });
    }
    //FF Customer Creation - Pack1/2 - Interface Check
    get disabled() {
        let disabled = {};
        disabled.firstName = !!(this.searchFields && this.searchFields.FirstName && this.disableFilled);
        disabled.lastName = !!(this.searchFields && this.searchFields.LastName && this.disableFilled);
        disabled.nationalId = !!(this.searchFields && this.searchFields.NationalIdentityNumber && this.disableFilled);
        disabled.email = !!(this.searchFields && this.searchFields.Email && this.disableFilled);
        disabled.phone = !!(this.searchFields && this.searchFields.Phone && this.disableFilled);
        return disabled;
    }

    connectedCallback() {
        //FF Customer Creation - Pack1/2 - Interface Check
        this.contactChannelList();
        //FF Customer Creation - Pack1/2 - Interface Check
        notCacheableCall({
            className: 'MRO_LC_IndividualEdit',
            methodName: 'initData'
        }).then((responseData) => {
            if (responseData.isError) {
                error(this, JSON.stringify(responseData.error));
            } else {
                this.isSaveUnIdentifiedInterlocutorAllowed = responseData.data.isSaveUnIdentifiedInterlocutorAllowed;
            }
        }).catch((errorMsg) => {
            error(this, errorMsg.body.output.errors[0].message);
        });
    }

    /**
     * @description Method to make call server for saving new Individual
     * @param {*} event 
     */
    //FF Customer Creation - Pack1/2 - Interface Check
    contactChannelList() {
        notCacheableCall({
            className: "MRO_LC_IndividualEdit",
            methodName: "getOptionList"
        }).then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                } else {
                    this.contactChannelOptionList = response.data.contactChannelOptionList;
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg.body.output.errors[0].message);
            });
    }
    //FF Customer Creation - Pack1/2 - Interface Check

    handleSave() {
        this.showLoadingSpinner = true;
        if (this.validateFields() === false) {
            let nationalId = this.template.querySelector('[data-id="NationalIdentityNumber__c"]').value;
            if (this.interactionId && this.isSaveUnIdentifiedInterlocutorAllowed && !nationalId && !this.disableFilled) {
                error(this, this.labels.fistnameOrLastnameRequired);
            } else {
                error(this, this.labels.requiredFields);
            }
            this.showLoadingSpinner = false;
        } else {
            let NationalIdentityNumberField =  this.template.querySelector('[data-id="NationalIdentityNumber__c"]');
            let NationalIdentityNumber = NationalIdentityNumberField.value;
            let foreignCitizenField =  this.template.querySelector('[data-id="ForeignCitizen__c"]');
            let foreignCitizen =  foreignCitizenField.value;
            if(foreignCitizen !== true) {
                let check = checkCNP(NationalIdentityNumber);
                if (!check.outCome) {
                    error(this, this.labels.invalidCnp.replace('{0}', check.errorCode).replace('{1}', NationalIdentityNumber));
                    NationalIdentityNumberField.classList.add('slds-has-error');
                    this.showLoadingSpinner = false;
                    return;
                }
            }

            this.populateInterlocutor();
            let inputs = { interactionId: this.interactionId, interlocutorDTO: this.interlocutor.toString() };
            notCacheableCall({
                className: 'MRO_LC_IndividualEdit',
                methodName: 'saveIndividual',
                input: inputs
            }).then((response) => {
                if (response.isError) {
                    this.showLoadingSpinner = false;
                    error(this, response.error);
                } else {
                    if (response.data.duplicateIndividuals) {
                        warn(this, this.labels.duplicatedRecord);
                        this.dispatchEvent(new CustomEvent('duplicatefound', { detail: {
                            'type': 'search',
                            'individualList': response.data.duplicateIndividuals,
                            'searchFields': this.searchFields
                        }}, {bubbles: true}));
                    } else {
                        const individualId = response.data.individualId;
                        if (individualId) {
                            success(this, `${this.interlocutor.firstName} ${this.interlocutor.lastName} ` + this.labels.individualCreated);
                            if (this.isRedirectAllowed !== 'false') {
                                this.addEventListener('success', this.handleNavigateToRecord(individualId));
                            }
                            this.dispatchEvent(new CustomEvent('success', {
                                detail: {
                                    'individualId': individualId
                                }
                            }, {bubbles: true}));
                        } else if (!individualId && this.interactionId && this.isSaveUnIdentifiedInterlocutorAllowed && !this.disableFilled) {
                            this.dispatchEvent(new CustomEvent('success', {
                                detail: {
                                    'individualId': null
                                }
                            }, {bubbles: true}));
                        }
                    }
                    this.showLoadingSpinner = false;
                }
            }).catch((errorMsg) => {
                this.showLoadingSpinner = false;
                error(this, errorMsg.body.output.errors[0].message);
            });
        }
    }

    handleCancel() {
        if(this.isConnectionRepresentative){
            this.dispatchEvent(new CustomEvent('cancel'));
        } else {
        if(this.isRedirectAllowed !== 'false'){
            this.addEventListener('cancel', this.handleNavigateToRecord(this.recordId));
        }
        this.dispatchEvent(new CustomEvent('cancel', {
            detail: {
                'searchFields': this.searchFields
            }
        }));
        }
    }

    handleBack() {
        if(this.isConnectionRepresentative){
            this.dispatchEvent(new CustomEvent('cancel'));
        }
        else {
        this.dispatchEvent(new CustomEvent('back', {
            detail: {
                'page': 'individualList',
                'searchFields': this.searchFields
            }
        }));
        }
    }

    /**
     * @description Method to make call server for updating an existing Individual
     * @param {*} event 
     */
    handleUpdate() {
        if (this.validateFields() === false) {
            error(this, this.labels.requiredFields);
        } else {
            this.interlocutor.id = this.recordId;
            this.populateInterlocutor();
            let inputs = { interactionId: this.interactionId, interlocutorDTO: this.interlocutor.toString() };
            notCacheableCall({
                className: 'MRO_LC_IndividualEdit',
                methodName: 'updateIndividual',
                input: inputs
            })
                .then((response) => {
                    console.log(' ***** '+JSON.stringify(response));
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    } else {
                        const individualId = response.data.individualId;
                        if (individualId) {
                            success(this, `Individual ${this.interlocutor.firstName}  ${this.interlocutor.lastName}  was updated!`);
                            if(this.isRedirectAllowed !== 'false'){
                                this.addEventListener('success', this.handleNavigateToRecord(individualId));
                            }
                            this.dispatchEvent(new CustomEvent('success', { bubbles: true }));
                        }
                    }
                })
                .catch((errorMsg) => {
                    error(this, errorMsg.body.output.errors[0].message);
                });
        }
    }

    /**
     * @description Handle change input
     *              Remove error class on input field if required and not filled
     * @param {*} event 
     */
    handleChangeInput(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
        if (inputCmp.fieldName === 'FirstName') {
            this.interlocutor.firstName = inputCmp.value;
        } else if (inputCmp.fieldName === 'LastName') {
            this.interlocutor.lastName = inputCmp.value;
        } else if (inputCmp.fieldName === 'NationalIdentityNumber__c') {
            this.interlocutor.nationalId = inputCmp.value;
        } else if (inputCmp.fieldName === 'ForeignCitizen__c') {
            this.interlocutor.ForeignCitizen = inputCmp.value;
        } else if (inputCmp.fieldName === 'Gender__c') {
            this.interlocutor.gender = inputCmp.value;
        } else if (inputCmp.fieldName === 'BirthCity__c') {
            this.interlocutor.birthCity = inputCmp.value;
        } else if (inputCmp.fieldName === 'BirthDate') {
            this.interlocutor.birthDate = inputCmp.value;
            //FF Customer Creation - Pack1/2 - Interface Check
        } else if (inputCmp.fieldName === 'IDDocEmissionDate__c') {
            this.interlocutor.iDDocEmissionDate = inputCmp.value;
        } else if (inputCmp.fieldName === 'IDDocValidityDate__c') {
            this.interlocutor.iDDocValidityDate = inputCmp.value;
        } else if (inputCmp.fieldName === 'IDDocumentNr__c') {
            this.interlocutor.iDDocumentNr = inputCmp.value;
        } else if (inputCmp.fieldName === 'IDDocumentType__c') {
            this.interlocutor.iDDocumentType = inputCmp.value;
        } else if (inputCmp.fieldName === 'IDDocumentSeries__c') {
            this.interlocutor.iDDocumentSeries = inputCmp.value;
        }else if (inputCmp.fieldName === 'IDDocIssuer__c') {
            this.interlocutor.iDDocIssuer = inputCmp.value;
        }
        //FF Customer Creation - Pack1/2 - Interface Check
        if (inputCmp.name === 'email') {
            this.interlocutor.email = inputCmp.value;
        } else if (inputCmp.name === 'tel') {
            this.interlocutor.phone = inputCmp.value;
            //FF Customer Creation - Pack1/2 - Interface Check
        }else if (inputCmp.name === 'contactChannel') {

           let selectedChannel = {value: event.detail.value, label: event.detail.value};
            this.selectedContactChannel.push(selectedChannel);
            this.contactChannelOptionList = this.contactChannelOptionList.filter(channelOption => {
                return channelOption.value !== selectedChannel.value;
            });

            let contactChannelList = [];
            this.selectedContactChannel.forEach(element => {
                contactChannelList.push(element.value);
            });
            this.contactChannelListString = contactChannelList.toString();
            console.log('contactChannelList: ' + contactChannelList);
            this.interlocutor.contactChannel = this.contactChannelListString;
        } else if (inputCmp.name === 'mobile') {
            this.interlocutor.mobile = inputCmp.value;
        }
        //FF Customer Creation - Pack1/2 - Interface Check
    }

    /**
     * @description Method to check all input fields ,with custom class wr-required, if are filled or not
     * @returns true if all fields are valid otherwise false
     */
    validateFields = () => {
        let areValid = true;
        let nationalId = this.template.querySelector('[data-id="NationalIdentityNumber__c"]').value;
        if (this.interactionId && this.isSaveUnIdentifiedInterlocutorAllowed && !nationalId && !this.disableFilled) {
            let firstname = this.template.querySelector('[data-id="FirstName"]').value;
            let lastname = this.template.querySelector('[data-id="LastName"]').value;
            if (!firstname && !lastname) {
                return false;
            }
            return true;
        }
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };



    populateInterlocutor() {
        this.interlocutor.firstName = this.template.querySelector('[data-id="FirstName"]').value;
        this.interlocutor.lastName = this.template.querySelector('[data-id="LastName"]').value;
        this.interlocutor.nationalId = this.template.querySelector('[data-id="NationalIdentityNumber__c"]').value;
        this.interlocutor.email = this.template.querySelector('[data-id="Email"]').value;
        this.interlocutor.phone = this.template.querySelector('[data-id="Phone"]').value;
        this.interlocutor.gender = this.template.querySelector('[data-id="Gender__c"]').value;
        this.interlocutor.birthDate = this.template.querySelector('[data-id="BirthDate"]').value;
        this.interlocutor.birthCity = this.template.querySelector('[data-id="BirthCity__c"]').value;
        //FF Customer Creation - Pack1/2 - Interface Check
        this.interlocutor.contactChannel = this.contactChannelListString;
        this.interlocutor.mobile = this.template.querySelector('[data-id="Mobile"]').value
        //FF Customer Creation - Pack1/2 - Interface Check
    }

    /**
     * Navigates to new individual record page 
     *
     * @param String individualId new Individual Id
     */
    handleNavigateToRecord(individualId) {
        if (individualId) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: individualId,
                    objectApiName: INDIVIDUAL_OBJECT,
                    actionName: 'view'
                }
            });
        } else {
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'Individual',
                    actionName: 'list'
                }
            });
        }
    }
    @api
    saveContact() {
        this.submitForm();
    }

    submitForm() {
        let fields = {};
        this.template.querySelectorAll("lightning-input-field").forEach(element => {
            fields[element.fieldName] = element.value;
        });
        if (this.validateContactFields()) {
            if(this.accountRecord.IsPersonAccount){
                this.handleCreateContactAccountRelations(fields)
            }
            if (!this.accountRecord.IsPersonAccount) {
                fields.AccountId = this.accountId;
                this.template.querySelector("lightning-record-edit-form").submit(fields);
            }
        }
    }
    handleSuccess(event) {
        let payload = JSON.parse(JSON.stringify(event.detail));
        this.contactId = payload.id;
            this.getContactRecord(this.contactId);
    }
    getContactRecord(contactId){
        let inputs = {
            contactId: contactId
        };
        notCacheableCall({
            className: "MRO_LC_ConnectionRepresentant",
            methodName: "getContactById",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        let contactRecord  = apexResponse.data.contactRecord;
                        const successEvent = new CustomEvent("savedconnection", {
                            detail: {
                                contactRecord: contactRecord
                            }
                        });
                        this.dispatchEvent(successEvent);
                    }
                }
            });
    }
    handleCreateContactAccountRelations(fields) {
        let accountRecord = {};
        accountRecord.FirstName = fields.FirstName;
        accountRecord.LastName = fields.LastName;
        accountRecord.PersonEmail = fields.Email;
        accountRecord.PersonMobilePhone = fields.Phone;
        accountRecord.NationalIdentityNumber__pc = fields.NationalIdentityNumber__c;
        let inputs = {
            accountId: this.accountRecord.Id,
            fields: JSON.stringify(accountRecord)
        };
        notCacheableCall({
            className: "MRO_LC_ConnectionRepresentant",
            methodName: "createContactAccountRelations",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.data.error) {
                        error(this, apexResponse.data.errorMsg);
                        return;
                    } else {
                        this.contactId = apexResponse.data.contactId;
                        this.getContactRecord(this.contactId);
                    }
                }
            });
    }
    validateContactFields() {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll("lightning-input-field"));
        requireFields.forEach((inputRequiredCmp) => {
            if (inputRequiredCmp.required) {
                let valueInput = inputRequiredCmp.value;
                if (!valueInput || (isNaN(valueInput) && valueInput.trim() === "")) {
                    inputRequiredCmp.classList.add("slds-has-error");
                    error(this, this.labels.requiredFields);
                    areValid = false;
                    return;
                }
            }
        });
        return areValid;
    }
}