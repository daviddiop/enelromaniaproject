/**
 * Created by BADJI on 11/07/2019.
 */

import { LightningElement, track, api } from 'lwc';
import {labels} from 'c/labels';
import { error } from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class productConfigurator extends LightningElement {

    @track rendered ;
    @api productList;
    @api productFamilyList;
    @track emptyProductOption;
    @track nameProductFamily ;
    labels = labels;


    @api
    open() {
        this.rendered = true;
    }

    close(){
        this.rendered = false;
    }
    addToCart(){
        this.rendered = false;
        const selectedEventLWC = new CustomEvent('selectproduct', {
            detail: {
                action: 'addToCart',
                product: this.productList
            }
        });
        this.dispatchEvent(selectedEventLWC);
    }
    /*confirmSelection(event){
        let poIndex = event.currentTarget.getAttribute("data-row-index");
        if (!poIndex) {
            return;
        }
        let familyPoList = this.familyProdList;
        let familyPo = familyPoList[poIndex];

        let selectedProductIds = familyPo.productList.reduce(this.reduceProductList(productIds, product));
        if (selectedProductIds.length < familyPo.min || selectedProductIds.length > familyPo.max) {
            error(this, labels.selectionsOutOfRange);
            return;
        }

         notCacheableCall({
            className: 'ProductCnt',
            methodName: 'listProductWithOptions',
            input: {
                accountId: selectedProductIds,
            },
        }).then(apexResponse => {
            if (apexResponse) {
                let result = this.productList.concat(apexResponse);
                this.productList = result;
                familyPoList.splice(poIndex, 1);
                this.productFamilyList = familyPoList;
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }*/

    reduceProductList(productIds, product){
        if (product.selected) {
            productIds.push(product.id);
            return productIds;
        } else {
            return null
        }

    }

}