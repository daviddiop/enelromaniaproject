/**
 * Created by Stefano Porcari on Jan 21,2020.
 */
import { LightningElement, track,api} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from "c/mroLabels";
import {error} from 'c/notificationSvc';

export default class MroCommoditySelection extends LightningElement {
    @api selectedValue;
    @track options = [];
    @track checkedOptions = [];
    @api disabled = false;
    labels = labels;
    @track placeHolder='';


    connectedCallback() {
        this.initialize();
    }

    initialize() {
        this.placeHolder = this.labels.selectCommodity;
        notCacheableCall({
            className: 'MRO_LC_CommoditySelection',
            methodName: 'getCommodityPicklist',
            input: {
                //'opportunityId': this.opportunityId,
            },
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    if (response.data.options) {
                        response.data.options.forEach(option => { option.checked = (option.value== this.selectedValue);
                        console.log(option.value+"    checked==="+option.checked + "  select=="+this.selectedValue)});
                        this.options = response.data.options;
                    }
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    // handle the selected value
    handleChange(event) {
        this.selectedValue = event.target.value;
        this.options.forEach(option => { option.checked = (option.value== this.selectedValue);});
        this.dispatchEvent(new CustomEvent('selected', { detail: {selectedCommodity :this.selectedValue} }));
    }
}