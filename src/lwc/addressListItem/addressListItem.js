import { LightningElement, api } from 'lwc';

export default class AddressListItem extends LightningElement {

    @api
    address


    get toString() {

        let lines = [];
        let line1 = [];
        if (this.address.streetType) line1.push(this.address.streetType);
        if (this.address.streetName) line1.push(this.address.streetName);
        if (this.address.streetNumber) line1.push(this.address.streetNumber);
        if (this.address.streetNumberExtn) line1.push(this.address.streetNumberExtn);
        lines.push(line1.join(' '));

        if (this.address.city || this.address.locality || this.address.postalCode) {
            let line2 = [];
            if (this.address.city) line2.push(this.address.city);
            if (this.address.province) line2.push(this.address.province);
            if (this.address.postalCode) line2.push(this.address.postalCode);
            lines.push(line2.join(' '));
        }
        return lines.join(', ');
    }

    handleSelection() {
        this.dispatchEvent(new CustomEvent('select', { bubbles: false, detail: { selected: this.address } }));
    }
}