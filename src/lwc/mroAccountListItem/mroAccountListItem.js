import { LightningElement, api, track } from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import { labels } from 'c/labels';
import { error } from 'c/notificationSvc';

export default class MroAccountListItem extends LightningElement {
    @api account;
    @api interactionId;

    @api blockSaveCustomerInteractionOnSelect = false;

    labels = labels;

    handleSelection() {
        if (!this.interactionId) {
            this.selectedAccount = this.account;
            this.dispatchEvent(new CustomEvent('selection'));
        } else {
            if(!this.blockSaveCustomerInteractionOnSelect){
                notCacheableCall({
                    className: 'MRO_LC_Account',
                    methodName: 'isInterlocutorExist',
                    input: { customerId: this.account.Id, interactionId: this.interactionId },
                })
                    .then((response) => {
                        if (response.isError) {
                            error(this, response.error);
                            //this.accountContactRelations = [];
                        }
                        else {
                            this.dispatchEvent(new CustomEvent('selection', {
                                detail: {
                                    'selectedAccountId': this.account.Id,
                                    'customerInteractionId': response.data.customerInteractionId,
                                    'interlocutorId': response.data.interlocutorId
                                },
                                bubbles: true
                            }));
                        }
                    })
                    .catch(errorMessage => {
                        error(this, errorMessage);
                    });
            } else {
                this.dispatchEvent(new CustomEvent('selection', {
                    detail: {
                        'selectedAccountId': this.account.Id,
                    },
                    bubbles: true
                }));
            }
        }
    }

    get isBusiness() {
        return !this.account.IsPersonAccount;
    }

    get isPerson() {
        return this.account.IsPersonAccount;
    }
}