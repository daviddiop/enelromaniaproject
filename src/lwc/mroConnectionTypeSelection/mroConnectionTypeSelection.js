/**
 * Created by Octavian on 1/29/2020.
 * Refactoring by Goudiaby on 18/04/2020
 */

import {LightningElement, track, api} from "lwc";
import {NavigationMixin} from "lightning/navigation";
import Id from "@salesforce/user/Id";
import {labels} from "c/labels";
import {error, success} from "c/notificationSvc";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";


export default class MroConnectionTypeSelection extends NavigationMixin(
    LightningElement) {
    @api dossierId;
    @api
    get recordTypeId(){
        return this._recordTypeId;
    }
    set recordTypeId(value){
        this._recordTypeId = value;
    }
    @api disabled = false;

    @track listConnectionType = null;
    @track connectionTypeId = "";
    @track connectionTypeName = "";
    @track _recordTypeId ;

    labels = labels;

    @api
    reload() {
        this.loadConnectionTypeData()
    }

    connectedCallback() {
        //this.loadConnectionTypeData();
    }

    /*loadConnectionTypeData() {
        console.log("DossierId = " + this.dossierId);
        if (this.dossierId) {
            let inputs = {dossierId: this.dossierId};
            notCacheableCall({
                className: "MRO_LC_ConnectionTypeSelection",
                methodName: "getConnectionTypeList",
                input: inputs
            })
                .then(response => {
                    if (response) {
                        if (response.isError) {
                            error(this, response.errorMsg);
                        } else {
                            let picklistValues = Object.values(response.data.pickListValues);
                            let options = [];
                            for (let i = 0; i < picklistValues.length; i++) {
                                options.push({label: picklistValues[i], value: picklistValues[i]});
                                console.log(picklistValues[i]);
                            }

                            this.listConnectionType = options;
                            if (response.data.initialValue) {
                                this.connectionTypeId = response.data.initialValue;
                                this.sendEvent(true);
                            }
                        }
                    }
                })
                .catch(errorMsg => {
                    error(this, errorMsg);
                });
        }
    }*/

    handleChange(event) {
        this.connectionTypeId = event.target.value;
        this.sendEvent(false);
    }

    sendEvent(isInitialValue) {
        console.log("ConnectionTypeID = " + this.connectionTypeId);
        const changedEvent = new CustomEvent("changed", {
            detail: {
                connectionTypeId: this.connectionTypeId,
                isInitialValue: isInitialValue
            }
        });
        this.dispatchEvent(changedEvent);
    }
}