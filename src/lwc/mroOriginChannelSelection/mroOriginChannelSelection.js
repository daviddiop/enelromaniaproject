/**
 * Created by goudiaby on 07/01/2020.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import cacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.cacheableCall";
import {error} from "c/notificationSvc";
import {labels} from "c/mroLabels";

export default class MroOriginChannelSelection extends LightningElement {
    labels = labels;
    @api objectApiName = '';
    @api disabled;
    @track _selectedOrigin = '';
    @track _selectedChannel = '';
    _mapOriginChannel;
    _originChannelEntries;
    _fieldManageability = false;

    @api
    get fieldManageability() {
        return this._fieldManageability;
    }

    set fieldManageability(value) {
        this._fieldManageability = value;
    }

    @api
    get selectedOrigin() {
        return this._selectedOrigin;
    }

    set selectedOrigin(value) {
        this._selectedOrigin = value;
        if (this._fieldManageability) {
            if (!this._originChannelEntries) {
                this.getOriginChannelEntries().then();
            }
        }
    }

    @api
    get selectedChannel() {
        return this._selectedChannel;
    }

    set selectedChannel(value) {
        this._selectedChannel = value;
        if (!this._selectedOrigin) {
            this._selectedChannel = null;
            return;
        }
        this.disabledChannel = false;
        this.getPicklistDependency();
    }

    @track originList;
    @track channelList;
    @track disabledOrigin = false;
    @track disabledChannel = true;
    @track showSpinner = true;

    connectedCallback() {
        this.retrieveOptions();
    }

    renderedCallback() {
        if (this.disabled) {
            this.disabledChannel = true;
            this.disabledOrigin = true;
        }
    }

    retrieveOptions() {
        notCacheableCall({
            className: "MRO_LC_OriginChannelSelection",
            methodName: "OriginChannelPicklistOptions",
            input: {
                'objectApiName': this.objectApiName
            }
        }).then(response => {
            if (response.isError){
                error(this, response.error);
                return;
            }
            //Spring 20 Apex response non-extensible with additional parameters.
            let result = Object.assign({}, response.data);
            let selectableEntries;
            ({
                originChannelEntries: selectableEntries,
                originChannelOptions: this._mapOriginChannel
            } = result);

            let originOptions = [];
            originOptions.push({value: '', label: this.labels.nonePicklist});
            Object.keys(this._mapOriginChannel).forEach(function (value) {
                if (selectableEntries.hasOwnProperty(value) && !selectableEntries[value]) {
                    originOptions.push({
                        label: value,
                        value: value
                    });
                }
            });
            this.originList = originOptions;
            if (this.selectedOrigin) {
                this._selectedOrigin = this.selectedOrigin;
                this.getPicklistDependency();
            }
            if (this.selectedChannel) {
                this._selectedChannel = this.selectedChannel;
            }
            this.showSpinner = false;
        }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg));
        });
    }

    getPicklistEntries() {
        cacheableCall({
            className: "MRO_LC_OriginChannelSelection",
            methodName: "GetPicklistEntries",
            input: {}
        }).then(response => {
            if (response.isError){
                error(this, response.error);
                return;
            }
            let result = Object.assign({}, response.data);
            let {originChannelPicklistEntries} = result;
            let originChannelEntries = {};
            originChannelPicklistEntries.forEach(function (field) {
                const nestedContainer = {};
                nestedContainer.QualifiedApiName = field["QualifiedApiName"];
                nestedContainer.isNotSelectable = field["isNotSelectable__c"];
                originChannelEntries[field["MasterLabel"]] = nestedContainer;
            });
            this._originChannelEntries = originChannelEntries;
        }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg));
        });
    }

    handleChangeOrigin(event) {
        this._selectedOrigin = event.target.value;
        this.disabledChannel = true;
        if (this._selectedOrigin === '') {
            this._selectedOrigin = null;
            this._selectedChannel = null;
        } else {
            this.getPicklistDependency();
        }
        this.addOriginChannelSelectionEvt();
    }

    getPicklistDependency() {
        let originSelected = this._selectedOrigin;
        console.log('MroOriginChannelSelection.getPicklistDependency - originSelected', originSelected);
        let channelList = this._mapOriginChannel && this._mapOriginChannel.hasOwnProperty(originSelected) ? this._mapOriginChannel[originSelected] : [];
        console.log('MroOriginChannelSelection.getPicklistDependency - channelList', channelList);
        this.enforceOrigin(originSelected);
        if (channelList && channelList.length > 0) {
            this.channelList = channelList;
            this.disabledChannel = false;
        }
        if (this._fieldManageability && this._originChannelEntries) {
            this.disabledOrigin = this._originChannelEntries[this._selectedOrigin].isNotSelectable;
        }
        if (this.disabled) {
            this.disabledChannel = true;
            this.disabledOrigin = true;
        }
    }

    enforceOrigin(selectedOrigin) {
        let originAlreadyExists = false;
        if (this.originList) {
            for (let element of this.originList) {
                if (element.value === selectedOrigin) {
                    originAlreadyExists = true;
                    break;
                }
            }
            if (!originAlreadyExists) {
                this.originList.push({value: selectedOrigin, label: selectedOrigin});
            }
        }
    }

    handleChangeChannel(event) {
        this._selectedChannel = event.target.value;
        this.addOriginChannelSelectionEvt();
    }

    @api
    enableChannel() {
        this.disabledChannel = false;
        this.retrieveOptions();
    }

    @api
    disableOriginChannel() {
        this.disabledOrigin = true;
        this.disabledChannel = true;
    }

    @api
    disableOrigin() {
        this.disabledOrigin = true;
    }

    @api
    enableOriginChannel(originFromInteraction) {
        this.disabledOrigin = !!originFromInteraction;
        this.disabledChannel = false;
    }

    getOriginChannelEntries = async () => {
        try {
            await this.getPicklistEntries();
        } catch (error) {
            error(this, 'Origin Channel component' + error);
        }
    };

    addOriginChannelSelectionEvt() {
        const originChannelEvt = new CustomEvent('selectionoriginchannel', {
            detail: {
                'selectedOrigin': this._selectedOrigin,
                'selectedChannel': this._selectedChannel
            }
        });
        this.dispatchEvent(originChannelEvt);
    }
}