import { LightningElement, api, track } from 'lwc';
import { labels } from 'c/labels';
import { error } from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import { NavigationMixin } from "lightning/navigation";

export default class MroAccountContactRelationsList extends NavigationMixin(LightningElement) {
    labels = labels;

    @api customerInteractionId;
    @api isRedirectAllowed;
    @api interactionId;
    @api showCancelButton;

    @track _customerId;
    @track account;
    @track _individualId;
    @track individual;
    @track customerInteraction;
    @track accountContactRelations;
    @track address;

    @api
    get customerId() {
        return this._customerId;
    }

    set customerId(value) {
        notCacheableCall({
            className: 'AccountContactRelationCnt',
            methodName: 'listAccountContactRelationByAccountId',
            input: {
                accountId: value,
            },
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                    this.accountContactRelations = [];
                }
                else {
                    this._customerId = value;
                    this.accountContactRelations = response.data.relationList;
                    this.account = response.data.account;
                    this.address = this.account.ResidentialStreetType__c+'-'+this.account.ResidentialStreetName__c
                        +'-'+this.account.ResidentialStreetNumber__c+'-'+this.account.ResidentialCity__c;
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    @api
    get individualId() {
        return this._individualId;
    }

    set individualId(value) {
        notCacheableCall({
            className: "AccountContactRelationCnt",
            methodName: "listAccountContactRelationByIndividualId",
            input: {
                'individualId': value
            }
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                    this.accountContactRelations = [];
                } else {
                    this.accountContactRelations = response.data.relations;
                    this.individual = response.data.individual;
                    this._individualId = value;
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    get from() {
        if (this.account) {
            return 'Account';
        }
        return 'Contact';
    }

    getSelectedAccountContactRelation(evt) {
        let input, method;
        if (this.account) {
            input = {
                contactId: evt.target.accountContactRelation.contactId,
                customerInteractionId: this.customerInteractionId
            };
            method = "assignContactToInteraction";
        } else if (this.individual) {
            input = {
                contactId: evt.target.accountContactRelation.contactId,
                customerId: evt.target.accountContactRelation.accountId,
                interactionId: this.interactionId
            };
            method = "addCustomerInteraction";
        }
        if (input) {
            notCacheableCall({
                className: "AccountContactRelationCnt",
                methodName: method,
                input: input
            }).then(response => {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.customerInteraction = response.data.customerInteraction;
                    let newCustomerInteractionId = null;
                    if (this.customerInteraction && this.customerInteraction.Id) {
                        newCustomerInteractionId = this.customerInteraction.Id;
                    } else {
                        newCustomerInteractionId = this.newCustomerInteractionId;
                    }
                    this.dispatchEvent(new CustomEvent("select", {
                        detail: {
                            'customerInteractionId': newCustomerInteractionId
                        }
                    }));
                }
            }).catch(errorMsg => {
                error(this, errorMsg);
            });
        } else {
            this.dispatchEvent(new CustomEvent("select", {
                detail: {
                    'customerInteractionId': this.customerInteractionId
                }
            }));
        }

    }

    // goToSearchComponent() {
    //     if (this.isRedirectAllowed !== 'false') {
    //         if (this.account) {
    //             this.addEventListener('newPress', this.navigateTo('c__AccountSearch'));
    //         } else if (this.individual) {
    //             this.addEventListener('newPress', this.navigateTo('c__IndividualSearch'));
    //         }
    //     }
    //     this.dispatchEvent(new CustomEvent("newPress"));
    // }

    cancel() {
        this.dispatchEvent(new CustomEvent("cancel", {
            detail: {
                'from': this.from,
                'customerInteractionId': this.customerInteractionId
            }
        }));
    }

    navigateTo(component) {
        this[NavigationMixin.Navigate]({
            type: "standard__component",
            attributes: {
                componentName: component
            }
        });
    }

    onBack() {
        let page = '';
        if (this.account) {
            page = 'accountList';
        } else if (this.individual) {
            page = 'individualList';
        }
        this.dispatchEvent(new CustomEvent('redirect', {
            detail: {
                'page': page
            }
        }));
    }

    onNew() {
        let page = '';
        if (this.account) {
            page = 'individualSearch';
        } else if (this.individual) {
            page = 'accountSearch';
        }
        this.dispatchEvent(new CustomEvent('redirect', {
            detail: {
                'page': page
            }
        }));
    }
}