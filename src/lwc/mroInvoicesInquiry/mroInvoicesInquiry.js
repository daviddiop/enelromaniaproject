import {api, LightningElement, track} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
import { NavigationMixin } from 'lightning/navigation';

const CLAIMED = 'Claimed';

export default class MroInvoicesInquiry extends NavigationMixin(LightningElement) {

    channelName = '/event/DocumentManagementSystemEvent__e';
    subscription = {};

    @api startDate = 'start';
    @api endDate = 'end';
    @api billingAccountNumber;
    @api customerCode;
    @api hideConfirmSelection;
    @api hideSelectionColumn = false;
    @api showInvoicesAlreadyMarked = false;
    @api isNotMandatory = false;
    @api disableSelectClaimedInvoices = false;
    @api hideTVAColumn = false;
    @api getSelectedInvoices() {
        this.onConfirmSelection();
    }
    @api subProcess;
    @api defaultSortColumn;
    @api defaultSortOrderAsc;
    @api disableSelectionColumn = false;

    @track invoiceList;
    @track invoiceDetailList;
    @track detailListRendered;
    @track selectAll;

    @track showLoadingSpinner = false;
    @track showLoadingSpinnerDetail = false;
    @track showIllustrationEmptyStat = false;
    @track showIllustrationEmptyStatDetail = false;

    labels = labels;
    paymentColumns = [
        { label: 'Payment Date', fieldName: 'paymentDate', type: 'date', sortable: true },
        { label: 'Amount', fieldName: 'paymentValue', type: 'number', sortable: true },
        { label: 'Type', fieldName: 'paymentType', type: 'text', sortable: true },
        { label: 'Document Number', fieldName: 'documentNumber', type: 'text', sortable: true },
        { label: 'Balance', fieldName: 'finalBalance', type: 'number', sortable: true },
        { label: 'Processing Date', fieldName: 'processingDate', type: 'date', sortable: true }
    ];
    billDetailColumns = [
        {label: 'Item', fieldName: 'item', type: 'text'},
        {label: 'Quantity', fieldName: 'quantity', type: 'number'},
        {label: 'Value', fieldName: 'value', type: 'number'},
        {label: 'VAT', fieldName: 'vat', type: 'text'},
        {label: 'Total', fieldName: 'total', type: 'number'},
        {label: 'From', fieldName: 'fromDate', type: 'date'},
        {label: 'To', fieldName: 'toDate', type: 'date'}
    ];
    meterDetailColumns = [
        {label: 'Serial Number', fieldName: 'serialNumber', type: 'text'},
        {label: 'Dial', fieldName: 'dial', type: 'text'},
        {label: 'Average Consumption', fieldName: 'averageConsumption', type: 'number'},
        {label: 'Old Index', fieldName: 'oldIndex', type: 'text'},
        {label: 'Old Index Type', fieldName: 'oldIndexType', type: 'text'},
        {label: 'New Index', fieldName: 'newIndex', type: 'text'},
        {label: 'New Index Type', fieldName: 'newIndexType', type: 'text'},
        {label: 'Quantity', fieldName: 'quantity', type: 'number'},
        {label: 'From', fieldName: 'fromDate', type: 'date'},
        {label: 'To', fieldName: 'toDate', type: 'date'}
    ];

    // Handles subscribe button click
    subscribeDMSEvent() {

        // Callback invoked whenever a new event message is received
        const messageCallback = (response) => {

            console.log('New message received : ', JSON.stringify(response));
            this.payload = JSON.stringify(response);
            // Response contains the payload of the new message received
            this.handleRetrieveInvoices();
        };

        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.channelName, -1, messageCallback).then(response => {

            // Response contains the subscription information on subscribe call
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
            this.subscription = response;
        });
    }

    renderedCallback(){
        if (this.invoiceList) {
            if (this.showInvoicesAlreadyMarked || this.disableSelectClaimedInvoices) {
                this.inhibitionRowsAlreadyMarked();
            }
            let self = this;
            this.invoiceList.map(function (invoice) {
                invoice['disabled'] = self.disableSelectionColumn || invoice.isClaimed;
            });
        }
    }

    handleRetrieveInvoices() {
        this.showLoadingSpinner = true;
        notCacheableCall({
            className: "MRO_LC_InvoicesInquiry",
            methodName: "listInvoice",
            input: {
                'startDate': this.startDate,
                'endDate': this.endDate,
                'billingAccountNumber': this.billingAccountNumber,
                'customerCode': this.customerCode,
                'subProcess': this.subProcess
            }
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, this.labels.unexpectedError);
            }
            if (apexResponse.data.error) {
                error(this, apexResponse.data.errorMsg);
            }
            if (!apexResponse.data.error && apexResponse.data.invoiceList){
                this.invoiceList = JSON.parse(JSON.stringify(apexResponse.data.invoiceList));
             }
//            this.invoiceList = JSON.parse(JSON.stringify(apexResponse.data));
            this.invoiceList = [];
            let returnValueFromApex = JSON.parse(JSON.stringify(apexResponse.data));
            let invoiceList = returnValueFromApex.invoiceList;
            console.log('invoiceList: ', JSON.stringify(invoiceList));
            let contentDocumentMap = returnValueFromApex.contentVersionMap;
            console.log('contentDocumentMap: ', JSON.stringify(contentDocumentMap));
            for (let i = 0; i < invoiceList.length; i++) {

                let invoiceFromList = invoiceList[i];
                let invoice = new Object();
                invoice.selected = false;
                invoice.invoiceId = invoiceFromList.invoiceId;
                invoice.invoiceSerialNumber = invoiceFromList.invoiceSerialNumber;
                invoice.invoiceNumber = invoiceFromList.invoiceNumber;
                invoice.status = invoiceFromList.status;
                invoice.invoiceType = invoiceFromList.invoiceType;
                invoice.invoiceIssueDate = invoiceFromList.invoiceIssueDate;
                invoice.invoiceDate = invoiceFromList.invoiceDate;
                invoice.total = invoiceFromList.total;
                invoice.balance = invoiceFromList.balance;
                invoice.tva = invoiceFromList.tva;
                invoice.claimed = invoiceFromList.claimed;
                invoice.isClaimed = invoiceFromList.isClaimed;
                invoice.rescheduling = invoiceFromList.rescheduling;
                invoice.paymentList = invoiceFromList.paymentList;
                invoice.fileNetId = invoiceFromList.fileNetId;
                invoice.valueWithoutVAT = invoiceFromList.valueWithoutVAT;
                invoice.penalty = invoiceFromList.penalty;
                invoice.recoveryAction = invoiceFromList.recoveryAction;
                invoice.showSpinner = false;

                if (contentDocumentMap != undefined) {

                    invoice.showDownload = false;
                    invoice.showSync = true;
                }
                for (let invoiceId in contentDocumentMap){
                    if (invoiceId == invoice.invoiceId) {
                        invoice.showDownload = true;
                        invoice.showSync = false;
                        let contentDocumentId = contentDocumentMap[invoiceId];
                        invoice.contentDocumentId = contentDocumentId;
                        invoice.previewhref = '/lightning/r/ContentDocument/' + contentDocumentId + '/view';
                        invoice.downloadhref = '/sfc/servlet.shepherd/document/download' + '/' + contentDocumentId;
                    }
                }
                this.invoiceList.push(invoice);
            }
            let invoices = this.invoiceList;
            this.showIllustrationEmptyStat = !this.invoiceList || this.invoiceList.length === 0;
            if(this.defaultSortColumn){
                let orderVal = this.defaultSortOrderAsc ? 1 : -1;
                this.invoiceList = invoices.sort((a, b) => {
                    return a[this.defaultSortColumn] < b[this.defaultSortColumn] ? orderVal : (a[this.defaultSortColumn] > b[this.defaultSortColumn] ? orderVal * -1 : 0);
                });
            }else {
                this.invoiceList = invoices;
            }

        }).catch((errorMsg) => {
                     this.showLoadingSpinner = false;
                     error(this, JSON.stringify(errorMsg));
         });
    }

    handleFilePreviewClick (event) {

        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
            pageName: 'filePreview'
            },
            state : {

             selectedRecordId:event.currentTarget.name
            }
        })
    }

    handleSync (event) {

        let invoiceId = event.target.id.split("-")[0];
        let fileNetId = event.target.value;
        let index = event.target.name;
        notCacheableCall({
            className: "MRO_LC_InvoicesInquiry",
            methodName: "getInvoiceFromFilenet",
            input: {
                'invoiceId': invoiceId
            }
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }

            this.invoiceList[index].showSpinner = true;
            this.subscribeDMSEvent();

        });
    }

    showPayments(event) {
        let foundInvoice = this.findInvoice(event.target.dataset.id);
        if (!foundInvoice) {
            return;
        }
        if (foundInvoice.hasOwnProperty('showPayments')) {
            foundInvoice.showPayments = !foundInvoice.showPayments;
        } else {
            foundInvoice['showPayments'] = true;
        }
    }
    showBillDetails(event) {
        let foundInvoiceDetail = this.findInvoiceDetail(event.target.dataset.id);
        if (!foundInvoiceDetail) {
            return;
        }
        if (foundInvoiceDetail.hasOwnProperty('showBillDetails')) {
            foundInvoiceDetail.showBillDetails = !foundInvoiceDetail.showBillDetails;
        } else {
            foundInvoiceDetail['showBillDetails'] = true;
        }
    }
    showMeterDetails(event) {
        let foundInvoiceDetail = this.findInvoiceDetail(event.target.dataset.id);
        if (!foundInvoiceDetail) {
            return;
        }
        if (foundInvoiceDetail.hasOwnProperty('showMeterDetails')) {
            foundInvoiceDetail.showMeterDetails = !foundInvoiceDetail.showMeterDetails;
        } else {
            foundInvoiceDetail['showMeterDetails'] = true;
        }
    }
    showInvoiceDetails(event) {
        var invoiceId = event.target.dataset.id;
        if (!invoiceId) {
            return;
        }
        this.detailListRendered = true;
        this.showLoadingSpinnerDetail = true;
        notCacheableCall({
            className: "MRO_LC_InvoicesInquiry",
            methodName: "getInvoiceDetails",
            input: {
                'invoiceId': invoiceId,
            }
        }).then(apexResponse => {
            this.showLoadingSpinnerDetail = false;
            if (!apexResponse) {
                error(this, this.labels.unexpectedError);
            }
            if (apexResponse.data.error) {
                error(this, apexResponse.data.errorMsg);
            }
            if (!apexResponse.data.error && apexResponse.data.invoiceDetailList){
                let invoice = this.invoiceList.find(v => {return v.invoiceId === invoiceId});
                this.invoiceDetailList = apexResponse.data.invoiceDetailList.map(v => {
                    if(!invoice) return v;
                    if(v.billDetailList){
                        v.billDetailList = v.billDetailList.sort((a, b) => {
                            return a.item < b.item ? -1 : (a.item > b.item ? 1 : 0);
                        })
                    }
                    v.penalties = invoice.penalty;
                    v.ACTRecup = invoice.recoveryAction;
                    return v;
                });
            }
            this.showIllustrationEmptyStatDetail = !this.invoiceDetailList || this.invoiceDetailList.length === 0;
        }).catch((errorMsg) => {
            this.showLoadingSpinnerDetail = false;
            error(this, JSON.stringify(errorMsg));
        });
    }
    isClaimed(event) {
        let foundInvoice = this.findInvoice(event.target.dataset.id);
        if (!foundInvoice) {
            return;
        }
        if (!foundInvoice.hasOwnProperty('isClaimed')) {
            foundInvoice['isClaimed'] = false;
        }
    }
    closeDetailList() {
        this.invoiceDetailList = [];
        this.detailListRendered = false;
    }
    onSelectAll(event) {
        let self = this;
        this.invoiceList.map(function(invoice) {
            if (!invoice.isClaimed){
                invoice.selected = event.target.checked;
            }
            return invoice
        });
    }
    onSelect(event) {
        let foundInvoice = this.findInvoice(event.target.dataset.id);
        if (!foundInvoice) {
            return;
        }
        foundInvoice.selected = event.target.checked;
    }
    findInvoice(invoiceId) {
        if (!invoiceId) {
            return null;
        }
        return this.invoiceList.find(function (invoice) {
            return invoice.invoiceId === invoiceId;
        });
    }
    findInvoiceDetail(invoiceDetailId) {
        if (!invoiceDetailId) {
            return null;
        }
        return this.invoiceDetailList.find(function (invoiceDetail) {
            return invoiceDetail.invoiceDetailId === invoiceDetailId;
        });
    }
    onConfirmSelection() {
        let self = this;
        if (!this.invoiceList) {
            return;
        }
        let invoiceList = this.invoiceList.reduce(function(myInvoiceLists, invoice) {
            if (invoice.selected === true) {
                let invoiceIdAndTotal = {
                    'invoiceId': invoice.invoiceId,
                    'total': invoice.total,
                    'invoiceSerialNumber': invoice.invoiceSerialNumber,
                    'invoiceNumber': invoice.invoiceNumber,
                    'fileNetId': invoice.fileNetId,
                };
                if (!self.disableSelectClaimedInvoices || invoice.status !== CLAIMED){
                    myInvoiceLists.selectedInvoiceList.push(invoiceIdAndTotal);
                }
                if (!invoice.isMarked) {
                    myInvoiceLists.markedInvoiceList.push(invoiceIdAndTotal);
                }
            }
            return myInvoiceLists;
        }, {selectedInvoiceList: [], markedInvoiceList: []});
        if (!invoiceList.selectedInvoiceList.length && !this.isNotMandatory) {
            error(this, 'Invoices didn\'t select.');
        } else {
            console.log(JSON.stringify(invoiceList));
            this.dispatchEvent(new CustomEvent('invoiceselect', {
                detail: {
                    'selectedInvoiceList': invoiceList.selectedInvoiceList,
                    'markedInvoiceList': invoiceList.markedInvoiceList
                }
            }));
        }
    }

    inhibitionRowsAlreadyMarked() {
        let rows = Array.from(this.template.querySelectorAll('tr.slds-hint-parent lightning-input'));
        rows.forEach(checkboxElement => {
            let invoice = this.findInvoice(checkboxElement.dataset.id);
            if (this.showInvoicesAlreadyMarked && invoice && invoice.isMarked){
                checkboxElement.disabled = true;
                checkboxElement.readOnly = true;
                checkboxElement.checked = true;
            }
            if (this.disableSelectClaimedInvoices && invoice && invoice.status === CLAIMED){
                checkboxElement.disabled = true;
                checkboxElement.readOnly = true;
                checkboxElement.checked = false;
            }
        });
    }
}