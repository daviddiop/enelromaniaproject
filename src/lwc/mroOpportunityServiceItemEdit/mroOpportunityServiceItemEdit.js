import {LightningElement, api, track, wire} from 'lwc';
import {getObjectInfo} from 'lightning/uiObjectInfoApi';
import SERVICESITE_OBJECT from '@salesforce/schema/ServiceSite__c';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.cacheableCall";
import init from '@salesforce/apex/MRO_LC_OpportunityServiceItem.initialize';
import getAvailableAddressesCountryRestricted from '@salesforce/apex/MRO_SRV_Address.getAvailableAddressesCountryRestricted';
import {labels} from 'c/mroLabels';
import {error, success, info} from 'c/notificationSvc';

export default class MroOpportunityServiceItemEdit extends LightningElement {

    label = labels;

    get REQUIRED_FIELDS_EE() {

        // Fields always mandatory
        let REQUIRED_FIELDS_EE =
                ['RecordTypeId', 'ServicePointCode__c', 'Distributor__c', 'EstimatedConsumption__c',
                'Commodity__c','SupplyOperation__c'];
        // NLC is mandatory only when field is shown within modal
        if(this.showServiceSiteSection) REQUIRED_FIELDS_EE.push('NLC__c');
        // Voltage fields and caen code are not mandatory in case of placeholder
        if(!this.isPlaceholder) REQUIRED_FIELDS_EE.push(['VoltageSetting__c', 'VoltageLevel__c', 'NACEReference__c']);
        //
        if (this.hasSubProcessNone) REQUIRED_FIELDS_EE.push('Trader__c');
        //
        if (this.isDiscoENEL) REQUIRED_FIELDS_EE.push(['AvailablePower__c','ContractualPower__c','PowerPhase__c']);
        //
        if (this.showNonDisconnectableReason) REQUIRED_FIELDS_EE.push('NonDisconnectableReason__c');
        return REQUIRED_FIELDS_EE;
    }

    get REQUIRED_FIELDS_GM() {

        // Fields always mandatory
        let REQUIRED_FIELDS_GM =
                ['RecordTypeId', 'ServicePointCode__c', 'Distributor__c', 'EstimatedConsumption__c',
                'Commodity__c','ConsumptionCategory__c','SupplyOperation__c'];
        // CLC is mandatory only when field is shown within modal
        if(this.showServiceSiteSection) REQUIRED_FIELDS_GM.push('CLC__c');
        //
        if(!this.isPlaceholder) REQUIRED_FIELDS_GM.push(['Pressure__c', 'NACEReference__c']);
        //
        if (this.hasSubProcessNone) REQUIRED_FIELDS_GM.push('Trader__c');
        return REQUIRED_FIELDS_GM;
    }

    @api typeOfVas = false;

    @api supplyId;
    @api servicePointId;
    @api serviceSiteId;
    @api servicePointCode;
    @api commodity;
    @api supply;

    @api opportunityId;
    @api accountId;
    @api hasTrader = false;
    @api hasSubProcessNone = false;
    @api showEndDate = false;
    @api isNewConnectionFlagReadOnly = false;
    @api hasSameOpportunityServiceItems = false;
    @api hasApplicantNotHolder = false;
    @api recordId;
    @api contract;
    @api product;
    @api contractAccount;
    @api contractId;

    @api readOnlyAddress = false;
    @api readOnlySiteAddress = false;
    @api autoSave = false;
    @api addressForced = false;
    @track addressForcedSite = false;
    @api title;
    @api requestType;
    @api isTemporaryConnection = false;
    @api isNewRecordPointAddress=false;
    @api disabledFields={
        distributor:false,
        availablePower:false,
        contractualPower:false,
        powerPhase:false,
        pressure: false,
        pressureLevel:false,
        consumptionCategory: false,
        voltageSetting:false,
        voltageLevel:false,
        flatRate:false,
        nonDisconnectable: false,
        supplyOperation: false,
        applicantNotHolder: false
    };
    @api hiddenFields={
        isNewConnection:false
    };
    @api isTransferOrProductChange = false;

    @api hasPreCheck = false;
    @api hidePreCheckButton = false;
    @api isNotSelectedOldTrader = false;
    @api isOpenEdit = false;
    @api isPlaceholder = false

    @track disablePreCheckButton = true;
    @track disableSaveButton = true;
    @track disableTrader = false;

    @track isNormalize;
    @track defaultTitle;
    @track reloadFormData=false;
    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
    @track isDiscoENEL = false;
    @track selfReadingEnabled = false;
    @track showNonDisconnectableReason = false;
    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check

    @track recordTypeId;
    electricRecordTypeId;
    gasRecordTypeId;
    serviceRecordTypeId;
    isSupplyStatusActive = false;

    //@track show
    @track spinner;
    @track loads = true;
    @track show = false;

    @track isGas = false;
    @track isElectric = false;
    @track isService = false;
    @track address = [];
    @track addressLoaded = false;
    @track objName;
    @track isCheckedPA = false;
    @track isCheckedSS = false;
    @track submitErrorMessage;
    @track availableAddresses;
    @track mapKWhByConsumptionNames;
    @track isVisibleServiceSites = true;
    @track distributionProvincesDetails;


    get readOnlyFields(){
        if (!this.isVAS){
            return "['country']";
        } else {
            return "[]";
        }
    }
    @track opportunityServiceItems = [];

    siteCLC;
    siteNLC;
    siteDescription;

    servicePoint;
    firstTimeEntry = true;
    @track successBoolean = true;
    @track disableNormalize = false;

    tmpOsi;
    @track distributorId;

    @wire(getObjectInfo, { objectApiName: SERVICESITE_OBJECT })
    serviceSiteObjectInfo;
    normalizedForVas = false;

    get voltageLevelClass() {

        let voltageLevelClass = '';
        voltageLevelClass = !this.isPlaceholder ? 'wr-required' : '';
        return voltageLevelClass;
    }

    get voltageClass() {

        let voltageClass = '';
        voltageClass = !this.isPlaceholder ? 'wr-required' : '';
        return voltageClass;
    }

    @api
    get serviceSiteTableColumns() {
        if (this.serviceSiteObjectInfo && this.serviceSiteObjectInfo.data) {
            const ssFields = this.serviceSiteObjectInfo.data.fields;
            return [
                /*
                {
                    'label': 'Id',
                    'fieldName': 'recordId',
                    'initialWidth': 50
                },
                 */
                {
                    'label': ssFields.SiteAddress__c.label,
                    'fieldName': 'addressString',
                    'initialWidth': 600,
                    'wrapText': true
                },
                {
                    'label': ssFields.CLC__c.label,
                    'fieldName': 'clc',
                    'initialWidth': 150
                },
                {
                    'label': ssFields.NLC__c.label,
                    'fieldName': 'nlc',
                    'initialWidth': 150
                },
                {
                    'label': ssFields.Description__c.label,
                    'fieldName': 'description',
                    'wrapText': true
                }
            ];
        }
        return [];
    }
    @api
    get editServiceSiteDisabled() {
        return !(this.selectedServiceSiteOption && this.selectedServiceSiteOption.isEditable);
    }
    @api
    get siteCLCEditDisabled() {
        return true;
    }
    @api
    get siteNLCEditDisabled() {
        return (this.isGas || !this.isDiscoENEL || (this.currentEditedServiceSiteId !== 'new' && this.selectedServiceSiteOption && !this.selectedServiceSiteOption.isNLCEditable));
    }
    @api
    get siteDescriptionEditDisabled() {
        return (this.currentEditedServiceSiteId !== 'new' && this.selectedServiceSiteOption && !this.selectedServiceSiteOption.isDescriptionEditable);
    }
    @api
    get siteAddressEditDisabled() {
        return this.disableServiceSite || (this.currentEditedServiceSiteId !== 'new' && this.selectedServiceSiteOption && !this.selectedServiceSiteOption.isAddressEditable);
    }
    @track serviceSite;
    @track serviceSitesList = [];
    @track selectedServiceSiteOption;
    @track selectedServiceSiteRows = [];
    @track checkedSite = false;
    @track siteAddressKeyField;
    //@track isSelected;
    @track serviceSiteAddress = [];
    @track serviceSiteAddressChange = [];
    @track checkSelection = false;
    @track showNewServiceSite;
    @track selectedServiceSite = false;
    @track disableServiceSite;
    @track newServiceSiteDisabled = true;
    currentEditedServiceSiteId;
    siteCLC;
    siteNLC;
    siteDescription;
    naceId;

    get isHardVAS() {
        return this.typeOfVas === labels.hardVas;
    }
    get isSoftVAS() {
        return this.typeOfVas === labels.softVas;
    }
    get isVAS(){
        return this.isSoftVAS || this.isHardVAS;
    }

    get showNonDisconnectable() {

        let showNonDisconnectable = true;
        showNonDisconnectable = !this.isPlaceholder;
        return showNonDisconnectable;
    }

    get showServiceSiteSection() {

        let showServiceSiteSection = true;
        showServiceSiteSection = !this.isVAS && !this.isPlaceholder;
        return showServiceSiteSection;
    }

    checkDisableServiceSite() {
        if (this.serviceSiteId) {
            this.disableServiceSite = true;
        } else {
            this.disableServiceSite = false;
        }
    }

    disableSeriviceSite() {
        if (this.serviceSiteId) {
            this.disableServiceSite = true;
        }else{
            this.disableServiceSite = false;
        }
    }

    connectedCallback() {

        if(!this.hasPreCheck){
            this.disableSaveButton = false;
        }

        this.addressLoaded=false;
        if(this.isHardVAS || this.isSoftVAS) {
            this.disableServiceSite=true;
        }


        this.checkDisableServiceSite();
        //this.spinner = true;
        this.defaultTitle = this.title ? this.title : this.label.createOsi;
        this.isNormalize = true;
        let loadPointAddress=false;
        if (this.recordId) {
            loadPointAddress=true;
            this.initOsiAddress();
        } else if (this.supplyId) {
            loadPointAddress=true;
            this.initialize();
        }
        if (this.servicePointId ) {
            loadPointAddress=true;
            this.initServicePoint();
            this.initSiteAddress();
        }
        else if (this.servicePointCode && this.commodity) {
            this.initialize();
        }

        if(!loadPointAddress){
            this.addressLoaded=true;
        }

        if (this.accountId) {
            //this.initSiteAddress();
            getAvailableAddressesCountryRestricted({
                accountId: this.accountId,
                restrictToCountry: "ROMANIA"
            }).then((response) => {
                this.availableAddresses = response;
            }).catch((errorMsg) => {
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.spinner = false;
            });
        }

        if (this.commodity === 'Electric') {
            this.getMapKWhByConsumption();
        }

        this.getNaceIdByCode();

        if(this.isTransferOrProductChange && this.isOpenEdit && this.recordId){
            this.loadOsiByServiceSiteDescr();
        }
    }

    handleError(event) {
        error(this, event.detail.message);
        this.spinner = false;
    }

    initialize() {
        init({
            supplyId: this.supplyId,
            servicePointId: this.servicePointId,
            servicePointCode: this.servicePointCode,
            opportunityId: this.opportunityId,
            accountId: this.accountId,
            commodity: this.commodity
        }).then((response) => {
            this.tmpOsi = response.osi;
            this.recordTypeId = this.tmpOsi.RecordTypeId;
            this.serviceSite = this.tmpOsi.ServiceSite__c;
            this.distributorId = this.tmpOsi.Distributor__c;

            let distributorField = this.template.querySelector('[data-id="distributor"]');

            if (this.distributorId && distributorField) {
                distributorField.value  = this.distributorId;
                this.disabledFields.distributor = true;
            }

            if (response.distributor) {
                this.isDiscoENEL = response.distributor.IsDisCoENEL__c;
                this.selfReadingEnabled = response.distributor.SelfReadingEnabled__c;
            }
            if(this.distributorId || response.distributor){
                this.isBusinessClient();
            }
            
            if(this.isNotSelectedOldTrader && this.hasPreCheck  && !this.servicePoint && response.distributor && this.servicePointCode){
                this.disablePreCheckButton = true;
                this.disableSaveButton = false;

            } else if(!this.isNotSelectedOldTrader && this.hasPreCheck  && !this.servicePoint && response.distributor && this.servicePointCode && this.isDiscoENEL){
                this.disablePreCheckButton = false;
                this.disableSaveButton = true;

            } else if(!this.isNotSelectedOldTrader && this.hasPreCheck  && !this.servicePoint && response.distributor && this.servicePointCode && !this.isDiscoENEL){
                this.disablePreCheckButton = true;
                this.disableSaveButton = false;
            }

            this.product = this.tmpOsi.Product__c;
            this.checkDisableServiceSite();

            if (response.recordType === 'Gas') {
                this.isGas = true;
            } else if (response.recordType === 'Electric') {
                this.isElectric = true;
            } else if (response.recordType === 'Service') {
                this.isService = true;
            }

            if (this.isHardVAS){
                this.initDeliveryAddress();
            } else {
                this.address = response.osiAddress;
            }

            this.addressLoaded = true;

            // Select the current ServiceSite if it's in edit mode
            if (this.showServiceSiteSection){
                this.selectCurrentServiceSite();
            }

            if (this.distributorId) {
                this.initSiteAddress();
            }

            this.show = true;
        }).catch((errorMsg) => {
            this.addressLoaded = true;
            error(this, errorMsg.body.message);
        });
    }

    initDeliveryAddress() {
        const inputs = {
            accountId: this.accountId,
            supplyId: this.supplyId
        };
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getDeliveryAddress',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            let addr = {};
            addr = response.data.deliveryAddress;
            this.address = addr;
            this.addressLoaded = true;
            this.show = true;
            this.spinner = false;
        })
            .catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.addressLoaded = true;
                this.spinner = false;
            });
    }

    autoforceAddress(){
        if (this.address.streetId === undefined){
            return;
        }

        this.readOnlyAddress = true;
        this.addressForced = true;
        this.disableNormalize = true;
        this.disable = true;
    }

    initOsiAddress() {
        let inputs = {osiId: this.recordId};
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getOsiAddressField',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            let addr = {};
            addr = response.data.osiAddress;
            this.address = addr;
            this.addressLoaded = true;
            this.show = true;
            // start add by david
            this.disableNormalize=addr.addressNormalized;
            if (addr.addressNormalized) {
                this.readOnlyAddress = true;
                this.addressForced = false;
                this.disableNormalize=true;
                this.disable=true;
            }
            //end add by david
            this.spinner = false;
        })
            .catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.addressLoaded = true;
                this.spinner = false;
            });
    }

    initSiteAddress() {
        let inputDistributorCmp = this.template.querySelector('[data-id="distributor"]');
        if(!this.distributorId && inputDistributorCmp && inputDistributorCmp.value){
          this.distributorId  = inputDistributorCmp.value;
        }

        let inputs = {
            accountId: this.accountId,
            opportunityId: this.opportunityId,
            recordId: this.recordId,
            serviceSiteId: this.serviceSiteId,   //added by Stefano Porcari 11/02/2020
            commodity: this.commodity,
            distributorId: this.distributorId,
            servicePointCode: this.servicePointCode
        };
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getListOSIServiceSite',
            input: inputs
        }).then((response) => {

            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }

            this.serviceSitesList = JSON.parse(JSON.stringify(response.data.serviceSitesList));
            if(this.isTransferOrProductChange && this.serviceSitesList && this.serviceSitesList.length > 0 && !this.isOpenEdit){
                let description = this.serviceSitesList[0].description;
                this.siteDescription = description;
            } else if (this.isTransferOrProductChange && this.serviceSitesList && this.serviceSitesList.length > 0 && this.isOpenEdit){
                this.serviceSitesList[0].description = this.siteDescription;
            }
            this.newServiceSiteDisabled = false;

            this.allOSI = response.data.allOSI;
            this.allSS = response.data.allSS;
            if (response.data.serviceSiteField) {
                this.serviceSite = response.data.serviceSiteField;
                this.siteAddressKeyField = response.data.siteAddressKeyField;
                this.newServiceSiteDisabled = true;
            }
            else if (this.serviceSiteId) {
                this.serviceSite = this.serviceSiteId;
            }
            this.selectCurrentServiceSite();
            this.spinner = false;
            this.checkSelection = true;
        }).catch((errorMsg) => {
            console.error(JSON.stringify(errorMsg));
            if(errorMsg.body){
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            } else {
                error(this,errorMsg);
            }
            this.spinner = false;
        });
    }

    resetSiteAddress() {
            this.isVisibleServiceSites = false;
            this.serviceSite = null;
            this.serviceSitesList = [];
            this.selectedServiceSiteOption = null;
            this.selectedServiceSiteRows = [];
            this.checkedSite = false;
            this.siteAddressKeyField = null;
            this.serviceSiteAddress = [];
            this.serviceSiteAddressChange = [];
            this.checkSelection = false;
            this.selectedServiceSite = false;
            this.currentEditedServiceSiteId = null;
            this.newServiceSiteDisabled = true;
            this.siteCLC = null;
            this.siteNLC = null;
            this.siteDescription = null;

    }

    initServicePoint() {
        let inputs = {servicePointId: this.servicePointId};
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getPointCodeFields',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                this.addressLoaded = true;
                return error(this, JSON.stringify(response.error));
            }
            this.servicePoint = response.data.servicePoint;
            if (this.servicePoint) {
                this.distributorId = this.servicePoint.Distributor;
            }

            let addr = {};
            addr = response.data.servicePointAddress;
            this.address = addr;
            this.addressLoaded = true;
            if (addr.addressNormalized) {
                this.readOnlyAddress = true;
                this.addressForced = false;
                this.disableNormalize=true;
                this.disable=true;
            }
            //end add by david
            this.show = true;
            this.spinner = false;
            if(this.reloadFormData){
                this.handleLoad();
            }
        })
            .catch((errorMsg) => {
                this.spinner = false;
                console.error(JSON.stringify(errorMsg));
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
            });
    }

    setSinglePhase(event) {
        if(this.isElectric){
            let inputContractualPowerCmp = event.target;
            if (inputContractualPowerCmp.fieldName === 'ContractualPower__c') {
                let contractualPowerCmp = this.template.querySelector('[data-id="powerPhase"]');
                if((inputContractualPowerCmp.value.length !== 0) && (inputContractualPowerCmp.value < 11)){
                    contractualPowerCmp.value = 'Single'
                } else if((inputContractualPowerCmp.value.length !== 0) && (inputContractualPowerCmp.value >= 11)){
                    contractualPowerCmp.value = 'Three';
                }else if(inputContractualPowerCmp.value.length === 0){
                    contractualPowerCmp.value = '';
                }
            }
        }

        this.removeError(event);
    }

    setConsumptionCategory(event) {
        if(this.isGas){
            let inputEstimatedConsumptionCmp = event.target;
            if (inputEstimatedConsumptionCmp.fieldName === 'EstimatedConsumption__c') {
                let consumptionCategoryCmp = this.template.querySelector('[data-id="consumptionCategory"]');
                if((inputEstimatedConsumptionCmp.value.length === 0) || ((inputEstimatedConsumptionCmp.value >= 0) && (inputEstimatedConsumptionCmp.value < 280))){
                    consumptionCategoryCmp.value = 'C1'
                }else if((inputEstimatedConsumptionCmp.value >= 280) && (inputEstimatedConsumptionCmp.value < 2800)){
                    consumptionCategoryCmp.value = 'C2';
                }else if((inputEstimatedConsumptionCmp.value >= 2800) && (inputEstimatedConsumptionCmp.value < 28000)){
                    consumptionCategoryCmp.value = 'C3';
                } else if((inputEstimatedConsumptionCmp.value >= 28000) && (inputEstimatedConsumptionCmp.value < 280000)){
                    consumptionCategoryCmp.value = 'C4';
                }else if(inputEstimatedConsumptionCmp.value > 280000){
                    consumptionCategoryCmp.value = 'C5';
                }
            }
        }

        this.removeError(event);
    }

    setVoltageSetting(event) {
        let inputVoltageLevelCmp = event.target;
        if (inputVoltageLevelCmp.fieldName === 'VoltageLevel__c') {
            //let powerPhaseCmp = this.template.querySelector('[data-id="powerPhase"]');
            let voltageSettingCmp = this.template.querySelector('[data-id="voltageSetting"]');
            if(inputVoltageLevelCmp.value === 'LV'){
                voltageSettingCmp.value = '230'
                //powerPhaseCmp.value = 'Single'
            }else if(inputVoltageLevelCmp.value === 'MV'){
                voltageSettingCmp.value = '6000';
                //powerPhaseCmp.value = 'Single'
            }else if(inputVoltageLevelCmp.value === 'HV'){
                voltageSettingCmp.value = '110000';
                //powerPhaseCmp.value = 'Single'
            }
        }
        this.removeError(event);
    }

    checkNonDisconnectable(){
        if(this.showNonDisconnectable) {
            let nonDisconnectableCmp = this.template.querySelector('[data-id="nonDisconnectable"]');
            if (nonDisconnectableCmp.value) {
                this.showNonDisconnectableReason = true;
            } else {
                this.showNonDisconnectableReason = false;
            }
       }
    }


    setDistributor(event) {

        this.disablePreCheckButton = true;

        let inputDistributorCmp = this.template.querySelector('[data-id="distributor"]');
        this.distributorId = inputDistributorCmp.value;

        if (this.distributorId) {

            this.isVisibleServiceSites = true;

            if (this.commodity == 'Gas') {
                let inputs = {distributorId: this.distributorId};
                notCacheableCall({
                    className: 'MRO_LC_OpportunityServiceItem',
                    methodName: 'distributorSelfReading',
                    input: inputs
                }).then((response) => {
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    }

                    this.selfReadingEnabled = response.data.selfReadingEnabled;
                    this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

                }).catch((errorMsg) => {
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                    this.spinner = false;
                });
            }

            if (this.commodity == 'Electric') {
                let inputs = {distributorId: this.distributorId};
                notCacheableCall({
                    className: 'MRO_LC_OpportunityServiceItem',
                    methodName: 'distributorIsDiscoENEL',
                    input: inputs
                }).then((response) => {
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    }

                    this.isDiscoENEL = response.data.isDisCoENEL;
                    this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;
                    let inputTrader = this.template.querySelector('[data-id="trader"]');

                    if(!this.isNotSelectedOldTrader && this.hasPreCheck && this.isDiscoENEL) {
                        if (inputTrader.value && this.servicePointCode) {
                            this.disablePreCheckButton = false;
                            this.disableSaveButton = true;
                        }
                    }else if(this.isNotSelectedOldTrader && this.hasPreCheck) {
                        if (this.servicePointCode) {
                            this.disablePreCheckButton = true;
                            this.disableSaveButton = false;
                        }
                    }else if(!this.isNotSelectedOldTrader && this.hasPreCheck && !this.isDiscoENEL){
                        if (inputTrader.value && this.servicePointCode) {
                            this.disablePreCheckButton = true;
                            this.disableSaveButton = false;
                        }
                    }

                }).catch((errorMsg) => {
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                    this.spinner = false;
                });
            }
            this.initSiteAddress();
        }
        else {
            this.resetSiteAddress();
        }
        this.removeError(event);
    }
    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check


    setTrader(event){
        if(this.hasPreCheck) {
            this.disablePreCheckButton = true;
            let inputTrader = event.target.value;
            let inputDistributor = this.template.querySelector('[data-id="distributor"]').value;

            if (inputDistributor && inputTrader && this.servicePointCode  && this.isDiscoENEL) {
                this.disablePreCheckButton = false;
                this.disableSaveButton = true;
            } else if (this.isNotSelectedOldTrader && inputDistributor && !inputTrader && this.servicePointCode) {
                this.disablePreCheckButton = true;
                this.disableSaveButton = false;
            } else if (inputDistributor && inputTrader && this.servicePointCode  && !this.isDiscoENEL) {
                this.disablePreCheckButton = true;
                this.disableSaveButton = false;
            }
        }
        this.removeError(event);
    }

    getObjectName(objId) {
        let inputs = {objId: objId};
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'objectAPIName',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            this.objName = response.data.objName;
            this.spinner = false;
        }).catch((errorMsg) => {
            this.spinner = false;
            console.error(JSON.stringify(errorMsg));
            if(errorMsg.body){
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            } else {
                error(this,errorMsg);
            }
        });
    }

    renderedCallback() {

        this.selectedServiceSite = true;
        if(!this.isVisibleServiceSites){
            this.selectedServiceSite = false;
            this.resetSiteAddress();
        }

        if(this.isSupplyStatusActive) {
            this.disablePreCheckButton = true;
            this.disableSaveButton = false;
            this.disableTrader = true;
        }

        cacheableCall({
            className: "MRO_UTL_Utils",
            methodName: "getAllConstants"
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.electricRecordTypeId = response.data.OSIRecordTypes.Electric;
                    this.gasRecordTypeId = response.data.OSIRecordTypes.Gas;
                    this.serviceRecordTypeId = response.data.OSIRecordTypes.Service;
                    switch (this.commodity) {
                        case 'Gas':
                            this.isGas = true;
                            this.recordTypeId = this.gasRecordTypeId;
                            break;
                        case 'Electric':
                            this.isElectric = true;
                            this.recordTypeId = this.electricRecordTypeId;
                            break;
                        case 'Service':
                            this.isService = true;
                            this.recordTypeId = this.serviceRecordTypeId;
                            break;
                    }

                    // Change record type to Service
                    if(this.isVAS) {
                        this.recordTypeId = this.serviceRecordTypeId;
                        this.isService = true;
                    }
                    /*
                    let gmPatt = /^(\d{14})$/i;
                    let eePatt = /^(\w{2}\d{3}\w{1}\d{8})$/i;
                    if (gmPatt.test(this.servicePointCode)) {
                        //this.isGas = true;
                        //this.recordTypeId = this.gasRecordTypeId;
                    } else if (eePatt.test(this.servicePointCode)) {
                        //this.isElectric = true;

                        //this.recordTypeId = this.electricRecordTypeId;
                    }
                     */
                    if (this.showNewServiceSite) {
                        if (this.isGas && !this.siteCLC) {
                            this.template.querySelector('lightning-input-field[data-id="CLC"]').value = this.servicePointCode;
                            this.siteCLC = this.servicePointCode;
                        }
                        else if (this.isElectric && !this.siteNLC) {
                            this.template.querySelector('lightning-input-field[data-id="NLC"]').value = this.servicePointCode;
                            this.siteNLC = this.servicePointCode;
                        }
                    }

                    // Select the current ServiceSite if it's in edit mode
                    // this.selectCurrentServiceSite();
                    this.show = true;

                    if(this.isVAS && !this.normalizedForVas) {
                        setTimeout(()=>{
                            let addressForm = this.template.querySelector('c-mro-address-form');
                            if (addressForm){
                                addressForm.normalize();
                                this.normalizedForVas = true;
                            }}, 1250);
                    }
                }
            }
        });
    }

    selectCurrentServiceSite(){

        if (this.serviceSite) {
            this.selectedServiceSiteRows = [this.serviceSite];
            if (this.serviceSitesList) {
                for (let servSite of this.serviceSitesList) {
                    if (servSite.recordId === this.serviceSite) {
                        this.selectedServiceSiteOption = servSite;
                        this.siteCLC = servSite.clc;
                        this.siteNLC = servSite.nlc;
                        break;
                    }
                }
            }
            this.selectedServiceSite = true;
        }
        else if (this.selectedServiceSiteOption) {
            this.selectedServiceSiteRows = [this.selectedServiceSiteOption.recordId];
            this.selectedServiceSite = true;
        }
        else {
            this.selectedServiceSite = false;
        }


        if(this.isTransferOrProductChange){
            this.disableServiceSite = false;
             this.newServiceSiteDisabled = true;
             if(this.selectedServiceSiteOption){
                 this.selectedServiceSiteOption.isDescriptionEditable = true;
                 this.selectedServiceSiteOption.isAddressEditable = false;
                 this.selectedServiceSiteOption.isEditable = true;
             } else {
                 this.selectedServiceSiteOption = {
                     isDescriptionEditable: true,
                     isAddressEditable: false,
                     isEditable: true
                 }
             }
        }
    }

    handleLoad() {
        if (this.loads) {
            if (this.commodity === 'Gas') {
                let inputDistributorCmp  =this.template.querySelector('[data-id="distributor"]');

                if(inputDistributorCmp.value){
                    let inputs = {distributorId: inputDistributorCmp.value};
                    notCacheableCall({
                        className: 'MRO_LC_OpportunityServiceItem',
                        methodName: 'distributorSelfReading',
                        input: inputs
                    }).then((response) => {
                        if (response.isError) {
                            error(this, JSON.stringify(response.error));
                        }
                        this.selfReadingEnabled = response.data.selfReadingEnabled;
                        this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

                    }).catch((errorMsg) => {
                        error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                        this.spinner = false;
                    });
                }

            }
            if (this.commodity === 'Electric') {

                let inputDistributorCmp  =this.template.querySelector('[data-id="distributor"]');

                if(inputDistributorCmp.value){
                    let inputs = {distributorId: inputDistributorCmp.value};
                    notCacheableCall({
                        className: 'MRO_LC_OpportunityServiceItem',
                        methodName: 'distributorIsDiscoENEL',
                        input: inputs
                    }).then((response) => {
                        if (response.isError) {
                            error(this, JSON.stringify(response.error));
                        }
                        this.isDiscoENEL = response.data.isDisCoENEL;
                        this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

                        if(!this.isNotSelectedOldTrader && this.hasPreCheck){
                            let inputDistributor = this.template.querySelector('[data-id="distributor"]');
                            let inputTrader = this.template.querySelector('[data-id="trader"]');
                            if(inputDistributor.value && inputTrader.value && this.servicePointCode && this.isDiscoENEL){
                                this.disablePreCheckButton = false;
                                this.disableSaveButton = true;
                            } else if(inputDistributor.value && inputTrader.value && this.servicePointCode && !this.isDiscoENEL){
                                this.disablePreCheckButton = true;
                                this.disableSaveButton = false;
                            }
                        }else if(this.isNotSelectedOldTrader && this.hasPreCheck) {
                            let inputDistributor = this.template.querySelector('[data-id="distributor"]');
                            if (inputDistributor.value  && this.servicePointCode) {
                                this.disablePreCheckButton = true;
                                this.disableSaveButton = false;
                            }
                        }

                    }).catch((errorMsg) => {
                        error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                        this.spinner = false;
                    });
                }
            }


            if (this.commodity === 'Gas') {
                this.isGas = true;
            } else if (this.commodity === 'Electric') {
                this.isElectric = true;
            } else if (this.commodity === 'Service') {
                this.isService = true;
            }

            if (!this.servicePointCode && !this.isService) {
                let inputCmp = this.template.querySelector('[data-id="myServicePointCode"]');
                this.servicePointCode = inputCmp.value;


                /*
                let gmPatt = /^(\d{14})$/i;
                let eePatt = /^(\w{2}\d{3}\w{1}\d{8})$/i;

                if (gmPatt.test(this.servicePointCode)) {
                    //this.isGas = true;
                } else if (eePatt.test(this.servicePointCode)) {
                    //this.isElectric = true;
                }
                 */
            }

            if (this.tmpOsi) {

                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    element.value = this.tmpOsi[element.fieldName] ? this.tmpOsi[element.fieldName] : undefined;
                });
            } else if (!this.recordId && this.servicePoint) {

                let fieldsList = Array.from(this.template.querySelectorAll('lightning-input-field'));
                fieldsList.forEach(element => {
                    let str = element.fieldName;
                    element.value = this.servicePoint[str.slice(0, str.length - 3)] ? this.servicePoint[str.slice(0, str.length - 3)] : undefined;

                });
                this.isBusinessClient();

                if(this.commodity == 'Gas') {
                    let inputDistributorCmp  = this.template.querySelector('[data-id="distributor"]');
                    if(!inputDistributorCmp.value){
                        inputDistributorCmp.value = this.servicePoint.Distributor;
                    }

                    if(inputDistributorCmp.value){

                        let inputs = {distributorId: inputDistributorCmp.value};
                        notCacheableCall({
                            className: 'MRO_LC_OpportunityServiceItem',
                            methodName: 'distributorSelfReading',
                            input: inputs
                        }).then((response) => {
                            if (response.isError) {
                                error(this, JSON.stringify(response.error));
                            }
                            this.selfReadingEnabled = response.data.selfReadingEnabled;
                            this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

                        }).catch((errorMsg) => {
                            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                            this.spinner = false;
                        });
                    }
                }


                if(this.commodity == 'Electric') {
                    let inputDistributorCmp  = this.template.querySelector('[data-id="distributor"]');
                    if(!inputDistributorCmp.value){
                        inputDistributorCmp.value = this.servicePoint.Distributor;
                    }

                    if(inputDistributorCmp.value){

                        let inputs = {distributorId: inputDistributorCmp.value};
                        notCacheableCall({
                            className: 'MRO_LC_OpportunityServiceItem',
                            methodName: 'distributorIsDiscoENEL',
                            input: inputs
                        }).then((response) => {
                            if (response.isError) {
                                error(this, JSON.stringify(response.error));
                            }
                            this.isDiscoENEL = response.data.isDisCoENEL;
                            this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

                            if(this.hasPreCheck && this.servicePoint.Trader && this.servicePoint.Distributor && this.servicePointCode && this.isDiscoENEL){
                                this.disablePreCheckButton = false;
                                this.disableSaveButton = true;

                            }else if(this.hasPreCheck && this.servicePoint.Trader && this.servicePoint.Distributor && this.servicePointCode && !this.isDiscoENEL){
                                this.disablePreCheckButton = true;
                                this.disableSaveButton = false;
                            }

                        }).catch((errorMsg) => {
                            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                            this.spinner = false;
                        });
                    }
                }

                //('this.address: '+ JSON.stringify(this.address));
                if(this.servicePoint && this.servicePoint.Distributor){

                  /*  let addr = {};
                    addr = JSON.parse(JSON.stringify(this.servicePoint.Address));
                    this.address = addr;*/
                    this.addressLoaded = true;
                    this.isVisibleServiceSites = true;
                    this.initSiteAddress();
                }
                if(this.hasPreCheck && this.servicePoint.Status === 'Active'){
                    this.isSupplyStatusActive = true;
                    this.disabledFields.applicantNotHolder = true;
                    if (this.hasApplicantNotHolder && this.servicePoint.Account !== this.accountId) {
                        let inputApplicantNotHolderCmp = this.template.querySelector('[data-id="applicantNotHolder"]');
                        if (inputApplicantNotHolderCmp) {
                            inputApplicantNotHolderCmp.value = true;
                        }
                    }
                }

            } else {
                if(this.hasPreCheck && !this.servicePoint && this.recordId) {
                    let inputs = {recordId: this.recordId};
                    notCacheableCall({
                        className: 'MRO_LC_OpportunityServiceItem',
                        methodName: 'getSupplyStatus',
                        input: inputs
                    }).then((response) => {
                        console.log('getSuppyStatus response', JSON.parse(JSON.stringify(response)));
                        if (response.isError) {
                            error(this, JSON.stringify(response.error));
                        }
                        if(response.data.supplyStatus === 'Active'){
                            this.isSupplyStatusActive = true;
                        }

                    }).catch((errorMsg) => {
                        error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                        this.spinner = false;
                    });

                }

                if (!this.isVAS){

                    let inputDistributorCmp = this.template.querySelector('[data-id="distributor"]');
                    if(inputDistributorCmp.value){
                        this.isVisibleServiceSites = true;
                        this.initSiteAddress();
                        //this.editServiceSiteDisabled();
                    }
                }
                if(this.commodity === 'Electric' && !this.servicePointId) {
                    let inputDistributorCmp  =this.template.querySelector('[data-id="distributor"]');
                        if(inputDistributorCmp.value){
                            let inputs = {distributorId: inputDistributorCmp.value};
                            notCacheableCall({
                                className: 'MRO_LC_OpportunityServiceItem',
                                methodName: 'distributorIsDiscoENEL',
                                input: inputs
                            }).then((response) => {
                                if (response.isError) {
                                    error(this, JSON.stringify(response.error));
                                }
                                this.isDiscoENEL = response.data.isDisCoENEL;
                                this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

                            }).catch((errorMsg) => {
                                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                                this.spinner = false;
                            });
                        }
                }

                if(this.commodity === 'Gas' && !this.servicePointId) {
                    let inputDistributorCmp  =this.template.querySelector('[data-id="distributor"]');
                    if(inputDistributorCmp.value){
                        let inputs = {distributorId: inputDistributorCmp.value};
                        notCacheableCall({
                            className: 'MRO_LC_OpportunityServiceItem',
                            methodName: 'distributorSelfReading',
                            input: inputs
                        }).then((response) => {
                            if (response.isError) {
                                error(this, JSON.stringify(response.error));
                            }
                            this.selfReadingEnabled = response.data.selfReadingEnabled;
                            this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

                        }).catch((errorMsg) => {
                            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                            this.spinner = false;
                        });
                    }
                }
                //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                if (!this.isService) {
                    this.template.querySelector('[data-id="myServicePointCode"]').value = this.servicePointCode;
                }
            }
            if (this.commodity === 'Electric') {
                let estimatedConsumptionEle = this.template.querySelector('[data-id="estimatedConsumptionEle"]');
                if(this.mapKWhByConsumptionNames && (this.mapKWhByConsumptionNames !== "{}") && (Object.keys(this.mapKWhByConsumptionNames).length !== 0) && (!estimatedConsumptionEle.value)){
                    if(this.address.province && this.address.addressNormalized){
                        if(this.mapKWhByConsumptionNames.hasOwnProperty(this.address.province)){
                            Object.keys(this.mapKWhByConsumptionNames).forEach((key) => {
                                if (this.address.province === key) {
                                    estimatedConsumptionEle.value = this.mapKWhByConsumptionNames[key];
                                }
                            });
                        }else{
                            estimatedConsumptionEle.value = this.mapKWhByConsumptionNames['Other'];
                        }
                    }
                }
            }

            let supplyOperationElement = this.template.querySelector('[data-id="supplyOperation"]');
            if (supplyOperationElement){
                supplyOperationElement.value = 'Permanent';
            }

            if(this.supply) {
                if (this.supply.NACEReference__c) {
                    this.template.querySelector('[data-id="naceReference"]').value = this.supply.NACEReference__c;
                }
                if (this.supply.SupplyOperation__c) {
                    this.template.querySelector('[data-id="supplyOperation"]').value = this.supply.SupplyOperation__c;
                }
                if (this.supply.TitleDeedValidity__c) {
                    this.template.querySelector('[data-id="endDate"]').value = this.supply.TitleDeedValidity__c;
                }
            }
            //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
            //if (this.commodity === 'Gas') {
                this.isBusinessClient();
            //}

            if (this.commodity === 'Gas') {
                this.distributorSelfReading();
            }

            if (this.commodity === 'Electric' ) {

                if(this.showNonDisconnectable) {
                    let nonDisconnectableCmp = this.template.querySelector('[data-id="nonDisconnectable"]');
                    if (nonDisconnectableCmp.value) {
                        this.showNonDisconnectableReason = true;
                    } else {
                        this.showNonDisconnectableReason = false;
                    }
                }

                this.distributorIsDiscoENEL();
            }
            //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
            this.validateVoltageSettingValue();
            if(this.hasSameOpportunityServiceItems) {
                this.getOSIList();
            }
            if ((this.autoSave) && (this.firstTimeEntry)) {
                this.spinner = false;
                let fieldList = {};

                this.template.querySelectorAll('lightning-input-field').forEach(element => {
                    fieldList[element.fieldName] = element.value;
                });

                fieldList.Account__c = this.accountId;
                fieldList.Opportunity__c = this.opportunityId;
                fieldList.ServicePoint__c = this.servicePointId;
                fieldList.ServiceSite__c = this.serviceSite;
                fieldList.ContractAccount__c = this.contractAccount;
                fieldList.Product__c = this.product;

                let addrForm = this.template.querySelector('div.pointAddressData c-mro-address-form');
                if(addrForm !== null){
                    let addrFields =  addrForm.getValues();
                    for (let f in addrFields) {
                        if (addrFields.hasOwnProperty(f)) {
                            fieldList[f] = addrFields[f];
                        }
                    }
                }

                if(!this.isPlaceholder || (fieldList.PointStreetName__c && fieldList.PointStreetName__c != '')) {

                    fieldList.PointAddressNormalized__c = true;
                } else {

                    fieldList.PointAddressNormalized__c = false;
                }

                if (this.successBoolean) {
                    if (fieldList.Distributor__c != null) {
                        this.template.querySelector('lightning-record-edit-form').submit(fieldList);
                        this.firstTimeEntry = false;
                    }
                }
            }
        }
    }

    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
    distributorIsDiscoENEL(){
        if( this.servicePointId && this.servicePoint && this.servicePoint.Distributor){
            let inputs = {distributorId: this.servicePoint.Distributor};
            notCacheableCall({
                className: 'MRO_LC_OpportunityServiceItem',
                methodName: 'distributorIsDiscoENEL',
                input: inputs
            }).then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                this.isDiscoENEL = response.data.isDisCoENEL;
                this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

            }).catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
           // this.IsDiscoENEL =true;

        }
    }
    distributorSelfReading(){
        if( this.servicePointId && this.servicePoint && this.servicePoint.Distributor){
            //this.distributorVal = this.servicePoint.Distributor;
            let inputs = {distributorId: this.servicePoint.Distributor};
            notCacheableCall({
                className: 'MRO_LC_OpportunityServiceItem',
                methodName: 'distributorSelfReading',
                input: inputs
            }).then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                this.selfReadingEnabled = response.data.selfReadingEnabled;
                this.distributionProvincesDetails = response.data.distributionProvincesDetails ? JSON.parse(response.data.distributionProvincesDetails) : null;

            }).catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
            // this.IsDiscoENEL =true;

        }
    }

    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
    validateVoltageSettingValue(){

        if( this.servicePointId && this.commodity ==='Electric' && this.servicePoint  ) {
            let voltage=0;

            let voltageSettingField=this.template.querySelector('[data-id="voltageSetting"]');
            let voltageLevelField=this.template.querySelector('[data-id="voltageLevel"]');
            let voltageLevel=voltageLevelField.value ;
            voltageLevel= this.servicePoint.VoltageLevel;
            if(!this.recordId ){
                if( this.servicePoint.Voltage){
                    voltage=this.servicePoint.Voltage * 1000;
                }
                if( this.servicePoint.VoltagLevel){
                    voltageLevel=this.servicePoint.VoltagLevel;
                }

            }
            if(voltageLevelField.value && voltageLevelField.value != ''){
                voltageLevel=voltageLevelField.value;
            }
            if(voltageSettingField.value && voltageSettingField.value != ''){
                voltage=voltageSettingField.value;
            }


            let osiId='';
            if(this.recordId){
                osiId=this.recordId;
            }
            if(voltageLevel && voltageLevel != '') {
                let inputs = {'voltage': voltage, 'voltageLevel': voltageLevel, 'osiId': this.recordId};

                notCacheableCall({
                    className: 'MRO_LC_OpportunityServiceItem',
                    methodName: 'checkRightVoltageValue',
                    input: inputs
                }).then((response) => {

                    if (this.servicePoint.VoltageLevel) {
                        voltageLevel = this.servicePoint.VoltageLevel;
                    }
                    if (!voltageLevelField.value || voltageLevelField.value == '') {
                        voltageLevelField.value = voltageLevel;
                    }

                    if (!response.data.error) {
                        if (this.recordId && response.data.voltageLevel) {
                            voltageLevel = response.data.voltageLevel;
                            voltageSettingField.value = voltageLevel;
                        }
                        voltageSettingField.value = response.data.voltageValue;


                    } else {

                        voltageSettingField.value = '';
                        //this.voltageSetting='';
                        error(this, JSON.stringify(response.data.errorMsg));
                    }

                })
                    .catch((errorMsg) => {
                        if (errorMsg.body) {
                            error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                        } else {
                            error(this, errorMsg);
                        }

                    });

            }

        }else{
            this.reloadFormData= true;
        }
    }

    handleSuccess(event) {
        if (this.successBoolean) {
            event.preventDefault();
            this.spinner = true;
            let payload = event.detail;
            this.dispatchEvent(new CustomEvent('save', {
                detail: {
                    'opportunityServiceItemId': payload.id,
                    'opportunityServiceItem': payload,
                    'contractId': this.contractId
                }

            }, {bubbles: true}));
        }
        if (this.autoSave) {
            this.successBoolean = false;
        }
    }

    saveServiceSite() {
        let clc = this.template.querySelector('lightning-input-field[data-id="CLC"]').value;
        let nlc = this.template.querySelector('lightning-input-field[data-id="NLC"]').value;
        let description = this.template.querySelector('lightning-input-field[data-id="description"]').value;
        if ((this.isElectric && !nlc) || (this.isGas && !clc)) {
            error(this, this.label.requiredFields);
            return;
        }

        const addrForm = this.template.querySelector('div.serviceSiteAddressData c-mro-address-form');
        if (!addrForm.isValid()) {
            error(this, this.label.validateAddress); //'Address must be verified');
            return;
        }
        if (this.distributionProvincesDetails) {
            let siteProvince = addrForm.getValues().SiteProvince__c;
            let found = false;
            for (let code in this.distributionProvincesDetails) {
                if (this.distributionProvincesDetails.hasOwnProperty(code) && this.distributionProvincesDetails[code].toUpperCase() === siteProvince.toUpperCase()) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                error(this, this.label.invalidDistributorProvince);
                return;
            }
        }

        /*
        //First duplicates check on the current Service Sites list
        const findNLC = (servSite) => servSite.nlc === nlc;
        const findCLC = (servSite) => servSite.clc === clc;
        if (this.serviceSitesList && this.serviceSitesList.length) {
            if ((this.isElectric && this.serviceSitesList.findIndex(findNLC) > 0) || (this.isGas && this.serviceSitesList.findIndex(findCLC) > 0)) {
                error(this, labels.duplicatedServiceSite);
                return;
            }
        }
         */

        //Second duplicates check on DB
        const inputs = {
            clc: clc,
            nlc: nlc,
            recordId: this.currentEditedServiceSiteId,
            accountId: this.accountId
        };
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'checkExistingServiceSite',
            input: inputs
        }).then((response) => {
            if (response.isError) {
                error(this, response.error);
                return;
            }
            if(!this.isTransferOrProductChange){
                if (response.data.duplicateFound) {
                    error(this, labels.duplicatedServiceSite);
                    return;
                }
            }

            this.selectedServiceSite = true;
            this.siteCLC = clc;
            this.siteNLC = nlc;
            this.siteDescription = description;

            //giupastore: I need to change the name of "fields" into "fieldsServiceSite" because of implementation on recordUpdate
            let fieldsServiceSite = {};
            let addrFields = addrForm.getValues();
            for (let f in addrFields) {
                if (addrFields.hasOwnProperty(f)) {
                    fieldsServiceSite[f] = addrFields[f];
                }
            }
            fieldsServiceSite.SiteAddressNormalized__c = !this.addressForcedSite;
            this.serviceSiteAddress = fieldsServiceSite;
            let key = 'newAdded';
            let record = {
                CLC__c: this.siteCLC,
                NLC__c: this.siteNLC,
                Description__c: this.siteDescription,
                SiteAddress__c: addrForm.getStringifiedAddress()
            };
            let valueSetValues = this.serviceSiteAddress;
            Object.keys(valueSetValues).forEach(function (val) {
                if (/AddressKey/.test(val)) {
                    key = valueSetValues[val];
                }
                record[val] = valueSetValues[val];
            });

            if (this.currentEditedServiceSiteId === 'new' || this.currentEditedServiceSiteId === this.recordId) {
                this.selectedServiceSiteOption = {
                    recordId: 'new',
                    objectType: 'OpportunityServiceItem__c',
                    record: record,
                    isCLCEditable: this.commodity === 'Gas',
                    isNLCEditable: this.commodity === 'Electric',
                    isDescriptionEditable: true,
                    isAddressEditable: true,
                    isEditable: true
                };
                if (this.serviceSitesList.length > 0 && this.serviceSitesList[0].recordId === 'new') {
                    this.serviceSitesList[0] = this.selectedServiceSiteOption;
                } else {
                    this.serviceSitesList.unshift(this.selectedServiceSiteOption);
                }
            }
            this.selectedServiceSiteOption.key = key;
            this.selectedServiceSiteOption.clc = record.CLC__c;
            this.selectedServiceSiteOption.nlc = record.NLC__c;
            this.selectedServiceSiteOption.description = record.Description__c;
            this.selectedServiceSiteOption.addressString = record.SiteAddress__c;
            this.selectedServiceSiteOption.address = addrForm.address;

            this.selectedServiceSiteRows = [this.selectedServiceSiteOption.recordId];
            this.showNewServiceSite = false;
            this.addSelectOption(true);
        }).catch((error) => {
            error(this, JSON.stringify(error));
        });
    }

    handleSelectOption(event) {
        this.isCheckedSS = false;
        this.selectedServiceSiteOption = event.detail.selectedRows[0];

        if (this.selectedServiceSiteOption && this.commodity === 'Electric') {
            let province =  this.selectedServiceSiteOption.address.province;
            let addrNormalized =  this.selectedServiceSiteOption.address.addressNormalized;
            let estimatedConsumptionEle = this.template.querySelector('[data-id="estimatedConsumptionEle"]');
            if(this.mapKWhByConsumptionNames && (this.mapKWhByConsumptionNames !== "{}") && (Object.keys(this.mapKWhByConsumptionNames).length !== 0) && (!estimatedConsumptionEle.value)){
                if(province && addrNormalized){
                    if(this.mapKWhByConsumptionNames.hasOwnProperty(province)){
                        Object.keys(this.mapKWhByConsumptionNames).forEach((key) => {
                            if (province === key) {
                                estimatedConsumptionEle.value = this.mapKWhByConsumptionNames[key];
                            }
                        });
                    }else{
                        estimatedConsumptionEle.value = this.mapKWhByConsumptionNames['Other'];
                    }
                }
            }
        }

        this.addSelectOption(true);
    }
    addSelectOption(updateLoading) {
        this.isCheckedSS = false;

        if (this.selectedServiceSiteOption) {
            //this.readOnlyAddress = false;
            this.selectedServiceSite = true;
            this.objName = this.selectedServiceSiteOption.objectType;
            this.siteCLC = this.selectedServiceSiteOption.clc;
            this.siteNLC = this.selectedServiceSiteOption.nlc;
            this.siteDescription = this.selectedServiceSiteOption.description;
        }

        if (!this.address || this.address.length === 0 || !this.address.addressKey) {

            const pointAddrForm = this.template.querySelector("div.pointAddressData c-mro-address-form");
            if (pointAddrForm) {
                pointAddrForm.copyAddress(this.selectedServiceSiteOption.address);
            }
        }

        if (this.serviceSiteAddress.SiteAddressNormalized__c) {
            this.readOnlySiteAddress = true;
            this.addressForcedSite = false;
        } else {
            this.readOnlySiteAddress = false;
        }

        if(updateLoading){
            this.loads = false;
        }
    }

    handleNewServiceSite(event) {

        if (this.commodity === 'Electric') {
            let estimatedConsumptionEle = this.template.querySelector('[data-id="estimatedConsumptionEle"]');
            if(this.mapKWhByConsumptionNames && (this.mapKWhByConsumptionNames !== "{}") && (Object.keys(this.mapKWhByConsumptionNames).length !== 0) && (!estimatedConsumptionEle.value)){
                if(this.address.province && this.address.addressNormalized){
                    if(this.mapKWhByConsumptionNames.hasOwnProperty(this.address.province)){
                        Object.keys(this.mapKWhByConsumptionNames).forEach((key) => {
                            if (this.address.province === key) {
                                estimatedConsumptionEle.value = this.mapKWhByConsumptionNames[key];
                            }
                        });
                    }else{
                        estimatedConsumptionEle.value = this.mapKWhByConsumptionNames['Other'];
                    }
                }
            }
        }

        this.currentEditedServiceSiteId = this.recordId ? this.recordId : 'new';
        this.isBusinessClient();
        this.serviceSiteAddressChange = JSON.parse(JSON.stringify(this.address));
        this.openServiceSiteModal();
    }

    handleEditServiceSite() {
        this.currentEditedServiceSiteId = this.selectedServiceSiteOption.recordId;
        this.serviceSiteAddressChange = JSON.parse(JSON.stringify(this.selectedServiceSiteOption.address));
        this.openServiceSiteModal();
    }

    openServiceSiteModal() {

        this.readOnlySiteAddress = (this.serviceSiteAddressChange.addressNormalized === true);
        this.addressForcedSite = this.addressForced;

        this.showNewServiceSite = true;

        this.loads = false;
    }

    closeModal() {
        this.showNewServiceSite = false;
    }

    handleCancelServiceSite() {
        this.showNewServiceSite = false;
    }

    addError(fieldName){
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if(element.fieldName === fieldName){
                element.classList.add('slds-has-error');
            }
        });
    }

    handleSubmit(event) {
        //if (this.serviceSite) {
        //    this.selectedServiceSiteOption = [this.serviceSite];
        //}
        this.spinner = true;
        this.submitErrorMessage = '';

        event.preventDefault();
        let fieldList = {
            'CLC__c': this.siteCLC,
            'NLC__c': this.siteNLC,
            'SiteDescription__c': this.siteDescription
        };
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fieldList[element.fieldName] = element.value;
        });

        let addrForm = this.template.querySelector('div.pointAddressData c-mro-address-form');

        if (!addrForm.isValid()) {
            this.isCheckedPA = true;
            if(!this.isPlaceholder) this.submitErrorMessage = this.submitErrorMessage + this.label.addressMustBeVerified;
            this.spinner = false;
            this.loads = false;
        }

        let addrFields = addrForm.getValues();
        for (let f in addrFields) {
            if (addrFields.hasOwnProperty(f)) {
                fieldList[f] = addrFields[f];
            }
        }
        if(this.showServiceSiteSection) {
            if (this.selectedServiceSiteOption) {
                this.objName = this.selectedServiceSiteOption.objectType;
                if (this.selectedServiceSiteOption.recordId && this.selectedServiceSiteOption.record) {
                    if (this.objName === 'ServiceSite__c') {
                        let SS = this.selectedServiceSiteOption.record;
                        for (let SSfield in SS) {
                            if (SS.hasOwnProperty(SSfield)) {
                                let OSIfield = (SSfield === 'Description__c') ? 'SiteDescription__c' : SSfield;
                                fieldList[OSIfield] = SS[SSfield];
                            }
                        }
                        fieldList.ServiceSite__c = this.selectedServiceSiteOption.recordId;
                    } else if (this.objName === 'OpportunityServiceItem__c') {
                        for (let osiField in this.selectedServiceSiteOption.record) {
                            if (this.selectedServiceSiteOption.record.hasOwnProperty(osiField)) {
                                fieldList[osiField] = this.selectedServiceSiteOption.record[osiField];
                            }
                        }
                        fieldList.ServiceSite__c = null;
                    }
                }
            } else {
                this.submitErrorMessage = this.label.requiredServiceSite;
            }
        }

        fieldList.Account__c = this.accountId;
        fieldList.Opportunity__c = this.opportunityId;
        fieldList.ServicePoint__c = this.servicePointId;
        if (this.tmpOsi) {
            fieldList.ServicePoint__c = this.tmpOsi.ServicePoint__c;
        }
        fieldList.Product__c = this.product;

        fieldList.Voltage__c = fieldList.VoltageSetting__c / 1000;

        if (this.tmpOsi) {
            fieldList.ServicePoint__c = this.tmpOsi.ServicePoint__c;
        }
        //start add by David
        if(!this.isPlaceholder || (fieldList.PointStreetName__c && fieldList.PointStreetName__c != '')) {

            fieldList.PointAddressNormalized__c = !this.addressForced;
        } else {

            fieldList.PointAddressNormalized__c = false;
        }
        // end add by David

        if (!this._validateFields(fieldList)) {
            this.spinner = false;
            if (this.submitErrorMessage !== '') {
                this.submitErrorMessage = this.submitErrorMessage + ' - ';
            }

            this.submitErrorMessage = this.submitErrorMessage + this.label.requiredFields;
            this.loads = false;
        }

        if (this.commodity === 'Electric') {
            if (parseFloat(fieldList.AvailablePower__c) < parseFloat(fieldList.ContractualPower__c)) {
                this.addError('ContractualPower__c');
                this.addError('AvailablePower__c');
                error(this, this.label.ContractualAvailablePowerValidation);
                this.spinner = false;
                return;
            }
        }

        if (this.submitErrorMessage !== '') {
            this.spinner = false;
            error(this, this.submitErrorMessage);
            return;
        }

        if(this.hasSameOpportunityServiceItems){
            if((this.commodity === 'Gas') && (this.opportunityServiceItems.length>0) && (this.opportunityServiceItems[0].ServicePointCode__c !== fieldList.ServicePointCode__c) && (this.opportunityServiceItems[0].Distributor__r.SelfReadingEnabled__c !== this.selfReadingEnabled)){
                this.spinner = false;
                error(this, this.label.allOSIShouldHaveTheSameSelfReading);
                return;
            }

            if(this.hasApplicantNotHolder && (this.opportunityServiceItems.length>0) && (this.opportunityServiceItems[0].ServicePointCode__c !== fieldList.ServicePointCode__c) && (this.opportunityServiceItems[0].Trader__c !== fieldList.Trader__c)){
                this.spinner = false;
                error(this, this.label.allOSIShouldHaveTheSameTrader);
                return;
            }

            if ((this.commodity === 'Gas') && this.hasApplicantNotHolder) {
                if((this.opportunityServiceItems.length>0) && (this.opportunityServiceItems[0].ServicePointCode__c !== fieldList.ServicePointCode__c) && (this.opportunityServiceItems[0].ApplicantNotHolder__c !== fieldList.ApplicantNotHolder__c)){
                    this.spinner = false;
                    error(this, this.label.allOSIShouldHaveTheSameApplicantNotHolder);
                    return;
                }

            } else if (this.commodity === 'Electric') {

                if(this.hasApplicantNotHolder && (this.opportunityServiceItems.length>0) && (this.opportunityServiceItems[0].ServicePointCode__c !== fieldList.ServicePointCode__c) && (this.opportunityServiceItems[0].ApplicantNotHolder__c !== fieldList.ApplicantNotHolder__c)){
                    this.spinner = false;
                    error(this, this.label.allOSIShouldHaveTheSameApplicantNotHolder);
                    return;
                }
                if((this.opportunityServiceItems.length>0) && (this.opportunityServiceItems[0].ServicePointCode__c !== fieldList.ServicePointCode__c) && (this.opportunityServiceItems[0].Distributor__r.IsDisCoENEL__c !== this.isDiscoENEL)){
                    this.spinner = false;
                    error(this, this.label.allDistributersShouldHaveTheSameDiscoEnel);
                    return;
                }
            }
        }
        if(this.isTransferOrProductChange){
            fieldList['SiteDescription__c'] = this.siteDescription;
        }

        if(this.isPlaceholder) {
            fieldList['IsPlaceholder__c'] = true;
        }

        this.template.querySelector('lightning-record-edit-form').submit(fieldList);
    }

    get addClassAutoSave() {
        if (this.autoSave) {
            return 'slds-hide';
        }
        return '';
    }

    _validateFields(fieldList) {
        if ((this.isHardVAS || this.isSoftVAS)){
            return true;
        }

        let labelFieldList = [];
        for (let i in fieldList) {
            if (fieldList.hasOwnProperty(i)) {
                if (this.isElectric) {
                    if (this.REQUIRED_FIELDS_EE.includes(i) && (
                        !fieldList[i] ||
                        (
                            (typeof fieldList[i] === 'string' || fieldList[i] instanceof String) &&
                            fieldList[i].trim() === ''
                        )
                    )
                    ) {
                        labelFieldList.push(i);
                    }
                }
                if (this.isGas) {
                    if (this.REQUIRED_FIELDS_GM.includes(i) && (
                        !fieldList[i] ||
                        (
                            (typeof fieldList[i] === 'string' || fieldList[i] instanceof String) &&
                            fieldList[i].trim() === ''
                        )
                    )
                    ) {
                        labelFieldList.push(i);
                    }
                }
                if ((i === 'PointProvince__c' || i === 'SiteProvince__c') && this.distributionProvincesDetails) {
                    let province = fieldList[i];
                    if (province) {
                        let found = false;
                        for (let code in this.distributionProvincesDetails) {
                            if (this.distributionProvincesDetails.hasOwnProperty(code) && this.distributionProvincesDetails[code].toUpperCase() === province.toUpperCase()) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            labelFieldList.push(i);
                            if (this.submitErrorMessage !== '') {
                                this.submitErrorMessage = this.submitErrorMessage + ' - ';
                            }
                            this.submitErrorMessage = this.submitErrorMessage + this.label.invalidDistributorProvince;
                        }
                    }
                }
            }
        }

        if(this.showServiceSiteSection) {
            if (!this.selectedServiceSite) {
                this.isCheckedSS = true;
                //this.isCheckedPA = false;
                if (this.submitErrorMessage !== '') {
                    this.submitErrorMessage = this.submitErrorMessage + ' - '
                }
                this.submitErrorMessage = this.submitErrorMessage + this.label.requiredServiceSite;
            }
            else if ((this.isElectric && !this.siteNLC) || (this.isGas && !this.siteCLC)) {
                this.submitErrorMessage = 'Incomplete Service Site data';
            }
        }

        if (labelFieldList.length !== 0) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (labelFieldList.includes(element.fieldName)) {
                    element.classList.add('slds-has-error');
                }
            });
            return false;
        }
        return true;
    }

    removeError(event) {
        let inputField = event.target;
        inputField.classList.remove('slds-has-error');
    }

    handleCancel() {
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    disabledSaveButton(event) {
        this.isNormalize = event.detail.isnormalize;
        this.isCheckedPA = false;
    }

    disabledSaveButtonPointAddress(event) {
        this.addressForced = event.detail.addressForced;
        this.isCheckedPA = false;
        let addrForm = this.template.querySelector('div.pointAddressData c-mro-address-form');

        let list = [];
        let valueSetValues = addrForm.getValues();

        Object.keys(valueSetValues).forEach(function (val) {
            list.push('"' +
                val.replace("__c", "").substring(5).charAt(0).toLowerCase() +
                val.replace("__c", "").substring(5).slice(1) +
                '":' +
                '"' +
                valueSetValues[val] +
                '"'
            );
        });
        this.address = JSON.parse('{' + list + '}');

        this.address.addressNormalized = event.detail.isnormalize;
    }

    disabledSaveButtonSiteAddress(event) {
        this.isNormalize = event.detail.isnormalize;
        this.isCheckedSS = false;
        this.addressForcedSite = event.detail.addressForced;
    }

    get addClassPA() {
        if (this.isCheckedPA && !this.isPlaceholder) {
            return 'slds-box wr-boxBorder';
            //return 'slds-box slds-grid slds-wrap slds-m-top_small slds-col slds-size_8-of-12 slds-m-right_medium wr-boxBorder';
        }
        return 'slds-box';
        //return 'slds-box slds-grid slds-wrap slds-m-top_small slds-col slds-size_8-of-12 slds-m-right_medium';
    }

    get addClassSS() {
        if (this.isCheckedSS) {
            return 'wr-boxBorder';
            //return 'slds-box slds-grid slds-wrap slds-m-top_small slds-col slds-size_4-of-12 wr-boxBorder';
        }
        //return 'slds-box slds-grid slds-wrap slds-m-top_small slds-col slds-size_4-of-12';
        return '';
    }

    get addClassHasSubProcessNone(){
        if (this.hasSubProcessNone) {
            return 'wr-required';
        }
        return '';
    }

    get addClassDistributor(){
        if (this.isDiscoENEL) {
            return 'wr-required';
        }
        return '';
    }
    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check

    isBusinessClient() {
        let inputs = {accountId: this.accountId};
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'isBusinessClient',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                else if (!response.data.isBusinessClient) {
                    let naceReference = this.template.querySelector('[data-id="naceReference"]');
                    let estimatedConsumption = this.template.querySelector('[data-id="estimatedConsumption"]');
                    let consumptionCategoryCmp = this.template.querySelector('[data-id="consumptionCategory"]');
                    if(!this.isTransferOrProductChange){
                        this.siteDescription = 'Locuinta';
                    } else {
                        if(!this.siteDescription){
                            this.siteDescription = 'Locuinta';
                        }
                    }
                    if(this.commodity === 'Gas'){
                        if((!estimatedConsumption.value)) {
                            estimatedConsumption.value = '9';
                            consumptionCategoryCmp.value = 'C1';
                        }
                    }

                    if(naceReference && !naceReference.value && this.naceId){
                        naceReference.value = this.naceId;
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    getMapKWhByConsumption() {
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getMapKWhByConsumptionName'
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let mapKWhByConsumptionNames = {};
                mapKWhByConsumptionNames= response.data.mapKWhByConsumptionNames;
                this.mapKWhByConsumptionNames = mapKWhByConsumptionNames;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    getOSIList() {
        let myOpportunity = this.template.querySelector('[data-id="myOpportunity"]');
        if(!this.opportunityId && myOpportunity.value){
            this.opportunityId= myOpportunity.value;
        }
        if(this.opportunityId){

            let inputs = {opportunityId: this.opportunityId};
            notCacheableCall({
                className: 'MRO_LC_OpportunityServiceItem',
                methodName: 'getOpportunityServiceItems',
                input: inputs
            })
                .then((response) => {
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    }
                    this.opportunityServiceItems = response.data.opportunityServiceItems;
                })
                .catch((errorMsg) => {
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                });
        }

    }

    callOutPreCheckData (){
        let inputDistributor = this.template.querySelector('[data-id="distributor"]').value;
        let inputTrader = this.template.querySelector('[data-id="trader"]').value;
        this.disableSaveButton = true;
        this.spinner = true;

        if(inputDistributor && inputTrader && this.servicePointCode){

            let inputs = {distributorId: inputDistributor, traderId: inputTrader, servicePointCode: this.servicePointCode, requestType: this.requestType};
            notCacheableCall({
                className: 'MRO_LC_OpportunityServiceItem',
                methodName: 'callOutPreCheckData',
                input: inputs
            })
                .then((response) => {
                    this.spinner = false;
                    if (response.isError) {
                        error(this, JSON.stringify(response.error));
                    }

                    if(response.data.preCheckOK){

                        if(response.data.msg === 'PreCheckDisabled'){
                            info(this, 'PreCheck CallOut Disabled');
                            this.disablePreCheckButton = true;
                            this.disableSaveButton = false;
                            return;
                        }else{
                            success(this, response.data.msg);
                            this.disablePreCheckButton = true;
                            this.disableSaveButton = false;
                            return;
                        }

                    }else{
                        error(this, response.data.msg);
                        return;
                    }
                })
                .catch((errorMsg) => {
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                    this.spinner = false;
                });
        }

    }

    get showPreCheckButton (){
        if(this.hasPreCheck){
            return '';
        }
        return 'slds-hidden';
    }

    getNaceIdByCode() {
        let naceCode = '9801';
        let input = {naceCode: naceCode};
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getNaceIdByCode',
            input: input
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                this.naceId = response.data.naceId;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }


    loadOsiByServiceSiteDescr(){
        let recordId = this.recordId;
        let inputs = {
            recordId: recordId
        };
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'updateServiceSiteDescription',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                if (response.data.osi){
                    let osi = response.data.osi;
                    this.siteDescription = osi.SiteDescription__c ? osi.SiteDescription__c : 'Locuinta';
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
    }
}