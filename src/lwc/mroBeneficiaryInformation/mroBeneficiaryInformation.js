import {LightningElement, track, api} from 'lwc';
import {labels} from "c/mroLabels";
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error} from 'c/notificationSvc';

export default class MroBeneficiaryInformation extends LightningElement {
    labels = labels;

    @api assetId;
    @api disabled;

    validateEmail(email) {
        const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regExp.test(String(email).toLowerCase());
    }

    validatePhone(phoneNumber) {
        const regExp = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;
        return regExp.test(phoneNumber);
    }

    isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    isNullOrEmpty(variable) {
        return (variable === undefined || variable === null || variable.length === 0);
    }

    @api
    getBeneficiaryDetails() {
        let beneficiaryNameField = this.template.querySelector('[data-id="ContactPerson__c"]');
        let beneficiaryPhoneField = this.template.querySelector('[data-id="ContactPhone__c"]');
        let beneficiaryEmailField = this.template.querySelector('[data-id="ContactEmail__c"]');
        return {
            Name: beneficiaryNameField ? beneficiaryNameField.value : null,
            Phone: beneficiaryPhoneField ? beneficiaryPhoneField.value : null,
            Email: beneficiaryEmailField ? beneficiaryEmailField.value : null,
        }
    }

    @api
    validateFields() {
        let beneficiary = this.getBeneficiaryDetails();

        // Check for the required fields
        if (this.isNullOrEmpty(beneficiary.Name) || this.isNullOrEmpty(beneficiary.Phone)){
            error(this, labels.nameAndPhoneAreMandatory);
            return false;
        }

        // Check the phone format
        if (!this.validatePhone(beneficiary.Phone)) {
            error(this, this.labels.invalidMobile.replace('{0}', beneficiary.Phone));
            return false;
        }

        // If email is provided, check its format
        if (!this.isNullOrEmpty(beneficiary.Email) && !this.validateEmail(beneficiary.Email)) {
            error(this, this.labels.invalidEmail.replace('{0}', beneficiary.Email));
            return false;
        }

        return true;
    }

    @api
    saveBeneficiaryInformation() {
        this.template.querySelector("lightning-record-edit-form").submit();
    }

    @api
    copyBeneficiaryInformation(accountId){
        if (this.isNullOrEmpty(accountId)){
            console.error('accountId is empty');
            return;
        }

        let inputs = {
            accountId: accountId,
            assetId: this.assetId
        };

        notCacheableCall({
            className: "MRO_LC_BeneficiaryInformation",
            methodName: "copyFromAccount",
            input: inputs
        })
            .then(response => {
                let result = Object.assign({}, response.data);

                if (result.error) {
                    error(this, response.errorMsg);
                    return;
                }

                let beneficiaryNameField = this.template.querySelector('[data-id="ContactPerson__c"]');
                let beneficiaryPhoneField = this.template.querySelector('[data-id="ContactPhone__c"]');
                let beneficiaryEmailField = this.template.querySelector('[data-id="ContactEmail__c"]');

                beneficiaryNameField.value = result.name;
                beneficiaryPhoneField.value = result.phone;
                beneficiaryEmailField.value = result.email;

                this.saveBeneficiaryInformation();
            }).catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    @api
    isEmpty(){
        let beneficiary = this.getBeneficiaryDetails();
        return this.isNullOrEmpty(beneficiary.Name) && this.isNullOrEmpty(beneficiary.Phone) && this.isNullOrEmpty(beneficiary.Email);
    }
    handleError(event) {
        error(this, event.detail.message);
    }

    handleSuccess(event) {

    }
}
