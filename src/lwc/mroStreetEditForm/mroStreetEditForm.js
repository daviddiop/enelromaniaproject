/**
 * Created by A701948 on 25/08/2019.
 */
import { LightningElement, api, wire, track } from "lwc";
import cacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.cacheableCall";
import { error, success } from "c/notificationSvc";
import getValues from "@salesforce/apex/MRO_LC_Address.getValues";

import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { labels } from "c/labels";

const CREATE_CITY_TEXT = ": ++Create New City";

export default class MroStreetEditForm extends LightningElement {

  @api recordId = "";

  @track showCreateCityForm = false;

  @track initial;
  formTitle;
  labels = labels;

  //-----------------------
  @track address = {};
  //-----------------------
  @track countries = [];
  @track provinces = [];
  @track cities = [];
  @track postalCodes = [];
  @track streetTypes = [];
  @track streetNames = [];
  @track fromStreetNumbers = [];
  @track toStreetNumbers = [];
  @track evenOdds = [];

  get readOnly() {
    return !!this.recordId;
  }

  listNames = {
    "country": "countries",
    "province": "provinces",
    "city": "cities",
    "postalCode": "PostalCodes",
    "streetType": "streetTypes",
    "streetName": "streetNames"
  };

  connectedCallback() {
    this.initialize();
    this.formTitle = this.recordId ? "Edit Street" : "New Street";
  }

  initialize() {
    this.recordId && this.retrieveStreet();
    this.initLists();
  }

  initLists() {
    cacheableCall({
      className: "MRO_UTL_Utils",
      methodName: "getAllConstants"
    })
      .then(apexResponse => {
        if (apexResponse) {
          if (apexResponse.isError) {
            error(this, apexResponse.error);
          } else {
            // this.streetTypes = apexResponse.data.streetTypes;
            // this.provinces = apexResponse.data.provinces.slice(0, 10);
            // this.countries = apexResponse.data.countries.slice(0, 10);
            this.evenOdds = apexResponse.data.evenOdds;
          }
        }
      });
  }

  retrieveStreet() {
    const inputs = { streetId: this.recordId };
    notCacheableCall({
      className: "MRO_LC_Address",
      methodName: "retrieveStreet",
      input: inputs
    }).then(apexResponse => {
      if (apexResponse) {
        if (apexResponse.isError) {
          error(this, apexResponse.error);
        } else {
          const address = {};
          address.city = apexResponse.data.City__r.Name;
          address.country = apexResponse.data.City__r.Country__c;
          address.province = apexResponse.data.City__r.Province__c;
          address.streetType = apexResponse.data.StreetType__c;
          address.streetName = apexResponse.data.Name;
          address.postalCode = apexResponse.data.PostalCode__c;
          address.evenOdd = apexResponse.data.Even_Odd__c;
          address.fromStreetNumber = apexResponse.data.FromStreetNumber__c;
          address.toStreetNumber = apexResponse.data.ToStreetNumber__c;
          this.address = address;
        }
      }
    });
  }

  closeModal() {
    this.dispatchEvent(new CustomEvent("formclose"));
  }

  // saveAddress() {
  //   if (this._checkMandatoryFields()) {
  //
  //     if (this.checkFieldsType()) {
  //       let addresses = this.template.querySelectorAll("c-mro-address-edit-form");
  //
  //       let address = {
  //         id: this.recordId,
  //         ...addresses[0].getAddress(),
  //         ...addresses[1].getAddress()
  //       };
  //       createAddress({ address })
  //         .then(() => {
  //           success(this, "address saved");
  //           this.closeModal();
  //         })
  //         .catch(error => {
  //           error(this, error);
  //           console.log(error);
  //         });
  //     }
  //   }
  // }
  //
  //
  // saveAddress2() {
  //   const address = {};
  //
  //   address.id = this.recordId;
  //   address.country = this.template.querySelector(".text-input-country").value;
  //   address.province = this.template.querySelector(".text-input-province").value;
  //   address.city = this.template.querySelector(".text-input-city").value;
  //
  //   address.postalCode = this.template.querySelector(".text-input-postalCode").value;
  //   address.streetType = this.template.querySelector(".text-input-streetType").value;
  //   address.streetName = this.template.querySelector(".text-input-streetName").value;
  //   address.fromStreetNumber = this.template.querySelector(".int-input-streetNumber").value;
  //   address.streetNumber = this.template.querySelector(".int-input-streetNumber").value;
  //   address.toStreetNumber = this.template.querySelector(".text-input-extension").value;
  //   address.evenOdd = "B";
  //   address.streetId = "";
  //
  //   address.locality = this.template.querySelector(".text-input-locality").value;
  //   address.building = this.template.querySelector(".text-input-building").value;
  //   address.apartement = this.template.querySelector(".text-input-apartement").value;
  //   address.floor = this.template.querySelector(".text-input-floor").value;
  //
  //   this.address1 = address;
  //
  //   createAddress({ address: this.address1 })
  //     .then(() => {
  //       success(this, "address saved");
  //       this.closeModal();
  //     })
  //     .catch(error => {
  //       error(this, error);
  //       console.log(error);
  //     });
  //   console.log("address1 values ", JSON.stringify(this.address1));
  // }

  handleChange(event) {
    this.removeError(event);
  }

  _checkMandatoryFields() {
    let addrFields = this.template.querySelectorAll("c-mro-address-input");
    let areValid = true;
    for (let f of addrFields) {
      if (f.required && !f.getValue()) {
        f.classList.add("slds-has-error");
        areValid = false;
      }
    }
    if (!areValid) {
      error(this, this.labels.requiredFields);
      return false;
    }
    return true;
  }

  checkFieldsType(event) {
    let { target, value } = event.detail;


    if (target === "FromStreetNumber" && !+value) {
      event.target.classList.add("slds-has-error");
      error(this, "From street number is invalid");
      return;
    }
    // f = Array.from(addrFields).find(e => e.element === "toStreetNumber");
    if (target === "FromStreetNumber" && !+value) {
      event.target.classList.add("slds-has-error");
      error(this, "To street number is invalid");
      return;
    }
    if (target === "EvenOdd" && !this.evenOdds.map(e => e.value).includes(value)) {
      event.target.classList.add("slds-has-error");
      error(this, "Even/Odd is invalid");
      return;
    }
    return true;
  }

  removeError(event) {
    event.target.classList.remove("slds-has-error");
  }

  async handleFieldChange(event) {
    debugger
    this.handleChange(event);
    this.address = { ...this.address, [event.detail.target]: event.detail.value };

    const { value, target } = event.detail;


    this.address = { ...this.address, [target]: value };

    const address = Object.keys(this.address).reduce((address, fieldName) => ({
      ...address,
      [fieldName.replace(/^\w/, (s) => s.toUpperCase())]: this.address[fieldName]
    }), {});

    if (value && value.trim() && value.trim().length >= 2 && this.listNames[target] /*&& addressPart && addressPart.ParentsFields__c*/) {
      const values = await getValues({ fieldName: target, address });
      const listValues = values.map(value => ({ key: value.replace(/\s*/g, "_"), value: value }));
      console.log("loaded", listValues.length, target);
      console.log("loaded", listValues);
      this[this.listNames[target]] = listValues;
    }

    // this.addressParts={...this.addressParts  }

  }

  //
  // onCityFormClose(event) {
  //     let { city, success, country, province } = event.detail;
  //     this.showCreateCityForm = false;
  //     this.template.querySelector(".city-input").setValue(success ? city : this.city);
  //     this.template.querySelector(".country-input").setValue(success ? country : "");
  //     this.template.querySelector(".province-input").setValue(success ? province : "");
  // }

  // createCity(city) {
  //     this.showCreateCityForm = true;
  //     this.city = city;
  // }

  // async handleCityChange(event) {
  //     const startWith = event.target.getValue().trim();
  //
  //     if (!startWith.includes(CREATE_CITY_TEXT)) {
  //         if (startWith && startWith.length >= 3) {
  //             let strings = await getCompletionStrings({
  //                 field: "city",
  //                 startWith
  //             });
  //             const createText = startWith + CREATE_CITY_TEXT;
  //             if (strings && strings.map) {
  //                 let cities = strings.map(s => ({ key: s, value: s }));
  //                 if (!strings.includes(startWith)) {
  //                     cities.push({ key: createText, value: createText });
  //                 }
  //                 this.cities = cities;
  //             }
  //         }
  //     } else {
  //         // alert("startwith = " + startWith);
  //         //this.showCreateCityForm = true
  //         let city = startWith.replace(CREATE_CITY_TEXT, "");
  //         this.createCity(city);
  //     }
  // }
}