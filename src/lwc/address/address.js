//import normalize from "@salesforce/apex/AddressCnt.normalize";
import { error, success } from "c/notificationSvc";

export default class Address {

    _id = ''
    _streetType = ''
    _streetName = ''
    _streetNumber = ''
    _city = ''
    _postalCode = ''
    _locality = ''
    _province = ''
    _country = ''
    _building = ''
    _apartment = ''
    _floor = ''
    _isValid = ''


    get id() {
        return this._id;
    }
    set id(id) {
        this._id = id;
    }
    get streetType() {
        return this._streetType;
    }
    set streetType(streetType) {
        this._streetType = streetType;
    }
    get streetName() {
        return this._streetName;
    }
    set streetName(streetName) {
        this._streetName = streetName;
    }
    get streetNumber() {
        return this._streetNumber;
    }
    set streetNumber(streetNumber) {
        this._streetNumber = streetNumber;
    }
    get city() {
        return this._city;
    }
    set city(city) {
        this._city = city;
    }
    get postalCode() {
        return this._postalCode;
    }
    set postalCode(postalCode) {
        this._postalCode = postalCode;
    }
    get locality() {
        return this._locality;
    }
    set locality(locality) {
        this._locality = locality;
    }
    get province() {
        return this._province;
    }
    set province(province) {
        this._province = province;
    }
    get country() {
        return this._country;
    }
    set country(country) {
        this._country = country;
    }
    get building() {
        return this._building;
    }
    set building(building) {
        this._building = building;
    }
    get apartment() {
        return this._apartment;
    }
    set apartment(apartment) {
        this._apartment = apartment;
    }
    get floor() {
        return this._floor;
    }
    set floor(floor) {
        this._floor = floor;
    }

    get formatted() {
        let lines = [];
        let streetLines = [];
        if (this.streetType) streetLines.push(this.streetType);
        if (this.streetName) streetLines.push(this.streetName);
        if (this.streetNumber) streetLines.push(this.streetNumber);
        lines.push(streetLines.join(' '));


        if (this.city || this.province || this.postalCode) {
            let divisionLines = [];
            if (this.city) divisionLines.push(this.city);
            if (this.province) divisionLines.push('(' + this.province + ')');
            if (this.postalCode) divisionLines.push(this.postalCode);
            lines.push(divisionLines.join(' '));
        }
        return lines.join(', ');
    }

    get isValid() {
        return this._isValid;
    }

    validate() {
        success(this, 'Validated');
        this._isValid = true;
        /*
        normalize({
            address: this.toJSON()

        })
            .then(response => {
                if (response) {
                    if (response.isValid) {
                        success(this, 'Validated')
                        this._isValid = true;
                    } else {
                        error(this, 'Address not valid');
                        this._isValid = false;
                    }
                }
            })
            .catch(errorMsg => {
                error(this, errorMsg);
            }); */
    }


    toJSON() {
        return {
            id: this.id,
            streetType: this.streetType,
            streetName: this.streetName,
            streetNumber: this.streetNumber,
            city: this.city,
            postalCode: this.postalCode,
            locality: this.locality,
            province: this.province,
            country: this.country,
            building: this.building,
            apartment: this.apartment,
            floor: this.floor,
            isValid: this.isValid
        }
    }

}