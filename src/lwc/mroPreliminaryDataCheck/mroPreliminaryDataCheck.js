/**
 * Created by tommasobolis on 20/10/2020.
 */

import { LightningElement, api, track } from 'lwc';
import { labels } from 'c/mroLabels';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import doPreliminaryDataCheck from '@salesforce/apex/MRO_LC_PreliminaryDataCheck.doPreliminaryDataCheck';

export default class MroPreliminaryDataCheck extends LightningElement {

    @api label = labels;
    @api recordId;
    @api doCheckOnInit=false;
    @api showSpinner=false;
    @api showPrompt;
    @api title = labels.dataPreliminaryCheck;

    continueButtonLabel = labels.continueLabel;

    @track blocking;
    @track warning;

    get hasBlocking() {
        let hasBlocking = this.blocking && this.blocking.length > 0;
        return hasBlocking;
    }

    get hasWarning() {
        let hasWarning = this.warning && this.warning.length > 0;
        return hasWarning;
    }

    get hasFailedChecks() {

        let hasFailedChecks = (this.blocking && this.blocking.length > 0) ||
                (this.warning && this.warning.length > 0);
        return hasFailedChecks;
    }

    connectedCallback() {
        console.log('MroPreliminaryDataCheck.connectedCallback - Enter');
        if(this.doCheckOnInit) this.doPreliminaryDataCheck();
    }

    @api
    doPreliminaryDataCheck() {
        console.log('MroPreliminaryDataCheck.doPreliminaryDataCheck - Enter');
        console.log('MroPreliminaryDataCheck.doPreliminaryDataCheck - recordId', this.recordId);
        this.showSpinner = true;
        doPreliminaryDataCheck( {recordId: this.recordId} )
            .then(result => {
                if (result.error == false) {
                    this.blocking = result.blocking;
                    this.warning = result.warning;
                    console.log('MroPreliminaryDataCheck.doPreliminaryDataCheck (Callback) blocking', result.blocking);
                    console.log('MroPreliminaryDataCheck.doPreliminaryDataCheck (Callback) warning', result.warning);
                    if(this.hasBlocking) {

                        this.dispatchEvent(new CustomEvent('blocking'));
                    }
                    if(this.hasFailedChecks) this.showPrompt = true;
                } else {
                    this.showError(result.errorMessage);
                    console.log('MroPreliminaryDataCheck.doPreliminaryDataCheck (Callback) errorMessage', result.errorMessage);
                    console.log('MroPreliminaryDataCheck.doPreliminaryDataCheck (Callback) errorStackTraceString', result.errorStackTraceString);
                }
                this.showSpinner = false;
            })
    }

    handleContinueClick(event) {
        this.dispatchEvent(new CustomEvent('continue'));
        this.showPrompt = false;
    }

    showError(message) {
       let toastTitle = 'Error';
       let toastMsg = message;
       let toastVariant = 'error';
       this.dispatchEvent(
           new ShowToastEvent({
               title: toastTitle,
               message: toastMsg,
               variant: toastVariant
           })
       );
   }
}