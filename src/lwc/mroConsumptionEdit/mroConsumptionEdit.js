import {LightningElement, api} from 'lwc';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';

export default class MroConsumptionEdit extends LightningElement {
    @api consumption;
    @api servicePointCode;
    @api roundConsumption;

    labels = labels;

    handleSubmit(event) {
        event.preventDefault();
        const fields = event.detail.fields;
        let fieldsToAdjust =  event.detail.fields;
        if(this.roundConsumption){
            let sum = 0;
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityJanuary__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityFebruary__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityMarch__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityApril__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityMay__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityJune__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityJuly__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityAugust__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantitySeptember__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityOctober__c']);
            sum = sum + this.extractDecimalPart(fieldsToAdjust['QuantityNovember__c']);
            let december = Math.round(parseFloat(fieldsToAdjust['QuantityDecember__c']) + sum);
            fieldsToAdjust['QuantityJanuary__c'] =  parseInt(fieldsToAdjust['QuantityJanuary__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityFebruary__c'] =  parseInt(fieldsToAdjust['QuantityFebruary__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityMarch__c'] =  parseInt(fieldsToAdjust['QuantityMarch__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityApril__c'] =  parseInt(fieldsToAdjust['QuantityApril__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityMay__c'] =  parseInt(fieldsToAdjust['QuantityMay__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityJune__c'] =  parseInt(fieldsToAdjust['QuantityJune__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityJuly__c'] =  parseInt(fieldsToAdjust['QuantityJuly__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityAugust__c'] =  parseInt(fieldsToAdjust['QuantityAugust__c']).toFixed(3).toString();
            fieldsToAdjust['QuantitySeptember__c'] =  parseInt(fieldsToAdjust['QuantitySeptember__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityOctober__c'] =  parseInt(fieldsToAdjust['QuantityOctober__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityNovember__c'] =  parseInt(fieldsToAdjust['QuantityNovember__c']).toFixed(3).toString();
            fieldsToAdjust['QuantityDecember__c'] =  december.toFixed(3).toString();
        }

        this.dispatchEvent(new CustomEvent('consumption_edit_save', {
            detail: {
                consumptionId: this.consumption.myId,
                consumptionDetails: this.roundConsumption ? fieldsToAdjust : fields
            }
        }));
    }

    handleCancel(event) {
        this.dispatchEvent(new CustomEvent('consumption_edit_cancel'));
    }

    extractDecimalPart(numberToEdit){
        let numberToAdd = parseFloat(numberToEdit) % 1;
        return  numberToAdd > 0 ?  numberToAdd : 0 ;
    }
}