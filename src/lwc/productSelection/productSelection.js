/**
 * Created by Octavian on 7/15/2019.
 */

import {LightningElement, api, track, wire} from 'lwc';
import {CurrentPageReference, NavigationMixin} from 'lightning/navigation';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/labels';
import {error} from 'c/notificationSvc';

export default class productSelection extends NavigationMixin(LightningElement) {
    FALSE = false;
    LABELS = labels;

    @api opportunityId;

    @track opportunity;
    @track currentPageReference;
    @track productFamilyList = [];
    @track productTypeList = [];
    @track productList = [];
    @track filteredProductList = [];
    @track productListInCart = [];
    @track navService;
    @track searchTerm;

    @track productFamily;
    @track productType;

    @track selectedProduct;

    //IN PROGRESS
    @track selectedPricebookId;
    @track pricebookList = [];

    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        this.currentPageReference = currentPageReference;
    }

    @api
    reload(newOpp) {
        this.opportunityId = newOpp;
        this.productListInCart = [];
        this.initialize();
    }

    connectedCallback() {
        this.initialize();
    }

    initialize() {

        notCacheableCall({
            className: 'ProductCnt',
            methodName: 'getInitData',
            input: {
                'opportunityId': this.opportunityId,
            },
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    if (response.data.productList) {
                        this.opportunity = response.data.opportunity;
                        this.productList = response.data.productList;
                        this.filteredProductList = response.data.productList;
                        let tmpProductFamilyList = ['All'].concat(response.data.productFamilyList);
                        this.productFamilyList = this.toLabelName(tmpProductFamilyList);
                        let tmpProductTypeList = ['All'].concat(response.data.productTypeList);
                        this.productTypeList = this.toLabelName(tmpProductTypeList);

                        if (response.data.pricebookList) {
                            this.pricebookList = response.data.pricebookList;
                        } else {
                            this.selectedPricebookId = response.data.pricebookId;
                        }
                        if (response.data.opportunity.Pricebook2Id) {
                            this.selectedPricebookId = response.data.opportunity.Pricebook2Id;
                        }
                        if (response.data.existProductList) {
                            this.productListInCart = response.data.existProductList;
                        }
                        let productCartComponent = this.template.querySelector('[data-id="productCart"]');
                        if (productCartComponent) {
                            if (productCartComponent instanceof Array) {
                                let productCartComponentToObj = Object.assign({}, productCartComponent);
                                productCartComponentToObj[0].reload();
                            } else {
                                productCartComponent.reload();
                            }
                        }
                    }
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    onCartClick() {
        this.template.querySelector('[data-id="productCart"]').open();
    }

    onProductFamilySelect(event) {
        this.productFamily = event.detail.name;
        if (this.productFamily === 'All') {
            this.filteredProductList = this.productList;
            return;
        }
        this.filteredProductList = this.filterByFamily(this.productList, this.productFamily);
    }

    onProductTypeSelect(event) {
        this.productType = event.detail.name;
        if (this.productType === 'All') {
            this.filteredProductList = this.productList;
            return;
        }
        this.filteredProductList = this.filterByProductType(this.productList, this.productType);
    }

    onKeyUp(event) {
        if (event.key === 'Enter') {
            this.search();
        }
    }

    onBlur() {
        this.search();
    }

    onSearchTermChange(event) {
        this.searchTerm = event.target.value;
        if (!this.searchTerm) {
            this.search();
        }
    }

    onProductSelect(event) {
        this.selectedProduct = event.detail.product;
        let action = event.detail.action;
        switch (action) {
            case "details": {
                this.template.querySelector('[data-id="productDetails"]').open();
                break;
            }
            case "add": {
                this.handleAddButtonPress();
                break;
            }
            case "addToCart": {
                let newProductList = event.detail.product;
                if (newProductList) {
                    let resultProductList = this.productListInCart.concat(newProductList);
                    this.productListInCart = resultProductList;
                }
                this.template.querySelector('[data-id="productCart"]').open();
                break;
            }
            case "configure": {
                let productConfigurator = this.template.querySelector('[data-id="productConfig"]');
                productConfigurator.productList = [this.selectedProduct];
                productConfigurator.productFamilyList = [];
                productConfigurator.open();
                break;
            }
            default:
                break;
        }
    }

    hasOpportunityId() {
        return !(this.opportunityId === '');
    }

    hasFilters() {
        return this.productFamilyList.length > 0 || this.productTypeList.length > 0
    }

    toLabelName(values) {
        if (!values || !values.length) {
            return [];
        }
        return values.map(function (value) {
            return {
                'label': value,
                'name': value,
                'expanded': true
            }
        })
    }

    filterByProductType(productList, productType) {
        return productList.filter(product => product.recordTypeName === productType);
    }

    filterByFamily(productList, family) {
        return productList.filter(product => product.family === family);
    }

    searchFilter(product) {
        return (product.name && product.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) !== -1);
    }

    search() {
        if (!this.searchTerm) {
            this.filteredProductList = this.productList;
        }
        if (this.searchTerm && this.searchTerm.length > 1) {
            this.filteredProductList = this.productList.filter(product => this.searchFilter(product));
        }
    }

    handleAddButtonPress() {
        notCacheableCall({
            className: 'ProductCnt',
            methodName: 'productsAndFamilies',
            input: {
                'productId': this.selectedProduct.id,
            },
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    let productConfigurator = this.template.querySelector('[data-id="productConfig"]');
                    productConfigurator.productList = response.data['productList'];
                    productConfigurator.productFamilyList = response.data['productFamilyList'];
                    productConfigurator.open();
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    handleCheckout(event) {
        let productList = event.target.productList;
        this.template.querySelector('[data-id="productCart"]').close();
        notCacheableCall({
            className: 'ProductCnt',
            methodName: 'checkout',
            input: {
                'opportunityId': this.opportunityId,
                'pricebookId': this.selectedPricebookId,
                'productList': JSON.stringify(productList),
            },
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.navigateBackToWizard();
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }


    navigateBackToWizard() {
        let requestType = this.opportunity.RequestType__c === 'Connection' ? 'Activation' : this.opportunity.RequestType__c;
        let wizard = 'c__MRO_LCP_' + requestType + 'Wizard';
        const navToWizardEvent = new CustomEvent('navtowizard', {
            detail: {
                wizardType: wizard,
                accountId: this.opportunity.AccountId,
                opportunityId: this.opportunityId
            }
        });
        this.dispatchEvent(navToWizardEvent);
    }
}