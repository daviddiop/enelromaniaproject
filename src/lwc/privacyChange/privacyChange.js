import { LightningElement, api, track } from 'lwc';
import { labels } from 'c/labels';
import { error, success } from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class PrivacyChange extends LightningElement {
    labels = labels;
    @api opportunityId;
    @api disabled = false;
    @track individualId;
    @track recordId;
    @track privacyChange = [];


    connectedCallback() {
        this.getIndividualId();
    }

    getIndividualId() {
        let inputs = {
            opportunityId: this.opportunityId
        };

        notCacheableCall({
            className: 'PrivacyChangeCnt',
            methodName: 'getIndividual',
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }

                this.individualId = response.data.individualId;
                this.privacyChange= response.data.privacyChange;
                if(this.privacyChange){
                    this.recordId = this.privacyChange.Id;
                }
            })
            .catch(err => {
                error(this, JSON.stringify(err));
            });
    }

     @api savePrivacyChange() {
         this.createRecordPrivacyChange();
     }
/*
    handleSuccess(event) {
        event.stopPropagation();
        let payload = event.detail;
         console.log('payload: ' + JSON.stringify(event.detail));
        const selectedEvent = new CustomEvent('success', {
            detail: {
                privacyChangeId: payload.id,
                dontProcess: payload.fields.HasOptedOutProcessing__c.value
            }
        });
        this.dispatchEvent(selectedEvent);
    }
    */
    handleError(event) {
        error(this, event.detail.message);
    }
    handleSuccess(event) {
          event.stopPropagation();
          let payload = event.detail;
          let dontProcess = false;
          this.template.querySelectorAll('lightning-input-field').forEach(element => {
              if (element.fieldName === "HasOptedOutProcessing__c") {
                  dontProcess = element.value;
              }
          });
          const selectedEvent = new CustomEvent('success', {
              detail: {
                  privacyChangeId: payload.id,
                  dontProcess: dontProcess
              }
          });
          this.dispatchEvent(selectedEvent);
      }
    createRecordPrivacyChange (){
        let fieldList = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fieldList[element.fieldName] = element.value;
        });

        if((this.privacyChange) && (this.privacyChange.Dossier__c != null) && (this.individualId)){
              this.template.querySelector('lightning-record-edit-form').recordId = '';
              fieldList.Individual__c = this.individualId;
        }

        if((this.individualId)){
              fieldList.Individual__c = this.individualId;
        }

        fieldList.Status__c = 'Pending';
        fieldList.Opportunity__c = this.opportunityId;

        this.template.querySelector('lightning-record-edit-form').submit(fieldList);

    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }
}