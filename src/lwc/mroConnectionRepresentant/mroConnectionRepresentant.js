/**
 * Created by David Diop on 10.04.2020.
 */

import {LightningElement, api, track} from 'lwc';
import {error, success} from "c/notificationSvc";
import {labels} from "c/mroLabels";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";

export default class MroConnectionRepresentant extends LightningElement {
    labels = labels;
    @api contactId;
    @api dossierId;
    @api accountId;
    @api accountRecord;
    @api disabledInput;
    @track accountDefaultName;

    @api
    saveContact() {
        this.submitForm();
    }

    submitForm() {
        let fields = {};
        this.template.querySelectorAll("lightning-input-field").forEach(element => {
            fields[element.fieldName] = element.value;
        });
        if (this.validateFields()) {
            if(this.accountRecord.IsPersonAccount){
                this.handleCreateContactAccountRelations(fields)
            }
            if (!this.accountRecord.IsPersonAccount) {
                fields.AccountId = this.accountId;
                this.accountDefaultName = fields.LastName;
                this.template.querySelector("lightning-record-edit-form").submit(fields);
            }
        }
    }

    handleSuccess(event) {
        let payload = JSON.parse(JSON.stringify(event.detail));
        this.contactId = payload.id;
        if (this.accountRecord.IsPersonAccount) {
            this.handleCreateContactAccountRelations(payload.id)
        } else {
            this.handleCreatePrivacyRecord();
            /*const successEvent = new CustomEvent("savedconnection", {
                detail: {
                    contactId: payload.id
                }
            });
            this.dispatchEvent(successEvent);*/
        }
    }
    handleCreatePrivacyRecord(){
        let privacyChangeComponent = this.template.querySelector('[data-id="privacyChange"]');
        if (privacyChangeComponent) {
            if (privacyChangeComponent instanceof Array) {
                let privacyChangeComponentToObj = Object.assign({}, privacyChangeComponent);
                privacyChangeComponentToObj[0].savePrivacyChange();
            } else {
                privacyChangeComponent.savePrivacyChange();
            }
        }
    }
    getPrivacyId(event){
        const successEvent = new CustomEvent("savedconnection", {
            detail: {
                contactId: this.contactId
            }
        });
        this.dispatchEvent(successEvent);
    }

    handleCreateContactAccountRelations(fields) {
        let accountRecord = {};
        accountRecord.FirstName = fields.FirstName;
        accountRecord.LastName = fields.LastName;
        accountRecord.PersonEmail = fields.Email;
        accountRecord.PersonMobilePhone = fields.Phone;
        let inputs = {
            //contactId: contactId,
            accountId: this.accountRecord.Id,
            fields: JSON.stringify(accountRecord)
        };
        notCacheableCall({
            className: "MRO_LC_ConnectionRepresentant",
            methodName: "createContactAccountRelations",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.contactId = apexResponse.data.contactId;
                        this.handleCreatePrivacyRecord();
                        /*const successEvent = new CustomEvent("savedconnection");
                        this.dispatchEvent(successEvent);*/
                    }
                }
            });
    }

    handleError(event) {
        error(this, event.detail.message);
        //error(this, JSON.stringify(event.detail.detail));
    }

    @api
    disableInputField(disabledFields) {
        this.disabledInput = disabledFields;
    }

    validateFields() {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll("lightning-input-field"));
        requireFields.forEach((inputRequiredCmp) => {
            if (inputRequiredCmp.required) {
                let valueInput = inputRequiredCmp.value;
                if (!valueInput || (isNaN(valueInput) && valueInput.trim() === "")) {
                    inputRequiredCmp.classList.add("slds-has-error");
                    error(this, this.labels.requiredFields);
                    areValid = false;
                    return;
                }
            }
        });
        return areValid;
    }
}