/**
 * Created by Boubacar Sow on 03/04/2020.
 */

import {LightningElement, track, api} from 'lwc';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class MroMeterDetailsQueryPeriodSelection extends LightningElement {
    @api serviceSiteId = 'a134E000002G4TcQAK';
    @api recordId;
    @api indexInterrogate='init';
    @track invoiceStartDateDefault;
    @track invoiceStartDate;
    @track invoiceEndDate;
    @api showSelect=false;
    @track display = true;


    @track supplyList;
    @track selectedSupplyId;
    @track selectedSupplyRecordType;
    @track selectedRows;
    @track servicePointId;

    supplyTableColumns = [
        {label: 'Type', fieldName: 'recordTypeName', type: 'text'},
        {label: 'Supply', fieldName: 'name', type: 'text'},
        {label: 'Status', fieldName: 'status', type: 'text'},
        {label: 'Service Point', fieldName: 'servicePointCode', type: 'text'},
        {label: 'Contract Account', fieldName: 'contractAccountName', type: 'text'},
        {label: 'Contract', fieldName: 'contractName', type: 'text'},
        {label: 'Company', fieldName: 'companyDivisionName', type: 'text'}
    ];


    labels = labels;

    connectedCallback() {
        this.invoiceStartDateDefault = this.setInvoiceStartDateDefault(new Date());
        this.invoiceStartDate = this.invoiceStartDateDefault;
        this.invoiceEndDate = this.setInvoiceEndDate(new Date())

        if(this.recordId) {
            this.loadSupplies();
        }

    }

    setInvoiceStartDateDefault(currentDay) {
        let threeMonthsAgo = new Date(currentDay.setMonth(currentDay.getMonth() - 2));
        let threeMonthsAgoDigit = threeMonthsAgo.getMonth();

        if (threeMonthsAgoDigit <= 9) {
            threeMonthsAgoDigit = '0' + threeMonthsAgoDigit;
        }

        return threeMonthsAgo.getFullYear() + "-" + threeMonthsAgoDigit + "-" + ("0" + threeMonthsAgo.getDate()).slice(-2);
    }

    setInvoiceEndDate(currentDay) {
        let monthDigit = currentDay.getMonth() + 1;

        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }

        return currentDay.getFullYear() + "-" + monthDigit + "-" + ("0" + currentDay.getDate()).slice(-2);
    }


    loadSupplies(){
        this.showLoadingSpinner = true;
        notCacheableCall({
            className: 'MRO_LC_MeterDetailsQuery',
            methodName: 'listSupplies',
            input: {
                'serviceSiteId': this.recordId
            }
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }
            if(apexResponse.data.length === 1){
                this.selectedRows = [apexResponse.data[0].Id];
                this.servicePointId = apexResponse.data[0].ServicePoint__c;
                this.selectedSupplyId = apexResponse.data[0].Id;
                this.selectedSupplyRecordType = apexResponse.data[0].RecordType.Name;
            }
            this.supplyList = apexResponse.data.map(item => {
                return {
                    Id : item.Id,
                    ServicePoint__c : item.ServicePoint__c,
                    name : item.Name,
                    status : item.Status__c,
                    recordTypeName : item.RecordType.Name,
                    servicePointCode : item.ServicePoint__r ? item.ServicePoint__r.Code__c : '',
                    contractAccountName: item.ContractAccount__r ? item.ContractAccount__r.Name : '',
                    contractName: item.Contract__r ? item.Contract__r.ContractNumber : '',
                    companyDivisionName: item.CompanyDivision__r ? item.CompanyDivision__r.Name : '',
                };
            });
        });
    }

    handleSupplySelection(event){
        this.selectedSupplyId = event.detail.selectedRows[0].Id;
        this.selectedSupplyRecordType = event.detail.selectedRows[0].recordTypeName;
        this.servicePointId = event.detail.selectedRows[0].ServicePoint__c;
        this.template.querySelector('c-mro-meter-details-query').hideList();
    }

    get disabled(){
        return !this.servicePointId || !this.invoiceStartDate || !this.invoiceEndDate;
    }


    onHandleInvoiceInquiryStartDateChange(event) {
        let inputStartDate = event.target.value;

        if (this.invoiceEndDate) {
            let parsedInputStartDate = Date.parse(inputStartDate);
            let parsedInvoiceEndDate = Date.parse(this.invoiceEndDate);

            if (parsedInputStartDate >= parsedInvoiceEndDate) {
                error(this, labels.startDateAfterEndDate);
                inputStartDate = "";
                event.target.value = inputStartDate;
            }
        }
        this.invoiceStartDate = inputStartDate;
    }
    @track value = 'SAP Query Index';
    get options() {
        return [
            { label: 'SAP Query Index', value: 'SAP Query Index' },
            { label: 'SAP Query IVR Gas', value: 'SAP Query IVR Gas' },
        ];
    }

    handleChange(event) {
        event.preventDefault();
        this.value = event.detail.value;
        if(this.value ==='SAP Query Index'){
            this.template.querySelector('c-mro-meter-details-query').getSelectedEntryIndex(true);
        }
         else if(this.value ==='SAP Query IVR Gas'){
             this.template.querySelector('c-mro-meter-details-query').getSelectedEntryIndex(false);
         }
    }

}