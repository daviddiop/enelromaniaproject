import {api, LightningElement, track} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';

export default class MroFisaInquiry extends LightningElement {

    @api codCompanie;
    @api codDePlataId;
    @api cuSold;
    @api facturiDela;
    @api startDate;
    @api endDate;
    @api facturiPanala;
    @api incasatDela;
    @api incasatPanala;
    @api partnerId;
    @api accountId;
    // Add by INSA for the task [ENLCRO-736]
    @api hideConfirmButton = false;
    @api hideCheckBox = false;
    @api retrieveName = 'Retrieve FISA';
    @api subProcess;
    @api dateSold;
    @api taxNum;
    @api processName;

    //Add by Bouba for the task [ENLCRO-867] Account Situation - Delta changes
    @api hideSelectionColumn;
    @track fisaList;
    @track fisaDetailsList = [];
    @track detailListRendered;
    @track selectAll;
    @track totalInvoiceValue = 0;
    @track totalDebtorTurnover = 0;
    @track totalCreditorTurnover = 0;
    @track totalHip = 0;
    @track disableButton = false;

    @track showLoadingSpinner = false;
    @track showIllustrationEmptyStat = false;

    labels = labels;
    fisaColumns = [
        {label: this.labels.nrCollectionDocument, fieldName: 'nrCollectionDocument', type: 'text', sortable: true},
        {label: this.labels.collectionDocument, fieldName: 'collectionDocument', type: 'text', sortable: true},
        {label: this.labels.dateOfReceiptDocument, fieldName: 'dateOfReceiptDocument', type: 'date', sortable: true},
        {label: this.labels.receiptDocumentValue, fieldName: 'receiptDocumentValue', type: 'number', sortable: true},
        {label: this.labels.collectionPartner, fieldName: 'collectionPartner', type: 'text', sortable: true},
        {label: this.labels.collectionOperationDate, fieldName: 'collectionOperationDate', type: 'date', sortable: true}
    ];
    handleRetrieveFisa() {
        this.showLoadingSpinner = true;
        notCacheableCall({
            className: "MRO_LC_FisaInquiry",
            methodName: "listFisa",
            input: {
                'codCompanie': this.codCompanie,
                'codDePlataId': this.codDePlataId,
                'cuSold': this.cuSold,
                'facturiDela': this.facturiDela,
                'facturiPanala': this.facturiPanala,
                'incasatDela': this.incasatDela,
                'incasatPanala': this.incasatPanala,
                'partnerId': this.partnerId,
                'dataSold': this.dateSold,
                'taxNum': this.taxNum,
                'accountId': this.accountId,
                'subProcess': this.subProcess,
                'processName': this.processName
            }
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, this.labels.unexpectedError);
            }
            if (apexResponse.data.error) {
                error(this, apexResponse.data.errorMsg);
            }
            if (!apexResponse.data.error && apexResponse.data.fisaList){
                this.fisaList = JSON.parse(JSON.stringify(apexResponse.data.fisaList));
            }
            this.showIllustrationEmptyStat = !this.fisaList || this.fisaList.length === 0;
            if(this.fisaList){
                for(let i = 0; i < this.fisaList.length; i++) {
                    this.fisaList[i].invoiceValue = parseFloat(this.fisaList[i].invoiceValue).toFixed(2);
                    this.fisaList[i].debtorTurnover = parseFloat(this.fisaList[i].debtorTurnover).toFixed(2);
                    this.fisaList[i].creditorTurnover = parseFloat(this.fisaList[i].creditorTurnover).toFixed(2);
                    this.fisaList[i].hip = parseFloat(this.fisaList[i].hip).toFixed(2);
                    if (this.fisaList[i].invoiceDate) {
                        this.fisaList[i].invoiceDate = this.formatDate(this.fisaList[i].invoiceDate);
                    }
                    if (this.fisaList[i].dueDate) {
                        this.fisaList[i].dueDate = this.formatDate(this.fisaList[i].dueDate);
                    }
                    if (this.fisaList[i].dateOfReceiptDocument) {
                        this.fisaList[i].dateOfReceiptDocument = this.formatDate(this.fisaList[i].dateOfReceiptDocument, true);
                        this.fisaList[i].dateOfReceiptDocumentFormatted = this.formatDate(this.fisaList[i].dateOfReceiptDocument);
                    }
                }
            }
            this.disableButton = true;
            this.emptyFisaList(this.fisaList);
            this.totalGeneral(this.fisaList);
        }).catch((errorMsg) => {
            this.showLoadingSpinner = false;
            error(this, JSON.stringify(errorMsg));
        });
    }

    formatDate(value, forDetails){
        let splitDate = value.split('-'),
            year = splitDate[0],
            month = splitDate[1],
            day = splitDate[2];
        return forDetails ? [year, month, day].join('-') : (day + '.' + month + '.' + year);
    }

    @api
    enabeRetrieveButton(){
        this.disableButton = false;
    }

    /**
     * @author Boubacar Sow
     * @description  compute the total general value in the table.
     *              [ENLCRO-867] Account Situation - Delta changes
     * @param fisaList
     */
    totalGeneral(fisaList) {
        if (fisaList) {
            this.totalInvoiceValue = 0;
            this.totalDebtorTurnover = 0;
            this.totalCreditorTurnover = 0;
            this.totalHip = 0;
            for (let fisa in fisaList) {
                this.totalInvoiceValue = parseFloat(this.totalInvoiceValue) + parseFloat(fisaList[fisa].invoiceValue);
                this.totalDebtorTurnover = parseFloat(this.totalDebtorTurnover) + parseFloat(fisaList[fisa].debtorTurnover);
                this.totalCreditorTurnover = parseFloat(this.totalCreditorTurnover) + parseFloat(fisaList[fisa].creditorTurnover);
                this.totalHip = parseFloat(this.totalHip) + parseFloat(fisaList[fisa].hip);

                this.totalInvoiceValue = parseFloat(this.totalInvoiceValue).toFixed(2);
                this.totalDebtorTurnover = parseFloat(this.totalDebtorTurnover).toFixed(2);
                this.totalCreditorTurnover = parseFloat(this.totalCreditorTurnover).toFixed(2);
                this.totalHip = parseFloat(this.totalHip).toFixed(2);
            }
        }
    }

    /**
     * @author Boubacar Sow
     * @description  check and fire event for the FisaListValue.
     *              [ENLCRO-867] Account Situation - Delta changes
     * @param fisaList
     */
    emptyFisaList(fisaList) {
        let isEmptyFisaList = false;
        if (!fisaList){
            isEmptyFisaList = true;
        }else{
            if (fisaList.length === 0) {
                isEmptyFisaList = true;
            }
        }
        this.dispatchEvent(new CustomEvent('checkfisalist', {
            detail: {
                'isEmptyFisaList': isEmptyFisaList
            }
        }));
    }

    showFisaDetails(event) {
        let fisaId = event.target.dataset.id;
        let foundFisa = this.findFisa(fisaId);
        if (!foundFisa) {
            return;
        }
        this.fisaDetailsList.push(foundFisa);
        this.detailListRendered = true;
    }

    closeDetailList() {
        this.fisaDetailsList = [];
        this.detailListRendered = false;
    }

    onSelectAll(event) {
        this.fisaList.map(function (fisa) {
            fisa.selected = event.target.checked;
            return fisa
        });
    }

    onSelect(event) {
        let foundFisa = this.findFisa(event.target.dataset.id);
        if (!foundFisa) {
            return;
        }
        foundFisa.selected = event.target.checked;
    }

    findFisa(fisaId) {
        if (!fisaId) {
            return null;
        }
        return this.fisaList.find(function (fisa) {
            return fisa.fisaId === fisaId;
        });
    }

    onConfirmSelection() {
        if (!this.fisaList || !this.fisaList.length) {
            return;
        }
        let total = 0;
        let mapFisaData=[] ;
          let markedFisaData;

        let selectedFisa = this.fisaList.reduce(function (fisaIds, fisa) {
            if (fisa.selected === true) {
                total = total + parseFloat(fisa.hip);
                fisaIds.push(fisa.fisaId);
               markedFisaData = {
                    'fisaId': fisa.fisaId,
                    'fileNetId': fisa.fileNetId,
                    'paymentCode': fisa.paymentCode
                };

               mapFisaData.push(markedFisaData);
            }
            return fisaIds;
        }, []);

        if (!selectedFisa.length) {
            error(this, 'Fisa didn\'t select.');
        } else {
            success(this, 'Selected successfully');
            this.dispatchEvent(new CustomEvent('fisaselect', {
                detail: {
                    'fisaIds': selectedFisa,
                    'totalAmount': total,
                    'fisaData': mapFisaData
                }
            }));
        }
    }
}