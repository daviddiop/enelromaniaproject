/**
 * Created by Vlad Mocanu on 04/02/2020.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {error, success} from 'c/notificationSvc';
import {NavigationMixin} from 'lightning/navigation';
import {labels} from 'c/mroLabels';

export default class MroWarrantiesInquiry extends NavigationMixin(LightningElement) {

    @api contractNumber;
    @api contractId;
    @api id_integr_ctr;
    @api nr_ext_pa;
    @api accountId;
    @api contractAccountId;
    @api recordId;
    @api hideConfirmSelection = false;
    @api displayOnContract = false;
    @api hideSelectionColumn = false;
    @api customTitle = 'Warranties Inquiry for contract number';

    @track warrantyList;
    @track selectAll;
    @track left;
    @track top;
    @track contractCustomFields = ["AccountId", "Status", "StartDate", "EndDate", "CompanyDivision__c"];
    @track disableConfirmSelection = true;
    labels = labels;

    connectedCallback() {
        if (this.displayOnContract) {
            this.getContractNumber();
            this.contractId = this.recordId;
        }
    }

    getContractNumber() {
        notCacheableCall({
            className: "MRO_LC_WarrantiesInquiry",
            methodName: "getContractNumber",
            input: {
                'contractId': this.recordId
            }
        }).then(response => {
            if (!response) {
                error(this, 'Unexpected Error');
            }
            if (response.isError) {
                error(this, response.error);
            }
            this.contractNumber = response.data.contractNumber;
        })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    handleRetrieveWarranties() {
        notCacheableCall({
            className: "MRO_LC_WarrantiesInquiry",
            methodName: "listWarranties",
            input: {
                'contractNumber': this.contractNumber,
                'contractId': this.contractId,
                'ID_INTEGR_CTR': this.id_integr_ctr,
                'NR_EXT_PA': this.nr_ext_pa,
                'recordId': this.recordId
            }
        }).then(response => {
            if (!response) {
                error(this, 'Unexpected Error');
            }
            if (response.isError) {
                error(this, response.error);
            }
            if (!response.data || response.data.length === 0) {
                error(this, 'There are no warranty records found in SAP for the selected Contract Account, please pick another Contract Account or cancel the request')
            } else {
                let warrantyList = JSON.parse(JSON.stringify(response.data));
                this.warrantyList = warrantyList.map(v => {
                    let date = new Date(v.startDate);
                    v.startDate = [('0' + date.getDate()).slice(-2), ('0' + (date.getMonth() + 1)).slice(-2), date.getFullYear()].join('-');
                    return v;
                });
                let confirmSelectionBtn = this.template.querySelector('[data-id=confirmSelection]');
                if (confirmSelectionBtn) {
                    confirmSelectionBtn.disabled = false;
                }
            }
        });
    }

    onSelectAll(event) {
        this.warrantyList.map(function (warranty) {
            warranty.selected = event.target.checked;
            return warranty;
        });
    }

    onSelect(event) {
        let foundWarranty = this.findWarranty(event.target.dataset.id);
        if (!foundWarranty) {
            return;
        }
        foundWarranty.selected = event.target.checked;
    }

    findWarranty(warrantyId) {
        if (!warrantyId) {
            return null;
        }
        return this.warrantyList.find(function (warranty) {
            return warranty.warrantyId === warrantyId;
        });
    }

    onConfirmSelection() {
        if (!this.warrantyList) {
            return;
        }
        let selectedWarrantyIds = [];
        let selectedWarranties = Object.values(this.warrantyList.reduce(function (warrantyIds, warranty) {
            if (warranty.selected === true) {
                warrantyIds.push({id: warranty.warrantyId, amount: warranty.amount});
                selectedWarrantyIds.push(warranty.warrantyId);
            }
            return warrantyIds;
        }, []));

        if (Object.keys(selectedWarranties).length === 0) {
            error(this, 'Please select at least one warranty record and reconfirm your selection');
        } else {
            this.dispatchEvent(new CustomEvent('warrantiesselect', {
                detail: {
                    'selectedWarranties': selectedWarranties
                }
            }));
            success(this, 'Warranties Confirmed: ' + selectedWarrantyIds)
        }
    }

    onClickContractNumber() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.contractId,
                objectApiName: 'Contract', // objectApiName is optional
                actionName: 'view'
            }
        });
    }

    get assignClass() {
        return this.active ? '' : 'slds-hint-parent';
    }

    showPop(event) {
        this.template.querySelector('c-mro-compact-layout-popover').showPopover(event);
    }

    closePop() {
        this.template.querySelector('c-mro-compact-layout-popover').closePopover();
    }
}