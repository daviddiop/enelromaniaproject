import { LightningElement, api } from 'lwc';
import { labels } from  'c/labels';

export default class FileMetadataList extends LightningElement {
    @api fileMetadataList;
    labels = labels;
}