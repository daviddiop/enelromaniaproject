import {LightningElement, api, track} from "lwc";
import {NavigationMixin} from "lightning/navigation";
import {error, success, warn} from "c/notificationSvc";
import fieldStyle from "@salesforce/resourceUrl/EnergyAppResources";
import {loadStyle} from "lightning/platformResourceLoader";
import {labels} from "c/labels";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import cacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.cacheableCall";

export default class MroServicePointAddressEdit extends NavigationMixin(LightningElement) {
    @api caseId;
    @api dossierId;

    @api
    get servicePointId() {
        return this._servicePointId;
    }
    set servicePointId(value) {
        this._servicePointId = value;
    }

    @api
    get servicePointCode() {
        return this._servicePointCode;
    }
    set servicePointCode(value) {
        this._servicePointCode = value;
    }

    @api supplyId;
    @api accountId;
    @api companyId;
    @api showHeader;
    @api closeModal = false;
    @api selectCaseFields = [];
    @api selectCaseFieldsValues;
    @api readOnlyAddress = false;
    @api addressForced = false;
    @api recordTypeCase;
    @api originSelected;
    @api channelSelected;
    @api title;
    @api readOnlyFields = [];
    @api billingProfileId;
    @api contractAccountId = null;     //alessio.murru@webresults.it -- 25/11/2020 -- ENLCRO-1827
    @api distributor;
    @api isServicePointWizard = false;

    @track address;
    @track defaultTitle;
    @track isNormalize;
    @track caseFields = [];
    @track showLoadingSpinner = true;
    @track recordTypePicklist = [];
    @track selectedOption;
    @track _servicePointId;
    @track _servicePointCode;
    @track readeOnlyFieldAddress;

    defaultCaseOrigin;
    defaultCaseStatus;
    labels = labels;

    connectedCallback() {
        Promise.all([
            loadStyle(this, fieldStyle + "/style.css")
        ])
            .then(() => {
            })
            .catch(err => {
                error(this, err.body.message);
            });
        if (this.selectCaseFieldsValues && Object.keys(this.selectCaseFieldsValues).length) {
            this.caseFields = Object.keys(this.selectCaseFieldsValues);
        } else if (this.selectCaseFields.length) {
            this.caseFields = this.selectCaseFields;
        }
        this.getConstants();
        this.spinner = true;
        this.isNormalize = true;
        if (this.caseId) {
            this.initCaseAddress();
        } else if (this.billingProfileId) {
            this.initBillingProfile();
        }
        if (this.servicePointId) {
            this.initServicePoint();
        }
        this.defaultTitle = this.title ? this.title : 'Service Point Address Change - ' + this.servicePointCode;
    }

    initBillingProfile() {
        this.spinner = true;
        notCacheableCall({
            className: "MRO_LC_OpportunityServiceItem",
            methodName: "getBillingProfileAddress",
            input: {billingProfileId: this.billingProfileId}
        }).then((response) => {
            let result = Object.assign({}, response.data);
            this.setCaseAddress(result.address);
            this.spinner = false;
        })
        .catch((errorMsg) => {
            this.spinner = false;
            error(this, errorMsg);

        });
    }

    initServicePoint() {
        this.spinner = true;
        let inputs = {servicePointId: this.servicePointId};
        console.log("this.servicePointId" + this.servicePointId);
        notCacheableCall({
            className: "MRO_LC_OpportunityServiceItem",
            methodName: "getPointCodeFields",
            input: inputs
        })
            .then((response) => {
                let result = Object.assign({}, response.data);
                if (response.isError) {
                    return error(this, JSON.stringify(response.error));
                }
                this.setCaseAddress(result.servicePointAddress);
                this.spinner = false;
            })
            .catch((errorMsg) => {
                this.spinner = false;
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));

            });
    }

    initCaseAddress() {
        let inputs = {caseId: this.caseId};
        notCacheableCall({
            className: "MRO_LC_Case",
            methodName: "getCaseAddressField",
            input: inputs
        })
            .then((response) => {
                let result = Object.assign({}, response.data);
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                this.setCaseAddress(result.caseAddress);
                this.spinner = false;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
    }

    setCaseAddress(addr) {
        this.address = addr;
        console.log("this.Address: " + JSON.stringify(this.address));
        if (addr.addressNormalized) {
            this.readOnlyAddress = true;
            this.addressForced = false;
        } else {
            this.readOnlyAddress = false;
        }
    }

    getConstants() {
        cacheableCall({
            className: "Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.defaultCaseOrigin = apexResponse.data.CASE_DEFAULT_CASE_ORIGIN;
                        this.defaultCaseStatus = apexResponse.data.CASE_STATUS_DRAFT;
                    }
                }
            });
    }

    handleLoad() {
        if (this.selectCaseFieldsValues) {
            this.template.querySelectorAll("lightning-input-field").forEach(element => {
                element.value = this.selectCaseFieldsValues[element.fieldName];
            });
        }
        this.showLoadingSpinner = false;
    }

    handleSubmit(event) {
        this.showLoadingSpinner = true;
        event.preventDefault();
        event.stopPropagation();

        let fields = {};
        this.template.querySelectorAll("lightning-input-field").forEach(element => {
            fields[element.fieldName] = element.value;
        });

        console.log("fields: " + JSON.stringify(fields));
        console.log("this.accountId: " + this.accountId);
        if (this.accountId) {
            fields.AccountId = this.accountId;
        }

        let mroAddrForm = this.template.querySelector("c-mro-address-form");
        if (!mroAddrForm.isValid()) {
            error(this, this.labels.addressMustBeVerified);
            this.showLoadingSpinner = false;
            return;
        }

        let addrFields = mroAddrForm.getValues();
        let address = this.address;
        let notes = 'Fields \t\t\tOld values \t\t New values\n\n';
        console.log('** address Fields ' + addrFields);
        for (let f in addrFields) {
            if (addrFields.hasOwnProperty(f)) {
                fields[f] = addrFields[f];
                //Check changed fields
                Object.keys(address).forEach(fieldName => {
                    if (address.hasOwnProperty(fieldName)) {
                        if (f.toLocaleLowerCase().includes(fieldName.toLocaleLowerCase()) && address[fieldName] !== addrFields[f] && fieldName !== 'addressKey' && !notes.includes(fieldName)) {
                            let changedValues = fieldName + '\t\t\t' + address[fieldName] + ' \t\t ' + addrFields[f];
                            notes = notes.concat(changedValues, '\n');
                        }
                    }
                });
            }
        }
        if (notes) {
            fields["DisCoNotes__c"] = notes;
        }
        console.log("fields Address " + JSON.stringify(fields));
        fields.AddressNormalized__c = this.addressForced === false;
        if (this.dossierId) {
            fields.Dossier__c = this.dossierId;
        }
        if (this.supplyId) {
            fields.Supply__c = this.supplyId;
        }
        if (this.companyId) {
            fields.CompanyDivision__c = this.companyId;
        }
        if (this.recordTypeCase) {
            fields.RecordTypeId = this.recordTypeCase;
        }
        if (this.channelSelected) {
            fields.Channel__c = this.channelSelected;
        }
        if (this.originSelected) {
            fields.Origin = this.originSelected;
        }
        if (this.billingProfileId) {
            fields.BillingProfile__c = this.billingProfileId;
        }
        //START alessio.murru@webresults.it -- 25/11/2020 -- ENLCRO-1827
        if (this.contractAccountId){
            fields.ContractAccount__c = this.contractAccountId;
        }
        //END alessio.murru@webresults.it -- 25/11/2020 -- ENLCRO-1827
        if(this.isServicePointWizard){
            if(this.distributor){
                fields.Distributor__c = this.distributor;
            } else{
                warn(this, this.labels.missingDistributorOrServicePoint);
            }
            fields.ServicePointCode__c = this._servicePointCode;
        }
        fields.Status = this.defaultCaseStatus;
        fields.AddressAddressNormalized__c = this.addressForced === false;

        console.log("fields: " + JSON.stringify(fields));

        if (this.validateFields()) {
            this.showLoadingSpinner = true;
            this.template.querySelector("lightning-record-edit-form").submit(fields);
        } else {
            error(this, this.labels.requiredFields);
        }
    }

    handleSuccess(event) {
        this.showLoadingSpinner = false;
        event.preventDefault();
        event.stopPropagation();
        success(this, "Case successfully updated");
        const successEvent = new CustomEvent("casesuccess",{detail: {newCaseId: event.detail.id}});
        this.dispatchEvent(successEvent);
        this.addClosingEvent();

    }

    handleError(event) {
        this.showLoadingSpinner = false;
        // error(this, event.detail.message);
    }

    handleCancel() {
        this.addClosingEvent();
    }

    removeError(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName) {
            inputCmp.classList.remove("slds-has-error");
        }
    }

    addClosingEvent() {
        const closeEvent = new CustomEvent("close");
        this.dispatchEvent(closeEvent);
    }

    validateFields() {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll("lightning-input-field.wr-required"));
        let requireFieldsCombobox = Array.from(this.template.querySelectorAll("lightning-combobox.wr-required"));
        requireFields = requireFields.concat(requireFieldsCombobox);
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (!valueInput || (isNaN(valueInput) && valueInput.trim() === "")) {
                inputRequiredCmp.classList.add("slds-has-error");
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        return areValid;
    }

    disabledSaveButton(event) {
        this.isNormalize = event.detail.isnormalize;
    }

    disabledSaveButtonCaseAddress(event) {
        this.isNormalize = event.detail.isnormalize;
        this.addressForced = event.detail.addressForced;
    }

    handleSelectOption(event) {
        this.selectedOption = event.target.value;
    }

    get prefixAddress() {
        return 'Address';
    }

    get spinnerClass() {
        if (this.showLoadingSpinner) {
            return 'slds-modal slds-fade-in-open slds-backdrop ';
        }
        return 'slds-modal slds-fade-in-open slds-backdrop slds-hide';
    }

    get footerClass() {
        if (this.showLoadingSpinner) {
            return 'slds-card__footer slds-align-right slds-grid slds-grid_align-end mro-pointer-event_none';
        }
        return 'slds-card__footer slds-align-right slds-grid slds-grid_align-end';
    }
}