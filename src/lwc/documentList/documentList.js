import { LightningElement, api } from 'lwc';

export default class DocumentList extends LightningElement {

    @api documentList
}