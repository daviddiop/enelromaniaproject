/**
 * Created by Giuseppe Mario Pastore on 02/03/2020.
 */

import {api, LightningElement, track} from 'lwc';
import SelectRequestType from '@salesforce/label/c.SelectRequestType';
import NotesOrOtherInformation from '@salesforce/label/c.NotesOrOtherInformation';
import TheNumberMustBeBetween1And12 from '@salesforce/label/c.TheNumberMustBeBetween1And12';
import {labels} from 'c/mroLabels';
import { error, success } from "c/notificationSvc";


export default class MroDecoRecoEdit extends LightningElement {

    @api isDecoRecoDisabled;
    @api selectedFields;
    @api disabled = false;


    @api reqTypeValueCmp;
    @track isDurationRequired = true;
    @api durationValueCmp;
    @api startDateValueCmp;
    @api noteValueCmp;
    @track requestTypeValue;
    @track isDurationDisabled = false;

    @track reqTypeValue;
    @track durationValue;
    @track startDateValue;
    @track noteValue;



    labels = {
        SelectRequestType,
        NotesOrOtherInformation,
        TheNumberMustBeBetween1And12
    };

    //reqTypeValue = this.template.querySelector('[data-id="selectRequestTypeId"]').value != null ? this.template.querySelector('[data-id="selectRequestTypeId"]').value : '';
    //durationValue = this.template.querySelector('[data-id="durationInputId"]').value != null ? this.template.querySelector('[data-id="durationInputId"]').value : '';
    //startDateValue = this.template.querySelector('[data-id="startDateId"]').value != null ? this.template.querySelector('[data-id="startDateId"]').value : '';
    //noteValue = this.template.querySelector('[data-id="noteValueId"]').value != null ? this.template.querySelector('[data-id="noteValueId"]').value : '';


   connectedCallback() {
        this.requestTypeValue = this.reqTypeValueCmp;
        this.durationValue = this.durationValueCmp;
        this.startDateValue = this.startDateValueCmp;
        this.noteValue = this.noteValueCmp;
    }

    renderedCallback() {
        this.requestTypeValue = this.reqTypeValueCmp;
        this.durationValue = this.durationValueCmp;
        this.startDateValue = this.startDateValueCmp;
        this.noteValue = this.noteValueCmp;
    }

    get optionsRequestType(){
    return [{label:'Disconnection', value:'DECO_C'},
            {label:'Reconnection', value:'RECO_C'}];
}
    requestTypeChange(){
        this.reqTypeValue = this.template.querySelector('[data-id="selectRequestTypeId"]').value;
        if(this.reqTypeValue === 'RECO_C'){
            this.isDurationDisabled = true;
            this.isDurationRequired = false;
            if(this.durationValue != null && this.durationValue !== '') {
                this.durationValue = this.template.querySelector('[data-id="durationInputId"]').value = '';
            }
        } else{
            this.isDurationDisabled = false;
            this.isDurationRequired = true;
        }


        this.checkFields();
    }


    durationChange(){
        this.durationValue = this.template.querySelector('[data-id="durationInputId"]').value;
        this.checkFields();
    }

    startDateChange(){
        this.startDateValue = this.template.querySelector('[data-id="startDateId"]').value;
        this.checkFields();
    }

    noteChange(){
        this.noteValue = this.template.querySelector('[data-id="noteValueId"]').value;
        this.checkFields();
    }

    validateStartDate(startDate) {
        if (startDate) {
            let currentDate = Date.now();
            let parsedStartDate = Date.parse(startDate);
            let startDateDay = new Date(startDate).getDay();

            if (parsedStartDate <= currentDate) {
                error(this, labels.requestStartDateInFuture);
                return false
            } else if ([0,6].includes(startDateDay)) {
                error(this, labels.notRegularWorkingDay);
                return false
            } else {
                return true
            }
        } else {
            return false
        }
    }

    checkFields(){
        if(this.reqTypeValue && this.reqTypeValue === 'RECO_C') {
            if(this.reqTypeValue && this.startDateValue) {
                this.isDecoRecoDisabled = !(this.reqTypeValue && this.validateStartDate(this.startDateValue));
            }
            else {
                this.isDecoRecoDisabled = true;
            }
        } else if(this.reqTypeValue && this.reqTypeValue === 'DECO_C'){
            this.isDecoRecoDisabled = !(this.reqTypeValue &&
                (this.durationValue && (this.durationValue > 0 && this.durationValue <= 12)) &&
                this.validateStartDate(this.startDateValue));
        } else {
            this.isDecoRecoDisabled = true;
            this.reqTypeValue = '';
        }

        const validityFieldEvent = new CustomEvent("validityfieldcheckevent", {
            detail:{
                "decoRecoDisabledValue": this.isDecoRecoDisabled,
                "requestTypeValue" : this.reqTypeValue,
                "durationValue" : this.durationValue,
                "startDateValue" : this.startDateValue,
                "noteValue" : this.noteValue
            }
        });
        this.dispatchEvent(validityFieldEvent);
    }
}