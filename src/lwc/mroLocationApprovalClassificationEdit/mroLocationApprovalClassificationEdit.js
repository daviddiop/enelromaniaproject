/**
 * Created by David on 03.02.2020.
 */
import {LightningElement, api, track} from 'lwc';
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';

export default class MroLocationApprovalClassificationEdit extends LightningElement {
    labels = labels;
    @track requiredSubType = false ;
    @track requiredExternal = false ;
    @track requiredDistance = false ;
    @track isRequiredForConnection = false ;
    @track showLoadingSpinner = true ;
    @track hasRendered = false;
    @track disabledInput = true;
    @track minDate;
    @api disabledForm ;
    @api recordTypeId ;
    @api caseId;
    @api isNewService = false;
    @api isLocationApprovalWizard = false;
    @api distributor = false;



    connectedCallback() {
        this.disabledInput = this.disabledForm;
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        today =  yyyy + '-' + mm  + '-' + dd;
        this.minDate = today;
        this.showLoadingSpinner = false;
    }

    /*renderedCallback() {
        if(this.disabledForm && !this.hasRendered){
            this.hasRendered = true;
            this.disabledInput = this.disabledForm;
        }

    }*/
    @api
    disableInputField(disabledFields){
        this.disabledForm = disabledFields;
        this.disabledInput = disabledFields;
    }

    @api
    changeCaseId(caseRecordId){
        this.caseId = caseRecordId;
    }

    handleLoad() {
        this.showLoadingSpinner = false;
        this.disabledInput = this.disabledForm;
    }

    handleType(event){
        this.showLoadingSpinner = false;
        let inputCmp = event.target.value;
         if(inputCmp){
            this.handleremove(event);
        }
        this.requiredSubType = inputCmp === labels.NewPermitRequest || inputCmp === labels.Correction;
        this.requiredExternal = inputCmp === labels.PlacementPermitExtension || inputCmp === labels.Correction;
        this.requiredDistance = false;
    }
    handleConnection(event){
        let inputCmp = event.target.value;
        this.isRequiredForConnection = inputCmp === true;
    }
    handleSubType(event){
        let inputCmp = event.target.value;
        if(inputCmp){
            this.handleremove(event);
        }
        this.requiredDistance = inputCmp === labels.MunicipalNetwork || inputCmp === labels.TransEuropeanTransportInfrastructure || inputCmp === labels.OpticalFiber;

    }
    handleremove(event){
        let inputCmp = event.target;
        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
    }

    @api
    handleConfirm() {
        this.showLoadingSpinner = true;
        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });
        let validate = this.validateField(fields);
        if (validate) {
            let checkDate = this.checkDate();
            if (checkDate) {
                const selectedEvent = new CustomEvent('confirm', {
                    detail: {
                        fields: fields
                    }
                });
                this.dispatchEvent(selectedEvent);
            }else{
                const selectedEvent = new CustomEvent('confirm', { });
                this.dispatchEvent(selectedEvent);
            }
        }
        this.showLoadingSpinner = false;
    }
    validateField(fields) {
        let validate = true;
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.required && (fields[element.fieldName] === "" || fields[element.fieldName] == null)) {
                element.classList.add('slds-has-error');
                validate = false
            }
        });
        return validate;
    }

    @api
    getValidFields(){
        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });
        return this.validateField(fields);
    }
    checkDate(){
        let refDate = this.template.querySelector('[data-id="refDate"]').value;
        let validDate = true;
        if(refDate < this.minDate){
            this.template.querySelector('[data-id="refDate"]').classList.add('slds-has-error');
            error(this, this.labels.RequestDateContraint);
            validDate = false;
        }
        return validDate;
    }

    disableDistributor(){
        return this.isLocationApprovalWizard || this.disabledInput;
    }
}