import { LightningElement, api, track, wire } from 'lwc';
import { error, success } from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import { labels } from  'c/labels';
import { createRecord, getRecordUi } from 'lightning/uiRecordApi';
import FILEMETADATA_OBJECT from '@salesforce/schema/FileMetadata__c';

export default class MroDocumentUpload extends LightningElement {
    @api parentRecordId;
    @api disableCheckDocument = false;

    @track parentRecord;
    @track fileMetadataId;
    @track fileMetadataList = [];

    labels = labels;
    loadDateTime;

    @wire(getRecordUi, { recordIds: '$parentRecordId', layoutTypes: 'Full', modes: 'View' })
    parentRecordUi({errorMsg, data}) {
        if (errorMsg) {
            console.error(errorMsg);
        } else if (data) {
            //console.log(JSON.parse(JSON.stringify(data)));
            this.parentRecord = data.records[this.parentRecordId];
        }
    }

    connectedCallback() {
        //this.getTempFileMetadata();
    }

    // getTempFileMetadata() {
    //     notCacheableCall({
    //         className: 'DocumentUploadCnt',
    //         methodName: 'getTempFileMetadata',
    //         input: { dossierId: this.dossierId }
    //     }).then((response) => {
    //         if (!response.isError) {
    //             this.fileMetadataId = response.data.fileMetadata.Id;
    //             this.loadDateTime = response.data.loadDateTime;
    //         } else {
    //             error(this, response.error);
    //         }
    //     }).catch((errorMsg) => {
    //         error(this, errorMsg);
    //     });
    // }

    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        const fields = {};
        if (this.parentRecord.apiName === 'Dossier__c') {
            fields.Dossier__c = this.parentRecordId;
        } else if (this.parentRecord.apiName === 'Case') {
            fields.Case__c = this.parentRecordId;
            fields.Dossier__c = this.parentRecord.fields.Dossier__c.value
        }
        fields.DocumentId__c = uploadedFiles[0].documentId;
        fields.Title__c = uploadedFiles[0].name
        const recordInput = { apiName: FILEMETADATA_OBJECT.objectApiName, fields };
        createRecord(recordInput)
            .then(fileMetadata => {
                this.fileMetadataList.push(fileMetadata.id);
                success(this, this.labels.documentUploaded);
            })
            .catch(exception => {
                error(this, exception.body.message);
            });

        // if (uploadedFiles && uploadedFiles.length) {
        //     notCacheableCall({
        //         className: 'DocumentUploadCnt',
        //         methodName: 'createFileMetadata',
        //         input: { fileMetadataId: this.fileMetadataId, documentId: uploadedFiles[0].documentId, dossierId: this.dossierId, loadDateTime: this.loadDateTime}
        //     }).then((response) => {
        //         if (response.isError) {
        //             error(this, response.error);
        //         } else {
        //             this.fileMetadataList = response.data.fileMetadataList;
        //             success(this, this.labels.documentUploaded);
        //         }
        //     })
        //     .catch((errorMsg) => {
        //         error(this, errorMsg);
        //     });
        // }
    }
}