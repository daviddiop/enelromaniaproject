import { LightningElement, api, track } from 'lwc';
import EAppStyle from '@salesforce/resourceUrl/EnergyAppResources';
import { loadStyle } from 'lightning/platformResourceLoader';
import { success,error } from 'c/notificationSvc';
import { labels } from 'c/labels';

export default class ValidatableDocumentCheck extends LightningElement {
    @api documentType;
    @api fileMetadataId = '';
    @api sectionOpened = false;
    @track _isValidated = false;
    @track isOpen = false;
    @track idValidatableDoc;
    labels = labels;


    renderedCallback() {
        Promise.all([
            loadStyle(this, EAppStyle + '/style.css'),
        ])
            .then(() => {
            })
            .catch(errno => {
                error(this, errno.body.message);
            });

        if (this.isValidated) {
            this.activeSectionCheckboxes().forEach((checkbox) => {
                checkbox.checked = true;
                checkbox.classList.remove('slds-has-error');
            });
        }
    }

    get validatableDocId() {
        return this.documentType.validatableDocument;
    }

    get isValidated() {
        return this.documentType.isValidated || this._isValidated;
    }

    /**
     * 
     * Handles click on the Check button for checking document type
     * 
     * */
    handleCheckClick() {
        let section = this.template.querySelector('.section-document');
        if (!this.documentType.documentItems) {
            this.handleSubmit();
            return;
        }
        if (section) {
            section.classList.remove('slds-hide');
            section.scrollIntoView({ behavior: "smooth"});
            this.isOpen = this.sectionOpened = !this.isOpen;
            this.dispatchEvent(new CustomEvent("changesection"));
        }

    }

    handleCancelClick() {
        let section = this.template.querySelector('.section-document');
        section.classList.add('slds-hide');
        this.isOpen = this.sectionOpened = false;
        this.hideErrPanel();
        this.dispatchEvent(new CustomEvent("changesection"));
    }

    handleValidateClick() {
        let showErrPanel = !this.validateFields();
        this.activeSectionCheckboxes().forEach((checkbox) => {
            if (!checkbox.checked) {
                checkbox.classList.add('slds-has-error');
                showErrPanel = true;
            }
        });

        if (showErrPanel) {
            this.template.querySelector('.error-panel').classList.replace('slds-hide', 'slds-show');
        } else {
            if (this.documentType.documentBundleItem) {
                this.documentBundleItem = this.documentType.documentBundleItem;
                this.handleSubmit();
                this.handleCancelClick();
            }
        }
    }

    getElement(self, selector) {
        return new Promise(function (resolve) {
            const element = self.querySelector(selector);
            if (element) {
                resolve(element);
            }
        });
    }

    handleSubmit() {
        const selfTemplate = this.template;
        const self = this;
        this.getElement(selfTemplate, '[data-id="FileMetadata"]').then(function (inputElement) {
            inputElement.value = self.fileMetadataId;
            self.template.querySelector('lightning-record-edit-form').submit();
        }, self);
    }

    handleSuccess(){
        this._isValidated = true;
        success(this, labels.documentValidated);
    }
    handleError(event) {
         error(this, event.detail.message);
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

    removeError(event) {
        event.target.classList.remove('slds-has-error');
        this.hideErrPanel();
    }
    
    hideErrPanel = () =>{
        let errPanel = this.template.querySelector('.error-panel');
        if (errPanel.classList.contains('slds-show')) {
            errPanel.classList.replace('slds-show', 'slds-hide');
        }
    };

    activeSectionCheckboxes = () => {
        return Array.from(this.template.querySelectorAll('[data-id="allCheckboxes"]'));
    }

}