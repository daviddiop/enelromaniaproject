/**
 * Created by Octavian on 6/16/2020.
 */

import {api, track, LightningElement} from 'lwc';

export default class MroBillingAdjustmentInformation extends LightningElement {

    @api showIndexValidity = false;

    @track checkBillReturnCodes = [];
    @track billingAdjustmentType;
    @track isDiscoEnel;
    @track isIndexValid = false;

    @api setBillingAdjustmentType(billingAdjustmentType) {
        this.billingAdjustmentType = billingAdjustmentType;
        console.log('setBillingAdjustmentType: ' + billingAdjustmentType);
    }

    @api setCheckBillReturnCodes(checkBillReturnCodes) {
        this.checkBillReturnCodes = checkBillReturnCodes;
        console.log('setReturnCodes: ' + checkBillReturnCodes);
    }

    @api setIsDiscoEnel(isDiscoEnel) {
        this.isDiscoEnel = isDiscoEnel;
        console.log('setIsDiscoEnel: ' + isDiscoEnel);
    }

    @api setIsIndexValid(isIndexValid) {
        this.isIndexValid = isIndexValid;
    }

    get indexValidityText() {
        let returnText = '';
        if(this.isIndexValid){
            returnText = 'Index has been validated';
        } else {
            returnText = 'Index has NOT been validated';
        }
        return returnText;
    }

    get enelDistributorText() {
        let returnText = '';
        if(this.isDiscoEnel == true){
            returnText = 'ENEL Distributie';
        } else {
            returnText = 'Distributor NON ENEL';
        }
        return returnText;
    }
}