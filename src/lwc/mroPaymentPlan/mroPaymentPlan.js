/**
 * Created by BADJI on 17/03/2020.
 * Updated by Goudiaby on 23/03/2020
 */

import {api, track, wire, LightningElement} from 'lwc';
import {getRecord} from 'lightning/uiRecordApi';

export default class MroPaymentPlan extends LightningElement {
    @api recordId;

    @api
    get amount() {
        return this._amount;
    }

    set amount(value) {
        this._amount = value;
    }

    @api months = 5;
    @track columns = [
        {label: 'Month', fieldName: 'Month', type: 'text'}
    ];
    @track valuesList =
        [{label: 'Month', value: 'Amount'}];
    @track dataObj = [];
    @track _amount;
    @track error;
    @track showPlan = false;

    @wire(getRecord, {recordId: '$recordId', fields: ['Case.Amount__c']})
    wiredRecord({error, data}) {
        if (data) {
            this._amount = data.fields.Amount__c.value;
        } else if (error) {
            this.error = error;
        }
    }

    connectedCallback() {
    }

    handleRetrievePlan(){
        let values = this._amount / this.months;
        console.log(' value '+values);
        for (let i = 1; i <= this.months; i++) {
            this.columns = [...this.columns, {
                label: '' + i,
                fieldName: '' + i,
                type: 'text'
            }];
            this.valuesList = [...this.valuesList, {
                label: '' + i,
                value: '' + values
            }];
        }
        let results = this.valuesList.reduce(function (map, obj) {
            map[obj.label] = obj.value;
            return map;
        }, {});
        this.dataObj.push(results);
        this.showPlan = true;
    }
}