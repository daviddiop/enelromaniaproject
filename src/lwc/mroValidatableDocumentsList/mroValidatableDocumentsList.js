import {LightningElement, api, track, wire} from 'lwc';
import {error, warn, success} from 'c/notificationSvc';
//import {labels} from 'c/labels';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { labels, formatLabel } from "c/mroLabels";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import DISCOFILEID_FIELD from '@salesforce/schema/FileMetadata__c.DiscoFileId__c';
import { refreshApex } from '@salesforce/apex';

export default class MroValidatableDocumentsList extends NavigationMixin(LightningElement)  {
    @api recordId;
    @track documentTypes;
    @track filemetadata;
    @track oneSectionOpened;
    @track showSpinner = false;
    @track disableDoneButton = false;
    @track disableCheckButtons = false;
    multipleDisCoFilesFound = false;
    archivingPollerId;

    labels = labels;

    @wire(getRecord, {recordId: '$recordId', fields: [DISCOFILEID_FIELD]})
    fileMetadataRecord;

    get discoFileId() {
        return getFieldValue(this.fileMetadataRecord.data, DISCOFILEID_FIELD);
    }

    connectedCallback() {
        this.loadValidatableDocumentsData();
    }

    @api
    loadValidatableDocumentsData() {
        const recordIdParam = this.recordId;

        if (recordIdParam === null) {
            error(this, this.labels.validateMetadata);
            return;
        }

        this.showSpinner = true;

        let inputs = {filemetadataId: recordIdParam};
        notCacheableCall({
            className: "MRO_LC_ValidatableDocumentsCnt",
            methodName: "getFileMetadataAndValidatableDocuments",
            input: inputs
        })
            .then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                        this.disableDoneButton = true;
                        this.disableCheckButtons = true;
                        this.showSpinner = false;
                        return;
                    }
                    this.filemetadata = response.data.filemetadata;
                    this.documentTypes = JSON.parse(JSON.stringify(response.data.documentTypes));
                    //console.log('documentTypes', this.documentTypes);
                    let fileCheckActivity = response.data.fileCheckActivity;
                    //console.log('fileCheckActivity', fileCheckActivity);
                    if (!this.documentTypes || this.documentTypes.length === 0) {
                        error(this, formatLabel(labels.notFound, labels.validatableDocuments));
                        this.disableDoneButton = true;
                        this.disableCheckButtons = true;
                        this.showSpinner = false;
                        return;
                    }
                    if (!fileCheckActivity) {
                        error(this, formatLabel(labels.notFound, labels.fileCheckActivity));
                        this.disableDoneButton = true;
                        this.disableCheckButtons = true;
                        this.showSpinner = false;
                        return;
                    }
                    this.disableCheckButtons = false;
                    if (Array.isArray(this.documentTypes)) {
                        let archiveOnDisCoCount = 0;
                        this.documentTypes.forEach(documentType => {
                            if (documentType.documentItemsDTO) {
                                documentType.documentItems = JSON.parse(documentType.documentItemsDTO);
                            }
                            if (documentType.archiveOnDisCo && !documentType.isArchivedOnDisCo) {
                                archiveOnDisCoCount++;
                            }
                        });
                        if (archiveOnDisCoCount > 1) {
                            this.multipleDisCoFilesFound = true;
                            warn(this, formatLabel(labels.multipleArchiveOnDiscoFound, archiveOnDisCoCount), true);
                        }
                    }
                }
                this.showSpinner = false;
            })
            .catch(errorMsg => {
                error(this, errorMsg);
                this.disableDoneButton = true;
                this.disableCheckButtons = true;
                this.showSpinner = false;
            });
    }

    get filemetadataDTO() {
        return this.filemetadata;
    }

    handleChangeSection(event) {
        this.oneSectionOpened = event.target.sectionOpened;
        this.disableDoneButton = event.detail.isOpen;
        this.disableCheckButtons = this.multipleDisCoFilesFound && event.detail.archiveOnDisCo && event.detail.isValidated;
    }

    handleDoneClick() {
        const recordIdParam = this.recordId;

        if (recordIdParam === null) {
            error(this, this.labels.validateMetadata);
            return;
        }

        this.showSpinner = true;

        let inputs = {filemetadataId: recordIdParam};
        notCacheableCall({
            className: "MRO_LC_ValidatableDocumentsCnt",
            methodName: "doneValidatableDocuments",
            input: inputs
        })
            .then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                        this.showSpinner = false;
                        return;
                    }

                    if (!response.data.awaitingArchiving) {
                        success(this, this.labels.checkDocumentCompleted)
                        this.showSpinner = false;
                        this.navigateToParentRecord();
                    } else {
                        this.archivingPollerId = setInterval(this.archivingOnDMSCheck.bind(this),5000);
                    }
                }
            })
            .catch(errorMsg => {
                error(this, errorMsg);
            });
    }

    handleNotDoneClick() {
        this.navigateToParentRecord();
    }

    archivingOnDMSCheck() {
        refreshApex(this.fileMetadataRecord);
        console.log('DiscoFileId', this.discoFileId);
        if (this.discoFileId) {
            clearInterval(this.archivingPollerId);
            this.archivingPollerId = null;
            this.handleDoneClick();
        }
    }

    navigateToParentRecord() {
        this[NavigationMixin.Navigate]({
           type: "standard__recordPage",
           attributes: {
               "recordId": this.filemetadata.caseId ? this.filemetadata.caseId : this.filemetadata.dossierId,
               "objectApiName": this.filemetadata.caseId ? 'Case' : 'Dossier__c',
               "actionName": "view"
           }
       });
   }
}