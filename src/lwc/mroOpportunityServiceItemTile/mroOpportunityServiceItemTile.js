import { LightningElement, api, wire, track } from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
//import { getRecord } from 'lightning/uiRecordApi';
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import { labels } from 'c/mroLabels';
import { error } from 'c/notificationSvc';

export default class OpportunityServiceItemTile extends LightningElement {
    label = labels;
    @api recordId;
    @api disabled;
    @api accountId;
    @api isRow = false;

    @api disabledFields={};
    @api hiddenFields={};
    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
    @api hasTrader = false;
    @api isNotSelectedOldTrader = false;
    @api hasSubProcessNone = false;
    @api showEndDate = false;
    @api isNewConnectionFlagReadOnly = false;
    //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
    @api autoSave = false;
    @api readOnlyAddress = false;
    @api selectOsiTileFields = [];
    @api showOsiTileFields = false;
    @api tileTitle;
    @api commodity;
    @api isTemporaryConnection = false;
    @api servicePointId;
    @api serviceSiteId;
    @api servicePointCode;
    @api isNewRecordPointAddress=false;
    @api opportunityId;
    @api hasSameOpportunityServiceItems = false;
    @api hasApplicantNotHolder = false;
    @api hasPreCheck = false;
    @api requestType;
    @api isTransferOrProductChange = false;

    @track defaultTileTitle;
    @track isOpenDelete = false;
    @track isOpenEdit = false;
    @track eletricRecordTypeId;
    @track gasRecordTypeId;
    @track recordTypeIdCmp;
    @track viewOsiTileFields = [];
    @track isGas;
    @track isElectric;
    @track isService;
    @api isPlaceholder = false

   // @api typeOfWork = false;
    @api typeOfVas = false;
    @track tileCommodity;

    get isHardVAS() {
        return this.typeOfVas === labels.hardVas;
    }
    get isSoftVAS() {
        return this.typeOfVas === labels.softVas;
    }
    get isVAS(){
        return this.isSoftVAS || this.isHardVAS;
    }

    get showVoltage() {
        let showVoltage = this.isElectric && !this.isPlaceholder;
        return showVoltage;
    }

    get showPressure() {
        let showPressure = this.isGas && !this.isPlaceholder;
        return showPressure;
    }

    /*
    @wire(getRecord, { recordId: '$recordId', fields: ['OpportunityServiceItem__c.RecordType.DeveloperName'] })
    wiredRecord({ error, data }) {
        if (data) {
            this.isElectric = data.fields.RecordType.value.fields.DeveloperName.value === 'Electric';
            this.isGas = data.fields.RecordType.value.fields.DeveloperName.value === 'Gas';
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.record = undefined;
        }
    }
     */

    connectedCallback() {
        this.defaultTileTitle = this.tileTitle ?  this.tileTitle : this.label.osiEdit;

        if ((this.selectOsiTileFields.length > 0)) {
            this.viewOsiTileFields = this.selectOsiTileFields.filter(function (el) {
                return (!(el.includes('.')));
            });
        }
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getOsiRecordTypeDevName',
            input: {
                'recordId': this.recordId
            },
        }).then(apexResponse => {
            if (apexResponse) {
                if (apexResponse.isError) {
                    error(this, apexResponse.error);
                } else {
                    if (apexResponse.data.osiRecordTypeDevName) {
                        this.isElectric = apexResponse.data.osiRecordTypeDevName === 'Electric';
                        if(this.isElectric){
                            this.tileCommodity = 'Electric'
                        }
                        this.isGas = apexResponse.data.osiRecordTypeDevName === 'Gas';
                        if(this.isGas){
                            this.tileCommodity = 'Gas'
                        }
                        this.isService = apexResponse.data.osiRecordTypeDevName === 'Service';
                        if(this.isService){
                            this.tileCommodity = 'Service'
                        }
                        console.log('this.isElectric: ' + this.isElectric);
                        console.log('isGas: ' + this.isGas);
                        console.log('isService: ' + this.isService);
                    }
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }

    get svgURLEle() {
        return SVG_URL + '/images/Electric.svg#electric';
    }
    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }

    editOppServiceItem() {
        //start add by David
        let inputs = {osiId: this.recordId};
        notCacheableCall({
            className: 'MRO_LC_OpportunityServiceItem',
            methodName: 'getOsiAddressField',
            input: inputs
        })
            .then((apexResponse) => {
                console.log('result of init supply search*** '+JSON.stringify(apexResponse));
                if (apexResponse.isError) {
                    error(this, JSON.stringify(apexResponse.error));
                }
                let addr = {};
                addr = apexResponse.data.osiAddress;

                if(addr.addressNormalized){

                }
                this.isOpenEdit = true;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
        this.readOnlyAddress = true;
        // end
    }

    deleteOppServiceItem() {
        this.isOpenDelete = true;
    }

    closeModal() {
        this.isOpenDelete = false;
    }

    closeModalEdit(event) {
        this.isOpenEdit = false;
        if(event && event.detail){
            let osiId = event.detail.opportunityServiceItemId;
            console.log("close osi=="+osiId);
            if(osiId){
                this.dispatchEvent(new CustomEvent('update', {
                    detail: {
                        'osiId': this.recordId
                    }
                }, { bubbles: true }));
            }
        }

    }

    deletedItem() {

        this.spinner = true;
        deleteRecord(this.recordId)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: this.label.success,
                        variant: 'success'
                    })
                );
                this.dispatchEvent(new CustomEvent('delete', {
                    detail: {
                        'recordId': this.recordId
                    }
                }, { bubbles: true }));
                this.isOpenDelete = false;
                this.spinner = false;
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error deleting record',
                        message: this.label.error,
                        variant: 'error'
                    })
                );
                this.spinner = false;
            });
    }

    closeAfterSave() {
        this.isOpenEdit = false;
    }

    @api
    getRecordId(){
        return this.recordId;
    }
}