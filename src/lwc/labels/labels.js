import servicePoint from '@salesforce/label/c.ServicePoint';
import find from '@salesforce/label/c.Find';
import pointSelection from '@salesforce/label/c.PointSelection';
import success from '@salesforce/label/c.Success';
import warning from '@salesforce/label/c.Warning';
import error from '@salesforce/label/c.Error';
import noDataEntered from '@salesforce/label/c.NoDataEntered';
import requiredFields from '@salesforce/label/c.RequiredFields';
import New from '@salesforce/label/c.New';
import Status from '@salesforce/label/c.Status';
import roles from '@salesforce/label/c.Roles';
import relatedContactList from '@salesforce/label/c.RelatedContactList';
import relatedAccountList from '@salesforce/label/c.RelatedAccountList';
import notFound from '@salesforce/label/c.NotFound';
import customerSearchResults from '@salesforce/label/c.CustomerSearchResults';
import VATNumber from '@salesforce/label/c.VATNumber';
import nationalIdentityNumber from '@salesforce/label/c.NationalIdentityNumber';
import BsnType from '@salesforce/label/c.BusinessType';
import active from '@salesforce/label/c.Active';
import phone from '@salesforce/label/c.Phone';
import email from '@salesforce/label/c.Email';
import accountServices from '@salesforce/label/c.AccountServices';
import search from '@salesforce/label/c.Search';
import supplySelectionLabel from '@salesforce/label/c.SupplySelection';
import selectSupply from '@salesforce/label/c.SelectSupply';
import filter from '@salesforce/label/c.Filter';
import servicePointSearchLabel from '@salesforce/label/c.ServicePointSearch';
import invalidCode from '@salesforce/label/c.InvalidCode';
import code from '@salesforce/label/c.Code';
import individualEditForm from '@salesforce/label/c.IndividualEditForm';
import newSearch from '@salesforce/label/c.NewSearch';
import newContract from '@salesforce/label/c.NewContract';
import select from '@salesforce/label/c.Select';
import contractEndDate from '@salesforce/label/c.ContractEndDate';
import customerSignedDate from '@salesforce/label/c.ContractCustomerSignedDate';
import contractName from '@salesforce/label/c.ContractName';
import customerCode from '@salesforce/label/c.CustomerCode';
import caseEdit from '@salesforce/label/c.CaseEdit';
import save from '@salesforce/label/c.Save';
import cancel from '@salesforce/label/c.Cancel';
import cancellationRequest from '@salesforce/label/c.CancellationRequest';
import cancellationReason from '@salesforce/label/c.CancellationReason';
import no from '@salesforce/label/c.No';
import yes from '@salesforce/label/c.Yes';
import informationRequired from '@salesforce/label/c.InformationRequired';
import documentChecking from '@salesforce/label/c.DocumentChecking';
import fileMetadata from '@salesforce/label/c.FileMetadata';
import createdDate from '@salesforce/label/c.CreatedDate';
import companyDivision from '@salesforce/label/c.CompanyDivision';
import selectDivision from '@salesforce/label/c.SelectDivision';
import dossier from '@salesforce/label/c.Dossier';
import validatableDocuments from '@salesforce/label/c.ValidatableDocuments';
import edit from '@salesforce/label/c.Edit';
import enforced from '@salesforce/label/c.Enforced';
import clone from '@salesforce/label/c.Clone';
import billingProfile from '@salesforce/label/c.BillingProfile';
import billingInformation from '@salesforce/label/c.BillingInformation';
import billingProfileEdit from '@salesforce/label/c.BillingProfileEdit';
import paymentMethod from '@salesforce/label/c.PaymentMethod';
import billingAddress from '@salesforce/label/c.BillingAddress';
import addressValidated from '@salesforce/label/c.AddressValidated';
import confirmationQuestion from '@salesforce/label/c.ConfirmationQuestion';
import confirmation from '@salesforce/label/c.Confirmation';
import confirm from '@salesforce/label/c.Confirm';
import addressMustBeVerified from '@salesforce/label/c.AddressMustBeVerified';
import close from '@salesforce/label/c.Close';
import duplicatedRecord from '@salesforce/label/c.DuplicatedRecord';
import customer from '@salesforce/label/c.Customer';
import pleaseSelect from '@salesforce/label/c.PleaseSelect';
import contactDetails from '@salesforce/label/c.ContactDetails';
import fistnameOrLastnameRequired from '@salesforce/label/c.FistnameOrLastnameRequired';
import individualCreated from '@salesforce/label/c.IndividualCreated';
import individualNotCreated from '@salesforce/label/c.IndividualNotCreated';
import documentValidated from '@salesforce/label/c.DocumentValidated';
import account from '@salesforce/label/c.Account';
import addressForced from '@salesforce/label/c.AddressForced';
import selectFamilyProduct from '@salesforce/label/c.SelectFamilyProduct';
import selectAddress from '@salesforce/label/c.SelectAddress';
import invalidFields from '@salesforce/label/c.InvalidFields';
import validateAddress from '@salesforce/label/c.ValidateAddress';
import validated from '@salesforce/label/c.Validated';
import mandatory from '@salesforce/label/c.Mandatory';
import requiredCheckDocument from '@salesforce/label/c.RequiredCheckDocument';
import validate from '@salesforce/label/c.Validate';
import update from '@salesforce/label/c.Update';
import opportunityServiceItem from '@salesforce/label/c.OpportunityServiceItem';
import add from '@salesforce/label/c.Add';
import productImage from '@salesforce/label/c.ProductImage';
import noImagePresent from '@salesforce/label/c.NoImagePresent';
import productName from '@salesforce/label/c.ProductName';
import recordType from '@salesforce/label/c.RecordType';
import productDescription from '@salesforce/label/c.ProductDescription';
import product from '@salesforce/label/c.Product';
import cart from '@salesforce/label/c.Cart';
import noCart from '@salesforce/label/c.NoCart';
import checkout from '@salesforce/label/c.Checkout';
import check from '@salesforce/label/c.Check';
import details from '@salesforce/label/c.Details';
import noDocument from '@salesforce/label/c.NoDocument';
import shortEntry from '@salesforce/label/c.ShortEntry';
import selectAccountType from '@salesforce/label/c.SelectAccountType';
import allSupplies from '@salesforce/label/c.AllSupplies';
import chooseDocument from '@salesforce/label/c.ChooseDocument';
import documentUpload from '@salesforce/label/c.DocumentUpload';
import recentUploadedDocuments from '@salesforce/label/c.RecentUploadedDocuments';
import documentUploaded from '@salesforce/label/c.DocumentUploaded';
import checkDocument from '@salesforce/label/c.CheckDocument';
import copyFromOtherAddresses from '@salesforce/label/c.CopyFromOtherAddresses';
import copyFromResidentialAddress from '@salesforce/label/c.CopyFromResidentialAddress';
import back from '@salesforce/label/c.Back';
import businessAccount from '@salesforce/label/c.PersonAccount';
import personAccount from '@salesforce/label/c.PersonAccount';
import quantity from '@salesforce/label/c.Quantity';
import totalPrice from '@salesforce/label/c.TotalPrice';
import unitPrice from '@salesforce/label/c.UnitPrice';
import siteAddress from '@salesforce/label/c.SiteAddress';
import noDescriptionProvided from '@salesforce/label/c.NoDescriptionProvided';
import total from '@salesforce/label/c.Total';
import shareTheKnowledge from '@salesforce/label/c.ShareTheKnowledge';
import noRelatedServicePoint from '@salesforce/label/c.NoRelatedServicePoint';
import validateMetadata from '@salesforce/label/c.ValidateMetadata';
import caseNumber from '@salesforce/label/c.CaseNumber';
import supplyCode from '@salesforce/label/c.SupplyCode';
import catalog from '@salesforce/label/c.Catalog';
import productOptionConfiguration from '@salesforce/label/c.ProductOptionConfiguration';
import defaultValue from '@salesforce/label/c.DefaultValue';
import value from '@salesforce/label/c.Value';
import key from '@salesforce/label/c.Key';
import min from '@salesforce/label/c.Min';
import max from '@salesforce/label/c.Max';
import selectProductOptionType from '@salesforce/label/c.SelectProductOptionType';
import noProductConfigurator from '@salesforce/label/c.NoProductConfigurator';
import addDetails from '@salesforce/label/c.AddDetails';
import addressInformations from '@salesforce/label/c.AddressInformations';
import remove from '@salesforce/label/c.Remove';
import none from '@salesforce/label/c.None';
import createNewRelation from '@salesforce/label/c.CreateNewRelation';
import customerNotInTheList from '@salesforce/label/c.CustomerNotInTheList';
import filters from '@salesforce/label/c.Filters';
import priceBook from '@salesforce/label/c.PriceBook';
import next from '@salesforce/label/c.Next';
import family from '@salesforce/label/c.Family';
import productType from '@salesforce/label/c.ProductType';
import noFilters from '@salesforce/label/c.NoFilters';
import noProducts from '@salesforce/label/c.NoProducts';
import emptyCompanyDivision from '@salesforce/label/c.EmptyCompanyDivision';
import selectionsOutOfRange from '@salesforce/label/c.SelectionsOutOfRange';
import interlocutorSearchResults from '@salesforce/label/c.InterlocutorSearchResults';
import notInterlocutorInTheList from '@salesforce/label/c.NotInterlocutorInTheList';
import searchForInterlocutor from '@salesforce/label/c.SearchForInterlocutor';
import searchForCustomer from '@salesforce/label/c.SearchForCustomer';
import noCustomerSelected from '@salesforce/label/c.NoCustomerSelected';
import addAnotherCustomer from '@salesforce/label/c.AddAnotherCustomer';
import addressNotValid from '@salesforce/label/c.AddressNotValid';
import typeText from '@salesforce/label/c.TypeText';
import typeDouble from '@salesforce/label/c.TypeDouble';
import typeHere from '@salesforce/label/c.TypeHere';
import switchIn from '@salesforce/label/c.SwitchIn';
import requiredServiceSite from '@salesforce/label/c.RequiredServiceSite';
import pointAddress from '@salesforce/label/c.PointAddress';
import serviceSite from '@salesforce/label/c.ServiceSite';
import contractAccountEdit from '@salesforce/label/c.ContractAccountEdit';
import commodity from '@salesforce/label/c.Commodity';
import privacyRecap from '@salesforce/label/c.PrivacyRecap';
import reactivation from '@salesforce/label/c.Reactivation';
import createCase from '@salesforce/label/c.CreateCase';
import createOsi from '@salesforce/label/c.CreateOsi';
import osiEdit from '@salesforce/label/c.OsiEdit';
import caseSuccessfullyUpdated from '@salesforce/label/c.CaseSuccessfullyUpdated';
import groupBy from '@salesforce/label/c.GroupBy';
import supplySearch from '@salesforce/label/c.SupplySearch';
import enforcedOnUser from '@salesforce/label/c.EnforcedOnUser';
import theFieldLengthShouldBeLessOrEqualThen10Characters from '@salesforce/label/c.TheFieldLengthShouldBeLessOrEqualThen10Characters';

const labels = {
    switchIn,
    selectionsOutOfRange,
    noProductConfigurator,
    none,
    filters,
    priceBook,
    confirm,
    next,
    value,    
    family,
    typeText,
    typeDouble,
    typeHere,
    selectFamilyProduct,
    productType,
    noFilters,
    noProducts,
    key,
    min,
    max,
    addDetails,
    contractEndDate,
    customerSignedDate,
    noCustomerSelected,
    addAnotherCustomer,
    productOptionConfiguration,
    defaultValue,
    selectProductOptionType,
    supplyCode,
    filter,
    selectSupply,
    allSupplies,
    caseNumber,
    validateMetadata,
    noRelatedServicePoint,
    shareTheKnowledge,
    total,
    noDescriptionProvided,
    code,
    siteAddress,
    unitPrice,
    totalPrice,
    quantity,
    product,
    businessAccount,
    personAccount,
    selectAccountType,
    shortEntry,
    noDocument,
    billingInformation,
    details,
    checkout,
    check,
    noCart,
    cart,
    productName,
    noImagePresent,
    productImage,
    add,
    opportunityServiceItem,
    update,
    validate,
    requiredCheckDocument,
    mandatory,
    confirmation,
    confirmationQuestion,
    servicePoint,
    companyDivision,
    selectDivision,
    pointSelection,
    find,
    success,
    warning,
    error,
    noDataEntered,
    requiredFields,
    New,
    Status,
    roles,
    relatedContactList,
    relatedAccountList,
    notFound,
    customerSearchResults,
    VATNumber,
    nationalIdentityNumber,
    BsnType,
    active,
    phone,
    email,
    accountServices,
    search,
    supplySelectionLabel,
    servicePointSearchLabel,
    invalidCode,
    individualEditForm,
    newSearch,
    newContract,
    select,
    contractName,
    customerCode,
    caseEdit,
    save,
    cancel,
    cancellationRequest,
    cancellationReason,
    no,
    yes,
    informationRequired,
    documentChecking,
    fileMetadata,
    createdDate,
    dossier,
    validatableDocuments,
    billingProfile,
    edit,
    enforced,
    clone,
    billingProfileEdit,
    paymentMethod,
    billingAddress,
    addressValidated,
    addressMustBeVerified,
    close,
    duplicatedRecord,
    customer,
    pleaseSelect,
    contactDetails,
    fistnameOrLastnameRequired,
    individualCreated,
    individualNotCreated,
    documentValidated,
    account,
    addressForced,
    selectAddress,
    invalidFields,
    validateAddress,
    validated,
    chooseDocument,
    documentUpload,
    recentUploadedDocuments,
    documentUploaded,
    checkDocument,
    copyFromResidentialAddress,
    copyFromOtherAddresses,
    back,
    catalog,
    addressInformations,
    remove,
    createNewRelation,
    customerNotInTheList,
    emptyCompanyDivision,
    interlocutorSearchResults, notInterlocutorInTheList, searchForInterlocutor,
    searchForCustomer,
    addressNotValid,
    requiredServiceSite,
    pointAddress,
    serviceSite,
    contractAccountEdit,
    commodity,
    privacyRecap,
    reactivation,
    createCase,
    createOsi,
    osiEdit,
    caseSuccessfullyUpdated,
    groupBy,
    supplySearch,
    enforcedOnUser,
    theFieldLengthShouldBeLessOrEqualThen10Characters
};

export { labels };