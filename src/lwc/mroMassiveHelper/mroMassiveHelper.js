/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 07.05.20.
 */

import {LightningElement, track, api} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error} from 'c/notificationSvc';

export default class MroMassiveHelper extends LightningElement {
    @api templateId;
    @api documentId;
    @api dossierId;
    @api params;
    @api disabled = false;

    @track interval;
    @track progress = 0;
    @track status = 'Creating File Import Job';
    @track sampleLinkHref;
    @track downloadParameter;
    @track isFirstGetProgress = true;
    @track jobId;

    connectedCallback(){
        if(this.dossierId && !this.documentId){
            this.loadJob();
        }
    }

    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        this.documentId = uploadedFiles[0].documentId;
        this.handleImport();
    }

    loadJob() {
        notCacheableCall({
            className: "MRO_LC_MassiveHelper",
            methodName: "loadJob",
            input: {
                dossierId: this.dossierId
            }
        }).then(response => {
            if (response) {
                if (response.data.error) {
                    error(this, response.data.errorMessage);
                } else if(response.data.jobId){
                    this.jobId = response.data.jobId;
                    this.setProgressInterval();
                }
            }
        });
    }

    handleImport() {
        notCacheableCall({
            className: "MRO_LC_MassiveHelper",
            methodName: "createFileImportJob",
            input: {
                templateId: this.templateId,
                documentId: this.documentId,
                dossierId: this.dossierId,
                params: this.params,
            }
        }).then(response => {
            if (response) {
                if (response.data.error) {
                    error(this, response.data.errorMessage);
                } else {
                    this.jobId = response.data.jobId;
                    this.setProgressInterval();
                }
            }
        });
    }

    setProgressInterval(){
        this.interval = setInterval(() => {
            this.getProgress();
        }, 3000);
    }

    getProgress() {
        notCacheableCall({
            className: "MRO_LC_MassiveHelper",
            methodName: "getProgress",
            input: {
                templateId: this.templateId,
                documentId: this.documentId,
                dossierId: this.dossierId,
            }
        }).then(response => {
            if (response) {
                if (response.data.error) {
                    error(this, response.data.errorMessage ? response.data.errorMessage : response.data.status);
                    this.sendEvent('error');
                } else {
                    this.progress = response.data.progress;
                    this.status = response.data.status;
                    if (response.data.progress === 100) {
                        if(this.isFirstGetProgress){
                            this.resetParameters();
                        }else {
                            this.sendEvent('uploadfinished');
                        }
                    }
                }
                this.isFirstGetProgress = false;
            }
        });
    }

    getSample() {
        notCacheableCall({
            className: "MRO_LC_MassiveHelper",
            methodName: "getSample",
            input: {
                templateId: this.templateId
            }
        }).then(response => {
            if (response) {
                if (response.data.error) {
                    error(this, response.data.errorMessage);
                } else {
                    this.downloadParameter = response.data.fileTemplateName + '.csv';
                    this.sampleLinkHref = response.data.sampleLinkHref;
                }
            }
        });
    }

    sendEvent(type, detail) {
        this.resetParameters();
        this.dispatchEvent(new CustomEvent(type, {
            detail: detail ? detail : {}
        }, {bubbles: true}));
    }

    resetParameters(){
        this.jobId = null;
        this.documentId = null;
        this.progress = null;
        this.status = null;
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    get controlDisabled(){
        return this.disabled || this.showProgress;
    }

    get showProgress(){
        return this.documentId || this.jobId;
    }
}