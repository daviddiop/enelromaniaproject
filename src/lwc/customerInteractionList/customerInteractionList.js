import {LightningElement, api, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error} from 'c/notificationSvc';
import { labels } from 'c/labels';

export default class CustomerInteractionList extends LightningElement {
    _interactionId;
    labels = labels;
    @track address='';

    @api
    get interactionId() {
        return this._interactionId;
    }
    set interactionId(value) {
        this._interactionId = value;
        this.initData();
    }

    @track customerInteractionList;
    @track rolesList;
    @track isInteractionClosed = false;

    @api
    get isNotEmpty() {
        return this.customerInteractionList && this.customerInteractionList.length > 0;
    }

    @api
    refresh() {
        this.initData();
    }

    customerInteractionListItemEventHandler(event) {
        let type = event.detail.type;
        let customerInteraction = Object.assign({}, event.detail.customerInteraction);
        if (type === 'delete') {
            this.deleteCustomerInteraction(customerInteraction);
        } else if (type === 'edit') {
            if (customerInteraction.role !== event.detail.newRole) {
                customerInteraction.role = event.detail.newRole;
                this.updateRole(customerInteraction);
            }
        }
    }

    initData() {
        if (this.interactionId) {
            notCacheableCall({
                className: 'CustomerInteractionCnt',
                methodName: 'initData',
                input: { interactionId: this.interactionId },
            }).then((response) => {
                this.rolesList = response.data.rolesList;
                this.isInteractionClosed = response.data.isInteractionClosed;
                this.customerInteractionList = response.data.customerInteractionList;

            }).catch(errorMessage => {
                error(this, errorMessage);
            });
        }
    }
    updateRole(customerInteraction) {
        notCacheableCall({
            className: 'CustomerInteractionCnt',
                 methodName: 'updateRole',
                 input: { customerInteraction: JSON.stringify(customerInteraction)},
            }).then((response) =>{
                this.customerInteractionList = response.data;
            }).catch(errorMessage => {
                error(this, errorMessage);
            });
    }


    @api
    deleteCustomerInteraction(customerInteraction) {
        notCacheableCall({
            className: 'CustomerInteractionCnt',
                 methodName: 'deleteCustomerInteraction',
                 input: { customerInteraction: JSON.stringify(customerInteraction)},
            }).then((response) =>{
                this.customerInteractionList = JSON.stringify(response);
                this.dispatchEvent(new CustomEvent('remove'));
            }).catch(errorMessage => {
                error(this, errorMessage);
        });
        /*deleteCustomerInteraction({customerInteraction: customerInteraction}).then(response => {
            this.customerInteractionList = response;
            this.dispatchEvent(new CustomEvent('remove'));
        })
        .catch(errorMessage => {
            error(this, errorMessage);
        });*/
    }
   /* updateRole(customerInteraction) {
        updateRole({customerInteraction: customerInteraction})
            .then(response => {
                this.customerInteractionList = response;
            })
            .catch(errorMessage => {
                error(this, errorMessage);
            });
    }*/

    onNew() {
        this.dispatchEvent(new CustomEvent('new'));
    }
}