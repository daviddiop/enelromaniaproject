/**
 * Created by tomasoni on 08/08/2019.
 */
import {LightningElement, api, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import {NavigationMixin} from 'lightning/navigation';
import {error} from 'c/notificationSvc';
import {labels} from 'c/mroLabels';

export default class contractAccountEdit extends NavigationMixin(LightningElement) {
    contractAccount;
    labels = labels;
    errorToast = error;
    @api contractAccountRecordId;
    @api accountId;
    @api buttonType;
    @api showHeader = false;
    @api commodity;
    @api market;
    @api inEnelArea = false;
    @api selfReadingEnabled = false;
    @api vatNumber;
    //FF  [ENLCRO-669] Contract Account Selection - Check Interface
    @api contractType;
    //FF  [ENLCRO-669] Contract Account Selection - Check Interface
    @api isLatePaymentPenalty = false;
    @api isPaymentTerms = false;
    //For Sub-Process Public Tender the contract type should be always business  and markt = Free
    @api hasSubProcessPublicTender = false;
    // For Sub-Process Public Tender the contract type should be always business  and markt = Free

    @api streetName;
    @api pointAddressProvince;
    @track showModalContract = false;
    @api readOnly = false;
    @api refundFilter;

    @api reuseBillingAccountNumber = false;
    @api opportunityServiceItems = [];
    billingAccountNumber = null;
    reusableContractAccount = null;

    // [ENLCRO-714]
    @api hideSelfReadingPeriodEndField = false;
    @track billingProfileId = '';
    @track disabled = true;
    @track show = false;
    @api companyDivisionId;
    @track continueHandleSuccess;
    @track showLoadingSpinner = true;
    @track isFieldDisabled = false;
    @track isFieldDisabledFrequency = false;
    @track selfReadingPeriodEndList;
    @api hasDirectDebitInBillingProfile = false;
    disableSaveButton =  true ;
    disableMarket = false;
    freeMarket;
    regulatedMarket;
    contractTypeResidential;
    contractTypeBusiness;
    paymentTerms10Days;
    paymentTerms15Days;
    paymentTerms30Days;
    paymentPenalty_01_PL_0GD;
    paymentPenalty_03_PL_15GD;
    paymentPenalty_04_PL_30GD;
    electricCommodity;
    gasCommodity;
    selfReadingPeriodEndVal;
    billingProfilePaymentMethod;

    _paymentTerms;
    _latePaymentPenalty;

    get isBillingProfileDisabled() {

        let isBillingProfileDisabled = false;
        isBillingProfileDisabled =
            this.reuseBillingAccountNumber && this.opportunityServiceItems && this.billingProfileId;
        return isBillingProfileDisabled;
    }

    @api
    get paymentTerms() {
        return this._paymentTerms;
    }
    set paymentTerms(value) {
        this._paymentTerms = value;
        if (value) {
            this.isPaymentTerms = true;
        }
    }

    @api
    get latePaymentPenalty() {
        return this._latePaymentPenalty;
    }
    set latePaymentPenalty(value) {
        this._latePaymentPenalty = value;
        if (value) {
            this.isLatePaymentPenalty = true;
        }
    }

    connectedCallback() {

        console.log('mroContractAccountEdit.connectedCallback - Enter');
        this.showLoadingSpinner = false;
        if (this.buttonType === 'new') {
            this.contractAccountRecordId = '';
            this.billingProfileId = '';
            if(this.reuseBillingAccountNumber && this.opportunityServiceItems) {
                this.getBillingAccountNumber();
            }
        }
        this.show = true;
        this.getConstants();
    }

    getBillingAccountNumber() {

        console.log('mroContractAccountEdit.getBillingAccountNumber - Enter');
        console.log('mroContractAccountEdit.getBillingAccountNumber - opportunityServiceItems',
                JSON.stringify(this.opportunityServiceItems));
        let inputs = {
            opportunityServiceItems: JSON.stringify(this.opportunityServiceItems)
        };
        notCacheableCall({
            className: 'MRO_LC_ContractAccount',
            methodName: 'getBillingAccountNumber',
            input: inputs
        })
        .then((response) => {

            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }
            console.log('mroContractAccountEdit.getBillingAccountNumber (Callback) - contractAccount', response.data.contractAccount);
            let contractAccount = response.data.contractAccount;
            let billingAccountNumber = contractAccount.BillingAccountNumber__c;
            let billingProfileId = contractAccount.BillingProfile__c;
            this.billingAccountNumber = billingAccountNumber;
            this.billingProfileId = billingProfileId;
            this.reusableContractAccount = contractAccount;
        })
        .catch((error) => {
            console.log('mroContractAccountEdit.getBillingAccountNumber (Callback) - error', error.body.message);
            this.errorToast(this, JSON.stringify(error.body.message));
        });
    }

    get showSelfReadingField (){
        if(this.selfReadingPeriodEndList){
            return 'slds-col slds-size_1-of-2';
        }
        return 'slds-hide';
    }

    get hideSelfReadingField (){
        if(this.selfReadingPeriodEndList){
            return 'slds-hide';
        }
        return 'slds-col slds-size_1-of-2';
    }

    get hideRequired (){
        if(this.selfReadingPeriodEndList){
            return '';
        }
        return 'wr-required';
    }

    handleLoadCA() {
        let selfReadingPeriodEnd = this.template.querySelector('[data-id="selfReadingPeriodEnd"]');
        let billingFrequency = this.template.querySelector('[data-id="billingFrequency"]');


        if ((this.buttonType !== 'new')&&  (this.billingProfileId === '')) {
            let inputCmp = this.template.querySelector('[data-id="myBillingProfileId"]');
            this.billingProfileId = inputCmp.value;
            this.show = true;
        }
        if (this.buttonType === 'new') {
            let inputCommodity = this.template.querySelector('[data-id="myCommodity"]');
            inputCommodity.value = this.commodity;
        }
        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
        console.log('this.contractType ' + this.contractType);
        if ((this.buttonType === 'new') && (this.contractType)) {
            let inputContractType = this.template.querySelector('[data-id="contractType"]');
            inputContractType.value = this.contractType;
            let inputMarket = this.template.querySelector('[data-id="myMarket"]');

            if (this.hasSubProcessPublicTender || (this.commodity === this.gasCommodity)) {
                this.market = 'Free';
            }

            if(this.market){
                inputMarket.value = this.market;
            }
            if(this.market){
                if (!this.paymentTerms) {
                    this.updatePaymentTerm(this.market);
                }
                if (!this.latePaymentPenalty) {
                    this.updateLatePaymentPenality(this.market);
                }
            }
        }

        if (this.inEnelArea && this.contractType === 'Residential' && this.commodity !== 'Gas' && (this.buttonType === 'new' || this.buttonType === 'clone' || this.buttonType === 'edit')) {
            let inputMarket = this.template.querySelector('[data-id="myMarket"]');
            if(inputMarket && inputMarket.value){
                this.getListSelfReadingPeriodByCompanyDivision(inputMarket.value);
            }
        }


        if (this.inEnelArea && this.contractType === 'Residential' && this.commodity !== 'Gas' && !selfReadingPeriodEnd.value && !billingFrequency.value && (this.buttonType === 'new') && (this.streetName)) {
            this.getMapStreetNameBySelfReadingPeriodAndBillingFrequency();
        }


        if (this.contractType === 'Business' && this.commodity !== 'Gas' && !selfReadingPeriodEnd.value && !billingFrequency.value && (this.buttonType === 'new')) {
            selfReadingPeriodEnd.value = '31';
            billingFrequency.value = 'Monthly';
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }

        if (this.commodity === 'Gas' && this.selfReadingEnabled && !selfReadingPeriodEnd.value && !billingFrequency.value && (this.buttonType === 'new')) {
            selfReadingPeriodEnd.value = '26';
            billingFrequency.value = 'Monthly';
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }

        if (this.commodity === 'Gas' && !this.selfReadingEnabled && !selfReadingPeriodEnd.value && !billingFrequency.value && (this.buttonType === 'new')) {
            selfReadingPeriodEnd.value = '0';
            billingFrequency.value = 'Monthly';
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }


        if ((this.contractType === 'Business' || this.commodity === 'Gas') && (this.buttonType === 'edit' || this.buttonType === 'clone')) {
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }

        if (!this.inEnelArea && this.contractType === 'Residential' && this.commodity !== 'Gas' && (this.buttonType === 'new' || this.buttonType === 'edit' || this.buttonType === 'clone')) {
            this.isFieldDisabled = true;
            selfReadingPeriodEnd.value = '31';
            this.isFieldDisabledFrequency = false;
        }

        if(this.buttonType === 'edit' || this.buttonType === 'clone') {
            let marketField = this.template.querySelector('[data-id="myMarket"]');
            if(marketField) {
                console.log('FF this.market: ' + marketField.value);
                if (marketField.value === 'Regulated') {
                    console.log('Entrato');
                    this.isLatePaymentPenalty = true;
                    this.isPaymentTerms = true;
                    this.disableMarket = true;
                } else if (marketField.value === 'Free') {
                    this.disableMarket = true;
                }
            }
        }

        if(this.billingProfileId){
            this.disableSaveButton = false;
        }
    }

    get hasBillingProfileIdLoaded() {
        return (this.buttonType === 'new') || ((this.buttonType !== 'new') && (this.billingProfileId !== ''));
    }

    get hasAccountId() {
        return (this.accountId != null);
    }

    handleCancel() {
        this.dispatchEvent(new CustomEvent('contractaccounteditclose'));
    }
    handleErrorCA(event) {
        error(this, event.detail.message);
        this.showLoadingSpinner = false;
    }
    handleSuccessCA(event) {
        event.preventDefault();
        event.stopPropagation();
        let payload = event.detail;
        const selectedEvent = new CustomEvent('contractaccounteditsave', {
            detail: {
                contractAccountEditId: payload.id,
                showModalContAcc: this.showModalContract,
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                contrAccountEditContractType: payload.fields.ContractType__c.value
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface

            }
        });
        this.dispatchEvent(selectedEvent);


        this.showLoadingSpinner = false;

        this.showModalContract = false;
    }

    handleSubmitCA(event) {

        event.preventDefault();
        event.stopPropagation();
        this.showLoadingSpinner = true;
        const fields = event.detail.fields;
        if (this.validateFields() && this.validateBillingProfilePaymentMethod()) {
            if (!this.billingProfileId) {
                error(this, this.labels.billingProfileNotExist);
                this.showLoadingSpinner = false;
                return;
            }

            fields.BillingProfile__c = this.billingProfileId;
            fields.CompanyDivision__c = this.companyDivisionId;

            if (this.buttonType === 'clone') {  //Should be EDIT BG
                this.contractAccountRecordId = '';
                fields.Account__c = this.accountId;
                this.template.querySelector('[data-id="contractAccountEditForm"]').recordId = this.contractAccountRecordId;
            }
            if (this.buttonType === 'new' && this.inEnelArea) {
                if(this.selfReadingPeriodEndVal){
                    fields.SelfReadingPeriodEnd__c = this.selfReadingPeriodEndVal;
                }

            }
            if(this.buttonType === 'new' && this.billingAccountNumber) {

                fields.BillingAccountNumber__c = this.billingAccountNumber;
                fields.DirectDebitActivationDate__c = this.reusableContractAccount.DirectDebitActivationDate__c;
                fields.DirectDebitAuth__c = this.reusableContractAccount.DirectDebitAuth__c;
                fields.DirectDebitTerminDate__c = this.reusableContractAccount.DirectDebitTerminDate__c;
            }
            this.template.querySelector('[data-id="contractAccountEditForm"]').submit(fields);
            this.showModalContract = true;

        }else{

            this.showLoadingSpinner = false;
        }

    }

    getBillingProfileRecordId(event) {
        event.stopPropagation();
        this.showModalContract = event.detail.showModalContract;
        this.billingProfileId = event.detail.billingProfileRecordId;
        this.billingProfilePaymentMethod = event.detail.billingProfilePaymentMethod;
        if(this.reuseBillingAccountNumber && this.reusableContractAccount && this.reusableContractAccount.BillingAccountNumber__c &&
                this.billingProfileId == this.reusableContractAccount.BillingProfile__c) {
            this.billingAccountNumber = this.reusableContractAccount.BillingAccountNumber__c;
        } else {
            this.billingAccountNumber = null;
        }
        if(this.billingProfileId){
            this.disableSaveButton = false;
        }
    }

    getEditButton (event) {
      event.stopPropagation();
      this.showModalContract = event.detail.showModalContract;
    }

    removeError(event) {
        let inputCmp = event.target;

        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
            if ((inputCmp.fieldName === 'Market__c') && (this.buttonType === 'new') && (this.contractType)) {
                let market=inputCmp.value;
                let paymentTermInputCmp = this.template.querySelector('[data-id="paymentTerms"]');
                this.updatePaymentTerm(market);
                this.updateLatePaymentPenality(market);
                if (this.inEnelArea && this.contractType === 'Residential' && this.commodity !== 'Gas' && (this.buttonType === 'new' || this.buttonType === 'edit')) {
                    this.getListSelfReadingPeriodByCompanyDivision(market);
                }
            }
        }
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        return areValid;
    };
    //FF  [ENLCRO-669] Contract Account Selection -  Check Interface
    get isContractTypeDisabled (){
        if(this.contractType){
            return true;
        }
       return false;
    }
    //FF  [ENLCRO-669] Contract Account Selection - Check Interface

    get isMarketDisabled (){
        return this.disableMarket || this.hasSubProcessPublicTender || (this.commodity===this.gasCommodity) || !!this.market ;
    }

    handleSelfReading(event) {
        let selectedSelfReadingPeriodEnd = this.selfReadingPeriodEndList.filter(function(selfReadingPeriodEnd) {
            return selfReadingPeriodEnd.value === event.detail.value;
        });
        let selfReadingPeriodEnd = this.template.querySelector('[data-id="selfReadingPeriodEnd"]');

        if (selectedSelfReadingPeriodEnd) {
            console.log('selectedSelfReadingPeriodEnd: ' + selectedSelfReadingPeriodEnd[0].value)
            if(selfReadingPeriodEnd){
                selfReadingPeriodEnd.value = selectedSelfReadingPeriodEnd[0].value;
                this.selfReadingPeriodEndVal= selectedSelfReadingPeriodEnd[0].value;
            }

            /*
            this.companyDivisionName = selectedCompany[0].label;
            this.companyDivisionId = selectedCompany[0].value;
            this.disableEnforced = false;

             */
        }

    }

    getListSelfReadingPeriodByCompanyDivision(market) {
        //console.log('this.pointAddressProvince: ' + JSON.stringify(this.pointAddressProvince));
        let inputs = {companyDivisionId: this.pointAddressProvince,vatNumber: this.vatNumber,market:market};
        notCacheableCall({
            className: 'MRO_LC_ContractAccount',
            methodName: 'getListSelfReadingPeriodByCompanyDivision',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let selfReadingPeriodEnd = this.template.querySelector('[data-id="selfReadingPeriodEnd"]');
                //console.log('response.data : ' + JSON.stringify(response.data));
                //console.log('response.data.selfReadingPeriodEnds: ' + JSON.stringify(response.data.selfReadingPeriodEnds));

                //console.log('response.data.: ' + JSON.stringify(response));
                if(response.data.selfReadingPeriodEnds){
                    this.selfReadingPeriodEndList = response.data.selfReadingPeriodEnds;
                    if(this.selfReadingPeriodEndList){
                        this.selfReadingPeriodEndVal = this.selfReadingPeriodEndList[0].value;
                    }
                    //console.log('this.selfReadingPeriodEndList[0].value: ' + JSON.stringify(this.selfReadingPeriodEndList[0].value));
                    if(selfReadingPeriodEnd && selfReadingPeriodEnd.value){
                        this.selfReadingPeriodEndVal = selfReadingPeriodEnd.value;
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    getMapStreetNameBySelfReadingPeriodAndBillingFrequency() {
        let inputs = {streetId: this.streetName};
        notCacheableCall({
            className: 'MRO_LC_ContractAccount',
            methodName: 'getMapStreetNameBySelfReadingPeriodAndBillingFrequency',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                /*
                if(!this.selfReadingPeriodEndList) {
                    ///selfReadingPeriodEnd
                    let mapStreetNameBySelfReadingPeriod = {};
                    let selfReadingPeriodEnd = this.template.querySelector('[data-id="selfReadingPeriodEnd"]');
                    mapStreetNameBySelfReadingPeriod = response.data.mapStreetNameBySelfReadingPeriod;
                    selfReadingPeriodEnd.value = '';
                    console.log('mapStreetNameBySelfReadingPeriod: ' + JSON.stringify(mapStreetNameBySelfReadingPeriod));

                    if (this.streetName && mapStreetNameBySelfReadingPeriod && (mapStreetNameBySelfReadingPeriod !== "{}") && (Object.keys(mapStreetNameBySelfReadingPeriod).length !== 0)) {
                        if (mapStreetNameBySelfReadingPeriod.hasOwnProperty(this.streetName)) {
                            Object.keys(mapStreetNameBySelfReadingPeriod).forEach((key) => {
                                if (this.streetName === key) {
                                    selfReadingPeriodEnd.value = mapStreetNameBySelfReadingPeriod[key];
                                }
                            });
                        }
                    }
                }
                ///selfReadingPeriodEnd
*/
                ///billingFrequency

                let mapStreetNameByBillingFrequency = {};
                let billingFrequency = this.template.querySelector('[data-id="billingFrequency"]');
                mapStreetNameByBillingFrequency = response.data.mapStreetNameByBillingFrequency;
                billingFrequency.value='';
                console.log('mapStreetNameByBillingFrequency: ' + JSON.stringify(mapStreetNameByBillingFrequency));
                if (this.streetName && mapStreetNameByBillingFrequency && (mapStreetNameByBillingFrequency !== "{}") && (Object.keys(mapStreetNameByBillingFrequency).length !== 0)){
                    if(mapStreetNameByBillingFrequency.hasOwnProperty(this.streetName)){
                        Object.keys(mapStreetNameByBillingFrequency).forEach((key) => {
                            if (this.streetName === key) {
                                billingFrequency.value = mapStreetNameByBillingFrequency[key];
                            }
                        });
                    }
                }
                ///billingFrequency
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    getConstants() {
        cacheableCall({
            className: "MRO_UTL_Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.contractTypeResidential = apexResponse.data.CONTRACTTYPE_RESIDENTIAL;
                        this.contractTypeBusiness=apexResponse.data.CONTRACTTYPE_BUSINESS;
                        this.freeMarket=apexResponse.data.MARKET_FREE;
                        this.regulatedMarket=apexResponse.data.MARKET_REGULATED;
                        this.paymentTerms10Days=apexResponse.data.CONTRACT_ACCOUNT_PAYMENT_TERM_DUE_10DAYS;
                        this.paymentTerms15Days=apexResponse.data.CONTRACT_ACCOUNT_PAYMENT_TERM_DUE_15DAYS;
                        this.paymentTerms30Days=apexResponse.data.CONTRACT_ACCOUNT_PAYMENT_TERM_DUE_30DAYS;
                        this.paymentPenalty_01_PL_0GD=apexResponse.data.CONTRACT_ACCOUNT_PAYMENT_PENALTY_01_PL_0GD;
                        this.paymentPenalty_03_PL_15GD=apexResponse.data.CONTRACT_ACCOUNT_PAYMENT_PENALTY_03_PL_15GD;
                        this.paymentPenalty_04_PL_30GD=apexResponse.data.CONTRACT_ACCOUNT_PAYMENT_PENALTY_04_PL_30GD;
                        this.electricCommodity = apexResponse.data.COMMODITY_ELECTRIC;
                        this.gasCommodity = apexResponse.data.COMMODITY_GAS;
                    }
                }
            });
    }

    updatePaymentTerm(market){
        let paymentTerms;
        let paymentTermInputCmp = this.template.querySelector('[data-id="paymentTerms"]');
        if(market === this.freeMarket){
            if (this.contractType === this.contractTypeResidential ) {
                paymentTerms = this.paymentTerms15Days;
            } else if (this.contractType === this.contractTypeBusiness) {
                paymentTerms = this.paymentTerms30Days;

            }
            this.isPaymentTerms = false;
        } else if (market === this.regulatedMarket){
            if (this.contractType === this.contractTypeResidential ) {
                paymentTerms = this.paymentTerms15Days;
            } else if (this.contractType === this.contractTypeBusiness) {
                paymentTerms = this.paymentTerms10Days;
            }
            this.isPaymentTerms = true;
        }
        if(paymentTerms){
            paymentTermInputCmp.value = paymentTerms;
        }

    }



    updateLatePaymentPenality(market){
        let paymentPenaltyInput= this.template.querySelector('[data-id="latePaymentPenalty"]');
        let paymentPenaltyValue;
        if(this.commodity && market && this.contractType){
            if(this.commodity === this.gasCommodity && market === this.freeMarket){
                paymentPenaltyValue = this.paymentPenalty_01_PL_0GD;
                this.isLatePaymentPenalty = false;
            } else if(this.commodity === this.electricCommodity){

                if(market === this.freeMarket){
                    if(this.contractType === this.contractTypeResidential){
                        paymentPenaltyValue = this.paymentPenalty_03_PL_15GD;
                    }else if(this.contractType === this.contractTypeBusiness){
                        paymentPenaltyValue = this.paymentPenalty_01_PL_0GD;
                    }
                    this.isLatePaymentPenalty = false;
                }else if(market === this.regulatedMarket) {
                    if(this.contractType === this.contractTypeResidential){
                        paymentPenaltyValue = this.paymentPenalty_04_PL_30GD;
                    }else if(this.contractType === this.contractTypeBusiness){
                        paymentPenaltyValue = this.paymentPenalty_04_PL_30GD;
                    }
                    this.isLatePaymentPenalty = true;
                }

            }
        }
        if(paymentPenaltyValue){
            paymentPenaltyInput.value=paymentPenaltyValue;
        }


    }

    validateBillingProfilePaymentMethod(){
        let valid = true;
        if(this.hasDirectDebitInBillingProfile && this.billingProfilePaymentMethod === 'Direct Debit'){
            error(this, this.labels.notAvailableToSelectADirectDebitPayment);
            this.showLoadingSpinner = false;
            valid = false;
        }
        return valid;
    }

}