import {LightningElement, api, track} from 'lwc';
import {error} from 'c/notificationSvc';
import {labels} from 'c/labels';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";

export default class ValidatableDocumentsList extends LightningElement {
    @api recordId;
    @track documentTypes;
    @track filemetadata;
    @track oneSectionOpened;

    labels = labels;

    connectedCallback() {
        this.loadValidatableDocumentsData();
    }

    loadValidatableDocumentsData() {
        const recordIdParam = this.recordId;

        if (recordIdParam === null) {
            error(this, this.labels.validateMetadata);
            return;
        }

        let inputs = {filemetadataId: recordIdParam};

        notCacheableCall({
            className: "ValidatableDocumentsCnt",
            methodName: "getFileMetadataAndValidatableDocuments",
            input: inputs
        })
            .then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                        return;
                    }
                    this.filemetadata = response.data.filemetadata;
                    this.documentTypes = response.data.documentTypes;
                }
            })
            .catch(errorMsg => {
                error(this, errorMsg);
            });
    }

    get filemetadataDTO() {
        return this.filemetadata;
    }

    handleChangeSection(event) {
        this.oneSectionOpened = event.target.sectionOpened;
    }
}