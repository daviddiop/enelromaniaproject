import { LightningElement, api, wire, track } from 'lwc';
import {labels} from 'c/labels';

export default class productOptionAppliedEdit extends LightningElement {

    labels = labels;
    @track helpText ='';
    @track label;
    @track defaultValue;
    @track listValues = [];
    @track mapValues = [];
    @track productOption ;
    @track isRequired;
    @track isSelectedOrRequired;

     // @api
    @api
    get productOptionEdit() {
        return null;
    }
    set productOptionEdit(option){
         this.productOption = option;
         this.label = this.productOption.label;
         this.defaultValue = this.productOption.defaultValue;
         this.listValues = this.productOption.listValues;
         this.mapValues = this.productOption.mapValues;
         this.helpText = this.productOption.helpText;
    }


    get isNumeric() {
       if(this.productOption.dataType === 'Currency' || this.productOption.dataType === 'Numeric'
                      || this.productOption.dataType === 'Percentage'){
         return true;
      }
    }
    get isDate(){
     return this.productOption.dataType === 'Date';
    }
    get isText(){
         return this.productOption.dataType === 'Text';
    }
    get isList(){
        return this.productOption.dataType === 'List';
    }
    get isKeyValue(){
        return this.productOption.dataType === 'Key/Value';
    }
    get isCheckedBox(){
        return this.productOption.dataType === 'Checkbox';
    }
    get isNotCheckedBox(){
        return this.productOption.dataType !== 'Checkbox';
    }
    get required(){
        if(this.isNotCheckedBox){
            return true;
        }
    }
    get isReadOnly(){
        return this.productOption.accessibility == 'Read only'
    }
    get isSelected(){
        return this.productOption.selected;
    }
    get selectedOrRequired(){
        if(this.required || this.isSelected){
            return true;
        }
    }
    get isHelpText(){
        if(this.productOption.helpText){
            return true;
        }
    }

}