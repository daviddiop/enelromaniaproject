/**
 * Created by Octavian on 11/20/2019.
 */

import {api, track, LightningElement} from 'lwc';

export default class MroMeterList extends LightningElement {
    @api meterList; //JSON containing the list of Meters
    @api validateReadings; //boolean value that enables/disables
    @api disabled=false;
    @api disallowEdit= false;
    @api title;
    @api titleCode;
    @api recordTypeName;
    @track defaultTitle="Meters";
    @api hideValidateButton= false;
    @api isUsedSelfReadingCmp = false;
    @api servicePointCode;
    @api skipMetersValidation = false;
    @api isTransferOrProductChange = false;
    @api allowEditOnlyActiveEnergy = false;
    @api processName;
    @api params;

    @track meterNumbers = [];
    @track meterQuadrantsJsons = [];
    @track hasContent = false;

    connectedCallback() {
        if(this.title){
            this.defaultTitle= this.title;
        }
        if(this.meterList)
        {

            let metersArray = Object.values(this.meterList)[0];
            this.meterNumbers.splice(0, this.meterNumbers.length);
            for(let i = 0; i < metersArray.length; i++)
            {
                let meterNumber = metersArray[i]["MeterNumber__c"];
                this.meterNumbers.push(meterNumber);
                this.meterQuadrantsJsons.push(JSON.stringify(metersArray[i]["Quadrants"]));
            }
            this.hasContent = true;
        }
    }
    renderedCallback(){
        if (this.meterList) {
            this.setQuadrantInfo();
        }
    }


    @api setMeterList(meterList){
        this.meterList = meterList;
        let metersArray = Object.values(this.meterList)[0];
        this.meterNumbers.splice(0, this.meterNumbers.length);
        this.meterQuadrantsJsons.splice(0, this.meterQuadrantsJsons.length);
        for(let i = 0; i < metersArray.length; i++)
        {
            let meterNumber = metersArray[i]["MeterNumber__c"];
            this.meterNumbers.push(meterNumber);
            this.meterQuadrantsJsons.push(JSON.stringify(metersArray[i]["Quadrants"]));
        }
        this.setQuadrantInfo();
    }

    setQuadrantInfo()
    {
        this.template.querySelectorAll('c-mro-meter-tile').forEach((element,index) =>{
            element.setQuadrantsInfo(this.meterQuadrantsJsons[index]);
        })
        this.hasContent = true;
    }

    handleEditMeter(event){
        this.dispatchEvent(new CustomEvent('meterListEdited', {
            detail: {
                'meterCode': this.titleCode,
                'returnCode' : event.detail.code
            }
        }));
    }

    handleValidate(event){
        let validationData=this.validateMeterInfo();
        this.dispatchEvent(new CustomEvent('meterListValidated', {
            detail: {
                'isMeterValidated': validationData.isMeterValidated,
                'meterInfo': validationData.meterInfo,
                'meterCode': validationData.meterCode
            }
        }));
    }
    @api
    validateMeterInfo(){
        let isMeterListValidated = true;
        let quadrantList = [];
        let validationData;

        if(this.isTransferOrProductChange) {
            if (this.skipMetersValidation) {
                this.template.querySelectorAll('c-mro-meter-tile').forEach(element => {
                    quadrantList.push(element.getMeterInfoValidated());
                });
                this.dispatchEvent(new CustomEvent('meterListValidated', {
                    detail: {
                        'isMeterValidated': isMeterListValidated,
                        'meterInfo': quadrantList,
                        'meterCode': this.titleCode
                    }
                }));
            } else {
                this.template.querySelectorAll('c-mro-meter-tile').forEach(element => {
                    if (!element.isMeterValidated()) {
                        isMeterListValidated = false;
                    } else {
                        quadrantList.push(element.getMeterInfoValidated());
                    }
                });
                validationData = {
                    'isMeterValidated': isMeterListValidated,
                    'meterInfo': quadrantList,
                    'meterCode': this.titleCode
                }
            }
        } else {
            if (this.skipMetersValidation) {
                this.template.querySelectorAll('c-mro-meter-tile').forEach(element => {
                    quadrantList.push(element.getMeterInfo());
                });
                this.dispatchEvent(new CustomEvent('meterListValidated', {
                    detail: {
                        'isMeterValidated': isMeterListValidated,
                        'meterInfo': quadrantList,
                        'meterCode': this.titleCode
                    }
                }));
            } else {
                this.template.querySelectorAll('c-mro-meter-tile').forEach(element => {
                    if (!element.isMeterValidated()) {
                        isMeterListValidated = false;
                    } else {
                        quadrantList.push(element.getMeterInfo());
                    }
                });
                validationData = {
                    'isMeterValidated': isMeterListValidated,
                    'meterInfo': quadrantList,
                    'meterCode': this.titleCode
                }
            }
        }
        return validationData;
    }



}