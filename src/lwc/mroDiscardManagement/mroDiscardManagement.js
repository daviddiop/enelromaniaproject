/**
 * Created by pes on 11/05/2020.
 */

import {LightningElement, api, track} from 'lwc';
import {error, success} from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/mroLabels';

export default class MroDiscardManagement extends LightningElement {
    @api activityId;
    @track showSpinner = false;
    @track saveDisabled = true;
    dynamicFormJson;
    labels = labels;

    connectedCallback() {
        this.showSpinner = true;

        notCacheableCall({
            className: 'MRO_LC_DiscardManagement',
            methodName: 'initialize',
            input: {
                "activityId": this.activityId
            }
        }).then((response) => {
            console.log('Discard Management init response', JSON.parse(JSON.stringify(response)));
            if (response.isError) {
                this.showSpinner = false;
                error(this, response.error);
            } else {
                this.dynamicFormJson = JSON.stringify(response.data.dynamicForm);
                const dynamicFormCmp = this.template.querySelector("[data-id='dynForm']");
                dynamicFormCmp.setFormJson(this.dynamicFormJson);
                console.log('dynamicFormJson:', this.dynamicFormJson);
                this.showSpinner = false;
                this.saveDisabled = false;
            }
        }).catch((err) => {
            //console.log(JSON.parse(JSON.stringify(err)));
            this.showSpinner = false;
            error(this, err);
        })
    }

    saveData() {
        this.showSpinner = true;
        let checkResult = this.template.querySelector("[data-id='dynForm']").checkData();
        console.log('MroDiscardManagement.saveData - checkResult', JSON.stringify(checkResult));
        if(checkResult.isValid) {

            let records = JSON.stringify(this.template.querySelector("[data-id='dynForm']").getFormData());
//            console.log('MroDiscardManagement.saveData - records', records);
            notCacheableCall({
                className: 'MRO_LC_DiscardManagement',
                methodName: 'saveDataAndCloseActivity',
                input: {
                    "activityId": this.activityId,
                    "dynamicForm": this.dynamicFormJson,
                    "records": records
                }
            }).then((response) => {
                if (response.isError) {
                    this.showSpinner = false;
                    error(this, response.error);
                    console.error(response.error, response.errorStackTrace);
                } else if (response.data.result === 'OK') {
                    this.showSpinner = false;
                    success(this, this.labels.requestUpdated);
                    this.fireCloseEvent(true);
                }
            }).catch((error) => {
                console.log(JSON.parse(JSON.stringify(err)));
                this.showSpinner = false;
                error(this, error);
            });
        } else {

            this.showSpinner = false;
            error(this, checkResult.errorMessages);
        }

    }

    cancel() {
        this.fireCloseEvent(false);
    }

    fireCloseEvent(saved) {
        const selectedEvent = new CustomEvent('closediscardmanagement', {
            detail: {
                saved: saved
            }
        });
        this.dispatchEvent(selectedEvent);
    }
}