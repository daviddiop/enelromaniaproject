import { LightningElement, api } from 'lwc';
import {labels} from 'c/labels';
import { NavigationMixin } from "lightning/navigation";

export default class MroAccountList extends  NavigationMixin(LightningElement) {
    @api accountList;
    @api interactionId; 
    @api isRedirectAllowed;

    @api blockSaveCustomerInteractionOnSelection = false;

    labels = labels; 

    handleSelection(event) {
        if(!this.blockSaveCustomerInteractionOnSelection){
            const selectionEvent = new CustomEvent('selection', {
                detail: {
                    accountId: event.detail.selectedAccountId,
                    customerInteractionId: event.detail.customerInteractionId,
                    interlocutorId: event.detail.interlocutorId
                }
            });
            this.dispatchEvent(selectionEvent);
        } else {
            console.log('Selection only with accId');
            const selectionEvent = new CustomEvent('selection', {
                detail: {
                    accountId: event.detail.selectedAccountId,
                }
            });
            this.dispatchEvent(selectionEvent);
        }
    }

    onBack() {
        this.dispatchEvent(new CustomEvent('back', {
            detail: {
                'page': 'accountSearch'
            }
        }));
    }

    onNew() {
        if(this.isRedirectAllowed !== 'false'){ 
            this.addEventListener('new', this.navigateToAccountSearch(this.interactionId));
        }
        this.dispatchEvent(new CustomEvent('new'));
    }

    navigateToAccountEdit(interactionId) {
        this[NavigationMixin.Navigate]({
            type: "standard__component",
            attributes: {
                componentName: "c__mroAccountEdit"
            },
            state: {
                interactionId: interactionId
            }
        });
    }
}