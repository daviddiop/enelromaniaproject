import { LightningElement, api, wire, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import addressValidated from "@salesforce/label/c.AddressValidated";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { error, success, warn } from "c/notificationSvc";
import { labels } from "c/labels";

export default class AddressEditForm extends LightningElement {
    //**Start Deprecated */
    @api residentialAddress;
    @api otherAddresses;
    //**End Deprecated */

    label = {
        addressValidated
    };

    //@api address;
    @track _address = {};
    @api availableAddresses = [];
    @track normalizedAddresses = [];

    @api objectApiName = "";
    labels = labels;
    @api isValidated = false;
    @api isForced = false;
    @api isReadOnly;
    @api disableNormalize;
    @api showMessage;
    @api addressPrefix;
    @api readeOnlyFieldAddress;

    @track spinner = false;
    @track showAddressPicker = false;
    hasRendered = false;
    hasRenderedField = false;

    privateChildrenRecord = {};
    fields = [];
    error;

    selectedAddress = "";
    initialized = false;

    objInfo;

    @api
    get address() {
        return this._address;
    }
    set address(value) {
        //console.log('this.address000000: '+ JSON.stringify(value));
        this._address = value;
    }


    get copyAddressEnabled() {
        return (!this.isReadOnly && this.addressesToCopy && this.addressesToCopy.length);
    }

    get addressesToCopy() {
        if (this.normalizedAddresses && this.normalizedAddresses.length) {
            return this.normalizedAddresses;
        }
        if (!this.availableAddresses) {
            return [];
        }
        return Object.keys(this.availableAddresses).map(function(key) {
            return { key: key, values: this.availableAddresses[key] };
        }, this);
    }

    connectedCallback() {

        this.initialize();
    }

    renderedCallback() {
        if (this.isReadOnly && !this.hasRendered) {
            this.hasRendered = true;
            if (!this.isForced) {
                this.isValidated = true;
            }
        }
        if (this.objInfo && !this.hasRenderedField){
           /* console.log('entrato 1');
            console.log('this.addresstttt: '+ JSON.stringify(this.address));*/
            this.hasRenderedField = this.address && (Object.keys(this.address).length !== 0);
           // console.log('c ' + this.hasRenderedField);
            this.initializeFields();
        }
        if (this.readeOnlyFieldAddress){
            this.readOnlyField(this.readeOnlyFieldAddress);
        }
    }

    initialize() {
        this.spinner = true;
        let inputs = { objectApiName: this.objectApiName };
        notCacheableCall({
            className: "AddressFormCnt",
            methodName: "getObjectInfo",
            input: inputs
        }).then(response => {
            if (response.isError) {
                error(this, this.labels.error);
            } else {
                this.objInfo = response.data;
                if (!this.initialized && this.objInfo) {
                    this.initializeFields();
                    this.initialized = true;
                }
                this.spinner = false;
            }
        });
    }

    initializeFields() {
        let addrInputs = this.querySelectorAll("c-mro-address-input");
        for (let i of addrInputs) {
            let fieldName = i.fieldName;
            if (this.objInfo[fieldName.toLowerCase()]) {
                i.setLabel(this.objInfo[fieldName.toLowerCase()]);
            }
            if (i.element && this.address && this.address[i.element]) {
                i.setValue(this.address[i.element]);
            }
            if (i.element === 'streetType' && !i.getValue()){
                i.setValue('STRADA');
            }
            if (i.element === 'country' && !i.getValue()){
                i.setValue('ROMANIA');
            }
        }
    }

    @api
    forceInitializeFields() {
        this.initializeFields();
    }

    _checkMandatoryFields(exceptFields = []) {
        let addrFields = this.querySelectorAll("c-mro-address-input");
        let areValid = true;
        for (let f of addrFields) {
            if (!exceptFields.includes(f.element)) {
                if (f.required && !f.getValue()) {
                    f.classList.add("slds-has-error");
                    areValid = false;
                }
            }
        }
        if (!areValid) {
            error(this, this.labels.requiredFields);
            return false;
        }
        return true;
    }


    @api
    normalize() {
        this.isValidated = false;
        this.isForced = false;
        let checkExtension = this.checkExtensionField();
        if (!this._checkMandatoryFields() || !checkExtension) {
            this.showMessage = true;
            return;
        }
        //this.spinner = true;
        let addr = this.getAddress();
        this.normalizeService(addr);
    }

    @api
    getAddress() {
        let addr = {};
        let addrInputs = this.querySelectorAll("c-mro-address-input");
        for (let i of addrInputs) {
            if (i.element) {
                addr[i.element] = i.getValue();
            }
        }
        //console.log(addr);
        return addr;
    }

    @api
    forceAddress() {
        this.spinner = true;
        this.isValidated = false;
        this.isForced = false;
        let checkExtension = this.checkExtensionField();

        if (!this._checkMandatoryFields() || !checkExtension || !this._checkMandatoryFieldsForceAddr()) {
            this.spinner = false;
            this.showMessage = true;
            return;
        }
        let addr = this.getAddress();
        addr.addressNormalized = false;
        this.createAddress({ address: addr, isForceAddress: true })
            .then((address) => {
                this.setAddress(address);
                this.isForced = true;
                this.hasRendered = true;
                this.spinner = false;
                let addrNormalize = true;
                let addressForced = true;
                this.dispatchNormalize(addrNormalize, addressForced, address);
            })
            .catch(e => {
                this.spinner = false;
                error(this, e);
                //console.log(e);
            });

    }

    @api
    isValid() {
        return this.isValidated || this.isForced;
    }

    copyFromOther() {
        this.showAddressPicker = true;
    }

    handleSelection(event) {
        this.selectedAddress = event.detail.selected;
    }

    confirmAddress() {
        this.copyAddress(this.selectedAddress);
        this.closeModal();
    }

    @api
    copyAddress(addr) {
        //console.log('Copy address:', JSON.stringify(addr));
        this.setAddress(addr);
        if (addr.addressNormalized) {
            this.isValidated = true;
            this.isReadOnly = true;
            this.disableNormalize = true;
            this.dispatchEvent(new CustomEvent("isnormalize", {
                detail: {
                    "readOnly": this.isReadOnly,
                    "addressForced": this.addressForced,
                    "address": this.selectedAddress
                }
            }));
        }
    }

    closeModal() {
        this.showAddressPicker = false;
    }

    @api
    setAddress(address) {
        let addrInputs = this.querySelectorAll("c-mro-address-input");
        for (let i of addrInputs) {
            if (i.element && address[i.element] !== undefined) {
                i.setValue(address[i.element]);
            }
        }
    }

    @api
    getAddressFields() {
        let fields = {};
        let addrInputs = this.querySelectorAll("c-mro-address-input");
        for (let i of addrInputs) {
            if (i.fieldName) {
                fields[i.fieldName] = i.getValue();
            }
        }
        return fields;
    }
    /**
     * @author Boubacar Sow
     * @description make the field address read only
     *              [ENLCRO-1962] Service Point Address Change - Make PROVINCE field read only
     * @param fieldName
     */
    @api
    readOnlyField(fieldName){
        let addrInputs = this.querySelectorAll("c-mro-address-input");
        //fieldName = 'AddressProvince__c';
        for (let i of addrInputs) {
            if (i.fieldName === fieldName){
                i.readOnly = true;
            }
        }
    }


    handleChildRegister(event) {
        event.stopPropagation();
        const item = event.detail;
        const guid = item.guid;
        this.privateChildrenRecord[guid] = item;
        this.fields.push(item.field);
        item.callbacks.registerDisconnectCallback(this.handleChildUnregister);
    }

    handleChildUnregister(event) {
        const item = event.detail;
        const guid = item.guid;
        this.privateChildrenRecord[guid] = undefined;
    }

    showError(message) {
        const evt = new ShowToastEvent({
            title: this.labels.error,
            message: message,
            variant: "error",
            mode: "dismissable"
        });
        this.dispatchEvent(evt);
    }

    normalizeService(addr) {
        this.spinner = true;
        let inputs = { address: JSON.stringify(addr) };
        notCacheableCall({
            className: "MRO_LC_Address",
            methodName: "normalize",
            input: inputs
        })
            .then(response => {
                this.spinner = false;
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                } else if (response.data) {
                    const { addresses, autoSuggest } = response.data;

                    if (addresses) {
                        if (addresses.length === 0) {
                            error(this, this.labels.invalidFields);
                        } else if (addresses.length === 1 && !autoSuggest) {
                            this.setAddress(addresses[0]);
                            this.isValidated = true;
                            this.dispatchNormalize(addresses, false, addresses[0]);
                        } else {
                            const normalizedAddresses = [];
                            for (let x in addresses) {
                                if (addresses[x]) {
                                    normalizedAddresses.push({ ...addresses[x], key: x, isNormalized: true });
                                }
                            }
                            this.normalizedAddresses = [{ key: "", values: normalizedAddresses }];
                            this.showAddressPicker = true;
                        }
                    } else {
                        error(this, "Unexpected Error");
                    }
                }
            }).catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    dispatchNormalize(addrNormalize, addressForced, address) {
        this.dispatchEvent(new CustomEvent("isnormalize", {
            detail: {
                "readOnly": addrNormalize,
                "addressForced": addressForced,
                "address": address
            }
        }));
    }

    createAddress({address, isForceAddress = false}) {
        return notCacheableCall({
            className: "MRO_LC_Address",
            methodName: "createAddress",
            input: {address: JSON.stringify(address), isForceAddress}
        }).then((apexResponse) => {
            if (apexResponse.isError) {
                throw JSON.stringify(apexResponse.error);
            }
            return apexResponse.data;
        }).catch(e => {
            throw  typeof e == "string" ? e : e.body.message;
        });
    }

    checkExtensionField() {
        let addrFields = this.querySelectorAll("c-mro-address-input");
        let areValid = true;
        for (let f of addrFields) {
            if ((f.fieldName === (this.addressPrefix + 'StreetNumberExtn__c')) && f.getValue().length>10) {
                    f.classList.add("slds-has-error");
                    areValid = false;
            }
        }
        if (!areValid) {
            error(this, this.labels.theFieldLengthShouldBeLessOrEqualThen10Characters);
        }
        return areValid;
    }

    _checkMandatoryFieldsForceAddr(exceptFields = []) {
        let addrFields = this.querySelectorAll("c-mro-address-input");
        let areValid = true;
        for (let f of addrFields) {
            if (!exceptFields.includes(f.element)) {
                if (f.fieldName === (this.addressPrefix + 'PostalCode__c') && !f.getValue()) {
                    f.classList.add("slds-has-error");
                    areValid = false;
                }
            }
        }
        if (!areValid) {
            error(this, this.labels.requiredFields);
            return false;
        }
        return true;
    }
}