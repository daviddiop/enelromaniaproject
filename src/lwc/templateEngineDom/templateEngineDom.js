import JsonLogic from 'c/jsonLogic';

/** Starts the process of reading a block of HTML code and interpreting it
 * using the given input JSON object. Iterates through every child tag of the given
 * HTML block and evaluates the contents. **/
export function decorateTemplate(HTMLContent__c, inputData, JSONSubsection = '', arrayVariable = '') {

    let children = getHtmlDocument(HTMLContent__c).body.children;
    let template = HTMLContent__c;

    for (let index = 0; index < children.length; index++) {
        let evaluatedChild = evaluateChild(children[index].outerHTML, inputData, JSONSubsection, arrayVariable);
        template = getMainFinalTemplate(children[index].outerHTML, evaluatedChild, template)
    }
    return template;
}

/** Evaluates the given child-tag (and all other tags nested inside this one)
 *  if a JSON object is available at input. **/
function evaluateChild(HTMLContent__c, inputData, JSONSubsection, arrayVariable) {
    if (inputData === undefined) {
        return '';
    }

    let updatedHtmlEval = evaluateHtmlContent(HTMLContent__c, inputData, JSONSubsection, arrayVariable);
    let updatedHtmlEvalInner = evaluateInnerChildren(updatedHtmlEval, inputData, JSONSubsection);

    return updateJsonReferences(updatedHtmlEvalInner, inputData, JSONSubsection);
}

/** Cleans the tag contents and checks if it is an iteration, a condition or plain html.
 * If JSONSubsection is provided, it will be used in evaluating any placeholders. **/
function evaluateHtmlContent(HTMLContent__c, inputData, JSONSubsection, arrayVariable) {
    let htmlTemplate = cleanHtmlContent(HTMLContent__c);
    htmlTemplate = matchIterationBlock(htmlTemplate, getHtmlTagName(htmlTemplate), inputData, JSONSubsection, arrayVariable);
    htmlTemplate = matchConditionBlock(htmlTemplate, getHtmlTagName(htmlTemplate), inputData, JSONSubsection);

    return htmlTemplate;
}

/** Searches for all the placeholders in the given template and evaluates them. **/
function updateJsonReferences(htmlTemplate, inputJson, JSONSubsection) {
    let findPlaceholders = /{((\w+\.?\w*)+?)}/g;
    let tpl = htmlTemplate;
    let match;

    while ((match = findPlaceholders.exec(htmlTemplate))) {
        tpl = updateFinalTemplate(match, tpl, inputJson, JSONSubsection)[1]
    }
    return tpl;
}

/** Every tag can have an unknown number of nested and/or consecutive child-tags.
 * If the tag has any children, they will be evaluated individually. **/
function evaluateInnerChildren(htmlTemplate, inputData, JSONSubsection) {
    let documentChildren = getHtmlDocument(htmlTemplate).body.children;
    let tpl = htmlTemplate;

    if (documentChildren.length > 0) {
        for (let index = 0; index < documentChildren.length; index++) {
            let evaluatedChildren = evaluateChildrenOfChild(index, documentChildren, htmlTemplate, inputData, JSONSubsection);
            tpl = getInnerChildFinalTemplate(documentChildren[index].outerHTML, evaluatedChildren, tpl)
        }
        return tpl;
    } else {
        return htmlTemplate
    }
}

/** If a child-tag is father to one or more child-tags, the main decorateTemplate process
 * is called on the initial child-tag for evaluation. **/
function evaluateChildrenOfChild(index, documentChildren, htmlTemplate, inputData, JSONSubsection) {
    let childOfChild = documentChildren[index];

    if (getHtmlDocument(childOfChild.innerHTML).body.children.length > 0) {
        return decorateTemplate(childOfChild.innerHTML, inputData, JSONSubsection)
    } else {
        return childOfChild.innerHTML;
    }
}

/** Checks if the given tag is an iteration. If true, it will be evaluated and the original
 * html block will be replaced with the result of the evaluation. **/
function matchIterationBlock(htmlTemplate, tagName, inputData, JSONSubsection, arrayVariable) {
    if (tagName === 'ITERATION') {
        let iterBlock = getHtmlDocument(htmlTemplate).body;
        let iterBlockNoQuot = iterBlock.innerHTML.replace(/&quot;/g, '\"')
            .replace(/"{/g, '\'{')
            .replace(/}"/g, '}\'');
        let evaluatedBlock = evaluateIterationBlock(iterBlock, inputData, JSONSubsection, arrayVariable);

        htmlTemplate = htmlTemplate.replace(iterBlockNoQuot, evaluatedBlock);
    }

    return htmlTemplate;
}

/** If arrayVariable is provided, this is an iteration tag inside another iteration tag and a new
 * array attribute has to be computed. Evaluates the html block using the final array attribute. **/
function evaluateIterationBlock(iterBlock, inputJson, JSONSubsection, arrayVariable) {
    let array = getArrayAttribute(iterBlock);
    let newArray = computeNewArray(array, arrayVariable);
    let val = getValue(inputJson, newArray.split('.'), 0);
    let updatedIterBlock = updateIterBlockArray(iterBlock, array, newArray);

    return computeIterBlock(val, updatedIterBlock, JSONSubsection);
}

/** Checks if the given tag is a condition. If true, it will be evaluated and the original
 * html block will be replaced with the result of the evaluation. **/
function matchConditionBlock(htmlTemplate, tagName, inputData, JSONSubsection) {
    if (tagName === 'IF') {
        let condBlock = getHtmlDocument(htmlTemplate).body;
        let condBlockNoQuot = condBlock.innerHTML.replace(/&quot;/g, '\"')
            .replace(/"{/g, '\'{')
            .replace(/}"/g, '}\'');
        let evaluatedBlock = evaluateConditionBlock(condBlock, inputData, JSONSubsection);

        htmlTemplate = htmlTemplate.replace(condBlockNoQuot, evaluatedBlock);
    }
    return htmlTemplate
}

/** Evaluates the condition attribute and if it is true, the else block is removed
 * from the original html. If the condition is false, then only the else block will be kept.
 * The main decorateTemplate process is then called to evaluate the remaining html template. **/
function evaluateConditionBlock(condBlock, inputJson, JSONSubsection) {
    let updatedTemplate;
    let elseBlock = getElseBlock(condBlock);
    let condition = evaluateCondition(condBlock, inputJson, JSONSubsection);

    if (condition) {
        updatedTemplate = keepIfBlock(condBlock.innerHTML, elseBlock)
    } else {
        updatedTemplate = keepElseBlock(elseBlock.outerHTML);
    }

    return decorateTemplate(updatedTemplate, inputJson, JSONSubsection);
}

/** Retrieves the value of the first attribute from a given tag **/
function getTagDetails(htmlTemplate, tag) {
    let findTagRegEx = setFindTagRegEx(tag);
    let tagLine = findTagRegEx.exec(htmlTemplate)[0];
    let tagContent = tagLine.replace('<' + tag + ' ', '');
    let attributes = /"[\s\S]*"/.exec(tagContent);

    return attributes[0];
}

/** Reads the json input and retrieves the element found at the provided path. **/
function getValue(inputJson, path, index) {
    let parseJson = JSON.parse(inputJson);

    if (index === path.length - 1) {
        if (parseJson[path[index]] !== undefined) {
            return parseJson[path[index]];
        }
    } else {
        return getValue(JSON.stringify(parseJson[path[index]]), path, index + 1);
    }
}

/** Parses the htmTemplate from String into a DOM Document. **/
function getHtmlDocument(htmlTemplate) {
    return new DOMParser().parseFromString(htmlTemplate, 'text/html');
}

/** Reads the htmlTemplate, parses it and retrieves the name of the main tag. **/
function getHtmlTagName(htmlTemplate) {
    let htmlDocument = getHtmlDocument(htmlTemplate);

    if (htmlDocument.body.firstElementChild) {
        return htmlDocument.body.firstElementChild.tagName;
    } else {
        return 'OTHER'
    }
}

/** Parsing html templates into DOM Documents alters their content. A cleanup is needed
 * in order to keep the content readable and ready for future evaluations. **/
function cleanHtmlContent(HTMLContent__c) {
    if (HTMLContent__c.includes('&quot')) {
        return HTMLContent__c.replace(/"{/g, '\'{')
            .replace(/}"/g, '}\'')
            .replace(/&quot;/g, '\"')
    } else {
        return HTMLContent__c;
    }
}

/** Prepares the original html content from decorateTemplate to be replaced by its evaluated version. **/
function cleanFinalTemplate(HTMLContent__c) {
    let arrayLines = HTMLContent__c.match(/array="(.*)">/g);
    let tpl = HTMLContent__c;

    if (arrayLines) {
        for (let index = 0; index < arrayLines.length; index++) {
            let arrayLineSingleQuotes = arrayLines[index].replace(/"/g, '\'');
            tpl = tpl.replace(arrayLines[index], arrayLineSingleQuotes)
        }
    }

    return tpl.replace(/"{/g, '\'{')
        .replace(/}"/g, '}\'')
        .replace(/&quot;/g, '\"')
}

/** Retrieves the value of the array attribute from an iteration tag. **/
function getArrayAttribute(iterBlock) {
    let iterationParams = getTagDetails(iterBlock.innerHTML, 'iteration');

    return iterationParams.replace(/"/g, '');
}

/** If arrayVariable is provided, computes a new version of the array attribute. **/
function computeNewArray(array, arrayVariable) {
    if (arrayVariable === '') {
        return array
    } else {
        return arrayVariable + "." + array;
    }
}

/** Reads an iteration block and replaces the old version of the array attribute with the new version. **/
function updateIterBlockArray(iterBlock, array, newArray) {
    let updatedIterBlock = getHtmlDocument(iterBlock.innerHTML).body.firstElementChild.innerHTML;

    return updatedIterBlock.replace(array, newArray);
}

/** Calls the main decorateTemplate process once for every element in the val array,
 * passing the element as input Json to the iterationBlock which needs to be evaluated.
 * The name of every element is also passed in case of nested iteration child-tags.**/
function computeIterBlock(val, iterBlock, JSONSubsection) {
    let response = '';

    if (val.length > 0) {
        val.forEach(element => {
            let elementFirstValue = Object.keys(element)[0];
            response += decorateTemplate(iterBlock, JSON.stringify(element), JSONSubsection, elementFirstValue);
        });
    }

    return removeCustomTags(response);
}

/** Removes the elseBlock from a condition template, keeping only the code which should
 * be evaluated when the condition is true. **/
function keepIfBlock(htmlTemplate, elseBlock) {
    let tpl = htmlTemplate.replace(elseBlock.outerHTML, '');
    let htmlDocument = getHtmlDocument(tpl);

    return htmlDocument.body.firstElementChild.innerHTML;
}

/** If present, keeps only the code which should be evaluated when the condition is false.
 * If the else clause is missing, the method returns an empty string. **/
function keepElseBlock(elseBlock) {
    if (elseBlock !== undefined) {
        let htmlDocument = getHtmlDocument(elseBlock);

        return htmlDocument.body.firstElementChild.innerHTML;
    } else {
        return '';
    }
}

/** Takes a conditional block of code and, if present, retrieves the contents of the else tag. **/
function getElseBlock(condBlock) {
    let htmlDocument = getHtmlDocument(condBlock.innerHTML);
    let elseBlock = htmlDocument.getElementsByTagName('else');

    if (elseBlock.length > 0) {
        elseBlock = elseBlock[0];
    }

    return elseBlock
}

/** Evaluates the truth by applying the JsonLogic Library using the inputJson
 *   and the rules defined in the condition attribute of the if tag. **/
function evaluateCondition(condBlock, inputJson, JSONSubsection) {
    let rules = computeRules(condBlock, JSONSubsection);

    return JsonLogic.apply(JSON.parse(rules), JSON.parse(inputJson));
}

/** Reads a conditional block of html and retrieves the value of the condition attribute.
 * If dirty, the value is cleaned of any DOM-type formatting and variables are updated
 * with the JSONSubsection parameter if it is provided.**/
function computeRules(condBlock, JSONSubsection) {
    let ifParams = getTagDetails(condBlock.outerHTML, 'if');
    let rules = ifParams.replace(/&quot;/g, '\"')
        .replace(/"{/g, '{')
        .replace(/}"/g, '}');

    return computeRulesIfJsonSubsection(rules, JSONSubsection);
}

/** Retrieves the final value of the rules to be used in evaluating a condition.
 * If JSONSubsection is provided, the variables inside the rules are updated accordingly. **/
function computeRulesIfJsonSubsection(rules, JSONSubsection) {
    if (JSONSubsection) {
        rules.match(/"var" : "[\S]*"/g).forEach(varAndPlaceholder => {
                let oldPlaceholder = varAndPlaceholder.split(':')[1]
                    .replace(' ', '')
                    .replace(/"/g, '');
                let newPlaceholder = JSONSubsection + "." + oldPlaceholder;
                rules = rules.replace(oldPlaceholder, newPlaceholder);
            }
        )
    }

    return rules
}

/** Returns the regular expression required to identify an iteration or a condition tag. **/
function setFindTagRegEx(tag) {
    if (tag === 'if') {
        return new RegExp('<' + tag + '[\\s\\S]*?}[\\S]>', 'gi');
    } else {
        return new RegExp('<' + tag + '[\\s\\S]*?>', 'gi');
    }
}

/** If present, updates the placeholder match with the given JSONSubsection and evaluates it,
 *  then returns the updated version of the template. **/
function updateFinalTemplate(match, htmlTemplate, inputJson, JSONSubsection) {
    if (JSONSubsection) {
        match[1] = JSONSubsection + '.' + match[1];
    }

    let jsonPath = match[1].split('.');
    let val = getValue(inputJson, jsonPath, 0);

    return [match[0], replaceTemplateVariables(htmlTemplate, match[0], val)];
}

/** Updates the html template by replacing the placeholder variable
 * with the provided value, if available. **/
function replaceTemplateVariables(htmlTemplate, variable, val) {
    if (val) {
        return htmlTemplate.replace(variable, val);
    } else {
        return htmlTemplate
    }
}

/** Replaces the contents of the htmlChild from the original htmlTemplate with the evaluated value of the child. **/
function getInnerChildFinalTemplate(htmlChild, evaluatedChild, htmlTemplate) {
    let cleanChild = cleanFinalTemplate(htmlChild);
    let cleanTemplate = cleanFinalTemplate(htmlTemplate);
    let childContent = cleanFinalTemplate(getHtmlDocument(cleanChild).body.firstElementChild.innerHTML);

    return cleanTemplate.replace(childContent, evaluatedChild);
}

/** Replaces the htmlChild from the original htmlTemplate with the evaluated value of the child. **/
function getMainFinalTemplate(htmlChild, evaluatedChild, htmlTemplate) {
    let cleanChild = cleanFinalTemplate(htmlChild);
    let cleanTemplate = cleanFinalTemplate(htmlTemplate);

    return cleanTemplate.replace(cleanChild, evaluatedChild);
}

/** Removes all custom "if" and "iteration" opening and closing tags. **/
function removeCustomTags(htmlTemplate) {
    let templateNoIfs = htmlTemplate.replace(/<if[\s\S]*?}[\S]>/gi, '')
        .replace(/<\/if>/gi, '');

    return templateNoIfs.replace(/<iteration[\s\S]*?>/gi, '')
        .replace(/<\/iteration>/gi, '');
}

/*
@Deprecated;
Used as return type for evaluateHtmlContent, but removed;
function evaluateHtmlIfJsonSubsection(htmlTemplate, inputData, JSONSubsection) {
    if (JSONSubsection) {
        return updateJsonReferences(htmlTemplate, inputData, JSONSubsection);
    } else {
        return htmlTemplate
    }
 }
 */


//TODO: replace getTagDetails function with .getAttribute() on htmlElements;