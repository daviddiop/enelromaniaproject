import { LightningElement, api, track } from 'lwc';
import addressValidated from '@salesforce/label/c.AddressValidated';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { error } from "c/notificationSvc";
import {labels} from 'c/labels';

export default class AddressEditForm extends LightningElement {
    //**Start Deprecated */
    @api residentialAddress;
    @api otherAddresses;
    //**End Deprecated */

    label = {
        addressValidated
    };

    @api address;
    @api availableAddresses = [];

    @api objectApiName = '';
    labels = labels;
    @api isValidated = false;
    @api isForced = false;
    @api isReadOnly;
    @api disableNormalize ;
    @api showMessage ;
    @api addressPrefix ;

    @track spinner = false;
    @track showAddressPicker = false;
    hasRendered = false;

    privateChildrenRecord = {};
    fields = [];
    error;

    selectedAddress = '';
    initialized = false;

    objInfo;

    get addressesToCopy() {
        if(!this.availableAddresses){
            return [];
        }
        return Object.keys(this.availableAddresses).map(function(key) {
            return {key:key, values:this.availableAddresses[key]};
        },this);
    }

     connectedCallback() {
            this.initialize();
        }

        renderedCallback() {
            if(this.isReadOnly && !this.hasRendered){
                this.hasRendered = true;
                this.isValidated = true;
            }
        }

    initialize() {
        this.spinner = true;
        let inputs = { objectApiName: this.objectApiName };
        notCacheableCall({
            className: "AddressFormCnt",
            methodName: "getObjectInfo",
            input: inputs
        }).then(response => {
                if (response.isError) {
                    error(this, this.labels.error);
                } else {
                    this.objInfo = response.data;
                    if (!this.initialized && this.objInfo) {
                        let addrInputs = this.querySelectorAll('c-address-input');
                        for (let i of addrInputs) {
                            let fieldName = i.fieldName;
                            if(this.objInfo[fieldName.toLowerCase()] ){
                                i.setLabel(this.objInfo[fieldName.toLowerCase()]);
                            }
                            if (i.element && this.address && this.address[i.element]) {
                                i.setValue(this.address[i.element]);
                            }
                        }
                        this.initialized = true;
                    }
                    this.spinner = false;
                }
            });
    }

    _checkMandatoryFields() {
        let addrFields = this.querySelectorAll('c-address-input');
        let areValid = true;
        for (let f of addrFields) {
            if (f.required && !f.getValue()) {
                f.classList.add('slds-has-error');
                areValid = false;
            }
        }
        if (!areValid){
            error(this, this.labels.requiredFields);
            return false;
        }
        return true;
    }

    @api
    normalize() {
        this.isValidated = false;
        this.isForced = false;
        if (!this._checkMandatoryFields()) {
            this.showMessage = true;
            return;
        }
        let addr = {};
        let addrInputs = this.querySelectorAll('c-address-input');
        for (let i of addrInputs) {
            if (i.element) {
                addr[i.element] = i.getValue();
            }
        }
        this.normalizeService(addr);
    }

    @api
    forceAddress() {
        this.spinner = true;
        this.isValidated = false;
        this.isForced = false;
        if (!this._checkMandatoryFields()) {
            this.spinner = false;
            this.showMessage = true;
            return;
        }
        this.isForced = true;
        this.hasRendered = true;
        this.spinner = false;
        let addrNormalize = true;
        let addressForced = true;
        this.dispatchNormalize(addrNormalize,addressForced);
    }

    @api
    isValid() {
        return this.isValidated || this.isForced;
    }

    copyFromOther() {
        this.showAddressPicker = true;
    }

    handleSelection(event) {
        this.selectedAddress = event.detail.selected;
    }

    confirmAddress() {
        let addr = this.selectedAddress;
        this.setAddress(addr);
        if(addr.AddressNormalized) {
        this.isValidated = true;
            this.isReadOnly = true;
            this.disableNormalize = true;
            this.dispatchEvent(new CustomEvent('isnormalize', { detail: { 'readOnly': this.isReadOnly, 'addressForced':this.addressForced}}));
        }
        this.closeModal();
    }

    closeModal() {
        this.showAddressPicker = false;
    }

    @api
    setAddress(address) {
        this.spinner = true;
        let addrInputs = this.querySelectorAll('c-address-input');
        for (let i of addrInputs) {
            if (i.element && address[i.element]) {
                i.setValue(address[i.element]);
            }
        }
        this.spinner = false;
    }

    @api
    getAddressFields() {
        let fields = {};
        let addrInputs = this.querySelectorAll('c-address-input');
        for (let i of addrInputs) {
            if (i.fieldName) {
                fields[i.fieldName] = i.getValue();
            }
        }
        return fields;
    }

    handleChildRegister(event) {
        event.stopPropagation();
        const item = event.detail;
        const guid = item.guid;
        this.privateChildrenRecord[guid] = item;
        this.fields.push(item.field);
        item.callbacks.registerDisconnectCallback(this.handleChildUnregister);
    }

    handleChildUnregister(event) {
        const item = event.detail;
        const guid = item.guid;
        this.privateChildrenRecord[guid] = undefined;
    }

    normalizeService(addr){
        let inputs = { address: JSON.stringify(addr) };
        notCacheableCall({
            className: "AddressCnt",
            methodName: "normalize",
            input: inputs
        }).then(response => {
            if ((response.data) && (response.data.length === 0)) {
                    error(this, this.labels.invalidFields);
                    this.spinner = false;
                    return;
                }
            if ((response.data) && (response.data.length === 1)) {
                this.setAddress(response.data[0]);
                this.isValidated = true;
                this.dispatchNormalize(response.data, false);
            } else {
                this.availableAddresses = [];
                for (let x in response.data) {
                    if (response.data[x]) {
                        this.availableAddresses.push(response.data[x]);
                    }
                }
                this.showAddressPicker = true;
            }
            this.spinner = false;
        });
    }

    dispatchNormalize(addrNormalize,addressForced){
        this.dispatchEvent(new CustomEvent('isnormalize', { detail: { 'readOnly': addrNormalize, 'addressForced':addressForced}}));
    }


}