/**
 * Created by napoli on 02/10/2019.
 */
const servicePointEleCodeRegex = /^(((EMO\d{4})|594((010[1-7]\d{16})|0[2-4]0[1-6]\d{8}))(?!000)\d{3})$|^(?!(EMO|(5940[1-4])))\w*/i;
const servicePointGasCodeRegex = /^[a-z]{3}((\-|\/)?\w+)+$/i;
const regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneRegex=/^[0][0-9]{9}$/;

const checkServicePointEleCode = (code) => {
    return servicePointEleCodeRegex.test(code);
};

const checkServicePointGasCode = (code) => {
    return servicePointGasCodeRegex.test(code);
};

const checkCNP = (cnp) => {
    cnp = cnp.trim();
    let cnpArray = cnp.split('');
    let control = '279146358279'.split('');
    let result;

    if ((cnpArray.length == 13) && (!isNaN(cnp))) {
        if(cnp.startsWith('0')) {
            //error first digit zero is not allowed
            result = {
                'outCome': false,
                'errorCode': 'E-CNP001'
            };
            return result;
        }
        let month = cnp.substr(3, 2);
        if(month < 1 || month > 12) {
            result = {
                'outCome': false,
                'errorCode': 'E-CNP002'
            };
            return result;
        }
        let day = cnp.substr(5, 2);
        if(day < 1 || day > 31) {
            result = {
                'outCome': false,
                'errorCode': 'E-CNP003'
            };
            return result;
        }
        let countryZone = cnp.substr(7, 2);
        if((countryZone < 1 || countryZone > 52) && countryZone != 99) {
            result = {
                'outCome': false,
                'errorCode': 'E-CNP004'
            };
            return result;
        }
        let sum = 0;
        let cnpDigit = 0;
        let controlDigit = 0;

        for(let i = 0; i < control.length; i++){
            cnpDigit = cnpArray[i];
            controlDigit = control[i];

            sum = sum + (cnpDigit * controlDigit);
        }
        let rest = (sum % 11);
        if (((rest < 10) && (rest == cnpArray[12])) || ((rest == 10) && (cnpArray[12] == 1))) {
            result = {
                'outCome': true,
                'errorCode': null
            };
            return result;
        } else { //error checksum digit
            result = {
                'outCome': false,
                'errorCode': 'E-CNP005'
            };
            return result;
        }
    } else { //cnp not numeric or length is not 13
        result = {
            'outCome': false,
            'errorCode': 'E-CNP006'
        };
        return result;
    }
};

const checkCUI = (cui) => {
    let result;
    cui = cui.trim();
    if (cui.replace('RO', '').length > 10) { //Code too long
        result = {
            'outCome': false,
            'errorCode': 'E-CUI001'
        };
        return result;
    }
    let cif = cui.replace('RO', '');
    if (!isNaN(cif)) {
        let cheia = '753217532';
        let rest;
        let cheiaArray = cheia.split('').reverse();
        let cuiArray = cif.split('').reverse();
        let controlul = cuiArray[0];
        cuiArray = cuiArray.slice(1, cif.length);
        let suma = 0;
        for (let i = 0; i < cuiArray.length; i++) {
            suma = suma + (cuiArray[i] * cheiaArray[i]);
        }
        if ((suma * 10) % 11 == 10) {
            rest = 0;
        } else {
            rest = ((suma * 10) % 11);
        }
        if (rest == controlul) { //Correct code
            result = {
                'outCome': true,
                'errorCode': ''
            };
            return result;
        } else { //Incorrect code
            result = {
                'outCome': false,
                'errorCode': 'E-CUI002'
            };
            return result;
        }
    } else { //CUI must be numeric
        result = {
            'outCome': false,
            'errorCode': 'E-CUI003'
        };
        return result;
    }
};

const checkONRC = (onrc) => {
    onrc = onrc.trim();

    let matchResult= onrc.match('^(J|F|C)\\d{2}\\/\\d+\\/\\d{4}$');


    let  result = {
        'outCome': (matchResult != null)
    };
    return result;

};

const checkMail = (mail) => {
    mail = mail.trim();

    let matchResult= regExpEmailformat.test(mail);

    let  result = {
        'outCome': matchResult
    };
    return result;

};

const checkPhone = (phone) => {
    phone = phone.trim();
    let matchResult= phoneRegex.test(phone);

    let  result = {
        'outCome': matchResult
    };
    return result;

};

export {checkServicePointEleCode, checkServicePointGasCode, checkCNP, checkCUI, checkONRC,checkMail,checkPhone};