/* eslint-disable no-else-return */
/* eslint-disable @lwc/lwc/no-async-operation */
/* eslint-disable no-unused-vars */
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
/* eslint-disable eqeqeq */
import { LightningElement, api, track } from 'lwc';
import CustomLookupSearch from '@salesforce/label/c.CustomLookupSearch';
import CustomLookupMissingInputValue from '@salesforce/label/c.CustomLookupMissingInputValue';
import CustomLookupCompleteThisField from '@salesforce/label/c.CustomLookupCompleteThisField';
import fetchLookupValues from '@salesforce/apex/MRO_LC_CustomLookup.fetchLookupValues';
import initialize from '@salesforce/apex/MRO_LC_CustomLookup.initialize';
import {error} from 'c/notificationSvc';
import {labels, formatLabel} from 'c/mroLabels';

// noinspection JSUnusedGlobalSymbols
export default class MroCustomLookup extends LightningElement {

    @api required = false;
    @api label;
    @api iconName;
    @api objectApiName;
    @api queryFields;
    @api queryWhere;
    @api filterSearchKeywordField;
    @api filterFields;
    @track selectedRecord = null;
    @api secondaryDisplayField;
    @api primaryDisplayField;
    @api recordNumberLimit;
    @api initialSelectedId = '';
    @api disabled = false;

    /** List of records founded */
    @track listOfSearchRecords = [];

    /** Keyword typed */
    @track searchKeyword = '';

    /** Message for no results founded */
    @track message;

    /** Boolean value indicating when showing the message */
    @track showMessage;

    /** Error resulting from the search for results */
    @track error;

    /** Boolean value indicating when the search is being performed */
    @track loading;

    /** Label retrieved dynamically from custom labels */
    @track searchLabel = CustomLookupSearch;

    /** Label retrieved dynamically from custom labels */
    @track invalidValueErrorMessage = CustomLookupMissingInputValue;

    /** Label retrieved dynamically from custom labels */
    @track missingValueErrorMessage = CustomLookupCompleteThisField;

    /** Boolean value indicating when to show the results list */
    @track showResultsList = false;

    /** Attribute that specifies the opening of the results list */
    @track ariaExpanded = false;

    /** Attribute that specifies if there is an error on input */
    @track ariaInvalid = false;

    /** Attribute that specifies whether you are in the results list with the mouse */
    @track mouseOverResultsList = false;

    /** Attribute used for to set the lightning-icon class */
    @track iconClass = "slds-m-top--x-small";

    labels = labels;

    connectedCallback () {
        if (this.initialSelectedId !== '') {
            initialize ({initialSelectedId: this.initialSelectedId,
                         objectApiName: this.objectApiName,
                         filterSearchKeywordField: this.filterSearchKeywordField,
                         primaryDisplayField: this.primaryDisplayField,
                         queryFields: this.queryFields,
                         queryWhere: this.queryWhere,
                         filterFields: this.filterFields,
                         secondaryDisplayField: this.secondaryDisplayField})
               .then(result => {
                   //console.log('result: ', result);
                   if (!result.error) {
                       //console.log('CustomLookup.connectedCallback - query:', result.query);
                       let sObject = result.initialRecord;
                       sObject.searchDisplayName = sObject[this.filterSearchKeywordField];
                       if (this.showPrimaryDisplayField) {

                           sObject.searchDisplayName = sObject[this.primaryDisplayField];
                           sObject.primaryDisplayField = sObject[this.primaryDisplayField];
                       }
                       this.selectedRecord = sObject;
                       console.log('initialRecord', JSON.stringify(result.initialRecord));
                   } else {
                       error(this, result.errorMessage);
                       console.log('CustomLookup.connectedCallback - errorStackTraceString', result.errorStackTraceString);
                   }
               })
               .catch(errorMsg => {
                    error(this, errorMsg);
                    this.error = errorMsg;
               })
       }
    }
    /**
     *  Method that catches the keypress event
     */
    keyPressController (event) {
        event.preventDefault();
        this.template.querySelector(".slds-spinner").classList.remove("slds-hide");
        this.searchKeyword = event.target.value;
        this.mouseOverResultsList = false;
        this.listOfSearchRecords = [];
        if (this.searchKeyword.length > 0) {
            this.loading = true;
            this.showResultsList = true;
            this.ariaExpanded = true;
            setTimeout(() => {
                this.search();
            }, 1000);
        } else {
            this.loading = false;
            this.showResultsList = false;
            this.selectedRecord = null;
            this.showMessage = false;
            this.template.querySelector(".slds-spinner").classList.add("slds-hide");
        }
    }

    /**
     *   Method that invoke the fetchLookupValues of CustomLookupCnt which searches for results
     */
    search () {
        if (!this.searchKeyword || !this.searchKeyword.length) {
            this.loading = false;
            let spinner = this.template.querySelector(".slds-spinner");
            if (spinner) {
                spinner.classList.add("slds-hide");
            }
            return;
        }
        fetchLookupValues({
            searchKeyword: this.searchKeyword,
            objectApiName: this.objectApiName,
            queryFields: this.queryFields,
            queryWhere: this.queryWhere,
            filterSearchKeywordField: this.filterSearchKeywordField,
            filterFields: this.filterFields,
            primaryDisplayField: this.primaryDisplayField,
            secondaryDisplayField: this.secondaryDisplayField,
            recordNumberLimit: this.recordNumberLimit
        })
            .then(result => {
                if (!result.error) {
                    //console.log('CustomLookup.search - query:', result.query);
                    if (result.returnList.length === 0) {
                        this.showMessage = true;
                        this.message = formatLabel(labels.notFound, this.searchKeyword);
                    } else {
                        this.showMessage = false;
                        this.message = '';
                    }
                    this.listOfSearchRecords = [];
                    for(let i = 0; i < result.returnList.length; i++) {
                        let sObject = result.returnList[i];
                        //console.log('Result item', sObject);
                        sObject.searchDisplayName = sObject[this.filterSearchKeywordField];
                        if (this.showPrimaryDisplayField()) {
                            sObject.searchDisplayName = sObject[this.primaryDisplayField];
                            sObject.primaryDisplayField = sObject[this.primaryDisplayField];
                        }
                        sObject.secondaryDisplayField = sObject[this.secondaryDisplayField];
                        //console.log('CustomLookup.search - sObject:', JSON.stringify(sObject));
                        this.listOfSearchRecords.push(sObject);
                    }
                } else {
                    console.log('CustomLookup.search - errorMessage', result.errorMessage);
                    console.log('CustomLookup.search - errorStackTraceString', result.errorStackTraceString);
                    this.showMessage = true;
                }
            })
            .catch (error => {
                console.log('CustomLookup.search - Enter catch');
                console.log('CustomLookup.search - Error: ', error);
                this.error = error;
                this.message = formatLabel(labels.notFound, this.searchKeyword);
            })
            .finally (() => {
                this.loading = false;
                this.template.querySelector(".slds-spinner").classList.add("slds-hide");
            });
    }

    /**
     *   Method that catches the selection event of a result
     */
    handleSelectionEvent (event) {
        event.preventDefault();
        this.selectedRecord = event.detail;
        this.showResultsList = false;
        this.ariaExpanded = false;
        if (this.showPrimaryDisplayField) {
            this.searchKeyWord = this.selectedRecord[this.primaryDisplayField];
        } else {
            this.searchKeyWord = this.selectedRecord[this.filterSearchKeywordField];
        }
        //console.log('CustomLookup.handleSelectionEvent - event: ', JSON.stringify(event));
        let newEvent = new CustomEvent ('select', {detail: this.selectedRecord});
        this.dispatchEvent(newEvent);
    }

    /**
     *   Method that catches the deselecting event of a result
     */
    handleRemoveButton (event) {
        event.preventDefault();
        this.selectedRecord = null;
        this.showResultsList = false;
        this.ariaExpanded = false;
        if (this.required == true) {
            this.ariaInvalid = true;
        } else {
            this.ariaInvalid = false;
        }
        this.mouseOverResultsList = false;
        this.searchKeyword = '';
        this.listOfSearchRecords = [];
        this.handleOnBlurInput();
        let newEvent = new CustomEvent('unselect', {detail: null});
        this.dispatchEvent(newEvent);
    }

    @api
    handleEmptyField () {
        this.selectedRecord = null;
        this.searchKeyword = '';
    }

    /**
     * This method set the fourth div class for the opening of results list
     */
    setFocusIn (event) {
        this.template.querySelector(".slds-dropdown-trigger").classList.add("slds-is-open");
    }

    /**
     * This method sets the fourth div class for the closing of results list
     */
    setFocusOut (event) {
        this.template.querySelector(".slds-dropdown-trigger").classList.remove("slds-is-open");
    }

    /**
     * This method sets the background color of the field when hovering over it with the mouse
     */
    setBackgroundGrey (event) {
        this.template.querySelector("input").style.background = "#f3f2f2";
    }

    /**
     * This method restores the background color of the field when exiting with the mouse from the field
     */
    setBackgroundWhite (event) {
        this.template.querySelector("input").style.background = "white";
    }

    /**
     * This method controls the behaviour of the field when it lose the focus
     */
    handleOnBlurInput (event) {
        console.log('CustomLookup - handleOnBlurInput ');
        let input = this.template.querySelector(".slds-input-has-icon").classList;
        let span = this.template.querySelector("lightning-icon").classList;
        if (this.required){
            if (this.mouseOverResultsList) {
                if (this.searchKeyword != '') {
                    this.showResultsList = true;
                    this.template.querySelector(".slds-dropdown").classList.remove("slds-has-focus");
                    this.template.querySelector(".slds-dropdown-trigger").classList.add("slds-is-open");
                }
            } else {
                this.showResultsList = false;
                input.add("slds-has-error");
                this.iconClass = "";
                this.ariaInvalid = true;
            }
        } else {
            if (this.mouseOverResultsList) {
                if (this.searchKeyword !== '') {
                    this.showResultsList = true;
                    this.template.querySelector(".slds-dropdown").classList.remove("slds-has-focus");
                    this.template.querySelector(".slds-dropdown-trigger").classList.add("slds-is-open");
                }
            } else {
                this.showResultsList = false;
                this.searchKeyword = '';
            }
        }
    }

    /**
     * This method controls the behaviour of the field when it is focused
     */
    handleOnClickInput (event) {
        this.template.querySelector(".slds-input-has-icon").classList.remove("slds-has-error");
        if (!this.template.querySelector("lightning-icon").classList.contains("slds-m-top--small")) {
            this.iconClass = "slds-m-top--x-small";
        }
        this.ariaInvalid = false;
        if (this.searchKeyword !== '') {
            this.showResultsList = true;
        }
    }

    /**
     * This method returns the error message of the field when it has an invalid value
     */
    get errorMessage () {
        if (this.searchKeyword === '' && this.ariaInvalid) {
            return this.missingValueErrorMessage;
        } else if (this.ariaInvalid) {
            return this.invalidValueErrorMessage;
        } else {
            return '';
        }
    }

    handleOnMouseOverResultsList (event) {
        this.showResultsList = true;
        this.mouseOverResultsList = true;
    }

    handleOnMouseOutResultsList (event) {
        this.mouseOverResultsList = false;
    }

    handleOnBlur (event) {
        console.log('CustomLookup - handleOnBlur ');
        this.showResultsList = false;
        this.template.querySelector(".slds-dropdown").classList.remove("slds-has-focus");
        this.template.querySelector(".slds-dropdown-trigger").classList.remove("slds-is-open");
    }

    handleOnClick (event) {
        console.log('CustomLookup - handleOnClick ');
    }

    handleOnFocus (event) {
        console.log('CustomLookup - handleOnFocus');
    }

    showPrimaryDisplayField () {
        return this.primaryDisplayField !== '';
    }

}