import { LightningElement, api, track } from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import { labels } from 'c/labels';

export default class SupplyList extends LightningElement {
    labels = labels;

    @api recordId;
    @api flexipageRegionWidth;
    @track supplies;
    @track loading = true;
    @track companyDivisionEnforced;

    connectedCallback () {
        this.getSupplies();
    }

    getSupplies () {
        let inputs = { accountId: this.recordId };
        notCacheableCall({
            className: 'MRO_LC_Supply',
            methodName: 'getSuppliesByAccountId',
            input: inputs
        })
            .then((response) => {
                if (response.data.error) {
                    console.error('* Error ',response.data.errorMsg);
                    error(this, response.data.errorMsg);
                    return;
                }
                this.supplies = response.data.supplies;
            })
            .catch((err) => {
                console.error('* Error ',err);
                error(this, JSON.stringify(err));
            });
    }

    @track showBody = false;

    @track supplyList = [];
    electricOn = false;
    gasOn = false;
    vasOn = false;

    @track showList;

    get colStyle () {
        if (this.flexipageRegionWidth === 'SMALL') {
            return 'slds-col slds-size_1-of-2 slds-p-top_small';
        }
        return 'slds-col slds-size_1-of-3 slds-p-top_small';
    }

    get electricTotalSize () {
        let filtered = this.supplies.filter(function (supply) {
            return supply.RecordType.DeveloperName === 'Electric';
        });
        return filtered.length;
    }

    get electricActiveSize () {
        let filtered = this.supplies.filter(function (supply) {
            return supply.RecordType.DeveloperName === 'Electric' && ((supply.Status__c === 'Active') || (supply.Status__c === 'Terminating'));
        });
        return filtered.length;
    }

    get vasTotalSize () {
        let filtered = this.supplies.filter(function (supply) {
            return supply.RecordType.DeveloperName === 'Service';
        });
        return filtered.length;
    }

    get vasActiveSize () {
        let filtered = this.supplies.filter(function (supply) {
            return supply.RecordType.DeveloperName === 'Service' && ((supply.Status__c === 'Active') || (supply.Status__c === 'Terminating'));
        });
        return filtered.length;
    }

    get gasTotalSize () {
        let filtered = this.supplies.filter(function (supply) {
            return supply.RecordType.DeveloperName === 'Gas';
        });
        return filtered.length;
    }

    get gasActiveSize () {
        let filtered = this.supplies.filter(function (supply) {
            return supply.RecordType.DeveloperName === 'Gas' && ((supply.Status__c === 'Active') || (supply.Status__c === 'Terminating'));
        });
        return filtered.length;
    }

    get svgURLEle () {
        return SVG_URL + '/images/Electric.svg#electric';
    }
    get svgURLGas () {
        return SVG_URL + '/images/Gas.svg#gas';
    }
    get svgURLVas () {
        return SVG_URL + '/images/Service.svg#service';
    }

    toggleElectric () {
        if (this.electricOn) {
            let filtered = this.supplyList.filter(function (supply) {
                return supply.RecordType.DeveloperName !== 'Electric';
            });
            this.supplyList = filtered;
        }
        else {
            if (this.supplies) {
                let filtered = this.supplies.filter(function (supply) {
                    return supply.RecordType.DeveloperName === 'Electric';
                });
                this.supplyList = this.supplyList.concat(filtered);
            }
        }
        this.electricOn = !this.electricOn;
        this.showList = this.supplyList.length > 0;
    }

    toggleGas () {
        if (this.gasOn) {
            let filtered = this.supplyList.filter(function (supply) {
                return supply.RecordType.DeveloperName !== 'Gas';
            });
            this.supplyList = filtered;
        }
        else {
            if (this.supplies) {
                let filtered = this.supplies.filter(function (supply) {
                    return supply.RecordType.DeveloperName === 'Gas';
                });
                this.supplyList = this.supplyList.concat(filtered);
            }
        }
        this.gasOn = !this.gasOn;
        this.showList = this.supplyList.length > 0;
    }

    toggleVas () {
        if (this.vasOn) {
            let filtered = this.supplyList.filter(function (supply) {
                return supply.RecordType.DeveloperName !== 'Service';
            });
            this.supplyList = filtered;
        }
        else {
            if (this.supplies) {
                let filtered = this.supplies.filter(function (supply) {
                    return supply.RecordType.DeveloperName === 'Service';
                });
                this.supplyList = this.supplyList.concat(filtered);
            }
        }
        this.vasOn = !this.vasOn;
        this.showList = this.supplyList.length > 0;
    }
}