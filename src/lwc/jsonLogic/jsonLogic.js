/**
 * (ES6 converted) JSONLogic library
 * @see http://jsonlogic.com/
 */

/**
 * Return an array that contains no duplicates (original not modified)
 * @param  {array} array   Original reference array
 * @return {array}         New array with no duplicates
 */
const arrayUnique = function(array) {
    const a = [];
    for (let i = 0, l = array.length; i < l; i++) {
        if (a.indexOf(array[i]) === -1) {
            a.push(array[i]);
        }
    }
    return a;
};

const operations = {
    "==": (a, b) => {
        return a == b;
    },
    "===": (a, b) => {
        return a === b;
    },
    "!=": (a, b) => {
        return a != b;
    },
    "!==": (a, b) => {
        return a !== b;
    },
    ">": (a, b) => {
        return a > b;
    },
    ">=": (a, b) => {
        return a >= b;
    },
    "<": (a, b, c) => {
        return (c === undefined) ? a < b : (a < b) && (b < c);
    },
    "<=": (a, b, c) => {
        return (c === undefined) ? a <= b : (a <= b) && (b <= c);
    },
    "!!": (a) => {
        return JsonLogic.truthy(a);
    },
    "!": (a) => {
        return JsonLogic.truthy(a);
    },
    "%": (a, b) => {
        return a % b;
    },
    "log": (a) => {
        return a;
    },
    "in": (a, b) => {
        if (!b || typeof b.indexOf === "undefined") return false;
        return (b.indexOf(a) !== -1);
    },
    "cat": () => {
        return Array.prototype.join.call(arguments, "");
    },
    "substr": (source, start, end) => {
        if (end < 0) {
            // JavaScript doesn't support negative end, this emulates PHP behavior
            const temp = String(source).substr(start);
            return temp.substr(0, temp.length + end);
        }
        return String(source).substr(start, end);
    },
    "+": () => {
        return Array.prototype.reduce.call(arguments, function (a, b) {
            return parseFloat(a) + parseFloat(b);
        }, 0);
    },
    "*": () => {
        return Array.prototype.reduce.call(arguments, function (a, b) {
            return parseFloat(a) * parseFloat(b);
        });
    },
    "-": (a, b) => {
        if (b === undefined) {
            return -a;
        } else {
            return a - b;
        }
    },
    "/": (a, b) => {
        return a / b;
    },
    "min": () => {
        return Math.min.apply(this, arguments);
    },
    "max": () => {
        return Math.max.apply(this, arguments);
    },
    "merge": () => {
        return Array.prototype.reduce.call(arguments, function (a, b) {
            return a.concat(b);
        }, []);
    },
    "var": function(a, b) {
        const not_found = (b === undefined) ? null : b;
        let data = this;
        if (typeof a === "undefined" || a === "" || a === null) {
            return data;
        }
        const sub_props = String(a).split(".");
        for (let i = 0; i < sub_props.length; i++) {
            if (data === null) {
                return not_found;
            }
            // Descending into data
            data = data[sub_props[i]];
            if (data === undefined) {
                return not_found;
            }
        }
        return data;
    },
    "missing": () => {
        /*
        Missing can receive many keys as many arguments, like {"missing:[1,2]}
        Missing can also receive *one* argument that is an array of keys,
        which typically happens if it's actually acting on the output of another command
        (like 'if' or 'merge')
        */

        const missing = [];
        const keys = Array.isArray(arguments[0]) ? arguments[0] : arguments;

        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            const value = JsonLogic.apply({"var": key}, this);
            if (value === null || value === "") {
                missing.push(key);
            }
        }

        return missing;
    },
    "missing_some": (need_count, options) => {
        // missing_some takes two arguments, how many (minimum) items must be present, and an array of keys (just like 'missing') to check for presence.
        const are_missing = JsonLogic.apply({"missing": options}, this);

        if (options.length - are_missing.length >= need_count) {
            return [];
        } else {
            return are_missing;
        }
    },
    "method": (obj, method, args) => {
        return obj[method].apply(obj, args);
    },
};

export default class JsonLogic {

    static is_logic(logic) {
        return (
            typeof logic === "object" && // An object
            logic !== null && // but not null
            !Array.isArray(logic) && // and not an array
            Object.keys(logic).length === 1 // with exactly one key
        );
    }

    /*
    This helper will defer to the JsonLogic spec as a tie-breaker when different language interpreters define different behavior for the truthiness of primitives.  E.g., PHP considers empty arrays to be falsy, but Javascript considers them to be truthy. JsonLogic, as an ecosystem, needs one consistent answer.

    Spec and rationale here: http://jsonlogic.com/truthy
    */
    static truthy(value) {
        if (Array.isArray(value) && value.length === 0) {
            return false;
        }
        return !!value;
    }


    static get_operator(logic) {
        return Object.keys(logic)[0];
    }

    static get_values(logic) {
        return logic[JsonLogic.get_operator(logic)];
    }

    static apply(logic, data) {
        // Does this array contain logic? Only one way to find out.
        if (Array.isArray(logic)) {
            return logic.map((l) => {
                return JsonLogic.apply(l, data);
            });
        }
        // You've recursed to a primitive, stop!
        if (!JsonLogic.is_logic(logic)) {
            return logic;
        }

        data = data || {};

        const op = JsonLogic.get_operator(logic);
        let values = logic[op];
        let i;
        let current;
        let scopedLogic, scopedData, filtered, initial;

        // easy syntax for unary operators, like {"var" : "x"} instead of strict {"var" : ["x"]}
        if (!Array.isArray(values)) {
            values = [values];
        }

        // 'if', 'and', and 'or' violate the normal rule of depth-first calculating consequents, let each manage recursion as needed.
        if (op === "if" || op === "?:") {
            /* 'if' should be called with a odd number of parameters, 3 or greater
            This works on the pattern:
            if( 0 ){ 1 }else{ 2 };
            if( 0 ){ 1 }else if( 2 ){ 3 }else{ 4 };
            if( 0 ){ 1 }else if( 2 ){ 3 }else if( 4 ){ 5 }else{ 6 };

            The implementation is:
            For pairs of values (0,1 then 2,3 then 4,5 etc)
            If the first evaluates truthy, evaluate and return the second
            If the first evaluates falsy, jump to the next pair (e.g, 0,1 to 2,3)
            given one parameter, evaluate and return it. (it's an Else and all the If/ElseIf were false)
            given 0 parameters, return NULL (not great practice, but there was no Else)
            */
            for (i = 0; i < values.length - 1; i += 2) {
                if (JsonLogic.truthy(JsonLogic.apply(values[i], data))) {
                    return JsonLogic.apply(values[i + 1], data);
                }
            }
            if (values.length === i + 1) return JsonLogic.apply(values[i], data);
            return null;
        } else if (op === "and") { // Return first falsy, or last
            for (i = 0; i < values.length; i += 1) {
                current = JsonLogic.apply(values[i], data);
                if (!JsonLogic.truthy(current)) {
                    return current;
                }
            }
            return current; // Last
        } else if (op === "or") {// Return first truthy, or last
            for (i = 0; i < values.length; i += 1) {
                current = JsonLogic.apply(values[i], data);
                if (JsonLogic.truthy(current)) {
                    return current;
                }
            }
            return current; // Last


        } else if (op === 'filter') {
            scopedData = JsonLogic.apply(values[0], data);
            scopedLogic = values[1];

            if (!Array.isArray(scopedData)) {
                return [];
            }
            // Return only the elements from the array in the first argument,
            // that return truthy when passed to the logic in the second argument.
            // For parity with JavaScript, reindex the returned array
            return scopedData.filter((datum) => {
                return JsonLogic.truthy(JsonLogic.apply(scopedLogic, datum));
            });
        } else if (op === 'map') {
            scopedData = JsonLogic.apply(values[0], data);
            scopedLogic = values[1];

            if (!Array.isArray(scopedData)) {
                return [];
            }

            return scopedData.map((datum) => {
                return JsonLogic.apply(scopedLogic, datum);
            });

        } else if (op === 'reduce') {
            scopedData = JsonLogic.apply(values[0], data);
            scopedLogic = values[1];
            initial = typeof values[2] !== 'undefined' ? values[2] : null;

            if (!Array.isArray(scopedData)) {
                return initial;
            }

            return scopedData.reduce(
                (accumulator, current) => {
                    return JsonLogic.apply(
                        scopedLogic,
                        {'current': current, 'accumulator': accumulator}
                    );
                },
                initial
            );

        } else if (op === "all") {
            scopedData = JsonLogic.apply(values[0], data);
            scopedLogic = values[1];
            // All of an empty set is false. Note, some and none have correct fallback after the for loop
            if (!scopedData.length) {
                return false;
            }
            for (i = 0; i < scopedData.length; i += 1) {
                if (!JsonLogic.truthy(JsonLogic.apply(scopedLogic, scopedData[i]))) {
                    return false; // First falsy, short circuit
                }
            }
            return true; // All were truthy
        } else if (op === "none") {
            filtered = JsonLogic.apply({'filter': values}, data);
            return filtered.length === 0;

        } else if (op === "some") {
            filtered = JsonLogic.apply({'filter': values}, data);
            return filtered.length > 0;
        }

        // Everyone else gets immediate depth-first recursion
        values = values.map((val) => {
            return JsonLogic.apply(val, data);
        });


        // The operation is called with "data" bound to its "this" and "values" passed as arguments.
        // Structured commands like % or > can name formal arguments while flexible commands (like missing or merge) can operate on the pseudo-array arguments
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments
        if (typeof operations[op] === "function") {
            return operations[op].apply(data, values);
        } else if (op.indexOf(".") > 0) { // Contains a dot, and not in the 0th position
            const sub_ops = String(op).split(".");
            let operation = operations;
            for (i = 0; i < sub_ops.length; i++) {
                // Descending into operations
                operation = operation[sub_ops[i]];
                if (operation === undefined) {
                    throw new Error("Unrecognized operation " + op +
                        " (failed at " + sub_ops.slice(0, i + 1).join(".") + ")");
                }
            }

            return operation.apply(data, values);
        }

        throw new Error("Unrecognized operation " + op);
    }

    static uses_data(logic) {
        const collection = [];

        if (JsonLogic.is_logic(logic)) {
            const op = JsonLogic.get_operator(logic);
            let values = logic[op];

            if (!Array.isArray(values)) {
                values = [values];
            }

            if (op === "var") {
                // This doesn't cover the case where the arg to var is itself a rule.
                collection.push(values[0]);
            } else {
                // Recursion!
                values.map((val) => {
                    collection.push.apply(collection, JsonLogic.uses_data(val));
                });
            }
        }

        return arrayUnique(collection);
    };

    static add_operation(name, code) {
        operations[name] = code;
    };

    static rm_operation(name) {
        delete operations[name];
    };

    static rule_like(rule, pattern) {
        if (pattern === rule) {
            return true;
        } // TODO : Deep object equivalency?
        if (pattern === "@") {
            return true;
        } // Wildcard!
        if (pattern === "number") {
            return (typeof rule === "number");
        }
        if (pattern === "string") {
            return (typeof rule === "string");
        }
        if (pattern === "array") {
            // !logic test might be superfluous in JavaScript
            return Array.isArray(rule) && !JsonLogic.is_logic(rule);
        }

        if (JsonLogic.is_logic(pattern)) {
            if (JsonLogic.is_logic(rule)) {
                const pattern_op = JsonLogic.get_operator(pattern);
                const rule_op = JsonLogic.get_operator(rule);

                if (pattern_op === "@" || pattern_op === rule_op) {
                    // echo "\nOperators match, go deeper\n";
                    return JsonLogic.rule_like(
                        JsonLogic.get_values(rule, false),
                        JsonLogic.get_values(pattern, false)
                    );
                }
            }
            return false; // pattern is logic, rule isn't, can't be eq
        }

        if (Array.isArray(pattern)) {
            if (Array.isArray(rule)) {
                if (pattern.length !== rule.length) {
                    return false;
                }
                /*
                  Note, array order MATTERS, because we're using this array test logic to consider arguments, where order can matter. (e.g., + is commutative, but '-' or 'if' or 'var' are NOT)
                */
                for (let i = 0; i < pattern.length; i += 1) {
                    // If any fail, we fail
                    if (!JsonLogic.rule_like(rule[i], pattern[i])) {
                        return false;
                    }
                }
                return true; // If they *all* passed, we pass
            } else {
                return false; // Pattern is array, rule isn't
            }
        }

        // Not logic, not array, not a === match for rule.
        return false;
    }
}