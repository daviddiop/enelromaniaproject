/**
 * Created by Bouba on 03/10/2019.
 */

import {api, LightningElement, track} from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error, success} from 'c/notificationSvc';
import {labels} from 'c/labels';

export default class MroCaseTile extends LightningElement {
   // FF Self Reading #WP3-P2
    RECORD_TYPE_EE = ['Termination_ELE', 'SwitchOut_ELE', 'TechnicalDataChange_ELE', 'MeterChangeEle', 'Connection_ELE', 'ContractualDataChange_ELE', 'MeterCheckEle', 'Meter_Reading_Ele','SelfReading_ELE'];
    RECORD_TYPE_GAS = ['Termination_GAS', 'SwitchOut_GAS', 'TechnicalDataChange_GAS', 'MeterChangeGas', 'Connection_GAS', 'ContractualDataChange_GAS', 'MeterCheckGas', 'Meter_Reading_Gas','SelfReading_GAS'];
    //FF Self Reading #WP3-P2
    RECORD_TYPE_SERVICE = ['Termination_SRV'];
    RECORD_TYPE_BP = ['BillingProfileChange'];
    RECORD_TYPE_MERGE = ['ContractMerge', 'RefundRequest', 'BailmentManagement', 'ANRECompensation'];

    labels = labels;
    @api
    get caseId() {
        return this._caseId;
    }
    set caseId(value) {
        this._caseId = value;
    }

    @api
    get caseList() {
        return this._caseList;
    }
    set caseList(value) {
        this._caseList = value;
    }

    @api dossierId;
    @api accountId;
    @api isCaseModalByAddressForm = false;
    @api isTerminationWizard = false;
    @api maxDate;
    @api isConnectionWizard = false;
    @api enableClone = false;
    @api isNonDisconnectableWizard = false;
    @api isTaxChangeWizard = false;
    @api isMeterChange = false ;
    @api isSwitchOut = false ;
    @api isPowerVoltage = false ;
    @api isPowerVoltageWizard = false ;
    @api isAuthorityCompensation = false;
    @api isInformationRequest = false;
    @api hideEditIcon = false;
    @api isRow = false;
    @api disabled = false;
    @api selectCaseFields = [];
    @api selectCaseTileFields = [];
    @api readOnlyAddress = false;
    @api addressForced = false;
    @api recordTypeCase;
    @api tileTitle;
    @api recordTypeId;
    @api defaultEffDate;
    @api originSelected ;
    @api channelSelected ;
    @api isDecoReco = false;
    @api connectionType = "";
    @api supplyType;
    @api supplyId;

    @track defaultTileTitle;
    @track caseFields = [];
    @track openConfirmModal;
    @track openEditModal = false;
    @track listRecordType = [];
    @track isGas;
    @track isElectric;
    @track isService;
    @track isBillingProfile;
    @track isContractMerge;
    @track isNormalize;
    @track caseAddress;
    @track isClone = false;
    @track _caseId;
    _caseList;

    connectedCallback() {

        if (this.isInformationRequest) {
            this.getSupplyType();
        }

        this.defaultTileTitle = this.tileTitle ? this.tileTitle : this.labels.caseEdit;
        let inputs = {recordId: this._caseId};
        notCacheableCall({
            className: 'MRO_LC_Supply',
            methodName: 'getDeveloperName',
            input: inputs
        })
            .then(response => {
                let recordType = response.data.listRecordType[0].RecordType.DeveloperName;
                this.isElectric = this.RECORD_TYPE_EE.includes(recordType);
                this.isGas = this.RECORD_TYPE_GAS.includes(recordType);
                this.isService = this.RECORD_TYPE_SERVICE.includes(recordType);
                this.isBillingProfile = this.RECORD_TYPE_BP.includes(recordType);
                this.isContractMerge = this.RECORD_TYPE_MERGE.includes(recordType);

                if ((!this.isRow) && (this.selectCaseTileFields.length > 0)) {
                    this.selectCaseTileFields = this.selectCaseTileFields.filter(function (el) {
                        return el !== 'Supply__c';
                    });
                }

                if (recordType.includes('TechnicalData')) {
                    if (this.isGas) {
                        this.checkGasFields();
                    } else if (this.isElectric) {
                        this.checkEleFields();
                    }
                } else {
                    this.caseFields = this.selectCaseFields;
                }

                this.error = undefined;
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    getSupplyType() {
        notCacheableCall({
            className: 'MRO_LC_Supply',
            methodName: 'getSupplyDeveloperNameById',
            input: {
                supplyId: this.supplyId
            }
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                this.supplyType = response.data.supplyRecordType;
                this.setInformationRequestSupplyIcon(this.supplyType);
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
    }

    setInformationRequestSupplyIcon(supplyType) {
            switch (supplyType) {
                case 'Electric': {
                    console.log('Electric');
                    this.RECORD_TYPE_EE.push('InformationRequest');
                    break;
                }
                case 'Service': {
                    console.log('Service');
                    this.RECORD_TYPE_SERVICE.push('InformationRequest');
                    break;
                }
                case 'Gas': {
                    console.log('Gas');
                    this.RECORD_TYPE_GAS.push('InformationRequest');
                    break;
                }
            }
    }

    initCaseAddress() {
        let inputs = {caseId: this.caseId};
        notCacheableCall({
            className: 'MRO_LC_Case',
            methodName: 'getCaseAddressField',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let addr = {};
                addr = response.data.caseAddress;
                this.caseAddress = addr;
                console.log('this.caseAddress: ' + JSON.stringify(this.caseAddress));
                if (addr.addressNormalized) {
                    this.readOnlyAddress = true;
                    this.addressForced = false;

                } else {
                    this.readOnlyAddress = false;
                }

                this.spinner = false;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
    }

    get svgURLEle() {
        return SVG_URL + '/images/Electric.svg#electric';
    }

    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }

    get svgURLService() {
        return SVG_URL + '/images/Service.svg#service';
    }

    get svgURLBPChange() {
        return SVG_URL + '/images/BPChange.svg#bpChange';
    }

    get svgURLContractMerge() {
        return SVG_URL + '/images/ContractMerge.svg#contractmerge';
    }

    closeModal() {
        this.openEditModal = false;
        this.openConfirmModal = false;
    }

    deleteCaseFromTile() {
        let inputs = {recordId: this.caseId};
        notCacheableCall({
            className: 'MRO_LC_Supply',
            methodName: 'deleteCase',
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }
                this.openConfirmModal = false;
                this.openEditModal = false;
                const deleteEvent = new CustomEvent('delete', {
                    detail: {
                        deleteRecord: this.caseId
                    }
                });
                this.dispatchEvent(deleteEvent);
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    editCaseIcon() {
        this.openEditModal = true;
        if (this.isCaseModalByAddressForm) {
            this.initCaseAddress();
        }
        //start  add by david
        if (this.isTaxChangeWizard || this.isTerminationWizard /*|| this.isSwitchOut*/) {
            //let inputCmp = this.template.querySelector('[data-id="subventionRecord"]');
            this.openEditModal = false;
            const editEvent = new CustomEvent('edit', {
                detail: {
                    recordCaseEdit: this._caseId
                }
            });
            this.dispatchEvent(editEvent);
        }
        // end add by david

    }

    editCopyIcon() {
        this.isClone = true;
        if (this.isCaseModalByAddressForm) {
            this.initCaseAddress();
        }

        this.openEditModal = true;
    }

    handleSuccessCase() {
        if(this.isClone)
        {
            console.log("Successfully created the clone");
            const cloneEvent = new CustomEvent('cloned');
            this.dispatchEvent(cloneEvent);
            this.isClone = false;
        }
        if (this.isAuthorityCompensation){
            this.dispatchEvent(new CustomEvent('editsuccess'));
        }
    }

    deleteCaseIcon() {
        this.openConfirmModal = true;
    }

    handleClose(event) {
        this.openEditModal = event.target.closeModal;
        if(this.isClone)
        {
            this.isClone = false;
        }
        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }

    handleSuccess(event){
        const successEvent = new CustomEvent('casesuccess');
        this.dispatchEvent(successEvent);
    }

    checkGasFields() {
        let myCaseFields = [...this.selectCaseFields];
        if (myCaseFields) {
            /*if (!myCaseFields.includes('ConversionFactor__c')) {
                myCaseFields.push('ConversionFactor__c');
            }*/
            if (!myCaseFields.includes('PressureLevel__c')) {
                myCaseFields.push('PressureLevel__c');
            }
            if (!myCaseFields.includes('Pressure__c')) {
                myCaseFields.push('Pressure__c');
            }
            if (!myCaseFields.includes('FirePlacesCount__c')) {
                myCaseFields.push('FirePlacesCount__c');
            }
            if (!myCaseFields.includes('ConsumptionCategory__c')) {
                myCaseFields.push('ConsumptionCategory__c');
            }
            if (!myCaseFields.includes('FlowRate__c')) {
                myCaseFields.push('FlowRate__c');
            }

            myCaseFields = myCaseFields.filter(function (value) {
                return value !== 'AvailablePower__c' && value !== 'ContractualPower__c' && value !== 'Voltage__c' && value !== 'VoltageLevel__c' && value !== 'PowerPhase__c' && value !== 'VoltageSetting__c';
            });
            this.caseFields = myCaseFields;
        }
    }

    checkEleFields() {
        let myCaseFields = [...this.selectCaseFields];
        console.log(myCaseFields);
        if (myCaseFields) {
            if (!myCaseFields.includes('PowerPhase__c')) {
                myCaseFields.push('PowerPhase__c');
            }
            if (!myCaseFields.includes('VoltageLevel__c')) {
                myCaseFields.push('VoltageLevel__c');
            }
            if (!myCaseFields.includes('Voltage__c')) {
                myCaseFields.push('Voltage__c');
            }
            if (!myCaseFields.includes('VoltageSetting__c')) {
                myCaseFields.push('VoltageSetting__c');
            }

            myCaseFields = myCaseFields.filter(function (value) {
                return value !== 'ConversionFactor__c' && value !== 'PressureLevel__c' && value !== 'Pressure__c'
                    && value !== 'ConsumptionCategory__c'&& value !== 'FlowRate__c'&& value !== 'FirePlacesCount__c';
            });
            this.caseFields = myCaseFields;
        }
    }

    get addClassHideEditIcon() {
        if (this.hideEditIcon) {
            return 'slds-hide';
        }
        return '';
    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }

}