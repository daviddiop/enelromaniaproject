import { LightningElement, api, track } from 'lwc';
import { labels } from 'c/mroLabels';
import { error, success } from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class PrivacyChange extends LightningElement {
    labels = labels;
    @api opportunityId;
    @api dossierId;
    @api contactId;
    @api hideMarketingContact = false;
    @api isSaved = false;

    @api
    get disabled() {
        return this._disabled || this._doNotCreatePrivacyChange;
    }
    set disabled(value) {
        this._disabled = value;
    }

    @api savePrivacyChange() {
        if (this._doNotCreatePrivacyChange === true) {
            const selectedEvent = new CustomEvent('success', {
                detail: {
                    doNotCreatePrivacyChange: this._doNotCreatePrivacyChange
                }
            });
            this.dispatchEvent(selectedEvent);
        } else {
            this.createRecordPrivacyChange();
        }
    }

    @api
    get canCreatePrivacyChange() {
        return !this._doNotCreatePrivacyChange;
    }

    _disabled = false;
    _doNotCreatePrivacyChange = false;

    @track individualId;
    @track recordId;
    @track privacyChange = [];

    connectedCallback() {
        console.log('contactId'+this.contactId);
        this.getIndividualId();
    }

    getIndividualId() {
        notCacheableCall({
            className: 'MRO_LC_PrivacyChange',
            methodName: 'getIndividual',
            input: {
                'opportunityId': this.opportunityId,
                'dossierId': this.dossierId,
                'contactId': this.contactId
            }
        }).then(response => {
            if (response.data.error) {
                error(this, response.data.errorMsg);
                return;
            }

            this.individualId = response.data.individualId;
            this.privacyChange = response.data.privacyChange;
            this._doNotCreatePrivacyChange = response.data.doNotCreatePrivacyChange;
            if (this.privacyChange) {
                if (this.privacyChange.Id) {
                    this.recordId = this.privacyChange.Id;
                } else {
                    this.recordId = null;
                    this.template.querySelectorAll('lightning-input-field').forEach(inputField => {
                        inputField.value = this.privacyChange[inputField.fieldName];
                    });
                }
            }
            else if (!this._doNotCreatePrivacyChange) {
                error(this, 'Unexpected error');
                return;
            }

        }).catch(err => {
            error(this, JSON.stringify(err));
        });
    }
/*
    handleSuccess(event) {
        event.stopPropagation();
        let payload = event.detail;
         console.log('payload: ' + JSON.stringify(event.detail));
        const selectedEvent = new CustomEvent('success', {
            detail: {
                privacyChangeId: payload.id,
                dontProcess: payload.fields.HasOptedOutProcessing__c.value
            }
        });
        this.dispatchEvent(selectedEvent);
    }
    */
    handleError(event) {
        error(this, event.detail.message);
    }
    handleSuccess(event) {
          event.stopPropagation();
          let payload = event.detail;
          let dontProcess = false;
          this.template.querySelectorAll('lightning-input-field').forEach(element => {
              if (element.fieldName === "HasOptedOutProcessing__c") {
                  dontProcess = element.value;
              }
          });
          const selectedEvent = new CustomEvent('success', {
              detail: {
                  privacyChangeId: payload.id,
                  dontProcess: dontProcess,
                  doNotCreatePrivacyChange: this._doNotCreatePrivacyChange
              }
          });
          this.dispatchEvent(selectedEvent);
    }
    createRecordPrivacyChange (){
        //let fieldList = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            this.privacyChange[element.fieldName] = element.value;
        });

        let marketingChannels = [];
        if (this.privacyChange.MarketingChannel__c) {
            marketingChannels = this.privacyChange.MarketingChannel__c.split(';');
        }
        if (marketingChannels && marketingChannels.indexOf('Phone') !== -1 && !this.privacyChange.MarketingPhone__c) {
            error(this, labels.marketingPhone + ' ' + labels.required);
            return;
        }
        if (marketingChannels && (marketingChannels.indexOf('Mobile Phone') !== -1 || marketingChannels.indexOf('SMS') !== -1) && !this.privacyChange.MarketingMobilePhone__c) {
            error(this, labels.marketingMobilePhone + ' ' + labels.required);
            return;
        }
        if (marketingChannels && marketingChannels.indexOf('Email') !== -1 && !this.privacyChange.MarketingEmail__c) {
            error(this, labels.marketingEmail + ' ' + labels.required);
            return;
        }

        if (this.isSaved && (this.privacyChange.HasOptedOutSolicit__c === false && this.privacyChange.HasOptedOutPartners__c === false) && !this.privacyChange.MarketingChannel__c) {
            error(this, labels.pleaseSelectAMarketingChannel);
            return;
        }
        /*
        if((this.privacyChange) && (this.privacyChange.Dossier__c != null) && (this.individualId)){
              this.template.querySelector('lightning-record-edit-form').recordId = '';
              fieldList.Individual__c = this.individualId;
        }

        if((this.individualId)){
              fieldList.Individual__c = this.individualId;
        }
         */

        this.privacyChange.Status__c = 'Pending';
        this.privacyChange.Dossier__c = this.dossierId;
        this.privacyChange.Opportunity__c = this.opportunityId;

        this.template.querySelector('lightning-record-edit-form').submit(this.privacyChange);

    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }
}