/**
 * Created by tommasobolis on 25/11/2020.
 */

import { LightningElement, api, track } from 'lwc';
import { labels } from 'c/mroLabels';
import { error } from 'c/notificationSvc';

export default class MroServicePointPlaceholder extends LightningElement {

    label = labels;

    @api disabled = false;
    @api opportunityId;
    @api opportunityServiceItems = [];
    @api commodity;
    @api accountId;
    @api hasTrader;
    @api isNotSelectedOldTrader;
    @api isNewConnectionFlagReadOnly;
    @api title;
    @api requestType;

    @track showModal = false;

    get isCreatePlaceholderDisabled() {

        return (this.opportunityServiceItems && this.opportunityServiceItems.length >= 1) || this.disabled;
    }

    connectedCallback() {

        console.log('MroServicePointPlaceholder.connectedCallback - opportunityServiceItems',
                JSON.stringify(this.opportunityServiceItems));
    }

    get showOpportunityServiceItems() {

        let showOpportunityServiceItems = false;
        showOpportunityServiceItems = this.opportunityServiceItems &&
                this.opportunityServiceItems.length == 1 &&
                this.opportunityServiceItems[0].IsPlaceholder__c;
        console.log('MroServicePointPlaceholder.showOpportunityServiceItems', showOpportunityServiceItems);
        return showOpportunityServiceItems;
    }

    get servicePointCode() {

        let servicePointCode = 'TMP';
        servicePointCode += this.commodity === 'Gas' ? 'G00' : 'E00';

        var d = new Date();
        var yyyy = String(d.getFullYear());
        var MM = String(d.getMonth() + 1).padStart(2, '0');
        var dd = String(d.getDate()).padStart(2, '0');
        var hh = String(d.getHours()).padStart(2, '0');
        var mm = String(d.getMinutes()).padStart(2, '0');
        var ss = String(d.getSeconds()).padStart(2, '0');
        var SSS = String(d.getMilliseconds()).padStart(3, '0');

        servicePointCode += yyyy + MM + dd + hh + mm + ss + SSS;

        return servicePointCode;
    }

    createPlaceholder(event) {

        this.showModal = true;
    }

    closeModal(event) {

        this.showModal = false;
    }

    handleOsiAdd(event) {

        console.log('MroServicePointPlaceholder.handleOsiAdd - Enter');
        let osiId = event.detail.opportunityServiceItemId;
        console.log('MroServicePointPlaceholder.handleOsiAdd - osiId', osiId);
        if(osiId) {
            this.dispatchEvent(new CustomEvent('add', {
                detail: {
                    'opportunityServiceItemId': osiId
                }
            }, { bubbles: true }));
        }
        this.showModal = false;
    }

    handleOsiDelete(event) {

        console.log('MroServicePointPlaceholder.handleOsiDelete - Enter');
        let osiId = event.detail.recordId;
        console.log('MroServicePointPlaceholder.handleOsiDelete - osiId', osiId);
        if(osiId) {
            this.dispatchEvent(new CustomEvent('delete', {
                detail: {
                    'recordId': osiId
                }
            }, { bubbles: true }));
        }
    }
}