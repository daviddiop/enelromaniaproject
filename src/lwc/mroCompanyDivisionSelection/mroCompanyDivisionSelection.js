/**
 * Created by david on 22/08/2019.
 */
import {LightningElement, track, api} from "lwc";
import {NavigationMixin} from "lightning/navigation";
import Id from "@salesforce/user/Id";
import {labels} from "c/labels";
import {error, success} from "c/notificationSvc";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";

export default class CompanyDivisionSelection extends NavigationMixin(
    LightningElement
) {
    // Added for compatibility with previous version - deprecated
    @api companyDivisionOnUser;
    @api userId;
    @api isEdit;
    @api isRedirectAllowed;
    @api disabledCompaniesDivision;
    @api defaultCompanyDivision;
    @api checkCompany;
    @api hideTitle;
    //--  
    
    @api disabled = false;
    @api wizardCompanyDivisionId = "";

    @track listCompanyDivision = null;
    @track userCompanyDivisionId = "";
    @track enforced = false;

    @track companyDivisionId = "";
    @track companyDivisionName = "";

    @api showTitle = false;
    @track title = "";
    @track icon = "";

    labels = labels;

    @api
    getCompanyDivisions(){
        return this.listCompanyDivision;
    }

    @api
    reload(){
        this.loadCompanyDivisionData()
    }

    @api
    setCompanyDivision() {
        if (this.wizardCompanyDivisionId) {
            this.companyDivisionId = this.wizardCompanyDivisionId;
        } else {
            this.companyDivisionId = this.userCompanyDivisionId;
        }
        this.sendEvent();
    }

    connectedCallback() {
        if (this.showTitle) {
            this.title = this.labels.companyDivision;
            this.icon = "utility:company";
        }
        this.loadCompanyDivisionData();
    }

    loadCompanyDivisionData() {
        let inputs = {userId: Id};
        notCacheableCall({
                className: "MRO_LC_CompanyDivision",
                methodName: "getCompanyDivisionData",
                input: inputs
            })
            .then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.errorMsg);
                    } else {
                        this.listCompanyDivision = response.data.listCompaniesDivision;
                        this.enforced = response.data.companyDivisionEnforced;
                        this.userCompanyDivisionId = response.data.companyDivisionId;
                        this.setCompanyDivision();
                    }
                }
            })
            .catch(errorMsg => {
                error(this, errorMsg);
            });
    }

    handleChange(event) {
        let filteredCompany = this.listCompanyDivision.filter(function (company) {
            return company.value === event.detail.value;
        });

        if (filteredCompany) {
            this.companyDivisionId = filteredCompany[0].value;
            this.companyDivisionName = filteredCompany[0].label;
            this.sendEvent();
        }
    }

    sendEvent() {
        this.dispatchEvent(
            new CustomEvent("changed", {
                detail: {
                    divisionId: this.companyDivisionId, 
                    divisionName: this.companyDivisionName,
                    isCompanyDivisionEnforced: this.enforced
                }
            })
        );
    }

    get isReadonly() {
        return ( (this.enforced) || (this.disabled) );
    }
    
}
