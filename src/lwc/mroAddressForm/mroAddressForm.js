import { LightningElement, api, track } from "lwc";
import normalizeLabel from "@salesforce/label/c.NormalizeAddress";
import forceAddressLabel from "@salesforce/label/c.ForceAddress";
//import cacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.cacheableCall";
import getStructure from "@salesforce/apex/MRO_SRV_Address.getAddressStructure";
import getValues from "@salesforce/apex/MRO_LC_Address.getValues";
import { error } from "c/notificationSvc";
import { labels } from "c/labels";

export default class MroAddressForm extends LightningElement {
    //**Start Deprecated */
    @api residentialAddress;
    @api otherAddresses;
    @api isFacultative = false;
    @api isProsumers = false;
    //**Start Deprecated */
    @track _address = {};
    label = {
        normalizeLabel,
        forceAddressLabel
    };

    @api objectApiName;
    @api addressPrefix = "";
    @api readeOnlyFieldAddress;
    //@api address = {};
    @api availableAddresses = [];
    @api addressLayoutApiName;

    //@api readOnly;
    @api readOnlyFields = [];
    @api disableSave;
    @api showMessage = false;
    @api disableNormalize = false;
    @api hideForceAddress = false;
    //@api readOnlyButton = false;
    @api addressNormalized = false;
    @api isNormalize = false;
    //@api isNewRecord;
    @api editDisabled = false;
    @api addressForced = false;

    labels = labels;


    @track addressParts = [];

    readOnlyAddress = false;

    @api
    get readOnly() {
        return (this.readOnlyAddress || this.editDisabled);
    }
    set readOnly(value) {
        this.readOnlyAddress = value;
        if (this.addressParts && Array.isArray(this.addressParts)) {
            this.addressParts.forEach(part => {
                part.readOnly = (value || this.readOnlyFields.includes(part.item.FieldName__c));
            });
        }
    }

    @api
    get address() {
        return this._address;
    }
    set address(value) {
        this._address = value;
    }

    initialized = false;

    inputDelaySecond = .5;
    getInputListValuesDelay;

    connectedCallback() {
        //console.log('this.addressffff: '+ JSON.stringify(this.address));
        if (!this.addressLayoutApiName) {
            this.addressLayoutApiName = "DefaultAddressLayout";
        }
        getStructure({
            addressStructureApiName: this.addressLayoutApiName
        }).then(response => {
            if (response) {
                let result = response.slice(0);
                let parts = [];
                for (let item of result) {
                    let pickValues = [];
                    for (let value of item.AddressPicklistValues__r || []) {
                        pickValues.push({
                            key: value.QualifiedApiName,
                            value: value.MasterLabel
                        });
                    }
                    let readOnlyField = (this.readOnly || this.readOnlyFields.includes(item.FieldName__c));

                    if (this.isProsumers) {
                        item.Required__c = false;
                    }

                    parts.push({
                        item: item,
                        values: pickValues,
                        readOnly: readOnlyField
                    });
                }
                this.addressParts = parts;
            }
        });
        this.initAutoComplete();
    }

    initAutoComplete() {
        this.getInputListValuesDelay = this.delay(this.getInputListValues, this.inputDelaySecond, (event) => [{
            fieldName: event.target.fieldName,
            detail: event.detail
        }]);
    }

    handleCityChange(event) {
        this.enableNormalize();
        this.removeError(event);
    }

    handleCountryChange(event) {
        this.enableNormalize();
        this.removeError(event);
    }

    renderedCallback() {
        //console.log('this.addressaaaa: '+ JSON.stringify(this.address));
        if (this.initialized) {
            return;
        }
        if (this.readOnly) {
            this.addressNormalized = true;
            this.isNormalize = true;
        }
        this.initialized = true;
    }

    @api
    getValues() {
        let addrForm = this.template.querySelector("c-mro-address-edit-form");
        return addrForm.getAddressFields();
    }

    @api
    getStringifiedAddress() {
        //console.log('ADDRESS:', JSON.stringify(this.address));
        let stringifiedAddress = '';
        if (this.address.streetName) {
            if (this.address.streetType) {
                stringifiedAddress += this.address.streetType+' ';
            }
            stringifiedAddress += this.address.streetName+' ';
            if (this.address.streetNumber) {
                stringifiedAddress += this.address.streetNumber;
                if (this.address.streetNumberExtn) {
                    stringifiedAddress += ' '+this.address.streetNumberExtn;
                }
            }
            stringifiedAddress += ', ';
        }
        if (this.address.city) {
            stringifiedAddress += this.address.city;
            if (this.address.province) {
                stringifiedAddress += ' ('+this.address.province+')';
            }
        }
        if (this.address.postalCode) {
            stringifiedAddress += ' '+this.address.postalCode;
        }
        if (this.address.country) {
            stringifiedAddress += ', '+this.address.country;
        }
        return stringifiedAddress;
    }

    @api
    isValid() {
        //let addrForm = this.template.querySelector('c-mro-address-edit-form');
        //return addrForm.isValid();
        if (this.readOnly) {
            this.isNormalize = true;
        }
        return this.isNormalize;
    }

    @api
    isNormalized() {
        return this.addressNormalized;
    }

    forceAddress() {
        let addrForm = this.template.querySelector("c-mro-address-edit-form");
        this.showMessage = addrForm.forceAddress();
        if (!this.showMessage) {
            this.disabledButton();
        }
    }

    @api
    normalize() {
        let addrForm = this.template.querySelector("c-mro-address-edit-form");
        this.showMessage = addrForm.normalize();
        if (!this.showMessage) {
            this.disabledButton();
        }
    }

    disabledButton() {
        this.disabledSave = false;
        this.dispatchEvent(new CustomEvent("changesavebutton", { detail: { "disabledSave": this.disabledSave } }));
    }

    handleNameChange(event) {
        event.stopPropagation();
    }

    enableNormalize() {
        if (this.disableNormalize) {
            this.disableNormalize = false;
            this.disabledSave = true;
            this.isNormalize = false;
            this.dispatchEvent(new CustomEvent("changesavebutton", {
                    detail: {
                        "disabledSave": this.disabledSave,
                        "isnormalize": this.isNormalize
                    }
                })
            );
        }
    }

    handleChange(event) {
        this.enableNormalize();
        this.removeError(event);
        this.getInputListValuesDelay(event);
    }

    handleChangeValue() {
        this.enableNormalize();
    }

    removeError(event) {
        event.target.classList.remove("slds-has-error");
    }

    handleIsReadOnly(event) {
        this.addressNormalized = true;
        this.readOnly = event.detail.readOnly;
        this.addressForced = event.detail.addressForced;
        if (event.detail.readOnly) {
            this.isNormalize = true;
            this.address = event.detail.address;
            this.dispatchEvent(new CustomEvent("updateaddressnormalized", {
                detail: {
                    "addressNormalized": true,
                    "isnormalize": true,
                    "addressForced": event.detail.addressForced,
                    "address": this.address
                }
            }));
        } else {
            error(this, this.labels.addressNotValid);
        }
    }

    editAddress() {
        this.readOnly = false;
        this.addressNormalized = false;
        this.disableNormalize = true;
        this.isNormalize = true;
        this.dispatchEvent(new CustomEvent("updateaddressnormalized", {
                detail: {
                    "addressNormalized": false,
                    "isnormalize": this.isNormalize,
                    "addressForced": this.addressForced
                }
            })
        );
    }

    async getInputListValues({ detail }) {

        const pAddress = this.getValues();
        const address = Object.keys(pAddress).reduce((add, k) => ({
            ...add,
            [k.replace(this.addressPrefix, "").replace("__c", "")]: pAddress[k]
        }), {});
        const { value, guid } = detail;

        const addressParts = [...this.addressParts];
        const addressPartIndex = addressParts.findIndex(a => a.item.FieldName__c === guid);
        const addressPart = addressParts[addressPartIndex];

        if (addressPart && value && value.trim() && value.trim().length >= 2 /*&& addressPart && addressPart.ParentsFields__c*/) {
            // debugger;
            //console.log("loading", addressPart.item.FieldName__c);
            const values = await getValues({ fieldName: addressPart.item.FieldName__c, address });
            const listValues = values.map(value => ({ key: value.replace(/\s*/g, "_"), value: value }));
            // debugger;
            //console.log("loaded", listValues.length, addressPart.item.FieldName__c);
            //console.log("loaded", listValues);
            addressParts.splice(addressPartIndex, 1, { ...addressPart, values: listValues });
            this.addressParts = addressParts;

        }

        // this.addressParts={...this.addressParts  }

    }


//delay function execution, and cancel concurrency
    delay(func, wait, getArgs) {
        const milliSecond = wait * 1000;
        let context, args;
        let timeout = null;
        const later = function() {
            timeout = null;
            func.apply(context, args);
            context = args = null;
        };
        return function() {
            context = this;
            args = arguments;
            args = getArgs ? getArgs.apply(this, args) : args;
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(later, milliSecond);
        };
    }

    @api
    copyAddress(addr) {
        const editForm = this.template.querySelector("c-mro-address-edit-form");
        if (editForm) {
            editForm.copyAddress(addr);
        }
    }
}
