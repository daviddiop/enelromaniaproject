/**
 * Created by vincenzo.scolavino on 26/03/2020.
 */

import { LightningElement, api, wire, track } from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import { labels } from 'c/labels';

export default class MroNlcclcCaseTile extends LightningElement {
    label = labels;
    @api isRow = false;
    @api recordTypeName;
    @api recordId;
    @api accountId;
    @api dossierId;
    @api supplyId;
    @api companyDivisionId;
    @api disabled;
    @api nlcClcCode;
    @api customerNotes;
    @api servicePointCode;

    @track spinner;
    @track isOpenDelete = false;
    @track isOpenEdit = false;
    @track isGas;
    @track isElectric;
    @track editForm = 'editForm';

    connectedCallback() {
        this.spinner = false;
        this.checkCommodity();
    }

    checkCommodity(){
        switch(this.recordTypeName){
            case 'Electric' : this.isElectric = true;
            break;
            case 'Gas' : this.isGas = true;
            break;
            default:
                break;
        }
    }

    get svgURLEle() {
        return SVG_URL + '/images/Electric.svg#electric';
    }
    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }

    editCase(){
        this.isOpenEdit = true;
    }

    deleteCase(){

    }

    closeModalEdit(){
        this.isOpenEdit = false;
    }

    handleNlcClcUpdated(event){
        let form = this.template.querySelector(this.getFormString(this.editForm));
        let fields = {};
        this.template.querySelectorAll(this.getFormOutputFieldsString(this.editForm)).forEach(element => {
            fields[element.fieldName] = element.value;
        });
        console.log('form : ' + form);
        console.log('fields' + JSON.stringify(fields));
        console.log('handleNlcClcUpdated');
        let currentRecordId= this.recordId;
        console.log(currentRecordId);
        this.recordId='000';
        console.log(this.recordId);
        this.recordId= currentRecordId;
        console.log(this.recordId);
        console.log('UPDATED NLCCLC : ' + this.nlcClcCode);
        this.dispatchEvent(new CustomEvent('edited', {
            detail : {
                'recordId': this.recordId
            }
        }));
    }

    deleteCase(){
        this.disabled = true;
        this.dispatchEvent(new CustomEvent('delete', {
            detail : {
                'recordId': this.recordId
            }
        }));
    }

    getFormInputFieldsString(formDataId){
        return  'lightning-record-edit-form[data-id="'+formDataId+'"] lightning-input-field';
    }
    getFormString(formDataId){
        return 'lightning-record-edit-form[data-id="'+formDataId+'"]';
    }

    getFormOutputFieldsString(formDataId){
        return  'lightning-record-edit-form[data-id="'+formDataId+'"] lightning-output-field';
    }

}