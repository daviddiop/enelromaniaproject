import {LightningElement, api} from 'lwc';
import {labels} from 'c/mroLabels';

export default class ContractAccountTile extends LightningElement {

    @api recordId;
    @api disabled = false;

    labels = labels;

    onDelete(){
        this.dispatchEvent(new CustomEvent('deletecontractaccount', {
            detail: {
                recordId: this.recordId
            }
        }));
    }


    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }

}