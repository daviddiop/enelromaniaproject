/**
 * Created by Boubacar Sow  on 15/10/2020.
 */

import {api, LightningElement, track, wire} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';
import {NavigationMixin} from "lightning/navigation";
import {getRecord} from 'lightning/uiRecordApi';

export default class MroAssociatedAccounts extends NavigationMixin(LightningElement) {

    @api hideConfirmSelection;
    @api recordId;

    @track paymentCode ;
    @track associatedAccountList;
    @track associatedAccount;
    @track detailRendered;
    @track notificationRendered;
    @track consumptionPlacesRendered;
    @track username;
    @track showLoadingSpinner = false;
    @track showIllustrationEmptyStat = false;

    labels = labels;

    connectedCallback(){
        this.hideConfirmSelection = true;
    }

    @wire(getRecord, {recordId: '$recordId', fields: ['ContractAccount__c.BillingAccountNumber__c']})
    wiredRecord({error, data}) {
        if (data) {
            this.paymentCode = data.fields.BillingAccountNumber__c.value;
            console.log('### paymentCode '+this.paymentCode);
        } else if (error) {
            let message = this.labels.unexpectedError;
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            console.log('### message '+message);
            error(this, message);
        }
    }

    /*get paymentCode(){
        return this.paymentCode;
    }*/

    handleRetreiveAssociatedAccount(){
        this.showLoadingSpinner = true;
        let input = {
            'paymentCode': this.paymentCode
        }
        notCacheableCall({
            className: "MRO_LC_AssociatedAccount",
            methodName: "listAssociatedAccount",
            input: input
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, this.labels.unexpectedError);
            }
            console.log('#### associatedAccount '+JSON.stringify(apexResponse.data));
            if (apexResponse.data.error) {
                error(this, apexResponse.data.errorMsg);
            }
            if (!apexResponse.data.error && apexResponse.data.associatedAccounts){
                this.associatedAccountList = JSON.parse(JSON.stringify(apexResponse.data.associatedAccounts));
                console.log('#### associatedAccount '+this.associatedAccountList);
            }
            this.showIllustrationEmptyStat = !this.associatedAccountList || this.associatedAccountList.length === 0;
        }).catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg));
        });

    }

    findAssociatedAccount(associatedAccountId, fieldName) {
        if (!associatedAccountId) {
            return null;
        }
        return this.associatedAccountList.find(function (associatedAccount) {
            return associatedAccount[fieldName] === associatedAccountId;
        });
    }

    moreDetails(event){
        let associatedAccountId = event.target.dataset.id;
        let foundAssociatedAccount = this.findAssociatedAccount(associatedAccountId,'username');
        console.log('### foundAssociatedAccount '+JSON.stringify(foundAssociatedAccount));

        if (!foundAssociatedAccount) {
            return;
        }
        this.associatedAccount = foundAssociatedAccount;
        this.detailRendered = true;

    }

    notification(event){
        let associatedAccountId = event.target.dataset.id;
        let foundAssociatedAccount = this.findAssociatedAccount(associatedAccountId,'username');
        console.log('### foundAssociatedAccount '+JSON.stringify(foundAssociatedAccount));

        if (!foundAssociatedAccount) {
            return;
        }
        this.associatedAccount = foundAssociatedAccount;
        this.notificationRendered = true;

    }

    consumptionPlaces(event){
        let associatedAccountId = event.target.dataset.id;
        let foundAssociatedAccount = this.findAssociatedAccount(associatedAccountId,'username');
        console.log('### foundAssociatedAccount '+JSON.stringify(foundAssociatedAccount));

        if (!foundAssociatedAccount) {
            return;
        }
        this.associatedAccount = foundAssociatedAccount;
        this.consumptionPlacesRendered = true;

    }

    closeMoreDetails() {
        this.associatedAccount = null;
        this.detailRendered = false;
    }

    closeNotificatiions() {
        this.associatedAccount = null;
        this.notificationRendered = false;
    }

    closeConsumptionPlaces() {
        this.associatedAccount = null;
        this.consumptionPlacesRendered = false;
    }

    navigateToAccount(event) {

        let associatedAccountId = event.target.dataset.id;
        let foundAssociatedAccount = this.findAssociatedAccount(associatedAccountId,'username');
        console.log('### foundAssociatedAccount '+JSON.stringify(foundAssociatedAccount));

        if (!foundAssociatedAccount) {
            return;
        }
        this.username = foundAssociatedAccount.username;
        console.log('### username '+this.username);
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.username,
                "objectApiName": "Account",
                "actionName": "view"
            }
        });
    }

}