import { LightningElement, api, wire, track } from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import { labels } from 'c/labels';

export default class MroNlcclcCaseTile extends LightningElement {
    label = labels;
    @api isRow = false;
    @api subProcess;
    @api recordId;
    @api accountId;
    @api dossierId;
    @api supplyId;
    @api origin;
    @api channel;
    @api recordType;
    @api disabled;
    @api isContractualDataChange;

    @track spinner;
    @track isOpenDelete = false;
    @track isOpenEdit = false;
    @track editForm = 'editForm';
    @track isContract = false;
    @track isContractAccount = false;
    @track isBillingProfile = false;
    @track isElectric = false;
    @track isGas = false;

    connectedCallback() {
        this.spinner = false;
        this.checkObjType();
        this.checkCommodity();
    }

    get svgURLEle() {
        return SVG_URL + '/images/Electric.svg#electric';
    }
    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }

    editCase(){
        this.isOpenEdit = true;
        this.template.querySelector('c-mro-contractual-data-edit').open();
    }

    closeModalEdit(){
        this.isOpenEdit = false;
    }

    handleContractDataUpdated(event){
        // place here code to handle update
    }

    deleteCase(){
        this.isOpenDelete = true;
    }

    confirmDeleteCase(){
        this.disabled = true;
        this.dispatchEvent(new CustomEvent('delete', {
            detail : {
                'recordId': this.recordId
            }
        }));
       this.closeModal();
    }

    closeModal() {
        this.isOpenDelete = false;
    }

    getFormInputFieldsString(formDataId){
        return  'lightning-record-edit-form[data-id="'+formDataId+'"] lightning-input-field';
    }
    getFormString(formDataId){
        return 'lightning-record-edit-form[data-id="'+formDataId+'"]';
    }

    getFormOutputFieldsString(formDataId){
        return  'lightning-record-edit-form[data-id="'+formDataId+'"] lightning-output-field';
    }

    checkObjType(){
        if(this.subProcess === 'Change Billing frequency')
            this.isContractAccount = true;
    }

    checkCommodity(){
        switch(this.recordType){
            case 'Electric' : this.isElectric = true;
                break;
            case 'Gas' : this.isGas = true;
                break;
            default:
                break;
        }
    }

}