import {api, LightningElement, track} from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error, success} from 'c/notificationSvc';
import {labels} from 'c/labels';

export default class CaseTile extends LightningElement {
    @api isConnectionWizard;

    RECORD_TYPE_EE = ['Termination_ELE', 'SwitchOut_ELE','TechnicalDataChange_ELE','Connection_ELE','ContractualDataChange_ELE','SelfReading_ELE'];
    RECORD_TYPE_GAS = ['Termination_GAS', 'SwitchOut_GAS','TechnicalDataChange_GAS','Connection_GAS','ContractualDataChange_GAS','SelfReading_GAS'];
    RECORD_TYPE_SERVICE = ['Termination_SRV'];
    RECORD_TYPE_BP = ['BillingProfileChange'];
    RECORD_TYPE_MERGE = ['ContractMerge'];

    labels = labels;
    @api caseId;
    @api isCaseModalByAddressForm =false;
    @api hideEditIcon = false;
    @api isRow = false;
    @api disabled = false;
    @api selectCaseFields = [];
    @api selectCaseTileFields = [];
    @api readOnlyAddress = false;
    @api addressForced = false;
    @api recordTypeCase;
    @api tileTitle;
    @track defaultTileTitle;

    @track caseFields = [];
    @track openConfirmModal;
    @track openEditModal = false;
    @track listRecordType = [];
    @track isGas;
    @track isElectric;
    @track isService;
    @track isBillingProfile;
    @track isContractMerge;
    @track isNormalize;
    @track caseAddress;

    connectedCallback() {

        this.defaultTileTitle = this.tileTitle ?  this.tileTitle : this.labels.caseEdit;

        let inputs = {recordId: this.caseId};
        notCacheableCall({
            className: 'SupplyCnt',
            methodName: 'getDeveloperName',
            input: inputs
        })
            .then(response => {
                let recordType = response.data.listRecordType[0].RecordType.DeveloperName;
                this.isElectric = this.RECORD_TYPE_EE.includes(recordType);
                this.isGas = this.RECORD_TYPE_GAS.includes(recordType);
                this.isService = this.RECORD_TYPE_SERVICE.includes(recordType);
                this.isBillingProfile = this.RECORD_TYPE_BP.includes(recordType);
                this.isContractMerge = this.RECORD_TYPE_MERGE.includes(recordType);

                if ((!this.isRow) && (this.selectCaseTileFields.length > 0)) {
                    this.selectCaseTileFields = this.selectCaseTileFields.filter(function (el) {
                        return el !== 'Supply__c';
                    });
                }

                if (recordType.includes('TechnicalData')){
                    if (this.isGas){
                        this.checkGasFields();
                    }else if (this.isElectric){
                        this.checkEleFields();
                    }
                }else {
                    this.caseFields = this.selectCaseFields;
                }

                this.error = undefined;
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }


    initCaseAddress() {
        let inputs = {caseId: this.caseId};
        notCacheableCall({
            className: 'CaseCnt',
            methodName: 'getCaseAddressField',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let addr = {};
                addr = response.data.caseAddress;
                this.caseAddress = addr;
                if(addr.addressNormalized){
                    this.readOnlyAddress = true;
                    this.addressForced = false;

                }else{
                    this.readOnlyAddress = false;
                 }

                this.spinner = false;
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
    }

    get svgURLEle() {
        return SVG_URL + '/images/Electric.svg#electric';
    }

    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }

    get svgURLService() {
        return SVG_URL + '/images/Service.svg#service';
    }

    get svgURLBPChange() {
        return SVG_URL + '/images/BPChange.svg#bpChange';
    }

    get svgURLContractMerge() {
        return SVG_URL + '/images/ContractMerge.svg#contractmerge';
    }

    closeModal() {
        this.openEditModal = false;
        this.openConfirmModal = false;
    }

    deleteCaseFromTile() {
        let inputs = {recordId: this.caseId};
        notCacheableCall({
            className: 'SupplyCnt',
            methodName: 'deleteCase',
            input: inputs
        })
            .then(response => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                    return;
                }
                this.openConfirmModal = false;
                this.openEditModal = false;
                const deleteEvent = new CustomEvent('delete', {
                    detail: {
                        deleteRecord: this.caseId
                    }
                });
                this.dispatchEvent(deleteEvent);
            })
            .catch(errorMsg => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    editCaseIcon() {
        this.openEditModal = true;
        if(this.isCaseModalByAddressForm){
            this.initCaseAddress();
        }
    }

    deleteCaseIcon() {
       this.openConfirmModal = true;
    }

    handleClose(event) {
        this.openEditModal = event.target.closeModal;
    }

    checkGasFields() {
        let myCaseFields = [...this.selectCaseFields];
        if (myCaseFields){
            if (!myCaseFields.includes('ConversionFactor__c')){
                myCaseFields.push('ConversionFactor__c');
            }
            if (!myCaseFields.includes('PressureLevel__c')){
                myCaseFields.push('PressureLevel__c');
            }
            if (!myCaseFields.includes('Pressure__c')){
                myCaseFields.push('Pressure__c');
            }
            myCaseFields = myCaseFields.filter(function (value) {
                return value !== 'Voltage__c' && value !== 'VoltageLevel__c' && value !== 'PowerPhase__c';
            });
            this.caseFields = myCaseFields;
        }
    }

    checkEleFields(){
        let myCaseFields = [...this.selectCaseFields];
        if (myCaseFields){
            if (!myCaseFields.includes('PowerPhase__c')){
                myCaseFields.push('PowerPhase__c');
            }
            if (!myCaseFields.includes('VoltageLevel__c')){
                myCaseFields.push('VoltageLevel__c');
            }
            if (!myCaseFields.includes('Voltage__c')){
                myCaseFields.push('Voltage__c');
            }
            myCaseFields = myCaseFields.filter(function (value) {
                return value !== 'ConversionFactor__c' && value !== 'PressureLevel__c' && value !== 'Pressure__c';
            });
            this.caseFields = myCaseFields;
        }
    }

    get addClassHideEditIcon() {
        if (this.hideEditIcon) {
            return 'slds-hide';
        }
        return '';
    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }


}