/**
 * Created by pes on 05/05/2020.
 */

import {LightningElement, api, track} from 'lwc';

export default class MroDynamicForm extends LightningElement {

    @api
    setFormJson(value) {
        //console.log('Setting form JSON:', value);
        this.form = JSON.parse(value);
        this.sections = this.form.sections;
    };
    @track form;
    @track sections;

    @api
    checkData() {

        var checkResult = {};
        checkResult.isValid = true;
        checkResult.errorMessages = '';
        this.template.querySelectorAll('c-mro-dynamic-form-address-section').forEach(addressSection => {

            var isCurrentValid = addressSection.isValid();
            console.log('MroDynamicForm.checkData - isCurrentValid', isCurrentValid);
            if(!isCurrentValid) { checkResult.errorMessages = checkResult.errorMessages + 'Address must be normalized.'; }
            checkResult.isValid = checkResult.isValid && addressSection.isValid();
        });
        return checkResult;
    }

    @api
    getFormData() {
        const formData = {};
        this.template.querySelectorAll('c-mro-dynamic-form-section').forEach(section => {

            const record = section.getSectionData();
            console.log('MroDynamicForm.getFormData - record (standard)', JSON.stringify(record));
            if (!formData.hasOwnProperty(record.Id)) formData[record.Id] = {};
            for (var key in record) {
                formData[record.Id][key] = record[key];
            }
        });
        this.template.querySelectorAll('c-mro-dynamic-form-address-section').forEach(section => {

            const record = section.getSectionData();
            console.log('MroDynamicForm.getFormData - record (address)', JSON.stringify(record));
            if (!formData.hasOwnProperty(record.Id)) formData[record.Id] = {};
            for (var key in record) {
                formData[record.Id][key] = record[key];
            }
        });
        return formData;
    }
}