import { LightningElement, track, api } from 'lwc';
import {labels} from 'c/labels';


export default class ProductSelectionCart extends LightningElement {
    @track rendered
    @track columns
    labels = labels;

    @api productList

    @api
    open() {
        this.rendered = true;
    }

    @api
    close() {
        this.rendered = false;
    }

    @api
    reload(){
        this.productList = [];
    }


    connectedCallback() {
        let actions = [
            { label: 'Configure', name: 'configure' },
            { label: 'Delete', name: 'delete' }
        ];
        this.columns = [
            { label: 'Name', fieldName: 'name', type: 'text' },
            { label: 'Description', fieldName: 'description', type: 'text' },
            { label: 'Action', type: 'action', typeAttributes: { rowActions: actions } }
        ];
    }

    handleRowAction(event) {
        const action = event.detail.action;
        const selectedProduct = event.detail.row;
        switch (action.name) {
            case 'configure':
                this.configure(selectedProduct);
                break;
            case 'delete':
                this.removeProduct(selectedProduct);
                break;
            default:
                break;
        }
    }

    handleCheckout() {
        let prdList = this.productList;
        const checkoutEvent = new CustomEvent('checkout', {
            detail: {
                productList: prdList
            }
        });
        this.dispatchEvent(checkoutEvent);
    }

    removeProduct(product) {
        let prdList = this.productList;
        let productIndex = prdList.indexOf(product);
        prdList.splice(productIndex, 1);
        this.productList = prdList;
    }

    get isEmpty() {
        return this.productList.length === 0;
    }

    configure(product) {
        this.close();

        const selectedEvent = new CustomEvent('selectproduct', {
            detail: {
                action: 'configure',
                product: product
            }
        });
        const selectedEventLWC = new CustomEvent('selectproduct', {
            detail: {
                action: 'configure',
                product: product
            }
        });
        this.dispatchEvent(selectedEvent);
        this.dispatchEvent(selectedEventLWC);
    }
}