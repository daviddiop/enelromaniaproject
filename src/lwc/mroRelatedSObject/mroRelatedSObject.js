/**
 * Created by tommasobolis on 29/06/2020.
 */

import { LightningElement, track, api} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import initialize from '@salesforce/apex/MRO_LC_RelatedSObjectCnt.initialize';

export default class relatedSObject extends NavigationMixin(LightningElement) {
    @track label = new Map() //object which contains the labels
    //label = new Map();
    @track relationshipSObjectId; //string which contains the relationship SObject's id
    @track relationshipSObjectType = ''; //string which contains the relationship SObject's type
    @track relationshipSObjectFieldsToGet = []; //array which contains the relationship SObject's fields API name which have to be visualized

    @api recordId = '$recordId'; //string which contains the record's Id of the SObject where the component is placed
    @api title; //String which contains the component's title
    @api icon; //String which contains the component's icon
    @api relationshipFieldAPIName; //string which contains the API Name of the field which contains the Id of the relationship SObject
    @api fieldSet; // String which contains the name of the FieldSet where is stored the relationship Object's API Fields Name to retrieve
    @api enableNavigateToSObject; //boolean which indicates whether is allowed Navigate to sObject (enable/disable the 'View' button)


    /*
        This method manage the init event of the component retrieving the relationship SObject's information from the 'initialize' method of the 'MRO_LC_RelatedSObjectCnt' Apex Class
    */
    connectedCallback() {
            // initialize component
            initialize( {recordId: this.recordId,
                         title: this.title,
                         fieldSet: this.fieldSet,
                         relationshipFieldAPIName: this.relationshipFieldAPIName
                        })

                    .then(result => {

                        if (result.error == false) {
                            this.relationshipSObjectId = result.relationshipSObjectId;
                            this.relationshipSObjectType = result.relationshipSObjectType;
                            this.relationshipSObjectFieldsToGet = result.relationshipSObjectFieldsToGet;
                            if(this.relationshipSObjectFieldsToGet === undefined) {

                                this.relationshipSObjectFieldsToGet = [
                                    {fieldApiName: "Id"},
                                    {fieldApiName: "CaseNumber"},
                                    {fieldApiName: "Name"},
                                    {fieldApiName: "CreatedDate"},
                                    {fieldApiName: "LastModifiedDate"}
                               ];
                            }
                            this.label = result.label;

                        } else {
                            //this.showTable = false;
                            console.log('MRO_LC_RelatedSObjectCnt.initialize: ', result.errorMessage);
                            console.log('MRO_LC_RelatedSObjectCnt.initialize: ', result.errorStackTraceString);
                        }
                    })
                    .catch (error => {
                        //this.showTable = false;
                        console.log('MRO_LC_RelatedSObjectCnt.initialize error ', error,  JSON.stringify(error));
                        //this.error = error;
                    })
                    .finally ( () => {

                        //this.showSpinner = false;
                    })
    }


    /*
        This method permits to navigate to relationship SObject specifying the relationship SObject's Id
    */
    handleViewClick (){
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.relationshipSObjectId,
                    actionName: 'view'
                }
            });
    }

}