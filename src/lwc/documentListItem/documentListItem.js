import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class DocumentListItem extends NavigationMixin(LightningElement) {

    @api documentId;
    @api fileId;

    handleCheckDocument() {
        this[NavigationMixin.Navigate]({
            type: "standard__component",
            attributes: {
                componentName: "c__validatableDocumentsListWrp"
            },
            state: {
                c__recordId: this.documentId
            }
        });
    }

    navigateToDocument() {
        this[NavigationMixin.Navigate]({
           type: "standard__recordPage",
           attributes: {
               "recordId": this.fileId,
               "objectApiName": "ContentDocument",
               "actionName": "view"
           }
       });
   }
}