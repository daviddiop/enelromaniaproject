/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 23.09.20.
 */

import {CurrentPageReference} from 'lightning/navigation';
import {LightningElement, track, wire, api} from 'lwc';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class MroTheftPeriodSelection extends LightningElement {

    @api recordId;
    @track theftStartDate;
    @track theftEndDate;
    @track servicePointId;
    @track selectedRows;

    @track supplyList;

    supplyTableColumns = [
        {label: 'Type', fieldName: 'recordTypeName', type: 'text'},
        {label: 'Supply', fieldName: 'name', type: 'text'},
        {label: 'Status', fieldName: 'status', type: 'text'},
        {label: 'Service Point', fieldName: 'servicePointCode', type: 'text'},
        {label: 'Contract Account', fieldName: 'contractAccountName', type: 'text'},
        {label: 'Contract', fieldName: 'contractName', type: 'text'},
        {label: 'Company', fieldName: 'companyDivisionName', type: 'text'}
    ];

    labels = labels;

    connectedCallback() {
        this.theftStartDate = this.getDateString(12);
        this.theftEndDate = this.getDateString();
        if(this.recordId) {
            this.loadSupplies();
        }
    }

    loadSupplies(){
        this.showLoadingSpinner = true;
        notCacheableCall({
            className: 'MRO_LC_TheftQuery',
            methodName: 'listSupplies',
            input: {
                'serviceSiteId': this.recordId
            }
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }
            if(apexResponse.data.length === 1){
                this.selectedRows = [apexResponse.data[0].Id];
                this.servicePointId = apexResponse.data[0].ServicePoint__c;
            }
            this.supplyList = apexResponse.data.map(item => {
                return {
                    Id : item.Id,
                    ServicePoint__c : item.ServicePoint__c,
                    name : item.Name,
                    status : item.Status__c,
                    recordTypeName : item.RecordType.Name,
                    servicePointCode : item.ServicePoint__r ? item.ServicePoint__r.Code__c : '',
                    contractAccountName: item.ContractAccount__r ? item.ContractAccount__r.Name : '',
                    contractName: item.Contract__r ? item.Contract__r.ContractNumber : '',
                    companyDivisionName: item.CompanyDivision__r ? item.CompanyDivision__r.Name : '',
                };
            });
        });
    }

    getDateString(decrementMonths) {
        let date = new Date();
        if (decrementMonths) {
            date = new Date(date.setMonth(date.getMonth() - decrementMonths));
        }
        return [date.getFullYear(), ('0' + (date.getMonth() + 1)).slice(-2), ('0' + date.getDate()).slice(-2)].join('-');
    }

    handleTheftStartDateChange(event) {
        let inputStartDate = event.target.value;

        if (this.theftEndDate) {
            let parsedInputStartDate = Date.parse(inputStartDate);
            let parsedTheftEndDate = Date.parse(this.theftEndDate);

            if (parsedInputStartDate >= parsedTheftEndDate) {
                error(this, labels.startDateAfterEndDate);
                inputStartDate = "";
                event.target.value = inputStartDate;
            }
        }
        this.theftStartDate = inputStartDate;
    }

    handleTheftEndDateChange(event) {
        let inputEndDate = event.target.value;

        if (this.theftStartDate) {
            let parsedInputEndDate = Date.parse(inputEndDate);
            let parsedTheftStartDate = Date.parse(this.theftStartDate);

            if (parsedTheftStartDate >= parsedInputEndDate) {
                error(this, labels.startDateAfterEndDate);
                inputEndDate = "";
                event.target.value = inputEndDate;
            }
        }
        this.theftEndDate = inputEndDate;
    }

    handleSupplySelection(event){
        this.servicePointId = event.detail.selectedRows[0].ServicePoint__c;
    }

    get disabled(){
        return !this.servicePointId || !this.theftStartDate || !this.theftEndDate;
    }
}