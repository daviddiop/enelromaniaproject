/**
 * Created by tommasobolis on 02/12/2020.
 */

import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { error } from "c/notificationSvc";

import getAddress from '@salesforce/apex/MRO_LC_DynamicForm.getAddress';

export default class MroDynamicFormAddressSection extends LightningElement {

    @api showSpinner=false;
    @api readOnly = false;

    @api objectType;
    _section;
    @api
    get section() {
        return this._section;
    }
    set section(value) {
        this._section = JSON.parse(JSON.stringify(value));
        this.title = this._section.title;
    }
    @track title;

    @track isRequiredAddressField = true;
    @track disableNormalize = false;
    @track isNormalize;
    @track addressForced = true;
    @track activity;
    @track address;
    @track availableAddresses;

    connectedCallback() {
        this.getAddress();
    }

    getAddress() {
        getAddress({
            recordId: this.section.recordId,
            objectType: this.objectType,
            fieldsPrefix: this.section.fieldsPrefix
        })
        .then((response) => {
//            console.log('MroDynamicFormAddressSection.getAddress - response: ' + JSON.stringify(response));
            if(response.error == false){
                this.address = response.address;
                this.isnormalize = this.address.addressNormalized;
                this.readOnly = this.address.addressNormalized;
                this.addressForced = !this.address.addressNormalized;
            } else {
                console.log('MroDynamicFormAddressSection.getAddress - error',
                        response.errorMessage, response.errorStackTrace);
            }
            this.showSpinner = false;
        }).catch((errorMsg) => {
            if(errorMsg.body){
                error(this, JSON.stringify(errorMsg.body.output));
                console.log(JSON.stringify(errorMsg.body.output))
            } else {
                error(this,errorMsg);
            }
            this.showSpinner = false;
        });
    }

    @api
    isValid() {
        let addrForm = this.template.querySelector("c-mro-address-form");
        return addrForm.isNormalized();
    }

    @api
    getSectionData() {
        const record = {
            Id: this._section.recordId
        };
        let addrForm = this.template.querySelector("c-mro-address-form");
        this.address = addrForm.getValues();
        for (var key in this.address) {

            record[key] = this.address[key];
        }
        return record;
    }

    handleAddressNormalized(event) {

        this.isNormalize = event.detail.isnormalize;
        this.addressForced = event.detail.addressForced;
        if(event.detail.addressNormalized) {
            this.address = event.detail.address;
            this.address.addressNormalized = this.isNormalize;
            console.log('MroDynamicFormAddressSection.handleAddressNormalized - address', JSON.stringify(this.address));
        }
    }
}