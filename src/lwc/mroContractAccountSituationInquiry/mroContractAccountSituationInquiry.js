import {LightningElement, api, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';

export default class MroContractAccountSituationInquiry extends LightningElement {

    @api recordId;
    @track companyDivisionId;
    @track companyDivisionCode;
    @track startDate;
    @track endDate;
    @track defaultDate;
    @track isValidDate = true;
    @track selectedFisaIds;
    @track accountId;
    @track codDePlataId;

    labels = labels;
    connectedCallback() {
        this.getContractAccount(this.recordId);
        this.setPeriodSelectionDate();
    }

    getContractAccount(contractAccountId) {
        let inputs = {contractAccountId};
        notCacheableCall({
            className: "MRO_LC_ContractAccount",
            methodName: "getContractAccount",
            input: inputs
        })
            .then((response) => {
                if (response.data.error) {
                    error(this, response.data.errorMsg);
                }
                if(response.data.contractAccount){
                    this.accountId = response.data.contractAccount.Account__c;
                    this.companyDivisionCode = response.data.contractAccount.CompanyDivision__r.Code__c;
                    this.companyDivisionId = response.data.contractAccount.CompanyDivision__c;
                    this.codDePlataId = response.data.contractAccount.BillingAccountNumber__c;
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    }

    setPeriodSelectionDate () {
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        let startDate = new Date(dateOfToday.getFullYear(), 0, 1);
        let firstDay = startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate();
        this.endDate = today;
        this.startDate = firstDay;
        this.defaultDate = firstDay;
    }
    handleStartDateChange(event) {
        let inputStartDate = event.target.value;
        if (this.endDate) {
            let parsedInputStartDate = Date.parse(inputStartDate);
            let parsedInvoiceEndDate = Date.parse(this.endDate);

            if (parsedInputStartDate >= parsedInvoiceEndDate) {
                error(this, labels.startDateAfterEndDate);
                inputStartDate = "";
                event.target.value = inputStartDate;
            }
        }
        this.startDate = inputStartDate;
        let fisaInquiryComponent = this.template.querySelector('c-mro-fisa-inquiry');
        if (fisaInquiryComponent){
            fisaInquiryComponent.enabeRetrieveButton();
        }

    }
    getFisaSelect(event) {
        this.selectedFisaIds = event.detail.fisaIds;
    }

    get hasCompanyCode(){
        return (this.companyDivisionId !== '' && this.companyDivisionId !== undefined && this.companyDivisionId !== null);
    }

}