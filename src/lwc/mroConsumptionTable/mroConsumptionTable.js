import {LightningElement, api, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';

export default class MroConsumptionTable extends LightningElement {
    @track productRateTypeSingle;
    @track productRateTypeDual;
    TYPE_NONE = 'None';
    @track consumptionTypeRateSingle;
    @track consumptionTypeRateDay;
    @track consumptionTypeRateNight;
    @track commodityGas;
    @track commodityElectric ;

    labels = labels;
    _type;
    _consumptions;

    @track measurement;
    @api updateConsumptions=false;

    @api osiList;
    @api supplyList;
    @api disabled = false;
    @api caseId;
    @api updateOsiEstimatedConsumption=false;

    @track selectedConsumption = null;
    @track editMode = false;
    @track selectedOsiServicePointCode;
    @track allConsumptionObjects;
    @track gasRating;
    @track isGas=false;
    @track validate=false;

    @api isContractualDataChange;
    @api roundConsumption = false;

    @api getAllConsumptionObjects(){
        this.allConsumptionObjects = this.outputOsiOrSupplyList.map(element => element.Consumption).flat(2);
        return this.allConsumptionObjects;
    }

    outputOsiOrSupplyList = [];

    @api set type(value) {
        this._type = value;
        this.generateData();
    }

    get type() {
        return this._type;
    }

    @api set consumptions(value) {
        this._consumptions = value;
        this.generateData();
    }

    @api update(changedOsiId) {
        console.log("update consumption table");
        this.generateData(changedOsiId,true);
    }

    get consumptions() {
        return this._consumptions;
    }

    generateData(changedOsiId,update) {
        if ((!this.osiList && !this.supplyList)|| !this.type || this.type === this.TYPE_NONE) {
            this.outputOsiOrSupplyList = [];
            return;
        }

        if(!this.commodityElectric){
            this.getConstants();
            return;
        }

        // Creates a new var as @api vars are immutable, filters out the incorrect data
        let filteredList=this.osiList;
        if(!filteredList){
            filteredList=this.supplyList;
        }
        /*Luca Ravicini consider generating data for one OSI and keep old data for others*/
        if(update && this.outputOsiOrSupplyList.length >0){
            let oldOutputList = {};
            if(changedOsiId){
                this.outputOsiOrSupplyList.forEach(ele => {
                    if(ele.Id !== changedOsiId){
                        oldOutputList[ele.Id]=ele.Consumption;
                    }
                });
                this.outputOsiOrSupplyList = JSON.parse(JSON.stringify(filteredList)).filter((element) => {
                    let elementType=element.RecordType.Name || element.RecordType.DeveloperName;
                    element.Consumption=oldOutputList[element.Id];

                    if(!this.osiList){
                        OSI['ServicePointCode__c']=OSI.ServicePoint__r.Name;
                        OSI['PointAddress__c']=OSI.ServicePoint__r.PointAddress__c;
                    }
                    element.OsiAndAddressLabel = OSI.ServicePointCode__c + " (" + OSI.PointAddress__c + ")";
                    return !(this.type === this.productRateTypeDual && elementType !== this.commodityElectric);
                });
            }
            else{
                this.outputOsiOrSupplyList.forEach(ele => {
                    if(!this.isChangedEstimatedConsumption(filteredList, ele)){
                        oldOutputList[ele.Id]=ele.Consumption;
                    }
                });
                this.outputOsiOrSupplyList = JSON.parse(JSON.stringify(filteredList)).filter((element) => {
                    let elementType=element.RecordType.Name || element.RecordType.DeveloperName;
                    if(oldOutputList[element.Id]){
                        element.Consumption=oldOutputList[element.Id];
                    }

                    if(!this.osiList){
                        element['ServicePointCode__c']=element.ServicePoint__r.Name;
                        element['PointAddress__c']=element.ServicePoint__r.PointAddress__c;
                    }
                    element.OsiAndAddressLabel = element.ServicePointCode__c + " (" + element.PointAddress__c + ")";

                    return !(this.type === this.productRateTypeDual && elementType !== this.commodityElectric);
                });
            }
        }else{
            this.outputOsiOrSupplyList = JSON.parse(JSON.stringify(filteredList)).filter((element) => {
                let elementType=element.RecordType.Name || element.RecordType.DeveloperName;
                return !(this.type === this.productRateTypeDual && elementType !== this.commodityElectric);
            });
        }

        this.isGas=filteredList.length > 0 && filteredList[0].RecordType.Name === this.commodityGas;
        this.outputOsiOrSupplyList.forEach(OSI => {
            let osis=this.osiList;
            if(!this.osiList){
                OSI['ServicePointCode__c']=OSI.ServicePoint__r.Name;
                OSI['PointAddress__c']=OSI.ServicePoint__r.PointAddress__c;
            }
            if((!changedOsiId || (changedOsiId && changedOsiId === OSI.Id)) && !OSI.Consumption) {
                console.log("generate date==="+OSI.ServicePointCode__c + "  update=="+update + "   changedOsi=="
                    +changedOsiId);
                OSI.Consumption = [];
                OSI.OsiAndAddressLabel = OSI.ServicePointCode__c + " (" + OSI.PointAddress__c + ")";
                if (this._consumptions && this._consumptions.length && !update) {
                    OSI.Consumption = this._consumptions.reduce(function (accumulator, currentValue, currentIndex, array) {

                        if (currentValue.OpportunityServiceItem__c === OSI.Id || currentValue.Supply__c === OSI.Id) {
                            let cloned = JSON.parse(JSON.stringify(currentValue));
                            cloned['myId'] = currentValue.Id;
                            if (!currentValue.Id) {
                                cloned['myId'] = OSI.Id + '_' + (currentIndex + 1);
                            }
                            cloned['hasOsi'] = false;
                            cloned['hasSupply'] = false;
                            if (osis) {
                                cloned['OpportunityServiceItem__c'] = OSI.Id;
                                cloned['hasOsi'] = true;

                            } else {
                                cloned['Supply__c'] = OSI.Id;
                                cloned['hasSupply'] = true;
                            }
                            accumulator.push(cloned);
                        }
                        return accumulator;
                    }, [])
                } else {
                    this.createNewConsumptions(OSI,update);
                }
            }
        });
    }

     /*Luca Ravicini: validate total consumption to be equal to OSI estimated one*/
    @api
    validateConsumptionsData() {
        let valid =true;
        let saved=true;
        if(!this.commodityElectric){
           this.validate=true;
           return;
        }
        if((this.osiList && this.osiList.length !== this.outputOsiOrSupplyList.length) ||
            (this.supplyList && this.supplyList.length !== this.outputOsiOrSupplyList.length)){
            this.generateData();
        }
        this.outputOsiOrSupplyList.forEach(OSI => {
            let osiEstimatedConsumption=this.calculateTotalEstiamted(OSI.Id);
            let diff = osiEstimatedConsumption - OSI.EstimatedConsumption__c;
            if(Math.abs(diff) > 0.01){
                this.update(OSI.Id);
                valid=false;
            }
            if(!(OSI.Consumption && OSI.Consumption.length > 0 && OSI.Consumption[0].Id)){
                saved=false;
            }
        });

        let validation ={valid: valid,saved: saved};
        const validateEvent = new CustomEvent("validate", { detail: {validation:validation}});
        this.dispatchEvent(validateEvent);
    }
    /*Luca Ravicini: update OSI estimated consumption*/
    updateEstimatedConsumption(osiId,editEvent) {
        let  estimatedConsumption= this.calculateTotalEstiamted(osiId);
       console.log("estimatedConsumption==="+estimatedConsumption);
        notCacheableCall({
            className: 'MRO_LC_Consumption',
            methodName: 'updateEstimatedConsumption',
            input: {
                osiId: osiId,
                estimatedConsumption: estimatedConsumption

            },
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else if(response.error){
                    error(this, response.errorMsg);
                }
                else  {
                    this.dispatchEvent(editEvent);
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }
    createNewConsumptions(OSI,update) {
        let estimated;
        let estimatedConsumption;
        let estimatedInt;
        if(this.osiList){
            estimated=OSI.EstimatedConsumption__c;
            estimatedConsumption=OSI.EstimatedConsumption__c;
        }else {
            estimated=OSI.ServicePoint__r.EstimatedConsumption__c;
            estimatedConsumption=OSI.ServicePoint__r.EstimatedConsumption__c;
        }
        if(!estimated){
            estimated=0;
            estimatedConsumption=0;
        }
        estimated = Number(estimated);
        if(this.isGas === true){
            estimated = estimated.toFixed(3);
        } else {
            if (this.type === this.productRateTypeDual){
                // estimated = (estimated / 24).toFixed(3); //before
                estimated = (estimated / 12).toFixed(3); //after
            }
            else{
                estimated = (estimated / 12).toFixed(3);
            }
        }

        let consumption ;
        let consumptionDay ;
        let consumptionNight ;
        let estimatedDay;
        let estimatedDayInt;
        let estimatedNight;
        let estimatedNightInt;
        /* Luca Ravicini modified: to reserve old consumption Id to aviod duplication
        * */
        let oldConsumptions={};
        if(update && this._consumptions) {
            this._consumptions.forEach(consum => {
                if(consum.Id && ((this.osiList && OSI.Id === consum.OpportunityServiceItem__c)
                   || (this.supplyList && consum.Supply__c === OSI.Id))){
                    let key=OSI.Id+consum.Type__c;
                    oldConsumptions[key] = consum.Id;
                }
            });
        }
        let elementType=OSI.RecordType.Name ||  OSI.RecordType.DeveloperName;
        if(this.isGas === true){
            consumption = {
                Commodity__c: elementType,
                QuantityJanuary__c: (estimated * this.gasRating.Jan__c).toFixed(2) ,
                QuantityFebruary__c: (estimated * this.gasRating.Feb__c).toFixed(2) ,
                QuantityMarch__c: (estimated * this.gasRating.Mar__c).toFixed(2),
                QuantityApril__c: (estimated * this.gasRating.Apr__c).toFixed(2),
                QuantityMay__c: (estimated * this.gasRating.May__c).toFixed(2),
                QuantityJune__c: (estimated * this.gasRating.Jun__c).toFixed(2),
                QuantityJuly__c: (estimated * this.gasRating.Jul__c).toFixed(2),
                QuantityAugust__c: (estimated * this.gasRating.Aug__c).toFixed(2),
                QuantitySeptember__c: (estimated * this.gasRating.Sep__c).toFixed(2),
                QuantityOctober__c: (estimated * this.gasRating.Oct__c).toFixed(2),
                QuantityNovember__c: (estimated * this.gasRating.Nov__c).toFixed(2),
                QuantityDecember__c: (estimated * this.gasRating.Dec__c).toFixed(2)
            };

        } else {
            if (this.isContractualDataChange === true || this.roundConsumption === true) {
                /*estimatedInt = Math.floor(Number(estimated));
                consumption = {
                    Commodity__c: elementType,
                    QuantityJanuary__c: estimatedInt.toFixed(3),
                    QuantityFebruary__c: estimatedInt.toFixed(3),
                    QuantityMarch__c: estimatedInt.toFixed(3),
                    QuantityApril__c: estimatedInt.toFixed(3),
                    QuantityMay__c: estimatedInt.toFixed(3),
                    QuantityJune__c: estimatedInt.toFixed(3),
                    QuantityJuly__c: estimatedInt.toFixed(3),
                    QuantityAugust__c: estimatedInt.toFixed(3),
                    QuantitySeptember__c: estimatedInt.toFixed(3),
                    QuantityOctober__c: estimatedInt.toFixed(3),
                    QuantityNovember__c: estimatedInt.toFixed(3),
                    QuantityDecember__c: this.type === this.productRateTypeDual ? (estimatedInt + (Math.round(Number((Number(estimatedConsumption) / 2))) - (estimatedInt * 12))).toFixed(3) :
                                                                                  (estimatedInt + (Math.round(Number(estimatedConsumption)) - (estimatedInt * 12))).toFixed(3)
                };*/ //before
                estimatedInt = Math.floor(Number(estimated));//after
                estimatedDay = ((estimated / 100) * 30).toFixed(3);
                estimatedNight = ((estimated / 100) * 70).toFixed(3);
                estimatedDayInt = Math.floor(Number(estimatedDay));
                estimatedNightInt = Math.floor(Number(estimatedNight));
                if(this.type === this.productRateTypeDual){
                    consumptionDay = {
                        Commodity__c: elementType,
                        QuantityJanuary__c: estimatedDayInt,
                        QuantityFebruary__c: estimatedDayInt,
                        QuantityMarch__c: estimatedDayInt,
                        QuantityApril__c: estimatedDayInt,
                        QuantityMay__c: estimatedDayInt,
                        QuantityJune__c: estimatedDayInt,
                        QuantityJuly__c: estimatedDayInt,
                        QuantityAugust__c: estimatedDayInt,
                        QuantitySeptember__c: estimatedDayInt,
                        QuantityOctober__c: estimatedDayInt,
                        QuantityNovember__c: estimatedDayInt,
                        QuantityDecember__c: ((((Number(estimatedConsumption)) / 100) * 30) - (estimatedDayInt * 11)),
                        hasOsi: false,
                        hasSupply: false
                    };
                    consumptionNight = {
                        Commodity__c: elementType,
                        QuantityJanuary__c: estimatedNightInt,
                        QuantityFebruary__c: estimatedNightInt,
                        QuantityMarch__c: estimatedNightInt,
                        QuantityApril__c: estimatedNightInt,
                        QuantityMay__c: estimatedNightInt,
                        QuantityJune__c: estimatedNightInt,
                        QuantityJuly__c: estimatedNightInt,
                        QuantityAugust__c: estimatedNightInt,
                        QuantitySeptember__c: estimatedNightInt,
                        QuantityOctober__c: estimatedNightInt,
                        QuantityNovember__c: estimatedNightInt,
                        QuantityDecember__c: ((((Number(estimatedConsumption)) / 100) * 70) - (estimatedNightInt * 11)),
                        hasOsi: false,
                        hasSupply: false
                    };
                    if(this.osiList){
                        consumptionDay['OpportunityServiceItem__c']=OSI.Id;
                        consumptionNight['OpportunityServiceItem__c']=OSI.Id;
                        consumptionDay['hasOsi']=true;
                        consumptionNight['hasOsi']=true;

                    }else {
                        consumptionDay['Supply__c']=OSI.Id;
                        consumptionNight['Supply__c']=OSI.Id;
                        consumptionDay['hasSupply']=true;
                        consumptionNight['hasSupply']=true;
                    }
                    if(this.caseId){
                        consumptionDay['Case__c']=this.caseId;
                        consumptionNight['Case__c']=this.caseId;
                    }
                } else {
                    consumption = {
                        Commodity__c: elementType,
                        QuantityJanuary__c: estimatedInt,
                        QuantityFebruary__c: estimatedInt,
                        QuantityMarch__c: estimatedInt,
                        QuantityApril__c: estimatedInt,
                        QuantityMay__c: estimatedInt,
                        QuantityJune__c: estimatedInt,
                        QuantityJuly__c: estimatedInt,
                        QuantityAugust__c: estimatedInt,
                        QuantitySeptember__c: estimatedInt,
                        QuantityOctober__c: estimatedInt,
                        QuantityNovember__c: estimatedInt,
                        QuantityDecember__c: ((Number(estimatedConsumption)) - (estimatedInt * 11))
                    };
                }
            } else {
                /*consumption = {
                    Commodity__c: elementType,
                    QuantityJanuary__c: estimated,
                    QuantityFebruary__c: estimated,
                    QuantityMarch__c: estimated,
                    QuantityApril__c: estimated,
                    QuantityMay__c: estimated,
                    QuantityJune__c: estimated,
                    QuantityJuly__c: estimated,
                    QuantityAugust__c: estimated,
                    QuantitySeptember__c: estimated,
                    QuantityOctober__c: estimated,
                    QuantityNovember__c: estimated,
                    QuantityDecember__c: estimated
                };*/ //before
                estimatedDay = (estimated / 100) * 30; //after
                estimatedNight = (estimated / 100) * 70;
                if(this.type === this.productRateTypeDual){
                    consumptionDay = {
                        Commodity__c: elementType,
                        QuantityJanuary__c: estimatedDay,
                        QuantityFebruary__c: estimatedDay,
                        QuantityMarch__c: estimatedDay,
                        QuantityApril__c: estimatedDay,
                        QuantityMay__c: estimatedDay,
                        QuantityJune__c: estimatedDay,
                        QuantityJuly__c: estimatedDay,
                        QuantityAugust__c: estimatedDay,
                        QuantitySeptember__c: estimatedDay,
                        QuantityOctober__c: estimatedDay,
                        QuantityNovember__c: estimatedDay,
                        QuantityDecember__c: estimatedDay,
                        hasOsi: false,
                        hasSupply: false
                    };
                    consumptionNight = {
                        Commodity__c: elementType,
                        QuantityJanuary__c: estimatedNight,
                        QuantityFebruary__c: estimatedNight,
                        QuantityMarch__c: estimatedNight,
                        QuantityApril__c: estimatedNight,
                        QuantityMay__c: estimatedNight,
                        QuantityJune__c: estimatedNight,
                        QuantityJuly__c: estimatedNight,
                        QuantityAugust__c: estimatedNight,
                        QuantitySeptember__c: estimatedNight,
                        QuantityOctober__c: estimatedNight,
                        QuantityNovember__c: estimatedNight,
                        QuantityDecember__c: estimatedNight,
                        hasOsi: false,
                        hasSupply: false
                    };
                    if(this.osiList){
                        consumptionDay['OpportunityServiceItem__c']=OSI.Id;
                        consumptionNight['OpportunityServiceItem__c']=OSI.Id;
                        consumptionDay['hasOsi']=true;
                        consumptionNight['hasOsi']=true;

                    }else {
                        consumptionDay['Supply__c']=OSI.Id;
                        consumptionNight['Supply__c']=OSI.Id;
                        consumptionDay['hasSupply']=true;
                        consumptionNight['hasSupply']=true;
                    }
                    if(this.caseId){
                        consumptionDay['Case__c']=this.caseId;
                        consumptionNight['Case__c']=this.caseId;
                    }
                } else {
                    consumption = {
                        Commodity__c: elementType,
                        QuantityJanuary__c: estimated,
                        QuantityFebruary__c: estimated,
                        QuantityMarch__c: estimated,
                        QuantityApril__c: estimated,
                        QuantityMay__c: estimated,
                        QuantityJune__c: estimated,
                        QuantityJuly__c: estimated,
                        QuantityAugust__c: estimated,
                        QuantitySeptember__c: estimated,
                        QuantityOctober__c: estimated,
                        QuantityNovember__c: estimated,
                        QuantityDecember__c: estimated
                    };
                }
            }
        }

        if(this.isGas === true || (this.isGas === false && (this.isContractualDataChange === true || this.roundConsumption === true) && this.type != this.productRateTypeDual) || (this.isGas === false && (this.isContractualDataChange === false || this.roundConsumption === false) && this.type != this.productRateTypeDual)){
            consumption['hasOsi']=false;
            consumption['hasSupply']=false;
            if(this.osiList){
                consumption['OpportunityServiceItem__c']=OSI.Id;
                consumption['hasOsi']=true;

            }else {
                consumption['Supply__c']=OSI.Id;
                consumption['hasSupply']=true;
            }
            if(this.caseId){
                consumption['Case__c']=this.caseId;
            }
        }
        if (this.type === this.productRateTypeSingle) {
            if(OSI.RecordType.Name === this.commodityElectric){
                this.measurement = ' [kWh]';
            }else if(OSI.RecordType.Name === this.commodityGas){
                this.measurement = ' [MWh]';
            }
            consumption.myId = OSI.Id + '_1';
            consumption.Type__c = this.consumptionTypeRateSingle;

            if(update){
                let consumptionId=oldConsumptions[OSI.Id+this.consumptionTypeRateSingle];
                if(consumptionId){
                    consumption.Id= consumptionId;
                    consumption.myId = consumption.Id;
                }

            }
            OSI.Consumption.push(consumption);
        } else if (this.type === this.productRateTypeDual && elementType === this.commodityElectric) {
            this.measurement = ' [kWh]';
            //let dayRateConsumption = JSON.parse(JSON.stringify(consumption)); //before
            let dayRateConsumption = JSON.parse(JSON.stringify(consumptionDay)); //after
            dayRateConsumption.Type__c = this.consumptionTypeRateDay;
            dayRateConsumption.myId = OSI.Id + '_1';
            /*if(this.isContractualDataChange === true || this.roundConsumption === true){
                consumption['QuantityDecember__c']= (estimatedInt + (Number((Number(estimatedConsumption) / 2)).toFixed(0) - (estimatedInt * 12))).toFixed(3);
            }*/
            //let nightRateConsumption = JSON.parse(JSON.stringify(consumption)); //before
            let nightRateConsumption = JSON.parse(JSON.stringify(consumptionNight)); //after
            nightRateConsumption.Type__c =  this.consumptionTypeRateNight;
            nightRateConsumption.myId = OSI.Id + '_2';
            if(update){
                let dayRateConsumptionId=oldConsumptions[OSI.Id+this.consumptionTypeRateDay];
                if(dayRateConsumptionId){
                    dayRateConsumption.Id= dayRateConsumptionId;
                    dayRateConsumption.myId= dayRateConsumption.Id;
                }
                let nightRateConsumptionId=oldConsumptions[OSI.Id+this.consumptionTypeRateNight];
                if(nightRateConsumptionId){
                    nightRateConsumption.Id= nightRateConsumptionId;
                    nightRateConsumption.myId= nightRateConsumption.Id;
                }

            }
            OSI.Consumption.push(dayRateConsumption, nightRateConsumption);
        }
    }

    handleEditClick(event) {
        let consumptionId = event.currentTarget.dataset.id;
        let osiId = event.currentTarget.dataset.osiId;
        let currentOSI = this.outputOsiOrSupplyList.find(element => element.Id === osiId);
        this.selectedConsumption = currentOSI.Consumption.find(element => element.myId === consumptionId);
        if(this.osiList){
            this.selectedOsiServicePointCode = currentOSI.ServicePointCode__c;
        }else {
            this.selectedOsiServicePointCode = currentOSI.ServicePoint__r.Name;
        }
        this.editMode = true;
    }

    handleEditCancel(event) {
        this.editMode = false;
    }

    handleEditSave(event) {
        let consumptionDetails = event.detail.consumptionDetails;
        let consumptionId = event.detail.consumptionId;
        let elementId;
        if(!this.osiList){
            elementId = consumptionDetails.Supply__c;
        }else {
            elementId = consumptionDetails.OpportunityServiceItem__c;
        }
        // let osiId = consumptionId.split('_')[0];

        this.editMode = false;
        consumptionDetails = JSON.parse(JSON.stringify(consumptionDetails));
        let currentOsi = this.outputOsiOrSupplyList.find(element => element.Id === elementId);
        let currentConsumption = currentOsi.Consumption.find(element => element.myId === consumptionId);

        Object.keys(consumptionDetails).forEach(element=>{
            currentConsumption[element] = consumptionDetails[element];
        });

        const editEvent = new CustomEvent("consumption_edit_save", { detail: event.detail});
        if(this.updateOsiEstimatedConsumption){
            this.updateEstimatedConsumption(elementId,editEvent);
        }else {
            this.dispatchEvent(editEvent);
        }

    }

    /*Luca Ravicini calculate total consumption for certain OSI or all*/
    @api
    calculateTotalEstiamted(osiId){
        this.allConsumptionObjects=this.getAllConsumptionObjects();
        let totalEstimated=0.00;

        this.allConsumptionObjects.forEach((consum) => {
            if(consum && (!osiId || (osiId && osiId === consum.OpportunityServiceItem__c))){
                totalEstimated = totalEstimated + Number(consum.QuantityJanuary__c )+ Number(consum.QuantityFebruary__c) + Number(consum.QuantityMarch__c)+
                    + Number(consum.QuantityApril__c) + Number(consum.QuantityMay__c) + Number(consum.QuantityJune__c) + Number(consum.QuantityJuly__c)
                    + Number(consum.QuantityAugust__c) + Number(consum.QuantitySeptember__c) + Number(consum.QuantityOctober__c)
                    + Number(consum.QuantityNovember__c) + Number(consum.QuantityDecember__c);

            }

        });
        return totalEstimated;
    }

    getConstants() {
        cacheableCall({
            className: "MRO_UTL_Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {

                        this.productRateTypeSingle=apexResponse.data.PRODUCT_RATE_TYPE_SINGLE;
                        this.productRateTypeDual=apexResponse.data.PRODUCT_RATE_TYPE_DUAL;
                        this.consumptionTypeRateSingle=apexResponse.data.CONSUMPTION_TYPE_SINGLE_RATE;
                        this.consumptionTypeRateDay=apexResponse.data.CONSUMPTION_TYPE_DAY_RATE;
                        this.consumptionTypeRateNight=apexResponse.data.CONSUMPTION_TYPE_NIGHT_RATE;
                        this.commodityGas=apexResponse.data.COMMODITY_GAS
                        this.commodityElectric=apexResponse.data.COMMODITY_ELECTRIC;
                        this.gasRating=apexResponse.data.CONSUMPTION_GAS_RATING;
                        this.generateData();
                        if(this.validate){
                            this.validateConsumptionsData();
                        }

                    }
                }
            });
    }

    isChangedEstimatedConsumption(updatedOsiList, oldOsiValue) {
        let isChanged = false;
        let osiEstimatedConsumption = this.calculateTotalEstiamted(oldOsiValue.Id);

        updatedOsiList.forEach(OSI => {
            if(OSI.Id === oldOsiValue.Id){
                let diff;
                if(OSI.EstimatedConsumption__c){
                    diff = OSI.EstimatedConsumption__c - osiEstimatedConsumption;
                } else if(OSI.ServicePoint__r.EstimatedConsumption__c){
                    diff = OSI.ServicePoint__r.EstimatedConsumption__c - osiEstimatedConsumption;
                }
                if(Math.abs(diff) > 2){
                    isChanged = true;
                }
            }
        });
        return isChanged;

    }
}