import {LightningElement, track, api} from 'lwc';
import {labels} from "c/mroLabels";
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class MroSupplyShortInfo extends LightningElement {
    labels = labels;
    productType = this.labels.single;

    @track supply = null;
    @track ExpiresIn = null;
    @track supplyContract = null;
    @track ContractEndDate = null;

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    isNullOrEmpty(variable) {
        return (variable === undefined || variable === null || variable.length === 0);
    }

    monthsInBetween(d1, d2) {
        let months;
        months = d2.getMonth() - d1.getMonth() + (12 * (d2.getFullYear() - d1.getFullYear()))

        if (d2.getDate() > d1.getDate() ? months++ : months);

        return months;
    }

    @api get supplyJson() {
        return this.supply;
    }

    set supplyJson(e) {
        if (!this.IsJsonString(e)) {
            console.log('supply is empty');
            return;
        }

        this.supply = JSON.parse(e);

        if (this.isNullOrEmpty(this.supply.Product__r)) {
            console.warn('empty product');
            return;
        }

        this.productType = this.supply.Product__r.Name.includes('Bundle') ? this.labels.bundle : this.labels.single;
    }

    connectedCallback() {
        if (this.supply) {
            let contractId = this.supply.Contract__c;
            let inputs = {contractId: contractId};
            notCacheableCall({
                className: 'MRO_LC_PartnerServiceWizard',
                methodName: 'getContractById',
                input: inputs
            }).then((response) => {
                this.supplyContract = response.data.contract;
                this.ContractEndDate = response.data.contractEndDate;

                if (this.isNullOrEmpty(this.ContractEndDate)) {
                    console.error('Empty contract end date');
                    return;
                }

                let todayDate = new Date(Date.now());
                let terminationDate = new Date(this.ContractEndDate);

                this.ExpiresIn = this.monthsInBetween(todayDate, terminationDate);
            }).catch((err) => {
                console.log(err);
            });
        }
    }
}
