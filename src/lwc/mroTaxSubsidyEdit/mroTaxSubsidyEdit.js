/**
 * Created by David Diop on 07.11.2019.
 */

import {LightningElement, api, track} from 'lwc';
import {error, success, warn} from 'c/notificationSvc';
import {labels} from 'c/mroLabels';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class mroTaxSubsidyEdit extends LightningElement {
    labels = labels;
    @api supply;
    @api supplyList;
    @api taxSubsidyList;
    @api taxSubsidyToUpdateList;

    @api accountId;
    @api accountRecord;
    @api dossierId;
    @api parentCase;
    @api origin;
    @api channel;
    @api isCreditManagement;
    @api requestype;
    /*@api caseId;*/
    @api taxSubsidyId;whereClause
    @api isPersonAccountRecord;

    @api isUpdateRequestType = false;
    @api isTaxToUpdate = false;
    @api isDisabled = false;

    @track currentSupply;
    @track showLoadingSpinner;
    @track recordTypeNameList = [this.labels.danubeDelta,this.labels.greenCertificates,this.labels.heatingSubvention,this.labels.miners,
        this.labels.payingSupplyExcise,this.labels.ministryEnergyQuota,this.labels.selfProducers,this.labels.vat];
    @track recordType;
    @track recordTypeName;
    @track recordTypeList = [];
    @track supplyListValid = [];
    @track supplyListInValid = [];
    @track requiredEndDate;
    @track requiredStartDate;
    @track showInstitutionFields;
    @track isValidRecordType = false;
    @track inValidRecordType = false;
    @track message = false;
    @track showAmountEnergy = false;
    @track showAmountPercent = false;
    @track subventionId;
    @track supplyRecordTypeId;
    @track showOption;
    @track startInputDate;
    @track endInputDate;

    @track heatingSubventionRT;
    @track ministryEnergyQuotaRT;
    @track danubeDeltaRT;
    @track minersRT;
    @track payingSupplyExciseRT;
    @track greenCertificatesRT;
    @track vatRT;
    @track selfProducersRT;
    @track distributionRT;

    @track heatingSubvention = false;
    @track danubeDelta = false;
    @track status;
    @track today;
    @track subventionInValid = [];
    @track amountEnergy ;
    @track provinceList = [];
    @track institutions = [];
    @track institutionsList = [];
    @track defaultInstitutionListMiner = [];
    @track defaultInstitutionListEnergy = [];
    @track showInstitutionMiner = false;
    @track showInstitutionEnergy = false;
    @track agenceList = [];
    @track isAgenceList = false;
    @track institutionSelected;
    @track defaultInstitution;
    @track communeSelected;
    @track agenceSelected;
    @track agenceSelectedList =  [];
    @track provinceSelected = '';
    @track localitySelected = '';
    @track showAgence;
    @track showLocality;
    @track showInstitution = true;
    @track showInstitutionDefault = true;
    @track showInstitutionNone = true;
    @track retroActive = false;
    @track retroActiveDate;
    @track activeStatus;
    @track errorValidSupply = '';
    @track errorInvalidDateheating = '';
    @track InvalidDateheating = false;
    @track ErrorFieldDate = '';

    @track defaultStartDate ;
    @track defaultEndDate ;
    @track InvalidUpdateDate = false;
    @track errorInvalidUpdateDate = '';

    connectedCallback() {
        this.showLoadingSpinner = true;
        if (this.supplyList) {
            this.currentSupply = this.supplyList[0];
            this.supplyListValid = this.supplyList;
        }
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        today = yyyy + '-' + mm + '-' + dd;
        this.today = today;
        notCacheableCall({
            className: "MRO_LC_TaxSubsidyEdit",
            methodName: "getSubventionType"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.supplyRecordTypeId = apexResponse.data.supplyElectricRecord;

                        this.heatingSubventionRT = apexResponse.data.Heating_subvention.value;
                        this.ministryEnergyQuotaRT = apexResponse.data.Ministry_Energy_Quota.value;
                        this.danubeDeltaRT = apexResponse.data.Danube_Delta.value;
                        this.minersRT = apexResponse.data.Miners.value;
                        this.payingSupplyExciseRT = apexResponse.data.Paying_supply_excise.value;
                        this.greenCertificatesRT = apexResponse.data.Green_certificates.value;
                        this.vatRT = apexResponse.data.VAT.value;
                        this.selfProducersRT = apexResponse.data.Self_producers.value;

                        this.status = apexResponse.data.activatingStatus;
                        this.activeStatus = apexResponse.data.activeStatus;
                        //this.distributionRT = apexResponse.data.Distribution.value;
                        this.showOption = true;
                        for (let name in apexResponse.data) {
                            if (this.recordTypeNameList.includes(name)) {
                                this.recordTypeList.push(apexResponse.data[name]);
                            }
                        }
                        this.institutions = apexResponse.data.institutions;
                        if (apexResponse.data.institutions.length !== 0) {
                            this.institutionsList = this.institutions;
                            let listPro = [];
                            apexResponse.data.institutions.forEach(institution => {
                                if (institution.PayableSubventions__c) {
                                    let payableSubventionList = institution.PayableSubventions__c.split(';');
                                    payableSubventionList.forEach(payable => {
                                        if (payable === this.labels.danubeDeltaInstitutions) {
                                            this.defaultInstitution = institution;
                                        }
                                        if (payable === this.labels.minersInstitutions) {
                                            this.defaultInstitutionListMiner.push(institution);
                                        }
                                        if (payable === this.labels.ministryEnergyQuotaInstitution) {
                                            this.defaultInstitutionListEnergy.push(institution);
                                        }
                                    });

                                /*if(institution.Name === 'MINISTERUL ENERGIEI I.M.M. SI MEDIULUI DE AFACERI'){
                                    this.defaultInstitution = institution;
                                }*/
                                /*if(institution.Name === 'SOCIETATEA NATIONALA DE INCHIDERI MINE VALEA JIULUI S.A.'){
                                    this.defaultInstitutionListMiner.push(institution);
                                }*/
                                }
                                if (institution.ResidentialProvince__c) {
                                    if (institution.PayableSubventions__c) {
                                        let payableSubventionList = institution.PayableSubventions__c.split(';');
                                        payableSubventionList.forEach(payable => {
                                            if (payable === this.labels.heatingSubventionInstitutions) {
                                                if (!listPro.includes(institution.ResidentialProvince__c)) {
                                                    listPro.push(institution.ResidentialProvince__c);
                                                    this.provinceList.push(
                                                        {
                                                            label: institution.ResidentialProvince__c,
                                                            value: institution.ResidentialProvince__c
                                                        });
                                                }
                                            }
                                        });
                                    }
                                }
                                //else {
                                    //this.institutionsList.push(institution);

                                //}
                            });
                        }
                        //this.selectProvince(this.institutions[0].ResidentialProvince__c);
                        if (!this.taxSubsidyId) {
                            this.showAmountPercent = false;
                            this.isValidRecordType = true;
                            this.recordType = this.danubeDeltaRT;
                            this.danubeDelta = true;
                            if (this.supplyList.length > 1) {
                                let recordTypeLists = [];
                                for (let j = 0; j < this.recordTypeList.length; j++) {
                                    if ((this.recordTypeList[j].value !== this.minersRT) &&
                                        (this.recordTypeList[j].value !== this.ministryEnergyQuotaRT) &&
                                        (this.recordTypeList[j].value !== this.danubeDeltaRT) &&
                                        (this.recordTypeList[j].value !== this.heatingSubventionRT)
                                    ) {
                                        recordTypeLists.push(this.recordTypeList[j]);
                                    }
                                }
                                this.recordTypeList = recordTypeLists;
                                this.recordType = this.greenCertificatesRT;
                            }
                            this.handleVerification();
                        } else {
                            this.supplyList = [];
                            this.subventionRecord();
                        }
                    }
                }
            });
    }

    handleChangeProvince(event){
        let province = event.target.value;
        this.showAgence = true;
        this.selectProvince(province);
    }
    handleChangeLocality(event){
        /*let locality = event.target.value;*/
        this.localitySelected = event.target.value;
        this.showLocality = true;

    }
    handleChangeInstitution(event){
        if (this.recordType === this.heatingSubventionRT) {
            this.communeSelected = event.target.value;
            this.agenceList = [];
            this.institutions.forEach(institution => {
                if(this.communeSelected === institution.Id){
                    if (institution.PaymentAgency__c) {
                        this.agenceList.push(
                            {
                                label: institution.PaymentAgency__r.Name,
                                value: institution.PaymentAgency__c
                            }
                        );
                    }
                }
            });

        }
        if (this.recordType === this.minersRT || this.recordType === this.ministryEnergyQuotaRT) {
            this.institutionSelected= event.target.value;
        }

        this.showInstitutionDefault = false;
        if (this.communeSelected) {
            this.showInstitutionNone = false;
            this.isAgenceList = true;
        }

    }
    selectProvince(province){
        this.isAgenceList = false;
        this.provinceSelected = province;
        let agenceList = [];
        this.agenceSelectedList = [];
        this.institutions.forEach(institution => {
            if(province === institution.ResidentialProvince__c){
                if (institution.PayableSubventions__c) {
                    let payableSubventionList = institution.PayableSubventions__c.split(';');
                    payableSubventionList.forEach(payable => {
                        if (payable === this.labels.heatingSubventionInstitutions) {
                            this.agenceSelectedList.push(institution);
                        }
                    });
                }

                //this.agenceSelected = institution;
                //this.agenceSelectedList.push(institution);
                this.agenceList = [];
                /* if (institution.PaymentAgency__c) {
                      if (!agenceList.includes(institution.PaymentAgency__c)){
                          agenceList.push(institution.PaymentAgency__c);
                      }
                      //communeList = institution.ResidentialLocality__c.split(',');
                  } */
                if (institution.PaymentAgency__c) {
                    agenceList.push(institution);
                }
            }
        });

        if (agenceList) {
            agenceList.forEach(institution => {
                this.agenceList.push(
                    {
                        label: institution.PaymentAgency__r.Name,
                        value: institution.PaymentAgency__c
                    });
            });
            this.agenceSelected = this.agenceList[0].label;
            //this.isAgenceList = true;
            if(this.communeSelected){
                this.isAgenceList = true;
            }else{
                this.showInstitutionNone = true;
            }
            if(this.taxSubsidyId){
                if (this.communeSelected) {
                    if (this.provinceSelected !== this.communeSelected.ResidentialProvince__c) {
                        this.showInstitutionDefault = false;
                        this.showInstitutionNone = true;
                        //this.communeSelected = '';
                        this.isAgenceList = false;
                    }
                }
            }

        }
    }

    handleLoad() {
        /*if (!this.taxSubsidyId) {
            this.showAmountPercent = false;
            this.isValidRecordType = true;
            this.recordType = this.danubeDeltaRT;

            this.handleVerification();
        } else {
            this.supplyList = [];
            this.subventionRecord();
        }*/
    }

    subventionRecord() {
        console.log('this.supplyList' + this.supplyList);
        let inputs = {recordId: this.taxSubsidyId};
        notCacheableCall({
            className: "MRO_LC_TaxSubsidyEdit",
            methodName: "getSubventionRecord",
            input: inputs
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.recordType = apexResponse.data.subventionRecord.RecordTypeId;
                        this.recordTypeName = apexResponse.data.subventionRecord.RecordType.Name;
                        this.amountEnergy = apexResponse.data.subventionRecord.AmountEnergy__c;
                        this.defaultStartDate = apexResponse.data.subventionRecord.StartDate__c;
                        this.defaultEndDate = apexResponse.data.subventionRecord.EndDate__c;
                        this.currentSupply = apexResponse.data.supply;
                        if (apexResponse.data.institution.length !== 0) {
                            if (this.recordType === this.heatingSubventionRT) {
                                if(apexResponse.data.institution.length !== 0){
                                    this.communeSelected = apexResponse.data.institution[0];
                                    this.selectProvince(apexResponse.data.institution[0].ResidentialProvince__c);
                                }
                                //this.selectProvince(apexResponse.data.institution[0].ResidentialProvince__c);
                            }
                            if (this.recordType === this.minersRT || this.recordType === this.ministryEnergyQuotaRT) {
                                this.institutionSelected = apexResponse.data.institution[0];

                            }
                        }
                        this.handleVerification();
                    }
                }
            });
    }


    handleCancel() {
        this.taxSubsidyId = '';
        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }

    handleSubvention(event) {
        this.showLoadingSpinner = true;
        this.recordType = event.target.value;
        if (this.taxSubsidyId) {
            this.showOption = false;
        }
        this.handleVerification();

    }
    handleDate(event){
       if(event.target.dataset.id === 'startDate'){
           this.template.querySelector('[data-id="startDate"]').classList.remove('slds-has-error');
       }
        if(event.target.dataset.id === 'endDate'){
            this.template.querySelector('[data-id="endDate"]').classList.remove('slds-has-error');
        }
    }

    handleVerification() {
        this.showLoadingSpinner = true;
        if (!this.taxSubsidyId) {
            this.changeFieldValue('EndDate__c', '');
        }


        if (this.recordType === this.heatingSubventionRT) {
            this.heatingSubvention = true;
            this.showAmountPercent = false;
            this.showAmountEnergy = false;
            this.danubeDelta = false;


            this.isValidRecordType = true;
        }
        if (this.recordType === this.ministryEnergyQuotaRT) {
            this.heatingSubvention = false;
            this.showAmountPercent = false;
            this.danubeDelta = false;
            this.showInstitutionEnergy = true;

            let isAccountControl = true;
            this.validateRecordType(isAccountControl);

        }
        if (this.recordType === this.danubeDeltaRT) {
            this.heatingSubvention = false;
            this.showAmountEnergy = true;
            this.showAmountPercent = false;
            this.danubeDelta = true;


            this.isValidRecordType = true;
        }
        if (this.recordType === this.minersRT) {
            this.heatingSubvention = false;
            this.showAmountPercent = false;
            this.showAmountEnergy = true;
            this.danubeDelta = false;
            this.showInstitutionMiner = true;

            let isAccountControl = false;
            this.validateRecordType(isAccountControl);
            if (!this.taxSubsidyId) {
                this.changeFieldValue('EndDate__c', '2999-12-31');
            }

        }
        if (this.recordType === this.payingSupplyExciseRT) {
            this.heatingSubvention = false;
            this.showAmountPercent = false;
            this.showAmountEnergy = false;
            this.danubeDelta = false;

            let isAccountControl = true;
            this.validateRecordType(isAccountControl);

        }
        if (this.recordType === this.greenCertificatesRT) {
            this.heatingSubvention = false;
            this.showAmountEnergy = false;
            this.danubeDelta = false;

            this.isValidRecordType = true;
        }
        if (this.recordType === this.vatRT) {
            this.heatingSubvention = false;
            this.showAmountPercent = false;
            this.showAmountEnergy = false;
            this.danubeDelta = false;

            this.isValidRecordType = true;
        }
        if (this.recordType === this.selfProducersRT) {
            this.heatingSubvention = false;

            this.showAmountPercent = false;
            this.showAmountEnergy = false;
            this.danubeDelta = false;

            let isAccountControl = false;
            this.validateRecordType(isAccountControl);
        }
        if (this.recordType === this.distributionRT) {
            this.heatingSubvention = false;

            this.showAmountPercent = false;
            this.showAmountEnergy = false;
            this.danubeDelta = false;

            this.isValidRecordType = true;
        }

        if (this.recordType === this.greenCertificatesRT) {
            this.showAmountPercent = true;
            this.showAmountEnergy = false;
            this.isValidRecordType = true;
            this.danubeDelta = false;
        }

        if (this.recordType === this.ministryEnergyQuotaRT) {
            this.showAmountEnergy = true;
            this.showAmountPercent = false;
            this.isValidRecordType = true;
            this.danubeDelta = false;
            this.showInstitutionMiner = false;
        }
        //test fields required
        if (this.recordType === this.heatingSubventionRT || this.recordType === this.ministryEnergyQuotaRT || this.recordType === this.payingSupplyExciseRT) {
            this.requiredEndDate = true;
            this.requiredStartDate = true;
        } else if (this.recordType !== this.danubeDeltaRT && this.recordType !== this.distributionRT) {
            this.requiredEndDate = false;
            this.requiredStartDate = true;
        } else {
            this.requiredEndDate = false;
            this.requiredStartDate = false;
        }
        this.showInstitutionFields = this.recordType === this.heatingSubventionRT || this.recordType === this.minersRT || this.recordType === this.ministryEnergyQuotaRT;
        // this.recordType === this.selfProducersRT
        this.showLoadingSpinner = false;

    }
    validateUpdateDate(){
        let startInputDate = this.template.querySelector('[data-id="startDate"]').value;
        let endInputDate = this.template.querySelector('[data-id="endDate"]').value;
        this.InvalidUpdateDate = false;
        if (this.isUpdateRequestType) {
            if (startInputDate < this.defaultStartDate) {
                this.InvalidUpdateDate = true;
                this.errorInvalidUpdateDate = this.labels.startDateForUpdateTax + '('+ this.defaultStartDate +')';
                return false;
            }
            if (endInputDate > this.defaultEndDate) {
                this.InvalidUpdateDate = true;
                this.errorInvalidUpdateDate = this.labels.EndDateForUpdateTax + '('+ this.defaultEndDate +')';
                return false;
            }
            return true;
        } else {
            return  true;
        }
    }

    validateDate() {
        let startInputDate = this.template.querySelector('[data-id="startDate"]').value;
        let endInputDate = this.template.querySelector('[data-id="endDate"]').value;
        this.InvalidUpdateDate = false;
        if (this.recordType === this.heatingSubventionRT) {
            let valid = this.validateUpdateDate();
            if (valid) {
                let dateOfTodayStar = new Date(startInputDate);
                let dateOfTodayEnd = new Date(endInputDate);
                let startDate = new Date(dateOfTodayStar.getFullYear(), dateOfTodayStar.getMonth(), 1);
                let endDate = new Date(dateOfTodayEnd.getFullYear(), dateOfTodayEnd.getMonth() + 1, 0);
                if (startDate.getDate() !== dateOfTodayStar.getDate()) {
                    this.InvalidDateheating = true;
                    this.errorInvalidDateheating = this.labels.startDayOfAMonth;
                    this.ErrorFieldDate = 'startDate';
                    return false;
                }
                if (endDate.getDate() !== dateOfTodayEnd.getDate()) {
                    this.InvalidDateheating = true;
                    this.errorInvalidDateheating = this.labels.lastDayOfAMonth;
                    this.ErrorFieldDate = 'endDate';
                    return false;
                }
                if (endInputDate < this.today) {
                    return false;
                } else if (this.requiredStartDate && this.requiredEndDate) {
                    return startInputDate <= endInputDate;
                } else if (this.requiredStartDate && !this.requiredEndDate && endInputDate != null) {
                    return startInputDate <= endInputDate;
                } else if (!this.requiredStartDate && !this.requiredEndDate && endInputDate != null && startInputDate != null) {
                    return startInputDate <= endInputDate;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            let valid = this.validateUpdateDate();
            /*startInputDate< this.today ||*/
            if (valid) {
                if (endInputDate < this.today) {
                    return false;
                } else if (this.requiredStartDate && this.requiredEndDate) {
                    return startInputDate <= endInputDate;
                } else if (this.requiredStartDate && !this.requiredEndDate && endInputDate != null) {
                    return startInputDate <= endInputDate;
                } else if (!this.requiredStartDate && !this.requiredEndDate && endInputDate != null && startInputDate != null) {
                    return startInputDate <= endInputDate;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        this.subventionInValid = [];
        this.showLoadingSpinner = true;
        this.InvalidDateheating = false;
        let fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });
        //test if fields is validate or
        let validate = this.validateField(fields);
        //fields added
        if (validate) {
            let validateDate = this.validateDate();
            if (!validateDate) {
                this.showLoadingSpinner = false;
                if(this.InvalidDateheating){
                    if(this.ErrorFieldDate === 'startDate'){
                    this.template.querySelector('[data-id="startDate"]').classList.add('slds-has-error');
                    }
                    if(this.ErrorFieldDate === 'endDate'){
                        this.template.querySelector('[data-id="endDate"]').classList.add('slds-has-error');
                    }
                    error(this, this.errorInvalidDateheating);
                } else if (this.isUpdateRequestType && this.InvalidUpdateDate) {
                    error(this, this.errorInvalidUpdateDate);
                } else {
                    this.template.querySelector('[data-id="startDate"]').classList.add('slds-has-error');
                    this.template.querySelector('[data-id="endDate"]').classList.add('slds-has-error');
                    error(this, this.labels.invalidRangeDate);
                }
            } else {
                this.template.querySelector('[data-id="startDate"]').classList.remove('slds-has-error');
                this.template.querySelector('[data-id="endDate"]').classList.remove('slds-has-error');
                fields.RecordTypeId = this.recordType;
                //fields.Supply__c = this.currentSupply.Id;
                /*if (!this.isValidRecordType) {
                    this.showLoadingSpinner = false;
                    error(this, (this.message));
                    return;
                } else {*/

                //this.showLoadingSpinner = false;
                //insert new TaxSubsidy__c object
                fields.Status__c = this.status;
                /*fields.Case__c = this.caseId;*/
                fields.Customer__c = this.accountId;

                if (fields.StartDate__c === null) {
                    fields.StartDate__c = this.today;
                }
                if (fields.EndDate__c === null && this.recordType === this.danubeDeltaRT) {
                    fields.EndDate__c = '2999-12-31';
                }
                if (this.taxSubsidyId) {
                    fields.Supply__c = this.currentSupply.Id;
                    if(this.recordType === this.heatingSubventionRT){
                        if (this.communeSelected) {
                            fields.Institution__c = this.communeSelected.Id;
                        }
                        for (let j = 0; j < this.institutions.length; j++) {
                            if (this.institutions[j].Id === this.communeSelected) {
                                if(!this.institutions[j].PaymentAgency__c){
                                    this.showLoadingSpinner = false;
                                    error(this, this.labels.missingPaymentAgency);
                                    return;
                                }
                            }
                        }
                        if(fields.AmountCurrency__c > 10000){
                            this.showLoadingSpinner = false;
                            error(this, this.labels.amountMaxValue);
                            return;
                        }
                    }
                    if(this.recordType === this.minersRT || this.recordType === this.ministryEnergyQuotaRT){
                        if (this.institutionSelected) {
                            fields.Institution__c = this.institutionSelected.Id;
                        }
                    }
                    if (this.retroActive) {

                        let retroDate = this.template.querySelector('[data-id="retroDate"]').value;
                        if (retroDate === "" || retroDate === null) {
                            this.template.querySelector('[data-id="retroDate"]').classList.add('slds-has-error');
                            this.showLoadingSpinner = false;
                            return;
                        }
                        else {
                            this.retroActiveDate = retroDate;
                            this.template.querySelector('[data-id="retroDate"]').classList.remove('slds-has-error');
                        }

                    }
                    if (!this.isValidRecordType) {
                        this.showLoadingSpinner = false;
                        error(this, (this.message));
                        return;
                    } else if (!this.validateSupplies()) {
                        this.showLoadingSpinner = false;
                        error(this, (this.errorValidSupply));

                        return;

                    } else {
                        if (this.taxSubsidyList && this.taxSubsidyList.length > 1) {
                            for (let j = 0; j < this.taxSubsidyList.length; j++) {
                                if (this.taxSubsidyList[j].RecordType.Id !== this.recordType) {
                                    this.subventionInValid.push(this.taxSubsidyList[j]);
                                }
                            }
                        }

                        if (this.subventionInValid.length !== 0 && !this.isUpdateRequestType) {
                            error(this, this.labels.subventionTypes);
                            this.showLoadingSpinner = false;
                            return;
                        } else {
                            let startInputDate = this.template.querySelector('[data-id="startDate"]').value;
                            if (startInputDate < this.today) {
                                if (!this.isCreditManagement && !this.isUpdateRequestType) {
                                    warn(this, this.labels.subventionAssigned);
                                    this.showLoadingSpinner = false;
                                    return;
                                }
                            }
                            if (this.isUpdateRequestType) {
                                const subventionInValid = [];
                                const subventionAlreadyEdit = [];
                                let validSubvention = true;
                                if (this.taxSubsidyToUpdateList && this.taxSubsidyToUpdateList.length > 0) {
                                    if (this.taxSubsidyList && this.taxSubsidyList.length > 0) {
                                        for (let j = 0; j < this.taxSubsidyToUpdateList.length; j++) {
                                            for (let i = 0; i < this.taxSubsidyList.length; i++) {
                                                if (this.taxSubsidyList[i].Case__c === this.taxSubsidyToUpdateList[j].Case__r.TechnicalDetails__c && this.taxSubsidyList[i].Id === this.taxSubsidyId) {
                                                    subventionAlreadyEdit.push(this.taxSubsidyToUpdateList[j]);
                                                }
                                                if (this.taxSubsidyList[i].RecordType.Id !== this.recordType) {
                                                    subventionInValid.push(this.taxSubsidyList[i]);
                                                }
                                            }
                                        }
                                            if (subventionInValid && subventionInValid.length > 0) {
                                                for (let k = 0; k < subventionInValid.length; k++) {
                                                    if (subventionInValid[k].Id === this.taxSubsidyId) {
                                                        validSubvention = false;
                                                    }
                                                }
                                            }
                                            /*if (this.taxSubsidyToUpdateList[j].RecordType.Id !== this.recordType) {
                                                 subventionInValid.push(this.taxSubsidyToUpdateList[j]);
                                            }*/

                                    }
                                }
                                if (subventionAlreadyEdit.length !== 0) {
                                    error(this, this.labels.subventionAlreadyEdit);
                                    this.showLoadingSpinner = false;
                                    return;
                                } else if (subventionInValid.length !== 0 && !this.isTaxToUpdate && !validSubvention) {
                                    error(this, this.labels.subventionTypes);
                                    this.showLoadingSpinner = false;
                                    return;
                                } else {
                                    if (this.recordType === this.ministryEnergyQuotaRT) {
                                        let defaultAmountEnergy = 1200;
                                        let selectedStartDate = new Date(this.template.querySelector('[data-id="startDate"]').value);
                                        let selectedEndDate = new Date(this.template.querySelector('[data-id="endDate"]').value);
                                        if (selectedStartDate.getFullYear() === selectedEndDate.getFullYear()) {
                                            if(this.amountEnergy){
                                                defaultAmountEnergy = 1200 -  this.amountEnergy;
                                                if (fields.AmountEnergy__c > defaultAmountEnergy) {
                                                    error(this, this.labels.amountGreaterThanQuota + '  ' +defaultAmountEnergy);
                                                    this.showLoadingSpinner = false;
                                                    return;
                                                    //fields.AmountEnergy__c = defaultAmountEnergy;
                                                } else {
                                                    this.createCase(fields);
                                                }
                                            } else {
                                                if(fields.AmountEnergy__c > 1200){
                                                    error(this, this.labels.amountGreaterThanQuota + '  ' +defaultAmountEnergy);
                                                    this.showLoadingSpinner = false;
                                                    return;
                                                } else {
                                                    this.createCase(fields);
                                                }
                                            }

                                        }else {
                                            if(fields.AmountEnergy__c > 1200){
                                                error(this, this.labels.amountGreaterThanQuota + '  ' +defaultAmountEnergy);
                                                this.showLoadingSpinner = false;
                                                return;
                                            } else {
                                                this.createCase(fields);
                                            }
                                        }
                                    }else {
                                        if (this.isTaxToUpdate) {
                                            this.template.querySelector("lightning-record-edit-form").submit(fields);
                                            this.taxSubsidyId = "";
                                        } else {
                                            this.createCase(fields);
                                        }
                                    }
                                }
                            } else {
                                this.template.querySelector("lightning-record-edit-form").submit(fields);
                                this.taxSubsidyId = "";
                            }
                        }

                        /*if (this.taxSubsidyList && this.taxSubsidyList.length > 1) {

                            let startInputDate = this.template.querySelector('[data-id="startDate"]').value;
                            if(startInputDate < this.today ){
                                warn(this,this.labels.subventionAssigned);
                            }
                            this.template.querySelector("lightning-record-edit-form").submit(fields);
                            this.taxSubsidyId = '';
                        } else {
                            let startInputDate = this.template.querySelector('[data-id="startDate"]').value;
                            if(startInputDate < this.today ){
                                warn(this,this.labels.subventionAssigned);
                            }
                            this.template.querySelector("lightning-record-edit-form").submit(fields);
                            this.taxSubsidyId = '';
                        }*/
                    }
                } else {
                    if (this.supplyListInValid.length !== 0) {
                        this.showLoadingSpinner = false;
                        warn(this, (this.message));
                    }
                    if (!this.validateSupplies()) {
                        this.showLoadingSpinner = false;
                        error(this, (this.errorValidSupply));
                        return;
                    }
                    if(this.recordType === this.heatingSubventionRT){
                        if (this.communeSelected) {
                            fields.Institution__c = this.communeSelected;
                        }
                        for (let j = 0; j < this.institutions.length; j++) {
                            if (this.institutions[j].Id === this.communeSelected) {
                                if(!this.institutions[j].PaymentAgency__c){
                                    this.showLoadingSpinner = false;
                                    error(this, this.labels.missingPaymentAgency);
                                    return;
                                }
                            }
                        }
                        if(fields.AmountCurrency__c > 10000){
                            this.showLoadingSpinner = false;
                            error(this, this.labels.amountMaxValue);
                            return;
                        }
                    }
                    if (this.retroActive) {
                        let retroDate = this.template.querySelector('[data-id="retroDate"]').value;
                        if (retroDate === "" || retroDate === null) {
                            this.template.querySelector('[data-id="retroDate"]').classList.add('slds-has-error');
                            this.showLoadingSpinner = false;
                            return;
                        }
                        else {
                            warn(this, this.labels.subventionAssigned);
                            this.retroActiveDate = retroDate;
                            this.template.querySelector('[data-id="retroDate"]').classList.remove('slds-has-error');
                        }
                    }
                    if (this.recordType === this.minersRT || this.recordType === this.ministryEnergyQuotaRT) {
                        if (this.institutionSelected) {
                            fields.Institution__c = this.institutionSelected;
                        }
                    }
                    if(this.recordType === this.danubeDeltaRT){
                        fields.Institution__c = this.defaultInstitution.Id;
                    }
                    if (this.supplyListValid.length !== 0) {
                        let startInputDate = this.template.querySelector('[data-id="startDate"]').value;
                        if (startInputDate < this.today) {
                            if (!this.isCreditManagement) {
                                warn(this, this.labels.invalidRangeDate);
                                this.showLoadingSpinner = false;
                                return;
                            }
                        }
                        if (this.taxSubsidyList && this.taxSubsidyList.length !== 0) {
                            for (let j = 0; j < this.taxSubsidyList.length; j++) {
                                if (this.taxSubsidyList[j].RecordType.Id !== this.recordType) {
                                    this.subventionInValid.push(this.taxSubsidyList[j]);
                                }
                            }
                        }
                        //for (let j = 0; j < this.supplyListValid.length; j++) {
                            //fields.Supply__c = this.supplyListValid[j].Id;
                            if (this.subventionInValid.length !== 0 && !this.isUpdateRequestType) {
                                error(this, this.labels.subventionTypes);
                                this.showLoadingSpinner = false;
                                return;
                            }
                            if (this.recordType === this.ministryEnergyQuotaRT) {
                                if(fields.AmountEnergy__c > 1200){
                                    error(this, this.labels.amountGreaterThanQuota  + '  ' + '1200');
                                    this.showLoadingSpinner = false;
                                    return;
                                } else {
                                    this.createCase(fields);
                                }
                            } else {
                                this.createCase(fields);
                            }
                            /*if(this.taxSubsidyList && this.taxSubsidyList.length !== 0){
                                if(this.taxSubsidyList[0].RecordType.Id !== this.recordType){
                                    error(this,this.labels.subventionTypes);
                                    this.showLoadingSpinner = false;
                                    return;
                                }
                            }*/

                        //}
                    }
                }
                //}
            }
        } else {
            error(this, this.labels.requiredFields);
            this.showLoadingSpinner = false;
            return;
        }
    }

    createTaxSubsidy(fields) {
        this.showLoadingSpinner = true;
        let inputs = {fields: JSON.stringify(fields)};
        notCacheableCall({
            className: "MRO_LC_TaxSubsidyEdit",
            methodName: "createTaxSubsidy",
            input: inputs
        }).then(apexResponse => {
            if (apexResponse) {
                if (apexResponse.isError) {
                    error(this, apexResponse.error);
                    this.showLoadingSpinner = false;
                } else {
                    this.showLoadingSpinner = false;
                    const closeEvent = new CustomEvent('save');
                    this.dispatchEvent(closeEvent);

                }
            }
        });
    }

    createCase(fields) {
        this.showLoadingSpinner = true;
        let inputs = {
            dossierId: this.dossierId,
            parentCaseId: this.parentCase,
            origin: this.origin,
            channel: this.channel,
            accountId: this.accountId,
            requestType : this.requestype,
            supplyId : this.currentSupply.Id,
            suppliesList: JSON.stringify(this.supplyListValid),
        };
        notCacheableCall({
            className: "MRO_LC_TaxSubsidyEdit",
            methodName: "createCaseToTaxSubsidy",
            input: inputs
        }).then(apexResponse => {
            if (apexResponse) {
                if (apexResponse.isError) {
                    error(this, apexResponse.error);
                    this.showLoadingSpinner = false;
                } else {
                    this.showLoadingSpinner = false;
                    //fields.Case__c = apexResponse.data.caseRecord.Id;
                    if (this.isUpdateRequestType) {
                        this.status = this.activeStatus;
                        fields.Case__c = apexResponse.data.casesList[0].Id;
                        fields.Status__c = this.status;
                        this.createTaxSubsidy(fields);
                    } else {
                        for (let j = 0; j < apexResponse.data.casesList.length; j++) {
                            fields.Case__c = apexResponse.data.casesList[j].Id;
                            fields.Supply__c = apexResponse.data.casesList[j].Supply__c;
                            console.log('####fields ' + JSON.stringify(fields));
                            if (this.recordType === this.minersRT) {
                                fields.Status__c = this.activeStatus;
                            }
                            console.log('####fields ' + JSON.stringify(fields));

                            this.template.querySelector('lightning-record-edit-form').submit(fields);
                        }
                    }
                }
            }
        });
    }

    handleSuccess(event) {
        this.taxSubsidyId = '';
        this.showLoadingSpinner = false;
        let taxSubsidyId = event.detail.id;
        if(this.retroActive){
            window.localStorage;
            localStorage.setItem(taxSubsidyId,this.retroActiveDate);
        }
        const closeEvent = new CustomEvent('save', {
            detail: {
                retroActive : this.retroActive,
                taxSubsidyId : taxSubsidyId
            }
        });
        this.dispatchEvent(closeEvent);
    }

    validateField(fields) {
        let validate = true;
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.required && (fields[element.fieldName] === "" || fields[element.fieldName] == null)) {
                element.classList.add('slds-has-error');
                validate = false
            }
        });
        return validate;
    }

    validateRecordType(isAccountControl) {
        this.supplyListValid = [];
        this.supplyListInValid = [];
        let recordType;
        if (this.taxSubsidyId) {
            recordType = this.currentSupply.RecordTypeId;
            if (isAccountControl) {
                if (this.isPersonAccountRecord && recordType === this.supplyRecordTypeId) {
                    this.isValidRecordType = true;
                } else {
                    if (this.recordType === this.payingSupplyExciseRT) {
                        if (this.isPersonAccountRecord && recordType !== this.supplyRecordTypeId) {
                            this.isValidRecordType = false;
                            this.message = this.labels.accountSupplyNotValid;
                        } else {
                            this.isValidRecordType = true;
                        }
                    } else {
                        this.isValidRecordType = false;
                        this.message = this.labels.accountSupplyNotValid;
                    }
                }
            } else if (recordType === this.supplyRecordTypeId) {
                this.isValidRecordType = true;
            } else {
                this.isValidRecordType = false;
                this.message = this.labels.supplyNotValid;
            }
        } else {
            for (let i = 0; i < this.supplyList.length; i++) {
                if (isAccountControl) {
                    if (this.isPersonAccountRecord && this.supplyList[i].RecordType["Id"] === this.supplyRecordTypeId) {
                        this.supplyListValid.push(this.supplyList[i]);
                    } else {
                        if (this.recordType === this.payingSupplyExciseRT) {
                            if (this.isPersonAccountRecord && this.supplyList[i].RecordType["Id"] !== this.supplyRecordTypeId) {
                                this.supplyListInValid.push(this.supplyList[i]);
                                this.message = this.labels.accountSupplyNotValid;
                            } else {
                                this.supplyListValid.push(this.supplyList[i]);
                            }
                        } else {
                            this.supplyListInValid.push(this.supplyList[i]);
                            this.message = this.labels.accountSupplyNotValid;
                        }
                    }
                } else if (this.supplyList[i].RecordType["Id"] === this.supplyRecordTypeId) {
                    this.supplyListValid.push(this.supplyList[i]);
                } else {
                    this.supplyListInValid.push(this.supplyList[i]);
                    this.message = this.labels.supplyNotValid;
                }
            }
        }
    }

    validateSupplies() {
        let isValidSelection = true;
        if ((this.recordType === this.minersRT) || (this.recordType === this.ministryEnergyQuotaRT) || (this.recordType === this.danubeDeltaRT)) {
            if (!this.isPersonAccountRecord) {
                isValidSelection = false;
                this.errorValidSupply = this.labels.suppliesFromRegulatedMarket
            } else {
                if ((this.supplyList.length > 0) && (this.supplyList[0].Market__c !== 'Regulated') && (!this.accountRecord.ResidentialAddress__c)) {
                    isValidSelection = false;
                    this.errorValidSupply = this.labels.suppliesFromRegulatedMarket
                }
            }
        }
        if(this.recordType === this.greenCertificatesRT){
            for(let i = 0; i < this.supplyList.length; i++){
                if(this.supplyList[i].RecordType["Id"] !== this.supplyRecordTypeId){
                    isValidSelection = false;
                    this.errorValidSupply = this.labels.greenCerticatesForGasSupplies;
                }
            }
        }
        return isValidSelection;
    }

    handleRetroActiveDate(event){
        let retroActive = event.target.checked;
        this.retroActive = retroActive;
    }

    /*validateAllRecordType(isAccountControl) {
        for (let i = 0; i < this.supplyList.length; i++) {
            if (isAccountControl) {
                if (this.isPersonAccountRecord && this.supplyList[i].RecordType["Id"] === this.supplyRecordTypeId) {
                    this.supplyListValid.push(this.supplyList[i]);
                } else {
                    this.supplyListInValid.push(this.supplyList[i]);
                    this.message = this.labels.accountSupplyNotValid;
                }
            } else if (this.supplyList[i].RecordType["Id"] === this.supplyRecordTypeId) {
                this.supplyListValid.push(this.supplyList[i]);
            } else {
                this.supplyListInValid.push(this.supplyList[i]);
                this.message = this.labels.supplyNotValid;
            }
        }
    }*/

    /**
     * @author Boubacar Sow
     * @description change element fields value .
     *              [ENLCRO-1010] Tax changes - Process Changes
     * @param fieldName
     */
    changeFieldValue(fieldName, value) {
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.fieldName === fieldName) {
                element.value = value;
            }
        });
    }


    /**
     * @author Boubacar Sow
     * @description make the field read only
     *              [ENLCRO-1010] Tax changes - Process Changes
     * @param fieldName
     */
    readOnlyField(fieldName, value) {
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if (element.fieldName === fieldName) {
                element.disabled = value;
            }
        });
    }

}