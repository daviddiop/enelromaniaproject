import { LightningElement, api, track } from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import { NavigationMixin } from 'lightning/navigation';
import { error } from 'c/notificationSvc';
import { labels } from 'c/labels';

export default class BillingProfileEdit extends NavigationMixin(LightningElement) {
    billingProfile;
    labels = labels;
    @api billingProfileRecordId;
    @api accountId = '';
    @api buttonType = '';
    @api showHeader = false;
    @api showModal = false;
    @api readOnly = false;
    @api addressForcedBilling = false;
    @track giroRecordTypeId;
    @track postalOrderRecordTypeId;
    @track recordTypeOptions = [];
    @track recordTypeId = '';
    @track disabled = true;
    @track selectedRecordType = '';
    @track address;
    @track show = true;
    @track isFax = '';
    @track isIban = '';
    @track isEmail = '';
    @track isNormalize;
    @track isRequiredAddressField = true;
    @track showLoadingSpinner = true;
    @track disableNormalize = false;

    connectedCallback() {
        this.showLoadingSpinner = false;
        this.isNormalize = true;
        if (this.isNewView) {
            this.billingProfileRecordId = '';
            this.address = '';
            this.show = true;

        }else{
            this.fillAddressFields();
        }
    }

    handleLoadBP() {
        this.recordTypeEditId();
    }
    renderedCallback() {
        if (!this.billingProfileRecordId) {
            this.show = true;
            this.showLoadingSpinner = false;
        }
    }

    fillAddressFields() {
        let inputs = {billingProfileRecordId: this.billingProfileRecordId};
        notCacheableCall({
            className: 'BillingProfileCnt',
            methodName: 'getAddressFields',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }

                let addr = {};
                addr = response.data.billingAddress;
                this.address = addr;
                this.show = true;
                if(addr.addressNormalized){
                    this.readOnly = true;
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    recordTypeEditId() {
        if (this.buttonType !== 'new') {
            let deliveryChannelCmp = this.template.querySelector('[data-id="myDeliveryChannel"]');
            let paymentMethod = this.template.querySelector('[data-id="paymentMethodVal"]');
            if (deliveryChannelCmp) {
                if (deliveryChannelCmp.value === 'Fax') {
                    this.isRequiredAddressField = true;
                    this.disableNormalize = true;
                    this.isNormalize = true;
                    this.isFax = 'wr-required';
                    this.isEmail = '';
                } else if (deliveryChannelCmp.value === 'Email') {
                    this.isRequiredAddressField = true;
                    this.disableNormalize = true;
                    this.isNormalize = true;
                    this.isEmail = 'wr-required';
                    this.isFax = '';
                } else {
                    this.disableNormalize = false;
                    this.isRequiredAddressField = false;
                    this.isNormalize = false;
                    this.isEmail = '';
                    this.isFax = '';
                }
            }

            if (paymentMethod){
                if (paymentMethod.value === 'Giro') {
                    this.isIban = 'wr-required';
                } else {
                    this.isIban = '';
                }
            }
        }
    }
    get isNewView() {
        return this.buttonType === 'new';

    }
    handleCancel() {
        this.dispatchEvent(new CustomEvent('close'));

    }
    handleErrorBP(event) {
        error(this, event.detail.message);
        this.showLoadingSpinner = false;
    }
    handleSuccessBP(event) {
        event.preventDefault();
        event.stopPropagation();
        let payload = event.detail;
        const selectedEvent = new CustomEvent('save', {
            detail: {
                billingProfileEditId: payload.id
            }
        });
        this.dispatchEvent(selectedEvent);
        this.showLoadingSpinner = false;
    }

    handleSubmitBP(event) {
        event.preventDefault();
        event.stopPropagation();
        this.showLoadingSpinner = true;
        const fields = {};
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            fields[element.fieldName] = element.value;
        });

        let addrForm = this.template.querySelector('c-address-form');

        if (!addrForm.isValid()) {
            error(this, this.labels.addressMustBeVerified);
            this.showLoadingSpinner = false;
            return;
        }

        let addrFields = addrForm.getValues();
        for (let f in addrFields) {
            if (addrFields.hasOwnProperty(f)) {
                fields[f] = addrFields[f];
            }
        }

        if (this.validateFields()) {

            if (this.buttonType === 'clone') {

                this.billingProfileRecordId = '';
                fields.Account__c = this.accountId;
                this.template.querySelector('lightning-record-edit-form').recordId = this.billingProfileRecordId;
            }

            fields.BillingAddressNormalized__c = this.addressForcedBilling === false;
            this.template.querySelector('lightning-record-edit-form').submit(fields);


        }else {
            this.showLoadingSpinner = false;
        }
    }

    removeError(event) {
        let inputCmp = event.target;

        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
    }

    paymentMethodChange(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName === 'PaymentMethod__c') {

            if (inputCmp.value === 'Giro') {
                this.isIban = 'wr-required';
            } else {
                this.isIban = '';
            }
        }
        this.removeError(event);
    }

    deliveryChannelChange(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName === 'DeliveryChannel__c') {

            if (inputCmp.value === 'Fax') {
                this.isRequiredAddressField = true;
                this.disableNormalize = true;
                this.isNormalize = true;
                this.isFax = 'wr-required';
                this.isEmail = '';

            } else if (inputCmp.value === 'Email') {
                this.isRequiredAddressField = true;
                this.disableNormalize = true;
                this.isNormalize = true;
                this.isEmail = 'wr-required';
                this.isFax = '';

            } else {

                this.disableNormalize = false;
                this.isRequiredAddressField = false;
                this.isNormalize = false;
                this.isEmail = '';
                this.isFax = '';
            }
        }
        this.removeError(event);
    }

    disabledSaveButton(event){
        this.isNormalize = event.detail.isnormalize;
    }
    disabledSaveButtonBillingAddress(event){
        this.isNormalize = event.detail.isnormalize;
        this.addressForcedBilling = event.detail.addressForced;
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        return areValid;
    };

}