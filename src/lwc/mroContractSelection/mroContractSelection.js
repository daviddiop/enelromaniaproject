/**
 * Created by goudiaby on 11/09/2019.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';

export default class MroContractSelection extends LightningElement {
    CHANNELS_WITHOUT_SALESUNIT = ['Back Office Sales', 'Back Office Customer Care', 'MyEnel', 'Enel.ro'];
    @api accountId;
    @api showCustomerSignedDate = false;
    @api companyDivisionId;
    @api commodity;
    @api market;
    @api excludeDraft = false;
    @api contractId;
    @api excludeContractIds;
    @api disabled =false;
    @api hideNew = false;
    @api hideList = false; //hide list of available contracts and auto check New Contract icon
    @api contractSignedDate;
    @api contractType;
    @api contractName;
    @api companySignedId;
    @api salesUnit;
    @api showUserDepartment = false;
    @api salesUnitId;
    @api salesman;
    @api contractTerm = 12;
    @api contractStartDate;
    //AS [ENLCRO-730] Product Change
    @api isMarketChange= false;
    @api isBusinessClient = false;
    @api isProductChange = false;
    @api fixedContract = false;
    //AS [ENLCRO-669] Contract Account Selection - Check Interface
    //For Sub-Process Public Tender the contract type should be always business  and markt = Free
    @api hasSubProcessPublicTender = false;
    // For Sub-Process Public Tender the contract type should be always business  and markt = Free
    @api tariffType;
    @api isContractTypeDisabled = false;
    @api hideName=false;
    @api showContractTerm = false;
    @api showContractStartDate = false;
    @api enableSalesman = false;
    @api contractStartDateRequired = false;
    @api isContractNameDisabled = false;
    @api enableSalesUnit = false;
    @api disableContractEditing = false;
    @api commodityToExclude = '';
    @api marketToExclude;

    @track contract;
    @track contracts;
    @track customerSignedDate;
    @track contractNameOnContract;
    @track companySignedIdOnContract;
    @track salesUnitIdOnContract;
    @track salesmanOnContract;
    @track isNew = false;
    @track selectedContract;
    @track hasRendered = false;
    @track disableInput = false;
    @track existingContract = false;
    labels = labels;
    @track contractValue;
    @track contractSignedDateValue;
    @track isReloaded = false;
    @track editContractFields = false;
    @track showSalesMan = false;
    @track showCompanySignedBy = false;
    @track showMarket=false;
    @track showCompanyDivision=false;
    @track disableSalesUnit = false;
    @track disableCompanySignedBy = false;
    @track showSalesUnitSearch = false;
    @track showCompanySignedBySearch = false;
    @track showSalesmanSearch = false;
    @track showSalesmanOnContract = false;
    @track showCompanySignedIdOnContract;
    @track selectedSalesUnit;
    @track selectedUser;
    @track selectedSalesman;
    @track selectedRecord;
    @track selectedSalesUnitCode;
    @track isChannelWithoutSalesUnit = false;
    @track isEditBoxDisabled = false;
    @track isEditBoxClosed = false;
    _salesChannel;

    get querySalesUnit(){
        return "RecordType.DeveloperName = 'SalesUnit' AND SalesDepartment__c = '"+this._salesChannel+"'";
    }

    get queryCompanySignedId(){
        if(this.salesUnit && !this.selectedSalesUnit){
            return 'SalesUnitID__c = \''+ this.salesUnit.SalesUnitID__c + '\'';
        } else if(this.selectedSalesUnit){
            return 'SalesUnitID__c = \''+ this.selectedSalesUnitCode + '\'';
        //} else if(this._salesChannel === 'Direct Sales (KAM)' && ((!this.salesUnit && !this.selectedSalesUnit && this.showUserDepartment) || (!this.companySignedId && !this.showUserDepartment))){
        } else if(this._salesChannel === 'Direct Sales (KAM)' && !this.salesUnit && !this.salesUnitId && !this.selectedSalesUnit){
            return "Id IN (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'KAMUserGroup')";
        } else {
            return '';
        }
    }

    get querySalesman(){
        if(this.salesUnitId && !this.selectedSalesUnit){
            return 'AccountId = \''+this.salesUnitId+'\'';
        } else if(this.selectedSalesUnit){
            return 'AccountId = \''+this.selectedSalesUnit+'\'';
        } else {
            return '';
        }
    }

    get disableSalesmanInput() {
        if(this.disableInput){
            return this.disableInput;
        } else {
            if(this.selectedSalesUnit != null){
                return false;
            } else {
                return true;
            }
        }
    }

    get disableCompanySignedInput() {
        if(this.disableInput){
            return this.disableInput;
        } else {
            if(this.selectedSalesUnit != null || this.selectedSalesUnit != undefined){
                return false;
            }
            //if(this._salesChannel === 'Direct Sales (KAM)' && ((!this.salesUnit && !this.selectedSalesUnit && this.showUserDepartment) || (!this.companySignedId && !this.showUserDepartment))){
            if(this._salesChannel === 'Direct Sales (KAM)' && ((!this.salesUnit && !this.salesUnitId && !this.selectedSalesUnit) || (!this.salesUnit && !this.selectedSalesUnit && this.salesUnitId && this.showUserDepartment))){
                return false;
            }
            if(this.isChannelWithoutSalesUnit && this.editContractFields){
                return false;
            }
            else {
                return true;
            }
        }
    }

    get disableContractNameInput() {
        if(this.disableInput){
            return this.disableInput;
        } else {
            if(this.isContractNameDisabled !== false){
                return true;
            } else {
                return false;
            }
        }
    }

    @api
    getContract() {
        return this.isNew ? 'new' : (this.selectedContract ? this.selectedContract.Id : null);
    }

    @api isEmptyContractList(){
        if(this.isProductChange){
            this.getContracts();
            return this.contracts.length === 0;
        }
    }

    @api
    validate(){
        // If sales unit field is shown, it has to be filled
        if (this.showSalesUnitSearch && this.isNullOrEmpty(this.selectedSalesUnit)){
            return false;
        }

        // If salesman field is shown, it has to be filled
        if (this.showSalesmanSearch && this.isNullOrEmpty(this.selectedSalesman)){
            return false;
        }

        return true;
    }

    @api
    getNewContractDetails() {
        let salesUnitIdFieldValue;
        let companySignedIdFieldValue;
        let salesmanFieldValue;
        const contractNameField = this.template.querySelector('[data-id="contractNameField"]');
        const contractTypeField = this.template.querySelector('[data-id="contractTypeField"]');
        const signedDateField = this.template.querySelector('[data-id="signedDateField"]');
        const companySignedIdField = this.template.querySelector('[data-id="companySignedIdField"]');
        const salesUnitIdField = this.template.querySelector('[data-id="salesUnitIdField"]');
        const salesmanField = this.template.querySelector('[data-id="salesmanField"]');
        const contractTermField = this.template.querySelector('[data-id="ContractTermField"]');
        const contractStartDateField = this.template.querySelector('[data-id="ContractStartDateField"]');
        if(salesUnitIdField){
            salesUnitIdFieldValue = salesUnitIdField.value;
        }
        else{
            salesUnitIdFieldValue = this.selectedSalesUnit;
        }
        if(companySignedIdField){
            companySignedIdFieldValue = companySignedIdField.value;
        }
        else{
            /*if (this._salesChannel !== 'Direct Sales (KAM)' && this._salesChannel !== 'Indirect Sales') {
                companySignedIdFieldValue = this.companySignedId;
            }*/
            if(this.isChannelWithoutSalesUnit){
                companySignedIdFieldValue = this.companySignedId;
            }else{
                companySignedIdFieldValue = this.selectedUser;
            }
        }
        if(salesmanField){
            salesmanFieldValue = salesmanField.value;
        }
        else{
            salesmanFieldValue = this.selectedSalesman;
        }
        return {
            name: contractNameField ? contractNameField.value : null,
            type: contractTypeField ? contractTypeField.value : null,
            customerSignedDate: signedDateField ? signedDateField.value : null,
            salesman: salesmanFieldValue ? salesmanFieldValue : null,
            companySignedId: companySignedIdFieldValue ? companySignedIdFieldValue : null,
            salesUnitId: salesUnitIdFieldValue ? salesUnitIdFieldValue : null,
            contractTerm: contractTermField ? contractTermField.value : null,
            contractStartDate: contractStartDateField ? contractStartDateField.value : null,
        }
    }


    /**
     * Converts the values of the Date fields to meet universal time standard,
     * it's to avoid string to date conversion errors on backend when calling Date.valueOf()
     * @return {object} a set of new values from the contract fields
     */
    @api getNewContractDetailsWithIsoDates() {
        const data = this.getNewContractDetails();
        data.customerSignedDate = (new Date(data.customerSignedDate)).toISOString();
        data.contractStartDate = (new Date(data.contractStartDate)).toISOString();
        return data;
    }

    @api
    get salesChannel() {
        return this._salesChannel;
    }
    set salesChannel(value) {
        this._salesChannel = value;
    }

    @api
    reloadContractList() {
        this.contractAddition();
        this.isReloaded = true;
    }

    @api
    setContractName(contractName){
        const contractNameField = this.template.querySelector('[data-id="contractNameField"]');
        if (!contractNameField){
            return;
        }

        contractNameField.value = contractName;
    }
    contractAddition (){
        this.customerSignedDate = new Date().toISOString().slice(0, 10);
        this.contractValue = this.contractId;
        this.contractSignedDateValue = this.contractSignedDate;
        if(this.contractValue){
            this.isNew = '';
        }

        if(!this.contractValue){
            let contractInputs = this.template.querySelectorAll('[data-id="checkBoxContract"]');
            contractInputs.forEach(inputElement => {
                inputElement.checked = false;
            });

            let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
            this.disableInput = false;
            this.existingContract=false;
            newContractCheckbox.checked = true;
        }

        if((this.contractSignedDateValue) || (this.contractValue)){
            this.customerSignedDate = this.contractSignedDateValue;
        }
        this.getContracts();
    }

    connectedCallback() {
        this.customerSignedDate = new Date().toISOString().slice(0, 10);
        if((this.contractSignedDateValue) || (this.contractValue)){
            this.customerSignedDate = this.contractSignedDateValue;
        }
        this.getContracts();
        this.isNew = !this.hideNew && !this.contractId;
    }

    renderedCallback() {
        if(this._salesChannel){
            this.isChannelWithoutSalesUnit = this.CHANNELS_WITHOUT_SALESUNIT.includes(this._salesChannel);
        }
        console.log('renderedCallback this.isChannelWithoutSalesUnit: '+this.isChannelWithoutSalesUnit);
        //if (this._salesChannel === 'Direct Sales (KAM)') {
        if (this._salesChannel !== 'Indirect Sales' && !this.isChannelWithoutSalesUnit) {
            this.disableSalesUnit = this.disableInput;
            this.disableCompanySignedBy = this.disableInput;
            this.showCompanySignedBy = true;
            if(this.isNew || (!this.isNew && !this.salesman)){
                this.showSalesMan = false;
                this.showSalesmanSearch = false;
            }

            if(!this.salesUnit){
                if(this.showUserDepartment && this.salesUnitId){
                    this.showSalesUnitSearch = false;
                }
                else{
                    this.showSalesUnitSearch = true;
                }
            }
            else{
                this.showSalesUnitSearch = false;
            }
            if(!this.companySignedId){
                this.showCompanySignedBySearch = true;
            }
            else{
                this.showCompanySignedBySearch = false;
            }
        }
        //else if (this._salesChannel === 'Indirect Sales' || this._salesChannel === 'Magazin Enel' || this._salesChannel === 'Magazin Enel Partner') {
        else if (this._salesChannel === 'Indirect Sales') {
            this.disableSalesUnit = this.disableInput;
            this.showSalesMan = true;
            if(this.isNew || (!this.isNew && !this.companySignedId)){
                this.showCompanySignedBy = false;
                this.showCompanySignedBySearch = false;
            }

            if(!this.salesUnit){
                this.showSalesUnitSearch = true;
            }
            if(!this.salesman){
                this.showSalesmanSearch = true;
            }
        }
        else { //this.isChannelWithoutSalesUnit = true
            this.disableSalesUnit = true;
            this.showSalesUnitSearch = false;
            this.showCompanySignedBy = true;
            /*if(this.isNew){
                this.disableCompanySignedBy = true;
                this.showCompanySignedBySearch = false;
            }
            else{
                this.showCompanySignedBySearch = true;
            }*/
            this.disableCompanySignedBy = true;
            this.showCompanySignedBySearch = false;

            if(this.isNew || (!this.isNew && !this.salesman)){
                this.showSalesMan = false;
            }
        }

        if (this.contracts && this.contracts.length && !this.hasRendered) {
            console.log(JSON.stringify(this.contracts));
            if (!this.isReloaded){
                this.contractAddition();
            }

            this.hasRendered = true;
            let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
            if (this.isNew) {
                this.disableInput = false;
                this.existingContract = false;
                newContractCheckbox.checked = true;
            }
            this.contractSelectionHelper(this.contractId);
            this.getContractEvent();
        }

        if (this.contractValue && this.contractValue !== 'new'){
            let contractInputs = this.template.querySelectorAll('[data-id="checkBoxContract"]');
            contractInputs.forEach(inputElement => {
                inputElement.checked = (inputElement.value === this.contractValue);
            });
            let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
            if(newContractCheckbox){
                this.existingContract = true;
                newContractCheckbox.checked = false;
            }
        }

        if (this.disabled){
            this.disableInput = true;
            this.isEditBoxDisabled = true;
            this.isEditBoxClosed = true;
        }
        else if (!this.selectedContract || !this.selectedContract.Id) {
            if (!this.contractValue && !this.isNew) {
                this.existingContract = true;
            } else if(this.isNew) {
                this.disableInput = false;
            } else {
                this.existingContract = false;
            }

            if (this.contractValue) {
                this.contractValue = '';
            }
        }else {
            if(this.editContractFields){
                this.editContractFields = true;
                this.disableInput = false;
                this.isEditBoxDisabled = true;
                this.isEditBoxClosed = false;
            }
            else{
                this.editContractFields = false;
                if(this.disableContractEditing){
                    this.isEditBoxDisabled = true;
                }
                else{
                    this.isEditBoxDisabled = false;
                }
                this.disableInput = true;
                this.isEditBoxClosed = true;
            }
        }
        if (this.isNew !== !this.hideNew && ((!this.contracts && !this.selectedContract))) {
            this.isNew = !this.hideNew;
            console.log('renderedCallback'+this.isNew);
        }
    }

    @api
    getContracts() {
        this.contracts = undefined;

        let inputs = {
            accountId: this.accountId,
            contractId: (this.contractId ? this.contractId : ''),
            contractIdList: (this.contractIdList ? this.contractIdList : ''),
            excludeContractIds: (this.excludeContractIds ? JSON.stringify(this.excludeContractIds) : null),
            companyDivisionId: this.companyDivisionId,
            commodity: this.commodity,
            market: this.market,
            //contractType: this.contractType,
            isProductChange: this.isProductChange,
            tariffType: this.tariffType,
            includeDraft: (this.excludeDraft ? 'false' : 'true'),
            commodityToExclude: this.commodityToExclude,
            marketToExclude: this.marketToExclude,
            fixedContract: this.fixedContract
        };
        notCacheableCall({
            className: 'MRO_LC_ContractSelection',
            methodName: 'getContractsByAccount',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, response.error);
                    return;
                }
                this.contracts = response.data.contracts;
                /**
                 * Luca Ravicini modification start for showing company division and market
                 * 13 Mar 2020
                 **/
                let contractsData= JSON.parse(JSON.stringify(this.contracts));
                for (let i = 0; i < contractsData.length; i++) {
                    let contract=contractsData[i];
                    let marketName = '';
                    if(contractsData[i].Supplies__r){
                        marketName=contract.Supplies__r[0].Market__c;
                    }
                    if(marketName){
                        this.showMarket=true;
                        contract['MarketName'] = marketName;
                        console.log("contract=="+JSON.stringify(contract));
                    }
                    if(contractsData[i].CompanyDivision__r){
                        this.showCompanyDivision=true;
                    }
                    if (contractsData[i].Id === this.contractId) {
                        this.selectedContract = contractsData[i];
                    }
                }
                this.contracts=Object.values(contractsData);
                /**
                 * Luca Ravicini modification end
                 **/
                console.log('Available contracts:', JSON.stringify(this.contracts));
            })
            .catch((err) => {
                error(this, JSON.stringify(err));
            });
    }

    handleContractSelection(event) {
        if (this.fixedContract) {
            return;
        }
        if (event.target.checked) {
            this.contractValue='';
            this.contractSelectionHelper(event.target.value);
            this.getContractEvent();

        }else{
            this.setNewContract();
            this.customerSignedDate = new Date().toISOString().slice(0, 10);
            const getCntrEvent = new CustomEvent('getcontract', {
                detail: {
                    newCustomerSignedDate: this.customerSignedDate,
                    newContractName: this.contractName,
                    newContractType: this.contractType,
                    newCompanySignedId: this.companySignedId,
                    newSalesman: this.salesman,
                    newSalesUnitId: this.salesUnitId,
                    newContractTerm: this.contractTerm,
                    newContractStartDate: this.contractStartDate,
                }
            });
            this.dispatchEvent(getCntrEvent);
        }

        let companyDivisionField = this.template.querySelector('[data-id="companyDivision"]');
        console.log("companyDivisionField=="+companyDivisionField);
        if(companyDivisionField){
            companyDivisionField.value= this.companyDivisionId;
        }
        let marketField = this.template.querySelector('[data-id="market"]');
        console.log("marketField=="+marketField);
        if(marketField){
            marketField.value=this.selectedContract.market;
        }

    }

    contractSelectionHelper(contractValue) {
        //this.contractValue='';
        if(this.contracts){
            this.selectedContract = this.contracts.find(function (contract) {
                return (contract.Id === contractValue);
            });
            this.editContractFields = false;
            this.isEditBoxClosed = true;
            this.isEditBoxDisabled = false;

            if (this.selectedContract) {
                console.log("contractSelectionHelper selectedContract: "+JSON.stringify(this.selectedContract));
                this.customerSignedDate = this.selectedContract["CustomerSignedDate"] ? this.selectedContract["CustomerSignedDate"] : null;
                this.market = this.selectedContract["MarketName"] ? this.selectedContract["MarketName"] : '';
                this.contractNameOnContract = this.selectedContract["Name"] ? this.selectedContract["Name"] : '';
                this.salesUnitIdOnContract = this.selectedContract["SalesUnit__c"] ? this.selectedContract["SalesUnit__c"] : null;
                this.companySignedIdOnContract = this.selectedContract["CompanySignedId"] ? this.selectedContract["CompanySignedId"] : null;
                this.salesmanOnContract = this.selectedContract["Salesman__c"] ? this.selectedContract["Salesman__c"] : null;
                if(this._salesChannel === 'Indirect Sales' || (this._salesChannel !== 'Indirect Sales' && this.salesmanOnContract != null)){
                    this.showSalesmanOnContract = true;
                }
                else{
                    this.showSalesmanOnContract = false;
                }
                if(this._salesChannel !== 'Indirect Sales' || (this._salesChannel === 'Indirect Sales' && this.companySignedIdOnContract != null)){
                    this.showCompanySignedIdOnContract = true;
                }
                else{
                    this.showCompanySignedIdOnContract = false;
                }

                if(this.editContractFields){
                    //this.contractName = this.contractNameOnContract;
                    //this.companySignedId = this.companySignedIdOnContract;
                    //this.salesman = this.salesmanOnContract;
                    this.contractName = '';
                    this.salesUnitId = null;
                    this.salesUnit = null;
                    this.selectedSalesUnit = null;
                    this.companySignedId = null;
                    this.salesman = null;

                    if(this.isChannelWithoutSalesUnit){
                        this.showSalesUnitSearch = false;
                        this.showCompanySignedBy = true;
                        this.showCompanySignedBySearch = false;
                        this.disableCompanySignedBy = true;
                        if(this.salesmanOnContract){
                            this.salesman = null;
                            this.showSalesMan = true;
                            this.showSalesmanSearch = false;
                        }
                    }
                    else{
                        /*this.salesUnitId = this.salesUnitIdOnContract;
                        this.salesUnit = this.salesUnitId;*/
                        /*this.showSalesUnitSearch = true;
                        this.showCompanySignedBySearch = true;*/

                        if(this.salesmanOnContract != null && this._salesChannel !== 'Indirect Sales'){
                            this.salesman = null;
                            this.showSalesMan = true;
                            this.showSalesmanSearch = false;
                        }
                        if(this.companySignedIdOnContract != null && this._salesChannel === 'Indirect Sales'){
                            this.companySignedId = null;
                            this.showCompanySignedBy = true;
                            this.showCompanySignedBySearch = false;
                        }

                    }

                }

                /*if(this.companySignedIdOnContract){
                    this.showCompanySignedBy = true;
                }
                */


                /*this.salesUnitId = this.selectedContract["SalesUnit__c"] ? this.selectedContract["SalesUnit__c"] : null;
                if(this.salesUnitId){
                    this.salesUnit = this.salesUnitId;
                    this.showSalesUnitSearch = false;
                }
                else {
                    this.showSalesUnitSearch = true;
                }
                this.companySignedId = this.selectedContract["CompanySignedId"] ? this.selectedContract["CompanySignedId"] : null;
                if(this.companySignedId){
                    this.showCompanySignedBy = true;
                    this.showCompanySignedBySearch = false;
                }
                else {
                    this.showCompanySignedBySearch = true;
                }
                this.salesman = this.selectedContract["Salesman__c"] ? this.selectedContract["Salesman__c"] : null;
                if(this.salesman){
                    this.showSalesMan = true;
                    this.showSalesmanSearch = false;
                }
                else {
                    this.showSalesmanSearch = true;
                }*/

                //this.disableInput = true;
                this.existingContract=true;
                this.isNew = false;
            } else {
                this.setNewContract();
            }
            this.checkSelectedContract(contractValue, this.isNew);
        }


    }

    setNewContract(){
        if((this.contractSignedDateValue) || (this.contractValue)){
            this.customerSignedDate = this.contractSignedDateValue;
        }
        this.selectedContract = null;
        this.isNew = true;
        this.disableInput = false;
        this.existingContract=false;
        let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
        newContractCheckbox.checked = this.isNew;
    }

    getContractEvent() {
        let consumptionConventions = null;
        if (this.selectedContract && this.selectedContract !=='new') {
            consumptionConventions = this.selectedContract.ConsumptionConventions__c;
            //AS  [ENLCRO-669] Contract Account Selection - Check Interface
            this.contractType = this.selectedContract.ContractType__c;
        }

        const getCntrEvent = new CustomEvent('getcontract', {
            detail: {
                selectedContract: this.getContract(),
                newCustomerSignedDate: this.customerSignedDate,
                consumptionConventions: consumptionConventions,
                contrContractType: this.contractType,
                newContractType: this.contractType,
                newCompanySignedId: this.companySignedId,
                newSalesman: this.salesman,
                newSalesUnitId: this.salesUnitId,
                newMarket: this.market
            }
        });
        this.dispatchEvent(getCntrEvent);
    }

    checkSelectedContract(contractId, isNewContract) {
        let newContractCheckbox = this.template.querySelector('[data-id="newContract"]');
        newContractCheckbox.checked = isNewContract;

        let contractInputs = this.template.querySelectorAll('[data-id="checkBoxContract"]');
        contractInputs.forEach(inputElement => {
            inputElement.checked = (inputElement.value === contractId);
        });

    }

    get classContractList() {
        if (!this.contracts) {
            return 'slds-size_10-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class';
        }
        if (this.hideNew) {
            if (this.disabled) {
                return 'slds-size_11-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class slds-theme_shade';
            }
            return 'slds-size_11-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class';
        }
        if (this.disabled) {
            return 'slds-size_10-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class slds-theme_shade';
        }
        return 'slds-size_10-of-12 slds-m-top_x-large slds-card_boundary slds-form-element slds-scrollable_y slds-class';
    }

    get classSelectedContractBox() {
        if (this.disabled) {
            return 'slds-grid slds-wrap slds-gutters_xx-small slds-align_absolute-center wr-pointer-event_none';
        }
        return 'slds-grid slds-wrap slds-gutters_xx-small slds-align_absolute-center';
    }

    get classGridNewContract() {
        if (this.hideNew) {
            return 'slds-hide slds-size_1-of-12 slds-m-horizontal_xx-small slds-align_absolute-center';
        }
        return 'slds-size_1-of-12 slds-m-horizontal_xx-small slds-align_absolute-center';
    }

    get classBoxCustomerSignDate(){
        if(this.showCustomerSignedDate){
            return 'slds-col slds-size_6-of-12 slds-hide'
        }else{
            return 'slds-col slds-size_6-of-12';
        }
    }

    get newContractTitle() {
        return this.labels.newContract;
    }

    handleSalesDataChange(){
        //if (this._salesChannel === 'Direct Sales (KAM)') {
        if (this._salesChannel !== 'Indirect Sales') {
            this.salesUnit = null;
            this.companySignedId = null;
            this.selectedSalesUnit = null;
            this.selectedUser = null;
            this.showSalesUnitSearch = true;
            this.showCompanySignedBySearch = true;
            //if existing contract has a value for salesman field but channel is not indirect, by clearing the Sales Unit it is cleared the salesman value too
            // and the renderedCallback hides the salesman field
            if(!this.isNew && this.salesman != null){
                this.salesman = null;
                this.selectedSalesman = null;
            }
        }
        else if (this._salesChannel === 'Indirect Sales') {
            this.selectedSalesUnit = null;
            this.selectedSalesman = null;
            this.showSalesUnitSearch = true;
            this.showSalesmanSearch = true;
        }
        //if ((this._salesChannel === 'Direct Sales (KAM)' || this._salesChannel === 'Indirect Sales')) {
        this.template.querySelectorAll('c-mro-custom-lookup').forEach(element => {
            element.handleEmptyField();
        });
        //}
    }

    handleCompanySignedIdChange(){
        this.companySignedId = null;
        this.selectedUser = null;
        this.showCompanySignedBySearch = true;
        this.salesUnit = null;
        this.selectedSalesUnit = null;
        this.showSalesUnitSearch = true;
        //if existing contract has a value for salesman field but channel is not indirect, by clearing the CompanySignedId it is cleared the salesman value too
        // and the renderedCallback hides the salesman field
        if(!this.isNew && this.salesman != null){
            this.salesman = null;
            this.selectedSalesman = null;
        }

        this.template.querySelectorAll('c-mro-custom-lookup').forEach(element => {
            element.handleEmptyField();
        });
    }

    handleSalesmanChange(){
        this.salesman = null;
        this.selectedSalesman = null;
        this.showSalesmanSearch = true;
        this.salesUnit = null;
        this.selectedSalesUnit = null;
        this.showSalesUnitSearch = true;
        //if existing contract has a value for companySignedId field but channel is indirect, by clearing the salesman it is cleared the companySignedId value too
        // and the renderedCallback hides the companySignedId field
        if(!this.isNew && this.companySignedId != null){
            this.companySignedId = null;
            this.selectedUser = null;
        }

        this.template.querySelectorAll('c-mro-custom-lookup').forEach(element => {
            element.handleEmptyField();
        });
    }

    handleContractDataChange(event){
        const getCntrEvent = new CustomEvent('getcontract', {
            detail: {
                newCustomerSignedDate: this.customerSignedDate,
                newContractName: this.contractName,
                newContractType: this.contractType,
                newCompanySignedId: this.companySignedId,
                newSalesUnitId: this.salesUnitId,
                newSalesman: this.salesman,
                newContractTerm: this.contractTerm,
                newContractStartDate: this.contractStartDate,
            }
        });
        this.dispatchEvent(getCntrEvent);
    }

    isNullOrEmpty (variable) {
        return (variable === undefined || variable === null || variable.length === 0);
    }

    removeError = () => {
        let areValid = true;

        let requireFields = Array.from(this.template.querySelectorAll('[data-id="signedDateField"]'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput === null || valueInput.trim() === '') {
                inputRequiredCmp.classList.remove('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

    get showDisabledContractType(){
        if (this.isContractTypeDisabled && !this.isNullOrEmpty(this.contractType)){
            return true;
        }
        return false;
    }
    get showList(){
        return this.contracts && !this.hideList;
    }

    get newContractChecked(){
        //return !this.showList;
        return !this.existingContract;
    }

    handleSelectSuEvent(event) {
        this.selectedSalesUnit = event.detail.Id;
        this.selectedSalesUnitCode = event.detail.SalesUnitID__c;
    }

    handleUnselectSuEvent(event){
        this.selectedSalesUnit = null;
        this.selectedSalesUnitCode = null;
        this.companySignedId = null;
        this.selectedUser = null;
        this.selectedSalesman = null;
        this.template.querySelectorAll('c-mro-custom-lookup').forEach(element => {
            element.handleEmptyField();
        });
    }

    handleSelectUserEvent(event) {
        this.selectedUser = event.detail.Id;
        console.log('event.detail'+JSON.stringify(event.detail));
        if(this._salesChannel === 'Direct Sales (KAM)' && !this.salesUnit && !this.salesUnitId && !this.selectedSalesUnit && !this.showUserDepartment){
            if(event.detail.Department){
                this.showSalesUnitSearch = false;
                this.showUserDepartment = true;
                //this.selectedSalesUnit = event.detail.Department;
                this.salesUnitId = event.detail.Department;
            }
        }
    }
    handleUnselectUserEvent(event){
        this.selectedUser = null;
        if(this._salesChannel === 'Direct Sales (KAM)' && !this.salesUnit && !this.selectedSalesUnit && this.showUserDepartment && this.salesUnitId){
            this.salesUnitId = null;
            this.showUserDepartment = false;
            this.showSalesUnitSearch = true;
        }
        else{
            this.salesUnitId = null;
            this.selectedSalesUnit = null;
            this.selectedSalesUnitCode = null;
            this.template.querySelectorAll('c-mro-custom-lookup').forEach(element => {
                element.handleEmptyField();
            });
        }

    }

    handleSelectSalesmanEvent(event) {
        this.selectedSalesman = event.detail.Id;
    }
    handleUnselectSalesmanEvent(event){
        this.selectedSalesman = null;
    }

    onEditClick() {
        this.editContractFields = true;
        this.isEditBoxDisabled = true;
        this.isEditBoxClosed = false;
    }
    onCloseClick(){
        this.editContractFields = false;
        this.isEditBoxDisabled = false;
        this.isEditBoxClosed = true;
        this.contractName = '';
        this.salesUnitId = null;
        this.salesUnit = null;
        this.companySignedId = null;
        this.salesman = null;
    }
}