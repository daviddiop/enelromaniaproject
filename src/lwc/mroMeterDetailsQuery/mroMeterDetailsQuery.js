/**
 * Created by Boubacar Sow  on 01/04/2020.
 */

import {api, LightningElement, track} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import {error, success} from 'c/notificationSvc';
import {NavigationMixin} from "lightning/navigation";

export default class MroMeterDetailsQuery extends NavigationMixin(LightningElement) {
    @api startDate ;
    @api endDate ;
    @api serviceSiteId ;
    @api hideConfirmSelection = false;
    @track meterDetailsList;
    @track selectAll;
    @track detailList;
    @track detailListRendered;
    @track isQueryIndex = false;
    @api typeIndex;
    // SM >> [ENLCRO-1841] Implementing the Meter Details query at Service Site object level
    @api disabled = false;
    @api servicePointId;
    @api selectedSupplyId;
    @api selectedSupplyRecordType;
    @track showLoadingSpinner = false;
    // SM << [ENLCRO-1841] Implementing the Meter Details query at Service Site object level




    labels = labels;

    readingsColumns = [
        { label: this.labels.readingDate, fieldName: 'readingDate', type: 'date', sortable: true },
        { label: this.labels.readingType, fieldName: 'readingType', type: 'text', sortable: true },
        { label: this.labels.dial, fieldName: 'dial', type: 'text', sortable: true },
        { label: this.labels.nrIntreg, fieldName: 'nrIntreg', type: 'number', sortable: true },
        { label: this.labels.index, fieldName: 'index', type: 'text', sortable: true },
        { label: this.labels.invoiced, fieldName: 'invoiced', type: 'text', sortable: true },
        { label: this.labels.quantity, fieldName: 'quantity', type: 'number', sortable: true },
        { label: this.labels.linkToImages, fieldName: 'linkToImages', type: 'text', sortable: true }
    ];

    handleRetrieveMeterDetails(){

        this.showLoadingSpinner = true;
        if(this.typeIndex === 'SAP Query Index'){
            // if selected supply is Gas the query should response the same result as IVR Gas query
            this.isQueryIndex = true;//this.selectedSupplyRecordType === 'Electric' ? true : false;
        } else {
            this.isQueryIndex = false;
        }

        notCacheableCall({
            className: "MRO_LC_MeterDetailsQuery",
            methodName: "listMeterDetails",
            input: {
                'startDate': this.startDate,
                'endDate': this.endDate,
                'recordId': this.servicePointId,
                'selectedSupplyId': this.selectedSupplyId,
                'isQueryIndex': this.isQueryIndex
            }
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
                this.showLoadingSpinner = false;
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
                this.showLoadingSpinner = false;
            }
            if (apexResponse.data.firstTableData.length < 1) {
                error(this, labels.emptyList + ' (SAP)');
                this.showLoadingSpinner = false;
            }
            this.meterDetailsList = JSON.parse(JSON.stringify(apexResponse.data.firstTableData));
            if (this.isQueryIndex === true) {
                this.detailList = JSON.parse(JSON.stringify(apexResponse.data.secondTableData));
            }
            this.showLoadingSpinner = false;
        });
    }

    closeDetailList() {
        this.detailListRendered = false;
    }
    @api
    getSelectedEntryIndex(value){
        this.isQueryIndex = value;
        this.meterDetailsList = null;
    }

    @api
    hideList(value){
        this.meterDetailsList = null;
    }

    onSelectAll(){

    }

    onSelect(){

    }

    showReadings(event){
        /*let details = this.findDetails(event.target.dataset.id);
        if (!details) {
            return;
        }
        if (details.hasOwnProperty('showReadings')) {
            details.showReadings = !details.showReadings;
        } else {
            details['showReadings'] = true;
        }*/
    }

    showDetails(event){
        let meterDetailsId = event.target.dataset.id;
        if (!meterDetailsId) {
            return;
        }
        this.detailListRendered = true;

        /*notCacheableCall({
            className: "MRO_LC_MeterDetailsQuery",
            methodName: "getDetails",
            input: {
                'meterDetailsId': meterDetailsId,
                'meterDetailsList': JSON.stringify(this.meterDetailsList),
            }
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
            }
            this.detailList = apexResponse.data;

        });*/

    }

    findMeterDetails(meterDetailsId) {
        if (!meterDetailsId) {
            return null;
        }
        return this.meterDetailsList.find(function (meterDetails) {
            return meterDetails.meterDetailsId === meterDetailsId;
        });
    }

    findDetails(detailsId) {
        if (!detailsId) {
            return null;
        }
        return this.detailList.find(function (details) {
            return details.detailsId === detailsId;
        });
    }

    onConfirmSelection(){

    }

    navigateToServiceSite(event) {

        let meterDetailsId = event.target.dataset.id;
        let foundMeterDetails = this.findMeterDetails(meterDetailsId);
        console.log('### foundMeterDetails '+JSON.stringify(foundMeterDetails));

        if (!foundMeterDetails) {
            return;
        }
        this.servicePointId = foundMeterDetails.servicePointId;
        console.log('### servicePointId '+this.servicePointId);
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.servicePointId,
                "objectApiName": "ServicePoint__c",
                "actionName": "view"
            }
        });
    }

}