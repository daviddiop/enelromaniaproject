/**
 * @since EAP2-109
 */

import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import {labels} from 'c/labels';

const showNotification = (sourceComponent, variant, message, sticky) => {
    const mode = sticky ? 'sticky' : 'dismissable';
    let eventDetails = {
        title: labels[variant],
        message: message,
        variant: variant,
        mode: mode
    };
    if (sourceComponent.dispatchEvent) { //The source component is a LWC
        sourceComponent.dispatchEvent(new ShowToastEvent(eventDetails));
    }

    else if (sourceComponent.showToast) { //The source component is a notificationLibrary Aura component
        sourceComponent.showToast(eventDetails);
    }
    else {
        if (console) {
            console.error('Invalid source component');
        }
    }
};

const error = (sourceComponent, message, sticky) => {
    if (console) {
        console.error(message);
    }
    showNotification(sourceComponent,'error', message, sticky);
};

const warn = (sourceComponent, message, sticky) => {
    if (console) {
        console.warn(message);
    }
    showNotification(sourceComponent, 'warning', message, sticky);
};

const info = (sourceComponent, message, sticky) => {
    if (console) {
        console.info(message);
    }
    showNotification(sourceComponent, 'info', message, sticky);
};

const success = (sourceComponent, message, sticky) => {
    if (console) {
        console.info(message);
    }
    showNotification(sourceComponent, 'success', message, sticky);
};


export {error, warn, info, success};