//FF Activation/SwitchIn - Interface Check - Pack2


import { LightningElement, api, track } from 'lwc';
import { labels } from 'c/mroLabels';
import { error, success } from 'c/notificationSvc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';

export default class mroRequestClassification extends LightningElement {
    labels = labels;
    @api opportunityId;
    @api dossierId;
    @api disabled = false;
    @api requestType;

    @track expirationDate;
    @track subProcess;
    @api hasSubProcessDefaultValue = false;
    @api isRequiredSubProcess = false;

    @track objectName = 'Opportunity';
    @track recordId;
    @track showExpirationDate = true;

    connectedCallback() {
        if(!this.opportunityId && this.dossierId) {
            this.objectName = 'Dossier__c';
            this.recordId= this.dossierId;
            this.showExpirationDate= false;
        } else {
            this.recordId = this.opportunityId;

        }
    }

    handleLoad (){
       let subProcess = this.template.querySelector('[data-id="subProcess"]').value
        if(!subProcess && this.hasSubProcessDefaultValue) {
            this.template.querySelector('[data-id="subProcess"]').value = 'None';
            const subProcessEvt = new CustomEvent('selectsubprocess', {
                detail: {
                    'subProcess': 'None'
                }
            });
            this.dispatchEvent(subProcessEvt);
        }
    }

    handleSubProcess(event) {
        let subProcessVal = event.target.value;
        if(!this.isRequiredSubProcess){
            subProcessVal = '' ;
        }
        const subProcessEvt = new CustomEvent('selectsubprocess', {
            detail: {
                'subProcess': subProcessVal
            }
        });
        this.dispatchEvent(subProcessEvt);

    }

    handleExpirationDate(event) {
        let selected = event.target.value;
        let todayDate = new Date();
        let min = todayDate.getFullYear() + "-" + (todayDate.getMonth() + 1) + "-" + (todayDate.getDate());

        if(!selected){
            error(this, this.labels.requiredFields);
            let dateVal = this.template.querySelector('[data-id="ExpirationDate"]');
            dateVal.classList.add('slds-has-error');
            this.expirationDate = null;
        }

        if (selected && (new Date(selected) < new Date(min))){
            error(this, this.labels.enteredDateShouldBeGreaterOrEqualThan +" "+ min);
            let dateVal = this.template.querySelector('[data-id="ExpirationDate"]');
            dateVal.classList.add('slds-has-error');

        }else {
            let dateVal = this.template.querySelector('[data-id="ExpirationDate"]');
            dateVal.classList.remove('slds-has-error');
        }
        const expirationDateEvt = new CustomEvent('selectexpirationdate', {
            detail: {
                'expirationDate': selected
            }
        });
        this.dispatchEvent(expirationDateEvt);


    }

    handleError(event) {
        error(this, event.detail.message);
    }

    get addClass() {
        if (this.isRequiredSubProcess) {
            return '';
        }
        return 'slds-hide';
    }

    @api
    refreshSubProcess(newSubProcessValue) {
        this.template.querySelector('[data-id="subProcess"]').value = newSubProcessValue;
        const subProcessEvt = new CustomEvent('selectsubprocess', {
            detail: {
                'subProcess': newSubProcessValue
            }
        });
        this.dispatchEvent(subProcessEvt);
    }
}