/**
 * Created by tommasobolis on 04/06/2020.
 */

import { api, wire, track, LightningElement } from 'lwc';
import { getRecord } from "lightning/uiRecordApi";
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import { error } from 'c/notificationSvc';
import CAMPAIGN_OBJECT from '@salesforce/schema/Campaign';
import IS_ACTIVE_FIELD from '@salesforce/schema/Campaign.IsActive';
import STATUS_FIELD from '@salesforce/schema/Campaign.Status';
import TYPE_FIELD from '@salesforce/schema/Campaign.Type';
import COMPANY_DIVISION_FIELD from '@salesforce/schema/Campaign.CompanyDivision__c';
import CONTRACT_TYPE_FIELD from '@salesforce/schema/Campaign.ContractType__c';

export default class MroCampaignButtons extends LightningElement {

    @api recordId;

    get showLoadTarget() {

        let showLoadTarget =
            this.isActive === true && this.status === 'Planned';
        return showLoadTarget;
    }

    @track isActive;
    @track status;
    @track type;
    @track companyDivision;
    @track contractType;

    @wire(getRecord, {
        recordId: '$recordId',
        fields: [
            IS_ACTIVE_FIELD,
            STATUS_FIELD,
            TYPE_FIELD,
            COMPANY_DIVISION_FIELD,
            CONTRACT_TYPE_FIELD]
    })
    getCampaign({ error, data }){
        if(data){
            var result = JSON.parse(JSON.stringify(data));
            console.log('campaign: ', result);
            this.isActive = result.fields.IsActive.value
            this.status = result.fields.Status.value;
            this.type = result.fields.Type.value;
            this.companyDivision = result.fields.CompanyDivision__c.value;
            this.contractType = result.fields.ContractType__c.value;

        }else if(error) {

            var result = JSON.parse(JSON.stringify(error));
            console.log(error.body.message);
        }
    };

    loadTarget(event) {
        let limitDate = new Date();
        limitDate.setDate(limitDate.getDate() + 60);
        let limitDateString = limitDate.getFullYear()+"-"+(limitDate.getMonth()+1).toString().padStart(2, "0")+"-"+limitDate.getDate().toString().padStart(2, "0");
        notCacheableCall({
            className: "MRO_LC_CampaignButtons",
            methodName: "loadTarget",
            input: {
                'recordId': this.recordId,
                'status': this.status,
                'type': this.type,
                'companyDivisionId': this.companyDivision,
                'contractType': this.contractType,
                'limitDate': limitDateString
            }
        }).then(apexResponse => {
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.data.error) {
                error(this, apexResponse.data.errorMsg + ' ' + ex.getStackTraceString());
                console.log(apexResponse.data.errorMsg + ' ' + ex.getStackTraceString());
            }
            console.log(apexResponse.data);
        });
    }
}