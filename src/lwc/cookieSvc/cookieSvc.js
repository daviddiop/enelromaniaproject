const INTERACTION_NAME = 'InteractionId';

export function createCookie (name, value, days) {
    let expires;
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + escape(value) + expires + "; path=/";
}
// const getLightningCookieName = (name) => {
//     return 'LSKey[c]' + name;
// };
export function setCookies (name, value) {
    this.createCookie(name, value, null);
}
export function clearCookies (name) {
    this.createCookie(name, '', null);
}
export function getCookie (name) {
    const cookieString = "; " + document.cookie;
    const parts = cookieString.split("; " + name + "=");
    if (parts.length === 2) {
        return parts.pop().split(";").shift();
    }
    return null;
}
export function setInteractionId(id) {
    this.setCookies(INTERACTION_NAME, id);
}
export function getInteractionId() {
   return this.getCookie(INTERACTION_NAME);
}
export function clearInteractionId() {
    this.clearCookies(INTERACTION_NAME);
}