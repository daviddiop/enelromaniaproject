import {LightningElement, api, track} from "lwc";
import {NavigationMixin} from 'lightning/navigation';
import {error, success} from 'c/notificationSvc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/labels';

export default class ProductOptionEdit extends NavigationMixin(LightningElement) {
    labels = labels;
    @api productId;
    @api recordId;

    @api showModal = false;

    @api recordTypeId;
    @track spinner;
    @track bundleProductId;
    @track recordTypeIdValue;
    @track familiesValue;
    @track recordTypeOptions = [];
    @track familiesOptionList = [];
    @track valueRecordTypeId;
    @track productRecordTypeId;
    @track familyRecordTypeId;

    @track type;
    @track defaultOptionList = [];
    @track defaultOptionKey = [];
    @track keyRow;
    @track valueRow;
    @track dfValue;
    @track dfValueList;
    @track dfValueKey;
    @track minValue;
    @track maxValue;
    @track minInputCmp;
    @track maxInputCmp;

    families = 'Electric family,Gas family';
    @track columnsKeyValue = [];
    @track columnsList = [];
    @track myRowsKeyValues = [];
    @track myRowsList = [];

    connectedCallback() {
        this.recordId = this.productId;
        this.getRecordTypes();
        this.getFamiliesProduct();
        this.initDataTable();
    }

    getFamiliesProduct() {
        let fmlOptions = [];
        notCacheableCall({
            className: "ProductOptionCnt",
            methodName: "getPicklistFamilies"
        }).then((responseData) => {
            if (responseData.isError) {
                error(this, JSON.stringify(responseData.error));
            } else {
                let families = responseData.data.familiesOptionList;
                Object.keys(families).forEach(function (key) {
                    fmlOptions.push({label: families[key], value: families[key]});
                });
                this.familiesOptionList = fmlOptions;
            }
        }).catch((errorMsg) => {
            error(this, errorMsg.body.output.errors[0].message);
        });
    }

    handleChange(event) {
        this.familiesValue = event.target.value;
    }

    handleChangeBundle(event) {
        this.bundleProductId = event.target.value;
    }

    getRecordTypes() {
        this.spinner = true;
        let rtsProductOption = [];
        let vlsSetProductOption = [];
        let myRowValueSet = [];
        let myRowKeyValueSet = [];
        let inputs = {recordId: this.recordId};
        notCacheableCall({
            className: "ProductOptionCnt",
            methodName: "getRecordTypeProductOption",
            input: inputs
        }).then((responseData) => {
            if (responseData.isError) {
                error(this, JSON.stringify(responseData.error));
            } else {
                let recordTypes = responseData.data.recordTypes;
                Object.keys(recordTypes).forEach(function (key) {
                    rtsProductOption.push({label: recordTypes[key], value: key});
                });
                if (this.recordId == null) {
                    this.recordTypeIdValue = responseData.data.valueRecordTypeId;//default                    
                } else {
                    //data value
                    this.recordTypeIdValue = responseData.data.recordTypeIdValue;
                    this.familiesValue = responseData.data.familiesValue;
                    this.minValue = responseData.data.minValue;
                    this.maxValue = responseData.data.maxValue;
                    this.type = responseData.data.typeValue;
                    this.dfValue = responseData.data.dfValue;
                    this.dfValueList = responseData.data.dfValue;
                    this.dfValueKey = responseData.data.dfValue;
                    if (this.type === 'List') {
                        let valueSetValues = JSON.parse(responseData.data.valueSetValue);
                        Object.keys(valueSetValues).forEach(function (value) {
                            vlsSetProductOption.push({
                                label: valueSetValues[value].value,
                                value: valueSetValues[value].value
                            });
                            myRowValueSet.push({id: value, value: valueSetValues[value].value});
                        });
                        this.defaultOptionList = vlsSetProductOption;
                        this.myRowsList = myRowValueSet;

                    } else if (this.type === 'Key/Value') {
                        let valueSetValues = JSON.parse(responseData.data.valueSetValue);
                        Object.keys(valueSetValues).forEach(function (value) {
                            vlsSetProductOption.push({
                                label: valueSetValues[value].value,
                                value: valueSetValues[value].value
                            });
                            myRowKeyValueSet.push({
                                id: value,
                                key: valueSetValues[value].key,
                                value: valueSetValues[value].value
                            });
                        });
                        this.defaultOptionKey = vlsSetProductOption;
                        this.myRowsKeyValues = myRowKeyValueSet;
                    }
                }
                this.valueRecordTypeId = responseData.data.valueRecordTypeId;
                this.productRecordTypeId = responseData.data.productRecordTypeId;
                this.familyRecordTypeId = responseData.data.familyRecordTypeId;
                this.recordTypeOptions = rtsProductOption;
                this.spinner = false;
            }
        }).catch((errorMsg) => {
            error(this, errorMsg.body.output.errors[0].message);
        });
    }

    initDataTable() {
        const actions = [
            {label: 'Remove', name: 'remove'}
        ];

        this.columnsKeyValue = [
            {label: "Key", fieldName: "key", type: "text", editable: false},
            {label: "Value", fieldName: "value", type: "text", editable: false},
            {
                type: 'button-icon',
                fixedWidth: 50,
                typeAttributes: {
                    iconName: 'utility:delete',
                    name: 'remove_row',
                    title: 'Remove',
                    variant: 'border-filled',
                    disabled: false,
                    rowActions: actions
                }
            }
        ];

        this.columnsList = [
            {label: 'Value', fieldName: 'value', type: 'text', editable: false},
            {
                type: 'button-icon',
                fixedWidth: 50,
                typeAttributes: {
                    iconName: 'utility:delete',
                    name: 'remove_row',
                    title: 'Remove',
                    variant: 'border-filled',
                    disabled: false,
                    rowActions: actions
                }
            }
        ];
    }

    resetFields() {
        this.dfValue = '';
        this.minValue = '';
        this.maxValue = '';
        this.bundleProductId = '';
        this.valueRow = '';
        this.keyRow = '';
        this.myRowsKeyValues = [];
        this.myRowsList = [];
        this.dfValueKey = '';
        this.dfValueList = '';
    }

    selectedRecordTypeId(event) {
        this.recordTypeIdValue = event.target.value;
        //this.emptyFields();
        this.bundleProductId = '';
        this.familiesValue = '';
        this.type = '';
        this.dfValue = '';

    }

    handleChangeType(event) {
        this.type = event.target.value;
        this.resetFields();
    }

    handleChangeInputValue(event) {
        this.valueRow = event.target.value;

    }

    handleChangeInputKey(event) {
        this.keyRow = event.target.value;
    }

    handleChangeInputMin(event) {
        this.minValue = event.target.value;
    }

    handleChangeInputMax(event) {
        this.maxValue = event.target.value;
    }

    handleMinMaxChange() {
        let minInput = this.template.querySelector('lightning-input.minNumber');
        let maxInput = this.template.querySelector('lightning-input.maxNumber');

        minInput.setCustomValidity("");
        maxInput.setCustomValidity("");

        if ((minInput.value > 0 && maxInput.value > 0) && maxInput.value < minInput.value) {
            maxInput.setCustomValidity('Range Not Valid');
        }
        maxInput.reportValidity();
        minInput.reportValidity();


    }

    handleClickAddColumn() {
        if (this.type === 'List') {
            let lengthPlaceList = this.defaultOptionList.length.toString();
            this.defaultOptionList.push({label: this.valueRow, value: this.valueRow});
            this.myRowsList.push({id: lengthPlaceList, value: this.valueRow});
            this.valueRow = '';
            let optionsCmp = this.template.querySelector('[data-id="defaultValueList"]');
            let columnsCmp = this.template.querySelector('[data-id="dataTableValueList"]');
            optionsCmp.options = this.defaultOptionList;
            columnsCmp.data = this.myRowsList;
        } else if (this.type === 'Key/Value') {
            let lengthPlaceKey = this.defaultOptionKey.length.toString();
            this.defaultOptionKey.push({label: this.valueRow, value: this.valueRow});

            this.myRowsKeyValues.push({id: lengthPlaceKey, key: this.keyRow, value: this.valueRow});
            this.valueRow = '';
            this.keyRow = '';
            let optionsCmp = this.template.querySelector('[data-id="defaultValueKey"]');
            let columnsCmp = this.template.querySelector('[data-id="dataTableKeyValue"]');
            optionsCmp.options = this.defaultOptionKey;
            columnsCmp.data = this.myRowsKeyValues;
        }

    }

    handleChangeDefaultValue(event) {
        if (this.type === 'List') {
            this.dfValueList = event.target.value;
        } else if (this.type === 'Key/Value') {
            this.dfValueKey = event.target.value;
        } else this.dfValue = event.target.value;
    }

    removeError(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName) {
            inputCmp.classList.remove('slds-has-error');
        }
    }

    validateFields = () => {
        let areValid = true;
        let minInputCmp = this.template.querySelector('lightning-input.minNumber');
        let maxInputCmp = this.template.querySelector('lightning-input.maxNumber');
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput == null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                error(this, this.labels.requiredFields);
                areValid = false;
            }
        });
        if (maxInputCmp && minInputCmp) {
            let allValid = minInputCmp.validity.valid && maxInputCmp.validity.valid;
            if (!allValid) {
                error(this, this.labels.invalidFields);
                areValid = false;
            }
        }
        return areValid;
    };

    addClosingEvent() {
        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }

    handleRowAction(event) {
        const row = event.detail.row;
        if (this.type === 'List') {
            let rowList = this.myRowsList;
            let defaultOptions = this.defaultOptionList;
            let rowIndex = rowList.indexOf(row);
            rowList.splice(rowIndex, 1);
            defaultOptions.splice(rowIndex, 1);
            let optionsCmp = this.template.querySelector('[data-id="defaultValueList"]');
            let columnsCmp = this.template.querySelector('[data-id="dataTableValueList"]');
            optionsCmp.options = defaultOptions;
            columnsCmp.data = rowList;
            this.myRowsList = rowList;
            this.defaultOptionList = defaultOptions;
        } else if (this.type === 'Key/Value') {
            let rowList = this.myRowsKeyValues;
            let defaultOptions = this.defaultOptionKey;
            let rowIndex = rowList.indexOf(row);
            rowList.splice(rowIndex, 1);
            defaultOptions.splice(rowIndex, 1);
            let optionsCmp = this.template.querySelector('[data-id="defaultValueKey"]');
            let columnsCmp = this.template.querySelector('[data-id="dataTableKeyValue"]');
            optionsCmp.options = defaultOptions;
            columnsCmp.data = rowList;
            this.myRowsKeyValues = rowList;
            this.defaultOptionKey = defaultOptions;
        }
    }

    navigateToListView() {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'ProductOption__c',
                actionName: 'list'
            }
        });
    }

    navigateToObjectView(productOptionId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                'recordId': productOptionId,
                'objectApiName': 'ProductOption__c',
                'actionName': 'view'

            }
        });
    }

    handleCancel() {
        let recordIdUi = this.template.querySelector('lightning-record-edit-form').recordId;
        if (recordIdUi) {
            this.navigateToObjectView(recordIdUi);
        } else {
            this.navigateToListView();
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        this.spinner = true;
        const fields = event.detail.fields;
        let finalDefaultValue;
        if (this.familiesValue) {
            fields.Family__c = this.familiesValue;
        }
        if (this.dfValue) {
            if (typeof this.dfValue === "boolean") {
                if (this.dfValue) {
                    finalDefaultValue = "true";
                } else {
                    finalDefaultValue = "false";
                }
            } else {
                finalDefaultValue = this.dfValue;
            }
        }
        if (finalDefaultValue) {
            fields.DefaultValue__c = finalDefaultValue;
        }
        if (this.minValue) {
            fields.Min__c = this.minValue;
        }
        if (this.maxValue) {
            fields.Max__c = this.maxValue;
        }
        if (this.validateFields()) {
            if (this.type) {
                if (this.type === 'List' && this.myRowsList.length && this.myRowsList) {
                    fields.ValueSet__c = JSON.stringify(this.myRowsList);
                }
                if (this.type === 'Key/Value' && this.myRowsKeyValues.length && this.myRowsKeyValues) {
                    fields.ValueSet__c = JSON.stringify(this.myRowsKeyValues);
                }
            }
            fields.RecordTypeId = this.recordTypeIdValue;
            this.template.querySelector('lightning-record-edit-form').submit(fields);
        }
        this.spinner = false;
    }

    handleSuccess() {
        if (this.productRecordTypeId) {
            notCacheableCall({
                className: "ProductOptionCnt",
                methodName: "createProdOptRelation",
                input: {
                    productOptionId: this.recordId,
                    productId: this.bundleProductId
                },
            }).then((responseData) => {
                if (responseData.isError) {
                    error(this, JSON.stringify(responseData.error));
                } else success(this, 'Product successfully updated');
                this.addClosingEvent();
                if (this.recordId) {
                    this.navigateToObjectView(this.recordId);
                } else {
                    this.navigateToListView();
                }

            }).catch((errorMsg) => {
                error(this, errorMsg.body.output.errors[0].message);
            });
        }
    }

    handleLoad() {
    }

    handleError(event) {
        error(this, event.detail.message);
        this.spinner = false;
    }

    get recordValue() {
        return this.recordTypeIdValue === this.valueRecordTypeId;
    }

    get recordProduct() {
        return this.recordTypeIdValue === this.productRecordTypeId;
    }

    get recordFamily() {
        return this.recordTypeIdValue === this.familyRecordTypeId;
    }

    get typeText() {
        return ((this.recordTypeIdValue === this.valueRecordTypeId) && (this.type === 'Text' || this.type === ''));
    }

    get typeInt() {
        return ((this.recordTypeIdValue === this.valueRecordTypeId) && (this.type === 'Percentage' || this.type === 'Currency' || this.type === 'Numeric'));
    }

    get typeCheckbox() {
        return ((this.recordTypeIdValue === this.valueRecordTypeId) && this.type === 'Checkbox');
    }

    get typeDate() {
        return ((this.recordTypeIdValue === this.valueRecordTypeId) && this.type === 'Date');
    }

    get typeCollection() {
        return ((this.recordTypeIdValue === this.valueRecordTypeId) && (this.type === 'List' || this.type === 'Key/Value'));
    }

    get typeKeyValue() {
        return ((this.recordTypeIdValue === this.valueRecordTypeId) && this.type === 'Key/Value');
    }

    get typeList() {
        return ((this.recordTypeIdValue === this.valueRecordTypeId) && this.type === 'List');
    }

}