import { LightningElement, api, track } from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import { labels } from 'c/labels';
import { error } from 'c/notificationSvc';

export default class AccountSearch extends LightningElement {
    @api interactionId;
    @api searchFields;
    @track spinner = false;

    @track showLoadingSpinner = true;
    labels = labels;
    @track vatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;

    onLoad(){
        if (this.searchFields) {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = this.searchFields[element.fieldName];
            });
        } else {
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                element.value = '';
            });
        }
        this.showLoadingSpinner = false;
    }
    handleError(event) {
        error(this, event.detail.message);
        this.showLoadingSpinner = false;
    }
    handleSubmit(event) {
        this.spinner = true;
        event.preventDefault();
        let accountObj = event.detail.fields;
        if (this.removeExtraFieldsAndCheckRequired(accountObj)) {
            this.showLoadingSpinner = false;
            this.spinner = false;
            error(this, labels.noDataEntered);
            return;
        }
        notCacheableCall({
            className: 'AccountCnt',
            methodName: 'searchAccount',
            input: accountObj
        }).then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            } else {
                this.dispatchEvent(new CustomEvent('search', {
                    detail: {
                        'accountList': response.data,
                        'account': accountObj
                    }
                }));
            }
        }).catch((errorMsg) => {
            error(this, errorMsg.body.output.errors[0].message);
            this.spinner = false;
        });
    }
    removeExtraFieldsAndCheckRequired(obj) {
        let fields = this.listAccountField();
        for (let prop in obj) {
            if (fields.indexOf(prop) === -1) {
                obj[prop] = undefined;
            }
        }
        for (let field of fields) {
            if (obj.hasOwnProperty(field) && obj[field]) {
                let value = obj[field];
                if (typeof value === 'string' && value.trim()) {
                    return false;
                } else if (typeof value !== 'string') {
                    return false;
                }
            }
        }
        return true;
    }
    listAccountField () {
        return ["Name","Email__c","Key__c","Phone"];
    }
}