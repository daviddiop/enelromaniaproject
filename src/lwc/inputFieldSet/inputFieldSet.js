import { LightningElement, track, api } from 'lwc';
import getFieldSet from '@salesforce/apex/LightningUtils.getFieldSet';
import { error, success } from 'c/notificationSvc';

export default class ApexWireMethodToFunction extends LightningElement {
    @api objectApiName;
    @api fieldSetApiName;
    @track error;
    @track fields;

    connectedCallback() {
        getFieldSet({ objectType: this.objectApiName, extraFieldsFieldSet: this.fieldSetApiName })
            .then(response => {
                this.fields = response.fields;
            })
            .catch(error => {
                this.error = error;
            });
    }
    handleError(event) {
        error(this, event.detail.message);
    }
    removeError (event) {
        // var fieldName = event.getSource().get("v.fieldName");
        // var dynFields = component.find('dynField');
        // if (!Array.isArray(dynFields)) {
        //     dynFields = [dynFields];
        // }
        // for (var d in dynFields) {
        //     if (fieldName == dynFields[d].getElements()[0].getAttribute('data-id')) {
        //         $A.util.removeClass(dynFields[d], 'slds-has-error');
        //     }
        // }
    }

    @api
    validateFieldSet(event) {

    }

}