/**
 * Created by David Diop on 26.02.2020.
 */

import {api, LightningElement, track} from "lwc";
import {error, success} from "c/notificationSvc";
import {labels} from "c/mroLabels";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";

export default class mroSendingChannelSelection extends LightningElement {
    labels = labels;
    @api dossierId;
    @api disabledInput;
    @api showEditButton = false;
    @api showConfirmButton = false;
    @api fixedValueInSMS = false;
    @api isMeterReading = false;
    @api contractAccountId;
    @api billingProfileId;
    @api isPassed = false;

    @track billingProfileAddressRecord;
    @track dossierRecord;
    @track isEmail = false;
    @track ismail = false;
    @track isSMS = false;
    @track emailDefaultValue;
    @track emailValue;
    @track phoneDefaultValue;
    @track phoneNumberValue;
    @track mailChannelTypeValue;
    @track addressDefaultValue= [];
    @track showLoadingSpinner = false;
    @track sendingChannel = false;
    @track editFields = false;
    @track mapDefaultListValue= [];

    @api
    reloadAddressList() {
        this.initialize();
        this. getBillingProfileAddress();
        this.disabledInput = true;
        this.editFields = true;
        this.isPassed = true;
    }

    connectedCallback() {
        if(!this.isPassed){
            console.log('dossierId: '+ this.dossierId +  ' FF contractAccountId : ' + this.contractAccountId + ' billingProfileId: ' + this.billingProfileId);
            this.disabledInput = true;
            this.editFields = true;
            this.initialize();
            if(this.contractAccountId || this.billingProfileId){
                this.getBillingProfileAddress();
            }
        }
    }

    initialize() {
        let inputs = {dossierId: this.dossierId};
        notCacheableCall({
            className: "MRO_LC_SendingChannelSelection",
            methodName: "getDossierRecord",
            input: inputs
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.dossierRecord = response.data;
                    if(!this.contractAccountId && !this.billingProfileId && this.dossierRecord && this.dossierRecord.Account__c) {
                        this.getBillingProfileAddressByAccountId(this.dossierRecord.Account__c);
                    }
                }
            }
        });
    }

    getBillingProfileAddress() {
        if(this.contractAccountId || this.billingProfileId){
            let inputs = {contractAccountId: this.contractAccountId, billingProfileId: this.billingProfileId};
            notCacheableCall({
                className: "MRO_LC_SendingChannelSelection",
                methodName: "getBillingProfileAddress",
                input: inputs
            }).then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        this.billingProfileAddressRecord = response.data.billingProfileAddress;
                        console.log('FF response.data.billingProfileAddress: ' + JSON.stringify(response.data.billingProfileAddress));
                        this.loadData();
                    }
                }
            });
        }
    }

    getBillingProfileAddressByAccountId(accountId) {
        if(!this.contractAccountId && !this.billingProfileId && accountId){
            let inputs = {accountId: accountId};
            notCacheableCall({
                className: "MRO_LC_SendingChannelSelection",
                methodName: "getBillingProfileAddressByAccountId",
                input: inputs
            }).then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        this.billingProfileAddressRecord = response.data.billingProfileAddress;
                        console.log('FF response.data.billingProfileAddress: ' + JSON.stringify(this.billingProfileAddressRecord));
                        this.loadData();
                    }
                }
            });
        }
    }

    loadData() {

        if (this.dossierRecord.SendingChannel__c) {
            this.isEmail = this.dossierRecord.SendingChannel__c === "Email";
            this.isSMS = this.dossierRecord.SendingChannel__c === "SMS";
            this.ismail = this.dossierRecord.SendingChannel__c === "Mail";
        }
        if (this.dossierRecord.Email__c) {
            this.emailDefaultValue = this.dossierRecord.Email__c;
        } else {
            if (this.dossierRecord.CustomerInteraction__r && this.dossierRecord.CustomerInteraction__r.Contact__r && this.dossierRecord.CustomerInteraction__r.Contact__r.Email) {
                this.emailDefaultValue = this.dossierRecord.CustomerInteraction__r.Contact__r.Email;
            } else {
                if (this.dossierRecord.Account__r && this.dossierRecord.Account__r.Email__c) {
                    this.emailDefaultValue = this.dossierRecord.Account__r.Email__c;
                }
                if (this.dossierRecord.Account__r && this.dossierRecord.Account__r.PersonEmail) {
                    this.emailDefaultValue = this.dossierRecord.Account__r.PersonEmail;
                }
            }
        }

        if(this.dossierRecord.Account__c){
            this.isBusinessClient(this.dossierRecord.Account__c);
        }

        if (this.dossierRecord.MailChannelType__c) {
            if(this.dossierRecord.MailChannelType__c === 'Billing Address'){
                this.mailChannelTypeValue='Billing Address';
            }else if(this.dossierRecord.MailChannelType__c === 'Residential Address'){
                this.mailChannelTypeValue='Residential Address';
            }
        }
        if (this.dossierRecord.Account__r && this.dossierRecord.Account__r.ResidentialAddress__c) {
            if (this.addressDefaultValue.length !== 0) {
                for (let i = 0; i < this.addressDefaultValue.length; i++) {
                    this.mapDefaultListValue.push(this.addressDefaultValue[i].label);
                }
                if (!this.mapDefaultListValue.includes(this.dossierRecord.Account__r.ResidentialAddress__c)) {
                    this.addressDefaultValue.push({
                        label: this.dossierRecord.Account__r.ResidentialAddress__c,
                        value: 'Residential Address'
                    });
                }
                if (this.billingProfileAddressRecord) {
                    if (!this.mapDefaultListValue.includes(this.billingProfileAddressRecord)) {
                        this.addressDefaultValue.push({
                            label: this.billingProfileAddressRecord,
                            value: 'Billing Address'
                        });
                    }
                }
            } else {
                this.addressDefaultValue.push({
                    label: this.dossierRecord.Account__r.ResidentialAddress__c,
                    value: 'Residential Address'
                });
                if (this.billingProfileAddressRecord) {
                    if (this.dossierRecord.Account__r.ResidentialAddress__c !== this.billingProfileAddressRecord) {
                        this.addressDefaultValue.push({
                            label: this.billingProfileAddressRecord,
                            value: 'Billing Address'
                        });
                    }
                }
            }
        }
    }

    @api
    disableInputField(disabledFields) {
        this.disabledInput = disabledFields;
    }

    @api
    saveSendingChannel() {
        this.submitForm();
    }

    handleLoad(){
        if (this.fixedValueInSMS){
            this.template.querySelectorAll('lightning-input-field').forEach(element => {
                if (!element.value){
                    element.value = "SMS";
                    element.disabled = true;
                    this.isSMS = true;
                }
            });

        }
    }

    handleChangeChannel(event) {
        this.showLoadingSpinner = true;
        let sendingChannel = event.target.value;
        this.isEmail = sendingChannel === "Email";
        this.ismail = sendingChannel === "Mail";
        this.isSMS = sendingChannel === "SMS";
        this.showLoadingSpinner = false;
    }
    handleSubmit(event) {
        event.preventDefault();
        this.submitForm();
    }

    submitForm() {
        let fields = {};
        this.template.querySelectorAll("lightning-input-field").forEach(element => {
            fields[element.fieldName] = element.value;
        });
        if (this.validateFields()) {
            this.sendingChannel = fields.SendingChannel__c;
            if (this.fixedValueInSMS){
                this.phoneNumberValue = fields.Phone__c;
            }
            if (this.isEmail){
                this.emailValue = fields.Email__c;
            }
            if (this.ismail){
                fields.MailChannelType__c = this.mailChannelTypeValue;
            }
            this.template.querySelector("lightning-record-edit-form").submit(fields);
        }
    }

    handleSuccess() {
        this.disabledInput = true;
        this.editFields = false;
        let detail = {
            sendingChannel: this.sendingChannel
        };
        if (this.fixedValueInSMS){
            detail['phone'] = this.phoneNumberValue;
        }
        if (this.isEmail){
            detail['email'] = this.emailValue;
        }
        const successEvent = new CustomEvent("savedsendingchannel", {
            detail:  detail
        });
        this.dispatchEvent(successEvent);
    }

    validateFields() {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll("lightning-input-field"));
        requireFields.forEach((inputRequiredCmp) => {
            if (inputRequiredCmp.required) {
                let valueInput = inputRequiredCmp.value;
                if (!valueInput || (isNaN(valueInput) && valueInput.trim() === "")) {
                    inputRequiredCmp.classList.add("slds-has-error");
                    error(this, this.labels.requiredFields);
                    areValid = false;
                    this.dispatchEvent(new CustomEvent('validatefieldsError', {
                                detail: {
                                    'areFieldsValidated': areValid
                                }})
                            );
                    return;
                }
            }
        });
        this.dispatchEvent(new CustomEvent('validatefields', {
            detail: {
                'areFieldsValidated': areValid
            }})
        );
        return areValid;
    }

    edit() {
        this.disableInputField(false);
        this.editFields = true;
    }

    @api
    disableEditButton(disabledFields) {
        this.editFields = disabledFields;
    }

    @api isNotRequired = false;
    @api get isRequired() {
        return !this.isNotRequired;
    }

    get disableSendingChannel(){
        return this.disabledInput || this.fixedValueInSMS;
    }
    handleSelectAddress(event) {
        this.mailChannelTypeValue = event.detail.value;
        console.log('FFF event.detail.value: ' + JSON.stringify(event.detail.value));
    }

    isBusinessClient(accountId) {
        let inputs = {accountId: accountId};
        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'isBusinessClient',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }

                if (this.dossierRecord.Phone__c) {
                    this.phoneDefaultValue = this.dossierRecord.Phone__c;

                } else {

                    if (this.dossierRecord.CustomerInteraction__r && this.dossierRecord.CustomerInteraction__r.Contact__r && this.dossierRecord.CustomerInteraction__r.Contact__r.MobilePhone) {
                        this.phoneDefaultValue = this.dossierRecord.CustomerInteraction__r.Contact__r.MobilePhone;
                        
                    } else if(this.dossierRecord.Account__r) {

                        if (response.data.isBusinessClient) {
                            this.phoneDefaultValue = '';
                        }else {
                            this.phoneDefaultValue = this.dossierRecord.Account__r.PersonMobilePhone;
                        }
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg));
            });
    }


}