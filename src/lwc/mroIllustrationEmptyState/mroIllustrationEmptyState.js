/**
 * Created by Boubacar Sow on 24/10/2020.
 */

import {LightningElement, api} from 'lwc';
import {labels} from 'c/mroLabels';

export default class MroIllustrationEmptyState extends LightningElement {

    @api message;
    labels = labels;

    connectedCallback() {
        if (!this.message){
            this.message = this.labels.emptyList;
        }
    }
}