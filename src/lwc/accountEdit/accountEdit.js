import {LightningElement, api, track} from "lwc";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {error, success} from "c/notificationSvc";
import {labels} from "c/labels";
import ACCOUNT_OBJECT from "@salesforce/schema/Account";

export default class AccountEdit extends LightningElement {
    @api recordId = "";
    @api account;
    @api interactionId;
    @api showRole = false;
    @api showCancel = false;
    @api isNewRecord = false;
    @api readOnly = false;
    @api addressForcedResidential = false;

    @track recordTypeId;
    @track accountObject = ACCOUNT_OBJECT;
    @track selectedRoles = [];
    @track address;
    @track showLoadingSpinner = false;

    businessRecordType;
    personRecordType;
    recordTypeOptions = [];
    rolesOptionList = [];
    isPerson = false;
    isEdit = false;
    addressForm = false;

    labels = labels;
    @track VatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;
    @track acc = {
        Name: "",
        VATNumber__c: "",
        Phone: "",
        isPersonAccount: false,
        Email__c: "",
        FirstName: "",
        LastName: "",
        NationalIdentityNumber__pc: "",
        PersonMobilePhone: "",
        PersonEmail: "",
        toString: function () {
            return JSON.stringify(this);
        }
    };

    connectedCallback() {
        this.isEdit = this.recordId !== "";
        this.initialize();
    }

    initialize() {
        notCacheableCall({
            className: "AccountCnt",
            methodName: "getOptionList"
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.addressForm = true;
                    this.personRecordType = response.data.personRecordType.value;
                    this.businessRecordType = response.data.businessRecordType.value;
                    this.recordTypeOptions.push(response.data.businessRecordType);
                    this.recordTypeOptions.push(response.data.personRecordType);
                    this.rolesOptionList = response.data.roleOptionList;
                    if (this.recordId) {
                        this.retrieveAccount(this.recordId);
                    } else {
                        this.isNewRecord = true;
                        this.recordTypeId = response.data.businessRecordType.value;
                        this.isBusiness = true;
                        this.isCreation = true;
                        this.isUpdate = false;
                        for (let field in this.account) {
                            if (field) {
                                this.acc[field] = this.account[field];
                            }
                        }
                        let addr = {
                            'streetNumber': null,
                            'streetNumberExtn': null,
                            'streetName': null,
                            'streetType': null,
                            'apartment': null,
                            'building': null,
                            'city': null,
                            'country': null,
                            'floor': null,
                            'locality': null,
                            'postalCode': null,
                            'province': null
                        };
                        this.address = addr;
                    }
                }
            }
        });
    }

    retrieveAccount(accountId) {
        let inputs = {accountId: accountId};
        notCacheableCall({
            className: "AccountCnt",
            methodName: "retrieveAccount",
            input: inputs
        }).then(response => {
            if (response) {
                if (response.isError) {
                    error(this, response.error);
                } else {
                    this.acc = response.data;
                    this.isUpdate = true;
                    if (response.data.IsPersonAccount.value === true) {
                        this.isPerson = true;
                    }
                    this.addressForm = true;
                    let address = {};
                    address.streetNumber = response.data.ResidentialStreetNumber__c;
                    address.streetName = response.data.ResidentialStreetName__c;
                    address.streetType = response.data.ResidentialStreetType__c;
                    address.apartment = response.data.ResidentialApartment__c;
                    address.building = response.data.ResidentialBuilding__c;
                    address.city = response.data.ResidentialCity__c;
                    address.country = response.data.ResidentialCountry__c;
                    address.floor = response.data.ResidentialFloor__c;
                    address.locality = response.data.ResidentialLocality__c;
                    address.postalCode = response.data.ResidentialPostalCode__c;
                    address.province = response.data.ResidentialProvince__c;
                    address.streetNumbersExtn = response.data.AddressStreetNumberExtn__c;
                    this.address = address;
                    if (response.data.ResidentialAddressNormalized__c) {
                        this.readOnly = true;
                    }
                }
            }
        });
    }

    selectedAccountType(event) {
        this.recordTypeId = event.target.value;
        this.isPerson = this.recordTypeId === this.personRecordType;
    }

    handleSaveAccount(event) {
        event.preventDefault();
        this.showLoadingSpinner = true;
        const fields = event.detail.fields;
        let rolesList = [];
        this.selectedRoles.forEach(element => {
            rolesList.push(element.value);
        });
        let rolesListString = rolesList.toString();

        fields.IsPersonAccount = this.recordTypeId !== this.businessRecordType;
        if (this.validateFields()) {
            let addrForm = this.template.querySelector('c-address-form');
            let addrFields = addrForm.getValues();
            // let isEmptyAddr = Object.entries(addrFields).filter(([k,v],i)=>!!v).length;
            // if(!isEmptyAddr) {
            if (!addrForm.isValid()) {
                error(this, 'Address must be verified');
                this.showLoadingSpinner = false;
                return;
            }
            for (let f in addrFields) {
                if (addrFields.hasOwnProperty(f)) {
                    fields[f] = addrFields[f];
                }
            }
            //start add by David
                if(this.addressForcedResidential === false){
                fields.ResidentialAddressNormalized__c = true;
            } else{
                fields.ResidentialAddressNormalized__c = false;
            }
            // }
            // end add by David
            let inputs = {
                account: JSON.stringify(fields),
                interactionRecord: this.interactionId,
                roles: rolesListString,
                recordTypeId: this.recordTypeId
            };

            notCacheableCall({
                className: 'AccountCnt',
                methodName: 'saveAccount',
                input: inputs
            })
                .then((response) => {
                    if (response) {
                        if (response.isError) {
                            error(this, response.error);
                        } else {
                            const acct = response.data;
                            if (acct) {
                                success(this, this.labels.success);
                                this.dispatchEvent(new CustomEvent('success', {
                                    detail: {
                                        'type': 'new',
                                        'account': acct
                                    }
                                }));
                            }
                        }
                        this.showLoadingSpinner = false;
                    }
                })
                .catch((errorMsg) => {
                    this.showLoadingSpinner = false;
                    error(this, errorMsg);
                });
        }
    }

    handleError(event) {
        error(this, event.detail.message);
        this.showLoadingSpinner = false;
    }

    updateAccount() {
        let addrForm = this.template.querySelector('c-address-form');
        if (!addrForm.isValid()) {
            error(this, 'Address must be verified');
            return;
        }

        let addrFields = addrForm.getValues();
        for (let f in addrFields) {
            if (addrFields.hasOwnProperty(f)) {
                this.acc[f] = addrFields[f];
            }
        }
        //start add by David
        if(this.addressForcedResidential === false){
            this.acc.ResidentialAddressNormalized__c = true;
        } else{
            this.acc.ResidentialAddressNormalized__c = false;
        }
        // end add by David
        let inputs = {account: JSON.stringify(this.acc)};
        notCacheableCall({
            className: 'AccountCnt',
            methodName: 'updateAccount',
            input: inputs
        })
            .then((response) => {
                if (response) {
                    if (response.isError) {
                        error(this, response.error);
                    } else {
                        const acct = response.data;
                        if (acct) {
                            success(this, this.labels.success);
                            this.dispatchEvent(new CustomEvent('success', {
                                detail: {
                                    'type': 'new',
                                    'account': acct
                                }
                            }));
                        }

                    }
                }
            })
            .catch((errorMsg) => {
                error(this, errorMsg);
            });
    }

    handleChangeInput(event) {
        let fieldName = event.target.fieldName;
        this.acc[fieldName] = event.target.value;
        event.target.classList.remove("slds-has-error");
    }

    handleOptionRolesChange(event) {
        let selectedRole = {value: event.detail.value, label: event.detail.value};
        this.selectedRoles.push(selectedRole);
        this.rolesOptionList = this.rolesOptionList.filter(roleOption => {
            return roleOption.value !== selectedRole.value;
        });
    }

    validateFields = () => {
        let areValid = true;
        let requireFields = Array.from(this.template.querySelectorAll('lightning-input-field.wr-required'));
        requireFields.forEach((inputRequiredCmp) => {
            let valueInput = inputRequiredCmp.value;
            if (valueInput === null || valueInput.trim() === '') {
                inputRequiredCmp.classList.add('slds-has-error');
                areValid = false;
            }
        });
        return areValid;
    };

    cancel() {
        this.dispatchEvent(
            new CustomEvent("cancel", {
                detail: {
                    type: "cancel"
                }
            })
        );
    }

    clear(event) {
        let selectedPill = {value: event.detail.name, label: event.detail.name};
        this.rolesOptionList.push(selectedPill);
        this.selectedRoles = this.selectedRoles.filter(roleOption => {
            return roleOption.value !== selectedPill.value;
        });
    }

    onBack() {
        this.dispatchEvent(new CustomEvent('back', {
            detail: {
                'page': 'accountList'
            }
        }));
    }
    disabledSaveButtonResiAddress(event){
        this.isNormalize = event.detail.isnormalize;
        this.addressForcedResidential = event.detail.addressForced;
    }
}