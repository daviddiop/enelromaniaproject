/**
 * Created by vincenzo.scolavino on 08/04/2020.
 */

import {LightningElement, api, track} from 'lwc';
import {error, success, warn} from 'c/notificationSvc';
import {labels} from 'c/mroLabels';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import cacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.cacheableCall';
import {checkPhone, checkMail} from "c/mroValidations";

export default class MroContractualDataEdit extends LightningElement {
    labels = labels;
    wiredApexClass = 'MRO_LC_ContractualDataEdit';
    getContractMethod = 'getContractData';

    @api showHeader;
    @api showComponent=false;
    @api readOnly;

    @api caseId;
    @api subProcess;
    @api supplyId;
    @api dossierId;
    @api accountId;
    @api origin;
    @api channel;
    @track clearCase=false;
    @track mapBankNameById;

    contentClass="slds-modal__content slds-p-around_medium";
    @track contentDivNoScrollClass=this.contentClass+" wr-modal-small";
    @track contentDivScrollClass= this.contentClass + " wr-modal-small2";
    @track contentDivClass;
    @track contractAccountFormId="editContractAccountForm";
    @track caseFormId="editCaseForm";
    @track contractFormId="editContractForm";
    @track billingProfileFormId="editBillingProfileForm";
    @track servicePointFormId="editServicePointForm";
    billingChannelDataId="billingChannelInput";
    billingMobilePhoneDataId="billingMobilePhone";
    billingEmailDataId="billingEmail";
    refundMethodDataId="refundMethod";
    refundIbanDataId='refundIban';
    refundBankDataId='refundBank';
    estimatedConsumptionDataId='estimatedConsumption';
    @track billingEmailClass ;
    @track billingPhoneClass ;
    @track refundIbanClass;
    @track refundDisabled=false;
    @track subProcessLoaded;
    REQUIRED_CLASS='wr-required';
    ERROR_CLASS='slds-has-error';

    @track newContractAccountId;
    @track contractAccountId;
    @track contractId;
    @track newContractId;
    @track billingProfileId;
    @track newBillingProfileId;
    @track supply;
    @track contractAccount;
    @track contract;
    @track billingProfile;
    @track servicePointId;
    @track newServicePointId;
    @track contractFilled;
    @track contractAccountFilled;
    @track billingFilled;
    @track fillBillingChannel;
    @track caseFilled;


    @api casePhase;
    @api caseStatus;
    @api recordTypeCase;

    @track contractStatus;

    @track subProcessChangeContractedQuantities ;
    @track subProcessChangeConsumptionConventions ;
    @track subProcessChangeBillingFrequency ;
    @track subProcessChangeInvoceIssue;
    @track subProcessChangeBillingChannel ;
    @track subProcessChangeRefundMethod ;
    @track subProcessChangeBillingPeriod ;
    @track subProcessChangeContratualValidity ;
    @track deliveryChannelEmail;
    @track deliveryChannelSMS;
    @track deliveryChannelMail;
    @track refundMethodBankTransfer;
    @track consumptionActiveStatus;
    @track consumptionActivatingStatus;
    @track deliveryChannel;
    @track productRateTypeSingle;
    @track productRateTypeDual;
    @track commodityGas;



    @track isC70Bf = false;
    @track isCII = false;
    @track isC70Bp = false;
    @track isC75Cvd = false;
    @track isC14Cq = false;
    @track isC65Cc = false;
    @track isC126Bsc = false;
    @track isCRf = false;

    @track contractAccountName;
    @track billingProfileNumber;
    @track currentBillingPeriod;
    @track contractNumber;
    @track contractStartDate;
    @track contractEndDate;
    @track contractedQuantities;
    @track consumptionConvention;
    @track estimatedQuantities;
    @track billingSendingChannel;
    @track refundMethod;
    @track consumptionList;
    @track modifiedConsumptionList;
    @track osiList;
    @track supplyList;
    @track modifiedOsiList;
    @track modifiedSupplyList;
    @track rateType;

    @track showContractAccountName;
    @track showBillingProfileNumber;
    @track showCurrentBillingFrequency;
    @track showCurrentInvoiceIssuingMethod;
    @track showCurrentBillingPeriod;
    @track showBillingPhone;
    @track showEditBillingPhone;
    @track showBillingEmail;
    @track showEditBillingEmail;
    @track showContractNumber;
    @track showServicePointName;
    @track showServiceMeterReadingControl;
    @track showContractStartDate;
    @track showContractEndDate;
    @track showSupplyActivationDate;
    @track showSupplyExpirationDate;
    @track showCaseEndDate;
    @track showContractedQuantities;
    @track showEditContractedQuantities;
    @track showConsumptionConvention;
    @track showEstimatedQuantities;
    @track showEditEstimatedQuantities;
    @track showBillingSendingChannel;
    @track showRefundMethod;

    @track fieldsMap = [];
    @track showLoadingSpinner;

    @track saveButtonDisabled = false;

    @api selfReadingPeriodEndList = [];
    @api selfReadingPeriodEndVal = '';
    @track isFieldDisabled = false;
    @track isFieldDisabledFrequency = false;

    @api isContractualDataChange;


    connectedCallback() {
        this.mapBankNameWithId();
    }


    getData(){
        if( !this.subProcess ){
            return;
        }
        this.showLoadingSpinner = true;
        let inputs = {
            supplyId : this.supplyId,
            subProcess : this.subProcess,
            caseId: this.caseId
        };
        notCacheableCall({
            className: this.wiredApexClass,
            methodName: this.getContractMethod,
            input: inputs
        }).then((response) => {
            if (response.data.error) {
                this.showLoadingSpinner = false;
                error(this, JSON.stringify(response.data.errorMsg));
            } else {
                if(response.data.supply){
                    this.supply=response.data.supply;
                    this.servicePointId=this.supply.ServicePoint__c;
                }
                this.contract=response.data.contract;
                this.contractAccount=response.data.contractAccount;
                if(this.contractAccount){
                    this.contractAccountId=this.contractAccount.Id;
                    this.billingProfileId = this.contractAccount.BillingProfile__c;
                    this.billingProfile = this.contractAccount.BillingProfile__r;

                }
                if(this.contract){
                    this.contractId=this.contract.Id;
                }
                if(this.isC70Bf || this.isC70Bp || this.isCRf || this.isC126Bsc){
                    if(!(this.contractAccount && this.billingProfile)){
                        let missingSobjects = this.contractAccount && !this.billingProfile ? 'Billing Profile' : !this.contractAccount && !this.billingProfile ? 'Billing Profile and Contract Account' : 'Contract Account';
                        error(this,  missingSobjects + '  not defined for the selected supply')
                        return;
                    }
                } else if(this.isC75Cvd || this.isC65Cc){
                    if(!this.contract){
                        error(this,  'Contract not defined for the selected supply');
                        return;
                    }
                } else if(this.isC14Cq){
                    if(!(this.supply.ServicePoint__c && this.contract)){
                        let missingSobjects = this.supply.ServicePoint__c && !this.contract ? 'Contract' : !this.supply.ServicePoint__c && !this.contract ? 'Service Point and Contract' : 'Service Point';
                        error(this, missingSobjects + ' not defined for the selected supply');
                        return;
                    }
                }
                if(response.data.case){
                    let myCase= response.data.case;
                    this.newContractId=myCase.Contract2__c;
                    this.newContractAccountId=myCase.ContractAccount2__c;
                    this.newBillingProfileId=myCase.BillingProfile2__c;
                }
                this.consumptionList=[];
                this.modifiedConsumptionList=[];
                if(response.data.consumptionList){
                    let allConsumption=response.data.consumptionList;
                    allConsumption.forEach(consum => {
                        if(consum.Status__c === this.consumptionActiveStatus){
                            this.consumptionList.push(consum);
                            if(!this.caseId){
                                this.modifiedConsumptionList.push(consum);
                            }
                        } else if(consum.Status__c === this.consumptionActivatingStatus && this.caseId && this.caseId === consum.Case__c){
                            this.modifiedConsumptionList.push(consum);
                        }
                    });
                    if(this.isContractualDataChange === true && this.modifiedConsumptionList){
                        this.modifiedConsumptionList.forEach(consumption => {
                            consumption['QuantityJanuary__c'] = Number(consumption['QuantityJanuary__c']).toFixed(3);
                            consumption['QuantityFebruary__c'] = Number(consumption['QuantityFebruary__c']).toFixed(3);
                            consumption['QuantityMarch__c'] = Number(consumption['QuantityMarch__c']).toFixed(3);
                            consumption['QuantityApril__c'] = Number(consumption['QuantityApril__c']).toFixed(3);
                            consumption['QuantityMay__c'] = Number(consumption['QuantityMay__c']).toFixed(3);
                            consumption['QuantityJune__c'] = Number(consumption['QuantityJune__c']).toFixed(3);
                            consumption['QuantityJuly__c'] = Number(consumption['QuantityJuly__c']).toFixed(3);
                            consumption['QuantityAugust__c'] = Number(consumption['QuantityAugust__c']).toFixed(3);
                            consumption['QuantitySeptember__c'] = Number(consumption['QuantitySeptember__c']).toFixed(3);
                            consumption['QuantityOctober__c'] = Number(consumption['QuantityOctober__c']).toFixed(3);
                            consumption['QuantityNovember__c'] = Number(consumption['QuantityNovember__c']).toFixed(3);
                            consumption['QuantityDecember__c'] = Number(consumption['QuantityDecember__c']).toFixed(3);
                        })
                    }
                    this.osiList=response.data.opportunityServiceItems;
                    if(!this.osiList){
                        this.supplyList=[];
                        this.supplyList.push(this.supply);
                    }
                    this.modifiedOsiList=this.osiList;
                    this.modifiedSupplyList=this.supplyList;

                    if(this.consumptionList.length === 1 || this.supply.RecordType.Name === this.commodityGas){
                        this.rateType=this.productRateTypeSingle;
                    }else {
                        this.rateType=this.productRateTypeDual;
                    }

                    console.log(this.consumptionList.length+"consumptionList==="+JSON.stringify(this.consumptionList));
                }
                if(!this.caseStatus){
                    this.caseStatus=response.data.caseStatus;
                }
                if(!this.casePhase){
                    this.casePhase=response.data.casePhase;
                }
                this.contractStatus=response.data.contractStatus;
                this.checkFieldsVisibility();

            }
            this.showLoadingSpinner = false;
            this.showComponent = true;
        })
            .catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.spinner = false;
            });
    }

    createConsumption(){
        this.showLoadingSpinner = true;
        let consumptionCmp=this.template.querySelectorAll('c-mro-consumption-table');
        //let consumptionCmp=this.template.querySelector('c-mro-consumption-table');
        for(let i = 0; i < consumptionCmp.length; i++){
            if(i = 1){
                this.modifiedConsumptionList= consumptionCmp[i].getAllConsumptionObjects();
            }
        }
        //this.modifiedConsumptionList= consumptionCmp.getAllConsumptionObjects();

        let inputs = {
            'consumptionList': JSON.stringify(this.modifiedConsumptionList),
            'contractId': this.contractId,
            'consumptionConventions': this.contract.ConsumptionConventions__c,
            'caseId': this.caseId,
            'supplyId': this.supplyId
        };
        notCacheableCall({
            className: this.wiredApexClass,
            methodName: "insertConsumptionList",
            input: inputs
        }).then((response) => {
            if (response.data.error) {
                this.showLoadingSpinner = false;
                error(this, JSON.stringify(response.data.errorMsg));
            } else {
                this.showSuccessEnd();

            }
            this.showLoadingSpinner = false;

        })
            .catch((errorMsg) => {
                console.error(JSON.stringify(errorMsg));
                if(errorMsg.body){
                    error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                } else {
                    error(this,errorMsg);
                }
                this.spinner = false;
            });
    }

    fillContractAccount() {
        if (this.contractAccount && !this.newContractAccountId) {
            this.template.querySelectorAll(this.getFormInputFieldsString(this.contractAccountFormId)).forEach(element => {
                if(!element.value){
                    element.value = this.contractAccount[element.fieldName];
                }

            });

        }
        this.setSelfReadingPeriod(this.supply, this.contractAccount);
    }

    fillBillingBrofile(){
        if (this.billingProfile && !this.newBillingProfileId ) {

           if(!this.billingFilled){
               this.setDeliverChannelValue(this.billingProfile.DeliveryChannel__c);
               this.setRefundValue(this.billingProfile.RefundMethod__c);
           }
           if(!this.billingFilled || this.fillBillingChannel){
               this.billingFilled=true;
               this.billingChannelFilled=true;
               this.template.querySelectorAll(this.getFormInputFieldsString(this.billingProfileFormId)).forEach(element => {
                   console.log("field name bill=="+element.fieldName + "  value=="+element.value);
                   if(!element.value || element.value=== '') {
                       element.value = this.billingProfile[element.fieldName];
                   }
               });
           }

        }


        if(this.newBillingProfileId && !this.billingFilled){
            this.billingFilled=true;
            let deliveryChannelCmp = this.getInputDataCmp(this.billingChannelDataId);
            if(deliveryChannelCmp) {
                this.setDeliverChannelValue(deliveryChannelCmp.value);
            }
            let refundMethodCmp = this.getInputDataCmp(this.refundMethodDataId);
            if(refundMethodCmp){
                this.setRefundValue(refundMethodCmp.value);
            }

        }

    }


    fillContract(){
        if (this.contract && !this.newContractId && !this.contractFilled) {
            this.contractFilled=true;
            this.template.querySelectorAll(this.getFormInputFieldsString(this.contractFormId)).forEach(element => {
                if(!element.value) {
                    element.value = this.contract[element.fieldName];
                }
            });
        }

    }

    fillCase(){
        if (this.supply && !this.caseId && !this.caseFilled) {
            this.caseFilled=true;
            this.template.querySelectorAll(this.getFormInputFieldsString(this.caseFormId)).forEach(element => {
                if(!element.value ) {
                    if(element.fieldName === 'EstimatedConsumption__c'){
                        element.value = this.supply.ServicePoint__r.EstimatedConsumption__c;
                    } else  if(element.fieldName === 'EndDate__c'){
                        element.value = this.supply.ExpirationDate__c
                    }

                }

            });
        }

    }



    checkSubProcess(){
        if(!this.subProcessLoaded){
            this.getConstants();
            return;
        }

        if(this.subProcess === this.subProcessChangeBillingFrequency){
            this.isC70Bf = true;
        } else if(this.subProcess === this.subProcessChangeBillingPeriod){
            this.isC70Bp = true;
        }else if(this.subProcess === this.subProcessChangeContratualValidity){
            this.isC75Cvd = true;
        }else if(this.subProcess === this.subProcessChangeContractedQuantities){
            this.isC14Cq = true;
        }else if(this.subProcess === this.subProcessChangeConsumptionConventions){
            this.isC65Cc = true;
        }else if(this.subProcess === this.subProcessChangeBillingChannel){
            this.isC126Bsc = true;
        }else if(this.subProcess === this.subProcessChangeRefundMethod){
            this.isCRf = true;
        }else if(this.subProcess === this.subProcessChangeInvoceIssue){
            this.isCII = true;
        }else {
            error(this, this.subProcess + 'is not valid');
            return;
        }
        /*switch(this.subProcess){
            case 'Change Billing frequency'                 : this.isC70Bf = true;
                break;
            case 'Change Invoice issuing method'            : this.isC70Bf = true;
                break;
            case 'Change Billing period'   : this.isC70Bp = true;
                break;
            case 'Change Contractual validity date'         : this.isC75Cvd = true;
                break;
            case 'Change Contracted Quantities'             : this.isC14Cq = true;
                break;
            case 'Change Consumption conventions'           : this.isC65Cc = true;
                break;
            case 'Change Billing sending channel'           : this.isC126Bsc = true;
                break;
            case 'Change Refund method'                     : this.isCRf = true;
                break;
            default : error(this, this.subProcess + 'is not valid');
                break;

        }*/

        this.getData();
    }


    get spinnerClass() {
        if (this.showLoadingSpinner) {
            return 'slds-modal slds-fade-in-open slds-backdrop';
        }
        return 'slds-modal slds-fade-in-open slds-backdrop slds-hide';
    }


    isContractEndDateDisabled(){
        return !this.isC75Cvd;
    }

    checkFieldsVisibility(){

        if(this.isC14Cq){
            this.contentDivClass=this.contentDivScrollClass;
        }else {
            this.contentDivClass=this.contentDivNoScrollClass;
        }
        this.showContractAccountName = this.isC70Bf || this.isC70Bp || this.isC126Bsc || this.isCRf;
        this.showBillingProfileNumber = this.isC70Bf || this.isC70Bp || this.isC126Bsc  || this.isCRf;
        this.showCurrentBillingFrequency = this.isC70Bf || this.isC70Bp;
        //this.showCurrentInvoiceIssuingMethod = this.isC70Bf;
        this.showCurrentBillingPeriod = this.isC70Bf || this.isC70Bp;
        this.showContractNumber = this.isC75Cvd || this.isC14Cq || this.isC65Cc;
        this.showContractStartDate =  this.isC14Cq || this.isC65Cc;
        this.showContractEndDate = this.isC14Cq || this.isC65Cc;
        this.showCaseEndDate = this.isC75Cvd;
        this.showSupplyActivationDate = this.isC75Cvd;
        this.showSupplyExpirationDate = this.isC75Cvd;
        this.showContractedQuantities = this.isC14Cq ;
        this.showEditContractedQuantities = this.isC14Cq;
        this.showConsumptionConvention = this.isC65Cc;
        this.showEstimatedQuantities = this.isC14Cq || this.isC65Cc ;
        this.showEditEstimatedQuantities=this.isC14Cq;
        this.showBillingSendingChannel = this.isC126Bsc;
        this.showRefundMethod = this.isCRf;
        this.showBillingPhone= this.isC126Bsc;
        this.showBillingEmail= this.isC126Bsc;
        this.showServicePointName=  this.isCII
        this.showServiceMeterReadingControl=  this.isCII
        if(this.isC65Cc || this.isC70Bf || this.isC70Bp || this.isC75Cvd || this.isCRf || this.isC14Cq){
            this.saveButtonDisabled=true;
        } else {
            this.saveButtonDisabled=false;
        }
    }

    resetBox(){
        this.isC70Bf = false;
        this.isC70Bp = false;
        this.isC75Cvd = false;
        this.isC14Cq = false;
        this.isC65Cc = false;
        this.isC126Bsc = false;
        this.isCRf = false;
        this.contractAccountName='';
        this.billingProfileNumber='';
        this.currentBillingPeriod='';
        this.contractNumber='';
        this.contractStartDate='';
        this.contractEndDate='';
        this.contractedQuantities='';
        this.consumptionConvention='';
        this.estimatedQuantities='';
        this.billingSendingChannel='';
        this.refundMethod='';
        this.showContractAccountName=false;
        this.showBillingProfileNumber=false;
        this.showCurrentBillingFrequency=false;
        this.showCurrentInvoiceIssuingMethod=false;
        this.showCurrentBillingPeriod=false;
        this.showContractNumber=false;
        this.showContractStartDate=false;
        this.showContractEndDate=false;
        this.showContractedQuantities=false;
        this.showEditContractedQuantities=false;
        this.showConsumptionConvention=false;
        this.showEstimatedQuantities=false;
        this.showEditEstimatedQuantities=false;
        this.showBillingSendingChannel=false;
        this.showCaseEndDate=false;
        this.showRefundMethod=false;
        this.newContractAccountId=null;
        this.newContractId=null;
        this.newBillingProfileId=null;
        this.contractFilled=false;
        this.billingFilled=false;
        this.fillBillingChannel=false;
        this.contractAccountFilled=false;
        this.caseFilled=false;
        this.showBillingPhone=false;
        this.showBillingEmail=false;
        this.showEditBillingPhone=false;
        this.showEditBillingEmail=false;

    }


    save(){
        this.handleSubmit();
    }

    closeModal(){
        this.showComponent = false;
        if(this.clearCase){
            this.caseId=null;
            this.newContractId=null;
            this.newContractAccountId=null;
            this.newBillingProfileId=null;
        }
        this.contractId=null;
        this.contractAccountId=null;
        this.billingProfileId=null;
        this.servicePointId=null;
        const closeModal = new CustomEvent("closemodal", { detail: { 'disableAdvancedSearch': false } });
        this.dispatchEvent(closeModal)
    }
    @api open() {
        this.resetBox();
        if(this.subProcess){
            this.checkSubProcess();
        }

    }

    @api close(){
        this.closeModal();
    }


    handleSubmit() {
        this.showLoadingSpinner = true;
        let valid=this.validateFields();
        console.log("valid==========="+valid);
        if(!this.caseId){
            this.clearCase=true;
        }
        if (valid) {
            if(this.isC126Bsc || this.isCRf){
                this.saveBillingProfile();
            }else if(this.isC65Cc){
                this.saveContract();
            } else if(this.isC70Bp || this.isC70Bf){
                this.saveContractAccount();
            } else if(this.isCII){
                this.saveCase();
            }else {
                this.saveCase();
            }

        } else {
            this.showLoadingSpinner = false;
            return;
        }
    }

    saveCase() {
        let fields = {};

        let form = this.template.querySelector(this.getFormString(this.caseFormId));
        this.template.querySelectorAll(this.getFormInputFieldsString(this.caseFormId)).forEach(element => {
            fields[element.fieldName] = element.value;
        });

        this.showLoadingSpinner = true;

        if(this.caseStatus){
            fields.Status=this.caseStatus;
        }
        if(this.casePhase){
            fields.Phase__c=this.casePhase;
        }else {
            fields.Status = this.defaultCaseStatus;
        }

        if (this.accountId) {
            fields.AccountId = this.accountId;
        }

        if (this.dossierId) {
            fields.Dossier__c = this.dossierId;
        }
        if(this.supplyId ){
            fields.Supply__c=this.supplyId;
        }

        if (!this.caseId && this.recordTypeCase ) {
            fields.RecordTypeId = this.recordTypeCase;
        }

        fields.Origin = this.defaultCaseOrigin;
        if(this.origin){
            fields.Origin = this.origin;
        }
        if(this.channel){
            fields.Channel__c= this.channel;
        }
        if(this.subProcess){
            fields.SubProcess__c= this.subProcess;
        }
        if(this.newContractAccountId){
            if(this.contractAccountId){
                fields.ContractAccount__c=this.contractAccountId;
            }
            fields.ContractAccount2__c=this.newContractAccountId;
        }

        if(this.newContractId){
            if(this.contractId){
                fields.Contract__c=this.contractId;
            }
            fields.Contract2__c=this.newContractId;
        }
        console.log("this.caseId===="+this.caseId);

        if(this.newBillingProfileId){
            if(this.billingProfile){
                fields.BillingProfile__c=this.billingProfileId;
            }
            fields.BillingProfile2__c=this.newBillingProfileId;
            fields.ContractAccount__c=this.contractAccountId;
        }



        console.log("case fields=="+JSON.stringify(fields));
        form.submit(fields);

    }

    saveContractAccount() {
        let fields = {};

        let form = this.template.querySelector(this.getFormString(this.contractAccountFormId));
        this.showLoadingSpinner = true;
        //this.copyObjectToFields(this.contractAccount,fields);
        this.template.querySelectorAll(this.getFormInputFieldsString(this.contractAccountFormId)).forEach(element => {
            fields[element.fieldName] = element.value;
        });

        if (this.accountId) {
            fields.Account__c = this.accountId;
        }
        if(this.newBillingProfileId){
            fields.BillingProfile__c=this.newBillingProfileId;
        }
        fields.IntegrationKey__c=null;
        if(this.selfReadingPeriodEndVal){
            fields.SelfReadingPeriodEnd__c = this.selfReadingPeriodEndVal;
        }

        form.submit(fields);

    }
    saveServicePoint(){
        let fields = {};
        let form = this.template.querySelector(this.getFormString(this.servicePointFormId));
        this.showLoadingSpinner = true;
        //this.copyObjectToFields(this.contractAccount,fields);
        this.template.querySelectorAll(this.getFormInputFieldsString(this.servicePointFormId)).forEach(element => {
            fields[element.fieldName] = element.value;
        });
        form.submit(fields);
    }

    copyObjectToFields(objectData,fields){
        console.log(" objectData==="+JSON.stringify(objectData));
        for(let key in objectData){
            if(objectData.hasOwnProperty(key)){
                console.log(key+'  key==='+objectData[key]);
                fields[key] =objectData[key];
            }
        }
        fields.Id=null;
    }

    saveContract() {
        let fields = {};

        let form = this.template.querySelector(this.getFormString(this.contractFormId));
        this.copyObjectToFields(this.contract,fields);
        this.template.querySelectorAll(this.getFormInputFieldsString(this.contractFormId)).forEach(element => {
            fields[element.fieldName] = element.value;
        });
        console.log("contract form==="+form);

        this.showLoadingSpinner = true;

        if (this.accountId) {
            fields.AccountId = this.accountId;
        }
        fields.IntegrationKey__c=null;
        if(this.contractStatus){
            fields.Status=this.contractStatus;
        }

        form.submit(fields);

    }

    saveBillingProfile() {
        let fields = {};

        let form = this.template.querySelector(this.getFormString(this.billingProfileFormId));
        //this.copyObjectToFields(this.billingProfile,fields);
        this.template.querySelectorAll(this.getFormInputFieldsString(this.billingProfileFormId)).forEach(element => {
            fields[element.fieldName] = element.value;
        });
        console.log("billing form==="+form);

        this.showLoadingSpinner = true;

        if (this.accountId) {
            fields.Account__c = this.accountId;
        }

        form.submit(fields);

    }

    handleSuccess(event) {
        event.preventDefault();
        event.stopPropagation();
        let apiName=event.detail.apiName;
        console.log("success apiName==========="+apiName);
        if(apiName === "BillingProfile__c"){
            this.newBillingProfileId=event.detail.id;
            this.saveCase();
            return;
        } else if (apiName === "ContractAccount__c") {
            this.newContractAccountId=event.detail.id;
            if(!this.caseId){
                this.saveCase();
                return;
            }
        } else  if (apiName === "Contract") {
            this.newContractId=event.detail.id;
            console.log("newContractId=="+this.newContractId);
            if(!this.caseId){
                this.saveCase();
                return;
            }
        } else if(apiName === "Case") {
            this.caseId=event.detail.id;

            if(this.isC14Cq ){
                this.createConsumption();
                return;
            }

        }else if (apiName === "ServicePoint__c"){
            this.newServicePointId=event.detail.id;
            this.saveCase();
            return;
        }

        this.showSuccessEnd();


    }

    handleError(event){
        console.log('error ****> ' + event);
        this.showLoadingSpinner = false;
        error(this, event.detail.message);
    }

    showSuccessEnd(){
        //success(this, 'Case Created Successfully');

        const successEvent = new CustomEvent("casecreated", { detail: { caseId: this.caseId } });
        this.dispatchEvent(successEvent);
        this.showLoadingSpinner = false;

        this.closeModal();
    }


    @api
    validateFields() {
        let valid = true;
        let requireFields = Array.from(this.template.querySelectorAll("lightning-input-field.wr-required"));

       // valid =this.validate();
        if(valid){
            requireFields.forEach((inputRequiredCmp) => {
                let valueInput = inputRequiredCmp.value;
                if (!valueInput || (isNaN(valueInput) && valueInput.trim() === "")) {
                    inputRequiredCmp.classList.add(this.ERROR_CLASS);
                    error(this, this.labels.requiredFields);
                    valid = false;
                }
            });

        }
        if(valid){
            if( this.showEditBillingPhone){

                let inputMobileCmp = this.getInputDataCmp(this.billingMobilePhoneDataId);
                let mobilePhone = inputMobileCmp.value;
                if(mobilePhone != null && mobilePhone != '') {
                    let check = checkPhone(mobilePhone);
                    if (!check.outCome) {
                        error(this, this.labels.invalidMobile.replace('{0}', mobilePhone));
                        valid = false;
                    }
                }

            }
            if(this.showEditBillingEmail){
                let inputEmailCmp = this.getInputDataCmp(this.billingEmailDataId);
                let email = inputEmailCmp.value;
                if(email != null && email != '') {
                    let check = checkMail(email);
                    if(valid){
                        valid = check.outCome;
                        if(!valid){
                            inputEmailCmp.classList.add(this.ERROR_CLASS);
                        }
                    }
                }
            }

            if(this.showRefundMethod){
                valid=this.checkRefundIbanValidation();
            }

        }

        return valid;
    }

    handleConsumptionConvention(event){
        let inputCmp = event.target;
        //this.showEstimatedQuantities=inputCmp.value;
        this.saveButtonDisabled=inputCmp.value === this.contract.ConsumptionConventions__c;
    }
    handleChangeDeliveryChannel(event){
        let inputCmp = event.target;
        let channel=inputCmp.value;
        this.setDeliverChannelValue(channel);
    }

    handleChangeRefundMethod(event) {
        let inputCmp = event.target;
        this.setRefundValue(inputCmp.value);
        this.removeError(event);
    }

    setRefundValue(value){
        if(this.isCRf){
            let refundIbanCmp=this.getInputDataCmp(this.refundIbanDataId);
            let refundBankCmp=this.getInputDataCmp(this.refundBankDataId);
            refundBankCmp.classList.remove(this.ERROR_CLASS);
            refundIbanCmp.classList.remove(this.ERROR_CLASS);
            if (value === this.refundMethodBankTransfer) {
                this.refundIbanClass = this.REQUIRED_CLASS;
                this.refundDisabled=false;
                refundIbanCmp.value=this.billingProfile[refundIbanCmp.fieldName];
                refundBankCmp.value=this.billingProfile[refundBankCmp.fieldName];
            } else {
                this.refundIbanClass = '';
                this.refundDisabled=true;
                refundIbanCmp.value='';
                refundBankCmp.value='';
            }
        }

    }
    removeError(event) {
        let inputMarket = this.contractAccount.Market__c;
        let companyDivisionId = this.supply.CompanyDivision__c;
        let vATNumber = this.supply.ServicePoint__r.Distributor__r.VATNumber__c;
        let inputCmp = event.target;
        let fieldName=inputCmp.fieldName;

        if (fieldName) {
            inputCmp.classList.remove(this.ERROR_CLASS);
            if (this.inEnelArea && this.contractType === 'Residential' && this.commodity !== 'Gas' && (this.buttonType === 'new' || this.buttonType === 'edit')) {
                this.getListSelfReadingPeriodByCompanyDivision(companyDivisionId, vATNumber, inputMarket);
            }
            if(fieldName === 'EstimatedConsumption__c'){
                if(this.modifiedOsiList){
                    this.modifiedOsiList.forEach(osi => {
                        osi.EstimatedConsumption__c=inputCmp.value;

                    });
                } else {
                    this.modifiedSupplyList.forEach(supply => {
                        supply.ServicePoint__r.EstimatedConsumption__c=inputCmp.value;

                    });
                }

                this.saveButtonDisabled=false;
                let consumptionCmp=this.template.querySelectorAll('c-mro-consumption-table');
                //let consumptionCmp=this.template.querySelector('c-mro-consumption-table');
                for(let i = 0; i < consumptionCmp.length; i++){
                    if(i = 1){
                        consumptionCmp[i].update();
                    }
                }


            }else if(this.isC75Cvd){
                this.saveButtonDisabled=inputCmp.value === '' || inputCmp.value === this.supply['ExpirationDate__c'];
            }else if(this.isCRf){
                let changeRefund=inputCmp.value.trim() !== '' && (!this.billingProfile[fieldName]   || inputCmp.value !== this.billingProfile[fieldName]);
                if(!changeRefund){
                    let refundMethodCmp= this.getInputDataCmp(this.refundMethodDataId);
                    let refundIbanCmp= this.getInputDataCmp(this.refundIbanDataId);
                    let refundBankCmp= this.getInputDataCmp(this.refundBankDataId);
                    let oldBankValue=this.billingProfile[refundBankCmp.fieldName];
                    let oldMethodValue=this.billingProfile[refundMethodCmp.fieldName];
                    let oldIbanValue=this.billingProfile[refundIbanCmp.fieldName];
                    let changeRefundBank=refundBankCmp.value && refundBankCmp.value !== '' && (!oldBankValue || refundBankCmp.value !== oldBankValue);
                    let changeRefundMethod=refundMethodCmp.value && refundMethodCmp.value !== '' && (!oldMethodValue || refundMethodCmp.value !== oldMethodValue);
                    let changeRefundIban=refundIbanCmp.value && refundIbanCmp.value !== '' && (!oldIbanValue || refundIbanCmp.value !== oldIbanValue);

                    changeRefund=  changeRefundBank || changeRefundIban || changeRefundMethod;

                }
               this.saveButtonDisabled=!changeRefund;
            } else if(this.isC70Bf || this.isC70Bp){
                this.saveButtonDisabled=inputCmp.value === '' || inputCmp.value === this.contractAccount[fieldName];
            } else if(this.isC126Bsc){
                this.checkSendingChannelChanges(inputCmp);
            }


        }

    }

    checkSendingChannelChanges(inputCmp){
        let fieldName=inputCmp.fieldName;
        if(this.deliveryChannel === this.deliveryChannelEmail || this.deliveryChannel === this.deliveryChannelSMS ){
            this.saveButtonDisabled= inputCmp.value === this.billingProfile[fieldName];
        }else {
            let change=inputCmp.value && inputCmp.value.trim() !== '' && (!this.billingProfile[fieldName]   || inputCmp.value !== this.billingProfile[fieldName]);
            console.log(change+"field change=="+fieldName + "    old val=="+this.billingProfile[fieldName]);
            if(!change){
                let mobileCmp=this.getInputDataCmp(this.billingMobilePhoneDataId);
                if(mobileCmp && fieldName !== mobileCmp.fieldName){
                    change= mobileCmp.value && mobileCmp.value.trim() !== '' && mobileCmp.value !== this.billingProfile[mobileCmp.fieldName];
                }else {
                    let emailCmp=this.getInputDataCmp(this.billingEmailDataId);
                    change=emailCmp && emailCmp.value && emailCmp.value.trim() !== '' && emailCmp.value !== this.billingProfile[emailCmp.fieldName];
                }
            }
            this.saveButtonDisabled=!change || this.deliveryChannel === '';

        }
    }

    handleEditConsumptions(event){
        //let consumptionCmp=this.template.querySelectorAll('c-mro-consumption-table')[1];
        //let consumptionCmp=this.template.querySelector('c-mro-consumption-table');
        let consumptionCmp=this.template.querySelectorAll('c-mro-consumption-table');
        //let consumptionCmp=this.template.querySelector('c-mro-consumption-table');
        let totalEstimated;
        for(let i = 0; i < consumptionCmp.length; i++){
            if(i = 1){
                totalEstimated = consumptionCmp[i].calculateTotalEstiamted();
            }
        }
        //let totalEstimated=consumptionCmp.calculateTotalEstiamted();
        this.getInputDataCmp(this.estimatedConsumptionDataId).value=totalEstimated;
        this.saveButtonDisabled=false;

    }

    setDeliverChannelValue(channel){
        if(this.isC126Bsc) {
            this.saveButtonDisabled=true;
            this.deliveryChannel=channel;
            this.fillBillingChannel=true;

            if (channel === this.deliveryChannelEmail) {
                this.showEditBillingEmail = true;
                this.showEditBillingPhone = false;
                this.billingEmailClass = this.REQUIRED_CLASS;
                this.billingPhoneClass = '';
            } else if (channel === this.deliveryChannelSMS) {
                this.showEditBillingEmail = false;
                this.showEditBillingPhone = true;
                this.billingPhoneClass = this.REQUIRED_CLASS;
                this.billingEmailClass = '';

            } else {
                this.showEditBillingEmail = false;
                this.showEditBillingPhone = false;
                this.billingPhoneClass = '';
                this.billingEmailClass = '';
                this.saveButtonDisabled=true;
            }
            let inputCmp=this.getInputDataCmp(this.billingChannelDataId);
            this.checkSendingChannelChanges(inputCmp);

        }
    }
    getFormInputFieldsString(formDataId){
        return  'lightning-record-edit-form[data-id="'+formDataId+'"] lightning-input-field';
    }

    getFormString(formDataId){
        return 'lightning-record-edit-form[data-id="'+formDataId+'"]';
    }

    getInputDataCmp(dataId){
        return this.template.querySelector('[data-id="'+dataId+'"]');
    }

    getConstants() {
        cacheableCall({
            className: "MRO_UTL_Utils",
            methodName: "getAllConstants"
        })
            .then(apexResponse => {
                if (apexResponse) {
                    if (apexResponse.isError) {
                        error(this, apexResponse.error);
                    } else {
                        this.subProcessChangeContractedQuantities= apexResponse.data.SUB_PROCESS_CHNG_CONTRACTED_QUANTITIES;
                        this.subProcessChangeConsumptionConventions = apexResponse.data.SUB_PROCESS_CHNG_CONSUMPTION_CONVENTIONS;
                        this.subProcessChangeBillingFrequency = apexResponse.data.SUB_PROCESS_CHNG_BILLING_FREQUENCY;
                        this.subProcessChangeBillingChannel= apexResponse.data.SUB_PROCESS_CHNG_BILLING_CHANNEL;
                        this.subProcessChangeRefundMethod = apexResponse.data.SUB_PROCESS_CHNG_REFUND_METHOD;
                        this.subProcessChangeBillingPeriod = apexResponse.data.SUB_PROCESS_CHNG_BILLING_PERIOD;
                        this.subProcessChangeContratualValidity= apexResponse.data.SUB_PROCESS_CHNG_CONTRACTUAL_VALIDITY;
                        this.subProcessChangeInvoceIssue= apexResponse.data.SUB_PROCESS_CHNG_INVOICE_ISSUE;
                        this.consumptionActiveStatus=apexResponse.data.CONSUMPTION_STATUS_ACTIVE;
                        this.consumptionActivatingStatus=apexResponse.data.CONSUMPTION_STATUS_ACTIVATING;
                        this.refundMethodBankTransfer=apexResponse.data.BILLING_PROFILE_REFUND_METHOD_BANK_TRANSFER;
                        this.deliveryChannelEmail=apexResponse.data.BILLING_PROFILE_DELIVERY_CHANNEL_EMAIL;
                        this.deliveryChannelMail=apexResponse.data.BILLING_PROFILE_DELIVERY_CHANNEL_MAIL;
                        this.deliveryChannelSMS=apexResponse.data.BILLING_PROFILE_DELIVERY_CHANNEL_SMS;
                        this.productRateTypeSingle=apexResponse.data.PRODUCT_RATE_TYPE_SINGLE;
                        this.productRateTypeDual=apexResponse.data.PRODUCT_RATE_TYPE_DUAL;
                        this.commodityGas=apexResponse.data.COMMODITY_GAS
                        this.subProcessLoaded=true;
                        this.checkSubProcess();
                    }
                }
            });
    }


    checkRefundIbanValidation() {
        let inputCmp = this.getInputDataCmp(this.refundIbanDataId);
        let input = inputCmp.value;
        if(!input){input='';}
        let iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
            code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
            digits;
        if((inputCmp.classList.contains('wr-required'))  && ((!code) || (iban.length !== this.getIBANSize(input)))) {
            inputCmp.classList.add(this.ERROR_CLASS);
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN008').replace('{1}', iban));
            //error(this, this.labels.incorrectIBANLength);
            return false;
        }

        if((!inputCmp.classList.contains(this.REQUIRED_CLASS) && (input.length !== 0))  && ((!code) || (iban.length !== this.getIBANSize(input)))) {
            inputCmp.classList.add(this.ERROR_CLASS);
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN008').replace('{1}', iban));
            return false;
        }
        // rearrange country code and check digits, and convert chars to ints
        let reArranged = String(iban).substring(4) + String(iban).substring(0, 4);
        let integerDigits = this.convertToInteger(reArranged);
        // final check
        if ((input.length !== 0)  && (this.mod97(integerDigits) !== 1)) {
            inputCmp.classList.add(this.ERROR_CLASS);
            error(this, this.labels.incorrectIBANNumber.replace('{0}', 'E-IBAN002').replace('{1}', iban));
            return false
        }

        return (input.length !== 0)  ? this.mod97(integerDigits) === 1 : true;
    }

    mod97(digits) {
        let checksum = digits.slice(0, 2), fragment;
        for (let offset = 2; offset < digits.length; offset += 7) {
            fragment = String(checksum) + digits.substring(offset, offset + 7);
            checksum = parseInt(fragment, 10) % 97;
        }
        return checksum;
    }
    convertToInteger(aString) {
        let result = "";
        for (let i = 0; i < aString.length; i++) {
            let ch = aString[i];
            if (isNaN(ch) ){
                result += ch.charCodeAt(0) - 55;
            } else{
                result += ch.toString();
            }
        }
        return result;
    }

    setSelfReadingPeriod(supply, contractAccount) {
        let inEnelArea = supply.InENELArea__c;
        let contractType = contractAccount.ContractType__c;
        let commodity = contractAccount.Commodity__c;
        let inputMarket = contractAccount.Market__c;
        let selfReadingPeriodEnd = this.template.querySelector('[data-id="selfReadingPeriodEnd"]');
        let billingFrequency = this.template.querySelector('[data-id="billingFrequency"]');
        let companyDivisionId = this.supply.CompanyDivision__c;
        let vATNumber = supply.ServicePoint__r.Distributor__r.VATNumber__c;
        let selfReadingEnabled = supply.ServicePoint__r.Distributor__r.SelfReadingEnabled__c;

        if (inEnelArea &&  contractType === 'Residential' && commodity !== 'Gas' ) {
            if(inputMarket) {
                this.getListSelfReadingPeriodByCompanyDivision(companyDivisionId, vATNumber, inputMarket);
            }
        }

        if (contractType === 'Business' && commodity !== 'Gas' && !selfReadingPeriodEnd.value && !billingFrequency.value) {
            selfReadingPeriodEnd.value = '31';
            billingFrequency = 'Monthly';
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }

        if (commodity === 'Gas' && selfReadingEnabled && !selfReadingPeriodEnd.value && !billingFrequency.value) {
            selfReadingPeriodEnd.value = '26';
            billingFrequency = 'Monthly';
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }

        if (commodity === 'Gas' && !selfReadingEnabled && !selfReadingPeriodEnd.value && !billingFrequency.value) {
            selfReadingPeriodEnd.value = '0';
            billingFrequency = 'Monthly';
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }

        if (contractType === 'Business' || commodity === 'Gas') {
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = true;
        }

        if (!inEnelArea && contractType === 'Residential' && commodity !== 'Gas') {
            selfReadingPeriodEnd.value = '31';
            this.isFieldDisabled = true;
            this.isFieldDisabledFrequency = false;
        }
        return [billingFrequency, selfReadingPeriodEnd];
    }

    getListSelfReadingPeriodByCompanyDivision(companyDivisionId, vATNumber, inputMarket) {
        let inputs = {companyDivisionId: companyDivisionId, vatNumber: vATNumber, market:inputMarket};

        notCacheableCall({
            className: 'MRO_LC_ContractAccount',
            methodName: 'getListSelfReadingPeriodByCompanyDivision',
            input: inputs
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let selfReadingPeriodEnd = this.template.querySelector('[data-id="selfReadingPeriodEnd"]');

                if(response.data.selfReadingPeriodEnds){
                    this.selfReadingPeriodEndList = response.data.selfReadingPeriodEnds;
                    if(this.selfReadingPeriodEndList){
                        this.selfReadingPeriodEndVal = this.selfReadingPeriodEndList[0].value;

                    }
                    if(selfReadingPeriodEnd && selfReadingPeriodEnd.value){
                        this.selfReadingPeriodEndVal = selfReadingPeriodEnd.value;
                    }
                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    setRefundBank(event) {
        let inputCmp = event.target;
        if (inputCmp.fieldName === 'RefundIBAN__c') {
            let refundBankCmp = this.template.querySelector('[data-id="refundBank"]');
            if(inputCmp.value.length > 7){

                console.log('inputCmp.value.length: ' + inputCmp.value.length);
                console.log('inputCmp.value.substring(4,8): ' + inputCmp.value.substring(4,8));
                Object.keys(this.mapBankNameById).forEach((key) => { //loop through keys array
                    if (inputCmp.value.substring(4,8) == key) {
                        refundBankCmp.value = this.mapBankNameById[key];
                    }
                });
            }else{
                refundBankCmp.value = '';
            }
        }
        this.removeError(event);
    }

    mapBankNameWithId() {
        notCacheableCall({
            className: 'MRO_LC_BillingProfile',
            methodName: 'getMapBankNameWithId'
        })
            .then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                }
                let mapBankNameWithId = {};
                mapBankNameWithId = response.data.mapBankId;
                this.mapBankNameById = mapBankNameWithId;
                console.log('mapBankNameById: ' + JSON.stringify(this.mapBankNameById));
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
            });
    }

    getIBANSize(input){
        let iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''); // keep only alphanumeric characters
        if(iban.length < 5) return 100;
        let code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/);
        if(code === null) return 100;
        let CODE_LENGTHS = {
            AL: 28,
            AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
            CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
            FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
            HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
            LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
            MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
            RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
        };

        let size = CODE_LENGTHS[code[1]] || 100;

        return size;
    }

    handleSelfReading(event) {
        let selectedSelfReadingPeriodEnd = this.selfReadingPeriodEndList.filter(function(selfReadingPeriodEnd) {
            return selfReadingPeriodEnd.value === event.detail.value;
        });
        let selfReadingPeriodEnd = this.template.querySelector('[data-id="selfReadingPeriodEnd"]');

        if (selectedSelfReadingPeriodEnd) {
            if(selfReadingPeriodEnd){
                selfReadingPeriodEnd.value = selectedSelfReadingPeriodEnd[0].value;
                this.selfReadingPeriodEndVal= selectedSelfReadingPeriodEnd[0].value;
                this.saveButtonDisabled = false;
            }
        }
    }

    get showSelfReadingCbx() {
        if(this.selfReadingPeriodEndList.length > 0){
            return 'slds-col slds-size_1-of-2';
        }
        return 'slds-hide';
    }

    get hideSelfReadingField() {
        if(this.selfReadingPeriodEndList.length > 0){
            return 'slds-hide';
        }
        return 'slds-col slds-size_1-of-2';
    }

    get hideRequired() {
        if(this.selfReadingPeriodEndList.length > 0){
            return '';
        }
        return 'wr-required';
    }

}