/**
 * Created by tommasobolis on 16/06/2020.
 */

/* eslint-disable no-console */
/* eslint-disable eqeqeq */
import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import {labels} from 'c/mroLabels';
import initialize from '@salesforce/apex/MRO_LC_FileRelatedListCnt.initialize';
import createRelatedList from '@salesforce/apex/MRO_LC_FileRelatedListCnt.createRelatedList';
import retrieveFromDMS from '@salesforce/apex/MRO_LC_FileRelatedListCnt.retrieveFromDMS';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';

export default class fileRelatedList extends NavigationMixin(LightningElement) {

    channelName = '/event/DocumentManagementSystemEvent__e';
    subscription = {};

    @api title; //String which contains the component's title
    @api icon; //String which contains the component's icon
    @api relationshipFieldAPIName; //string which contains the API Name of the field which contains the Id of the relationship SObject
    @api fieldSet; // String which contains the name of the FieldSet where is stored the relationship Object's API Fields Name to retrieve
    @api enableNavigateToSObject; //boolean which indicates whether is allowed Navigate to sObject (enable/disable the 'View' button)
    @api recordId = '$recordId';
    @api listType;

    labels = labels;

    /** This attribute contains the records to show */
    @track relatedList = [];
    @track label = new Map();

    /** This attribute is used for show the spinner */
    @track showSpinner = false;
    @track sortDirection = 'ASC';

    //Pagination attributes
    @track currentPageNumber=1;
    @track numOfPages=1;
    @track recordsForPage=5;
    @track numOfRecords;
    @track shownPageRecordsList=[];

    //@track showPagination=false;

    get showPagination() {
        let showPagination = this.numOfRecords !== undefined && this.recordsForPage !== undefined && this.numOfRecords > this.recordsForPage;
        return showPagination;
    }

    connectedCallback() {

        this.showSpinner = true;
        this.subscribeDMSEvent();
        this.init();
    }

    // Handles subscribe button click
    subscribeDMSEvent() {

        // Callback invoked whenever a new event message is received
        const messageCallback = (response) => {

            console.log('New message received : ', JSON.stringify(response));
            this.payload = JSON.stringify(response);
            // Response contains the payload of the new message received
            this.init();
        };

        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.channelName, -1, messageCallback).then(response => {

            // Response contains the subscription information on subscribe call
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
            this.subscription = response;
        });
    }

    init() {

        initialize( {recordId: this.recordId,
                     title: this.title,
                     relationshipFieldAPIName: this.relationshipFieldAPIName,
                     fieldSet: this.fieldSet})

        .then(result => {
            if (result.error == false) {

                    this.label = result.label;
                    this.numOfRecords = result.relatedList.records.length;
                    this.relatedList = result.relatedList;
                    this.title = result.label.title;

                    this.numOfPages=Math.ceil(this.numOfRecords /this.recordsForPage);
                    //if(this.numOfRecords>this.recordsForPage){
                    //    this.showPagination=true;
                    //}
                    this.showPage(this.currentPageNumber);
                    console.log('MRO_LC_FileRelatedListCnt.initialize - relatedList', JSON.stringify(this.relatedList));
            }else {

                this.showError(result.errorMessage);
                console.log('MRO_LC_FileRelatedListCnt.initialize (Callback) errorMessage', result.errorMessage);
                console.log('MRO_LC_FileRelatedListCnt.initialize (Callback) errorStackTraceString', result.errorStackTraceString);
            }
            this.showSpinner = false;
        })
    }
    //records for page
    showPage(pageNum){
        let allRecordsList = this.relatedList.records;
        if(allRecordsList !== undefined){
           let recordsForPage = this.recordsForPage;
           let shownPageRecordsList = [];
           let leftBracket = (pageNum - 1) * recordsForPage;
           let rightBracket = leftBracket + recordsForPage;
           if (rightBracket > allRecordsList.length) rightBracket = allRecordsList.length;
           for (let i = leftBracket; i < rightBracket; i++) {

                allRecordsList[i].showDownload = false;
                allRecordsList[i].showSync = false;
                allRecordsList[i].showLink = false;
                allRecordsList[i].showSpinner = false;
                allRecordsList[i].noAction = false;
                if(allRecordsList[i].documentId != null)  {

                   allRecordsList[i].showDownload = true;
                   if(allRecordsList[i].documentIdSobjectType == 'ContentDocument') {
                      allRecordsList[i].previewhref = '/lightning/r/ContentDocument/' + allRecordsList[i].documentId + '/view';
                      allRecordsList[i].downloadhref = '/sfc/servlet.shepherd/document/download' + '/' + allRecordsList[i].documentId;
                   } else {
                      allRecordsList[i].previewhref = '/' + allRecordsList[i].documentId;
                   }
                } else if(allRecordsList[i].documentUrl != null) {

                     allRecordsList[i].showLink = true;
                 } else if(allRecordsList[i].externalId != null || allRecordsList[i].discoFileId != null) {

                    allRecordsList[i].showSync = true;
                } else {

                    allRecordsList[i].noAction = true;
                }

                allRecordsList[i].values = allRecordsList[i].values.slice(0, this.relatedList.columns.length);
                if (allRecordsList[i].values.length > 0) {
                    for (let j = 0; j < allRecordsList[i].values.length; j++) {
                        try {
                            allRecordsList[i].values[j].key = j;
                        } catch (error) {

                        }
                    }
                }
                shownPageRecordsList.push(allRecordsList[i]);
           }
           this.shownPageRecordsList=shownPageRecordsList;
        }
        this.showSpinner=false;
    }
    //event handler
    buttonPageClickHandler(event){
        this.showSpinner=true;
        let newPage = event.detail.pageNumber;
        this.currentPageNumber=newPage;
        this.showPage(newPage);
    }

    // Navigate to view Record Page
    navigateToViewRecordPage(event) {

        //var recordId = event.target.name;
        var recordId = event.currentTarget.name;
        console.log('navigateToViewRecordPage - recordId', recordId);

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": recordId,
                "actionName": "view"
            },
        });
    }

    sortRelatedList(event){
        if(this.sortDirection == 'ASC'){
            this.sortDirection = 'DESC';
        } else if(this.sortDirection == 'DESC'){
            this.sortDirection = 'ASC';
        }
        var columnToSort = event.currentTarget.name;
        var columnToSortIndex = event.currentTarget.dataset.index;

        createRelatedList( {recordId: this.recordId,
                            relationshipFieldAPIName: this.relationshipFieldAPIName,
                            fieldSet: this.fieldSet,
                            orderByColumnIndex: columnToSortIndex,
                            orderByDirection: this.sortDirection})

            .then(result => {
                if (result.error == false) {

                    this.numOfRecords = result.relatedList.records.length;
                    this.relatedList = result.relatedList;
                    this.numOfPages=Math.ceil(this.numOfRecords /this.recordsForPage);
                    if(this.numOfRecords>this.recordsForPage){
                        this.showPagination=true;
                    }
                    this.showPage(this.currentPageNumber);

                    var arrowIconList = this.template.querySelectorAll("lightning-icon");
                    arrowIconList.forEach(function(element){

                        if(element.dataset.name == 'sortRow' && element.dataset.index == columnToSortIndex){
                            element.classList.remove('slds-hide');
                            element.classList.add('slds-show');
                        }else if(element.dataset.name == 'sortRow' && element.dataset.index != columnToSortIndex){
                            element.classList.remove('slds-show');
                            element.classList.add('slds-hide');
                        }
                    },this);

                }else {

                    this.showError(result.errorMessage);
                    console.log('MRO_LC_FileRelatedListCnt.createRelatedList (Callback) errorMessage', result.errorMessage);
                    console.log('MRO_LC_FileRelatedListCnt.createRelatedList (Callback) errorStackTraceString', result.errorStackTraceString);
                }
                this.showSpinner = false;
            })
    }

    get IsSortDirectionEqualsASC(){
        return this.sortDirection == 'ASC' ? true : false;
    }

    get IsSortDirectionEqualsDESC(){
        return this.sortDirection == 'DESC' ? true : false;
    }

    handleSync(event) {

        let fileMetadataId = event.target.id.split("-")[0];
        console.log('Record Id', fileMetadataId);
        console.log('shownPageRecordsList', JSON.stringify(this.shownPageRecordsList));
        for (let i = 0; i < this.shownPageRecordsList.length; i++) {

            console.log('Record', JSON.stringify(this.shownPageRecordsList[i]));
            if(this.shownPageRecordsList[i].id == fileMetadataId) {

                this.shownPageRecordsList[i].showSpinner = true;
                this.shownPageRecordsList[i].showSync = false;
            }
        }
        retrieveFromDMS( {recordId: fileMetadataId} )

        .then(result => {
            if (result.error == false) {

                if(result.code = 'OK') {

                    this.showSuccess('Data retrieval from document management system started.');
                } else {

                    this.showError('Error occurred sending retrieval request to document management system. ' + result.description);
                }
            }else {

                this.showError(result.errorMessage + ' ' + result.errorStackTraceString);
                console.log('MRO_LC_FileRelatedListCnt.retrieveFromDMS (Callback) errorMessage', result.errorMessage);
                console.log('MRO_LC_FileRelatedListCnt.retrieveFromDMS (Callback) errorStackTraceString', result.errorStackTraceString);
            }
            this.showSpinner = false;
        })
   }

   showSuccess(message) {

       let toastTitle = this.labels.success;
       let toastMsg = message;
       let toastVariant = 'success';

       this.dispatchEvent(
           new ShowToastEvent({
               title: toastTitle,
               message: toastMsg,
               variant: toastVariant
           })
       );
   }

   showError(message) {

       let toastTitle = this.labels.success;
       let toastMsg = message;
       let toastVariant = 'error';

       this.dispatchEvent(
           new ShowToastEvent({
               title: toastTitle,
               message: toastMsg,
               variant: toastVariant
           })
       );
   }
 }