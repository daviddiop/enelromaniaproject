import { LightningElement, api, track } from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import { error } from 'c/notificationSvc';
import { labels } from 'c/labels';

export default class IndividualListItem extends LightningElement {
    @api interlocutor;
    @api interactionId;

    labels = labels;
    @track VatNumberIdentityNumber = labels.VATNumber+' / '+labels.nationalIdentityNumber;
    interlocutorClick() {
        let interlocutorClone = Object.assign({}, this.interlocutor);
        interlocutorClone.showAccountContactRelation = false;
        if (this.interactionId) {
            notCacheableCall({
                className: 'IndividualCnt',
                methodName: 'updateInteraction',
                input: {
                    individualId: interlocutorClone.Id,
                    interactionId: this.interactionId
                }
            }).then((response) => {
                if (response.isError) {
                    error(this, JSON.stringify(response.error));
                } else {
                    interlocutorClone.showAccountContactRelation = !response.data.isRedirectToInteraction;
                    this.dispatchEvent(new CustomEvent('select', {
                        detail: {
                            'individual': interlocutorClone
                        }
                    }));
                }
            }).catch((errorMsg) => {
                error(this, errorMsg.body.output.errors[0].message);
            });
        } else {
            this.dispatchEvent(new CustomEvent('select', {
                detail: {
                    'individual': interlocutorClone
                }
            }));
        }
    }
}