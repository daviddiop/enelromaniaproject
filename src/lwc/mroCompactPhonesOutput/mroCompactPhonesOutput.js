/**
 * Created by aspiga on 12/12/2019.
 */
import {LightningElement, api, track} from 'lwc';
import {labels} from "c/mroLabels";

export default class MroCompactPhonesOutput extends LightningElement {
    labels = labels;
    @api title = this.labels.phoneNumbers;
    @api phone;
    @api mobilePhone;
    @api display = "comfy"; // otherwise it can be 'compact', with title and phone numbers on the same row

    get numbersDivClass() {
        return this.display === 'comfy' ? 'slds-form-element__control' : 'slds-form-element__control slds-grid';
    }
    get blockDivClass() {
        return this.display === 'comfy' ? 'slds-form-element_stacked slds-form-element' : 'slds-form-element_stacked slds-form-element slds-grid';
    }
    get firstNumberClass() {
        return this.display === 'comfy' ? 'slds-media slds-media_center slds-media_small slds-m-bottom--xx-small' : 'slds-media slds-media_center slds-media_small';
    }

}