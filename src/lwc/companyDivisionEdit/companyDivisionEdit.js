import { LightningElement, track, api } from "lwc";
import { NavigationMixin } from "lightning/navigation";
import Id from "@salesforce/user/Id";
import { labels } from "c/labels";
import { error, success } from "c/notificationSvc";
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";

export default class CompanyDivisionSelectionEdit extends NavigationMixin(LightningElement) {
    @api userId = Id;
    @track disableEnforced = false;
    @track companyDivisionId = "";
    @track companyDivisionName;
    @track companyDivisionEnforced = false;
    @track companyDivisionList;
    @track isEdit = false;
    oldCompanyDivisionId;
    oldCompanyDivisionName;
    oldCompanyDivisionEnforced;
    oldDisableEnforced;
    labels = labels;

    connectedCallback() {
        this.loadCompanyDivisionData();
    }

    editClick() {
        this.isEdit = true;
        this.oldCompanyDivisionId = this.companyDivisionId;
        this.oldCompanyDivisionName = this.companyDivisionName;
        this.oldCompanyDivisionEnforced = this.companyDivisionEnforced;
        this.oldDisableEnforced = this.disableEnforced;
    }

    cancelClick() {
        this.isEdit = false;
        this.companyDivisionId = this.oldCompanyDivisionId;
        this.companyDivisionName = this.oldCompanyDivisionName;
        this.companyDivisionEnforced = this.oldCompanyDivisionEnforced;
        this.disableEnforced = this.oldDisableEnforced;
    }

    saveClick() {
        let inputCmp = this.template.querySelector("lightning-combobox");
        if (inputCmp.value === null || inputCmp.value === "") {
            inputCmp.setCustomValidity(this.labels.requiredFields);
            inputCmp.reportValidity();
        } else {
            this.updateUserCompanyDivision();
            this.isEdit = false;
        }
    }

    handleKeyUp(event) {
        if (event.key === "Enter") {
            this.onSaveClick();
        }
    }

    loadCompanyDivisionData() {
        let inputs = { userId: this.userId };
        notCacheableCall({
            className: "CompanyDivisionCnt",
            methodName: "getCompanyDivisionData",
            input: inputs
        })
            .then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.errorMsg);
                    } else {
                        if (response.data) {
                            if (response.data.companyDivision) {
                                this.companyDivisionName = response.data.companyDivision.Name;
                                this.companyDivisionId = response.data.companyDivision.Id;
                                if(response.data.companyDivisionEnforced){
                                    this.companyDivisionEnforced = response.data.companyDivisionEnforced;
                                }
                                if (!this.companyDivisionId) {
                                    this.disableEnforced = true;
                                    this.companyDivisionEnforced = false;
                                }
                            }
                            if (response.data.listCompaniesDivision) {
                                this.companyDivisionList = response.data.listCompaniesDivision;
                            }
                        }
                    }
                }
            })
            .catch(errorMsg => {
                error(this, errorMsg);
            });
    }

    updateUserCompanyDivision() {
        let inputs = {
            companyId: this.companyDivisionId,
            userId: this.userId,
            companyDivisionEnforced: this.companyDivisionEnforced
        };
        notCacheableCall({
            className: "CompanyDivisionCnt",
            methodName: "updateUserCompanyDivision",
            input: inputs
        })
            .then(response => {
                if (response) {
                    if (response.isError) {
                        error(this, response.errorMsg);
                    } else {
                        success(this, "Company division successfully updated");
                    }
                }
            })
            .catch(errorMsg => {
                error(this, errorMsg);
            });
    }

    handleDivisionChange(event) {
        let selectedCompany = this.companyDivisionList.filter(function(company) {
            return company.value === event.detail.value;
        });

        if (selectedCompany) {
            this.companyDivisionName = selectedCompany[0].label;
            this.companyDivisionId = selectedCompany[0].value;
            this.disableEnforced = false;
        }
    }

    navigateToCompanyDivision() {
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                recordId: this.companyDivisionId,
                objectApiName: "CompanyDivision__c",
                actionName: "view"
            }
        });
    }

    handleChange() {
        let inputCmp = this.template.querySelector("lightning-input");
        this.companyDivisionEnforced = inputCmp.checked;
    }


}