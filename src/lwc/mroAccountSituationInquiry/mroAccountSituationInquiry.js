/**
 * Created by aspiga on 24/03/2020.
 */

import {LightningElement, track,api} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {labels} from 'c/mroLabels';
import {error} from 'c/notificationSvc';
export default class MroAccountSituationInquiry extends NavigationMixin(LightningElement) {

    @api recordId;
    @track companyDivisionId;
    @track companyDivisionCode;
    @track startDate;
    @track endDate;
    @track defaultDate;
    @track isValidDate = true;
    @track selectedFisaIds;

    labels = labels;
    connectedCallback() {
        this.setPeriodSelectionDate();
    }

    companyDivisionChange(event) {
        console.log('this.companyDivision event: ' + JSON.stringify(event.detail));
        if(event.detail.divisionId){
            this.companyDivisionId = event.detail.divisionId;
            console.log('this.companyDivisionId: '+this.companyDivisionId);
        }
        if(this.companyDivisionId){
            this.getCompanyDivisionCode(this.companyDivisionId);
        }
    }

    getCompanyDivisionCode(companyDivisionId) {
        let inputs = {companyDivisionId};
        notCacheableCall({
            className: "MRO_LC_AccountSituation",
            methodName: "retrieveCompanyDivisionCode",
            input: inputs
        })
        .then((response) => {
            if (response.isError) {
                error(this, JSON.stringify(response.error));
            }

            this.companyDivisionCode = response.data.companyDivisionCode;
            console.log('this.companyDivisionCode: '+this.companyDivisionCode);
        })
        .catch((errorMsg) => {
            error(this, JSON.stringify(errorMsg));
        });

    }
    setPeriodSelectionDate () {
        let dateOfToday = new Date();
        let today = dateOfToday.getFullYear() + "-" + (dateOfToday.getMonth() + 1) + "-" + dateOfToday.getDate();
        //let minDate = dateOfToday.getFullYear()+ "-" +((dateOfToday.getMonth() + 1)-3) + "-" + dateOfToday.getDate();
        let startDate = new Date(dateOfToday.getFullYear(), 0, 1);
        let firstDay = startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate();
        this.endDate = today;
        //this.minDate = today;
        this.startDate = firstDay;
        this.defaultDate = firstDay;
    }
    handleStartDateChange(event) {

        //console.log('event.detail Date: ' + event.target.value);
        let inputStartDate = event.target.value;
        if (this.endDate) {
            let parsedInputStartDate = Date.parse(inputStartDate);
            let parsedInvoiceEndDate = Date.parse(this.endDate);

            if (parsedInputStartDate >= parsedInvoiceEndDate) {
                error(this, labels.startDateAfterEndDate);
                inputStartDate = "";
                event.target.value = inputStartDate;
            }
        }
        this.startDate = inputStartDate;
        let fisaInquiryComponent = this.template.querySelector('c-mro-fisa-inquiry');
        if (fisaInquiryComponent){
            fisaInquiryComponent.enabeRetrieveButton();
        }
    }
    getFisaSelect(event) {
        this.selectedFisaIds = event.detail.fisaIds;
    }

    get hasCompanyCode(){
        //console.log('companyDivisionId: ' + this.companyDivisionId);
        return (this.companyDivisionId !== '' && this.companyDivisionId !== undefined && this.companyDivisionId !== null);
    }
}