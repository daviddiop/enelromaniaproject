import { LightningElement, api, wire, track } from 'lwc';
import SVG_URL from '@salesforce/resourceUrl/EnergyAppResources';
//import { getRecord } from 'lightning/uiRecordApi';
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import { labels } from 'c/labels';

export default class OpportunityServiceItemTile extends LightningElement {
    label = labels;
    @api recordId;
    @api disabled;
    @api accountId;
    @api isRow = false;
    @api autoSave = false;
    @api readOnlyAddress = false;
    @api selectOsiTileFields = [];
    @api showOsiTileFields = false;
    @api tileTitle;
    @track defaultTileTitle;


    @track isOpenDelete = false;
    @track isOpenEdit = false;
    @track eletricRecordTypeId;
    @track gasRecordTypeId;
    @track recordTypeIdCmp;
    @track viewOsiTileFields = [];


    @track isGas;
    @track isElectric;
    /*
    @wire(getRecord, { recordId: '$recordId', fields: ['OpportunityServiceItem__c.RecordType.DeveloperName'] })
    wiredRecord({ error, data }) {
        if (data) {
            this.isElectric = data.fields.RecordType.value.fields.DeveloperName.value === 'Electric';
            this.isGas = data.fields.RecordType.value.fields.DeveloperName.value === 'Gas';
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.record = undefined;
        }
    }
     */

    connectedCallback() {
        this.defaultTileTitle = this.tileTitle ?  this.tileTitle : this.label.osiEdit;

        if ((this.selectOsiTileFields.length > 0)) {
            this.viewOsiTileFields = this.selectOsiTileFields.filter(function (el) {
                return (!(el.includes('.')));
            });
        }
        notCacheableCall({
            className: 'OpportunityServiceItemCnt',
            methodName: 'getOsiRecordTypeDevName',
            input: {
                'recordId': this.recordId
            },
        }).then(apexResponse => {
            if (apexResponse) {
                if (apexResponse.isError) {
                    error(this, apexResponse.error);
                } else {
                    if (apexResponse.data.osiRecordTypeDevName) {
                        this.isElectric = apexResponse.data.osiRecordTypeDevName === 'Electric';
                        this.isGas = apexResponse.data.osiRecordTypeDevName === 'Gas';
                    }
                }
            }
        }).catch(errorMsg => {
            error(this, errorMsg);
        });
    }

    get cardClass() {
        if (this.disabled) {
            return 'slds-class slds-theme_shade';
        }
        return 'slds-class';
    }

    get svgURLEle() {
        return SVG_URL + '/images/Electric.svg#electric';
    }
    get svgURLGas() {
        return SVG_URL + '/images/Gas.svg#gas';
    }

    editOppServiceItem() {
        this.isOpenEdit = true;
        //start add by David
        let inputs = {osiId: this.recordId};
        notCacheableCall({
            className: 'OpportunityServiceItemCnt',
            methodName: 'getOsiAddressField',
            input: inputs
        })
            .then((apexResponse) => {
                if (apexResponse.isError) {
                    error(this, JSON.stringify(apexResponse.error));
                }
                let addr = {};
                addr = apexResponse.data.osiAddress;
                if(addr.addressNormalized){

                }
            })
            .catch((errorMsg) => {
                error(this, JSON.stringify(errorMsg.body.output.errors[0].message));
                this.spinner = false;
            });
        this.readOnlyAddress = true;
        // end
    }

    deleteOppServiceItem() {
        this.isOpenDelete = true;
    }

    closeModal() {
        this.isOpenDelete = false;
    }

    closeModalEdit() {
        this.isOpenEdit = false;
    }

    deletedItem() {

        deleteRecord(this.recordId)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: this.label.success,
                        variant: 'success'
                    })
                );
                this.dispatchEvent(new CustomEvent('delete', {
                    detail: {
                        'recordId': this.recordId
                    }
                }, { bubbles: true }));
                this.isOpenDelete = false;
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error deleting record',
                        message: this.label.error,
                        variant: 'error'
                    })
                );
            });
    }

    closeAfterSave() {
        this.isOpenEdit = false;
    }

}