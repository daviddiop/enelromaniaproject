/**
 * Created by Boubacar Sow on 28/07/2020.
 */

import {api, LightningElement, track, wire} from 'lwc';
import notCacheableCall from "@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall";
import {labels} from 'c/mroLabels';
import {error, success} from 'c/notificationSvc';
import {NavigationMixin} from "lightning/navigation";
import {getRecord} from 'lightning/uiRecordApi';

export default class MroZfofmQuery extends NavigationMixin(LightningElement) {
    @api startDate;
    @api endDate;
    @api caseId;
    @api contractAccountId;
    @api accountId;
    @api cnpCui;
    @api documentType;
    @api c39 ;
    @api codCompany ;
    @api hideConfirmSelection = false;
    @api recordId;
    @track zpftFilnetList;
    @track showLoadingSpinner = false;

    @api
    get amount() {
        return this._amount;
    }

    set amount(value) {
        this._amount = value;
    }

    @api months = 5;
    @track columns;
    @track valuesList =
        [{label: 'Month', value: 'Amount'}];
    @track dataObj = [];
    @track _amount;
    @track error;
    @track showPlan = false;


    labels = labels;

    @wire(getRecord, {recordId: '$recordId', fields: ['Case.Amount__c']})
    wiredRecord({error, data}) {
        if (data) {
            this._amount = data.fields.Amount__c.value;
        } else if (error) {
            this.error = error;
        }
    }

    connectedCallback(){
        this.hideConfirmSelection = true;
        this.caseId = this.recordId;
    }
    handleRetrieveFilnet(){
        let input = {
            'caseId': this.caseId
        };

        this.showLoadingSpinner = true;

        notCacheableCall({
            className: "MRO_LC_FileNetDoc",
            methodName: "listDoc",
            input: input
        }).then(apexResponse => {
            this.showLoadingSpinner = false;
            if (!apexResponse) {
                error(this, 'Unexpected Error');
            }
            if (apexResponse.isError) {
                error(this, apexResponse.error);
                return;
            }
            if (!apexResponse.data.error){
                this.zpftFilnetList = JSON.parse(JSON.stringify(apexResponse.data));
                if (!this.zpftFilnetList || this.zpftFilnetList.length === 0){
                    error(this, 'empty list ZFOFM');
                }
            }
        }).catch((errorMsg) => {
            this.showLoadingSpinner = false;
            error(this, JSON.stringify(errorMsg));
        });

    }

    findzfofm(zfofmId,fieldName) {
        if (!zfofmId) {
            return null;
        }
        return this.zpftFilnetList.find(function (zfofm) {
            return zfofm[fieldName] === zfofmId;
        });
    }

    navigateToContractAccount(event) {

        let zfofmId = event.target.dataset.id;
        let foundZfofm = this.findzfofm(zfofmId,'contractAccountId');
        console.log('### foundZfofm '+JSON.stringify(foundZfofm));

        if (!foundZfofm) {
            return;
        }
        this.contractAccountId = foundZfofm.contractAccountId;
        console.log('### contractAccountId '+this.contractAccountId);
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.contractAccountId,
                "objectApiName": "ContractAccount__c",
                "actionName": "view"
            }
        });
    }

    navigateToAccount(event) {

        let zfofmId = event.target.dataset.id;
        let foundZfofm = this.findzfofm(zfofmId,'accountId');
        console.log('### foundZfofm '+JSON.stringify(foundZfofm));

        if (!foundZfofm) {
            return;
        }
        this.accountId = foundZfofm.accountId;
        console.log('### accountId '+this.accountId);
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.accountId,
                "objectApiName": "Account",
                "actionName": "view"
            }
        });
    }

    navigateToCase(event) {

        let zfofmId = event.target.dataset.id;
        let foundZfofm = this.findzfofm(zfofmId,'caseId');
        console.log('### foundZfofm '+JSON.stringify(foundZfofm));

        if (!foundZfofm) {
            return;
        }
        this.caseId = foundZfofm.caseId;
        console.log('### caseId '+this.caseId);
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                "recordId": this.caseId,
                "objectApiName": "Case",
                "actionName": "view"
            }
        });
    }

    handleRetrievePlan(){
        this.handleRetrieveFilnet();
        if(this.columns){
            return;
        }
        this.columns = [
            {label: 'Month', fieldName: 'Month', type: 'text'}
        ];
        let values = this._amount / this.months;
        console.log(' value '+values);
        for (let i = 1; i <= this.months; i++) {
            this.columns = [...this.columns, {
                label: '' + i,
                fieldName: '' + i,
                type: 'text'
            }];
            this.valuesList = [...this.valuesList, {
                label: '' + i,
                value: '' + values
            }];
        }
        let results = this.valuesList.reduce(function (map, obj) {
            map[obj.label] = obj.value;
            return map;
        }, {});
        this.dataObj = [results];
        this.showPlan = true;
    }


}