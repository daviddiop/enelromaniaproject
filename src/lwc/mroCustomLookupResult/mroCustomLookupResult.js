/* eslint-disable eqeqeq */
/* eslint-disable no-unused-vars */
import { LightningElement, api } from 'lwc';

export default class MroCustomLookupResult extends LightningElement {

    @api result;
    @api objectApiName;
    @api iconName;
    @api text;
    @api primaryDisplayField = '';
    @api secondaryDisplayField;

    /**
     *  Method that catches the event of selecting a result and passes this to parent
     */
    selectRecord (event) {
        let newEvent = new CustomEvent ('selectedrecord', {detail: this.result});
        this.dispatchEvent(newEvent);
    }

    showPrimaryDisplayField () {
        return this.primaryDisplayField !== '';
    }

}