import {LightningElement, api, track} from 'lwc';
import notCacheableCall from '@salesforce/apex/ApexServiceLibraryCnt.notCacheableCall';
import {error} from 'c/notificationSvc';
import { labels } from 'c/labels';

export default class MroCustomerInteractionList extends LightningElement {
    _interactionId;
    labels = labels;
    @track address='';
    @track _disabled;

    @api
    get interactionId() {
        return this._interactionId;
    }
    set interactionId(value) {
        this._interactionId = value;
        this.initData();
    }

    @api
    get disabled() {
        return this._disabled;
    }
    set disabled(value) {
        this._disabled = value;
    }

    @api
    get enableMultipleCustomerSelection() {
        return this.isMultipleCustomerSelectionEnabled;
    }
    set enableMultipleCustomerSelection(value) {
        this.isMultipleCustomerSelectionEnabled = value;
    }

    get addClass() {
        if (this._disabled) {
            return "slds-box slds-box_x-small slds-media slds-m-around_xxx-small slds-theme_shade wr-pointer-event_none";
        }
        return "slds-box slds-box_x-small slds-media slds-m-around_xxx-small slds-theme_default";
    }

    get hrefClass() {
        if (this._disabled) {
            return "slds-has-blur-focus wr-pointer-event_none";
        }
        return "slds-text-link";
    }

    @track customerInteractionList;
    @track rolesList;
    @track isInteractionClosed = false;
    @track isMultipleCustomerSelectionEnabled = true;

    @api
    get isNotEmpty() {
        return this.customerInteractionList && this.customerInteractionList.length > 0;
    }

    @api
    get isMultipleSelectionDisabled() {
        if(this.isMultipleCustomerSelectionEnabled == true){
            return false;
        }
        return this.isNotEmpty;
    }

    @api
    refresh() {
        this.initData();
    }

    customerInteractionListItemEventHandler(event) {
        let type = event.detail.type;
        let customerInteraction = Object.assign({}, event.detail.customerInteraction);
        if (type === 'delete') {
            this.deleteCustomerInteraction(customerInteraction);
        } else if (type === 'edit') {
            if (customerInteraction.role !== event.detail.newRole) {
                customerInteraction.role = event.detail.newRole;
                this.updateRole(customerInteraction);
            }
        }
    }

    initData() {
        if (this.interactionId) {
            notCacheableCall({
                className: 'MRO_LC_CustomerInteraction',
                methodName: 'initData',
                input: { interactionId: this.interactionId },
            }).then((response) => {
                this.rolesList = response.data.rolesList;
                this.isInteractionClosed = response.data.isInteractionClosed;
                this.customerInteractionList = response.data.customerInteractionList;
            }).catch(errorMessage => {
                error(this, errorMessage);
            });
        }
    }
    updateRole(customerInteraction) {
        notCacheableCall({
            className: 'MRO_LC_CustomerInteraction',
            methodName: 'updateRole',
            input: { customerInteraction: JSON.stringify(customerInteraction)},
        }).then((response) =>{
            this.customerInteractionList = response.data;
        }).catch(errorMessage => {
            error(this, errorMessage);
        });
    }


    @api
    deleteCustomerInteraction(customerInteraction) {
        notCacheableCall({
            className: 'MRO_LC_CustomerInteraction',
            methodName: 'deleteCustomerInteraction',
            input: { customerInteraction: JSON.stringify(customerInteraction)},
        }).then((response) =>{
            this.customerInteractionList = JSON.stringify(response);
            this.dispatchEvent(new CustomEvent('remove'));
        }).catch(errorMessage => {
            error(this, errorMessage);
        });
        /*deleteCustomerInteraction({customerInteraction: customerInteraction}).then(response => {
            this.customerInteractionList = response;
            this.dispatchEvent(new CustomEvent('remove'));
        })
        .catch(errorMessage => {
            error(this, errorMessage);
        });*/
    }
    /* updateRole(customerInteraction) {
         updateRole({customerInteraction: customerInteraction})
             .then(response => {
                 this.customerInteractionList = response;
             })
             .catch(errorMessage => {
                 error(this, errorMessage);
             });
     }*/

    onNew() {
        this.dispatchEvent(new CustomEvent('new'));
    }
}