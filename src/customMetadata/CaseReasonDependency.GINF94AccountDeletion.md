<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GINF94AccountDeletion</label>
    <protected>false</protected>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Account Deletion</value>
    </values>
    <values>
        <field>CaseSubType__c</field>
        <value xsi:type="xsd:string">GINF94 - Enel Website/MyEnel Account Information</value>
    </values>
</CustomMetadata>