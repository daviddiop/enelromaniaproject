<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Site Addresses (OSI)</label>
    <protected>false</protected>
    <values>
        <field>AddressPrefix__c</field>
        <value xsi:type="xsd:string">Site</value>
    </values>
    <values>
        <field>QueryCriteria__c</field>
        <value xsi:type="xsd:string">Opportunity__r.IsClosed = false</value>
    </values>
    <values>
        <field>QueryRelation__c</field>
        <value xsi:type="xsd:string">Opportunity__r.AccountId</value>
    </values>
    <values>
        <field>SObjectApiName__c</field>
        <value xsi:type="xsd:string">OpportunityServiceItem__c</value>
    </values>
</CustomMetadata>
