<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>II Contract Acceptance Email</label>
    <protected>false</protected>
    <values>
        <field>SendingChannel__c</field>
        <value xsi:type="xsd:string">Email</value>
    </values>
    <values>
        <field>Source__c</field>
        <value xsi:type="xsd:string">InternetInterface</value>
    </values>
    <values>
        <field>TokenLifetime__c</field>
        <value xsi:type="xsd:double">730.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">ContractAcceptance</value>
    </values>
    <values>
        <field>URLPath__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
