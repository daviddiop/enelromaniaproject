<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>INF42UpdatingAdministrativeInfo</label>
    <protected>false</protected>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Updating administrative info</value>
    </values>
    <values>
        <field>CaseSubType__c</field>
        <value xsi:type="xsd:string">INF42 - Supply contracts</value>
    </values>
</CustomMetadata>