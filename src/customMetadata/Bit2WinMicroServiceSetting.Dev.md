<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Dev</label>
    <protected>false</protected>
    <values>
        <field>ClientId__c</field>
        <value xsi:type="xsd:string">b3weN3Lr0.19!</value>
    </values>
    <values>
        <field>EndPoint__c</field>
        <value xsi:type="xsd:string">https://b2winginsvileu3.herokuapp.com/b2wgin/api/19r1.1</value>
    </values>
    <values>
        <field>OrgId__c</field>
        <value xsi:type="xsd:string">00D1l0000000U8nEAE</value>
    </values>
</CustomMetadata>
