<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SwitchIn_GAS - Expired Contracts</label>
    <protected>false</protected>
    <values>
        <field>CaseRecordTypeDevName__c</field>
        <value xsi:type="xsd:string">SwitchIn_GAS</value>
    </values>
    <values>
        <field>CaseTypeCode__c</field>
        <value xsi:type="xsd:string">G01</value>
    </values>
    <values>
        <field>Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SLACalendarDays__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SLADays__c</field>
        <value xsi:type="xsd:double">15.0</value>
    </values>
    <values>
        <field>SubProcess__c</field>
        <value xsi:type="xsd:string">ExpiredContract</value>
    </values>
</CustomMetadata>
