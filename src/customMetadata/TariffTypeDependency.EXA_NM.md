<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EXA_NM</label>
    <protected>false</protected>
    <values>
        <field>NewTariffType__c</field>
        <value xsi:type="xsd:string">NM</value>
    </values>
    <values>
        <field>OldTariffType__c</field>
        <value xsi:type="xsd:string">D</value>
    </values>
    <values>
        <field>ProductCap__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ProductIndexType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ProductIndexed__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ProductNameEndCharacter__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ProductSGP__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Retention__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SalesTypeSoftVas__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SupplyFlatRate__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
