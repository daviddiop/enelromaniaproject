<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Termination_SRV - VAS Termination</label>
    <protected>false</protected>
    <values>
        <field>CaseRecordTypeDevName__c</field>
        <value xsi:type="xsd:string">Termination_SRV</value>
    </values>
    <values>
        <field>CaseTypeCode__c</field>
        <value xsi:type="xsd:string">C124</value>
    </values>
    <values>
        <field>Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SLACalendarDays__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SLADays__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>SubProcess__c</field>
        <value xsi:type="xsd:string">Termination of an existing Soft VAS</value>
    </values>
</CustomMetadata>
