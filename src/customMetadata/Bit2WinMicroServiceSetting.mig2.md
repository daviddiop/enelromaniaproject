<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MIG 2</label>
    <protected>false</protected>
    <values>
        <field>ClientId__c</field>
        <value xsi:type="xsd:string">82w3nE1r0d3Vm162</value>
    </values>
    <values>
        <field>EndPoint__c</field>
        <value xsi:type="xsd:string">https://b2winginsvileu3.herokuapp.com/b2wgin/api/19r1.1</value>
    </values>
    <values>
        <field>OrgId__c</field>
        <value xsi:type="xsd:string">00D3N0000008nCDUAY</value>
    </values>
</CustomMetadata>
