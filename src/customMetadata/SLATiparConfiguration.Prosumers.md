<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prosumers</label>
    <protected>false</protected>
    <values>
        <field>CaseRecordTypeDevName__c</field>
        <value xsi:type="xsd:string">Prosumers</value>
    </values>
    <values>
        <field>CaseTypeCode__c</field>
        <value xsi:type="xsd:string">C211</value>
    </values>
    <values>
        <field>Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SLACalendarDays__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SLADays__c</field>
        <value xsi:type="xsd:double">15.0</value>
    </values>
    <values>
        <field>SubProcess__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
