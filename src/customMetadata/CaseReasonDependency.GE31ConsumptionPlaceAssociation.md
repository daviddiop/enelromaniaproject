<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GE31ConsumptionPlaceAssociation</label>
    <protected>false</protected>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Consumption Place Association</value>
    </values>
    <values>
        <field>CaseSubType__c</field>
        <value xsi:type="xsd:string">GE31 - MyEnel account (create / access / modify)</value>
    </values>
</CustomMetadata>