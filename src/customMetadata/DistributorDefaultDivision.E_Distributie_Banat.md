<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>E Distributie Banat</label>
    <protected>false</protected>
    <values>
        <field>CompanyDivisionVATNumber__c</field>
        <value xsi:type="xsd:string">RO22000460</value>
    </values>
    <values>
        <field>DistributorVATNumber__c</field>
        <value xsi:type="xsd:string">RO14490379</value>
    </values>
</CustomMetadata>
