<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Street Type</label>
    <protected>false</protected>
    <values>
        <field>AddressLayout__c</field>
        <value xsi:type="xsd:string">DefaultAddressLayout</value>
    </values>
    <values>
        <field>ColSize__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>FieldName__c</field>
        <value xsi:type="xsd:string">streetType</value>
    </values>
    <values>
        <field>Hidden__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>ParentsFields__c</field>
        <value xsi:type="xsd:string">Country,Province,City</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SourceField__c</field>
        <value xsi:type="xsd:string">StreetType</value>
    </values>
    <values>
        <field>SourceTable__c</field>
        <value xsi:type="xsd:string">Street</value>
    </values>
</CustomMetadata>
