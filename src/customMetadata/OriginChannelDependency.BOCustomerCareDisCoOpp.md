<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BOCustomerCareDisCo</label>
    <protected>false</protected>
    <values>
        <field>ChannelPicklistEntry__c</field>
        <value xsi:type="xsd:string">BackOfficeCustomerCare</value>
    </values>
    <values>
        <field>Channel__c</field>
        <value xsi:type="xsd:string">Magazin Enel</value>
    </values>
    <values>
        <field>ObjectApiName__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>OriginPicklistEntry__c</field>
        <value xsi:type="xsd:string">DisCo</value>
    </values>
    <values>
        <field>Origin__c</field>
        <value xsi:type="xsd:string">Mail</value>
    </values>
</CustomMetadata>
