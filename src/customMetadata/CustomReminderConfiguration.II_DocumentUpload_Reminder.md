<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>II Document Upload Reminder</label>
    <protected>false</protected>
    <values>
        <field>ApexClass__c</field>
        <value xsi:type="xsd:string">MRO_BA_TouchPointReminderHandler</value>
    </values>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">IIDocUplRem</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Internet Interface Document Upload Reminder</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ReminderHours__c</field>
        <value xsi:type="xsd:double">24.0</value>
    </values>
</CustomMetadata>
