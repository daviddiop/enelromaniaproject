<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Refused</label>
    <protected>false</protected>
    <values>
        <field>CampaignType__c</field>
        <value xsi:type="xsd:string">Tacit Renewal</value>
    </values>
    <values>
        <field>HasRespondedStatus__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsDefaultStatus__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SortOrder__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
</CustomMetadata>
