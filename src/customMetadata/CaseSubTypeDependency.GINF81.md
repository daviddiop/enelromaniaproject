<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GINF81</label>
    <protected>false</protected>
    <values>
        <field>CaseRecordType__c</field>
        <value xsi:type="xsd:string">InformationRequest</value>
    </values>
    <values>
        <field>CaseType__c</field>
        <value xsi:type="xsd:string">For network activity</value>
    </values>
    <values>
        <field>DisableInvoiceMarking__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>HasBillingAdjustment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>HasInvoiceSelection__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>HasParentCaseField__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">GINF81 - Deadlines for supply cessation / resumption. failure to comply with the contractual conditions or at the clients&apos; request</value>
    </values>
    <values>
        <field>SupplyType__c</field>
        <value xsi:type="xsd:string">Gas</value>
    </values>
</CustomMetadata>
