<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Connection_ELE - Temp. with Definit. Sol</label>
    <protected>false</protected>
    <values>
        <field>CaseRecordTypeDevName__c</field>
        <value xsi:type="xsd:string">Connection_ELE</value>
    </values>
    <values>
        <field>CaseTypeCode__c</field>
        <value xsi:type="xsd:string">C127</value>
    </values>
    <values>
        <field>Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SLACalendarDays__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SLADays__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>SubProcess__c</field>
        <value xsi:type="xsd:string">Temporary Connection with Definitive Solution</value>
    </values>
</CustomMetadata>
