<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>sap</label>
    <protected>false</protected>
    <values>
        <field>ActionValue__c</field>
        <value xsi:type="xsd:string">action=&quot;/CRM2SAP&quot;</value>
    </values>
    <values>
        <field>TimeOut__c</field>
        <value xsi:type="xsd:double">60000.0</value>
    </values>
</CustomMetadata>
