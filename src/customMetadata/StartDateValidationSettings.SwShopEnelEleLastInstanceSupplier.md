<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SwShopEnelEleLastInstanceSupplier</label>
    <protected>false</protected>
    <values>
        <field>ChannelOrigin__c</field>
        <value xsi:type="xsd:string">Shop</value>
    </values>
    <values>
        <field>Commodity__c</field>
        <value xsi:type="xsd:string">Electric</value>
    </values>
    <values>
        <field>DefaultIncrement__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>IsCustomerReading__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsDefaultWorkingDay__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsEnelArea__c</field>
        <value xsi:type="xsd:string">true</value>
    </values>
    <values>
        <field>IsMarketChange__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsSameOwner__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsValidationWorkingDay__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Process__c</field>
        <value xsi:type="xsd:string">SwitchIn</value>
    </values>
    <values>
        <field>SubProcess__c</field>
        <value xsi:type="xsd:string">LastInstanceSupplier</value>
    </values>
    <values>
        <field>ValidationIncrement__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
</CustomMetadata>
