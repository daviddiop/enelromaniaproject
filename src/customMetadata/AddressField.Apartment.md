<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Apartment</label>
    <protected>false</protected>
    <values>
        <field>AddressLayout__c</field>
        <value xsi:type="xsd:string">DefaultAddressLayout</value>
    </values>
    <values>
        <field>ColSize__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>FieldName__c</field>
        <value xsi:type="xsd:string">apartment</value>
    </values>
    <values>
        <field>Hidden__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">12.0</value>
    </values>
    <values>
        <field>ParentsFields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SourceField__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SourceTable__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
