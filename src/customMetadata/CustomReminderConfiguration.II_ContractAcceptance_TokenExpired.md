<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>II Contract Acceptance Token Expired</label>
    <protected>false</protected>
    <values>
        <field>ApexClass__c</field>
        <value xsi:type="xsd:string">MRO_BA_TouchPointReminderHandler</value>
    </values>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">IIConAcpTokExp</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Internet Interface Contract Acceptance Token Expir</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ReminderHours__c</field>
        <value xsi:type="xsd:double">144.0</value>
    </values>
</CustomMetadata>
