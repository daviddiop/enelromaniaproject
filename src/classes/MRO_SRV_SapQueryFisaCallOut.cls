/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 27.07.20.
 */

public with sharing class MRO_SRV_SapQueryFisaCallOut extends MRO_SRV_SapQueryHelper {

    private static final String requestTypeQueryFisa = 'QueryFISA';

    public static MRO_SRV_SapQueryFisaCallOut getInstance() {
        return new MRO_SRV_SapQueryFisaCallOut();
    }

    public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {

        Set<String> conditionFields = new Set<String>{
                'CompanyDivisionCode', 'BillingAccountNumber', 'WithBalance', 'InvoiceIssuedFrom', 'InvoiceIssuedTo', 'InvoiceClearedFrom', 'InvoiceClearedTo', 'CustomerCode', 'DataSold', 'TaxNum'
        };

        return buildSimpleRequest(argsMap, conditionFields);
    }

    public List<MRO_LC_FisaInquiry.Fisa> getList(MRO_LC_FisaInquiry.FisaRequestParam params) {
        Map<String, Object> conditions = new Map<String, Object>{
                'CompanyDivisionCode' => params.codCompanie
        };

        if (params.codDePlataId != null) {
            conditions.put('BillingAccountNumber', params.codDePlataId);
        }

        if (params.cuSold != null) {
            conditions.put('WithBalance', params.cuSold);
        }

        if (params.facturiDela != null) {
            conditions.put('InvoiceIssuedFrom', parseDate(params.facturiDela));
        }

        if (params.facturiPanala != null) {
            conditions.put('InvoiceIssuedTo', parseDate(params.facturiPanala));
        }

        if (params.incasatDela != null) {
            conditions.put('InvoiceClearedFrom', parseDate(params.incasatDela));
        }

        if (params.incasatPanala != null) {
            conditions.put('InvoiceClearedTo', parseDate(params.incasatPanala));
        }

        if (params.partnerId != null) {
            conditions.put('CustomerCode', params.partnerId);
        }

        if (params.dataSold != null) {
            conditions.put('DataSold', parseDate(params.dataSold));
        }

        if (params.taxNum != null) {
            conditions.put('TaxNum', params.taxNum);
        }

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeQueryFisa, conditions, 'MRO_SRV_SapQueryFisaCallOut');

        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
            System.debug(calloutResponse.responses[0].objects);

            System.debug(calloutResponse.responses[0].header);

            List<MRO_LC_FisaInquiry.Fisa> fisaList = new List<MRO_LC_FisaInquiry.Fisa>();

            for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {
                Map<String, String> responseWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);

                MRO_LC_FisaInquiry.Fisa fisaDTO = new MRO_LC_FisaInquiry.Fisa(responseWoMap);

                fisaList.add(fisaDTO);
            }

            System.debug(fisaList);
            return fisaList;
        }
        return null;
    }
}