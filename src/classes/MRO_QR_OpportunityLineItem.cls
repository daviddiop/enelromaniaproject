public inherited sharing class MRO_QR_OpportunityLineItem {
    public static MRO_QR_OpportunityLineItem getInstance() {
        return (MRO_QR_OpportunityLineItem)ServiceLocator.getInstance(MRO_QR_OpportunityLineItem.class);
    }

    public OpportunityLineItem getById(Id opportunityLineItemId) {
        List<OpportunityLineItem> opportunityLineItemList = [
            SELECT Id, Name, Product2Id, Product2.Name, Product2.CommercialProduct__r.RateType__c,
                    Product2.RecordTypeId, Product2.RecordType.DeveloperName, Product2.CommercialProduct__r.ProductCategory__c
            FROM OpportunityLineItem
            WHERE Id = :opportunityLineItemId
        ];

        if (opportunityLineItemList.isEmpty()){
            return null;
        }

        return opportunityLineItemList.get(0);
    }

    public List<OpportunityLineItem> getOLIsByOpportunityId(Id opportunityId) {
        return [
            SELECT Id, Name, UnitPrice, Quantity, Product2Id, Product2.Name, Product2.CommercialProduct__r.RateType__c,
                Product2.RecordTypeId, Product2.RecordType.DeveloperName, Product2.CommercialProduct__c,
                Product2.CommercialProduct__r.ProductTerm__c, Product2.CommercialProduct__r.AdditionalServiceDetails__c
            FROM OpportunityLineItem
            WHERE OpportunityId = :opportunityId
        ];
    }

    public List<OpportunityLineItem> listByProductAndOpportunity(Id productId, Id opportunityId) {
        return [
            SELECT Id, Name, Product2Id, Product2.Name,Product2.CommercialProduct__r.NE__IsPromo__c,
                Product2.RecordTypeId, UnitPrice, Quantity, Product2.RecordType.DeveloperName, Product2.CommercialProduct__c
            FROM OpportunityLineItem
            WHERE Product2Id = :productId
            AND OpportunityId = :opportunityId
        ];
    }

    public List<NE__Order__c> getOrdersByOpportunityId(Id opportunityId) {
        return [
                SELECT Id, Name
                FROM NE__Order__c
                WHERE NE__OptyId__c = :opportunityId
        ];
    }
}