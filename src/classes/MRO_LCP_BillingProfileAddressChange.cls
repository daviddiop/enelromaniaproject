public class MRO_LCP_BillingProfileAddressChange extends ApexServiceLibraryCnt {

    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_SRV_CompanyDivision companyDivisionSrv = MRO_SRV_CompanyDivision.getInstance();
    private static MRO_QR_CompanyDivision companyDivisionQuery = MRO_QR_CompanyDivision.getInstance();


    private static MRO_QR_ServicePoint servicePointQuery = MRO_QR_ServicePoint.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();

    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();

    public class init extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestId = params.get('genericRequestId');
            String gDPRParentRequestId = params.get('gDPRParentRequestId');

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'BillingProfileAddressChange');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);

                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                    response.put('genericRequestId', dossier.Parent__c);
                }

                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(gDPRParentRequestId)) {
                    dossierSrv.updateParent(dossier.Id, gDPRParentRequestId);
                }

                if (String.isNotBlank(dossier.Id)) {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                    response.put('caseTile', cases);
                }

                String code = 'BillingProfileAddressChangeCode_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);

                if (String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }

                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('accountId', accountId);
                response.put('billingProfileAddressChangeRecordType',
                    Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BillingProfileAddressChange').getRecordTypeId());
            } catch (Exception ex) {

                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class updateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);

            try {

                Map<String, String> params = asMap(jsonInput);
                String dossierId = params.get('dossierId');
                String origin = params.get('origin');
                String channel = params.get('channel');

                Dossier__c dossier = new Dossier__c ();
                dossier.Id = dossierId;
                if(String.isNotBlank(channel)){
                    dossier.Channel__c = channel;
                }
                if(String.isNotBlank(origin)){
                    dossier.Origin__c = origin;
                }
                databaseSrv.updateSObject(dossier);

            } catch (Exception ex) {

                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);

            try {

                Map<String, String> params = asMap(jsonInput);

                List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
                String dossierId = params.get('dossierId');
                caseSrv.generateAddressVerificationActivities(
                        caseQuery.listByIds((new Map<Id,Case>(oldCaseList)).keySet()), null);
                caseSrv.setNewCaseAndDossierBillingProfile(oldCaseList, dossierId);

            } catch (Exception ex) {

                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);

            try {

                Map<String, String> params = asMap(jsonInput);

                List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
                String dossierId = params.get('dossierId');

                caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId);

            } catch (Exception ex) {

                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    //START alessio.murru@webresults.it -- 25/11/2020 -- ENLCRO-1827
    public class getSupply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);

            try {
                Map<String, String> params = asMap(jsonInput);
                String contractAccountId = params.get('contractAccountId');

                if (String.isBlank(contractAccountId)) {
                    throw new WrtsException(System.Label.ContractAccount + ' - ' + System.Label.MissingId);
                }
                Supply__c supply = [Select Id from Supply__c where ContractAccount__c =:contractAccountId
                                    AND Status__c = 'Active'
                                    AND ActivationDate__c != null ORDER BY ActivationDate__c desc limit 1];

                response.put('supplyId', supply);

            } catch (Exception ex) {

                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class cloneBillingProfileForCase extends AuraCallable{
        public override Object perform(final String jsonInput){
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);

            try{
                Map<String, String> params = asMap(jsonInput);
                String caseId = params.get('caseId');


                Case caseRecord = MRO_QR_Case.getInstance().getById(caseId);

                BillingProfile__c bp = MRO_QR_BillingProfile.getInstance().getById(caseRecord.BillingProfile__c);

                BillingProfile__c bp2 = bp.clone(false, true, false, false);

                insert bp2;

                caseRecord.BillingProfile2__c = bp2.Id;

                update caseRecord;

            }catch (Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }
}