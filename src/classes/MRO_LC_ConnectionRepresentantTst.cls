/**
 * Created by David DIOP on 04.05.2020.
 */
@IsTest
public with sharing class MRO_LC_ConnectionRepresentantTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        /*BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            //caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    public static void createContactAccountRelationsTst(){
        String vat = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        String NationalIdentityNumber = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();

        Account account = MRO_UTL_TestDataFactory.account().personAccount().build();
        Map<String, String > inputJSON = new Map<String, String>{
                'fields' => JSON.serialize(account)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ConnectionRepresentant', 'createContactAccountRelations', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }

    @IsTest
    public static void getContactByIdTst(){
        Contact contactRecord  = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'contactId' => contactRecord.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ConnectionRepresentant', 'getContactById', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        // Handle an Exception
        inputJSON = new Map<String, String>{
            'contactId' => 'contactId'
        };
        response = TestUtils.exec(
            'MRO_LC_ConnectionRepresentant', 'getContactById', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    public static void updateContactTst(){
        Contact contactRecord  = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        Account accountRecord  = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
                'accountRecord' => JSON.serialize(accountRecord),
                'contactRecord' => JSON.serialize(contactRecord)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ConnectionRepresentant', 'updateContact', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }

    @IsTest
    public static void updateContactOkTst(){
        Contact contactRecord  = [
                SELECT Id,LastName,FirstName,Email,Phone
                FROM Contact
                LIMIT 1
        ];
        Account account = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert  account;
        Account accountRecord  = [
                SELECT Id,IsPersonAccount
                FROM Account
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'accountRecord' => JSON.serialize(accountRecord),
                'contactRecord' => JSON.serialize(contactRecord)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ConnectionRepresentant', 'updateContact', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }


}