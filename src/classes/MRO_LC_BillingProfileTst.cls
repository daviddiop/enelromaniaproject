@isTest
public with sharing class MRO_LC_BillingProfileTst {

    @testSetup
    private static void setup() {

        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createCustomerCodeSequencer().build();
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        Account acc = TestDataFactory.account().setName('Person').build();
        insert acc;

        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Account__c = acc.Id;
        servicePoint.Trader__c = acc.Id;
        servicePoint.Distributor__c = acc.Id;
        insert servicePoint;
        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = acc.Id;
        insert contract;
        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;
        BillingProfile__c billingProfile = TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=acc.Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            //supply.BillingProfile__c = billingProfile.Id;
            supplyList.add(supply);
        }
        insert supplyList;
    }


    @IsTest
    static void getAddressFieldsTest() {
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
            LIMIT 1
        ];
        Test.startTest();
            Map<String, String > inputJSON = new Map<String, String>{
                'billingProfileRecordId' => billingProfile.Id
            };
            Object response = TestUtils.exec('MRO_LC_BillingProfile', 'getAddressFields', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('billingAddress') != null);
    }    

    @IsTest
    static void getAddressFieldsExceptionTest() {
        Test.startTest();
            try {
                Map<String, String > inputJSON = new Map<String, String>{
                    'billingProfileRecordId' => ''
                };
                Object response = TestUtils.exec('MRO_LC_BillingProfile', 'getAddressFields', inputJSON, false);
            } catch (Exception e) {
                System.assert(true, e.getMessage());                
            }
        Test.stopTest();
    } 


    @isTest
    private static void getActiveSuppliesTest() {
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
        ];
        Map<String, String > inputJSON = new Map<String, String>{
        'billingProfileRecordId' => billingProfile.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_BillingProfile', 'getActiveSupplies', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('enableEditButtonBP'));
    }

    @isTest
    private static void getActiveSuppliesExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
        'billingProfileRecordId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('MRO_LC_BillingProfile', 'getActiveSupplies', inputJSON, false);
        } catch (Exception e) {
            System.assertEquals(true, e.getMessage() == 'billingProfileRecordId is required');
        }
        Test.stopTest();
    }

    @isTest
    private static void getBillingProfileRecordsTest() {
        Account myAccount = [
            SELECT Id
            FROM Account
        ];
        Map<String, String > inputJSON = new Map<String, String>{
        'accountId' => myAccount.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_BillingProfile', 'getBillingProfileRecords', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('listBillingProfile') != null);
    }

    @isTest
    private static void getBillingProfileRecordsExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
        'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('MRO_LC_BillingProfile', 'getBillingProfileRecords', inputJSON, false);
        } catch (Exception e) {
            System.assertEquals(true, e.getMessage() == 'accountId is required');
        }
        Test.stopTest();
    }

    @IsTest
    private static void isBusinessClientTest(){
        Account myAccount = [
            SELECT Id
            FROM Account
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => myAccount.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_BillingProfile', 'isBusinessClient', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isBusinessClient') != null);
        Test.stopTest();

    }

    @IsTest
    private static void getMapBankNameWithIdTest(){
        Map<String, String > inputJSON = new Map<String, String>{
            '' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_BillingProfile', 'getMapBankNameWithId', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('mapBankId') != null);
        Test.stopTest();
    }

}