/**
 * Created by  Moussa Fofana  on 07/08/2019.
 */
@isTest
public with sharing class CaseCntTst {

    @testSetup
    static void setup() {
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        listAccount.add(TestDataFactory.account().personAccount().build());
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.Key__c = '1324433';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.BusinessType__c = 'Commercial areas';
        insert accountTrader;

        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        BillingProfile__c billingProfile = TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.PaymentMethod__c = 'Postal Order';
        insert billingProfile;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_ELE').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @isTest
    public static void InitializeTechnicalDataChangeTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'CaseCnt', 'InitializeTechnicalDataChange', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => '',
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        response = TestUtils.exec(
                'CaseCnt', 'InitializeTechnicalDataChange', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @isTest
    static void InitializeTechnicalDataChangeExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'CaseCnt', 'InitializeTechnicalDataChange', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    private static void updateCaseListTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                        AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'CaseCnt', 'updateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    /*@isTest
    private static void updateCaseListExceptionTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                        AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => caseList[0].Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'CaseCnt', 'updateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }*/
    
    @isTest
    private static void CancelProcessTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };
        
        Test.startTest();
        Object response = TestUtils.exec(
            'CaseCnt', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    static void getCaseDescribeTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'recordTypePicklistValues' => 'Electric'
        };
        Object response = TestUtils.exec(
                'CaseCnt', 'getCaseDescribe', inputJSON, true);
        Test.stopTest();
    }

    @isTest
    static void getCaseAddressFieldTest() {
        Case caseRecord = [
            SELECT Id,RecordTypeId
            FROM Case
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'caseId' => caseRecord.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'CaseCnt', 'getCaseAddressField', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        system.assert(result.get('error') == false );
        Test.stopTest();
    }

    @isTest
    static void getCaseAddressFieldExcepTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'caseId' => '3433334555'
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'CaseCnt', 'getCaseAddressField', inputJSON, true);
        Test.stopTest();
    }

    @isTest
    static void getCaseAddressFieldIsBlankTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'caseId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'CaseCnt', 'getCaseAddressField', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true );
        }catch (Exception e){
            system.assert(true, e.getMessage());
        }

        Test.stopTest();
    }

}