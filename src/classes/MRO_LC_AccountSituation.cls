/**
 * MRO_LC_AccountSituation
 *
 * @author  INSA BADJI
 * @version 1.0
 * @description Update Cases with
 *              [ENLCRO-117] Account Situation #WP3-P1
 * 12/11/2019 : INSA - Original
 */
public with sharing class MRO_LC_AccountSituation extends ApexServiceLibraryCnt {

    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();

    private static MRO_SRV_Dossier dossierServ = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_CompanyDivision companyDivisionQuery = MRO_QR_CompanyDivision.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
    //private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    //private static ScriptTemplateQueries scriptTempQuery = ScriptTemplateQueries.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    /**
      * @author  INSA BADJI
      * @description Initialize method to get data at the beginning for Account Situation
      * @date 12/11/2019
      * @param accountId
      * @param dossierId
      * @param interactionId
      *
      * @return  Map<String, Object> response:
      */
    public class Initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String genericRequestId = params.get('genericRequestId');
            String companyDivisionId = '';
            String templateId = params.get('templateId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Account accountRecord = accountQuery.findAccount(accountId);


                /*if (String.isNotBlank(dossierId)) {
                    Dossier__c dossierCheckParentId = dossierQuery.getById(dossierId);

                    if (String.isNotBlank(dossierCheckParentId.Parent__c)) {
                        dossierId = dossierCheckParentId.Id;
                    } else if (dossierCheckParentId.RecordType.DeveloperName == 'GenericRequest') {
                        genericRequestId = dossierId;
                        dossierId = '';
                    }
                }*/

            try {
                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'AccountSituation');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                if (String.isNotBlank(dossier.Id)) {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                    response.put('caseData', cases);
                }
                dossier.SendingChannel__c = 'Email';
                if (accountRecord.IsPersonAccount) {
                    dossier.Email__c = accountRecord.PersonEmail;
                }else {
                    dossier.Email__c = accountRecord.Email__c;
                }
                databaseSrv.updateSObject(dossier);
                Set<String> companyDivisionIds = new Set<String>();
                List<Supply__c> supplyList = supplyQuery.listByAccountId(accountId);
                if (supplyList.size() > 0) {
                    companyDivisionIds = SobjectUtils.setNotNullByField(supplyList, Supply__c.CompanyDivision__c);
                }
                response.put('companyDivisionIds', companyDivisionIds);

                String code = 'AccountSituationWizardTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }
                response.put('originSelected', dossier.Origin__c);
                response.put('channelSelected', dossier.Channel__c);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionCode', dossier.CompanyDivision__r.Code__c);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('accountId', accountId);
                response.put('accountSituation', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AccountSituation').getRecordTypeId());
                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public class updateOriginAndChannelOnDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            //String companyDivisionId = params.get('companyDivisionId');
            if(String.isNotBlank(dossierId) && String.isNotBlank(channelSelected) && String.isNotBlank(originSelected)) {
                dossierServ.updateOriginAndChannelOnDossier(dossierId, channelSelected, originSelected);
            }
            response.put('error', false);
            return response;
        }
    }
    public class updateDossierCompanyDivision extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String companyDivisionId = params.get('companyDivisionId');
            if(String.isNotBlank(dossierId) && String.isNotBlank(companyDivisionId)){
                dossierServ.updateCompanyDivisionByDossierId(dossierId,companyDivisionId);
            }
            response.put('error', false);
            return response;
        }
    }

    public class retrieveCompanyDivisionCode extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String companyDivisionId = params.get('companyDivisionId');
            if (String.isBlank(companyDivisionId)) {
                response.put('isValid', false);
                //response.put('message', System.Label.ContractAccountRequired);
                return response;
            }
            CompanyDivision__c companyDivision = companyDivisionQuery.getById(companyDivisionId);
            if (companyDivision != null) {
                response.put('companyDivisionCode', companyDivision.Code__c);
                response.put('isValid', true);
            }
            return response;
        }
    }
    /**
     * @author  INSA BADJI
     * @description Create a case with selected and confirmed FisaInquiry. The Phase__c will be 'MI010' and status of dossier 'New'
     * @date 19/11/2019
     * @param accountId
     * @param dossierId
     * @param selectedFisa
     * @param recordTypeId
     * @param companyDivisionId
     * @param contractAccountId
     * @param startDate
     * @param endDate
     *
     * @return  Map<String, Object> response: returning error message equals to false
     */
    public class insertCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Date startDate = (Date) JSON.deserialize(params.get('startDate'), Date.class);
            Date endDate = (Date) JSON.deserialize(params.get('endDate'), Date.class);
            String selectedOrigin = params.get('selectedOrigin');
            String SelectedChannel = params.get('selectedChannel');
            CreateCaseInput createCaseParams =(CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);

            try {

                Case caseRecord = caseSrv.insertCaseForAccountSituation(createCaseParams, startDate, endDate, selectedOrigin, SelectedChannel);
                if (caseRecord != null) {
                    dossierSrv.updateDossierStatus(caseRecord.Dossier__c);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            response.put('error', false);
            return response;
        }
    }
    /**
      * @author  INSA BADJI
      * @description To cancel the  process of dossier and setting the statut to canceled. We won't create a case
      * @date 19/11/2019
      * @param dossierId
      *
      * @return  Map<String, Object> response: returning error message equals to false
      */
    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            dossierSrv.setCanceledOnDossier(dossierId);
            response.put('error', false);
            return response;
        }
    }

    public class CreateCaseInput{
        @AuraEnabled
        public String selectedFisa{get; set;}
        @AuraEnabled
        public String recordTypeId{get; set;}
        @AuraEnabled
        public String accountId{get; set;}
        @AuraEnabled
        public String dossierId{get; set;}
        @AuraEnabled
        public String companyDivisionId{get; set;}
        @AuraEnabled
        public String contractAccountId { get; set; }
        @AuraEnabled
        public String subProcessSelected { get; set; }
        @AuraEnabled
        public String outCome { get; set; }
    }

}