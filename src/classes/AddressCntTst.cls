@isTest
public with sharing class AddressCntTst {

    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.BusinessType__c = 'NGO';
        insert businessAccount;
        Account personAccount = TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert personAccount;
    }

    @isTest
    static  void normalizeTest(){
        Map<String, string> objectTest1 = new Map<String, string>{
                'ResidentialCity'=>'ResidentialCity'
        };
        Map<String, string> address = new Map<String, string>{
                'address'=>JSON.serialize(objectTest1)
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'AddressCnt', 'normalize', address, true);
        Test.stopTest();
    }

    @isTest
    static void getExtraFieldsTest(){
        AddressCnt.AddressDTO AddressDTO = new AddressCnt.AddressDTO();
        AddressDTO.building = 'building';
        AddressDTO.apartment = 'building';
        AddressDTO.city = 'building';
        AddressDTO.country = 'building';
        AddressDTO.locality = 'building';
        AddressDTO.postalCode = 'building';
        AddressDTO.province = 'building';
        AddressDTO.streetName = 'building';
        AddressDTO.streetNumber = 'building';
        AddressDTO.streetNumberExtn = 'building';
        AddressDTO.streetType = 'building';
        AddressDTO.floor = 'building';
        AddressDTO.addressNormalized = true;
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'objectApiName' => 'OpportunityServiceItem__c',
                'extraFieldsFieldSet'=>'PointAddress'
        };
        Object response = TestUtils.exec(
                'AddressCnt', 'getExtraFields', inputJSON, true);
        Test.stopTest();
    }
    @isTest
    static void normalizeAddressTest(){
        Test.startTest();
        Map<String, Object> address = new Map<String, Object>();
        Account account = new Account();
        account.Key__c = '3333333';
        address.put('String',account);
        List<Map<String, Object>> addressValues = AddressService.normalize(address);
        Test.stopTest();
    }
    @isTest
    static void getExtraFieldsAddressTest(){
        Test.startTest();
        String objectApiName = 'OpportunityServiceItem__c';
        String fields = 'PointAddress';
        String address = AddressService.getExtraFields(objectApiName,fields);
        System.assertEquals(true,address!= null);
        Test.stopTest();
    }

    @isTest
    static void getAddressStructureTest(){
        Test.startTest();
        String addressStructureApiName = 'DefaultAddressLayout';
        AddressService.AddressDTO AddressDTO = new AddressService.AddressDTO();
        AddressDTO.building = 'building';
        AddressDTO.apartment = 'building';
        AddressDTO.city = 'building';
        AddressDTO.country = 'building';
        AddressDTO.locality = 'building';
        AddressDTO.postalCode = 'building';
        AddressDTO.province = 'building';
        AddressDTO.streetName = 'building';
        AddressDTO.streetNumber = 'building';
        AddressDTO.streetNumberExtn = 'building';
        AddressDTO.streetType = 'building';
        AddressDTO.floor = 'building';
        AddressDTO.addressNormalized = true;
        List<AddressField__mdt> address = AddressService.getAddressStructure(addressStructureApiName);
        Test.stopTest();
    }
    @isTest
    static void getAvailableAddressesTest(){
        Test.startTest();
        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'BusinessAccount1';
        accountTrader.BusinessType__c = 'Commercial areas';
        insert accountTrader;
        String addressStructureApiName = 'DefaultAddressLayout';
        Map<String,List<Map<String, Object>>> address = AddressService.getAvailableAddresses(addressStructureApiName,accountTrader.Id);
        Test.stopTest();
    }

}