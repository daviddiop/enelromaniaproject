/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 23.09.20.
 */

public with sharing class MRO_SRV_SapQueryTheftCallOut extends MRO_SRV_SapQueryHelper {
    private static final String requestTypeQueryTheft = 'QueryTheft';

    public static MRO_SRV_SapQueryTheftCallOut getInstance() {
        return new MRO_SRV_SapQueryTheftCallOut();
    }

    public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {

        Set<String> conditionFields = new Set<String>{
                'ServicePointCode', 'StartDate', 'EndDate'
        };

        return buildSimpleRequest(argsMap, conditionFields);
    }

    public MRO_LC_TheftQuery.TheftResponse getList(MRO_LC_TheftQuery.TheftRequestParam params) {
        Map<String, Object> conditions = new Map<String, Object>{
                'ServicePointCode' => params.servicePointCode,
                'StartDate' => params.startDate,
                'EndDate' => params.endDate
        };

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeQueryTheft, conditions, 'MRO_SRV_SapQueryTheftCallOut');

        if (calloutResponse == null || calloutResponse.responses.isEmpty() || calloutResponse.responses[0] == null) {
            return null;
        }
        System.debug(calloutResponse.responses[0].code);
        System.debug(calloutResponse.responses[0].description);

        if (calloutResponse.responses[0].header != null && calloutResponse.responses[0].header.fields != null && !calloutResponse.responses[0].header.fields.isEmpty() && calloutResponse.responses[0].header.fields[0].name == 'ErrorMessage') {
            throw new WrtsException(calloutResponse.responses[0].header.fields[0].value);
        }

        if (calloutResponse.responses[0].objects == null || calloutResponse.responses[0].objects.isEmpty()) {
            return null;
        }
        System.debug(calloutResponse.responses[0].objects);

        MRO_LC_TheftQuery.TheftResponse theftResponse = new MRO_LC_TheftQuery.TheftResponse();

        if(calloutResponse.responses[0].objects[0].objectType == 'QueryTheftResponse') {
            for (wrts_prcgvr.MRR_1_0.WObject childWo : calloutResponse.responses[0].objects[0].objects) {
                Map<String, String> childWoMap = MRO_UTL_MRRMapper.wobjectToMap(childWo);
                if (childWo.objectType == 'Document') {
                    MRO_LC_TheftQuery.TheftDocument documentDTO = new MRO_LC_TheftQuery.TheftDocument(childWoMap);
                    theftResponse.documents.add(documentDTO);
                } else if (childWo.objectType == 'Process') {
                    MRO_LC_TheftQuery.TheftProcess processDTO = new MRO_LC_TheftQuery.TheftProcess(childWoMap);
                    theftResponse.processes.add(processDTO);
                }
            }
        }

        System.debug(theftResponse);
        return theftResponse;
    }
}