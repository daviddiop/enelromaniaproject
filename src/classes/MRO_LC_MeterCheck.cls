/**
 * Created by Boubacar Sow on 31/10/2019.
 * @version 1.0
 * @description
 *              [ENLCRO-154] Meter Check - Implementation (Part 1: without integration of invoices capability)
 *              [ENLCRO-156] Meter Check - Implementation (Part 2: Integration of invoices capability)
 *

 */

public with sharing class MRO_LC_MeterCheck  extends ApexServiceLibraryCnt {
    
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static CaseQueries caseQuery = CaseQueries.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    //Add by INSA for task [ENLCRO-538]
    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();


    /**
     * @author  Boubacar Sow
     * @description  Initialize method to get data at the beginning for Meter Check
     */
    public with sharing class InitializeMeterCheck extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String templateId = params.get('templateId');
            String genericRequestId = params.get('genericRequestId');


            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            /*//if (String.isNotBlank(interactionId)) {
                if (String.isNotBlank(dossierId)) {
                    Dossier__c dossierCheckParentId = dossierQuery.getById(dossierId);
                    if (String.isNotBlank(dossierCheckParentId.Parent__c)) {
                        dossierId = dossierCheckParentId.Id;
                    } else if (dossierCheckParentId.RecordType.DeveloperName == 'GenericRequest') {
                        genericRequestId = dossierId;
                        dossierId = '';
                    }
                }
           // }*/


            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType,'MeterCheck');
            Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
            if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                dossierSrv.updateParent(dossier.Id, genericRequestId);
                dossier.Parent__c = genericRequestId;
            }

            if (String.isNotBlank(dossier.Id)) {
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                response.put('caseTile', cases);
            }

            String code = 'MeterCheckTemplate_1';
            ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
            if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                response.put('templateId', scriptTemplate.Id);
            }

            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('accountId', accountId);
            response.put('genericRequestId', dossier.Parent__c);
            response.put('meterCheckRTEle', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId());
            System.debug('###');
            response.put('meterCheckRTGas', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckGas').getRecordTypeId());
            response.put('error', false);
            return response;
        }
    }
    /*
    *    @Author: INSA BADJI
    *    Descriptio: task [ENLCRO-539]  Get data of selected supply in advanced Seardh
    *    Date 30/01/2020
    *    @params:supplyId
     */
    public with sharing class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            Supply__c supplyRecord = supplyQuery.getById(supplyId);
            response.put('supply', supplyRecord);
            return response;
        }
    }

    /**
     * @author  Boubacar Sow
     * @description  CreateCase method to save data  for Meter Check
     */
    public class CreateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            
            System.debug('####');
            List<Supply__c> supplies = (List<Supply__c>) JSON.deserialize(params.get('supply'), List<Supply__c>.class);
            String dossierId = params.get('dossierId');
            String accountId = params.get('accountId');
            String contractAccountId = params.get('contractAccountId');
            String contractId = params.get('contractId');
            String invoiceIds = params.get('invoiceIds');
            String origin = params.get('origin');
            String channel = params.get('channel');
            String notes = params.get('notes');
            String subType = params.get('subType');
            String companyDivisionId;
            String recordTypeId;
            Map<String, String> meterCheckRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('MeterCheck');
            try {
                if (supplies[0] != null && (String.isNotBlank(accountId)) && (String.isNotBlank(dossierId))) {
                    recordTypeId = supplies[0].RecordType.DeveloperName == 'Electric' ? meterCheckRecordTypes.get('MeterCheckEle') : meterCheckRecordTypes.get('MeterCheckGas');
                    companyDivisionId = supplies[0].CompanyDivision__c;
                    if (subType != 'SealUnseal') {
                        subType = null;
                    }
                    Case caseInvoice = caseSrv.insertCaseWithInvoice(supplies[0], accountId, dossierId, recordTypeId, invoiceIds,'RE010', notes, origin, channel,contractAccountId, contractId, subType);
                    if(caseInvoice != null){
                        dossierSrv.updateDossierChannel(dossierId, channel, origin);
                        dossierSrv.updateDossierCompanyDivision(dossierId, companyDivisionId);
                        response.put('error', false);
                    }
                }
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }
    
    /**
     * @author Boubacar Sow
     * @description CancelProcess method to concel dossier statut  for Meter Check
     */
    public with sharing class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            
            String dossierId = params.get('dossierId');
            
            dossierSrv.setCanceledOnDossier(dossierId);
            response.put('error', false);
            return response;
        }
    }

}