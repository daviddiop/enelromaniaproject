/**
 * Created by ferhati on 21/10/2019.
 */
@isTest
public with sharing class ReactivationWizardCntTst {
    @testSetup
    private static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new list<Account>();
        listAccount.add(TestDataFactory.account().businessAccount().build());
        listAccount.add(TestDataFactory.account().personAccount().build());
        insert listAccount;
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;
        //update user;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity()
                .setAccount(listAccount[0].Id).setCompany(companyDivision.Id)
                .build();
        insert opportunity;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        insert interaction;

        Contact contact = TestDataFactory.contact().createContact()
                .setAccount(listAccount[0].Id)
                .build();
        insert contact;


        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;


        /*OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem()
                .setOpportunity(opportunity.Id)
                .build();
        insert opportunityServiceItem;*/

        City__c city = new City__c(Name = 'JIMBOLIA', Province__c = 'TIMIS', Country__c = 'ROMANIA');
        insert city;
        Street__c street = MRO_UTL_TestDataFactory.street().createBuilder().build();
        street.City__c = city.Id;
        insert street;

        OpportunityServiceItem__c opportunityServiceItem = MRO_UTL_TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        opportunityServiceItem.PointStreetId__c = street.Id;
        insert opportunityServiceItem;

        BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile()
                .setAccount(listAccount[0].Id)
                .build();
        insert billingProfile;

        Product2 product2 = TestDataFactory.product2().build();
        insert product2;

        PricebookEntry pricebookEntry = TestDataFactory.pricebookEntry()
                .setProduct2Id(product2.Id)
                .setPricebook2Id(Test.getStandardPricebookId())
                .build();
        insert pricebookEntry;

        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint()
                .setAccountId(listAccount[0].Id)
                .setRecordType(Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId())
                .build();
        insert servicePoint;

        Supply__c supply = TestDataFactory.supply().createSupplyBuilder()
                .setAccount(listAccount[0].Id)
                .setServicePoint(servicePoint.Id)
                .setCompany(companyDivision.Id)
                .build();
        insert supply;

        OpportunityLineItem opportunityLineItem = TestDataFactory.opportunityLineItem().createOpportunityLineItem()
                .setOpportunity(opportunity.Id)
                .setProduct(product2.Id)
                .setPricebookEntry(pricebookEntry.Id)
                .build();
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;

        Contract contract =  TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
    }

    @isTest
    static void initializeTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Opportunity opp = [
                SELECT Id,StageName, AccountId
                FROM Opportunity
                LIMIT 1
        ];



        Test.startTest();
        Object response = TestUtils.exec('ReactivationWizardCnt', 'initialize',
                new Map<String, String>{
                        'accountId' => account.Id,
                        'opportunityId' => opp.Id,
                        'customerInteraction' => customerInteraction.Id
                },  true);

        Test.stopTest();

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    @isTest
    static void initializeEmptyOpportunity() {
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];

        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];

        /*User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivisionId;
        update user;*/
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;

            Test.startTest();
            Map<String, String > inputJSON = new Map<String, String>{
                    'accountId' => acc.Id,
                    'opportunityId' => null,
                    'customerInteractionId ' => customerInteraction.Id
            };
            Object response = TestUtils.exec(
                    'ReactivationWizardCnt', 'initialize', inputJSON, true);
            Test.stopTest();
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(false, result.get('error'));
    }


    @isTest
    static void initializeExceptionTest() {
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Opportunity opp = [
                SELECT Id,StageName, AccountId
                FROM Opportunity
                LIMIT 1
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => null,
                'opportunityId' => opp.Id,
                'customerInteraction' => customerInteraction.Id
        };
        Object response = TestUtils.exec('ReactivationWizardCnt', 'initialize', inputJSON, false);
        System.assertEquals(response, System.Label.Account + ' - ' + System.Label.MissingId);
    }


    @isTest
    static void updateOpportunityTest() {
        Opportunity opp = [
                SELECT Id,StageName
                FROM Opportunity
                LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('ReactivationWizardCnt', 'updateOpportunity', new Map<String, String>{
                'opportunityId' => opp.Id, 'stage' => opp.stageName
        }, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertNotEquals(null, result.get('opportunityId'));

        opp.StageName = 'Closed Won';
        Object responseChangeStage = TestUtils.exec('ReactivationWizardCnt', 'updateOpportunity', new Map<String, String>{
                'opportunityId' => opp.Id, 'stage' => opp.stageName
        }, true);
        TestUtils.exec('ReactivationWizardCnt', 'updateOpportunity', new Map<String, String>{
                'opportunityId' => '', 'stage' => opp.stageName
        }, false);
        Test.stopTest();
    }

    @isTest
    static void updateOpportunityExceptionTest() {
        Opportunity opp = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => null,
                'stage' => 'Closed Won'
        };

        Object response = TestUtils.exec('ReactivationWizardCnt', 'updateOpportunity', inputJSON, false);
        Test.stopTest();
        System.assertEquals(response, System.Label.Opportunity + ' - ' + System.Label.MissingId);
    }

    @isTest
    static void updateOsiListTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
                SELECT Id,ServicePointCode__c
                FROM OpportunityServiceItem__c
                LIMIT 10
        ];

        ReactivationWizardCnt.InputData inputData = new ReactivationWizardCnt.InputData();
        inputData.opportunityServiceItems = listOppServiceItem;

        Test.startTest();
        Object response = TestUtils.exec('ReactivationWizardCnt', 'updateOsiList', inputData, true);
        Test.stopTest();

        System.assertEquals(null, response);
    }

    /*@isTest
    static void checkOsiTest() {
        OpportunityServiceItem__c oppServiceItem = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => oppServiceItem.Id
        };

        Object response = TestUtils.exec('ReactivationWizardCnt', 'checkOsi', inputJSON, true);
        Test.stopTest();
        Map<String, Object> opportunityServiceItemData = (Map<String, Object>) response;
        System.assert(opportunityServiceItemData.get('opportunityServiceItem') != null, 'Why not? ' + opportunityServiceItemData);
    }*/

   @isTest
    static void checkOsiExceptionTest() {
        OpportunityServiceItem__c oppServiceItem = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => null
        };
        Object response = TestUtils.exec('ReactivationWizardCnt', 'checkOsi', inputJSON, false);
        Test.stopTest();
        System.assertEquals(response, System.Label.OpportunityServiceItem + ' - ' + System.Label.MissingId);
    }

    @isTest
    static void linkOliToOsiTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
                SELECT Id,ServicePointCode__c
                FROM OpportunityServiceItem__c
                LIMIT 10
        ];

        String productId = [
                SELECT Id
                FROM Product2
        ].Id;

        ReactivationWizardCnt.InputData inputData = new ReactivationWizardCnt.InputData();
        inputData.opportunityServiceItems = listOppServiceItem;
        inputData.product2Id = productId;

        Test.startTest();
        Object response = TestUtils.exec('ReactivationWizardCnt', 'linkOliToOsi', inputData, true);
        Test.stopTest();

        System.assertEquals(null, response);
    }
    @isTest
    static void updateCompanyDivisionInOpportunityTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('ReactivationWizardCnt', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }
    @isTest
    static void updateCompanyDivisionInOpportunityExTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => null,
                'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('ReactivationWizardCnt', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }

    @isTest
    static void updateContractAndContractSignedDateOnOpportunityTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        Contract contract = [
            SELECT Id
            FROM Contract
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id,
            'customerSignedDate' => '',
            'contractId' => contract.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('ReactivationWizardCnt', 'updateContractAndContractSignedDateOnOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }
}