public with sharing class IndividualCnt extends ApexServiceLibraryCnt {

    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static InteractionQueries interactionQuery = InteractionQueries.getInstance();

    private static IndividualService individualSrv = IndividualService.getInstance();

    public with sharing class initData extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return new Map<String, Object>{
                'isSaveUnIdentifiedInterlocutorAllowed' => SettingProvider.isSaveUnIdentifiedInterlocutorAllowed()
            };
        }
    }

    /**
    * @description Class for checking customer presence an perform some operations
    */
    public with sharing class saveIndividual extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            String interlocutorDto = params.get('interlocutorDTO');

            IndividualService.Interlocutor individualDTO = (IndividualService.Interlocutor)
                JSON.deserialize(interlocutorDto, IndividualService.Interlocutor.class);

            Boolean isInterlocutorUpgrade = false;
            if (String.isNotBlank(interactionId)) {
                Interaction__c interaction = interactionQuery.findInteractionById(interactionId);
                isInterlocutorUpgrade = String.isNotBlank(interaction.InterlocutorFirstName__c) || String.isNotBlank(interaction.InterlocutorLastName__c);
            }

            Boolean isPartSaveAllowed = SettingProvider.isSaveUnIdentifiedInterlocutorAllowed();
            if (String.isNotBlank(interactionId) && isPartSaveAllowed && String.isBlank(individualDTO.nationalId) && !isInterlocutorUpgrade) {
                if (String.isBlank(individualDTO.firstName) && String.isBlank(individualDTO.lastName)) {
                    throw new WrtsException(System.Label.FirstName + ' or ' + System.Label.LastName + ' - ' + System.Label.Required);
                }
            } else {
                if (String.isBlank(individualDTO.firstName)) {
                    throw new WrtsException(System.Label.FirstName + ' - ' + System.Label.Required);
                }
                if (String.isBlank(individualDTO.lastName)) {
                    throw new WrtsException(System.Label.LastName + ' - ' + System.Label.Required);
                }
                if (String.isBlank(individualDTO.nationalId)) {
                    throw new WrtsException(System.Label.NationalIdentityNumber + ' - ' + System.Label.Required);
                }
            }
            return individualSrv.saveIndividual(individualDTO, interactionId, isInterlocutorUpgrade);
        }
    }

    /**
    * @description Class for checking customer presence an perform some operations
    */
    public with sharing class updateIndividual extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            String interlocutorDto = params.get('interlocutorDTO');

            IndividualService.Interlocutor individualDTO = (IndividualService.Interlocutor)JSON.deserialize(interlocutorDto, IndividualService.Interlocutor.class);

            List<CustomerInteraction__c> customerInteractionList = new List<CustomerInteraction__c>();

            if (String.isBlank(individualDTO.nationalId)) {
                throw new WrtsException(System.Label.NationalIdentityNumber + ' - ' + System.Label.Required);
            }

            Savepoint sp = Database.setSavepoint();
            try {
                if (String.isNotBlank(interactionId)) {
                    customerInteractionList = customerInteractionQuery.listByInteractionId(interactionId);
                }

                Boolean hasCustomer = !customerInteractionList.isEmpty();

                if (individualSrv.checkIndividualDuplicate(individualDTO)) {
                    throw new WrtsException(System.Label.NationalIdentityNumber + ' - ' + System.Label.Duplicated);
                }

                individualDTO.id = individualSrv.updateIndividual(individualDTO, hasCustomer);
                
                response.put('individualId', individualDTO.id);
                response.put('hasCustomer', hasCustomer);

            } catch (Exception exc) {
                Database.rollback(sp);
                throw new WrtsException(exc.getMessage());
            }
            return response;
        }
    }

    public class updateInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String individualId = params.get('individualId');
            String interactionId = params.get('interactionId');
            if (String.isBlank(individualId)) {
                throw new WrtsException(System.Label.Individual + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            return new Map<String, Object>{
            'isRedirectToInteraction' => individualSrv.findOrCreateSameContact(individualId, interactionId)
            };
        }
    }

    public with sharing class searchInterlocutor extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Interaction__c interaction = (Interaction__c) JSON.deserialize(jsonInput, Interaction__c.class);
            return individualSrv.searchIndividual(interaction);
        }
    }

}