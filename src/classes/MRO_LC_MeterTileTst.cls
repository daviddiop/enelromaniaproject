/**
 * Created by Boubacar Sow on 07/09/2020.
 */

@IsTest
private class MRO_LC_MeterTileTst {
    @TestSetup
    static void setup() {
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = 'E0120200617144259786';
        insert servicePoint;

        SAPCallouts__c sapCallouts = new SAPCallouts__c();
        sapCallouts.MeterList__c = true;
    }

    @IsTest
    public static void validateQuadrantIndex(){

        ServicePoint__c servicePoint = [
            SELECT Id, Code__c
            FROM ServicePoint__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'QuadrantName' => 'QuadrantName',
            'QuadrantIndex' => '20124',
            'ServicePointCode' => servicePoint.Code__c,
            'MeterSerialNumber' => '1024778',
            'DialId' => 'DialId'
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_MeterTile', 'validateQuadrantIndex', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('indexValidated') == true);

        inputJSON = new Map<String, String>{
            'QuadrantName' => 'Energie Reactiva Inductva',
            'QuadrantIndex' => '20',
            'ServicePointCode' => servicePoint.Code__c,
            'MeterSerialNumber' => '1024778',
            'DialId' => 'DialId'
        };

        response = TestUtils.exec('MRO_LC_MeterTile', 'validateQuadrantIndex', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('indexValidated') == false);

        inputJSON = new Map<String, String>{
            'QuadrantName' => 'Energie Reactiva Capacitiva',
            'QuadrantIndex' => '2',
            'ServicePointCode' => servicePoint.Code__c,
            'MeterSerialNumber' => '1024778',
            'DialId' => 'DialId'
        };

        response = TestUtils.exec('MRO_LC_MeterTile', 'validateQuadrantIndex', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('indexValidated') == false);
        Test.stopTest();
    }

}