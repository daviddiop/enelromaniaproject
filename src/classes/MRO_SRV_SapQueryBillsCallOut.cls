/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 27.07.20.
 */

public with sharing class MRO_SRV_SapQueryBillsCallOut extends MRO_SRV_SapQueryHelper {

    private static final String requestTypeQueryBills = 'QueryBills';
    private static final String requestTypeQueryBillsDetails = 'QueryBillsDetails';
    private static final String requestTypeCheckBill = 'CheckBill';

    public static MRO_SRV_SapQueryBillsCallOut getInstance() {
        return new MRO_SRV_SapQueryBillsCallOut();
    }

    public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {
        Map<String, Object> conditions = (Map<String, Object>) argsMap.get('conditions');
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);
        String requestType = parameters.get('requestType');
        Set<String> conditionFields;

        if(requestType != requestTypeCheckBill){
            conditionFields = new Set<String>{
                    'BillingAccountNumber', 'StartDate', 'EndDate', 'CustomerCode', 'InvoiceId'
            };
            return buildSimpleRequest(argsMap, conditionFields);
        }

        if(requestType == requestTypeCheckBill) {
            conditionFields = new Set<String>{
                    'InvoiceNumber', 'InvoiceSerialNumber'
            };
            List<Map<String, Object>> bills = (List<Map<String, Object>>)conditions.get('bills');

            wrts_prcgvr.MRR_1_0.WObject wo;
            for(Map<String, Object> bill : bills) {
                wo = new wrts_prcgvr.MRR_1_0.WObject();
                wo.id = '';
                wo.name = requestType;
                wo.objectType = requestType;
                for (String fieldName : conditionFields) {
                    Object fieldValue = bill.get(fieldName);
                    if (fieldValue != null) {
                        wo.fields.add(MRO_UTL_MRRMapper.createField(fieldName, fieldValue));
                    }
                }
                request.objects.add(wo);
            }
        }

        return request;
    }

    public List<MRO_LC_InvoicesInquiry.Invoice> getList(MRO_LC_InvoicesInquiry.InvoiceRequestParam params) {
        Map<String, Object> conditions = new Map<String, Object>();
        try {
            conditions.put('StartDate', parseDate(params.startDate));
            conditions.put('EndDate', parseDate(params.endDate));
        } catch (Exception e) {
            System.debug('Invalid Date InvoiceRequestParam ' + params.startDate + ' ' + params.endDate);
        }

        if (params.billingAccountNumber != null) {
            conditions.put('BillingAccountNumber', params.billingAccountNumber);
        }

        if (params.customerCode != null) {
            conditions.put('CustomerCode', params.customerCode);
        }

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeQueryBills, conditions, 'MRO_SRV_SapQueryBillsCallOut');

        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
            System.debug(calloutResponse.responses[0].objects);
            System.debug(calloutResponse.responses[0].description);

            List<MRO_LC_InvoicesInquiry.Invoice> invoicesList = new List<MRO_LC_InvoicesInquiry.Invoice>();

            for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {
                Map<String, String> responseWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);

                System.debug(responseWoMap.get('ErrorMessage'));

                if (responseWo.objects != null && responseWo.objects.size() != 0) {
                    for (wrts_prcgvr.MRR_1_0.WObject invoiceWo : responseWo.objects) {
                        Map<String, String> invoiceWoMap = MRO_UTL_MRRMapper.wobjectToMap(invoiceWo);

                        MRO_LC_InvoicesInquiry.Invoice invoiceDTO = new MRO_LC_InvoicesInquiry.Invoice(invoiceWoMap);

                        if (invoiceWo.objects != null && invoiceWo.objects.size() != 0) {
                            for (wrts_prcgvr.MRR_1_0.WObject paymentWo : invoiceWo.objects) {
                                Map<String, String> paymentWoMap = MRO_UTL_MRRMapper.wobjectToMap(paymentWo);
                                MRO_LC_InvoicesInquiry.Payment paymentDto = new MRO_LC_InvoicesInquiry.Payment(paymentWoMap);
                                invoiceDTO.paymentList.add(paymentDto);
                            }
                        }

                        invoicesList.add(invoiceDTO);
                    }
                }
            }

            System.debug(invoicesList);
            return invoicesList;
        }
        return null;
    }

    public List<MRO_LC_InvoicesInquiry.InvoiceDetails> getDetails(String invoiceId) {
        Map<String, Object> conditions = new Map<String, Object>{
                'InvoiceId' => invoiceId
        };

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeQueryBillsDetails, conditions, 'MRO_SRV_SapQueryBillsCallOut');

        if (calloutResponse == null || calloutResponse.responses.isEmpty() || calloutResponse.responses[0] == null) {
            return null;
        }
        System.debug(calloutResponse.responses[0].code);
        System.debug(calloutResponse.responses[0].description);

        if (calloutResponse.responses[0].objects == null || calloutResponse.responses[0].objects.isEmpty()) {
            return null;
        }
        System.debug(calloutResponse.responses[0].objects);

        List<MRO_LC_InvoicesInquiry.InvoiceDetails> invoiceDetailsList = new List<MRO_LC_InvoicesInquiry.InvoiceDetails>();

        Integer counter = 0;
        for (wrts_prcgvr.MRR_1_0.WObject invoiceDetailsWo : calloutResponse.responses[0].objects) {
            Map<String, String> invoiceDetailsWoMap = MRO_UTL_MRRMapper.wobjectToMap(invoiceDetailsWo);

            MRO_LC_InvoicesInquiry.InvoiceDetails invoiceDetailsDTO = new MRO_LC_InvoicesInquiry.InvoiceDetails(invoiceDetailsWoMap);
            invoiceDetailsDTO.invoiceDetailId = String.valueOf(counter);
            counter++;

            if (invoiceDetailsWo.objects != null && invoiceDetailsWo.objects.size() != 0) {
                for (wrts_prcgvr.MRR_1_0.WObject childWo : invoiceDetailsWo.objects) {
                    Map<String, String> childWoMap = MRO_UTL_MRRMapper.wobjectToMap(childWo);
                    if (childWo.objectType == 'InvoiceRow') {
                        MRO_LC_InvoicesInquiry.BillDetails billDetailsDto = new MRO_LC_InvoicesInquiry.BillDetails(childWoMap);
                        invoiceDetailsDTO.billDetailList.add(billDetailsDto);
                    } else if (childWo.objectType == 'InvoicedReading') {
                        MRO_LC_InvoicesInquiry.MeterDetails meterDetailsDto = new MRO_LC_InvoicesInquiry.MeterDetails(childWoMap);
                        invoiceDetailsDTO.meterDetailList.add(meterDetailsDto);
                    }
                }
            }

            invoiceDetailsList.add(invoiceDetailsDTO);
        }

        System.debug(invoiceDetailsList);
        return invoiceDetailsList;
    }

    public MRO_LC_InvoicesInquiry.CheckBillResponse checkBill(MRO_LC_InvoicesInquiry.CheckBillParams checkBillParam) {
        List<MRO_LC_InvoicesInquiry.CheckBillParams> checkBillParams = new List<MRO_LC_InvoicesInquiry.CheckBillParams>{checkBillParam};
        List<MRO_LC_InvoicesInquiry.CheckBillResponse> checkBillResponses = checkBill(checkBillParams);
        return checkBillResponses.isEmpty() ? null : checkBillResponses[0];
    }

    public List<MRO_LC_InvoicesInquiry.CheckBillResponse> checkBill(List<MRO_LC_InvoicesInquiry.CheckBillParams> checkBillParams) {
        List<Map<String, Object>> bills = new List<Map<String, Object>>();
        for(MRO_LC_InvoicesInquiry.CheckBillParams param : checkBillParams){
            bills.add(new Map<String, Object>{
                    'InvoiceNumber' => param.invoiceNumber,
                    'InvoiceSerialNumber' => param.invoiceSerialNumber
            });
        }
        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeCheckBill, new Map<String, Object>{
                'bills' => bills
        }, 'MRO_SRV_SapQueryBillsCallOut');

        System.debug(calloutResponse);
        List<MRO_LC_InvoicesInquiry.CheckBillResponse> checkBillResponses = new List<MRO_LC_InvoicesInquiry.CheckBillResponse>();
        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
            for(wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects){
                Map<String, String> responseWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);
                MRO_LC_InvoicesInquiry.CheckBillResponse checkBillResponse = new MRO_LC_InvoicesInquiry.CheckBillResponse(responseWoMap);

                if(responseWo.objects != null) {
                    for (wrts_prcgvr.MRR_1_0.WObject detailWo : responseWo.objects) {
                        Map<String, String> detailWoMap = MRO_UTL_MRRMapper.wobjectToMap(detailWo);
                        checkBillResponse.details.add(new MRO_LC_InvoicesInquiry.CheckBillDetail(detailWoMap));
                    }
                }
                checkBillResponses.add(checkBillResponse);
            }
            System.debug(checkBillResponses);
            return checkBillResponses;
        }
        return null;
    }
}