/**
 * Created by BADJI on 13/07/2020.
 */

@IsTest
public with sharing class MRO_LC_CustomerCompensationWizardTst {

    @TestSetup
    private static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'MeterCheckEle', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'MeterCheckEle', recordTypeEle, '');
        insert phaseDI010ToRC010;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'MeterCheckTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        List<ContractAccount__c> listContractAccount = new List<ContractAccount__c>();
        for (Integer i = 0; i< 5; i++){
            ContractAccount__c contractAccount =  MRO_UTL_TestDataFactory.contractAccount().build();
            listContractAccount.add(contractAccount);
        }
        insert listContractAccount;
        /*BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CustomerCompensation').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    public static void initializeTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    static void upsertCaseTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Case cases =[
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        List<ContractAccount__c> listContractAccounts = [
            SELECT Id
            FROM ContractAccount__c
            LIMIT 3
        ];
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';
        Test.startTest();
        MRO_LC_CustomerCompensationWizard.RequestParams params =  new MRO_LC_CustomerCompensationWizard.RequestParams();
        params.caseId = cases.Id;
        params.dossierId = dossier.Id;
        params.originSelected = selectedOrigin;
        params.channelSelected = selectedChannel;
       // params.contractAccountFromId = listContractAccounts[0].Id;
        //params.contractAccountToId = listContractAccounts[1].Id;
        params.selectedInvoices = invoiceIds;
        Object response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'upsertCase', params, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();

    }

    @IsTest
    static void setChannelAndOriginTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Case cases =[
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        List<ContractAccount__c> listContractAccounts = [
            SELECT Id
            FROM ContractAccount__c
            LIMIT 3
        ];
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';
        Test.startTest();
        MRO_LC_CustomerCompensationWizard.RequestParams params =  new MRO_LC_CustomerCompensationWizard.RequestParams();
        params.caseId = cases.Id;
        params.dossierId = dossier.Id;
        params.originSelected = selectedOrigin;
        params.channelSelected = selectedChannel;
        //params.contractAccountFromId = listContractAccounts[0].Id;
        //params.contractAccountToId = listContractAccounts[1].Id;
        params.selectedInvoices = invoiceIds;
        Object response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'setChannelAndOrigin', params, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void cancelProcessTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Case cases =[
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        Test.startTest();
        MRO_LC_CustomerCompensationWizard.CancelProcessParams params =  new MRO_LC_CustomerCompensationWizard.CancelProcessParams();
        params.caseId = cases.Id;
        params.dossierId = dossier.Id;
        Object response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'cancelProcess', params, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void cancelProcessExceptionTest(){

        Test.startTest();
        MRO_LC_CustomerCompensationWizard.CancelProcessParams params =  new MRO_LC_CustomerCompensationWizard.CancelProcessParams();
        params.caseId = 'casesId';
        params.dossierId = 'dossierId';
        Object response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'cancelProcess', params, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    static void saveProcessTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Case cases =[
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        Test.startTest();
        MRO_LC_CustomerCompensationWizard.SaveProcessParams params =  new MRO_LC_CustomerCompensationWizard.SaveProcessParams();
        params.caseId = cases.Id;
        params.dossierId = dossier.Id;
        params.isDraft = false;
        Object response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'saveProcess', params, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void saveProcessExceptionTest(){

        Test.startTest();
        MRO_LC_CustomerCompensationWizard.SaveProcessParams params =  new MRO_LC_CustomerCompensationWizard.SaveProcessParams();
        params.caseId = 'casesId';
        params.dossierId = 'dossierId';
        params.isDraft = false;
        Object response = TestUtils.exec(
            'MRO_LC_CustomerCompensationWizard', 'saveProcess', params, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }
}