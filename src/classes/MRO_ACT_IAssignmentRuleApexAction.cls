/**
 * Created by tommasobolis on 17/12/2020.
 */

public interface MRO_ACT_IAssignmentRuleApexAction {

    /**
     * @return set of queue developer names used by implemented class
     */
    Set<String> getQueueDeveloperNames();

    /**
      * Execute ApexAction
      * @param args current context object
      * @return Object with some context information
      */
    Object execute(Object args);
}