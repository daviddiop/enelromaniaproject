public with sharing class MRO_LC_ContractMerge extends ApexServiceLibraryCnt {

    private static MRO_SRV_Dossier dossierService = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseService = MRO_SRV_Case.getInstance();
    private static MRO_SRV_DatabaseService databaseService = MRO_SRV_DatabaseService.getInstance();
    private static MRO_SRV_Contract contractService = MRO_SRV_Contract.getInstance();
    private static MRO_SRV_Opportunity oppService = MRO_SRV_Opportunity.getInstance();
    private static MRO_SRV_Index indexService = MRO_SRV_Index.getInstance();
    private static MRO_UTL_Constants constantsService = new MRO_UTL_Constants();

    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Index indexQuery = MRO_QR_Index.getInstance();
    private static MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();

    private static final String CONTRACT_MERGE = 'ContractMerge';
    private static final String BUSINESS = 'Business';

    private static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(CONTRACT_MERGE).getRecordTypeId();

    public with sharing class Initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Dossier__c dossier = dossierService.generateDossier(accountId, dossierId, interactionId, null, dossierRecordType, CONTRACT_MERGE);
            if(dossier.SubProcess__c == null) {
                dossier.SubProcess__c = 'Contract Account Split';
                databaseService.updateSObject(dossier);
            }
            if (String.isNotBlank(genericRequestId)){
                dossierService.updateParentGenericRequest(dossier.Id, genericRequestId);
            }

            List<Case> caseList = caseQuery.getCasesByDossierId(dossier.Id);
            if (caseList.size() > 0) {
                Case firstCase = caseList.get(0);
                response.put('companyDivisionId', firstCase.CompanyDivision__c);
                response.put('contractId', firstCase.Contract__c != null ? firstCase.Contract__c : firstCase.Supply__r.Contract__c);
                response.put('contractAccountId', firstCase.ContractAccount__c);
                response.put('effectiveDate', firstCase.EffectiveDate__c);
                response.put('tariffType', firstCase.Supply__r.Product__r.CommercialProduct__r.TariffType__c);
                response.put('inEnelArea', firstCase.Supply__r.InENELArea__c);
                response.put('market', firstCase.Supply__r.Market__c);
                if (firstCase.Supply__r.ServicePoint__c != null) {
                    if (firstCase.Supply__r.ServicePoint__r.ConsumerType__c != null && firstCase.Supply__r.ServicePoint__r.ConsumerType__c.contains(BUSINESS)) {
                        response.put('contractType', BUSINESS);
                    } else {
                        response.put('contractType', firstCase.Supply__r.ServicePoint__r.ConsumerType__c);
                    }
                }
                Set<String> supplyContractAccountIds = new Set<String>();
                Set<String> supplyContractIds = new Set<String>();
                for (Case theCase : caseList) {
                    supplyContractIds.add(theCase.Supply__r.Contract__c);
                    supplyContractAccountIds.add(theCase.Supply__r.ContractAccount__c);
                }
                response.put('supplyContractAccountIds', supplyContractAccountIds);
                response.put('supplyContractIds', supplyContractIds);
            }
            response.put('dossierId', dossier.Id);
            response.put('cases', caseList);
            response.put('effectiveDate', Date.today());
            response.put('dossier', dossier);
            response.put('commodity', dossier.Commodity__c);
            response.put('subProcess', dossier.SubProcess__c);
            response.put('account', accountQuery.findAccount(accountId));
            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');

            oppService.setChannelAndOrigin(null, dossierId, origin, channel);
            return true;
        }
    }

    private static Set<String> getAllRelatedServiceSiteIds(List<String> supplyIds,  List<Case> cases){
        List<Supply__c> supplies = supplyQuery.getSuppliesByIds(supplyIds);

        Set<String> supplyServiceSiteIds = SobjectUtils.setNotNullByField(supplies, Supply__c.ServiceSite__c);
        Set<String> caseServiceSiteIds = SobjectUtils.setNotNullByField(cases, Case.ServiceSite__c);

        Set<String> allServiceSiteIds = new Set<String>();
        allServiceSiteIds.addAll(caseServiceSiteIds);
        allServiceSiteIds.addAll(supplyServiceSiteIds);

        return allServiceSiteIds;
    }

    public with sharing class CheckCasesEqualToSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('CheckCasesEqualToSupplies');

            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String commodity = params.get('commodity');

            // Prepare input data
            List<Case> casesFromWizard = (List<Case>)JSON.deserialize(params.get('cases'), List<Case>.class);
            if (casesFromWizard.isEmpty()) {
                throw new WrtsException(Label.RequiredFields);
            }

            // Get supplies from current cases
            Set<Id> supplyIds = SobjectUtils.setIdByField(casesFromWizard, Case.Supply__c);
            Map<Id, Supply__c> supplyToId = supplyQuery.getByIds(supplyIds);

            // Get all related supplies
            Set<String> serviceSiteIdsFromSupplies = SobjectUtils.setNotNullByField(supplyToId.values(), Supply__c.ServiceSite__c);
            List<Supply__c> allSupplyList = supplyQuery.getActiveSuppliesByServiceSiteIds(serviceSiteIdsFromSupplies);
            if (allSupplyList.isEmpty()) {
                throw new WrtsException(Label.SuppliesNotFound);
            }

            // Filter out Service Supplies
            for (Integer i= allSupplyList.size() - 1; i >= 0; i--) {
                Supply__c supply = allSupplyList.get(i);
                if (supply.RecordType.DeveloperName != commodity) {
                    allSupplyList.remove(i);
                }
            }

            // Compare the number of supplies with the number of cases and if different, return an error
            if (allSupplyList.size() != casesFromWizard.size()){
                response.put('error', true);
                response.put('errorMsg', Label.YouHaveToMoveAllSupplies);
            } else {
                response.put('error', false);
            }

            return response;
        }
    }

    public with sharing class CreateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String commodity = params.get('commodity');
            String origin = params.get('origin');
            String channel = params.get('channel');

            List<String> newSupplyIds = (List<String>)JSON.deserialize(params.get('supplyIds'), List<String>.class);

            List<Case> existingCases = caseQuery.getCasesByDossierId(dossierId);
            Map<String, SObject> caseToSupplyMap = SobjectUtils.mapByFieldAndSobject(existingCases, Case.Supply__c);

            System.debug('caseToSupplyMap');
            System.debug(caseToSupplyMap);

            if (newSupplyIds.isEmpty() || String.isBlank(accountId) || String.isBlank(dossierId)) {
                throw new WrtsException(Label.RequiredFields);
            }

            Dossier__c dossier = dossierQuery.getById(dossierId);

            Set<String> allServiceSiteIds = getAllRelatedServiceSiteIds(newSupplyIds, existingCases);
            List<Supply__c> allSupplyList = supplyQuery.getActiveSuppliesByServiceSiteIds(allServiceSiteIds);
            if (allSupplyList.isEmpty()) {
                throw new WrtsException(Label.SuppliesNotFound);
            }

            Supply__c firstSupply = null;
            for (Supply__c supply : allSupplyList) {
                if (!newSupplyIds.contains(supply.Id) || supply.RecordType.DeveloperName != commodity) {
                    continue;
                }
                firstSupply = supply;
                break;
            }

            if (firstSupply == null) {
                throw new WrtsException(Label.SuppliesNotFound);
            }

            String rateType = null;
            if (firstSupply.Product__c != null && firstSupply.Product__r.CommercialProduct__c != null) {
                rateType = firstSupply.Product__r.CommercialProduct__r.RateType__c;
            }
            Boolean inEnelArea = firstSupply.InENELArea__c;
            String market = firstSupply.Market__c;
            String consumerType = null;
            String trader = null;
            if (String.isNotBlank(firstSupply.ServicePoint__c)) {
                consumerType = firstSupply.ServicePoint__r.ConsumerType__c;
                trader = firstSupply.ServicePoint__r.Trader__c;
            }
            Set<String> supplyContractAccountIds = new Set<String>();
            Set<String> supplyContractIds = new Set<String>();

            Map<String, String> caseRecordTypes = MRO_UTL_Constants.getCaseRecordTypes(CONTRACT_MERGE);
            String caseRecordTypeId = caseRecordTypes.get(CONTRACT_MERGE);
            List<Case> newCaseList = new List<Case>();

            for (Supply__c supply : allSupplyList) {
                if (supply.RecordType.DeveloperName != commodity) {
                    continue;
                }
                if (dossier.SubProcess__c == System.Label.ContractAccountSplit) {
                    supplyContractAccountIds.add(supply.ContractAccount__c);
                }
                supplyContractIds.add(supply.Contract__c);
                if (supplyContractIds.size() > 1) {
                    throw new WrtsException(Label.SelectedSuppliesMustHaveSameContract);
                }
                if (supply.Product__c == null) {
                    throw new WrtsException(Label.SupplyDoNotHaveProduct);
                }
                if (supply.Product__r.CommercialProduct__c == null) {
                    throw new WrtsException(Label.ProductWithoutCommercialProduct);
                }
                if (supply.Product__r.CommercialProduct__r.RateType__c == null) {
                    throw new WrtsException(Label.RateTypeIsEmpty);
                }
                if (rateType != '' && rateType != supply.Product__r.CommercialProduct__r.RateType__c) {
                    throw new WrtsException(Label.SuppliesHasDifferentRateTypes);
                }
                if (inEnelArea != supply.InENELArea__c) {
                    throw new WrtsException(Label.SuppliesHasDifferentEnelAreas);
                }
                if (String.isBlank(supply.Market__c)) {
                    throw new WrtsException(Label.SupplyMarketIsBlank);
                }
                if (market != supply.Market__c) {
                    throw new WrtsException(Label.SuppliesHasDifferentMarkets);
                }
                if (String.isBlank(supply.ServicePoint__c)) {
                    throw new WrtsException(Label.SupplyServicePointIsBlank);
                }
                if (String.isBlank(supply.ServicePoint__r.ConsumerType__c)) {
                    throw new WrtsException(Label.ConsumerTypeIsBlank);
                }
                if (consumerType != supply.ServicePoint__r.ConsumerType__c) {
                    throw new WrtsException(Label.ServicePointsHasDifferentConsumerTypes);
                }
                if (String.isBlank(supply.ServicePoint__r.Trader__c)) {
                    throw new WrtsException(Label.TraderIsBlank);
                }
                if (trader != supply.ServicePoint__r.Trader__c) {
                    throw new WrtsException(Label.ServicePointsHasDifferentTraders);
                }
                market = supply.Market__c;
                if (commodity == MRO_UTL_Constants.GAS && market == constantsService.MARKET_REGULATED) {
                    throw new WrtsException(Label.RegulatedMarketIsNotAvailableForGas);
                }
                rateType = supply.Product__r.CommercialProduct__r.RateType__c;
                inEnelArea = supply.InENELArea__c;
                consumerType = supply.ServicePoint__r.ConsumerType__c;
                trader = supply.ServicePoint__r.Trader__c;

                Case contractMoveCase;
                if (caseToSupplyMap.containsKey(supply.Id)){
                    contractMoveCase = (Case)caseToSupplyMap.get(supply.Id);
                } else {
                    contractMoveCase = caseService.createCaseForContractMerge(supply, supply.ServiceSite__c, accountId, dossierId, caseRecordTypeId, dossier.SubProcess__c);
                    if (contractMoveCase != null) {
                        contractMoveCase.Origin = origin;
                        contractMoveCase.Channel__c = channel;
                        contractMoveCase.SLAExpirationDate__c = Date.today().addDays(15);
                    }
                }
                newCaseList.add(contractMoveCase);
            }
            databaseService.upsertSObject(newCaseList);

            Set<String> plus10Channels = new Set<String>{'Partner', 'Call Center'};
            Date effectiveDate = null;
            if (plus10Channels.contains(channel)) {
                effectiveDate = Date.today().addDays(10);
            } else {
                if (commodity == MRO_UTL_Constants.ELECTRIC) {
                    effectiveDate = Date.today().addDays(3);
                } else {
                    effectiveDate = Date.today();
                }
            }

            System.debug('newCaseList');
            System.debug(newCaseList);
            Map<String, Object> response = new Map<String, Object>();
            response.put('cases', newCaseList);
            response.put('inEnelArea', inEnelArea);
            response.put('market', market);
            response.put('supplyContractAccountIds', supplyContractAccountIds);
            response.put('supplyContractIds', supplyContractIds);
            if (consumerType != null && consumerType.contains(BUSINESS)) {
                response.put('contractType', BUSINESS);
            } else {
                response.put('contractType', consumerType);
            }
            response.put('effectiveDate', effectiveDate);

            return response;
        }
    }

    public with sharing class setContractAndContractAccount extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);

            String contractId = params.get('contractId');
            String contractAccountId = params.get('contractAccountId');
            List<Case> caseList = (List<Case>)JSON.deserialize(params.get('caseList'), List<Case>.class);

            List<Case> updatedCaseList = caseService.setContractAndContractAccount(caseList, contractId, contractAccountId);

            return updatedCaseList;
        }
    }

    public with sharing class UpdateCases extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String contractId = params.get('contractId');
            String contractAccountId = params.get('contractAccountId');
            String subProcess = params.get('subProcess');

            Boolean isDraft = false;
            String isDraftString = params.get('isDraft');
            if (String.isNotBlank(isDraftString)) {
                isDraft = Boolean.valueOf(isDraftString);
            }

            if (!MRO_UTL_Utils.isId(contractId, Contract.getSObjectType())) {
                contractId = null;
            }

            List<Case> cases = (List<Case>)JSON.deserialize(params.get('caseList'), List<Case>.class);

            String companyDivisionId;
            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            try {
                if ((String.isBlank(contractAccountId)) && (String.isBlank(contractId)) && !isDraft) {
                    throw new WrtsException(System.Label.AtLeastOneOfTheFollowingRequestsMustBeSelected);
                }
                if (!cases.isEmpty()) {
                    companyDivisionId = cases[0].CompanyDivision__c;
                    if (isDraft) {
                        dossierService.updateDossierCompanyDivision(dossierId, companyDivisionId, constantsService.DOSSIER_STATUS_DRAFT);
                        caseService.updateCaseContractAccountAndContractId(cases, contractAccountId, contractId, constantsService.CASE_STATUS_DRAFT);
                    } else {
                        if (subProcess == 'Contract Merge') {
                            List<Case> caseList = caseQuery.getCasesByDossierId(dossierId);
                            Set<String> serviceSiteIds = SobjectUtils.setNotNullByField(caseList, Case.ServiceSite__c);

                            List<Supply__c> thisActiveSupplyList = supplyQuery.listSuppliesByServiceSiteIdsAndStatus(serviceSiteIds, constantsService.SUPPLY_STATUS_ACTIVE);
                            Map<String, Set<String>> contractToThisSupplySet = SobjectUtils.mapFieldAndSet(thisActiveSupplyList, Supply__c.Contract__c, Supply__c.Id);

                            List<Supply__c> allActiveSupplyList = supplyQuery.listSuppliesByContractIdsAndStatus(contractToThisSupplySet.keySet(),
                                constantsService.SUPPLY_STATUS_ACTIVE);
                            Map<String, Set<String>> contractToAllSupplySet = SobjectUtils.mapFieldAndSet(allActiveSupplyList, Supply__c.Contract__c, Supply__c.Id);

                            Dossier__c dossier = new Dossier__c(
                                Id = dossierId,
                                PrintBundleSelection__c = 'AnnexB. AnnexB'
                            );
                            for (String theContractId : contractToAllSupplySet.keySet()) {
                                Set<String> allSupplies = contractToAllSupplySet.get(theContractId);
                                Set<String> thisSupplies = contractToThisSupplySet.get(theContractId);
                                if (allSupplies.size() == thisSupplies.size() && allSupplies.containsAll(thisSupplies)) {
                                    dossier.PrintBundleSelection__c = 'ContractTerm, AnnexB';
                                    break;
                                }

                            }
                            databaseService.updateSObject(dossier);

                            Contract contract = MRO_QR_Contract.getInstance().getById((Id) contractId);
                            Integer additionalActCounter = contract.AdditionalActCounter__c != null ? contract.AdditionalActCounter__c.intValue() : 0;
                            contract.AdditionalActCounter__c = additionalActCounter+1;
                            databaseService.updateSObject(contract);
                        } else  {
                            /*SubProcess - Contract account split*/
                            caseService.setEffectiveDate(cases, Date.today());
                        }
                        dossierService.updateDossierCompanyDivision(dossierId, companyDivisionId);
                        caseService.updateCaseContractAccountAndContractId(cases, contractAccountId, contractId);

                        if (String.isNotBlank(dossierId)) {
                            caseService.applyAutomaticTransitionOnDossierAndCases(dossierId);
                        }
                    }
                    response.put('error', false);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class setEffectiveDate extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Date effectiveDate = (Date)JSON.deserialize(params.get('effectiveDate'), Date.class);
            List<Case> cases = (List<Case>)JSON.deserialize(params.get('caseList'), List<Case>.class);

            List<Case> updatedCaseList = caseService.setEffectiveDate(cases, effectiveDate);

            return updatedCaseList;
        }
    }

    public with sharing class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);

            caseService.setCanceledOnCaseAndDossier(caseList, dossierId, 'CN010', constantsService.CASE_STATUS_CANCELED);

            return true;
        }
    }

    public with sharing class updateCommodityToDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String commodity = params.get('commodity');

            Dossier__c doss = new Dossier__c();
            doss.Id = dossierId;
            doss.Commodity__c = commodity;
            databaseService.upsertSObject(doss);

            Map<String, Object> response = new Map<String, Object>();
            response.put('dossierId', doss.Id);
            response.put('commodity', doss.Commodity__c);
            return response;
        }
    }

    public with sharing class getSelfReadingInterval extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String contractAccountId = params.get('contractAccountId');

            Date todayDate = system.today();
            Integer todayMonth = todayDate.month();
            Integer todayYear = todayDate.year();
            Boolean isInsideInterval = false;

            ContractAccount__c contractAccount = contractAccountQuery.getById(contractAccountId);

            if (contractAccount != null) {
                if (String.isNotBlank(contractAccount.SelfReadingPeriodEnd__c)){
                    Integer selfReadingPeriodEnd = Integer.valueOf(contractAccount.SelfReadingPeriodEnd__c);
                    Date endDate = Date.newInstance(todayYear, todayMonth, selfReadingPeriodEnd);
                    Date beginDate = Date.newInstance(todayYear, todayMonth, selfReadingPeriodEnd).addDays(-10);

                    if (todayDate > beginDate && todayDate <= endDate){
                        isInsideInterval = true;
                    }
                }
            }

            Map<String, Object> response = new Map<String, Object>();
            response.put('isInsideInterval', isInsideInterval);
            return response;
        }
    }

    public with sharing class getMeters extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String eneltel = params.get('eneltel');
            String caseId = params.get('caseId');

            Map<String, Object> response = new Map<String, Object>();
            String metersJsonString = getMetersWithSaved(eneltel, caseId);
            response.put('metersJson', metersJsonString);
            response.put('hasNonActiveEnergy', hasNonActiveEnergy(metersJsonString));
            return response;
        }
    }

    public with sharing class getSavedMeters extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');

            Map<String, String> metersJsonMap = indexService.getCaseMeterData(dossierId, false);

            Map<String, Object> response = new Map<String, Object>();
            response.put('metersJson', metersJsonMap);
            response.put('error', false);
            return response;
        }
    }

    private static Boolean hasNonActiveEnergy(String metersJsonString) {
        MRO_SRV_Index.MeterList meterList = (MRO_SRV_Index.MeterList) JSON.deserialize(metersJsonString, MRO_SRV_Index.MeterList.class);
        Boolean hasNonActiveEnergy = false;
        if (meterList != null && meterList.Meters != null) {
            for (MRO_SRV_Index.MeterElement meterElement : meterList.Meters) {
                for (MRO_SRV_Index.QuadrantElement quadrantElement : meterElement.Quadrants) {
                    String idCadran = quadrantElement.ID_CADRAN;
                    if (idCadran.substring(0, 3) != '1.8' || idCadran == '1.8.0N') {
                        hasNonActiveEnergy = true;
                    }
                }
            }
        }
        return hasNonActiveEnergy;
    }

    public static String getMetersWithSaved(String eneltel, String caseId) {
        String metersJsonString = indexService.getMeterDataByEnelId(eneltel);
        if (caseId == null) {
            return metersJsonString;
        }
        String metersJsonStringForDeserialize = metersJsonString.remove('__c');
        MRO_SRV_Index.MeterList meterList = (MRO_SRV_Index.MeterList) JSON.deserialize(metersJsonStringForDeserialize, MRO_SRV_Index.MeterList.class);
        Map<Id, Map<String, Index__c>> metersIndexMap = indexService.findIndexesByCaseIds(new Set<Id>{
                caseId
        });
        Map<String, Index__c> meterIndexMapForCase = metersIndexMap.get(caseId);

        for (MRO_SRV_Index.MeterElement meterElement : meterList.Meters) {
            for(MRO_SRV_Index.QuadrantElement quadrantElement : meterElement.Quadrants){
                quadrantElement.SAP_INDEX = quadrantElement.INDEX;
                quadrantElement.INDEX = 0;
                if (meterIndexMapForCase != null && meterIndexMapForCase.get(meterElement.MeterNumber) != null) {
                    quadrantElement = setIndexValues(quadrantElement, meterIndexMapForCase.get(meterElement.MeterNumber));
                }
                quadrantElement.VALIDATED = quadrantElement.INDEX > 0;
            }
        }
        return JSON.serialize(meterList).replace('MeterNumber', 'MeterNumber__c');
    }

    private static MRO_SRV_Index.QuadrantElement setIndexValues(MRO_SRV_Index.QuadrantElement quadrantElement, Index__c index){
        switch on quadrantElement.ID_CADRAN {
            when '1.8.0' {
                quadrantElement.INDEX = index.ActiveEnergy__c == null ? 0 : index.ActiveEnergy__c;
            }
            when '1.8.1' {
                quadrantElement.INDEX = index.ActiveEnergyR1__c == null ? 0 : index.ActiveEnergyR1__c;
            }
            when '1.8.2' {
                quadrantElement.INDEX = index.ActiveEnergyR2__c == null ? 0 : index.ActiveEnergyR2__c;
            }
            when '1.8.3' {
                quadrantElement.INDEX = index.ActiveEnergyR3__c == null ? 0 : index.ActiveEnergyR3__c;
            }when else {
                quadrantElement.INDEX = 0;
            }
        }
        return quadrantElement;
    }

    public with sharing class createIndex extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);

            Id accountId = params.get('accountId');
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);

            Map<String, Object> response = new Map<String, Object>();
            if(caseList.isEmpty()){
                response.put('error', false);
                return response;
            }

            Set<Id> supplyIds = new Set<Id>();
            for(Case caseRecord : caseList){
                supplyIds.add(caseRecord.Supply__c);
            }
            Map<Id, Supply__c> supplyById = new Map<Id, Supply__c>(supplyQuery.getByIds(supplyIds));

            Map<Id, Case> casesById = new Map<Id, Case>(caseList);
            Map<String, List<List<MRO_SRV_Index.MeterQuadrant>>> metersByCaseId = (Map<String, List<List<MRO_SRV_Index.MeterQuadrant>>>) JSON.deserialize(params.get('meterInfo'), Map<String, List<List<MRO_SRV_Index.MeterQuadrant>>>.class);

            List<Index__c> indexes = indexQuery.getIndexesByCaseIds(casesById.keySet());
            Map<String, Index__c> indexByCaseIdAndMeterNumber = new Map<String, Index__c>();
            for(Index__c index : indexes){
                indexByCaseIdAndMeterNumber.put(index.Case__c + index.MeterNumber__c, index);
            }
            List<Index__c> indexesForUpsert = new List<Index__c>();

            for(Id caseId : casesById.keySet()){
                for(List<MRO_SRV_Index.MeterQuadrant> caseMeterQuadrants : metersByCaseId.get(caseId)) {
                    Case caseRecord = casesById.get(caseId);

                    Double energieActiva = 0;
                    Double energieActivaR1 = 0;
                    Double energieActivaR2 = 0;
                    Double energieActivaR3 = 0;
                    for (Integer i = 0; i < caseMeterQuadrants.size(); i++) {
                        String idCadran = caseMeterQuadrants.get(i).idCadran;

                        if (idCadran == '1.8.0') {
                            energieActiva = caseMeterQuadrants.get(i).index;
                        } else if (idCadran == '1.8.1') {
                            energieActivaR1 = caseMeterQuadrants.get(i).index;
                        } else if (idCadran == '1.8.2') {
                            energieActivaR2 = caseMeterQuadrants.get(i).index;
                        } else if (idCadran == '1.8.3') {
                            energieActivaR3 = caseMeterQuadrants.get(i).index;
                        }
                    }

                    if(!caseMeterQuadrants.isEmpty()) {
                        Index__c indexObject = indexByCaseIdAndMeterNumber.get(caseId + caseMeterQuadrants[0].meterNumber);
                        if (indexObject != null) {
                            indexObject.ActiveEnergy__c = energieActiva;
                            indexObject.ActiveEnergyR1__c = energieActivaR1;
                            indexObject.ActiveEnergyR2__c = energieActivaR2;
                            indexObject.ActiveEnergyR3__c = energieActivaR3;
                        } else {
                            String servicePointId = supplyById.get(caseRecord.Supply__c).ServicePoint__c;
                            indexObject = indexService.createIndexForSelfReading(caseId, accountId, caseMeterQuadrants[0].meterNumber, servicePointId, Date.today(),
                                    energieActiva, energieActivaR1, energieActivaR2, energieActivaR3, 0, 0);
                        }
                        indexesForUpsert.add(indexObject);
                    }
                }
            }
            if(indexesForUpsert.size() > 0){
                databaseService.upsertSObject(indexesForUpsert);
            }

            return true;
        }
    }

    public with sharing class updateCurrentFlow extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String currentFlow = params.get('currentFlow');
            Id dossierId = params.get('dossierId');

            databaseService.updateSObject(new Dossier__c(
                Id = dossierId,
                SubProcess__c = currentFlow
            ));

            return true;
        }
    }
}