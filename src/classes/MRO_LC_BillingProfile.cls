public with sharing class MRO_LC_BillingProfile extends ApexServiceLibraryCnt {
    static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();
    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();
    static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    static List<ApexServiceLibraryCnt.Option> recordTypePicklist  = new List<ApexServiceLibraryCnt.Option>();

    public class getAddressFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();

            Map<String, String> params = asMap(jsonInput);
            String billingProfileRecordId = params.get('billingProfileRecordId');
            if (String.isBlank(billingProfileRecordId)) {
                throw new WrtsException(System.Label.BillingProfile + ' - ' + System.Label.MissingId);
            }
            BillingProfile__c billingProfile = billingProfileQuery.getById(billingProfileRecordId);
            if (billingProfile != null) {
                billingProfile.BillingCountry__c='ROMANIA';
                MRO_SRV_Address.AddressDTO addressFromBilling = MRO_LC_BillingProfile.copyBillingAddressToDTO(billingProfile);
                response.put('billingAddress', addressFromBilling);
            }
            return response;
        }
    }
    public class isBusinessClient extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.BillingProfile + ' - ' + System.Label.MissingId);
            }
            Account account = accQuery.findAccount(accountId);
            if (account != null) {
                if(account.RecordType.DeveloperName == 'Business' || account.RecordType.DeveloperName == 'BusinessProspect'){
                    response.put('isBusinessClient', true);
                }
                else{
                    response.put('isBusinessClient', false);
                }
            }
            return response;
        }
    }
    public with sharing class getActiveSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String billingProfileRecordId = params.get('billingProfileRecordId');
            if (String.isBlank(billingProfileRecordId)) {
                throw new WrtsException(System.Label.BillingProfile + ' - ' + System.Label.MissingId);
            }

            List<Supply__c> supplyList = supplyQuery.getActiveSuppliesByBillingProfile(billingProfileRecordId);
            List<Case> caseList = caseQuery.getCasesByBillingProfile(billingProfileRecordId);
            response.put('enableEditButtonBP', supplyList.isEmpty() && caseList.isEmpty());
            return response;
        }
    }

    public with sharing class getBillingProfileRecords extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            List<BillingProfile__c> listBillingProfile = billingProfileQuery.getByAccountId(accountId);
            response.put('listBillingProfile', listBillingProfile);
            return response;
        }
    }

    public with sharing class getMapBankNameWithId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, Id> mapBankId = billingProfileQuery.getMapBankId();
            response.put('mapBankId', mapBankId);
            return response;
        }
    }

    public static MRO_SRV_Address.AddressDTO copyBillingAddressToDTO(BillingProfile__c billingProfile) {
        MRO_SRV_Address.AddressDTO addressDTO = new MRO_SRV_Address.AddressDTO(billingProfile, 'Billing');
        Street__c street = MRO_QR_Address.findExactStreet1(addressDTO);
        if (street != null) {
            addressDTO.streetId = street.Id;
        }
        return addressDTO;
    }

    public with sharing class getListDeliveryChannelBusiness extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            List<ApexServiceLibraryCnt.Option> deliveryChannelBusinesses = billingProfileQuery.getListDeliveryChannelBusiness();
            System.debug('FF deliveryChannelBusinesses: ' + deliveryChannelBusinesses);
            response.put('deliveryChannelBusinesses', deliveryChannelBusinesses);
            return response;
        }
    }
}