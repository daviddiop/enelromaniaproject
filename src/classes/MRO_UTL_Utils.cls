/**
 * Created by Roberto Tomasoni on 16/01/2019.
 *
 * Contains various utility methods used in all project
 *
 * @author Roberto Tomasoni
 * @version 1.0
 * @code 001
 */

public with sharing class MRO_UTL_Utils extends ApexServiceLibraryCnt {
    public static boolean firstTimeEntry = false;
    private static MRO_QR_User userQuery = MRO_QR_User.getInstance();
    /**
     * Utility class for SelectOptions in lightning
     */
    public with sharing class KeyVal {
        @AuraEnabled
        public String key { get; set; }
        @AuraEnabled
        public Object value { get; set; }
        @AuraEnabled
        public Object label { get; set; }

        public KeyVal(String key, Object value) {
            this.key = key;
            this.value = value;
        }
        public KeyVal(String key, Object value, Object label) {
            this.key = key;
            this.value = value;
            this.label = label;
        }
    }


    /**
     * Format an exception stackTrace
     *
     * @param e exception
     * @return formatted stacktrace
     * @code 13
     */
    public static String getStackTraceFromException(Exception e) {
        return e.getStackTraceString().replace('\n', ' ') + ' ' + e.getMessage().replace('\n', ' ');
    }

    /**
     * Method class that allows to get the system's constants on Lightning Component
     */
    public with sharing class getAllConstants extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return MRO_UTL_Constants.getAllConstants();
        }
    }

    @AuraEnabled
    public static List<Option> getPickListValues (String objectApiName, String fieldApiName) {
        List<Option> pickListValuesList= new List<Option>();
        Schema.SObjectType sObjct = Schema.getGlobalDescribe().get(objectApiName);
        Schema.DescribeSObjectResult result = sObjct.getDescribe() ;
        Map<String,Schema.SObjectField> fields = result.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.add(new Option(pickListVal.getLabel() ,pickListVal.getValue()));
        }
        return pickListValuesList;
    }

   /**
     * @author Luca Ravicini
     * @description get field label of SObject
     * @param  fieldName field api name
     * @param objectName sObject api name
     * @since May 12,2020
     *
     */
    public static  String getFieldLabel(String fieldName, String objectName){
        SObjectType type = Schema.getGlobalDescribe().get(objectName);
        Map<String,Schema.SObjectField> mfields = type.getDescribe().fields.getMap();
        SObjectField field = mfields.get(fieldName);
        return  field.getDescribe().getLabel();
    }

    /**
     * @author Luca Ravicini
     * @description get  pick list entries for a given field
     * @param  objectApiName sObject api name
     * @param fieldApiName field api name
     * @since Feb 28,2020
     *
     */
    public static List<Schema.PicklistEntry> getPickList (String objectApiName, String fieldApiName) {

        Schema.SObjectType sObjct = Schema.getGlobalDescribe().get(objectApiName);
        Schema.DescribeSObjectResult result = sObjct.getDescribe() ;
        Map<String,Schema.SObjectField> fields = result.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        if (!Schema.getGlobalDescribe().containsKey(objectApiName)){
            System.debug('OBJNAME NOT FOUND :' + objectApiName);
            return null;
        }
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectApiName);
        if (objType==null){
            return new List<Schema.PicklistEntry>();
        }
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(fieldApiName) ){
            return new List<Schema.PicklistEntry>();
        }

        return ple;
    }
    /**
     * @author Luca Ravicini
     * @description get map of pick list values of controlling picklist and its dependent one
     * @param objectName sObject api name
     * @param contrfieldName controlling field api name
     * @param depfieldName dependent field api name
     * @since Feb 28,2020
     *
     */
    public static Map<String,Map<String,String>> getDependentPickList(String objName, String contrfieldName, String depfieldName){

        Map<String,Map<String,String>> objResults = new Map<String,Map<String,String>>();
        //get the string to sobject global map
        MRO_UTL_BitSet bitSetObj = new MRO_UTL_BitSet();
        List<Schema.PicklistEntry> contrEntries = getPickList(objName, contrfieldName);
        List<Schema.PicklistEntry> depEntries = getPickList(objName, depfieldName);
        if (contrEntries == null || depEntries == null){
            return null;
        }

        if (contrEntries.isEmpty() || depEntries.isEmpty()){

            return objResults;
        }

        List<Integer> controllingIndexes = new List<Integer>();
        for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String value = ctrlentry.getValue();
            objResults.put(value,new Map<String,String>());
            controllingIndexes.add(contrIndex);
        }
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper>();
        for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){
            Schema.PicklistEntry depentry = depEntries[dependentIndex];
            objEntries.add(depentry);
        }
        objJsonEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
        List<Integer> indexes;
        for (PicklistEntryWrapper objJson : objJsonEntries){
            if (objJson.validFor==null || objJson.validFor==''){
                continue;
            }
            indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
            for (Integer idx : indexes){
                String contrValue = contrEntries[idx].getValue();
                objResults.get(contrValue).put(objJson.value,objJson.label);
            }
        }
        objEntries = null;
        objJsonEntries = null;
        return objResults;
    }

    public class PicklistEntryWrapper{

        public PicklistEntryWrapper(){
        }
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
    }

    public static Boolean isKAM (String userId) {
        Boolean isKAM = false;
        Group kamGroupRecord = userQuery.getQueueByDeveloperNameAndType('Regular', 'KAMUserGroup');
        if (kamGroupRecord != null && String.isNotBlank(userId)) {
            GroupMember groupMemberRecord = userQuery.getGroupMemberByGroupIdAndUserId(kamGroupRecord.Id, userId);
            if (groupMemberRecord != null) {
              isKAM = true;
            }
        }
        return isKAM;
    }

    public static Boolean isId(String idToCheck, SObjectType sfObjectType) {
        try {
            Id sObjectId = Id.valueOf(idToCheck);
            return sObjectId.getSobjectType() == sfObjectType;
        } catch (Exception exp) {
            System.debug(LoggingLevel.ERROR, exp.getMessage());
            System.debug(LoggingLevel.ERROR, exp.getStackTraceString());
            return false;
        }
    }

    public static String getDMLErrorMessage(DmlException ex) {
        List<String> dmlMessages = new List<String>();
        for (Integer i = 0; i < ex.getNumDml(); i++) {
            dmlMessages.add(ex.getDmlMessage(i));
        }
        return String.join(dmlMessages, '; ');
    }
}