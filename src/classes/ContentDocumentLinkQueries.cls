public with sharing class ContentDocumentLinkQueries {

    public static ContentDocumentLinkQueries getInstance() {
        return (ContentDocumentLinkQueries)ServiceLocator.getInstance(ContentDocumentLinkQueries.class);
    }

    public List<ContentDocumentLink> listByContentDocumentAndEntityId(String contentDocumentId, String entityId) {
        return [
            SELECT Id, ContentDocumentId, LinkedEntityId, ShareType, Visibility
            FROM ContentDocumentLink
            WHERE ContentDocumentId = :contentDocumentId
                AND LinkedEntityId = :entityId
        ];
    }
}