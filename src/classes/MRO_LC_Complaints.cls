public with sharing class MRO_LC_Complaints extends ApexServiceLibraryCnt {

    private static final String COMPLAINT_NAME = 'Complaint';

    static MRO_UTL_Constants constantSrv = MRO_UTL_Constants.getAllConstants();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Index indexQuery = MRO_QR_Index.getInstance();
    private static MRO_SRV_Index indexServ = MRO_SRV_Index.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_ServiceSite serviceSiteQuery = MRO_QR_ServiceSite.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();

    static String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(COMPLAINT_NAME).getRecordTypeId();
    static String caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(COMPLAINT_NAME).getRecordTypeId();
    private static String caseNewStatus = constantSrv.CASE_STATUS_NEW;
    private static String dossierNewStatus = constantSrv.DOSSIER_STATUS_NEW;

    public inherited sharing class init extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String genericRequestId = params.get('genericRequestId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Map<String, Object> response = new Map<String, Object>();

            Account acc = accountQuery.findAccount(accountId);
            response.put('isPersonAccount', acc.IsPersonAccount);

            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, null, dossierRecordTypeId,COMPLAINT_NAME);
            if (String.isBlank(dossierId)) {
                dossier = dossierQuery.getById(dossier.Id);
            }
            if(!String.isBlank(genericRequestId)){
                dossierSrv.updateParent(dossier.Id, genericRequestId);

                Dossier__c parentDossier = dossierQuery.getById(genericRequestId);
                dossier.CustomerInteraction__c = parentDossier.CustomerInteraction__c;
                databaseSrv.updateSObject(dossier);
            }
            List<Case> caseList = caseQuery.getCasesByDossierId(dossier.id);
            if(!caseList.isEmpty()) {
                Case complaintCase = caseList.get(0);
                if(complaintCase.Supply__c != null){
                    Supply__c selectedSupply = supplyQuery.getById(complaintCase.Supply__c);
                    List<Account> distributors = new List<Account>();
                    response.put('supplyType', selectedSupply.RecordType.DeveloperName);
                    if(selectedSupply.ServicePoint__r.Distributor__c != null){
                        Set<Id> distIds = new Set<Id>();
                        distIds.add(selectedSupply.ServicePoint__r.Distributor__c);
                        distributors = accountQuery.listDistributorsByIds(distIds);
                    }
                    if(distributors.size()>0){
                        response.put('IsDisCoENEL__c',distributors[0].IsDisCoENEL__c);
                        response.put('DistributorProvince', distributors[0].ResidentialProvince__c);
                    }

                    if(selectedSupply.ServiceSite__c != null) {
                        ServiceSite__c serviceSite = serviceSiteQuery.getById(selectedSupply.ServiceSite__c);
                        if(serviceSite != null){
                            response.put('ServiceSiteProvince', serviceSite.SiteProvince__c);
                        }
                    }
                }
            }
            Date endDate = Date.today();
            Date startDate = endDate.addMonths(-12);
            response.put('startDate', startDate);
            response.put('endDate', endDate);
            response.put('caseList', caseList);
            response.put('dossier', dossier);
            response.put('complaintRecordTypeId', caseRecordTypeId);
            System.debug('### response '+JSON.serializePretty(response));
            return response;
        }
    }

    public inherited sharing class GetAvailableSubTypes extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String complaintType = params.get('complaintType');
            String supplyType = params.get('supplyType');

            try {
                List<CaseSubTypeDependency__mdt> availableSubTypes = [SELECT MasterLabel, HasInvoiceSelection__c, HasBillingAdjustment__c, DisableInvoiceMarking__c, Label__c, HasParentCaseField__c
                FROM CaseSubTypeDependency__mdt WHERE CaseType__c = :complaintType AND SupplyType__c = :supplyType AND CaseRecordType__c = :COMPLAINT_NAME ORDER BY MasterLabel];
                response.put('availableSubTypes', availableSubTypes);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class GetAvailableReasons extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String caseSubType = params.get('caseSubType');
            try {
                List<CaseReasonDependency__mdt> availableSubTypes = [SELECT MasterLabel, Label__c
                FROM CaseReasonDependency__mdt WHERE CaseSubType__c = :caseSubType ORDER BY MasterLabel];
                response.put('availableReasons', availableSubTypes);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class saveComplaint extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            String caseString = params.get('caseObject');
            String markedInvoiceIdsString = params.get('markedInvoiceIds');
            markedInvoiceIdsString = markedInvoiceIdsString.remove(' ');
            String selectedInvoiceIdsString = params.get('selectedInvoiceIds');
            selectedInvoiceIdsString = selectedInvoiceIdsString.remove(' ');
            String dossierId = params.get('dossierId');
            String accountId = params.get('accountId');
            String correctionType = params.get('correctionType');
            String selectedChannel = params.get('channelSelected');
            String selectedOrigin = params.get('originSelected');
            //Date SLAExpirationDate = Date.valueOf(params.get('SLAExpirationDate'));

            if (String.isBlank(caseString)) {
                throw new WrtsException(System.Label.Case + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(dossierId)) {
                throw new WrtsException(System.Label.Dossier + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Case complaint = (Case)JSON.deserialize(caseString, Case.class);

            if(!caseList.isEmpty()){
                complaint.Id = caseList.get(0).Id;
                Supply__c supply = supplyQuery.getById(caseList.get(0).Supply__c);
                if(supply != null) {
                    complaint.Distributor__c = supply.ServicePoint__r?.Distributor__c;
                    complaint.Trader__c = supply.ServicePoint__r?.Trader__c;
                }
                complaint.Description = 'POD';
            } else {
                complaint.Dossier__c = dossierId;
                complaint.AccountId = accountId;
                complaint.RecordTypeId = caseRecordTypeId;
                complaint.Description = 'ADRESA';
            }

            Date startDate = Date.today();
            complaint.StartDate__c = startDate;
            complaint.EndDate__c = Date.newInstance(2999, 12, 31);
            complaint.EffectiveDate__c = startDate;
            if (!String.isBlank(markedInvoiceIdsString)) {
                List<String> markedInvoiceIdsList = (List<String>)JSON.deserialize(markedInvoiceIdsString, List<String>.class);
                complaint.MarkedInvoices__c = String.join(markedInvoiceIdsList, ',').removeEnd(',');
            }
            if (!String.isBlank(selectedInvoiceIdsString)) {
                List<String> selectedInvoiceIdsList = (List<String>)JSON.deserialize(selectedInvoiceIdsString, List<String>.class);
                complaint.SelectedInvoices__c = String.join(selectedInvoiceIdsList, ',').removeEnd(',');
            }

            /*if (SLAExpirationDate != null) {
                complaint.SLAExpirationDate__c = SLAExpirationDate;
            }*/
            String typeCode;
            complaint.Status = caseNewStatus;
            if(complaint.SubType__c != null) {
                typeCode = complaint.SubType__c.split(' - ')[0];
                if ((typeCode == 'A042' || typeCode == 'E05-ES') && !String.isBlank(correctionType)) {
                    complaint.SubProcess__c = correctionType;
                    if (correctionType != 'Manual') {
                        Map<String, String> SAPResponse = sendRequestToSAP();
                        String outcome = SAPResponse.get('RECL_FIN');
                        if (!String.isBlank(outcome)) {
                            complaint.Outcome__c = outcome;
                        }
                    }
                } else {
                    SLATiparConfiguration__mdt slaTipar = MRO_QR_SLATiparConfiguration.getInstance().getByRecordTypeAndTypeCode(COMPLAINT_NAME, typeCode);
                    if (slaTipar != null) {
                        if (slaTipar.DeveloperName.startsWith('Complaint_Disco')) {
                            complaint.SubProcess__c = 'DISCO';
                        } else if (slaTipar.DeveloperName.startsWith('Complaint_SAP')) {
                            complaint.SubProcess__c = 'Manual';
                        }
                    }else{
                        complaint.CaseTypeCode__c = typeCode;
                    }
                }
            }
            System.debug(complaint);
            if(complaint.Id == null && complaint.AddressProvince__c != null){
                List<Account> distributors = [SELECT Id, AccountNumber, DistributionProvincesDetails__c FROM Account WHERE RecordType.DeveloperName = 'Distributor' AND IsDisCoENEL__c = TRUE];
                String traderNumber;
                System.debug(distributors);
                for(Account distributor : distributors){
                    if(distributor.DistributionProvincesDetails__c == null){
                        continue;
                    }
                    Map<String, String> provinciesMap = (Map<String, String>)JSON.deserialize(distributor.DistributionProvincesDetails__c.toLowerCase(), Map<String, String>.class);
                    if(provinciesMap == null){
                        continue;
                    }
                    if(provinciesMap.values().contains(complaint.AddressProvince__c.toLowerCase())){
                        complaint.Distributor__c = distributor.Id;
                        traderNumber = distributor.AccountNumber == 'C101762631' || distributor.AccountNumber == 'C101731188' ? 'C101685800' : 'C101823634';
                        break;
                    }
                }
                System.debug(complaint.Distributor__c);
                System.debug(traderNumber);
                if(traderNumber != null){
                    List<Account> traders = [SELECT Id FROM Account WHERE RecordType.DeveloperName = 'Trader' AND AccountNumber = :traderNumber LIMIT 1];
                    if(traders.size() > 0){
                        complaint.Trader__c = traders[0].Id;
                    }
                }
            }

            String parentCaseId = complaint.ParentId;
            if(String.isNotEmpty(parentCaseId)){
                Case parentCase = caseQuery.getById(parentCaseId);
                if(complaint.CustomerNotes__c.indexOf(parentCase.CaseNumber) == -1){
                    complaint.CustomerNotes__c = complaint.CustomerNotes__c + ' / ' + parentCase.CaseNumber + ' / ' + parentCase.ExternalRequestID__c;
                }
            }

            try{
                if(complaint.Id == null && complaint.Trader__c != null){
                    List<CompanyDivision__c> companyDivisions = [SELECT Id FROM CompanyDivision__c WHERE IsActive__c = TRUE AND Trader__c =:complaint.Trader__c LIMIT 1];
                    if (!companyDivisions.isEmpty()) {
                        complaint.CompanyDivision__c = companyDivisions[0].Id;
                    }
                }
                databaseSrv.upsertSObject(complaint);
                if(correctionType == 'Manual'){
                    applyTransitionForManualBA(complaint.Id);
                }
                Dossier__c updatedDossier = new Dossier__c(
                        Id = dossierId,
                        Status__c = dossierNewStatus,
                        Origin__c = selectedOrigin,
                        Channel__c = selectedChannel,
                        SubProcess__c = complaint.SubProcess__c
                );
                if(complaint.CompanyDivision__c != null){
                    updatedDossier.CompanyDivision__c = complaint.CompanyDivision__c;
                }
                if(typeCode != null){
                    if (typeCode != 'R_A011') {
                        updatedDossier.PrintBundleSelection__c = 'CLAIM';
                    } else if (complaint.IncidentDate__c != null) {
                        Date twoWeeksAgo = Date.today().addDays(-14);
                        System.debug(twoWeeksAgo > complaint.IncidentDate__c);
                        updatedDossier.PrintBundleSelection__c = twoWeeksAgo > complaint.IncidentDate__c ? 'DESP14' : 'DESP';
                    }
                }
                databaseSrv.updateSObject(updatedDossier);

                MRO_SRV_Case.getInstance().applyAutomaticTransitionOnDossierAndCases(dossierId);
                response.put('complaint',JSON.serialize(complaint));
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
        private Map<String, String> sendRequestToSAP(){
            Map<String, String> response = new Map<String, String>();
            response.put('RECL_FIN', 'Justified');
            return response;
        }
        private Boolean applyTransitionForManualBA(String complaintId){
            Case complaintCase = caseQuery.getById(complaintId);
            if(complaintCase != null){
                List <wrts_prcgvr__PhaseTransition__c> transitions = transitionsUtl.getTransitions(complaintCase, 'ManualBA', 'A');
                wrts_prcgvr__PhaseTransition__c transition = transitions.isEmpty() ? null : transitions[0];
                if (transition != null) {
                    Map<String, Object> transitionResponse = transitionsUtl.applyTransitions(complaintCase, transition);
                }
            }
            else{
                return false;
            }
            return true;
        }

    }

    public inherited sharing class CheckSelectedSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Id> selectedSupplyIds = (List<Id>) JSON.deserialize(params.get('selectedSupplyIds'), List<Id>.class);
            //String selectedSupplyIds = params.get('selectedSupplyIds');
            if (selectedSupplyIds.isEmpty()) {
                throw new WrtsException('Supplies' + ' - ' + System.Label.MissingId);
            }
            try {
                List<Supply__c>  supplies = supplyQuery.getSuppliesByIds(selectedSupplyIds);
                List<Account> distributors = new List<Account>();
                if(supplies.size()>0 && supplies[0].ServicePoint__r.Distributor__c != null){
                    Set<Id> distIds = new Set<Id>();
                    distIds.add(supplies[0].ServicePoint__r.Distributor__c);
                    distributors = accountQuery.listDistributorsByIds(distIds);
                    if(supplies[0].ServiceSite__c != null) {
                        ServiceSite__c serviceSite = serviceSiteQuery.getById(supplies[0].ServiceSite__c);
                        if(serviceSite != null){
                            response.put('ServiceSiteProvince', serviceSite.SiteProvince__c);
                        }
                    }
                }
                if(distributors.size()>0){
                    response.put('IsDisCoENEL__c',distributors[0].IsDisCoENEL__c);
                    response.put('DistributorProvince', distributors[0].ResidentialProvince__c);
                }
                if(supplies[0].RecordType.DeveloperName == 'Service'){
                    throw new WrtsException('You cannot select this type of service. You must select a commodity one.');
                }
                response.put('supplies',supplies);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class CheckBill extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String markedInvoiceId = params.get('markedInvoiceId');
            String invoiceSerialNumber = markedInvoiceId.split(' ')[0];
            String invoiceNumber = markedInvoiceId.split(' ')[1];

            Set<String> returnCodes = new Set<String>();
            if(MRO_SRV_SapQueryBillsCallOut.checkSapEnabled()) {
                MRO_LC_InvoicesInquiry.CheckBillResponse checkBillsResult = MRO_SRV_SapQueryBillsCallOut.getInstance().checkBill(new MRO_LC_InvoicesInquiry.CheckBillParams(invoiceNumber, invoiceSerialNumber));
                returnCodes = checkBillsResult.getCodes();
            }else{
                switch on invoiceNumber {
                    when '1000000' {
                        returnCodes.add('COD 06');
                    }
                    when '1000001' {
                        returnCodes.add('COD 01');
                        returnCodes.add('COD 06');
                    }
                    when '1000002' {
                        returnCodes.add('COD 05');
                    }
                    when '1000004' {
                        returnCodes.add('COD 07');
                    }
                    when '1000005' {
                        returnCodes.add('COD 04');
                        returnCodes.add('COD 05');
                    }
                    when else {
                        returnCodes.add('COD 01');
                        returnCodes.add('COD 05');
                    }
                }
            }

            response.put('returnCodes', returnCodes);
            response.put('error', false);
            return response;
        }
    }

    public inherited sharing class GetMeters extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('selectedSupplyId');
            String eneltel;
            if(String.isNotEmpty(supplyId)){
                Supply__c supply = supplyQuery.getById(supplyId);
                if(supply != null && supply.ServicePoint__r != null){
                    eneltel = supply.ServicePoint__r.ENELTEL__c;
                }
            }
            String metersJsonString = MRO_SRV_Index.getInstance().getMeterDataByEnelId(eneltel);
            response.put('metersJson', metersJsonString);
            response.put('error', false);
            return response;
        }
    }

    public inherited sharing class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Supply__c> supplies = (List<Supply__c>) JSON.deserialize(params.get('supplies'), List<Supply__c>.class);
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String selectedChannel = params.get('channelSelected');
            String selectedOrigin = params.get('originSelected');
            List<Case> newCases = new List<Case>();
            try {
                if (!supplies.isEmpty() && String.isNotBlank(accountId) && String.isNotBlank(dossierId) && String.isNotBlank(selectedOrigin) && String.isNotBlank(selectedChannel)) {
                    Dossier__c updatedDossier = new Dossier__c(Id = dossierId, Origin__c = selectedOrigin, Channel__c = selectedChannel);
                    updatedDossier.CompanyDivision__c = supplies[0].CompanyDivision__c;
                    databaseSrv.updateSObject(updatedDossier);

                    /*Dossier__c parentDossier = dossierQuery.getById(dossierId);
                    Date SLAExpirationDate;
                    if (parentDossier != null) {
                        SLAExpirationDate = parentDossier.SLAExpirationDate__c;
                    }*/
                    for (Supply__c supply : supplies) {
                        Case complaintCase = caseSrv.createCaseForComplaints(supply, accountId, dossierId, caseRecordTypeId, selectedOrigin, selectedChannel);
                        //complaintCase.SLAExpirationDate__c = SLAExpirationDate;
                        newCases.add(complaintCase);
                    }
                    List<Case> updatedCases = caseSrv.insertCaseRecords(newCases, caseList);
                    response.put('cases', updatedCases);
                    response.put('error', false);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class CreateIndex extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            List<MeterQuadrant> meterQuadrants = (List<MeterQuadrant>) JSON.deserialize(params.get('meterInfo'), List<MeterQuadrant>.class);
            Id accountId = params.get('accountId');
            Index__c indexObject = new Index__c();
            if (!caseList.isEmpty()) {
                Id supplyId = caseList.get(0).Supply__c;
                Supply__c supply = supplyQuery.getById(supplyId);
                Id servicePointId = supply.ServicePoint__c;
                Id caseId = caseList.get(0).Id;
                Double energieInductva = 0;
                Double energieCapacitiva = 0;
                Double energieActiva = 0;
                Double energieActiva1 = 0;
                Double energieActiva2 = 0;
                Double energieActiva3 = 0;
                for (Integer i = 0; i < meterQuadrants.size(); i++) {
                    String idCadran = meterQuadrants.get(i).idCadran;
                    if (idCadran == '1.8.0') {
                        energieActiva = meterQuadrants.get(i).index;
                    }else if (idCadran == '1.8.1') {
                        energieActiva1 = meterQuadrants.get(i).index;
                    } else if (idCadran == '1.8.2') {
                        energieActiva2 = meterQuadrants.get(i).index;
                    } else if (idCadran == '1.8.3') {
                        energieActiva3 = meterQuadrants.get(i).index;
                    }
                }

                indexObject = indexQuery.getByCaseIdAndMeterName(caseId, meterQuadrants[0].meterNumber);
                if(indexObject != null) {
                    indexObject.ActiveEnergy__c = energieActiva;
                    indexObject.ActiveEnergyR1__c = energieActiva1;
                    indexObject.ActiveEnergyR2__c = energieActiva2;
                    indexObject.ActiveEnergyR3__c = energieActiva3;
                    databaseSrv.updateSObject(indexObject);
                }
                else {
                    indexObject = indexServ.createIndexForSelfReading(caseId, accountId, meterQuadrants[0].meterNumber, servicePointId, Date.today(),
                            energieActiva, energieActiva1, energieActiva2, energieActiva3, energieInductva, energieCapacitiva);
                    databaseSrv.insertSObject(indexObject);
                }
            }

            response.put('error', false);
            return response;
        }
    }

    public inherited sharing class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            Savepoint sp = Database.setSavepoint();
            try {
                caseSrv.setCanceledOnCaseAndDossier(caseList, dossierId);
                response.put('error', false);
            }catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return new Map<String, Object>();
        }
    }

    public class MeterQuadrant {
        @AuraEnabled
        public String meterNumber { get; set; }
        @AuraEnabled
        public String quadrantName { get; set; }
        @AuraEnabled
        public Double index { get; set; }
        @AuraEnabled
        public String idCadran { get; set; }
    }
}