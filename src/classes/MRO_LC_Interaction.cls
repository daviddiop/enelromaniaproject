public with sharing class MRO_LC_Interaction  extends ApexServiceLibraryCnt {
    private static final String GENERIC_REQUEST_NAME = 'GenericRequest';

    private static final MRO_SRV_Interaction interactionSrv = MRO_SRV_Interaction.getInstance();
    private static final MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static final MRO_QR_Dossier dossierQueries = MRO_QR_Dossier.getInstance();
    private static final MRO_QR_Contract contractQueries = MRO_QR_Contract.getInstance();
    private static final MRO_QR_Interaction interactionQuery = MRO_QR_Interaction.getInstance();

    static String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(GENERIC_REQUEST_NAME).getRecordTypeId();

    public with sharing class listOptions extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return new Map<String, Object>{
                'statusList' => interactionSrv.listOptions(Interaction__c.Status__c)
            };
        }
    }

    public with sharing class findById extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            return interactionSrv.findById(interactionId);
        }
    }

    public with sharing class insertInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return interactionSrv.insertInteraction();
        }
    }

    public with sharing class upsertInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            MRO_SRV_Interaction.InteractionDTO interactionDto = (MRO_SRV_Interaction.InteractionDTO)JSON.deserialize(jsonInput, MRO_SRV_Interaction.InteractionDTO.class);
            return interactionSrv.upsertInteraction(interactionDto);
        }
    }

    public with sharing class removeInterlocutor extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            return interactionSrv.removeInterlocutor(interactionId);
        }
    }

    public with sharing class getGenericRequestInformation extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String interactionId = params.get('interactionId');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }

            MRO_SRV_Interaction.InteractionDTO interactionDTO = interactionSrv.findById(interactionId);
            String accountId = (interactionDTO != null)? interactionDTO.customerId : null;

            boolean hasKAM = false;
            List<Contract> contracts = contractQueries.getContractsByAccount(accountId);

            for(Contract contract : contracts) {
                if(contract.CompanySignedId != null)
                {
                    hasKAM = true;
                    break;
                }
            }
            response.put('hasKAM', hasKAM);
            response.put('genericRequestRecordTypeId',dossierRecordTypeId);

            return response;
        }
    }

    public with sharing class createGenericRequestDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String interactionId = params.get('interactionId');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }

            MRO_SRV_Interaction.InteractionDTO interactionDTO = interactionSrv.findById(interactionId);
            String accountId = (interactionDTO != null)? interactionDTO.customerId : null;
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            try{
                Dossier__c genericRequestDossier = dossierSrv.generateDossier(accountId, null, interactionId, null, dossierRecordTypeId, null, null, null);
                dossierSrv.updateDossierChannel(genericRequestDossier.Id, interactionDTO.channel, interactionDTO.origin);
                response.put('genericRequestDossierId', genericRequestDossier.Id);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateGenericRequestDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String dossierId = params.get('dossierId');
            String comments = params.get('comments');
            String SLAExpirationDate = params.get('SLAExpirationDate');
            String assignmentProvince = params.get('assignmentProvince');
            String queueDeveloperName = params.get('queue');
            if (String.isBlank(dossierId)) {
                throw new WrtsException(System.Label.Dossier + ' - ' + System.Label.MissingId);
            }

            try{
                Dossier__c dossier = new Dossier__c(
                        Id = dossierId,
                        Comments__c = comments,
                        AssignmentProvince__c = assignmentProvince,
                        PumaUrgency__c = params.get('pumaUrgency'),
                        EnelRegistrationDateAndNumber__c = params.get('enelRegistrationDateAndNumber'),
                        FileNumber__c = params.get('fileNumber')
                );

                dossierSrv.updateGenericRequestDossier(dossier, queueDeveloperName, SLAExpirationDate);
                response.put('genericRequestDossierId', dossierId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public with sharing class saveInstitutionInterlocutor extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            String institutionId = params.get('institutionAccountId');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(institutionId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Interaction__c interactionToUpdate = interactionQuery.findInteractionById(interactionId);
            if(interactionToUpdate != null){
                interactionToUpdate.Institution__c = institutionId;
                Account institutionAccount = MRO_QR_Account.getInstance().findAccount(institutionId);
                try{
                    interactionSrv.updateInteraction(interactionToUpdate);
                    response.put('institutionId', institutionId);
                    response.put('institution', institutionAccount);
                    response.put('error', false);
                } catch (Exception ex) {
                    response.put('error', true);
                    response.put('errorMsg', ex.getMessage());
                    response.put('errorTrace', ex.getStackTraceString());
                }
            }
            return response;
        }
    }
    public with sharing class saveInterlocutorType extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            String interlocutorType = params.get('interlocutorType');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(interlocutorType)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }

            Interaction__c interactionToUpdate = interactionQuery.findInteractionById(interactionId);
            if(interactionToUpdate != null){
                interactionToUpdate.InterlocutorType__c = interlocutorType;
                try{
                    interactionSrv.updateInteraction(interactionToUpdate);
                    response.put('error', false);
                } catch (Exception ex) {
                    response.put('error', true);
                    response.put('errorMsg', ex.getMessage());
                    response.put('errorTrace', ex.getStackTraceString());
                }
            }
            return response;
        }
    }

}