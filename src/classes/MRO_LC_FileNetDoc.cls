/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 15.10.20.
 */

public with sharing class MRO_LC_FileNetDoc extends ApexServiceLibraryCnt {

    public with sharing class listDoc extends AuraCallable {
        public override Object perform(final String jsonInput) {
            RequestParam params = (RequestParam) JSON.deserialize(jsonInput, RequestParam.class);

            if (params.caseId == null) {
                throw new WrtsException('Empty Case Id');
            }

            Case caseRecord = MRO_QR_Case.getInstance().getById(params.caseId);
            params.companyDivisionCode = caseRecord.CompanyDivision__r.Code__c;
            params.documentIssuedFrom = String.valueOf(caseRecord.EffectiveDate__c != null ? caseRecord.EffectiveDate__c : Date.today().addMonths(-1));
            params.documentIssuedTo = String.valueOf(Date.today().addDays(1));
            params.requestId = caseRecord.CaseNumber;
            params.documentType = '0604_CM_ISU';
            params.customerCode = caseRecord.Account.AccountNumber;
            params.processCode = caseRecord.CaseTypeCode__c;

            if (MRO_SRV_SapQueryFilenetDocList.checkSapEnabled()) {

                List<FileNetDoc> fileNetList = MRO_SRV_SapQueryFilenetDocList.getInstance().getList(params);

                return fileNetList;
            }
            return listDummyDoc(params, caseRecord);
        }
    }

    public static List<FileNetDoc> listDummyDoc(RequestParam params, Case caseRecord) {
        List<FileNetDoc> docList = new List<FileNetDoc>();
        for (Integer i = 0; i < 10; i++) {
            docList.add(new FileNetDoc(
                    'DOC10000' + i,
                    params.customerCode,
                    '202029417100009225E784D8E68512DCA98255' + i,
                    params.documentType,
                    params.processCode,
                    params.requestId,
                    caseRecord.EffectiveDate__c
            ));
        }
        return docList;
    }

    public class RequestParam {
        @AuraEnabled
        public String companyDivisionCode { get; set; }
        @AuraEnabled
        public String processCode { get; set; }
        @AuraEnabled
        public String documentIssuedFrom { get; set; }
        @AuraEnabled
        public String documentIssuedTo { get; set; }
        @AuraEnabled
        public String requestId { get; set; }
        @AuraEnabled
        public String integrationId { get; set; }
        @AuraEnabled
        public String customerCode { get; set; }
        @AuraEnabled
        public String taxNumber { get; set; }
        @AuraEnabled
        public String documentType { get; set; }
        @AuraEnabled
        public String caseId { get; set; }

        public RequestParam() {
        }

        public Map<String, Object> toMap(){
            Map<String, Object> conditions = new Map<String, Object>{
                    'CompanyDivisionCode' => this.companyDivisionCode
            };

            if (this.processCode != null) {
                conditions.put('ProcessCode', this.processCode);
            }

            if (this.requestId != null) {
                conditions.put('RequestId', this.requestId);
            }

            if (this.customerCode != null) {
                conditions.put('CustomerCode', this.customerCode);
            }

            if (this.integrationId != null) {
                conditions.put('IntegrationId', this.integrationId);
            }

            if (this.taxNumber != null) {
                conditions.put('TaxNumber', this.taxNumber);
            }

            if (this.documentType != null) {
                conditions.put('DocumentType', this.documentType);
            }

            if (this.documentIssuedFrom != null) {
                conditions.put('DocumentIssuedFrom', MRO_SRV_SapQueryFilenetDocList.parseDate(this.documentIssuedFrom));
            }

            if (this.documentIssuedTo != null) {
                conditions.put('DocumentIssuedTo', MRO_SRV_SapQueryFilenetDocList.parseDate(this.documentIssuedTo));
            }
            return conditions;
        }
    }

    public class FileNetDoc {
        @AuraEnabled
        public String documentId { get; set; }
        @AuraEnabled
        public String customerCode { get; set; }
        @AuraEnabled
        public String filenetId { get; set; }
        @AuraEnabled
        public String documentType { get; set; }
        @AuraEnabled
        public String processCode { get; set; }
        @AuraEnabled
        public String requestId { get; set; }
        @AuraEnabled
        public Date documentIssuedFrom { get; set; }


        public FileNetDoc(String documentId, String customerCode, String filenetId,
                String documentType, String processCode, String requestId, Date documentIssuedFrom) {
            this.documentId = documentId;
            this.customerCode = customerCode;
            this.filenetId = filenetId;
            this.documentType = documentType;
            this.processCode = processCode;
            this.requestId = requestId;
            this.documentIssuedFrom = documentIssuedFrom;
        }

        public FileNetDoc(Map<String, String> stringMap) {
            this.documentId = stringMap.get('DocumentId');
            this.customerCode = stringMap.get('CustomerCode');
            this.filenetId = stringMap.get('FilenetId');
            this.documentType = stringMap.get('DocumentType');
            this.processCode = stringMap.get('ProcessCode');
            this.requestId = stringMap.get('RequestId');

            try {
                this.documentIssuedFrom = MRO_SRV_SapQueryFilenetDocList.parseDate(stringMap.get('DocumentIssuedFrom'));
            } catch (Exception e) {
                System.debug('Invalid Date FileNetDoc ' + stringMap.get('DocumentIssuedFrom'));
            }
        }
    }
}