/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 12, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_FileMetadata {
    private static MRO_QR_FileMetadata fileMetadataQuery = MRO_QR_FileMetadata.getInstance();
    private static ContentDocumentQueries contentDocumentQuery = ContentDocumentQueries.getInstance();
    private static ContentDocumentLinkQueries contentDocumentLinkQuery = ContentDocumentLinkQueries.getInstance();

    private static DatabaseService databaseSrv = DatabaseService.getInstance();

    public static MRO_SRV_FileMetadata getInstance() {
        return (MRO_SRV_FileMetadata) ServiceLocator.getInstance(MRO_SRV_FileMetadata.class);
    }

    public FileMetadataDTO getFileMetadataById(String fileMetadataId) {
        FileMetadata__c fileMetadata = String.isNotBlank(fileMetadataId) ? fileMetadataQuery.findById(fileMetadataId) : null;
        return fileMetadata != null ? new FileMetadataDTO(fileMetadata) : null;
    }

    public FileMetadata__c insertFileMetadata(String dossierId) {
        FileMetadata__c fileMetadata = new FileMetadata__c(Dossier__c = dossierId);
        databaseSrv.insertSObject(fileMetadata);
        return fileMetadata;
    }

    public FileMetadata__c findOrCreateTempFileMetadata() {
        FileMetadata__c fileMetadata = fileMetadataQuery.findByTitle(Label.TempFileMetadataName);
        if (fileMetadata == null) {
            fileMetadata = new FileMetadata__c(
                Title__c = Label.TempFileMetadataName
            );
            databaseSrv.insertSObject(fileMetadata);
        }
        return fileMetadata;
    }

    public void insertFileMetadata(String tempFileMetadataId, String documentId, String dossierId) {
        ContentDocument document = contentDocumentQuery.findById(documentId);
        FileMetadata__c fileMetadata = new FileMetadata__c(
            Dossier__c = dossierId,
            DocumentId__c = documentId,
            Title__c = document.Title
        );
        databaseSrv.insertSObject(fileMetadata);
        List<ContentDocumentLink> linkList = contentDocumentLinkQuery.listByContentDocumentAndEntityId(documentId, tempFileMetadataId);
        List<ContentDocumentLink> linkToCreateList = new List<ContentDocumentLink>();
        for (ContentDocumentLink link : linkList) {
            ContentDocumentLink newLink = link.clone(false, false, false, false);
            newLink.LinkedEntityId = fileMetadata.Id;
            linkToCreateList.add(newLink);
        }
        databaseSrv.deleteSObject(linkList);
        databaseSrv.insertSObject(linkToCreateList);
    }

    /**
    *
    * author: INSA BADJI
    * Description: Insert a File Metadata for Invoice
    * Params: DossierId and Inatance of FileMatadata__c
     */
    public void insertFileMetadaForInvoiceAndFisa(String type, List<MRO_LC_InvoiceDuplicate.Invoice> listInvoices,String idDocType, Dossier__c updatedDossier, List<Case> listCases){

        List<FileMetadata__c> listFileMetadata = new List<FileMetadata__c>();
        if(listInvoices.size() > 0){
            for(Integer i =0; i < listInvoices.size();i++){
                if (String.isNotBlank(listInvoices[i].fileNetId)) {
                    String invoiceId = '';
                    if (String.isNotBlank(listInvoices[i].invoiceId)) {
                        invoiceId = type+' '+listInvoices[i].invoiceId;
                    }
                    FileMetadata__c fileMetadata = new FileMetadata__c(
                        Title__c = invoiceId,
                        TemplateClass__c = 'ArchiveProcessing',
                        TemplateCode__c = 'ArchiveProduction',
                        TemplateVersion__c = 'v1',
                        DocumentType__c = idDocType,
                        Dossier__c = updatedDossier.Id,
                        ExternalId__c = listInvoices[i].fileNetId,
                        Case__c = listCases[0].Id
                    );
                    listFileMetadata.add(fileMetadata);
                }

            }
        }
        if (listFileMetadata.size() > 0) {
            databaseSrv.insertSObject(listFileMetadata);
        }
    }

    public void insertFileMetadaForFisa(String type, List<MRO_LC_FisaInquiry.Fisa> listFisas,String idDocType, Dossier__c updatedDossier, List<Case> listCases){

        List<FileMetadata__c> listFileMetadata = new List<FileMetadata__c>();
        if(listFisas.size() > 0){
            for(Integer i =0; i < listFisas.size();i++){
                if (String.isNotBlank(listFisas[i].fileNetId)) {
                    String fisa = '';
                    if (type == 'Fisa' && String.isNotBlank(listFisas[i].fisaId)) {
                        fisa = type+' '+listFisas[i].fisaId;
                    }

                    FileMetadata__c fileMetadata = new FileMetadata__c(
                        Title__c = fisa,
                        TemplateClass__c = 'ArchiveProcessing',
                        TemplateCode__c = 'ArchiveProduction',
                        TemplateVersion__c = 'v1',
                        Dossier__c = updatedDossier.Id,
                        ExternalId__c = listFisas[i].fileNetId

                    );
                    if (listCases.size() > 0 && String.isNotBlank(idDocType)) {
                        fileMetadata.Case__c = listCases[0].Id;
                        fileMetadata.DocumentType__c = idDocType;
                    }
                    listFileMetadata.add(fileMetadata);

                }

            }
        }
        if (listFileMetadata.size() > 0) {
            databaseSrv.insertSObject(listFileMetadata);
        }
    }

    public class FileMetadataDTO {
        @AuraEnabled
        public String id { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String dossierName { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public Datetime createdDate { get; set; }

        public FileMetadataDTO() {
        }

        public FileMetadataDTO(FileMetadata__c fileMetadata) {
            this.id = fileMetadata.Id;
            this.name = fileMetadata.Name;
            this.dossierId = fileMetadata.Dossier__c;
            this.dossierName = fileMetadata.Dossier__c != null ? fileMetadata.Dossier__r.Name : '';
            this.caseId = fileMetadata.Case__c;
            this.createdDate = fileMetadata.CreatedDate;
        }
    }
}