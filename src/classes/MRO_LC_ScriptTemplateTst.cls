/**
 * Created by Boubacar Sow  on 26/12/2019.
 * @author Bouabacr Sow
 * @date  26/12/2019
 * @description Class test for the MRO_LC_ScriptTemplate Controller.
 *              [ENLCRO-353] Power Voltage Change  Implementation Wizard.
 *
 */
@IsTest
public with sharing class MRO_LC_ScriptTemplateTst {
    @TestSetup
    private static void setup() {
        ScriptTemplate__c myST = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        insert myST;

        ScriptTemplateElement__c mySTE = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        mySTE.ScriptTemplate__c	= myST.id;
        insert mySTE;
    }

    @IsTest
    private static void getScriptElementsTest(){
        ScriptTemplate__c myST = [
            SELECT Id
            FROM ScriptTemplate__c
            LIMIT 1
        ];
        Test.startTest();
        Map<String, String> jsonInput = new Map<String, String>{
                'templateId' => myST.Id
        };
        Object response = TestUtils.exec('MRO_LC_ScriptTemplate', 'getScriptElements', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false, 'No error found');

        Test.stopTest();
    }

    @IsTest
    private static void getScriptElementsExceptionTest(){
        Test.startTest();
        Map<String, String> jsonInput = new Map<String, String>{
            'templateId' => ''
        };
        Object response = TestUtils.exec('MRO_LC_ScriptTemplate', 'getScriptElements', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assert(true, 'Error found');
        Test.stopTest();
    }

}