public with sharing class MRO_QR_CatalogItem {

	public static MRO_QR_CatalogItem getInstance() {
		return (MRO_QR_CatalogItem)ServiceLocator.getInstance(MRO_QR_CatalogItem.class);
	}

	public NE__Catalog_Item__c getByCommercialProductId(String commProdId){
		return [
				SELECT id, NE__Max_Qty__c, NE__Category_Max_Qty__c
				from NE__Catalog_Item__c
				where NE__ProductId__c =: commProdId limit 1
		];
	}



}