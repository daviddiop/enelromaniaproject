public class RegistryChangeWizardCnt extends ApexServiceLibraryCnt {
    private static final AccountQueries accountQuerySrv = AccountQueries.getInstance();
    private static final AccountService accountSrv = AccountService.getInstance();
    private static final CaseQueries caseQuerySrv = CaseQueries.getInstance();
    private static AccountContactRelationService accountContactRelationSrv = AccountContactRelationService.getInstance();

    public class getOptionList extends AuraCallable {
        public override Object perform ( final String jsonInput ){
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account +' - '+ System.Label.MissingId);
            }
            Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
            Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');
            Schema.RecordTypeInfo businessRt = accountRts.get('BusinessProspect');
            List<String> accountRecordTypeList = new List<String>{
                    personRt.getName(),
                    businessRt.getName()
            };

            return new Map<String,Object> {
                   // 'hasCompanyDivision' => hasCompanyDivision,
                    'personRecordType'=> new Map<String,String>{'label'=>personRt.getName(),'value'=>personRt.getRecordTypeId()},
                    'businessRecordType'=> new Map<String,String>{'label'=>businessRt.getName(),'value'=>businessRt.getRecordTypeId()}
            };
        }
    }

    public class retrieveAccount extends AuraCallable{
        public override Object perform (final String jsonInput){
            String accountId = (String)JSON.deserializeUntyped(jsonInput);
            return accountQuerySrv.findAccount(accountId);
        }
    }
    public class getCaseList extends AuraCallable{
        public override Object perform (final String jsonInput){
            String accountId = (String)JSON.deserializeUntyped(jsonInput);
            Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
            String  fastRecordTypeId = caseRts.get('RegistryChangeFast').getRecordTypeId();
            String  slowRecordTypeId = caseRts.get('RegistryChangeSlow').getRecordTypeId();
            return caseQuerySrv.listRelatedCases(accountId,slowRecordTypeId,fastRecordTypeId);
        }
    }
    public class getContactList extends AuraCallable{
        public override Object perform (final String jsonInput){
            Account account = (Account)JSON.deserialize(jsonInput,Account.class);
            return accountContactRelationService.listRelatedAccounts(account);
        }
    }
    public class saveNewRegistry extends AuraCallable{
        public override  Object perform (final String jsonInput){
            InputData inputData = (InputData) JSON.deserialize(jsonInput, InputData.class);
            return accountSrv.saveRegistryChange(inputData);
        }
    }

    public class InputData {
        @AuraEnabled
        public Case slowcase {get;set;}
        @AuraEnabled
        public Case fastcase {get;set;}
        @AuraEnabled
        public String accountId {get;set;}
        @AuraEnabled
        public String companyDivisionId {get;set;}
        @AuraEnabled
        public Boolean residentialAddressForced {get;set;}
    }

}