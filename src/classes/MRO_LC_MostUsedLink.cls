/**
 * Created by Vlad Mocanu on 21/01/2020.
 */

public with sharing class MRO_LC_MostUsedLink extends ApexServiceLibraryCnt {
    private static MRO_QR_MostUsedLink mostUsedLinkQueries = MRO_QR_MostUsedLink.getInstance();
    private static MRO_SRV_MostUsedLink mostUsedLinkSrv = MRO_SRV_MostUsedLink.getInstance();

    public class upsertMostUsedLink extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Id linkId = params.get('linkId');
            Id userId = UserInfo.getUserId();
            Map<String, Object> response = new Map<String, Object>();

            try {
                mostUsedLinkSrv.upsertMostUsedLink(linkId, userId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class getListOfMostUsedLinkIds extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            try {
                List<MostUsedLink__c> selectedMostUsedLinks = mostUsedLinkQueries.getMostUsedLinksByUser(UserInfo.getUserId(), null);

                List<Id> idList = new List<Id>();
                for (Integer i = 0; i < selectedMostUsedLinks.size(); i++) {
                    idList.add(selectedMostUsedLinks[i].Id);

                }
                response.put('idList', idList);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}