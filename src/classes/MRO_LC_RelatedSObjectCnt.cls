/**
 * Created by tommasobolis on 29/06/2020.
 */
public with sharing class MRO_LC_RelatedSObjectCnt {

    /**
     *This method is used to initialize the 'mlm_lwc-relatedSObject' LWC component
     * @param recordId contains the record Id of the SObject where the 'mlm_lwc-relatedSObject' component is placed
     * @param relationshipFieldAPIName contains the API Name of the field which contains the Id of the relationship sObject
     * @return Map<String,Object> contains either relationship SObject's Id, Type and API Name of the fields to get or the error's information
     */
    @AuraEnabled
    public static Map<String, Object> initialize(Id recordId, String title, String fieldSet, String relationshipFieldAPIName){

        Map<String, Object> returnValue = new Map<String, Object> ();
        returnValue.put('error', false);
        try {

            String sObjectType = recordId.getSobjectType().getDescribe().getName(); //retrieve the SObject type of the SObject where the component 'mlm_lwc-relatedSObject' is placed
            //retrive the relationship SObject's Id
            String sQuery = ' SELECT Id, ' + String.escapeSingleQuotes(relationshipFieldAPIName)  +
                    ' FROM ' + String.escapeSingleQuotes(sObjectType) +
                    ' WHERE Id = \'' +  String.escapeSingleQuotes(recordId) + '\' LIMIT 1';

            List<SObject> results = Database.query(sQuery);
            Id relationshipSObjectId = (Id) results[0].get(relationshipFieldAPIName);

            sObjectType = relationshipSObjectId.getSobjectType().getDescribe().getName(); //retrieve the SObject type of the relationship SObject
            //populate label Map
            Map<String, String> label = new Map<String,String>();
            if(String.isNotBlank(title) && title.contains('$Label.')){
                String[] split = title.split('\\.');
                String userLanguage = UserInfo.getLanguage();
                List<String> translations = MRO_UTL_LabelTranslator.translate(new List<String>{split[1]}, userLanguage);
                label.put('title', translations[0]);

            } else if (String.isNotBlank(title) && !title.contains('$Label.')){

                label.put('title', title);
            }else{

                String sObjectLabel = Schema.getGlobalDescribe().get(sObjectType).getDescribe().getLabel();
                label.put('title', sObjectLabel);
            }
            label.put('view', System.Label.View);

            //retrieve the list of API Name fields to get from the FieldSet 'RelatedSObjectComponentFieldSet' defined in the relationship SObject
            DescribeSObjectResult objResult = Schema.getGlobalDescribe().get(sObjectType).getDescribe();
            Schema.FieldSet fs = objResult.fieldSets.getMap().get(fieldSet); //'RelatedSObjectComponentFieldSet'

            //populate the Map<String,Object> to return
            returnValue.put('relationshipSObjectId', relationshipSObjectId);
            returnValue.put('relationshipSObjectType', sObjectType);

            if(fs != null) {

                List<Schema.FieldSetMember> fsms = fs.getFields();
                List<String> relationshipSObjectFieldsToGet = new List<String>();
                for(Schema.FieldSetMember fsm: fsms){
                    relationshipSObjectFieldsToGet.add(fsm.getFieldPath());
                }
                returnValue.put('relationshipSObjectFieldsToGet', relationshipSObjectFieldsToGet);
            }

            returnValue.put('label', label);

        }catch(Exception e){

            returnValue.put('error', true);
            returnValue.put('errorMessage', e.getMessage());
            returnValue.put('errorStackTraceString', e.getStackTraceString());
        }
        return returnValue;
    }
}