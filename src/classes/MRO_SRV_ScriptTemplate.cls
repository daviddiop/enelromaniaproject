/**
 * Created by Boubacar Sow  on 23/12/2019.
 */

public with sharing class MRO_SRV_ScriptTemplate /*extends ApexServiceLibraryCnt*/ {
    private static MRO_QR_ScriptTemplate scriptElementQuery = MRO_QR_ScriptTemplate.getInstance();
    private static String scriptTemplateKeyPrefix = ScriptTemplate__c.sObjectType.getDescribe().getKeyPrefix();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    static MRO_UTL_Constants constantSrv = MRO_UTL_Constants.getAllConstants();

    public static MRO_SRV_ScriptTemplate getInstance() {
        return (MRO_SRV_ScriptTemplate)ServiceLocator.getInstance(MRO_SRV_ScriptTemplate.class);
    }

/*public with sharing class getScriptElements extends AuraCallable {
public override Object perform(final String jsonInput) {
Map<String, String> params = asMap(jsonInput);
String templateId = params.get('templateId');

if (String.isBlank(templateId) || !templateId.startsWith(scriptTemplateKeyPrefix)) {
throw new ScriptException(System.Label.ScriptTemplate +' - '+ System.Label.MissingId);

}
return scriptElementQuery.getByScriptTemplateId(templateId);
}
}*/

    /**
     * @author Boubacar Sow
     * @date 23/12/2019
     * @description  get or insert a  ScriptTemplate.
     *              [ENLCRO-357] Power Voltage Change Implementation Script Template
     *
     * @param templateId
     *
     * @return ScriptTemplate__c
     */
    public ScriptTemplate__c generateScriptTemplate(String templateId, String code){
        ScriptTemplate__c scriptTemplate;
        if (String.isBlank(templateId)) {
            scriptTemplate = new ScriptTemplate__c();
            scriptTemplate.Code__c = code;
            databaseSrv.insertSObject(scriptTemplate);
        }else{
            System.debug('getRecord');
            scriptTemplate = scriptElementQuery.getScriptTemplateById(templateId);
        }
        return scriptTemplate;
    }

    /**
     * @author Boubacar Sow
     * @date 23/12/2019
     * @description Insert a ScriptTemplateElement.
     *              [ENLCRO-357] Power Voltage Change Implementation Script Template
     *
     * @param templateId
     * @param htmlContent
     * @param recordTypeId
     * @param variable
     * @param order
     *
     * @return ScriptTemplateElement__c
     */
    public ScriptTemplateElement__c insertScriptTemplateElement(String templateId, String htmlContent, String variable, double order){
        ScriptTemplateElement__c scriptTemplateElement;
        if (String.isNotBlank(templateId)) {
            ScriptTemplate__c scriptTemplate = scriptElementQuery.getScriptTemplateById(templateId);
            scriptTemplateElement = new  ScriptTemplateElement__c();
            scriptTemplateElement.HTMLContent__c = htmlContent;
            scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
            scriptTemplateElement.Variable__c = variable;
            scriptTemplateElement.Order__c = order;
            databaseSrv.insertSObject(scriptTemplateElement);
        }
        return scriptTemplateElement;
    }

    public List<ScriptTemplateElement__c> getScriptTemplateElements(String templateId){
        if (String.isBlank(templateId) || !templateId.startsWith(scriptTemplateKeyPrefix)) {
            throw new ScriptException(System.Label.ScriptTemplate +' - '+ System.Label.MissingId);

        }
        return scriptElementQuery.getByScriptTemplateId(templateId);
    }

    /**
    * @author Luca Ravicini
    * @description get ScriptTemplateElement__c by ScriptTemplate__c Code__c [ENLCRO-699] - ScriptManagment
    * @param  templateCode the ScriptTemplate__c Code__c
    * @since Mar 16,2020
    *
   */
    public List<ScriptTemplateElement__c> getScriptTemplateElementsByCode(String templateCode){

        return scriptElementQuery.getByScriptTemplateByCode(templateCode);
    }

    public class ScriptException extends Exception {

    }

}