/**
 * Created by ferhati on 02/08/2019.
 */

public with sharing class MRO_LC_PrivacyChange extends ApexServiceLibraryCnt {
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();
    private static MRO_QR_PrivacyChange privacyChangeQuery = MRO_QR_PrivacyChange.getInstance();
    private static MRO_QR_Opportunity opportunityQuery = MRO_QR_Opportunity.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();

    public static MRO_LC_PrivacyChange getInstance() {
        return new MRO_LC_PrivacyChange();
    }

    public with sharing class getIndividual extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String individualId = '';
            try {
                String opportunityId = params.get('opportunityId');
                String dossierId = params.get('dossierId');
                String contactRecordId = params.get('contactId');
                //Contact contactRecord = (Contact) JSON.deserialize(params.get('contactRecord'), Contact.class);
                //String contactRecord = params.get('contactId');
                CustomerInteraction__c customerInteraction;
                Account customer;
                PrivacyChange__c privacyChange;
                response.put('doNotCreatePrivacyChange', false);

                if (!String.isBlank(dossierId)) {
                    Dossier__c dossier = dossierQuery.getById(dossierId);
                    customerInteraction = dossier.CustomerInteraction__r;
                    customer = dossier.Account__r;
                    privacyChange = privacyChangeQuery.getPendingPrivacyChangeByDossierId(dossierId);
                }
                else if (!String.isBlank(opportunityId)) {
                    Opportunity opp = opportunityQuery.getOpportunityById(opportunityId);
                    customerInteraction = opp.CustomerInteraction__r;
                    customer = opp.Account;
                    privacyChange = privacyChangeQuery.getPendingPrivacyChangeByOpportunityId(opportunityId);
                }

                if (privacyChange != null) {
                    individualId = privacyChange.Individual__c;
                } else if(privacyChange == null &&  contactRecordId != null){
                    Contact contact = contactQuery.getById(contactRecordId);
                    Individual individualRecord;
                    if (contact.Individual != null) {
                        individualRecord = contact.Individual;
                        individualId = individualRecord.Id;
                    } else {
                        individualRecord = new Individual();
                        individualRecord.FirstName = contact.FirstName;
                        individualRecord.LastName = contact.LastName;
                        individualRecord.NationalIdentityNumber__c = contact.NationalIdentityNumber__c;
                        individualRecord.HasOptedOutTracking= false;
                        individualRecord.HasOptedOutProfiling = false;
                        individualRecord.HasOptedOutProcessing = false;
                        individualRecord.HasOptedOutSolicit = false;
                        individualRecord.HasOptedOutGeoTracking = false;
                        individualRecord.HasOptedOutPartners__c = false;
                        insert individualRecord;
                        individualId = individualRecord.Id;
                    }
                    privacyChange = new PrivacyChange__c(
                            Individual__c = individualId,
                            Contact__c = contact.Id,
                            HasOptedOutTracking__c = individualRecord.HasOptedOutTracking,
                            HasOptedOutProfiling__c = individualRecord.HasOptedOutProfiling,
                            HasOptedOutProcessing__c = individualRecord.HasOptedOutProcessing,
                            HasOptedOutSolicit__c = individualRecord.HasOptedOutSolicit,
                            HasOptedOutGeoTracking__c = individualRecord.HasOptedOutGeoTracking,
                            HasOptedOutPartners__c = individualRecord.HasOptedOutPartners__c,
                            CanStorePiiElsewhere__c = individualRecord.CanStorePiiElsewhere
                    );
                }
                else {
                    Id contactId;
                    if (customerInteraction != null && customerInteraction.Contact__c != null) {
                        contactId = customerInteraction.Contact__c;
                    }
                    else if (customer != null && customer.IsPersonAccount) {
                        contactId = customer.PersonContactId;
                    }
                    if (contactId != null) {
                        Contact contact = contactQuery.getById(contactId);
                        individualId = contact.IndividualId;
                        if (individualId != null) {
                            Individual individual = contact.Individual;
                            privacyChange = new PrivacyChange__c(
                                Individual__c = individualId,
                                Contact__c = contact.Id,
                                HasOptedOutTracking__c = individual.HasOptedOutTracking,
                                HasOptedOutProfiling__c = individual.HasOptedOutProfiling,
                                HasOptedOutProcessing__c = individual.HasOptedOutProcessing,
                                HasOptedOutSolicit__c = individual.HasOptedOutSolicit,
                                HasOptedOutGeoTracking__c = individual.HasOptedOutGeoTracking,
                                HasOptedOutPartners__c = individual.HasOptedOutPartners__c,
                                ShouldForget__c = individual.ShouldForget,
                                SendIndividualData__c = individual.SendIndividualData,
                                CanStorePiiElsewhere__c = individual.CanStorePiiElsewhere,
                                MarketingChannel__c = contact.MarketingContactChannel__c,
                                MarketingEmail__c = contact.MarketingEmail__c,
                                MarketingMobilePhone__c = contact.MarketingMobilePhone__c,
                                MarketingPhone__c = contact.MarketingPhone__c
                            );
                        }
                        else {
                            response.put('doNotCreatePrivacyChange', true);
                        }
                    } else {
                        response.put('doNotCreatePrivacyChange', true);
                    }
                }
                response.put('individualId', individualId);
                response.put('privacyChange', privacyChange);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getIndividualPrivacyRecap extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String individualId = '';
            String label = '';
            try {

                String accountId = params.get('accountId');


                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account acc = accountQuery.findAccount(accountId);
                List<AccountContactRelation> listContacts= accountQuery.listContacts(accountId);

                if(acc.IsPersonAccount){
                    individualId = acc.PersonIndividualId;
                    if(String.isBlank(individualId)){
                        label = System.Label.PersonIndividualNotSet;
                    }

                }else {

                    if (!listContacts.isEmpty()) {

                        for (AccountContactRelation accContRelation : listContacts) {

                            if (accContRelation.Contact.IndividualId != null) {
                                individualId = accContRelation.Contact.IndividualId;
                            }
                        }
                        if(String.isBlank(individualId)){
                            label = System.Label.LegalRepresentativeNotSet;
                        }
                    }else{
                        if(String.isBlank(individualId)){
                            label = System.Label.LegalRepresentativeNotSet;
                        }
                    }
                }


                response.put('individualId', individualId);
                response.put('label', label);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}