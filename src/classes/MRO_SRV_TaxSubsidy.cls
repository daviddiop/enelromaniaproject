/**
 * Created by David diop on 18.11.2019.
 */

public with sharing class MRO_SRV_TaxSubsidy extends ApexServiceLibraryCnt{
    static DatabaseService databaseSrv = DatabaseService.getInstance();
    static Constants constantSrv = Constants.getAllConstants();
    private static String dossierNewStatus = constantSrv.DOSSIER_STATUS_NEW;
    private static String caseNewStatus = constantSrv.CASE_STATUS_NEW;

    public static MRO_SRV_TaxSubsidy getInstance() {
        return new MRO_SRV_TaxSubsidy();
    }
    /**
    * @author  David Diop
    * @description delete TaxSubsidy Record.
    * @history 18/11/2019 David diop - Original
    * @param taxSubsidyId
    * @return
    */
    public void deleteTaxSubsidy(String recordId) {
        databaseSrv.deleteSObject(recordId);
    }
    public void updateTaxSubsidy(TaxSubsidy__c taxSubsidy){
        databaseSrv.updateSObject(taxSubsidy);
    }

    public void createTaxSubsidy(TaxSubsidy__c taxSubsidy){
        databaseSrv.insertSObject(taxSubsidy);
    }
    /**
    * @author  David Diop
    * @description setNewOnCaseAndDossier (update Dossier and Case) with Tax Change wizard.
    * @history 18/11/2019 David diop - Original
    * @param caseRecordId
    * @param dossierId
    * @param companyDivisionId
    * @return
    */
    public void setNewOnCaseAndDossier(String caseRecord, String dossierId, String companyDivisionId) {
        if (String.isNotBlank(caseRecord) && String.isNotBlank(dossierId)) {
            System.debug('div'+companyDivisionId);
            if (dossierId != null) {
                Dossier__c dossier = new Dossier__c (Id = dossierId, Status__c = dossierNewStatus, CompanyDivision__c = companyDivisionId);
                databaseSrv.updateSObject(dossier);
            }
            if (caseRecord != null ) {
                Case caseUpdated = new Case(Id = caseRecord, Dossier__c = dossierId, Status = caseNewStatus);
                databaseSrv.updateSObject(caseUpdated);
            }
        }
    }
}