/**
 * Created by giupastore on 31/08/2020.
 */

public with sharing class MRO_LC_DecoRecoNotification extends ApexServiceLibraryCnt {

	private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
	private static final String CLAIMED = 'Claimed';
	private static final String MARKED = 'Marked';
	private static List<String> markedStatuses = new List<String>{CLAIMED, MARKED};
	private static Map<String, String> sapStatusToStatus = new Map<String, String>{
		'R' => CLAIMED,
		'D' => MARKED,
		'I' => 'Temporary Marked'
	};

	public with sharing class listDisconnectionNotification extends AuraCallable {
		public override Object perform(final String jsonInput) {
			DisconnectionNotificationParam params = (DisconnectionNotificationParam) JSON.deserialize(jsonInput, DisconnectionNotificationParam.class);
			if (params.startDate == null) {
				throw new WrtsException('Start date is empty');
			}
			if (params.endDate == null) {
				throw new WrtsException('End date is empty');
			}

            Supply__c supply;
            if (params.supplyId != null ) {
                supply = MRO_QR_Supply.getInstance().getById(params.supplyId);
                params.customerCode = supply.Account__c;
            }
            if (params.customerCode == null ) {
                throw new WrtsException('CustomerCode is empty');
            }
            Account acc = accountQuery.findAccount(params.customerCode);
            if (acc != null && acc.AccountNumber != null) {
                params.customerCode = acc.AccountNumber;
            }

			if(MRO_SRV_SapQueryDecoNotification.checkSapEnabled()){
                List<DisconnectionNotification> decoRecoList = MRO_SRV_SapQueryDecoNotification.getInstance().getDecoRecoList(params);

                if (supply != null && supply.ServiceSite__r != null) {
                    return filterBySupply(supply, decoRecoList);
                }

				return decoRecoList;
			}

			return listDummyDisconnectionNotification(params, supply);
		}
	}

    private static List<DisconnectionNotification> filterBySupply(Supply__c supply, List<DisconnectionNotification> originList){
        List<DisconnectionNotification> filteredList = new List<DisconnectionNotification>();
        for(DisconnectionNotification notification : originList){
            String consumptionPlaceId = supply.RecordType.DeveloperName == 'Electric' ? supply.ServiceSite__r.IntegrationKeyELE__c : (supply.RecordType.DeveloperName == 'Gas' ? supply.ServiceSite__r.IntegrationKeyGAS__c : null);
            if(consumptionPlaceId == notification.consumptionPlaceId){
                filteredList.add(notification);
            }
        }
        return filteredList;
    }

	public static List<DisconnectionNotification> listDummyDisconnectionNotification (DisconnectionNotificationParam params, Supply__c supply) {
		List<MRO_LC_DecoRecoNotification.DisconnectionNotification> disconnectionNotificationList = new List<MRO_LC_DecoRecoNotification.DisconnectionNotification>();

		for (Integer i = 0; i < 10; i++) {
			disconnectionNotificationList.add(new DisconnectionNotification(
				'100000' + i,
				Date.today().addDays(i),
				'notif type ' + getDummyStatus(i) ,
				 'SAP status ' + getDummyStatus(i),
				'100000' + i,
				'return reason ' + i,
				'990000' +i,
				'notif status ' +getDummyStatus(i) ,
				'dummy error message'
			));
		}
        if (supply != null && supply.ServiceSite__r != null) {
            return filterBySupply(supply, disconnectionNotificationList);
        }
		return disconnectionNotificationList;
	}


	private static String getDummyStatus(Integer index) {
		if (Math.mod(index, 2) == 0) {
			return '';
		} else if (Math.mod(index, 3) == 0) {
			return 'D';
		} else if (Math.mod(index, 5) == 0) {
			return 'R';
		} else if (Math.mod(index, 7) == 0) {
			return 'I';
		}
		return '';
	}

	public class DisconnectionNotificationParam {
		@AuraEnabled
		public String customerCode { get; set; }
		@AuraEnabled
		public String startDate { get; set; }
		@AuraEnabled
		public String endDate { get; set; }
		@AuraEnabled
		public String supplyId { get; set; }

		public DisconnectionNotificationParam ( String customerCode) {
			this.startDate = startDate;
			this.endDate = endDate;
			this.customerCode = customerCode;
		}
	}

	public class DisconnectionNotification {
		@AuraEnabled
		public String notificationNumber { get; set; }
		@AuraEnabled
		public Date notificationDate { get; set; }
		@AuraEnabled
		public String notificationType { get; set; }
		@AuraEnabled
		public String sapStatus { get; set; }
		@AuraEnabled
		public String consumptionPlaceId { get; set; }
		@AuraEnabled
		public String returnReason { get; set; }
		@AuraEnabled
		public String notificationId { get; set; }
		@AuraEnabled
		public String notificationStatus { get; set; }
		@AuraEnabled
		public String errorMessage { get; set; }

		@AuraEnabled
		public String getStatus() {
			if (String.isNotBlank(sapStatus)) {
				String status = sapStatusToStatus.get(sapStatus);
				if (status == null) {
					status = 'Unknown';
				}
				return status;
			}
			return sapStatus;
		}

		@AuraEnabled
		public Boolean getIsMarked() {
			return markedStatuses.contains(getStatus());
		}

		public DisconnectionNotification(String notificationNumber, Date notificationDate, String notificationType, String sapStatus, String consumptionPlaceId, String returnReason, String notificationId,
			String notificationStatus, String errorMessage ) {
			this.notificationNumber = notificationNumber;
			this.notificationDate = notificationDate;
			this.notificationType = notificationType;
			this.sapStatus = sapStatus;
			this.consumptionPlaceId = consumptionPlaceId;
			this.returnReason = returnReason;
			this.notificationId = notificationId;
			this.notificationStatus = notificationStatus;
			this.errorMessage = errorMessage;
		}

		public DisconnectionNotification(Map<String, String> stringMap) {
			this.notificationNumber = stringMap.get('Number');
			this.notificationDate = MRO_SRV_SapQueryDecoNotification.parseDate(stringMap.get('Date'));
			this.sapStatus = stringMap.get('Status');
			this.notificationType =  stringMap.get('Type');
			this.consumptionPlaceId = stringMap.get('ConsumptionPlaceId');
			this.returnReason = stringMap.get('MailBounceReason');
			this.notificationId = stringMap.get('NotificationId');
			this.notificationStatus = stringMap.get('NotificationStatus');
			this.errorMessage = stringMap.get('ErrorMessage');
		}
	}

}