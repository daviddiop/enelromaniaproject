public class MRO_LC_PhotovoltaicPanelsWizard extends ApexServiceLibraryCnt {

    private static MRO_QR_Opportunity opportunityQuery = MRO_QR_Opportunity.getInstance();
    private static MRO_QR_OpportunityLineItem oliQuery = MRO_QR_OpportunityLineItem.getInstance();
    private static MRO_QR_OpportunityServiceItem osiQuery = MRO_QR_OpportunityServiceItem.getInstance();
    private static MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static final UserQueries userQuery = UserQueries.getInstance();
    private static MRO_SRV_Opportunity oppService = MRO_SRV_Opportunity.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static PrivacyChangeQueries privacyChangeQuery = PrivacyChangeQueries.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_SRV_Dossier dossierService = MRO_SRV_Dossier.getInstance();
    private static String dossierDraftStatus = constantsSrv .DOSSIER_STATUS_DRAFT;
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_SRV_Case caseService = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();

    public static final String FLOW_CANCELLATION = 'Cancellation';

    public static final String PHOTOVOLTAIC_PANELS = 'PhotovoltaicPanels';
    public static final String ACQUISITION = 'Acquisition';
    public static final String DISCONNECTION = 'Disconnection';

    public with sharing class checkSelectedSupply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            String opportunityId = params.get('opportunityId');


            Supply__c selectedSupply = [
                SELECT Id, Account__c, ServicePoint__c, ServicePoint__r.Name, ServicePoint__r.ENELTEL__c, ContractAccount__r.ContractType__c
                FROM Supply__c
                WHERE Id =: supplyId
            ];

            List<OpportunityServiceItem__c> existing = [
                SELECT Id, ServicePoint__c, ServicePointCode__c
                FROM OpportunityServiceItem__c
                WHERE Opportunity__c =: opportunityId
                    AND (
                    ServicePoint__c =: selectedSupply.ServicePoint__c
                        OR ServicePointCode__c =: selectedSupply.ServicePoint__r.Name
                    )
            ];
            if (!existing.isEmpty()) {
                throw new WrtsException(System.Label.TheSupplyIsAlreadySelectedForCurrentOpportunity);
            }
            String contractType = selectedSupply.ContractAccount__r.ContractType__c;

            Map<String, Object> response = new Map<String, Object>();
            response.put('contractType', contractType);
            response.put('eneltel', selectedSupply.ServicePoint__r.ENELTEL__c);
        return response;
    }
    }

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String opportunityId = params.get('opportunityId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String dossierId = params.get('dossierId');
            String genericRequestId = params.get('genericRequestId');

            User currentUserInfos = userQuery.getCompanyDivisionId(UserInfo.getUserId());

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Account acc = accQuery.findAccount(accountId);

            if (String.isBlank(companyDivisionId)) {
                companyDivisionId = null;
            }

            CustomerInteraction__c customerInteraction;
            List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);
            if (cus != null && !cus.isEmpty()) {
                customerInteraction = cus[0];
            }

            Opportunity opp;
            if (String.isBlank(opportunityId)) {
                opp = oppService.insertOpportunityByCustomerInteraction(customerInteraction, accountId, PHOTOVOLTAIC_PANELS, companyDivisionId);
            } else {
                opp = opportunityQuery.getOpportunityById(opportunityId);
                //String contractId = opp.ContractId != null ? opp.ContractId : '';
                response.put('companyDivisionId', opp.CompanyDivision__c);
                response.put('companyDivisionName', opp.CompanyDivision__r.Name);
                response.put('companySignedId', opp.CompanySignedBy__c);
                response.put('contractIdFromOpp', opp.ContractId);
                response.put('contractType', opp.ContractType__c);
                response.put('contractName', opp.ContractName__c);
                response.put('salesman', opp.Salesman__c);
                response.put('subProcess', opp.SubProcess__c);
                response.put('expirationDate', opp.ExpirationDate__c); response.put('customerSignedDate', opp.ContractSignedDate__c);
                response.put('opportunityCompanyDivisionId', opp.CompanyDivision__c);
                if (String.isNotBlank(opp.ContractId)) {
                    Contract opportunityContract = contractQuery.getById(opp.ContractId);
                    if (opportunityContract != null) {
                        response.put('consumptionConventions', opportunityContract.ConsumptionConventions__c);
                    }
                }
            }

            // add dossier
            Dossier__c dossier;
            Id dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(ACQUISITION).getRecordTypeId();
            if (String.isBlank(dossierId)) {
                List<Dossier__c> existingDossiers = dossierQuery.listDossiersByOpportunity(opp.Id);
                List<Dossier__c> candidateDossiers = new List<Dossier__c>();
                for (Dossier__c existingDossier : existingDossiers) {
                    if (existingDossier.RecordTypeId == dossierRecordTypeId && existingDossier.RequestType__c == 'SwitchIn'
                        && existingDossier.Status__c != 'Closed' && existingDossier.Status__c != 'Canceled') {
                        candidateDossiers.add(existingDossier);
                    }
                }
                if (!candidateDossiers.isEmpty()) {
                    dossier = candidateDossiers[0];
                    dossierId = dossier.Id;
                    response.put('commodity', dossier.Commodity__c);
                    if (candidateDossiers.size() > 1) {
                        System.debug('WARNING: more than 1 Dossier found: '+candidateDossiers);
                    }
                }
                else {
                    if(String.isNotBlank(opportunityId)){
                        List<Dossier__c> dossierList = dossierQuery.listDossiersByOpportunity(opportunityId);
                        if(!dossierList.isEmpty()) {
                            dossier = dossierList.get(0);
                            dossierId = dossier.Id;
                            response.put('commodity', dossier.Commodity__c);
                        }
                    }
                    if(String.isBlank(dossierId)) {
                        dossier = new Dossier__c ();
                        dossier.Account__c = accountId;
                        dossier.CompanyDivision__c = String.isNotBlank(companyDivisionId) ? companyDivisionId : null;
                        dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Acquisition').getRecordTypeId();
                        dossier.Status__c = dossierDraftStatus;
                        dossier.Opportunity__c = opp.Id;
                        dossier.RequestType__c = PHOTOVOLTAIC_PANELS;

                        if (customerInteraction != null) {
                            dossier.CustomerInteraction__c = customerInteraction.Id;
                            dossier.Origin__c = customerInteraction.Interaction__r.Origin__c;
                            dossier.Channel__c = customerInteraction.Interaction__r.Channel__c;
                        }

                        if (genericRequestId != null) {
                            dossier.Parent__c = genericRequestId;
                        }
                        databaseSrv.insertSObject(dossier);
                        dossierId = dossier.Id;
                    }
                }
                if (String.isNotBlank(genericRequestId)){
                    dossierService.updateParentGenericRequest(dossier.Id, genericRequestId);
                }
            } else {
                dossier = dossierQuery.getById(dossierId);
                response.put('commodity', dossier.Commodity__c);
            }

            List<OpportunityLineItem> olis = oliQuery.getOLIsByOpportunityId(opp.Id);
            List<OpportunityServiceItem__c> osis = osiQuery.getOSIsByOpportunityId(opp.Id);

            Supply__c currentSupply = null;
            List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
            if (cases.size() > 0) {
                Case caseRecord = cases[0];
                response.put('caseRecord', caseRecord);
                if (String.isNotBlank(caseRecord.Supply__c)){
                    currentSupply = MRO_QR_Supply.getInstance().getById(caseRecord.Supply__c);
                }
            }

            if (osis.size() > 0){
                Id supplyId = osis[0].ServicePoint__r.CurrentSupply__c;
                if (String.isNotBlank(supplyId)){
                    currentSupply = MRO_QR_Supply.getInstance().getById(supplyId);
                }
            }

            String contractAccountId = '';
            if (!osis.isEmpty()) {
                contractAccountId = osis[0].ContractAccount__c;
                response.put('activationDate',  osis[0].StartDate__c);
                response.put('terminationDate', osis[0].EndDate__c);
                response.put('eneltel', osis[0].ServicePoint__r.ENELTEL__c);
            }

            if (!olis.isEmpty()) {
                String productName = olis[0].Product2.Name;
                dossier.PrintBundleSelection__c = productName;
                databaseSrv.updateSObject(dossier);
            }

            // This is to retrieve the RecordType.DeveloperName
            dossier = dossierQuery.getById(dossier.Id);

            response.put('opportunityId', opp.Id);
            response.put('opportunity', opp);
            response.put('opportunityLineItems', olis);
            response.put('opportunityServiceItems', osis);
            response.put('dossierId', dossierId);
            response.put('dossier', dossier);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('contractAccountId', contractAccountId);
            response.put('stage', opp.StageName);
            response.put('user', currentUserInfos);
            response.put('currentFlow', opp.SubProcess__c);
            response.put('useBit2WinCart', constantsSrv.IS_BIT2WIN_CPQ_ENABLED);
            response.put('currentSupply', currentSupply);
            response.put('error', false);

            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('setChannelAndOrigin');
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');

            System.debug('origin: ' + origin);
            System.debug('dossierId: ' + dossierId);
            System.debug('channel: ' + channel);

            oppService.setChannelAndOrigin(opportunityId, dossierId, origin, channel);
            return true;
        }
    }

    public with sharing class setSubProcessAndExpirationDate extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String subProcess =  params.get('subProcess');
            Date expirationDate = (Date) JSON.deserialize(params.get('expirationDate'), Date.class);
            oppService.setSubProcessAndExpirationDate(opportunityId,subProcess,expirationDate);
            return true;
        }
    }

    public with sharing class updateCurrentFlow extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String currentFlow = params.get('currentFlow');
            Id accountId = params.get('accountId');
            Id opportunityId = params.get('opportunityId');
            Id dossierId = params.get('dossierId');

            Opportunity opportunity = new Opportunity();
            opportunity.Id = opportunityId;
            opportunity.SubProcess__c = currentFlow;
            databaseSrv.updateSObject(opportunity);

            Id recordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(ACQUISITION).getRecordTypeId();
            if (currentFlow == FLOW_CANCELLATION) {
                recordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(DISCONNECTION).getRecordTypeId();
            }

            Dossier__c dossier = new Dossier__c(
                Id = dossierId,
                RecordTypeId = recordTypeId,
                SubProcess__c = currentFlow
            );
            databaseSrv.updateSObject(dossier);

            if (currentFlow == 'Acquisition') {
                List<Case> openCaseList = caseQuery.listOpenCasesByAccountAndRecordTypeNameandSubProcess(accountId, PHOTOVOLTAIC_PANELS, currentFlow);
                response.put('hasOpenCases', openCaseList.size() > 0);
            }

            return response;
        }
    }

    public with sharing class updateDates extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            Id osiId = params.get('osiId');
            Date activationDate = Date.valueOf(params.get('activationDate'));
            Date terminationDate = Date.valueOf(params.get('terminationDate'));
            OpportunityServiceItem__c osi = osiQuery.getById(osiId);
            osi.StartDate__c = activationDate;
            osi.EndDate__c = terminationDate;

            try {
                databaseSrv.updateSObject(osi);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateSupply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Id supplyId = params.get('supplyId');
            String status = params.get('status');
            Map<String, Object> response = new Map<String, Object>();

            Supply__c supply = MRO_QR_Supply.getInstance().getById(supplyId);
            if (!String.isNotBlank(status)){
                throw new WrtsException('Status is empty');
            }

            supply.Status__c = status;
            try {
                databaseSrv.updateSObject(supply);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String currentFlow = params.get('currentFlow');
            String selectedCommodity = params.get('selectedCommodity');
            String dossierId = params.get('dossierId');
            String privacyChangeId = params.get('privacyChangeId');
            String productName = params.get('productName');
            String productPrice = params.get('productPrice');
            String stage = params.get('stage');
            Opportunity opps;
            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
//            try {
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.StageName = stage;

                System.debug('PSW: opp.Id =' + opp.Id);
                //opp.ContractId = String.isBlank(contractId) ? null : contractId;
                databaseSrv.upsertSObject(opp);

                if (stage == 'Closed Won') {
                    opps = opportunityQuery.getOpportunityById(opportunityId);
                    Contract myContract = opps.Contract != null ? opps.Contract : new Contract();
                    if (opps.ContractSignedDate__c != null) {
                        myContract.CustomerSignedDate = opps.ContractSignedDate__c;
                    }

                    System.debug('currentFlow');
                    System.debug(currentFlow);
                    Map<String, String> opportunityParams = new Map<String, String>();
                    opportunityParams.put('currentFlow', currentFlow);
                    if (currentFlow != FLOW_CANCELLATION) {
                        opportunityParams.put('productName', productName);
                        opportunityParams.put('productPrice', productPrice);
                        opportunityParams.put('selectedCommodity', selectedCommodity);
                        opportunityParams.put('assetId', params.get('assetId'));
                    }
                    MRO_SRV_Opportunity.generateAcquisitionChain(opp.Id, PHOTOVOLTAIC_PANELS, myContract, privacyChangeId, dossierId, opportunityParams);

                    if (String.isNotBlank(dossierId)){
                        caseService.applyAutomaticTransitionOnDossierAndCases(dossierId);
                    }
                }

                response.put('opportunityId', opp.Id);
                response.put('error', false);
//            } catch (Exception ex) {
//                //Database.rollback(sp);
//                response.put('error', true);
//                response.put('errorMsg', ex.getMessage());
//                response.put('errorTrace', ex.getStackTraceString());
//            }
            return response;
        }
    }

    public with sharing class updateOsiList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            UpdateOsiListInput updateOsiListParams = (UpdateOsiListInput) JSON.deserialize(jsonInput, UpdateOsiListInput.class);

            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
                for (OpportunityServiceItem__c osi : updateOsiListParams.opportunityServiceItems) {
                    osiList.add(new OpportunityServiceItem__c(Id = osi.Id, ContractAccount__c = updateOsiListParams.contractAccountId));
                }
                databaseSrv.updateSObject(osiList);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class checkOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String osiId = params.get('osiId');

            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            try {
                OpportunityServiceItem__c osi = osiQuery.getById(osiId);
                response.put('opportunityServiceItem', osi);
                Opportunity opp = osi.Opportunity__r;
                ServicePoint__c point = MRO_SRV_Opportunity.instantiateServicePoint(osi);
                databaseSrv.upsertSObject(point);

                Supply__c supply = MRO_SRV_Opportunity.instantiateSupply(opp, osi);
                supply.ServicePoint__c = point.Id;
                databaseSrv.insertSObject(supply);

                Case tkt = MRO_SRV_Opportunity.instantiateCase(opp, osi, PHOTOVOLTAIC_PANELS);
                tkt.Supply__c = supply.Id;
                databaseSrv.insertSObject(tkt);

                response.put('opportunityServiceItem', osi);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            Database.rollback(sp);
            return response;
        }
    }

    public with sharing class createAsset extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String assetId = params.get('assetId');

            Opportunity opp = opportunityQuery.getById(opportunityId);

            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                assetId = MRO_SRV_Opportunity.insertAsset(opp, assetId);
                response.put('error', false);
                response.put('assetId', assetId);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class linkOliToOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            LinkToOsiInput linkOliToOsiParams = (LinkToOsiInput) JSON.deserialize(jsonInput, LinkToOsiInput.class);

            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
                for (OpportunityServiceItem__c osi : linkOliToOsiParams.opportunityServiceItems) {
                    osiList.add(new OpportunityServiceItem__c(Id = osi.Id, Product__c = linkOliToOsiParams.oli.Product2Id));
                }
                databaseSrv.updateSObject(osiList);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    // Classes for deserialize JSON into Objects
    public class UpdateOsiListInput {
        @AuraEnabled
        public List<OpportunityServiceItem__c> opportunityServiceItems { get; set; }
        @AuraEnabled
        public String contractAccountId { get; set; }
    }

    public class LinkToOsiInput {
        @AuraEnabled
        List<OpportunityServiceItem__c> opportunityServiceItems { get; set; }
        @AuraEnabled
        OpportunityLineItem oli { get; set; }
    }

    public with sharing class updateCompanyDivisionInOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String companyDIvisionId = params.get('companyDivisionId');

            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
            try {
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.companyDivision__c = companyDIvisionId;
                databaseSrv.upsertSObject(opp);
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateContractAndContractSignedDateOnOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String customerSignedDate = params.get('customerSignedDate');
            String contractId = params.get('contractId');
            String contractType = params.get('contractType');

            Opportunity opportunityToUpdate = new Opportunity();
            opportunityToUpdate.Id = opportunityId;
            opportunityToUpdate.ContractId = String.isBlank(contractId) ? null : contractId;
            opportunityToUpdate.ContractSignedDate__c = String.isBlank(customerSignedDate) ? null : Date.valueOf(customerSignedDate);
            opportunityToUpdate.ContractType__c = contractType;
            databaseSrv.upsertSObject(opportunityToUpdate);

            Map<String, Object> response = new Map<String, Object>();
            response.put('opportunityId', opportunityToUpdate.Id);
            return response;
        }
    }

    public class RequestParams {
        @AuraEnabled
        public Id supplyId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
    }

    public with sharing class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams createCaseParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);
            if (createCaseParams.supplyId == null) {
                response.put('error', true);
                response.put('errorMsg', 'No Supplies Selected');
                return response;
            }
            Supply__c supply = supplyQuery.getById(createCaseParams.supplyId);

            Map<String, String> caseRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('PhotovoltaicPanels');
            Account acc = accQuery.findAccount(supply.Account__c);
            Case caseRecord = new Case(
                Supply__c = supply.Id,
                CompanyDivision__c = supply.CompanyDivision__c,
                AccountId = supply.Account__c,
                AccountName__c = acc.Name,
                RecordTypeId = (String) caseRecordTypes.get('PhotovoltaicPanels'),
                Dossier__c = createCaseParams.dossierId,
                Status = 'Draft',
                Origin = createCaseParams.originSelected,
                Channel__c = createCaseParams.channelSelected,
                Phase__c = 'RE010',
                Contract__c = supply.Contract__c
            );
            List<Case> cases = caseService.insertCaseRecords(new List<Case>{caseRecord}, new List<Case>());
            response.put('caseRecord', cases[0]);
            return response;
        }
    }

}