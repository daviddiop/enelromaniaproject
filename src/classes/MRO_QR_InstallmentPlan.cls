/**
 * Created by Movsar Bekaev on 07/23/2023.
 */

public inherited sharing class MRO_QR_InstallmentPlan {

    public static MRO_QR_InstallmentPlan getInstance() {
        return new MRO_QR_InstallmentPlan();
    }

    public InstallmentPlan__c getByAssetId(Id assetId) {
        List<InstallmentPlan__c> installmentPlans = [
                SELECT Id, Case__c, StartDate__c, NumberOfInstallments__c, Plan__c
                FROM InstallmentPlan__c
                WHERE Asset__c = :assetId
        ];

        if (installmentPlans.size() > 0) {
            return installmentPlans.get(0);
        }
        return null;
    }

    public InstallmentPlan__c getById(Id Id) {
        List<InstallmentPlan__c> installmentPlans = [
            SELECT Id, Asset__c, Case__c, StartDate__c, NumberOfInstallments__c, Plan__c
            FROM InstallmentPlan__c
            WHERE Id = :Id
        ];

        if (installmentPlans.size() > 0) {
            return installmentPlans.get(0);
        }
        return null;
    }

    public List<InstallmentPlan__c> listByCaseId(Id caseId) {
        return [
                SELECT Id, StartDate__c, CreatedDate, Case__c, Plan__c, TotalAmount__c, NumberOfInstallments__c, BillingFrequency__c, SelfReadingDay__c
                FROM InstallmentPlan__c
                WHERE Case__c = :caseId
        ];
    }

    public List<InstallmentPlan__c> listByAccountId(Id customerId) {
        return [
                SELECT Id, StartDate__c, CreatedDate, Case__c, Plan__c
                FROM InstallmentPlan__c
                WHERE Customer__c = :customerId
        ];
    }
}