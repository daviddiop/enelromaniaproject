/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 26.11.20.
 */

public with sharing class MRO_LC_IvrGaz extends ApexServiceLibraryCnt {

    public with sharing class listIndex extends AuraCallable {
        public override Object perform(final String jsonInput) {
            IvrGazRequestParam params = (IvrGazRequestParam) JSON.deserialize(jsonInput, IvrGazRequestParam.class);
            if (String.isEmpty(params.startDate) || String.isEmpty(params.endDate) || String.isEmpty(params.enelTel)) {
                throw new WrtsException('Missing request parameters');
            }

            if (MRO_SRV_SapQueryIvrGazCallOut.checkSapEnabled()) {
                return MRO_SRV_SapQueryIvrGazCallOut.getInstance().getList(params);
            }
            return listDummyIndex(params);
        }
    }

    public static IvrGazResponse listDummyIndex(IvrGazRequestParam params) {
        IvrGazResponse ivrGazResponse = new IvrGazResponse();

        for (Integer i = 0; i < 3; i++) {
            ivrGazResponse.indexes.add(new Index(
                    'SP100000' + i,
                    Date.today().addDays((i + 1)),
                    i * 100
            ));
        }
        return ivrGazResponse;
    }

    public class IvrGazRequestParam {
        @AuraEnabled
        public String enelTel { get; set; }
        @AuraEnabled
        public String startDate { get; set; }
        @AuraEnabled
        public String endDate { get; set; }

        public IvrGazRequestParam() {

        }
    }

    public class IvrGazResponse {
        @AuraEnabled
        public List<Index> indexes { get; set; }

        public IvrGazResponse() {
            this.indexes = new List<Index>();
        }
    }

    public class Index {
        @AuraEnabled
        public String meterSerialNumber { get; set; }
        @AuraEnabled
        public Date readingDate { get; set; }
        @AuraEnabled
        public Double index { get; set; }

        public Index(String meterSerialNumber, Date readingDate, Double index) {
            this.meterSerialNumber = meterSerialNumber;
            this.readingDate = readingDate;
            this.index = index;
        }

        public Index(Map<String, String> stringMap) {
            this.meterSerialNumber = stringMap.get('MeterSerialNumber');
            this.readingDate = MRO_SRV_SapQueryFisaCallOut.parseDate(stringMap.get('ReadingDate'));
            this.index = MRO_SRV_SapQueryFisaCallOut.parseDouble(stringMap.get('Index'));
        }
    }
}