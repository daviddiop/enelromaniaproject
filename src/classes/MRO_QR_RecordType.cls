/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   giu 25, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_RecordType {

    public static MRO_QR_RecordType getInstance() {
        return new MRO_QR_RecordType();
    }

    public Map<String, RecordType> getRecordTypesByObjectAndDeveloperNames(String objectTypeName, Set<String> developerNames) {
        List<RecordType> recordTypes = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :objectTypeName AND DeveloperName IN :developerNames];
        Map<String, RecordType> recordTypesMap = new Map<String, RecordType>();
        for (RecordType rt : recordTypes) {
            recordTypesMap.put(rt.DeveloperName, rt);
        }
        return recordTypesMap;
    }
}