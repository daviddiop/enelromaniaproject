/**
 * Created by goudiaby on 01/04/2019.
 */

public with sharing class DocumentItemQueries {
    public static DocumentItemQueries getInstance() {
        return new DocumentItemQueries();
    }

    public List<DocumentItem__c> listDocumentItemsByDocumentTypeId(Set<String> documentTypesIds) {
        return [
                SELECT Id,Name,DocumentType__c,ValidatableDocumentField__c,Mandatory__c,
                        DocumentType__r.Name
                FROM DocumentItem__c
                WHERE DocumentType__c IN :documentTypesIds
        ];
    }
}