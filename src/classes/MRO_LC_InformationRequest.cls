/**
 * Created by Octavian on 4/9/2020.
 */

public with sharing class MRO_LC_InformationRequest extends ApexServiceLibraryCnt {
    private static final String INFORMATION_REQUEST = 'InformationRequest';

    static MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_SRV_Supply supplySrv = MRO_SRV_Supply.getInstance();

    static String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(INFORMATION_REQUEST).getRecordTypeId();
    static String caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(INFORMATION_REQUEST).getRecordTypeId();
    static String caseNewStatus = constantsUtl.CASE_STATUS_NEW;

    public class init extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String genericRequestDossierId = params.get('genericRequestDossierId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Map<String, Object> response = new Map<String, Object>();
            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, null, dossierRecordTypeId, INFORMATION_REQUEST);
            if(!String.isBlank(genericRequestDossierId)){
                dossierSrv.updateParentGenericRequest(dossier.Id, genericRequestDossierId);
            }
            List<Case> caseList = caseQuery.getCasesByDossierId(dossier.Id);
            if(!caseList.isEmpty()) {
                Case informationRequestCase = caseList.get(0);
                response.put('case', informationRequestCase);
            }
            response.put('dossier', dossier);
            response.put('informationRequestRecordTypeId', caseRecordTypeId);
            System.debug('### response '+JSON.serializePretty(response));
            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String cancelReason = params.get('cancelReason');
            String detailsReason = params.get('detailsReason');
            List<Case> caseList = caseQuery.getCasesByDossierId(dossierId);

            for (Case caseRecord : caseList) {
                caseRecord.CancellationReason__c = cancelReason;
                caseRecord.CancellationDetails__c = detailsReason;
            }

            Savepoint sp = Database.setSavepoint();
            try {
                caseSrv.setCanceledOnCaseAndDossier(caseList, dossierId);
                response.put('error', false);
            }catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return new Map<String, Object>();
        }
    }

    public class SaveDraftDossierAndCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Savepoint sp = Database.setSavepoint();
            String dossierId = params.get('dossierId');
            String originSelected = params.get('originSelected');
            String channelSelected = params.get('channelSelected');
            List<Case> caseList = new List<Case>();

            response.put('error', false);
            try {
                dossierSrv.updateDossierChannel(dossierId, channelSelected, originSelected);

                if (params.get('caseList').length() > 0) {
                    caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
                    caseSrv.saveDraftCaseForInformationRequest(caseList, originSelected, channelSelected);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class UpdateDossierAndCaseStatus extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            Savepoint sp = Database.setSavepoint();
            try {
                dossierSrv.updateDossierCompanyDivision(dossierId, null);
                Date slaExpirationDate = MRO_UTL_Date.addWorkingDays(Date.today(), 15);
                Dossier__c updatedDossier = new Dossier__c(
                        Id = dossierId,
                        Origin__c = originSelected,
                        Channel__c = channelSelected,
                        SLAExpirationDate__c = slaExpirationDate
                );
                databaseSrv.upsertSObject(updatedDossier);

                List<Case> dossierCases = caseQuery.getCasesByDossierId(dossierId);

                if (dossierCases.size() > 0) {
                    Case informationRequestCase = dossierCases[0];
                    informationRequestCase.SLAExpirationDate__c = slaExpirationDate;
                    informationRequestCase.Status = caseNewStatus;

                    databaseSrv.upsertSObject(informationRequestCase);
                    caseSrv.applyAutomaticTransitionOnDossierAndCases(dossierId);
                }
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return new Map<String, Object>();
        }
    }

    public with sharing class SetChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            Map<String, String> params = asMap(jsonInput);

            dossierSrv.updateDossierChannel(params.get('dossierId'), params.get('selectedChannel'), params.get('selectedOrigin'));

            return response;
        }
    }

    public inherited sharing class CheckSelectedSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Id> selectedSupplyIds = (List<Id>) JSON.deserialize(params.get('selectedSupplyIds'), List<Id>.class);
            if (selectedSupplyIds.isEmpty()) {
                throw new WrtsException('Supplies' + ' - ' + System.Label.MissingId);
            }
            try {
                List<Supply__c>  supplies = supplyQuery.getSuppliesByIds(selectedSupplyIds);
                List<Account> distributors = new List<Account>();
                if(supplies.size()>0 && supplies[0].ServicePoint__r.Distributor__c != null){
                    Set<Id> distIds = new Set<Id>();
                    distIds.add(supplies[0].ServicePoint__r.Distributor__c);
                    distributors = accountQuery.listDistributorsByIds(distIds);
                }
                if(distributors.size()>0){
                    response.put('IsDisCoENEL__c',distributors[0].IsDisCoENEL__c);
                    response.put('DistributorProvince', distributors[0].ResidentialProvince__c);
                }
                response.put('supplies',supplies);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class CreateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Supply__c> supplies = (List<Supply__c>) JSON.deserialize(params.get('supplies'), List<Supply__c>.class);
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String selectedChannel = params.get('channelSelected');
            String selectedOrigin = params.get('originSelected');
            List<Case> newCases = new List<Case>();
            try {
                if (!supplies.isEmpty() && String.isNotBlank(accountId) && String.isNotBlank(dossierId) && String.isNotBlank(selectedOrigin) && String.isNotBlank(selectedChannel)) {
                    for (Supply__c supply : supplies) {
                        Case complaintCase = caseSrv.createCaseForInformationRequest(supply, accountId, dossierId, caseRecordTypeId, selectedOrigin, selectedChannel);
                        newCases.add(complaintCase);
                    }
                    Account account = accountQuery.findAccount(accountId);
                    List<Case> newCasesWithAccountInfo = caseSrv.updateCasesByAccount(newCases, account);
                    List<Case> updatedCases = caseSrv.insertCaseRecords(newCasesWithAccountInfo, caseList);
                    response.put('cases', updatedCases);
                    response.put('error', false);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class UpdateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id caseId = params.get('caseId');
            String notes = params.get('notes');
            String subType = params.get('subType');
            Case caseIR = caseQuery.getById(caseId);
            caseIR.CaseTypeCode__c = subType.split(' ')[0];
            caseIR.CustomerNotes__c = notes;

            try {
                databaseSrv.updateSObject(caseIR);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class SetSupplyBillingProfile extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Id> selectedSupplyIds = (List<Id>) JSON.deserialize(params.get('selectedSupplyIds'), List<Id>.class);
            if (selectedSupplyIds.isEmpty()) {
                throw new WrtsException('Supplies' + ' - ' + System.Label.MissingId);
            }
            String supplyId = selectedSupplyIds[0];
            List<Supply__c> updatedSupplies = new List<Supply__c>();

            try {
                if (String.isNotBlank(supplyId)) {
                    Supply__c selectedSupply = supplyQuery.getById(supplyId);
                    Id contractBillingProfile = selectedSupply.ContractAccount__r.BillingProfile__c;
                    if (String.isNotBlank(contractBillingProfile)) {
                        selectedSupply.BillingProfile__c = contractBillingProfile;
                        updatedSupplies.add(selectedSupply);
                        supplySrv.updateSupplies(updatedSupplies);
                    }
                    response.put('updatedSupplies', updatedSupplies);
                    response.put('error', false);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class GetAvailableSubTypes extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String informationRequestType = params.get('informationRequestType');
            String supplyType = params.get('supplyType');

            try {
                List<CaseSubTypeDependency__mdt> availableSubTypes = [SELECT MasterLabel, HasInvoiceSelection__c, HasBillingAdjustment__c, DisableInvoiceMarking__c, Label__c
                FROM CaseSubTypeDependency__mdt WHERE CaseType__c = :informationRequestType AND SupplyType__c = :supplyType AND CaseRecordType__c = 'InformationRequest' ORDER BY MasterLabel];
                response.put('availableSubTypes', availableSubTypes);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public inherited sharing class GetAvailableReasons extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String caseSubType = params.get('caseSubType');
            try {
                List<CaseReasonDependency__mdt> availableSubTypes = [SELECT MasterLabel, Label__c
                FROM CaseReasonDependency__mdt WHERE CaseSubType__c = :caseSubType ORDER BY MasterLabel];
                response.put('availableReasons', availableSubTypes);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

}