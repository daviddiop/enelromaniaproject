/**
 * @author  Stefano Porcari
 * @since   Dec 24, 2019
 * @desc    Service class for ValidatableDocument__c object
 * 
 */
public with sharing class MRO_SRV_ValidatableDocument {
    private static MRO_QR_ValidatableDocument validatableDocumentQuery = MRO_QR_ValidatableDocument.getInstance();
    private static final MRO_QR_FileMetadata fileMetadataQueries = MRO_QR_FileMetadata.getInstance();
    private static final MRO_QR_Dossier dossierQueries = MRO_QR_Dossier.getInstance();
    private static final MRO_QR_Activity activityQueries = MRO_QR_Activity.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();
    private static final MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_UTL_Constants costantsUtl = new MRO_UTL_Constants();

    public static MRO_SRV_ValidatableDocument getInstance() {
        return new MRO_SRV_ValidatableDocument();
    }

    /**
     * Get a Map of Validatable documents object with Document Bundle item key from Dossier
     *
     * @param parentRecordId
     *
     * @return Map Validatable document
     */
    public Map<String, ValidatableDocument__c> getMapDocumentItemBundleValidatableDoc(Id parentRecordId) {
        String sObjectType = parentRecordId.getSobjectType().getDescribe().getName();
        Map<String, ValidatableDocument__c> mapDocumentItemBundleValidatableDoc = new Map<String, ValidatableDocument__c>();
        List<ValidatableDocument__c> validatableDocuments;
        switch on sObjectType {
            when 'Case' {
                validatableDocuments = validatableDocumentQuery.findByCaseId(parentRecordId);
            }
            when 'Dossier__c' {
                validatableDocuments = validatableDocumentQuery.findByDossierId(parentRecordId);
            }
            when else {
                throw new WrtsException('Unsupported parent object for a Validatable Document');
            }
        }
        for (ValidatableDocument__c validatableDocument : validatableDocuments) {
            if (validatableDocument.DocumentBundleItem__c != null) {
                mapDocumentItemBundleValidatableDoc.put(
                        String.valueOf(validatableDocument.DocumentBundleItem__c), validatableDocument
                );
            }
        }
        return mapDocumentItemBundleValidatableDoc;
    }

    public Boolean hasNonValidatableDocument(Id parentRecordId) {
        String sObjectType = parentRecordId.getSobjectType().getDescribe().getName();
        List<ValidatableDocument__c> nonValidatableDocuments;
        switch on sObjectType {
            when 'Case' {
                nonValidatableDocuments = validatableDocumentQuery.findNonValidatableByCaseId(parentRecordId);
            }
            when 'Dossier__c' {
                nonValidatableDocuments = validatableDocumentQuery.findNonValidatableByDossierId(parentRecordId);
            }
            when else {
                throw new WrtsException('Unsupported parent object for a Validatable Document');
            }
        }
        return !nonValidatableDocuments.isEmpty();
    }

    public List<MRO_UTL_DocumentValidationChecks.Result> validateDocument(ValidatableDocument__c validatableDocument, Id fileMetadataId, List<MRO_SRV_DocumentItem.DocumentItemDTO> documentItemsDTO, Boolean isConfirmed) {
        Savepoint sp = Database.setSavepoint();
        try {
            List<MRO_UTL_DocumentValidationChecks.Result> results = new List<MRO_UTL_DocumentValidationChecks.Result>();
            ValidatableDocument__c validatableDocumentOld = validatableDocumentQuery.getById(validatableDocument.Id);
            validatableDocument.Dossier__c = validatableDocumentOld.Dossier__c;
            validatableDocument.Case__c = validatableDocumentOld.Case__c;
            validatableDocument.ValidationActivity__c = validatableDocumentOld.ValidationActivity__c;
            validatableDocument.DocumentBundleItem__c = validatableDocumentOld.DocumentBundleItem__c;
            validatableDocument.FileMetadata__c = fileMetadataId;

            Boolean isValid = true;

            if (documentItemsDTO != null && !documentItemsDTO.isEmpty()) {
                MRO_UTL_DocumentValidationChecks checks = new MRO_UTL_DocumentValidationChecks(validatableDocument);
                for (MRO_SRV_DocumentItem.DocumentItemDTO documentItem : documentItemsDTO) {
                    if (!String.isBlank(documentItem.customValidationLevel) && !String.isBlank(documentItem.customValidationMethod)) {
                        if (documentItem.customValidationLevel == 'Error' || (documentItem.customValidationLevel == 'Warning' && !isConfirmed)) {
                            MRO_UTL_DocumentValidationChecks.Result res = (MRO_UTL_DocumentValidationChecks.Result) checks.call(documentItem.customValidationMethod,
                                    new Map<String, Object>{
                                            'isBlocking' => (documentItem.customValidationLevel == 'Error')
                                    });
                            if (res.code != MRO_UTL_DocumentValidationChecks.ResultCode.PASS) {
                                isValid = false;
                            }
                            results.add(res);
                        }
                    }
                }
            }

            if (isValid) {
                databaseSrv.updateSObject(validatableDocument);
                if (documentItemsDTO != null && !documentItemsDTO.isEmpty()) {
                    Map<String, SObject> parentRecords = this.writeParentRecordFields(validatableDocument, documentItemsDTO);
                    databaseSrv.updateSObject(parentRecords.values());
                }

                if (results.isEmpty()) {
                    results.add(new MRO_UTL_DocumentValidationChecks.Result(MRO_UTL_DocumentValidationChecks.ResultCode.PASS, null));
                }
            }

            return results;
        } catch (Exception e) {
            Database.rollback(sp);
            throw e;
        }
    }

    private Map<String, SObject> writeParentRecordFields(ValidatableDocument__c validatableDocument, List<MRO_SRV_DocumentItem.DocumentItemDTO> documentItemsDTO) {
        Map<String, SObject> parentRecords = new Map<String, SObject>();
        for (MRO_SRV_DocumentItem.DocumentItemDTO docItem : documentItemsDTO) {
            if (!String.isBlank(docItem.validatableDocumentParentField)) {
                List<String> pathParts = docItem.validatableDocumentParentField.split('\\.', 2);
                String parentObjectName = pathParts[0];
                String parentFieldName = pathParts[1];
                System.debug('### Parent Object Name: ' + parentObjectName + ' - Parent Field Name: ' + parentFieldName);
                String parentRelationField = parentObjectName.endsWith('__c') ? parentObjectName : parentObjectName + '__c';
                if (validatableDocument.get(parentRelationField) != null) {
                    SObject parentRecord = parentRecords.get(parentObjectName);
                    if (parentRecord == null) {
                        parentRecord = Schema.getGlobalDescribe().get(parentObjectName).newSObject();
                        parentRecord.Id = (Id) validatableDocument.get(parentRelationField);
                        parentRecords.put(parentObjectName, parentRecord);
                    }
                    parentRecord.put(parentFieldName, validatableDocument.get(docItem.validatableDocumentField));
                }
            }
        }
        return parentRecords;
    }

    public void commitCustomerSignedDates(Set<Id> validatableDocumentIds) {
        if (!validatableDocumentIds.isEmpty()) {
            Map<Id, ValidatableDocument__c> customerSignedDateDocuments = new Map<Id, ValidatableDocument__c>(validatableDocumentQuery.getByIds(validatableDocumentIds));
            Map<Id, Opportunity> opportunitiesToUpdate = new Map<Id, Opportunity>();
            Map<Id, Contract> contractsToUpdate = new Map<Id, Contract>();
            for (ValidatableDocument__c vd : customerSignedDateDocuments.values()) {
                if (vd.Dossier__r != null && vd.Dossier__r.Opportunity__r != null && vd.Dossier__r.Opportunity__r.ContractSignedDate__c == null) {
                    Opportunity opp = vd.Dossier__r.Opportunity__r;
                    opp.ContractSignedDate__c = vd.CustomerSignedDate__c;
                    opp.StageName = costantsUtl.OPPORTUNITY_STAGE_CLOSED_WON;
                    opportunitiesToUpdate.put(opp.Id, opp);
                    if (opp.Contract != null && opp.Contract.CustomerSignedDate == null) {
                        Contract cntr = opp.Contract;
                        cntr.CustomerSignedDate = vd.CustomerSignedDate__c;
                        if (cntr.Status != costantsUtl.CONTRACT_STATUS_ACTIVATED && cntr.Status != costantsUtl.CONTRACT_STATUS_TERMINATED) {
                            cntr.Status = costantsUtl.CONTRACT_STATUS_SIGNED;
                        }
                        contractsToUpdate.put(cntr.Id, cntr);
                    }
                }
            }
            databaseSrv.updateSObject(opportunitiesToUpdate.values());
            databaseSrv.updateSObject(contractsToUpdate.values());
        }
    }

    /**
     * @description Method that allows to close Document validation (completed or cancelled) based on acceptance response
     * @param dossierId Dossier request
     * @param accepted if Customer accept the request or not
     * @param acceptanceDate Date acceptance timestamp
     *
     * @return Map Boolean success - message
     */
    public Map<String, Object> contractAcceptanceForEnelEasy(String dossierId, Boolean accepted, Date acceptanceDate) {
        Boolean success = true;
        String message;
        Set<Id> validationActivityIds = new Set<Id>();
        Set<String> toBeArchiveDMS = new Set<String>();
        Dossier__c dossier = dossierQueries.getById(dossierId);
        List<ValidatableDocument__c> validatableDocuments = validatableDocumentQuery.listUncheckedValidatableDocumentForDossier(dossierId);
        Boolean inEnelArea = false;
        Savepoint sp;
        Map<String, String> correspondingDocumentTypeDisco = new Map<String, String>{
                'DISCO20104' => 'CI',
                'DISCO20102' => 'ACT_PROPRIETAR'
        };
        try {
            if (!dossier.Cases__r.isEmpty()) {
                inEnelArea = dossier.Cases__r[0].IsDisCoENEL__c;
            }
            if (validatableDocuments.isEmpty()) {
                throw new WrtsException('Validatable documents to be accepted not found');
            } else {
                validationActivityIds.add(validatableDocuments[0].ValidationActivity__c);
            }
            if (accepted) {
                if (acceptanceDate == null) {
                    acceptanceDate = Date.today();
                }
                Set<String> contractDocumentTypeCodes = new Set<String>{
                        'CTR_ELEC', 'CTR_GAZ', 'CTR_VAS'
                };
                //Map All FileMetadata - TypeCode
                Map<String, FileMetadata__c> allFMMap = fileMetadataQueries.mapDocumentTypeCodes(dossierId);
                Map<Id, ValidatableDocument__c> validatableDocumentsToUpdate = new Map<Id, ValidatableDocument__c>();
                Set<Id> fileMetadataIdsUploaded = new Set<Id>();
                Boolean closeValidationActivity = true;
                for (ValidatableDocument__c vd : validatableDocuments) {
                    String docTypeCode = vd.DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c;
                    if (allFMMap.containsKey(docTypeCode)) {
                        //Set Customer Signed Date on Validatable Document of the Contract
                        if (contractDocumentTypeCodes.contains(docTypeCode)) {
                            vd.CustomerSignedDate__c = acceptanceDate;
                        }
                        vd.FileMetadata__c = allFMMap.get(docTypeCode).Id;
                        if (allFMMap.get(docTypeCode).PrintAction__c == null) {
                            System.debug('fileMetadataIdsUploaded ' + fileMetadataIdsUploaded);
                            fileMetadataIdsUploaded.add(allFMMap.get(docTypeCode).Id);
                        }
                        validatableDocumentsToUpdate.put(vd.Id, vd);
                    } else if (correspondingDocumentTypeDisco.containsKey(docTypeCode)) {
                        //Check if we have document type identified as DISCO file
                        if (allFMMap.containsKey(correspondingDocumentTypeDisco.get(docTypeCode))) {
                            vd.FileMetadata__c = allFMMap.get(correspondingDocumentTypeDisco.get(docTypeCode)).Id;
                            validatableDocumentsToUpdate.put(vd.Id, vd);
                        }
                        if (vd.DocumentBundleItem__r.ArchiveOnDisCo__c && inEnelArea) {
                            toBeArchiveDMS.add(vd.Id);
                            closeValidationActivity = false;
                        }
                    }
                }

                if (!toBeArchiveDMS.isEmpty()) {
                    for (Id discoDocument : toBeArchiveDMS) {
                        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse archivingResponse = executeArchiveDMS(discoDocument);
                        System.debug('archivingResponse: ' + archivingResponse);
                    }
                }
                sp = Database.setSavepoint();
                if (!validatableDocumentsToUpdate.isEmpty()) {
                    update validatableDocumentsToUpdate.values();
                }
                //Close all necessary activities documents and make transition
                if (closeValidationActivity){
                    closeValidationActivities(validationActivityIds, dossier, fileMetadataIdsUploaded);
                }
            } else {
                Map<Id, wrts_prcgvr__Activity__c> activitiesToCancel = new Map<Id, wrts_prcgvr__Activity__c>();
                for (ValidatableDocument__c vd : validatableDocuments) {
                    if (!activitiesToCancel.containsKey(vd.ValidationActivity__c)) {
                        activitiesToCancel.put(vd.ValidationActivity__c, new wrts_prcgvr__Activity__c(
                                Id = vd.ValidationActivity__c,
                                wrts_prcgvr__Status__c = CONSTANTS.ACTIVITY_STATUS_CANCELLED
                        ));
                    }
                }
                if (!activitiesToCancel.isEmpty()) {
                    databaseSrv.updateSObject(activitiesToCancel.values());
                }

                Map<String, Object> cancelResult = dossierSrv.cancelDossierAndCases(dossier, 'Customer request', '');
                if (!String.isBlank((String) cancelResult.get('error'))) {
                    success = false;
                    message = (String) cancelResult.get('error');
                }
            }
        } catch (Exception e) {
            if (sp != null) Database.rollback(sp);
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            success = false;
            message = e.getMessage();
        }
        return new Map<String, Object>{
                'success' => success,
                'message' => message
        };
    }

    /**
    * @author  Baba Goudiaby
    * @description Closed Validation Documents
    *
    * @date 15/11/2020
    * @param validationActivityIds Document Validation Activity Ids
    * @param dossier Dossier
    * @param fileMetadataIdsUploaded fileMetadataUploaded

    */
    public void closeValidationActivities(Set<Id> validationActivityIds, Dossier__c dossier, Set<Id> fileMetadataIdsUploaded) {
        List<wrts_prcgvr__Activity__c> activitiesToClose = new List<wrts_prcgvr__Activity__c>();
        if (!validationActivityIds.isEmpty() && fileMetadataIdsUploaded != null && !fileMetadataIdsUploaded.isEmpty()) {
            List<wrts_prcgvr__Activity__c> fileCheckActivities = activityQueries.getOpenFileCheckActivitiesByParentIds(fileMetadataIdsUploaded);
            for (wrts_prcgvr__Activity__c activity : fileCheckActivities) {
                activity.wrts_prcgvr__Status__c = CONSTANTS.ACTIVITY_STATUS_COMPLETED;
            }
            activitiesToClose.addAll(fileCheckActivities);
        }
        Map<Id, wrts_prcgvr__Activity__c> activitiesMap = activityQueries.getByIds(validationActivityIds);
        Boolean syncCases = false;
        for (Id activityId : validationActivityIds) {
            wrts_prcgvr__Activity__c activity = activitiesMap.get(activityId);
            if (activity == null || activity.wrts_prcgvr__IsClosed__c) {
                continue;
            }
            syncCases |= !activity.DoNotSyncCases__c;
            activitiesToClose.add(new wrts_prcgvr__Activity__c(
                Id = activityId,
                wrts_prcgvr__Status__c = CONSTANTS.ACTIVITY_STATUS_COMPLETED
            ));
        }

        if (!activitiesToClose.isEmpty()) {
            databaseSrv.updateSObject(activitiesToClose);

            List<Case> cases = MRO_QR_Case.getInstance().getCasesByDossierId(dossier.Id);
            dossierSrv.checkAndApplyAutomaticTransitionToDossierAndCases(dossier, syncCases ? cases : null, CONSTANTS.DOCUMENT_VALIDATION_TAG, false, true, false);
        }
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse executeArchiveDMS(Id validatableDocumentId) {
        try {
            String className = 'MRO_SRV_DmsCallOut';
            String flowName = 'dmsUpload';
            String endpointName = 'dms';

            Map<String, Object> argsMap = new Map<String, Object>();
            argsMap.put('method', className);
            argsMap.put('sender', new ValidatableDocument__c(Id = validatableDocumentId));

            Map<String, String> parameters = new Map<String, String>();
            parameters.put('requestType', flowName);
            parameters.put('timeout', '4000');
            parameters.put('endPoint', endpointName);
            argsMap.put('parameters', parameters);

            MRO_SRV_SendMRRcallout instance = new MRO_SRV_SendMRRcallout();
            return (wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse) instance.execute((Object) argsMap, false, true);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            throw ex;
        }
    }

    public void checkAndCloseValidationActivity(Map<Id, FileMetadata__c> candidateDocumentDiscoToClose, Map<Id, Dossier__c> dossierToProcess) {
        Map<Id, ValidatableDocument__c> validatableDocumentsMap = new Map<Id, ValidatableDocument__c>();
        Set<Id> fileMetadataToCloseFileCheckAct = new Set<Id>();
        Map<Id, Boolean> candidateActivitiesToClose = new Map<Id, Boolean>();
        Set<Id> activitiesToClose = new Set<Id>();
        validatableDocumentsMap = dossierToProcess.isEmpty() ? new Map<Id, ValidatableDocument__c>() : MRO_QR_ValidatableDocument.getInstance().listDiscoValidatableDocumentForDossier(dossierToProcess.keySet());
        for (ValidatableDocument__c vd : validatableDocumentsMap.values()) {
            candidateActivitiesToClose.put(vd.ValidationActivity__c, true);
            if ((vd.FileMetadata__c == null || vd.FileMetadata__r.DiscoFileId__c == null) && candidateDocumentDiscoToClose.containsKey(vd.FileMetadata__c) && vd.DocumentBundleItem__r.Mandatory__c) {
                candidateActivitiesToClose.put(vd.ValidationActivity__c, false);
                continue;
            }
            if (candidateDocumentDiscoToClose.containsKey(vd.FileMetadata__c)) {
                fileMetadataToCloseFileCheckAct.add(vd.FileMetadata__c);
            }
        }

        for (String activityId : candidateActivitiesToClose.keySet()) {
            if (candidateActivitiesToClose.get(activityId) == true) {
                activitiesToClose.add(activityId);
            }
        }
        closeValidationActivities(activitiesToClose, dossierToProcess.values()[0], fileMetadataToCloseFileCheckAct);
    }

}