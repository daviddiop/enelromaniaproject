/**
 * Created by napoli on 27/11/2019.
 */
@isTest
public with sharing class MRO_LC_IndividualEditTst {

    @testSetup
    static void setup() {
        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert individual;

        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;

        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = businessAccount.Id;
        contact.IndividualId = individual.Id;
        insert contact;
    }

    @IsTest
    static void testInitDataTrue() {
        insert new EnergyApp2Settings__c(Name = 'Default', AllowSaveUnIdentifiedInterlocutor__c = true);
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_IndividualEdit','initData', new Map<String, String>(),true);
        System.assertEquals(true, (Boolean)result.get('isSaveUnIdentifiedInterlocutorAllowed'));
        Test.stopTest();
    }

    @IsTest
    static void testInitDataFalse() {
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_IndividualEdit','initData', new Map<String, String>(),true);
        System.assertEquals(false, (Boolean)result.get('isSaveUnIdentifiedInterlocutorAllowed'));
        Test.stopTest();
    }

  /*  @isTest
    static void saveIndividualTest(){
        String interlocutorDto = JSON.serializePretty(new Map<String, String>{
                'firstName' => 'Andrea',
                'lastName' => 'Sane',
                //'nationalId' => '987654321',
                'nationalId' => MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber(),
                'email' => 'ggInterlocutor@test.com',
                'phone' => '3394152356'
        });
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                WHERE Interlocutor__c = null
        ];

        Test.startTest();
        System.debug('interactionId -- '+interaction.Id);
        System.debug('interlocutorDTO -- '+interlocutorDTO);
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_IndividualEdit','saveIndividual',
                new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDto},true);
        System.assert(result.get('hasCustomer') != true);
        List<Individual> individualList = [
                SELECT Id
                FROM Individual
        ];
        System.assert( individualList.size() == 2);
        Test.stopTest();
    }*/

    @isTest
    static void saveIndividualIsBlankFirstNameTest(){
        String interlocutorDto = JSON.serializePretty(new Map<String, String>{
                'firstName' => null,
                'lastName' => 'Sane',
                'nationalId' => '987654321',
                'email' => 'ggInterlocutor@test.com',
                'phone' => '3394152356'
        });
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                WHERE Interlocutor__c = null
        ];
        Test.startTest();
        String response = (String) TestUtils.exec('MRO_LC_IndividualEdit','saveIndividual',
                new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDto},false);
        System.assert(true, response);
        Test.stopTest();
    }


    @isTest
    static void saveIndividualIsBlankLastNameTest(){
        String interlocutorDto = JSON.serializePretty(new Map<String, String>{
                'firstName' => 'Andrea',
                'lastName' => null,
                'nationalId' => '987654321',
                'email' => 'ggInterlocutor@test.com',
                'phone' => '3394152356'
        });
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                WHERE Interlocutor__c = null
        ];
        Test.startTest();
        String response = (String) TestUtils.exec('MRO_LC_IndividualEdit','saveIndividual',
                new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDto},false);
        System.assert(true, response);
        Test.stopTest();
    }

    @isTest
    static void saveUnidentifiedIndividual(){
        String interlocutorDto = JSON.serialize(new Map<String, String>{
                'firstName' => 'Andrea'
        });
        Interaction__c interaction = [
                SELECT Id, InterlocutorFirstName__c
                FROM Interaction__c
                WHERE Interlocutor__c = null
        ];
        System.assertEquals(null, interaction.InterlocutorFirstName__c);

        insert new EnergyApp2Settings__c(Name = 'Default', AllowSaveUnIdentifiedInterlocutor__c = true);

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_IndividualEdit','saveIndividual',
                new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDto},true);

        System.assertEquals(false, result.get('hasCustomer'));
        List<Individual> individualList = [
                SELECT Id
                FROM Individual
        ];
        System.assert( individualList.size() == 1);

        Interaction__c updatedInteraction = [
                SELECT Id, InterlocutorFirstName__c
                FROM Interaction__c
                WHERE Id = :interaction.Id
        ];
        System.assertEquals('Andrea', updatedInteraction.InterlocutorFirstName__c);

        Test.stopTest();
    }

    @isTest
    static void saveIndividualIsBlankNationalId(){
        Contact contact = [
                SELECT Id,Email,Phone
                FROM Contact
                Limit 1
        ];
        Individual individual = [
                SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                FROM Individual
                limit 1
        ];
        individual.NationalIdentityNumber__c = '';
        String interlocutorDTO = JSON.serializePretty(new Map<String, String>{
                'firstName' => individual.FirstName ,
                'lastName' => individual.LastName,
                'nationalId' => individual.NationalIdentityNumber__c,
                'email' => 'ggInterlocutor@test.com',
                'phone' => '3394152356'
        });
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                WHERE Interlocutor__c = NULL
        ];
        Test.startTest();
        Object response=TestUtils.exec('MRO_LC_IndividualEdit','saveIndividual',new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDTO},false);
        system.assertEquals('National Identity Number - required',response);
        Test.stopTest();
    }

    /* @isTest
       static void saveIndividualDuplicateIndividual(){
          Contact contact = [
                  SELECT Id,Email,Phone
                  FROM Contact
                  Limit 1
          ];
          Individual individual = [
                  SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                  FROM Individual
                  limit 1
          ];
          Individual individual2 = MRO_UTL_TestDataFactory.individual().setLastName('duplicate').build();
          individual.LastName = individual2.LastName;
          String interlocutorDTO = JSON.serializePretty(new Map<String, String>{
                 'firstName' => individual.FirstName ,
                 'lastName' => individual.LastName,
                 'nationalId' => individual.NationalIdentityNumber__c,
                 'email' => 'ggInterlocutor@test.com',
                 'phone' => '3394152356'
          });
          Interaction__c interaction = [
                  SELECT Id FROM Interaction__c
                  WHERE Interlocutor__c = NULL
          ];
          Test.startTest();
          Object response=TestUtils.exec('MRO_LC_IndividualEdit','saveIndividual',new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDTO},false);
          system.assertEquals('Duplicate records found: '+individual.Id,response);
          Test.stopTest();
      }
  */
    @isTest
    static void updateIndividual(){
        Contact contact = [
                SELECT Id,Email,Phone
                FROM Contact
                Limit 1
        ];
        Individual individual = [
                SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                FROM Individual
                limit 1
        ];
        String interlocutorDTO = JSON.serializePretty(new Map<String, String>{
                'Id' => individual.Id,
                'firstName' => 'individualFirstName' ,
                'lastName' => individual.LastName,
                'nationalId' => individual.NationalIdentityNumber__c,
                'email' => 'ggInterlocutor@test.com',
                'phone' => '3394152356'
        });
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT  1
        ];
        interaction.Interlocutor__c = individual.Id;
        update  interaction;
        Test.startTest();
        Object response=TestUtils.exec('MRO_LC_IndividualEdit','updateIndividual',new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDTO},true);
        Map<String, Object> createdMap=(Map<String, Object>)response;
        System.assert(createdMap.get('hasCustomer') != true);
        Individual individual2 = [
                SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                FROM Individual
                WHERE Id =:individual.Id
        ];
        system.assertEquals('individualFirstName',individual2.FirstName);
        Test.stopTest();
    }

    @isTest
    static void updateIndividualcheckIndividualDuplicate(){
        Contact contact = [
                SELECT Id,Email,Phone
                FROM Contact
                Limit 1
        ];
        Individual individual = [
                SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                FROM Individual
                LIMIT 1
        ];
        individual.NationalIdentityNumber__c = '';
        String interlocutorDTO = JSON.serializePretty(new Map<String, String>{
                'Id' => individual.Id,
                'firstName' => individual.FirstName ,
                'lastName' => individual.LastName,
                'nationalId' => individual.NationalIdentityNumber__c,
                'email' => 'ggInterlocutor@test.com',
                'phone' => '3394152356'
        });
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                WHERE Interlocutor__c = NULL
        ];
        Test.startTest();
        Object response=TestUtils.exec('MRO_LC_IndividualEdit','updateIndividual',new Map<String, String>{'interactionId'=>interaction.Id,'interlocutorDTO'=>interlocutorDTO},false);
        system.assertEquals('National Identity Number - required',response);
        Test.stopTest();
    }

    @isTest
    static void updateInteraction() {
        Individual individual = [
                SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                FROM Individual
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Contact contact = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        CustomerInteraction__c personCustomerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, account.Id, contact.Id).build();
        insert personCustomerInteraction;
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_IndividualEdit', 'updateInteraction', new Map<String, String>{
                'individualId' => individual.Id, 'interactionId' => interaction.Id
        }, true);
        System.assert(response != true);
        List<CustomerInteraction__c> customerInteractionsList = [
                SELECT Id
                FROM CustomerInteraction__c
        ];
        system.assert(customerInteractionsList.size() == 1);
        Test.stopTest();
    }

    @isTest
    static void updateInteractionTest() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Account myPersonAccount = [
                SELECT Id
                FROM Account
                WHERE IsPersonAccount = true
                LIMIT 1
        ];
        Contact myContact = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        Individual myIndividual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert myIndividual;

        Account myAccountWithoutContact = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert myAccountWithoutContact;
        Account myAccount3 = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert myAccount3;
        Account myAccount4 = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert myAccount4;

        Contact myContact3 = MRO_UTL_TestDataFactory.Contact().createContact().setIndividual(myIndividual.Id).setAccount(myAccount3.Id).build();
     //   insert myContact3;
     //   insert MRO_UTL_TestDataFactory.Contact().createContact().setIndividual(myIndividual.Id).setAccount(myAccount3.Id).build();
        insert MRO_UTL_TestDataFactory.account().personAccount().setPersonIndividual(myIndividual.Id).build();

        insert MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, myPersonAccount.Id, myContact.Id).build();
        insert MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, myAccountWithoutContact.Id, null).build();

      //  insert MRO_UTL_TestDataFactory.AccountContactRelation().createAccountContactRelation(myContact3.Id, myAccount4.Id).build();

        List<AccountContactRelation> acrList1 = [
                SELECT Id, AccountId, ContactId, Roles, IsDirect
                FROM AccountContactRelation
        ];

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_IndividualEdit', 'updateInteraction', new Map<String, String>{
                'individualId' => myIndividual.Id, 'interactionId' => interaction.Id
        }, true);
        Test.stopTest();

        System.assertEquals(true, result.get('isRedirectToInteraction'));
        List<AccountContactRelation> acrList2 = [
                SELECT Id
                FROM AccountContactRelation
        ];
        System.assertEquals(acrList1.size() + 2, acrList2.size());
    }

    @isTest
    static void updateInteractionTest2() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Individual myIndividual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert myIndividual;

        Account myAccountWithoutContact = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert myAccountWithoutContact;
        insert MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, myAccountWithoutContact.Id, null).build();

        List<AccountContactRelation> acrList1 = [
                SELECT Id, AccountId, ContactId, Roles, IsDirect
                FROM AccountContactRelation
        ];

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_IndividualEdit', 'updateInteraction', new Map<String, String>{
                'individualId' => myIndividual.Id, 'interactionId' => interaction.Id
        }, true);
        Test.stopTest();

        System.assertEquals(true, result.get('isRedirectToInteraction'));
        List<AccountContactRelation> acrList2 = [
                SELECT Id
                FROM AccountContactRelation
        ];
        System.assertEquals(acrList1.size() + 1, acrList2.size());
    }

    @isTest
    static void updateInteractionIsEmptyCustomerInter() {
        Individual individual = [
                SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                FROM Individual
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_IndividualEdit', 'updateInteraction', new Map<String, String>{
                'individualId' => individual.Id, 'interactionId' => interaction.Id
        }, true);
        System.assert(response != true);
        List<CustomerInteraction__c> customerInteractionsList = [
                SELECT Id
                FROM CustomerInteraction__c
        ];
        system.assert(customerInteractionsList.size() == 0);
        Test.stopTest();
    }

    @isTest
    static void updateInteractionIsBlankIndividualId() {
        String individualId = null;
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_IndividualEdit', 'updateInteraction', new Map<String, String>{
                'individualId' => individualId, 'interactionId' => interaction.Id
        }, false);
        system.assertEquals(response, 'Individual - Missing Id');
        Test.stopTest();
    }

    @isTest
    static void updateInteractionIsBlankInteractionId() {
        Individual individual = [
                SELECT Id,Name,FirstName,LastName,NationalIdentityNumber__c
                FROM Individual
                LIMIT 1
        ];
        String interactionId = null;
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_IndividualEdit', 'updateInteraction', new Map<String, String>{
                'individualId' => individual.Id, 'interactionId' => interactionId
        }, false);
        system.assertEquals(response, 'Interaction - Missing Id');
        Test.stopTest();
    }

    @isTest
    static void searchInterlocutorByIndividualAndContactFields() {
        Individual myIndividual = [
                SELECT Id, FirstName, LastName, NationalIdentityNumber__c
                FROM Individual
                LIMIT 1
        ];
        myIndividual.FirstName = 'first';
        myIndividual.LastName = 'last';
        myIndividual.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        update myIndividual;

        Contact myContact = [
                SELECT Id, Email, Phone
                FROM Contact
                WHERE IndividualId = :myIndividual.Id
                LIMIT 1
        ];
        myContact.Email = 'test@test.com';
        myContact.Phone = '12345';
        update myContact;

        Interaction__c interaction = new Interaction__c(
                InterlocutorFirstName__c = 'first',
                InterlocutorLastName__c = 'last',
                //InterlocutorNationalIdentityNumber__c = 'nationalId',
                InterlocutorNationalIdentityNumber__c = myIndividual.NationalIdentityNumber__c,
                InterlocutorEmail__c = 'test@test.com',
                InterlocutorPhone__c = '12345'
        );

        Test.startTest();
        List<Individual> individualList = (List<Individual>)TestUtils.exec('MRO_LC_IndividualEdit', 'searchInterlocutor', interaction, true);
        Test.stopTest();

        System.assertEquals(1, individualList.size());
        System.assertEquals(myIndividual.Id, individualList.get(0).Id);
    }
    @isTest
    static void searchInterlocutorByContactFields() {
        Individual myIndividual = [
                SELECT Id, FirstName, LastName, NationalIdentityNumber__c
                FROM Individual
                LIMIT 1
        ];
        Contact myContact = [
                SELECT Id, Email, Phone
                FROM Contact
                WHERE IndividualId = :myIndividual.Id
                LIMIT 1
        ];
        myContact.Email = 'test@test.com';
        myContact.Phone = '12345';
        update myContact;

        Interaction__c interaction = new Interaction__c(
                InterlocutorEmail__c = 'test@test.com',
                InterlocutorPhone__c = '12345'
        );

        Test.startTest();
        List<Individual> individualList = (List<Individual>)TestUtils.exec('MRO_LC_IndividualEdit', 'searchInterlocutor', interaction, true);
        Test.stopTest();

        System.assertEquals(1, individualList.size());
        System.assertEquals(myIndividual.Id, individualList.get(0).Id);
    }
}