@isTest()
public class MRO_SRV_AddressTst {
    @testsetup static void setup(){
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account ac=new Account(Name='account');
        insert ac;
        City__c city = new City__c(Name = 'JIMBOLIA', Province__c = 'TIMIS', Country__c = 'ROMANIA');
        insert city;
        Street__c[] streets = new Street__c[]{
                new Street__c(Name = 'ANTOL', City__c = city.Id, StreetType__c = 'PIATA', PostalCode__c = '123',
                        ToStreetNumber__c = 100, FromStreetNumber__c = 1)

        };
        insert streets;
    }
    @isTest static void normalize(){
        Test.startTest();
        Map<String, object>address=new Map<String, object>();
        MRO_SRV_Address.normalize(address);
        Test.stopTest();
    }
    @isTest static void getExtraFields(){
        Test.startTest();
        MRO_SRV_Address.getExtraFields('City__c','name');
        Test.stopTest();
    }
    @isTest static void getAddressStructure(){
        Test.startTest();
        MRO_SRV_Address.getAddressStructure('PostalCode');
        Test.stopTest();
    }
    @isTest static void AddressDTO(){
        Map<String, String> addressField=new Map<String, String>();
        addressField.put('streetId','1az');
        addressField.put('streetNumber','12');
        addressField.put('streetNumberExtn','14');
        addressField.put('streetName','milan');
        addressField.put('streetType','vil');
        addressField.put('apartment','appart');
        addressField.put('building','address');
        addressField.put('city','milan');
        addressField.put('country','italia');
        addressField.put('floor','a');
        addressField.put('locality','milan');
        addressField.put('postalCode','milamo11');
        addressField.put('province','milano');
        addressField.put('addressNormalized','true');
        addressField.put('addressKey','11');
        Map<String, String> addressFieldunNormalize=new Map<String, String>();
        addressFieldunNormalize.put('streetId','1az');
        addressFieldunNormalize.put('streetNumber','12');
        addressFieldunNormalize.put('streetNumberExtn','14');
        addressFieldunNormalize.put('streetName','milan');
        addressFieldunNormalize.put('streetType','vil');
        addressFieldunNormalize.put('apartment','appart');
        addressFieldunNormalize.put('building','address');
        addressFieldunNormalize.put('city','milan');
        addressFieldunNormalize.put('country','italia');
        addressFieldunNormalize.put('floor','a');
        addressFieldunNormalize.put('locality','milan');
        addressFieldunNormalize.put('postalCode','milamo11');
        addressFieldunNormalize.put('province','milano');
        addressFieldunNormalize.put('addressNormalized',null);
        addressFieldunNormalize.put('addressKey','11');
        Map<String, String> addressFieldExeption=new Map<String, String>();
        addressFieldExeption.put('streetId','1az');
        addressFieldExeption.put('streetNumber','12');
        addressFieldExeption.put('streetNumberExtn','14');
        Test.startTest();
        Account ac =[select Id,ResidentialStreetId__c,ResidentialStreet__c,ResidentialStreetName__c,ResidentialStreetNumber__c,ResidentialStreetNumberExtn__c
                ,ResidentialStreetType__c,ResidentialBuilding__c,ResidentialBlock__c,ResidentialApartment__c,ResidentialCountry__c,ResidentialCity__c,ResidentialFloor__c,
                ResidentialLocality__c,ResidentialPostalCode__c,ResidentialProvince__c,ResidentialAddressNormalized__c,ResidentialAddressKey__c from Account][0];
        MRO_SRV_Address.AddressDTO AddressDTO=new MRO_SRV_Address.AddressDTO();
        MRO_SRV_Address.AddressDTO AddressDTO1=new MRO_SRV_Address.AddressDTO(addressField);
        MRO_SRV_Address.AddressDTO AddressDTOExeption=new MRO_SRV_Address.AddressDTO(addressField);
        MRO_SRV_Address.AddressDTO AddressDTOUnNormalize=new MRO_SRV_Address.AddressDTO(addressFieldunNormalize);
        MRO_SRV_Address.AddressDTO AddressDTO2=new MRO_SRV_Address.AddressDTO(ac,'Residential');
        AddressDTO1.populateRecordAddressFields(ac, 'Residential');
        Test.stopTest();

    }
    @isTest static void getAvailableAddresses(){
        Account ac =[select Id from Account][0];
        Test.startTest();
        MRO_SRV_Address.getAvailableAddresses('',''+ac.Id);
        Test.stopTest();
    }





}