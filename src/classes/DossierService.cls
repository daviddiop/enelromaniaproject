/**
 * Create by Moussa Fofana on 29/07/2019.
 */ 
public with sharing class DossierService {

    static Constants constantSrv = Constants.getAllConstants();
    private static DossierQueries dossierQuery = DossierQueries.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery= CustomerInteractionQueries.getInstance();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    private static String dossierDraftStatus = constantSrv.DOSSIER_STATUS_DRAFT;
    private static String dossierNewStatus = constantSrv.DOSSIER_STATUS_NEW;
    private static String dossierCanceledStatus = constantSrv.DOSSIER_STATUS_CANCELED;

    public static DossierService getInstance() {
        return (DossierService)ServiceLocator.getInstance(DossierService.class);
    }

    public Dossier__c generateDossier(String accountId, String dossierId, String interactionId, String companyDivisionId, String recordTypeId,String requestType){
        Dossier__c dossier;
        if (String.isBlank(dossierId)) {
            List<CustomerInteraction__c> customerInteractions = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);
            dossier = new Dossier__c ();
            if(!customerInteractions.isEmpty()){
                dossier.CustomerInteraction__c = customerInteractions[0].Id;
            }
            dossier.Account__c = accountId;
            dossier.CompanyDivision__c = String.isNotBlank(companyDivisionId) ? companyDivisionId : null;
            dossier.RecordTypeId = recordTypeId;
            dossier.Status__c= dossierDraftStatus;
            dossier.RequestType__c = requestType;
            databaseSrv.insertSObject(dossier);
        } else {
            dossier = dossierQuery.getById(dossierId); 
        }
        return dossier;        
    }

    public void updateDossierStage(String dossierId,String newStage) {
        Dossier__c updatedDossier = new Dossier__c(Id = dossierId, Status__c = newStage);
        databaseSrv.updateSObject(updatedDossier);
    }

    public void updateDossierCompanyDivision(String dossierId,String companyDivisionId) {
        Dossier__c updatedDossier = new Dossier__c(Id = dossierId, CompanyDivision__c = companyDivisionId, Status__c = dossierNewStatus);
        databaseSrv.updateSObject(updatedDossier);
    }
}