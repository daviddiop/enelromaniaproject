/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   02/04/2020
* @desc
* @history
*/
public with sharing class MRO_SRV_DmsCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout {
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap){
        System.debug('MRO_SRV_DmsCallOut.buildMultiRequest');

        //Sobject
        SObject obj = (SObject) argsMap.get('sender');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        SObject document;

        //Query
        if(obj instanceof ValidatableDocument__c){
            document = [SELECT   DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c,
                    FileMetadata__r.Id,
                    FileMetadata__r.ExternalId__c,
                    FileMetadata__r.DocumentID__c,
                    FileMetadata__r.DiscoFileId__c,
                    FileMetadata__r.Title__c,
                    FileMetadata__r.Dossier__r.CompanyDivision__r.VatNUmber__c,
                    FileMetadata__r.Dossier__r.Account__r.VatNUmber__c,
                    FileMetadata__r.Dossier__r.Account__r.NationalIdentityNumber__pc,
                    Case__r.ExternalRequestID__c
            FROM    ValidatableDocument__c WHERE Id = :obj.Id
            AND    FileMetadata__c != NULL];
        } else if(obj instanceof FileMetadata__c){
            document = [SELECT
                    Id,
                    ExternalId__c,
                    DocumentID__c,
                    DiscoFileId__c,
                    Title__c,
                    Dossier__r.CompanyDivision__r.VatNUmber__c,
                    Dossier__r.Account__r.VatNUmber__c,
                    Dossier__r.Account__r.NationalIdentityNumber__pc,
                    Case__r.ExternalRequestID__c,
                    ExternalRequestID__c
            FROM    FileMetadata__c WHERE Id = :obj.Id];
        }




        //to WObject
        Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject(document, null);

        //Add mrrObj to the request
        request.objects.add(mrrObj);

        //Add request to Multirequest
        multiRequest.requests.add(request);

        return multiRequest;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response = new wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse();
        response.success = false;
        if (calloutResponse != null && calloutResponse.responses != null && !calloutResponse.responses.isEmpty()) {
            if (calloutResponse.responses[0].code == 'OK') {
                response.success = true;
            }
            response.message = calloutResponse.responses[0].description;
        }
        return response;
    }
}