public with sharing class QueryBuilder {

    public static final String AND_OPERATOR = 'AND';
    public static final String OR_OPERATOR = 'OR';

    public SObjectType objectType;
    public List<SObjectField> fields;
    public List<String> fieldNames;
    public List<String> relationshipFieldNames;
    public QueryBuilder.Condition condition;
    public QueryBuilder subQueryBuilder;

    public static QueryBuilder getInstance() {
        QueryBuilder builder = new QueryBuilder();
        builder.relationshipFieldNames = new List<String>();
        return builder;
    }

    public QueryBuilder setObject(SObjectType objectName) {
        this.objectType = objectName;
        return this;
    }

    public QueryBuilder setFields(List<SObjectField> fields) {
        this.fields = fields;
        this.fieldNames = new List<String>();
        for (SObjectField field : fields) {
            fieldNames.add(field.getDescribe().getName());
        }
        return this;
    }

    public QueryBuilder addField(SObjectField relationshipField, SObjectField field) {
        relationshipFieldNames.add(relationshipField.getDescribe().getRelationshipName() + '.' + field.getDescribe().getName());
        return this;
    }

    public QueryBuilder setCondition(Condition condition) {
        this.condition = condition;
        return this;
    }

    public QueryBuilder setCondition(List<Condition> conditionList, String operator) {
        if (conditionList.isEmpty()) {
            return this;
        }
        if (operator == AND_OPERATOR) {
            this.condition = new LogicCondition().andOp(conditionList);
        } else if (operator == OR_OPERATOR) {
            this.condition = new LogicCondition().orOp(conditionList);
        }
        return this;
    }

    public QueryBuilder setSubQuery(QueryBuilder subQueryBuilder) {
        this.subQueryBuilder = subQueryBuilder;
        return this;
    }

    public String build() {
//        validate();
        String relationShipFields = '';
        if (relationshipFieldNames.size() > 0) {
            relationShipFields = String.join(relationshipFieldNames, ', ') + ', ';
        }
        String selectClause = 'SELECT ' + relationShipFields + String.join(fieldNames, ', ').removeEnd(', ');
        if (subQueryBuilder != null) {
            selectClause += ', (' + subQueryBuilder.build(objectType) + ')';
        }
        String fromClause = 'FROM ' + objectType.getDescribe().getName();

        String whereClause = '';
        if (condition != null) {
            whereClause = 'WHERE ' + condition.getExpression();
        }
        return selectClause + ' ' + fromClause + ' ' + whereClause;
    }

    public String build(SObjectType parentSobject) {
//        validate();
        String selectClause = 'SELECT ' + String.join(fieldNames, ', ').removeEnd(', ');
        String fromClause = 'FROM ' + getChildRelationshipName(parentSobject);
        String whereClause = '';
        if (condition != null) {
            whereClause = 'WHERE ' + condition.getExpression();
        }
        return selectClause + ' ' + fromClause + ' ' + whereClause;
    }

    public List<sObject> execute() {
        try {
            String queryString = build();
            System.debug(queryString);
            return Database.query(queryString);
        } catch (Exception exp) {
            //TODO: logging
        }
        return null;
    }
    
    /*
    private void validate() {
        for (SObjectField objField : this.fields) {
            List<Schema.SObjectType> sObjectTypeList = objField.getDescribe().getReferenceTo();
            System.debug(sObjectTypeList);

            Boolean isSameType = false;
            for (SObjectType currentType : sObjectTypeList) {
                System.debug(currentType);
                if (currentType == objectType) {
                    isSameType = true;
                    break;
                }
            }
            if (!isSameType) {
                throw new QueryBuilderException('Query contains unexpected field for ' + objectType);
            }
        }
    }*/

    private String getChildRelationshipName(SObjectType parentSobject) {
        List<Schema.ChildRelationship> childRelationshipList = parentSobject.getDescribe().getChildRelationships();
        for (Schema.ChildRelationship childRelation : childRelationshipList) {
            if (childRelation.getChildSObject() == objectType) {
                return childRelation.getRelationshipName();
            }
        }
        return null;
    }

    public with sharing class LogicCondition implements Condition {
        public List<Condition> conditionList;
        public String operator;

        public void addOp(List<Condition> conditionList, String operator) {
            this.conditionList = conditionList;
            this.operator = operator;
        }

        public LogicCondition andOp(List<Condition> conditionList) {
            addOp(conditionList, AND_OPERATOR);
            return this;
        }

        public LogicCondition orOp(List<Condition> conditionList) {
            addOp(conditionList, OR_OPERATOR);
            return this;
        }

        public String getExpression() {
            String result = '';
            for (QueryBuilder.Condition cond : conditionList) {
                if (cond instanceof FieldCondition) {
                    result += cond.getExpression();
                }
                if (cond instanceof LogicCondition) {
                    result += '(' + cond.getExpression() + ')';
                }
                result += ' ' + this.operator + ' ';
            }
            return result.removeEnd(' ' + this.operator + ' ');
        }
    }


    public with sharing class FieldCondition implements Condition {
        public SObjectField field;
        public String value;
        public String operator;
        public Condition eqOp(SObjectField field, String value) {
            this.field = field;
            this.operator = '=';
            this.value = '\'' + value + '\'';
            return this;
        }
        public Condition likeOp(SObjectField field, String value) {
            this.field = field;
            this.operator = 'LIKE';
            this.value = '\'' + value + '%\'';
            return this;
        }
        public Condition fullLikeOp(SObjectField field, String value) {
            this.field = field;
            this.operator = 'LIKE';
            this.value = '\'%' + value + '%\'';
            return this;
        }
        public Condition InOp(SObjectField field, Set<Id> ids) {
            this.field = field;
            this.operator = 'IN';
            this.value = '(';
            for (Id theId : ids) {
                this.value += '\'' + theId + '\'';
            }
            this.value += ')';

            return this;
        }
        public String getExpression() {
            return field.getDescribe().getName() + ' ' + operator + ' ' + value;
        }
    }

    public interface Condition {
        String getExpression();
    }

    public with sharing class QueryBuilderException extends Exception {}
}