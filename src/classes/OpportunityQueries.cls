public with sharing class OpportunityQueries {

    public static OpportunityQueries getInstance() {
        return (OpportunityQueries) ServiceLocator.getInstance(OpportunityQueries.class);
    }

    public Opportunity getById(String OpportunityId) {
        List<Opportunity> opportunityList = [
                SELECT Id, Name, Pricebook2Id, Type, AccountId, Account.Name, StageName, CloseDate, CompanyDivision__c, ContractId, CustomerInteraction__c,
                    CustomerInteraction__r.Contact__c,RequestType__c,ContractSignedDate__c,
                    (
                        SELECT Id, ServicePointCode__c, RecordType.DeveloperName, Product__c,Account__c,
                               Distributor__c, BillingProfile__c, ContractAccount__c, Trader__c, PointAddressNormalized__c, PointStreetType__c,
                               PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c, PointApartment__c, PointBuilding__c, PointCity__c,
                               PointCountry__c, PointFloor__c, Market__c, NonDisconnectable__c, PointLocality__c,
                               PointPostalCode__c, PointProvince__c, PointAddressKey__c, PointStreetId__c, Voltage__c, VoltageLevel__c, PressureLevel__c,
                               Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c, ServiceSite__c, SiteAddressKey__c, SiteStreetId__c,
                               SiteAddressNormalized__c, SiteApartment__c, SiteBuilding__c,SiteCity__c, SiteCountry__c, SiteFloor__c,
                               SiteStreetType__c, SiteLocality__c, SitePostalCode__c, SiteProvince__c, SiteStreetName__c, SiteStreetNumber__c,
                               SiteStreetNumberExtn__c, AvailablePower__c, EstimatedConsumption__c, ServicePoint__r.CurrentSupply__c,
                               ServicePoint__r.CurrentSupply__r.CompanyDivision__c
                        FROM OpportunityServiceItems__r
                    ),
                    (
                        SELECT Id, Product2Id, ProductCode,
                            Product2.Id, Product2.Name, Product2.RecordType.DeveloperName, Product2.RecordType.Name, Product2.Description,
                            Product2.Image__c, Product2.Family
                        FROM OpportunityLineItems
                    )
                FROM Opportunity
                WHERE Id = :OpportunityId
        ];

        return opportunityList.get(0);
    }

    /**
    * @author INSA BADJI
    * @description  Select an Opportunity by Id
    *
    * @param opportunityId Id of Opportunity
    */
    public Opportunity getOpportunityById(String opportunityId) {
        return [
                SELECT Id, Name, StageName,ContractSignedDate__c,
                        CompanyDivision__c,CompanyDivision__r.Name,CustomerInteraction__c,
                        AccountId,ContractId
                FROM Opportunity
                WHERE Id = :opportunityId
        ];
    }

}