/**
 * Created by Moussa on 31/07/2019.
 *
 */
@isTest
public with sharing class WizardHeaderCntTst {
    @testSetup
    private static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.BusinessType__c = 'NGO';
        Account personAccount = TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        List<Account> listAccount = new List<Account>();
        listAccount.add(businessAccount);
        listAccount.add(personAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Dossier__c dossier = TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        insert opportunity;        
      
    }

    @isTest
    public static void initializeTest(){
        Account account = [
			SELECT Id,Name
			FROM Account
			LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id,StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];        

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];        
        CompanyDivision__c companyDivision = [
            SELECT Id,Name 
            FROM CompanyDivision__c 
            WHERE Name = 'ENEL 1' 
            LIMIT 1
        ];        
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'opportunityId' => opp.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('WizardHeaderCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('response') != null );
        Test.stopTest();

    }    


    @isTest
    public static void initializeExceptionTest(){        
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => '',
            'opportunityId' => '',
            'companyDivisionId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('WizardHeaderCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();

    }     
}