/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   29/05/2020
* @desc
* @history
*/
public with sharing class MRO_SRV_PrecheckPODCallout implements MRO_SRV_SendMRRcallout.IMRRCallout {
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap) {
        System.debug('MRO_SRV_PrecheckPODCallout.buildMultiRequest');


        String distributorVat = (String) argsMap.get('distributorVat');
        String traderVat = (String) argsMap.get('traderVat');
        String pod = (String) argsMap.get('pod');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        //Fill header fields
        request.header.fields.add(MRO_UTL_MRRMapper.createField('Code_Flux', parameters.get('requestType')));
        request.header.fields.add(MRO_UTL_MRRMapper.createField('Distributor_Vat', distributorVat));
        request.header.fields.add(MRO_UTL_MRRMapper.createField('Trader_Vat', traderVat));
        request.header.fields.add(MRO_UTL_MRRMapper.createField('POD', pod));

        //Add request to Multirequest
        multiRequest.requests.add(request);

        return multiRequest;
    }

    public static PrecheckPODResult buildPrecheckPODResult(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse) {
        PrecheckPODResult precheckPODResult = new PrecheckPODResult();
        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
//            if(calloutResponse.responses[0].code == 'OK'){
            precheckPODResult.code = calloutResponse.responses[0].code;
            precheckPODResult.description = (calloutResponse.responses[0].code == 'OK') ? 'Pod OK' : calloutResponse.responses[0].description;
            if (calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
                for (wrts_prcgvr.MRR_1_0.WObject wo : calloutResponse.responses[0].objects) {
                    if (wo.objectType == 'OpportunityServiceItem__c' && !wo.fields.isEmpty()) {
                        OpportunityServiceItem__c osi = (OpportunityServiceItem__c) MRO_UTL_MRRMapper.wobjectToSObject(wo);
                        precheckPODResult.osi = osi;
                    }
                }
//            }
            }
            if (calloutResponse.responses[0].header.fields != null && !calloutResponse.responses[0].header.fields.isEmpty()) {
                for (wrts_prcgvr.MRR_1_0.Field f : calloutResponse.responses[0].header.fields) {
                    switch on f.name {
                        when 'Outcome' {
                            precheckPODResult.outcome = f.value;
                        }
                        when 'ContractState' {
                            precheckPODResult.contractState = f.value;
                        }
                        when 'Trader_Vat' {
                            precheckPODResult.traderVat = f.value;
                        }
                        when 'POD' {
                            precheckPODResult.podCode = f.value;
                        }
                        when 'NoteNewContract' {
                            precheckPODResult.noteNewContract = f.value;
                        }
                        when 'Distributor_Vat' {
                            precheckPODResult.distributorVat = f.value;
                        }
                    }
                    System.debug('fieldType ' + f.fieldType);
                    System.debug('name ' + f.name);
                    System.debug('value ' + f.value);
                    System.debug('----------------');
                }
            }
        }
        return precheckPODResult;
    }

    public class PrecheckPODResult{
        public OpportunityServiceItem__c osi;
        public String outcome;
        public String contractState;
        public String traderVat;
        public String podCode;
        public String noteNewContract;
        public String distributorVat;
        public String code;
        public String description;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response = new wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse();
        if(calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
            if(calloutResponse.responses[0].code == 'OK'){
                response.success = true;
            }else{
                response.success = false;
            }
            response.message = calloutResponse.responses[0].description;
        }
        return response;
    }
}
