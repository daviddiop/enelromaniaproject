/**
 * Created by goudiaby on 26/08/2019.
 */

@IsTest
public with sharing class CancellationRequestCntTst {
    @TestSetup
    static void setup() {
        CancellationRequest__c cancellationRequestCase = MRO_UTL_TestDataFactory.cancellationRequest().newCancellationRequestBuilder('Case').build();
        insert cancellationRequestCase;

        CancellationRequest__c cancellationRequestDossier = MRO_UTL_TestDataFactory.cancellationRequest().newCancellationRequestBuilder('Dossier').build();
        insert cancellationRequestDossier;

        Case myCase = TestDataFactory.caseRecordBuilder().newCase().build();
        insert myCase;

        Dossier__c dossier = TestDataFactory.Dossier().build();
        insert dossier;
    }

    @IsTest
    static void getCancellationReasonsTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'objectName' => 'Case',
                'recordType' => 'SwitchIn',
                'originPhase' => 'RE010'
        };
        Test.startTest();
        Object response = TestUtils.exec('CancellationRequestCnt','getCancellationReasons',inputJSON,true);
        Map<String,Object> createdOptionList = (Map<String,Object>)response;
        System.assertEquals(true, createdOptionList.get('listCancellationReasons') != null);
        Test.stopTest();
    }

    @IsTest
    static void updateSobjectToCanceledCaseTest() {
        Case myCase = [ SELECT Id FROM Case WHERE Status='New' LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => myCase.Id,
                'objectName' => 'Case',
                'cancellationReason' => 'Reason 1'
        };
        Test.startTest();
        Object response = TestUtils.exec('CancellationRequestCnt','updateSobjectToCanceled',inputJSON,true);
        Test.stopTest();
        Case myCaseCanceled = [SELECT Id FROM Case WHERE Status='Canceled' LIMIT 1];
        System.assert(myCaseCanceled != null, 'Why not? ' + myCaseCanceled);
    }

    @IsTest
    static void updateSobjectToCanceledDossierTest() {
        Dossier__c dossier = [ SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => dossier.Id,
                'objectName' => 'Dossier',
                'cancellationReason' => 'Reason 3'
        };
        Test.startTest();
        Object response = TestUtils.exec('CancellationRequestCnt','updateSobjectToCanceled',inputJSON,true);
        Test.stopTest();
        Dossier__c dossierCanceled = [SELECT Id FROM Dossier__c WHERE Status__c = 'Canceled' LIMIT 1];
        System.assert(dossierCanceled != null, 'Why not? ' + dossierCanceled);
    }

    @IsTest
    static void getFieldsetTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'objectName' => 'Case'
        };
        Object response = TestUtils.exec(
                'CancellationRequestCnt', 'getFieldset', inputJSON, true);
        Test.stopTest();
    }

    @isTest
    static void initializeExceptionTest() {
        Test.startTest();
        try {
            Object response = TestUtils.exec('CancellationRequestCnt', 'updateSobjectToCanceled', null, false);
            Map<String, Object> result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
}