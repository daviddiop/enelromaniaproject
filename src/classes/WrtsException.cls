//Code = 08C
public with sharing class WrtsException extends Exception {
    public with sharing class GenericException extends Exception {}
    public with sharing class DatabaseException extends Exception {}
}