/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 02.04.20.
 */
@IsTest
public with sharing class MRO_LC_GDPRRequestWizardTst {

    @TestSetup
    private static void setup() {

        //Insert Sequencer
        Sequencer__c sequencer = new Sequencer__c(Sequence__c = 1, SequenceLength__c = 1, Type__c = 'CustomerCode');
        insert sequencer;


        //Insert Accounts
        List<Account> listAccount = new List<Account>();
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        listAccount.add(personAccount);

        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.BusinessType__c = 'NGO';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = businessAccount.Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, personAccount.Id, contact.Id).build();
        insert customerInteraction;

    }

    static Map<String, Object> initialize() {
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
                LIMIT 1
        ];

        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'interactionId' => interaction.Id,
                'companyDivisionId' => companyDivision.Id
        };

        List<Dossier__c> dossiers = [
                SELECT Id, CancellationDetails__c
                FROM Dossier__c
                LIMIT 1
        ];
        if (dossiers.size() > 0) {
            inputJSON.put('dossierId', dossiers[0].Id);
        }

        Object response = TestUtils.exec(
                'MRO_LC_GDPRRequestWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));

        return result;
    }

    @IsTest
    static void upsertCaseTest() {
        Map<String, Object> initResponse = initialize();

        List<Utils.KeyVal> requestTypeOptions = (List<Utils.KeyVal>)initResponse.get('requestTypeOptions');
        String requestType = (String)requestTypeOptions.get(0).value;

        List<Utils.KeyVal> customerNeedsOptions = (List<Utils.KeyVal>)initResponse.get('customerNeedsOptions');
        String customerNeeds = (String)customerNeedsOptions.get(0).value;

        Map<String, Object> inputJSON = new Map<String, Object>{
                'accountId' => (String) initResponse.get('accountId'),
                'dossierId' => (String) initResponse.get('dossierId'),
                'originSelected' => 'Email',
                'channelSelected' => 'Back Office Sales',
                'requestType' => requestType
        };

        //initial create case and fill request type
        Object response = TestUtils.exec(
                'MRO_LC_GDPRRequestWizard', 'upsertCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        String caseId = (String)result.get('caseId');
        System.assertEquals(false, result.get('error'));
        System.assertNotEquals(null, caseId);

        inputJSON.put('caseId', caseId);
        inputJSON.put('customerNeeds', customerNeeds);

        //update customer needs field
        response = TestUtils.exec(
                'MRO_LC_GDPRRequestWizard', 'upsertCase', inputJSON, true);
        result = (Map<String, Object>) response;

        System.assertEquals(false, result.get('error'));

        List<Case> cases = [SELECT CustomerNeeds__c, SubProcess__c FROM Case WHERE Id = :caseId];
        System.assertEquals(1, cases.size());
        Case newCase = cases[0];
        System.assertEquals(customerNeeds, newCase.CustomerNeeds__c);
        System.assertEquals(requestType, newCase.SubProcess__c);
    }

    @IsTest
    static void setChannelAndOriginTest() {
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_GDPRRequestWizard', 'setChannelAndOrigin', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void cancelProcessTest() {
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_GDPRRequestWizard', 'cancelProcess', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void saveProcessTest() {
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_GDPRRequestWizard', 'saveProcess', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    /*@IsTest
    static void cancelProcessErrorTest() {
        createCase();
        Map<String, Object> requestParams = getRequestParams();
        requestParams.put('dossierId', '');

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'cancelProcess', requestParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void saveProcessErrorTest() {
        createCase();
        Map<String, Object> requestParams = getRequestParams();
        requestParams.put('dossierId', '');

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'saveProcess', requestParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
        Test.stopTest();
    }*/

    static Map<String, Object> getRequestParams() {
        Map<String, Object> initResponse = initialize();

        Map<String, Object> inputJSON = new Map<String, Object>{
                'caseId' => (String) initResponse.get('caseId'),
                'accountId' => (String) initResponse.get('accountId'),
                'dossierId' => (String) initResponse.get('dossierId'),
                'originSelected' => 'Email',
                'channelSelected' => 'Back Office Sales',
                'salesman' => null,
                'salesChannel' => null,
                'cancelReason' => '',
                'detailsReason' => null,
                'isDraft' => false
        };
        return inputJSON;
    }


}