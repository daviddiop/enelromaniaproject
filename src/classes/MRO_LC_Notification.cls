/**
 * Created by Boubacar Sow on 15/10/2020.
 */

public with sharing class MRO_LC_Notification extends  ApexServiceLibraryCnt {

    public with sharing class listNotification extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String username = params.get('username');
            System.debug('### username '+username);

            try {
                if (String.isBlank(username)) {
                    throw new WrtsException(System.Label.username+' '+System.Label.IsMissing);
                }
                List<Notification> notifications = MRO_SRV_MyEnelAccountSituationCallout.getInstance().getNotificationsList(username);
                if (!notifications.isEmpty()) {
                    response.put('notifications', notifications);
                    response.put('error', false);
                    return response;
                }
                notifications = new List<Notification>();
                notifications.add(new Notification(
                    'type tres tres complet',
                    'DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription',
                    true,
                    true,
                    true
                ));
                response.put('notifications', notifications);
                response.put('error', false);
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                if (e instanceof NullPointerException) {
                    response.put('errorMsg', System.Label.ServiceIsDown);
                }
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }

    public class Notification {
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public String description { get; set; }
        @AuraEnabled
        public Boolean email { get; set; }
        @AuraEnabled
        public Boolean sms { get; set; }
        @AuraEnabled
        public Boolean push { get; set; }

        public Notification(String type, String description, Boolean email, Boolean sms, Boolean push) {
            this.type = type;
            this.description = description;
            this.email = email;
            this.sms = sms;
            this.push = push;
        }

        public Notification(Map<String, String> stringMap) {
            this.type = stringMap.get('code');
            this.description = stringMap.get('description');
            this.email = stringMap.get('email') == null ? null : Boolean.valueOf(stringMap.get('email'));
            this.sms = stringMap.get('sms') == null ? null: Boolean.valueOf(stringMap.get('sms'));
            this.push = stringMap.get('push') == null ? null: Boolean.valueOf(stringMap.get('push'));
        }
    }

}