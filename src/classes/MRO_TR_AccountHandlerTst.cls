/**
 * Created by napoli on 26/09/2019.
 */

@IsTest
private class MRO_TR_AccountHandlerTst {
    @IsTest
    static void testBehavior() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder =  MRO_UTL_TestDataFactory.account().personAccount();
        Account acc = AccountBuilder.build();
        Database.SaveResult[] lsr = Database.insert(new Account[]{acc},false);
        for (Database.SaveResult sr : lsr) {
            System.assertEquals(true, sr.isSuccess());
        }
        accountBuilder.setNationalIdentityNumber('1800101221145');
        lsr = Database.update(new Account[]{acc},false);
        for (Database.SaveResult sr : lsr) {
            System.assertEquals(false, sr.isSuccess());
        }
    }
}