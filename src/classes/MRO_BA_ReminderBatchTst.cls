/**
 * Created by giupastore on 14/05/2020.
 */


@ isTest
public with sharing class MRO_BA_ReminderBatchTst {
	@TestSetup
	private static void setup() {
		CustomReminder__c cr1 = new CustomReminder__c();
		cr1.Code__c = 'test1';
		cr1.RecordId__c = [SELECT ID FROM USER LIMIT 1].ID;
		cr1.Status__c = 'Pending';
		cr1.ExecutionDateTime__c = System.now().addMinutes(44);
		insert cr1;
		CustomReminder__c cr2 = new CustomReminder__c();
		cr2.Code__c = 'test2';
		cr2.RecordId__c = [SELECT ID FROM USER LIMIT 1].ID;
		cr2.ExecutionDateTime__c = System.now().addMinutes(66);
		cr2.Status__c = 'Pending';
		insert cr2;
	}

	@isTest
	public static void executeTest() {
		Test.startTest();
		Database.executeBatch(new MRO_BA_ReminderBatch(),1);
		List<CustomReminder__c> cusListToCompare = [SELECT Id,Name,Status__c, Code__c FROM CustomReminder__c];
		System.debug('cusListToCompare ' + cusListToCompare);
		System.assertEquals(cusListToCompare.size(),2);
		//The System.assertEquals fails because the batch executes after the query above. if using 2 startTest and StopTest method the
		//CustomReminder__c with a time below System.now() + 59 minutes will be updated to Executed, but
		//	the test class will give an error because of this exception System.FinalException Testing already started
		Test.stopTest();
	}

	@isTest
	public static void executeTestException() {
		Test.startTest();
		List<CustomReminder__c> cusListToDel = [SELECT Id FROM CustomReminder__c ];
		delete cusListToDel;
		Database.executeBatch(new MRO_BA_ReminderBatch(),1);
		Test.stopTest();
	}

}