/**
 * Created by Boubacar Sow on 15/10/2020.
 */

public with sharing class MRO_LC_AssociatedAccount extends  ApexServiceLibraryCnt {

    public with sharing class listAssociatedAccount extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String paymentCode = params.get('paymentCode');
            System.debug('### paymentCode '+paymentCode);

            try {
                if (String.isBlank(paymentCode)) {
                    throw new WrtsException(System.Label.PaymentCode+' '+System.Label.IsMissing);
                }
                List<AssociatedAccount> associatedAccounts  = MRO_SRV_MyEnelAccountSituationCallout.getInstance().getAssociatedAccountsList(paymentCode);
                System.debug('### associatedAccounts '+associatedAccounts);
                if (!associatedAccounts.isEmpty()) {
                    response.put('associatedAccounts', associatedAccounts);
                    response.put('error', false);
                    return  response;
                }
                associatedAccounts  = listAssociatedAccount(paymentCode);
                response.put('associatedAccounts', associatedAccounts);
                response.put('error', false);
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                if (e instanceof NullPointerException) {
                    response.put('errorMsg', System.Label.ServiceIsDown);
                }
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }

    public static List<AssociatedAccount> listAssociatedAccount(String paymentCode) {
        List<AssociatedAccount> associatedAccountList = new List<AssociatedAccount>();
            associatedAccountList.add(new AssociatedAccount(
                '0011w00000Ii9niAAB',
                'mail',
                'phone',
                '1Name',
                '0Name',
                'status',
                'type',
                true,
                true,
                Date.today(),
                Date.today()
            ));
        return associatedAccountList;
    }

    public class AssociatedAccount {
        @AuraEnabled
        public String username { get; set; }
        @AuraEnabled
        public String email { get; set; }
        @AuraEnabled
        public String phone { get; set; }
        @AuraEnabled
        public String firstName { get; set; }
        @AuraEnabled
        public String lastName { get; set; }
        @AuraEnabled
        public String status { get; set; }
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public Boolean unique { get; set; }
        @AuraEnabled
        public Boolean master { get; set; }
        @AuraEnabled
        public Date createdDate { get; set; }
        @AuraEnabled
        public Date lastLogin { get; set; }

        public AssociatedAccount(String username, String email, String phone,String firstName, String lastName,
            String status, String type, Boolean unique, Boolean master, Date createdDate, Date lastLogin ) {
            this.username = username;
            this.email = email;
            this.phone = phone;
            this.firstName = firstName;
            this.lastName = lastName;
            this.status = status;
            this.type = type;
            this.unique = unique;
            this.master = master;
            this.createdDate = createdDate;
            this.lastLogin = lastLogin;
        }

        public AssociatedAccount(Map<String, String> stringMap) {
            this.username = stringMap.get('username');
            this.email = stringMap.get('email');
            this.phone = stringMap.get('phone');
            this.firstName = stringMap.get('firstName');
            this.lastName = stringMap.get('lastName');
            this.status = stringMap.get('status');
            this.type = stringMap.get('type');
            this.unique = stringMap.get('unique') == null ? null: Boolean.valueOf(stringMap.get('unique'));
            this.master = stringMap.get('master') == null ? null: Boolean.valueOf(stringMap.get('master'));
            this.createdDate = stringMap.get('createdDate') == null ? null: Date.valueOf(stringMap.get('createdDate'));
            this.lastLogin = stringMap.get('lastLogin') == null ? null: Date.valueOf(stringMap.get('lastLogin'));
        }

    }

}