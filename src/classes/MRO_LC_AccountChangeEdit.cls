/**
 * @author  Luca Ravicing
 * @since   Mar 19, 2020
 * @desc   Controller class for account change edit component
 *
 */
public with sharing class MRO_LC_AccountChangeEdit extends ApexServiceLibraryCnt {
    private static final MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static final MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static final MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    public static final String VAT_PREFIX='RO';


    public with sharing class initialize extends AuraCallable{
        public override Object perform (final String jsonInput){
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                if (String.isBlank(dossierId)) {
                    throw new WrtsException(System.Label.Dossier + ' - ' + System.Label.MissingId);
                }
                Account account= accountQuery.findAccount(accountId);
                Id caseId;
                List<Case> dossierCases = caseQuery.getCasesByDossierId(dossierId);
                if(!dossierCases.isEmpty()){
                    caseId = dossierCases.get(0).Id;
                }
                response.put('caseId',caseId);
                response.put('account', account);
                response.put('vatPrefix',VAT_PREFIX);
                response.put('defaultCaseOrigin',constantsSrv.CASE_DEFAULT_CASE_ORIGIN);
                response.put('defaultCaseStatus',constantsSrv.CASE_STATUS_DRAFT);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

}