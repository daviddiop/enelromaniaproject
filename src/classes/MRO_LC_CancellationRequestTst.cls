/**
 * Created by goudiaby on 26/08/2019.
 */

@IsTest
public with sharing class MRO_LC_CancellationRequestTst {
    @TestSetup
    static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SwitchIn_ELE').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, '');
        insert phaseDI010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account account = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert account;

        CancellationRequest__c cancellationRequestCase = MRO_UTL_TestDataFactory.cancellationRequest().newCancellationRequestBuilder('Case').build();
        insert cancellationRequestCase;

        CancellationRequest__c cancellationRequestDossier = MRO_UTL_TestDataFactory.cancellationRequest().newCancellationRequestBuilder('Dossier').build();
        insert cancellationRequestDossier;

        Opportunity opportunity = MRO_UTL_TestDataFactory.opportunity().createOpportunity().build();
        opportunity.AccountId = account.Id;
        opportunity.LostReason__c = 'Request another offer';
        insert opportunity;
        Case myCase = TestDataFactory.caseRecordBuilder().newCase().build();
        myCase.EffectiveDate__c = Date.today().addDays(-10);
        myCase.RecordTypeId = recordTypeSwiEle;
        insert myCase;
        Dossier__c dossier = TestDataFactory.Dossier().build();
        dossier.Opportunity__c = opportunity.Id;
        insert dossier;
    }

    @IsTest
    static void checkConditionsTest() {
        Case myCase = [SELECT Id,Phase__c FROM Case LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => myCase.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'CheckConditions', inputJSON, true);
        Map<String, Object> isCancellable = (Map<String, Object>) response;
        System.assertEquals(true, isCancellable != null);
        Test.stopTest();
    }

    @IsTest
    static void checkConditionsExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'CheckConditions', inputJSON, false);
        System.assertEquals(true, response != null);
        Test.stopTest();
    }


    @IsTest
    static void getCancellationReasonsTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'objectName' => 'Case',
                'recordType' => 'SwitchIn',
                'originPhase' => 'RE010'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'GetCancellationReasons', inputJSON, true);
        Map<String, Object> createdOptionList = (Map<String, Object>) response;
        System.assertEquals(true, createdOptionList.get('listCancellationReasons') != null);
        Test.stopTest();
    }

    @IsTest
    static void getCancellationReasonsExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'objectName' => 'Case',
                'recordType' => 'B',
                'originPhase' => 'RE010'
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'GetCancellationReasons', inputJSON, false);
            Map<String, Object> createdOptionList = (Map<String, Object>) response;
            System.assertEquals(true, createdOptionList.get('warn') != null);
            Test.stopTest();
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
    }

    @IsTest
    static void updateSobjectToCanceledCaseTest() {
        Case myCase = [SELECT Id,Phase__c,RecordTypeId,RecordType.DeveloperName FROM Case WHERE Status = 'New' LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => myCase.Id,
                'objectName' => 'Case',
                'cancellationReason' => 'Reason 1'
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'UpdateSobjectCancellation', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assert(result.get('isSuccess') == true, 'Why not? ' + result);
    }

    @IsTest
    static void updateSobjectNoTransitionTest() {
        Case myCase = [SELECT Id,Phase__c,RecordTypeId,RecordType.DeveloperName FROM Case WHERE Status = 'New' LIMIT 1];
        myCase.Phase__c = 'DI010';
        update myCase;
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => myCase.Id,
                'objectName' => 'Case',
                'cancellationReason' => 'Retention accepted'
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'UpdateSobjectCancellation', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assert(result.get('error') != null, 'Why not? ' + result);
    }

    @IsTest
    static void updateSobjectToCanceledCaseExceptionTest() {
        Case myCase = [SELECT Id,Phase__c,Status, RecordTypeId,RecordType.DeveloperName FROM Case WHERE Status = 'New' LIMIT 1];
        myCase.Status = 'Canceled';
        update myCase;
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => myCase.Id,
                'objectName' => 'Case',
                'cancellationReason' => 'Retention accepted'
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'UpdateSobjectCancellation', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') != null);
    }


    @IsTest
    static void updateSobjectToCanceledDossierTest() {
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'objectId' => dossier.Id,
                'objectName' => 'Dossier__c',
                'cancellationReason' => 'Request another offer'
        };
        Test.startTest();
        TestUtils.exec('MRO_LC_CancellationRequest', 'UpdateSobjectCancellation', inputJSON, true);
        Test.stopTest();
    }

    @IsTest
    static void getFieldsetTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'objectName' => 'Case'
        };
        Object response = TestUtils.exec(
                'MRO_LC_CancellationRequest', 'GetFieldset', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result != null);
        Test.stopTest();
    }

    @IsTest
    static void initializeExceptionTest() {
        Test.startTest();
        try {
            Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'UpdateSobjectCancellation', null, false);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    static void updateCancellationReasonsTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opp.Id,
            'cancellationDetails' => 'Details  for cancelling',
            'cancellationReason' => 'Request another offer'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'updateCancellationReasons', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    static void updateCancellationReasonsExceptionTest(){
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => 'oppId',
            'cancellationDetails' => 'Details  for cancelling',
            'cancellationReason' => 'Request another offer'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CancellationRequest', 'updateCancellationReasons', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }
}