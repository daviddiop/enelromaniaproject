/**
 * Created by goudiaby on 12/09/2019.
 */
/**
* @description Class controller for Contract components
* @author Baba Goudiaby
* @date 12-09-2019
 */
public with sharing class ContractCnt extends ApexServiceLibraryCnt{
    private static ContractQueries contractQuery = ContractQueries.getInstance();

    /**
     * get Active Contracts by Account
     */
    public class getContractsByAccount extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String companyDivisionId = params.get('companyDivisionId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            List<Contract> contracts = contractQuery.getActiveContractsByAccount(accountId,companyDivisionId);

            response.put('contracts', contracts);
            response.put('error', false);
            return response;
        }
    }
}