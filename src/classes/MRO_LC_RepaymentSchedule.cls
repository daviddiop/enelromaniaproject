/**
 * Created by Vlad Mocanu on 13/11/2019.
 * Updated by Goudiaby on 23/03/2020
 */

public with sharing class MRO_LC_RepaymentSchedule extends ApexServiceLibraryCnt {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static  MRO_SRV_Case mroSrvCase =  MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    //static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('GenericRequest').getRecordTypeId();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();

    private static MRO_QR_FileMetadata fileMetadataQuery = MRO_QR_FileMetadata.getInstance();
    private static MRO_SRV_FileMetadata fileMetadataSrv = MRO_SRV_FileMetadata.getInstance();
    static MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String genericRequestId = params.get('genericRequestId');
            String companyDivisionId = '';
            String creditManagementId = [SELECT Id FROM Group WHERE DeveloperName = 'CreditManagementQueue' AND Type = 'Queue' LIMIT 1].Id;
            Map<String, Object> response = new Map<String, Object>();
            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                    /*if (String.isNotBlank(dossierId)) {
                        Dossier__c dossierCheckParentId = dossierQuery.getById(dossierId);

                        if (String.isNotBlank(dossierCheckParentId.Parent__c)) {
                            dossierId = dossierCheckParentId.Id;
                        } else if (dossierCheckParentId.RecordType.DeveloperName == 'GenericRequest') {
                            genericRequestId = dossierId;
                            dossierId = '';
                        }
                    }*/


                Dossier__c dossier;
                if (String.isBlank(dossierId)) {
                    dossier = new Dossier__c ();
                    List<CustomerInteraction__c> customerInteractions = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);
                    if (!customerInteractions.isEmpty()) {
                        dossier.CustomerInteraction__c = customerInteractions[0].Id;
                        dossier.Origin__c = customerInteractions[0].Interaction__r.Origin__c;
                        dossier.Channel__c = customerInteractions[0].Interaction__r.Channel__c;
                    }
                    dossier.Account__c = accountId;
                    dossier.RecordTypeId = dossierRecordType;
                    dossier.Status__c = 'Draft';
                    dossier.RequestType__c = 'RepaymentSchedule';

                    databaseSrv.insertSObject(dossier);
                    dossierId = dossier.Id;
                } else {
                    dossier = dossierQuery.getById(dossierId);
                }

                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }

                List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
                Case caseRecord = new Case();
                if (cases.size() != 0) {
                    caseRecord = cases[0];
                }

                String code = 'RepaymentWizardCodeTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }
                Date staggerStartDate = System.today();
                for (Integer i = 0; i < 10;) {
                    while (MRO_UTL_Date.isNotWorkingDate(staggerStartDate)) {
                        staggerStartDate = staggerStartDate.addDays(1);
                    }
                    staggerStartDate = staggerStartDate.addDays(1);
                    i++;
                }


                List<Supply__c> listSuppliesLatePayment = supplyQuery.listSuppliesByAccountId(accountId);
                response.put('listSupplies', listSuppliesLatePayment);
                response.put('repaymentRequest', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('RepaymentRequest').getRecordTypeId());
                response.put('caseRecord', caseRecord);
                response.put('dossierId', dossierId);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('accountId', accountId);
                response.put('creditManagementId', creditManagementId);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionCode', dossier.CompanyDivision__r.Code__c);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('staggerStartDate', staggerStartDate);
                response.put('error', false);
                System.debug('response = ' + response);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CreateCaseInput createCaseParams = (CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);
            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> repaymentRequestRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('RepaymentRequest');
            String creditManagementId = [SELECT Id FROM Group WHERE DeveloperName = 'CreditManagementQueue' AND Type = 'Queue' LIMIT 1].Id;
            List<Case> newCases = new List<Case>();
            String recordTypeId;

            response.put('error', false);
            try {
                if ((createCaseParams.totalAmount > 0)
                    && (createCaseParams.staggerStartDate != null)
                    && (createCaseParams.numberOfInstallments > 0)
                    && (String.isNotBlank(createCaseParams.listOfInvoices))
                    && (String.isNotBlank(createCaseParams.accountId))
                    && (String.isNotBlank(createCaseParams.dossierId))) {
                    Date expirationDate = System.today();
                    for (Integer i = 0; i < 15; i++) {
                        while (MRO_UTL_Date.isNotWorkingDate(expirationDate)) {
                            expirationDate = expirationDate.addDays(1);
                        }
                        expirationDate = expirationDate.addDays(1);
                    }

                    System.debug(' expirationDate expirationDate : ' + expirationDate);

                    Boolean isAutomaticRequest = createCaseParams.isAutomatedRequest;
                    System.debug('is Automatic  ' + isAutomaticRequest);
                    recordTypeId = repaymentRequestRecordTypes.get('RepaymentRequest');
                    Case repaymentScheduleCase = new Case(
                            Amount__c = createCaseParams.totalAmount,
                            StartDate__c = createCaseParams.staggerStartDate,
                            Count__c = createCaseParams.numberOfInstallments,
                            MarkedInvoices__c = createCaseParams.listOfInvoices,
                            AccountId = createCaseParams.accountId,
                            RecordTypeId = recordTypeId,
                            Dossier__c = createCaseParams.dossierId,
                            Status = 'New',
                            Channel__c = createCaseParams.selectedChannel,
                            Origin = createCaseParams.selectedOrigin,
                            Frequency__c = createCaseParams.rateFrequency,
                            CustomerNotes__c = createCaseParams.notes,
                            SLAExpirationDate__c = expirationDate,
                            EffectiveDate__c = System.today(),
                            CompanyDivision__c = createCaseParams.companyDivisionId

                    );
                    System.debug(' repaymentScheduleCase : ' + repaymentScheduleCase);
                    if (String.isNotBlank(createCaseParams.caseId)) {
                        repaymentScheduleCase.Id = createCaseParams.caseId;
                    }
                    if (isAutomaticRequest != null && !isAutomaticRequest) {
                        repaymentScheduleCase.Status = 'Draft';
                        repaymentScheduleCase.OwnerId = creditManagementId;
                    }

                    String accid = createCaseParams.accountId;
                    Supply__c supply = supplyQuery.getByAccountIdInElectricAndGas(accid);
                    if(supply != null){
                            if(supply.ServicePoint__r.Distributor__c != null) {
                                repaymentScheduleCase.Distributor__c = supply.ServicePoint__r.Distributor__c;
                            }
                    }

                    Database.DMLOptions dmo = new Database.DMLOptions();
                    dmo.assignmentRuleHeader.useDefaultRule = true;
                    repaymentScheduleCase.setOptions(dmo);
                    Boolean isUpdateCase, isUpdateDossier;

                    newCases.add(repaymentScheduleCase);
                    if (!newCases.isEmpty()) {
                        isUpdateCase = databaseSrv.upsertSObject(newCases);
                    }
                    Dossier__c dossier = new Dossier__c (
                        Id = createCaseParams.dossierId,
                        Status__c = 'New',
                        Channel__c = createCaseParams.selectedChannel,
                        Origin__c = createCaseParams.selectedOrigin
                    );

                    if (isAutomaticRequest != null && !isAutomaticRequest) {
                        dossier.Status__c = 'Draft';
                        //dossier.OwnerId = creditManagementId;
                    }
                    if (dossier != null) {
                       isUpdateDossier = databaseSrv.upsertSObject(dossier);
                    }
                   /* List<MRO_LC_FisaInquiry.Fisa> fisaData  =(List<MRO_LC_FisaInquiry.Fisa>)JSON.deserialize(createCaseParams.selectedfisaData, List<MRO_LC_FisaInquiry.Fisa>.class);
                    List<DocumentType__c> docType = fileMetadataQuery.getDocumentTypes('Fisa');
                    String idDocType = '';
                    if(!docType.isEmpty()){
                        idDocType = docType[0].Id;
                    }
                    if (!fisaData.isEmpty()) {
                        fileMetadataSrv.insertFileMetadaForFisa('Fisa',fisaData,idDocType,dossier, newCases);
                    }*/
                    System.debug('dossier = ' + dossier);
                    System.debug('createCaseParams.dossierId = ' + createCaseParams.dossierId);
                    if (isUpdateDossier) {
                        mroSrvCase.applyAutomaticTransitionOnDossierAndCases(dossier.Id);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class createDraftCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CreateCaseInput createCaseParams = (CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);
            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> repaymentRequestRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('RepaymentRequest');
            List<Case> newCases = new List<Case>();
            String recordTypeId;

            response.put('error', false);
            List<Case> listCases = new List<Case>();

            try {
                if ((createCaseParams.totalAmount > 0)
                    && (createCaseParams.staggerStartDate != null)
                    && (createCaseParams.numberOfInstallments > 0)
                    && (String.isNotBlank(createCaseParams.listOfInvoices))
                    && (String.isNotBlank(createCaseParams.accountId))
                    && (String.isNotBlank(createCaseParams.dossierId))
                    ) {
                    Date expirationDate = System.today();
                    for (Integer i = 0; i < 15; i++) {
                        while (MRO_UTL_Date.isNotWorkingDate(expirationDate)) {
                            expirationDate = expirationDate.addDays(1);
                        }
                        expirationDate = expirationDate.addDays(1);
                    }
                    recordTypeId = repaymentRequestRecordTypes.get('RepaymentRequest');
                    Case repaymentScheduleCase = new Case(
                            Amount__c = createCaseParams.totalAmount,
                            StartDate__c = createCaseParams.staggerStartDate,
                            Count__c = createCaseParams.numberOfInstallments,
                            MarkedInvoices__c = createCaseParams.listOfInvoices,
                            AccountId = createCaseParams.accountId,
                            RecordTypeId = recordTypeId,
                            Dossier__c = createCaseParams.dossierId,
                            Status = 'Draft',
                            Channel__c = createCaseParams.selectedChannel,
                            Origin = createCaseParams.selectedOrigin,
                            Frequency__c = createCaseParams.rateFrequency,
                            CustomerNotes__c = createCaseParams.notes,
                            SLAExpirationDate__c = expirationDate,
                            EffectiveDate__c = System.today(),
                            CompanyDivision__c = createCaseParams.companyDivisionId

                    );
                    System.debug('repaymentScheduleCase: ' + repaymentScheduleCase);
                    if (String.isNotBlank(createCaseParams.caseId)) {
                        repaymentScheduleCase.Id = createCaseParams.caseId;
                    }

                    String accid = createCaseParams.accountId;
                    Supply__c supply = supplyQuery.getByAccountIdInElectricAndGas(accid);
                    if(supply != null){
                            if(supply.ServicePoint__r.Distributor__c != null) {
                                repaymentScheduleCase.Distributor__c = supply.ServicePoint__r.Distributor__c;
                            }
                    }

                    Database.DMLOptions dmo = new Database.DMLOptions();

                    dmo.assignmentRuleHeader.useDefaultRule = true;
                    repaymentScheduleCase.setOptions(dmo);
                    newCases.add(repaymentScheduleCase);
                    if (!newCases.isEmpty()) {
                        System.debug('newCases: ' + newCases);
                        databaseSrv.upsertSObject(newCases);
                    }
                    if(repaymentScheduleCase != null){
                        listCases.add(repaymentScheduleCase);
                    }
                }
                Dossier__c dossier = new Dossier__c (
                    Id = createCaseParams.dossierId,
                    Status__c = 'Draft',
                    Channel__c = createCaseParams.selectedChannel,
                    Origin__c = createCaseParams.selectedOrigin
                );
                if (dossier != null) {
                    databaseSrv.upsertSObject(dossier);
                }

                /*
                List<MRO_LC_FisaInquiry.Fisa> listFisas  =(List<MRO_LC_FisaInquiry.Fisa>)JSON.deserialize(createCaseParams.listOfInvoices, List<MRO_LC_FisaInquiry.Fisa>.class);
                List<DocumentType__c> docType = fileMetadataQuery.getDocumentTypes('Fisa');
                String idDocType = '';
                if(!docType.isEmpty()){
                    idDocType = docType[0].Id;
                }

                if (!listFisas.isEmpty()) {
                    fileMetadataSrv.insertFileMetadaForFisa('Fisa',listFisas,idDocType,dossier, listCases);
                } */

                System.debug('dossier = ' + dossier);
                System.debug('createCaseParams.dossierId = ' + createCaseParams.dossierId);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class cancelRepayment extends AuraCallable {
        public override Object perform(final String jsonInput) {
            String dossierId = asMap(jsonInput).get('dossierId');
            String caseId = asMap(jsonInput).get('caseId');
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (String.isNotBlank(dossierId)) {
                    Dossier__c dossier = new Dossier__c (id = dossierId, Status__c = 'Canceled');
                    if (dossier != null) {
                        databaseSrv.upsertSObject(dossier);
                    }
                }
                if (String.isNotBlank(caseId)) {
                    Case caseRecord = new Case (Id = caseId, Status = 'Canceled', Phase__c = 'RC010');
                    if (caseRecord != null) {
                        databaseSrv.upsertSObject(caseRecord);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class getCompanyDivisionByPaymentCode extends AuraCallable {
        public override Object perform(final String jsonInput) {
            String paymentCode = asMap(jsonInput).get('paymentCode');
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (String.isBlank(paymentCode)) {
                    throw new WrtsException(System.Label.PaymentCode + ' ' + System.Label.IsMissing);
                }
                ContractAccount__c contractAccount = contractAccountQuery.getByBillingAccountNumber(paymentCode);
                if (contractAccount != null) {
                    response.put('companyDivisionId', contractAccount.CompanyDivision__c);
                    response.put('error', false);

                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public class updateDossierCompanyDivision extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String companyDivisionId = params.get('companyDivisionId');
            if(String.isNotBlank(dossierId) && String.isNotBlank(companyDivisionId)){
                dossierSrv.updateCompanyDivisionByDossierId(dossierId,companyDivisionId);
            }
            response.put('error', false);
            return response;
        }
    }

    public class CreateCaseInput {
        @AuraEnabled
        public Double totalAmount { get; set; }
        @AuraEnabled
        public Date staggerStartDate { get; set; }
        @AuraEnabled
        public Integer numberOfInstallments { get; set; }
        @AuraEnabled
        public String listOfInvoices { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String selectedChannel { get; set; }
        @AuraEnabled
        public String selectedOrigin { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String companyDivisionId { get; set; }
        @AuraEnabled
        public Boolean isAutomatedRequest { get; set; }
        @AuraEnabled
        public Integer SLAWorkingDays { get; set; }
        @AuraEnabled
        public String notes { get; set; }
        @AuraEnabled
        public String rateFrequency { get; set; }
        @AuraEnabled
        public String selectedfisaData { get; set; }
    }

}