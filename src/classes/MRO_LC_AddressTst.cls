/**
*
* Test class for normalisation
* @author Mario Spagnuolo
* @date 09/09/2019
*/

@IsTest
private class MRO_LC_AddressTst {
    @testsetup static void setup() {
        City__c city = new City__c(Name = 'JIMBOLIA', Province__c = 'TIMIS', Country__c = 'ROMANIA');
        insert city;
        Street__c[] streets = new Street__c[]{
                new Street__c(Name = 'ANTOL', City__c = city.Id, StreetType__c = 'PIATA', PostalCode__c = '123',
                        ToStreetNumber__c = 100, FromStreetNumber__c = 1),
                new Street__c(Name = 'BUCARES', City__c = city.Id, StreetType__c = 'STRADA', PostalCode__c = '123',
                        ToStreetNumber__c = 100, FromStreetNumber__c = 1)

        };
        insert streets;        
        Street__c street = new Street__c(Name = 'ANTOL1', City__c = city.Id, StreetType__c = 'PIATA', PostalCode__c = '123',
                        ToStreetNumber__c = 1000, Verified__c=true, FromStreetNumber__c = 1);
        insert street;

    }
    
    @isTest
    static void getValuesTest(){
        Map<String, String> address = new Map<String, String>{
             'City' => 'JIMBOLIA',
             'Province' => 'TIMIS',
             'Country' => 'ROMANIA'
        };            
        String fieldName = 'city';
        MRO_LC_Address.getValues(fieldName, address);
    }
    
    @isTest
    static void normalizeTest(){
       Map<String, String> address = new Map<String, String>();
        address.put('streetName', 'ANTOL');
        address.put('postalCode', '123');
        address.put('streetType', 'PIATA');
        address.put('streetNumber', '100');
        address.put('city', 'JIMBOLIA');
        address.put('province', 'TIMIS');
        address.put('country', 'ROMANIA');
		Map<String, String> jsons = new Map<String, String>();
        jsons.put('address',JSON.serialize(address));
        
        MRO_LC_Address.normalize normal = new MRO_LC_Address.normalize();
        try{
        	normal.perform(JSON.serialize(jsons));
        }catch(Exception e){
            system.debug('message error: '+ e.getMessage()+'	 ' +e.getStackTraceString());
        }
    }
    
    @IsTest
    static void completeAddressTest() {
        Map<String, Object> address = new Map<String, Object>{
                'field' => 'province',
                'startWith' => 'TIMIS'
        };
        Map<String, String> jsons = new Map<String, String>();
        jsons.put('address',JSON.serialize(address));
        
        MRO_LC_Address.getCompletionStrings normal = new MRO_LC_Address.getCompletionStrings();
        normal.perform(JSON.serialize(jsons));

        List<String> result = MRO_LC_Address.getCompletionStrings('streetName', 'an');
        result = MRO_LC_Address.getCompletionStrings('city', 'jim');
        result = MRO_LC_Address.getCompletionStrings('postalCode', '12');
        result = MRO_LC_Address.getCompletionStrings('country', 'rom');
        result = MRO_LC_Address.getCompletionStrings('province', 'tim');
    }
    
    @isTest
    static void retrieveStreetTest(){
        Map<String, Object> address = new Map<String, Object>{
                'streetId' => [select id from Street__c limit 1][0].Id
        };
        Map<String, String> jsons = new Map<String, String>();
        jsons.put('address',JSON.serialize(address));
        
        MRO_LC_Address.retrieveStreet normal = new MRO_LC_Address.retrieveStreet();
        normal.perform(JSON.serialize(jsons));
    }
    
    @isTest
    static void getCityTest(){
        Map<String, String> address = new Map<String, String>{
                'city' => 'JIMBOLIA',
                'field' => 'province'
        };
        Map<String, String> jsons = new Map<String, String>();
        jsons.put('address',JSON.serialize(address));
        
        MRO_LC_Address.getFieldByCity normal = new MRO_LC_Address.getFieldByCity();
        normal.perform(JSON.serialize(jsons));
        
        Map<String, String> address1 = new Map<String, String>{
                'city' => 'JIMBOLIA',
                'field' => 'country'
        };
        Map<String, String> jsonss = new Map<String, String>();
        jsonss.put('address',JSON.serialize(address1));
        MRO_LC_Address.getFieldByCity normal1 = new MRO_LC_Address.getFieldByCity();
        normal1.perform(JSON.serialize(jsonss));
    }
    
    /*@isTest static void createAddress() {
        Integer n = ([
                SELECT Name
                FROM Street__c
                WHERE City__r.Name = 'JIMBOLIA' and
                City__r.Province__c = 'TIMIS' and Name = 'ANTOL' and StreetType__c = 'PIATA'
                and FromStreetNumber__c = 1 and ToStreetNumber__c = 100 and
                PostalCode__c = '123'
        ]).size();

        Map<String, String> address = new Map<String, String>();
        address.put('city', 'JIMBOLIA');
        //address.put('stateName', 'ALBA');
        //address.put('streetName', 'ANTOL');
       // address.put('postalCode', '123');
        address.put('streetType', 'PIATA');
       // address.put('evenOdd', 'B');
       // address.put('fromStreetNumber', '1');
       address.put('streetNumber', '100');
        //address.put('toStreetNumber',100);
        address.put('province', 'TIMIS');
        address.put('country', 'ROMANIA');

        Test.startTest();
        MRO_LC_Address.createAddress ca = new MRO_LC_Address.createAddress();
        Map<String, String> input = new Map<String, String>{
            'address' => JSON.serialize(address),
            'isForceAddress' => 'false'
        };
        ca.perform(JSON.serialize(input));
        Test.stopTest();

        System.assert(([SELECT Name FROM Street__c WHERE City__r.Name = 'JIMBOLIA' and City__r.Province__c = 'TIMIS' and Name = 'ANTOL' and
                        StreetType__c = 'PIATA' and FromStreetNumber__c = 1 and ToStreetNumber__c = 100
                        and PostalCode__c = '123']).size() == n);
    }

	@isTest static void createAddress2() {
        Map<String, String> address = new Map<String, String>();
        address.put('city', 'JIMBOLIA22');
        address.put('stateName', 'ALBA');
        address.put('streetName', 'ANTOL0');
        address.put('postalCode', '123');
        address.put('streetType', 'PIATA');
        address.put('evenOdd', 'B');
        address.put('fromStreetNumber', '1');
        address.put('streetNumber', '100');
        address.put('toStreetNumber', '100');
        address.put('province', 'TIMIS');
        address.put('country', 'ROMANIA');

        Test.startTest();
        MRO_LC_Address.createAddress ca = new MRO_LC_Address.createAddress();
        Map<String, String> input = new Map<String, String>{
            'address' => JSON.serialize(address),
            'isForceAddress' => 'true'
        };
        ca.perform(JSON.serialize(input));
        Test.stopTest();
    }*/
    
    @isTest
    static void createAddressPerformTest(){
        Map<String, String> address = new Map<String, String>();
        address.put('city', 'JIMBOLIA');
        address.put('stateName', 'ALBA');
        address.put('streetName', 'ANTOL3');
        address.put('postalCode', '123');
        address.put('streetType', 'PIATA1');
        address.put('evenOdd', 'B');
        address.put('fromStreetNumber', '1');
        address.put('streetNumber', '1000');
        address.put('toStreetNumber', '1000');
        address.put('province', 'TIMIS');
        address.put('country', 'ROMANIA');
        Map<String, String> jsons = new Map<String, String>();
        jsons.put('address',JSON.serialize(address));
        jsons.put('isForceAddress','false');
        
        MRO_LC_Address.createAddress normal = new MRO_LC_Address.createAddress();
        try{
        	normal.perform(JSON.serialize(jsons));
        }catch(Exception e){
            system.debug('message error: '+ e.getMessage()+'	 ' +e.getStackTraceString());
        }
    }
    
    @isTest
    static void streetToAddressDtoTest(){
       MRO_SRV_Address.AddressDTO address = new MRO_SRV_Address.AddressDTO();
        address.streetNumber = '1000';
        address.city = 'JIMBOLIA';
        address.streetName = 'ANTOL1';
        address.postalCode = '123';
        address.streetType = 'PIATA';
        address.streetNumber = '1000';
        address.province = 'TIMIS';
        address.country = 'ROMANIA';
        MRO_LC_Address.NormalizeResponse test = MRO_LC_Address.normalize(address);
    }
    @isTest
    static void streetToAddressDtoTest2(){
       MRO_SRV_Address.AddressDTO address = new MRO_SRV_Address.AddressDTO();
        address.streetNumber = '1000';
        address.city = 'JIMBOLIA';
        address.streetName = 'ANTOL12';
        address.postalCode = '123';
        address.streetType = 'PIATA';
        address.streetNumber = '1000';
        address.province = 'TIMIS';
        address.country = 'ROMANIA';
        try{
        	MRO_LC_Address.NormalizeResponse test = MRO_LC_Address.normalize(address);
        }catch(Exception e){
            system.debug(e.getStackTraceString());
        }
    }
    @isTest
    static void streetToAddressDtoTest3(){
       MRO_SRV_Address.AddressDTO address = new MRO_SRV_Address.AddressDTO();
        address.streetNumber = '1000';
        address.city = 'JIMBOLIA';
        address.streetName = 'ANTOL';
        address.postalCode = '123';
        address.streetType = 'PIATA';
        address.streetNumber = '1000';
        address.province = 'TIMIS';
        address.country = 'ROMANIA445';
        try{
        	MRO_LC_Address.NormalizeResponse test = MRO_LC_Address.normalize(address);
        }catch(Exception e){
            system.debug(e.getStackTraceString());
        }
    }
    @isTest
    static void streetToAddressDtoTest4(){
       MRO_SRV_Address.AddressDTO address = new MRO_SRV_Address.AddressDTO();
        address.streetNumber = '1000';
        address.city = 'JIMBOLIAffy';
        address.streetName = 'ANTOL';
        address.postalCode = '123';
        address.streetType = 'PIATA';
        address.streetNumber = '1000';
        address.province = 'TIMIS';
        address.country = 'ROMANIA';
        try{
        	MRO_LC_Address.NormalizeResponse test = MRO_LC_Address.normalize(address);
        }catch(Exception e){
            system.debug(e.getStackTraceString());
        }
    }
    @isTest
    static void streetToAddressDtoTest5(){
       MRO_SRV_Address.AddressDTO address = new MRO_SRV_Address.AddressDTO();
        address.streetNumber = '1000';
        address.city = 'JIMBOLIA';
        address.streetName = 'ANTOL';
        address.postalCode = '123gjh';
        address.streetType = 'PIATA';
        address.streetNumber = '1000';
        address.province = 'TIMIS';
        address.country = 'ROMANIA';
        try{
        	MRO_LC_Address.NormalizeResponse test = MRO_LC_Address.normalize(address);
        }catch(Exception e){
            system.debug(e.getStackTraceString());
        }
    }
    @isTest
    static void streetToAddressDtoTest6(){
       MRO_SRV_Address.AddressDTO address = new MRO_SRV_Address.AddressDTO();
        address.streetNumber = '1000';
        address.city = 'JIMBOLIA';
        address.streetName = 'ANTOL';
        address.postalCode = '123';
        address.streetType = 'PIATAgfd';
        address.streetNumber = '1000';
        address.province = 'TIMIS';
        address.country = 'ROMANIA';
        try{
        	MRO_LC_Address.NormalizeResponse test = MRO_LC_Address.normalize(address);
        }catch(Exception e){
            system.debug(e.getStackTraceString());
        }
    }
    @isTest
    static void streetToAddressDtoTest7(){
       MRO_SRV_Address.AddressDTO address = new MRO_SRV_Address.AddressDTO();
        address.streetNumber = '1000';
        address.city = 'JIMBOLIA';
        address.streetName = 'ANTOL';
        address.postalCode = '123';
        address.streetType = 'PIATA';
        address.streetNumber = '1000';
        address.province = 'TIMISsrty';
        address.country = 'ROMANIA';
        try{
        	MRO_LC_Address.NormalizeResponse test = MRO_LC_Address.normalize(address);
        }catch(Exception e){
            system.debug(e.getStackTraceString());
        }
    }
    
    @isTest
    static void getFieldByCity(){
        MRO_LC_Address.getProvincesByCity('JIMBOLIA');
        MRO_LC_Address.getCountriesByCity('JIMBOLIA');
        MRO_LC_Address.asMap(''); 
    }
    
    @isTest
    static void getExtraFieldsTest(){
        try{
        	MRO_LC_Address.getExtraFields('street', 'streetName,streetType,StreetNumber,postalCode');
        }catch(Exception msg){
            system.debug(msg.getStackTraceString());
        }
    }
}