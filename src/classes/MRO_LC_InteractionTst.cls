@IsTest
public with sharing class MRO_LC_InteractionTst {

    @TestSetup
    static void setup(){
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        Individual newIndividual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert newIndividual;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction()
            .createInteraction()
            .setInterlocutor(newIndividual.Id)
            .build();
        insert interaction;

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SwitchIn_ELE').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, '');
        insert phaseDI010ToRC010;
        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'MeterChangeTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        /* BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
         billingProfile.Account__c=listAccount[0].Id;
         insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Status__c = 'Draft';
        dossier.RequestType__c = 'MeterChange';
        dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            //caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterChangeEle').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;

    }

    @IsTest
    static void listOptionsTest(){
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec('MRO_LC_Interaction','listOptions',null,true);
        Test.stopTest();

        System.assertNotEquals(null, response.get('statusList'));
    }

    @IsTest
    static void findByIdTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
        ];

        Test.startTest();
        MRO_SRV_Interaction.InteractionDTO resultInteraction = (MRO_SRV_Interaction.InteractionDTO)TestUtils.exec(
        'MRO_LC_Interaction','findById',new Map<String, String>{'interactionId' => interaction.Id},true);
        System.assertEquals(resultInteraction.channel, 'Back Office Sales' );

        Object responseException = TestUtils.exec('InteractionCnt','findById',new Map<String, String>{'interactionId'=>''},false);
        System.assertEquals(responseException, System.Label.Interaction + ' - ' + System.Label.MissingId );
        Test.stopTest();
    }

    @IsTest
    static void insertInteractionTest(){
        Test.startTest();
        MRO_SRV_Interaction.InteractionDTO resultInteraction = (MRO_SRV_Interaction.InteractionDTO)TestUtils.exec(
        'MRO_LC_Interaction','insertInteraction', null, true );
        Test.stopTest();

        System.assertEquals(resultInteraction.status, 'New' );
    }

    @IsTest
    static void upsertInteractionTest(){
        MRO_SRV_Interaction.InteractionDTO interactionDTO = new MRO_SRV_Interaction.InteractionDTO();
        interactionDTO.comments = 'Test';

        Test.startTest();
        MRO_SRV_Interaction.InteractionDTO resultInteraction = (MRO_SRV_Interaction.InteractionDTO)TestUtils.exec(
        'MRO_LC_Interaction','upsertInteraction', interactionDTO, true );
        Test.stopTest();

        System.assertNotEquals(null, resultInteraction);
        System.assertEquals('Test', resultInteraction.comments);
    }

    @IsTest
    static void removeInterlocutorTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id, Name, Customer__c, Contact__c, Interaction__c,
                Customer__r.Name, Customer__r.VATNumber__c, Customer__r.IsPersonAccount,
                Customer__r.NationalIdentityNumber__pc, Customer__r.PersonContactId, Interaction__r.Interlocutor__c
            FROM CustomerInteraction__c
            WHERE Interaction__c = :interaction.Id
        ];
        delete  customerInteraction;

        Test.startTest();
        MRO_SRV_Interaction.InteractionDTO resultInteraction = (MRO_SRV_Interaction.InteractionDTO)TestUtils.exec(
        'MRO_LC_Interaction','removeInterlocutor',new Map<String, String>{'interactionId'=>interaction.Id},true);
        Interaction__c updatedInteraction = [
            SELECT Id, Interlocutor__c
            FROM Interaction__c
            WHERE Id = :interaction.Id
        ];
        System.assertEquals(null, updatedInteraction.Interlocutor__c);

        String error = (String)TestUtils.exec('MRO_LC_Interaction','removeInterlocutor',new Map<String, String>{'interactionId'=>''},false);
        System.assertEquals(System.Label.Interaction + ' - ' + System.Label.MissingId, error);
        Test.stopTest();
    }

    @IsTest
    static void removeInterlocutorEmptyIdTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id, Name, Customer__c, Contact__c, Interaction__c,
                Customer__r.Name, Customer__r.VATNumber__c, Customer__r.IsPersonAccount,
                Customer__r.NationalIdentityNumber__pc, Customer__r.PersonContactId, Interaction__r.Interlocutor__c
            FROM CustomerInteraction__c
            WHERE Interaction__c = :interaction.Id
        ];
        delete  customerInteraction;

        Test.startTest();
        MRO_SRV_Interaction.InteractionDTO resultInteraction = (MRO_SRV_Interaction.InteractionDTO)TestUtils.exec(
            'MRO_LC_Interaction','removeInterlocutor',new Map<String, String>{'interactionId'=>interaction.Id},true);
        Test.stopTest();

        System.assertEquals(null, resultInteraction.interlocutorFirstname);
        System.assertEquals(' ', resultInteraction.getInterlocutorFullname());
        System.assertEquals(false, resultInteraction.getIsClosed());
    }

    @IsTest
    static void removeInterlocutorWithCustomerInteractionTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        /*
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert businessAccount;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        insert contact;
        insert MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, businessAccount.Id, contact.Id).build();
*/
        Test.startTest();
        MRO_SRV_Interaction.InteractionDTO resultInteraction = (MRO_SRV_Interaction.InteractionDTO)TestUtils.exec(
            'MRO_LC_Interaction','findById',new Map<String, String>{'interactionId'=>interaction.Id},true);
        System.assertNotEquals(null, resultInteraction.customerId);
        System.assertNotEquals(null, resultInteraction.customerInteractionIds);

        String error = (String)TestUtils.exec(
            'MRO_LC_Interaction','removeInterlocutor',new Map<String, String>{'interactionId'=>interaction.Id},false);
        System.assertEquals(Label.InterlocutorRemoveDenied, error);
        Test.stopTest();
    }

    @IsTest
    static void getGenericRequestInformationTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        /*
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert businessAccount;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        insert contact;
        insert MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, businessAccount.Id, contact.Id).build();
*/
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Interaction','getGenericRequestInformation',new Map<String, String>{'interactionId'=>interaction.Id},true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('hasKAM') != null);

        Test.stopTest();
    }

    @IsTest
    static void createGenericRequestDossierTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        /*
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert businessAccount;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        insert contact;
        insert MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, businessAccount.Id, contact.Id).build();
*/
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Interaction','createGenericRequestDossier',new Map<String, String>{'interactionId'=>interaction.Id},true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();
    }

    @IsTest
    static void updateGenericRequestDossierTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'comments' => 'comments',
                'SLAExpirationDate' => JSON.serialize(System.today()),
                'assignmentProvince' => 'assignmentProvince',
                'queue' => 'Developer'
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Interaction','updateGenericRequestDossier',inputJSON,true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    static void saveInstitutionInterlocutorTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Institution').getRecordTypeId();
        update account;
        Map<String, String > inputJSON = new Map<String, String>{
                'interactionId' => interaction.Id,
                'institutionAccountId' => account.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Interaction','saveInstitutionInterlocutor',inputJSON,true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId();
        update account;

        inputJSON = new Map<String, String>{
            'interactionId' => interaction.Id,
            'institutionAccountId' => account.Id
        };
         response = TestUtils.exec(
            'MRO_LC_Interaction','saveInstitutionInterlocutor',inputJSON,true);
        result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    static void saveInterlocutorTypeTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Institution').getRecordTypeId();
        update account;
        Map<String, String > inputJSON = new Map<String, String>{
                'interactionId' => interaction.Id,
                'interlocutorType' => 'Institution'
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Interaction','saveInterlocutorType',inputJSON,true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        inputJSON = new Map<String, String>{
            'interactionId' => interaction.Id,
            'interlocutorType' => 'skjdd'
        };
         response = TestUtils.exec(
            'MRO_LC_Interaction','saveInterlocutorType',inputJSON,true);
        result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }
}