/**
 * Created by moustapha on 31/08/2020.
 */

@IsTest
private class MRO_LC_IndividualTst {
    @TestSetup
    static void setup() {
        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        individual.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert individual;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        //interaction.Interlocutor__c = individual.Id;
        interaction.Channel__c = 'MyEnel';
        insert interaction;
    }

    @IsTest
    static void saveIndividualTest() {

        String interactionId = [
                SELECT id
                FROM Interaction__c
                LIMIT 1
        ].Id;
        Individual individual = new Individual();

        MRO_SRV_Individual.Interlocutor individualDto = new MRO_SRV_Individual.Interlocutor();
        individualDto.id = individual.Id;
        individualDto.firstName = 'ameth';
        individualDto.lastName = 'seck';
        individualDto.nationalId = '1800101221144';
        individualDto.ForeignCitizen = false;
        Map<String, String> params = new Map<String, String>{
                'interactionId' => interactionId,
                'interlocutorDTO' => JSON.serialize(individualDto)
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_Individual','saveIndividual', params,true);

        Test.stopTest();
        System.debug('#-response1-# '+response);
        System.assert(null != response);
    }
    @IsTest
    static void updateIndividualTest() {

        String interactionId = [
                SELECT id
                FROM Interaction__c
                LIMIT 1
        ].Id;
        Individual individual = [
                SELECT id,NationalIdentityNumber__c
                FROM Individual
                LIMIT 1
        ];

        MRO_SRV_Individual.Interlocutor individualDto = new MRO_SRV_Individual.Interlocutor();
        individualDto.id = individual.Id;
        individualDto.firstName = 'Tapha';
        individualDto.lastName = 'Diouf';
        individualDto.nationalId = individual.NationalIdentityNumber__c;
        Map<String, String> params = new Map<String, String>{
                'interactionId' => interactionId,
                'interlocutorDTO' => JSON.serialize(individualDto)
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_Individual','updateIndividual', params,true);

        Test.stopTest();
        System.debug('#-response2-# '+response);
        System.assert(null != response);
    }
@IsTest
    static void updateInteractionTest() {

        String interactionId = [
                SELECT id
                FROM Interaction__c
                LIMIT 1
        ].Id;
        String individualId = [
                SELECT id
                FROM Individual
                LIMIT 1
        ].Id;

        Map<String, String> params = new Map<String, String>{
                'interactionId' => interactionId,
                'individualId' => individualId
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_Individual','updateInteraction', params,true);

        Test.stopTest();
        System.debug('#-response3-# '+response);
        System.assert(null != response);
    }
    @IsTest
    static void searchInterlocutorTest() {

        Interaction__c interaction = [
                SELECT id,Origin__c
                FROM Interaction__c
                LIMIT 1
        ];
        System.debug('#-# '+interaction);
        Map<String, String> params = new Map<String, String>{
                'interaction' => JSON.serialize(interaction)
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_Individual','searchInterlocutor', params,true);

        Test.stopTest();

        System.assert(null != response);
    }
    @IsTest
    static void searchContactTest() {

        Interaction__c interaction = [
                SELECT id,Origin__c
                FROM Interaction__c
                LIMIT 1
        ];
        System.debug('#-# '+JSON.serialize(interaction));
        Map<String, String> params = new Map<String, String>{
                'interaction' => JSON.serialize(interaction)
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_Individual','searchContact', params,true);

        Test.stopTest();

        System.assert(null != response);
    }
    @IsTest
    static void initDataTest() {


        Map<String, String> params = new Map<String, String>{
                'interaction' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_Individual','initData', params,true);

        Test.stopTest();
        System.debug('#-response3-# '+response);
        System.assert(null != response);
    }
}