/**
 * @author  Alessandro 
 * @since   dec 09, 2019
 * @desc   MRO_TR_FileMetadataHandler for the FileMetadata__c object
 * @history 
 */

global with sharing class MRO_TR_FileMetadataHandler implements TriggerManager.ISObjectTriggerHandler {
    private static MRO_QR_Dossier mroQrDossier = MRO_QR_Dossier.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    global void afterDelete() {}

    global void afterInsert() {
        if (!Test.isRunningTest()) {
           processFileMetadata(Trigger.new);
           handlesCallout(Trigger.new);
        }
    }

    global void afterUndelete() {}

    global void afterUpdate() {
        List<FileMetadata__c> paidInvoices = new List<FileMetadata__c>();
        Map<Id, FileMetadata__c> candidateDocumentDiscoToClose = new Map<Id, FileMetadata__c> ();
        for(FileMetadata__c fileMetadata : (List<FileMetadata__c>)Trigger.new) {
            if (fileMetadata.PaymentDate__c != null && Trigger.oldMap.get(fileMetadata.Id).get('PaymentDate__c') == null) {
                paidInvoices.add(fileMetadata);
            }

            if (fileMetadata.DiscoFileId__c != null && Trigger.oldMap.get(fileMetadata.Id).get('DiscoFileId__c') == null) {
                candidateDocumentDiscoToClose.put(fileMetadata.Id,fileMetadata);
            }

            Set<String> documentIdUpdatedFMIds = new Set<String>();
            if((fileMetadata.ExternalId__c != null || fileMetadata.DiscoFileId__c != null) &&
                    String.isNotBlank(fileMetadata.DocumentId__c) &&
                    fileMetadata.DocumentId__c != ((Map<Id, FileMetadata__c>)Trigger.oldMap).get(fileMetadata.Id).DocumentId__c) {

                documentIdUpdatedFMIds.add(fileMetadata.Id);
            }
            if(!documentIdUpdatedFMIds.isEmpty()) {

                DocumentManagementSystemEvent__e dmsEvent = new DocumentManagementSystemEvent__e(
                        FileMetadataIds__c = String.join(new List<String>(documentIdUpdatedFMIds), ';')
                );
                // Call method to publish events
                Database.SaveResult sr = EventBus.publish(dmsEvent);
                // Inspect publishing result
                if (sr.isSuccess()) {

                    System.debug('Successfully published event.');
                } else {

                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Error returned: ' +
                                err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }
            }
        }

        this.handlePaidInvoices(paidInvoices);
        if (!candidateDocumentDiscoToClose.isEmpty()){
            this.handleDocumentsDiscoActivitiesToClose(candidateDocumentDiscoToClose);
        }
    }

    global void beforeDelete() {}

    global void beforeInsert() {}
    
    global void beforeUpdate() {
        for (FileMetadata__c fm : (List<FileMetadata__c>) Trigger.new) {
            FileMetadata__c oldFm = (FileMetadata__c)Trigger.oldMap.get(fm.Id);
            if (fm.PrintAction__c != null && fm.DocumentId__c != null
                && fm.ExternalId__c != null && fm.ExternalId__c != oldFm.ExternalId__c && !(oldFm.ExternalId__c != null && !oldFm.ExternalId__c.startsWith('{'))) {
                fm.DocumentId__c = null;
            }
        }
    }

    private void processFileMetadata(List<FileMetadata__c> newList) {
        List<wrts_prcgvr__Activity__c> activities= new List<wrts_prcgvr__Activity__c >();
        Set<Id> caseIds = new Set<Id>();
        Set<Id> dossierIds = new Set<Id>();
        for (FileMetadata__c file: newList) {
            if (file.PrintAction__c == null) {
                if (file.Case__c != null) {
                    caseIds.add(file.Case__c);
                } else if (file.Dossier__c != null) {
                    dossierIds.add(file.Dossier__c);
                }
            }
        }
        Map<Id, Case> casesMap = caseIds.isEmpty() ? new Map<Id, Case>() : new Map<Id, Case>(MRO_QR_Case.getInstance().listByIdsForActivities(caseIds));
        Map<Id, Dossier__c> dossiersMap = dossierIds.isEmpty() ? new Map<Id, Dossier__c>() : mroQrDossier.getByIds(dossierIds);
        for (FileMetadata__c file: newList) {
            if (file.PrintAction__c == null) {
                if (file.Case__c != null) {
                    Case c = casesMap.get(file.Case__c);
                    wrts_prcgvr__Activity__c activity = new wrts_prcgvr__Activity__c(
                        Type__c = constantsSrv.ACTIVITY_FILE_CHECK_TYPE,
                        wrts_prcgvr__ObjectId__c = file.Id,
                        Case__c = file.Case__c,
                        wrts_prcgvr__Phase__c = c.Phase__c
                    );
                    activities.add(activity);
                } else if (file.Dossier__c != null) {
                    Dossier__c dossier = dossiersMap.get(file.Dossier__c);
                    if (dossier.RecordType.DeveloperName != 'GenericRequest') {
                        wrts_prcgvr__Activity__c activity = new wrts_prcgvr__Activity__c(
                            Type__c = constantsSrv.ACTIVITY_FILE_CHECK_TYPE,
                            wrts_prcgvr__ObjectId__c = file.Id,
                            Dossier__c = file.Dossier__c,
                            wrts_prcgvr__Phase__c = dossier.Phase__c
                        );
                        activities.add(activity);
                    }
                }
            }
         }
         upsert activities;  
     }

    private void handlesCallout(List<FileMetadata__c> newList){
        ((wrts_prcgvr.Interfaces_1_0.ICalloutUtils) wrts_prcgvr.VersionManager.newClassInstance('CalloutUtils'))
                .bulkSend(new Map<String, Object>{
                'newObjects' => newList,
                'oldObjects' => newList
        });
    }

    private void handlePaidInvoices(List<FileMetadata__c> paidInvoices) {
        if (!paidInvoices.isEmpty()) {
            Set<Id> caseIds = new Set<Id>();
            for (FileMetadata__c fm : paidInvoices) {
                if (fm.Case__c != null) {
                    caseIds.add(fm.Case__c);
                }
            }
            List<Case> cases = MRO_QR_Case.getInstance().listByIds(caseIds);
            MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
            Set<String> recordTypeToCheck = new Set<String>();
            recordTypeToCheck.add('Connection_ELE');
            recordTypeToCheck.add('MeterChangeEle');
            recordTypeToCheck.add('TechnicalDataChange_ELE');
            for (Case c : cases) {
                if (recordTypeToCheck.contains(c.RecordType.DeveloperName)) {
                    transitionsUtl.checkAndApplyAutomaticTransitionWithTag(c, constantsSrv.DISCO_TAG);
                }
            }
        }
    }

    private void handleDocumentsDiscoActivitiesToClose(Map<Id, FileMetadata__c> candidateDocumentDiscoToClose) {
        Map<Id, Dossier__c> dossierToProcess = new Map<Id, Dossier__c>();
        System.debug(' handleDocumentsDiscoActivitiesToClose '+candidateDocumentDiscoToClose);

        Map<Id, FileMetadata__c> mapDocumentDiscoToProcessByDossier = new Map<Id, FileMetadata__c>();
        Map<Id, FileMetadata__c> documentDiscoToProcess = new Map<Id, FileMetadata__c>();
        if (!candidateDocumentDiscoToClose.isEmpty()) {
            Set<Id> dossierIds = new Set<Id>();
            for (FileMetadata__c fm : candidateDocumentDiscoToClose.values()) {
                if (fm.Dossier__c != null && !mapDocumentDiscoToProcessByDossier.containsKey(fm.Dossier__c)) {
                    mapDocumentDiscoToProcessByDossier.put(fm.Dossier__c, fm);
                }
            }
            dossierIds = mapDocumentDiscoToProcessByDossier.keySet();
            Map<Id, Dossier__c> dossiers = dossierIds.isEmpty() ? new Map<Id, Dossier__c>() : MRO_QR_Dossier.getInstance().getDossierByIds(dossierIds);
            if (!dossiers.isEmpty()) {
                System.debug(' dossiers '+dossiers);

                for (Dossier__c dossier : dossiers.values()) {
                    if (dossier.Origin__c == MRO_UTL_Constants.ORIGIN_ENEL_EASY){
                        dossierToProcess.put(dossier.Id, dossier);
                    }
                }
            }
            if (!dossierToProcess.isEmpty()) {
                for (FileMetadata__c fm : candidateDocumentDiscoToClose.values()) {
                    if (dossierToProcess.containsKey(fm.Dossier__c)) {
                        documentDiscoToProcess.put(fm.Id, fm);
                    }
                }
                System.debug(' documentDiscoToProcess '+documentDiscoToProcess);

                MRO_SRV_ValidatableDocument.getInstance().checkAndCloseValidationActivity(documentDiscoToProcess, dossierToProcess);
            }
        }
    }
}