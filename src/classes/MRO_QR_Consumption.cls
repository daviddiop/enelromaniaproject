public with sharing class MRO_QR_Consumption {
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();

    public static MRO_QR_Consumption getInstance() {
        return (MRO_QR_Consumption)ServiceLocator.getInstance(MRO_QR_Consumption.class);
    }

    public List<Consumption__c> listByOsiIds(Set<Id> osiIds) {
        return [
            SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c
            FROM Consumption__c
            WHERE OpportunityServiceItem__c IN :osiIds
        ];
    }

    public Consumption__c getBySupplyAndCase(String supplyId, String caseId) {
        List<Consumption__c> consumptions = [
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c, Supply__r.ServicePoint__r.Code__c
                FROM Consumption__c
                WHERE Supply__c = :supplyId AND Case__c = :caseId
        ];
        return consumptions.size() > 0 ? consumptions[0] : null;
    }

    public List<Consumption__c> listBySupplyIds(Set<Id> supplyIds) {
        return [
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c, Status__c
                FROM Consumption__c
                WHERE Supply__c IN :supplyIds AND  Status__c !=: constantsSrv.CONSUMPTION_STATUS_OLD
        ];
    }

    public List<Consumption__c> listBySupplyId(String supplyId) {
        return [
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c
                FROM Consumption__c
                WHERE Supply__c = :supplyId
        ];
    }

    public List<Consumption__c> listByCaseId(String caseId) {
        return [
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c
                FROM Consumption__c
                WHERE Case__c = :caseId
        ];
    }

    public Map<Id,Consumption__c> getBySupplyIds(Set<Id> supplyIds) {
        return new Map<Id,Consumption__c>([
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c
                FROM Consumption__c
                WHERE Supply__c IN :supplyIds
        ]);
    }

    public Map<Id,Consumption__c> getActivatingByCaseIds(Set<Id> caseIds) {
        return new Map<Id,Consumption__c>([
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c
                FROM Consumption__c
                WHERE Case__c IN :caseIds AND Status__c = 'Activating'
        ]);
    }

    public List<Consumption__c> listByContractId(String contractId) {
        return new List<Consumption__c>([
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c,Supply__r.Contract__c,Status__c
                FROM Consumption__c
                WHERE Supply__r.Contract__c = :contractId
        ]);
    }

    public Map<Id,Consumption__c> getActiveByContractIds(Set<Id> contractIds) {
        return new Map<Id,Consumption__c>([
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c,Supply__r.Contract__c,Status__c
                FROM Consumption__c
                WHERE Supply__r.Contract__c IN :contractIds AND Status__c = 'Active'
        ]);
    }

    public Map<Id,Consumption__c> getActiveBySupplyIds(Set<Id> supplyIds) {
        return new Map<Id,Consumption__c>([
                SELECT Case__c, Commodity__c, Customer__c, Id, OpportunityServiceItem__c, QuantityApril__c, QuantityAugust__c, QuantityDecember__c,
                        QuantityFebruary__c, QuantityJanuary__c, QuantityJuly__c, QuantityJune__c, QuantityMarch__c, QuantityMay__c, QuantityNovember__c,
                        QuantityOctober__c, QuantitySeptember__c, Supply__c, Type__c,Supply__r.Contract__c,Status__c
                FROM Consumption__c
                WHERE Supply__c IN :supplyIds AND Status__c = 'Active'
        ]);
    }
}