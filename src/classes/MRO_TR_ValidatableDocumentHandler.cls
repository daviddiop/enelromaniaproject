/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 24, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_TR_ValidatableDocumentHandler implements TriggerManager.ISObjectTriggerHandler {
    private static MRO_SRV_ValidatableDocument validatableDocumentSrv = MRO_SRV_ValidatableDocument.getInstance();

    public void beforeInsert() {
    }

    public void beforeUpdate() {
    }

    public void beforeDelete() {
    }

    public void afterInsert() {
    }

    public void afterUpdate() {
        Set<Id> customerSignedDateDocumentIds = new Set<Id>();
        for (ValidatableDocument__c vd : (List<ValidatableDocument__c>) Trigger.new) {
            if (vd.CustomerSignedDate__c != null && vd.CustomerSignedDate__c != (Date) Trigger.oldMap.get(vd.Id).get('CustomerSignedDate__c')) {
                customerSignedDateDocumentIds.add(vd.Id);
            }
        }
        validatableDocumentSrv.commitCustomerSignedDates(customerSignedDateDocumentIds);
    }

    public void afterDelete() {
    }

    public void afterUndelete() {
    }
}