/**
 * Created by Boubacar Sow on 21/01/2020.
 * @author Boubacar Sow
 * @description
 * @date 21/01/2020
 *              [ENLCRO-425] Switch Out - Implement the logic of date check
 *              [ENLCRO-426] Switch Out - Implement the logic of Validity check
 *              [ENLCRO-428] Switch Out - Retention creation
 */

public with sharing class MRO_UTL_SwitchOut {

    private static MRO_QR_Activity activityQuery = MRO_QR_Activity.getInstance();
    private static MRO_SRV_Activity activitySrv = MRO_SRV_Activity.getInstance();
    static MRO_UTL_Constants constantSrv = MRO_UTL_Constants.getAllConstants();
    public static MRO_QR_Case casesQuery = MRO_QR_Case.getInstance();
    static Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();

    public static MRO_UTL_SwitchOut getInstance() {
        return (MRO_UTL_SwitchOut) ServiceLocator.getInstance(MRO_UTL_SwitchOut.class);

    }


    /**
     * @author  Boubacar Sow
     * @description Switch Out - Implement the logic of date check
     *              [ENLCRO-425] Switch Out - Implement the logic of date check
     * @date 16/01/2020
     *
     * @param caseRecord
     *
     * @return
     */
    public Boolean logicDateCheck(Date effectiveDate, Date referenceDate, Date contractEndDate, Boolean isDisCoAtoAEnabled, String origin, Boolean isVIPCustomer) {
        System.debug('### Effective Date: '+effectiveDate);
        System.debug('### Reference Date: '+referenceDate);
        System.debug('### Contract End Date: '+contractEndDate);
        System.debug('### DisCo AtoA: '+isDisCoAtoAEnabled);
        System.debug('### Origin: '+origin);
        System.debug('### VIP: '+isVIPCustomer);

        Boolean check;
        Map<String, Boolean > criteria = new Map<String, Boolean>();

        Integer effectiveDateHighThreshold = MRO_UTL_SettingProvider.getSwOutEffectiveDateHT();
        System.debug('### effectiveDateHighThreshold: '+effectiveDateHighThreshold);
        Integer effectiveDateLowThresholdAtoA = MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(true);
        System.debug('### effectiveDateLowThresholdAtoA: '+effectiveDateLowThresholdAtoA);
        Integer effectiveDateLowThresholdNonAtoA = MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false);
        System.debug('### effectiveDateLowThresholdNonAtoA: '+effectiveDateLowThresholdNonAtoA);
        Integer delayOriginMail = MRO_UTL_SettingProvider.getSwOutOriginDelay(true);
        System.debug('### delayOriginMail: '+delayOriginMail);
        Integer delayOriginOthers = MRO_UTL_SettingProvider.getSwOutOriginDelay(false);
        System.debug('### delayOriginOthers: '+delayOriginOthers);

        //if (caseRecord.EffectiveDate__c != null && caseRecord.ReferenceDate__c != null) {
        criteria.put(
            'logic_1',
            effectiveDate >= (referenceDate.addDays(effectiveDateHighThreshold))
        );
        criteria.put(
            'logic_2',
            effectiveDate < (referenceDate.addDays(effectiveDateHighThreshold))
        );
        //if (caseRecord.Origin != null && caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c != null) {
        criteria.put(
            'logic_3',
            (isDisCoAtoAEnabled == true && origin == 'Mail') &&
                    (effectiveDate >= (referenceDate.addDays(delayOriginMail + effectiveDateLowThresholdAtoA)))
        );
        criteria.put(
            'logic_4',
            (isDisCoAtoAEnabled == false && origin == 'Mail') &&
                    (effectiveDate >= (referenceDate.addDays(delayOriginMail + effectiveDateLowThresholdNonAtoA)))
        );
        criteria.put(
            'logic_5',
            (isDisCoAtoAEnabled == true && origin != 'Mail') &&
                    (effectiveDate >= (referenceDate.addDays(delayOriginOthers + effectiveDateLowThresholdAtoA)))
        );
        criteria.put(
            'logic_6',
            (isDisCoAtoAEnabled == false && origin != 'Mail') &&
                    (effectiveDate >= (referenceDate.addDays(delayOriginOthers + effectiveDateLowThresholdNonAtoA)))
        );
        criteria.put(
            'logic_9',
            (isDisCoAtoAEnabled == true && origin == 'Mail') &&
                    (effectiveDate < (referenceDate.addDays(delayOriginMail + effectiveDateLowThresholdAtoA)))
        );
        criteria.put(
            'logic_10',
            (isDisCoAtoAEnabled == false && origin == 'Mail') &&
                    (effectiveDate < (referenceDate.addDays(delayOriginMail + effectiveDateLowThresholdNonAtoA)))
        );
        criteria.put(
            'logic_11',
            (isDisCoAtoAEnabled == true && origin != 'Mail') &&
                    (effectiveDate < (referenceDate.addDays(delayOriginOthers + effectiveDateLowThresholdAtoA)))
        );
        criteria.put(
            'logic_12',
            (isDisCoAtoAEnabled == false && origin != 'Mail') &&
                    (effectiveDate >= (referenceDate.addDays(delayOriginOthers + effectiveDateLowThresholdNonAtoA)))
        );
        //}
        criteria.put(
            'logic_7',
            (effectiveDate == contractEndDate)
        );
        //if (caseRecord.Account.VIP__c != null) {
        criteria.put(
            'logic_8',
            (isVIPCustomer == true)
        );
        //}
        //}


        System.debug('### criteria 1 ' + criteria.get('logic_1'));
        System.debug('### criteria 2 ' + criteria.get('logic_2'));
        System.debug('### criteria 3 ' + criteria.get('logic_3'));
        System.debug('### criteria 4 ' + criteria.get('logic_4'));
        System.debug('### criteria 5 ' + criteria.get('logic_5'));
        System.debug('### criteria 6 ' + criteria.get('logic_6'));
        System.debug('### criteria 7 ' + criteria.get('logic_7'));
        System.debug('### criteria 8 ' + criteria.get('logic_8'));
        System.debug('### criteria 9 ' + criteria.get('logic_9'));
        System.debug('### criteria 10 ' + criteria.get('logic_10'));
        System.debug('### criteria 11 ' + criteria.get('logic_11'));
        System.debug('### criteria 12 ' + criteria.get('logic_12'));
        System.debug('#########################################');
        Boolean test = criteria.get('logic_3') || criteria.get('logic_4') || criteria.get('logic_5') || criteria.get('logic_6');
        System.debug('### test ' + test);


        System.debug('### criteria ' + criteria);
        if (!criteria.isEmpty()) {
            check =
                    criteria.get('logic_1') ||
                            (
                                    criteria.get('logic_2') &&
                                            (
                                                    criteria.get('logic_3') || criteria.get('logic_4') || criteria.get('logic_5') || criteria.get('logic_6')
                                            )
                                            && (criteria.get('logic_7') || criteria.get('logic_8'))
                            )
                            || criteria.get('logic_9') || criteria.get('logic_10') || criteria.get('logic_11') || criteria.get('logic_12')
                    ;
        }
        System.debug('### check ' + check);
        return check;

    }

    public Boolean logicDateCheck(String caseId) {
        if (String.isBlank(caseId)) {
            return null;
        }
        Case caseRecord = casesQuery.getCaseForLogicCheckById(caseId);
        if (caseRecord == null) {
            return null;
        }
        return this.logicDateCheck(caseRecord.EffectiveDate__c, caseRecord.ReferenceDate__c, caseRecord.Contract__r.EndDate,
                                   caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c, caseRecord.Origin, caseRecord.Account.VIP__c);
    }


    /**
     * @author  Boubacar Sow
     * @description Switch Out - Implement the logic of Validity check
     *              [ENLCRO-426] Switch Out - Implement the logic of Validity check
     * @date 20/01/2020
     *
     * @param caseRecord
     *
     * @return Boolean
     */
    public Map<String, Boolean > logicValidityCheck(String caseId) {
        if (String.isBlank(caseId)) {
            return null;
        }
        Boolean check;
        Map<String, Boolean > criteria = new Map<String, Boolean>();
        Map<String, Boolean > response = new Map<String, Boolean>();
        Case caseRecord = casesQuery.getCaseForLogicCheckById(caseId);

        if (caseRecord == null) {
            return null;
        }

        if (caseRecord.Reason__c == 'Fraud') {
            return response;
        }

        //if (caseRecord.EffectiveDate__c != null && caseRecord.ReferenceDate__c != null) {
        criteria.put(
                'logic_1',
                caseRecord.EffectiveDate__c < (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutEffectiveDateHT()))
        );
        //if (caseRecord.Origin != null && caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c != null) {
        criteria.put(
                'logic_2',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == true && caseRecord.Origin == 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(true) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(true))))
        );
        criteria.put(
                'logic_3',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == false && caseRecord.Origin == 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(true) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false))))
        );
        criteria.put(
                'logic_4',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == true && caseRecord.Origin != 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(false) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(true))))
        );
        criteria.put(
                'logic_5',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == false && caseRecord.Origin != 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(false) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false))))
        );
        //}
        //}
        System.debug('### criteria ' + criteria);
        if (!criteria.isEmpty()) {
            check =
//                criteria.get('logic_1') &&
                    (criteria.get('logic_2') || criteria.get('logic_3') || criteria.get('logic_4') || criteria.get('logic_5'))
                    ;
        }
        System.debug('### check ' + check);
        response.put('logic_1', criteria.get('logic_1'));
        response.put('allLogic', check);
        return response;

    }


    /**
     * @author  Boubacar Sow
     * @description  Switch Out - Retention creation
     *              [ENLCRO-428] Switch Out - Retention creation
     * @date 21/01/2020
     *
     * @param caseRecord
     *
     * @return Boolean
     */
    public Boolean logicRetentionCheck(String caseId) {
        if (String.isBlank(caseId)) {
            return null;
        }
        Boolean check;
        Map<String, Boolean > criteria = new Map<String, Boolean>();
        Case caseRecord = casesQuery.getCaseForLogicCheckById(caseId);

        if (caseRecord == null) {
            return null;
        }
        //if (caseRecord.EffectiveDate__c != null && caseRecord.ReferenceDate__c != null) {
        //if (caseRecord.Origin != null && caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c != null) {
        criteria.put(
                'logic_1',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == true && caseRecord.Origin == 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(true) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(true))))
        );
        criteria.put(
                'logic_2',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == false && caseRecord.Origin == 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(true) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false))))
        );
        criteria.put(
                'logic_3',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == true && caseRecord.Origin != 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(false) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(true))))
        );
        criteria.put(
                'logic_4',
                (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == false && caseRecord.Origin != 'Mail') &&
                        (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(false) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false))))
        );
        //}
        //}

        System.debug('### criteria ' + criteria);
        if (!criteria.isEmpty()) {
            check = criteria.get('logic_1') || criteria.get('logic_2') || criteria.get('logic_3') || criteria.get('logic_4');
        }
        System.debug('### check ' + check);
        return check;

    }

    /**
    * @author  David DIOP
    * @description Switch Out - Implement the logic of customer Retention
    * Updated by BG - 21/02/2020
    * [ENLCRO-403]
    * @date 24/01/2020
    * @param caseList
    * @return
    */
    public void insertRetentionCase(List<Case> caseList) {
        String accountId = caseList[0].AccountId;
        String dossierId = caseList[0].Dossier__c;
        Id dossierRetention;
        List<wrts_prcgvr__Activity__c> listCustomerRetention = activityQuery.getActivityByAccount(accountId);
        if (listCustomerRetention.isEmpty()) {
            wrts_prcgvr__Activity__c customerRetention = activitySrv.insertCustomerActivity(caseList[0], constantSrv.CUSTOMER_RETENTION);
            listCustomerRetention.add(customerRetention);
        }
        if (caseRts.get('SwitchOut_ELE').getRecordTypeId() == caseList[0].RecordTypeId) {
            List<wrts_prcgvr__Activity__c> listDossierRetentionElectric = activityQuery.getActivityElectricByDossier(dossierId);
            if (listDossierRetentionElectric.isEmpty()) {
                wrts_prcgvr__Activity__c dossierRetentionEle = activitySrv.insertDossierActivity(caseList[0], listCustomerRetention[0].Id, constantSrv.DOSSIER_RETENTION_ELE);
                dossierRetention = dossierRetentionEle.Id;
            } else {
                dossierRetention = listDossierRetentionElectric[0].Id;
            }
        } else {
            List<wrts_prcgvr__Activity__c> listDossierRetentionGas = activityQuery.getActivityGasByDossier(dossierId);
            if (listDossierRetentionGas.isEmpty()) {
                wrts_prcgvr__Activity__c dossierRetentionGas = activitySrv.insertDossierActivity(caseList[0], listCustomerRetention[0].Id, constantSrv.DOSSIER_RETENTION_GAS);
                dossierRetention = dossierRetentionGas.Id;
            } else {
                dossierRetention = listDossierRetentionGas[0].Id;
            }
        }

        activitySrv.insertChildActivity(caseList, constantSrv.SERVICE_POINT_RETENTION, dossierRetention);
    }
}