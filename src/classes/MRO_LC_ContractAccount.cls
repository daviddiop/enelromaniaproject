/**
 * Created by Boubacar Sow on 07/08/2019.
 */

public with sharing class MRO_LC_ContractAccount extends ApexServiceLibraryCnt {
    static MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();
    static MRO_QR_Address addressQuery = MRO_QR_Address.getInstance();
    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    static MRO_QR_CompanyDivision companyDivisionQuery = MRO_QR_CompanyDivision.getInstance();
    static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();

    public with sharing class getActiveSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String contractAccountRecordId = params.get('contractAccountId');
            if (String.isBlank(contractAccountRecordId)) {
                throw new WrtsException(System.Label.ContractAccount + ' - ' + System.Label.MissingId);
            }

            List<Supply__c> supplyList = supplyQuery.getActiveSuppliesByContractAccount(contractAccountRecordId);
            List<Case> caseList = caseQuery.getCasesByContractAccount(contractAccountRecordId);

            response.put('enableEditButtonCA', supplyList.isEmpty() && caseList.isEmpty());
            return response;
        }
    }

    /**
     * @author Boubacar Sow
     * @modified 25/11/2019
     * @description refactor the methode getContractAccountRecords
     *               [ENLCRO-196] Integrate ContractAccountSelection in the DemostratedPaymentWizard
     */
    public with sharing class getContractAccountRecords extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String filter = params.get('filter');
            String companyDivisionId=params.get('companyDivisionId');
            String commodity = params.get('commodity');
            String market= params.get('market');
            String contractId = params.get('contractId');
            Boolean isAccountSituationWizard = (Boolean) JSON.deserialize(params.get('isAccountSituationWizard'), Boolean.class);
            Boolean isDemonstratedPaymentWizard = (Boolean) JSON.deserialize(params.get('isDemonstratedPaymentWizard'), Boolean.class);
            Boolean isInvoiceDuplicateWizard = (Boolean) JSON.deserialize(params.get('isInvoiceDuplicateWizard'), Boolean.class);
            Boolean isBillingProfileAddressChangeWizard = (Boolean) JSON.deserialize(params.get('isBillingProfileAddressChangeWizard'), Boolean.class);
            Set<String> contractAccountIdList = params.get('contractAccountIdList') != null
                ? (Set<String>) JSON.deserialize(params.get('contractAccountIdList'), Set<String>.class) : null;
            Set<String> excludeContractAccountIds = params.get('excludeContractAccountIds') != null
                ? (Set<String>) JSON.deserialize(params.get('excludeContractAccountIds'), Set<String>.class) : null;
            String contractType = params.get('contractType');
            List<Id> supplyIds = params.get('supplyIds') != null ? (List<Id>) JSON.deserialize(params.get('supplyIds'), List<Id>.class) : null;

            String val ='';
            String dataType= '';
            String operator ='';
            String field ='';

            if (String.isNotBlank(filter) ) {
                Filter filterDto = (Filter)JSON.deserialize(filter, Filter.class);
                if(String.isNotBlank(filterDto.value) && String.isNotBlank(filterDto.dataType) && String.isNotBlank(filterDto.operator) && String.isNotBlank(filterDto.field)){
                    val = filterDto.value;
                    dataType= filterDto.dataType;
                    operator = filterDto.operator;
                    field = filterDto.field;
                }
            }

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                Map<String, Object> contractAccountsResult = contractAccountQuery.getByAccountAndCompanyDivisionId(accountId, companyDivisionId,
                    dataType, field, operator, val, commodity, contractType, contractId, supplyIds, market, contractAccountIdList, excludeContractAccountIds);
                List<ContractAccount__c> listContractAccount = (List<ContractAccount__c>) contractAccountsResult.get('contractAccounts');
                response.put('listContractAccount', listContractAccount);
                response.put('latePaymentPenalty', contractAccountsResult.get('latePaymentPenalty'));
                response.put('paymentTerms', contractAccountsResult.get('paymentTerms'));
                response.put('error', false);
            }catch (Exception ex){
                System.debug(ex.getStackTraceString());
                System.debug(ex.getMessage());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class Filter {
        @AuraEnabled
        public String dataType {get; set;}
        @AuraEnabled
        public String field {get; set;}
        @AuraEnabled
        public String operator {get; set;}
        @AuraEnabled
        public String value {get; set;}
    }

    public with sharing class getMapStreetNameBySelfReadingPeriodAndBillingFrequency extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> mapStreetNameByBillingFrequency = new Map<String, String>();
            Map<String, String> mapStreetNameBySelfReadingPeriod = new Map<String, String>();
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String streetId = params.get('streetId');

            if(String.isNotBlank(streetId )){
                mapStreetNameByBillingFrequency = addressQuery.getMapStreetNameByBillingFrequency(streetId);
                mapStreetNameBySelfReadingPeriod = addressQuery.getMapStreetNameBySelfReadingPeriod(streetId);
            }

            response.put('mapStreetNameByBillingFrequency', mapStreetNameByBillingFrequency);
            response.put('mapStreetNameBySelfReadingPeriod', mapStreetNameBySelfReadingPeriod);

            return response;
        }
    }

    public class getAccountId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String contractAccountId = params.get('contractAccountId');
            if (String.isBlank(contractAccountId)) {
                response.put('error', true);
                response.put('errorMsg', 'ContractAccountId is empty');
                return response;
            }
            ContractAccount__c contractAccount = contractAccountQuery.getById(contractAccountId);
            if (contractAccount != null) {
                response.put('accountId', contractAccount.Account__c);
                response.put('error', false);
            }
            return response;
        }
    }

    public class getContractAccount extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String contractAccountId = params.get('contractAccountId');
            if (String.isBlank(contractAccountId)) {
                response.put('error', true);
                response.put('errorMsg', 'ContractAccountId is empty');
                return response;
            }
            ContractAccount__c contractAccount = contractAccountQuery.getById(contractAccountId);
            response.put('contractAccount', contractAccount);
            response.put('error', false);
            return response;
        }
    }

    public class getCompanyDivision extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String contractAccountId = params.get('contractAccountId');
            if (String.isBlank(contractAccountId)) {
                response.put('error', true);
                response.put('errorMsg', 'ContractAccountId is empty');
                return response;
            }
            ContractAccount__c contractAccount = contractAccountQuery.getById(contractAccountId);
            if (contractAccount != null) {
                response.put('companyDivisionId', contractAccount.CompanyDivision__c);
                response.put('companyDivisionCode', contractAccount.CompanyDivision__r.Code__c);
                response.put('error', false);
            }
            return response;
        }
    }

    public with sharing class getListSelfReadingPeriodByCompanyDivision extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String companyDivisionId = params.get('companyDivisionId');
            String vatNumber = params.get('vatNumber');
            String market = params.get('market');

            if(String.isNotBlank(companyDivisionId)){
                CompanyDivision__c companyDivision = companyDivisionQuery.getName(companyDivisionId);
                if(companyDivision != null){
                    //List<ApexServiceLibraryCnt.Option> selfReadingPeriodOptions = addressQuery.getListProvinceBySelfReadingPeriod(pointAddressProvince);
                    List<ApexServiceLibraryCnt.Option> selfReadingPeriodOptions = addressQuery.getListSelfReadingPeriodByCompanyDivision(companyDivision.Name,vatNumber,market);
                    //System.debug('FF selfReadingPeriodList: ' + selfReadingPeriodOptions);
                    response.put('selfReadingPeriodEnds', selfReadingPeriodOptions);
                }
            }
            return response;
        }
    }

    public with sharing class getBillingAccountNumber extends AuraCallable {
        public override Object perform(final String jsonInput) {

            System.debug('MRO_LC_ContractAccount.getBillingAccountNumber - Enter');
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            try {

                Map<String, String> params = asMap(jsonInput);
                List<OpportunityServiceItem__c> opportunityServiceItems =
                        params.get('opportunityServiceItems') != null ?
                        (List<OpportunityServiceItem__c>) JSON.deserialize(params.get('opportunityServiceItems'),
                                List<OpportunityServiceItem__c>.class) :
                        null;
                System.debug('MRO_LC_ContractAccount.getBillingAccountNumber opportunityServiceItems' + opportunityServiceItems);
                Set<Id> contractAccountIds = new Set<Id>();
                Set<Id> servicePointIds = new Set<Id>();
                for(OpportunityServiceItem__c opportunityServiceItem : opportunityServiceItems) {

                    contractAccountIds.add(opportunityServiceItem.ContractAccount__c);
                    servicePointIds.add(opportunityServiceItem.ServicePoint__c);
                    if(contractAccountIds.size() > 1) return response;
                }
                System.debug('MRO_LC_ContractAccount.getBillingAccountNumber contractAccountIds' + contractAccountIds);
                System.debug('MRO_LC_ContractAccount.getBillingAccountNumber servicePointIds' + servicePointIds);
                if(!contractAccountIds.isEmpty() && contractAccountIds.size() == 1) {

                    String contractAccountId = (String)contractAccountIds.iterator().next();
                    Set<Id> caServicePointIds = new Set<Id>();
                    for(Supply__c supply : supplyQuery.getActiveAndActivatingSuppliesByContractAccount(contractAccountId)) {

                        caServicePointIds.add(supply.ServicePoint__c);
                    }
                    System.debug('MRO_LC_ContractAccount.getBillingAccountNumber caServicePointIds' + caServicePointIds);
                    if(!servicePointIds.containsAll(caServicePointIds)) return response;
                    ContractAccount__c contractAccount = contractAccountQuery.getById(contractAccountId);
                    System.debug('MRO_LC_ContractAccount.getBillingAccountNumber contractAccount' + contractAccount);
                    response.put('contractAccount', contractAccount);
                }

            }catch (Exception ex){
                System.debug('MRO_LC_ContractAccount.getBillingAccountNumber exception '+
                        ex.getMessage() + ' ' + ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}