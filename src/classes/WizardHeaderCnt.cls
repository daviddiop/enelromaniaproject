/**
 * Created by Moussa Fofana at 24/07/2019.
 */ 
public with sharing class WizardHeaderCnt extends ApexServiceLibraryCnt {

    private static OpportunityQueries oppQuery = OpportunityQueries.getInstance();
    private static DossierQueries dossierQuery = DossierQueries.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static CompanyDivisionQueries compQuery = CompanyDivisionQueries.getInstance();
    
    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String opportunityId = params.get('opportunityId');
            String companyDivisionId = params.get('companyDivisionId');
            String dossierId = params.get('dossierId');            
            WizardHeaderCnt.ResponseData res = new WizardHeaderCnt.ResponseData();
            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                Account acc = accountQuery.findAccount(accountId);
                res.account =  acc;
                res.accountId = accountId;
                // Handling of CompanyDivision data
                if (String.isNotBlank(companyDivisionId)) {
                    CompanyDivision__c companyDivision = compQuery.getById(companyDivisionId);
                    res.companyDivisionName = companyDivision.Name;
                    res.companyDivisionId = companyDivision.Id;
                }
                // Handling of Dossier data
                Dossier__c dos;
                if (String.isNotBlank(dossierId)) {
                    dos = dossierQuery.getById(dossierId);
                    res.dossier = dos;
                    res.companyDivisionName = dos.CompanyDivision__r.Name;
                    res.companyDivisionId = dos.CompanyDivision__c;
                }
                // Handling of Opportunity data
                Opportunity opp;
                if (String.isNotBlank(opportunityId)) {
                    opp = oppQuery.getOpportunityById(opportunityId);
                    res.opportunityId = opp.Id;
                    res.opportunity = opp;
                    res.companyDivisionName = opp.CompanyDivision__r.Name;
                    res.companyDivisionId = opp.CompanyDivision__c;
                }
                res.accountPersonRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId();
                res.accountPersonProspectRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId();
                res.accountBusinessRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId();
                res.accountBusinessProspectRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId();             
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            response.put('response', res);
            return response;
        }
    }

    public class ResponseData{
        @AuraEnabled
        public String accountId {get; set;}
        @AuraEnabled
        public String opportunityId {get; set;}
        @AuraEnabled
        public String companyDivisionId {get; set;}
        @AuraEnabled
        public String companyDivisionName {get; set;}
        @AuraEnabled
        public String accountPersonRT {get; set;}
        @AuraEnabled
        public String accountPersonProspectRT {get; set;}
        @AuraEnabled
        public String accountBusinessRT {get; set;}
        @AuraEnabled
        public String accountBusinessProspectRT {get; set;}
        @AuraEnabled
        public Account account {get; set;}
        @AuraEnabled
        public Opportunity opportunity {get; set;}
        @AuraEnabled
        public Dossier__c dossier {get; set;}

        public ResponseData() {}
    }
}