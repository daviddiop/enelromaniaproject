public with sharing class ProductCnt extends ApexServiceLibraryCnt {

    static OpportunityQueries opportunityQuery = OpportunityQueries.getInstance();
    static OpportunityLineItemQueries oliQuery = OpportunityLineItemQueries.getInstance();
    static ProductQueries productQuery = ProductQueries.getInstance();

    static ProductService productSrv = ProductService.getInstance();
    static DatabaseService databaseSrv = DatabaseService.getInstance();

    public class getInitData extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            if (String.isBlank(opportunityId)) {
                throw new WrtsException(System.Label.Opportunity + ' - ' + System.Label.MissingId);
            }
            Map<String, Object> response = new Map<String, Object>();
            Opportunity requiredOpportunity = opportunityQuery.getById(opportunityId);
            response.put('opportunity', requiredOpportunity);
            if (requiredOpportunity.OpportunityLineItems.size() > 0) {
                List<ProductService.Product> existProductList = new List<ProductService.Product>();
                for (OpportunityLineItem oli : requiredOpportunity.OpportunityLineItems) {
                    existProductList.add(new ProductService.Product(oli));
                }
                response.put('existProductList', existProductList);
            }
            if (String.isNotBlank(requiredOpportunity.Pricebook2Id)) {
                response.putAll(getProductAndFamilies(requiredOpportunity.Pricebook2Id));
            } else {
                List<Pricebook2> pricebookList = productQuery.listActivePricebooks();
                if (pricebookList.size() == 1) {
                    response.putAll(getProductAndFamilies(pricebookList.get(0).Id));
                    response.put('pricebookId', pricebookList.get(0).Id);
                } else {
                    response.put('pricebookList', pricebookList);
                }
            }
            return response;
        }
    }

    public class getProductList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String pricebookId = params.get('pricebookId');
            if (String.isBlank(pricebookId)) {
                throw new WrtsException(System.Label.PriceBook + ' - ' + System.Label.MissingId);
            }
            return getProductAndFamilies(pricebookId);
        }
    }

    public class productsAndFamilies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String productId = params.get('productId');
            if (String.isBlank(productId)) {
                throw new WrtsException(System.Label.Product + ' - ' + System.Label.MissingId);
            }
            return productSrv.productsAndFamilies(productId);
        }
    }

    public class listProductWithOptions extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Set<String> productIds = (Set<String>)JSON.deserialize(jsonInput, Set<String>.class);
            if (productIds == null) {
                throw new WrtsException(System.Label.Product + ' - ' + System.Label.MissingId);
            }
            return productSrv.listProductWithOptions(productIds);
        }
    }

    private static Map<String, Object> getProductAndFamilies(String pricebookId) {
        Map<String, Object> response = new Map<String, Object>();

        List<ProductService.Product> productList = new List<ProductService.Product>();
        Set<String> productFamilySet = new Set<String>();
        Set<String> recordTypeNameSet = new Set<String>();

        List<PricebookEntry> pbeList = productQuery.listActivePricebookEntryByPricebookId(pricebookId);
        for (PricebookEntry pbe : pbeList) {
            productFamilySet.add(pbe.Product2.Family);
            recordTypeNameSet.add(pbe.Product2.RecordType.Name);
            productList.add(new ProductService.Product(pbe.Product2));
        }

        response.put('productList', productList);
        response.put('productFamilyList', productFamilySet);
        response.put('productTypeList', recordTypeNameSet);
        return response;
    }

    public class checkout extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String pricebookId = params.get('pricebookId');

            Opportunity opp = new Opportunity(Id = opportunityId, Pricebook2Id= pricebookId);
            databaseSrv.updateSObject(opp);

            List<ProductService.Product> productList = (List<ProductService.Product>)
                JSON.deserialize(params.get('productList'), List<ProductService.Product>.class);

//            databaseSrv.deleteSObject(oliQuery.getOLIsByOpportunityId(opportunityId));

            List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
            for(ProductService.Product currentProduct : productList) {
                if (String.isNotBlank(currentProduct.oliId)) {
                    continue;
                }
                OpportunityLineItem tmp = new OpportunityLineItem();
                tmp.Product2Id = currentProduct.Id;
                tmp.OpportunityId = opportunityId;
                tmp.Quantity = 1;
                tmp.UnitPrice = 0;
                oliList.add(tmp);
            }
            databaseSrv.insertSObject(oliList);
            return null;
        }
    }
}