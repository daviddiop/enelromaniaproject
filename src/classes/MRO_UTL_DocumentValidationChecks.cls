/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 18, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_UTL_DocumentValidationChecks implements Callable {
    public Enum ResultCode { PASS, WARNING, ERROR }
    public class Result {
        public ResultCode code;
        public String message;

        public Result(ResultCode code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    private static final MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private ValidatableDocument__c validatableDocument;
    private Dossier__c dossierInt;
    private Dossier__c dossier {
        get {
            if (this.dossierInt == null) {
                this.dossierInt = dossierQuery.getByIdWithOpportunity(this.validatableDocument.Dossier__c);
            }
            return this.dossierInt;
        }
    }

    public MRO_UTL_DocumentValidationChecks(ValidatableDocument__c validatableDocument) {
        this.validatableDocument = validatableDocument;
    }

    public Object call(String method, Map<String, Object> args) {
        Boolean isBlocking = (Boolean) args.get('isBlocking');
        switch on method {
            when 'checkSwitchNotificationDate' {
                return this.checkSwitchNotificationDate(isBlocking);
            }
            when 'checkCustomerSignedDate' {
                return this.checkCustomerSignedDate(isBlocking);
            }
            when else {
                throw new WrtsException('Method not implemented');
            }
        }
    }

    public Result checkSwitchNotificationDate(Boolean isBlocking) {
        Integer term = ProcessDateThresholds__c.getInstance().SwOTCTerm__c != null ? ProcessDateThresholds__c.getInstance().SwOTCTerm__c.intValue() : 0;
        System.debug('### term: '+term);
        System.debug('### Switch Notification Date: '+this.validatableDocument.SwitchNotificationDate__c);
        if (this.validatableDocument.SwitchNotificationDate__c != null) {
            System.debug('### Dossier: '+this.dossier);
            if (this.dossier.RequestType__c == 'SwitchIn' && this.dossier.Opportunity__r.RequestedStartDate__c != null
                && this.validatableDocument.SwitchNotificationDate__c > this.dossier.Opportunity__r.RequestedStartDate__c - term) {
                return new Result(isBlocking ? ResultCode.ERROR : ResultCode.WARNING, Label.InvalidNotificationDate);
            } else if (this.dossier.RequestType__c == 'SwitchOut') {
                List<Case> cases = MRO_QR_Case.getInstance().listCasesForSwitchOutDateCheckByDossierId(this.dossier.Id);
                Boolean checkPassed = true;
                for (Case caseRecord : cases) {
                    System.debug('### Case: '+caseRecord);
                    MRO_UTL_SwitchOut switchOutUtl = MRO_UTL_SwitchOut.getInstance();
                    if (!switchOutUtl.logicDateCheck(caseRecord.EffectiveDate__c, this.validatableDocument.SwitchNotificationDate__c,
                                                     caseRecord.Contract__r.EndDate, caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c,
                                                     caseRecord.Origin, caseRecord.Account.VIP__c)) {
                        checkPassed = false;
                        break;
                    }
                    System.debug('### Check passed: '+checkPassed);
                }
                if (!checkPassed) {
                    return new Result(isBlocking ? ResultCode.ERROR : ResultCode.WARNING, Label.InvalidNotificationDate);
                }
            }
        }
        return new Result(ResultCode.PASS, null);
    }

    public Result checkCustomerSignedDate(Boolean isBlocking) {
        if (this.validatableDocument.CustomerSignedDate__c == null || this.validatableDocument.CustomerSignedDate__c > Date.today()) {
            return new Result(isBlocking ? ResultCode.ERROR : ResultCode.WARNING, Label.InvalidCustomerSignedDate);
        }
        return new Result(ResultCode.PASS, null);
    }
}