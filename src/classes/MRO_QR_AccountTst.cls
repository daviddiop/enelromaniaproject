/**
 * Created by napoli on 27/11/2019.
 */

@IsTest
private class MRO_QR_AccountTst {

    @IsTest
    static void listAccountByVatNumberTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert businessAccount;
        List<Account> accountList = accountQuery.listAccountByVatNumber(businessAccount.VATNumber__c);
        System.assert(accountList.size() == 1);
    }

    @IsTest
    static void listByNationalIdTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert personAccount;
        List<Account> accountList = accountQuery.listByNationalId(personAccount.NationalIdentityNumber__pc);
        System.assert(accountList.size() == 1);
    }

    @IsTest
    static void listByKeyTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        personAccount.key__c = personAccount.NationalIdentityNumber__pc;
        insert personAccount;
        List<Account> accountList = accountQuery.listByKey(personAccount.Key__c);
        System.assert(accountList.size() == 1);
    }

    @IsTest
    static void getByIdsTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert personAccount;
        Map<Id, Account> accountMap = accountQuery.getByIds(new set<Id>{personAccount.Id});
        System.assert(accountMap.size() == 1);
    }

    @IsTest
    static void findAccountTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert personAccount;
        Account acc = accountQuery.findAccount(personAccount.Id);
        System.assert(acc.Id == personAccount.Id);
    }

    @IsTest
    static void listByIndividualIdTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Individual ind = MRO_UTL_TestDataFactory.individual().build();
        ind.LastName = 'test';
        ind.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert ind;
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        personAccount.PersonIndividualId = ind.Id;
        insert personAccount;
        List<Account> accountList = accountQuery.listByIndividualId(ind.Id);
        System.assert(accountList.size() == 1);
    }

    @IsTest
    static void listByIdsTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert personAccount;
        List<Account> accountList = accountQuery.listByIds(new List<Id>{personAccount.Id});
        System.assert(accountList.size() == 1);
    }

    @IsTest
    static void listContactsTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert businessAccount;
        Contact con = MRO_UTL_TestDataFactory.contact().createContact().build();
        con.AccountId = businessAccount.Id;
        insert con;
     //   AccountContactRelation acr = MRO_UTL_TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id, businessAccount.Id).build();
      //  insert acr;
        List<AccountContactRelation> acrList = accountQuery.listContacts(businessAccount.Id);
        System.assert(acrList.size() == 0);
    }

    @IsTest
    static void getTradersTest() {
        MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account traderAccount = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert traderAccount;
        Id RecordTypeTrader = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        List<Account> accountList = accountQuery.getTraders(RecordTypeTrader);
        System.assert(accountList.size() == 1);
    }
}