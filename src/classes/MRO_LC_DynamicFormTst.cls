/**
 * Created by moustapha on 28/08/2020.
 */

@IsTest
public with sharing class MRO_LC_DynamicFormTst {
    @IsTest
    public static void parseLabelsTest() {
        Map<String, String> labels = new Map<String, String>{
                'name' => 'name',
                'name' => 'save',
                'name' => 'cancel'
        };
        Test.startTest();
        Map<String, Object> response  = MRO_LC_DynamicForm.parseLabels(labels);
        System.debug('#-result-# '+response.get('labels'));
        system.assertEquals(true, response.get('labels') != null );
        Test.stopTest();
    }
}