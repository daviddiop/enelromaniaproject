/**
 * Created by BADJI on 25/08/2020.
 */

@IsTest
public with sharing class MRO_LC_SupplyTst {

    @TestSetup
    private static void setup(){

        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createCustomerCodeSequencer().build();
        insert sequencer;

        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert personAccount;
        List<Supply__c> supplyList = new List<Supply__c>();

        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = personAccount.Id;
        insert contract;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();

        insert companyDivision;
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;

        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = personAccount.Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;

        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supply.ServicePoint__c = servicePoint.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        List<Case> caseList = new List<Case>();
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = personAccount.Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;

    }

    @IsTest
    public static void getSupplyTileTest(){

        Supply__c supply =[ SELECT Id FROM Supply__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
            'recordId' => supply.Id
        };
        Test.startTest();
            Object response = TestUtils.exec(
                'MRO_LC_Supply', 'getSupplyTile', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('supplyData') != null);
        Test.stopTest();
    }

    @IsTest
    public static  void getDeveloperNameTest(){
        Case cases = [SELECT Id FROM Case LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
            'recordId' => cases.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Supply', 'getDeveloperName', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('listRecordType') != null);
        Test.stopTest();
    }

    @IsTest
    public static void getSuppliesByAccountIdTest(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => acc.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Supply', 'getSuppliesByAccountId', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('supplies') != null);
        Test.stopTest();
    }

    @IsTest
    public static void getSuppliesByAccountIdExceptionTest(){
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => 'accId'
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Supply', 'getSuppliesByAccountId', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    public static void getSuppliesByServicePointTest(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        List<String> listStrings = new List<String>();
        listStrings.add('Contract__c');

        Map<String, String > inputJSON = new Map<String, String>{
            'servicePointCode' =>'',
            'accountId' => acc.Id,
            'status' => 'Active',
            'contractId' =>'',
            'notOwnAccount'=>'true',
            'selectFields' => JSON.serialize(listStrings),
            'companyDivisionId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Supply', 'getSuppliesByServicePoint', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('supplyFieldsSP') != null);
        Test.stopTest();
    }

    @IsTest
    public static void getSuppliesByServicePointExceptionTest(){
        List<String> listStrings = new List<String>();
        listStrings.add('Contract__c');

        Map<String, String > inputJSON = new Map<String, String>{
            'servicePointCode' =>'',
            'accountId' => 'accId',
            'status' => 'Active',
            'contractId' =>'',
            'notOwnAccount'=>'true',
            'selectFields' => 'test',
            'companyDivisionId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Supply', 'getSuppliesByServicePoint', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    public static  void getSuppliesByContractTest(){

        Account acc = [SELECT Id FROM Account LIMIT 1];
        List<String> listStrings = new List<String>();
        listStrings.add('Contract__c');

        Map<String, String > inputJSON = new Map<String, String>{
            'servicePointCode' =>'',
            'accountId' => acc.Id,
            'status' => 'Active',
            'contractId' =>'',
            'notOwnAccount'=>'true',
            'selectFields' => JSON.serialize(listStrings),
            'companyDivisionId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Supply', 'getSuppliesByContract', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('supplyFieldsCT') != null);

        inputJSON = new Map<String, String>{
            'servicePointCode' =>'',
            'accountId' => 'accId',
            'status' => 'Active',
            'contractId' =>'',
            'notOwnAccount'=>'true',
            'selectFields' => 'test',
            'companyDivisionId' => ''
        };
         response = TestUtils.exec(
            'MRO_LC_Supply', 'getSuppliesByContract', inputJSON, true);
         result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();


    }

    @IsTest
    static void getLookupFields(){
        Map<String, String > inputJSON = new Map<String, String>{
            'objName' =>'Supply__c'
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_Supply', 'getLookupFields', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('lookupFields') != null);

        inputJSON = new Map<String, String>{
            'objName' =>'test'
        };
        response = TestUtils.exec(
            'MRO_LC_Supply', 'getLookupFields', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    static void deleteCaseTest(){
        Case cases = [SELECT Id FROM Case LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
            'recordId' => cases.Id
        };
        Test.startTest();
        TestUtils.exec(
            'MRO_LC_Supply', 'deleteCase', inputJSON, true);
        Object response =
            TestUtils.exec(
                'MRO_LC_Supply', 'deleteCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }



        }