public with sharing class MRO_SRV_Supply extends ApexServiceLibraryCnt {

    static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    static MRO_QR_ServicePoint servicePointQuery = MRO_QR_ServicePoint.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();
    static Map<String, Schema.SObjectField> supplyFieldsMap = Schema.SObjectType.Supply__c.fields.getMap();
    static DatabaseService databaseSrv = DatabaseService.getInstance();

    public static MRO_SRV_Supply getInstance() {
        return new MRO_SRV_Supply();
    }

    public class getRecordTypes extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('servicePointRecordType', new Map<String, String>{
                    'label' => System.Label.SearchServicePoint, 'value' => 'servicePoint'
            });
            response.put('contractRecordType', new Map<String, String>{
                    'label' => System.Label.SearchContract, 'value' => 'contact'
            });
            return response;
        }
    }

    public void createAndInsertForEachOsi(Opportunity opp) {
        List<Supply__c> supplyList = new List<Supply__c>();

        for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
            Supply__c supply = instantiatePrefilledWithOsiData(opp, osi);
            supplyList.add(supply);
        }
        databaseSrv.insertSObject(supplyList);
    }

    public Supply__c instantiatePrefilledWithOsiData(Opportunity opp, OpportunityServiceItem__c osi) {
        Supply__c supply = new Supply__c();
        Id supplyRecordTypeId = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get(osi.RecordType.DeveloperName).getRecordTypeId();
        supply.RecordTypeId = supplyRecordTypeId;
        supply.OpportunityServiceItem__c = osi.Id;
        supply.Account__c = opp.AccountId;
        supply.Contract__c = opp.ContractId;
        supply.ContractAccount__c = osi.ContractAccount__c;
        supply.NonDisconnectableReason__c = osi.NonDisconnectableReason__c;
        supply.ServicePoint__c = osi.ServicePoint__c;
        supply.ServiceSite__c = osi.ServiceSite__c;
        supply.Market__c = osi.ContractAccount__r.Market__c;
        supply.InENELArea__c = osi.Distributor__r.IsDisCoENEL__c;
        supply.NonDisconnectable__c = osi.NonDisconnectable__c;
        supply.FlatRate__c = osi.FlatRate__c;
        supply.Status__c = 'Activating';
        supply.CompanyDivision__c = opp != null ? opp.CompanyDivision__c : null ;
        return supply;
    }

    public void actualizeSupplyOnServicePoint(Set<Id> servicePointIds) {
        System.debug('RT>>>>>>>>>> actualizeSupplyOnServicePoint');
        List<ServicePoint__c> toUpdate = new List<ServicePoint__c>();
        List<ServicePoint__c> involvedPoints = servicePointQuery.getPointsWithSupplies(servicePointIds);
        for (ServicePoint__c point : involvedPoints) {

            if (point.CurrentSupply__c == null && !point.Supplies__r.isEmpty()) {
                point.CurrentSupply__r = point.Supplies__r.get(0);
            }
            /*
            if (point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVE) {
                continue;
            }*/

            for (Supply__c supply : point.Supplies__r) {
                /*if (supply.Id == point.CurrentSupply__c) {
                    continue;
                }*/

                if (supply.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVE) {
                    point.CurrentSupply__r = supply;
                    break;
                }

                if (supply.Status__c == constantsSrv.SUPPLY_STATUS_TERMINATING &&
                        (point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_NOTACTIVE || point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVATING)) {
                    point.CurrentSupply__r = supply;
                    continue;
                }

                if (supply.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVATING && point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_NOTACTIVE) {
                    point.CurrentSupply__r = supply;
                    continue;
                }
            }


            if (point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_NOTACTIVE) {
                point.CurrentSupply__c = null;
                point.CurrentSupply__r = null;
                point.Account__c = null;
            }
            if (point.CurrentSupply__r != null) {
                point.CurrentSupply__c = point.CurrentSupply__r.Id;
                point.Account__c = point.CurrentSupply__r.Account__c;
            }

            toUpdate.add(point);
        }

        if (!toUpdate.isEmpty()) {
            this.generateENELTELCodes(toUpdate);
            databaseSrv.updateSObject(toUpdate);
        }
    }

    public List<String> getLookupFields(String objName) {
        List<String> lookupFields = new List<String> ();
        //map<string, Map<String, Schema.SObjectField>> objectFieldsMap = new map<string, Map<String, Schema.SObjectField>>(); //Object field schema map
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();

        for (Schema.SObjectField fld : objectFields.values()) {

            schema.describeFieldResult dfield = fld.getDescribe();
            String fldType = String.valueOf(dfield.getType());

            if (fldType == 'REFERENCE') { // Lookup field!
                lookupFields.add(String.valueOf(dfield.Name));
                system.debug(String.valueOf(dfield.Name) + ' = ' + fldType);
            }
        }
        return lookupFields;
    }

    public void deleteCase(String recordId) {
        databaseSrv.deleteSObject(recordId);
    }

    /**
    * @description Get List of supplies from Activities
    * @param activities
    * @author BG
    * @return List<Supply__c>
    */
    public List<Supply__c> listSuppliesFromActivity(List<wrts_prcgvr__Activity__c> activities) {
        List<Id> supplyIds = new List<Id>();
        for (wrts_prcgvr__Activity__c activity : activities) {
            if (activity.Case__c != null && activity.Case__r.Supply__c != null) {
                supplyIds.add(activity.Case__r.Supply__c);
            }
        }
        return supplyQuery.getSuppliesByIds(supplyIds);
    }

    @AuraEnabled
    public static Map<String, Boolean> getFieldType(String fieldName) {
        system.debug('fieldName ' + fieldName);
        system.debug('supplyFieldsMap ' + supplyFieldsMap);
        Map<String, Boolean> values = new Map<String, Boolean>();
        Schema.SObjectField field = supplyFieldsMap.get(fieldName);
        Schema.DisplayType FldType = field.getDescribe().getType();
        system.debug('supplyFieldsMap' + supplyFieldsMap);
        system.debug('FldType.name()' + FldType.name());
        system.debug(fieldName + ' = ' + FldType.name().containsIgnoreCase('REFERENCE'));
        values.put(fieldName, FldType.name().containsIgnoreCase('REFERENCE'));
        return values;
    }

    public void generateENELTELCodes(List<ServicePoint__c> servicePoints) {
        if (!servicePoints.isEmpty()) {
            List<ServicePoint__c> newENELAreaServicePoints = new List<ServicePoint__c>();
            List<ServicePoint__c> newNonENELAreaServicePoints = new List<ServicePoint__c>();

            Set<Id> distributorIds = new Set<Id>();
            for (ServicePoint__c sp : servicePoints) {
                System.debug('### sp.Id: '+sp.Id);
                System.debug('### sp.ENELTEL__c: '+sp.ENELTEL__c);
                System.debug('### sp.CurrentSupply__c: '+sp.CurrentSupply__c);
                System.debug('### sp.Distributor__c: '+sp.Distributor__c);
                if (sp.ENELTEL__c == null && sp.CurrentSupply__r != null && sp.Distributor__c != null) {
                    distributorIds.add(sp.Distributor__c);
                }
            }
            System.debug('### distributorIds: '+distributorIds);
            Map<Id, Account> distributors = new Map<Id, Account>(accountQuery.listDistributorsByIds(distributorIds));
            Set<Id> companyDivisionIds = new Set<Id>();

            for (ServicePoint__c sp : servicePoints) {
                if (sp.ENELTEL__c == null && sp.CurrentSupply__r != null && sp.Distributor__c != null) {
                    Account distributor = distributors.get(sp.Distributor__c);
                    if (distributor.IsDisCoENEL__c) {
                        newENELAreaServicePoints.add(sp);
                    }
                    else {
                        newNonENELAreaServicePoints.add(sp);
                        companyDivisionIds.add(sp.CurrentSupply__r.CompanyDivision__c);
                    }
                }
            }

            if (!newENELAreaServicePoints.isEmpty()) {
                for (ServicePoint__c sp : newENELAreaServicePoints) {
                    sp.ENELTEL__c = sp.Code__c.right(9);
                }
            }

            if (!newNonENELAreaServicePoints.isEmpty()) {
                MRO_QR_Sequencer sequencerQueries = MRO_QR_Sequencer.getInstance();
                Map<Id, Sequencer__c> sequencersMap = sequencerQueries.getENELTELSequencers(companyDivisionIds);

                for (ServicePoint__c sp : newNonENELAreaServicePoints) {
                    Sequencer__c seq = sequencersMap.get(sp.CurrentSupply__r.CompanyDivision__c);
                    System.debug('### sequencer: ' + seq);
                    seq.Sequence__c += 1;
                    String partialCode = String.valueOf(seq.Sequence__c.intValue()).leftPad(seq.SequenceLength__c.intValue(), '0');
                    /*
                    Integer checkDigit = 0;
                    for (String ch : partialCode.split('')) {
                        checkDigit += Integer.valueOf(ch);
                    }
                    sp.ENELTEL__c = seq.CompanyPrefix__c + '9' + partialCode + Math.mod(checkDigit, 10);
                     */
                    sp.ENELTEL__c = seq.CompanyPrefix__c + partialCode;
                }

                databaseSrv.updateSObject(sequencersMap.values());
            }
        }
    }

    public List<Supply__c> updateSupplies(List<Supply__c> supplyList) {
        List<Supply__c> output = supplyList;
        if(supplyList!=null && !supplyList.isEmpty()) {
            try {
                databaseSrv.updateSObject(output);
            } catch (Exception e) {
                throw e;
            }
        }
        return output;
    }

    public Supply__c copySupply(Supply__c oldSupply){
        return new Supply__c(
                FlatRate__c = oldSupply.FlatRate__c,
                ServicePoint__c = oldSupply.ServicePoint__c,
                ServiceSite__c = oldSupply.ServiceSite__c,
                NACEReference__c = oldSupply.NACEReference__c,
                Market__c = oldSupply.Market__c,
                NonDisconnectable__c = oldSupply.NonDisconnectable__c,
                RecordTypeId = oldSupply.RecordTypeId,
                NonDisconnectableReason__c = oldSupply.NonDisconnectableReason__c,
                BillingProfile__c = oldSupply.BillingProfile__c,
                CompanyDivision__c = oldSupply.CompanyDivision__c,
                TitleDeedValidity__c = oldSupply.TitleDeedValidity__c,
                SupplyOperation__c = oldSupply.SupplyOperation__c,
                Product__c = oldSupply.Product__c,
                Contract__c = oldSupply.Contract__c,
                Account__c = oldSupply.Account__c
        );
    }

    public void unsuspendVASActivationCases(List<Supply__c> activatingVASSupplies) {
        Map<Id, Case> vasActivationCasesToUnsuspend = new Map<Id, Case>();
        Map<Id, Case> vasActivationCasesToPush = new Map<Id, Case>();
        for (Supply__c supply : activatingVASSupplies) {
            if (supply.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVATING && supply.Activator__c != null && supply.Activator__r.Status == constantsSrv.CASE_STATUS_ONHOLD) {
                Case vasActivationCase = supply.Activator__r;
                vasActivationCase.Status = constantsSrv.CASE_STATUS_NEW;
                vasActivationCasesToUnsuspend.put(vasActivationCase.Id, vasActivationCase);
                if (supply.Contract__r.Status == constantsSrv.CONTRACT_STATUS_SIGNED) {
                    vasActivationCasesToPush.put(vasActivationCase.Id, vasActivationCase);
                }
            }
        }
        if (!vasActivationCasesToUnsuspend.isEmpty()) {
            update vasActivationCasesToUnsuspend.values();
        }
        if (!vasActivationCasesToPush.isEmpty()) {
            MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
            for (Case c : vasActivationCasesToPush.values()) {
                transitionsUtl.checkAndApplyAutomaticTransitionWithTag(c, constantsSrv.DOCUMENT_VALIDATION_TAG);
            }
        }
    }
}