/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 08.05.20.
 */

public with sharing class MRO_QR_FileImportDataRow {

    public static MRO_QR_FileImportDataRow getInstance() {
        return new MRO_QR_FileImportDataRow();
    }

    public List<FileImportDataRow__c> listByFileImportJobID(String jobId) {
        return [SELECT Id, JSONRow__c, FileImportJob__c, ErrorMessage__c FROM FileImportDataRow__c WHERE FileImportJob__c = :jobId];
    }

    public String lastErrorFromJob(String jobId){
        return lastRowWithErrorFromJob(jobId)?.ErrorMessage__c;
    }

    public FileImportDataRow__c lastRowWithErrorFromJob(String jobId){
        List<FileImportDataRow__c> rows = [SELECT Id, ErrorMessage__c FROM FileImportDataRow__c WHERE FileImportJob__c = :jobId AND ErrorMessage__c != null ORDER BY CreatedDate DESC LIMIT 1];
        if(rows.size() > 0){
            return rows[0];
        }
        return null;
    }
}