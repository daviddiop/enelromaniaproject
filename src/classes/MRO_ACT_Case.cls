/**
 * Created by vincenzo.scolavino on 03/04/2020.
 */

global with sharing class MRO_ACT_Case implements wrts_prcgvr.Interfaces_1_0.IApexAction {
    private static MRO_QR_Supply supplyQuery            = MRO_QR_Supply.getInstance();
    public static MRO_QR_Case caseQuery                 = MRO_QR_Case.getInstance();
    public static MRO_QR_ServicePoint servicePointQuery = MRO_QR_ServicePoint.getInstance();
    public static MRO_QR_ServiceSite serviceSiteQuery   = MRO_QR_ServiceSite.getInstance();
    private static DatabaseService databaseSrv          = DatabaseService.getInstance();
    private static final MRO_UTL_Constants CONSTANTS    = MRO_UTL_Constants.getAllConstants();
    private static final MRO_SRV_VAS vasService         = new MRO_SRV_VAS();
    private static final MRO_QR_Asset assetQuery        = MRO_QR_Asset.getInstance();
    private static final MRO_QR_InstallmentPlan installmentPlanQuery = MRO_QR_InstallmentPlan.getInstance();

    public Object execute(Object param) {
        System.debug('Class : MRO_ACT_Case -- method : execute');
        System.debug('input : ' + param);

        Map<String, Object> paramsMap = (Map<String, Object>) param;
        Case c = (Case) paramsMap.get('sender');
        String method = (String) paramsMap.get('method');
        System.debug('case : ' + c);

        wrts_prcgvr__PhaseTransition__c transition = (wrts_prcgvr__PhaseTransition__c) paramsMap.get('transition');
        //wrts_prcgvr__PhaseTransitionDetail__c action = (wrts_prcgvr__PhaseTransitionDetail__c) paramsMap.get('action');

        this.methodSwitcher(method, c, transition);

        return null;
    }

    private void methodSwitcher(String method, Case obj, wrts_prcgvr__PhaseTransition__c transition) {
        System.debug('methodSwitcher');
        switch on method {
            when 'sendNlcClcDataChangeToSap' {
                this.sendNlcClcDataChangeToSap(obj);
            }
            when 'hardVasSapAnswerReceived' {
                this.hardVasSapAnswerReceived(obj);
            }
            when 'softVasSapAnswerReceived' {
                this.softVasSapAnswerReceived(obj);
            }
            when 'pushActivationCase' {
                this.pushActivationCase(obj);
            }
            when 'hardVASProductDelivered' {
                this.updateAssetDeliveryDate(obj);
            }
            when 'hardVASProductInstalled' {
                this.updateAssetInstallationDate(obj);
                this.updateCaseInvoiceDate(obj);
                this.updateInstallmentPlan(obj);
            }
            when 'assignToCreditManagement' {
                this.assignToCreditManagement(obj);
            }
            when 'assignToCustomerCare' {
                this.assignToCustomerCare(obj);
            }
            when 'dynamicQueueReassignment' {
                this.dynamicQueueReassignment(obj, transition);
            }
            when 'atrExecutorCorrection' {
                this.atrExecutorCorrection(obj);
            }
            when 'resubmitCallout' {
                this.resubmitCallout(obj);
            }
            when 'commitServicePointTechnicalData' {
                this.commitServicePointTechnicalData(obj);
            }
            when 'increaseCaseNumber' {
                this.increaseCaseNumber(obj);
            }
            when 'processPowerVoltageChangeToProductChange' {
                this.processPowerVoltageChangeToProductChange(obj);
            }
            when else {
                throw new WrtsException('Unrecognized method: ' + method);
            }
        }
    }

    public void updateAssetDeliveryDate(Case c) {
        Asset caseAsset = assetQuery.getById(c.AssetId);
        caseAsset.NE__Delivery_Date__c = Date.today();
        databaseSrv.updateSObject(caseAsset);
    }

    public void updateAssetInstallationDate(Case c) {
        Asset caseAsset = assetQuery.getById(c.AssetId);
        caseAsset.InstallDate = Date.today();
        databaseSrv.updateSObject(caseAsset);
    }

    public void updateCaseInvoiceDate(Case c) {
        List<InstallmentPlan__c> installmentPlans = installmentPlanQuery.listByCaseId(c.Id);
        if (installmentPlans.isEmpty()){
            return ;
        }
        InstallmentPlan__c installmentPlan = installmentPlans.get(0);

        c.InvoiceDate__c = installmentPlan.StartDate__c;
    }

    public void updateInstallmentPlan(Case c) {
        List<InstallmentPlan__c> installmentPlans = installmentPlanQuery.listByCaseId(c.Id);
        if (installmentPlans.isEmpty()){
            return ;
        }
        InstallmentPlan__c installmentPlan = installmentPlans.get(0);

        if (Date.today() <= installmentPlan.StartDate__c) {
            MRO_LC_ExternalProductWizard.updateInstallmentPlan(c.Id);
        }
    }

    public void softVasSapAnswerReceived(Case c) {
        System.debug('softVasSapAnswerReceived');
        updateSoftVasCaseEffectiveDate(c);
    }

    public void hardVasSapAnswerReceived(Case c) {
        System.debug('hardVasSapAnswerReceived');
    }

    private void updateSoftVasCaseEffectiveDate(Case c){
        c.EffectiveDate__c = vasService.computeVasActivationDate();
    }

    private void upsertCase(Case c){
        if (!Utils.firstTimeEntry) {
            Utils.firstTimeEntry = true;
        } else {
            Utils.firstTimeEntry = false;
            return;
        }

        databaseSrv.upsertSObject(c);
    }

    public void sendNlcClcDataChangeToSap(Case c) {
        //Case cs = caseQuery.getById(c.Id);
        //boolean operationSuccess = true;

        // SEND DATA TO SAP WAITING FOR RESPONSE
        //-------------------------------------

        /*if (operationSuccess) {
            String newNclClc = c.NLCCLC__c;
            Supply__c s = supplyQuery.getById(c.Supply__c);
            ServiceSite__c ss = serviceSiteQuery.getById(s.ServiceSite__c);

            boolean isElectricCommodity = ss.CLC__c == null || String.isBlank(ss.CLC__c);
            if (isElectricCommodity)
                ss.NLC__c = newNclClc;
            else
                ss.CLC__c = newNclClc;

            databaseSrv.updateSObject(ss);

            if(isElectricCommodity && ss.NLC__c!=newNclClc){
                throw new WrtsException('Error in updating NLC__c in ServiceSite: ' + ss.Id);
            }
            if(!isElectricCommodity && ss.CLC__c!=newNclClc){
                throw new WrtsException('Error in updating CLC__c in ServiceSite: ' + ss.Id);
            }
         */
        System.debug('data sent to sap ');

        //}
    }

    private void pushActivationCase(Case connectionCase) {
        //connectionCase = caseQuery.getById(connectionCase.Id);
        //connectionCase.SuspendChildren__c = false;
        //update connectionCase; -- Cannot retrigger!

        List<Case> childrenCases = caseQuery.listSuspendedChildrenCases(connectionCase.Id);
        List<Case> casesToPush = new List<Case>();
        Date newEffectiveDate = MRO_UTL_Date.isNotWorkingDate(Date.today()) ? MRO_UTL_Date.addWorkingDays(Date.today(), 1) : Date.today();
        for (Case c : childrenCases) {
            c.Status = CONSTANTS.CASE_STATUS_NEW;
            c.EffectiveDate__c = newEffectiveDate;
            if (c.Dossier__r.Phase__c == CONSTANTS.DOSSIER_PHASE_DOCUMENTSVERIFIED) {
                casesToPush.add(c);
            }
        }
        update childrenCases;

        MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
        for (Case c : casesToPush) {
            transitionsUtl.checkAndApplyAutomaticTransitionWithTag(c, CONSTANTS.DOCUMENT_VALIDATION_TAG, true, false);
        }
    }

    private void resubmitCallout(Case caseRecord) {
        if (FeatureManagement.checkPermission('MRO_ResubmitCallouts')) {
            wrts_prcgvr__AsyncJob__c pendingCallout = MRO_QR_AsyncJob.getInstance().getPendingCalloutJobByRecordId(caseRecord.Id);
            if (pendingCallout != null) {
                delete pendingCallout;
            }
            Case c = new Case(Id = caseRecord.Id);
            MRO_UTL_ApexContext.FORCE_CALLOUTS_RETRIGGERING = true;
            update c;
        } else {
            throw new WrtsException(String.format(Label.PermissionTo, new List<String>{'resubmit callouts'}));
        }
    }

    private void assignToCreditManagement(Case caseRecord){
        Group creditManagementQueue = MRO_QR_User.getInstance().getQueueByDeveloperName('CreditManagementQueue');
        if(creditManagementQueue != null) {
            update new Case(Id = caseRecord.Id, OwnerId = creditManagementQueue.Id);
        }
    }

    private void assignToCustomerCare(Case caseRecord){
        Group customerCareQueue = MRO_QR_User.getInstance().getQueueByDeveloperName('CustomerCare');
        if(customerCareQueue != null) {
            update new Case(Id = caseRecord.Id, OwnerId = customerCareQueue.Id);
        }
    }

    private void dynamicQueueReassignment(Case caseRecord, wrts_prcgvr__PhaseTransition__c transition) {
        User createdBy = [SELECT Department FROM User WHERE Id = :caseRecord.CreatedById];
        Id queueId;
        //TODO - Calculate destination queue based on CreatedBy, Origin and Phase
        caseRecord.OwnerId = queueId;
    }

    private void atrExecutorCorrection(Case caseRecord) {
        String fieldsTemplateCode = caseRecord.NeedsDesigner__c ? 'ATR_EXEC_DESIGN_CORRECTION' : 'ATR_EXEC_CORRECTION';
        MRO_SRV_Activity.getInstance().insertDataEntryActivity(caseRecord.Id, caseRecord.OwnerId, null, fieldsTemplateCode, false);
    }

    private void commitServicePointTechnicalData(Case caseRecord) {
        Case c = caseQuery.getById(caseRecord.Id);
        ServicePoint__c sp;
        if (c.Supply__c != null) {
            sp = new ServicePoint__c(
                Id = c.Supply__r.ServicePoint__c
            );
        } else if (c.ServicePointCode__c != null) {
            sp = servicePointQuery.getListServicePoints(c.ServicePointCode__c)[0];
        }
        sp = MRO_SRV_ServicePoint.getInstance().updateServicePoint(sp, caseRecord);
        update sp;
    }

    private void increaseCaseNumber(Case caseRecord) {
        if(caseRecord.IntegrationKey__c == null){
            caseRecord.IntegrationKey__c = caseRecord.CaseNumber;
        }
        List<String> split = caseRecord.IntegrationKey__c.split('_');
        Integer increment = split.size() > 1 ? Integer.valueOf(split[1]) : 0;
        caseRecord.IntegrationKey__c = split[0] + '_' + (increment + 1);
    }

    public void processPowerVoltageChangeToProductChange(Case caseRecord) {
        // Query because createProductChangeActivity method need case's related object fields
        Case caseRec = caseQuery.getById(caseRecord.Id);
        if (caseRec != null) {
            MRO_UTL_PowerVoltageChange powerVoltageChangeUTL = MRO_UTL_PowerVoltageChange.getInstance();
            Boolean checkIsCaseChanged = powerVoltageChangeUTL.logicIsCaseChanged(caseRec);
            ServicePoint__c sp = caseRec.Supply__r.ServicePoint__r;
            sp = MRO_SRV_ServicePoint.getInstance().updateServicePoint(sp, caseRecord);
            update sp;
            if (checkIsCaseChanged) {
                powerVoltageChangeUtl.createProductChangeActivity(new List<Case>{caseRec});
            }
        }
    }
}