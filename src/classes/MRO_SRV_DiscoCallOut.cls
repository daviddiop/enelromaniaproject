/* Created by napoli on 04/11/2019.
 */

public with sharing class MRO_SRV_DiscoCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout {
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap){
        System.debug('MRO_SRV_DiscoCallOut.buildMultiRequest');

        //Sobject
        SObject obj = (SObject) argsMap.get('sender');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        //Query
        Case richiesta = [
                SELECT
                //Case
                        Type,
                        Description,
                        Outcome__c,
                        Designer__c,
                        ExecutionTerm__c,
                        CustomerNotes__c,
                        DisCoNotes__c,
                        SourceId,
                        ExternalRequestID__c,
                        IntegrationKey__c,
                        EffectiveDate__c,
                        IncidentDate__c,
                        Market__c,
                        Subject,
                        SubProcess__c,
                        CustomerConfirmationDate__c,
                        Subtype__c,
                        Distance__c,
                        RequiredForConnection__c,
                        CaseTypeCode__c,
                        AccountNationalIdentityNumber__c,
                        //
                        CreatedDate,
                        RecordType.DeveloperName,
                        PowerPhase__c,
                        Voltage__c,
                        VoltageSetting__c,
                        ContractualPower__c,
                        AvailablePower__c,
                        //
                        DesignTerm__c,
                        DesignAmount__c,
                        ExecutionAmount__c,
                        //Indirizzo nuova fornitura CONN-001
                        AddressStreetType__c,
                        AddressStreetName__c,
                        AddressPostalCode__c,
                        AddressStreetNumber__c,
                        AddressStreetNumberExtn__c,
                        AddressBlock__c,
                        AddressFloor__c,
                        AddressApartment__c,
                        AddressCity__c,
                        AddressProvince__c,
                        AddressBuilding__c,
                        //ParentCase
                        Parent.IntegrationKey__c,
                        Parent.ExternalRequestID__c,
                        //DESIGNER
                        Designer__r.VATNumber__c,

                        //EXECUTOR
                        Executor__r.VATNumber__c,

                        //Distributor
                        Distributor__r.VATNumber__c,
                        Supply__r.ServicePoint__r.Distributor__r.VATNumber__c,

                        //Trader
                        Dossier__r.CompanyDivision__r.VATNumber__c,
                        CompanyDivision__r.VATNumber__c,
                        Trader__r.VATNumber__c,
                        Supply__r.ServicePoint__r.Trader__r.VATNumber__c,

                        //Contract
                        Contract__r.Name,
                        Contract__r.CustomerSignedDate,

                        //Supply
                        Supply__r.ExpirationDate__c,
                        Supply__r.NACECode__c,
                        Supply__r.SupplyOperation__c,

                        //ServicePoint
                        Supply__r.ServicePoint__c,
                        Supply__r.ServicePoint__r.ContractualPower__c,
                        Supply__r.ServicePoint__r.PointApartment__c,
                        Supply__r.ServicePoint__r.PointBlock__c,
                        Supply__r.ServicePoint__r.PointCity__c,
                        Supply__r.ServicePoint__r.PointStreetName__c,
                        Supply__r.ServicePoint__r.PointProvince__c,
                        Supply__r.ServicePoint__r.PointFloor__c,
                        Supply__r.ServicePoint__r.PointStreetNumber__c,
                        Supply__r.ServicePoint__r.PointStreetNumberExtn__c,
                        Supply__r.ServicePoint__r.PointBuilding__c,
                        Supply__r.ServicePoint__r.AvailablePower__c,
                        Supply__r.ServicePoint__r.OwnerId,
                        Supply__r.ServicePoint__r.Voltage__c,
                        Supply__r.ServicePoint__r.PointStreetType__c,
                        Supply__r.ServicePoint__r.Name,
                        Supply__r.ServicePoint__r.PointPostalCode__c,
                        Supply__r.ServicePoint__r.CurrentSupply__c,
                        Supply__r.ServicePoint__r.EstimatedConsumption__c,

                        //ServiceSite
                        ServiceSite__r.SiteApartment__c,
                        ServiceSite__r.SiteBlock__c,
                        ServiceSite__r.SiteBuilding__c,
                        ServiceSite__r.SiteCity__c,
                        ServiceSite__r.SiteCountry__c,
                        ServiceSite__r.SiteFloor__c,
                        ServiceSite__r.SiteLocality__c,
                        ServiceSite__r.SitePostalCode__c,
                        ServiceSite__r.SiteProvince__c,
                        ServiceSite__r.SiteStreetName__c,
                        ServiceSite__r.SiteStreetNumber__c,
                        ServiceSite__r.SiteStreetNumberExtn__c,
                        ServiceSite__r.SiteStreetType__c,

                        //Account
                        Account.ResidentialBlock__c,
                        Account.ResidentialCity__c,
                        Account.ONRCCode__c,
                        Account.Name,
                        Account.ConsumerType__c,
                        Account.RecordType.DeveloperName,
                        Account.ResidentialStreetName__c,
                        Account.ResidentialCountry__c,
                        Account.ResidentialFloor__c,
                        Account.ResidentialStreetNumber__c,
                        Account.ResidentialStreetNumberExtn__c,
                        Account.ResidentialStreetType__c,
                        Account.ResidentialPostalCode__c,
                        Account.ResidentialProvince__c,
                        Account.ResidentialApartment__c,
                        Account.ResidentialAddress__c,
                        Account.ResidentialBuilding__c,
                        Account.Email__c,
                        Account.PersonEmail,
                        Account.IsPersonAccount,
                        Account.VATNumber__c,
                        Account.Fax,
                        Account.NationalIdentityNumber__pc,
                        Account.LegalRepresentative__pc,
                        Account.Phone,
                        Account.PersonMobilePhone,

                        //Contact
                        Account.PrimaryContact__r.MobilePhone,
                        Account.PrimaryContact__r.Phone,
                        Account.RecordType.IsPersonType,
                        //
                        Contact.Phone,
                        Contact.MobilePhone,
                        Contact.Fax,
                        Contact.Email,
                        Contact.Name,

                        //BillingProfile
                        BillingProfile__r.Bank__r.Name,
                        BillingProfile__r.IBAN__c,
                        BillingProfile__r.RefundBank__r.Name,
                        BillingProfile__r.RefundIBAN__c,
                        BillingProfile__r.BillingApartment__c,
                        BillingProfile__r.BillingBlock__c,
                        BillingProfile__r.BillingBuilding__c,
                        BillingProfile__r.BillingCity__c,
                        BillingProfile__r.BillingStreetName__c,
                        BillingProfile__r.BillingProvince__c,
                        BillingProfile__r.BillingFloor__c,
                        BillingProfile__r.BillingStreetNumber__c,
                        BillingProfile__r.BillingStreetNumberExtn__c,
                        BillingProfile__r.BillingStreetType__c,
                        BillingProfile__r.BillingPostalCode__c,
                        BillingProfile__r.BillingCountry__c,
                        //
                        ContractAccount__r.BillingProfile__r.BillingApartment__c,
                        ContractAccount__r.BillingProfile__r.BillingBlock__c,
                        ContractAccount__r.BillingProfile__r.BillingCity__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetName__c,
                        ContractAccount__r.BillingProfile__r.BillingProvince__c,
                        ContractAccount__r.BillingProfile__r.BillingFloor__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetNumber__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetNumberExtn__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetType__c,
                        ContractAccount__r.BillingProfile__r.BillingPostalCode__c,
                        ContractAccount__r.BillingProfile__r.BillingCountry__c,
                        //Index
                (SELECT
                        Id,
                        MeterNumber__c,
                        ActiveEnergyR1__c,
                        ActiveEnergyR2__c,
                        ActiveEnergyR3__c,
                        CapacitiveEnergy__c,
                        InductiveEnergy__c
                FROM Indexes__r),

                        //Dossier
                        Dossier__c, //Don't delete, it is used in the query below
                        Dossier__r.Name,
                        Dossier__r.ApplicantType__c,
                        Dossier__r.SwitchNotificationDate__c,
                        Dossier__r.ReferenceDate__c,
                        Dossier__r.Opportunity__r.Amount,
                        Dossier__r.Opportunity__r.RequestType__c

                FROM Case
                WHERE Id = :obj.Id LIMIT 1
        ];

        if(String.isBlank(richiesta.AddressStreetNumber__c)) richiesta.AddressStreetNumber__c = 'FN';
        if(richiesta.Supply__c != null && richiesta.Supply__r.ServicePoint__c != null && String.isBlank(richiesta.Supply__r.ServicePoint__r.PointStreetNumber__c)) richiesta.Supply__r.ServicePoint__r.PointStreetNumber__c = 'FN';
        if(richiesta.AccountId != null && String.isBlank(richiesta.Account.ResidentialStreetNumber__c))  richiesta.Account.ResidentialStreetNumber__c = 'FN';
        if(richiesta.BillingProfile__c != null && String.isBlank(richiesta.BillingProfile__r.BillingStreetNumber__c)) richiesta.BillingProfile__r.BillingStreetNumber__c = 'FN';
        if(richiesta.ContractAccount__c != null && richiesta.ContractAccount__r.BillingProfile__c != null && String.isBlank(richiesta.ContractAccount__r.BillingProfile__r.BillingStreetNumber__c)) richiesta.ContractAccount__r.BillingProfile__r.BillingStreetNumber__c = 'FN';

        Set<String> processWithRefundIBAN = new Set<String>();
        processWithRefundIBAN.add('ATRExtension');
        processWithRefundIBAN.add('Location_Approval');
        if(processWithRefundIBAN.contains(richiesta.RecordType.DeveloperName) && richiesta.BillingProfile__c != null) {
            if(String.isNotBlank(richiesta.BillingProfile__r.RefundIBAN__c)) richiesta.BillingProfile__r.IBAN__c = richiesta.BillingProfile__r.RefundIBAN__c;
            if(richiesta.BillingProfile__r.RefundBank__r != null) richiesta.BillingProfile__r.Bank__r = richiesta.BillingProfile__r.RefundBank__r;
        }

        if(richiesta.Account.Email__c == null && richiesta.Account.PersonEmail != null && richiesta.Account.IsPersonAccount == true) richiesta.Account.Email__c = richiesta.Account.PersonEmail;

        //Change RequestId
        request.header.requestId = request.header.requestId + '|' + richiesta.IntegrationKey__c;

        //FileMetadata
        Set<String> processWithDocumentsRelatedToCase = new Set<String>{'Connection_ELE','MeterChangeEle','TechnicalDataChange_ELE','ATRExtension','Location_Approval'};
        List<ValidatableDocument__c> documentsFromDossier;
        if (processWithDocumentsRelatedToCase.contains(richiesta.RecordType.DeveloperName)) {
            documentsFromDossier = [SELECT FileMetadata__r.DiscoFileId__c,CustomerSignedDate__c,
                    DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c
            FROM ValidatableDocument__c
            WHERE FileMetadata__c != NULL AND FileMetadata__r.DiscoFileId__c != NULL
            AND Case__c = :richiesta.Id
            AND DocumentBundleItem__r.ArchiveOnDisCo__c = TRUE
            AND DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c LIKE 'DISCO%'];
        } else {
            documentsFromDossier = [
                    SELECT FileMetadata__r.DiscoFileId__c,CustomerSignedDate__c,
                            DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c
                    FROM ValidatableDocument__c
                    WHERE FileMetadata__c != NULL AND FileMetadata__r.DiscoFileId__c != NULL
                    AND Dossier__c = :richiesta.Dossier__c
                    AND DocumentBundleItem__r.ArchiveOnDisCo__c = TRUE
                    AND DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c LIKE 'DISCO%'
            ];
        }

        if(request.header.requestType == 'CANC-001'){
            richiesta.DisCoNotes__c = 'Retention';
            if(richiesta.Distributor__c == null) richiesta.Distributor__r = richiesta.Supply__r.ServicePoint__r.Distributor__r;
        }

        if(richiesta.RecordType.DeveloperName == 'Complaint' && richiesta.Parent != null){
            richiesta.Parent.IntegrationKey__c = null;
            richiesta.Parent.ExternalRequestID__c = null;
        }

        //to WObject
        Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject(richiesta, null);

        if(richiesta.RecordType.DeveloperName == 'Complaint'){
            mrrObj.fields.add(MRO_UTL_MRRMapper.createField('ApplicantType__c', richiesta.Dossier__r.ApplicantType__c.toUpperCase()));
        }

        for(ValidatableDocument__c vd : documentsFromDossier){
            Wrts_prcgvr.MRR_1_0.WObject mrrDoc = MRO_UTL_MRRMapper.sObjectToWObject(vd, 'ValidatableDocument__r');
            MRO_UTL_MRRMapper.appendChildRecords(richiesta.Dossier__c, mrrObj, mrrDoc);
        }

        wrts_prcgvr.MRR_1_0.field f = new wrts_prcgvr.MRR_1_0.field();
        f.fieldType = 'String';
        f.name = 'Trader_Vat';
        f.value = richiesta.Dossier__r.CompanyDivision__r.VATNumber__c;
        request.header.fields.add(f);

        if(documentsFromDossier != null && !documentsFromDossier.isEmpty()){
            for (ValidatableDocument__c validatableDocument : documentsFromDossier) {
                if (validatableDocument.CustomerSignedDate__c != null) {
                    wrts_prcgvr.MRR_1_0.field customerSignedDate = new wrts_prcgvr.MRR_1_0.field();
                    customerSignedDate.fieldType = 'Date';
                    customerSignedDate.name = 'CustomerConfirmationDate__c';
                    customerSignedDate.value = String.valueOf((Date) validatableDocument.CustomerSignedDate__c);

                    mrrObj.fields.add(customerSignedDate);
                    break;
                }
            }
        }

        //Add mrrObj to the request
        request.objects.add(mrrObj);

        //Add request to Multirequest
        multiRequest.requests.add(request);

        return multiRequest;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response = new wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse();
        if(calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
            if(calloutResponse.responses[0].code == 'OK'){
                response.success = true;
                if(calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
                    List<Case> caseList = new List<Case>();
                    for(wrts_prcgvr.MRR_1_0.WObject wo : calloutResponse.responses[0].objects){
                        if(wo.objectType == 'Case'){
                            Case c = (Case) MRO_UTL_MRRMapper.wobjectToSObject(wo);
                            if(c.Id != null) caseList.add(c);
                        }
                    }
                    if(!caseList.isEmpty()) update caseList;
                }
            }else{
                response.success = false;
                //            errorType = 'Functional error';
            }
            response.message = calloutResponse.responses[0].description;
        }
        return response;
    }
}