
public with sharing class MRO_UTL_PowerVoltageChange {

    private static MRO_QR_Activity activityQuery = MRO_QR_Activity.getInstance();
    private static MRO_SRV_Activity activitySrv = MRO_SRV_Activity.getInstance();
    static MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();
    public static MRO_QR_Case casesQuery = MRO_QR_Case.getInstance();
    static Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();

    public static MRO_UTL_PowerVoltageChange getInstance() {
        return (MRO_UTL_PowerVoltageChange) ServiceLocator.getInstance(MRO_UTL_PowerVoltageChange.class);
    }

    public void createProductChangeActivity(List<Case> caseList) {
        String accountId = caseList[0].AccountId;
        String dossierId = caseList[0].Dossier__c;
        Id dossierProductChange;
        List<wrts_prcgvr__Activity__c> listCustomerProductChange = activityQuery.getActivityByAccountAndType(accountId, constantsUtl.CUSTOMER_PRODUCT_CHANGE);
        if (listCustomerProductChange.isEmpty()) {
            wrts_prcgvr__Activity__c customerProductChange = activitySrv.insertCustomerActivity(caseList[0], constantsUtl.CUSTOMER_PRODUCT_CHANGE);
            listCustomerProductChange.add(customerProductChange);
        }
        if (caseRts.get('TechnicalDataChange_ELE').getRecordTypeId() == caseList[0].RecordTypeId) {
            List<wrts_prcgvr__Activity__c> listDossierProductChangeElectric = activityQuery.getActivityByDossierAndType(dossierId, constantsUtl.DOSSIER_PRODUCT_CHANGE_ELE);
            if (listDossierProductChangeElectric.isEmpty()) {
                wrts_prcgvr__Activity__c dossierProductChangeEle = activitySrv.insertDossierActivity(caseList[0], listCustomerProductChange[0].Id, constantsUtl.DOSSIER_PRODUCT_CHANGE_ELE);
                dossierProductChange = dossierProductChangeEle.Id;
            } else {
                dossierProductChange = listDossierProductChangeElectric[0].Id;
                activitySrv.updateActivityStatus(listDossierProductChangeElectric[0],constantsUtl.ACTIVITY_STATUS_NOT_STARTED);
            }
        } else {
            List<wrts_prcgvr__Activity__c> listDossierProductChangeGas = activityQuery.getActivityByDossierAndType(dossierId, constantsUtl.DOSSIER_PRODUCT_CHANGE_GAS);
            if (listDossierProductChangeGas.isEmpty()) {
                wrts_prcgvr__Activity__c dossierProductChangeGas = activitySrv.insertDossierActivity(caseList[0], listCustomerProductChange[0].Id, constantsUtl.DOSSIER_PRODUCT_CHANGE_GAS);
                dossierProductChange = dossierProductChangeGas.Id;
            } else {
                dossierProductChange = listDossierProductChangeGas[0].Id;
                activitySrv.updateActivityStatus(listDossierProductChangeGas[0],constantsUtl.ACTIVITY_STATUS_NOT_STARTED);
            }
        }

        activitySrv.insertChildActivity(caseList, constantsUtl.SERVICE_POINT_PRODUCT_CHANGE, dossierProductChange);
    }

    public Boolean logicProductChangeCheck(Case caseRecord) {

        Boolean check = null;
        Map<String, Boolean > criteria = new Map<String, Boolean>();

        if (caseRecord.Supply__c != null && caseRecord.Supply__r.ServicePoint__c != null && caseRecord.Supply__r.ServicePoint__r.Distributor__c != null) {
            criteria.put(
                    'logic_1',
                    (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == true && caseRecord.Origin == 'Mail') &&
                            (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(true) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(true))))
            );
            criteria.put(
                    'logic_2',
                    (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == false && caseRecord.Origin == 'Mail') &&
                            (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(true) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false))))
            );
            criteria.put(
                    'logic_3',
                    (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == true && caseRecord.Origin != 'Mail') &&
                            (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(false) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(true))))
            );
            criteria.put(
                    'logic_4',
                    (caseRecord.Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c == false && caseRecord.Origin != 'Mail') &&
                            (caseRecord.EffectiveDate__c >= (caseRecord.ReferenceDate__c.addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(false) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false))))
            );
        }

        if (!criteria.isEmpty()) {
            check = criteria.get('logic_1') || criteria.get('logic_2') || criteria.get('logic_3') || criteria.get('logic_4');
        }

        return check;
    }

    public Boolean logicIsCaseChanged(Case caseRecord){

        Boolean isChanged = false;
        Map<String, Boolean > criteria = new Map<String, Boolean>();

        if(caseRecord.Supply__c != null && caseRecord.Supply__r.ServicePoint__c != null){
            /*criteria.put(
                    'field_1',
                    (caseRecord.AvailablePower__c != caseRecord.Supply__r.ServicePoint__r.AvailablePower__c)
            );
            criteria.put(
                    'field_2',
                    (caseRecord.ContractualPower__c != caseRecord.Supply__r.ServicePoint__r.ContractualPower__c)
            );
            criteria.put(
                    'field_3',
                    (caseRecord.EstimatedConsumption__c != caseRecord.Supply__r.ServicePoint__r.EstimatedConsumption__c)
            );
            criteria.put(
                    'field_4',
                    (caseRecord.PowerPhase__c != caseRecord.Supply__r.ServicePoint__r.PowerPhase__c)
            );*/
            criteria.put(
                    'field_5',
                    (caseRecord.VoltageLevel__c != caseRecord.Supply__r.ServicePoint__r.VoltageLevel__c)
            );
            /*criteria.put(
                    'field_6',
                    (caseRecord.Voltage__c != caseRecord.Supply__r.ServicePoint__r.Voltage__c)
            );*/
        }

        if(!criteria.isEmpty()){
            //isChanged = criteria.get('field_1') || criteria.get('field_2') || criteria.get('field_3') || criteria.get('field_4') || criteria.get('field_5') || criteria.get('field_6');
            isChanged = criteria.get('field_5');
        }

        return isChanged;
    }

}