/**
 * Created by A701948 on 15/09/2019.
 */

public with sharing class MRO_LC_Address {
    private static MRO_SRV_Address addressSrv = MRO_SRV_Address.getInstance();

    @AuraEnabled(cacheable=true)
    public static List< String> getValues(String fieldName, Map<String, String> address) {
        AddressField__mdt[] addressFields = MRO_SRV_Address.getAddressStructure('DefaultAddressLayout');
        AddressField__mdt addressField ;
        for (AddressField__mdt a : addressFields) {
            if (a.FieldName__c == fieldName) {
                addressField = a;
                break;
            }set
        }
        Set<String> results = new Set<String>() ;
        if (addressField != null && String.isNotBlank(addressField.SourceTable__c)) {
            String sourceField = (addressField.SourceField__c).trim() ;
//            String qualifiedApiName = (String.isNotBlank(addressField.SourceField__c) ? addressField.SourceField__c : addressField.QualifiedApiName).trim();
            if (sourceField != 'Name') {
                sourceField += '__c';
            }
            String startWith = address.get(addressField.QualifiedApiName).trim() + '%' ;
            if (addressField.QualifiedApiName == 'StreetName') {
                startWith = '%' + startWith;
            }
            String queryString = 'SELECT ' + sourceField + ' FROM ' + addressField.SourceTable__c.trim() + '__c WHERE ' + sourceField + ' LIKE :startWith';
            if (String.isNotBlank(addressField.ParentsFields__c)) {
                for (String parentFieldName : addressField.ParentsFields__c.split(',')) {
                    AddressField__mdt parentField ;
                    for (AddressField__mdt a : addressFields) {
                        if (a.FieldName__c == parentFieldName) {
                            parentField = a;
                            break;
                        }
                    }
                    parentFieldName = parentField.SourceField__c.trim() ;
                    if (parentFieldName != 'Name') {
                        parentFieldName += '__c';
                    }
                    if (String.isNotBlank(parentField.SourceTable__c) && parentField.SourceTable__c.trim() != addressField.SourceTable__c.trim()) {
                        parentFieldName = parentField.SourceTable__c.trim() + '__r' + '.' + parentFieldName;
                    }
                    String parentValue = address.get(parentField.QualifiedApiName.trim()).trim();

                    queryString += ' AND ' + parentFieldName + ' = \'' + parentValue + '\'';
                }
            }
//            queryString += ' GROUP BY ' + qualifiedApiName;
//            for (AggregateResult result : Database.query(queryString)) {
//                results.add(result.get(qualifiedApiName).toString());
//            }
            queryString += ' LIMIT 50 ' ;
            System.debug(queryString);
            for (SObject result : Database.query(queryString)) {
                results.add(result.get(sourceField).toString());
            }
        }
        return new List<String>(results);
    }

    public with sharing class normalize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('params inputs' + jsonInput);

            Map<String, String> params = asMap(jsonInput);
            Map<String, String> address = (Map<String, String>) JSON.deserialize(params.get('address'), Map<String, String>.class);
            System.debug('params address' + address);

	        MRO_SRV_Address.AddressDTO address_dto = new MRO_SRV_Address.AddressDTO(address);
            NormalizeResponse normaliseResponse = normalize(address_dto);
//          List<Map<String, String>> addresses = new List<Map<String, String>>();
            System.debug('params result address' + normaliseResponse);

            if (normaliseResponse.addresses.size() == 0) {
                throw new MRO_SRV_Address.AddressException('Invalid inputs, Address not found');
            }

            return normaliseResponse;
        }
    }

    public with sharing class retrieveStreet extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String streetId = params.get('streetId');
            return MRO_QR_Address.findStreet(streetId);
        }
    }


    public with sharing class getCompletionStrings extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('params inputs' + jsonInput);
            Map<String, String> params = asMap(jsonInput);
            List<String> strings = getCompletionStrings(params.get('field'), params.get('startWith'));
//            List<Map<String, String>> addresses = new List<Map<String, String>>();
            System.debug('params result' + strings);
// addresses.add(address);
            return strings;
        }
    }

    public with sharing class getFieldByCity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('params inputs' + jsonInput);
            List<String> strings ;
            Map<String, String> params = asMap(jsonInput);
            String city = params.get('city');
            switch on params.get('field') {
                when 'province' {
                    strings = getProvincesByCity(city);
                }
                when 'country' {
                    strings = getCountriesByCity(city);
                }
            }
            System.debug('params result' + strings);
            return strings;
        }
    }


    public with sharing class createAddress extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('params inputs' + jsonInput);
            Map<String, String> params = asMap(jsonInput);
            Map<String, String> address = (Map<String, String>) JSON.deserialize(params.get('address'), Map<String, String>.class);
            Boolean isForceAddress = (Boolean) JSON.deserialize(params.get('isForceAddress'), Boolean.class);

            MRO_SRV_Address.AddressDTO address_dto = new MRO_SRV_Address.AddressDTO(address);
            Street__c exactStreet = MRO_QR_Address.findExactStreet1(address_dto);
            if (exactStreet == null) {
                exactStreet = addressSrv.insertStreet(addressSrv.insertCity(address), address, isForceAddress);
                //exactStreet = MRO_QR_Address.findExactStreet1(address_dto);
            }
            address_dto.streetId = exactStreet.Id;
            address_dto.addressKey = MRO_UTL_Guid.newGuid();
            return address_dto;
        }
    }


    /**
    * Normalise an address or propose a list of address close to the input
    *
    * @param address input address
    *
    * @return matching address or list of address close to the input
    */
    @AuraEnabled
    public static MRO_LC_Address.NormalizeResponse normalize(MRO_SRV_Address.AddressDTO address_dto) {
        Boolean autoSuggest;
        System.debug('address_dto ' + address_dto);
// List<Street__c> addresses = new List<Street__c>();
        Street__c[] streets;


        streets = MRO_QR_Address.findExactStreetQuery1(address_dto);

        if (streets.size() == 0) {
            autoSuggest = true;
            streets = MRO_QR_Address.findExactStreetQuery2(address_dto);
        }
        System.debug('size=' + streets.size());
        List<Map<String, Object>> addresses = new List<Map<String, Object>>();

        for (Street__c street : streets) {
	        MRO_SRV_Address.AddressDTO foundAddress = streetToAddressDto(street, address_dto.streetNumber);
	        foundAddress.addressKey = MRO_UTL_Guid.newGuid();
            addresses.add((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(foundAddress)));
        }
        if (addresses.size() == 0) {
	        Map<String, String> address = (Map<String, String>) JSON.deserialize(JSON.serialize(address_dto), Map<String, String>.class);
            for (String field : address.keySet()) {
                if (String.isNotEmpty(((String) address.get(field))) && !isAddressFieldExists(field, address.get(field))) {
                    throw new MRO_SRV_Address.AddressException(getNotExistsFieldErrorString(field));
                }
            }
        }
        NormalizeResponse normalizeResponse = new NormalizeResponse();
        normalizeResponse.addresses = addresses;
        normalizeResponse.autoSuggest = autoSuggest;
        return normalizeResponse;
    }

//    public static MRO_LC_Address.NormalizeResponse normalize(Map<String, Object> address) {
//        Boolean autoSuggest;
//        System.debug('address ' + address);
//// List<Street__c> addresses = new List<Street__c>();
//        Street__c[] streets;
//        AddressDTO address_dto = (AddressDTO) JSON.deserialize(JSON.serialize(address), AddressDTO.class);
//        Long streetNumber = Long.valueOf(address_dto.streetNumber);
//        System.debug('address_dto ' + address_dto);
//
////check exact
//
//        streets = MRO_QR_Address.findExactStreetQuery1(address_dto);
//
//        if (streets.size() == 0) {
//            autoSuggest = true;
////build query args
//            Map<String, String[]> addressStrings = new Map<String, String[]>();
//            for (String prop : address.keySet()) {
//                addressStrings.put(prop, buildLikeCriteria(String.valueOf(address.get(prop))));
//            }
//            System.debug('# like criteria ' + addressStrings);
//
////find with all fields
//            streets = MRO_QR_Address.findStreets(
//                    addressStrings.get('country'), addressStrings.get('province'), addressStrings.get('city'),
//                    addressStrings.get('streetType'), addressStrings.get('streetName'), addressStrings.get('postalCode'),
//                    streetNumber);
//
//            System.debug('streets#1 ' + streets);
//            if (streets.size() == 0) {
////reduce criteria omit postalcode
//                streets = MRO_QR_Address.findStreets(
//                        addressStrings.get('country'), addressStrings.get('province'), addressStrings.get('city'),
//                        addressStrings.get('streetType'), addressStrings.get('streetName'),
//                        null,
//                        streetNumber);
//                System.debug('streets#2 ' + streets);
//                if (streets.size() == 0) {
////reduce criteria omit street name
//                    streets = MRO_QR_Address.findStreets(
//                            addressStrings.get('country'), addressStrings.get('province'), addressStrings.get('city'),
//                            addressStrings.get('streetType'), addressStrings.get('streetName'),
//                            null,
//                            null);
//                    if (streets.size() != 0) {
//                        throw new AddressException('Please check the street number');
//                    }
//                    System.debug('streets#3 ' + streets);
//                }
//            }
//        }
//
//        List<Map<String, Object>> addresses = new List<Map<String, Object>>();
//
//        for (Street__c street : streets) {
//            addresses.add((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(streetToAddressDto(street, streetNumber))));
//        }
//        if (addresses.size() == 0) {
//            for (String field : address.keySet()) {
//                if (String.isNotEmpty(((String) address.get(field))) && !isAddressFieldExists(field, address.get(field))) {
//                    throw new AddressException(getNotExistsFieldErrorString(field));
//                }
//            }
//        }
//        NormalizeResponse normalizeResponse = new NormalizeResponse();
//        normalizeResponse.addresses = addresses;
//        normalizeResponse.autoSuggest = autoSuggest;
//        return normalizeResponse;
//    }


//    static String ForceAddressPermissionName = 'ForceAddress';

    static Boolean isAddressFieldExists(String field, Object value) {
        switch on field {
            when 'country', 'province', 'city' {
                return MRO_QR_Address.fieldExist(field, String.valueOf(value), 'city');
            }
            when 'streetName', 'postalCode', 'streetType' {
                return MRO_QR_Address.fieldExist(field, String.valueOf(value), 'street');
            }

        }
        return true;

    }

    static String getNotExistsFieldErrorString(String field) {
        String error;
        switch on field {
            when 'country' {
                error = ' Country does not exists';
            }
            when 'province' {
                error = ' Province does not exists';
            }
            when 'city' {
                error = ' City does not exists';
            }
            when 'streetName' {
                error = ' Street Name does not exists';
            }
            when 'postalCode' {
                error = ' Postal Code does not exists';
            }
            when 'streetType' {
                error = ' Street type does not exists';
            }
        }
        return error;
    }

/**
 * Split text to word for a like query
 *
 * @param potentialName text to split
 *
 * @return list of word surrounded with '%'
 */
    static List<String> buildLikeCriteria(String potentialName) {
        Integer criteriaMinLength = 3;
        if (String.isBlank(potentialName)) return null;

        List<String> partialMatches = new List<String>();
        for (String fragment : potentialName.split(' ')) {
            if (fragment.length() >= criteriaMinLength) {
                partialMatches.add('%' + fragment + '%');
            }
        }
        return partialMatches.isEmpty() ? new List<String>{
                potentialName
        } : partialMatches;
    }

//    @AuraEnabled(cacheable=true)
//    public static Boolean hasForceAddressPermission() {
//        return [
//                SELECT Id, PermissionSet.Name,AssigneeId
//                FROM PermissionSetAssignment
//                WHERE PermissionSet.Name = :ForceAddressPermissionName AND AssigneeId = :UserInfo.getUserId()
//        ].size() != 0;
//    }

/**
 *
 *
 */
    @AuraEnabled
    public static String getExtraFields(String objectApiName, String addressFieldSet) {
        return JSON.serialize(LightningUtils.getFieldSet(objectApiName, addressFieldSet));
    }

/**
 * retrieves provinces according to the given city
 *
 * @param city:name of the city
 *
 * @return the list provinces found
 */
    @AuraEnabled(cacheable=true)
    public static List< String> getProvincesByCity(String city) {
        Set< String> results = new Set<String>();
        for (City__c c : MRO_QR_Address.getProvinceByCity(city)) {
            results.add(c.Province__c);
        }
        return new List<String>(results);
    }

/**
 * retrieves countries according to the given city
 *
 * @param city:name of the city
 *
 * @return the list countries found
 */
    @AuraEnabled(cacheable=true)
    public static List< String> getCountriesByCity(String city) {
        Set< String> results = new Set<String>();
        for (City__c c : MRO_QR_Address.getCountryNameByCityName(city)) {
            results.add(c.Country__c);
        }
        return new List<String>(results);
    }

/**
 * retrieves the precised 'field' of records that start by 'statWith'
 *
 * @param field:the field to retrieve in records
 *
 * @param startWith: text with which fields have to start to be taken
 *
 * @return the list of taken records fields
 */
    @AuraEnabled(cacheable=true)
    public static List< String>getCompletionStrings(String field, String startWith) {
        if (String.isEmpty(field) || String.isEmpty(startWith)) {
            System.debug('Empty input');
            return null;
        }
        startWith = startWith.trim();
        System.debug('field  "' + field + '" startWith  "' + startWith + '"');
        Set<String> results = new Set<String>();
        startWith += '%';
        switch on field {
            when 'streetName' {
                for (Street__c street : MRO_QR_Address.getStreetsStartingWith(startWith)) {
                    results.add(street.Name);
                }
            }
            when 'province' {
                for (City__c city : MRO_QR_Address.getProvincesStartingWith(startWith)) {
                    results.add(city.Province__c);
                }
            }
            when 'city' {
                for (City__c city : MRO_QR_Address.getCitiesStartingWith(startWith)) {
                    results.add(city.Name);
                }
            }
            when 'country' {
                for (City__c city : MRO_QR_Address.getCountriesStartingWith(startWith)) {
                    results.add(city.Country__c);
                }
            }
            when 'postalCode' {
                for (Street__c street : MRO_QR_Address.getPostalCodesStartingWith(startWith)) {
                    results.add(street.PostalCode__c);
                }
            }
        }
        System.debug('Result ' + results);
        return new List<String>(results);
    }

    public static Map<String, String> asMap(String jsonInput) {
        if (jsonInput == 'null' || String.isBlank(jsonInput)) {
            return new Map<String, String>();
        }
        return (Map<String, String>) JSON.deserialize(jsonInput, Map<String, String>.class);
    }

    static MRO_SRV_Address.AddressDTO streetToAddressDto(Street__c street, String streetNumber) {
        MRO_SRV_Address.AddressDTO dto = new MRO_SRV_Address.AddressDTO();
        dto.streetId = street.Id;
        dto.streetNumber = streetNumber;
        dto.streetName = street.Name;
        dto.streetType = street.StreetType__c;
        dto.country = street.City__r.Country__c;
        dto.city = street.City__r.Name;
        dto.province = street.City__r.Province__c;
        dto.postalCode = street.PostalCode__c;
        return dto;
    }

    public class NormalizeResponse {
        @AuraEnabled
        public List<Map<String, Object>> addresses;
        @AuraEnabled
        public Boolean autoSuggest;
    }

// BG commented - unused code
/*@AuraEnabled
public static Map<String, Object> validate(List<Map<String,Object>> fields) {
    Map<String, Object> response = new Map<String, Object>();
    try{
        response.put('error', false);
        response.put('isValid', true);
    } catch(Exception ex) {
        response.put('error', true);
        response.put('errorMsg', ex.getMessage());
        response.put('errorTrace', ex.getStackTraceString());
    }
    return response;
}

@AuraEnabled
public static Map<String, Object> normalizedAddress(List<Map<String,Object>> addressFieldSetValue) {
    Map<String, Object> response = new Map<String, Object>();
    try{

        response.put('normalizedAddress',addressFieldSetValue);
        response.put('error', false);
        response.put('isValid', true);
    } catch(Exception ex) {
        response.put('error', true);
        response.put('errorMsg', ex.getMessage());
        response.put('errorTrace', ex.getStackTraceString());
    }
    return response;
}*/

}