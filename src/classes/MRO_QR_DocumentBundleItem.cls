/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   nov 18, 2019
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_DocumentBundleItem {
    private static MRO_QR_DocumentBundleItem singleton;

    public static MRO_QR_DocumentBundleItem getInstance() {
        if (singleton == null) {
            singleton = new MRO_QR_DocumentBundleItem();
        }
        return singleton;
    }

    public List<DocumentBundleItem__c> listDocumentBundleItemsByBundlesIds(Set<Id> documentBundlesIds) {
        return [SELECT Id, DocumentBundle__c, DocumentBundle__r.ForceBillingDeliveryChannel__c, DocumentBundle__r.ForceArchivingChannel__c,
                       TemplateClass__c, TemplateCode__c, TemplateVersion__c, Archive__c, Master__c, AttachToEmail__c,
                       ProductTemplateCodeField__c, EnableLanguages__c, DocumentType__c, DocumentType__r.DocumentTypeCode__c, ArchiveOnDisCo__c,
	                   SendingChannels__c, Origins__c, Channels__c, SubProcess__c, CustomerTypes__c, Commodities__c, ContractAddition__c,
                       EnelArea__c, Market__c, PrintBundleSelection__c, SkipIfEmptyOnProduct__c,
                        DocumentBundle__r.Recipient__c, DocumentBundle__r.RecordType.DeveloperName
                FROM DocumentBundleItem__c
                WHERE DocumentBundle__c IN :documentBundlesIds];
    }

    public List<DocumentBundleItem__c> listDocumentBundleItemsByDossierId(Id dossierId) {
        List<ValidatableDocument__c> validatableDocuments = [
            SELECT DocumentBundleItem__c, DocumentBundleItem__r.DocumentBundle__c,
                   DocumentBundleItem__r.DocumentType__c, DocumentBundleItem__r.DocumentType__r.Name,
                   DocumentBundleItem__r.Mandatory__c, DocumentBundleItem__r.ArchiveOnDisCo__c
            FROM ValidatableDocument__c
            WHERE Dossier__c = :dossierId AND DocumentBundleItem__c != NULL AND
                (FileMetadata__c = NULL OR ValidationActivity__r.wrts_prcgvr__IsClosed__c = FALSE)
        ];
        Map<Id, DocumentBundleItem__c> documentBundleItemMap = new Map<Id, DocumentBundleItem__c>();
        for (ValidatableDocument__c vd : validatableDocuments) {
            documentBundleItemMap.put(vd.DocumentBundleItem__c, vd.DocumentBundleItem__r);
        }
        return documentBundleItemMap.values();
    }

    public List<DocumentBundleItem__c> listDocumentBundleItemsByCaseId(Id caseId) {
        List<ValidatableDocument__c> validatableDocuments = [
            SELECT DocumentBundleItem__c, DocumentBundleItem__r.DocumentBundle__c,
                   DocumentBundleItem__r.DocumentType__c, DocumentBundleItem__r.DocumentType__r.Name,
                   DocumentBundleItem__r.Mandatory__c, DocumentBundleItem__r.ArchiveOnDisCo__c
            FROM ValidatableDocument__c
            WHERE Case__c = :caseId AND DocumentBundleItem__c != NULL AND
                (FileMetadata__c = NULL OR ValidationActivity__r.wrts_prcgvr__IsClosed__c = FALSE)
        ];
        Map<Id, DocumentBundleItem__c> documentBundleItemMap = new Map<Id, DocumentBundleItem__c>();
        for (ValidatableDocument__c vd : validatableDocuments) {
            documentBundleItemMap.put(vd.DocumentBundleItem__c, vd.DocumentBundleItem__r);
        }
        return documentBundleItemMap.values();
    }
}