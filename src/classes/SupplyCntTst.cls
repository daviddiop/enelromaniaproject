@isTest
private with sharing class SupplyCntTst {
    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account account = TestDataFactory.account().personAccount().build();
        insert account;

        List<CompanyDivision__c> listCompaniesDivision = new List<CompanyDivision__c>();
        for (Integer i = 0; i < 1; i++) {
            CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(i).build();
            listCompaniesDivision.add(companyDivision);
        }
        insert listCompaniesDivision;

        User user = TestDataFactory.user().standardUser().build();
        user.CompanyDivisionId__c = listCompaniesDivision[0].Id;
        insert user;

        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '43252435624654';
        servicePoint.Account__c = account.Id;
        servicePoint.Trader__c = account.Id;
        servicePoint.Distributor__c = account.Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        Contract contract = TestDataFactory.Contract().createContract().build();
        contract.Name = 'Name11062019';
        contract.AccountId = account.Id;
        insert contract;

        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(listCompaniesDivision[0].Id).build();
        supply.Contract__c = contract.Id;
        supply.Account__c = account.Id;
        //supply.ServicePoint__c = servicePoint.Id;
        insert supply;

        Supply__c supply2 = TestDataFactory.supply().createSupplyBuilder().setCompany(listCompaniesDivision[0].Id).build();
        supply2.Contract__c = contract.Id;
        supply2.Account__c = account.Id;
        //supply2.ServicePoint__c = servicePoint.Id;
        supply2.Status__c = 'Terminating';
        insert supply2;

        Case caseRecord = TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(listCompaniesDivision[0].Id).build();
        caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_ELE').getRecordTypeId();
        insert caseRecord;
        SupplyCnt.getInstance();
    }


    @isTest
    static void getDeveloperNameTest() {
        User user = [SELECT Id,CompanyDivisionId__c FROM User WHERE Email = 'userdivision@test.com' LIMIT 1];
        System.runAs(user){
            Case caseRecord = [SELECT Id FROM Case];

            Map<String, String> jsonInput = new Map<String, String>{
                'recordId' => caseRecord.Id
            };
            Test.startTest();
            Object response = TestUtils.exec('SupplyCnt', 'getDeveloperName', jsonInput, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true , result.get('listRecordType') != null);
            Test.stopTest();
        }
    }

    @isTest
    static void getDeveloperNameExceptionTest() {
        User user = [SELECT Id,CompanyDivisionId__c FROM User WHERE Email = 'userdivision@test.com' LIMIT 1];
        System.runAs(user){
            Map<String, String> jsonInput = new Map<String, String>{
                    'recordId' => ''
            };
            Test.startTest();
            try {
                Object response = TestUtils.exec('SupplyCnt', 'getDeveloperName', jsonInput, false);
            }catch (Exception e){
                System.assert(true, e.getMessage());
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getSuppliesByAccountIdTest() {
        User user = [SELECT Id,CompanyDivisionId__c FROM User WHERE Email = 'userdivision@test.com' LIMIT 1];
        System.runAs(user){
            Account myAccount = [SELECT Id FROM Account LIMIT 1];

            Map<String, String> jsonInput = new Map<String, String>{
                    'accountId' => myAccount.Id
            };
            Test.startTest();
            Object response = TestUtils.exec('SupplyCnt', 'getSuppliesByAccountId', jsonInput, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true , result.get('supplies') != null);
            Test.stopTest();
        }
    }

    @isTest
    static void getSuppliesByAccountIdExceptionTest() {
        Account myAccount = [SELECT Id FROM Account LIMIT 1];
        Map<String, String> jsonInput = new Map<String, String>{
                'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('SupplyCnt', 'getSuppliesByAccountId', jsonInput, true);                
        } catch (Exception e) {
            System.assert(true , e.getMessage());                
        }
        Test.stopTest();
    }


    @isTest
    static void suppliesByServicePointTest() {
        //User user = [SELECT Id,CompanyDivisionId__c FROM User WHERE Email = 'userdivision@test.com' LIMIT 1];
        //System.runAs(user){
        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
                LIMIT 1
        ];
        Account myAccount = [SELECT Id FROM Account LIMIT 1];
            ServicePoint__c myServicePoint = [SELECT Id,Code__c FROM ServicePoint__c LIMIT 1];
            String status = 'Active';
            Contract myContract = [SELECT Id FROM Contract LIMIT 1];
            Supply__c supply = [SELECT Id,CompanyDivision__c FROM Supply__c Limit 1];
            supply.CompanyDivision__c = companyDivision.Id;
            update supply;

            String notOwnAcc = 'false';
            List<String> fields = new List<String>() ;
            fields.add('Name');
            fields.add('RecordTypeId');
            fields.add('ServicePoint__c');
            fields.add('Contract__c');
            String selFields = JSON.serialize(fields);
            Map<String, String> jsonInput = new Map<String, String>{
                'accountId' => myAccount.Id,
                'servicePointCode' => myServicePoint.Code__c,
                'status' => status,
                'contractId' => myContract.Id,
                'notOwnAccount' => notOwnAcc,
                'selectFields' => selFields,
                'companyDivisionId' => companyDivision.Id
            };
            Test.startTest();
            Object response = TestUtils.exec('SupplyCnt', 'getSuppliesByServicePoint', jsonInput, true);
            Map<String, Object> result = (Map<String, Object>) response;
            Test.stopTest();
       // }
    }

    @isTest
    static void getRecordTypesTest() {
        Test.startTest();
        Object response = TestUtils.exec('SupplyService', 'getRecordTypes', null, true);
        //Map<String, Object> result = (Map<String, Object>) response;
        //System.assertNotEquals(result.get('supplyData'), null);
        Test.stopTest();
    }
/*
    @isTest
    static void getSupplyTileTest() {
            Supply__c supply = [SELECT Id  FROM Supply__c];
            Map<String, String> jsonInput = new Map<String, String>{
                    'recordId' => supply.Id
            };
            Test.startTest();
            Object response = TestUtils.exec('SupplyCnt', 'getSupplyTile', jsonInput, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertNotEquals(result.get('supplyData'), null);
            Test.stopTest();
    }

    @isTest
    static void getSupplyTileExceptionTest() {
            Map<String, String> jsonInput = new Map<String, String>{
                    'recordId' => ''
            };
            Test.startTest();
                Object response = TestUtils.exec('SupplyCnt', 'getSupplyTile', jsonInput, false);
            Test.stopTest();
    }*/

    @isTest
    static void suppliesByServicePointExceptionTest() {
        String status = 'Active';
        String notOwnAcc = 'notOwnAcc';
        String selFields = 'null';
        Map<String, String> jsonInput = new Map<String, String>{
            'accountId' => null,
            'servicePointCode' => null,
            'status' => status,
            'contractId' => null,
            'notOwnAccount' => notOwnAcc,
            'selectFields' => selFields
        };

        Test.startTest();
        Object response = TestUtils.exec('SupplyCnt', 'getSuppliesByServicePoint', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(result.get('error'), true);
        Test.stopTest();
    }

    @isTest
    static void actualizeSupplyOnServicePointTest() {
        List<ServicePoint__c> myServicePointList = [SELECT Id FROM ServicePoint__c];
        Set<Id> myServicePointSet = new Set<Id>();
        for (ServicePoint__c x : myServicePointList){
            myServicePointSet.add(x.Id);
        }

        Test.startTest();
        SupplyService supplyServ = new SupplyService();
        supplyServ.actualizeSupplyOnServicePoint(myServicePointSet);
    }

    @isTest
    static void suppliesByContractTest() {
        /*User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];*/
        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
                LIMIT 1
        ];

        //System.runAs(user){
            Account myAccount = [
                SELECT Id
                FROM Account
                LIMIT 1
            ];
            Contract myContract = [
                SELECT Id, CompanyDivision__c, ContractNumber
                FROM Contract
                LIMIT 1
            ];
            myContract.CompanyDivision__c = companyDivision.Id;
            update myContract;

            String status = 'Active';
            String notOwnAcc = 'false';
            String contractCode = 'Name11062019';
            List<String> fields = new List<String>() ;
            fields.add('Name');
            fields.add('RecordTypeId');
            fields.add('Account__c');
            fields.add('Contract__c');
            Map<String, String> jsonInput = new Map<String, String>{
                'accountId' => myAccount.Id,
                'contractCode' => contractCode,
                'status' => status,
                'contractId' => myContract.Id,
                'notOwnAccount' => notOwnAcc,
                'selectFields' => JSON.serialize(fields),
                'companyDivisionId' => companyDivision.Id
            };

            Test.startTest();
            Map<String, Object> result = (Map<String, Object>)TestUtils.exec(
                'SupplyCnt', 'getSuppliesByContract', jsonInput, true);
            Test.stopTest();
        //}
    }

    @isTest
    static void suppliesByContractExceptionTest() {
        String status = 'Active';
        String notOwnAcc = 'false';
        String contractCode = '';
        String selFields = '';
        Map<String, String> jsonInput = new Map<String, String>{
            'accountId' => null,
            'contractCode' => contractCode,
            'status' => status,
            'contractId' => null,
            'notOwnAccount' => notOwnAcc,
            'selectFields' => selFields
        };

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('SupplyCnt', 'getSuppliesByContract', jsonInput, true);
        System.assertEquals(result.get('error'), true);
        Test.stopTest();
    }

    @isTest
    static void getLookupFieldsTest() {
        Map<String, String> jsonInput = new Map<String, String>{
            'objName' => 'Supply__c'
        };
        Test.startTest();
        Object response = TestUtils.exec('SupplyCnt', 'getLookupFields', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('lookupFields') != null);
        Test.stopTest();
    }

    @isTest
    static void getLookupFieldsExceptionTest() {
        Map<String, String> jsonInput = new Map<String, String>{
            'objName' => null
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('SupplyCnt', 'getLookupFields', jsonInput, true);            
        } catch (Exception e) {
            System.assert(true, e.getMessage());
            
        }
        Test.stopTest();
    }

    @isTest
    static void deleteCaseTest() {
        Case caseRecord = [SELECT Id FROM Case];
        Map<String, String> jsonInput = new Map<String, String>{
                'recordId' => caseRecord.Id
        };

        Test.startTest();
        Object response = TestUtils.exec('SupplyCnt', 'deleteCase', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertNotEquals(result, null);

    }

    @isTest
    static void deleteCaseExceptionTest() {
        String caseRecordId = 'recordIdrecordIdrecordIdrecordIdrecordId';
        Map<String, String> jsonInput = new Map<String, String>{
                'recordId' => caseRecordId
        };
        Test.startTest();
        Object response = TestUtils.exec('SupplyCnt', 'deleteCase', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(result.get('error'), true);

    }


    @isTest
    static void getRecordTypeTest() {
        Test.startTest();
        //SupplyService supplyserv = new SupplyService();
        Object response = TestUtils.exec('SupplyService', 'getRecordTypes',null, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(null,result.get('error'));

    }


}