/**
 * Created by vincenzo.scolavino on 10/03/2020.
 */

public with sharing class MRO_LC_NLCCLCDataChange extends ApexServiceLibraryCnt{

    private static MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();
    private static MRO_QR_Account accQuery                   = MRO_QR_Account.getInstance();
    private static UserQueries userQuery                     = UserQueries.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_Case      caseQuery                = MRO_QR_Case.getInstance();
    private static MRO_QR_Dossier dossierQuery               = MRO_QR_Dossier.getInstance();
    private static MRO_UTL_Constants constantsSrv            = new MRO_UTL_Constants();
    private static MRO_SRV_DatabaseService databaseSrv       = MRO_SRV_DatabaseService.getInstance();
    private static MRO_SRV_Dossier dossierServ               = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseServ                     = MRO_SRV_Case.getInstance();
    private static MRO_SRV_PrivacyChange privacyChangeServ   = MRO_SRV_PrivacyChange.getInstance();
    private static String dossierNewStatus                   = constantsSrv.DOSSIER_STATUS_NEW;
    private static String dossierDraftStatus                 = constantsSrv.DOSSIER_STATUS_DRAFT;
    private static final String dossierRequestType           = 'NLC-CLC Update';

    @AuraEnabled
    public static Map<String, Object> checkSelectedSupply(String supplyId, String dossierId) {
        Map<String, Object> response = new Map<String,Object>();
        System.debug('Class : MRO_LC_NLCCLCDataChange -- method : checkSelectedSupply');
        System.debug('input : ' + supplyId + ' ' + dossierId);
        try{
            response.put('error', false);
            response.put('isValid', true);
            if(dossierId==null || String.isBlank(dossierId))
                throw new WrtsException('Dossier id is null');

            Supply__c supply = [SELECT Id, Account__c,CompanyDivision__c, ServiceSite__c, ServicePoint__c,ServicePoint__r.Code__c, ServicePoint__r.Name
            FROM Supply__c WHERE Id=: supplyId];
            response.put('supply', supply);

            List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
            for(Case c : cases){
                if(c.Supply__c.equals(supplyId)){
                    response.put('isValid', false);
                    response.put('message', System.Label.TheSelectedSupplyIsAlreadyRegisteredToTheCurrentCustomer);
                    return response;
                }
            }

        }catch(Exception ex){
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());

        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> updateCasesStatus(List<Case> inputList) {
        List<Case> cases = inputList;
        Map<String, Object> response = new Map<String, Object>();
        System.debug('Class : MRO_LC_NLCCLCDataChange -- method : updateCasesStatus');
        System.debug('input : ' + inputList);
        try {
            if(inputList.isEmpty())
                throw new WrtsException('Case List is empty');

            for(Case c : cases){
                c.Status = constantsSrv.CASE_STATUS_NEW;
            }
            caseServ.updateCases(cases);
            response.put('cases', cases);
            response.put('error', false);
            System.debug('cases status updated : ' + cases);
        } catch (Exception ex) {
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> saveNlcClcDataChange(String dossierId, List<Case> inputList, Boolean isDraft) {
        List<Case> cases = inputList;
        Map<String, Object> response = new Map<String, Object>();
        System.debug('Class : MRO_LC_NLCCLCDataChange -- method : saveNlcClcDataChange');
        System.debug('input : ' + inputList);
        PrivacyChange__c pc = new PrivacyChange__c();
        Savepoint sp = Database.setSavepoint();
        try {
            Dossier__c doss = new Dossier__c();
            doss.Id = dossierId;
            doss.Status__c = isDraft ? dossierDraftStatus : dossierNewStatus;
            databaseSrv.upsertSObject(doss);
            doss = dossierQuery.getById(dossierId);
            for(Case c : cases){
                c.Status = isDraft ? constantsSrv.CASE_STATUS_DRAFT : constantsSrv.CASE_STATUS_NEW;
                c.Origin = doss.Origin__c;
                c.StartDate__c = Date.today();
                c.EffectiveDate__c = Date.today();
            }
            caseServ.updateCases(cases);

            dossierServ.checkAndApplyAutomaticTransitionToDossierAndCases(doss, cases, constantsSrv.CONFIRM_TAG, true, true, false);

            response.put('cases', cases);
            response.put('error', false);
            System.debug('nlc clc data change completed for dossier : '+dossierId + ' and cases : ' + cases);
        } catch (Exception ex) {
            Database.rollback(sp);
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }


    /*public with sharing class getScriptTemplate extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String scriptTemplateCode = params.get('scriptTemplateCode');
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : getScriptTemplate --- method : perform');
            System.debug('input : ' + jsonInput);
            try{
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(scriptTemplateCode);
                response.put('data',scriptTemplate);
                response.put('error', false);
            }catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }*/


    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : initialize --- method : perform');
            System.debug('input : ' + jsonInput);
            String accountId = params.get('accountId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String dossierId = params.get('dossierId');
            String genericRequestDossierId = params.get('genericRequestId');

            User currentUserInfos = userQuery.getCompanyDivisionId(UserInfo.getUserId());
            List<Case> cases = new List<Case>();

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Account acc = accQuery.findAccount(accountId);

            if (String.isBlank(companyDivisionId)) {
                companyDivisionId = null;
            }

            Dossier__c dossier;
            if (String.isBlank(dossierId)) {
                Id cusId = null;
                List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);
                /* if(!cus.isEmpty()){
                    cusId = cus[0].Id;
                }*/
                String interactionOrigin;
                String interactionChannel;
                if (!cus.isEmpty()) {
                    cusId = cus[0].Id;
                    interactionOrigin = cus[0].Interaction__r.Origin__c;
                    interactionChannel = cus[0].Interaction__r.Channel__c;
                }
                if (genericRequestDossierId != null) {
                    Dossier__c genericRequestDossier = dossierQuery.getById(genericRequestDossierId);
                    dossier = genericRequestDossier.clone();
                    dossier.Parent__c = genericRequestDossierId;
                } else{
                    dossier = new Dossier__c();
                    dossier.Origin__c = interactionOrigin;
                    dossier.Channel__c = interactionChannel;
                }

                //dossier = new Dossier__c ();
                Map<String, String> caseRecordTypes = MRO_UTL_Constants.getDossierRecordTypes('Change');
                dossier.Account__c = accountId;
                dossier.CompanyDivision__c = String.isNotBlank(companyDivisionId) ? companyDivisionId : null;
                dossier.RecordTypeId = caseRecordTypes.get('Change');
                dossier.Status__c = dossierDraftStatus;
                dossier.RequestType__c= dossierRequestType;

                if (!cus.isEmpty()) {
                    dossier.CustomerInteraction__c = cusId;
                }
                databaseSrv.insertSObject(dossier);
                dossierId = dossier.Id;
            }
            else{
                dossier = dossierQuery.getById(dossierId);
                cases = caseQuery.getCasesByDossierId(dossierId);
            }

            String code = 'NLCCLCChangeWizardCodeTemplate_1';
            ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
            if (scriptTemplate != null) {
                response.put('templateId', scriptTemplate.Id);
            }


            System.debug('User' + currentUserInfos);
            System.debug('Company division id ' + currentUserInfos.CompanyDivisionId__c);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('dossierId', dossierId);
            response.put('dossier', dossier);
            response.put('cases', cases);
            response.put('dossierCommodityEditStatus',dossierDraftStatus);
            response.put('user', currentUserInfos);
            response.put('companyDivisionId', currentUserInfos.CompanyDivisionId__c);
            response.put('useBit2WinCart', constantsSrv.IS_BIT2WIN_CPQ_ENABLED);
            response.put('error', false);

            return response;
        }
    }
    public with sharing class  commodityChangeActions extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String commodity = params.get('commodity');
            Map<String, Object> response = new Map<String, Object>();
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : commodityChangeActions --- method : perform');
            System.debug('input : ' + jsonInput);
            try {
                Dossier__c doss = dossierServ.updateDossierCommodity(dossierId,commodity);
                response.put('dossierId', doss.Id);
                response.put('commodity', doss.Commodity__c);
                response.put('error', false);

            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }


    public with sharing class updateDossierStatus extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : updateDossierStatus --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            try {
                Dossier__c doss = new Dossier__c();
                doss.Id = dossierId;
                doss.Status__c = dossierNewStatus;
                databaseSrv.upsertSObject(doss);
                response.put('dossierId', doss.Id);
                response.put('status', doss.Status__c);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateDossierCompanyDivision extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String companyDivisionId = params.get('companyDivisionId');
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : updateDossierCompanyDivision --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            try {
                Dossier__c doss = new Dossier__c();
                doss.Id = dossierId;
                doss.CompanyDivision__c = companyDivisionId;
                databaseSrv.upsertSObject(doss);
                response.put('dossierId', doss.Id);
                response.put('companyDivisionId', doss.CompanyDivision__c);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }


    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');
            dossierServ.setChannelAndOrigin(dossierId,origin,channel);
            return true;
        }
    }

    public with sharing class getUpdatedCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : getUpdatedCaseList --- method : perform');
            System.debug('input : ' + jsonInput);
            List<Case> cases;
            Map<String, Object> response = new Map<String, Object>();
            try {
                cases = caseQuery.getCasesByDossierId(dossierId);
                response.put('cases', cases);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            System.debug('output : ' + response.get('cases'));
            return response;
        }
    }

    public with sharing class deleteSelectedCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('recordId');
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : deleteCase --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            try {
                caseServ.deleteCase(recordId);
                response.put('recordId', recordId);
                response.put('error', false);
                System.debug('case deleted with id : ' + recordId);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }

    }

    public with sharing class updatePrivacyChangeDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String privacyChangeId = params.get('privacyChangeId');
            System.debug('Class : MRO_LC_NLCCLCDataChange -- inner : deleteCase --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            PrivacyChange__c pc = new PrivacyChange__c();
            try {
                pc = MRO_SRV_PrivacyChange.updatePrivacyChangeDossier(privacyChangeId,dossierId);
                response.put('privacyChange', pc);
                response.put('error', false);
                System.debug('privacy change updated with id : ' + privacyChangeId);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    @AuraEnabled
    public static Map<String,Object> deleteCases( String dossierId) {
        Boolean isDeleted = false;
        Map<String, Object> response = new Map<String, Object>();
        try {
            List<Case> caseListToDelete = caseQuery.getCasesByDossierId(dossierId);
            System.debug('caseListToDelete LIST **************************> ' + caseListToDelete);
            if ( caseListToDelete != null && !caseListToDelete.isEmpty() ) {
                databaseSrv.deleteSObject(caseListToDelete);
                isDeleted = true;
            }
            response.put('isDeleted', isDeleted);
            response.put('error', false);
        } catch (Exception ex) {
            //Database.rollback(sp);
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }


    public with sharing class InputData {
        @AuraEnabled
        public String billingProfileId { get; set; }
        @AuraEnabled
        public String contractAccountId { get; set; }
        @AuraEnabled
        public String product2Id { get; set; }
        @AuraEnabled
        public List<OpportunityServiceItem__c> opportunityServiceItems { get; set; }
        @AuraEnabled
        public List<String> osiIds { get; set; }
        @AuraEnabled
        public OpportunityLineItem oli { get; set; }
    }

    public class UpdateTraderToOsiListInput {
        @AuraEnabled
        public List<OpportunityServiceItem__c> opportunityServiceItems { get; set; }
        @AuraEnabled
        public String companyDivisionId { get; set; }
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
    }
    public class ConsumptionDetails {
        @AuraEnabled
        public List<Consumption__c> consumptionList { get; set; }
        @AuraEnabled
        public String contractId { get; set; }
        @AuraEnabled
        public Boolean consumptionConventions { get; set; }
    }

    public class MetersDetails {
        @AuraEnabled
        //public List<MRO_SRV_Index.MeterInfoList> metersInfo { get; set; }
        public List<List<MRO_SRV_Index.MeterQuadrant>> metersInfo { get; set; }
        @AuraEnabled
        public List<String> metersCodes { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public Boolean selfReadingProvided { get; set; }

    }

}