/**
 * Test class for SwitchInWizardCnt
 *
 * @author Moussa Fofana
 * @version 1.0
 * @description Test class for SwitchInWizardCnt
 * @uses
 * @code
 * @history
 * 2019-15-05:  Moussa Fofana
 * 2019-27-11:  David Diop
 */

@isTest
public with sharing class SwitchInWizardCntTst {
    @testSetup
    static void setup() {
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
       User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;
        //update user;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        List<Account> listAccount = new list<Account>();
        listAccount.add(TestDataFactory.account().businessAccount().build());
        insert listAccount;
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        insert opportunity;
        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        Contact contact = TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[0].Id;
        insert contact;
        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
       
        OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        insert opportunityServiceItem;
        BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[0].Id;
        insert billingProfile;
        Product2 product2 =TestDataFactory.product2().build();
        insert product2;
        PricebookEntry pricebookEntry = TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = product2.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        insert pricebookEntry;
        OpportunityLineItem opportunityLineItem = TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityID = opportunity.Id;
        opportunityLineItem.Product2ID = product2.Id;
        opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;
    
        PrivacyChange__c privacyChange = TestDataFactory.privacyChange().createPrivacyChangeBuilder().setOpportunity(opportunity.Id).build();
        privacyChange.Status__c = 'Active';
        insert privacyChange;

        Contract contract = TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
    }

    @isTest
    static void initializeTest() {
        Account account = [
			SELECT Id,Name
			FROM Account
			LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
			SELECT Id
			FROM CustomerInteraction__c
			LIMIT 1
        ];
        Opportunity opportunity = [
			SELECT Id
			FROM Opportunity
			LIMIT 1
        ];
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ].Id;

        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => opportunity.Id,
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivisionId
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'SwitchInWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }

    @isTest
    static void initializeTestIsBlankOpportunityId() {
        Account account = [
			SELECT Id,Name
			FROM Account
			LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
			SELECT Id
			FROM CustomerInteraction__c
			LIMIT 1
        ];
        String opportunityId = '';


        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => '',
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => null
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'SwitchInWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @isTest
    static void initializeExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '',
            'opportunityId' => '',
            'interactionId' => '',
            'companyDivisionId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'SwitchInWizardCnt', 'initialize', inputJSON, false);
        }catch (Exception e){
            system.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void updateOpportunityTest(){
        Opportunity opportunity = [
			SELECT Id
			FROM Opportunity
			LIMIT 1
        ];
        PrivacyChange__c privacyChange = [
            SELECT  Id
            FROM PrivacyChange__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id,
            'privacyChangeId' => privacyChange.Id,
            'stage' => 'Closed Won'
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'SwitchInWizardCnt', 'updateOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }

    @isTest
    static void updateOpportunityTestException(){

        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => null,
            'stage' => 'Closed Won'
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'SwitchInWizardCnt', 'updateOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();

    }
    @isTest
    static void updateCompanyDivisionInOpportunityTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('SwitchInWizardCnt', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }
    @isTest
    static void updateCompanyDivisionInOpportunityExTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => null,
                'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('SwitchInWizardCnt', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }

    @isTest
    public static void linkOliToOsiTest(){
        List<OpportunityServiceItem__c>  listOsi = [
                SELECT Id, BillingProfile__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        OpportunityLineItem oli = [
                SELECT Id,Product2Id
                FROM OpportunityLineItem
                LIMIT 1
        ];
        ActivationWizardCnt.InputData inputData = new ActivationWizardCnt.InputData();
        inputData.opportunityServiceItems = listOsi;
        inputData.oli = oli;
        Test.startTest();
        Object response = TestUtils.exec('SwitchInWizardCnt', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }
    @isTest
    public static void linkOliToOsiExTest(){
        List<OpportunityServiceItem__c>  listOsi = [
                SELECT Id, BillingProfile__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        OpportunityLineItem oli = [
                SELECT Id,Product2Id
                FROM OpportunityLineItem
                LIMIT 1
        ];
        ActivationWizardCnt.InputData inputData = new ActivationWizardCnt.InputData();
        inputData.opportunityServiceItems = null;
        Test.startTest();
        Object response = TestUtils.exec('SwitchInWizardCnt', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }

    @isTest
    static void updateOsiListTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
                SELECT Id,ServicePointCode__c
                FROM OpportunityServiceItem__c
                LIMIT 10
        ];

        SwitchInWizardCnt.UpdateOsiListInput updateOsiListInput = new SwitchInWizardCnt.UpdateOsiListInput ();
        updateOsiListInput.opportunityServiceItems = listOppServiceItem;

        Test.startTest();
        Object response = TestUtils.exec('SwitchInWizardCnt', 'updateOsiList', updateOsiListInput, true);
        Test.stopTest();

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    @isTest
    static void updateOsiListExTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
                SELECT Id,ServicePointCode__c
                FROM OpportunityServiceItem__c
                LIMIT 10
        ];

        SwitchInWizardCnt.UpdateOsiListInput updateOsiListInput = new SwitchInWizardCnt.UpdateOsiListInput ();
        updateOsiListInput.opportunityServiceItems = null;

        Test.startTest();
        Object response = TestUtils.exec('SwitchInWizardCnt', 'updateOsiList', updateOsiListInput, true);
        Test.stopTest();

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }


    @isTest
    static void checkOsiTest() {
        OpportunityServiceItem__c oppServiceItem = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => oppServiceItem.Id
        };

        Object response = TestUtils.exec('SwitchInWizardCnt', 'checkOsi', inputJSON, true);
        Test.stopTest();
        Map<String, Object> opportunityServiceItemData = (Map<String, Object>) response;
        System.assert(opportunityServiceItemData.get('opportunityServiceItem') != null, 'Why not? ' + opportunityServiceItemData);
    }
    @isTest
    static void checkOsiExTest() {
        OpportunityServiceItem__c oppServiceItem = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => null
        };

        Object response = TestUtils.exec('SwitchInWizardCnt', 'checkOsi', inputJSON, true);
        Test.stopTest();
        Map<String, Object> resul = (Map<String, Object>) response;
        System.assert(true,resul.get('error'));
    }

    @isTest
    static void updateContractAndContractSignedDateOnOpportunityTest() {
        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        Contract contract = [
            SELECT Id
            FROM Contract
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id,
            'customerSignedDate' => '',
            'contractId'=> contract.Id
        };

        Object response = TestUtils.exec('SwitchInWizardCnt', 'updateContractAndContractSignedDateOnOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> resul = (Map<String, Object>) response;
        System.assert(true,resul.get('error')== false);
    }

    @isTest
    static void updateContractAndContractSignedDateOnOpportunityExcepTest() {
        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        Contract contract = [
            SELECT Id
            FROM Contract
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => '',
            'customerSignedDate' => '',
            'contractId'=> contract.Id
        };

        Object response = TestUtils.exec('SwitchInWizardCnt', 'updateContractAndContractSignedDateOnOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> resul = (Map<String, Object>) response;
        System.assert(true,resul.get('error')== true);
    }

    /*@isTest
    static void updateOsiListTest(){
        List<OpportunityServiceItem__c> OpportunityServiceItemList = [
			SELECT Id
			FROM OpportunityServiceItem__c
        ];
        BillingProfile__c billingProfile = [
			SELECT Id
			FROM BillingProfile__c
			lIMIT 1
        ];

        SwitchInWizardCnt.UpdateOsiListInput inputData = new SwitchInWizardCnt.UpdateOsiListInput();
        inputData.billingProfileId = billingProfile.Id;
        inputData.opportunityServiceItems = OpportunityServiceItemList;

        String opportunityServiceItemsListString = JSON.serialize(OpportunityServiceItemList);
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityServiceItems' => opportunityServiceItemsListString,
            'billingProfileId' => billingProfile.Id
        }

        Test.startTest();
        Object response = TestUtils.exec(
                'SwitchInWizardCnt', 'updateOsiList', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }

    /*@isTest
    static void updateOsiListExceptionTest(){
        List<OpportunityServiceItem__c> OpportunityServiceItemList = [
			SELECT Id
			FROM OpportunityServiceItem__c
        ];
        String  billingProfileId = 'qddsfeff224454212DDDFDSF';
        Test.startTest();
        Map<String, Object> response = SwitchInWizardCnt.updateOsiList(OpportunityServiceItemList, billingProfileId);
        System.assertEquals(true, response.get('error'));
        Test.stopTest();
    }

    @isTest
    static void checkOsiTest(){
        OpportunityServiceItem__c opportunityServiceItem = [
			SELECT Id,ServicePointCode__c, RecordType.DeveloperName, Product__c,
				Distributor__c,BillingProfile__c, Trader__c, PointStreetType__c, PointStreetName__c, PointStreetNumber__c, PointApartment__c, PointBuilding__c, PointCity__c,
				PointCountry__c, PointFloor__c,Market__c,NonDisconnectable__c, PointLocality__c, PointPostalCode__c, PointProvince__c, Voltage__c, VoltageLevel__c, PressureLevel__c, Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c,
				Opportunity__c, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Pricebook2Id, Opportunity__r.Type, Opportunity__r.Account.Name, Opportunity__r.StageName, Opportunity__r.CloseDate, Opportunity__r.CompanyDivision__c
			FROM OpportunityServiceItem__c
			LIMIT 1
        ];
        Test.startTest();
        Map<String, Object> response = SwitchInWizardCnt.checkOsi(opportunityServiceItem.Id);
        System.assertEquals(false, response.get('error'));
        System.assertEquals(opportunityServiceItem, response.get('opportunityServiceItem'));
        Test.stopTest();
    }

    @isTest
    static void checkOsiExceptionTest(){
        String opportunityServiceItemId = null;
        Test.startTest();
        Map<String, Object> response = SwitchInWizardCnt.checkOsi(opportunityServiceItemId);
        System.assertEquals(true , response.get('error'));
        Test.stopTest();
    }

    @isTest
    static void linkOliToOsiTest(){
        List<OpportunityServiceItem__c> OpportunityServiceItemList = [
			SELECT Id
			FROM OpportunityServiceItem__c
        ];
        OpportunityLineItem opportunityLineItem = [
			SELECT Id,Product2Id
			FROM OpportunityLineItem
			LIMIT 1
        ];
        Test.startTest();
        Map<String, Object> response = SwitchInWizardCnt.linkOliToOsi(OpportunityServiceItemList, opportunityLineItem);
        System.assertEquals(false, response.get('error'));
        Test.stopTest();

    }

    @isTest
    static void linkOliToOsiExceptionTest(){
        List<OpportunityServiceItem__c> OpportunityServiceItemList = [
			SELECT Id
			FROM OpportunityServiceItem__c
        ];
        Test.startTest();
        Map<String, Object> response = SwitchInWizardCnt.linkOliToOsi(OpportunityServiceItemList,null);
        System.assertEquals(true, response.get('error'));
        Test.stopTest();
    }*/
}