/**
 * Created by napoli on 24/09/2019.
 */

public class MRO_UTL_Validations {

    public class Result {
        public Boolean outCome;
        public String errorCode;
    }

    public static Result checkCNP(String cnp) {
        Result result = new Result();
        cnp = cnp.trim();
        String[] cnpArray = cnp.split('');
        String[] control = '279146358279'.split('');
        if ((cnpArray.size() == 13) && cnp.isNumeric()) {

            if (cnp.startsWith('0')) {
                //error first digit zero is not allowd
                result.outCome = false;
                result.errorCode = 'E-CNP001';
                return result;
            }

            Integer month = Integer.valueOf(cnp.mid(3, 2));
            if (month < 1 || month > 12) {
                result.outCome = false;
                result.errorCode = 'E-CNP002';
                return result;
            }

            Integer day = Integer.valueOf(cnp.mid(5, 2));
            if (day < 1 || day > 31) {
                result.outCome = false;
                result.errorCode = 'E-CNP003';
                return result;
            }

            Integer countryZone = Integer.valueOf(cnp.mid(7, 2));
            if ((countryZone < 1 || countryZone > 52) && countryZone != 99) {
                result.outCome = false;
                result.errorCode = 'E-CNP004';
                return result;
            }

            Integer sum = 0;
            Integer cnpDigit = 0;
            Integer controlDigit = 0;

            for (Integer i = 0; i < control.size(); i++) {
                cnpDigit = Integer.valueOf(cnpArray[i]);
                controlDigit = Integer.valueOf(control[i]);

                sum = sum + (cnpDigit * controlDigit);
            }

            Long rest = Math.mod(sum, 11);

            if (((rest < 10) && (rest == Integer.valueOf(cnpArray[12]))) || ((rest == 10) && (Integer.valueOf(cnpArray[12]) == 1))) {
                result.outCome = true;
                result.errorCode = '';
                return result;
            } else { //error checksum digit
                result.outCome = false;
                result.errorCode = 'E-CNP005';
                return result;
            }
        } else { //cnp not numeric or length is not 13
            result.outCome = false;
            result.errorCode = 'E-CNP006';
            return result;
        }
    }

    public static Result checkCUI(String cui) {
        Result result = new Result();
        cui = cui.trim();
        if (cui.replace('RO', '').length() > 10) { //Code too long
            result.outCome = false;
            result.errorCode = 'E-CUI001';
            return result;
        }
        String cif = cui.replace('RO', '');
        if (cif.isNumeric()) {
            String cheia = '753217532';
            String keia, cifre, controlul;
            Integer suma;
            Double rest;
            keia = cheia.reverse();
            cifre = cif.reverse();
            String[] keiaArray = keia.split('');
            controlul = cifre.left(1);
            cifre = cifre.right(cifre.length() - 1);
            String[] cifreArray = cifre.split('');
            suma = 0;
            for (Integer i = 0; i < cifre.length(); i++) {
                suma = suma + Integer.valueOf(cifreArray[i]) * Integer.valueOf(keiaArray[i]);
            }
            if (Math.mod((suma * 10), 11) == 10) {
                rest = 0;
            } else {
                rest = Math.mod((suma * 10), 11);
            }
            if (rest == Integer.valueOf(controlul)) { //Correct code
                result.outCome = true;
                result.errorCode = '';
                return result;
            } else { //Incorrect code
                result.outCome = false;
                result.errorCode = 'E-CUI002';
                return result;
            }
        } else { //CUI must be numeric
            result.outCome = false;
            result.errorCode = 'E-CUI003';
            return result;
        }
    }


    public static Result checkIbanValidation(String IBAN) {
        Result result = new Result();
        System.debug('Check_CIN_IBAN');
        Map<String, Integer> letter_dic = new Map<String, Integer>();
        letter_dic.put('A', 10);
        letter_dic.put('B', 11);
        letter_dic.put('C', 12);
        letter_dic.put('D', 13);
        letter_dic.put('E', 14);
        letter_dic.put('F', 15);
        letter_dic.put('G', 16);
        letter_dic.put('H', 17);
        letter_dic.put('I', 18);
        letter_dic.put('J', 19);
        letter_dic.put('K', 20);
        letter_dic.put('L', 21);
        letter_dic.put('M', 22);
        letter_dic.put('N', 23);
        letter_dic.put('O', 24);
        letter_dic.put('P', 25);
        letter_dic.put('Q', 26);
        letter_dic.put('R', 27);
        letter_dic.put('S', 28);
        letter_dic.put('T', 29);
        letter_dic.put('U', 30);
        letter_dic.put('V', 31);
        letter_dic.put('W', 32);
        letter_dic.put('X', 33);
        letter_dic.put('Y', 34);
        letter_dic.put('Z', 35);

        Map<String, String> country_dic = new Map<String, String>();
        country_dic.put('AL', '28Albania');
        country_dic.put('AD', '24Andorra');
        country_dic.put('AT', '20Austria');
        country_dic.put('BE', '16Belgium');
        country_dic.put('BA', '20Bosnia');
        country_dic.put('BG', '22Bulgaria');
        country_dic.put('HR', '21Croatia');
        country_dic.put('CY', '28Cyprus');
        country_dic.put('CZ', '24Czech Republic');
        country_dic.put('DK', '18Denmark');
        country_dic.put('EE', '20Estonia');
        country_dic.put('FO', '18Faroe Islands');
        country_dic.put('FI', '18Finland');
        country_dic.put('FR', '27France');
        country_dic.put('DE', '22Germany');
        country_dic.put('GI', '23Gibraltar');
        country_dic.put('GR', '27Greece');
        country_dic.put('GL', '18Greenland');
        country_dic.put('HU', '28Hungary');
        country_dic.put('IS', '26Iceland');
        country_dic.put('IE', '22Ireland');
        country_dic.put('IL', '23Israel');
        country_dic.put('IT', '27Italy');
        country_dic.put('LV', '21Latvia');
        country_dic.put('LI', '21Liechtenstein');
        country_dic.put('LT', '20Lithuania');
        country_dic.put('LU', '20Luxembourg');
        country_dic.put('MK', '19Macedonia');
        country_dic.put('MT', '31Malta');
        country_dic.put('MU', '30Mauritius');
        country_dic.put('MC', '27Monaco');
        country_dic.put('ME', '22Montenegro');
        country_dic.put('NL', '18Netherlands');
        country_dic.put('NO', '15Northern Ireland');
        country_dic.put('PO', '28Poland');
        country_dic.put('PT', '25Portugal');
        country_dic.put('RO', '24Romania');
        country_dic.put('SM', '27San Marino');
        country_dic.put('SA', '24Saudi Arabia');
        country_dic.put('RS', '22Serbia');
        country_dic.put('SK', '24Slovakia');
        country_dic.put('SI', '19Slovenia');
        country_dic.put('ES', '24Spain');
        country_dic.put('SE', '24Sweden');
        country_dic.put('CH', '21Switzerland');
        country_dic.put('TR', '26Turkey');
        country_dic.put('TN', '24Tunisia');
        country_dic.put('GB', '22United Kingdom');

        String stringa = ''; //Nella stringa ci mette il bban+ITxx e al posto dei caratteri alfanumerici sostituisce i numeri della letter_dic
        Integer length = IBAN.length();
        String country = IBAN.substring(0, 2);
        if (country_dic.containsKey(country)) {
            String data = country_dic.get(country);
            Integer length_c = Integer.valueOf(data.substring(0, 2));
            String name_c = data.substring(2, data.length());
            if (length == length_c) {
                String header = IBAN.substring(0, 4);
                String body = IBAN.substring(4, IBAN.length());
                IBAN = body + header ;
                String[] IBAN_array = IBAN.split('');
                //IBAN_array.remove(0);

                for (String s : IBAN_array) {
                    if (letter_dic.containsKey(s)) {
                        String value = String.valueOf(letter_dic.get(s));
                        stringa = stringa + value;
                    } else {
                        stringa = stringa + s;
                    }
                }
                Boolean valid;
                System.debug('Rearrange: ' + stringa);
                Integer step = 9, i = 0;
                String resto = '';
                /* CALCOLO MOD 97
                A partire dalla cifra più a sinistra di stringa, costruisco un numero utilizzando le prime 9 cifre che chiamo N
                Calcolo N mod 97
                Costruisco un nuovo N a 9 cifre concatenando il risultato dell'operazione sopra con le prossime 7 cifre di stringa.
                Se rimangono meno di 7 cifre, costruisco un nuovo N aggiungendo le cifre rimanenti.
                Ripeto l'operazione sino a elaborare tutta la stringa
                */
                do {
                    if (i + step > stringa.length()) {
                        step = stringa.length() - i;
                    }
                    if (!(resto + stringa.substring(i, i + step)).isNumeric()) {
                        result.outCome = false; //gnapoli fix check iban 20150421
                        result.errorCode = 'E-IBAN001';
                    }

                    Integer N = Integer.valueOf(resto + stringa.substring(i, i + step));
                    resto = String.valueOf(Math.mod(N, 97));
                    i = i + step;step = 7;
                } while (i < stringa.length());

                System.debug(resto);
                if (resto != '1') {
                    valid = false;
                } else {
                    valid = true;
                }
                if (!valid) {
                    result.outCome = false;
                    result.errorCode = 'E-IBAN002';
                    //System.debug('IBAN non valido');
                } else {
                    result.outCome = true;
                    result.errorCode = '';
                    //System.debug('IBAN corretto');
                }
            } else {
                result.outCome = false;
                result.errorCode = 'E-IBAN003';
                //System.debug('Lunghezza IBAN errata');
            }
        } else {
            result.outCome = false;
            result.errorCode = 'E-IBAN004';
            //System.debug('Codice paese errato!');
        }
        return result;
    }

    public static Result checkONRC (String ONRCCode) {
        Result result = new Result();
        final String  ONRCregex = '^(J|F|C)\\d{2}\\/\\d+\\/\\d{4}$';
        result.outCome = Pattern.matches(ONRCregex,ONRCCode);
        return result;
    }

    public static Result checkPhone(String phone) {
        Result result = new Result();
        final String  phoneRegex = '^[0][0-9]{9}$';
        result.outCome = Pattern.matches(phoneRegex,phone);
        return result;
    }


    /**
     * @description Check if an email is valid or not
     * @author Goudiaby
     * @param email Email to be check
     * @return true if is valid otherwise false
    */
    public static Boolean isValidEmail(String email) {
        String inputString = email != null ? email : '';
        String defaultRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        return Pattern.compile(defaultRegex).matcher(inputString).matches();
    }

    /**
     * @description Check if an phone is valid Romania Phone or not
     * @author Goudiaby
     * @param phone Email to be check
     * @return true if it is valid otherwise false
    */
    public static Boolean isValidRomaniaPhone(String phone) {
        String inputString = phone != null ? phone : '';
        String defaultRegex = '(^(07[0-9]{1})([0-9]{7}))';
        return Pattern.compile(defaultRegex).matcher(inputString).matches();
    }
}