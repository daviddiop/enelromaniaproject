/**
 * Created by Boubacar Sow on 15/01/2020.
 */

public with sharing class MRO_QR_User {
    public static MRO_QR_User getInstance() {
        return new MRO_QR_User();
    }

    /**
     * Returns the context division's company Id
     * @param userId
     *
     * @return User record
     */
    public User getCompanyDivisionId(String userId) {
        List<User> currentUsers = [
                SELECT Id,CompanyDivisionId__c,CompanyDivisionEnforced__c, SalesSupportUser__c
                FROM User
                WHERE Id = :userId AND IsActive = TRUE
                LIMIT 1
        ];
        return currentUsers.isEmpty() ? null : currentUsers.get(0);
    }

    /**
     * @author Boubacar Sow
     * @date 03/12/2020
     * @description  [ENLCRO-1859] Location Approval - MROCRMDEF-3109
     * @param userId
     *
     * @return User
     */
    public User getStateProvince(String userId) {
        List<User> currentUsers = [
                SELECT Id, Name, State, CompanyDivisionId__c
                FROM User
                WHERE Id = :userId AND IsActive = TRUE
                LIMIT 1
        ];
        return currentUsers.isEmpty() ? null : currentUsers.get(0);
    }

    public User getUserData(String userId) {
        List<User> currentUsers = [
                SELECT Id,CompanyDivisionId__c,CompanyDivisionEnforced__c, SalesUnitID__c,Department, SalesSupportUser__c
                FROM User
                WHERE Id = :userId AND IsActive = TRUE
                LIMIT 1
        ];
        return currentUsers.isEmpty() ? null : currentUsers.get(0);
    }

    /**
     *
     *
     * @param userId
     *
     * @return
     */
    public User getUserProfileId(String userId){
        List<User> currentUsers = [
            SELECT Id,Username,ProfileId
            FROM User
            WHERE Id = :userId AND IsActive = TRUE
            LIMIT 1
        ];
        return currentUsers.isEmpty() ? null : currentUsers.get(0);

    }

    /**
     * Returns the queue with the specified DeveloperName
     * @param queue DeveloperName
     *
     * @return Queue
     */
    public Group getQueueByDeveloperName(String groupDeveloperName) {
        List<Group> currentGroups = [
                SELECT Id,Name, DeveloperName
                FROM Group
                WHERE Type = 'Queue' AND DeveloperName = :groupDeveloperName
                LIMIT 1
        ];
        return currentGroups.isEmpty() ? null : currentGroups.get(0);
    }

    /**
     * Returns the queue with the specified DeveloperName
     * @param type DeveloperName
     *
     * @return Queue
     */
    public Group getQueueByDeveloperNameAndType(String type,String groupDeveloperName) {
        List<Group> currentGroups = [
                SELECT Id,Name,Type, DeveloperName
                FROM Group
                WHERE Type =:type AND DeveloperName = :groupDeveloperName
                LIMIT 1
        ];
        return currentGroups.isEmpty() ? null : currentGroups.get(0);
    }

    /**
     * Returns the group member with the specified group id and user id
     * @param groupId Group id
     * @param userId User Id
     *
     * @return GroupMember
     */
    public GroupMember getGroupMemberByGroupIdAndUserId(String groupId, Id userId) {
        List<GroupMember> currentGroupMembers = [
                SELECT Id
                FROM GroupMember
                WHERE GroupId = :groupId AND UserOrGroupId = :userId
                LIMIT 1
        ];
        return currentGroupMembers.isEmpty() ? null : currentGroupMembers.get(0);
    }

    /**
     * @param userIds
     * @param queryFields
     * @return
     */
    public List<User> getUsers(Set<String> userIds, Set<String> queryFields) {

        String queryString =
            'SELECT ' + String.join(new List<String>(queryFields), ', ') + ' ' +
            'FROM User ' +
            'WHERE Id IN (\'' + String.join(new List<String>(userIds), '\', \'') + '\')';
        return Database.query(queryString);
    }

    /**
     *
     * @param queueDeveloperNames
     * @return
     */
    public List<Group> getQueueByDeveloperName(Set<String> queueDeveloperNames) {

        return [
            SELECT Id,Name, DeveloperName
            FROM Group
            WHERE Type = 'Queue' AND DeveloperName IN :queueDeveloperNames
        ];
    }

    /**
     *
     *
     * @param Marca__c
     *
     * @return User
     */
    public User getUserByMarca(String marca) {
        List<User> currentUsers = [
                SELECT Id,CompanyDivisionId__c,CompanyDivisionEnforced__c, SalesUnitID__c,Department, SalesSupportUser__c
                FROM User
                WHERE Marca__c = :marca AND IsActive = TRUE
                LIMIT 1
        ];
        return currentUsers.isEmpty() ? null : currentUsers.get(0);
    }


}