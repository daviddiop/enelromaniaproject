public with sharing class MRO_SRV_Bit2Win {
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    public static MRO_SRV_Bit2Win getInstance() {
        return (MRO_SRV_Bit2Win) ServiceLocator.getInstance(MRO_SRV_Bit2Win.class);
    }

    Bit2WinMicroServiceSetting__mdt currentBit2WinSetting = getBit2WinSettings(UserInfo.getOrganizationId());
    //Define as a default value
    String endPoint = currentBit2WinSetting != null ? currentBit2WinSetting.EndPoint__c : 'https://b2winginsvileu3.herokuapp.com/b2wgin/api/19r1.1';
    String organizationId = currentBit2WinSetting != null ? currentBit2WinSetting.OrgId__c : '00D3N0000008nCDUAY';
    String clientId = currentBit2WinSetting != null ? currentBit2WinSetting.ClientId__c : '82w3nE1r0d3Vm162';
    String token = '';


    Map<String, String> requestHeaders = new Map<String, String>{
            'organizationId' => organizationId,
            'clientId' => clientId,
            'token' => token,
            'x-forwarded-proto' => 'https',
            'Content-Type' => 'application/json'
    };

    public void cloneOrder(String orderId) {
//        String orderId = 'a2B0Q000001XAsRUAW';
        try {
            GenerateTokenResponse tokenResponse = (GenerateTokenResponse) sendToHerokuApi('generateToken', null, GenerateTokenResponse.class);
            requestHeaders.put('token', tokenResponse.token);

            Map<String, Object> factMap = getFactMap(orderId);
            Map<String, Object> configurationMap = (Map<String, Object>) factMap.get('configuration');
            String catalogId = (String) configurationMap.get('NE__CatalogId__c');

            RetrieveItemsRequest requestObject = new RetrieveItemsRequest(new Option(false, factMap), new RetrieveItem(catalogId, configurationMap));
            String retrieveResponse = (String) sendToHerokuApi('RetrieveItems', JSON.serialize(requestObject), null);

            Map<String, Object> checkOutResult = checkoutCart(prepareCheckOutRequest(retrieveResponse));

            Map<String, Object> checkoutConfigurationMap = (Map<String, Object>) checkOutResult.get('configuration');
            System.debug(checkoutConfigurationMap.get('Id'));

        } catch (Exception exp) {
            System.debug(exp);
        }
    }

    public Map<String, Object> addItemAndCheckOutCart(String sessionId, String catalogItemId, String itemCode, String oppId) {
        Map<String, Object> cart = new Map<String, Object>();
        Map<String, Object> checkOutResult = new Map<String, Object>();
        try {
            GenerateTokenResponse tokenResponse = (GenerateTokenResponse) sendToHerokuApi('generateToken', null, GenerateTokenResponse.class);
            requestHeaders.put('token', tokenResponse.token);
            System.debug('*** requestHeaders *** ' + requestHeaders);
            Opportunity opp = [SELECT Id,AccountId FROM Opportunity WHERE Id = :oppId];
            Map<String, Object> configurationMap = new Map<String, Object>();
            configurationMap.put('NE__OptyId__c', opp.Id);
            configurationMap.put('NE__AccountId__c', opp.AccountId);
            configurationMap.put('NE__ConfigurationStatus__c', 'Valid');
            configurationMap.put('NE__OrderStatus__c', 'Active');

            List<MRO_SRV_Bit2Win.ItemData> items = new List<MRO_SRV_Bit2Win.ItemData>();
            ItemData item = new ItemData(new Map<String, Object>{
                    'id' => catalogItemId,
                    'itemCode' => itemCode
            });
            items.add(item);
            System.debug('*** items *** ' + items);
            System.debug('*** configurationMap *** ' + configurationMap);
            //If we have the sessionId catalogId and categoryId are not necessary
            UpsertItem upsertItem = new UpsertItem(sessionId, configurationMap, items);
            UpsertItemsRequest requestObject = new UpsertItemsRequest(upsertItem);
            String upsertResponse = (String) sendToHerokuApi('upsertItems', JSON.serialize(requestObject), null);

            cart = checkoutCart(prepareCheckOutRequest(upsertResponse, false, configurationMap));

            checkOutResult = createOliFromCart(cart);
        } catch (Exception exp) {
            System.debug(LoggingLevel.ERROR, exp.getMessage());
        }
        return checkOutResult;
    }

    public Map<String, Object> createOliFromCart(Map<String, Object> checkoutResult) {
        Boolean success = true;
        String message;
        Map<String, Object> result = new Map<String, Object>();
        result.put('success', success);
        result.put('message', message);
        Savepoint sp = Database.setSavepoint();
        try {
            Map<String, Object> configuration = (Map<String, Object>) checkoutResult.get('configuration');
            Map<String, Object> cartItem = getItemDataFromCart(checkoutResult);
            String opportunityId = (String) configuration.get('NE__OptyId__c');
            String commercialProd = (Id) cartItem.get('productid');
            List<Pricebook2> priceBookList = [
                    SELECT Id
                    FROM Pricebook2
                    WHERE IsStandard = TRUE
                    LIMIT 1
            ];

            Opportunity myOpportunity = new Opportunity(Id = opportunityId, Pricebook2Id = priceBookList.get(0).Id);
            databaseSrv.updateSObject(myOpportunity);

            List<Product2> product2List = [
                    SELECT Id, Name, CommercialProduct__c
                    FROM Product2
                    WHERE CommercialProduct__c = :commercialProd
                    LIMIT 1
            ];
            System.debug('*** product2List  ' + product2List);
            List<Product2> product2Insert = new List<Product2>();
            product2Insert.add(new Product2(
                    Name = (String) cartItem.get('productname'),
                    CommercialProduct__c = (Id) cartItem.get('productid'),
                    Key__c = String.valueOf(System.currentTimeMillis()),
                    IsActive = true
            ));
            databaseSrv.insertSObject(product2Insert);

            OpportunityLineItem oli = new OpportunityLineItem();
            oli.Product2Id = !product2List.isEmpty() ? product2List[0].Id : '';
            oli.OpportunityId = opportunityId;
            oli.Quantity = Decimal.valueOf((String) cartItem.get('qty'));
            oli.UnitPrice = getPrice((Decimal.valueOf((String) cartItem.get('totalonetimefeediscounted'))), Decimal.valueOf((String) cartItem.get('recurringchargediscounted')));
            databaseSrv.insertSObject(oli);

            result.put('oli', oli);
        } catch (Exception exc) {
            System.debug(LoggingLevel.ERROR, exc.getStackTraceString());
            Database.rollback(sp);
            success = false;
            message = exc.getMessage();
        }
        return result;
    }

    public Map<String, Object> getItemDataFromCart(Map<String, Object> checkoutResult) {
        List<Object> cartList = (List<Object>) checkoutResult.get('cart');
        Map<String, Object> cart = (Map<String, Object>) cartList.get(0);
        Map<String, Object> itemData = (Map<String, Object>) cart.get('fields');
        return itemData;
    }

    //TODO: To be merged with the existent prepareCheckOutRequest method
    public String prepareCheckOutRequest(String checkoutRequest, Boolean fromExistingOrder, Map<String, Object> additionalConfigData) {
        Map<String, Object> checkoutRequestMap = (Map<String, Object>) JSON.deserializeUntyped(checkoutRequest);
        checkoutRequestMap.remove('sessionInfo');
        checkoutRequestMap.remove('listOfItems');
        checkoutRequestMap.remove('listOfCategories');
        checkoutRequestMap.remove('listOfBundle');

        Map<String, Object> configuration = (Map<String, Object>) checkoutRequestMap.get('configuration');
        if (additionalConfigData != null && !additionalConfigData.isEmpty()) {
            configuration.putAll(additionalConfigData);
        }
        configuration.remove('itemsCounter');

        if (fromExistingOrder) {
            configuration.remove('id');
            Map<String, Object> sessionParameters = (Map<String, Object>) checkoutRequestMap.get('sessionParameters');
            sessionParameters.remove('ordId');
            List<Object> cartList = (List<Object>) checkoutRequestMap.get('cart');
            Map<String, Object> cart = (Map<String, Object>) cartList.get(0);
            Map<String, Object> fields = (Map<String, Object>) cart.get('fields');
            fields.remove('id');
            fields.remove('orderitemid');
        }

        return JSON.serialize(checkoutRequestMap);
    }

    public String prepareCheckOutRequest(String retrieveResponse) {
        return prepareCheckOutRequest(retrieveResponse, true, null);
        /*Map<String, Object> retrieveResponseMap = (Map<String, Object>) JSON.deserializeUntyped(retrieveResponse);
        retrieveResponseMap.remove('sessionInfo');
        retrieveResponseMap.remove('listOfItems');
        retrieveResponseMap.remove('listOfCategories');
        retrieveResponseMap.remove('listOfBundle');
        retrieveResponseMap.remove('itemsCounter');

        Map<String, Object> configuration = (Map<String, Object>)retrieveResponseMap.get('configuration');
        configuration.remove('id');

        Map<String, Object> sessionParameters = (Map<String, Object>)retrieveResponseMap.get('sessionParameters');
        sessionParameters.remove('ordId');

        List<Object> cartList = (List<Object>)retrieveResponseMap.get('cart');
        Map<String, Object> cart = (Map<String, Object>)cartList.get(0);
        Map<String, Object> fields = (Map<String, Object>)cart.get('fields');
        fields.remove('id');
        fields.remove('orderitemid');

        return Json.serialize(retrieveResponseMap);*/
    }

    public Map<String, Object> getFactMap(String orderId) {
        String resultObjectString = sendToSFApi(new Map<String, Object>{
                'requestData' => new Map<String, String>{
                        'action' => 'retrieveContextData',
                        'parameterString' => 'ordId=' + orderId
                }
        });

        Map<String, String> currentFactsMap = (Map<String, String>) JSON.deserialize(resultObjectString, Map<String, String>.class);
        Map<String, Object> resultFactMap = new Map<String, Object>();
        for (String factKey : currentFactsMap.keySet()) {
            if (factKey.contains('___')) {
                String subKey = factKey.split('___').get(0);
                Object subKeyValue = resultFactMap.get(subKey);
                if (subKeyValue == null) {
                    subKeyValue = new List<Object>();
                    resultFactMap.put(subKey, subKeyValue);
                }
                ((List<Object>) subKeyValue).add(JSON.deserializeUntyped(currentFactsMap.get(factKey)));
            } else {
                String factJson = currentFactsMap.get(factKey);
                resultFactMap.put(factKey, JSON.deserializeUntyped(factJson));
            }
        }
        return resultFactMap;

    }

    public Map<String, Object> checkoutCart(String jsonString) {
        String resultObjectString = sendToSFApi(new Map<String, Object>{
                'requestData' => new Map<String, String>{
                        'action' => 'checkoutCart',
                        'dataInterceptorString' => jsonString
                }
        });
        return (Map<String, Object>) JSON.deserializeUntyped(resultObjectString);
    }

    public String sendToSFApi(Map<String, Object> requestBody) {
        String retrieveDataUrl = Url.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/NE/B2WGin_retrieveData';

        HttpRequest request = new HttpRequest();
        request.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST');
        request.setEndpoint(retrieveDataUrl);
        request.setTimeout(120000);
        request.setBody(JSON.serialize(requestBody));

        Http httpExecutor = new Http();

        HttpResponse response = httpExecutor.send(request);
        Integer statusCode = response.getStatusCode();

        if (statusCode >= 200 && statusCode < 300) {
            Map<String, String> responseObject = (Map<String, String>) JSON.deserialize(response.getBody(), Map<String, String>.class);
            String resultObject = responseObject.get('result');
            return resultObject.replace('{}', '"{}"');
        } else {
            throw new WrtsException(response.getStatus() + ' ' + response.getBody());
        }
    }

    public Object sendToHerokuApi(String path, String body, Type jsonType) {
        HttpRequest request = new HttpRequest();
        setHeaders(request, requestHeaders);
        request.setMethod('POST');
        request.setEndpoint(endPoint + '/' + path);
        if (String.isNotBlank(body)) {
            request.setBody(body);
        }

        Http httpExecutor = new Http();
        HttpResponse response = httpExecutor.send(request);
        Integer statusCode = response.getStatusCode();
        if (statusCode >= 200 && statusCode < 300) {
            System.debug(response.getBody());
            if (jsonType != null) {
                return JSON.deserialize(response.getBody(), jsonType);
            } else {
                return response.getBody();
            }
        } else {
            throw new WrtsException(response.getStatus() + ' ' + response.getBody());
        }
    }

    private void setHeaders(HttpRequest request, Map<String, String> headers) {
        for (String key : headers.keySet()) {
            request.setHeader(key, headers.get(key));
        }
    }

    private class GenerateTokenResponse {
        public String errorCode;
        public String token;
    }

    private class RetrieveCatalogsRequest {
        public RetrieveCatalog retrieveCatalogs;
        public Option options;

        public RetrieveCatalogsRequest() {
            this.retrieveCatalogs = new RetrieveCatalog();
            this.options = new Option();
        }

        public RetrieveCatalogsRequest(MRO_SRV_Bit2Win.RetrieveCatalog retrieveCatalogs, MRO_SRV_Bit2Win.Option options) {
            this.retrieveCatalogs = retrieveCatalogs;
            this.options = options;
        }
    }

    private class RetrieveCatalog {
        public SessionInfo sessionInfo;

        public RetrieveCatalog() {
            this.sessionInfo = new SessionInfo();
        }
    }

    private class SessionInfo {
        public String Id;

        public SessionInfo() {
            this.Id = null;
        }

        public SessionInfo(String sessionId) {
            this.Id = sessionId;
        }
    }

    private class ItemData {
        public Map<String, Object> fields;

        public ItemData() {
            this.fields = new Map<String, Object>();
        }

        public ItemData(Map<String, Object> items) {
            this.fields = items;
        }
    }

    private class Option {
        public Boolean GetStructure;
        public Boolean GetAllData;
        public String userLanguage;
        public Map<String, Object> facts;

        public Option() {
            this.facts = new Map<String, Object>();
        }

        public Option(Boolean getStructure, Boolean getAllData, String userLanguage, Map<String, Object> facts) {
            this.GetStructure = getStructure;
            this.GetAllData = getAllData;
            this.userLanguage = userLanguage;
            this.facts = facts;
        }

        public Option(Boolean getStructure, Map<String, Object> facts) {
            this.GetStructure = getStructure;
            this.GetAllData = false;
            this.userLanguage = 'en_US';
            this.facts = facts;
        }
    }

    private class RetrieveItemsRequest {
        public Option options;
        public RetrieveItem retrieveItems;

        public RetrieveItemsRequest(MRO_SRV_Bit2Win.Option options, MRO_SRV_Bit2Win.RetrieveItem retrieveItems) {
            this.options = options;
            this.retrieveItems = retrieveItems;
        }

        public RetrieveItemsRequest() {
            this.options = new Option();
            this.retrieveItems = new RetrieveItem();
        }
    }

    private class RetrieveItem {
        public Map<String, Object> additionalData;
        public String catalogId;
        public String categoryId;
        public String complexProductId;
        public Map<String, Object> configuration;
        public Integer page;
        public SessionInfo sessionInfo;
        public Map<String, Object> sessionParameters;

        public RetrieveItem() {
            this.additionalData = new Map<String, Object>();
            this.configuration = new Map<String, Object>();
            this.sessionParameters = new Map<String, Object>();
        }

        public RetrieveItem(String catalogId, Map<String, Object> configuration) {
            this.catalogId = catalogId;
            this.configuration = configuration;
            this.additionalData = new Map<String, Object>();
            this.sessionParameters = new Map<String, Object>();
        }
    }

    private class UpsertItem {
        public Map<String, Object> additionalData;
        public String catalogId;
        public String categoryId;
        public String complexProductId;
        public Map<String, Object> configuration;
        public Integer page;
        public SessionInfo sessionInfo;
        public Map<String, Object> sessionParameters;
        public List<ItemData> itemsToAdd;

        public UpsertItem() {
            this.additionalData = new Map<String, Object>();
            this.configuration = new Map<String, Object>();
            this.sessionParameters = new Map<String, Object>();
        }

        public UpsertItem(String sessionId, Map<String, Object> configuration, List<ItemData> items) {
            this.sessionInfo = new SessionInfo(sessionId);
            this.configuration = configuration;
            this.additionalData = new Map<String, Object>();
            //this.sessionParameters = new Map<String, Object>();
            this.itemsToAdd = items;
        }
    }

    private class UpsertItemsRequest {
        public UpsertItem upsertItems;

        public UpsertItemsRequest(MRO_SRV_Bit2Win.UpsertItem upsertItems) {
            this.upsertItems = upsertItems;
        }

        public UpsertItemsRequest() {
            this.upsertItems = new UpsertItem();
        }
    }

    private static Decimal getPrice(Decimal price, Decimal recurringPrice) {
        if (price != null) {
            return price;
        } else if (recurringPrice != null) {
            return recurringPrice;
        } else {
            return 0;
        }
    }

    public Bit2WinMicroServiceSetting__mdt getBit2WinSettings(String orgId) {
        List<Bit2WinMicroServiceSetting__mdt> bit2winSettings = [
                SELECT OrgId__c,EndPoint__c, ClientId__c
                FROM Bit2WinMicroServiceSetting__mdt
                WHERE OrgId__c = :orgId
                LIMIT 1
        ];
        if (bit2winSettings.isEmpty()) {
            return null;
        }
        return bit2winSettings.get(0);
    }
}