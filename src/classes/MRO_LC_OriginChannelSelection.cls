/**
 * Created by goudiaby on 08/01/2020.
 * @version 1.0
 * @description
 *          [ENLCRO-325] Channel and Origin Selection - Implementation
 * @testedIn MRO_LC_OriginChannelSelectionTst
 */

public with sharing class MRO_LC_OriginChannelSelection extends ApexServiceLibraryCnt {
    static MRO_SRV_OriginChannel originChannelService = MRO_SRV_OriginChannel.getInstance();
    private static MRO_QR_OriginChannel originChannelQuery = MRO_QR_OriginChannel.getInstance();
    /**
     * @description Caller class for getting Origin Channel Picklist Options
     */
    public with sharing class OriginChannelPicklistOptions extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String objectAPIName = params.get('objectApiName');
            response.put('originChannelOptions', originChannelService.getOriginChannelOptions(objectAPIName));
            response.put('originChannelEntries', originChannelService.getSelectableOriginEntries());
            return response;
        }
    }

    /**
     * @description Caller class for getting Origin Channel Picklist Entries
     */
    public with sharing class GetPicklistEntries extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('originChannelPicklistEntries', originChannelQuery.getOriginChannelEntries());
            return response;
        }
    }
}