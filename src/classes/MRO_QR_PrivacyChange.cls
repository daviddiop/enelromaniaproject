/**
 * Created by ferhati on 05/08/2019.
 */

public with sharing class MRO_QR_PrivacyChange {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public static MRO_QR_PrivacyChange getInstance() {
        return (MRO_QR_PrivacyChange)ServiceLocator.getInstance(MRO_QR_PrivacyChange.class);
    }

    public List<PrivacyChange__c> getPrivacyChangesByIndividual(Id individualId) {
        List<PrivacyChange__c> privacyChanges = [
                SELECT Id, Dossier__c, Opportunity__c, Status__c, CreatedDate, EffectiveDate__c, DossierType__c, Individual__c, Contact__c,
                       HasOptedOutGeoTracking__c, HasOptedOutSolicit__c, HasOptedOutProcessing__c, HasOptedOutProfiling__c,
                       HasOptedOutTracking__c, SendIndividualData__c, ShouldForget__c, CanStorePiiElsewhere__c, HasOptedOutPartners__c,
                       MarketingChannel__c, MarketingEmail__c, MarketingMobilePhone__c, MarketingPhone__c
                FROM PrivacyChange__c
                WHERE Individual__c = :individualId
                ORDER BY CreatedDate DESC
                LIMIT 1000
        ];
        return privacyChanges;
    }

    public PrivacyChange__c getPrivacyChangeByOpportunityId (String opportunityId) {
        List<PrivacyChange__c> privacyChange = [
                SELECT Id,Dossier__c,Opportunity__c
                FROM PrivacyChange__c
                WHERE Opportunity__c = :opportunityId
                LIMIT 1
        ];
        if (privacyChange.isEmpty()) {
            return null;
        }
        return privacyChange.get(0);
    }

    public PrivacyChange__c getPendingPrivacyChangeByDossierId(Id dossierId) {
        List<PrivacyChange__c> privacyChanges = [
                SELECT Id, HasOptedOutTracking__c, HasOptedOutProfiling__c, HasOptedOutProcessing__c, HasOptedOutSolicit__c, HasOptedOutGeoTracking__c,
                       SendIndividualData__c, ShouldForget__c, CanStorePiiElsewhere__c, HasOptedOutPartners__c,
                       MarketingChannel__c, MarketingEmail__c, MarketingMobilePhone__c, MarketingPhone__c,
                       Individual__c, Contact__c, Status__c, CreatedDate, EffectiveDate__c, Opportunity__c, Dossier__c, DossierType__c,
                       Dossier__r.CustomerInteraction__c, Dossier__r.CustomerInteraction__r.Contact__c
                FROM PrivacyChange__c
                WHERE Dossier__c = :dossierId AND Status__c = :CONSTANTS.PRIVACYCHANGE_STATUS_PENDING
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
        if (privacyChanges.isEmpty()) {
            return null;
        }
        return privacyChanges[0];
    }

    public PrivacyChange__c getPendingPrivacyChangeByOpportunityId(Id opportunityId) {
        List<PrivacyChange__c> privacyChanges = [
                SELECT Id, HasOptedOutTracking__c, HasOptedOutProfiling__c, HasOptedOutProcessing__c, HasOptedOutSolicit__c, HasOptedOutGeoTracking__c,
                       SendIndividualData__c, ShouldForget__c, CanStorePiiElsewhere__c, HasOptedOutPartners__c,
                       MarketingChannel__c, MarketingEmail__c, MarketingMobilePhone__c, MarketingPhone__c,
                       Individual__c, Contact__c, Status__c, CreatedDate, EffectiveDate__c, Opportunity__c, Dossier__c, DossierType__c,
                       Dossier__r.CustomerInteraction__c, Dossier__r.CustomerInteraction__r.Contact__c
                FROM PrivacyChange__c
                WHERE Opportunity__c = :opportunityId AND Status__c = :CONSTANTS.PRIVACYCHANGE_STATUS_PENDING
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
        if (privacyChanges.isEmpty()) {
            return null;
        }
        return privacyChanges[0];
    }

    public List<PrivacyChange__c> getByIds(Set<Id> ids) {
        return [
                SELECT Id, Dossier__c, Opportunity__c, Status__c, EffectiveDate__c, DossierType__c, Individual__c, Contact__c,
                       HasOptedOutGeoTracking__c, HasOptedOutSolicit__c, HasOptedOutProcessing__c, HasOptedOutProfiling__c,
                       HasOptedOutTracking__c, SendIndividualData__c, ShouldForget__c, CanStorePiiElsewhere__c, HasOptedOutPartners__c,
                       MarketingChannel__c, MarketingEmail__c, MarketingMobilePhone__c, MarketingPhone__c
                FROM PrivacyChange__c
                WHERE  Dossier__c IN :ids
        ];
    }



    public List<PrivacyChange__c> getPrivacyChangeIndividuals(Set<Id> ids) {
        return [
                SELECT Id, Dossier__c, Opportunity__c, Status__c, CreatedDate, EffectiveDate__c, DossierType__c, Individual__c, Contact__c,
                       HasOptedOutGeoTracking__c, HasOptedOutSolicit__c, HasOptedOutProcessing__c, HasOptedOutProfiling__c,
                       HasOptedOutTracking__c, SendIndividualData__c, ShouldForget__c, CanStorePiiElsewhere__c, HasOptedOutPartners__c,
                       MarketingChannel__c, MarketingEmail__c, MarketingMobilePhone__c, MarketingPhone__c
                FROM PrivacyChange__c
                WHERE Individual__c IN :ids and Status__c = :CONSTANTS.PRIVACYCHANGE_STATUS_ACTIVE
        ];
    }
}