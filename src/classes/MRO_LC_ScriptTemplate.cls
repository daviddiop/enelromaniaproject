/**
 * Created by Boubacar Sow  on 24/12/2019.
 *
 * @author Bouabacr Sow
 * @date  24/12/2019
 * @description Class controller for the MRO_LC_ScriptTemplate Component.
 *              [ENLCRO-353] Power Voltage Change  Implementation Wizard.
 */
public with sharing class MRO_LC_ScriptTemplate extends ApexServiceLibraryCnt {
    private static MRO_SRV_ScriptTemplate scriptTemplateSrv = MRO_SRV_ScriptTemplate.getInstance();

    public with sharing class getScriptElements extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String templateId = params.get('templateId');
            String templateCode = params.get('templateCode');
            List<ScriptTemplateElement__c> scriptTemplateElements ;
            try{
                if (!String.isBlank(templateCode)){
                    scriptTemplateElements = scriptTemplateSrv.getScriptTemplateElementsByCode(templateCode);
                } else {
                    scriptTemplateElements = scriptTemplateSrv.getScriptTemplateElements(templateId);
                }

                response.put('error', false);
                response.put('scriptElements', scriptTemplateElements);
            }catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response ;
        }

    }

}