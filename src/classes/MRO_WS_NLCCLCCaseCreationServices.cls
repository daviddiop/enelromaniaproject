global with sharing class MRO_WS_NLCCLCCaseCreationServices {
	private static final wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration PhaseManagerIntegration =
	(wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration');
	private static final MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
	private static final MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
	private static final MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
	private static final MRO_QR_Supply supplyQry = MRO_QR_Supply.getInstance();
	private static MRO_QR_User userQuery = MRO_QR_User.getInstance();
	private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

	webservice static wrts_prcgvr.MRR_1_0.MultiResponse post(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest) {
		wrts_prcgvr.MRR_1_0.MultiResponse multiResponse = MRO_UTL_MRRMapper.initMultiResponse();
		String stackTrace;

		Savepoint sp = Database.setSavepoint();

		try {
			for (wrts_prcgvr.MRR_1_0.Request request : multiRequest.requests) {
				Map<String, Object> result = new Map<String, Object>();
				//Case creation process
				result = caseCreation(request);
				wrts_prcgvr.MRR_1_0.Response response = (wrts_prcgvr.MRR_1_0.Response) result.get('response');
				System.debug('response->>'+ response);

				if (result != null && response.code == 'OK' && result.get('dossierId') InstanceOf Id) {
					List<Case> listCasesByDossier;
					Dossier__c dossierCreated = dossierQuery.getById((String)result.get('dossierId'));
					listCasesByDossier = caseQuery.getCasesByDossierId(dossierCreated.Id);
					dossierSrv.checkAndApplyAutomaticTransitionToDossierAndCases(
							dossierCreated,
							listCasesByDossier,
							CONSTANTS.CONFIRM_TAG,
							false,
							false,
							false
					);
				}

				multiResponse.responses.add(response);
			}

		} catch (Exception e) {
			Database.rollback(sp);
			wrts_prcgvr.MRR_1_0.Response response = new wrts_prcgvr.MRR_1_0.Response();
			response.code = 'KO';
			response.header = multiRequest.requests[0].header;
			response.header.requestTimestamp = String.valueOf(System.now());
			response.description = e.getMessage();
			multiResponse.responses.add(response);
			stackTrace = e.getStackTraceString();
			system.debug('Error1: ' + e.getMessage() + e.getStackTraceString());
		}


		return multiResponse;
	}

	static Map<String, Object> caseCreation(wrts_prcgvr.MRR_1_0.Request request) {
		wrts_prcgvr.MRR_1_0.Response response = new wrts_prcgvr.MRR_1_0.Response();
		Map<String, Object> result = new Map<String, Object>();

		try {
			response = MRO_UTL_MRRMapper.initResponse(request);
			Case caseWObj, caseToInsert = new Case();
			Supply__c supply = new Supply__c();
			Map<string, string> supplyFieldsMap, servicePointFieldsMap, serviceSiteFieldsMap, recordTypeFieldsMap = new Map<string, string>();

			/*
			 * Supply
			 */
			List<wrts_prcgvr.MRR_1_0.WObject> supplyWObjects =
					MRO_UTL_MRRMapper.selectWobjects('Case/Supply__r', request);

			if (supplyWObjects != null && !supplyWObjects.isEmpty()) {
				supplyFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(supplyWObjects[0]);
				System.debug('supplyFieldsMap' + supplyFieldsMap);
				if (String.isBlank(supplyFieldsMap.get('SAPContractCode__c'))) throw new WrtsException('SAPContractCode is missing');
			} else throw new WrtsException('Missing Supply in MRR');

			/*
			 * RecordType
			 */
			List<wrts_prcgvr.MRR_1_0.WObject> recordTypeWObjects =
					MRO_UTL_MRRMapper.selectWobjects('Case/Supply__r/RecordType', request);

			if (recordTypeWObjects != null && !recordTypeWObjects.isEmpty()) {
				recordTypeFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(recordTypeWObjects[0]);
				System.debug('recordTypeFieldsMap' + recordTypeFieldsMap);
				if (String.isBlank(recordTypeFieldsMap.get('DeveloperName'))) {
					throw new WrtsException('RecordType.DeveloperName is missing');
				}
			} else throw new WrtsException('Missing RecordType.DeveloperName in MRR');

			/*
			 * ServicePoint__c
			 */
			List<wrts_prcgvr.MRR_1_0.WObject> servicePointWObjects =
					MRO_UTL_MRRMapper.selectWobjects('Case/Supply__r/ServicePoint__r', request);

			if (servicePointWObjects != null && !servicePointWObjects.isEmpty()) {
				servicePointFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(servicePointWObjects[0]);
				System.debug('servicePointFieldsMap' + servicePointFieldsMap);
				if (String.isBlank(servicePointFieldsMap.get('ENELTEL__c'))) throw new WrtsException('ENELTEL is missing');
				if (String.isBlank(servicePointFieldsMap.get('Code__c'))) throw new WrtsException('Code__c is missing');
			} else throw new WrtsException('Missing ServicePoint in MRR');

			//query supply by SAPContractCode__c and RT DeveloperName and ENELTEL__c
			List<Supply__c> supplyList = [SELECT Id, Account__c, CompanyDivision__c FROM Supply__c
			WHERE SAPContractCode__c =: supplyFieldsMap.get('SAPContractCode__c')
			AND RecordType.DeveloperName =: recordTypeFieldsMap.get('DeveloperName')
			AND ENELTEL__c =: servicePointFieldsMap.get('ENELTEL__c')];

			if(!supplyList.isEmpty()){
				supply = supplyList[0];
			} else throw new WrtsException('Supply is missing');

			/*
			 * ServiceSite__c
			 */
			List<wrts_prcgvr.MRR_1_0.WObject> serviceSiteWObjects =
					MRO_UTL_MRRMapper.selectWobjects('Case/Supply__r/ServiceSite__r', request);

			System.debug('serviceSiteWObjects' + serviceSiteWObjects);

			if (serviceSiteWObjects != null && !serviceSiteWObjects.isEmpty()) {
				serviceSiteFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(serviceSiteWObjects[0]);
				System.debug('serviceSiteFieldsMap' + serviceSiteFieldsMap);

				if (recordTypeFieldsMap.get('DeveloperName') == 'Electric') {
					if (String.isBlank(serviceSiteFieldsMap.get('NLC__c'))) throw new WrtsException('NLC is missing');
				}

				if (recordTypeFieldsMap.get('DeveloperName') == 'Gas') {
					if (String.isBlank(serviceSiteFieldsMap.get('CLC__c'))) throw new WrtsException('CLC is missing');
				}

			} else throw new WrtsException('Missing ServiceSite in MRR');

			/*
			 * Case
			 */
			List<wrts_prcgvr.MRR_1_0.WObject> caseWObjects =
					MRO_UTL_MRRMapper.selectWobjects('Case', request);



			System.debug('caseWObjects->>>>' + caseWObjects);
			System.debug('requestCase->>>>' + request);

			if (caseWObjects != null && !caseWObjects.isEmpty()) {
				caseWObj = (Case) MRO_UTL_MRRMapper.wobjectToSObject(caseWObjects[0]);
				System.debug('caseWObj->>>' + caseWObj);
				if (caseWObj.NLCCLC__c == null || (String.isBlank(caseWObj.NLCCLC__c))){
					throw new WrtsException('NLCCLC__c on Case is missing');
				}
				if (caseWObj.ServicePointCode__c == null || (String.isBlank(caseWObj.ServicePointCode__c))){
					throw new WrtsException('ServicePointCode__c on Case is missing');
				}
			} else throw new WrtsException('Case missing in MRR');


			/*
			 * Case creation process
			 */

			Id dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
			User newOwner = userQuery.getUserByMarca('RL02301');

			Dossier__c dossier = dossierSrv.generateDossier(null, null, null, null, dossierRecordTypeId, 'NLC-CLC Update');
			dossier.Origin__c = 'Internal';
			dossier.OwnerId = newOwner.Id;
			dossier.Channel__c = 'Back Office';
			dossier.Status__c = 'New';
			dossier.Commodity__c = recordTypeFieldsMap.get('DeveloperName');
			dossier.Account__c = supply.Account__c;
			dossier.CompanyDivision__c = supply.CompanyDivision__c;
			update dossier;

			Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NLC_CLC_Update').getRecordTypeId();
			caseToInsert.RecordTypeId = caseRecordTypeId;
			caseToInsert.Origin = 'Internal';
			caseToInsert.Channel__c = 'Back Office Customer Care';
			caseToInsert.CustomerNotes__c = 'Massive Correction';
			caseToInsert.Supply__c = supply.Id;
			caseToInsert.AccountId = supply.Account__c;
			caseToInsert.CompanyDivision__c = supply.CompanyDivision__c;
			caseToInsert.Dossier__c = dossier.Id;
			caseToInsert.EffectiveDate__c = System.today();
			caseToInsert.OwnerId = newOwner.Id;
			//correction
			caseToInsert.NLCCLC__c = caseWObj.NLCCLC__c;
			caseToInsert.ServicePointCode__c = caseWObj.ServicePointCode__c;


			insert caseToInsert;

			System.debug('caseToInsert' + caseToInsert);

			result.put('caseId', caseToInsert.Id);
			result.put('dossierId', dossier.Id);

			Case cs = [SELECT CaseNumber, format(CreatedDate), Status, Description FROM Case WHERE Id = :caseToInsert.Id];
			cs.Status = 'Procesat';
			cs.Description = 'Procesat cu success';
			Wrts_prcgvr.MRR_1_0.WObject caseMRR = MRO_UTL_MRRMapper.sObjectToWObject(cs, 'Case');

			response.objects.add(caseMRR);
			result.put('response', response);

		} catch (Exception e) {
			response.Code = 'KO';
			String description = e.getMessage() + ' ' + e.getStackTraceString();
			response.header.requestTimestamp = String.valueOf(System.now());
			response.description = description;
			system.debug('Error: ' + description);
			result.put('response', response);
		}
		return result;

	}
}