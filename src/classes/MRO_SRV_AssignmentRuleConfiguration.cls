/**
 * Created by tommasobolis on 03/11/2020.
 */

public with sharing class MRO_SRV_AssignmentRuleConfiguration {

    private static Map<Id, User> userMap { get; set; } { userMap = new Map<Id, User>(); }

    private static MRO_QR_AssignmentRuleConfiguration assignmentRuleConfigurationQry = MRO_QR_AssignmentRuleConfiguration.getInstance();
    private static MRO_QR_User userQry = MRO_QR_User.getInstance();

    /**
     * @return
     */
    public static MRO_SRV_AssignmentRuleConfiguration getInstance() {

        return (MRO_SRV_AssignmentRuleConfiguration) ServiceLocator.getInstance(MRO_SRV_AssignmentRuleConfiguration.class);
    }

    /**
     * @param newSObjects
     * @param oldSObjectMap
     * @param triggeringFieldName
     * @return
     */
    public void applyAssignmentRuleLogic(List<SObject> newSObjects, Map<Id, SObject> oldSObjectMap,
            String triggeringFieldName) {

        List<SObject> filteredSObjects = null;
        Map<Id, AssignmentRuleConfiguration__mdt> sObjectIdToMatchedAssignmentRuleConfigurationMap = null;
        try {

            // Get list of sObject for which assignment rules must be evaluated
            filteredSObjects = getSObjectToEvaluate(newSObjects, oldSObjectMap, triggeringFieldName);
            System.debug('MRO_SRV_AssignmentRuleConfiguration.applyAssignmentRuleLogic - filteredSObjects: ' +
                    filteredSObjects);
            // Check if at least a sObject to be evaluated exists
            if (!filteredSObjects.isEmpty()) {

                // Build map of sObject Id to matching assignment rule configuration
                sObjectIdToMatchedAssignmentRuleConfigurationMap =
                        getSObjectIdToMatchedAssignmentRuleConfigurationMap(filteredSObjects, triggeringFieldName);
                System.debug('MRO_SRV_AssignmentRuleConfiguration.applyAssignmentRuleLogic - sObjectIdToMatchedAssignmentRuleConfigurationMap: ' +
                        sObjectIdToMatchedAssignmentRuleConfigurationMap);
                // Check if at least a matching assignment rule has been identified
                if (!sObjectIdToMatchedAssignmentRuleConfigurationMap.isEmpty()) {

                    // Get queues information
                    Map<String, Id> queueDeveloperNameToQueueIdMap = getQueueDeveloperNameToQueueIdMap(
                            sObjectIdToMatchedAssignmentRuleConfigurationMap.values());
                    // Init list of related sObejcts to update
                    List<SObject> relatedSObjectsToUpdate = new List<SObject>();
                    // Iterate over sObjects
                    for (SObject sObj : filteredSObjects) {

                        // Check if a matching assignment rule has been identified for  current sObject
                        if (sObjectIdToMatchedAssignmentRuleConfigurationMap.containsKey(sObj.Id)) {

                            AssignmentRuleConfiguration__mdt assignmentRuleConfiguration =
                                    sObjectIdToMatchedAssignmentRuleConfigurationMap.get(sObj.Id);
                            // Check if debug is active
                            if (assignmentRuleConfiguration.Debug__c) {

                                MRO_SRV_ExecutionLog.createDebugLog(
                                        'Assignment Rule Execution',
                                        assignmentRuleConfiguration.SObjectname__c + ' Assignment Rule Execution',
                                        'Applying assignment rule to ' + assignmentRuleConfiguration.SObjectname__c + ' with id ' + sObj.Id,
                                        '',
                                        assignmentRuleConfiguration.MasterLabel,
                                        sObj.Id
                                );
                            }
                            // Check if field to store old owner exists
                            if(sObj.getSobjectType().getDescribe().fields.getMap().keySet().contains('originalowner__c')) {

                                sObj.put('OriginalOwner__c', sObj.get('OwnerId'));
                            }
                            String ownerId = null;
                            // Check if dynamic code must be executed or not
                            if (String.isBlank(assignmentRuleConfiguration.ApexClassName__c) ||
                                    String.isBlank(assignmentRuleConfiguration.ApexClassMethod__c)) {

                                // Get new owner id
                                ownerId = assignmentRuleConfiguration.AssigneeType__c == 'Queue' ?
                                        queueDeveloperNameToQueueIdMap.get(assignmentRuleConfiguration.Assignee__c) :
                                        assignmentRuleConfiguration.Assignee__c;
                            } else {

                                // Init parameters
                                Map<String, Object> params = new Map<String, Object>();
                                params.put('apexClassMethod', assignmentRuleConfiguration.ApexClassMethod__c);
                                params.put('sObject', sObj);
                                params.put('owner', userMap.containsKey((Id)sObj.get('OwnerId')) ?
                                        userMap.get((Id)sObj.get('OwnerId')) : null);
                                // Initialize dynamic class instance
                                MRO_ACT_IAssignmentRuleApexAction arApexAction = (MRO_ACT_IAssignmentRuleApexAction)
                                        Type.forName(assignmentRuleConfiguration.ApexClassName__c).newInstance();
                                // Execute dynamic method
                                ownerId = assignmentRuleConfiguration.AssigneeType__c == 'Queue' ?
                                        queueDeveloperNameToQueueIdMap.get((String)arApexAction.execute(params)) :
                                        (Id)arApexAction.execute(params);
                            }
                            if(String.isNotBlank(ownerId)) {

                                // Set new owner id
                                sObj.put('OwnerId', ownerId);
                                // Build list of related sObjects to be updated/reassigned
                                relatedSObjectsToUpdate.addAll(
                                        getRelatedSObjectToUpdate(sObj, assignmentRuleConfiguration, ownerId));
                            }
                        }
                    }
                    if(!relatedSObjectsToUpdate.isEmpty()) {

                        update relatedSObjectsToUpdate;
                    }
                }
            }
        } catch (Exception ex) {

            System.debug('MRO_SRV_AssignmentRuleConfiguration.applyAssignmentRuleLogic - Exception ' +
                    ex.getMessage() + ' ' + ex.getStackTraceString());
            MRO_SRV_ExecutionLog.createDebugLog(
                    'Apex Exception',
                    'Error Applying Assignment Rules',
                    ex.getMessage(),
                    ex.getStackTraceString(),
                    'MRO_SRV_AssignmentRuleConfiguration.applyAssignmentRuleLogic',
                    filteredSObjects != null && !filteredSObjects.isEmpty() ? filteredSObjects[0].Id : null
            );
        }
    }

    /**
     *
     * @param newSObjects
     * @param oldSObjectMap
     * @param triggeringFieldName
     * @return
     */
    public List<SObject> getSObjectToEvaluate(List<SObject> newSObjects, Map<Id, SObject> oldSObjectMap,
        String triggeringFieldName) {

        List<SObject> sObjectToEvaluate = new List<SObject>();
        if(oldSObjectMap == null) {

            sObjectToEvaluate = newSObjects;
        } else {

            for(SObject sObj : newSObjects) {

                if((String)sObj.get(triggeringFieldName) != (String)oldSObjectMap.get(sObj.Id).get(triggeringFieldName)) {

                    sObjectToEvaluate.add(sObj);
                }
            }
        }
        return sObjectToEvaluate;
    }

    public Map<String, Id> getQueueDeveloperNameToQueueIdMap(List<AssignmentRuleConfiguration__mdt> assignmentRuleConfigurations) {

        Map<String, Id> queueDeveloperNameToQueueIdMap = new Map<String, Id>();
        Set<String> queueDeveloperNames = new Set<String>();
        Set<String> apexClassNames = new Set<String>();
        for(AssignmentRuleConfiguration__mdt assignmentRuleConfiguration : assignmentRuleConfigurations) {

            if(assignmentRuleConfiguration.AssigneeType__c == 'Queue' &&
                    String.isNotBlank(assignmentRuleConfiguration.Assignee__c)) {

                queueDeveloperNames.add(assignmentRuleConfiguration.Assignee__c);
            }
            if(assignmentRuleConfiguration.AssigneeType__c == 'Queue' &&
                    String.isNotBlank(assignmentRuleConfiguration.ApexClassName__c)) {

                apexClassNames.add(assignmentRuleConfiguration.ApexClassName__c);
            }
        }
        for(String apexClassName : apexClassNames) {

            // Initialize dynamic class instance
            MRO_ACT_IAssignmentRuleApexAction arApexAction = (MRO_ACT_IAssignmentRuleApexAction)
                    Type.forName(apexClassName).newInstance();
            queueDeveloperNames.addAll(arApexAction.getQueueDeveloperNames());
        }
        for(Group queue : userQry.getQueueByDeveloperName(queueDeveloperNames)) {

            queueDeveloperNameToQueueIdMap.put(queue.DeveloperName, queue.Id);
        }
        return queueDeveloperNameToQueueIdMap;
    }

    /**
     *
     * @param sObj
     * @param assignmentRuleConfiguration
     * @param ownerId
     * @return
     */
    public List<SObject> getRelatedSObjectToUpdate(SObject sObj,
            AssignmentRuleConfiguration__mdt assignmentRuleConfiguration, String ownerId) {

        List<SObject> relatedSObjectToUpdate = new List<SObject>();
        // Check if related sObject to be assigned have been configured
        if(String.isNotBlank(assignmentRuleConfiguration.RelatedSObjectsToAssignFieldNames__c)) {

            for(String fieldName : (Set<String>)JSON.deserialize(
                    assignmentRuleConfiguration.RelatedSObjectsToAssignFieldNames__c, Set<String>.class)) {

                if((Id)sObj.get(fieldName) != null) {

                    SObject sObjToUpdate = ((Id)sObj.get(fieldName)).getSObjectType().newSObject((Id)sObj.get(fieldName));
                    sObjToUpdate.put('OwnerId', ownerId);
                    relatedSObjectToUpdate.add(sObjToUpdate);
                }
            }
        }
        return relatedSObjectToUpdate;
    }

    /**
     *
     * @param sObjects
     * @param triggeringFieldName
     * @return
     */
    public Map<Id, AssignmentRuleConfiguration__mdt> getSObjectIdToMatchedAssignmentRuleConfigurationMap(List<SObject> sObjects,
            String triggeringFieldName) {

        Map<Id, AssignmentRuleConfiguration__mdt> sObjectIdToMatchedAssignmentRuleConfigurationMap =
                new Map<Id, AssignmentRuleConfiguration__mdt>();

        // Get candidate assignment rule configuration
        List<AssignmentRuleConfiguration__mdt> candidateAssignmentRuleConfigurations =
                getCandidateAssignmentRuleConfigurations(sObjects, triggeringFieldName);

        // Query all user owners with all needed fields
        Set<String> userQueryFields = getUserQueryFields(candidateAssignmentRuleConfigurations);
        Set<String> userIds = new Set<String>();
        for(SObject sobj : sObjects) {

            if(isUser((String)sobj.get('OwnerId'))) {

                userIds.add((String)sobj.get('OwnerId'));
            }
        }

        userMap.putAll(userQry.getUsers(userIds, userQueryFields));

        for(SObject sobj : sObjects) {

            for(AssignmentRuleConfiguration__mdt assignmentRuleConfiguration : candidateAssignmentRuleConfigurations) {

                User owner = isUser((String)sobj.get('OwnerId')) && userMap.containsKey((Id)sobj.get('OwnerId'))?
                        userMap.get((Id)sobj.get('OwnerId')) : null;
                if(match(sobj, owner, assignmentRuleConfiguration)) {

                    sObjectIdToMatchedAssignmentRuleConfigurationMap.put(sobj.Id, assignmentRuleConfiguration);
                    break;
                }
            }
        }

        return sObjectIdToMatchedAssignmentRuleConfigurationMap;
    }

    /**
     *
     * @param ownerId
     * @return
     */
    public Boolean isUser(String ownerId) {

        return ((Id)ownerId).getSobjectType().getDescribe().getName() == Schema.SObjectType.User.name;
    }

    /**
     *
     * @param sObjects
     * @param triggeringFieldName
     * @return
     */
    public List<AssignmentRuleConfiguration__mdt> getCandidateAssignmentRuleConfigurations(List<SObject> sObjects,
            String triggeringFieldName) {

        List<AssignmentRuleConfiguration__mdt> assignmentRuleConfigurations = null;
        if(!sObjects.isEmpty()) {

            String sObjectName = sObjects[0].getSobjectType().getDescribe().getName();
            Set<String> sObjectTriggeringFieldValues = new Set<String>();
            for(SObject sobj : sObjects) {

                sObjectTriggeringFieldValues.add((String) sobj.get(triggeringFieldName));
            }
            if(!sObjects[0].getSobjectType().getDescribe().fields.getMap().keySet().contains('RecordTypeId') ||
                    sObjects[0].get('RecordTypeId') == null) {

                assignmentRuleConfigurations = assignmentRuleConfigurationQry.getAssignmentRuleConfigurations(
                    sObjectName, triggeringFieldName, sObjectTriggeringFieldValues);
            } else {

                Set<String> sObjectRecordTypeDeveloperNames = new Set<String>();
                for(SObject sobj : sObjects) {

                    sObjectRecordTypeDeveloperNames.add(Schema.getGlobalDescribe().get(sObjectName).getDescribe()
                            .getRecordTypeInfosById().get((String)sobj.get('RecordTypeId')).getDeveloperName());
                }

                for(AssignmentRuleConfiguration__mdt arc : assignmentRuleConfigurationQry.getAssignmentRuleConfigurations(
                        sObjectName, triggeringFieldName, sObjectTriggeringFieldValues)) {

                    if(String.isNotBlank(arc.SObjectRecordTypeDeveloperName__c)) {

                        for (String sObjectRecordTypeDeveloperName : sObjectRecordTypeDeveloperNames) {

                            if (arc.SObjectRecordTypeDeveloperName__c.split(';').contains(sObjectRecordTypeDeveloperName)) {

                                assignmentRuleConfigurations.add(arc);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return assignmentRuleConfigurations;
    }

    /**
     *
     * @param assignmentRuleConfigurations
     * @return
     */
    public Set<String> getSObjectQueryFields(List<AssignmentRuleConfiguration__mdt> assignmentRuleConfigurations) {

        Set<String> sObjectQueryFields = new Set<String>();
        sObjectQueryFields.add('Id');
        sObjectQueryFields.add('RecordTypeId');
        for(AssignmentRuleConfiguration__mdt assignmentRuleConfiguration : assignmentRuleConfigurations) {

            sObjectQueryFields.add(assignmentRuleConfiguration.SObjectTriggeringFieldName__c);
            if( String.isNotBlank(assignmentRuleConfiguration.SObjectAdditionalTriggeringConditions__c)) {

                Map<String, String> fieldNameToFieldValueMap = (Map<String, String>)
                        JSON.deserializeUntyped(assignmentRuleConfiguration.SObjectAdditionalTriggeringConditions__c);
                sObjectQueryFields.addAll(fieldNameToFieldValueMap.keySet());
            }
        }
        return sObjectQueryFields;
    }

    /**
     *
     * @param assignmentRuleConfigurations
     * @return
     */
    public Set<String> getUserQueryFields(List<AssignmentRuleConfiguration__mdt> assignmentRuleConfigurations) {

        Set<String> userQueryFields = new Set<String>();
        userQueryFields.add('Id');
        for(AssignmentRuleConfiguration__mdt assignmentRuleConfiguration : assignmentRuleConfigurations) {

            if( String.isNotBlank(assignmentRuleConfiguration.OwnerTriggeringConditions__c)) {

                Map<String, Object> fieldNameToFieldValueMap = (Map<String, Object>)
                        JSON.deserializeUntyped(assignmentRuleConfiguration.OwnerTriggeringConditions__c);
                userQueryFields.addAll(fieldNameToFieldValueMap.keySet());
            }
        }
        return userQueryFields;
    }

    /**
     *
     * @param sObj
     * @param assignmentRuleConfiguration
     * @return
     */
    public Boolean match(SObject sObj, AssignmentRuleConfiguration__mdt assignmentRuleConfiguration) {

        return match(sObj, null, assignmentRuleConfiguration);
    }

    /**
     *
     * @param sObj
     * @param owner
     * @param assignmentRuleConfiguration
     * @return
     */
    public Boolean match(SObject sObj, User owner, AssignmentRuleConfiguration__mdt assignmentRuleConfiguration) {

        try {

            // Check sObject Name
            if( sObj.getSobjectType().getDescribe().getName() != assignmentRuleConfiguration.SObjectName__c) {

                return false;
            }
            // Check record type
            if( String.isNotBlank(assignmentRuleConfiguration.SObjectRecordTypeDeveloperName__c)) {

                if( sObj.get('RecordTypeId') == null ||
                    !getRecordTypeIds(assignmentRuleConfiguration.SObjectName__c,
                            assignmentRuleConfiguration.SObjectRecordTypeDeveloperName__c, ';')
                                    .contains((Id)sObj.get('RecordTypeId'))) {

                    return false;
                }
            }
            // Check sObject triggering field value
            if( String.isNotBlank(assignmentRuleConfiguration.SObjectTriggeringFieldName__c) &&
                    (String)sObj.get(assignmentRuleConfiguration.SObjectTriggeringFieldName__c) !=
                            assignmentRuleConfiguration.SObjectTriggeringFieldValue__c) {

                return false;
            }
            // Check additional sObject conditions
            if( String.isNotBlank(assignmentRuleConfiguration.SObjectAdditionalTriggeringConditions__c)) {

                Map<String, Object> fieldNameToFieldValueMap = (Map<String, Object>)
                    JSON.deserializeUntyped(assignmentRuleConfiguration.SObjectAdditionalTriggeringConditions__c);
                for(String fieldName : fieldNameToFieldValueMap.keySet()) {

                    if( !((String)fieldNameToFieldValueMap.get(fieldName))
                                .split(';').contains((String)sObj.get(fieldName)) &&
                        (String)sObj.get(fieldName) != (String)fieldNameToFieldValueMap.get(fieldName)) {

                        return false;
                    }
                }
            }
            // Check additional owner conditions
            if( String.isNotBlank(assignmentRuleConfiguration.OwnerTriggeringConditions__c)) {

                Map<String, Object> fieldNameToFieldValueMap = (Map<String, Object>)
                        JSON.deserializeUntyped(assignmentRuleConfiguration.OwnerTriggeringConditions__c);
                for(String fieldName : fieldNameToFieldValueMap.keySet()) {

                    if(!((String)fieldNameToFieldValueMap.get(fieldName))
                            .split(';').contains((String)owner.get(fieldName)) &&
                            (String)owner.get(fieldName) != (String)fieldNameToFieldValueMap.get(fieldName)) {

                        return false;
                    }
                }
            }

        } catch (Exception ex) {

            System.debug('MRO_SRV_AssignmentRuleConfiguration.match: ' + ex.getMessage() + ' ' + ex.getStackTraceString());
            return false;
        }
        return true;
    }

    private Set<Id> getRecordTypeIds(String sObjectName, String recordTypeDeveloperNames, String separator) {

        Set<Id> recordTypeIds = new Set<Id>();
        if(String.isNotBlank(recordTypeDeveloperNames) && String.isNotBlank(separator)) {

            for (String recordTypeDeveloperName : recordTypeDeveloperNames.split(separator)) {

                recordTypeIds.add(Schema.getGlobalDescribe().get(sObjectName).getDescribe()
                        .getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName)
                        .getRecordTypeId());
            }
        }
        return recordTypeIds;
    }
}