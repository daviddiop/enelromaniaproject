public with sharing class MRO_LC_InvoiceDuplicate extends ApexServiceLibraryCnt {

    private static DossierService dossierSrv = DossierService.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static BillingProfileQueries billingProfileQuery = BillingProfileQueries.getInstance();
    private static final InteractionService interactionSrv = InteractionService.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();
    //private static String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('GenericRequest').getRecordTypeId();
    private static String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
    private static String caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('InvoiceDuplicate').getRecordTypeId();
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();
    private static String dossierDraftStatus = constantsSrv.DOSSIER_STATUS_DRAFT;
    private static String dossierNewStatus = constantsSrv.DOSSIER_STATUS_NEW;
    private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_FileMetadata fileMetadataQuery = MRO_QR_FileMetadata.getInstance();
    private static MRO_SRV_FileMetadata fileMetadataSrv = MRO_SRV_FileMetadata.getInstance();

    public with sharing class init extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String templateId = params.get('templateId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }



            Account acc = accountQuery.findAccount(accountId);
            Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, null, dossierRecordTypeId, 'InvoiceDuplicate');
            dossier = dossierQuery.getById(dossier.Id);
            if(String.isBlank(dossier.Parent__c) && String.isNotBlank(genericRequestId)){
                dossierSr.updateParent(dossier.Id, genericRequestId);
                dossier.Parent__c = genericRequestId;
            }
            String code = 'InvoiceDuplicateTemplate_1';
            ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
            if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                response.put('templateId', scriptTemplate.Id);
            }

            response.put('dossier' , dossier);
            response.put('dossierId', dossier.Id);
            response.put('genericRequestId', dossier.Parent__c);
            response.put('account' , acc);
            response.put('channelOptionList' , interactionSrv.listOptions(Dossier__c.SendingChannel__c));
            response.put('error', false);

                return response; /*new Map<String, Object>{
                        'dossier' => dossier,
                        'account' => acc,
                        'channelOptionList' => interactionSrv.listOptions(Dossier__c.SendingChannel__c)
                };*/
        }
    }

    public with sharing class findContractAccount extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');

            if (String.isBlank(supplyId)) {
                throw new WrtsException(System.Label.Supply + ' - ' + System.Label.MissingId);
            }

            Supply__c theSupply = supplyQuery.getById(supplyId);
            if (theSupply == null || String.isBlank(theSupply.ContractAccount__c)) {
                throw new WrtsException(String.format(Label.NotFound, new List<Object>{
                        Label.ContractAccount
                }));
            }
            ContractAccount__c contractAccount = contractAccountQuery.getById(theSupply.ContractAccount__c);
            BillingProfile__c billingProfile;
            if (contractAccount != null && String.isNotBlank(contractAccount.BillingProfile__c)) {
                billingProfile = billingProfileQuery.getById(contractAccount.BillingProfile__c);
            }
            return new Map<String, Object>{
                    'contractAccountId' => theSupply.ContractAccount__c,
                    'contractAccount' => contractAccount,
                    'billingProfile' => billingProfile
            };
        }
    }

    public with sharing class saveCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String accountId = params.get('accountId');
            String contractAccountId = params.get('contractAccountId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            String markedInvoicesJson = params.get('invoiceIds');
            String invoiceList = params.get('invoiceList');
            String fileNetIds = params.get('fileNetIds');
            String addressEmail = params.get('addressEmail');
            String sendingChannel = params.get('sendingChannel');
            String companyDivisionId =params.get('companyDivisionId');

            if (String.isBlank(dossierId)) {
                throw new WrtsException(System.Label.Dossier + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(contractAccountId)) {
                throw new WrtsException(System.Label.ContractAccount + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(markedInvoicesJson)) {
                throw new WrtsException(System.Label.InvoiceId + ' - ' + System.Label.MissingId);
            }
            List<MRO_LC_InvoiceDuplicate.Invoice> listInvoices = (List<MRO_LC_InvoiceDuplicate.Invoice>)JSON.deserialize(invoiceList, List<MRO_LC_InvoiceDuplicate.Invoice>.class);
            List<String> markedInvoiceIds = (List<String>)JSON.deserialize(markedInvoicesJson, List<String>.class);
            List<String> fileNetIdsList = (List<String>)JSON.deserialize(fileNetIds, List<String>.class);
            String invoiceIdsString = String.join(markedInvoiceIds, ',').removeEnd(',');
            System.debug('addressEmail'+addressEmail);
            Case newCase = caseSrv.insertCase(
                    companyDivisionId,
                    caseRecordTypeId,
                    dossierId,
                    accountId,
                    contractAccountId,
                    invoiceIdsString,
                    addressEmail,
                    channelSelected,
                    originSelected
            );
            List<Case> listCases = new List<Case>();
            if(newCase != null){
                listCases.add(newCase);
            }
            Dossier__c updatedDossier = dossierSr.updateDossierWithChannelAndStatusAndSending(dossierId, channelSelected, originSelected, dossierNewStatus, sendingChannel);
           if(String.isNotBlank(updatedDossier.Id) && String.isNotBlank(companyDivisionId)){
               dossierSr.updateDossierCompanyDivision(updatedDossier.Id,companyDivisionId);
           }
            List<DocumentType__c> docType = fileMetadataQuery.getDocumentTypes('Invoice');
            String idDocType = '';
            if(!docType.isEmpty()){
                idDocType = docType[0].Id;
            }

            if (!listInvoices.isEmpty()) {
                fileMetadataSrv.insertFileMetadaForInvoiceAndFisa('Invoice',listInvoices,idDocType,updatedDossier, listCases);
            }
            caseSrv.applyAutomaticTransitionOnDossierAndCases(updatedDossier.Id);
            return new Map<String, Object>{
                'case' => JSON.serialize(newCase)
            };
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            String companyDivisionId = params.get('companyDivisionId');
            if (String.isBlank(dossierId)) {
                throw new WrtsException(System.Label.Dossier + ' - ' + System.Label.MissingId);
            }
            caseSrv.setCanceledOnCaseAndDossier(new List<Case>(), dossierId);
            dossierSr.updateDossierCompanyDivisionWithChannel(dossierId, companyDivisionId, channelSelected, originSelected);
            response.put('error', false);
            return response;
        }
    }

    /**
    * @author  DAVID DIOP
    * @description update dossier
    * @date 20/01/2020
    * @param DossierId,channelSelected,originSelected
    *
    */
    public class UpdateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            String sendingChannel = params.get('sendingChannel');
            System.debug('sendingChannel'+sendingChannel);
            dossierSr.updateDossierWithChannelAndSending(dossierId,channelSelected,originSelected,dossierDraftStatus);
            response.put('error', false);
            return response;
        }
    }

    public class getValidDate extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Date startDate =  (Date) JSON.deserialize(params.get('startDate'), Date.class);
            Date endDate =  (Date) JSON.deserialize(params.get('endDate'), Date.class);
            Boolean startDateValid = MRO_UTL_Date.isNotWorkingDate(startDate);
            Boolean endDateValid = MRO_UTL_Date.isNotWorkingDate(endDate);
            response.put('error', false);
            response.put('startDateValid', startDateValid);
            response.put('endDateValid', endDateValid);
            return response;
        }
    }

    public class Invoice {
        @AuraEnabled
        public String invoiceId { get; set; }
        @AuraEnabled
        public String invoiceNumber { get; set; }
        @AuraEnabled
        public String invoiceSerialNumber { get; set; }
        @AuraEnabled
        public String sapStatus { get; set; }
        @AuraEnabled
        public String invoiceType { get; set; }
        @AuraEnabled
        public Date invoiceIssueDate { get; set; }
        @AuraEnabled
        public Date invoiceDate { get; set; }
        @AuraEnabled
        public Double total { get; set; }
        @AuraEnabled
        public String fileNetId { get; set; }
        @AuraEnabled
        public Double tva { get; set; }
    }

}