/**
 * Created by tomasoni on 07/08/2019.
 */

public with sharing class ContractAccountCnt extends ApexServiceLibraryCnt {
    static ContractAccountQueries contractAccountQuery = ContractAccountQueries.getInstance();
    static SupplyQueries supplyQuery = SupplyQueries.getInstance();

    public with sharing class getActiveSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String contractAccountRecordId = params.get('contractAccountId');
            if (String.isBlank(contractAccountRecordId)) {
                throw new WrtsException(System.Label.ContractAccount + ' - ' + System.Label.MissingId);
            }

            List<Supply__c> supply = supplyQuery.getActiveSuppliesByContractAccount(contractAccountRecordId);
            response.put('enableEditButtonCA', supply.isEmpty());
            return response;
        }
    }

    public with sharing class getContractAccountRecords extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String companyDivisionId=params.get('companyDivisionId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(companyDivisionId)) {
                throw new WrtsException(System.Label.CompanyDivision + ' - ' + System.Label.MissingId);
            }
            List<ContractAccount__c> listContractAccount = contractAccountQuery.getByAccountAndCompanyDivisionId(accountId,companyDivisionId);
            response.put('listContractAccount', listContractAccount);
            return response;
        }
    }
}