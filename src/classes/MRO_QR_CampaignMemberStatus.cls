/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 23, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_CampaignMemberStatus {

    public static MRO_QR_CampaignMemberStatus getInstance() {
        return (MRO_QR_CampaignMemberStatus) ServiceLocator.getInstance(MRO_QR_CampaignMemberStatus.class);
    }

    public Map<Id, List<CampaignMemberStatus>> getCampaignMemberStatusesByCampaignIds(Set<Id> campaignIds) {
        List<CampaignMemberStatus> campaignMemberStatuses = [SELECT Id, CampaignId, Label, IsDefault, HasResponded, SortOrder
                                                             FROM CampaignMemberStatus WHERE CampaignId IN :campaignIds
                                                             ORDER BY CampaignId, SortOrder];
        Map<Id, List<CampaignMemberStatus>> result = new Map<Id, List<CampaignMemberStatus>>();
        for (CampaignMemberStatus cms : campaignMemberStatuses) {
            if (result.containsKey(cms.CampaignId)) {
                result.get(cms.CampaignId).add(cms);
            }
            else {
                result.put(cms.CampaignId, new List<CampaignMemberStatus>{cms});
            }
        }
        return result;
    }
}