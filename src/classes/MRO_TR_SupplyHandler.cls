/**
* Trigger handler class for Supply Object
* @author Guerini Ivano
*
*/
public with sharing class MRO_TR_SupplyHandler implements TriggerManager.ISObjectTriggerHandler {
    private static MRO_SRV_Supply supplySrv = MRO_SRV_Supply.getInstance();
    private static MRO_SRV_ServiceSite serviceSiteSrv = MRO_SRV_ServiceSite.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();
    private static final Set<String> ACTIVE_SUPPLY_STATUSES = new Set<String>{CONSTANTS.SUPPLY_STATUS_ACTIVE, CONSTANTS.SUPPLY_STATUS_TERMINATING};

    public void beforeInsert() {}
    public void beforeDelete() {}
    public void beforeUpdate() {}

    public void afterInsert() {
        Set<Id> servicePointInvolved = new Set<Id>();
        Set<Id> serviceSiteIdsToUpdate = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.new) {
            if (supply.ServicePoint__c != null) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
            if (ACTIVE_SUPPLY_STATUSES.contains(supply.Status__c) && supply.ServiceSite__c != null) {
                serviceSiteIdsToUpdate.add(supply.ServiceSite__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
        if (!serviceSiteIdsToUpdate.isEmpty()) {
            serviceSiteSrv.updateActiveSuppliesCounts(serviceSiteIdsToUpdate);
        }
    }

    public void afterUpdate() {
        Set<Id> servicePointInvolved = new Set<Id>();
        Set<Id> serviceSiteIdsToUpdate = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.new) {
            Supply__c oldSupply = (Supply__c) Trigger.oldMap.get(supply.Id);
            if ( (supply.ServicePoint__c != null)
              && ( (supply.Status__c != ((Supply__c)Trigger.oldMap.get(supply.Id)).Status__c)
              || (supply.ServicePoint__c != ((Supply__c)Trigger.oldMap.get(supply.Id)).ServicePoint__c) )
               ) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
            if (supply.ServiceSite__c != oldSupply.ServiceSite__c) {
                if (supply.ServiceSite__c != null) {
                    serviceSiteIdsToUpdate.add(supply.ServiceSite__c);
                }
                if (oldSupply.ServiceSite__c != null) {
                    serviceSiteIdsToUpdate.add(oldSupply.ServiceSite__c);
                }
            }
            else if (supply.ServiceSite__c != null && supply.Status__c != oldSupply.Status__c &&
                    (ACTIVE_SUPPLY_STATUSES.contains(supply.Status__c) ^ ACTIVE_SUPPLY_STATUSES.contains(oldSupply.Status__c))) {
                serviceSiteIdsToUpdate.add(supply.ServiceSite__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
        System.debug('### serviceSiteIdsToUpdate: '+serviceSiteIdsToUpdate);
        if (!serviceSiteIdsToUpdate.isEmpty()) {
            serviceSiteSrv.updateActiveSuppliesCounts(serviceSiteIdsToUpdate);
        }
    }

    public void afterDelete() {
        Set<Id> servicePointInvolved = new Set<Id>();
        Set<Id> serviceSiteIdsToUpdate = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.old) {
            if (supply.ServicePoint__c != null) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
            if (ACTIVE_SUPPLY_STATUSES.contains(supply.Status__c) && supply.ServiceSite__c != null) {
                serviceSiteIdsToUpdate.add(supply.ServiceSite__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
        if (!serviceSiteIdsToUpdate.isEmpty()) {
            serviceSiteSrv.updateActiveSuppliesCounts(serviceSiteIdsToUpdate);
        }
    }

    public void afterUndelete() {
        Set<Id> servicePointInvolved = new Set<Id>();
        Set<Id> serviceSiteIdsToUpdate = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.new) {
            if (supply.ServicePoint__c != null) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
            if (ACTIVE_SUPPLY_STATUSES.contains(supply.Status__c) && supply.ServiceSite__c != null) {
                serviceSiteIdsToUpdate.add(supply.ServiceSite__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
        if (!serviceSiteIdsToUpdate.isEmpty()) {
            serviceSiteSrv.updateActiveSuppliesCounts(serviceSiteIdsToUpdate);
        }
    }
}