/**
 *
 *
 * @author Salvatore Pignanelli
 * @version 1.0
 * @description
 * @history
 * 01/12/2020 : Salvatore Pignanelli
*/
public with sharing class MRO_QR_ContentVersion {

    public static MRO_QR_ContentVersion getInstance() {
        return (MRO_QR_ContentVersion)ServiceLocator.getInstance(MRO_QR_ContentVersion.class);
    }

    public List<ContentVersion> findById (List<String> contentVersionId) {

        return [
                SELECT Id, ContentDocumentId
                FROM ContentVersion
                WHERE Id = :contentVersionId
        ];
    }
}