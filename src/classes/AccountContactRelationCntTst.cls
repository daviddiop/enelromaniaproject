@isTest
public with sharing class AccountContactRelationCntTst {
    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new List<Account>();
        String vat = TestDataFactory.CreateFakeVatNumber();
        String NationalIdentityNumber = TestDataFactory.CreateFakeNationalIdentityNumber();
        listAccount.add(TestDataFactory.account().personAccount().build());
        listAccount.add(TestDataFactory.account().businessAccount().build());
        Account account = TestDataFactory.account().setName('BusinessName').build();
        Account accountKey = TestDataFactory.account().setKey('1324433').build();
        Account accountVATNumber = TestDataFactory.account().setVATNumber(vat).build();
        Account account2 = TestDataFactory.account().businessAccount().build();
        account2.Name = account.Name;
        account2.Key__c = accountKey.Key__c;
        account2.VATNumber__c= accountVATNumber.VATNumber__c;
        listAccount.add(account2);
        insert listAccount;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        insert interaction;

        Individual individual = TestDataFactory.individual().createIndividual().build();
        insert individual;

        Contact contact = TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        contact.IndividualId = individual.Id;
        insert contact;

        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[1].Id, contact.Id).build();
        insert customerInteraction;

        AccountContactRelation accountContactRelation = TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id, listAccount[0].Id).build();
        insert accountContactRelation;
    }

    @isTest
    static void listAccountContactRelationByAccountIdTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('AccountContactRelationCnt', 'listAccountContactRelationByAccountId', new Map<String, String>{
                'accountId' => account.Id
        }, true);
        Map<String, Object> mapOptions = (Map<String, Object>) response;
        System.assert(mapOptions.size() == 2);
        system.assert(mapOptions.get('account') != null);
        Test.stopTest();
    }

    @isTest
    static void listAccountContactRelationByAccountIdTestIsBlankAccountId() {
        String accoundId2 = null;
        Test.startTest();
        try {
            Object response = TestUtils.exec('AccountContactRelationCnt', 'listAccountContactRelationByAccountId', new Map<String, String>{
                'accountId' => accoundId2
            }, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void listAccountContactRelationByIndividualIdTest() {
        Individual individual = [
                SELECT Id
                FROM Individual
                LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('AccountContactRelationCnt', 'listAccountContactRelationByIndividualId', new Map<String, String>{
                'individualId' => individual.Id
        }, true);
        Map<String, Object> mapOptions = (Map<String, Object>) response;
        System.assert(((Individual) mapOptions.get('individual')).Id == individual.Id);
        Test.stopTest();
    }

    @isTest
    static void listAccountContactRelationByIndividualIdTestIsBlankIndividualId() {
        String individualId = null;
        Test.startTest();
        try {
            Object response = TestUtils.exec('AccountContactRelationCnt', 'listAccountContactRelationByIndividualId', individualId, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }


    @isTest
    static void assignContactToInteractionTest() {
        Contact contact = TestDataFactory.contact().createContact().build();
        insert contact;
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('AccountContactRelationCnt', 'assignContactToInteraction', new Map<String, String>{
                'contactId' => contact.Id, 'customerInteractionId' => customerInteraction.Id
        }, true);
        Boolean mapOptions = (Boolean) response;
        System.assert(mapOptions == true);
        CustomerInteraction__c customerInteractionUpdate = [
                SELECT Id,Contact__c
                FROM CustomerInteraction__c
        ];
        System.assert(customerInteractionUpdate.Contact__c == contact.Id);
        Test.stopTest();
    }

    @isTest
    static void assignContactToInteractionTest2() {
        Individual myIndividual = TestDataFactory.individual().createIndividual().build();
        insert myIndividual;
        Contact myContact = TestDataFactory.contact().createContact().setIndividual(myIndividual.Id).build();
        insert myContact;
        CustomerInteraction__c myCustomerInteraction = [
            SELECT Id, Interaction__c
            FROM CustomerInteraction__c
            LIMIT 1
        ];

        Test.startTest();
        Boolean result = (Boolean)TestUtils.exec('AccountContactRelationCnt', 'assignContactToInteraction',
            new Map<String, String>{'contactId' => myContact.Id, 'customerInteractionId' => myCustomerInteraction.Id}, true);
        Test.stopTest();

        System.assert(result);
        Interaction__c myInteraction = [
            SELECT Id, Interlocutor__c
            FROM Interaction__c
            WHERE Id = :myCustomerInteraction.Interaction__c
        ];
        System.assertEquals(myIndividual.Id, myInteraction.Interlocutor__c);
    }

    @isTest
    static void assignContactToInteractionTestIsBlankContactId() {
        Test.startTest();
        String contactId = null;
        String customerInteractionId2 = null;
        try {
            Object response = TestUtils.exec('AccountContactRelationCnt', 'assignContactToInteraction', new Map<String, String>{
                'contactId' => contactId, 'customerInteractionId' => customerInteractionId2
            }, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void assignContactToInteractionTestIsBlankCustomerInteractionId() {
        Contact contact = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        Test.startTest();
        String customerInteractionId2 = null;
        try {
            Object response = TestUtils.exec('AccountContactRelationCnt', 'assignContactToInteraction', new Map<String, String>{
                'contactId' => contact.Id, 'customerInteractionId' => customerInteractionId2
            }, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void addCustomerInteractionTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                WHERE Name = :'BusinessName'
                LIMIT 1
        ];
        Contact contact = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.contactId = contact.Id;
        customerInteractionDTO.customerId = account.Id;
        customerInteractionDTO.interactionId = interaction.Id;
        Test.startTest();
        Object response = TestUtils.exec('AccountContactRelationCnt', 'addCustomerInteraction', customerInteractionDTO, true);
        Map<String, Object> mapOptions = (Map<String, Object>) response;
        System.assert(((CustomerInteraction__c) mapOptions.get('customerInteraction')).Id != null);
        List<CustomerInteraction__c> listCustomerInteractions = [
                SELECT Id
                FROM CustomerInteraction__c
        ];
        System.assert(listCustomerInteractions.size() == 2);
        Test.stopTest();

    }

    @isTest
    static void addCustomerInteractionTestIsBlankInteractionId() {
        Account account = [
                SELECT Id,Name
                FROM Account
                WHERE Name = :'BusinessName'
                LIMIT 1
        ];
        Contact contact = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.contactId = contact.Id;
        customerInteractionDTO.customerId = account.Id;
        customerInteractionDTO.interactionId = null;
        Test.startTest();
        try {
            Object response = TestUtils.exec('AccountContactRelationCnt', 'addCustomerInteraction', customerInteractionDTO, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void addCustomerInteractionTestIsBlankCustomerId() {
        Contact contact = [
                SELECT Id
                FROM Contact
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.contactId = contact.Id;
        customerInteractionDTO.customerId = null;
        customerInteractionDTO.interactionId = interaction.Id;
        Test.startTest();
        try {
            Object response = TestUtils.exec('AccountContactRelationCnt', 'addCustomerInteraction', customerInteractionDTO, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();

    }

    @isTest
    static void addCustomerInteractionTestIsBlankContactId() {
        Account account = [
                SELECT Id,Name
                FROM Account
                WHERE Name = :'BusinessName'
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.contactId = null;
        customerInteractionDTO.customerId = account.Id;
        customerInteractionDTO.interactionId = interaction.Id;
        Test.startTest();
        try {
            Object response = TestUtils.exec('AccountContactRelationCnt', 'addCustomerInteraction', customerInteractionDTO, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
}