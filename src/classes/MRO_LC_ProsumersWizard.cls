/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 02.03.20.
 */

public with sharing class MRO_LC_ProsumersWizard extends ApexServiceLibraryCnt {

    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();

    private static MRO_QR_Opportunity opportunityQuery = MRO_QR_Opportunity.getInstance();
    private static MRO_SRV_Opportunity oppService = MRO_SRV_Opportunity.getInstance();

    private static MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_SRV_Contract contractSrv = MRO_SRV_Contract.getInstance();
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();

    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Consumption consumptionQuery = MRO_QR_Consumption.getInstance();

    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Acquisition').getRecordTypeId();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    private static List<String> consumptionFieldNamesList = new List<String>{'QuantityJanuary__c', 'QuantityFebruary__c', 'QuantityMarch__c', 'QuantityApril__c', 'QuantityMay__c', 'QuantityJune__c', 'QuantityJuly__c', 'QuantityAugust__c', 'QuantitySeptember__c', 'QuantityOctober__c', 'QuantityNovember__c', 'QuantityDecember__c'};

    private static MRO_QR_User userQuery = MRO_QR_User.getInstance();

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String opportunityId = params.get('opportunityId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestDossierId = params.get('genericRequestDossierId');

            User currentUserInfos = userQuery.getCompanyDivisionId(UserInfo.getUserId());

            if (String.isBlank(accountId)) {
                response.put('error', true);
                response.put('errorMsg', System.Label.Account + ' - ' + System.Label.MissingId);
                return response;
            }

            Account acc = accQuery.findAccount(accountId);

            Id cusId = null;
            List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);

            String interactionOrigin;
            String interactionChannel;
            if (!cus.isEmpty()) {
                cusId = cus[0].Id;
                interactionOrigin = cus[0].Interaction__r.Origin__c;
                interactionChannel = cus[0].Interaction__r.Channel__c;
            }

            Opportunity opp;
            Contract contractRecord;
            if (String.isBlank(opportunityId)) {
                opp = oppService.insertOpportunityByCustomerInteraction(cusId, accountId, 'Prosumers');
                opp.Origin__c = interactionOrigin;
                opp.Channel__c = interactionChannel;
                databaseSrv.upsertSObject(opp);
                opportunityId = opp.Id;
            } else {
                opp = opportunityQuery.getOpportunityById(opportunityId);

                if (opp.ContractId != null) {
                    contractRecord = contractQuery.getById(opp.ContractId);
                    response.put('contract', contractRecord);
                }
            }

            Dossier__c dossier;
            if(dossierId == null) {
                if (genericRequestDossierId != null) {
                    Dossier__c genericRequestDossier = dossierQuery.getById(genericRequestDossierId);
                    dossier = genericRequestDossier.clone();
                    dossier.Parent__c = genericRequestDossierId;
                    opp.Origin__c = dossier.Origin__c;
                    opp.Channel__c = dossier.Channel__c;
                    databaseSrv.upsertSObject(opp);
                }else{
                    dossier = new Dossier__c();
                    dossier.Origin__c = interactionOrigin;
                    dossier.Channel__c = interactionChannel;
                }

                dossier.CustomerInteraction__c = cusId;
                dossier.Opportunity__c = opportunityId;
                dossier.Account__c = accountId;
                dossier.CompanyDivision__c = String.isNotBlank(companyDivisionId) ? companyDivisionId : null;
                dossier.RecordTypeId = dossierRecordType;
                dossier.Status__c = constantsSrv.DOSSIER_STATUS_DRAFT;
                dossier.RequestType__c = 'Prosumers';
                dossier.Commodity__c = constantsSrv.COMMODITY_ELECTRIC;
                databaseSrv.insertSObject(dossier);

            }else{
                dossier = dossierQuery.getById(dossierId);
            }

            List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
            if (cases.size() > 0) {
                Case caseRecord = cases[0];
                response.put('caseRecord', caseRecord);

                if(caseRecord.Supply__c != null && caseRecord.Supply__r.Contract__c != null){
                    response.put('contractType', caseRecord.Supply__r.Contract__r.ContractType__c);
                }
                if (caseRecord.BillingProfile__c != null){
                    Map<String, Object> checkBPResponse = checkBillingProfile(caseRecord, caseRecord.BillingProfile__c);
                    if(!(Boolean)checkBPResponse.get('error')){
                        response.put('billingProfileId', caseRecord.BillingProfile__c);
                    }
                }
                if (caseRecord.Supply__c != null) {
                    Consumption__c consumption = consumptionQuery.getBySupplyAndCase(caseRecord.Supply__c, caseRecord.Id);
                    response.put('consumption', consumption);
                    ConsumptionCalculation calculation = new ConsumptionCalculation(contractRecord, null);
                    response.put('consumptionLabels', calculation.getLabels());
                    if (consumption != null) {
                        response.put('servicePointCode', consumption.Supply__r.ServicePoint__r.Code__c);
                    }
                }
            }

            response.put('opportunityId', opp.Id);
            response.put('opportunity', opp);
            response.put('isClosed', opp.StageName == constantsSrv.OPPORTUNITY_STAGE_QUOTED || opp.StageName == constantsSrv.OPPORTUNITY_STAGE_CLOSED_LOST);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('stage', opp.StageName);
            response.put('error', false);
            response.put('salesSupportUser', currentUserInfos.SalesSupportUser__c);

            return response;
        }
    }
    public class CheckStartDateParams {
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public Date startDate { get; set; }
    }

    public with sharing class CheckStartDate extends AuraCallable {
        public override Object perform(final String jsonInput) {
            // This method checks whether the selected contract start date is minimum today()+5 working days [ENLCRO-1848]
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            CheckStartDateParams checkStartDateParams = (CheckStartDateParams) JSON.deserialize(jsonInput, CheckStartDateParams.class);
            Date selectedDate = checkStartDateParams.startDate;

            Date today = Date.today();
            Date firstAvailableDate = MRO_UTL_Date.addWorkingDays(today, constantsSrv.START_DATE_OFFSET_IN_BUSINESS_DAYS);

            if (selectedDate < firstAvailableDate){
                response.put('error', true);
                response.put('errorMsg', System.Label.ActivationDateMustBeGreaterThanErrorMsg + firstAvailableDate.format());
            }

            return response;
        }
    }
    public class RequestParams {
        @AuraEnabled
        public Id supplyId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
    }

    public with sharing class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams createCaseParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);
            if (createCaseParams.supplyId == null) {
                response.put('error', true);
                response.put('errorMsg', 'No Supplies Selected');
                return response;
            }
            Dossier__c dossier = dossierQuery.getById(createCaseParams.dossierId);
            Supply__c supply = supplyQuery.getSupplyWithServicePointById(createCaseParams.supplyId);
            /*supply.Opportunity__c = createCaseParams.opportunityId;
            databaseSrv.insertSObject(supply);*/

            List<Case> existingCases = [SELECT Id FROM Case WHERE RecordType.DeveloperName = 'Prosumers' AND Status != :constantsSrv.CASE_STATUS_CANCELED AND Supply__r.ServiceSite__c = :supply.ServiceSite__c LIMIT 1];
            if (existingCases.size() > 0) {
                throw new WrtsException('Selected Supply has another open Prosumers process');
            }

            Map<String, String> prosumersRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('Prosumers');

            Case caseRecord = new Case(
                    Supply__c = supply.Id,
                    CompanyDivision__c = supply.CompanyDivision__c,
                    AccountId = dossier.Account__c,
                    AccountName__c = dossier.Account__r.Name,
                    RecordTypeId = (String) prosumersRecordTypes.get('Prosumers'),
                    Dossier__c = createCaseParams.dossierId,
                    Status = constantsSrv.CASE_STATUS_DRAFT,
                    Origin = createCaseParams.originSelected,
                    Channel__c = createCaseParams.channelSelected,
                    Phase__c = constantsSrv.CASE_START_PHASE,
                    ServiceSite__c = supply.ServiceSite__c,
                    Distributor__c = supply.ServicePoint__r.Distributor__c
                    //TmpOldContract__c = supply.Contract__c
            );
            databaseSrv.insertSObject(caseRecord);
            caseRecord = caseQuery.getById(caseRecord.Id);
            response.put('caseRecord', caseRecord);

            if(supply.Contract__c != null){
                response.put('contractType', supply.Contract__r.ContractType__c);
            }

            return response;
        }
    }

    public class ConsumptionParams {
        @AuraEnabled
        public Id caseId { get; set; }
        @AuraEnabled
        public Id contractId { get; set; }
        @AuraEnabled
        public Id consumptionId { get; set; }
    }

    public with sharing class createConsumption extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            ConsumptionParams createConsumptionParams = (ConsumptionParams) JSON.deserialize(jsonInput, ConsumptionParams.class);
            if (createConsumptionParams.caseId == null) {
                response.put('error', true);
                response.put('errorMsg', 'No Cases Selected');
                return response;
            }

            Case caseRecord = caseQuery.getById(createConsumptionParams.caseId);
            Decimal caseAvailablePower = caseRecord.AvailablePower__c;

            Contract contract = contractQuery.getById(createConsumptionParams.contractId);
            ConsumptionCalculation calculation = new ConsumptionCalculation(contract, caseAvailablePower);

            Consumption__c consumption = new Consumption__c(Supply__c = caseRecord.Supply__c, Case__c = caseRecord.Id, Customer__c = caseRecord.AccountId);
            if(createConsumptionParams.consumptionId != null){
                consumption.Id = createConsumptionParams.consumptionId;
            }

            Integer counter = 1;
            for(String fieldName : consumptionFieldNamesList){
                consumption.put(fieldName, calculation.getQuantityForMonth(counter));
                counter++;
            }

            databaseSrv.upsertSObject(consumption);

            response.put('caseRecord', caseRecord);
            response.put('consumption', consumption);
            response.put('consumptionLabels', calculation.getLabels());
            response.put('servicePointCode', caseRecord.Supply__r.ServicePoint__r.Code__c);

            return response;
        }
    }

    public class ConsumptionCalculation{

        public Contract contract;
        public Decimal availablePower;
        private Double constParam = 2.10844072913038;

        ConsumptionCalculation(Contract contract, Decimal availablePower){
            this.contract = contract;
            this.availablePower = availablePower;
        }

        public List<Map<String, String>> getLabels(){
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType consumptionSchema = schemaMap.get('Consumption__c');
            Map<String, Schema.SObjectField> fieldMap = consumptionSchema.getDescribe().fields.getMap();
            List<Map<String, String>> labelsMap = new List<Map<String, String>>();

            Integer offset = this.contract != null && this.contract.StartDate != null ? this.contract.StartDate.month() - 1 : 0;
			Integer offsetYear = this.contract != null && this.contract.StartDate != null ? this.contract.StartDate.year() : 0;
            for(Integer i = 0; i < 12; i++){
                Integer currentMonth = offset + i;
				Integer currentYear = offsetYear;
                if(currentMonth > 11) {
                    currentMonth = currentMonth - 12;
                    currentYear = offsetYear + 1;
                }
                String fieldName = consumptionFieldNamesList.get(currentMonth);
				//labelsMap.add(new Map<String, String>{'name' => fieldName, 'value' => fieldMap.get(fieldName).getDescribe().getLabel()});
                labelsMap.add(new Map<String, String>{'name' => fieldName, 'value' => fieldMap.get(fieldName).getDescribe().getLabel()+' '+currentYear});
            }
            return labelsMap;
        }

        public Double getQuantityForMonth(Integer month){
            Integer daysInMonth = getDays12Months(contract, month);
            return (constParam * availablePower * daysInMonth).setScale(6);
        }

        private Integer getDays12Months(Contract contract, Integer currentMonth){
            if (contract.StartDate == null) {
                return 0;
            }

            Integer yearStart = contract.StartDate.year();
            Integer monthStart = contract.StartDate.month();
            Integer dayStart = contract.StartDate.day();

            if(currentMonth < monthStart){
                yearStart++;
            }
            Integer daysInCurrentMonth = Date.daysInMonth(yearStart, currentMonth);

            Boolean contractStartInCurrentMonth = monthStart == currentMonth;

            if(contractStartInCurrentMonth){
                return daysInCurrentMonth - dayStart + 1;
            }

            return daysInCurrentMonth;
        }

    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams channelOriginParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);
            oppService.setChannelAndOrigin(channelOriginParams.opportunityId, channelOriginParams.dossierId, channelOriginParams.originSelected, channelOriginParams.channelSelected);
            return response;
        }
    }

    public class UpdateContractParams {
        @AuraEnabled
        public String supplyId { get; set; }
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String customerSignedDate { get; set; }
        @AuraEnabled
        public String contractType { get; set; }
        @AuraEnabled
        public String contractName { get; set; }
        @AuraEnabled
        public String companySignedId { get; set; }
        @AuraEnabled
        public String salesman { get; set; }
        @AuraEnabled
        public String salesChannel { get; set; }
        @AuraEnabled
        public String contractStartDate { get; set; }
        @AuraEnabled
        public String salesSupportUser { get; set; }
    }

    public with sharing class updateContractInformation extends AuraCallable {
        public override Object perform(final String jsonInput) {
            UpdateContractParams updateContractParams = (UpdateContractParams) JSON.deserialize(jsonInput, UpdateContractParams.class);
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            Date customerSignedDate = updateContractParams.customerSignedDate != null ? Date.valueOf(updateContractParams.customerSignedDate) : null;
            Date contractStartDate = updateContractParams.contractStartDate != null ? Date.valueOf(updateContractParams.contractStartDate) : null;
            String salesSupportUser = updateContractParams.SalesSupportUser;

            try {

                Opportunity opp = opportunityQuery.getOpportunityById(updateContractParams.opportunityId);

                Contract contract;

                if(updateContractParams.supplyId != null){
                    Supply__c supply = supplyQuery.getById(updateContractParams.supplyId);
                    if(supply != null && supply.Contract__c != null){
                        Contract oldContract = contractQuery.getById(supply.Contract__c);
                        contract = oldContract.clone();
                        contract.Key__c = null;
                        contract.CompanyDivision__c = supply.CompanyDivision__c;
                        contract.RelatedContract__c = oldContract.Id;
                        //contract.CustomerCode__c = null;
                        if(supply.Product__c != null && supply.Product__r.CommercialProduct__c != null) {
                            contract.ContractTerm = (Integer) supply.Product__r.CommercialProduct__r.ProductTerm__c;
                        }
                    }
                }

                if(contract == null){
                    contract = new Contract();
                }

                if (opp.ContractId != null) {
                    contract.Id = opp.ContractId;
                }
                contract.AccountId = opp.AccountId;
                contract.Status = constantsSrv.CASE_STATUS_DRAFT;
                contract.Prosumer__c = true;
                contract.CustomerSignedDate = customerSignedDate;
                contract.Name = updateContractParams.contractName;
                contract.ContractType__c = updateContractParams.contractType;
                contract.StartDate = contractStartDate;
                contract.SalesSupportUser__c = salesSupportUser;
                if (updateContractParams.salesChannel == 'Indirect Sales') {
                    contract.Salesman__c = updateContractParams.salesman;
                } else if (updateContractParams.salesChannel == 'Direct Sales (KAM)') {
                    contract.CompanySignedId = updateContractParams.companySignedId;
                }
                databaseSrv.upsertSObject(contract);

                opp.ContractId = contract.Id;
                opp.ContractType__c = contract.ContractType__c;
                databaseSrv.upsertSObject(opp);
                response.put('contract', contract);

                /*Supply__c supply = [SELECT Id FROM Supply__c WHERE Opportunity__c = :updateContractParams.opportunityId LIMIT 1];
                if(supply != null){
                    supply.Contract__c = contract.Id;
                    databaseSrv.upsertSObject(supply);
                }*/

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class BillingProfileSelectionParams {
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String billingProfileId { get; set; }
    }


    public with sharing class handleBillingProfileSelection extends AuraCallable {
        public override Object perform(final String jsonInput) {
            BillingProfileSelectionParams casParams = (BillingProfileSelectionParams) JSON.deserialize(jsonInput, BillingProfileSelectionParams.class);
            Map<String, Object> response = new Map<String, Object>();

            Case caseRecord = caseQuery.getById(casParams.caseId);

            response = checkBillingProfile(caseRecord, casParams.billingProfileId);
            if((Boolean)response.get('error')){
                return response;
            }

            Savepoint sp = Database.setSavepoint();
            try {
                Case c = new Case(Id = casParams.caseId, BillingProfile__c = casParams.billingProfileId);
                databaseSrv.updateSObject(c);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    private static Map<String, Object> checkBillingProfile(Case caseRecord, String billingProfileId){
        Map<String, Object> response = new Map<String, Object>{'error' => false};

        BillingProfile__c bp = MRO_QR_BillingProfile.getInstance().getById(billingProfileId);
        if(caseRecord.Account.IsPersonAccount && bp.RefundMethod__c != 'Invoice'){
            response.put('error', true);
            response.put('errorMsg', 'For Person Account the Refund Method must be Invoice');
        }else if(!caseRecord.Account.IsPersonAccount && bp.RefundMethod__c != 'Bank Transfer'){
            response.put('error', true);
            response.put('errorMsg', 'For Business Accounts the Refund Method must be Bank Transfer');
        }
        return response;
    }

    public class SaveProcessParams {
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public Boolean isDraft { get; set; }
        @AuraEnabled
        public String salesSupportUser { get; set; }
    }

    public with sharing class saveProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveProcessParams saveProcessParams = (SaveProcessParams) JSON.deserialize(jsonInput, SaveProcessParams.class);
            Map<String, Object> response = new Map<String, Object>();

            String opportunityId = saveProcessParams.opportunityId;
            try {
                Opportunity opp = opportunityQuery.getOpportunityById(opportunityId);
                opp.StageName = saveProcessParams.isDraft ? constantsSrv.OPPORTUNITY_STAGE_QUALIFICATION : constantsSrv.OPPORTUNITY_STAGE_QUOTED;
                opp.SalesSupportUser__c = saveProcessParams.salesSupportUser;

                Contract contract = contractQuery.getById(opp.ContractId);
                opp.CompanyDivision__c = contract.CompanyDivision__c;

                List<SObject> objectsForUpsert = new List<SObject>();

                List<Case> cases = caseQuery.getCasesByDossierId(saveProcessParams.dossierId);
                if (cases.size() > 0) {
                    Case currentCase = cases[0];
                    currentCase.Contract2__c = opp.ContractId;
                    currentCase.EffectiveDate__c = opp.Contract.StartDate;

                    if (!saveProcessParams.isDraft) {
                        currentCase.Status = constantsSrv.CASE_STATUS_NEW;
                        Supply__c supply = new Supply__c(Id = currentCase.Supply__c, Prosumer__c = true/*ContractAccount__c = currentCase.ContractAccount__c*/);
                        objectsForUpsert.add(supply);
                    }
                    objectsForUpsert.add(currentCase);
                }

                if (!saveProcessParams.isDraft) {
                    Dossier__c dossier = new Dossier__c(
                            Id = saveProcessParams.dossierId,
                            Status__c = constantsSrv.DOSSIER_STATUS_NEW,
                            CompanyDivision__c = contract.CompanyDivision__c
                    );

                    objectsForUpsert.add(dossier);
                }
                objectsForUpsert.add(opp);
                databaseSrv.upsertSObject(objectsForUpsert);

                if(!saveProcessParams.isDraft){
                    MRO_SRV_Case.getInstance().applyAutomaticTransitionOnDossierAndCases(saveProcessParams.dossierId);
                }

                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateCaseByProsumerPrice extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String caseId = params.get('caseId');

            if (String.isBlank(caseId)) {
                throw new WrtsException('Case - ' + System.Label.MissingId);
            }

            try {
                Case caseRecord = caseQuery.getById(caseId);
                if(caseRecord != null && caseRecord.Supply__c != null && caseRecord.Supply__r.ServicePoint__c != null && caseRecord.Supply__r.ServicePoint__r.Distributor__c != null){
                    RegulatedTariff__c prosumerTariff = opportunityQuery.getProsumerPrice(caseRecord.Supply__r.ServicePoint__r.VoltageLevel__c, caseRecord.Supply__r.ServicePoint__r.Distributor__r.VATNumber__c);

                    if(prosumerTariff != null){
                        caseRecord.ProsumerPrice__c = prosumerTariff.PriceValue__c;
                    }

                    databaseSrv.updateSObject(caseRecord);
                }

                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

}