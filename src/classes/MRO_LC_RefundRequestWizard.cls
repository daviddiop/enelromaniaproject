/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 16.03.20.
 */

public with sharing class MRO_LC_RefundRequestWizard extends ApexServiceLibraryCnt {

    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();

    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();

    private static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();


    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                response.put('error', true);
                response.put('errorMsg', System.Label.Account + ' - ' + System.Label.MissingId);
                return response;
            }

            Account acc = accQuery.findAccount(accountId);

            Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'RefundRequest');

            if(!String.isBlank(genericRequestId)){
                dossierSr.updateParentGenericRequest(dossier.Id, genericRequestId);
            }

            List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
            response.put('caseTile', cases);

            if (cases.size() > 0) {
                response.put('billingProfileId', cases[0].BillingProfile__c);
            }

            response.put('isClosed', dossier.Status__c != constantsSrv.DOSSIER_STATUS_DRAFT);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('error', false);

            return response;
        }
    }

    public with sharing class RequestParams {
        @AuraEnabled
        public Id supplyId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
    }

    public with sharing class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams createCaseParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);
            Supply__c supply = supplyQuery.getSupplyWithServicePointById(createCaseParams.supplyId);

            Map<String, String> refundRequestRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('RefundRequest');
            Account acc = accQuery.findAccount(supply.Account__c);
            Date today = Date.today();

            Case newCase = new Case(
                    Supply__c = supply.Id,
                    CompanyDivision__c = supply.CompanyDivision__c,
                    AccountId = supply.Account__c,
                    AccountName__c = acc.Name,
                    RecordTypeId = (String) refundRequestRecordTypes.get('RefundRequest'),
                    Dossier__c = createCaseParams.dossierId,
                    Status = constantsSrv.CASE_STATUS_DRAFT,
                    Origin = createCaseParams.originSelected,
                    Channel__c = createCaseParams.channelSelected,
                    Phase__c = constantsSrv.CASE_START_PHASE,
                    ServiceSite__c = supply.ServiceSite__c,
                    EffectiveDate__c = today,
                    StartDate__c = today
            );
            databaseSrv.insertSObject(newCase);
            response.put('caseTile', new List<Case>{ newCase });
            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams channelOriginParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);

            dossierSr.updateDossierChannel(channelOriginParams.dossierId, channelOriginParams.channelSelected, channelOriginParams.originSelected);

            return response;
        }
    }

    public with sharing class BillingProfileSelectionParams {
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String billingProfileId { get; set; }
    }

    public with sharing class handleBillingProfileSelection extends AuraCallable {
        public override Object perform(final String jsonInput) {
            BillingProfileSelectionParams bpsParams = (BillingProfileSelectionParams) JSON.deserialize(jsonInput, BillingProfileSelectionParams.class);
            Boolean isInvoice = false;
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                Case c = new Case(Id = bpsParams.caseId, BillingProfile__c = bpsParams.billingProfileId);
                isInvoice = billingProfileQuery.getById(bpsParams.billingProfileId).RefundMethod__c == constantsSrv.BILLING_PROFILE_REFUND_METHOD_INVOICE ? true : false;
                if (!isInvoice){
                    databaseSrv.updateSObject(c);
                }
                response.put('isInvoice', isInvoice);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class CancelProcessParams {
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String cancelReason { get; set; }
        @AuraEnabled
        public String detailsReason { get; set; }
    }

    public with sharing class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CancelProcessParams cancelProcessParams = (CancelProcessParams) JSON.deserialize(jsonInput, CancelProcessParams.class);
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                databaseSrv.upsertSObject(new Dossier__c(
                        Id = cancelProcessParams.dossierId,
                        Status__c = constantsSrv.DOSSIER_STATUS_CANCELED,
                        CancellationReason__c = cancelProcessParams.cancelReason,
                        CancellationDetails__c = cancelProcessParams.detailsReason
                ));
                if (String.isNotBlank(cancelProcessParams.caseId)) {
                    databaseSrv.upsertSObject(new Case(
                            Id = cancelProcessParams.caseId,
                            Status = constantsSrv.CASE_STATUS_CANCELED,
                            Phase__c = constantsSrv.CASE_END_PHASE,
                            CancellationReason__c = cancelProcessParams.cancelReason,
                            CancellationDetails__c = cancelProcessParams.detailsReason
                    ));
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class SaveProcessParams {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public Boolean isDraft { get; set; }
    }

    public with sharing class saveProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveProcessParams saveProcessParams = (SaveProcessParams) JSON.deserialize(jsonInput, SaveProcessParams.class);
            String dossierId = saveProcessParams.dossierId;
            Map<String, Object> response = new Map<String, Object>();
            try {
                databaseSrv.upsertSObject(new Dossier__c(
                        Id = dossierId,
                        Status__c = saveProcessParams.isDraft ? constantsSrv.DOSSIER_STATUS_DRAFT : constantsSrv.DOSSIER_STATUS_NEW
                ));
                if (saveProcessParams.caseId != null && !saveProcessParams.isDraft) {
                    databaseSrv.upsertSObject(new Case(
                            Id = saveProcessParams.caseId,
                            Status = constantsSrv.CASE_STATUS_NEW
                    ));
                    caseSrv.applyAutomaticTransitionOnDossierAndCases(dossierId);
                }
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}