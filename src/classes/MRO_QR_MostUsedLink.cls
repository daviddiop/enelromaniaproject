/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   gen 31, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_MostUsedLink {
    public static MRO_QR_MostUsedLink getInstance() {
        return new MRO_QR_MostUsedLink();
    }

    public List<MostUsedLink__c> getMostUsedLinksByUser(Id userId, Integer rowsLimit) {
        String query = 'SELECT Id, ServiceLink__c FROM MostUsedLink__c WHERE User__c = :userId ORDER BY Count__c DESC';
        if (rowsLimit != null && rowsLimit > 0) {
            query += ' LIMIT '+rowsLimit;
        }
        return (List<MostUsedLink__c>) Database.query(query);
    }

    public List<MostUsedLink__c> getMostUsedLink(Id linkId, String userId) {
        return [
                SELECT Id, Count__c
                FROM MostUsedLink__c
                WHERE ServiceLink__c = :linkId
                AND User__c = :userId
        ];
    }
}