public with sharing class SupplyService extends ApexServiceLibraryCnt {

    static ServicePointQueries servicePointQuery = ServicePointQueries.getInstance();
    static Constants constantsSrv = new Constants();
    static DatabaseService databaseSrv = DatabaseService.getInstance();

    public static SupplyService getInstance() {
        return new SupplyService();
    }

    public class getRecordTypes extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('servicePointRecordType', new Map<String, String>{
                    'label' => System.Label.SearchServicePoint, 'value' => 'servicePoint'
            });
            response.put('contractRecordType', new Map<String, String>{
                    'label' => System.Label.SearchContract, 'value' => 'contact'
            });
            return response;
        }
    }

    public void actualizeSupplyOnServicePoint(Set<Id> servicePointIds) {

        List<ServicePoint__c> toUpdate = new List<ServicePoint__c>();
        List<ServicePoint__c> involvedPoints = servicePointQuery.getPointsWithSupplies(servicePointIds);
        for (ServicePoint__c point : involvedPoints) {

            if (point.CurrentSupply__c == null && !point.Supplies__r.isEmpty()) {
                point.CurrentSupply__r = point.Supplies__r.get(0);
            }
            /*
            if (point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVE) {
                continue;
            }*/

            for (Supply__c supply : point.Supplies__r) {
                /*if (supply.Id == point.CurrentSupply__c) {
                    continue;
                }*/

                if (supply.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVE) {
                    point.CurrentSupply__r = supply;
                    break;
                }

                if (supply.Status__c == constantsSrv.SUPPLY_STATUS_TERMINATING &&
                        (point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_NOTACTIVE || point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVATING)) {
                    point.CurrentSupply__r = supply;
                    continue;
                }

                if (supply.Status__c == constantsSrv.SUPPLY_STATUS_ACTIVATING && point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_NOTACTIVE) {
                    point.CurrentSupply__r = supply;
                    continue;
                }
            }

            if (point.CurrentSupply__r.Status__c == constantsSrv.SUPPLY_STATUS_NOTACTIVE) {
                point.CurrentSupply__c = null;
                point.CurrentSupply__r = null;
                point.Account__c = null;
            }
            if (point.CurrentSupply__r != null) {
                point.CurrentSupply__c = point.CurrentSupply__r.Id;
                point.Account__c = point.CurrentSupply__r.Account__c;
            }

            toUpdate.add(point);
        }

        if (!toUpdate.isEmpty()) {
            databaseSrv.updateSObject(toUpdate);
        }
    }

    public void deleteCase(String recordId) {
        databaseSrv.deleteSObject(recordId);
    }
}