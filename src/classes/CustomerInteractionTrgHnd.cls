public class CustomerInteractionTrgHnd implements TriggerManager.ISObjectTriggerHandler {
    private static final CustomerInteractionService customerInteractionSrv = CustomerInteractionService.getInstance();

    public void beforeInsert() {
        customerInteractionSrv.setUniqueCode((List<CustomerInteraction__c>)Trigger.new);
    }

    public void beforeUpdate() {
        customerInteractionSrv.setUniqueCode((List<CustomerInteraction__c>)Trigger.new);
    }

    public void beforeDelete() {}

    public void afterInsert() {}

    public void afterUpdate() {}

    public void afterDelete() {}

    public void afterUndelete() {}
}