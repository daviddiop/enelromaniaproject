/**
 * Created by David on 07.11.2019.
 */
@IsTest
public with sharing class MRO_LC_TaxChangeTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        mdt
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
       /** BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            //caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
        TaxSubsidy__c taxSubsidy = MRO_UTL_TestDataFactory.taxSubsidy().taxSubsidyVAT().build();
        taxSubsidy.Case__c = caseList[0].Id;
        taxSubsidy.AmountCurrency__c = 1000;
        taxSubsidy.AmountEnergy__c =5000;
        taxSubsidy.AmountPercent__c = 17;
        taxSubsidy.Supply__c = supplyList[0].Id;
        taxSubsidy.Customer__c = listAccount[0].Id;
        taxSubsidy.EndDate__c =  Date.newInstance(2040, 03, 04);
        taxSubsidy.StartDate__c = Date.newInstance(2018, 03, 04);
        insert taxSubsidy;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'TaxChangeTemplate_1';
        insert scriptTemplate;


    }
    @IsTest
    public static void InitializeTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'interactionId' => customerInteraction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TaxChange', 'initializeTaxChange', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => '',
                'interactionId' => customerInteraction.Id
        };
        response = TestUtils.exec(
                'MRO_LC_TaxChange', 'initializeTaxChange', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @IsTest
    private static void updateCaseListTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                        AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id, Status__c
                FROM Dossier__c
                LIMIT 1
        ];
        List<TaxSubsidy__c> listTaxSubsidies = [
                SELECT Id, Supply__c,Case__c
                FROM TaxSubsidy__c
                LIMIT 1
        ];
        List<TaxSubsidy__c> taxSubsidiesListToUpdate = [
                SELECT Id, Supply__c,Case__c
                FROM TaxSubsidy__c
                LIMIT 1
        ];

        String taxSubsidiesListString = JSON.serialize(listTaxSubsidies);
        String taxSubsidiesListToUpdateString = JSON.serialize(taxSubsidiesListToUpdate);
        Map<String, String > inputJSON = new Map<String, String>{
                'taxSubsidiesList' => taxSubsidiesListString,
                'taxSubsidiesListToUpdate' => taxSubsidiesListToUpdateString,
                'dossierId' => dossier.Id,
                'requestType' => System.Label.UpdateRequest
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TaxChange', 'updateCaseListAndTax', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    private static void CancelProcessTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,CancellationReason__c,
                        AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id, Status__c
                FROM Dossier__c
                LIMIT 1
        ];
        List<TaxSubsidy__c> listTaxSubsidies = [
            SELECT Id, Supply__c,Case__c
            FROM TaxSubsidy__c
            LIMIT 1
        ];
        String listTaxSubsidiesString = JSON.serialize(listTaxSubsidies);
        Map<String, String > inputJSON = new Map<String, String>{
                'taxSubsidiesList' => listTaxSubsidiesString,
                'caseRecord' => caseList[0].Id,
                'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TaxChange', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void getSupplyRecordTest() {
        String supplyId = [
                SELECT Id
                FROM Supply__c
                LIMIT 1
        ].Id;
        List<Id> ids = new List<Id>();
        ids.add(supplyId);
        MRO_LC_TaxChange.supplyInputIds suppliesInputIds = new MRO_LC_TaxChange.supplyInputIds();
        suppliesInputIds.supplyIds = ids;
        /*Map<String, String > inputJSON = new Map<String, String>{
                'supplyId' => supplyId
        };*/

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TaxChange', 'getSupplyRecord', suppliesInputIds, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void updateDossierTest(){
        Dossier__c dossier = [
            SELECT Id, Status__c
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TaxChange', 'updateDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    private static void updateCaseAndDossierTest(){
        Dossier__c dossier = [
            SELECT Id, Status__c
            FROM Dossier__c
            LIMIT 1
        ];

        String supplyId = [
            SELECT Id
            FROM Supply__c
            LIMIT 1
        ].Id;
        String isRetroActiveDate = 'true';
        List<TaxSubsidy__c> listTaxSubsidies = [
            SELECT Id, Supply__c,Case__c
            FROM TaxSubsidy__c
            LIMIT 1
        ];
        String taxSubsidiesListString = JSON.serialize(listTaxSubsidies);
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'taxSubsidiesList' => taxSubsidiesListString,
            'dossierId' => dossier.Id,
            'isRetroActiveDate' => isRetroActiveDate,
            'supplyId' => supplyId
        };
        Object response = TestUtils.exec(
            'MRO_LC_TaxChange', 'updateCaseAndDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    private static void updateTaxSubsidyStatutTest(){
        TaxSubsidy__c taxSubsidiesListToUpdate = [
            SELECT Id, Supply__c,Case__c
            FROM TaxSubsidy__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'taxSubsidyId' => taxSubsidiesListToUpdate.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TaxChange', 'updateTaxSubsidyStatut', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void updateOldTaxSubsidyStatutTest(){
        TaxSubsidy__c taxSubsidiesListToUpdate = [
            SELECT Id, Supply__c,Case__c
            FROM TaxSubsidy__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'parentCaseId' => taxSubsidiesListToUpdate.Case__c
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TaxChange', 'updateOldTaxSubsidyStatut', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

}