/**
 * Created by Antonella Loche on 31/03/2020.
 */

public with sharing class MRO_SRV_GenesysCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout {
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap){
        System.debug('MRO_SRV_GenesysCallOut.buildMultiRequest');

        //Sobject
        Sobject obj = (sObject) argsMap.get('sender');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        //Query
        Case richiesta = [//CASE
               select //Contact.MarketingPhone__c, 
            			//supply__r.ServicePoint__r.ENELTEL__c,
					  	dossier__r.customerinteraction__r.Interaction__r.Name, 
					  	dossier__r.customerinteraction__r.Interaction__r.Comments__c,
					  	dossier__r.customerinteraction__r.Customer__r.IntegrationKey__c
				from case
				where Id = :obj.Id limit 1
        ];

        //to WObject
        Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject(richiesta, null);

        //Add mrrObj to the request
        request.objects.add(mrrObj);

        //Add request to Multirequest
        multiRequest.requests.add(request);

        return multiRequest;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        return null;
    }
}