/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 09.06.20.
 */

public with sharing class MRO_SRV_SapSelfReadingCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout {

    private static final String requestTypeCheckIndex = 'CheckIndex';
    private static final String requestTypeQueryIndex = 'QueryIndex';

    private String endpointQueryIndex = 'sapquery';
    private String endpointCheckIndex = 'sapquery';

    public static MRO_SRV_SapSelfReadingCallOut getInstance() {
        return new MRO_SRV_SapSelfReadingCallOut();
    }

    public static MRO_SRV_SapSelfReadingCallOut getInstance(String endpointQueryIndex, String endpointCheckIndex) {
        return new MRO_SRV_SapSelfReadingCallOut(endpointQueryIndex, endpointCheckIndex);
    }

    public MRO_SRV_SapSelfReadingCallOut(){

    }

    public MRO_SRV_SapSelfReadingCallOut(String endpointQueryIndex, String endpointCheckIndex){
        this.endpointQueryIndex = endpointQueryIndex;
        this.endpointCheckIndex = endpointCheckIndex;
    }

    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap) {

        Map<String, Object> conditions = (Map<String, Object>) argsMap.get('conditions');

        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');

        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        String requestType = parameters.get('requestType');

        wrts_prcgvr.MRR_1_0.WObject wo;

        Set<String> conditionFields;
        if(requestType == requestTypeQueryIndex) {
            wo = new wrts_prcgvr.MRR_1_0.WObject();
            wo.id = '';
            wo.name = requestType;
            wo.objectType = requestType;
            conditionFields = new Set<String>{
                    'ENELTEL', 'StartDate', 'EndDate'
            };
            for (String fieldName : conditionFields) {
                Object fieldValue = conditions.get(fieldName);
                if (fieldValue != null) {
                    wo.fields.add(MRO_UTL_MRRMapper.createField(fieldName, fieldValue));
                }
            }
            request.objects.add(wo);
        }

        if(requestType == requestTypeCheckIndex) {
            conditionFields = new Set<String>{
                    'DialId', 'Index', 'ServicePointCode', 'MeterSerialNumber', 'CheckAll', 'InvoiceNumber', 'InvoiceSerialNumber'
            };
            List<Map<String, Object>> readings = (List<Map<String, Object>>)conditions.get('readings');

            for(Map<String, Object> reading : readings) {
                wo = new wrts_prcgvr.MRR_1_0.WObject();
                wo.id = '';
                wo.name = requestType;
                wo.objectType = requestType;
                for (String fieldName : conditionFields) {
                    Object fieldValue = reading.get(fieldName);
                    if (fieldValue != null) {
                        wo.fields.add(MRO_UTL_MRRMapper.createField(fieldName, fieldValue));
                    }
                }
                request.objects.add(wo);
            }
        }

        multiRequest.requests.add(request);

        System.debug(multiRequest);

        return multiRequest;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse) {
        return null;
    }

    public static Boolean checkSapEnabled() {
        List<SAPCallouts__c> sapCalloutsCustomSetting = [SELECT MeterList__c FROM SAPCallouts__c];
        return sapCalloutsCustomSetting[0].MeterList__c;
    }

    public class QueryIndexResponseDTO {
        public String ErrorMessage { get; set; }
        public String InvoicedAmount { get; set; }
        public String CustomerName { get; set; }
        public String SupplyIntegrationKey { get; set; }
        public String LastInvoiceDate { get; set; }
        public String LastInvoicedReadingDate { get; set; }
        public String InvoicedEstimatedAmount { get; set; }
        public String EstimatedInvoicesCount { get; set; }
        public String MeterSerialNumber { get; set; }
        public String SmartMeter { get; set; }
        public String LastInvoiceStatus { get; set; }
        public String MeterType { get; set; }
        public String LastInvoicedReading { get; set; }
        public String ReadingFrequency { get; set; }
        public String ReadingInterval { get; set; }
        public String ReadingRoute { get; set; }

        public List<ReadingDTO> readings { get; set; }

        public Map<String, Object> toMap() {
            return new Map<String, Object>{
                    'MeterNumber__c' => this.MeterSerialNumber,
                    'Quadrants' => readingsToMap()
            };
        }

        private List<Map<String, Object>> readingsToMap() {
            List<Map<String, Object>> readingsMapList = new List<Map<String, Object>>();
            for (ReadingDTO readingDTO : this.readings) {
                readingsMapList.add(readingDTO.toMap());
            }
            return readingsMapList;
        }

        public QueryIndexResponseDTO(){
            this.readings = new List<ReadingDTO>();
        }

        public QueryIndexResponseDTO(Map<String, String> stringMap) {
            this.MeterSerialNumber = stringMap.get('MeterSerialNumber');
            this.InvoicedAmount = stringMap.get('InvoicedAmount');
            this.CustomerName = stringMap.get('CustomerName');
            this.SupplyIntegrationKey = stringMap.get('SupplyIntegrationKey');
            this.LastInvoiceDate = stringMap.get('LastInvoiceDate');
            this.LastInvoicedReadingDate = stringMap.get('LastInvoicedReadingDate');
            this.InvoicedEstimatedAmount = stringMap.get('InvoicedEstimatedAmount');
            this.EstimatedInvoicesCount = stringMap.get('EstimatedInvoicesCount');
            this.SmartMeter = stringMap.get('SmartMeter');
            this.LastInvoiceStatus = stringMap.get('LastInvoiceStatus');
            this.MeterType = stringMap.get('MeterType');
            this.LastInvoicedReading = stringMap.get('LastInvoicedReading');
            this.ReadingFrequency = stringMap.get('ReadingFrequency');
            this.ReadingInterval = stringMap.get('ReadingInterval');
            this.ReadingRoute = stringMap.get('ReadingRoute');
            this.readings = new List<ReadingDTO>();
        }
    }

    public class ReadingDTO {
        public String dialId { get; set; }
        public String dialType { get; set; }
        public String indexIntegerPart { get; set; }
        public String readingDate { get; set; }
        public Double index { get; set; }
        public String type { get; set; }
        public String invoiced { get; set; }
        public String amount { get; set; }
        public Boolean checkAll { get; set; }
        public String imageLink { get; set; }
        public String dmsImageLink { get; set; }

        public String servicePointCode { get; set; }
        public String meterSerialNumber { get; set; }
        public String invoiceSerialNumber { get; set; }
        public String invoiceNumber { get; set; }

        public Map<String, Object> toMap() {
            return new Map<String, Object>{
                    'ID_CADRAN' => this.dialId,
                    'CADRAN' => this.dialType,
                    'INDEX' => this.index,
                    'VALIDATED' => false
            };
        }
        public ReadingDTO() {

        }

        public ReadingDTO(Map<String, String> stringMap) {
            this.dialId = stringMap.get('DialId');
            this.dialType = stringMap.get('DialType');
            this.index = Double.valueOf(stringMap.get('Index'));
            this.indexIntegerPart = stringMap.get('IndexIntegerPart');
            this.readingDate = stringMap.get('ReadingDate');
            this.type = stringMap.get('Type');
            this.invoiced = stringMap.get('Invoiced');
            this.amount = stringMap.get('Amount');
            this.imageLink = stringMap.get('ImageLink');
            this.dmsImageLink = stringMap.get('DmsImageLink');
        }
    }

    public QueryIndexResponseDTO getQueryIndexResponseDummy(String enelId, Date startDate, Date endDate){
        QueryIndexResponseDTO queryIndexResponse = new QueryIndexResponseDTO();
        queryIndexResponse.CustomerName = 'Customer Name';
        queryIndexResponse.SupplyIntegrationKey = '1175940182_1';
        queryIndexResponse.MeterSerialNumber = '01804685/2017';
        queryIndexResponse.LastInvoiceStatus = 'incasat';

        for (Integer i = 0; i < 3; i++) {
            ReadingDTO newReading = new ReadingDTO();
            newReading.dialId = '1.8.' + i;
            newReading.dialType = 'Energie Activă Zona ' + i;
            newReading.indexIntegerPart = '0' + i;
            newReading.readingDate = '2020012' + i;
            newReading.index = 1218 + i;
            newReading.type = 'Citire contor de comp.utilităţi - SAP Citire ED in timpul lunii ' + i;
            newReading.invoiced = 'NU';
            queryIndexResponse.readings.add(newReading);
        }
        return queryIndexResponse;
    }

    public QueryIndexResponseDTO getQueryIndexResponse(String enelId, Date startDate, Date endDate){
        return getQueryIndexResponse(enelId, startDate, endDate, false);
    }

    public QueryIndexResponseDTO getQueryIndexResponse(String enelId, Date startDate, Date endDate, Boolean onlyLastReading){
        if(startDate == null || endDate == null){
            endDate = Date.today();
            startDate = endDate.addDays(-90);
        }

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpointQueryIndex, 'QueryIndex', new Map<String, Object>{
                'ENELTEL' => enelId,
                'StartDate' => startDate,
                'EndDate' => endDate
        });

        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
            System.debug(calloutResponse.responses[0].objects);

            if(calloutResponse.responses[0].header != null && calloutResponse.responses[0].header.fields != null && !calloutResponse.responses[0].header.fields.isEmpty() && calloutResponse.responses[0].header.fields[0].name == 'ErrorMessage'){
                throw new WrtsException(calloutResponse.responses[0].header.fields[0].value);
            }

            QueryIndexResponseDTO queryIndexResponse;

            for (wrts_prcgvr.MRR_1_0.WObject wo : calloutResponse.responses[0].objects) {
                Map<String, String> woMap = MRO_UTL_MRRMapper.wobjectToMap(wo);
                queryIndexResponse = new QueryIndexResponseDTO(woMap);
                if (wo.objects != null && wo.objects.size() != 0) {
                    Map<String, ReadingDTO> readingsByCadran = new Map<String, ReadingDTO>();
                    for (wrts_prcgvr.MRR_1_0.WObject childWo : wo.objects) {
                        Map<String, String> childWoMap = MRO_UTL_MRRMapper.wobjectToMap(childWo);
                        ReadingDTO newReading = new ReadingDTO(childWoMap);
                        readingsByCadran = checkExistingReading(newReading, readingsByCadran);

                        if(!onlyLastReading) queryIndexResponse.readings.add(newReading);
                    }

                    if(onlyLastReading) queryIndexResponse.readings.addAll(readingsByCadran.values());
                }
            }
            System.debug(queryIndexResponse);
            return queryIndexResponse;
        }
        return null;
    }

    public String getList(String enelId, Date startDate, Date endDate) {
        QueryIndexResponseDTO queryIndexResponse = getQueryIndexResponse(enelId, startDate, endDate, true);
        if(queryIndexResponse == null) return null;

        String meterDtoString = JSON.serialize(new Map<String, Object>{
                'Meters' => new List<Map<String, Object>>{queryIndexResponse.toMap()}
        });
        return meterDtoString;
    }

    private static Map<String, ReadingDTO> checkExistingReading(ReadingDTO newReading, Map<String, ReadingDTO> readingsMap) {
        ReadingDTO existingReading = readingsMap.get(newReading.dialId);
        if (existingReading == null) {
            readingsMap.put(newReading.dialId, newReading);
            return readingsMap;
        }
        try {
            Date newReadingDate = parseStringDate(newReading.readingDate);
            Date existingReadingDate = parseStringDate(existingReading.readingDate);
            if(newReadingDate > existingReadingDate){
                readingsMap.put(newReading.dialId, newReading);
                return readingsMap;
            }
        } catch (Exception e) {
            System.debug('#### Invalid date ' + newReading.readingDate + ' ' + existingReading.readingDate);
        }
        return readingsMap;
    }

    private static Date parseStringDate(String stringDate){
        if(stringDate.length() == 8){
            String year = stringDate.substring(0, 4);
            System.debug(year);
            String month = stringDate.substring(4, 6);
            System.debug(month);
            String day = stringDate.substring(6, 8);
            System.debug(day);
            return Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
        }
        return MRO_SRV_SapQueryHelper.parseDate(stringDate);
    }

    public Map<String, String> validate(ReadingDTO readingDTO) {
        return validate(new List<ReadingDTO>{readingDTO});
    }

    public Map<String, String> validate(List<ReadingDTO> readingDTOs) {
        List<Map<String, Object>> readings = new List<Map<String, Object>>();
        for(ReadingDTO rd : readingDTOs){
            readings.add(new Map<String, Object>{
                    'DialId' => rd.dialId,
                    'Index' => rd.index,
                    'ServicePointCode' => rd.servicePointCode,
                    'MeterSerialNumber' => rd.meterSerialNumber,
                    'CheckAll' => rd.checkAll,
                    'InvoiceNumber' => rd.invoiceNumber,
                    'InvoiceSerialNumber' => rd.invoiceSerialNumber
            });
        }
        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpointCheckIndex, 'CheckIndex', new Map<String, Object>{
                'readings' => readings
        });

        System.debug(calloutResponse);
        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {

            if(calloutResponse.responses[0].code == 'NOK'){
                return new Map<String, String>{
                        'returnCode' => calloutResponse.responses[0].code,
                        'returnMessage' => calloutResponse.responses[0].description
                };
            }

            if (calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
                System.debug(calloutResponse.responses[0].objects);

                wrts_prcgvr.MRR_1_0.WObject wo = calloutResponse.responses[0].objects[0];
                Map<String, String> woMap = MRO_UTL_MRRMapper.wobjectToMap(wo);
                Map<String, String> response = new Map<String, String>{
                        'returnCode' => woMap.get('Code'),
                        'returnMessage' => woMap.get('ReturnMessage')
                };
                if (wo.objects != null && !wo.objects.isEmpty() && wo.objects[0].objectType == 'Detail') {
                    Map<String, String> woDetailMap = MRO_UTL_MRRMapper.wobjectToMap(wo.objects[0]);
                    response.put('returnCode', woDetailMap.get('Code'));
                    response.put('returnMessage', woDetailMap.get('Message'));
                }

                System.debug(response);
                return response;
            }
        }
        return null;
    }

    private wrts_prcgvr.MRR_1_0.MultiResponse executeCallout(String endPoint, String requestType, Map<String, Object> conditions) {
        String className = 'MRO_SRV_SapSelfReadingCallOut';

        Map<String, Object> argsMap = new Map<String, Object>();
        argsMap.put('method', className);
        argsMap.put('conditions', conditions);

        Map<String, String> parameters = new Map<String, String>();
        parameters.put('requestType', requestType);
        parameters.put('timeout', '60000');
        parameters.put('endPoint', endPoint);
        argsMap.put('parameters', parameters);

        MRO_SRV_SendMRRcallout instance = new MRO_SRV_SendMRRcallout();
        Object executionResult = instance.execute((Object) argsMap, true);
        System.debug(executionResult);
        if (executionResult instanceof wrts_prcgvr.MRR_1_0.MultiResponse) {
            return (wrts_prcgvr.MRR_1_0.MultiResponse) executionResult;
        }
        return null;
    }
}