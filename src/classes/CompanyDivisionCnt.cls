/**
 * Created by goudiaby on 07/05/2019.
 */

/**
 * @description Controller for CompanyDivision Component
 */
public with sharing class CompanyDivisionCnt extends ApexServiceLibraryCnt {
    private static UserService userSrv = UserService.getInstance();
    private static CompanyDivisionService companyDivisionSrv = CompanyDivisionService.getInstance();
    private static UserQueries userQuery = UserQueries.getInstance();

    /**
   * @description Class to get Company Division Data from User id and all company Division
   */
    public with sharing class getCompanyDivisionData extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            CompanyDivision__c currCompanyDivision;
            User user;
            Boolean companyDivisionEnforced;
            String userId = params.get('userId');
            if (String.isBlank(userId)) {
                throw new WrtsException(System.Label.User + ' - ' + System.Label.MissingId);
            }

            user = userQuery.getCompanyDivisionId(userId);
            companyDivisionEnforced = user.CompanyDivisionEnforced__c;

            currCompanyDivision = companyDivisionSrv.getCompanyDivisionInfosFromUser(userId);
            List<ApexServiceLibraryCnt.Option> companyDivisionOptions = companyDivisionSrv.getListOptionsCompanyDivision();

            response.put('companyDivision', currCompanyDivision);
            response.put('listCompaniesDivision', companyDivisionOptions);
            response.put('companyDivisionEnforced', companyDivisionEnforced);

            if(currCompanyDivision.Id!=null){
                response.put('companyDivisionId', currCompanyDivision.Id);
                response.put('companyDivisionName', currCompanyDivision.Name);
            }

            return response;
        }
    }

    /**
     * @description Class to update a company division User
     */
    public with sharing class updateUserCompanyDivision extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String userId = params.get('userId');
            String companyId = params.get('companyId');
            Boolean companyDivisionEnforced = (Boolean)JSON.deserialize(params.get('companyDivisionEnforced'),Boolean.class);
            if (String.isBlank(userId)) {
                throw new WrtsException(System.Label.User + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(companyId)) {
                throw new WrtsException(System.Label.Company + ' - ' + System.Label.MissingId);
            }

            String idCurrentUser = userSrv.updateCompanyDivision(userId, companyId,companyDivisionEnforced);
            response.put('currentUserId', idCurrentUser);
            return response;
        }
    }
    /*public with sharing class updateUserCompanyDivisiontest extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            System.debug('jsonInput'+jsonInput);
            String userId = params.get('userId');
            String companyId = params.get('companyId');
            Boolean companyDivisionEnforced = (Boolean)JSON.deserialize(params.get('companyDivisionEnforced'),Boolean.class);
            System.debug('companyDivisionEnforced'+companyDivisionEnforced);
            if (String.isBlank(userId)) {
                throw new WrtsException(System.Label.User + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(companyId)) {
                throw new WrtsException(System.Label.Company + ' - ' + System.Label.MissingId);
            }

            String idCurrentUser = userSrv.updateCompanyDivisionTest(userId, companyId,companyDivisionEnforced);
            response.put('currentUserId', idCurrentUser);
            return response;
        }
    }*/
}