/**
 * Created by napoli on 28/10/2019.
 */

public with sharing class MRO_SRV_ExecutionLog {
    public static ExecutionLog__c createDebugLog(String type, String subject, String message,  String stackTraceString, String source, String refRecordId) {
        ExecutionLog__c eLog = new ExecutionLog__c();
        eLog.RecordTypeId = Schema.SObjectType.ExecutionLog__c.getRecordTypeInfosByDeveloperName().get('Debug').getRecordTypeId();
        eLog.Message__c = message.length() >= 131072 ? message.substring(0, 131071) : message;
        eLog.StackTrace__c = stackTraceString.length() >= 131072 ? stackTraceString.substring(0, 131071) : stackTraceString;
        eLog.Type__c = type;
        eLog.RefRecordId__c = refRecordId;
        eLog.Subject__c = subject;
        eLog.Source__c = source;
        eLog.ExternalId__c = 'debug_' + Datetime.now().getTime();
        insert eLog;
        if (message.length() >= 131072) {
            Attachment messageAtt= new Attachment();
            messageAtt.Name = 'errorDescription.txt';
            messageAtt.Body = Blob.valueOf(message);
            messageAtt.ParentId = eLog.Id;
            insert messageAtt;
        }
        if (stackTraceString.length() >= 131072) {
            Attachment stackTraceAtt = new Attachment();
            stackTraceAtt.Name = 'stackTrace.txt';
            stackTraceAtt.Body = Blob.valueOf(stackTraceString);
            stackTraceAtt.ParentId = eLog.Id;
            insert stackTraceAtt ;
        }
        return eLog;
    }

    public static ExecutionLog__c createDebugLog(Exception e, String subject, String source, String refRecordId) {
        if (e != null) {
            String message =  'Exception -- Message: ' + e.getMessage() + ' -- Cause: ' + e.getCause() + ' -- Type :' + e.getTypeName();
            String stackTraceString = 'Stack Trace: '+ e.getStackTraceString();
            return createDebugLog('Apex Exception', subject, message, stackTraceString, source, refRecordId);
        }
        return null;
    }

    public static ExecutionLog__c createIntegrationLog(String type, String requestId, String requestType, String refRecordId, String message, String code, String response, String source, String subject, String parentRequestId, String systemString, String errorType) {
        return createIntegrationlog(type, requestId, requestType, refRecordId, message, null, code, response, source, subject, parentRequestId, systemString, errorType);
    }

    public static ExecutionLog__c createIntegrationLog(String type, String requestId, String requestType, String refRecordId, String message, String stackTrace, String code, String response, String source, String subject, String parentRequestId, String systemString, String errorType) {
        //Query Execution Log Settings
        List<ExecutionLogSetting__mdt> ExecutionLogSettings;
        ExecutionLogSettings = [
                SELECT  LogLevel__c
                FROM    ExecutionLogSetting__mdt
                WHERE   RequestType__c = :requestType
                AND System__c = :systemString
        ];
        //Query Execution Log Settings
        if(ExecutionLogSettings.isEmpty()){
            ExecutionLogSettings = [
                    SELECT  LogLevel__c
                    FROM    ExecutionLogSetting__mdt
                    WHERE   RequestType__c = '*'
                    AND System__c = :systemString
            ];
        }
        Boolean createIntegrationLog = true;
        if(!ExecutionLogSettings.isEmpty() && ExecutionLogSettings.size() == 1){
            /*if(ExecutionLogSettings[0].LogLevel__c == 'Off'){
                createIntegrationLog = false;
                System.debug('Log level is Off -> It Doesn\'t create Integration log');
            } else if(ExecutionLogSettings[0].LogLevel__c == 'Full'){
                createIntegrationLog = true;
                System.debug('Log level is All -> It create Integration log');
            } else if(ExecutionLogSettings[0].LogLevel__c == 'All Errors' && (errorType == 'Functional error' || errorType == 'Technical error')){
                createIntegrationLog = true;
                System.debug('Log level is All Errors -> It create Integration log');
            } else if(ExecutionLogSettings[0].LogLevel__c == 'Technical Errors' && errorType == 'Technical error'){
                createIntegrationLog = true;
                System.debug('Log level is Technical Errors and error type is Technical error -> It create Integration log');
            } else if(ExecutionLogSettings[0].LogLevel__c == 'Technical Errors' && errorType == 'Functional error'){
                createIntegrationLog = false;
                System.debug('Log level is Technical Errors and error type is Functional error -> It Doesn\'t create Integration log');
            }*/

            switch on ExecutionLogSettings[0].LogLevel__c {
                when 'Off' {
                    createIntegrationLog = false;
                    System.debug('Log level is Off -> It Doesn\'t create Integration log');
                }
                when 'Full' {
                    createIntegrationLog = true;
                    System.debug('Log level is All -> It create Integration log');
                }
                when 'All Errors' {
                    if(errorType == 'Functional error' || errorType == 'Technical error'){
                        createIntegrationLog = true;
                        System.debug('Log level is Technical Errors and error type is Technical error -> It create Integration log');
                    }
                }
                when 'Technical Errors' {
                    if(errorType == 'Technical error'){
                        createIntegrationLog = true;
                        System.debug('Log level is Technical Errors and error type is Technical error -> It create Integration log');
                    } else{
                        createIntegrationLog = false;
                        System.debug('Log level is Technical Errors and error type is Functional error -> It Doesn\'t create Integration log');
                    }
                }
                when else {
                    createIntegrationLog = true;
                }
            }
        }
        //if(createIntegrationLog) MRO_SRV_ExecutionLog.createIntegrationLog('CallOut', multiRequest.requests[0].header.requestId, multiRequest.requests[0].header.requestType, obj.Id, messageString, code, responseString, null, null, null);
        if(createIntegrationLog == false) return null;
        // CREATE EXECUTION LOG END

        if (!String.isBlank(refRecordId) && ((Id) refRecordId).getSobjectType().getDescribe().getName() == 'PrintAction__c') {
            List<ExecutionLog__c> existingLogs = [SELECT Id, ExternalId__c, CreatedDate FROM ExecutionLog__c WHERE ExternalId__c = :requestId];
            if (!existingLogs.isEmpty()) {
                for (ExecutionLog__c log : existingLogs) {
                    log.ExternalId__c += '_' + log.CreatedDate.getTime();
                }
                update existingLogs;
            }
        }

        ExecutionLog__c eLog = new ExecutionLog__c();
        eLog.RecordTypeId = Schema.SObjectType.ExecutionLog__c.getRecordTypeInfosByDeveloperName().get('Integration').getRecordTypeId();
        eLog.Type__c = type;
        eLog.RequestType__c = requestType;
        eLog.RefRecordId__c = refRecordId;
        eLog.Message__c = (message !=null && message.length() >= 131072) ? message.substring(0, 131071) : message;
        eLog.StackTrace__c = stackTrace;
        eLog.Code__c = (code !=null && code.length() >= 255) ? code.substring(0, 254) : code;
        eLog.Response__c = (response !=null && response.length() >= 131072) ? response.substring(0, 131071) : response;
        eLog.Source__c = source;
        eLog.Subject__c = subject;
        eLog.ExternalId__c = requestId;
        eLog.ExternalSystem__c = systemString;
        if (String.isNotBlank(parentRequestId)) {
            List<ExecutionLog__c> parentLogTempList = [SELECT Id FROM ExecutionLog__c WHERE ExternalId__c = :parentRequestId];
            if(!parentLogTempList.isEmpty()) eLog.ParentLog__c =  parentLogTempList[0].Id;

            /*ExecutionLog__c parentLogTemp = new ExecutionLog__c(ExternalId__c = parentRequestId);
            eLog.ParentLog__r =  parentLogTemp;*/
            if (requestId == parentRequestId) {
                eLog.ExternalId__c = requestId + '_' + Datetime.now().getTime();
            }
           /* upsert eLog;
        } else {
            insert eLog;*/
        }
        insert eLog;
        if (message !=null && message.length() >= 131072) {
            Attachment messageAtt= new Attachment();
            messageAtt.Name = 'Message.txt';
            messageAtt.Body = Blob.valueOf(message);
            messageAtt.ParentId = eLog.Id;
            insert messageAtt;
        }
        if (code !=null && code.length() >= 255) {
            Attachment codeAtt= new Attachment();
            codeAtt.Name = 'Code.txt';
            codeAtt.Body = Blob.valueOf(code);
            codeAtt.ParentId = eLog.Id;
            insert codeAtt;
        }
        return eLog;
    }

    public static void createAndSaveLog(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest, wrts_prcgvr.MRR_1_0.MultiResponse multiResponse, Id objectId) {
        wrts_prcgvr.MRR_1_0.Request request = multiRequest.requests[0];
        wrts_prcgvr.MRR_1_0.Response response = multiResponse.responses[0];
        System.debug('response Description '+response.description + ' Code ' +response.code);
        String code = String.isNotBlank(response.description) ? response.code + ' - ' + response.description : response.code;
        String messageString = MRO_UTL_MRRMapper.serializeMultirequest(multiRequest);
        String responseString = MRO_UTL_MRRMapper.serializeMultiResponse(multiResponse);
        MRO_SRV_ExecutionLog.createIntegrationLog('CallIn', request.header.requestId, request.header.requestType, objectId, messageString, code, responseString, null, null, null, 'internetinterface', null);
    }
}