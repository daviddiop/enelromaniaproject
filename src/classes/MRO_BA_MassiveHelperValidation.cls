/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 08.05.20.
 */

global with sharing class MRO_BA_MassiveHelperValidation implements Database.Batchable<SObject> {

    MRO_QR_FileImportJob jobQuery = MRO_QR_FileImportJob.getInstance();
    MRO_QR_FileTemplateColumn columnsQuery = MRO_QR_FileTemplateColumn.getInstance();
    MRO_QR_FileImportDataRow rowsQuery = MRO_QR_FileImportDataRow.getInstance();

    global final String query;
    global final String entity;
    global final Map<String, String> params;

    Map<String, List<FileColumnDataDTO>> mapRowsWithErrors;
    Map<String, List<FileColumnDataDTO>> mapColsWithApexAction;
    Map<String, List<FileColumnDataDTO>> mapRowsToTransform;

    global MRO_BA_MassiveHelperValidation(String q, String e, Map<String, String> p) {
        query = q;
        entity = e;
        params = p;
    }

    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext batchableContext, List<SObject> scope) {
        String jobID = batchableContext.getJobId();
        jobID = jobID.substring(0, 15);

        this.mapRowsWithErrors = new Map<String, List<FileColumnDataDTO>>();
        this.mapColsWithApexAction = new Map<String, List<FileColumnDataDTO>>();
        this.mapRowsToTransform = new Map<String, List<FileColumnDataDTO>>();

        FileImportJob__c fij = jobQuery.getByJobId(jobID);
        List<FileTemplateColumn__c> columnsHeader = columnsQuery.getByFileTemplateIdOrderASC(fij.FileTemplate__c);
        try {
            for (SObject s : scope) {
                FileImportDataRow__c fidr = (FileImportDataRow__c) s;

                String delimiter = fidr.FileImportJob__r.FileTemplate__r.FileDelimiter__c;
                List<String> values = fidr.RawRow__c.split(delimiter, -1);
                System.debug('############ check delimiter');
                System.debug(JSON.serialize(values));
                System.debug(values.size());
                System.debug(columnsHeader.size());
                Boolean hasError = checkDelimiter(values, columnsHeader);
                if (hasError) {
                    addTo(this.mapRowsWithErrors, fidr.Id, new FileColumnDataDTO('', '', 'Invalid Delimiter'));
                } else {
                    for (Integer index = 0; index < columnsHeader.size(); index++) {
                        FileTemplateColumn__c ftc = columnsHeader.get(index);
                        FileColumnDataDTO fcDTO = new FileColumnDataDTO(fidr, ftc, values.get(index));
                        Boolean hasValidationError = false;

                        //apex validation
                        if (!String.isBlank(fcDTO.validationAction)) {
                            System.debug('####### System.debug(execute): !String.isBlank(fcDTO.validationAction');
                            addTo(this.mapColsWithApexAction, fidr.Id, fcDTO);
                        }

                        //check mandatory
                        if (fcDTO.mandatory && (String.isBlank(fcDTO.value) || String.isEmpty(fcDTO.value))) {
                            System.debug('####### System.debug(execute): (fcDTO.mandatory)');
                            System.debug('####### System.debug(execute): Mandatory field cannot be Empty ');
                            addTo(this.mapRowsWithErrors, fidr.Id, new FileColumnDataDTO('', '', 'Mandatory field ' + fcDTO.columnName + ' can not be empty'));
                            hasValidationError = true;
                        }

                        //regexp validation
                        if (!String.isBlank(fcDTO.regExpr)) {
                            System.debug('####### System.debug(execute): !String.isBlank(fcDTO.regExpr');
                            System.debug('####### System.debug(execute): !String.isBlank(fcDTO.regExpr: ' + fcDTO.regExpr);
                            String regexp = fcDTO.regExpr.replace('\\\\', '\\');
                            if (!Pattern.compile(regexp).matcher(String.valueOf(fcDTO.value)).matches()) {
                                System.debug('####### System.debug(execute): Error for Regular Expression ');
                                fcDTO = new FileColumnDataDTO('', '', 'Error in Regular Expression ' + fcDTO.columnName);
                                addTo(this.mapRowsWithErrors, fidr.Id, fcDTO);
                                hasValidationError = true;
                            }

                        }
                        if (!hasValidationError) {
                            addTo(this.mapRowsToTransform, fidr.Id, fcDTO);
                        }
                    }
                }
                System.debug(mapRowsWithErrors);
                if (mapRowsWithErrors.size() > 0 && fij.FileTemplate__r.AllOrNone__c) {
                    fij.SkipJob__c = true;
                    fij.Status__c = 'Rejected';
                    update fij;
                    handleSave(scope);
                    System.abortJob(batchableContext.getJobId());
                    return;
                }
            }
            handleValidationApex();
            handleSave(scope);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            fij.Status__c = 'Rejected';
            update fij;
        }
    }

    global void finish(Database.BatchableContext batchableContext) {
        String jobID = batchableContext.getJobId();
        jobID = jobID.substring(0, 15);
        System.debug('####### finish BC.getJobId()' + jobID);
        FileImportJob__c fij = jobQuery.getByJobId(jobID);
        if(fij.SkipJob__c) {
            return;
        }

        try {
            String finalAction = fij.FileTemplate__r.FinishAction__c;
            System.debug('####### finish    finalAction: ' + finalAction);
            if(!String.isBlank(finalAction)) {
                MRO_INT_MassiveHelper mh = (MRO_INT_MassiveHelper) Type.forName(finalAction).newInstance();
                List<String> rowsJsonList = new List<String>();
                List<FileImportDataRow__c> listToUpdate = rowsQuery.listByFileImportJobID(fij.Id);
                /*for(FileImportDataRow__c fidr :listToUpdate) {
                    rowsJsonList.add(fidr.JSONRow__c);
                }*/
                mh.performFinishAction(listToUpdate, params);
            }
            fij.Status__c = 'Completed';
        } catch(Exception ex) {
            System.debug(ex.getMessage());
            fij.Status__c = 'Rejected';
        }
        update fij;
    }

    private void handleSave(List<SObject> scope) {
        List<FileImportDataRow__c> listToUpdate = new List<FileImportDataRow__c>();
        System.debug('####### excecute    handleSave: ');
        for(SObject s :scope) {
            FileImportDataRow__c fidr = (FileImportDataRow__c) s;
            System.debug('####### excecute    handleSave FileImportDataRow__c ID: ' + fidr.Id);
            System.debug('####### excecute    handleSave this.mapRowsWithErrors.containsKey(fidr.Id): ' + this.mapRowsWithErrors.containsKey(fidr.Id));
            System.debug('####### excecute    handleSave this.mapRowsToTrasform.containsKey(fidr.Id): ' + this.mapRowsToTransform.containsKey(fidr.Id));
            if(this.mapRowsWithErrors.containsKey(fidr.Id)) {
                bindError(this.mapRowsWithErrors.get(fidr.Id), fidr);
            } else {
                bind(this.mapRowsToTransform.get(fidr.Id), fidr);
            }
            listToUpdate.add(fidr);
        }
        update listToUpdate;
    }

    public FileImportDataRow__c bind(List<FileColumnDataDTO> source, FileImportDataRow__c dest) {
        String jsonstring = '{';
        for(FileColumnDataDTO dto :source) {
            jsonstring += '"' + dto.columnName + '":"' + dto.value + '",';
        }
        System.debug('####### jsonstring Before: ' + jsonstring);
        jsonstring  = jsonstring.substring(0, jsonstring.lastIndexOf(','));
        jsonstring += '}';
        System.debug('####### jsonstring After: ' + jsonstring);
        dest.Status__c = 'Validated';
        dest.JSONRow__c = jsonstring;
        return dest;
    }

    public FileImportDataRow__c bindError(List<FileColumnDataDTO> source, FileImportDataRow__c dest) {
        List<String> messages = new List<String>();

        FileColumnDataDTO firstDtoWithError;
        System.debug('####### bindError: ');
        for(FileColumnDataDTO dto :source) {
            System.debug('####### bindError dto.hasError: ' + dto.hasError);
            if(dto.hasError) {
                firstDtoWithError = dto;
                messages.add(dto.messageError);
            }
        }
        String allMessages = String.join(messages, '; ');
        if(allMessages.length() >= 131072){
            allMessages = allMessages.substring(0, 131071);
        }
        dest.Status__c = firstDtoWithError?.status;
        dest.ErrorMessage__c = firstDtoWithError?.messageError;
        dest.ErrorStackTrace__c = firstDtoWithError?.messageError;
        return dest;
    }

    private Boolean checkDelimiter(List<String> contentFileLines, List<FileTemplateColumn__c> columnsHeader) {
        return contentFileLines.size() != columnsHeader.size();
    }

    private Map<String, List<FileColumnDataDTO>> addTo(Map<String, List<FileColumnDataDTO>> mapToAdd, String key, FileColumnDataDTO elemToAdd) {
        List<FileColumnDataDTO> listItem;
        if (mapToAdd.containsKey(key)) {
            listItem = mapToAdd.get(key);
        } else {
            listItem = new List<FileColumnDataDTO>();
        }
        listItem.add(elemToAdd);
        mapToAdd.put(key, listItem);
        return mapToAdd;
    }

    private void handleValidationApex() {
        if(this.mapColsWithApexAction.size() == 0) {
            return;
        }
        Set<String> apexClassSet = retrieveSetWithValidation();
        Map<String, FileColumnDataDTO> results = new Map<String, FileColumnDataDTO>();
        for (String key :apexClassSet) {
            MRO_INT_MassiveHelper mh = (MRO_INT_MassiveHelper) Type.forName(key).newInstance();
            results.putAll(mh.performValidAction(groupByValidationClass(this.mapColsWithApexAction, key)));
        }
        handleResultsValidationApex(results);
    }

    private Map<String, FileColumnDataDTO> groupByValidationClass(Map<String, List<FileColumnDataDTO>> mapToGroup, String groupBy) {
        Map<String, FileColumnDataDTO> response = new Map<String, FileColumnDataDTO>();
        Set<String> keyset = mapToGroup.keySet();
        for(String key :keyset) {
            List<FileColumnDataDTO> l = mapToGroup.get(key);
            for(FileColumnDataDTO fcDTO :l) {
                if(groupBy == fcDTO.validationAction) {
                    response.put(key, fcDTO);
                }
            }
        }
        return response;
    }

    private void handleResultsValidationApex(Map<String, FileColumnDataDTO> results) {
        Set<String> idKeySet = results.keySet();
        for(String key :idKeySet) {
            FileColumnDataDTO fcDTO = results.get(key);
            if(fcDTO.hasError) {
                fcDTO = new FileColumnDataDTO(fcDTO.errorCode, fcDTO.stackTraceError, fcDTO.messageError);
                addTo(this.mapRowsWithErrors, key, fcDTO);
            } else {
                addTo(this.mapRowsToTransform, key, fcDTO);
            }
        }
    }

    public Set<String> retrieveSetWithValidation() {
        Set<String> apexClassSet = new Set<String>();
        Set<String> colSet = this.mapColsWithApexAction.keySet();
        //retrieve columns with apex validation class
        for(String key :colSet) {
            List<FileColumnDataDTO> dtoList = this.mapColsWithApexAction.get(key);
            for(FileColumnDataDTO fcDTO :dtoList) {
                apexClassSet.add(fcDTO.validationAction);
            }
            break;
        }
        return apexClassSet;
    }

    global with sharing class FileColumnDataDTO {
        public String errorCode { get; set; }
        public String stackTraceError { get; set; }
        public String messageError { get; set; }
        public Boolean hasError { get; set; }
        public String status { get; set; }
        public String validationAction { get; set; }
        public String regExpr { get; set; }
        public String columnName { get; set; }
        public Boolean mandatory { get; set; }
        public Decimal index { get; set; }
        public String value { get; set; }

        public FileColumnDataDTO(FileImportDataRow__c fidr, FileTemplateColumn__c ftc, String value) {
            this.validationAction = ftc.ValidationAction__c;
            this.regExpr = ftc.ValidationRegex__c;
            this.columnName = ftc.HeaderName__c;
            this.mandatory = ftc.Mandatory__c;
            this.index = ftc.Index__c;
            this.value = value;
            this.hasError = false;
        }

        public FileColumnDataDTO(String errorCode, String stackTraceError, String messageError) {
            this.errorCode = errorCode;
            this.stackTraceError = stackTraceError;
            this.messageError = messageError;
            this.status = 'Rejected';
            this.hasError = true;
        }
    }
}