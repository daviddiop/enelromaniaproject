/**
 * Created by Boubacar Sow on 16/07/2020.
 */

@IsTest
private class MRO_LC_ContractMergeTst {

    @TestSetup
    static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('CN010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ContractMerge').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'ContractMerge', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('CN010', 'RC010', 'ContractMerge', recordTypeEle, '');
        insert phaseDI010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        /*Account traderAccount = MRO_UTL_TestDataFactory.account().traderAccount().build();
        traderAccount.Name = 'traderAccount';*/
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        NE__Product__c comProduct = new NE__Product__c();
        comProduct.Name = 'comProduct1';
        insert comProduct;
        Product2 product1 = TestDataFactory.product2().build();
        product1.CommercialProduct__c = comProduct.Id;
        insert product1;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().setAccount(listAccount[0].Id).build();
        contractAccount.SelfReadingPeriodEnd__c = '4';
        insert  contractAccount;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.ConsumerType__c = 'Residential';
        servicePoint.Trader__c = accountTrader.Id;
        insert servicePoint ;
        ServiceSite__c serviceSite = MRO_UTL_TestDataFactory.serviceSite().createServiceSite().build();
        insert serviceSite;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        //contract.EndDate= Date.newInstance(2030, 02, 17);
        insert contract;
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Product__c = product1.Id;
            supply.Status__c = 'Active';
            supply.ServiceSite__c = serviceSite.Id;
            supply.Market__c = 'Free';
            supply.ServicePoint__c = servicePoint.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        List<Dossier__c> dossiers = new List<Dossier__c>();
        Dossier__c dossier1 = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier1.Account__c = listAccount[0].Id;
        dossier1.Phase__c = 'RE010';
        dossiers.add(dossier1);
        Dossier__c dossier2 = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier2.Account__c = listAccount[0].Id;
        dossier2.Phase__c = 'RE010';
        dossier2.SLAExpirationDate__c = System.today();
        dossiers.add(dossier2);
        insert dossiers;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossiers[0].Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ContractMerge').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
        Index__c index = MRO_UTL_TestDataFactory.index().createIndex().build();
        index.Case__c = caseList[0].Id;
        index.MeterNumber__c = '12334FF';
        index.ReadingDate__c = System.today();
        insert  index;
    }

    @IsTest
    public static void initializeTest(){
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        List<Dossier__c> dossiers = [
            SELECT Id
            FROM Dossier__c
            LIMIT 2
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossiers[0].Id,
            'interactionId' => interaction.Id,
            'genericRequestId' => dossiers[1].Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ContractMerge', 'Initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('dossierId') == dossiers[0].Id);

        Test.stopTest();
    }

    @IsTest
    public static void setChannelAndOriginTest(){
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        List<Dossier__c> dossiers = [
            SELECT Id
            FROM Dossier__c
            LIMIT 2
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossiers[0].Id,
            'origin' => 'Email',
            'channel' => 'Back Office'
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ContractMerge', 'setChannelAndOrigin', inputJSON, true);
        //Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, response == true );

        Test.stopTest();
    }

    @IsTest
    public static void CreateCaseTest(){
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        List<Dossier__c> dossiers = [
            SELECT Id
            FROM Dossier__c
            LIMIT 2
        ];
        List<Case> caseList = [
            SELECT Id
            FROM Case
            LIMIT 5
        ];

        List<Supply__c> supplies = [
            SELECT Id
            FROM Supply__c
            LIMIT 5
        ];
        List<String> supplyIds = new List<String>();
        for(Integer i = 0; i < supplies.size(); i++){
            supplyIds.add(supplies[i].Id);
        }
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossiers[0].Id,
            'commodity' => 'Gas',
            'supplyIds' => JSON.serialize(supplyIds),
            'caseList' => JSON.serialize(caseList)
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'CreateCase', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('market') == 'Free' );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void setContractAndContractAccountTest(){
        Contract contract = [
            SELECT Id
            FROM Contract
            LIMIT 1
        ];
        ContractAccount__c contractAccount = [
            SELECT Id
            FROM ContractAccount__c
            LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id
            FROM Case
            LIMIT 5
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'contractId' => contract.Id,
            'contractAccountId' => contractAccount.Id,
            'caseList' => JSON.serialize(caseList)
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'setContractAndContractAccount', inputJSON, true);
            System.assertEquals(true, response != null );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void UpdateCasesTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Contract contract = [
            SELECT Id
            FROM Contract
            LIMIT 1
        ];
        ContractAccount__c contractAccount = [
            SELECT Id
            FROM ContractAccount__c
            LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id
            FROM Case
            LIMIT 5
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'contractId' => contract.Id,
            'contractAccountId' => contractAccount.Id,
            'caseList' => JSON.serialize(caseList),
            'isDraft' => JSON.serialize(true)
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'UpdateCases', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == false );

            inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'contractId' => contract.Id,
                'contractAccountId' => contractAccount.Id,
                'caseList' => JSON.serialize(caseList),
                'isDraft' => JSON.serialize(false)
            };
            response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'UpdateCases', inputJSON, true);
            result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == false );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void setEffectiveDateTest(){
        List<Case> caseList = [
            SELECT Id
            FROM Case
            LIMIT 5
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'effectiveDate' => JSON.serialize(System.today()),
            'caseList' => JSON.serialize(caseList)
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'setEffectiveDate', inputJSON, true);
            System.assertEquals(true, response != null );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void CancelProcessTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id
            FROM Case
            LIMIT 5
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'caseList' => JSON.serialize(caseList)
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'CancelProcess', inputJSON, true);
            //Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, response != null );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void updateCommodityToDossierTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'commodity' => 'Gas'
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'updateCommodityToDossier', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('dossierId') == dossier.Id );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void getSelfReadingIntervalTest(){
        ContractAccount__c contractAccount = [
            SELECT Id
            FROM ContractAccount__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'contractAccountId' => contractAccount.Id
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'getSelfReadingInterval', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('isInsideInterval') != null );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void getMetersTest(){
        ContractAccount__c contractAccount = [
            SELECT Id
            FROM ContractAccount__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'contractAccountId' => contractAccount.Id
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'getMeters', null, false);
            //Map<String, Object> result = (Map<String, Object>) response;
            //System.assertEquals(true, result.get('metersJson') != null );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void getSavedMetersTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'getSavedMeters', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == false );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void createIndexTest(){
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c
            FROM Case
            LIMIT 5
        ];

        MRO_SRV_Index.MeterQuadrant meterQuadrant = new MRO_SRV_Index.MeterQuadrant();
        meterQuadrant.index = 0123544;
        meterQuadrant.meterNumber = '12334FF';
        meterQuadrant.quadrantName = 'Energie Reactiva Inductva';
        List<MRO_SRV_Index.MeterQuadrant> lMeterQuadrants = new List<MRO_SRV_Index.MeterQuadrant>();
        lMeterQuadrants.add(meterQuadrant);
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'caseList' => JSON.serialize(caseList),
            'meterInfo' => JSON.serialize(lMeterQuadrants)
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'createIndex', inputJSON, true);
            //Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, response == true );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    public static void updateCurrentFlowTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'currentFlow' => ''
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec(
                'MRO_LC_ContractMerge', 'updateCurrentFlow', inputJSON, true);
            System.assertEquals(true, response == true );
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

}