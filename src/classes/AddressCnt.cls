public with sharing class AddressCnt  extends ApexServiceLibraryCnt{

    public class normalize extends  AuraCallable{
        public override  Object perform ( final String jsonInput ){
            Map<String, String> params = asMap(jsonInput);

            List<Map<String, String>> addresses = new List<Map<String, String>>();
            Map<String, String> address = (Map<String, String>)JSON.deserialize(params.get('address'), Map<String, String>.class);
            addresses.add(address);
            return addresses;
        }
    }

    public class getExtraFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String extraFieldsFieldSet = params.get('extraFieldsFieldSet');
            String objectApiName = params.get('objectApiName');
            response = LightningUtils.getFieldSet(objectApiName, extraFieldsFieldSet);
            return  response;
        }
    }

    public with sharing class AddressDTO {
        @AuraEnabled
        public String streetNumber {get; set;}
        @AuraEnabled
        public String streetNumberExtn {get; set;}
        @AuraEnabled
        public String streetName {get; set;}
        @AuraEnabled
        public String streetType {get; set;}
        @AuraEnabled
        public String apartment {get; set;}
        @AuraEnabled
        public String building {get; set;}
        @AuraEnabled
        public String city {get; set;}
        @AuraEnabled
        public String country {get; set;}
        @AuraEnabled
        public String floor {get; set;}
        @AuraEnabled
        public String locality {get; set;}
        @AuraEnabled
        public String postalCode {get; set;}
        @AuraEnabled
        public String province {get; set;}
        @AuraEnabled
        public Boolean addressNormalized {get; set;}
        public AddressDTO() {
        }
    }
}