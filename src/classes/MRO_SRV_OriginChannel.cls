/**
 * Created by goudiaby on 07/01/2020.
 * @version 1.0
 * @description Class Service for Origin Channel Selection
 *          [ENLCRO-325] Channel and Origin Selection - Implementation
 *
 */
public with sharing class MRO_SRV_OriginChannel {

    private static MRO_QR_OriginChannel originChannelQuery = MRO_QR_OriginChannel.getInstance();

    private static Map<String, String> channelApiNameToOrigin = new Map<String, String> {
        'EnelRo' => 'Web',
        'EnelEasy' => 'Web',
        'MyEnel' => 'Web',
        'MagazinEnel' => 'Shop',
        'MagazinEnelPartner' => 'Shop',
        'InfoKiosk' => 'Shop',
        'BackOfficeSales' => 'Shop',
        'BackOfficeCustomerCare' => 'Shop',
        'DirectSales' => 'Shop',
        'IndirectSales' => 'Partner',
        'CallCenter' => 'Partner'
    };

    /**
     * @description Get Class instance
     * @return instance MRO_SRV_OriginChannel Class
     */
    public static MRO_SRV_OriginChannel getInstance() {
        return (MRO_SRV_OriginChannel) ServiceLocator.getInstance(MRO_SRV_OriginChannel.class);
    }

    /**
    * @author  Baba GOUDIABY
    * @description get a Map of Origin Channel Options  (-origin - { channel 1, channel 2})
    * [ENLCRO-325] Channel and Origin Selection - Implementation
    * @param objectApiName
    * @return Map<String, List<ApexServiceLibraryCnt.Option>>
    */
    public Map<String, List<ApexServiceLibraryCnt.Option>> getOriginChannelOptions(String objectApiName) {
        List<ApexServiceLibraryCnt.Option> channelOptions;
        Map<String, List<ApexServiceLibraryCnt.Option>> mapOriginChannelsOptions = new Map<String, List<ApexServiceLibraryCnt.Option>>();

        for (OriginChannelDependency__mdt originChannelDependency : originChannelQuery.getOriginChannelDependencies(objectApiName)) {
            channelOptions = new List<ApexServiceLibraryCnt.Option>();
            String origin = originChannelDependency.OriginPicklistEntry__r.Label;
            String channel = originChannelDependency.ChannelPicklistEntry__r.Label;
            String cusPermissionChannel = String.valueOf(originChannelDependency.ChannelPicklistEntry__r.CustomPermission__c);
            String cusPermissionOrigin = String.valueOf(originChannelDependency.OriginPicklistEntry__r.CustomPermission__c);
            Boolean hasPermissionChannel = FeatureManagement.checkPermission(cusPermissionChannel);
            Boolean hasPermissionOrigin = FeatureManagement.checkPermission(cusPermissionOrigin);
            if (!hasPermissionOrigin) {
                continue;
            }
            ApexServiceLibraryCnt.Option channelOpt = new ApexServiceLibraryCnt.Option(channel, channel);
            if (hasPermissionChannel) {
                if (!mapOriginChannelsOptions.containsKey(origin)) {
                    mapOriginChannelsOptions.put(origin, channelOptions);
                    channelOptions.add(channelOpt);
                } else {
                    mapOriginChannelsOptions.get(origin).add(channelOpt);
                }
            }
        }
        return mapOriginChannelsOptions;
    }

    /**
    * @author  Baba GOUDIABY
    * @description get a Map Selectable Origin Entries (-origin - isNotSelectable)
    * [ENLCRO-325] Channel and Origin Selection - Implementation
    * @return Map<String, Boolean>
    */
    public Map<String, Boolean> getSelectableOriginEntries() {
        Map<String, Boolean> mapOriginChannelsEntries = new Map<String, Boolean>();
        for (OriginChannelPicklistEntry__mdt originChannelPicklist : originChannelQuery.getOriginChannelEntries()) {
            mapOriginChannelsEntries.put(originChannelPicklist.MasterLabel, originChannelPicklist.isNotSelectable__c);
        }
        return mapOriginChannelsEntries;
    }

    public String getOriginByChannelLabel(String channelLabel) {
        List<OriginChannelPicklistEntry__mdt> entryList = originChannelQuery.getOriginChannelEntries();
        Map<String, String> labelToApiName =
            SobjectUtils.mapTwoFields(entryList, OriginChannelPicklistEntry__mdt.MasterLabel, OriginChannelPicklistEntry__mdt.QualifiedApiName);
        String channelApiName = labelToApiName.get(channelLabel);
        if (String.isBlank(channelApiName)) {
            return null;
        }
        return channelApiNameToOrigin.get(channelApiName);
    }

}