/**
 * Created by goudiaby on 29/03/2019.
 */

public with sharing class DocumentBundleItemQueries {

    public static DocumentBundleItemQueries getInstance() {
        return new DocumentBundleItemQueries();
    }

    public List<DocumentBundleItem__c> listDocumentBundleItemsFromValidatableDocs(String dossierId) {
        return [
                SELECT Id,DocumentBundle__c,DocumentType__r.Name,DocumentType__c,
                        Mandatory__c
                FROM DocumentBundleItem__c
                WHERE Id IN (SELECT DocumentBundleItem__c FROM ValidatableDocument__c WHERE Dossier__c = :dossierId)
        ];
    }

}