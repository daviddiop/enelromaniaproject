/**
 * @author  Luca Ravicini
 * @since   Jun 2, 2020
 * @desc   Test for MRO_LC_ContractualDataEdit class
 *
 */
@IsTest
private class MRO_LC_ContractualDataEditTst {
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();

    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 1000.0,SequenceLength__c = 7.0);
        insert sequencer;
        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder1 =  MRO_UTL_TestDataFactory.account().personAccount();
        Account acc1 = AccountBuilder1.build();

        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder2 =  MRO_UTL_TestDataFactory.account().businessAccount();
        Account acc2 = AccountBuilder2.build();
        Database.SaveResult[] lsr = Database.insert(new Account[]{acc1, acc2}, false);

        Dossier__c dossier0 = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.GENERIC_REQUEST).setRequestType(constantsUtl.REQUEST_TYPE_CONTRACTUAL_DATA_CHANGE).build();
        insert dossier0;

        Dossier__c dossier1 = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(constantsUtl.REQUEST_TYPE_CONTRACTUAL_DATA_CHANGE).build();
        insert dossier1;

        Dossier__c dossier2 = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(constantsUtl.REQUEST_TYPE_CONTRACTUAL_DATA_CHANGE).build();
        insert dossier2;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePointGas().build();
        servicePoint.Account__c = acc1.Id;
        insert servicePoint;

        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = acc1.Id;
        contract.Status = constantsUtl.CONTRACT_STATUS_DRAFT;
        insert contract;

        Contract contract2 = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract2.AccountId = acc1.Id;
        contract2.Name = 'contract2';
        insert contract2;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = acc1.Id;
        billingProfile.IBAN__c = null;
        insert billingProfile;

        BillingProfile__c billingProfile2 = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile2.Account__c = acc1.Id;
        billingProfile2.IBAN__c = null;
        insert billingProfile2;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        contractAccount.Account__c = acc1.Id;
        contractAccount.BillingProfile__c = billingProfile.Id;
        insert contractAccount;

        ContractAccount__c contractAccount2 = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        contractAccount2.Account__c = acc1.Id;
        contractAccount2.BillingProfile__c = billingProfile2.Id;
        insert contractAccount2;

        Schema.RecordTypeInfo caseRecordTypeInfo = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(constantsUtl.CASE_RECORD_TYPE_CONTRACTUAL_DATA_CHANGE);
        Id caseRecordTypeId = caseRecordTypeInfo.getRecordTypeId();
        Case case1 = MRO_UTL_TestDataFactory.caseRecordBuilder().newCase().build();
        case1.Dossier__c = dossier2.Id;
        case1.AccountId = acc1.Id;
        case1.RecordTypeId = caseRecordTypeId;
        case1.Contract__c = contract.Id;
        case1.Contract2__c = contract2.Id;
        case1.ContractAccount__c = contractAccount.Id;
        case1.ContractAccount2__c = contractAccount2.Id;
        case1.BillingProfile__c = billingProfile.Id;
        case1.BillingProfile2__c = billingProfile2.Id;
        insert case1;

    }

    @IsTest
    static void getContractData() {
        Case myCase = [
                SELECT Id, Dossier__c, Contract__c, Contract2__c, ContractAccount__c, ContractAccount2__c,
                        Supply__c, BillingProfile__c, BillingProfile2__c
                FROM Case
        ];
        Contract contract = [
                SELECT Id, ConsumptionConventions__c
                FROM Contract WHERE Id =: myCase.Contract__c
        ];
        contract.ConsumptionConventions__c = true;
        update contract;

        ServicePoint__c servicePoint = [
                SELECT Id
                FROM ServicePoint__c
                LIMIT 1
        ];

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().build();
        supply.ServicePoint__c = servicePoint.Id;
        supply.Contract__c = contract.Id;
        insert supply ;

        myCase.Supply__c = supply.Id;
        update myCase;

        String subProcess = constantsUtl.SUB_PROCESS_CHNG_CONTRACTED_QUANTITIES;

        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => supply.Id,
                'caseId' => myCase.Id,
                'subProcess' => subProcess
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_ContractualDataEdit', 'getContractData', inputJSON, true);
            Test.stopTest();

            System.assertEquals(false, response.get('error'));
            Supply__c responseSupply = (Supply__c)response.get('supply');
            System.assertEquals(supply.Id, responseSupply.Id);
            Case responseCase = (Case)response.get('case');
            System.assertEquals(myCase.Id, responseCase.Id);
            Contract responseContract = (Contract)response.get('contract');
            System.assertEquals(contract.Id, responseContract.Id);
            ContractAccount__c responseContractAccount = (ContractAccount__c)response.get('contractAccount');
            System.assertEquals(myCase.ContractAccount__c, responseContractAccount.Id);
            System.assertEquals(constantsUtl.CASE_START_PHASE, response.get('casePhase'));
            System.assertEquals(constantsUtl.CASE_STATUS_DRAFT, response.get('caseStatus'));
            System.assertEquals(constantsUtl.CONTRACT_STATUS_DRAFT, response.get('contractStatus'));

        } catch (Exception exc) {

        }
    }

    @IsTest
    static void getContractDataError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => '000'
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_ContractualDataEdit', 'getContractData', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void insertConsumptionList() {
        Case myCase = [
                SELECT Id, Dossier__c, Contract__c, Contract2__c, ContractAccount__c, ContractAccount2__c,
                        Supply__c, BillingProfile__c, BillingProfile2__c, RecordType.Name
                FROM Case
        ];
        Contract contract = [
                SELECT Id, ConsumptionConventions__c
                FROM Contract WHERE Id =: myCase.Contract__c
        ];
        contract.ConsumptionConventions__c = true;
        update contract;

        ServicePoint__c servicePoint = [
                SELECT Id
                FROM ServicePoint__c
                LIMIT 1
        ];

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().build();
        supply.ServicePoint__c = servicePoint.Id;
        supply.Contract__c = contract.Id;
        insert supply ;

        myCase.Supply__c = supply.Id;
        update myCase;
        List<Consumption__c> consumptionList = new List<Consumption__c>();
        Consumption__c consumption = new Consumption__c(Commodity__c = supply.RecordType.Name, QuantityJanuary__c = 10.0, QuantityFebruary__c= 10.0,
                QuantityMarch__c = 10.0, QuantityApril__c = 10.0, QuantityMay__c = 10.0, QuantityJune__c = 10.0, QuantityJuly__c = 10.0,
                QuantityAugust__c = 10.0, QuantitySeptember__c = 10.0, QuantityOctober__c = 10.0, QuantityNovember__c = 10.0, QuantityDecember__c = 10.0);

        consumptionList.add(consumption);
        Map<String, Object> inputJSON = new Map<String, Object>{
                'supplyId' => supply.Id,
                'caseId' => myCase.Id,
                'consumptionList' => JSON.serializePretty(consumptionList)
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_ContractualDataEdit', 'insertConsumptionList', inputJSON, true);
            Test.stopTest();

            System.assertEquals(false, response.get('error'));
            List<Consumption__c> loadedConsumptions = [
                    SELECT Id
                    FROM Consumption__c WHERE Supply__c =: supply.Id
            ];
            System.assert(loadedConsumptions.size() == consumptionList.size());

        } catch (Exception exc) {

        }
    }

    @IsTest
    static void insertConsumptionListError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => '000'
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_ContractualDataEdit', 'insertConsumptionList', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

}