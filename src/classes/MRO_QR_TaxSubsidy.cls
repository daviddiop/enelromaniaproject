/**
 * Created by David diop on 18.11.2019.
 */

public with sharing class MRO_QR_TaxSubsidy {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();
    public static MRO_QR_TaxSubsidy getInstance() {
        return (MRO_QR_TaxSubsidy) ServiceLocator.getInstance(MRO_QR_TaxSubsidy.class);
    }

    /**
  * @author  David DIOP
  * @description getTaxSubsidyByCaseId
  * [192] Tax change
  * @date 18/11/2019
  * @param caseId
  * @return  List<TaxSubsidy__c>
  */
    public List<TaxSubsidy__c> getTaxSubsidyByCaseId(String caseId) {
        List<TaxSubsidy__c> subsidyList = [
                SELECT Id, Case__c,Name,Supply__c,Supply__r.CompanyDivision__c,RecordTypeId,RecordType.Name
                FROM TaxSubsidy__c
                WHERE Case__c  = :caseId AND Status__c = 'Activating'
        ];
        return subsidyList;
    }

    public List<TaxSubsidy__c> getTaxSubsidyByDossier(String dossierId) {
        List<String> status = new List<String>{
            'Activating', 'Active', 'Not Active'
        };
        List<TaxSubsidy__c> subsidyList = [
            SELECT Id, Case__c,Name, Status__c, Supply__c,Supply__r.CompanyDivision__c,Supply__r.Name,
                RecordTypeId,RecordType.Name,Case__r.Dossier__c,Case__r.SubProcess__c
            FROM TaxSubsidy__c
            WHERE Case__r.Dossier__c = :dossierId AND Status__c IN :status
        ];
        return subsidyList;
    }

    /**
    * @author  David DIOP
    * @description getTaxSubsidyByAccountId
    * [192] Tax change
    * @date 17/03/2020
    * @param accountId
    * @return  List<TaxSubsidy__c>
    */
    public List<TaxSubsidy__c> getTaxSubsidyByAccountId(String accountId) {
        List<String> status = new List<String>{
            'Active','Terminating'
        };

        List<TaxSubsidy__c> subsidyList = [
            SELECT Id,Customer__c,Name, Status__c, Case__c,Case__r.Id,Supply__c,
                Supply__r.CompanyDivision__c,RecordTypeId,RecordType.Name, StartDate__c, EndDate__c
            FROM TaxSubsidy__c
            WHERE Customer__c = :accountId AND Status__c IN :status
            AND  EndDate__c >: System.today() AND Case__r.Status  =: CONSTANTS.CASE_STATUS_EXECUTED
        ];
        //Case__r.TechnicalDetails__c = NULL AND
        return subsidyList;
    }

    public List<TaxSubsidy__c> getTaxSubsidyByParentId(Set<Id> parentIds,String accountId,String dossierId) {
       /* List<Case> caseRecordList = [
                SELECT Id,TechnicalDetails__c
                FROM Case
                WHERE Id IN :parentIds
        ];
        List<String> response = new List<String>();
        for(Id caseIds:parentIds){
            response.add(caseIds);
        }
        Set<Id> CaseIds = new Set<Id>();*/
        /*for (Case caseRecord : caseRecordList) {
            response.add(caseRecord.TechnicalDetails__c);
        }*/
        /*System.debug('response'+response);
        System.debug('parentIds'+parentIds);*/
        List<TaxSubsidy__c> subsidyList = [
                SELECT Id,Customer__c,Name, Case__c,Case__r.TechnicalDetails__c,Case__r.Dossier__c ,Supply__c,Supply__r.CompanyDivision__c,RecordTypeId,RecordType.Name,EndDate__c
                FROM TaxSubsidy__c
                WHERE Customer__c =: accountId AND Case__r.Dossier__c =:dossierId
        ];
        return subsidyList;
        /*return new Map<Id, TaxSubsidy__c>([
                SELECT Id,Customer__c,Name, Case__c,Case__r.TechnicalDetails__c ,Supply__c,Supply__r.CompanyDivision__c,RecordTypeId,RecordType.Name
                FROM TaxSubsidy__c
                WHERE  Case__c IN :parentIds AND Customer__c =: accountId
        ]);*/
    }

    public List<TaxSubsidy__c> getTaxSubsidyByIds(Set<Id> Ids) {
        return new List<TaxSubsidy__c>([
                SELECT Id,Customer__c,Name, Case__c,Case__r.TechnicalDetails__c ,Supply__c,Supply__r.CompanyDivision__c,RecordTypeId,RecordType.Name
                FROM TaxSubsidy__c
                WHERE  Id IN :Ids
        ]);
    }

    /**
    * @author  David DIOP
    * @description getTaxSubsidyById  for get supply and recordTypeId
    * [192] Tax change
    * @date 18/11/2019
    * @param TaxSubsidy
    * @return  TaxSubsidy
    */
    public TaxSubsidy__c getTaxSubsidyById(String recordId) {
        List<TaxSubsidy__c> subsidyList = [
            SELECT Id,Name,Supply__c,Institution__c,RecordTypeId,RecordType.Name,Case__c,
                Case__r.Dossier__c, Case__r.TechnicalDetails__c, AmountEnergy__c,EndDate__c,StartDate__c
            FROM TaxSubsidy__c
            WHERE Id = :recordId
        ];
        return subsidyList.get(0);
    }
    /**
    * @author  David DIOP
    * @description getByIds   get list TaxSubsidy related to the caseId
    * used in commit Service
    * @date 21/11/2019
    * @param Set<Id> taxSubsidyId
    * @return  Map<Id, TaxSubsidy__c>
    */
    public Map<Id, TaxSubsidy__c> getByCaseIds(Set<Id> caseIds) {
        return new Map<Id, TaxSubsidy__c>([
            SELECT Id, Status__c,Case__c,Case__r.TechnicalDetails__c,EndDate__c,StartDate__c,Case__r.IntegrationKey__c,Supply__c,Supply__r.ServiceSite__c,RecordTypeId,RecordType.Name
            FROM TaxSubsidy__c
            WHERE Case__c IN :caseIds
        ]);
    }

    /**
     * @author Boubacar Sow
     * @date 26/06/2020
     * @description  [ENLCRO-1010] Tax changes - Process Changes
     * @param caseId
     *
     * @return
     */
    public TaxSubsidy__c getByCaseId(Id caseId) {
        List<TaxSubsidy__c> subsidyList = [
            SELECT Id, Name, Supply__c, Status__c,
                Case__c,Case__r.TechnicalDetails__c
            FROM TaxSubsidy__c
            WHERE Case__c = :caseId
        ];
        return subsidyList.get(0);
    }

}