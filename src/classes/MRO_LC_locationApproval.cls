/**
 * Created by David DIOP on 03.02.2020.
 */

public with sharing class MRO_LC_locationApproval extends ApexServiceLibraryCnt {

    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    static Constants constantSrv = Constants.getAllConstants();
    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();

    static ServiceSiteQueries servSiteQueries = ServiceSiteQueries.getInstance();
    static MRO_QR_ServiceSite servSiteQr = MRO_QR_ServiceSite.getInstance();
    static List<ApexServiceLibraryCnt.Option> serviceSitePicklistValues  = new List<ApexServiceLibraryCnt.Option>();

    private static MRO_QR_Account mroQrAccount = MRO_QR_Account.getInstance();
    private static MRO_QR_User userQuery = MRO_QR_User.getInstance();


    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Connection').getRecordTypeId();
    static String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Location_Approval').getRecordTypeId();
    static String caseDraftStatus = constantSrv.CASE_STATUS_DRAFT ;
    static String caseNewStatus = constantSrv.CASE_STATUS_NEW;
    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            System.debug('accountId'+accountId);
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestId = params.get('genericRequestId');
            Map<String, Object> response = new Map<String, Object>();
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            try {
               /* if (String.isNotBlank(dossierId)) {
                    Dossier__c dossierCheckParentId = dossierQuery.getById(dossierId);
                    if (String.isNotBlank(dossierCheckParentId.Parent__c)) {
                        dossierId = dossierCheckParentId.Id;
                    } else if (dossierCheckParentId.RecordType.DeveloperName == 'GenericRequest') {
                        genericRequestId = dossierId;
                        dossierId = '';
                    }
                }*/

                Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'LocationAproval');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSr.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                if (String.isNotBlank(dossierId)) {
                    Contact contactRecord = new Contact();
                    List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
                    if(cases.size()>0){
                        if(String.isNotBlank(cases[0].ContactId)) {
                            contactRecord = contactQuery.getById(cases[0].ContactId);
                            response.put('contactRecord', contactRecord);
                        }
                    }
                    response.put('caseRecord', cases);
                }
                Account acc = mroQrAccount.findAccount(accountId);
                List<ServiceSite__c> serviceSite = servSiteQueries.getListServiceSite(accountId);
                List<String> sitesAddress = new List<String>();
                serviceSitePicklistValues = new List<ApexServiceLibraryCnt.Option>();
                if(!serviceSite.isEmpty()) {
                    for (ServiceSite__c servSite : serviceSite) {
                        if(servSite.Id != null) {
                            sitesAddress = OpportunityServiceItemCnt.serviceSiteOsiAddress(servSite, null);
                            serviceSitePicklistValues.add(new ApexServiceLibraryCnt.Option(String.join(sitesAddress, '  '), servSite.Id));
                        }
                    }
                }
                String code = 'LocationApprovalTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }
                response.put('serviceSite',serviceSitePicklistValues);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);

                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('accountId', accountId);
                response.put('account', acc);
                response.put('accountPersonRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId());
                response.put('accountPersonProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId());
                response.put('accountBusinessRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId());
                response.put('accountBusinessProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId());
                response.put('caseRecordType',Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Location_Approval').getRecordTypeId());
                response.put('error', false);


            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class createServiceSite extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            serviceSiteInput serviceInformationInput = (serviceSiteInput) JSON.deserialize(params.get('serviceSite'), serviceSiteInput.class);
            ServiceSite__c service = serviceInformationInput.serviceSite;
            try {
                databaseSrv.insertSObject(serviceInformationInput.serviceSite);
                response.put('serviceRecord',serviceInformationInput.serviceSite);
                response.put('error',false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    /**
     * @author Boubacar Sow
     * @date 07/12/2020
     * @description [ENLCRO-1894] Location Approval - Distributor and company division - Changes
     */
    public class getDistributorByServiceSiteAddres extends AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String serviceSiteId = params.get('serviceSiteId');

            if (String.isBlank(serviceSiteId)) {
                throw new WrtsException(System.Label.ServiceSite + ' - ' + System.Label.Required);
            }

            try {
                ServiceSite__c serviceSite = servSiteQr.getById(serviceSiteId);
                User currentUserInfos = userQuery.getStateProvince(UserInfo.getUserId());
                response.put('companyDivisionId',currentUserInfos.CompanyDivisionId__c);
                if (serviceSite != null && String.isNotBlank(serviceSite.SiteProvince__c)) {
                    List<Account> distributors = mroQrAccount.getDistributorEnel();
                    System.debug('###distributors '+distributors);
                    if(distributors != null){
                        Boolean isServiceSiteProvinceMatch = false;
                        for(Account distributor : distributors){
                            if(distributor.DistributionProvincesDetails__c != null){
                                Map<String, String> provinciesMap = (Map<String, String>)JSON.deserialize(distributor.DistributionProvincesDetails__c.toLowerCase(), Map<String, String>.class);
                                if(provinciesMap.values().contains(serviceSite.SiteProvince__c.toLowerCase())){
                                    response.put('distributor',distributor);
                                    response.put('error',false);
                                    System.debug('###distributor '+distributor);
                                    isServiceSiteProvinceMatch = true;
                                    break;
                                }else {
                                    isServiceSiteProvinceMatch = false;
                                }
                            }
                        }
                        if (!isServiceSiteProvinceMatch) {
                            throw new WrtsException(System.Label.ProvinceAddressAndDistributor);
                        }

                    }else {
                        throw new WrtsException(System.Label.ProvinceAddressAndDistributor);
                    }

                }else {
                    throw new WrtsException(System.Label.ProvinceIsEmpty);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }

    }


    /**
     * @author Boubacar Sow
     * @date 16/11/2020
     * @description [ENLCRO-1774] Service Site Addresses should have Edit functionality.
     *
     */
    public class getServiceSiteAddressField extends AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String serviceSiteId = params.get('serviceSiteId');

            if (String.isBlank(serviceSiteId)) {
                throw new WrtsException(System.Label.ServiceSite + ' - ' + System.Label.Required);
            }
            try {
                ServiceSite__c serviceSite = servSiteQr.getById(serviceSiteId);
                if (serviceSite != null) {
                    MRO_SRV_Address.AddressDTO addressFromServiceSite = MRO_LC_locationApproval.copyServiceSiteAddressToDTO(serviceSite);
                    response.put('error', false);
                    response.put('serviceSiteAddress', addressFromServiceSite);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }

    }

    /**
     *@author Boubacar Sow
     * @date  18/11/2020
     * @description [ENLCRO-1774] Service Site Addresses should have Edit functionality.
     */
    public class getNewServiceSite extends AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Id> serviceSiteIds = (List<Id>) JSON.deserialize(params.get('serviceSitesIds'), List<Id>.class);

            if (serviceSiteIds.isEmpty()) {
                throw new WrtsException(System.Label.ServiceSite + ' - ' + System.Label.Required);
            }
            try {
                Set<Id> ids = new Set<Id>();
                for (Id serviceSiteId: serviceSiteIds){
                    ids.add(serviceSiteId);
                }
                Map<Id, Supply__c> idSupplyMap = supplyQuery.getSupplyByServiceSiteIds(ids);
                if (idSupplyMap != null) {
                    List<Supply__c> supplies =idSupplyMap.values() ;
                    for (Supply__c supply: supplies){
                        ids.remove(supply.ServiceSite__c);
                    }
                }
                response.put('newServiceSite',ids);
                response.put('error',false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }

    }

    /**
     * @author Boubacar Sow
     * @date 16/11/2020
     * @description [ENLCRO-1774] Service Site Addresses should have Edit functionality.
     * @param serviceSite
     *
     * @return
     */
    public static MRO_SRV_Address.AddressDTO copyServiceSiteAddressToDTO(ServiceSite__c serviceSite) {
        MRO_SRV_Address.AddressDTO addressDTO = new MRO_SRV_Address.AddressDTO(serviceSite, 'Site');
        return addressDTO;
    }

    /**
     * @author Boubacar Sow
     * @date  17/11/2020
     * @description  [ENLCRO-1774] Service Site Addresses should have Edit functionality.
     */
    public class updateServiceSite extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            serviceSiteInput serviceInformationInput = (serviceSiteInput) JSON.deserialize(params.get('serviceSite'), serviceSiteInput.class);
            ServiceSite__c serviceSite = serviceInformationInput.serviceSite;
            try {
                System.debug('###serviceSite '+serviceSite);
                if (serviceSite != null && String.isNotBlank(serviceSite.Id)) {
                    databaseSrv.updateSObject(serviceSite);
                    System.debug('###serviceSite '+serviceSite);
                }
                response.put('serviceSiteRecord',serviceSite);
                response.put('error',false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }


    public class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String caseString = params.get('caseFields');
            Case caseInformationInput = (Case) JSON.deserialize(caseString, Case.class);
            String serviceSiteId = params.get('serviceSiteId');
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String billingId = params.get('billingId');
            String originSelected = params.get('origin');
            String channelSelected = params.get('channel');
            String companyDivisionId = params.get('companyDivisionId');
            try {
                if (String.isBlank(serviceSiteId)) {
                    throw new WrtsException('Service Site  - ' + System.Label.MissingId);
                }
                Case caseRecord = caseSrv.insertCaseWithLocationApproval(caseInformationInput,accountId,dossierId,caseRecordType,
                        'RE010',serviceSiteId,billingId,originSelected,channelSelected,caseNewStatus);
                if (caseRecord.Status != caseDraftStatus) {
                    Date effectiveDate = caseRecord.ReferenceDate__c.addDays(1);
                    for (integer i = 0; i < 4;) {
                        while (MRO_UTL_Date.isNotWorkingDate(effectiveDate)) {
                            effectiveDate = effectiveDate.addDays(1);
                        }
                        effectiveDate = effectiveDate.addDays(1);
                        i++;
                    }
                    while (MRO_UTL_Date.isNotWorkingDate(effectiveDate)) {
                        effectiveDate = effectiveDate.addDays(1);
                    }
                    caseRecord.SLAExpirationDate__c  = effectiveDate;
                    caseRecord.CompanyDivision__c = companyDivisionId;
                    caseRecord.CaseTypeCode__c = 'C27';
                    databaseSrv.updateSObject(caseRecord);
                }
                Dossier__c dossier = dossierSr.updateDossierLocation(dossierId,channelSelected,originSelected,caseInformationInput.SubType__c);
                dossier.CompanyDivision__c = companyDivisionId;
                databaseSrv.updateSObject(dossier);
                caseSrv.applyAutomaticTransitionOnDossierAndCases(dossierId);
                response.put('error',false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }


    public class saveDraftRequest extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String caseString = params.get('caseFields');
            Case caseInformationInput = (Case) JSON.deserialize(caseString, Case.class);
            String serviceSiteId = params.get('serviceSiteId');
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String billingId = params.get('billingId');
            String originSelected = params.get('origin');
            String channelSelected = params.get('channel');
            String companyDivisionId = params.get('companyDivisionId');

            try {

                if (String.isBlank(serviceSiteId)) {
                    throw new WrtsException('Service Site  - ' + System.Label.MissingId);
                }
                Case caseRecord = caseSrv.insertCaseWithLocationApproval(caseInformationInput, accountId, dossierId, caseRecordType,
                        'RE010', serviceSiteId, billingId, originSelected, channelSelected,caseDraftStatus);
                if (caseRecord.Status == caseDraftStatus) {
                    caseRecord.CompanyDivision__c = companyDivisionId;
                    databaseSrv.updateSObject(caseRecord);
                }
                Dossier__c dossier = dossierSr.updateDossierChannel(dossierId, channelSelected, originSelected);
                dossier.CompanyDivision__c = companyDivisionId;
                databaseSrv.updateSObject(dossier);

                dossierSr.updateDossierChannel(dossierId, channelSelected, originSelected);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    /**
 * @author David DIOP
 * @description  CancelProcess method to cancel dossier statut  for Location Approval
 */
    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String caseId = params.get('caseId');
            dossierSr.setCanceledOnDossier(dossierId);
            response.put('error', false);
            return response;
        }
    }

    public with sharing class getRefundMethod extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String billingProfileId = params.get('billingProfileId');
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                String refundMethod = billingProfileQuery.getById(billingProfileId).RefundMethod__c;
                response.put('refundMethod', refundMethod);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class serviceSiteInput {
        @AuraEnabled
        public ServiceSite__c serviceSite { get; set; }
    }
    public class caseInputFields {
        @AuraEnabled
        public Case caseRecord { get; set; }
    }


}