/**
 * Created by Vlad Mocanu on 24/10/2019.
 */

public interface IJsonBuilder {
    SObject getData(String recordId);
}