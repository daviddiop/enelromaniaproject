/**
 * Created by giupastore on 14/05/2020.
 */

public with sharing class MRO_SRV_ReminderService {

	public static MRO_SRV_ReminderService getInstance() {
		return (MRO_SRV_ReminderService) ServiceLocator.getInstance(MRO_SRV_ReminderService.class);
	}

	/**
	 * @author    Francesco Sabatini (francesco.sabatini@webresults.it)
	 * @date      04/02/2020
	 * @description Apex method used to insert the reminders.
	 *              It accepts in input a list containing the request wrappers and returns a Map in which
	 *              the key is the request and the value is the list of the inserted reminder ids.
	 *              The reminder status is initially set to “Pending“
	 * @param  List<MRO_ReminderServiceRequestWrapper>
	 * @return   Map<MRO_ReminderServiceRequestWrapper, List<Id>>
	 */
	public void insertReminder(List<MRO_ReminderServiceRequestWrapper> wrapperList) {
		Savepoint sp = Database.setSavepoint();
		try {
			Map<MRO_ReminderServiceRequestWrapper, List<Id>> response = new Map<MRO_ReminderServiceRequestWrapper, List<Id>>();
			performInsert(wrapperList);
		} catch(Exception e) {
			Database.rollback(sp);
			System.debug('@@@@@@@@@ Exception occured: ' + e.getMessage());
		}
	}

	private void performInsert(List<MRO_ReminderServiceRequestWrapper> wrapperList) {
		List<CustomReminder__c> reminders = new List<CustomReminder__c>();
		String recordRelatedId;
		String status = 'Pending';
		List<ID> relatedObjectIds = new List<ID>();
		List<ID> crIds = new List<ID>();
		Set<String> codes = new Set<String>();
		codes.addAll(getCodes(wrapperList));
		Map<String, CustomReminderConfiguration__mdt> mapCodeCfg = createMapCfg(codes);
		DateTime d = System.Now();

		for (MRO_ReminderServiceRequestWrapper elem :wrapperList) {
			List<String> l = elem.reminderCodes;
			recordRelatedId = elem.recordRelatedId;
			relatedObjectIds.add(recordRelatedId);
			for(String code :l) {
				CustomReminder__c cr = new CustomReminder__c();
				Integer hoursToAdd = (Integer) mapCodeCfg.get(code).ReminderHours__c;
				cr.RecordId__c = recordRelatedId;
				cr.Status__c = status;
				cr.Code__c = code;
				cr.ExecutionDateTime__c = d.addHours(hoursToAdd);
				reminders.add(cr);
			}
		}
		insert reminders;
	}

	private Set<String> getCodes(List<MRO_ReminderServiceRequestWrapper> wrapperList) {
		Set<String> answer = new Set<String>();
		for(MRO_ReminderServiceRequestWrapper rsrw :wrapperList) {
			answer.addAll(rsrw.reminderCodes);
		}
		return answer;
	}


	private Map<String, CustomReminderConfiguration__mdt> createMapCfg(Set<String> codes) {
		List<String> codelist = new List<String>(codes);
		List<CustomReminderConfiguration__mdt> cfgCrList = MRO_QR_ReminderQueries.getInstance().getCfgReminderByCode(codelist);
		Map<String, CustomReminderConfiguration__mdt> mapCodeCfg = new Map<String, CustomReminderConfiguration__mdt>();
		for(String key :codelist) {
			for(CustomReminderConfiguration__mdt crCfg :cfgCrList) {
				if(key == crCfg.Code__c) {
					mapCodeCfg.put(key, crCfg);
				}
			}
		}
		return mapCodeCfg;
	}


	/**
	 * @author    Francesco Sabatini (francesco.sabatini@webresults.it)
	 * @date      04/02/2020
	 * @description Apex method used to update the status of the reminders in “Removed“.
	 * @param  List<Id>
	 * @return   void
	 */
	public void removeReminder(List<Id> reminderIds) {
		Savepoint sp = Database.setSavepoint();
		try {
			List<CustomReminder__c> reminders = MRO_QR_ReminderQueries.getInstance().getByListIds(reminderIds);
			List<CustomReminder__c> remindersToUpdate = new List<CustomReminder__c>();
			for (CustomReminder__c cr :reminders) {
				cr.Status__c = 'Removed';
				remindersToUpdate.add(cr);
			}
			update remindersToUpdate;
		} catch(Exception e) {
			Database.rollback(sp);
		}
	}

	/**
	 * @author    Francesco Sabatini (francesco.sabatini@webresults.it)
	 * @date      04/02/2020
	 * @description Inner class to handle the insert request.
	 *              The class contains the following properties
	 */
	public class MRO_ReminderServiceRequestWrapper {
		public String recordRelatedId { get; set; }
		public List<String> reminderCodes { get; set; }

		public MRO_ReminderServiceRequestWrapper(String recordRelatedId, List<String> reminderCodes) {

			this.recordRelatedId = recordRelatedId;
			this.reminderCodes = reminderCodes;
		}

		public boolean equals(Object obj) {
			MRO_ReminderServiceRequestWrapper other = (MRO_ReminderServiceRequestWrapper) obj;
			if(!(this.recordRelatedId == (other.recordRelatedId))) {
				return false;
			}
			if(this.reminderCodes == null) {
				if(other.reminderCodes != null) {
					return false;
				}
			}
			return true;
		}

		public Integer hashCode() {
			final Integer prime = 31;
			Integer result = 1;
			result = prime * result + ((this.recordRelatedId == null) ? 0 :this.recordRelatedId.hashCode());
			return result;
		}
	}
}