public inherited sharing class OpportunityLineItemQueries {
    
    public static OpportunityLineItemQueries getInstance() {
        return (OpportunityLineItemQueries)ServiceLocator.getInstance(OpportunityLineItemQueries.class);
    }

    public List<OpportunityLineItem> getOLIsByOpportunityId(Id opportunityId) {
        return [
            SELECT Id, Name, Product2Id, Product2.Name,
                    Product2.RecordTypeId, Product2.RecordType.DeveloperName
            FROM OpportunityLineItem
            WHERE OpportunityId = :opportunityId
        ];
    }
}