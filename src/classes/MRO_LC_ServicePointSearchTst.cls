/**
 * Created by moustapha on 28/08/2020.
 */

@IsTest
public with sharing class MRO_LC_ServicePointSearchTst {
    @TestSetup
    private static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        Account myAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        myAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert myAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = myAccount.Id;
        insert billingProfile;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        contractAccount.BillingProfile__c = billingProfile.Id;
        insert contractAccount;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder()
                .setAccount(myAccount.Id)
                .setCompany(companyDivision.Id)
                .setContractAccount(contractAccount.Id)
                .build();
        supply.RecordTypeId = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert supply;

        ServicePoint__c sp = MRO_UTL_TestDataFactory.servicePoint().createServicePointEle().build();
        sp.Account__c = myAccount.Id;
        sp.Code__c = 'ServicePointCode';
        sp.CurrentSupply__c = supply.Id;
        sp.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert sp;
    }
    @isTest
    public static void findCodeTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                WHERE FirstName = 'Person'
                LIMIT 1
        ];
        System.debug('#--# '+account);
        ServicePoint__c sp = [
                SELECT Id,Code__c
                FROM ServicePoint__c
                LIMIT 1
        ];
        System.debug('#--# '+sp);
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'pointCode' => sp.Code__c
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ServicePointSearch', 'findCode', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.debug('#-result-# '+result);
        system.assertEquals(true, result != null );
        Test.stopTest();

    }
    @isTest
    public static void findCodeTm2Test(){
        Account account = [
                SELECT Id,Name
                FROM Account
                WHERE FirstName = 'Person'
                LIMIT 1
        ];
        System.debug('#--# '+account);
        ServicePoint__c sp = [
                SELECT Id,Code__c
                FROM ServicePoint__c
                LIMIT 1
        ];
        System.debug('#--# '+sp);
        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
                LIMIT 1
        ];
        System.debug('#--# '+companyDivision);
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'pointCode' => sp.Code__c,
                'companyDivisionId' => companyDivision.Id,
                'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ServicePointSearch', 'findCodeTm2', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.debug('#-result-# '+result);
        system.assertEquals(true, result != null );
        Test.stopTest();

    }
}