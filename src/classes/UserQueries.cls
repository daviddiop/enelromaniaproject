/**
 * Created by goudiaby on 27/05/2019.
 */

public with sharing class UserQueries {
    public static UserQueries getInstance() {
        return new UserQueries();
    }

    /**
     * Returns the context division's company Id
     * @param userId
     *
     * @return User record
     */
    public User getCompanyDivisionId(String userId) {
        List<User> currentUsers = [
                SELECT Id,CompanyDivisionId__c,CompanyDivisionEnforced__c
                FROM User
                WHERE Id = :userId AND IsActive = TRUE
                LIMIT 1
        ];
        return currentUsers.isEmpty() ? null : currentUsers.get(0);
    }
}