/**
 * Created by Octavian on 11/14/2019.
 */

public with sharing class MRO_LC_SelfReadingWizard extends ApexServiceLibraryCnt {
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_SRV_Dossier dossierServ = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Index indexQuery = MRO_QR_Index.getInstance();
    private static MRO_SRV_Case caseServ = MRO_SRV_Case.getInstance();
    private static MRO_SRV_Index indexServ = MRO_SRV_Index.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('TechnicalRequest').getRecordTypeId();
    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String genericRequestId = params.get('genericRequestId');
            Map<String, Object> response = new Map<String, Object>();
            try {

                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account acc = accountQuery.findAccount(accountId);
                Dossier__c dossier = dossierServ.generateDossier(accountId, dossierId, interactionId, null, dossierRecordType,'SelfReading');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierServ.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                if (String.isNotBlank(dossierId)) {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                    response.put('caseTile', cases);
                }

                String code = 'SelfReadingWizardCodeTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null) {
                    response.put('templateId', scriptTemplate.Id);
                }

                response.put('originSelected', dossier.Origin__c);
                response.put('channelSelected', dossier.Channel__c);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('accountId', accountId);
                response.put('account', acc);
                response.put('error', false);


            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class checkSelectedSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Id> selectedSupplyIds = (List<Id>) JSON.deserialize(params.get('selectedSupplyIds'), List<Id>.class);
            //String selectedSupplyIds = params.get('selectedSupplyIds');
            if (selectedSupplyIds.isEmpty()) {
                throw new WrtsException('Supplies' + ' - ' + System.Label.MissingId);
            }
            try {
                List<Supply__c>  supplies = supplyQuery.getSuppliesByIds(selectedSupplyIds);

                if(!supplies[0].ServicePoint__r?.Distributor__r?.SelfReadingEnabled__c){
                    response.put('error', true);
                    response.put('errorMsg', 'Self Reading not allowed for this distributor');
                    return response;
                }

                response.put('supplies',supplies);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class getSelfReadingInterval extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String dossierId = params.get('dossierId');
            Date todayDate = system.today();
            Integer todayMonth = todayDate.month();
            Integer todayYear= todayDate.year();
            Boolean isInsideInterval = false;
            List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
            if (!cases.isEmpty()) {
                if(String.isNotBlank(cases[0].ContractAccount__r.SelfReadingPeriodEnd__c)){
                    Integer selfReadingPeriodEnd =Integer.valueOf(cases[0].ContractAccount__r.SelfReadingPeriodEnd__c);
                    Date endDate = Date.newInstance(todayYear, todayMonth, selfReadingPeriodEnd);
                    if (todayDate.day() > selfReadingPeriodEnd) {
                        endDate.addMonths(1);
                    }
                    Date beginDate = null;
                    System.debug('RecordType: ' + cases[0].RecordType.DeveloperName);
                    System.debug('Self Reading Period End: ' + cases[0].ContractAccount__r.SelfReadingPeriodEnd__c);
                    System.debug('Self Reading Period End Integer: ' + Integer.valueOf(cases[0].ContractAccount__r.SelfReadingPeriodEnd__c));
                    if (cases[0].RecordType.DeveloperName == 'SelfReading_GAS' && selfReadingPeriodEnd == 26) {
                        beginDate = endDate.addDays(-5);
                        System.debug('Start Date 1: ' + beginDate);
                    } else {
                        beginDate = endDate.addDays(-10);
                        System.debug('Start Date 2: ' + beginDate);
                    }
                    System.debug('isInsideInterval: ' + (todayDate > beginDate && todayDate <= endDate));
                    if (todayDate > beginDate && todayDate <= endDate) {
                        isInsideInterval = true;
                    }
                }
            }
            response.put('isInsideInterval',isInsideInterval);
            response.put('error', false);
            return response;
        }
    }

    public class getSavedMeters extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            try{

                Map<String, String> metersJsonMap = indexServ.getCaseMeterData(dossierId, false);
                System.debug('metersJsonMapLC: ' + metersJsonMap);
                response.put('metersJson', metersJsonMap);
                response.put('error', false);
            }
            catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }

    }

    public class getMeters extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Supply__c> supplies = (List<Supply__c>) JSON.deserialize(params.get('selectedSupply'), List<Supply__c>.class);
            if(supplies == null || supplies.size() == 0 || supplies[0].ServicePoint__r == null || supplies[0].ServicePoint__r.ENELTEL__c == null){
                response.put('error', true);
                response.put('errorMsg', 'Empty ENEL Id');
                return response;
            }

            String enelId = supplies[0].ServicePoint__r.ENELTEL__c;
            String meterJsonString = MRO_SRV_Index.getInstance().getMeterDataByEnelId(enelId);

            if(supplies[0].RecordType != null && supplies[0].RecordType.DeveloperName == 'Gas'){
                meterJsonString = filterGasMeters(meterJsonString);
            }
            response.put('metersJson', meterJsonString);
            response.put('error', false);
            return response;
        }
    }

    private static String filterGasMeters(String metersJsonString) {
        if (!MRO_SRV_SapSelfReadingCallOut.checkSapEnabled()) {
            metersJsonString = getDummyGasMeters();
        }
        String metersJsonStringForDeserialize = metersJsonString.remove('__c');
        MRO_SRV_Index.MeterList meterList = (MRO_SRV_Index.MeterList) JSON.deserialize(metersJsonStringForDeserialize, MRO_SRV_Index.MeterList.class);

        for (MRO_SRV_Index.MeterElement meterElement : meterList.Meters) {

            for (Integer i = meterElement.Quadrants.size() - 1; i >= 0; i--) {
                MRO_SRV_Index.QuadrantElement quadrantElement = meterElement.Quadrants.get(i);
                if (quadrantElement.ID_CADRAN != 'VOL') meterElement.Quadrants.remove(i);
            }
        }
        return JSON.serialize(meterList).replace('MeterNumber', 'MeterNumber__c');
    }

    private static String getDummyGasMeters() {
        return '{' +
                '"Meters": [' +
                '{' +
                '"MeterNumber__c": "01804685/2017",' +
                '"Quadrants": [' +
                '{' +
                '"ID_CADRAN": "VOL",' +
                '"CADRAN": "Volumul măsurat M3",' +
                '"INDEX": "100",' +
                '"VALIDATED": false' +
                '},' +
                '{' +
                '"ID_CADRAN": "PCS",' +
                '"CADRAN": "Puterii Calorifice Superioare",' +
                '"INDEX": "100",' +
                '"VALIDATED": false' +
                '},' +
                '{' +
                '"ID_CADRAN": "ENE",' +
                '"CADRAN": "Energia gazelor naturale – kWh",' +
                '"INDEX": "100",' +
                '"VALIDATED": false' +
                '}' +
                ']' +
                '}' +
                ']' +
                '}';
    }

    public class createIndex extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            List<MeterQuadrant> meterQuadrants = (List<MeterQuadrant>) JSON.deserialize(params.get('meterInfo'), List<MeterQuadrant>.class);
            Id accountId = params.get('accountId');
            Index__c indexObject;
            if (!caseList.isEmpty()) {
                Id supplyId = caseList.get(0).Supply__c;
                Supply__c supply = supplyQuery.getById(supplyId);
                Id servicePointId = supply.ServicePoint__c;
                Id caseId = caseList.get(0).Id;

                indexObject = indexQuery.getByCaseIdAndMeterName(caseId, meterQuadrants[0].meterNumber);
                if(indexObject == null) {
                    indexObject = indexServ.createIndexForSelfReading(caseId, accountId, meterQuadrants[0].meterNumber, servicePointId, Date.today(),
                            0, 0, 0, 0, 0, 0);
                }
                setIndexValues(indexObject, meterQuadrants);
                databaseSrv.upsertSObject(indexObject);
            }

            response.put('error', false);
            return response;
        }
    }

    private static void setIndexValues(Index__c indexObject, List<MeterQuadrant> meterQuadrants){
        for (Integer i = 0; i < meterQuadrants.size(); i++) {
            String idCadran = meterQuadrants.get(i).idCadran;
            if (idCadran.substring(0, 3) == '5.8') {
                indexObject.InductiveEnergy__c = meterQuadrants.get(i).index;
            } else if (idCadran.substring(0, 3) == '8.8') {
                indexObject.CapacitiveEnergy__c = meterQuadrants.get(i).index;
            } else if (idCadran == '1.8.0') {
                indexObject.ActiveEnergy__c = meterQuadrants.get(i).index;
            } else if (idCadran == '1.8.1') {
                indexObject.ActiveEnergyR1__c = meterQuadrants.get(i).index;
            } else if (idCadran == '1.8.2') {
                indexObject.ActiveEnergyR2__c = meterQuadrants.get(i).index;
            } else if (idCadran == '1.8.3') {
                indexObject.ActiveEnergyR3__c = meterQuadrants.get(i).index;
            }else if(idCadran == 'VOL'){
                indexObject.Volume__c = meterQuadrants.get(i).index;
            }
        }
    }

    public class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CancelProcessInput cancelProcessParams = (CancelProcessInput) JSON.deserialize(jsonInput, CancelProcessInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = new List<Case>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (!cancelProcessParams.oldCaseList.isEmpty() && String.isNotBlank(cancelProcessParams.dossierId)) {
                    Dossier__c dossier = new Dossier__c (id = cancelProcessParams.dossierId, Status__c = 'Canceled');
                    if (dossier != null) {
                        databaseSrv.upsertSObject(dossier);
                    }

                    for (Case caseRecord : cancelProcessParams.oldCaseList) {
                        caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status = 'Canceled'));
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class SaveDraft extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CancelProcessInput cancelProcessParams = (CancelProcessInput) JSON.deserialize(jsonInput, CancelProcessInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = new List<Case>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (!cancelProcessParams.oldCaseList.isEmpty() && String.isNotBlank(cancelProcessParams.dossierId)) {
                    Dossier__c dossier = new Dossier__c (id = cancelProcessParams.dossierId, Status__c = 'Draft');
                    if (dossier != null) {
                        databaseSrv.upsertSObject(dossier);
                    }

                    for (Case caseRecord : cancelProcessParams.oldCaseList) {
                        caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status = 'Draft'));
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CancelProcessInput {
        @AuraEnabled
        public List<Case> oldCaseList { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
    }

    public class MeterInfoList {
        @AuraEnabled
        public List<MeterQuadrant> meterInfoList { get; set; }
    }

    public class MeterQuadrant {
        @AuraEnabled
        public String meterNumber { get; set; }
        @AuraEnabled
        public String quadrantName { get; set; }
        @AuraEnabled
        public Double index { get; set; }
        @AuraEnabled
        public String idCadran { get; set; }
    }


    public with sharing class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            Map<String, String> selfReadingRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('SelfReading');
            String recordTypeId;
            //String recordTypeId = caseRecordTypes.get('SelfReading');
            //recordTypeId = supply.RecordType.DeveloperName == 'Gas' ? SelfReadingRecordTypes.get('Termination_GAS') : supply.RecordType.DeveloperName == 'Electric' ? SelfReadingRecordTypes.get('Termination_ELE') : SelfReadingRecordTypes.get('Termination_Service');
            List<Supply__c> supplies = (List<Supply__c>) JSON.deserialize(params.get('supplies'), List<Supply__c>.class);
            List<Case> caseList = params.get('caseList') != null ? (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class) : null;
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String selectedChannel = params.get('channelSelected');
            String selectedOrigin = params.get('originSelected');
            List<Case> newCases = new List<Case>();
            try {
                if (!supplies.isEmpty() && String.isNotBlank(accountId) && String.isNotBlank(dossierId) && String.isNotBlank(selectedOrigin) && String.isNotBlank(selectedChannel)) {
                    for (Supply__c supply : supplies) {
                        recordTypeId = supply.RecordType.DeveloperName == 'Gas' ? selfReadingRecordTypes.get('SelfReading_GAS') : selfReadingRecordTypes.get('SelfReading_ELE');
                        Case selfReadingCase = caseServ.createCaseForSelfReading(supply, accountId, dossierId, recordTypeId, selectedOrigin, selectedChannel);
                        selfReadingCase.Supply__r = supply;
                        newCases.add(selfReadingCase);
                    }
                    List<Case> updatedCases = caseServ.insertCaseRecords(newCases, caseList);
                    response.put('cases', updatedCases);
                    response.put('error', false);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String companyDivisionId;

            if (oldCaseList[0] != null) {
                companyDivisionId = oldCaseList[0].CompanyDivision__c;
            }

            caseServ.setNewOnCaseAndDossier(oldCaseList, dossierId, companyDivisionId);
            response.put('error', false);
            return response;
        }
    }
    public class updateOriginAndChannelOnDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            //String companyDivisionId = params.get('companyDivisionId');
            if(String.isNotBlank(dossierId) && String.isNotBlank(channelSelected) && String.isNotBlank(originSelected)) {
                dossierServ.updateOriginAndChannelOnDossier(dossierId, channelSelected, originSelected);
            }
            response.put('error', false);
            return response;
        }
    }
    public class updateCompanyDivisionAndCommodityOnDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String caseRecordTypeId = params.get('caseRecordTypeId');
            String companyDivisionId = params.get('companyDivisionId');
            if(String.isNotBlank(dossierId) && String.isNotBlank(caseRecordTypeId) && String.isNotBlank(companyDivisionId)){
                String commodity = (caseRecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SelfReading_ELE').getRecordTypeId()) ? 'Electric' : 'Gas';
                dossierServ.updateCompanyDivisionAndCommodityOnDossier(dossierId,companyDivisionId,commodity);
            }
            response.put('error', false);
            return response;
        }
    }
}