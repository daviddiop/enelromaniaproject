public class DocumentUploadCnt extends ApexServiceLibraryCnt {

    private static final FileMetadataService fileMetadataSrv = FileMetadataService.getInstance();
    private static final FileMetadataQueries fileMetadataQuery = FileMetadataQueries.getInstance();

    public class getTempFileMetadata extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return new Map<String, Object>{
                'fileMetadata' => fileMetadataSrv.findOrCreateTempFileMetadata(),
                'loadDateTime' => Datetime.now()
            };
        }
    }

    public class createFileMetadata extends AuraCallable {
        public override Object perform(final String jsonInput) {
            InputParams inputParam = (InputParams)JSON.deserialize(jsonInput, InputParams.class);

            if (String.isBlank(inputParam.fileMetadataId)) {
                throw new WrtsException(System.Label.FileMetadata + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(inputParam.documentId)) {
                throw new WrtsException(System.Label.Document + ' - ' + System.Label.MissingId);
            }
            Datetime loadDateTime = inputParam.loadDateTime;
            if (loadDateTime == null) {
                loadDateTime = Datetime.now();
            }
            fileMetadataSrv.insertFileMetadata(inputParam.fileMetadataId, inputParam.documentId, inputParam.dossierId);
            return new Map<String, Object>{
                'fileMetadataList' => fileMetadataQuery.listRecent(10, loadDateTime)
            };
        }
    }

    public class InputParams {
        @AuraEnabled
        public String fileMetadataId {get; set;}
        @AuraEnabled
        public String documentId {get; set;}
        @AuraEnabled
        public String dossierId {get; set;}
        @AuraEnabled
        public Datetime loadDateTime {get; set;}
    }
}