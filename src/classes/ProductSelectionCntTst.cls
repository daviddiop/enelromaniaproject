/**
 * Created by David Diop on 29.11.2019.
 */
@IsTest
public with sharing class ProductSelectionCntTst {
    @testSetup
    static void setup() {

        CompanyDivision__c companyDivision = TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Pricebook2 pricebook = TestDataFactory.priceBook().build();
        insert pricebook;

        Product2 product1 = TestDataFactory.product2().build();
        insert product1;

        Product2 product2 = TestDataFactory.product2()
            .setFamily('Default')
            .build();
        insert product2;

        insert TestDataFactory.pricebookEntry()
            .setProduct2Id(product1.Id)
            .setPricebook2Id(Test.getStandardPricebookId())
            .build();

        ProductOption__c productOption1 = TestDataFactory.productOption()
            .setRecordType(Schema.SObjectType.ProductOption__c.getRecordTypeInfosByDeveloperName().get(ProductService.VALUE_TYPE).getRecordTypeId())
            .build();
        insert productOption1;

        ProductOption__c productOption2 = TestDataFactory.productOption()
            .setRecordType(Schema.SObjectType.ProductOption__c.getRecordTypeInfosByDeveloperName().get(ProductService.FAMILY_TYPE).getRecordTypeId())
            .build();
        insert productOption2;

        insert TestDataFactory.productOptionRelation()
            .setProductId(product1.Id)
            .setProductOptionId(productOption1.Id)
            .build();

        insert TestDataFactory.productOptionRelation()
            .setProductId(product1.Id)
            .setProductOptionId(productOption2.Id)
            .build();

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.Pricebook2Id = pricebook.Id;
        insert opportunity;
    }
    @isTest
    static void getInitDataTest() {
        Opportunity opp = [
            SELECT Id,Name
            FROM Opportunity
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opp.Id
        };
        Object response = TestUtils.exec(
            'ProductSelectionCnt', 'getInitData', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id
        };

        response = (Map<String, Object>) TestUtils.exec('ProductSelectionCnt', 'getInitData', inputJSON, true);

        Pricebook2 pricebook = TestDataFactory.priceBook().build();
        insert pricebook;

        response = (Map<String, Object>) TestUtils.exec('ProductSelectionCnt', 'getInitData', inputJSON, true);
        Test.stopTest();
    }

    @isTest
    static void getInitDataIsBlankOppIdTest() {
        Opportunity opp = [
            SELECT Id,Name
            FROM Opportunity
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => ''
        };
        try {
            TestUtils.exec('ProductSelectionCnt', 'getInitData', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    static void checkoutTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        System.assertEquals(2, productList.size());

        Opportunity opportunity = [
            SELECT Id,Name
            FROM Opportunity
            LIMIT 1
        ];

        ProductService.Product product = new ProductService.Product();
        product.id = productList.get(0).Id;
        product.name = 'Test';
        product.description = 'Test';
        product.imageUrl = 'Test';
        product.family = 'Test';
        product.recordTypeName = 'Test';
        product.productOptions = new List<ProductService.ProductOption>();

        Test.startTest();
        Object response = TestUtils.exec('ProductSelectionCnt', 'checkout',
            new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'pricebookId' => Test.getStandardPricebookId(),
                'productList' => JSON.serialize(new List<ProductService.Product>{product})
            }, true);
        System.assertEquals(null, response);
        Test.stopTest();
    }

    @isTest
    static void listProductWithOptionsTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        Test.startTest();
        List<ProductService.Product> response = (List<ProductService.Product>) TestUtils.exec('ProductSelectionCnt', 'listProductWithOptions',
            new Set<String>{
                productList.get(0).Id
            }, true);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(1, response.size());
        System.assertEquals(1, response.get(0).productOptions.size());
    }
    @IsTest
    static void listProductWithOptionsIsBlankProductIdsTest() {

        Test.startTest();

        try {
            String response = (String) TestUtils.exec('ProductSelectionCnt', 'listProductWithOptions',
                null, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();

    }
    @IsTest
    static void productsAndFamiliesTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'productId' => productList.get(0).Id
        };
        System.assertEquals(2, productList.size());
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('ProductSelectionCnt', 'productsAndFamilies', inputJSON, true);
        Test.stopTest();
        System.assertNotEquals(null, response);
    }

    @IsTest
    static void productsAndFamiliesIsBlankProductIdTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'productId' => null
        };
        Test.startTest();
        try {
            String response = (String) TestUtils.exec('ProductSelectionCnt', 'productsAndFamilies', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @IsTest
    static void getProductListTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'pricebookId' => Test.getStandardPricebookId()
        };
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('ProductSelectionCnt', 'getProductList', inputJSON, true);
        Test.stopTest();
        System.assertNotEquals(null, response);
        System.assertEquals(3, response.size());
    }

    @IsTest
    static void getProductListIsBlankProductIdTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'pricebookId' => ''
        };
        try {
            String response = (String) TestUtils.exec('ProductSelectionCnt', 'getProductList', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
}