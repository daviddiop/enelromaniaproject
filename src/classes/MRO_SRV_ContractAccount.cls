public with sharing class MRO_SRV_ContractAccount {
    private static MRO_SRV_DatabaseService databaseService = MRO_SRV_DatabaseService.getInstance();

    public static MRO_SRV_ContractAccount getInstance() {
        return (MRO_SRV_ContractAccount)ServiceLocator.getInstance(MRO_SRV_ContractAccount.class);
    }

    public List<ContractAccount__c> updateContractAccounts(List<ContractAccount__c> contractAccountsList) {
        List<ContractAccount__c> output = contractAccountsList;
        if(contractAccountsList!=null && !contractAccountsList.isEmpty()) {
            try {
                databaseService.updateSObject(output);
            } catch (Exception e) {
                throw e;
            }
        }
        return output;
    }

    public ContractAccount__c changeBillingProfileField(ContractAccount__c contractAccount, Id billingProfileId){
        contractAccount.BillingProfile__c = billingProfileId;

        return contractAccount;
    }
}