/**
 * Created by goudiaby on 30/04/2020.
 * Updated 28/07/2020
 */

public with sharing class MRO_UTL_InternetInterfaceServices {

    public static MRO_UTL_InternetInterfaceServices getInstance() {
        return (MRO_UTL_InternetInterfaceServices) ServiceLocator.getInstance(MRO_UTL_InternetInterfaceServices.class);
    }

    public static final Set<String> ALLOWED_FLUX = new Set<String>{
            'AddOfferDocumentRequest',
            'AddContractDocumentRequest',
            'FormularOfertaSiteRequest',
            'OfferResult'
    };

    public static final Set<String> ALLOWED_LANGUAGE= new Set<String>{
            'ro',
            'en',
            'hu'
    };

    public static final Map<String, String> ERROR_CODE_MESSAGES_FORMULA_SITE = new Map<String, String>{
            'E0' => 'Formular neidentificat',
            'E1' => 'Trebuie completat nume si prenume',
            'E2' => 'Trebuie completat telefon',
            'E3' => 'Trebuie completat mail',
            'E4' => 'Telefon incorect',
            'E5' => 'Adresa mail incorecta',
            'E6' => 'Trebuie completata limba',
            'E7' => 'Limba nerecunoscuta',
            'E10' => 'Nu se poate cod client fara eneltel',
            'E11' => 'Nu se poate eneltel fara cod client',
            'E12' => 'Pereche cod client eneltel neidentificata',
            'E20' => 'Eroare in BD'
    };

    public static final Map<String, String> ERROR_CODE_MESSAGES_ADD_DOCUMENT = new Map<String, String>{
            'E1' => 'Token invalid or expired',
            'E2' => 'Trebuie atasat un document CI/BI/CUI',
            'E3' => 'Trebuie atasat un act de spatiu'
    };

    public static final Map<String, Map<Boolean, String>> URL_CODE_MAP = new Map<String, Map<Boolean, String>>{
            '1' => new Map<Boolean, String>{
                    true => 'e',
                    false => 'n'
            },
            '5' => new Map<Boolean, String>{
                    false => 'gn'
            },
            '8' => new Map<Boolean, String>{
                    true => 'de',
                    false => 'dn'
            },
            '9' => new Map<Boolean, String>{
                    false => 'dgn'
            }
    };


    public NE__Product__c getProductByCode( String productCode ) {
        List<NE__Product__c> productList = [
                SELECT Id,ProductCode__c
                FROM NE__Product__c
                WHERE ProductCode__c  = :productCode LIMIT 1
        ];

        return productList.isEmpty() ? null : productList.get(0);
    }

}