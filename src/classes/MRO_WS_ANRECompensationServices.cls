/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 19.10.20.
 */

global with sharing class MRO_WS_ANRECompensationServices {

    private static final wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration PhaseManagerIntegration =
            (wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration');
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();
    private static final MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static final MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static final MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();

    webservice static wrts_prcgvr.MRR_1_0.MultiResponse post(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest) {
        wrts_prcgvr.MRR_1_0.MultiResponse multiResponse = MRO_UTL_MRRMapper.initMultiResponse();
        Id caseId;
        String stackTrace;
        Savepoint sp = Database.setSavepoint();
        try {
            for (wrts_prcgvr.MRR_1_0.Request request : multiRequest.requests) {
                System.debug('*****MRO_WS_ANRECompensationServices  Request ' + request);
                wrts_prcgvr.MRR_1_0.Response response;
                Map<String, Object> result;

                result = processRequest(request);

                if (response == null) response = (wrts_prcgvr.MRR_1_0.Response) result.get('response');

                if (result != null && response.code == 'OK' &&  result.get('dossierId') InstanceOf Id) {
                    List<Case> listCasesByDossier;
                    Dossier__c dossierCreated = dossierQuery.getById((String)result.get('dossierId'));
                    listCasesByDossier = caseQuery.getCasesByDossierId(dossierCreated.Id);
                    System.debug('*******ApplyAutomaticTransitionToDossier');
                    dossierSrv.checkAndApplyAutomaticTransitionToDossierAndCases(
                            dossierCreated,
                            listCasesByDossier,
                            CONSTANTS.CONFIRM_TAG,
                            false,
                            false,
                            false
                    );
                }
                multiResponse.responses.add(response);
            }
        } catch (Exception e) {
            Database.rollback(sp);
            wrts_prcgvr.MRR_1_0.Response response = new wrts_prcgvr.MRR_1_0.Response();
            response.code = 'KO';
            response.header = multiRequest.requests[0].header;
            response.header.requestTimestamp = String.valueOf(System.now());
            response.description = e.getMessage();
            multiResponse.responses.add(response);
            stackTrace = e.getStackTraceString();
            system.debug('Error1: ' + e.getMessage() + e.getStackTraceString());
        } finally {
            createAndSaveLog(multiRequest, multiResponse, caseId, stackTrace);
        }
        return multiResponse;
    }

    static Map<String, Object> processRequest(wrts_prcgvr.MRR_1_0.Request request) {
        wrts_prcgvr.MRR_1_0.Response response;
        Map<String, Object> result = new Map<String, Object>();

        try {
            response = MRO_UTL_MRRMapper.initResponse(request);
            result.put('response', response);
            Case caseRecord;
            Account accountWObj;
            ServicePoint__c servicePointWObj;

            List<wrts_prcgvr.MRR_1_0.WObject> CaseList = MRO_UTL_MRRMapper.selectWobjects('Case', request);
            System.debug('******request ' + request);
            System.debug('******CaseList ' + CaseList);

            if (CaseList != null && !CaseList.isEmpty()) {
                caseRecord = (Case) MRO_UTL_MRRMapper.wobjectToSObject(CaseList[0]);
            } else {
                throw new WrtsException('Case in MRR');
            }

            System.debug('******caseRecord ' + caseRecord);

            List<wrts_prcgvr.MRR_1_0.WObject> accountList = MRO_UTL_MRRMapper.selectWobjects('Case/Account', request);

            if (accountList != null && !accountList.isEmpty()) {
                accountWObj = (Account) MRO_UTL_MRRMapper.wobjectToSObject(accountList[0]);
            } else {
                throw new WrtsException('missing Account in MRR');
            }
            System.debug('******accountWObj ' + accountWObj);

            Map<string, string> recordTypeFieldsMap = new Map<string, string>();

            List<wrts_prcgvr.MRR_1_0.WObject> recordTypeWObjects =
                    MRO_UTL_MRRMapper.selectWobjects('Case/Account/RecordType', request);

            if (recordTypeWObjects != null && !recordTypeWObjects.isEmpty()) {
                recordTypeFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(recordTypeWObjects[0]);
                System.debug('recordTypeFieldsMap' + recordTypeFieldsMap);
                if (String.isBlank(recordTypeFieldsMap.get('DeveloperName'))) {
                    throw new WrtsException('RecordType.DeveloperName is missing');
                }
            } else throw new WrtsException('Missing RecordType.DeveloperName in MRR');

            List<wrts_prcgvr.MRR_1_0.WObject> servicePointList = MRO_UTL_MRRMapper.selectWobjects('Case/Supply__r/ServicePoint__r', request);

            if (servicePointList != null && !servicePointList.isEmpty()) {
                servicePointWObj = (ServicePoint__c) MRO_UTL_MRRMapper.wobjectToSObject(servicePointList[0]);
            } else {
                throw new WrtsException('missing ServicePoint__c in MRR');
            }
            System.debug('******servicePointWObj ' + servicePointWObj);

            Id dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
            MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
            Dossier__c dossier = dossierSrv.generateDossier(null, null, null, null, dossierRecordTypeId, 'ANRECompensation');
            dossier.Origin__c = 'Internal';
            dossier.Channel__c = 'Back Office Customer Care';
            dossier.Status__c = 'New';
            update dossier;

            Account accountRecord;
            if(accountWObj != null) {
                List<Account> accounts = [SELECT Id FROM Account WHERE Name = :accountWObj.Name AND RecordType.DeveloperName = :recordTypeFieldsMap.get('DeveloperName') LIMIT 1];
                if(accounts.size() > 0){
                    accountRecord = accounts[0];
                    System.debug('******accountRecord ' + accountRecord);
                }
            }
            List<Supply__c> supplies;
            if(servicePointWObj != null && servicePointWObj.Code__c != null) {
                supplies = [SELECT Id, CompanyDivision__c, RecordType.DeveloperName FROM Supply__c WHERE ServicePoint__r.Code__c = :servicePointWObj.Code__c];
                System.debug('******supplies ' + supplies);
            }

            Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ANRECompensation').getRecordTypeId();
            Case newCase = new Case();
            newCase.Dossier__c = dossier.Id;
            newCase.RecordTypeId = caseRecordTypeId;
            newCase.CompensationType__c = 'Distributie';
            newCase.CaseTypeCode__c = 'C168';
            newCase.Origin = 'Internal';
            newCase.Channel__c = 'Back Office Customer Care';
            newCase.EffectiveDate__c = caseRecord.EffectiveDate__c;
            newCase.CompensationCode__c = caseRecord.CompensationCode__c;
            newCase.IncidentDate__c = caseRecord.IncidentDate__c;
            newCase.StartDate__c = caseRecord.StartDate__c;
            newCase.Amount__c = caseRecord.Amount__c;
            newCase.AccountId = accountRecord != null ? accountRecord.Id : null;
            if (supplies.size() > 0){
                newCase.Supply__c = supplies[0].Id;
                newCase.CompanyDivision__c = supplies[0].CompanyDivision__c;
                newCase.SupplyType__c = getSupplyType(supplies[0].RecordType.DeveloperName);
            }

            MRO_SRV_DatabaseService.getInstance().insertSObject(newCase);

            result.put('caseId', newCase.Id);
            result.put('dossierId', dossier.Id);

            System.debug('Result: ' + result);

            Case cs = [SELECT CaseNumber, FORMAT(CreatedDate), Status, Description FROM Case WHERE Id = :newCase.Id];
            cs.Status = 'Procesat';
            cs.Description = 'Procesat cu success';
            Wrts_prcgvr.MRR_1_0.WObject caseMRR = MRO_UTL_MRRMapper.sObjectToWObject(cs, 'Case');
            response.objects.add(caseMRR);

        } catch (Exception e) {
            response.code = 'KO';
            String description = e.getMessage() + ' ' + e.getStackTraceString();
            response.description = description;
            System.debug('Error2: ' + e.getMessage() + e.getStackTraceString());
            result.put('response', response);
        }
        return result;
    }

    private static String getSupplyType(String supplyRecordType) {
        switch on (supplyRecordType) {
            when 'Electric' {
                return 'Electricity';
            }
            when 'Gas' {
                return 'Gas';
            }
            when 'Service' {
                return 'VAS';
            }
            when else {
                return '';
            }
        }
    }

    private static void createAndSaveLog(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest, wrts_prcgvr.MRR_1_0.MultiResponse multiResponse, Id caseId, String stackTrace) {
        wrts_prcgvr.MRR_1_0.Request request = multiRequest.requests[0];
        wrts_prcgvr.MRR_1_0.Response response = multiResponse.responses[0];
        String code = String.isNotBlank(response.description) ? response.code + ' - ' + response.description : response.code;
        String messageString = MRO_UTL_MRRMapper.serializeMultirequest(multiRequest);
        String responseString = MRO_UTL_MRRMapper.serializeMultiResponse(multiResponse);
        MRO_SRV_ExecutionLog.createIntegrationLog('CallIn', request.header.requestId, request.header.requestType, caseId, messageString, stackTrace, code, responseString, null, null, request.header.requestId, 'Tibco Massive Creation', null);
    }

    private static void checkAndApplyAutomaticTransitionWithTagAndContext(SObject record, String tag, String context) {
        System.debug('### Record: ' + record);
        System.debug('### Tag: ' + tag);
        System.debug('### Context: ' + context);
        List<wrts_prcgvr__PhaseTransition__c> phaseTransitions = (List<wrts_prcgvr__PhaseTransition__c>) PhaseManagerIntegration.getTransitions(new Map<String, Object>{
                'object' => record,
                'type' => 'A',
                'context' => context,
                'tags' => tag
        });
        //get transitions END
        System.debug('### Found transitions: ' + phaseTransitions);

        //apply transitions START
        if (phaseTransitions.size() == 1) {
            PhaseManagerIntegration.applyTransition(new Map<String, Object>{
                    'object' => record,
                    'transition' => phaseTransitions[0],
                    'context' => context
            });
        }
    }
}