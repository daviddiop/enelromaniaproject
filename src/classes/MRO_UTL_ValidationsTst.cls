/**
 * Created by napoli on 24/09/2019.
 */

@isTest
private class MRO_UTL_ValidationsTst {
    @isTest
    static void unit_test_1() {
        String cnp = '1800101221144';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //correct
        System.assertEquals(true, result.outCome);
        System.assertEquals('', result.errorCode);
    }

    @isTest
    static void unit_test_2() {
        String cnp = '0800101221144';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //not correct: begins with zero
        System.assertEquals(false, result.outCome);
        System.assertEquals('E-CNP001', result.errorCode);
    }

    @isTest
    static void unit_test_3() {
        String cnp = '1801301221144';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //not correct: month\'s digits not correct
        System.assertEquals(false, result.outCome);
        System.assertEquals('E-CNP002', result.errorCode);
    }

    @isTest
    static void unit_test_4() {
        String cnp = '1800132221144';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //not correct: day\'s digits not correct
        System.assertEquals(false, result.outCome);
        System.assertEquals('E-CNP003', result.errorCode);
    }

    @isTest
    static void unit_test_5() {
        String cnp = '1800101531144';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //not correct: country zone\'s digits not correct
        System.assertEquals(false, result.outCome);
        System.assertEquals('E-CNP004', result.errorCode);
    }

    @isTest
    static void unit_test_6() {
        String cnp = '180010122114';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //not correct: 12 digits
        System.assertEquals(false, result.outCome);
        System.assertEquals('E-CNP006', result.errorCode);
    }

    @isTest
    static void unit_test_7() {
        String cnp = '1800101221145';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //checksum error
        System.assertEquals(false, result.outCome);
        System.assertEquals('E-CNP005', result.errorCode);
    }

    @isTest
    static void unit_test_8() {
        String cnp = 'A800101221144';
        MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(cnp); //not numeric
        System.assertEquals(false, result.outCome);
        System.assertEquals('E-CNP006', result.errorCode);
    }
}