/**
 * Created by goudiaby on 16/07/2019.
 */

public with sharing class BillingProfileEditCnt extends ApexServiceLibraryCnt {
    static BillingProfileQueries billingProfileQuery = BillingProfileQueries.getInstance();

    public class getAddressFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();

            Map<String, String> params = asMap(jsonInput);
            String billingProfileRecordId = params.get('billingProfileRecordId');
            if (String.isBlank(billingProfileRecordId)) {
                throw new WrtsException(System.Label.BillingProfile + ' - ' + System.Label.MissingId);
            }
            BillingProfile__c billingProfile = billingProfileQuery.getById(billingProfileRecordId);
            if (billingProfile != null) {
                AddressService.AddressDTO addressFromBilling = BillingProfileEditCnt.copyBillingAddressToDTO(billingProfile);
                response.put('billingAddress', addressFromBilling);
            }
            return response;
        }
    }

    public class getRecordTypes extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, Schema.RecordTypeInfo> billingProfileRts = Schema.SObjectType.BillingProfile__c.getRecordTypeInfosByDeveloperName();
            Schema.RecordTypeInfo giro = billingProfileRts.get('Giro');
            Schema.RecordTypeInfo postalOrder = billingProfileRts.get('PostalOrder');
            response.put('giroRecordType', new Map<String, String>{
                    'label' => giro.getName(), 'value' => giro.getRecordTypeId()
            });
            response.put('postalOrderRecordType', new Map<String, String>{
                    'label' => postalOrder.getName(), 'value' => postalOrder.getRecordTypeId()
            });
            return response;
        }
    }

    public static AddressService.AddressDTO copyBillingAddressToDTO(BillingProfile__c billingProfile) {
        AddressService.AddressDTO addressDTO = new AddressService.AddressDTO();
        addressDTO.streetNumber = billingProfile.BillingStreetNumber__c;
        addressDTO.streetNumberExtn = billingProfile.BillingStreetNumberExtn__c;
        addressDTO.streetName = billingProfile.BillingStreetName__c;
        addressDTO.streetType = billingProfile.BillingStreetType__c;
        addressDTO.apartment = billingProfile.BillingApartment__c;
        addressDTO.building = billingProfile.BillingBuilding__c;
        addressDTO.city = billingProfile.BillingCity__c;
        addressDTO.country = billingProfile.BillingCountry__c;
        addressDTO.floor = billingProfile.BillingFloor__c;
        addressDTO.locality = billingProfile.BillingLocality__c;
        addressDTO.postalCode = billingProfile.BillingPostalCode__c;
        addressDTO.province = billingProfile.BillingProvince__c;
        return addressDTO;
    }
}