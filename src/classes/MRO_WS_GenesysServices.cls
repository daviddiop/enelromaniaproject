/**
 * Created by goudiaby on 08/06/2020.
 */

global with sharing class MRO_WS_GenesysServices {
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Interaction interactionQuery = MRO_QR_Interaction.getInstance();
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    webService static wrts_prcgvr.MRR_1_0.MultiResponse post(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest) {
        System.debug('****MRO_WS_GenesysServices: ' + multiRequest);
        wrts_prcgvr.MRR_1_0.MultiResponse multiResponse = new wrts_prcgvr.MRR_1_0.MultiResponse();
        multiResponse.responses = new List<wrts_prcgvr.MRR_1_0.Response>();
        wrts_prcgvr.MRR_1_0.Request request = multiRequest.requests[0];

        System.debug('*****MRO_WS_GenesysServices Request ' + request);
        wrts_prcgvr.MRR_1_0.Response response;
        Map<String, Object> result = new Map<String, Object>();
        if (request.header.requestType == 'SurveyRequest') {
            result = gen001Response(request);
        } else if (request.header.requestType == 'InteractionIVRRequest') {
            result = interactionIvrResponse(request);
        } else {
            response = new wrts_prcgvr.MRR_1_0.Response();
            response.code = 'KO';
            response.header = request.header;
            response.header.requestTimestamp = String.valueOf(System.now());
            response.description = '999 - RequestType not recognized: ' + request.header.requestType ;
        }
        if (response == null) {
            response = (wrts_prcgvr.MRR_1_0.Response) result.get('response');
        }
        multiResponse.responses.add(response);
        Id objectId;

        System.debug('response ******* ' + response);
        if (result != null && !result.isEmpty()) {
            objectId = (Id) result.get('objectId');
            //MRO_SRV_ExecutionLog.createAndSaveLog(multiRequest, multiResponse, objectId);
        }
        return multiResponse;
    }
    /**
     * Manage request from Genesys service Survey
     * @return result
     */
    static Map<String, Object> gen001Response(wrts_prcgvr.MRR_1_0.Request request) {
        System.debug('gen001 Response: ' + request);
        Map<String, Object> result = new Map<String, Object>();
        wrts_prcgvr.MRR_1_0.Response response ;
        response = MRO_UTL_MRRMapper.initResponse(request);
        String pathStringInteraction = 'Interaction__r';
        /*String pathStringSurvey = 'Interaction__r/SurveyAnswer__r';
        String pathStringAccount = 'Interaction__r/Customer__r';
        String pathStringCase = 'Case';*/
        result.put('response', response);
        try {
            if (request.objects.size() > 0) {
                Map<String, Interaction__c> interactionMap = new Map<String, Interaction__c>();
                Map<String, String> accountMap = new Map<String, String>();
                Map<String, String> caseMap = new Map<String, String>();
                Map<String, SurveyAnswer__c> surveyMap = new Map<String, SurveyAnswer__c>();

                List<wrts_prcgvr.MRR_1_0.WObject> interactionWObjects = MRO_UTL_MRRMapper.selectWobjects(pathStringInteraction, request);
                if (interactionWObjects != null && !interactionWObjects.isEmpty()) {
                    for (wrts_prcgvr.MRR_1_0.WObject interactionWoInfo : interactionWObjects) {
                        Interaction__c interaction = (Interaction__c) MRO_UTL_MRRMapper.wobjectToSObject(interactionWoInfo);
                        if (interaction.CallId__c == null || (String.isBlank(interaction.CallId__c))) {
                            throw new WrtsException('Interaction call Id is blank ');
                        }
                        interactionMap.put(interaction.CallId__c, interaction);

                        List<wrts_prcgvr.MRR_1_0.WObject> accountWObjects = MRO_UTL_MRRMapper.selectWobjects('Customer__r', interactionWoInfo.objects);
                        if (accountWObjects != null && !accountWObjects.isEmpty()) {
                            Account account = (Account) MRO_UTL_MRRMapper.wobjectToSObject(accountWObjects[0]);
                            if (account != null && account.IntegrationKey__c != null) {
                                accountMap.put(interaction.CallId__c, account.IntegrationKey__c);
                            }
                        }

                        List<wrts_prcgvr.MRR_1_0.WObject> caseWObjects = MRO_UTL_MRRMapper.selectWobjects('case', interactionWoInfo.objects);
                        if (caseWObjects != null && !caseWObjects.isEmpty()) {
                            Case caseRecord = (Case) MRO_UTL_MRRMapper.wobjectToSObject(caseWObjects[0]);
                            if (caseRecord != null && caseRecord.IntegrationKey__c != null) {
                                caseMap.put(interaction.CallId__c, caseRecord.IntegrationKey__c);
                            }
                        }

                        List<wrts_prcgvr.MRR_1_0.WObject> surveyWObjects = MRO_UTL_MRRMapper.selectWobjects('SurveyAnswer__r', interactionWoInfo.objects);
                        if (surveyWObjects != null && !surveyWObjects.isEmpty()) {
                            SurveyAnswer__c survey = (SurveyAnswer__c) MRO_UTL_MRRMapper.wobjectToSObject(surveyWObjects[0]);
                            surveyMap.put(interaction.CallId__c, survey);
                        } else {
                            throw new WrtsException('Survey Object is empty');
                        }
                    }
                    Map<String, Interaction__c> listInteraction = interactionQuery.getListInteractionByCallId(interactionMap.keySet());
                    Map<String, Case> listCase = caseQuery.getListCaseByCaseNumber(caseMap.values());
                    Map<String, Account> listAccount = accountQuery.getListAccountByIntegrationKey(accountMap.values());
                    for (Interaction__c interaction : listInteraction.values()) {
                        String caseNumber = caseMap.get(interaction.CallId__c);
                        String accountIntegrationKey = accountMap.get(interaction.CallId__c);
                        SurveyAnswer__c surveyRecord = surveyMap.get(interaction.CallId__c);

                        surveyRecord.Interaction__c = interaction.Id;
                        surveyRecord.Case__c = listCase.get(caseNumber).Id;
                        surveyRecord.Customer__c = listAccount.get(accountIntegrationKey).Id;
                        surveyMap.put(interaction.CallId__c, surveyRecord);
                    }
                    databaseSrv.insertSObject(surveyMap.values());
                    result.put('surveyAnswer', surveyMap.values());
                    System.debug('surveyMap.values()'+surveyMap.values());
                } else {
                    throw new WrtsException('Interaction Objects is empty ' + interactionWObjects);
                }
            } else {
                throw new WrtsException('000 - Request Objects is empty ' + request.objects);
            }
        } catch (Exception e) {
            response.code = 'KO';
            //response.header.fields.clear();
            response.description = e.getMessage();
            result.put('response', response);
        }
        return result;
    }
    /**
     * Manage request from Genesys service Interaction IVR
     * @return result
     */
    static Map<String, Object> interactionIvrResponse(wrts_prcgvr.MRR_1_0.Request request) {
        System.debug('gen001 Response: ' + request);
        Map<String, Object> result = new Map<String, Object>();
        wrts_prcgvr.MRR_1_0.Response response ;
        response = MRO_UTL_MRRMapper.initResponse(request);
        result.put('response', response);
        try {
            List<wrts_prcgvr.MRR_1_0.WObject> objectsResponse = response.objects;
            getInteractions(100, objectsResponse);
        } catch (Exception e) {
            response.code = 'KO';
            response.description = '*** Error ' + e.getMessage() + ' ' + e.getStackTraceString();
        }
        System.debug('response interactionIvrResponse ******* ' + response);
        result.put('response', response);
        return result;
    }

    /**
     * Method to construct an mrrObject and to be added to the mrrResponse
     * @param nDays
     * @param objectsResponse
     */
    static void getInteractions(Integer nDays, List<wrts_prcgvr.MRR_1_0.WObject> objectsResponse) {
        Map<String, Wrts_prcgvr.MRR_1_0.WObject> mrrInteractions = new Map<String, wrts_prcgvr.MRR_1_0.WObject>();
        try {
            DateTime todayDate = DateTime.now();
            System.debug('todayDate'+todayDate);
            Integer hours;
            if(todayDate.hour() == 00){
                hours = 23;
            } else {
                hours = todayDate.hour()-1;
            }
            Integer minutes;
            if(todayDate.minute() == 00){
                minutes = 59;
            } else {
                minutes = todayDate.minute()-1;
            }
            Integer seconds1 = todayDate.second();
            Date myDate = Date.newInstance( todayDate.year(), todayDate.month(), todayDate.day() );
            Time myTime = Time.newInstance( hours, minutes, todayDate.second(), todayDate.millisecond());
            DateTime objMinDT = DateTime.newInstance( myDate, myTime );
            Datetime dateBeforeSevenDays = todayDate.addDays(-7);
            System.debug('dateBeforeSevenDays'+dateBeforeSevenDays);

            List<String> conditions = new List<String>();
            conditions.add('CallId__c != null');
            conditions.add('InterlocutorPhone__c != null');
            conditions.add('ENELTEL__c != null');
            conditions.add('createdDate < :todayDate');
            conditions.add('createdDate >= :objMinDT');
            String query = 'SELECT ' + String.join(fieldMap.values(), ',') + ' FROM Interaction__c ';
            query += ' WHERE ' + String.join(conditions, ' AND ') + ' LIMIT 200';

            System.debug('Query Interaction ' + query);
            List<Interaction__c> interactions =  Database.query(query);
            if(!interactions.isEmpty()){
            for (SObject obj : interactions) {

                Interaction__c interaction = (Interaction__c) (obj);
                System.debug('interaction.CreatedDate---'+interaction.CreatedDate);
                DateTime createdDate = interaction.CreatedDate;
                Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject(interaction, 'Interaction__r');
                //if(createdDate.getTime() < todayDate.getTime() && createdDate.getTime() >=  objMinDT.getTime()){
                    mrrInteractions.put(interaction.Id, mrrObj);
               //}
            }
            }
            Wrts_prcgvr.MRR_1_0.WObject mrrObjAccount;
            Wrts_prcgvr.MRR_1_0.WObject mrrObjCase;
            System.debug('mrrInteractions' + mrrInteractions.keySet());
            Set<String> idInteractions = mrrInteractions.keySet();
            List<SurveyAnswer__c> surveys = [
                    SELECT Id,Customer__c,CreatedDate,Interaction__c,Customer__r.IntegrationKey__c,Customer__r.PersonIndividual.HasOptedOutPartners__c,Customer__r.PersonIndividual.HasOptedOutSolicit
                    FROM SurveyAnswer__c
                    WHERE  CreatedDate >= : dateBeforeSevenDays AND CreatedDate < :todayDate
                    LIMIT 200
            ];
            Set<String> accountIntegrationIds = new Set<String>();
            System.debug('surveys'+surveys);
            if(!surveys.isEmpty()){
                for(SurveyAnswer__c surveyAnswer :surveys){
                    if(surveyAnswer.Customer__c !=  null){
                        accountIntegrationIds.add(surveyAnswer.Customer__r.IntegrationKey__c);
                    }
                }
            }
            Map<Id, Map<String, String>> additionalData = getAdditionalIntegrationData(idInteractions);
            for (String idInteraction : idInteractions) {
                mrrObjAccount = MRO_UTL_MRRMapper.createCustomMRRObject('Account', 'Customer__r', '');
                mrrObjCase = MRO_UTL_MRRMapper.createCustomMRRObject('Case', 'Case', '');
                Wrts_prcgvr.MRR_1_0.WObject mrrObj = mrrInteractions.get(idInteraction);
                Map<String, String> mapCase = additionalData.get(idInteraction);
                if (mapCase != null && !mapCase.isEmpty()) {
                    System.debug('*** map Case ' + mapCase);
                    if (String.isNotBlank(mapCase.get('caseKey'))) {
                        wrts_prcgvr.MRR_1_0.Field caseInteraction = MRO_UTL_MRRMapper.createField('IntegrationKey__c', mapCase.get('caseKey'));
                        mrrObjCase.fields.add(caseInteraction);
                    }
                    if (String.isNotBlank(mapCase.get('caseSubtype'))) {
                        String subtype = mapCase.get('caseSubtype');
                        System.debug('caseSubtype' + subtype.substringAfter('-'));
                        System.debug('caseSubtype' + subtype.substringBefore('-'));
                        wrts_prcgvr.MRR_1_0.Field caseSubTypeTripletaCode = MRO_UTL_MRRMapper.createField('Tripleta_Cod', subtype.substringBefore('-'));
                        mrrObjCase.fields.add(caseSubTypeTripletaCode);

                        wrts_prcgvr.MRR_1_0.Field caseSubTypeTripleta_Desc = MRO_UTL_MRRMapper.createField('Tripleta_Desc', subtype.substringAfter('-'));
                        mrrObjCase.fields.add(caseSubTypeTripleta_Desc);
                    }
                    if (String.isNotBlank(mapCase.get('accountKey'))) {
                        wrts_prcgvr.MRR_1_0.Field accountInteraction = MRO_UTL_MRRMapper.createField('IntegrationKey__c', mapCase.get('accountKey'));
                        if(!accountIntegrationIds.contains(mapCase.get('accountKey'))){
                            mrrObjAccount.fields.add(accountInteraction);
                        }
                        /*mrrObjAccount.fields.add(accountInteraction);*/
                    }
                    mrrObj.objects.add(mrrObjCase);
                    mrrObj.objects.add(mrrObjAccount);
                }
                objectsResponse.add(mrrObj);
            }
        } catch (Exception e) {
            System.debug('*** Error ' + e.getCause() + ' ' + e.getStackTraceString());
        }
    }
    /**
     * Method to get data from Case related to an interaction
     * @param interactionIds
     *
     * @return Map Additionnal to be add on mrrObject Interaction
     */
    static Map<Id, Map<String, String>> getAdditionalIntegrationData(Set<String> interactionIds) {
        Map<String, String> mapData;
        Map<Id, Map<String, String>> mapInteractionCaseData = new Map<Id, Map<String, String>>();
        for (Case myCase : getCasesByInteraction(interactionIds)) {

            mapData = new Map<String, String>();
            mapData.put('caseKey', myCase.CaseNumber);
            mapData.put('caseSubtype', myCase.SubType__c);
            mapData.put('accountKey', myCase.Dossier__r.CustomerInteraction__r.Customer__r.IntegrationKey__c);
            mapInteractionCaseData.put(myCase.Dossier__r.CustomerInteraction__r.Interaction__c, mapData);
        }
        System.debug('*** mapInteractionCaseData ' + mapInteractionCaseData);
        return mapInteractionCaseData;
    }

    /**
     * Method that allows us to get Case / Account related to an one interaction
     * @param interactionIds
     *
     * @return List Case with Interaction
     */
    static List<Case> getCasesByInteraction(Set<String> interactionIds) {
        return [
                SELECT Id,CaseNumber,Dossier__r.CustomerInteraction__r.Customer__r.IntegrationKey__c,SubType__c,
                        Dossier__r.CustomerInteraction__r.Interaction__c
                FROM Case
                WHERE Dossier__r.CustomerInteraction__r.Interaction__c IN :interactionIds AND Dossier__r.CustomerInteraction__c != NULL
                AND (Dossier__r.CustomerInteraction__r.Customer__r.PersonIndividual.HasOptedOutPartners__c = FALSE OR Dossier__r.CustomerInteraction__r.Customer__r.PersonIndividual.HasOptedOutSolicit = FALSE)
        ];
    }

    /**
     * Map for Interactions Field
     */
    public static Map<String, String> fieldMap = new Map<String, String>{
            'Id' => 'Id',
            'callId' => 'CallId__c',
            'enelTel' => 'ENELTEL__c',
            'phone' => 'InterlocutorPhone__c',
            'direction' => 'Direction__c',
            'callDescription' => 'Comments__c',
            'agentTag' => 'CreatedBy.Marca__c',
            'agentCompany' => 'CreatedBy.CompanyName',
            'dateCall' => 'CreatedDate'
    };
}