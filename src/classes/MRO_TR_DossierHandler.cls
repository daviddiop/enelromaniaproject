/**
 * @author   Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since    Sep 24, 2019
 * @desc     Trigger handler for the Dossier__c object
 */

global with sharing class MRO_TR_DossierHandler implements TriggerManager.ISObjectTriggerHandler {
    //This flag can be manually overridden by Apex code to force enabling/disabling the skip of the non-bulk Compatibility Check
    global static Boolean SKIP_COMPATIBILITY_CHECK = FeatureManagement.checkPermission('MRO_SkipTriggerCompatibilityCheck');
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();
    private static MRO_QR_TouchPoint touchPointQueries = MRO_QR_TouchPoint.getInstance();
    private static MRO_SRV_Token tokenSrv = MRO_SRV_Token.getInstance();
    // Initialize assignment rule service class
    private static MRO_SRV_AssignmentRuleConfiguration assignmentRuleConfigurationSrv =
            MRO_SRV_AssignmentRuleConfiguration.getInstance();

    global void beforeInsert() {

        // Apply custom assignment rules
        assignmentRuleConfigurationSrv.applyAssignmentRuleLogic(Trigger.new, null, 'Phase__c');

        if (!Test.isRunningTest()) {
            /*
            //Sets Phase description
            ((wrts_prcgvr.Interfaces_1_0.IPhaseManagerUtils) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerUtils'))
                    .bulkSetPhaseDescriptions(new Map<String,Object>{
                        'triggerNew'            =>Trigger.new,
                        'triggerOld'            => Trigger.old,
                        'descriptionFieldName'  => 'PhaseDescription__c'
            });
             */
        }
    }

    global void beforeDelete() {}

    global void beforeUpdate() {

        // Apply custom assignment rules
        assignmentRuleConfigurationSrv.applyAssignmentRuleLogic(Trigger.new, null, 'Phase__c');

        Map<Id, Token__c> tokensMap = new Map<Id, Token__c>();
        for (Dossier__c dossier : (List<Dossier__c>) Trigger.new) {
            if (dossier.ExternalAccessIdentifier__c == null && dossier.Opportunity__c != null
                && dossier.Status__c == CONSTANTS.DOSSIER_STATUS_NEW && Trigger.oldMap.get(dossier.Id).get('Status__c') == CONSTANTS.DOSSIER_STATUS_DRAFT
                && (dossier.SendingChannel__c == CONSTANTS.SENDING_CHANNEL_EMAIL || dossier.SendingChannel__c == CONSTANTS.SENDING_CHANNEL_SMS)) {
                List<TouchPointConfiguration__mdt> conf = touchPointQueries.getTouchPointConfiguration(CONSTANTS.TOUCHPOINTCONFIGURATION_SOURCE_CRM, CONSTANTS.TOUCHPOINTCONFIGURATION_TYPE_CONTRACTACCEPTANCE, dossier.SendingChannel__c);
                if (!conf.isEmpty()) {
                    String touchPointUrl = IntegrationSettings__c.getOrgDefaults().InternetInterfaceTouchPointBaseUrl__c+'/t'+(dossier.Language__c != null ? dossier.Language__c.left(1).toLowerCase() : 'r');
                    Token__c token = tokenSrv.insertTouchPointToken(conf[0], touchPointUrl, false);
                    tokensMap.put(dossier.Id, token);
                }
            }
        }
        if (!tokensMap.isEmpty()) {
            insert tokensMap.values();
            for (Id dossierId : tokensMap.keySet()) {
                ((Dossier__c) Trigger.newMap.get(dossierId)).ExternalAccessIdentifier__c = tokensMap.get(dossierId).Id;
            }
        }

        if (!Test.isRunningTest()) {
            /*
            //Sets Phase description
            ((wrts_prcgvr.Interfaces_1_0.IPhaseManagerUtils) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerUtils'))
                    .bulkSetPhaseDescriptions(new Map<String,Object>{
                        'triggerNew'            =>Trigger.new,
                        'triggerOld'            => Trigger.old,
                        'descriptionFieldName'  => 'PhaseDescription__c'
            });
             */

            //Checks Phase Transition and prepares Activities
            ((wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration'))
                .beforeUpdate(new Map<String, Object>{
                'oldObjects' => Trigger.old,
                'newObjects' => Trigger.new
            });

            //Checks Mandatory Activities
            ((wrts_prcgvr.Interfaces_1_0.IActivityUtils) wrts_prcgvr.VersionManager.newClassInstance('ActivityUtils'))
                .bulkCheckCompleted(new Map<String, Object>{
                'triggerNew' => Trigger.new,
                'triggerOld' => Trigger.old
            });
        }
    }

    global void afterInsert() {
        if (!Test.isRunningTest()) {
            if (!SKIP_COMPATIBILITY_CHECK) {
                for (Dossier__c d : (List<Dossier__c>) Trigger.new) {
                    wrts_prcgvr.Interfaces_1_0.IObjectCompatibilityInt ObjectCompatibility =
                            (wrts_prcgvr.Interfaces_1_0.IObjectCompatibilityInt) wrts_prcgvr.VersionManager.newClassInstance('ObjectCompatibility');

                    wrts_prcgvr.ObjectCompatibility_1_0.CheckResult result = (wrts_prcgvr.ObjectCompatibility_1_0.CheckResult) ObjectCompatibility.check(new Map<String, Object>{
                            'sObject' => d
                    });

                    //list of incompatible records
                    if (result.incompatibilities != null && !result.incompatibilities.isEmpty()) {
                        List<Id> incompatibleDossiersIds = new List<Id>((new Map<Id, SObject>(result.incompatibilities)).keySet());
                        d.addError(Label.IncompatibleDossiersFound.replace('{0}', String.join(incompatibleDossiersIds, ', ')));
                    }
                }
            }
        }
    }

    global void afterDelete() {}

    global void afterUndelete() {}

    global void afterUpdate() {
        if (!Test.isRunningTest()) {
            //Execute creation Activities
            ((wrts_prcgvr.Interfaces_1_0.IActivityUtils) wrts_prcgvr.VersionManager.newClassInstance('ActivityUtils'))
                .bulkSaveActivityContext(null);

            //Handles callout (update only)
            List<SObject> dossierChangedPhaseNew = new List<SObject>();
            List<SObject> dossierChangedPhaseOld = new List<SObject>();
            for (SObject so : Trigger.new) {
                if (MRO_UTL_ApexContext.FORCE_CALLOUTS_RETRIGGERING || Trigger.oldMap.get(so.Id).get('Phase__c') != so.get('Phase__c')) {
                    dossierChangedPhaseNew.add(so);
                    dossierChangedPhaseOld.add(Trigger.oldMap.get(so.Id));
                }
            }
            if(!dossierChangedPhaseNew.isEmpty()){
                ((wrts_prcgvr.Interfaces_1_0.ICalloutUtils) wrts_prcgvr.VersionManager.newClassInstance('CalloutUtils'))
                        .bulkSend(new Map<String, Object>{
                        'newObjects' => dossierChangedPhaseNew,
                        'oldObjects' => dossierChangedPhaseOld
                });
            }
        }

        Set<Id> changedParentDossierIds = new Set<Id>();
        for (Dossier__c dossier : (List<Dossier__c>) Trigger.new) {
            if (dossier.Parent__c != Trigger.oldMap.get(dossier.Id).get('Parent__c')) {
                changedParentDossierIds.add(dossier.Id);
            }
        }
        if (!changedParentDossierIds.isEmpty()) {
            List<Case> casesToUpdate = MRO_QR_Case.getInstance().listCasesByDossierIds(changedParentDossierIds);
            MRO_TR_CaseHandler.FORCE_CALCULATE_SLA_TIPAR = true;
            update casesToUpdate;
            MRO_TR_CaseHandler.FORCE_CALCULATE_SLA_TIPAR = false;
        }
    }
}