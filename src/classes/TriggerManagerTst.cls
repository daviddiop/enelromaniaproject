/**
 * Test class for TriggerManagerTst
 *
 * @author Moussa Fofana
 * @version 1.0
 * @description Test class for TriggerManager
 * @uses
 * @code
 * @history
 * 2019-07-10:  Moussa Fofana
 */
@isTest
public  class TriggerManagerTst {

    @testSetup
    private static void setup() {
        System.debug('ici=====>> '+ProcessDateThresholds__c.getOrgDefaults().SwOutOriginOtherDelay__c);
        System.debug('here=====>> '+Date.today().addDays(MRO_UTL_SettingProvider.getSwOutOriginDelay(false) + MRO_UTL_SettingProvider.getSwOutEffectiveDateLT(false)));
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.BusinessType__c = 'NGO';
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        List<Account> listAccount = new List<Account>();
        listAccount.add(businessAccount);
        listAccount.add(personAccount);

        insert listAccount;

        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[0].Id;
        contact.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert contact;

        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;

        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        interaction.Channel__c = 'MyEnel';
        insert interaction;

        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[0].Id;
        insert billingProfile;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        contractAccount.BillingProfile__c = billingProfile.Id;
        insert contractAccount;

        Account distributor = new Account();
        distributor.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        distributor.Name = 'distributor';
        distributor.AtoAEnabled__c =true;
        insert distributor;

        Account myTraderAccount = MRO_UTL_TestDataFactory.account().traderAccount().build();
        myTraderAccount.Name = 'Trader interlocutor';
        myTraderAccount.BusinessType__c = 'Commercial areas';
        insert myTraderAccount;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Account__c = listAccount[0].Id;
        servicePoint.Trader__c = myTraderAccount.Id;
        servicePoint.Distributor__c = distributor.Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;


        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder()
                    .setAccount(listAccount[1].Id)
                    .setCompany(companyDivision.Id)
                    .setContractAccount(contractAccount.Id)
                    .build();
            supply.RecordTypeId = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
            supply.Contract__c = contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[0].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Origin = 'Mail';
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
    }


    @isTest
    private static void handleTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
                FROM Case
                LIMIT 2
        ];
        Test.startTest();
        try {
            caseList[0].Supply__c = null;
            update caseList[0];
            delete caseList[1];
            undelete caseList[1];
            TriggerManager.handle(new TestTrgHnd());

        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    private static void afterDeleteTest() {
        List<Supply__c> supplyList = [
                SELECT Id, RecordTypeId,Market__c
                FROM Supply__c
                LIMIT 2
        ];
        Test.startTest();
        try {
            supplyList[0].Market__c = 'Free';
            update supplyList[0];
            delete supplyList[0];
            undelete supplyList[1];
            TriggerManager.handle(new TestTrgHnd());

        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    public  class TestTrgHnd implements TriggerManager.ISObjectTriggerHandler {
        public void beforeInsert() {}
        public void beforeDelete() {}
        public void beforeUpdate() {}
        public void afterInsert() {}
        public void afterDelete() {}
        public void afterUpdate() {}
        public void afterUndelete() {}
    }

}