/**
 * Created by ferhati on 02/08/2019.
 */

public with sharing class PrivacyChangeCnt extends ApexServiceLibraryCnt {
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static PrivacyChangeQueries privacyChangeQuery = PrivacyChangeQueries.getInstance();
    private static OpportunityQueries opportunityQuery = OpportunityQueries.getInstance();

    public static PrivacyChangeCnt getInstance() {
        return new PrivacyChangeCnt();
    }

    public with sharing class getIndividual extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String individualId = '';
            try {
                String opportunityId = params.get('opportunityId');
                //Map<String,String> opportunityRecord = asMap (opportunity);
                //System.debug('opportunityRecord: ' + opportunityRecord);

                //String accountId = opportunityRecord.get('AccountId');
                //String opportunityId = opportunityRecord.get('Id');
                //String customerInteractionId = opportunityRecord.get('CustomerInteraction__c');

//opportunityRecord: {AccountId=0011t00000bVZo1AAG,  CustomerInteraction__c=a0j1t000001YVySAAW, Id=0061t00000GZ8Q3AAL



                //getPrivacyChangeByOpportunityId
                Opportunity opportunityRecord= opportunityQuery.getOpportunityById(opportunityId);
                String accountId = opportunityRecord.AccountId;
                String customerInteractionId = opportunityRecord.CustomerInteraction__c;

                List<CustomerInteraction__c> customerInteraction= customerInteractionQuery.getInterlocutorByCustomerInteractionId(customerInteractionId);
                List<AccountContactRelation> listContacts= accountQuery.listContacts(accountId);
                Account acc = accountQuery.findAccount(accountId);
                PrivacyChange__c privacyChangeByOpportunity= privacyChangeQuery.getPrivacyChangeByOpportunityId(opportunityId);

                if(privacyChangeByOpportunity != null){
                    response.put('privacyChange', privacyChangeByOpportunity);
                }

                if(privacyChangeByOpportunity == null){

                    if((!customerInteraction.isEmpty())){
                        individualId = customerInteraction.get(0).Interaction__r.Interlocutor__c;
                    }else if(String.isBlank(individualId)){
                        if(acc.IsPersonAccount){
                            individualId = acc.PersonIndividualId;

                        }else {
                            if (!listContacts.isEmpty()) {
                                for (AccountContactRelation accContRelation : listContacts) {
                                    if (accContRelation.Contact.IndividualId != null) {
                                        individualId = accContRelation.Contact.IndividualId;
                                    }
                                }
                            }
                        }
                    }

                    if(String.isNotBlank(individualId)){
                        PrivacyChange__c privacyChangeByIndividual = privacyChangeQuery.getPrivacyChange(individualId);
                        if(privacyChangeByIndividual != null){
                            response.put('privacyChange', privacyChangeByIndividual);
                        }
                    }
                }
                response.put('individualId', individualId);
                //response.put('opportunityId', opportunityId);


            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getIndividualPrivacyRecap extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String individualId = '';
            String label = '';
            try {

                String accountId = params.get('accountId');


                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account acc = accountQuery.findAccount(accountId);
                List<AccountContactRelation> listContacts= accountQuery.listContacts(accountId);

                if(acc.IsPersonAccount){
                    individualId = acc.PersonIndividualId;
                    if(String.isBlank(individualId)){
                        label = System.Label.PersonIndividualNotSet;
                    }

                }else {

                    if (!listContacts.isEmpty()) {

                        for (AccountContactRelation accContRelation : listContacts) {

                            if (accContRelation.Contact.IndividualId != null) {
                                individualId = accContRelation.Contact.IndividualId;
                            }
                        }
                        if(String.isBlank(individualId)){
                            label = System.Label.LegalRepresentativeNotSet;
                        }
                    }else{
                        if(String.isBlank(individualId)){
                            label = System.Label.LegalRepresentativeNotSet;
                        }
                    }
                }


                response.put('individualId', individualId);
                response.put('label', label);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}