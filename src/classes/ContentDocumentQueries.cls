public with sharing class ContentDocumentQueries {

    public static ContentDocumentQueries getInstance() {
        return (ContentDocumentQueries)ServiceLocator.getInstance(ContentDocumentQueries.class);
    }
/*
    public List<ContentDocument> listRecent(Integer sizeLimit) {
        return [
            SELECT Id, Title, CreatedDate, OwnerId
            FROM ContentDocument
            ORDER BY CreatedDate DESC
            LIMIT :sizeLimit
        ];
    }
*/
    public ContentDocument findById(String documentId) {
        return [
            SELECT Id, Title
            FROM ContentDocument
            WHERE Id = :documentId
        ];
    }
}