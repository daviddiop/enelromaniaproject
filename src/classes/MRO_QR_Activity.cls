/**
 * @author  Stefano Porcari
 * @since   Dec 24, 2019
 * @desc   Query class for wrts_prcgvr__Activity__c object
 *
 */
public with sharing class MRO_QR_Activity {
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    public static MRO_QR_Activity getInstance() {
        return (MRO_QR_Activity) ServiceLocator.getInstance(MRO_QR_Activity.class);
    }

    /**
    * @author  David DIOP
    * @description get list Activities by accountId  where type = Customer Retention
    * Updated by BG -  21/02/2020
    * @see [ENLCRO-601] Customer feedback to be corrected
    * @date 24/01/2020
    * @param List<Case> caseList
    * @return List<wrts_prcgvr__Activity__c>
    */
    public List<wrts_prcgvr__Activity__c> getActivityByAccount(String accountId) {
        return [
                SELECT Id,Type__c,Dossier__c,Account__c
                FROM wrts_prcgvr__Activity__c
                WHERE Type__c = :constantsSrv.CUSTOMER_RETENTION AND Account__c = :accountId
        ];
    }

    /**
    * @author Baba Goudiaby
    * @description get list Activities by accountId  where type = Dossier Retention
    * @see [ENLCRO-601] Customer feedback to be corrected
    * @date 07/04/2020
    * @param List<Case> caseList
    * @return List<wrts_prcgvr__Activity__c>
    */
    public List<wrts_prcgvr__Activity__c> getActivityGasByDossier(String dossierId) {
        return [
                SELECT Id,Type__c,Dossier__c,Account__c
                FROM wrts_prcgvr__Activity__c
                WHERE Type__c = :constantsSrv.DOSSIER_RETENTION_GAS AND Dossier__c = :dossierId
        ];
    }

    /**
* @author Baba Goudiaby
* @description get list Activities by accountId  where type = Dossier Retention
* @see [ENLCRO-601] Customer feedback to be corrected
* @date 07/04/2020
* @param List<Case> caseList
* @return List<wrts_prcgvr__Activity__c>
*/
    public List<wrts_prcgvr__Activity__c> getActivityElectricByDossier(String dossierId) {
        return [
                SELECT Id,Type__c,Dossier__c,Account__c
                FROM wrts_prcgvr__Activity__c
                WHERE Type__c = :constantsSrv.DOSSIER_RETENTION_ELE AND Dossier__c = :dossierId
        ];
    }

    /**
   * @author  Baba Goudiaby
   * @description get Activity by Id
   * @date 13/02/2020
   * @param String activityId
   * @return wrts_prcgvr__Activity__c activiyy
   */
    public wrts_prcgvr__Activity__c getById(String activityId) {
        List<wrts_prcgvr__Activity__c> activities = [
                SELECT
                    Id,wrts_prcgvr__IsClosed__c,Parent__c,Case__c,Case__r.Status,Case__r.Supply__c,Case__r.Phase__c,Case__r.Supply__r.ServicePoint__c,
                    wrts_prcgvr__Status__c,Type__c,Dossier__c,wrts_prcgvr__DueDate__c,wrts_prcgvr__ObjectId__c,
                    FieldsTemplate__c, wrts_prcgvr__Description__c, AddressType__c, toLabel(AddressType__c) AddressTypeLabel,
                    SendingAddressNormalized__c, SendingStreetType__c,
                    SendingStreetId__c, SendingAddressKey__c,
                    SendingStreetName__c, SendingStreetNumber__c,
                    SendingStreetNumberExtn__c, SendingApartment__c,
                    SendingBuilding__c, SendingBlock__c,
                    SendingCity__c, SendingCountry__c,
                    SendingFloor__c,SendingPostalCode__c,
                    SendingProvince__c, SendingLocality__c
                FROM wrts_prcgvr__Activity__c
                WHERE Id =:activityId
        ];

        if (activities !=null && !activities.isEmpty()) {
            return activities.get(0);
        }
        return null;
    }

    public Map<Id, wrts_prcgvr__Activity__c> getByIds(Set<Id> activityIds) {
        return new Map<Id, wrts_prcgvr__Activity__c>([
                SELECT
                    Id,wrts_prcgvr__IsClosed__c,Parent__c,Case__c,Case__r.Status,Case__r.Supply__c,Case__r.Phase__c,
                    wrts_prcgvr__Status__c,Type__c,Dossier__c,wrts_prcgvr__DueDate__c,wrts_prcgvr__ObjectId__c,
                    FieldsTemplate__c, wrts_prcgvr__Description__c, DoNotSyncCases__c,
                    AddressType__c, toLabel(AddressType__c) AddressTypeLabel,
                    SendingAddressNormalized__c, SendingStreetType__c,
                    SendingStreetId__c, SendingAddressKey__c,
                    SendingStreetName__c, SendingStreetNumber__c,
                    SendingStreetNumberExtn__c, SendingApartment__c,
                    SendingBuilding__c, SendingBlock__c,
                    SendingCity__c, SendingCountry__c,
                    SendingFloor__c,SendingPostalCode__c,
                    SendingProvince__c, SendingLocality__c
                FROM wrts_prcgvr__Activity__c
                WHERE Id IN :activityIds
        ]);
    }

    /**
    * @author  Baba Goudiaby
    * @description get list Activities by Parent Id
    * @see [ENLCRO-601] Customer feedback to be corrected
    * @date 13/02/2020
    * @param String parentId
    * @return List<wrts_prcgvr__Activity__c> child activiyies
    */
    public List<wrts_prcgvr__Activity__c> getActivitiesByParentId(String parentId) {
        return [
                SELECT Id,wrts_prcgvr__IsClosed__c,Parent__c,Case__c,Case__r.Supply__c,wrts_prcgvr__Status__c,Case__r.Supply__r.ServicePoint__c,
                        Type__c,Dossier__c,wrts_prcgvr__DueDate__c,CancellationReason__c,CancellationDetails__c
                FROM wrts_prcgvr__Activity__c
                WHERE Parent__c =: parentId AND wrts_prcgvr__IsClosed__c = FALSE AND wrts_prcgvr__DueDate__c >= TODAY
                AND Case__r.Phase__c != 'RC010' AND Case__r.Supply__c != NULL
        ];
    }

    public wrts_prcgvr__Activity__c getByFileMetadataAndType(String fileMetadataId, String type) {
        List<wrts_prcgvr__Activity__c> activities = [
                SELECT Id, wrts_prcgvr__IsClosed__c
                FROM wrts_prcgvr__Activity__c
                WHERE wrts_prcgvr__ObjectId__c = :fileMetadataId AND wrts_prcgvr__IsClosed__c = FALSE
                    AND Type__c = :type
        ];
        if (!activities.isEmpty()) {
            return activities.get(0);
        }
        return null;
    }

    public List<wrts_prcgvr__Activity__c> findOpenValidationActivitiesByParentRecordId(Id parentRecordId) {
        return [SELECT Id, Name, wrts_prcgvr__IsClosed__c, DoNotSyncCases__c,
                    (SELECT FileMetadata__c, DocumentBundleItem__r.Mandatory__c FROM Validatable_Documents__r)
                FROM wrts_prcgvr__Activity__c
                WHERE wrts_prcgvr__ObjectId__c = :parentRecordId
                AND wrts_prcgvr__IsClosed__c = false
                AND Type__c = :constantsSrv.ACTIVITY_DOCUMENT_VALIDATION_TYPE];
    }

    public List<wrts_prcgvr__Activity__c> getActivitiesByDossierId(List<Case> caseList, String type) {
        return[
                SELECT Id,Type__c,Dossier__c,Dossier__r.Account__c
                FROM wrts_prcgvr__Activity__c
                WHERE Type__c = :type AND Dossier__r.Account__c =: caseList[0].AccountId
        ];
    }

    /**
    * @author  David DIOP
    * @description get list Activities by caseIds
    * @date 29/01/2020
    * @param Set<Id> caseIds
    * @return Map<Id, wrts_prcgvr__Activity__c>
    */
    public Map<Id, wrts_prcgvr__Activity__c> getByCaseId(Set<Id> caseIds) {
        return new Map<Id, wrts_prcgvr__Activity__c>([
                SELECT Id,wrts_prcgvr__IsClosed__c,Case__c, wrts_prcgvr__Status__c,
                        Type__c,Dossier__c,wrts_prcgvr__DueDate__c
                FROM wrts_prcgvr__Activity__c
                WHERE Case__c IN :caseIds
        ]);
    }

    public Map<Id, wrts_prcgvr__Activity__c> getByDossierId(Set<Id> dossierIds) {
        return new Map<Id, wrts_prcgvr__Activity__c>([
                SELECT Id,wrts_prcgvr__IsClosed__c,Case__c, wrts_prcgvr__Status__c,
                        Type__c,Dossier__c,wrts_prcgvr__DueDate__c
                FROM wrts_prcgvr__Activity__c
                WHERE Dossier__c IN :dossierIds
        ]);
    }

    /**
    * @description get List Activities Retention by CasesId
    * @date 13/08/2020
    * @param Set<Id> caseIds
    * @return Map<Id, wrts_prcgvr__Activity__c>
    */
    public Map<Id, wrts_prcgvr__Activity__c> getRetentionsByCaseId(Set<Id> caseIds) {
        return new Map<Id, wrts_prcgvr__Activity__c>([
                SELECT Id,wrts_prcgvr__IsClosed__c,Case__c, wrts_prcgvr__Status__c,
                        Type__c,Dossier__c,wrts_prcgvr__DueDate__c
                FROM wrts_prcgvr__Activity__c
                WHERE Case__c IN :caseIds AND Case__c != NULL
                AND wrts_prcgvr__IsClosed__c = FALSE
                AND Type__c IN ('Service Point Retention', 'Dossier Retention Gas', 'Dossier Retention Electric', 'Product Change', 'Customer Retention')
        ]);
    }

    public Map<Id, wrts_prcgvr__Activity__c> getTacitRenewalActivitiesByOpportunities(Set<Id> opportunyIds, Id tacitRenewalCampaignId) {
        List<wrts_prcgvr__Activity__c> activities = [SELECT Id, Account__c, Contact__c, Opportunity__c, wrts_prcgvr__ObjectId__c, wrts_prcgvr__Status__c
                                                     FROM wrts_prcgvr__Activity__c
                                                     WHERE Campaign__c = :tacitRenewalCampaignId AND Opportunity__c IN :opportunyIds];
        Map<Id, wrts_prcgvr__Activity__c> result = new Map<Id, wrts_prcgvr__Activity__c>();
        for (wrts_prcgvr__Activity__c act : activities) {
            result.put(act.Opportunity__c, act);
        }
        return result;
    }

    public Map<Id, wrts_prcgvr__Activity__c> getTacitRenewalActivitiesByOpportunities(Set<Id> opportunyIds, Set<Id> tacitRenewalCampaignIds) {
        List<wrts_prcgvr__Activity__c> activities = [SELECT Id, Account__c, Contact__c, Opportunity__c, wrts_prcgvr__ObjectId__c, wrts_prcgvr__Status__c
                                                     FROM wrts_prcgvr__Activity__c
                                                     WHERE Campaign__c IN :tacitRenewalCampaignIds AND Opportunity__c IN :opportunyIds];
        Map<Id, wrts_prcgvr__Activity__c> result = new Map<Id, wrts_prcgvr__Activity__c>();
        for (wrts_prcgvr__Activity__c act : activities) {
            result.put(act.Opportunity__c, act);
        }
        return result;
    }

    public List<wrts_prcgvr__Activity__c> listOtherTacitRenewalActivities(Set<Id> campaignIds, Set<Id> campaignMemberIds, Set<Id> excludeActivityIds) {
        return [SELECT Id, wrts_prcgvr__ObjectId__c FROM wrts_prcgvr__Activity__c
                WHERE Type__c = :constantsSrv.ACTIVITY_TACIT_RENEWAL_TYPE AND wrts_prcgvr__IsClosed__c = FALSE AND Campaign__c IN :campaignIds
                    AND wrts_prcgvr__ObjectId__c IN :campaignMemberIds AND Id NOT IN :excludeActivityIds];
    }

    public Map<Id, List<wrts_prcgvr__Activity__c>> listOpenActivitiesByTypesAndParentIds(Set<String> types, Set<Id> parentRecordIds) {
        List<wrts_prcgvr__Activity__c> activities = [SELECT Id, Type__c, wrts_prcgvr__ObjectId__c FROM wrts_prcgvr__Activity__c
                                                     WHERE wrts_prcgvr__ObjectId__c IN :parentRecordIds
                                                         AND wrts_prcgvr__IsClosed__c = FALSE AND Type__c IN :types];
        Map<Id, List<wrts_prcgvr__Activity__c>> result = new Map<Id, List<wrts_prcgvr__Activity__c>>();
        for (wrts_prcgvr__Activity__c act : activities) {
            if (result.containsKey(act.wrts_prcgvr__ObjectId__c)) {
                result.get(act.wrts_prcgvr__ObjectId__c).add(act);
            } else {
                result.put(act.wrts_prcgvr__ObjectId__c, new List<wrts_prcgvr__Activity__c>{act});
            }
        }
        return result;
    }

    public Map<Id, List<wrts_prcgvr__Activity__c>> listOpenMandatoryActivitiesByParentIds(Set<Id> parentRecordIds) {
        List<wrts_prcgvr__Activity__c> activities = [SELECT Id, Type__c, wrts_prcgvr__ObjectId__c FROM wrts_prcgvr__Activity__c
                                                     WHERE wrts_prcgvr__ObjectId__c IN :parentRecordIds
                                                         AND wrts_prcgvr__IsClosed__c = FALSE AND wrts_prcgvr__IsRequired__c = TRUE];
        Map<Id, List<wrts_prcgvr__Activity__c>> result = new Map<Id, List<wrts_prcgvr__Activity__c>>();
        for (wrts_prcgvr__Activity__c act : activities) {
            if (result.containsKey(act.wrts_prcgvr__ObjectId__c)) {
                result.get(act.wrts_prcgvr__ObjectId__c).add(act);
            } else {
                result.put(act.wrts_prcgvr__ObjectId__c, new List<wrts_prcgvr__Activity__c>{act});
            }
        }
        return result;
    }

    public List<wrts_prcgvr__Activity__c> getServicePointRetentionActivitiesByParentIds(Set<Id> parentIds) {
        return [
                SELECT
                    Id,wrts_prcgvr__IsClosed__c,Parent__c,Case__c,Case__r.Supply__c,wrts_prcgvr__Status__c,
                    Type__c,Dossier__c,wrts_prcgvr__DueDate__c,CancellationReason__c,CancellationDetails__c
                FROM
                    wrts_prcgvr__Activity__c
                WHERE
                    Parent__c IN :parentIds AND
                    Type__c = :constantsSrv.ACTIVITY_CASE_RETENTION_TYPE
        ];
    }

    public List<wrts_prcgvr__Activity__c> getOpenFileCheckActivitiesByParentIds(Set<Id> parentRecordIds) {
        return [
                SELECT Id, wrts_prcgvr__ObjectId__c, wrts_prcgvr__Status__c
                FROM wrts_prcgvr__Activity__c
                WHERE wrts_prcgvr__ObjectId__c IN :parentRecordIds
                AND wrts_prcgvr__IsClosed__c = FALSE AND Type__c = 'File Check'
        ];
    }

    public List<wrts_prcgvr__Activity__c> getActivityByAccountAndType(String accountId, String activityType) {
        return [
                SELECT Id,Type__c,Dossier__c,Account__c
                FROM wrts_prcgvr__Activity__c
                WHERE Type__c = :activityType AND Account__c = :accountId
        ];
    }

    public List<wrts_prcgvr__Activity__c> getActivityByDossierAndType(String dossierId, String activityType) {
        return [
                SELECT Id,Type__c,Dossier__c,Account__c
                FROM wrts_prcgvr__Activity__c
                WHERE Type__c = :activityType AND Dossier__c = :dossierId
        ];
    }
    public List<wrts_prcgvr__Activity__c> getActivityListByParentId(String parentId) {
        return [
                SELECT Id,wrts_prcgvr__IsClosed__c,Parent__c,Case__c,Case__r.Supply__c,wrts_prcgvr__Status__c,Case__r.Supply__r.ServicePoint__c,
                        Type__c,Dossier__c,wrts_prcgvr__DueDate__c,CancellationReason__c,CancellationDetails__c
                FROM wrts_prcgvr__Activity__c
                WHERE Parent__c =: parentId AND wrts_prcgvr__IsClosed__c = FALSE
                AND Case__r.Phase__c != 'RC010' AND Case__r.Supply__c != NULL
        ];
    }

    public  List<wrts_prcgvr__Activity__c> getActivitiesByCaseIdAndType(Set<Id> caseIds, List<String> activityType) {
        return new List<wrts_prcgvr__Activity__c>([
                SELECT Id,wrts_prcgvr__IsClosed__c,Case__c, wrts_prcgvr__Status__c,
                        Type__c,Dossier__c,wrts_prcgvr__DueDate__c
                FROM wrts_prcgvr__Activity__c
                WHERE Case__c IN :caseIds AND Type__c IN :activityType
        ]);
    }

    public List<wrts_prcgvr__Activity__c> getActivityListByDossierId(Set<Id> dossierIds) {
        return new List<wrts_prcgvr__Activity__c>([
                SELECT Id,wrts_prcgvr__IsClosed__c,Case__c, wrts_prcgvr__Status__c,
                        Type__c,Dossier__c,wrts_prcgvr__DueDate__c
                FROM wrts_prcgvr__Activity__c
                WHERE Dossier__c IN :dossierIds
        ]);
    }
}