/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 23, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_CampaignMemberStatus {
    private static MRO_QR_CampaignMemberStatus campaignMemberStatusQueries = MRO_QR_CampaignMemberStatus.getInstance();
    private static MRO_QR_CampaignMemberStatusConfig campaignMemberStatusConfigQueries = MRO_QR_CampaignMemberStatusConfig.getInstance();
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    public static MRO_SRV_CampaignMemberStatus getInstance() {
        return (MRO_SRV_CampaignMemberStatus) ServiceLocator.getInstance(MRO_SRV_CampaignMemberStatus.class);
    }

    public void configureCampaignMemberStatuses(List<Campaign> campaigns) {
        Savepoint sp = Database.setSavepoint();
        try {
            Set<Id> campaignIds = (new Map<Id, Campaign>(campaigns)).keySet();
            Map<Id, List<CampaignMemberStatus>> existingStatusesMap = campaignMemberStatusQueries.getCampaignMemberStatusesByCampaignIds(campaignIds);
            List<CampaignMemberStatus> campaignMemberStatusesToInsert = new List<CampaignMemberStatus>();
            List<CampaignMemberStatus> campaignMemberStatusesToUpdate = new List<CampaignMemberStatus>();
            List<CampaignMemberStatus> campaignMemberStatusesToDelete = new List<CampaignMemberStatus>();
            Set<String> campaignTypes = new Set<String>();
            for (Campaign camp : campaigns) {
                if (camp.Type != null) {
                    campaignTypes.add(camp.Type);
                }
            }
            Map<String, List<CampaignMemberStatusConfig__mdt>> configsMap = campaignMemberStatusConfigQueries.getCampaignMemberStatusConfigsByCampaignTypes(campaignTypes);

            //Insert missing statuses
            for (Campaign camp : campaigns) {
                if (camp.Type != null && configsMap.containsKey(camp.Type)) {
                    //System.debug('### Managing campaign (1): ' + JSON.serializePretty(camp));
                    Map<String, CampaignMemberStatus> existingStatuses = getStatusesMap(existingStatusesMap.get(camp.Id));
                    //System.debug('### Existing statuses (1): ' + JSON.serializePretty(existingStatuses));
                    Integer maxOrder = existingStatuses.size();
                    List<CampaignMemberStatusConfig__mdt> configs = configsMap.get(camp.Type);
                    for (CampaignMemberStatusConfig__mdt conf : configs) {
                        //System.debug('### Config (1): ' + JSON.serializePretty(conf));
                        if (!existingStatuses.containsKey(conf.MasterLabel)) {
                            //System.debug('### Config not found. Adding ' + conf.MasterLabel);
                            maxOrder++;
                            CampaignMemberStatus newStatus = new CampaignMemberStatus(
                                CampaignId = camp.Id,
                                IsDefault = false,
                                Label = conf.MasterLabel,
                                HasResponded = conf.HasRespondedStatus__c,
                                SortOrder = maxOrder
                            );
                            //System.debug('### newStatus: ' + JSON.serializePretty(newStatus));
                            campaignMemberStatusesToInsert.add(newStatus);
                        }
                    }
                }
            }
            //System.debug('### statuses to be inserted: ' + JSON.serializePretty(campaignMemberStatusesToInsert));
            databaseSrv.insertSObject(campaignMemberStatusesToInsert);
            //System.debug('### new inserted statuses: ' + JSON.serializePretty(campaignMemberStatusesToInsert));
            for (CampaignMemberStatus newStatus : campaignMemberStatusesToInsert) {
                existingStatusesMap.get(newStatus.CampaignId).add(newStatus);
            }

            //Updating existing statuses and deleting old ones
            for (Campaign camp : campaigns) {
                if (camp.Type != null && configsMap.containsKey(camp.Type)) {
                    //System.debug('### Managing campaign (2): ' + JSON.serializePretty(camp));
                    Map<String, CampaignMemberStatus> existingStatuses = getStatusesMap(existingStatusesMap.get(camp.Id));
                    //System.debug('### Existing statuses (2): ' + JSON.serializePretty(existingStatuses));
                    List<CampaignMemberStatusConfig__mdt> configs = configsMap.get(camp.Type);
                    for (CampaignMemberStatusConfig__mdt conf : configs) {
                        //System.debug('### Config (2): ' + JSON.serializePretty(conf));
                        CampaignMemberStatus status = existingStatuses.get(conf.MasterLabel);
                        //System.debug('### Existing status - before: ' + JSON.serializePretty(status));
                        status.IsDefault = conf.IsDefaultStatus__c;
                        status.HasResponded = conf.HasRespondedStatus__c;
                        status.SortOrder = conf.SortOrder__c.intValue();
                        //System.debug('### Existing status - after: ' + JSON.serializePretty(status));
                        campaignMemberStatusesToUpdate.add(status);
                        existingStatuses.remove(conf.MasterLabel);
                    }
                    //System.debug('### Existing statuses to be deleted: ' + existingStatuses.keySet());
                    campaignMemberStatusesToDelete.addAll(existingStatuses.values());
                }
            }
            //System.debug('### statuses to be deleted: ' + JSON.serializePretty(campaignMemberStatusesToDelete));
            databaseSrv.deleteSObject(campaignMemberStatusesToDelete);
            //System.debug('### statuses to be updated: ' + JSON.serializePretty(campaignMemberStatusesToUpdate));
            databaseSrv.updateSObject(campaignMemberStatusesToUpdate);
        }
        catch (Exception e) {
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
            throw e;
        }
    }

    private static Map<String, CampaignMemberStatus> getStatusesMap(List<CampaignMemberStatus> statuses) {
        Map<String, CampaignMemberStatus> result = new Map<String, CampaignMemberStatus>();
        for (CampaignMemberStatus status : statuses) {
            result.put(status.Label, status);
        }
        return result;
    }
}