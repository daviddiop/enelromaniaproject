public inherited sharing class ScriptTemplateQueries {

    private static ScriptTemplateQueries instance = null;

    public static ScriptTemplateQueries getInstance() {
        if(instance == null) instance = new ScriptTemplateQueries();
        return instance;
    }

    public List<ScriptTemplateElement__c> getByScriptTemplateId(String templateId) {
        return [SELECT Id, HTMLContent__c, Name, RecordTypeId, RecordType.DeveloperName, Variable__c 
            FROM ScriptTemplateElement__c 
            WHERE ScriptTemplate__c=:templateId 
            ORDER BY Order__c];
    }
}