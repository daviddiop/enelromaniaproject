/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 08, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_UTL_ApexContext {
    public static Boolean FORCE_CALLOUTS_RETRIGGERING = false;
}