/**
 * Created by Antonella Loche on 23/01/2020.
 */
    
@IsTest
public with sharing class  MRO_LC_RepaymentScheduleTst {
    @TestSetup
    static void setup() {
        String vat = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
 
        Sequencer__c seq = new Sequencer__c();
        seq.Type__c = 'CustomerCode';
        seq.SequenceLength__c=9;
        seq.Sequence__c=2507;
        Insert seq;
        System.debug('SEQUENCE');
        
        List<Account> listAccount = new list<Account>();
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();
        
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        
		Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[1].Id;
        insert contract;
        
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        
		CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;        

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;        

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[1].Id;
        insert dossier;
        
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Account__c = listAccount[1].Id;
        servicePoint.Trader__c = listAccount[1].Id;
        servicePoint.Distributor__c = listAccount[1].Id;
        servicePoint.ENELTEL__c = '1234567';
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[1].Id;
        billingProfile.PaymentMethod__c = 'Giro';
        insert billingProfile;
        
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Account__c = listAccount[1].Id;
            supply.ServicePoint__c = servicePoint.Id;
            supply.CompanyDivision__c = companyDivision.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[i].Id;
            caseRecord.CompanyDivision__c = supplyList[i].CompanyDivision__c;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
        
        String RecordTypeId = Schema.SObjectType.TaxSubsidy__c.getRecordTypeInfosByDeveloperName().get('VAT').getRecordTypeId();
        
		Case repaymentScheduleCase = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
        repaymentScheduleCase.AccountId = listAccount[1].Id;
        repaymentScheduleCase.Amount__c = 1000;
        repaymentScheduleCase.StartDate__c = Date.today();
        repaymentScheduleCase.Count__c = 1.00;
        repaymentScheduleCase.MarkedInvoices__c = null;
        repaymentScheduleCase.RecordTypeId = RecordTypeId;
        repaymentScheduleCase.Dossier__c = dossier.id;
        repaymentScheduleCase.Status = 'Draft';
		repaymentScheduleCase.CompanyDivision__c = supplyList[0].CompanyDivision__c;
    }
    
    @IsTest
    public static void initialize() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id, Status__c
                FROM Dossier__c
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String> {
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'customerInteractionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivisionId
        };
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();
	}

    @IsTest
    public static void initializeInvalidAccount() {
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id, Status__c
                FROM Dossier__c
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        
        Test.startTest();
 		Map<String, String> inputJSON = new Map<String, String> {
                'accountId' => null,
                'dossierId' => dossier.Id,
                'customerInteractionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivisionId
        };
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
 
        Test.stopTest();
	}

    @IsTest
    public static void initializeError() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String> {
                'accountId' => account.Id,
                'dossierId' => '',
                'customerInteractionId' => '',
                'companyDivisionId' => ''
        };
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
	}

    @IsTest
    public static void createCaseTest() {
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Trader__c,AccountId, RecordTypeId
                FROM Case
                LIMIT 2
        ];
        List<Account> myAccount = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                LIMIT 1
        ];        
        Test.startTest();
        
         // Create Invoice 
		List <MRO_LC_InvoicesInquiry.Invoice> invoiceList = MRO_LC_InvoicesInquiry.listDummyInvoice(null);
        String invoiceListStr = JSON.serialize(invoiceList);
        
        MRO_LC_RepaymentSchedule.CreateCaseInput createCaseInput = new MRO_LC_RepaymentSchedule.CreateCaseInput();
        createCaseInput.totalAmount = 100;
        createCaseInput.staggerStartDate = Date.today();
        createCaseInput.numberOfInstallments = 10;
        createCaseInput.companyDivisionId = companyDivision.id;
        createCaseInput.accountId = myAccount[0].Id;
        createCaseInput.dossierId = dossier.Id;
        createCaseInput.listOfInvoices = invoiceListStr;
        
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'createCase', createCaseInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
	}
    
    @IsTest
    public static void createCaseErrorTest() {
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Trader__c,AccountId, RecordTypeId
                FROM Case
                LIMIT 2
        ];
        List<Account> myAccount = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                LIMIT 1
        ];        
        Test.startTest();
        
         // Create Invoice 
		List <MRO_LC_InvoicesInquiry.Invoice> invoiceList = MRO_LC_InvoicesInquiry.listDummyInvoice(null);
        String invoiceListStr = JSON.serialize(invoiceList);
        
        MRO_LC_RepaymentSchedule.CreateCaseInput createCaseInput = new MRO_LC_RepaymentSchedule.CreateCaseInput();
        createCaseInput.totalAmount = 100;
        createCaseInput.staggerStartDate = Date.today();
        createCaseInput.numberOfInstallments = 10;
        createCaseInput.companyDivisionId = companyDivision.id;
        createCaseInput.accountId = myAccount[0].Id;
        createCaseInput.dossierId = myAccount[0].Id;
        createCaseInput.listOfInvoices = invoiceListStr;
        
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'createCase', createCaseInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
	}

    @IsTest
    public static void createDraftCaseTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
       CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                LIMIT 1
        ];        
  

         // Create Invoice 
		List <MRO_LC_InvoicesInquiry.Invoice> invoiceList = MRO_LC_InvoicesInquiry.listDummyInvoice(null);
        String invoiceListStr = JSON.serialize(invoiceList);
        
        MRO_LC_RepaymentSchedule.CreateCaseInput createCaseInput = new MRO_LC_RepaymentSchedule.CreateCaseInput();
        createCaseInput.totalAmount = 1000;
        createCaseInput.staggerStartDate = Date.today();
        createCaseInput.numberOfInstallments = 10;
        createCaseInput.companyDivisionId = companyDivision.Id;
        createCaseInput.accountId = account.Id;
        createCaseInput.dossierId = dossier.Id;
        createCaseInput.listOfInvoices = invoiceListStr;
    
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'createDraftCase', createCaseInput, true);

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        
        Test.stopTest();
	}
 
    @IsTest
    public static void createDraftCaseErrorTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
       CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                LIMIT 1
        ];        
  

         // Create Invoice 
		List <MRO_LC_InvoicesInquiry.Invoice> invoiceList = MRO_LC_InvoicesInquiry.listDummyInvoice(null);
        String invoiceListStr = JSON.serialize(invoiceList);
        
        MRO_LC_RepaymentSchedule.CreateCaseInput createCaseInput = new MRO_LC_RepaymentSchedule.CreateCaseInput();
        createCaseInput.totalAmount = 1000;
        createCaseInput.staggerStartDate = Date.today();
        createCaseInput.numberOfInstallments = 10;
        createCaseInput.companyDivisionId = account.Id;
        createCaseInput.accountId = account.Id;
        createCaseInput.dossierId = account.Id;
        createCaseInput.listOfInvoices = invoiceListStr;
    
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'createDraftCase', createCaseInput, true);

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
       
        Test.stopTest();
	}
 
    @IsTest
    public static void cancelRepaymentTest(){
        Dossier__c dossier = [
                SELECT Id, Status__c
                FROM Dossier__c
                LIMIT 1
        ];
        

        Test.startTest();
       Map<String, String > inputJSON = new Map<String, String> {
                'dossierId' => dossier.id
        };
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'cancelRepayment', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
      
        Test.stopTest();
	}

    @IsTest
    public static void cancelRepaymentErrorTest(){
        Dossier__c dossier = [
                SELECT Id, Status__c
                FROM Dossier__c
                LIMIT 1
        ];
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];

       Test.startTest();
       Map<String, String > inputJSON = new Map<String, String> {
                'dossierId' => account.id
        };
        Object response = TestUtils.exec(
                'MRO_LC_RepaymentSchedule', 'cancelRepayment', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
     
        Test.stopTest();
	}
    
}