/**
 * Created by goudiaby on 31/12/2019.
 */
/**
 * @description Class for cancellation Component
 */
public with sharing class MRO_LC_CancellationRequest extends ApexServiceLibraryCnt {
    private static MRO_SRV_CancellationRequest cancellationRequestSrv = MRO_SRV_CancellationRequest.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_SRV_Opportunity opportunitySrv = MRO_SRV_Opportunity.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
    private static MRO_QR_Activity activityQuery = MRO_QR_Activity.getInstance();

    /**
     * @description Class to Check Conditions to proceed to the cancellation
     */
    public class CheckConditions extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String requestId = params.get('objectId');
            if (String.isBlank(requestId)) {
                throw new WrtsException(requestId + ' - ' + System.Label.MissingId);
            }
            if (requestId.startsWith('500')){
                response = MRO_UTL_Cancellation.isCancellable(requestId);
            }else {
                response.put('isCancellable',true);
            }
            return response;
        }
    }

    /**
     * @description Class to get fields for Fieldset Object's
     */
    public class GetFieldset extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String objectName = params.get('objectName');
            response = LightningUtils.getFieldSet(objectName, 'Cancellation');
            return response;
        }
    }

    /**
   * @description Class to get Company Division Data from User id and all company Division
   */
    public with sharing class GetCancellationReasons extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String objectName = params.get('objectName');
            String recordType = params.get('recordType');
            String originPhase = params.get('originPhase');
            List<ApexServiceLibraryCnt.Option> cancellationReasonsOptions = cancellationRequestSrv.getListCancellationReasons(objectName, recordType, originPhase);
            /*if (cancellationReasonsOptions == null || cancellationReasonsOptions.isEmpty()) {
                response.put('warn', System.Label.EmptyCancellationReasons);
            }*/
            response.put('listCancellationReasons', cancellationReasonsOptions);
            return response;
        }
    }

    /**
    * @description Class to update Sobject
    */
    public with sharing class UpdateSobjectCancellation extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String objectId = params.get('objectId');
            String objectName = params.get('objectName');
            String cancellationCause = params.get('cancellationReason');
            String cancellationDetails = params.get('cancellationDetails');
            Boolean isLocationApproval = false;
            Boolean isContractualDataChange = false;
            Boolean isProsumers = false;

            if (String.isBlank(objectId)) {
                throw new WrtsException(objectName + ' - ' + System.Label.MissingId);
            }
            switch on objectName {
                when 'Case' {
                    Case myCase = caseQuery.getForCancellationById(objectId);

                    if (myCase.RecordType.DeveloperName == 'Location_Approval') {
                        isLocationApproval = true;
                    }

                    if (myCase == null || myCase.Status == 'Canceled') {
                        response.put('error', System.Label.RequestAlreadyCancelled);
                        return response;
                    } else {
                        List <wrts_prcgvr__PhaseTransition__c> transitions = transitionsUtl.getTransitions(myCase, 'Cancellation', 'A');
                        List <wrts_prcgvr__PhaseTransition__c> manualTransitions = transitionsUtl.getTransitions(myCase, 'Cancellation', 'M');

                        if (!(manualTransitions.isEmpty() || manualTransitions.size() > 1) && isLocationApproval) {
                            cancelActivitiesOnCase(myCase);
                        }

                        if (transitions.isEmpty() || transitions.size() > 1) {
                            String template = Label.NotFound + '.{1}';
                            List<Object> parameters = new List<Object>{
                                Label.CancellationTransition, Label.ContactSalesforceAdministrator
                            };
                            response.put('error', String.format(template, parameters));
                        } else {
                            response = caseSrv.cancelCase(myCase, cancellationCause, cancellationDetails, transitions[0]);
                        }
                    }
                }
                when 'Dossier__c' {
                    Dossier__c dossier = dossierQuery.getById(objectId);
                    List <wrts_prcgvr__PhaseTransition__c> manualTransitions = transitionsUtl.getTransitions(dossier, 'Cancellation', 'M');

                    if (dossier.RequestType__c == 'Contractual Data Change') {
                        isContractualDataChange = true;
                    } else if (dossier.RequestType__c == 'Prosumers') {
                        isProsumers = true;
                    }

                    if (!(manualTransitions.isEmpty() || manualTransitions.size() > 1) && (isContractualDataChange || isProsumers)) {
                        cancelActivitiesOnDossier(dossier);
                    }

                    response = dossierSrv.cancelDossierAndCases(dossier, cancellationCause, cancellationDetails);
                }
            }
            return response;
        }
    }


    public with sharing class updateCancellationReasons extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String cancellationReason = params.get('cancellationReason');
            String cancellationDetails = params.get('cancellationDetails');

            try{
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.CloseDate=System.today();
                opp.StageName=constantsSrv.OPPORTUNITY_STAGE_CLOSED_LOST;
                databaseSrv.upsertSObject(opp);
                List<Dossier__c> dossiers=dossierQuery.listDossiersByOpportunity(opportunityId);
                if(!dossiers.isEmpty()){
                    Dossier__c dossier= dossiers.get(0);
                    dossierSrv.updateDossierCancellationReason(dossier,cancellationReason, cancellationDetails);
                    List<Case> cases= caseQuery.getCasesByDossierId(dossier.Id);
                    if(!cases.isEmpty()){
                        caseSrv.updateCaseCancellationReason(cases,cancellationReason, cancellationDetails);
                    }
                }

                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public static void cancelActivitiesOnCase(Case caseToCancel) {
        Set<Id> caseIdSet = new Set<Id>();
        caseIdSet.add(caseToCancel.Id);
        Map<Id, wrts_prcgvr__Activity__c> activityMap = activityQuery.getByCaseId(caseIdSet);

        cancelActivities(activityMap);
    }

    public static void cancelActivitiesOnDossier(Dossier__c dossierToCancel) {
        Set<Id> dossierIdSet = new Set<Id>();
        dossierIdSet.add(dossierToCancel.Id);
        Map<Id, wrts_prcgvr__Activity__c> activityMap = activityQuery.getByDossierId(dossierIdSet);

        cancelActivities(activityMap);
    }

    public static void cancelActivities(Map<Id, wrts_prcgvr__Activity__c> activityMap) {
        List<wrts_prcgvr__Activity__c> activitiesToCancel = new List<wrts_prcgvr__Activity__c>();

        if (activityMap.size() > 0) {
            for (wrts_prcgvr__Activity__c activity : activityMap.values()) {
                if (activity.wrts_prcgvr__IsClosed__c == false) {
                    activity.wrts_prcgvr__Status__c = constantsSrv.ACTIVITY_STATUS_CANCELLED;
                    activity.wrts_prcgvr__IsClosed__c = true;
                    activity.wrts_prcgvr__Phase__c = constantsSrv.CASE_END_PHASE;
                    activitiesToCancel.add(activity);
                }
            }
        }

        databaseSrv.updateSObject(activitiesToCancel);
    }
}