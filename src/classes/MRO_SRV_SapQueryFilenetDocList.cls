/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.10.20.
 */

public with sharing class MRO_SRV_SapQueryFilenetDocList extends MRO_SRV_SapQueryHelper {
    private static final String requestTypeQueryFilenetDoc = 'QueryFilenet';
    private static MRO_SRV_SapQueryFilenetDoc filenetDocSrv = MRO_SRV_SapQueryFilenetDoc.getInstance();

    public static MRO_SRV_SapQueryFilenetDocList getInstance() {
        return new MRO_SRV_SapQueryFilenetDocList();
    }

    public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {
        Set<String> conditionFields = new Set<String>{
                'CompanyDivisionCode', 'ProcessCode', 'DocumentIssuedFrom', 'DocumentIssuedTo', 'RequestId', 'IntegrationId', 'CustomerCode', 'TaxNumber', 'DocumentType'
        };
        wrts_prcgvr.MRR_1_0.Request simpleRequest = buildSimpleRequest(argsMap, conditionFields);
        return simpleRequest;
    }

    public List<MRO_LC_FileNetDoc.FileNetDoc> getList(MRO_LC_FileNetDoc.RequestParam params) {
        Map<String, Object> conditions = params.toMap();
        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeQueryFilenetDoc, conditions, 'MRO_SRV_SapQueryFilenetDocList');
        List<MRO_LC_FileNetDoc.FileNetDoc> filenetDocList = filenetDocSrv.responseToDto(calloutResponse);

        filenetDocSrv.createFileMetadata(filenetDocList);

        return filenetDocList;

    }
}