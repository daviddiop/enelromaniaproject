/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   13/01/2020
* @desc
* @history
*/
global with sharing class MRO_TR_ExecutionLogHandler implements TriggerManager.ISObjectTriggerHandler  {

    global void afterDelete() {}

    global void afterInsert() {}

    global void afterUndelete() {}

    global void afterUpdate() {}

    global void beforeDelete() {}

    global void beforeInsert() {
        this.populateParentRecordLookups(Trigger.new);
    }

    global void beforeUpdate() {
        this.populateParentRecordLookups(Trigger.new);
    }

    private void populateParentRecordLookups(List<ExecutionLog__c> executionLogList) {
        List<ExecutionLog__c> caseGroupLogs = new List<ExecutionLog__c>();
        for (ExecutionLog__c executionLog : executionLogList) {
            if (Trigger.isUpdate && executionLog.RefRecordId__c == Trigger.oldMap.get(executionLog.Id).get('RefRecordId__c')) {
                continue;
            }
            if (String.isBlank(executionLog.RefRecordId__c) || !(executionLog.RefRecordId__c instanceof Id)) {
                executionLog.Case__c = null;
                executionLog.CaseGroup__c = null;
                continue;
            }
            Id parentRecordId = (Id) executionLog.RefRecordId__c;
            if (parentRecordId.getSobjectType() == Schema.Case.SObjectType) {
                executionLog.Case__c = parentRecordId;
                if (executionLog.Type__c == 'GroupCallOutItem') {
                    caseGroupLogs.add(executionLog);
                }
            }
            else if (parentRecordId.getSobjectType() == Schema.CaseGroup__c.SObjectType) {
                executionLog.CaseGroup__c = parentRecordId;
            }
            else if (parentRecordId.getSobjectType() == Schema.FileMetadata__c.SObjectType) {
                executionLog.FileMetadata__c = parentRecordId;
            }
            else if (parentRecordId.getSobjectType() == Schema.ValidatableDocument__c.SObjectType) {
                executionLog.ValidatableDocument__c = parentRecordId;
            }
            else if (parentRecordId.getSobjectType() == Schema.PrintAction__c.SObjectType) {
                executionLog.PrintAction__c = parentRecordId;
            }
            else {
                executionLog.Case__c = null;
                executionLog.CaseGroup__c = null;
                executionLog.FileMetadata__c = null;
                executionLog.ValidatableDocument__c = null;
                executionLog.PrintAction__c = null;
            }
        }
        if (!caseGroupLogs.isEmpty()) {
            MRO_SRV_CaseGroup.getInstance().addLogsToCaseGroups(caseGroupLogs);
        }
    }
}