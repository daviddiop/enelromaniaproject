/*** MRO_TR_City is a class used to concatenate
* City__c StateName__c with City__c Name
* and store this result in StateAndCity__c.
* This filed is used to duplicate rules and foreign key.
* @author Stefano Porcari
* @version 1.0
* @since 1.0
*/
public with sharing class MRO_TR_City implements TriggerManager.ISObjectTriggerHandler {
    public void beforeInsert() {
        for (City__c city : (List<City__c>) Trigger.new) {
            city.StateAndCity__c = city.Province__c + '&' + city.Name;
        }
    }
    public void beforeDelete() {
    }
    public void beforeUpdate() {
        for (City__c city : (List<City__c>) Trigger.new) {
            city.StateAndCity__c = city.Province__c + '&' + city.Name;
        }
    }
    public void afterInsert() {
    }
    public void afterUpdate() {
    }
    public void afterDelete() {
    }
    public void afterUndelete() {
    }
}