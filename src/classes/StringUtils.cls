public with sharing class StringUtils {

    public static String getEmptyIfNull(String inputString) {
        if (inputString == null) {
            return '';
        }
        return inputString;
    }

    public static Object tryToDeserialize(String jsonString, Type apexType) {
        try {
            return JSON.deserialize(jsonString, apexType);
        } catch (Exception exp) {
            return null;
        }
    }

    public static String incrementLastNumber(String inputString) {
        List<String> stringArray = inputString.split('');
        Integer foundIndex = null;
        Integer endIndex = stringArray.size() - 1;
        for (Integer i = endIndex; i > -1; i--) {
            if (stringArray[i].isNumeric() && foundIndex == null) {
                foundIndex = i;
            }
        }
        String outputString = '';
        for (Integer i = 0; i < stringArray.size(); i++) {
            String stringPart = stringArray[i];
            if (i == foundIndex) {
                Integer numberValue = Integer.valueOf(stringArray[i]);
                numberValue++;
                stringPart = String.valueOf(numberValue);
            }
            outputString += stringPart;
        }
        return outputString;
    }
}