public with sharing class PickJsonBuilder extends ApexServiceLibraryCnt {

    public with sharing class getJsonByObjectNameAndId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            if (params.get('objectJsonBuilder') != null && params.get('recordId') != null) {
                String jsonBuilderPath = params.get('objectJsonBuilder');
                IJsonBuilder builder = (IJsonBuilder) Type.forName(jsonBuilderPath).newInstance();
                return builder.getData(params.get('recordId'));
            } else {
                throw new WrtsException(System.Label.MissingParameter);
            }
        }
    }
}