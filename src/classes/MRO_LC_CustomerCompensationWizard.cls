/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 10.04.20.
 */

public with sharing class MRO_LC_CustomerCompensationWizard extends ApexServiceLibraryCnt{
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();

    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSr = MRO_SRV_Case.getInstance();

    private static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();

    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    private static final String transferBetweenContracts = 'Transfer Amount Between Customers';
    private static final String transferBetweenContractAccounts = 'Transfer Amount Between Contract Accounts';

    private static final List<Utils.KeyVal> requestTypeOptions {
        get {
            List<Utils.KeyVal> values = new List<Utils.KeyVal>();
            values.add(new Utils.KeyVal(transferBetweenContracts, transferBetweenContracts));
            values.add(new Utils.KeyVal(transferBetweenContractAccounts, transferBetweenContractAccounts));
            return values;
        }
    }


    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                response.put('error', true);
                response.put('errorMsg', System.Label.Account + ' - ' + System.Label.MissingId);
                return response;
            }

            Account acc = accQuery.findAccount(accountId);


            Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'CustomerCompensation');
            List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);

            if(!String.isBlank(genericRequestId)){
                dossierSr.updateParentGenericRequest(dossier.Id, genericRequestId);
            }

            Date endDate = Date.today();
            Date startDate = endDate.addYears(-1);
            response.put('startDate', startDate);
            response.put('endDate', endDate);

            if(cases.size() > 0){
                response.put('caseRecord', cases.get(0));
            }

            response.put('requestTypeOptions', requestTypeOptions);
            response.put('isClosed', dossier.Status__c != constantsSrv.DOSSIER_STATUS_DRAFT);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('error', false);

            return response;
        }
    }


    public with sharing class RequestParams {
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
        @AuraEnabled
        public String requestType { get; set; }
        @AuraEnabled
        public String selectedInvoices { get; set; }
        @AuraEnabled
        public String supplyFromId { get; set; }
        @AuraEnabled
        public String supplyToId { get; set; }
    }

    public with sharing class upsertCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams upsertCaseParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);
            Dossier__c dossier = dossierQuery.getById(upsertCaseParams.dossierId);
            Map<String, String> recordTypes = MRO_UTL_Constants.getCaseRecordTypes('CustomerCompensation');

            Case newCase = new Case(
                    Id = upsertCaseParams.caseId,
                    AccountId = dossier.Account__c,
                    AccountName__c = dossier.Account__r.Name,
                    RecordTypeId = (String) recordTypes.get('CustomerCompensation'),
                    Dossier__c = upsertCaseParams.dossierId,
                    Status = constantsSrv.CASE_STATUS_DRAFT,
                    Origin = upsertCaseParams.originSelected,
                    Channel__c = upsertCaseParams.channelSelected,
                    Phase__c = constantsSrv.CASE_START_PHASE,
                    SubProcess__c = upsertCaseParams.requestType,
                    Supply__c = upsertCaseParams.supplyFromId,
                    MarkedInvoices__c = upsertCaseParams.selectedInvoices
            );

            if(upsertCaseParams.supplyFromId != null){
                Supply__c supplyFrom = supplyQuery.getById(upsertCaseParams.supplyFromId);
                newCase.ContractAccount__c = supplyFrom.ContractAccount__c;
            }
            if(upsertCaseParams.supplyToId != null){
                Supply__c supplyTo = supplyQuery.getById(upsertCaseParams.supplyToId);
                newCase.ContractAccount2__c = supplyTo.ContractAccount__c;
            }

            databaseSrv.upsertSObject(newCase);

            List<Case> cases = caseQuery.getCasesByDossierId(upsertCaseParams.dossierId);

            response.put('caseRecord', cases[0]);
            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams channelOriginParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);

            dossierSr.updateDossierChannel(channelOriginParams.dossierId, channelOriginParams.channelSelected, channelOriginParams.originSelected);

            return response;
        }
    }

    public with sharing class SaveProcessParams {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String supplyId { get; set; }
        @AuraEnabled
        public Boolean isDraft { get; set; }
    }

    public with sharing class saveProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveProcessParams saveProcessParams = (SaveProcessParams) JSON.deserialize(jsonInput, SaveProcessParams.class);
            Map<String, Object> response = new Map<String, Object>();
            try {
                databaseSrv.upsertSObject(new Dossier__c(
                        Id = saveProcessParams.dossierId,
                        Status__c = saveProcessParams.isDraft ? constantsSrv.DOSSIER_STATUS_DRAFT : constantsSrv.DOSSIER_STATUS_NEW
                ));
                if (saveProcessParams.caseId != null && !saveProcessParams.isDraft) {
                    databaseSrv.upsertSObject(new Case(
                            Id = saveProcessParams.caseId,
                            Status = constantsSrv.CASE_STATUS_NEW,
                            CompanyDivision__c = supplyQuery.getById(saveProcessParams.supplyId).CompanyDivision__c,
                            EffectiveDate__c = Date.today()
                    ));
                }
                caseSr.applyAutomaticTransitionOnDossierAndCases(saveProcessParams.dossierId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}