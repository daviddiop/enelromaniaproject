/**
 * Created by Boubacar Sow on 01/07/2020.
 * @description [ENLCRO-1060] Enel.Ro - Test Classes development.
 */

@IsTest
public with sharing class MRO_LC_ConnectionWizardTst {
    @TestSetup
    static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'MeterCheckEle', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'MeterCheckEle', recordTypeEle, '');
        insert phaseDI010ToRC010;

        activityIntegrationTemplate__c activityIntegrationTemplateSetting = activityIntegrationTemplate__c.getOrgDefaults();
        activityIntegrationTemplateSetting.TemplateCode__c = '0e506e43-a9d8-4b65-abdf-d5d36e4e8a0c';
        insert activityIntegrationTemplateSetting;

        String templateCode = '0e506e43-a9d8-4b65-abdf-d5d36e4e8a0c';
        wrts_prcgvr.Interfaces_1_1.IActivityIntegration activityIntegration =
            (wrts_prcgvr.Interfaces_1_1.IActivityIntegration) wrts_prcgvr.VersionManager.newClassInstance('ActivityIntegration');

        /*ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'TechnicalDataChangeWizardCodeTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;*/


        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        //businessAccount.VATNumber__c= '1343324';
        businessAccount.Key__c = '1324433';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        /*wrts_prcgvr__Activity__c[] activities = (wrts_prcgvr__Activity__c[]) activityIntegration.createFromTemplate(
            templateCode, new Map<String, Object>{
                'objectId' => servicePoint.Id
            });

        insert activities;*/

        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Connection_ELE').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    public static void initializeTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id
        };
        response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );

        Test.stopTest();
    }

    @IsTest
    public static void saveDraftBPTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'billingProfileId' => billingProfile.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'saveDraftBP', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'billingProfileId' => billingProfile.Id+'ddls44'
        };
        response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'saveDraftBP', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );

        Test.stopTest();
    }

    @IsTest
    public static void updateDossierConnectionTypeAndChannelOriginTest() {
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Internal',
            'connectionType' => ''
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'updateDossierConnectionTypeAndChannelOrigin', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @IsTest
    public static void updateDossierCompanyDivisionTest() {
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'companyDivisionId' => companyDivision.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'updateDossierCompanyDivision', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @IsTest
    public static void checkCasesTest() {
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'checkCases', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('result') == 'OK' );

        Test.stopTest();
    }

    @IsTest
    public static void deleteCloneCaseTest() {
        Case caseRecord = [
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'caseId' => caseRecord.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'deleteCloneCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );


        inputJSON = new Map<String, String>{
            'caseId' => caseRecord.Id+'4sdks'
        };
        response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'deleteCloneCase', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );

        Test.stopTest();
    }

    @IsTest
    public static void updateChannelAndOriginTest() {
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Internal'
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'updateChannelAndOrigin', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @IsTest
    public static void updateCaseListTest() {
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
            LIMIT 1
        ];

        Contact contact = [
            SELECT Id
            FROM Contact
            LIMIT 1
        ];
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'accountId' => account.Id,
            'oldCaseList' => caseListString,
            'billingProfileId' => billingProfile.Id,
            'connectionType' => '',
            'contactId' => contact.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'updateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.debug('###errorMsg '+result.get('errorMsg'));
        System.debug('###errorTrace '+result.get('errorTrace'));
        System.assertEquals(true, result.get('error') == true );

        inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'oldCaseList' => caseListString,
            'billingProfileId' => billingProfile.Id,
            'contactId' => contact.Id+'kdk744'
        };
        response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'updateCaseList', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );

        Test.stopTest();
    }

    @IsTest
    public static void cancelProcessTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'oldCaseList' => caseListString
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'cancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );


        inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id+'445s',
            'oldCaseList' => caseListString
        };
        response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'cancelProcess', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );

        Test.stopTest();
    }

    @IsTest
    public static void getAddressTest() {
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ConnectionWizard', 'getAddress', null, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

}