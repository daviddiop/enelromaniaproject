/**
 * Created by tomasoni on 07/08/2019.
 */

public with sharing class MRO_QR_ContractAccount {

    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();

    public static MRO_QR_ContractAccount getInstance() {
        return new MRO_QR_ContractAccount();
    }

    public List<ContractAccount__c> listByBillingAccountNumber(String billingAccountNumber) {
        return [
            SELECT Id, Account__c
            FROM ContractAccount__c
            WHERE BillingAccountNumber__c = :billingAccountNumber
        ];
    }

    /**
     * @author Boubacar Sow
     * @date 30/10/2020
     * @description [ENLCRO-1672] Company Division must appear on CASE - Repayment Schedule
     * @param billingAccountNumber
     *
     * @return ContractAccount__c
     */
    public ContractAccount__c getByBillingAccountNumber(String billingAccountNumber) {
            List<ContractAccount__c> contractAccounts = [
            SELECT Id, CompanyDivision__c
            FROM ContractAccount__c
            WHERE BillingAccountNumber__c = :billingAccountNumber
        ];
        if (contractAccounts.isEmpty()) {
            return null;
        }
        return contractAccounts.get(0);
    }

    public ContractAccount__c getById(String contractAccountId) {
        return [
                SELECT Id, Name, BillingFrequency__c, PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c, SelfReadingPeriodEnd__c,
                       DirectDebitActivationDate__c, DirectDebitTerminDate__c,
                       BillingAccountNumber__c,
                        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                       StartDate__c, EndDate__c, Key__c,ActiveSuppliesCount__c, Market__c, Commodity__c, ContractType__c,
                        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                       BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c,
                       BillingProfile__r.Fax__c, BillingProfile__r.Name,BillingProfile__r.MobilePhone__c,
                       BillingProfile__r.Email__c, BillingProfile__r.PaymentMethod__c,BillingProfile__r.RefundMethod__c,
                       BillingProfile__r.BillingStreetType__c, BillingProfile__r.BillingStreetName__c,
                       BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c,
                       BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c,
	                   BillingProfile__r.BillingAddressKey__c, BillingProfile__r.BillingStreetId__c,
                       BillingProfile__r.RefundIBAN__c,BillingProfile__r.RefundBank__c, BillingProfile__r.BillingAddress__c,
                       CompanyDivision__c, CompanyDivision__r.Code__c, Account__c,
                        Account__r.AccountNumber, IntegrationKey__c
                FROM ContractAccount__c
                WHERE Id = :contractAccountId
        ];
    }

    public List<ContractAccount__c> getByAccountId(String accountId) {
        return [
                SELECT Id, Name, BillingFrequency__c, PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c,
                       StartDate__c, EndDate__c, Key__c,ActiveSuppliesCount__c, Market__c, Commodity__c,
                        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                       BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c,ContractType__c,
                        //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                       BillingProfile__r.Fax__c, BillingProfile__r.Name,
                       BillingProfile__r.Email__c, BillingProfile__r.PaymentMethod__c,
                       BillingProfile__r.BillingStreetType__c, BillingProfile__r.BillingStreetName__c,
                       BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c,
                       BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c,
	                   BillingProfile__r.BillingAddressKey__c, BillingProfile__r.BillingStreetId__c, BillingProfile__r.BillingAddress__c
                FROM ContractAccount__c
                WHERE Account__c = :accountId
                ORDER BY Name
        ];
    }
    
    /**
     * @author Boubacar Sow
     * @Created 25/11/2019
     * @description: retreive ContractAccount by Account and CompanyDivision dynamicaly request
     *               [ENLCRO-196] Integrate ContractAccountSelection in the DemostratedPaymentWizard
     * @param accountId
     * @param companyDivisionId
     */
    public List<ContractAccount__c> getByAccountAndCompanyDivisionId(String accountId,String companyDivisionId) {
        String query = 'SELECT Id, Name, CompanyDivision__c, CompanyDivision__r.Name, BillingAccountNumber__c, BillingFrequency__c, PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c,'+
            'StartDate__c, EndDate__c, Key__c, BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c, BillingProfile__r.Fax__c, '+
            'BillingProfile__r.Name, BillingProfile__r.Email__c, BillingProfile__r.PaymentMethod__c, BillingProfile__r.BillingStreetType__c, '+
            'BillingProfile__r.BillingStreetName__c, BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c, '+
            'BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c, BillingProfile__r.RefundMethod__c, BillingProfile__r.RefundIBAN__c, IntegrationKey__c FROM ContractAccount__c';

        List<String> conditions = new List<String>();
        List<ContractAccount__c> contractAccounts;

        if (String.isNotBlank(accountId)) {
            conditions.add('Account__c = \'' + accountId + '\'');
        }
    
        if (String.isNotBlank(companyDivisionId)) {
            conditions.add('CompanyDivision__c = \'' + companyDivisionId + '\'');
        }
        System.debug('###conditions '+conditions);
        if (!conditions.isEmpty()) {
            query +=' WHERE ' + String.join(conditions, ' AND ') + ' ORDER BY Name ';
        }
        
        System.debug('###Query '+query);
        contractAccounts = Database.query(query);
        return contractAccounts;
    }

    public List<ContractAccount__c> getByAccountAndCompanyDivisionId(String accountId, String companyDivisionId, String dataType, String field,
            String operator, String val, String commodity, String contractType, List<Id> supplyIds, String market) {
        return (List<ContractAccount__c>) getByAccountAndCompanyDivisionId(accountId, companyDivisionId, dataType, field,
                operator, val, commodity, contractType, null, supplyIds, market, null, null).get('contractAccounts');
    }
//FF  [ENLCRO-669] Contract Account Selection - Check Interface
    public Map<String, Object> getByAccountAndCompanyDivisionId(String accountId, String companyDivisionId, String dataType, String field,
            String operator, String val, String commodity, String contractType, String contractId, List<Id> supplyIds, String market, Set<String> contractAccountIdSet, Set<String> excludeContractAccountIds) {
        String query = 'SELECT Id, Name, CompanyDivision__c, CompanyDivision__r.Name, BillingAccountNumber__c, BillingFrequency__c, ' +
            'PaymentTerms__c, LatePaymentPenalty__c, ActiveSuppliesCount__c, Market__c, Commodity__c, BillingProfile__c, DirectDebitAuth__c, ContractType__c, '+
            'StartDate__c, EndDate__c, Key__c, BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c, BillingProfile__r.Fax__c, '+
            'BillingProfile__r.Name, BillingProfile__r.Email__c, BillingProfile__r.PaymentMethod__c, BillingProfile__r.BillingStreetType__c, '+
            'BillingProfile__r.BillingStreetName__c, BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c, '+
            'BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c, BillingProfile__r.RefundMethod__c, BillingProfile__r.RefundIBAN__c, IntegrationKey__c FROM ContractAccount__c';

        List<String> stringOperator = new List<String>{'=','!=','LIKE','IN','NOT IN','like','in','not in'};
        List<String> numberOperator = new List<String>{'=','!=','<','<=','>','>='};
        List<String> booleanOperator = new List<String>{'=','!='};
        List<String> conditions = new List<String>();
        List<ContractAccount__c> contractAccounts;
        Boolean contractAccountHasField = ContractAccount__c.getSobjectType().getDescribe().fields.getMap().keySet().contains(field.toLowerCase());
        String latePaymentPenalty, paymentTerms;

        Map<String, Object> result = new Map<String, Object>();

        if (String.isNotBlank(contractId) && contractId != 'new') {
            Contract contract = MRO_QR_Contract.getInstance().getWithActiveSuppliesByIds(new Set<Id>{contractId})[0];
            if (contract.AccountId != null) {
                accountId = contract.AccountId;
            }
            if (contract.CompanyDivision__c != null) {
                companyDivisionId = contract.CompanyDivision__c;
            }
            if (contract.ContractType__c != null) {
                contractType = contract.ContractType__c;
            }
            for (Supply__c supply : contract.Supplies__r) {
                if (supply.RecordType.DeveloperName != 'Service' && supply.ContractAccount__c != null) {
                    latePaymentPenalty = supply.ContractAccount__r.LatePaymentPenalty__c;
                    result.put('latePaymentPenalty', latePaymentPenalty);
                    paymentTerms = supply.ContractAccount__r.PaymentTerms__c;
                    result.put('paymentTerms', paymentTerms);
                }
            }
        }
        if (String.isNotBlank(accountId)) {
            conditions.add('Account__c = \'' + accountId + '\'');
        }

        if (String.isNotBlank(commodity)) {
            conditions.add('Commodity__c = \'' + commodity + '\'');
        }
        if (String.isNotBlank(market)) {
            conditions.add('Market__c = \'' + market + '\'');
        }

        if (String.isNotBlank(companyDivisionId)) {
            conditions.add('CompanyDivision__c = \'' + companyDivisionId + '\'');
        }
            //FF  [ENLCRO-669] Contract Account Selection - Check Interface
        if (String.isNotBlank(contractType)) {
            conditions.add('ContractType__c = \'' + contractType + '\'');
        }

        if (String.isNotBlank(latePaymentPenalty)) {
            conditions.add('LatePaymentPenalty__c = \'' + latePaymentPenalty + '\'');
        }

        if (String.isNotBlank(paymentTerms)) {
            conditions.add('PaymentTerms__c = \'' + paymentTerms + '\'');
        }

        if(contractAccountHasField || field == 'BillingProfile__r.RefundMethod__c'){
            if(String.isNotBlank(val) && String.isNotBlank(dataType) && String.isNotBlank(operator) && String.isNotBlank(field)){
                if((dataType == 'string') && (stringOperator.contains(operator))){
                    conditions.add(field + ' ' + operator + ' ' + '\'' + val + '\'');
                }else if ((dataType == 'number') && (numberOperator.contains(operator))){
                    conditions.add(field + ' ' + operator + ' '  + val);
                }else if ((dataType == 'boolean') && (booleanOperator.contains('\'' + operator + '\'')) ){
                    conditions.add(field + ' ' + operator + ' '  + val);
                }
            }
        }

        if (supplyIds != null && !supplyIds.isEmpty()) {
            List<Supply__c> supplyList = supplyQuery.getSuppliesByIds(supplyIds);
            if (supplyList.size() > 0) {
                Set<String> contractAccountIdsFromSupply = SobjectUtils.setNotNullByField(supplyList, Supply__c.ContractAccount__c);
                if (contractAccountIdSet == null) {
                    contractAccountIdSet = contractAccountIdsFromSupply;
                } else {
                    contractAccountIdSet.addAll(contractAccountIdsFromSupply);
                }
            }
        }
        if (contractAccountIdSet != null && !contractAccountIdSet.isEmpty()) {
            List<String> contractAccountIds = new List<String>();
            for(String currentContractAccountId: contractAccountIdSet){
                contractAccountIds.add('\''+currentContractAccountId+'\'');
            }
            System.debug('##### contractAccountIds '+contractAccountIds);

            conditions.add('Id IN ' + contractAccountIds);
        }
        if (excludeContractAccountIds != null && !excludeContractAccountIds.isEmpty()) {
            List<String> contractAccountIds = new List<String>();
            for (String currentContractAccountId: excludeContractAccountIds){
                contractAccountIds.add('\''+currentContractAccountId+'\'');
            }
            conditions.add('Id NOT IN ' + contractAccountIds);
        }
        if (!conditions.isEmpty()) {
            query +=' WHERE ' + String.join(conditions, ' AND ') + ' ORDER BY Name ';
            System.debug('###Queryconditions: '+ conditions);
        }

        System.debug('###Query: '+ query);
        contractAccounts = Database.query(query);
        result.put('contractAccounts', contractAccounts);
        return result;
    }
//FF  [ENLCRO-669] Contract Account Selection - Check Interface
    /**
     * @author Boubacar Sow
     * @Created 12/12/2019
     * @description: retreive the map of Id and ContractAccount by the contractAccount Ids.
     *               [ENLCRO-317] Update Contract Account's Active Supplies Count on request commit
     * @param contractAccountIds
     */
    public Map<Id, ContractAccount__c> getByIds(Set<Id> contractAccountIds) {
        return new Map<Id, ContractAccount__c>([
                SELECT Id, CompanyDivision__c, ActiveSuppliesCount__c,BillingFrequency__c,SelfReadingPeriodEnd__c, Market__c,
                    Name, Account__c,  PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c,
                    StartDate__c, EndDate__c, Key__c,  Commodity__c, ContractType__c,PreviousPaymentTerms__c,
                    BillingAccountNumber__c,LatePaymentPenalty__c,SAPStatus__c

                FROM ContractAccount__c
                WHERE Id IN :contractAccountIds
        ]);
    }

    /**
  * @author Giuseppe Mario Pastore
  * @Created 08/07/2020
  * @description: retreive a list of AccountId and return a Map<Id, ContractAccount__c> with all related values
  *               [ENLCRO-317] Update Contract Account's Active Supplies Count on request commit
  * @param contractAccountIds
  */
    public Map<Id, ContractAccount__c> getContractAccountsByAccountIds(Set<Id> accountIds) {
        return new Map<Id, ContractAccount__c>([
            SELECT Id, Name, Account__c, BillingFrequency__c, PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c,
                StartDate__c, EndDate__c, Key__c,ActiveSuppliesCount__c, Market__c, Commodity__c,
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c,ContractType__c,
                //FF  [ENLCRO-669] Contract Account Selection - Check Interface
                BillingProfile__r.Fax__c, BillingProfile__r.Name,
                BillingProfile__r.Email__c, BillingProfile__r.PaymentMethod__c,
                BillingProfile__r.BillingStreetType__c, BillingProfile__r.BillingStreetName__c,
                BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c,
                BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c,
                BillingProfile__r.BillingAddressKey__c, BillingProfile__r.BillingStreetId__c,PreviousPaymentTerms__c
            FROM ContractAccount__c
            WHERE Account__c IN :accountIds
        ]);
    }


}