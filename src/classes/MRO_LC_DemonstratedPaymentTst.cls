/**
 * Created by Boubacar Sow on 11/11/2019.
 * @version 1.0
 * @description
 *          [ENLCRO-195] Create MRO_LCP_DemonstratedPaymentWizard Aura Component
 *          [ENLCRO-196] Integrate ContractAccountSelection in the DemostratedPaymentWizard
 *          [ENLCRO-197] Integrate Invoices Inquiry Component in the DemonstratedPaymentWizard
 *
 */
@IsTest
public with sharing class MRO_LC_DemonstratedPaymentTst {
    @TestSetup
    static void setup() {

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('DemonstratedPayment').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'DemonstratedPayment', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        /*BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        
        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().setAccount(listAccount[0].Id).build();
        insert contractAccount;
        
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;
        
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            //caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('DemonstratedPayment').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'DemonstatedPayement';
        insert scriptTemplate;
    }
    @IsTest
    public static void InitializeDemonstratedPaymentTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
    
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_DemonstratedPayment', 'InitializeDemonstratedPayment', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false, 'Error is found' );
    
        inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => '',
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        response = TestUtils.exec(
            'MRO_LC_DemonstratedPayment', 'InitializeDemonstratedPayment', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false, 'Error is found');
    
        Test.stopTest();
    }
    
    
    @IsTest
    static void InitializeDemonstratedPaymentExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                'MRO_LC_DemonstratedPayment', 'InitializeDemonstratedPayment', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true, 'Error is found');
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    
    @IsTest
    public static void CreateCaseTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];
        ContractAccount__c contractAccount  = [
            SELECT Id,Name,BillingAccountNumber__c
            FROM ContractAccount__c
            LIMIT 1
        ];
        MRO_LC_DemonstratedPayment.PaymentInformationInput inputPaymentInformation = new MRO_LC_DemonstratedPayment.PaymentInformationInput();
        inputPaymentInformation.amount = 1000000;
        inputPaymentInformation.effectiveDate = System.today();
        String inputPaymentInformationString = JSON.serialize(inputPaymentInformation);
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'contractAccountId' => contractAccount.Id,
            'companyDivisionId' => companyDivision.Id,
            'invoiceIds' => invoiceIds,
            'paymentInformation' => inputPaymentInformationString
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_DemonstratedPayment', 'CreateCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false,'Error is found');
        
        Test.stopTest();
    }
    
    @IsTest
    public static void CreateCaseExceptionTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];
        ContractAccount__c contractAccount  = [
            SELECT Id,Name,BillingAccountNumber__c
            FROM ContractAccount__c
            LIMIT 1
        ];
        MRO_LC_DemonstratedPayment.PaymentInformationInput inputPaymentInformation = new MRO_LC_DemonstratedPayment.PaymentInformationInput();
        inputPaymentInformation.amount = 1000000;
        inputPaymentInformation.effectiveDate = System.today();
        String inputPaymentInformationString = JSON.serialize(inputPaymentInformation);
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id+'FLS48522',
            'dossierId' => dossier.Id,
            'contractAccountId' => contractAccount.Id,
            'companyDivisionId' => companyDivision.Id,
            'invoiceIds' => invoiceIds,
            'paymentInformation' => inputPaymentInformationString
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_DemonstratedPayment', 'CreateCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true,'Error is found');
        Test.stopTest();
    }
    
    
    @IsTest
    private static void CancelProcess() {
        
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };
        
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_DemonstratedPayment', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false,'Error is found');
        Test.stopTest();
    }

}