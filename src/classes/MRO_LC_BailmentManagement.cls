/**
 * Created by Vlad Mocanu on 19/02/2020.
 */

public with sharing class MRO_LC_BailmentManagement extends ApexServiceLibraryCnt {
    private static final String BAILMENT_MANAGEMENT = 'BailmentManagement';

    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_UTL_Constants constants = MRO_UTL_Constants.getAllConstants();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();


    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String accountId = params.get('accountId');
            String parentCaseId = params.get('parentCaseId');
            String genericRequestDossierId = params.get('genericRequestDossierId');
            String interactionId = params.get('interactionId');
            CustomerInteraction__c customerInteraction;
            List<CustomerInteraction__c> interactionList = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);
            if (interactionList != null && !interactionList.isEmpty()) {
                customerInteraction = interactionList[0];
            }
            Map<String, Object> response = new Map<String, Object>();

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                Dossier__c dossier;
                if (String.isBlank(dossierId)) {
                    dossier = new Dossier__c ();
                    dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
                    dossier.RequestType__c = BAILMENT_MANAGEMENT;
                    dossier.Status__c = 'Draft';
                    //dossier.Phase__c = 'Request Entry';
                    dossier.Account__c = accountId;

                    if (parentCaseId != null) {
                        response.put('parentCaseId', parentCaseId);
                        Case parentCase = caseQuery.getById(parentCaseId);
                        Id parentCaseSupplyId = parentCase.Supply__c;
                        Dossier__c parentCaseDossier = dossierQuery.getById(parentCase.Dossier__c);
                        dossier.Origin__c = parentCaseDossier.Origin__c;
                        dossier.Channel__c = parentCaseDossier.Channel__c;
                        response.put('parentCaseSupplyId', parentCaseSupplyId);
                    }

                    if (customerInteraction != null) {
                        dossier.CustomerInteraction__c = customerInteraction.Id;
                        dossier.Origin__c = customerInteraction.Interaction__r.Origin__c;
                        dossier.Channel__c = customerInteraction.Interaction__r.Channel__c;
                    }

                    if (genericRequestDossierId != null) {
                        response.put('parentDossierId', genericRequestDossierId);
                        Dossier__c parentDossier = dossierQuery.getById(genericRequestDossierId);
                        dossier.Origin__c = parentDossier.Origin__c;
                        dossier.Channel__c = parentDossier.Channel__c;
                        dossier.SendingChannel__c = parentDossier.SendingChannel__c;
                        dossier.Account__c = parentDossier.Account__c;
                        dossier.AssignmentProvince__c = parentDossier.AssignmentProvince__c;
                        dossier.Phone__c = parentDossier.Phone__c;
                        dossier.Email__c = parentDossier.Email__c;
                        dossier.SLAExpirationDate__c = parentDossier.SLAExpirationDate__c;
                        dossier.Parent__c = parentDossier.Id;
                    }

                    Date slaExpirationDate = addBusinessDays(Date.today(), 5);
                    dossier.SLAExpirationDate__c = slaExpirationDate;
                    databaseSrv.insertSObject(dossier);
                    dossierId = dossier.Id;
                    response.put('dossier', dossier);
                } else {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
                    Dossier__c existingDossier = dossierQuery.getById(dossierId);
                    response.put('caseTile', cases);
                    response.put('dossier', existingDossier);
                }
                response.put('dossierId', dossierId);
                response.put('accountId', accountId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class getContractIdBySupplyId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            Map<String, Object> response = new Map<String, Object>();
            Set<Id> contractIdSet = new Set<Id>();

            try {
                if (String.isBlank(supplyId)) {
                    throw new WrtsException(System.Label.Supply + ' - ' + System.Label.MissingId);
                }
                String contractId = supplyQuery.getById(supplyId).Contract__c;
                contractIdSet.add(contractId);
                String accountId = supplyQuery.getById(supplyId).Account__c;
                String contractAccountId = supplyQuery.getById(supplyId).ContractAccount__c;
                Contract contract = contractQuery.getByIds(contractIdSet).get(contractId);
                String accountIntegrationKey = accountQuery.findAccount(accountId).IntegrationKey__c;

                response.put('contractId', contract.Id);
                response.put('contractNumber', contract.ContractNumber);
                response.put('contractIntegrationKey', contract.IntegrationKey__c);
                response.put('accountIntegrationKey', accountIntegrationKey);
                response.put('contractAccountId', contractAccountId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String cancelReason = params.get('cancelReason');
            String detailsReason = params.get('detailsReason');

            List<Case> caseList = caseQuery.getCasesByDossierId(dossierId);
            for (Case caseRecord : caseList) {
                caseRecord.CancellationReason__c = cancelReason;
                caseRecord.CancellationDetails__c = detailsReason;
            }

            Savepoint sp = Database.setSavepoint();
            try {
                caseSrv.setCanceledOnCaseAndDossier(caseList, dossierId);

                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return new Map<String, Object>();
        }
    }

    public class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CreateCaseInput createCaseParams = (CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);
            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> bailmentManagementRecordTypes = MRO_UTL_Constants.getCaseRecordTypes(BAILMENT_MANAGEMENT);
            List<Case> newCases = new List<Case>();
            Date slaExpirationDate = dossierQuery.getById(createCaseParams.dossierId).SLAExpirationDate__c;
            String bailmentManagementRecordTypeId;

            response.put('error', false);
            try {
                if (String.isNotBlank(createCaseParams.dossierId) && String.isNotBlank(createCaseParams.contractId) && String.isNotBlank(createCaseParams.selectedOrigin)
                        && String.isNotBlank(createCaseParams.selectedChannel) && String.isNotBlank(createCaseParams.supplyId) && String.isNotBlank(createCaseParams.accountId)) {
                    bailmentManagementRecordTypeId = bailmentManagementRecordTypes.get(BAILMENT_MANAGEMENT);

                    Case bailmentManagementDraftCase = new Case(
                            RecordTypeId = bailmentManagementRecordTypeId,
                            Dossier__c = createCaseParams.dossierId,
                            Contract__c = createCaseParams.contractId,
                            Origin = createCaseParams.selectedOrigin,
                            Channel__c = createCaseParams.selectedChannel,
                            Supply__c = createCaseParams.supplyId,
                            AccountId = createCaseParams.accountId,
                            //SLAExpirationDate__c = slaExpirationDate,
                            Phase__c = constants.CASE_START_PHASE,
                            Status = 'Draft');

                    if(createCaseParams.parentCaseId != null){
                        bailmentManagementDraftCase.ParentId = createCaseParams.parentCaseId;
                    }

                    Database.DMLOptions dmo = new Database.DMLOptions();
                    dmo.assignmentRuleHeader.useDefaultRule = true;
                    bailmentManagementDraftCase.setOptions(dmo);
                    newCases.add(bailmentManagementDraftCase);
                    if (!newCases.isEmpty()) {
                        databaseSrv.insertSObject(newCases);
                    }
                    response.put('caseList', newCases);
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class saveDraftCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveCaseInput saveDraftCaseParams = (SaveCaseInput) JSON.deserialize(jsonInput, SaveCaseInput.class);
            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            List<Case> newCases = new List<Case>();

            response.put('error', false);

            try {
                Case bailmentCase = new Case(
                        Dossier__c = saveDraftCaseParams.dossierId,
                        Status = 'Draft',
                        SelectedWarranties__c = saveDraftCaseParams.listOfWarranties,
                        Amount__c = saveDraftCaseParams.totalWarrantyValue,
                        Reason__c = saveDraftCaseParams.withdrawReason);

                if (String.isNotBlank(saveDraftCaseParams.caseId)) {
                    bailmentCase.Id = saveDraftCaseParams.caseId;
                }

                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule = true;
                bailmentCase.setOptions(dmo);
                newCases.add(bailmentCase);
                if (!newCases.isEmpty()) {
                    System.debug('newCases: ' + newCases);
                    databaseSrv.upsertSObject(newCases);
                }
                Dossier__c dossier = new Dossier__c (
                        Id = saveDraftCaseParams.dossierId,
                        Status__c = 'Draft',
                        Channel__c = saveDraftCaseParams.selectedChannel,
                        Origin__c = saveDraftCaseParams.selectedOrigin
                );
                if (dossier != null) {
                    databaseSrv.upsertSObject(dossier);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public Map<String, Object> cancelEverything(String jsonInput) {
        Map<String, String> params = ApexServiceLibraryCnt.asMap(jsonInput);
        Map<String, Object> response = new Map<String, Object>();
        Id dossierId = Id.valueOf(params.get('dossierId'));
        String cancelReason = String.valueOf(params.get('cancelReason'));
        String detailsReason = String.valueOf(params.get('detailsReason'));
        Savepoint sp = Database.setSavepoint();
        response.put('error', false);
        try {
            cancelDossier(dossierId, cancelReason, detailsReason);
        } catch (Exception ex) {
            Database.rollback(sp);
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }

    private Boolean cancelDossier(Id dossierId, String cancelReason, String detailsReason) {
        if (dossierId == null){
            return false;
        }
        Savepoint sp = Database.setSavepoint();
        try {
            databaseSrv.upsertSObject(new Dossier__c(
                    Id = dossierId,
                    Status__c = constants.DOSSIER_STATUS_CANCELED,
                    CancellationReason__c = cancelReason,
                    CancellationDetails__c = detailsReason
            ));
            return true;
        } catch (Exception ex) {
            Database.rollback(sp);
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            return false;
        }
    }

    public class saveCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveCaseInput saveCaseParams = (SaveCaseInput) JSON.deserialize(jsonInput, SaveCaseInput.class);
            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            List<Case> newCases = new List<Case>();

            response.put('error', false);
            try {
                if (String.isNotBlank(saveCaseParams.dossierId) && String.isNotBlank(saveCaseParams.listOfWarranties) &&
                        String.isNotBlank(saveCaseParams.caseId) && String.isNotBlank(saveCaseParams.withdrawReason)) {

                    Case BailmentManagementNewCase = new Case(
                            Dossier__c = saveCaseParams.dossierId,
                            Id = saveCaseParams.caseId,
                            Status = 'New',
                            Phase__c = 'RE010',
                            SelectedWarranties__c = saveCaseParams.listOfWarranties,
                            Amount__c = saveCaseParams.totalWarrantyValue,
                            StartDate__c = Date.today(),
                            EffectiveDate__c = Date.today(),
                            Reason__c = saveCaseParams.withdrawReason);

                    Database.DMLOptions dmo = new Database.DMLOptions();
                    dmo.assignmentRuleHeader.useDefaultRule = true;
                    BailmentManagementNewCase.setOptions(dmo);
                    newCases.add(BailmentManagementNewCase);
                    if (!newCases.isEmpty()) {
                        databaseSrv.upsertSObject(newCases);
                    }
                    Dossier__c dossier = new Dossier__c (
                            Id = saveCaseParams.dossierId,
                            Status__c = 'New',
                            Channel__c = saveCaseParams.selectedChannel,
                            Origin__c = saveCaseParams.selectedOrigin
                    );
                    if (dossier != null) {
                        databaseSrv.upsertSObject(dossier);
                        caseSrv.applyAutomaticTransitionOnDossierAndCases(saveCaseParams.dossierId);
                    }
                    response.put('dossier', dossier);
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class SaveCaseInput {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String listOfWarranties { get; set; }
        @AuraEnabled
        public String withdrawReason { get; set; }
        @AuraEnabled
        public String selectedOrigin { get; set; }
        @AuraEnabled
        public String selectedChannel { get; set; }
        @AuraEnabled
        public Double totalWarrantyValue { get; set; }
    }

    public class CreateCaseInput {
        @AuraEnabled
        public String parentCaseId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String contractId { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String supplyId { get; set; }
        @AuraEnabled
        public String selectedOrigin { get; set; }
        @AuraEnabled
        public String selectedChannel { get; set; }
    }

    public static Boolean isWeekendDay(Date dateParam) {
        Boolean result = false;
        Date startOfWeek = dateParam.toStartOfWeek();
        Integer dayOfWeek = dateParam.day() - startOfWeek.day();
        result = dayOfWeek == 0 || dayOfWeek == 6 ? true : false;

        return result;
    }

    public static Date addBusinessDays(Date startDate, Integer businessDaysToAdd) {
        Date finalDate = startDate;
        Integer direction = businessDaysToAdd < 0 ? -1 : 1;

        while (businessDaysToAdd != 0) {
            finalDate = finalDate.addDays(direction);
            if (!isWeekendDay(finalDate)) {
                businessDaysToAdd -= direction;
            }
        }
        return finalDate;
    }

}