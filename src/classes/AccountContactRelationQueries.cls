public with sharing class AccountContactRelationQueries {

    public static AccountContactRelationQueries getInstance() {
        return (AccountContactRelationQueries)ServiceLocator.getInstance(AccountContactRelationQueries.class);
    }
// ST Unused
//    public List<AccountContactRelation> getAccountContactRelation(String accountId, String contactId) {
//        return [
//            SELECT Id, AccountId, ContactId
//            FROM AccountContactRelation
//            WHERE AccountId = :accountId
//            OR ContactId = :contactId
//        ];
//    }

    public List<AccountContactRelation> listByIndividualId(String individualId) {
        return [
            SELECT Id, AccountId, ContactId, Roles, IsDirect,
                Account.Name, Account.RecordType.Name, Account.BusinessType__c, Account.VATNumber__c, Account.NationalIdentityNumber__pc, Account.IsPersonAccount,
                Contact.Name, Contact.IndividualId, Contact.Phone, Contact.MobilePhone, Contact.Email, Contact.FirstName, Contact.LastName
            FROM AccountContactRelation
            WHERE Contact.IndividualId =: individualId
        ];
    }

    public List<AccountContactRelation> listByAccountId(Id accountId) {
        return [
            SELECT Id, AccountId, ContactId, Roles, IsDirect,
                Account.Name, Account.RecordType.Name, Account.BusinessType__c, Account.VATNumber__c, Account.NationalIdentityNumber__pc, Account.IsPersonAccount,
                Contact.Name, Contact.IndividualId, Contact.Phone, Contact.MobilePhone, Contact.Email, Contact.FirstName, Contact.LastName
            FROM AccountContactRelation
            WHERE AccountId = :accountId
                AND ContactId != null
                AND Contact.IndividualId != null
        ];
    }

    public List<AccountContactRelation> listByContactIdsAndAccountIds(Set<String> contactIds, Set<String> accountIds) {
        return [
            SELECT Id, AccountId, ContactId, Roles
            FROM AccountContactRelation
            WHERE ContactId IN :contactIds
                AND AccountId IN :accountIds
        ];
    }
    public List<AccountContactRelation> listByContactIdsORAccountIds(Set<String> contactIds, Set<String> accountIds) {
        return [
                SELECT Id, AccountId, ContactId, Roles
                FROM AccountContactRelation
                WHERE ContactId IN :contactIds
                OR AccountId IN :accountIds
        ];
    }

    public List<AccountContactRelation> listByAccountIds( Set<String> accountIds) {
        return [
            SELECT Id, AccountId, Account.IsPersonAccount, Account.PersonIndividualId, ContactId, Roles
            FROM AccountContactRelation
            WHERE AccountId IN :accountIds
        ];
    }

// ST Unused
//    public List<AccountContactRelation> listByContactIdsAndAccountId(Set<String> contactIds, String accountId) {
//        return [
//            SELECT Id, AccountId, ContactId, Roles
//            FROM AccountContactRelation
//            WHERE ContactId IN :contactIds
//                AND AccountId = :accountId
//                AND ContactId != null
//        ];
//    }

    public AccountContactRelation findByAccountIdAndContactId(String accountId, String contactId) {
        List<AccountContactRelation> relation = [
            SELECT Id, AccountId, ContactId
            FROM AccountContactRelation
            WHERE AccountId = :accountId
            AND ContactId = :contactId
        ];
        if (relation.isEmpty()) {
            return null;
        }
        return relation.get(0);
    }
}