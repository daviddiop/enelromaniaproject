/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 08.05.20.
 */

public with sharing class MRO_QR_FileTemplateColumn {

    public static MRO_QR_FileTemplateColumn getInstance() {
        return new MRO_QR_FileTemplateColumn();
    }

    public List<FileTemplateColumn__c> getByFileTemplateIdOrderASC(String ftId){
        return [SELECT Id, ValidationAction__c, HeaderName__c, ValidationRegex__c, Mandatory__c, Index__c, FileTemplate__r.FileDelimiter__c , FileTemplate__r.FileTemplateName__c, SampleData__c FROM FileTemplateColumn__c WHERE FileTemplate__c = :ftId ORDER BY Index__c ASC];
    }

    public List<FileTemplateColumn__c> getByFileTemplateId(String ftId){
        return [SELECT Id, HeaderName__c FROM FileTemplateColumn__c WHERE FileTemplate__c = :ftId];
    }
}