/**
 * Created by David Diop on 22.07.2020.
 */
@IsTest
public with sharing class MRO_WS_GenesysServicesTst {
    @TestSetup
    private static void setup() {
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createSequencer().build();
        insert sequencer;

        Account accountObject = MRO_UTL_TestDataFactory.account().personAccount().build();
        accountObject.ResidentialStreetNumber__c = '2';
        insert accountObject;

        Account accountObject2 = MRO_UTL_TestDataFactory.account().personAccount().build();
        accountObject2.FirstName = 'Person2';
        insert accountObject2;

        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        insert businessAccount;


        List<Individual> individualList = new List<Individual>();
        for (Integer i = 0; i < 2; i++) {
            Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
            individualList.add(individual);
        }
        insert individualList;
        List<Interaction__c> interactionList = new List<Interaction__c>();
        for (Integer i = 0; i < 2; i++) {
            Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
            interaction.CallId__c = '072902EF62ABF01'+i;
            interaction.InterlocutorPhone__c = '23344233';
            interaction.Interlocutor__c = individualList[i].Id;
            interactionList.add(interaction);
        }
        insert interactionList;


        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = businessAccount.Id;
        contact.IndividualId = individualList[0].Id;
        insert contact;

        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interactionList[0].Id, accountObject.Id, contact.Id).build();
        insert customerInteraction;

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = accountObject.Id;
        dossier.CustomerInteraction__c = customerInteraction.Id;
        insert dossier;

        List<Case> caseList = new List<Case>();
        for (Integer i = 0; i < 2; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    public static void postTest() {

        List<Account> accountObject = [
                SELECT Id,IntegrationKey__c
                FROM Account
                WHERE FirstName =: 'Person' OR  FirstName =: 'Person2'
                LIMIT 2
        ];
        List<Case> caseObject = [
                SELECT Id,IntegrationKey__c
                FROM Case
                LIMIT 2
        ];
        List<Interaction__c> interactionObject= [
                SELECT Id,CallId__c
                FROM Interaction__c
                LIMIT 2
        ];
        System.debug('interactionObject'+interactionObject.size());
        wrts_prcgvr.MRR_1_0.Request request = new wrts_prcgvr.MRR_1_0.Request();
        //Header
        request.header = new wrts_prcgvr.MRR_1_0.Header();
        request.header.requestId = MRO_UTL_Guid.NewGuid();
        request.header.requestTimestamp = String.valueOf(System.now());
        request.header.requestType = 'SurveyRequest';
        request.header.fields = new List<wrts_prcgvr.MRR_1_0.Field>();

        wrts_prcgvr.MRR_1_0.WObject mrrAccount1 = MRO_UTL_MRRMapper.sObjectToWObject(accountObject[0], 'Customer__r');
        wrts_prcgvr.MRR_1_0.WObject mrrCase1 = MRO_UTL_MRRMapper.sObjectToWObject(caseObject[0], 'Case');
        wrts_prcgvr.MRR_1_0.WObject mrrInteraction1 = MRO_UTL_MRRMapper.sObjectToWObject(interactionObject[0], 'Interaction__r');
        Wrts_prcgvr.MRR_1_0.WObject mrrObjSurvey1 = MRO_UTL_MRRMapper.createCustomMRRObject('SurveyAnswer__c', 'SurveyAnswer__r', '');

        mrrInteraction1.objects.add(mrrAccount1);
        mrrInteraction1.objects.add(mrrCase1);
        mrrInteraction1.objects.add(mrrObjSurvey1);
        request.objects = new List<wrts_prcgvr.MRR_1_0.WObject>();
        request.objects.add(mrrInteraction1);

        wrts_prcgvr.MRR_1_0.WObject mrrAccount2 = MRO_UTL_MRRMapper.sObjectToWObject(accountObject[1], 'Customer__r');
        wrts_prcgvr.MRR_1_0.WObject mrrCase2 = MRO_UTL_MRRMapper.sObjectToWObject(caseObject[1], 'Case');
        wrts_prcgvr.MRR_1_0.WObject mrrInteraction2 = MRO_UTL_MRRMapper.sObjectToWObject(interactionObject[1], 'Interaction__r');
        Wrts_prcgvr.MRR_1_0.WObject mrrObjSurvey2 = MRO_UTL_MRRMapper.createCustomMRRObject('SurveyAnswer__c', 'SurveyAnswer__r', '');

        mrrInteraction2.objects.add(mrrAccount2);
        mrrInteraction2.objects.add(mrrCase2);
        mrrInteraction2.objects.add(mrrObjSurvey2);
        request.objects.add(mrrInteraction2);
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = new wrts_prcgvr.MRR_1_0.MultiRequest();
        multiRequest.requests = new List<wrts_prcgvr.MRR_1_0.Request>();
        multiRequest.requests.add(request);
        wrts_prcgvr.MRR_1_0.MultiResponse mr = MRO_WS_GenesysServices.post(multiRequest);

        wrts_prcgvr.MRR_1_0.Request requestTwo = new wrts_prcgvr.MRR_1_0.Request();
        //Header
        requestTwo.header = new wrts_prcgvr.MRR_1_0.Header();
        requestTwo.header.requestId = MRO_UTL_Guid.NewGuid();
        requestTwo.header.requestTimestamp = String.valueOf(System.now());
        requestTwo.header.requestType = 'InteractionIVRRequest';
        requestTwo.header.fields = new List<wrts_prcgvr.MRR_1_0.Field>();
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequestTwo = new wrts_prcgvr.MRR_1_0.MultiRequest();
        multiRequestTwo.requests = new List<wrts_prcgvr.MRR_1_0.Request>();
        multiRequestTwo.requests.add(requestTwo);
        mr = MRO_WS_GenesysServices.post(multiRequestTwo);

        requestTwo.header.requestType = '';
        mr = MRO_WS_GenesysServices.post(multiRequestTwo);
    }
}