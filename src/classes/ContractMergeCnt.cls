/**
 * Created by goudiaby on 01/10/2019.
 */

public with sharing class ContractMergeCnt extends ApexServiceLibraryCnt {
    private static DossierService dossierServ = DossierService.getInstance();
    private static CaseQueries caseQuery = CaseQueries.getInstance();
    private static CaseService caseServ = CaseService.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('ContractMerge').getRecordTypeId();

    public with sharing class Initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Dossier__c dossier = dossierServ.generateDossier(accountId, dossierId, interactionId, null, dossierRecordType, 'ContractMerge');
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                response.put('dossierId', dossier.Id);
                response.put('cases', cases);
                response.put('dossier', dossier);
                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class CreateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            Map<String, String> caseRecordTypes = Constants.getCaseRecordTypes('ContractMerge');
            String recordTypeId = caseRecordTypes.get('ContractMerge');

            List<Supply__c> supplies = (List<Supply__c>) JSON.deserialize(params.get('supplies'), List<Supply__c>.class);
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');

            List<Case> newCases = new List<Case>();
            try {
                if (!supplies.isEmpty() && (String.isNotBlank(accountId)) && (String.isNotBlank(dossierId))) {
                    for (Supply__c supply : supplies) {
                        Case mergingCase = caseServ.createCaseForContractMerge(supply, accountId, dossierId, recordTypeId);
                        newCases.add(mergingCase);
                    }
                    List<Case> updatedCases = caseServ.insertCaseRecords(newCases, caseList);
                    response.put('cases', updatedCases);
                    response.put('error', false);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class UpdateCases extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String contractId = params.get('contractId');
            String contractAccountId = params.get('contractAccountId');
            List<Case> cases = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);

            String companyDivisionId;
            Savepoint sp = Database.setSavepoint();
            response.put('error', true);
            try {
                if ((String.isBlank(contractAccountId)) && (String.isBlank(contractId))) {
                    throw new WrtsException(System.Label.AtLeastOneOfTheFollowingRequestsMustBeSelected);
                }
                if (!cases.isEmpty()) {
                    companyDivisionId = cases[0].CompanyDivision__c;
                    dossierServ.updateDossierCompanyDivision(dossierId, companyDivisionId);
                    caseServ.updateCaseContractAccountAndContractId(cases, contractAccountId, contractId);
                    response.put('error', false);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String dossierId = params.get('dossierId');
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);

            caseServ.setCanceledOnCaseAndDossier(caseList, dossierId);
            response.put('error', false);
            return response;
        }
    }
}