@IsTest
private class MRO_LC_NLCCLCDataChangeTst {
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();

    @testSetup
    static void setup() {
        String dossierRequestType                 = constantsUtl.REQUEST_TYPE_NLC_CLC_DATA_CHANGE;
        String dossierNewStatus                   = constantsUtl.DOSSIER_STATUS_NEW;
        String dossierDraftStatus                 = constantsUtl.DOSSIER_STATUS_DRAFT;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 1000.0,SequenceLength__c = 7.0);
        insert sequencer;
        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder1 =  MRO_UTL_TestDataFactory.account().personAccount();
        Account acc1 = AccountBuilder1.build();
        acc1.FirstName = 'TEST';

        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder2 =  MRO_UTL_TestDataFactory.account().personAccount();
        Account acc2 = accountBuilder2.build();

        Database.SaveResult[] lsr = Database.insert(new Account[]{acc1, acc2 }, false);

        Dossier__c dossierWithoutOpenedCases = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(dossierRequestType).build();
        dossierWithoutOpenedCases.Status__c = dossierDraftStatus;
        dossierWithoutOpenedCases.Account__c = acc1.Id;
        insert dossierWithoutOpenedCases;

        Dossier__c dossierWithOpenedCases = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(dossierRequestType).build();
        dossierWithOpenedCases.Status__c = dossierDraftStatus;
        insert dossierWithOpenedCases;

        Case case1 = MRO_UTL_TestDataFactory.caseRecordBuilder().newCase().build();
        case1.Dossier__c = dossierWithOpenedCases.Id;
        case1.AccountId = acc1.Id;

        Supply__c supply1 = MRO_UTL_TestDataFactory.supply().build();
        insert supply1;
        case1.Supply__c = supply1.Id;
        insert case1;

        Interaction__c interaction1 = MRO_UTL_TestDataFactory.interaction().build();
        insert interaction1;

        CompanyDivision__c companyDivision1 = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision1;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'NLCCLCChangeWizardCodeTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;


    }

    @IsTest
    static void checkSelectedSupplyWithoutSupplyId() {
        Test.startTest();
        String supplyId = null;
        String dossierId = [SELECT id FROM Dossier__c LIMIT 1].Id;
        Object responseObj = MRO_LC_NLCCLCDataChange.checkSelectedSupply(supplyId,dossierId);
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(true, response.get('error'));
        System.assert(response.get('errorMsg') != null);
        System.assert(response.get('errorTrace') != null);
    }

    @IsTest
    static void checkSelectedSupplyWithoutDossierId() {
        Test.startTest();
        String supplyId = [SELECT id FROM Supply__c].Id;
        String dossierId = null;
        Object responseObj = MRO_LC_NLCCLCDataChange.checkSelectedSupply(supplyId,dossierId);
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(true, response.get('error'));
        System.assert(response.get('errorMsg') != null);
        System.assert(response.get('errorTrace') != null);
    }

    @IsTest
    static void checkSelectedSupplyAlreadyRegisteredToTheCurrentCustomer() {
        Test.startTest();
        String supplyId = [SELECT id FROM Supply__c].Id;
        String dossierId = [SELECT id,Dossier__c FROM Case where Dossier__c!=''].Dossier__c;
        Object responseObj = MRO_LC_NLCCLCDataChange.checkSelectedSupply(supplyId,dossierId);
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(false, response.get('isValid'));
        System.assert(response.get('message') == System.Label.TheSelectedSupplyIsAlreadyRegisteredToTheCurrentCustomer);
    }

    @IsTest
    static void updateCasesStatusEmptyList(){
        Test.startTest();
        Object responseObj = MRO_LC_NLCCLCDataChange.updateCasesStatus(new List<Case>());
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(true, response.get('error'));
        System.assert(response.get('errorMsg') != null);
        System.assert(response.get('errorTrace') != null);
    }
    @IsTest
    static void updateCasesStatusTest(){
        List<Case> listCases = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        Test.startTest();
        Object responseObj = MRO_LC_NLCCLCDataChange.updateCasesStatus(listCases);
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
    }
    @IsTest
    static void updateCasesStatusNullList(){
        Test.startTest();
        Object responseObj = MRO_LC_NLCCLCDataChange.updateCasesStatus(null);
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(true, response.get('error'));
        System.assert(response.get('errorMsg') != null);
        System.assert(response.get('errorTrace') != null);
    }

    @IsTest
    static void saveNlcClcDataChangeWithoutDossierId(){
        Test.startTest();
        String dossierId = '';
        Object responseObj = MRO_LC_NLCCLCDataChange.saveNlcClcDataChange(dossierId,new List<Case>(),false);
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(true, response.get('error'));
        System.assert(response.get('errorMsg') != null);
        System.assert(response.get('errorTrace') != null);
    }
    @IsTest
    static void saveNlcClcDataChangeWithDossierId(){
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        List<Case> listCases = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        Test.startTest();
        String dossierId = dossier.Id;
        Object responseObj = MRO_LC_NLCCLCDataChange.saveNlcClcDataChange(dossierId,listCases,false);
        Map<String,Object> response = (Map<String, Object>) responseObj;
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
    }

    @IsTest
    static void initialize(){
        Account account = [
                SELECT Id, Name, Email__c, VATNumber__c,NACECode__c,NACEReference__c,Key__c,PrimaryContact__c,
                        ONRCCode__c, Phone, PersonMobilePhone, PersonEmail, PersonContactId,NationalIdentityNumber__pc, ForeignCitizen__pc,ForeignCompany__c,
                        LastName, FirstName, IsPersonAccount, Active__c, PersonIndividualId, RecordTypeId, RecordType.Name, RecordType.DeveloperName,
                        IntegrationKey__c,JudicialAdministrator__c,JudicialLiquidator__c,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        Fax,Website,VIP__c,VATExempted__c,BusinessType__c,NumberOfEmployees,AnnualRevenue,AnnualRevenueYear__c,ContactChannel__pc,MarketingContactChannel__pc,
                        IDDocEmissionDate__pc,IDDocValidityDate__pc,IDDocumentNr__pc,IDDocumentType__pc,IDDocumentSeries__pc,IDDocIssuer__pc,BirthCity__pc,PersonBirthdate,Gender__pc,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        ResidentialStreetType__c, ResidentialStreetNumber__c, ResidentialStreetNumberExtn__c, ResidentialStreetName__c, ResidentialProvince__c, ResidentialPostalCode__c,
                        ResidentialFloor__c, ResidentialLocality__c, ResidentialCountry__c, ResidentialCity__c, ResidentialBuilding__c, ResidentialBlock__c, ResidentialApartment__c,ResidentialAddressNormalized__c,
                        ResidentialAddressKey__c, ResidentialStreetId__c,SelfReadingEnabled__c,
                        InsolvencyBankruptcyStatus__c, InsolvencyStartDate__c, InsolvencyEndDate__c, BankruptcyDate__c, ResidentialAddress__c
                FROM Account WHERE FirstName = 'TEST'
        ];

        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
        ];

        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
        ];

        String dossierId = [SELECT id,Dossier__c FROM Case where Dossier__c!=''].Dossier__c;

        Dossier__c dossier = [SELECT Id, Name, Status__c, RecordTypeId, RecordType.DeveloperName, Commodity__c,Comments__c,
                Email__c, Phone__c, SendingChannel__c, RequestType__c, SubProcess__c, Phase__c, Origin__c, Channel__c,
                Account__c, Account__r.IsPersonAccount, Account__r.PersonIndividualId, Account__r.PersonContactId, Account__r.Name,
                Account__r.ResidentialAddress__c, Account__r.Email__c, Account__r.PersonEmail, Account__r.Phone, Account__r.PersonHomePhone,
                CustomerInteraction__c, CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                CustomerInteraction__r.Interaction__r.Interlocutor__c,Opportunity__c,
                CustomerInteraction__r.Contact__c, CustomerInteraction__r.Contact__r.Email, CustomerInteraction__r.Contact__r.Phone,
                CompanyDivision__c, CompanyDivision__r.Name,OwnerId,CompanyDivision__r.Code__c,
                HasJustifyingDocuments__c FROM Dossier__c where Id = :dossierId];

        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'interactionId' => interaction.Id,
                'companyDivisionId' => companyDivision.Id,
                'dossierId' => dossierId
        };

        List<Case> cases = [SELECT Id, CaseNumber, RecordTypeId, RecordType.Name, RecordType.DeveloperName, SubProcess__c,
                BillingProfile__c, ContractAccount__c, Contract__c, Phase__c,ServicePointCode__c,AccountId,
                // FF Self Reading #WP3-P2
                CompanyDivision__c,Status,Supply__c,Supply__r.RecordType.Name, StartDate__c, EndDate__c, MarkedInvoices__c,ContractAccount__r.SelfReadingPeriodEnd__c,
                // FF Self Reading #WP3-P2
                EffectiveDate__c, ReferenceDate__c, Origin, Channel__c, CustomerNotes__c, Trader__c, Distributor__c,
                Executor__c, ExecutionTerm__c, ExecutionAmount__c, Designer__c, DesignTerm__c, DesignAmount__c,
                NLCCLC__c, VoltageLevel__c, VoltageSetting__c, Voltage__c, ContractualPower__c, AvailablePower__c, ServiceSite__c, PowerPhase__c,
                Reason__c,Count__c,Amount__c,ContractAccount__r.StartDate__c,ContractAccount__r.CompanyDivision__c, CompensationType__c, CompensationCode__c, Description,Dossier__c, Supply__r.Contract__c, CustomerNeeds__c,
                IncidentDate__c, ParentId, EvidenceDocumentsReceivedDate__c, DistributorResponseDate__c,
                ContractAccount2__c, TMPPaymentDate__c FROM Case where Dossier__c = :dossierId];

        Test.startTest();
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                'MRO_LC_NLCCLCDataChange', 'initialize', inputJSON, true);
        Test.stopTest();

        System.assertEquals(account.Id, response.get('accountId'));
        //System.assertEquals(account, response.get('account'));
        System.assertEquals(dossierId, response.get('dossierId'));
        System.assertEquals(constantsUtl.DOSSIER_STATUS_DRAFT, response.get('dossierCommodityEditStatus'));
        System.assertEquals(false, response.get('error'));
    }

    @IsTest
    static void initializeWithEmptyDossier(){
        Account account = [
                SELECT Id, Name, Email__c, VATNumber__c,NACECode__c,NACEReference__c,Key__c,PrimaryContact__c,
                        ONRCCode__c, Phone, PersonMobilePhone, PersonEmail, PersonContactId,NationalIdentityNumber__pc, ForeignCitizen__pc,ForeignCompany__c,
                        LastName, FirstName, IsPersonAccount, Active__c, PersonIndividualId, RecordTypeId, RecordType.Name, RecordType.DeveloperName,
                        IntegrationKey__c,JudicialAdministrator__c,JudicialLiquidator__c,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        Fax,Website,VIP__c,VATExempted__c,BusinessType__c,NumberOfEmployees,AnnualRevenue,AnnualRevenueYear__c,ContactChannel__pc,MarketingContactChannel__pc,
                        IDDocEmissionDate__pc,IDDocValidityDate__pc,IDDocumentNr__pc,IDDocumentType__pc,IDDocumentSeries__pc,IDDocIssuer__pc,BirthCity__pc,PersonBirthdate,Gender__pc,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        ResidentialStreetType__c, ResidentialStreetNumber__c, ResidentialStreetNumberExtn__c, ResidentialStreetName__c, ResidentialProvince__c, ResidentialPostalCode__c,
                        ResidentialFloor__c, ResidentialLocality__c, ResidentialCountry__c, ResidentialCity__c, ResidentialBuilding__c, ResidentialBlock__c, ResidentialApartment__c,ResidentialAddressNormalized__c,
                        ResidentialAddressKey__c, ResidentialStreetId__c,SelfReadingEnabled__c,
                        InsolvencyBankruptcyStatus__c, InsolvencyStartDate__c, InsolvencyEndDate__c, BankruptcyDate__c, ResidentialAddress__c
                FROM Account WHERE FirstName = 'TEST'
        ];

        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
        ];

        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
        ];

        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'interactionId' => interaction.Id,
                'companyDivisionId' => companyDivision.Id
        };



        Test.startTest();
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                'MRO_LC_NLCCLCDataChange', 'initialize', inputJSON, true);
        Test.stopTest();

        System.assertEquals(account.Id, response.get('accountId'));
        //System.assertEquals(account, response.get('account'));
        Id dossierId = (Id)response.get('dossierId');
        System.assert(dossierId != null);

        List<Case> cases = [SELECT Id, CaseNumber, RecordTypeId, RecordType.Name, RecordType.DeveloperName, SubProcess__c,
                BillingProfile__c, ContractAccount__c, Contract__c, Phase__c,ServicePointCode__c,AccountId,
                // FF Self Reading #WP3-P2
                CompanyDivision__c,Status,Supply__c,Supply__r.RecordType.Name, StartDate__c, EndDate__c, MarkedInvoices__c,ContractAccount__r.SelfReadingPeriodEnd__c,
                // FF Self Reading #WP3-P2
                EffectiveDate__c, ReferenceDate__c, Origin, Channel__c, CustomerNotes__c, Trader__c, Distributor__c,
                Executor__c, ExecutionTerm__c, ExecutionAmount__c, Designer__c, DesignTerm__c, DesignAmount__c,
                NLCCLC__c, VoltageLevel__c, VoltageSetting__c, Voltage__c, ContractualPower__c, AvailablePower__c, ServiceSite__c, PowerPhase__c,
                Reason__c,Count__c,Amount__c,ContractAccount__r.StartDate__c,ContractAccount__r.CompanyDivision__c, CompensationType__c, CompensationCode__c, Description,Dossier__c, Supply__r.Contract__c, CustomerNeeds__c,
                IncidentDate__c, ParentId, EvidenceDocumentsReceivedDate__c, DistributorResponseDate__c,
                ContractAccount2__c, TMPPaymentDate__c FROM Case where Dossier__c =: dossierId];
        System.assert(response.get('dossier') != null);
        System.assertEquals(cases, response.get('cases'));
        System.assertEquals(constantsUtl.DOSSIER_STATUS_DRAFT, response.get('dossierCommodityEditStatus'));
        System.assertEquals(false, response.get('error'));
    }


    @IsTest
    static void commodityChangeActions() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c LIMIT 1
        ];

        dossier.Commodity__c = constantsUtl.COMMODITY_GAS;
        update dossier;

        String commodity = constantsUtl.COMMODITY_ELECTRIC;
        Test.startTest();
        Map<String, Object> inputJSON = new Map<String, Object>{
                'dossierId' => dossier.Id,
                'commodity' => commodity
        };
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                'MRO_LC_NLCCLCDataChange', 'commodityChangeActions', inputJSON, true);
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
        dossier = [
                SELECT Id, Commodity__c
                FROM Dossier__c WHERE Id =: dossier.Id
        ];
        System.assertEquals(commodity, dossier.Commodity__c);
    }

    @IsTest
    static void commodityChangeActionsError() {
        try {
            Test.startTest();
            Map<String, String> inputJSON = new Map<String, String>{
                    'dossierId' => '123'
            };
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'commodityChangeActions', inputJSON, true);
            Test.stopTest();

            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);

        } catch (Exception exc) {

        }
    }

    @IsTest
    static void updateDossierStatus() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c LIMIT 1
        ];

        dossier.Status__c = constantsUtl.DOSSIER_STATUS_DRAFT;
        update dossier;

        Test.startTest();
        Map<String, Object> inputJSON = new Map<String, Object>{
                'dossierId' => dossier.Id
        };
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                'MRO_LC_NLCCLCDataChange', 'updateDossierStatus', inputJSON, true);
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
        dossier = [
                SELECT Id, Status__c
                FROM Dossier__c WHERE Id =: dossier.Id
        ];
        System.assertEquals(constantsUtl.DOSSIER_STATUS_NEW, dossier.Status__c);
    }

    @IsTest
    static void updateDossierStatusError() {
        try {
            Test.startTest();
            Map<String, String> inputJSON = new Map<String, String>{
                    'dossierId' => '123'
            };
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'updateDossierStatus', inputJSON, true);
            Test.stopTest();

            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);

        } catch (Exception exc) {

        }
    }

    @IsTest
    static void updateDossierCompanyDivision() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c LIMIT 1
        ];

        dossier.CompanyDivision__c = null;
        update dossier;

        String companyDivisionId = [
                SELECT Id
                FROM CompanyDivision__c LIMIT 1
        ].Id;

        dossier.CompanyDivision__c = null;
        update dossier;

        Test.startTest();
        Map<String, Object> inputJSON = new Map<String, Object>{
                'dossierId' => dossier.Id,
                'companyDivisionId' => companyDivisionId
        };
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                'MRO_LC_NLCCLCDataChange', 'updateDossierCompanyDivision', inputJSON, true);
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
        dossier = [
                SELECT Id, CompanyDivision__c
                FROM Dossier__c WHERE Id =: dossier.Id
        ];
        System.assertEquals(companyDivisionId, dossier.CompanyDivision__c);
    }

    @IsTest
    static void updateDossierCompanyDivisionError() {
        try {
            Test.startTest();
            Map<String, String> inputJSON = new Map<String, String>{
                    'dossierId' => '123'
            };
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'updateDossierCompanyDivision', inputJSON, true);
            Test.stopTest();

            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);

        } catch (Exception exc) {

        }
    }

    @IsTest
    static void setChannelAndOriginWithOnlyDossier() {

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c LIMIT 1
        ];
        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'setChannelAndOrigin', inputJSON, true);
            Test.stopTest();
            dossier = [
                    SELECT Id, Origin__c, Channel__c
                    FROM Dossier__c WHERE Id =: dossier.Id
            ];
            System.assertEquals(response.get('error'), false);
            System.assertEquals(null, dossier.Origin__c);
            System.assertEquals(null, dossier.Channel__c);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void setChannelAndOriginError() {
        Map<String, String> inputJSON = new Map<String, String>{};
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'setChannelAndOrigin', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void deleteSelectedCase() {

        Case myCase = [
                SELECT Id, Dossier__c
                FROM Case
        ];

        Map<String, String> inputJSON = new Map<String, String>{
                'recordId' => myCase.Id
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'deleteSelectedCase', inputJSON, true);
            Test.stopTest();
            System.assertEquals(false, response.get('error'));
            System.assertEquals(myCase.Id, response.get('recordId'));
            List<Case> cases = [
                    SELECT Id
                    FROM Case WHERE Id =: myCase.Id
            ];
            System.assert(cases.isEmpty());
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void deleteSelectedCaseError() {

        Map<String, String> inputJSON = new Map<String, String>{
                'recordId' => '123'
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'deleteSelectedCase', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void updatePrivacyChangeDossier() {
        Dossier__c dossier = [
                SELECT Id, (SELECT Id FROM PrivacyChanges__r)
                FROM Dossier__c LIMIT 1
        ];

        delete dossier.PrivacyChanges__r;

        PrivacyChange__c privacyChange = MRO_UTL_TestDataFactory.PrivacyChange().createPrivacyChangeBuilder().build();
        insert privacyChange;

        Test.startTest();
        Map<String, Object> inputJSON = new Map<String, Object>{
                'dossierId' => dossier.Id,
                'privacyChangeId' => privacyChange.Id
        };
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                'MRO_LC_NLCCLCDataChange', 'updatePrivacyChangeDossier', inputJSON, true);
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
        PrivacyChange__c responsePrivacyChange = (PrivacyChange__c)response.get('privacyChange');
        PrivacyChange__c loadedPrivacyChange = [
                SELECT Id, Dossier__c
                FROM PrivacyChange__c WHERE Id =: privacyChange.Id
        ];
        System.assertEquals(dossier.Id, loadedPrivacyChange.Dossier__c);
        System.assertEquals(dossier.Id, responsePrivacyChange.Dossier__c);
    }

    @IsTest
    static void updatePrivacyChangeDossierError() {
        try {
            Test.startTest();
            Map<String, String> inputJSON = new Map<String, String>{
                    'dossierId' => '123',
                    'privacyChangeId' => '123'
            };
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'updatePrivacyChangeDossier', inputJSON, true);
            Test.stopTest();

            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);

        } catch (Exception exc) {

        }
    }
/*
    @IsTest
    static void getScriptTemplateTest(){
        Map<String, String > inputJSON = new Map<String, String>{
                'scriptTemplateCode' => 'Code'
        };

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_NLCCLCDataChange', 'getScriptTemplate',
                inputJSON, true);
        system.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void getScriptTemplateExceptionTest(){
        Map<String, String > inputJSON = new Map<String, String>{
                'scriptTemplateCode' => 'null'
        };

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_NLCCLCDataChange', 'getScriptTemplate',
                inputJSON, true);
        system.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }
*/

    @IsTest
    static void getUpdatedCaseListTest(){
        Dossier__c dossier =[
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_NLCCLCDataChange', 'getUpdatedCaseList',
                inputJSON, true);
        system.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void getUpdatedCaseListExceptionTest(){

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => 'dossierId'
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_NLCCLCDataChange', 'getUpdatedCaseList',
                inputJSON, true);
        system.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    static void deleteCase() {
        List<Dossier__c> dossier =[
                SELECT Id
                FROM Dossier__c
                LIMIT 2
        ];
        Map<String, Object> response = MRO_LC_NLCCLCDataChange.deleteCases(dossier[1].Id);
            Test.startTest();
        System.assertEquals(true, response.get('error') == false);
            Test.stopTest();
    }

    @IsTest
    static void deleteCaseKO() {
        String Dossier = 'jfhdjfdf12145';
        Map<String, Object> response = MRO_LC_NLCCLCDataChange.deleteCases(Dossier);
        Test.startTest();
        System.assertEquals(true, response.get('error') == true);
        Test.stopTest();
    }


    @IsTest
    static void InitializeExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'MRO_LC_NLCCLCDataChange', 'initialize', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == true );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
}