public with sharing class CustomerInteractionService {

    private static final CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static final AccountContactRelationQueries accountContactRelationQuery = AccountContactRelationQueries.getInstance();
    private static final ContactQueries contactQuery = ContactQueries.getInstance();

    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    public static CustomerInteractionService getInstance() {
        return (CustomerInteractionService)ServiceLocator.getInstance(CustomerInteractionService.class);
    }

    public List<CustomerInteractionDTO> listByInteractionId(String interactionId) {
        if(interactionId == null) {
            return new List<CustomerInteractionDTO>();
        }
        List<CustomerInteraction__c> customerInteractionList = customerInteractionQuery.listByInteractionId(interactionId);

        Set<String> contactIds = SobjectUtils.setNotNullByField(customerInteractionList, CustomerInteraction__c.Contact__c);
        Set<String> accountIds = SobjectUtils.setNotNullByField(customerInteractionList, CustomerInteraction__c.Customer__c);
        List<AccountContactRelation> accountContactRelations = accountContactRelationQuery.listByContactIdsAndAccountIds(contactIds, accountIds);

        Map<String, AccountContactRelation> accountIdAndContactIdToRelation = new Map<String, AccountContactRelation>();
        for(AccountContactRelation relation : accountContactRelations) {
            accountIdAndContactIdToRelation.put('' + relation.AccountId + relation.ContactId, relation);
        }
        return mapToDto(customerInteractionList, accountIdAndContactIdToRelation);
    }

    private CustomerInteractionDTO mapToDto(CustomerInteraction__c customerInteractionObj) {
        CustomerInteractionDTO customerInteraction = new CustomerInteractionDTO();
        customerInteraction.id = customerInteractionObj.Id;
        customerInteraction.interactionId = customerInteractionObj.Interaction__c;
        customerInteraction.customerId = customerInteractionObj.Customer__c;
        customerInteraction.customerName = customerInteractionObj.Customer__r.Name;

        customerInteraction.address = customerInteractionObj.Customer__r.ResidentialAddress__c;
        customerInteraction.contactId = customerInteractionObj.Contact__c;
        customerInteraction.isCustomerIsPerson = customerInteractionObj.Customer__r.IsPersonAccount;
        customerInteraction.isCustomerAndInterlocutor = false;
        if(customerInteractionObj.Customer__r.IsPersonAccount) {
            customerInteraction.customerVatOrId = customerInteractionObj.Customer__r.NationalIdentityNumber__pc;
            if(customerInteractionObj.Interaction__r.Interlocutor__c != null
              && customerInteractionObj.Customer__r.PersonIndividualId == customerInteractionObj.Interaction__r.Interlocutor__c) {
              customerInteraction.isCustomerAndInterlocutor = true;
            }
        } else {
            customerInteraction.customerVatOrId = customerInteractionObj.Customer__r.VATNumber__c;
        }
        return customerInteraction;
    }

    private List<CustomerInteractionDTO> mapToDto(List<CustomerInteraction__c> customerInteractionObjList, Map<String, AccountContactRelation> accountIdAndContactIdToRoles) {
        List<CustomerInteractionDTO> customerInteractionList = new List<CustomerInteractionDTO>();
        for(CustomerInteraction__c customerInteractionObj : customerInteractionObjList) {
            CustomerInteractionDTO customerInteraction = mapToDto(customerInteractionObj);
            AccountContactRelation relation = accountIdAndContactIdToRoles.get('' + customerInteractionObj.Customer__c + customerInteractionObj.Contact__c);
            if(relation != null) {
               customerInteraction.relationId = relation.Id;
               customerInteraction.role = relation.Roles;
            }
            customerInteractionList.add(customerInteraction);
        }
        return customerInteractionList;
    }

    /**
    * @author Moussa Fofana
    * @description This method return a customer interaction from dto
    * @param CustomerInteractionDTO
    * @return CustomerInteraction__c
    */
    public CustomerInteraction__c mapFromDto(CustomerInteractionDTO customInteractionDTO){
        CustomerInteraction__c customerInteraction = new CustomerInteraction__c();
        customerInteraction.Customer__c=customInteractionDTO.customerId;
        customerInteraction.Contact__c=customInteractionDTO.contactId;
        customerInteraction.Interaction__c=customInteractionDTO.interactionId;
        return customerInteraction;
    }

    /**
    * @author Moussa Fofana
    * @description This method return a customer interaction from dto
    * @param CustomerInteraction__c
    * @return CustomerInteraction__c 
    */
    public CustomerInteraction__c upsertCustomerInteraction(CustomerInteraction__c customerInteraction){
        databaseSrv.upsertSObject(customerInteraction);
        return customerInteraction;
    }

// TODO: ST not used
//    public CustomerInteraction__c insertCustomerInteraction(String accountId, String interactionId){
//        return insertCustomerInteraction(accountId, interactionId, null);
//    }

    public CustomerInteraction__c insertCustomerInteraction(String accountId, String interactionId, String contactId){
        CustomerInteraction__c customerInteractionObj = new CustomerInteraction__c(
            Customer__c = accountId,
            Interaction__c = interactionId,
            Contact__c = contactId
        );
        databaseSrv.insertSObject(customerInteractionObj);
        return customerInteractionObj;
    }

    public void deleteById(String customerInteractionId) {
        databaseSrv.deleteSObject(customerInteractionId);
    }

    public void assignContactToInteraction(String contactId, String customerInteractionId) {
        CustomerInteraction__c customerInteractionObj = customerInteractionQuery.findById(customerInteractionId);
        Contact contactObj = contactQuery.findById(contactId);
        Savepoint sp = Database.setSavepoint();
        try {
            CustomerInteraction__c customerInteraction = new CustomerInteraction__c(
                Id = customerInteractionId,
                Contact__c = contactId
            );
            databaseSrv.updateSObject(customerInteraction);
            if(String.isNotBlank(contactObj.IndividualId)) {
                Interaction__c interaction = new Interaction__c(
                    Id = customerInteractionObj.Interaction__c,
                    Interlocutor__c = contactObj.IndividualId
                );
                databaseSrv.updateSObject(interaction);
            }
        }
        catch (Exception e) {
            Database.rollback(sp);
            throw e;
        }
    }

    public void setUniqueCode(List<CustomerInteraction__c> customerInteractionList) {
        for(CustomerInteraction__c customerInteraction : customerInteractionList) {
            customerInteraction.UniqueCode__c = (String)customerInteraction.Customer__c + (String)customerInteraction.Interaction__c;
        }
    }


    public with sharing class CustomerInteractionDTO {

        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String customerName {get; set;}
        @AuraEnabled
        public String customerVatOrId {get; set;}
        @AuraEnabled
        public String role {get; set;}
        @AuraEnabled
        public String address {get; set;}
        @AuraEnabled
        public String relationId {get; set;}
        @AuraEnabled
        public String customerId {get; set;}
        @AuraEnabled
        public String contactId {get; set;}
        @AuraEnabled
        public String interactionId {get; set;}
        @AuraEnabled
        public Boolean isCustomerIsPerson {get; set;}
        @AuraEnabled
        public Boolean isCustomerAndInterlocutor {get; set;}

        public CustomerInteractionDTO() {}
//        public CustomerInteractionDTO(CustomerInteraction__c customerInteraction) {
//            this.Id = customerInteraction.Id;
//        }
    }
}