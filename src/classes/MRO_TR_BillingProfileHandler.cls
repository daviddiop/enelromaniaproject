/**
 * Created by ferhati on 10/02/2020.
 */

global with sharing class MRO_TR_BillingProfileHandler implements TriggerManager.ISObjectTriggerHandler {

    global void afterDelete() {}

    global void afterInsert() {}

    global void afterUndelete() {}

    global void afterUpdate() {}

    global void beforeDelete() {}

    global void beforeInsert() {
        this.checkIbanValidation(Trigger.new, null);
        System.debug('beforeInsert trigger BillingProfile 1 - Queries used in this apex code so far: ' + Limits.getQueries());
        this.checkRefundIbanValidation(Trigger.new, null);
        System.debug('beforeInsert trigger BillingProfile 2 - Queries used in this apex code so far: ' + Limits.getQueries());
    }

    global void beforeUpdate() {
        this.checkIbanValidation(Trigger.new, (Map<Id, BillingProfile__c>)Trigger.oldMap);
        System.debug('beforeUpdate trigger BillingProfile 1 - Queries used in this apex code so far: ' + Limits.getQueries());
        this.checkRefundIbanValidation(Trigger.new, (Map<Id, BillingProfile__c>)Trigger.oldMap);
        System.debug('beforeUpdate trigger BillingProfile 2 - Queries used in this apex code so far: ' + Limits.getQueries());
    }

    private void checkIbanValidation(List<BillingProfile__c> billingProfileListNew, Map<Id, BillingProfile__c> billingProfileMapOld) {
        for(BillingProfile__c newBillingProfile : billingProfileListNew) {
            if (newBillingProfile.IBAN__c != null && (billingProfileMapOld == null || (newBillingProfile.IBAN__c != billingProfileMapOld.get(newBillingProfile.Id).IBAN__c))) {
                MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkIbanValidation(newBillingProfile.IBAN__c);
                if (result.outCome == false){
                    System.debug('result.errorCode: ' + result.errorCode);
                    System.debug('newBillingProfile.IBAN__c: ' + newBillingProfile.IBAN__c);
                    newBillingProfile.IBAN__c.addError(String.format(Label.incorrectIBANNumber, new List<String>{result.errorCode, newBillingProfile.IBAN__c}));
                }
            }
        }
    }

    private void checkRefundIbanValidation(List<BillingProfile__c> billingProfileListNew, Map<Id, BillingProfile__c> billingProfileMapOld) {
        for(BillingProfile__c newBillingProfile : billingProfileListNew) {
            if (newBillingProfile.RefundIBAN__c != null && (billingProfileMapOld == null || (newBillingProfile.RefundIBAN__c != billingProfileMapOld.get(newBillingProfile.Id).RefundIBAN__c))) {
                MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkIbanValidation(newBillingProfile.RefundIBAN__c);
                if (result.outCome == false){
                    System.debug('result.errorCode: ' + result.errorCode);
                    System.debug('newBillingProfile.IBAN__c: ' + newBillingProfile.RefundIBAN__c);
                    newBillingProfile.RefundIBAN__c.addError(String.format(Label.incorrectIBANNumber, new List<String>{result.errorCode, newBillingProfile.RefundIBAN__c}));
                }
            }
        }
    }
}