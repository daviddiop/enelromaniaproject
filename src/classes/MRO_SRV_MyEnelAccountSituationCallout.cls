/**
 * Created by Boubacar Sow on 20/10/2020.
 */

public with sharing class MRO_SRV_MyEnelAccountSituationCallout implements MRO_SRV_SendMRRcallout.IMRRCallout {

    private static final String requestTypeAssociatedAccount = 'GetAssociatedAccounts';
    private static final String requestTypeNotifications = 'GetAccountNotificationSettings';
    private static final String requestTypeConsumptionPlaces = 'GetAccountConsumptionPlace';
    private static final String endPoint = 'myenel';

    public static MRO_SRV_MyEnelAccountSituationCallout getInstance(){
        return new MRO_SRV_MyEnelAccountSituationCallout();
    }



    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap) {
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        Map<String, Object> conditions = (Map<String, Object>) argsMap.get('conditions');
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);
        String requestType = parameters.get('requestType');
        wrts_prcgvr.MRR_1_0.Field fieldParam;

        if(requestType == requestTypeAssociatedAccount){
            fieldParam = MRO_UTL_MRRMapper.createField('paymentCode', conditions.get('paymentCode'));
        }
        if(requestType != requestTypeAssociatedAccount){
                fieldParam = MRO_UTL_MRRMapper.createField('username', conditions.get('username'));
        }

        request.header.fields.add(fieldParam);
        multiRequest.requests.add(request);

        return multiRequest;

    }

    public List<MRO_LC_AssociatedAccount.AssociatedAccount> getAssociatedAccountsList (String paymentCode){
        Map<String, Object> conditions = new Map<String, Object>{
            'paymentCode' => paymentCode
        };

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(endPoint, requestTypeAssociatedAccount, conditions, 'MRO_SRV_MyEnelAccountSituationCallout');

        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
            System.debug('###calloutResponse.responses[0].objects '+calloutResponse.responses[0].objects);
            System.debug('###calloutResponse.responses[0].description '+calloutResponse.responses[0].description);

            List<MRO_LC_AssociatedAccount.AssociatedAccount> associatedAccountsList = new List<MRO_LC_AssociatedAccount.AssociatedAccount>();

            for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {
                Map<String, String> responseWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);

                System.debug(responseWoMap.get('ErrorMessage'));
                if (responseWo.objects != null && responseWo.objects.size() != 0) {
                    for (wrts_prcgvr.MRR_1_0.WObject associatedAccountWo : responseWo.objects) {
                        Map<String, String> associatedAccountWoMap = MRO_UTL_MRRMapper.wobjectToMap(associatedAccountWo);

                        MRO_LC_AssociatedAccount.AssociatedAccount associatedAccount = new MRO_LC_AssociatedAccount.AssociatedAccount(associatedAccountWoMap);

                        System.debug('###associatedAccount '+associatedAccount);

                        associatedAccountsList.add(associatedAccount);
                    }
                }
            }

            System.debug('###associatedAccountsList '+associatedAccountsList);
            return associatedAccountsList;
        }

        return null;
    }


    public List<MRO_LC_Notification.Notification> getNotificationsList (String username){
        Map<String, Object> conditions = new Map<String, Object>{
            'username' => username
        };

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(endPoint, requestTypeNotifications, conditions, 'MRO_SRV_MyEnelAccountSituationCallout');

        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
            System.debug(calloutResponse.responses[0].objects);
            System.debug(calloutResponse.responses[0].description);

            List<MRO_LC_Notification.Notification> notificationsList = new List<MRO_LC_Notification.Notification>();

            for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {
                Map<String, String> responseWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);

                System.debug(responseWoMap.get('ErrorMessage'));

                if (responseWo.objects != null && responseWo.objects.size() != 0) {
                    for (wrts_prcgvr.MRR_1_0.WObject notificationWo : responseWo.objects) {
                        Map<String, String> notificationWoMap = MRO_UTL_MRRMapper.wobjectToMap(notificationWo);

                        MRO_LC_Notification.Notification notification = new MRO_LC_Notification.Notification(notificationWoMap);

                        System.debug('###notification '+notification);

                        notificationsList.add(notification);
                    }
                }
            }

            System.debug('###notificationsList '+notificationsList);
            return notificationsList;
        }

        return null;
    }


    public List<MRO_LC_ConsumptionPlaces.ConsumptionPlaces> getConsumptionPlacesList (String username){
        Map<String, Object> conditions = new Map<String, Object>{
            'username' => username
        };

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(endPoint, requestTypeConsumptionPlaces, conditions, 'MRO_SRV_MyEnelAccountSituationCallout');

        if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {
            System.debug(calloutResponse.responses[0].objects);
            System.debug(calloutResponse.responses[0].description);

            List<MRO_LC_ConsumptionPlaces.ConsumptionPlaces> consumptionPlacesList = new List<MRO_LC_ConsumptionPlaces.ConsumptionPlaces>();

            for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {
                Map<String, String> responseWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);

                System.debug(responseWoMap.get('ErrorMessage'));

                if (responseWo.objects != null && responseWo.objects.size() != 0) {
                    for (wrts_prcgvr.MRR_1_0.WObject consumptionPlacesWo : responseWo.objects) {
                        Map<String, String> consumptionPlacesWoMap = MRO_UTL_MRRMapper.wobjectToMap(consumptionPlacesWo);

                        MRO_LC_ConsumptionPlaces.ConsumptionPlaces consumptionPlaces = new MRO_LC_ConsumptionPlaces.ConsumptionPlaces(consumptionPlacesWoMap);

                        System.debug('###consumptionPlaces '+consumptionPlaces);

                        consumptionPlacesList.add(consumptionPlaces);
                    }
                }
            }

            System.debug('###consumptionPlacesList '+consumptionPlacesList);
            return consumptionPlacesList;
        }

        return null;
    }



    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse) {
        return null;
    }

    public wrts_prcgvr.MRR_1_0.MultiResponse executeCallout(String endPoint, String requestType, Map<String, Object> conditions, String className) {

        Map<String, Object> argsMap = new Map<String, Object>();
        argsMap.put('method', className);
        argsMap.put('conditions', conditions);

        Map<String, String> parameters = new Map<String, String>();
        parameters.put('requestType', requestType);
        parameters.put('timeout', '60000');
        parameters.put('endPoint', endPoint);
        argsMap.put('parameters', parameters);

        MRO_SRV_SendMRRcallout instance = new MRO_SRV_SendMRRcallout();
        Object executionResult = instance.execute((Object) argsMap, true);
        System.debug(executionResult);
        if (executionResult instanceof wrts_prcgvr.MRR_1_0.MultiResponse) {
            return (wrts_prcgvr.MRR_1_0.MultiResponse) executionResult;
        }
        return null;
    }

}