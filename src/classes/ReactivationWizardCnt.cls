/**
 * Created by ferhati on 21/10/2019.
 */

public with sharing class ReactivationWizardCnt extends ApexServiceLibraryCnt {
    private static OpportunityQueries opportunityQuery = OpportunityQueries.getInstance();
    private static OpportunityLineItemQueries oliQuery = OpportunityLineItemQueries.getInstance();
    private static OpportunityServiceItemQueries osiQuery = OpportunityServiceItemQueries.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();

    private static OpportunityService oppService = OpportunityService.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static final UserQueries userQuery = UserQueries.getInstance();

    public with sharing class  initialize extends AuraCallable{
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String opportunityId = params.get('opportunityId');
            String interactionId = params.get('interactionId');

            String currentCompanyDivisionId = params.get('companyDivisionId');
            User currentUserInfos = userQuery.getCompanyDivisionId(UserInfo.getUserId());
            Opportunity opp;

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account +' - '+ System.Label.MissingId);
            }

            Account acc = accQuery.findAccount(accountId);
            if(String.isBlank(currentCompanyDivisionId)){
                currentCompanyDivisionId=null;
            }

            if (String.isBlank(opportunityId)){
                Id cusId = null;
                List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);

                if(!cus.isEmpty()){
                    cusId = cus[0].Id;
                }

                opp = oppService.insertOpportunityByCustomerInteraction(cusId, accountId, 'Reactivation',currentCompanyDivisionId);
            } else {
                opp = opportunityQuery.getOpportunityById(opportunityId);
                response.put('companyDivisionId', opp.CompanyDivision__c);
                response.put('companyDivisionName', opp.CompanyDivision__r.Name);
                response.put('contractIdFromOpp', opp.ContractId);
                response.put('customerSignedDate', opp.ContractSignedDate__c);
                response.put('opportunityCompanyDivisionId', opp.CompanyDivision__c);
            }
            List<OpportunityLineItem> olis = oliQuery.getOLIsByOpportunityId(opp.Id);
            List<OpportunityServiceItem__c> osis = osiQuery.getOSIsByOpportunityId(opp.Id);
            String contractAccountId = '';
            if (!osis.isEmpty()) {
                contractAccountId = osis[0].ContractAccount__c;
            }
            response.put('opportunityId', opp.Id);
            response.put('opportunity', opp);
            response.put('opportunityLineItems', olis);
            response.put('opportunityServiceItems', osis);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('contractAccountId', contractAccountId);
            response.put('stage', opp.StageName);
            response.put('user', currentUserInfos);
            response.put('error', false);
            return  response;
        }
    }

    public with sharing class updateOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String privacyChangeId = params.get('privacyChangeId');
            String contractId = params.get('contractId');
            String stage = params.get('stage');
            Opportunity opps;
            Opportunity opp = new Opportunity();
            if (String.isBlank(opportunityId)) {
                throw new WrtsException(System.Label.Opportunity +' - '+ System.Label.MissingId);
            }
            opp.Id = opportunityId;
            opp.StageName = stage;
            databaseSrv.upsertSObject(opp);
            if (stage == 'Closed Won') {
                Contract myContract = new Contract();
                opps = opportunityQuery.getOpportunityById(opportunityId);
                myContract.Id = String.isBlank(opps.ContractId) ? null : opps.ContractId;
                if (opps.ContractSignedDate__c != null) {
                    myContract.CustomerSignedDate = opps.ContractSignedDate__c;
                }
                OpportunityService.generateAcquisitionChain(opp.Id, 'Reactivation', myContract, privacyChangeId);
            }
            return new Map<String, Object>{
                    'opportunityId'=> opp.Id
            };
        }
    }

    public with sharing class updateOsiList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
            InputData inputData = (InputData) JSON.deserialize(jsonInput, InputData.class);
            for (OpportunityServiceItem__c osi : inputData.opportunityServiceItems) {
                osiList.add(new OpportunityServiceItem__c(Id = osi.Id, ContractAccount__c = inputData.contractAccountId));
            }
            databaseSrv.updateSObject(osiList);
            return null;
        }
    }

    public with sharing class checkOsi extends AuraCallable{
        public override Object perform(final String jsonInput){
            Map<String, String> params = asMap(jsonInput);
            Savepoint sp = Database.setSavepoint();
            String osiId = params.get('osiId');
            if (String.isBlank(osiId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem +' - '+ System.Label.MissingId);
            }
            OpportunityServiceItem__c osi = osiQuery.getById(osiId);
            Opportunity opp = osi.Opportunity__r;
            ServicePoint__c point = OpportunityService.instantiateServicePoint(osi);
            databaseSrv.upsertSObject(point);

            Supply__c supply = OpportunityService.instantiateSupply(opp, osi);
            supply.ServicePoint__c = point.Id;
            databaseSrv.insertSObject(supply);

            Case myCase = OpportunityService.instantiateCase(opp, osi, 'Reactivation');
            myCase.Supply__c = osi.ServicePoint__r.CurrentSupply__c;
            databaseSrv.insertSObject(myCase);
            Database.rollback(sp);
            return new Map<String, Object>{
                    'opportunityServiceItem' =>osi
            };
        }
    }
    public with sharing class linkOliToOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
            InputData inputData = (InputData) JSON.deserialize(jsonInput, InputData.class);
            for (OpportunityServiceItem__c osi : inputData.opportunityServiceItems) {
                osiList.add(new OpportunityServiceItem__c(Id = osi.Id, Product__c = inputData.product2Id));
            }
            databaseSrv.updateSObject(osiList);
            return null;
        }
    }
    public with sharing class InputData {
        @AuraEnabled
        public String contractAccountId{get;set;}
        @AuraEnabled
        public String product2Id{get;set;}
        @AuraEnabled
        public List<OpportunityServiceItem__c> opportunityServiceItems {get;set;}
        @AuraEnabled
        public String traderId {get;set;}
        @AuraEnabled
        public String accountId {get;set;}
        @AuraEnabled
        public List<Supply__c> supplyList {get;set;}
    }

    public with sharing class updateCompanyDivisionInOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String companyDIvisionId=params.get('companyDivisionId');

            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
            try {
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.companyDivision__c=companyDIvisionId;
                databaseSrv.upsertSObject(opp);
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public with sharing class updateContractAndContractSignedDateOnOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String customerSignedDate = params.get('customerSignedDate');
            String contractId = params.get('contractId');

            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
            try {
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.ContractId = String.isBlank(contractId) ? null : contractId;
                opp.ContractSignedDate__c = String.isBlank(customerSignedDate) ? null : Date.valueOf(customerSignedDate);
                databaseSrv.upsertSObject(opp);
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}