/**
 * Created by Boubacar Sow on 08/11/2019.
 * @version 1.0
 * @description
 *          [ENLCRO-195] Create MRO_LCP_DemonstratedPaymentWizard Aura Component
 *          [ENLCRO-196] Integrate ContractAccountSelection in the DemostratedPaymentWizard
 *          [ENLCRO-197] Integrate Invoices Inquiry Component in the DemonstratedPaymentWizard
 *          [ENLCRO-450] Demonstrated Payment - Integrate Proces Checklist.
 *
 *
 */

public Inherited sharing class MRO_LC_DemonstratedPayment extends ApexServiceLibraryCnt {
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static CaseQueries caseQuery = CaseQueries.getInstance();
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();
    private static String dossierNewStatus = constantsSrv.DOSSIER_STATUS_NEW;
    private static MRO_SRV_ScriptTemplate scriptTemplateSrv = MRO_SRV_ScriptTemplate.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    //static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('GenericRequest').getRecordTypeId();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
    
    /**
     * @author Boubacar Sow
     * @description  Initialize method to get data at the beginning for Demonstrated Payment
     */
    public class InitializeDemonstratedPayment extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String templateId = params.get('templateId');
            String genericRequestId  = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            /*    if (String.isNotBlank(dossierId)) {
                    Dossier__c dossierCheckParentId = dossierQuery.getById(dossierId);

                    if (String.isNotBlank(dossierCheckParentId.Parent__c)) {
                        dossierId = dossierCheckParentId.Id;
                    } else if (dossierCheckParentId.RecordType.DeveloperName == 'GenericRequest') {
                        genericRequestId = dossierId;
                        dossierId = '';
                    }
                } */

            try {
                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'Demonstrated Payment');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c)  && String.isNotBlank(genericRequestId) ){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                if (String.isNotBlank(dossier.Id)) {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                    response.put('caseTile', cases);
                }
                String  code = 'DemonstatedPayement';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }

                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('accountId', accountId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    /**
     * @author Boubacar Sow
     * @description CreateCase method to save data  for Demonstrated Payment
     */
    public class CreateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            
            System.debug('####');
            PaymentInformationInput paymentInformationInput = (PaymentInformationInput) JSON.deserialize(params.get('paymentInformation'), PaymentInformationInput.class);
            System.debug('#### PaymentInformationInput '+paymentInformationInput);
            String dossierId = params.get('dossierId');
            String accountId = params.get('accountId');
            String invoiceIds = params.get('invoiceIds');
            String companyDivisionId = params.get('companyDivisionId');
            String contractAccountId = params.get('contractAccountId');
            String channel = params.get('channel');
            String origin = params.get('origin');
            System.debug('channel'+channel);
            System.debug('origin'+origin);
            String recordTypeId;
            Map<String, String> demonstratedPaymentRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('DemonstratedPayment');
            try {
                if ((String.isNotBlank(companyDivisionId)) && (String.isNotBlank(contractAccountId)) && (String.isNotBlank(accountId)) && (String.isNotBlank(dossierId)) && (String.isNotBlank(invoiceIds)) ) {
                    recordTypeId = demonstratedPaymentRecordTypes.get('DemonstratedPayment');
                    Case caseInvoice = caseSrv.insertCaseWithInvoiceForDemonstratedPayment(accountId, dossierId, recordTypeId, companyDivisionId, contractAccountId, invoiceIds, paymentInformationInput.amount, paymentInformationInput.effectiveDate, paymentInformationInput.note, origin, channel, 'RE010');
                    System.debug('#### case: ' + caseInvoice);
                    if (caseInvoice != null) {
                        Dossier__c updatedDossier =  dossierSrv.updateDossierCompanyDivisionWithChannel(dossierId, dossierNewStatus, companyDivisionId, channel, origin);
                        caseSrv.applyAutomaticTransitionOnDossierAndCases(updatedDossier.Id);
                        response.put('error', false);
                    }
                }
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }
    
    /**
     * @author Boubacar Sow
     * @description  CancelProcess method to concel dossier statut  for Demonstrated Payment
     */
    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            
            String dossierId = params.get('dossierId');
    
            dossierSrv.setCanceledOnDossier(dossierId);
            response.put('error', false);
            return response;
        }
    }

    // Classes for deserialize JSON into Objects
    public class PaymentInformationInput {
        @AuraEnabled
        public Double amount { get; set; }
        @AuraEnabled
        public Date effectiveDate { get; set; }
        @AuraEnabled
        public String note { get; set; }
    }


}