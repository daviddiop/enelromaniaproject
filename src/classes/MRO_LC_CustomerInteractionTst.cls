/**
 * Created by BADJI on 20/07/2020.
 */

@IsTest
public with sharing class MRO_LC_CustomerInteractionTst {

    @TestSetup
    static void setup(){
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        AccountContactRelation accountContactRelation = MRO_UTL_TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id,listAccount[0].Id).build();
        AccountContactRelation accountContactRelation2 = MRO_UTL_TestDataFactory.AccountContactRelation().setRoles('Utente business').build();
        accountContactRelation.Roles = accountContactRelation2.Roles;
        insert accountContactRelation;
    }

    @IsTest
    static void initDataTest(){
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'interactionId' => customerInteraction.Id
        };
        Test.startTest();
        TestUtils.exec(
            'MRO_LC_CustomerInteraction', 'initData', inputJSON, false);
        Test.stopTest();
    }

    @IsTest
    static void updateRoleTest() {
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        AccountContactRelation accountContactRelation2 = MRO_UTL_TestDataFactory.AccountContactRelation().setRoles('Utente business').build();
        accountContactRelation.Roles = accountContactRelation2.Roles;
        update accountContactRelation;
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        MRO_SRV_CustomerInteraction.CustomerInteractionDTO CustomerInteractionDTO = new MRO_SRV_CustomerInteraction.CustomerInteractionDTO();
        CustomerInteractionDTO.relationId = accountContactRelation.Id;
        CustomerInteractionDTO.role = accountContactRelation.Roles ;
        CustomerInteractionDTO.interactionId = interaction.Id;
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Object response=TestUtils.exec('MRO_LC_CustomerInteraction','updateRole',jsonInput,true);
        Test.startTest();
        System.assertNotEquals(response , new List<MRO_SRV_CustomerInteraction.CustomerInteractionDTO>());
        AccountContactRelation accountContactRelation3 = [
            SELECT Id,Roles
            FROM AccountContactRelation
            WHERE Id =:accountContactRelation.Id
        ];
        System.assertEquals(accountContactRelation3.Roles,'Utente business');
        Test.stopTest();
    }

    @isTest
    static void deleteCustomerInteractionTest() {
        List<CustomerInteraction__c> customerInteractionList1 = [
            SELECT Id
            FROM CustomerInteraction__c
        ];
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        MRO_SRV_CustomerInteraction.CustomerInteractionDTO customerInteractionDTO = new MRO_SRV_CustomerInteraction.CustomerInteractionDTO();
        customerInteractionDTO.relationId = accountContactRelation.Id;
        customerInteractionDTO.role = accountContactRelation.Roles;
        customerInteractionDTO.interactionId = interaction.Id;
        customerInteractionDTO.id = customerInteractionList1[0].Id;
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Object response=TestUtils.exec('MRO_LC_CustomerInteraction','deleteCustomerInteraction',jsonInput,true);
        List<CustomerInteraction__c> customerInteractionList2 = [
            SELECT Id
            FROM CustomerInteraction__c
        ];
        Test.startTest();
        system.assertEquals(customerInteractionList2.size() , customerInteractionList1.size()-1 );
        Test.stopTest();
    }

    @IsTest
    static void deleteCustomerInteractionTestIsBlankId() {
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        MRO_SRV_CustomerInteraction.CustomerInteractionDTO customerInteractionDTO = new MRO_SRV_CustomerInteraction.CustomerInteractionDTO();
        customerInteractionDTO.Id = '';
        customerInteractionDTO.relationId = accountContactRelation.Id;
        customerInteractionDTO.role = accountContactRelation.Roles;
        customerInteractionDTO.interactionId = interaction.Id;
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Test.startTest();
        try {
            Object response=TestUtils.exec('MRO_LC_CustomerInteraction','deleteCustomerInteraction',jsonInput,false);
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();

    }
}