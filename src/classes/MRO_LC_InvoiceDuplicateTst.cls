@IsTest
private class MRO_LC_InvoiceDuplicateTst {

    @TestSetup
    static void setup() {

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'MeterCheckEle', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'MeterCheckEle', recordTypeEle, '');
        insert phaseDI010ToRC010;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'InvoiceDuplicateTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        /*BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            // caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    static void testInit() {
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert businessAccount;

        Map<String, String> input = new Map<String, String>{
            'accountId' => businessAccount.Id
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InvoiceDuplicate','init', input,true);

        Test.stopTest();

        System.assertNotEquals(null, result);
//        System.assertEquals(1, result.size());
        System.assertNotEquals(null, result.get('dossier'));
        System.assertEquals(businessAccount.Id, ((Dossier__c)result.get('dossier')).Account__c);
    }

    @IsTest
    static void testFindContractAccount() {
        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        insert contractAccount;
        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setContractAccount(contractAccount.Id).build();
        insert supply;

        Map<String, String> input = new Map<String, String>{
            'supplyId' => supply.Id
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InvoiceDuplicate','findContractAccount', input,true);

        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.get('contractAccountId'));
        System.assertEquals(contractAccount.Id, result.get('contractAccountId'));
    }

    @IsTest
    static void testSaveCase() {
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert businessAccount;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setAccount(businessAccount.Id).build();
        insert dossier;
        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        insert contractAccount;

        Map<String, String> input = new Map<String, String>{
            'accountId' => businessAccount.Id,
            'contractAccountId' => contractAccount.Id,
            'dossierId' => dossier.Id,
            'invoiceIds' => JSON.serialize(new List<String>{'1', '2', '3'}),
            'channel' => 'channel1'
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InvoiceDuplicate','saveCase', input,true);

        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(1, result.size());

        String caseString = (String)result.get('case');
        System.assertNotEquals(null, caseString);
        System.assertNotEquals('', caseString);

        Case newCase = (Case)JSON.deserialize(caseString, Case.class);
        System.assertNotEquals(null, newCase);
        System.assertEquals(businessAccount.Id, newCase.AccountId);
        System.assertEquals(dossier.Id, newCase.Dossier__c);
        System.assertEquals('1,2,3', newCase.MarkedInvoices__c);
        System.assertEquals(contractAccount.Id, newCase.ContractAccount__c);
    }

    @IsTest
    static void testCancelProcess() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Map<String, String> input = new Map<String, String>{
            'dossierId' => dossier.Id
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InvoiceDuplicate','CancelProcess', input,true);
        system.assertEquals(true, result.get('error') == false );

        input = new Map<String, String>{
                'dossierId' => ''
        };
        try {
            Object response = TestUtils.exec(
                    'MRO_LC_InvoiceDuplicate', 'CancelProcess', input, false);
            result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }


        Test.stopTest();
    }
    @IsTest
    private static void updateDossierTest() {
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().build();
        insert dossier;
        String dossierId = [
                SELECT Id,SendingChannel__c
                FROM Dossier__c
                LIMIT 1
        ].Id;
        System.debug('#-# '+dossier);
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'channelSelected' => 'MyEnel',
                'originSelected' => 'Internal',
                'sendingChannel' => 'SMS'
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_InvoiceDuplicate', 'UpdateDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
 @IsTest
    private static void getValidDateTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'startDate' => ''+Date.today(),
                'endDate' => ''+Date.today() + 20
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_InvoiceDuplicate', 'getValidDate', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
}