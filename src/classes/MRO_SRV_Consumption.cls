public with sharing class MRO_SRV_Consumption {

    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_QR_Consumption consumptionQuery = MRO_QR_Consumption.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();

    public static MRO_SRV_Consumption getInstance() {
        return (MRO_SRV_Consumption)ServiceLocator.getInstance(MRO_SRV_Consumption.class);
    }

    public List<Consumption__c> upsertConsumption(List<Consumption__c> consumptionList) {
        for (Consumption__c consumption : consumptionList) {
            consumption.RecordTypeId = Schema.SObjectType.Consumption__c.getRecordTypeInfosByDeveloperName().get('Estimated').getRecordTypeId();
        }
        databaseSrv.upsertSObject(consumptionList);
        return consumptionList;
    }

    public List<Consumption__c> updateConsumptions(List<Consumption__c> consumptionList) {
        List<Consumption__c> output = consumptionList;
        if(consumptionList!=null && !consumptionList.isEmpty()) {
            try {
                databaseSrv.updateSObject(output);
            } catch (Exception e) {
                throw e;
            }
        }
        return output;
    }

    public void updateCasesConsumptions(Case myCase) {
        List<Consumption__c> consumptions = new List<Consumption__c>();

        Boolean oldConsumptionFlag = myCase.Contract__r.ConsumptionConventions__c;
        Boolean newConsumptionFlag = myCase.Contract2__r.ConsumptionConventions__c;
        System.debug('old flag ' + oldConsumptionFlag);
        System.debug('new flag ' + newConsumptionFlag);

        if (!oldConsumptionFlag && newConsumptionFlag) {
            List<Supply__c> supplies = supplyQuery.getSuppliesRateTypeByContractId(myCase.Contract__c);
            Consumption__c newConsumption = new Consumption__c();

            for (Supply__c supply : supplies) {
                Decimal estimatedValue=supply.ServicePoint__r.EstimatedConsumption__c;
                if(estimatedValue == null){
                    estimatedValue=0.0;
                }
                if (supply.RecordType.Name == constantsSrv.COMMODITY_GAS) {
                    Decimal value = estimatedValue;
                    newConsumption = initGasConsumption(supply.Id, value, constantsSrv.CONSUMPTION_STATUS_ACTIVATING, constantsSrv.CONSUMPTION_TYPE_SINGLE_RATE, myCase.Id);
                    consumptions.add(newConsumption);
                } else {
                    String rateType=supply.Product__r.CommercialProduct__r.RateType__c;
                    if(rateType == null){
                        rateType=constantsSrv.PRODUCT_RATE_TYPE_SINGLE;
                    }
                    if (rateType == constantsSrv.PRODUCT_RATE_TYPE_SINGLE) {
                        consumptions.add(this.initConsumptionWithTotal(supply.Id, estimatedValue, constantsSrv.CONSUMPTION_STATUS_ACTIVATING, constantsSrv.CONSUMPTION_TYPE_SINGLE_RATE, myCase.Id));
                    } else if (rateType == constantsSrv.PRODUCT_RATE_TYPE_DUAL) {
                        consumptions.addAll(this.initConsumptionDualWithTotal(supply.Id,estimatedValue, constantsSrv.CONSUMPTION_STATUS_ACTIVATING, myCase.Id));
                    }
                }

            }
        }

        if(!consumptions.isEmpty()){
            upsertConsumption(consumptions);
        }

    }

    public Consumption__c initConsumption(String supplyId, Decimal monthlyValue, String status,String type, String caseId){
        Consumption__c output = new Consumption__c();
        output.Supply__c = supplyId;
        output.Case__c=caseId;
        output.Status__c = status;
        output.Type__c=type;
        output.Commodity__c=constantsSrv.COMMODITY_ELECTRIC;
        output.QuantityJanuary__c = monthlyValue;
        output.QuantityFebruary__c = monthlyValue;
        output.QuantityMarch__c = monthlyValue;
        output.QuantityApril__c = monthlyValue;
        output.QuantityMay__c = monthlyValue;
        output.QuantityJune__c = monthlyValue;
        output.QuantityJuly__c = monthlyValue;
        output.QuantityAugust__c = monthlyValue;
        output.QuantitySeptember__c = monthlyValue;
        output.QuantityOctober__c = monthlyValue;
        output.QuantityNovember__c = monthlyValue;
        output.QuantityDecember__c = monthlyValue;
        return output;
    }

    public Consumption__c initGasConsumption(String supplyId, Decimal monthlyValue, String status,String type, String caseId){
        Consumption__c output = new Consumption__c();
        GasConsumptionRating__c gasRating = [
                SELECT Jan__c, Feb__c, Mar__c, Apr__c, May__c, Jun__c,
                        Jul__c, Aug__c, Sep__c,Oct__c, Nov__c, Dec__c
                FROM GasConsumptionRating__c
                LIMIT 1
        ];
        output.Supply__c = supplyId;
        output.Case__c=caseId;
        output.Commodity__c=constantsSrv.COMMODITY_GAS;
        output.Status__c = status;
        output.Type__c=type;
        output.QuantityJanuary__c = monthlyValue * gasRating.Jan__c;
        output.QuantityFebruary__c = monthlyValue * gasRating.Feb__c;
        output.QuantityMarch__c = monthlyValue * gasRating.Mar__c;
        output.QuantityApril__c = monthlyValue * gasRating.Apr__c;
        output.QuantityMay__c = monthlyValue * gasRating.May__c;
        output.QuantityJune__c = monthlyValue * gasRating.Jun__c;
        output.QuantityJuly__c = monthlyValue * gasRating.Jul__c;
        output.QuantityAugust__c = monthlyValue * gasRating.Aug__c;
        output.QuantitySeptember__c = monthlyValue * gasRating.Sep__c;
        output.QuantityOctober__c = monthlyValue * gasRating.Oct__c;
        output.QuantityNovember__c = monthlyValue * gasRating.Nov__c;
        output.QuantityDecember__c = monthlyValue * gasRating.Dec__c;
        return output;
    }


    public  List<Consumption__c> initConsumptionDualWithTotal (String supplyId, Decimal estimatedTotal, String status, String caseId){
        List<Consumption__c> dualConsumption = new List<Consumption__c>();
        Consumption__c dayConsumption = new Consumption__c();
        Consumption__c nightConsumption = new Consumption__c();
        dayConsumption.Supply__c = supplyId;
        dayConsumption.Case__c = caseId;
        dayConsumption.Status__c = status;
        dayConsumption.Type__c = constantsSrv.CONSUMPTION_TYPE_DAY_RATE;
        dayConsumption.Commodity__c=constantsSrv.COMMODITY_ELECTRIC;
        nightConsumption.Supply__c = supplyId;
        nightConsumption.Case__c = caseId;
        nightConsumption.Status__c = status;
        nightConsumption.Type__c  = constantsSrv.CONSUMPTION_TYPE_NIGHT_RATE;
        nightConsumption.Commodity__c = constantsSrv.COMMODITY_ELECTRIC;
        Integer decemberDay;
        Integer decemberNight;
        Integer monthlyValue = Integer.valueOf((estimatedTotal / 24).round(System.RoundingMode.DOWN));

        if(monthlyValue == 0){
            decemberDay = Integer.valueOf((estimatedTotal / 2).round(System.RoundingMode.DOWN));
            decemberNight = Integer.valueOf((estimatedTotal / 2).round(System.RoundingMode.CEILING));
        } else {
            decemberDay = Integer.valueOf((monthlyValue + ((estimatedTotal - (monthlyValue * 24)) / 2)).round(System.RoundingMode.DOWN));
            decemberNight = Integer.valueOf((monthlyValue + ((estimatedTotal - (monthlyValue * 24)) / 2)).round(System.RoundingMode.CEILING));
        }

        dayConsumption.QuantityJanuary__c = monthlyValue;
        dayConsumption.QuantityFebruary__c = monthlyValue;
        dayConsumption.QuantityMarch__c = monthlyValue;
        dayConsumption.QuantityApril__c = monthlyValue;
        dayConsumption.QuantityMay__c = monthlyValue;
        dayConsumption.QuantityJune__c = monthlyValue;
        dayConsumption.QuantityJuly__c = monthlyValue;
        dayConsumption.QuantityAugust__c = monthlyValue;
        dayConsumption.QuantitySeptember__c = monthlyValue;
        dayConsumption.QuantityOctober__c = monthlyValue;
        dayConsumption.QuantityNovember__c = monthlyValue;
        dayConsumption.QuantityDecember__c = decemberDay;

        nightConsumption.QuantityJanuary__c = monthlyValue;
        nightConsumption.QuantityFebruary__c = monthlyValue;
        nightConsumption.QuantityMarch__c = monthlyValue;
        nightConsumption.QuantityApril__c = monthlyValue;
        nightConsumption.QuantityMay__c = monthlyValue;
        nightConsumption.QuantityJune__c = monthlyValue;
        nightConsumption.QuantityJuly__c = monthlyValue;
        nightConsumption.QuantityAugust__c = monthlyValue;
        nightConsumption.QuantitySeptember__c = monthlyValue;
        nightConsumption.QuantityOctober__c = monthlyValue;
        nightConsumption.QuantityNovember__c = monthlyValue;
        nightConsumption.QuantityDecember__c = decemberNight;

        dualConsumption.add(dayConsumption);
        dualConsumption.add(nightConsumption);

        return dualConsumption;
    }

    public  Consumption__c initConsumptionWithTotal (String supplyId, Decimal estimatedTotal, String status,String type, String caseId){
        Consumption__c output = new Consumption__c();

        output.Supply__c = supplyId;
        output.Case__c=caseId;
        output.Status__c = status;
        output.Type__c=type;
        output.Commodity__c=constantsSrv.COMMODITY_ELECTRIC;
        Integer monthlyValue = Integer.valueOf((estimatedTotal / 12).round(System.RoundingMode.DOWN));
        output.QuantityJanuary__c = monthlyValue;
        output.QuantityFebruary__c = monthlyValue;
        output.QuantityMarch__c = monthlyValue;
        output.QuantityApril__c = monthlyValue;
        output.QuantityMay__c = monthlyValue;
        output.QuantityJune__c = monthlyValue;
        output.QuantityJuly__c = monthlyValue;
        output.QuantityAugust__c = monthlyValue;
        output.QuantitySeptember__c = monthlyValue;
        output.QuantityOctober__c = monthlyValue;
        output.QuantityNovember__c = monthlyValue;
        output.QuantityDecember__c = monthlyValue +Integer.valueOf((estimatedTotal - (monthlyValue * 12)).round(System.RoundingMode.CEILING));

        return output;
    }
}