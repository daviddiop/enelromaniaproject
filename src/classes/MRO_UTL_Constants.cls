public with sharing class MRO_UTL_Constants {

    @AuraEnabled
    public final String INTEGRATION_PROFILE_NAME = 'MRO - Integration';

    @AuraEnabled
    public final String DOSSIER_STARTED_BY_CUSTOMER_REQUEST = 'Customer Request';
    @AuraEnabled
    public final String DOSSIER_STARTED_BY_OPERATOR_PROPOSAL = 'Operator Proposal';

    @AuraEnabled
    public final String COMMODITY_ELECTRIC = 'Electric';
    @AuraEnabled
    public final String COMMODITY_GAS = 'Gas';
    @AuraEnabled
    public final String COMMODITY_COMBO = 'Combo';

    @AuraEnabled
    public final String CONSUMERTYPE_LARGE_BUSINESS = 'BusinessLarge';
    @AuraEnabled
    public final String CONSUMERTYPE_SMALL_BUSINESS = 'BusinessSmall';
    @AuraEnabled
    public final String CONSUMERTYPE_RESIDENTIAL = 'Residential';

    @AuraEnabled
    public final String CONTRACTTYPE_BUSINESS = 'Business';
    @AuraEnabled
    public final String CONTRACTTYPE_RESIDENTIAL = 'Residential';

    @AuraEnabled
    public final String ACCOUNT_RECORDTYPENAME_BUSINESS = 'Business';
    @AuraEnabled
    public final String ACCOUNT_RECORDTYPENAME_PERSON = 'Person';

    @AuraEnabled
    public final String CASE_START_PHASE = 'RE010';
    @AuraEnabled
    public final String CASE_SEND_TO_SAP_PHASE = 'BL010';
    @AuraEnabled
    public final String CASE_END_PHASE = 'RC010';

    @AuraEnabled
    public final String CASE_DISCO_PHASE = 'DI010';
    @AuraEnabled
    public final String CASE_PHASE_BL = 'BL010';
    @AuraEnabled
    public final String CASE_DEFAULT_CASE_ORIGIN = 'Phone';
    @AuraEnabled
    public final String CASE_STATUS_DRAFT = 'Draft';
    @AuraEnabled
    public final String CASE_STATUS_NEW = 'New';
    @AuraEnabled
    public final String CASE_STATUS_ONHOLD = 'On Hold';
    @AuraEnabled
    public final String CASE_STATUS_CANCELED = 'Canceled';
    @AuraEnabled
    public final String CASE_STATUS_EXECUTED = 'Executed';
    @AuraEnabled
    public final String CASE_REASON_TRADER_CHANGE = 'Trader change';

    @AuraEnabled
    public final String CASE_OUTCOME_JUSTIFIED = 'Justified';
    @AuraEnabled
    public final String CASE_OUTCOME_UNJUSTIFIED = 'Unjustified';
    @AuraEnabled
    public final String CASE_OUTCOME_PARTIALLY_JUSTIFIED = 'Partially justified';
    @AuraEnabled
    public final String CASE_OUTCOME_UNRESOLVABLE = 'Unresolvable';

    @AuraEnabled
    public final String DOSSIER_STATUS_DRAFT = 'Draft';
    @AuraEnabled
    public final String DOSSIER_STATUS_NEW = 'New';
    @AuraEnabled
    public final String DOSSIER_STATUS_WORKING = 'Working';
    @AuraEnabled
    public final String DOSSIER_STATUS_CANCELED = 'Canceled';
    @AuraEnabled
    public final String DOSSIER_STATUS_CLOSED = 'Closed';

    @AuraEnabled
    public final String DOSSIER_REQUESTTYPE_CONNECTION = 'Connection';
    @AuraEnabled
    public final String DOSSIER_REQUESTTYPE_METERCHANGE = 'MeterChange';
    @AuraEnabled
    public final String DOSSIER_REQUESTTYPE_TECHNICALDATACHANGE = 'TechnicalDataChange';
    @AuraEnabled
    public final String DOSSIER_REQUESTTYPE_METERCHECK = 'MeterCheck';

    @AuraEnabled
    public final String DOSSIER_PHASE_DOCUMENTSVERIFIED = 'DC040';
    @AuraEnabled
    public final String DOSSIER_PHASE_REQUEST_CLOSED = 'Request closed';

    @AuraEnabled
    public final String OPPORTUNITY_STAGE_QUALIFICATION = 'Qualification';
    @AuraEnabled
    public final String OPPORTUNITY_STAGE_PROPOSAL = 'Proposal';
    @AuraEnabled
    public final String OPPORTUNITY_STAGE_QUOTED = 'Quoted';
    @AuraEnabled
    public final String OPPORTUNITY_STAGE_WAIT_CONFIRMATION = 'Awaiting confirmation';
    @AuraEnabled
    public final String OPPORTUNITY_STAGE_CLOSED_WON = 'Closed Won';
    @AuraEnabled
    public final String OPPORTUNITY_STAGE_CLOSED_LOST = 'Closed Lost';

    @AuraEnabled
    public final String CAMPAIGN_TYPE_TACIT_RENEWAL = 'Tacit Renewal';

    @AuraEnabled
    public final Integer ZERO = 0;
    @AuraEnabled
    public final Integer INCREMENT_TO_ONE = 1;
    @AuraEnabled
    public final Integer DECREMENT_TO_ONE = -1;

    // ScripTemplate
    @AuraEnabled
    public final String CODE_SCRIPT_TEMPLATE = 'Default Code';

    //Customer Data Change
    @AuraEnabled
    public final String MODIFYING_CDC_PERMISSION = 'UserAllowedModifyData';
    @AuraEnabled
    public final String CUSTOM_PERMISSION_MDFY_ACC_NAME = 'MRO_Account_Name';
    @AuraEnabled
    public final String CUSTOM_PERMISSION_MDFY_VAT_NUMBER = 'MRO_VATNumber';
    @AuraEnabled
    public final String CUSTOM_PERMISSION_MDFY_CNP = 'MRO_National_Identity_Number';

    @AuraEnabled
    public final String CUSTOMER_DATA_CHANGE_RECORD_TYPE = 'Change';
    @AuraEnabled
    public final String  REQUEST_TYPE_CUSTOMER_DATA_CHANGE = 'CustomerDataChange';

    @AuraEnabled
    public final String  REQUEST_TYPE_CONTRACTUAL_DATA_CHANGE = 'Contractual Data Change';

    @AuraEnabled
    public final String  CASE_RECORD_TYPE_CONTRACTUAL_DATA_CHANGE = 'ContractualDataChange';

    @AuraEnabled
    public final String  REQUEST_TYPE_NLC_CLC_DATA_CHANGE = 'NLC-CLC Update';


    //sub process
    @AuraEnabled
    public final String SUB_PROCESS_PRODCHNG_SAMEMARKNEWCON_APINAME = 'ChangeProductWithinTheSameMarketPlusNewContract';
    @AuraEnabled
    public final String SUB_PROCESS_PRODCHNG_SAMEMARK_APINAME = 'ChangeProductWithinTheSameMarket';
    @AuraEnabled
    public final String SUB_PROCESS_PRODCHNG_FREETOREG_APINAME = 'ChangeMarketFromFreeToRegulated';
    @AuraEnabled
    public final String SUB_PROCESS_PRODCHNG_REGTOFREE_APINAME = 'ChangeMarketFromRegulatedToFree';

    @AuraEnabled
    public final String SUB_PROCESS_CHNG_ACC_NAME = 'Change Account Name';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_COMPANY_NAME = 'Change Company Name';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_CONTACT = 'Change Contact Data';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_CNP = 'Change CNP';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_CUI = 'Change CUI';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_ONRC = 'Change ONRC';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_CAEN = 'Change CAEN';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_CONTRACTED_QUANTITIES = 'Change Contracted Quantities';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_CONSUMPTION_CONVENTIONS = 'Change Consumption conventions';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_BILLING_FREQUENCY = 'Change Billing frequency';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_BILLING_CHANNEL = 'Change Billing sending channel';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_REFUND_METHOD = 'Change Refund method';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_BILLING_PERIOD = 'Change Billing period';
	@AuraEnabled
	public final String SUB_PROCESS_CHNG_INVOICE_ISSUE = 'Change invoice issuing method';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_CONTRACTUAL_VALIDITY = 'Change Contractual validity date';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_RESIDENTIAL_ADDRESS = 'Change Residential Address';
    @AuraEnabled
    public final String SUB_PROCESS_CHNG_OTHER_FIELDS= 'Change Other Fields';

    @AuraEnabled
    public final String SUB_PROCESS_SWIN_NONE= 'None';
    @AuraEnabled
    public final String CUSTOMER_PRODUCT_CHANGE = 'Customer Product Change';
    @AuraEnabled
    public final String DOSSIER_PRODUCT_CHANGE_ELE = 'Dossier Product Change Electric';
    @AuraEnabled
    public final String DOSSIER_PRODUCT_CHANGE_GAS = 'Dossier Product Change Gas';
    @AuraEnabled
    public final String SERVICE_POINT_PRODUCT_CHANGE = 'Service Point Product Change';

    // TaxSubsidy
    @AuraEnabled
    public final String TAXSUBSIDY_STATUS_ACTIVE = 'Active';
    @AuraEnabled
    public final String TAXSUBSIDY_STATUS_NOTACTIVE = 'Not Active';
    //Supply
    @AuraEnabled
    public final String SUPPLY_STATUS_ACTIVE = 'Active';
    @AuraEnabled
    public final String SUPPLY_STATUS_ACTIVATING = 'Activating';
    @AuraEnabled
    public final String SUPPLY_STATUS_TERMINATING = 'Terminating';
    @AuraEnabled
    public final String SUPPLY_STATUS_TERMINATED = 'Terminated';
    @AuraEnabled
    public final String SUPPLY_STATUS_NOTACTIVE = 'Not Active';

    //Contract
    @AuraEnabled
    public final String CONTRACT_STATUS_ACTIVATED = 'Activated';
    @AuraEnabled
    public final String CONTRACT_STATUS_DRAFT = 'Draft';
    @AuraEnabled
    public final String CONTRACT_STATUS_INAPPROVALPROCESS = 'In Approval Process';
    @AuraEnabled
    public final String CONTRACT_STATUS_TERMINATED = 'Terminated';
    @AuraEnabled
    public final String CONTRACT_STATUS_SIGNED = 'Signed';
    @AuraEnabled
    public final String CONTRACT_ACCOUNT_PAYMENT_TERM_DUE_10DAYS = 'Due Date 10 Days';
    @AuraEnabled
    public final String CONTRACT_ACCOUNT_PAYMENT_TERM_DUE_15DAYS = 'Due Date 15 Days';
    @AuraEnabled
    public final String CONTRACT_ACCOUNT_PAYMENT_TERM_DUE_30DAYS = 'Due Date 30 Days';
    @AuraEnabled
    public final String MARKET_FREE = 'Free';
    @AuraEnabled
    public final String MARKET_REGULATED = 'Regulated';
    @AuraEnabled
    public final String CONTRACT_ACCOUNT_PAYMENT_PENALTY_01_PL_0GD = '01-PL-0GD';
    @AuraEnabled
    public final String CONTRACT_ACCOUNT_PAYMENT_PENALTY_03_PL_15GD = '03-PL-15GD';
    @AuraEnabled
    public final String CONTRACT_ACCOUNT_PAYMENT_PENALTY_04_PL_30GD = '04-PL-30GD';


    //Contract Transfer
    @AuraEnabled
    public final String OPPORTUNITY_REQUEST_TYPE_TRANSFER = 'Transfer';
    @AuraEnabled
    public final Integer DIS_CO_NON_ENEL_ADDED_WORKING_DAYS = 10;
    @AuraEnabled
    public final Integer DIS_CO_ENEL_ADDED_WORKING_DAYS = 3;

    //Billing Profile
    @AuraEnabled
    public final Integer TRS_EE_ENEL_WEB_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsEEEnelWebWorkDays__c);
    @AuraEnabled
    public final Integer TRS_EE_NON_ENEL_WEB_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsEENonEnelWebWorkDays__c);
    @AuraEnabled
    public final Integer TRS_GAS_WEB_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsGASWebWorkDays__c);
    @AuraEnabled
    public final Integer TRS_EE_ENEL_SHOP_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsEEEnelShopWorkDays__c);
    @AuraEnabled
    public final Integer TRS_EE_NON_ENEL_SHOP_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsEENonEnelShopWorkDays__c);
    @AuraEnabled
    public final Integer TRS_GAS_SHOP_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsGASShopWorkDays__c);
    @AuraEnabled
    public final Integer TRS_EE_ENEL_PARTNER_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsEEEnelPartnerWorkDays__c);
    @AuraEnabled
    public final Integer TRS_EE_NON_ENEL_PARTNER_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsEENonEnelPartnerWorkDays__c);
    @AuraEnabled
    public final Integer TRS_GAS_PARTNER_WORK_DAYS= Integer.valueOf(StartDateSettings__c.getInstance().TrsGASPartnerWorkDays__c);
    @AuraEnabled
    public final Integer TRANSFER_DEFAULT_CONTRACT_TERM = 12;

    //Meter
    @AuraEnabled
    public final String INDUCTIVE_ENERGY = 'Energie Reactiva Inductva';
    @AuraEnabled
    public final String  ACTIVE_ENERGY = 'Energie Activa - Rata Unica';
    @AuraEnabled
    public final String CAPACITIVE_ENERGY = 'Energie Reactiva Capacitiva';

    //Billing Profile
    @AuraEnabled
    public final String BILLINGPROFILE_RECORDTYPE_GIRO = 'Direct Debit';
    @AuraEnabled
    public final String BILLING_PROFILE_REFUND_METHOD_BANK_TRANSFER = 'Bank Transfer';
    @AuraEnabled
    public final String BILLING_PROFILE_REFUND_METHOD_INVOICE = 'Invoice';
    @AuraEnabled
    public final String BILLING_PROFILE_DELIVERY_CHANNEL_EMAIL = 'Email';
    @AuraEnabled
    public final String BILLING_PROFILE_DELIVERY_CHANNEL_MAIL = 'Mail';
    @AuraEnabled
    public final String BILLING_PROFILE_DELIVERY_CHANNEL_SMS = 'SMS';

    //Privacy Change
    @AuraEnabled
    public final String PRIVACYCHANGE_STATUS_PENDING = 'Pending';
    @AuraEnabled
    public final String PRIVACYCHANGE_STATUS_ACTIVE = 'Active';
    @AuraEnabled
    public final String PRIVACYCHANGE_STATUS_OLD = 'Old';
    @AuraEnabled
    public final String PRIVACYCHANGE_STATUS_SKIPPED = 'Skipped';

    //Print Action
    @AuraEnabled
    public final String PRINTACTION_PHASE_NEW = 'New';
    @AuraEnabled
    public final String PRINTACTION_PHASE_PRINTING = 'Printing';
    @AuraEnabled
    public final String PRINTACTION_PHASE_DONE = 'Done';

    @AuraEnabled
    public final String REPRINT_TEMPLATE_CLASS = 'ArchiveProcessing';
    @AuraEnabled
    public final String REPRINT_TEMPLATE_CODE = 'ArchiveProduction';

    @AuraEnabled
    public final String SENDING_CHANNEL_EMAIL = 'Email';
	@AuraEnabled
    public final String SENDING_CHANNEL_MAIL = 'Mail';
	@AuraEnabled
    public final String SENDING_CHANNEL_SMS = 'SMS';
    @AuraEnabled
    public final String SENDING_CHANNEL_ARCHIVING = 'Archiving';
    @AuraEnabled
    public final String MAIL_CHANNEL_TYPE_RESIDENTIAL = 'Residential Address';
    @AuraEnabled
    public final String MAIL_CHANNEL_TYPE_BILLING = 'Billing Address';

    //Phase Manager
    @AuraEnabled
    public final String PHASE_MANAGER_INTEGRATION_CLASS='PhaseManagerIntegration';
    @AuraEnabled
    public final String PHASE_QRY_MAP_KEY_OBJECT='object';
    @AuraEnabled
    public final String PHASE_QRY_MAP_KEY_TYPE='type';
    @AuraEnabled
    public final String PHASE_QRY_MAP_KEY_TAGS='tags';
    @AuraEnabled
    public final String PHASE_TRANS_APPLY_MAP_KEY_OBJECT='object';
    @AuraEnabled
    public final String PHASE_TRANS_APPLY_MAP_KEY_TRANS='transition';
    @AuraEnabled
    public final String PHASE_AUTOMATIC_TRANSITION = 'A';
    @AuraEnabled
    public final String SERVICE_POINT_RETENTION = 'Service Point Retention';
    @AuraEnabled
    public final String CUSTOMER_RETENTION = 'Customer Retention';
    @AuraEnabled
    public final String DOSSIER_RETENTION_ELE = 'Dossier Retention Electric';
    @AuraEnabled
    public final String DOSSIER_RETENTION_GAS = 'Dossier Retention Gas';
    @AuraEnabled
    public final String RETENTION_CLOSED = 'Retention Closed';
    @AuraEnabled
    public final String RETENTION_CANCELLED = 'Retention cancelled';
    @AuraEnabled
    public final String PHASE_TRANSITION_SUCCESS = 'isSuccess';

    // Consumption
    @AuraEnabled
    public final String CONSUMPTION_STATUS_ACTIVE = 'Active';
    @AuraEnabled
    public final String CONSUMPTION_STATUS_ACTIVATING = 'Activating';
    @AuraEnabled
    public final String CONSUMPTION_STATUS_OLD = 'Old';
    @AuraEnabled
    public final String CONSUMPTION_TYPE_SINGLE_RATE = 'Single Rate';
    @AuraEnabled
    public final String CONSUMPTION_TYPE_DAY_RATE = 'Day Rate';
    @AuraEnabled
    public final String CONSUMPTION_TYPE_NIGHT_RATE = 'Night Rate';
    @AuraEnabled
    public final GasConsumptionRating__c CONSUMPTION_GAS_RATING= GasConsumptionRating__c.getInstance();

    //Document Validation
    @AuraEnabled
    public final String DOCUMENT_VALIDATION_TAG = 'DocumentsVerified';
    @AuraEnabled
    public final String CANCELLATION_TAG = 'Cancellation';
    @AuraEnabled
    public final String PRINT_DONE_TAG = 'PrintDone';
    @AuraEnabled
    public final String CONFIRM_TAG = 'Confirm';
    @AuraEnabled
    public final String DISCO_TAG = 'DisCo';

    //Document Bundle
    public final String DOCUMENTBUNDLE_RECIPIENT_TRADER = 'Trader';
    public final String DOCUMENTBUNDLE_RECIPIENT_DISTRIBUTOR = 'Distributor';
    public final String DOCUMENTBUNDLE_RECIPIENT_CUSTOMER = 'Customer';
    public final String DOCUMENTBUNDLE_RECORDTYPE_DEVELOPERNAME_OUTPUT = 'OutputBundle';

    //Document Type
    public final String DOCUMENTTYPE_DOCUMENT_TYPE_CODE_RENUNT_SCHIMB_FURN = 'RENUNT_SCHIMB_FURN';

    @AuraEnabled
    public final String ACTIVITY_ADDRESS_VERIFICATION_TYPE = 'Address Verification';
    @AuraEnabled
    public final String ACTIVITY_FILE_CHECK_TYPE = 'File Check';
    @AuraEnabled
    public final String ACTIVITY_DOCUMENT_VALIDATION_TYPE = 'Document Validation';
    @AuraEnabled
    public final String ACTIVITY_PRINT_TYPE = 'Print';
    @AuraEnabled
    public final String ACTIVITY_TOUCHPOINT_TYPE = 'TouchPoint';
    @AuraEnabled
    public final String ACTIVITY_TACIT_RENEWAL_TYPE = 'Tacit Renewal';
    @AuraEnabled
    public final String ACTIVITY_DISCARD_MANAGEMENT_TYPE = 'Discard';
    @AuraEnabled
    public final String ACTIVITY_DATA_ENTRY_TYPE = 'DataEntry';
    @AuraEnabled
    public final String ACTIVITY_CASE_RETENTION_TYPE = 'Service Point Retention';
    @AuraEnabled
    public final String ACTIVITY_DOSSIER_ELE_RETENTION_TYPE = 'Dossier Retention Ele';
    @AuraEnabled
    public final String ACTIVITY_DOSSIER_GAS_RETENTION_TYPE = 'Dossier Retention Gas';

    @AuraEnabled
    public final String ACTIVITY_STATUS_COMPLETED = 'Completed';
    @AuraEnabled
    public final String ACTIVITY_STATUS_CANCELLED = 'Cancelled';
    @AuraEnabled
    public final String ACTIVITY_STATUS_CLOSED_LOST = 'Closed Lost';
    @AuraEnabled
    public final String ACTIVITY_STATUS_CONFIRMED = 'Confirmed';
    @AuraEnabled
    public final String ACTIVITY_STATUS_CLOSED_WON = 'Closed Won';

    @AuraEnabled
    public final String PARTNER_SERVICE_VAS_ACTIVATION ='Activation of a new VAS';
    @AuraEnabled
    public final String PARTNER_SERVICE_VAS_TERMINATION ='Termination of an existing Soft VAS';
    @AuraEnabled
    public final String PARTNER_SERVICE_VAS_SUBSTITUTION ='Activation of a new VAS in substitution of an existing one';

    @AuraEnabled
    public final String OPPORTUNITY_SUB_PROCESS_CANCELLATION = 'Cancellation';
    @AuraEnabled
    public final String OPPORTUNITY_SUB_PROCESS_INSTANT_DELIVERY = 'Instant Delivery';
    @AuraEnabled
    public final String OPPORTUNITY_SUB_PROCESS_DISPATCHED_WITH_INSTALLATION = 'Dispatched with installation';
    @AuraEnabled
    public final String OPPORTUNITY_SUB_PROCESS_DISPATCHED_WITHOUT_INSTALLATION = 'Dispatched without installation';

    @AuraEnabled
    public final String REQUEST_TYPE_EXTERNAL_PRODUCT = 'ExternalProduct';
    @AuraEnabled
    public final String REQUEST_TYPE_PARTNER_SERVICE = 'PartnerService';

    /** START - TOUCH-POINT CONFIGURATION CONSTANTS
    /** Touch-Point custom metadata source field value constants */
    public final String TOUCHPOINTCONFIGURATION_SOURCE_CRM = 'CRM';
    public final String TOUCHPOINTCONFIGURATION_SOURCE_INTERNETINTERFACE = 'InternetInterface';
    /** Touch-Point custom metadata type field value constants */
    public final String TOUCHPOINTCONFIGURATION_TYPE_DOCUMENTUPLOAD = 'DocumentUpload';
    public final String TOUCHPOINTCONFIGURATION_TYPE_CONTRACTACCEPTANCE = 'ContractAcceptance';
    /** Touch-Point custom metadata sending channel field value constants */
    public final String TOUCHPOINTCONFIGURATION_SENDINGCHANNEL_EMAIL = 'Email';
    public final String TOUCHPOINTCONFIGURATION_SENDINGCHANNEL_SMS = 'SMS';
    public final String TOUCHPOINTCONFIGURATION_SENDINGCHANNEL_API = 'API';
    /** Document bundle codes */
    public final String DOCUMENTBUNDLECODE_IINEWREQUESTEMAIL = 'IINEWREQUESTEMAIL';
    public final String DOCUMENTBUNDLECODE_IIEMAILCONTRACTACCEPTANCE = 'IIEMAILCONTRACTACCEPTANCE';
    public final String DOCUMENTBUNDLECODE_IIDOCUMENTUPLOADREMINDEREMAIL = 'IIDOCUMENTUPLOADREMINDEREMAIL';
    public final String DOCUMENTBUNDLECODE_IIDOCUMENTUPLOADEXPIREDEMAIL = 'IIDOCUMENTUPLOADEXPIREDEMAIL';
    public final String DOCUMENTBUNDLECODE_IIDOCUMENTUPLOADCOMPLETEDEMAIL = 'IIDOCUMENTUPLOADCOMPLETEDEMAIL';
    public final String DOCUMENTBUNDLECODE_SWI_OUT_RETENTION_TRADER_OUT = 'SWI_OUT_RETENTION_TRADER_OUT'; //SP 20201214
    /** Custom Remider Configuration Codes */
    public final String CUSTOMREMCONFIGCODE_IIDOCUMENTUPLOADREMINDER = 'IIDocUplRem';
    public final String CUSTOMREMCONFIGCODE_IIDOCUMENTUPLOADTOKENEXPIRED = 'IIDocUplTokExp';
    public final String CUSTOMREMCONFIGCODE_IICONTRACTACCEPTANCEREMINDER = 'IIContrAcptRem';
    public final String CUSTOMREMCONFIGCODE_IICONTRACTACCEPTANCETOKENEXPIRED = 'IIContrAcptTokExp';
    /** END - TOUCH-POINT CONFIGURATION CONSTANTS */

    public final Integer PARTNER_SERVICE_SLA_EXPIRATION_OFFSET = 3;
    public final Integer EXTERNAL_PRODUCT_SLA_EXPIRATION_OFFSET = 5;

    //Product Catalog
    public final Boolean IS_BIT2WIN_CPQ_ENABLED = ProductCatalogSettings__c.getInstance().EnableBit2WinCPQ__c;
    @AuraEnabled
    public final String PRODUCT_RATE_TYPE_SINGLE = 'Single Rate';
    @AuraEnabled
    public final String PRODUCT_RATE_TYPE_DUAL= 'Dual Rate';

    public final Set<String> MULTIPOINT_PROCESSES_RECORDTYPE_DEVNAMES = new Set<String>{
        'Activation_ELE',
        'SwitchIn_ELE',
        'Transfer_ELE',
        'ProductChange_ELE',
        'NonDisconnectable_POD_ELE',
        'ContractMerge',
        'Termination_ELE',
        'DecoReco',
        'ContractualDataChange'
    };

    @AuraEnabled
    public final String CASEGROUP_STATUS_STACKING = 'Stacking';
    @AuraEnabled
    public final String CASEGROUP_STATUS_READY = 'Ready';
    @AuraEnabled
    public final String CASEGROUP_STATUS_COMPLETED = 'Completed';
    @AuraEnabled
    public final String CASEGROUP_STATUS_ERROR = 'Error';

    @AuraEnabled
    public final String CASETYPE_ACT_VAS = 'C123';
    @AuraEnabled
    public final String CASETYPE_REZ_VAS = 'C124';
    @AuraEnabled
    public final String CASETYPE_REFUND = 'C77';
    @AuraEnabled
    public final String CASETYPE_PHOTOVOLTAIC_ACQUISITION = 'C201';
    @AuraEnabled
    public final String CASETYPE_PHOTOVOLTAIC_CANCELLATION = 'C205';

    @AuraEnabled
    public final String PRODUCT_NAME_CONFORT = 'Confort';
    @AuraEnabled
    public final String PRODUCT_NAME_RELAXAT = 'Relaxat';
    @AuraEnabled
    public final String PRODUCT_NAME_ASIGURAT = 'Asigurat';

    @AuraEnabled
    public final String VAS_RELAXAT_CONFORT = 'VAS Relaxat / Confort';
    @AuraEnabled
    public final String VAS_ASIGURAT = 'VAS Asigurat';
    @AuraEnabled
    public final String VAS_GENERAL = 'General VAS';

    @AuraEnabled
    public final String PRODUCT_NAME_PLACEHOLDER = 'Product Name Placeholder';

    @AuraEnabled
    public final String BLANK_FIELD_PLACEHOLDER = '{BLANK}';

    @AuraEnabled
    public final String ASSET_STATUS_CANCELLED = 'Cancelled';
    @AuraEnabled
    public final String ASSET_STATUS_ACTIVE = 'Active';
    @AuraEnabled
    public final Integer START_DATE_OFFSET_IN_BUSINESS_DAYS = 5;

    @AuraEnabled
    public final String VAS = 'Service';
    @AuraEnabled
    public final String SALES_TYPE_HARD_VAS = 'Hard VAS';
    @AuraEnabled
    public final String SALES_TYPE_SOFT_VAS = 'Soft VAS';

    public static final String ELECTRIC = 'Electric';
    public static final String GAS = 'Gas';
    public static final String SERVICE = 'Service';

    //ORIGIN & CHANNEL
    public static final String ORIGIN_ENEL_EASY ='EnelEasy';
    public static final String ORIGIN_WEB ='Web';
    public static final String ORIGIN_CHAT ='Chat';
    public static final String ORIGIN_PHONE_IN ='Phone In';
    public static final String ORIGIN_PHONE_OUT ='Phone Out';
    public static final String ORIGIN_INTERNAL = 'Internal';

    public static final String CHANNEL_CALL_CENTER ='Call Center';
    public static final String CHANNEL_MY_ENEL ='MyEnel';
    public static final String CHANNEL_ENEL_RO ='Enel.ro';
    public static final String CHANNEL_BO_CUCA = 'Back Office Customer Care';

    public static final String ACQUISITION = 'Acquisition';
    public static final String GENERIC_REQUEST = 'GenericRequest';
    public static final String CHANGE = 'Change';
    public static final String CANCELLATION = 'Cancellation';

    public static final String ENELX_QUEUE = 'EnelX';

    public static Boolean isFirstRunBeforeDeleteCaseActivity = true;

    public final String SUBPROCESS_CONNECTION_PERMANENT = 'Permanent Connection';
    public final String SUBPROCESS_CONNECTION_TEMPORARY_DEFINITIVE_SOLUTION = 'Temporary Connection with Definitive Solution';
    public final String SUBPROCESS_CONNECTION_TEMPORARY_SOLUTION = 'Temporary Connection with Temporary Solution';

    public final String ACTIVITY_STATUS_NOT_STARTED = 'Not Started';

    //Address values
    @AuraEnabled
    public final List<Utils.KeyVal> streetTypes {
        get{
            List<Utils.KeyVal> values = new List<Utils.KeyVal>();
            for(Schema.PicklistEntry pckEntry : Account.ResidentialStreetType__c.getDescribe().getPicklistValues()){
                values.add(new Utils.KeyVal(pckEntry.getValue(),pckEntry.getLabel()));
            }
            return values;
        }
    }

    @AuraEnabled
    public final List<Utils.KeyVal> countries {
        get{
            List<Utils.KeyVal> values = new List<Utils.KeyVal>();
            for(Schema.PicklistEntry pckEntry : Account.ResidentialCountry__c.getDescribe().getPicklistValues()){
                values.add(new Utils.KeyVal(pckEntry.getValue(),pckEntry.getLabel()));
            }
            return values;
        }
    }

    /**
	 *  get OSIRecordTypes values from OpportunityServiceItem__c
	 */
    @AuraEnabled
    public final Map<String, String> OSIRecordTypes {
        get{
            Map<String, String> values = new Map<String, String>();
            Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName();
            values.put(ELECTRIC, recordTypes.get(ELECTRIC).getRecordTypeId());
            values.put(GAS, recordTypes.get(GAS).getRecordTypeId());
            values.put(SERVICE, recordTypes.get(SERVICE).getRecordTypeId());
            return values;
        }
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, String> getCaseRecordTypes(String type) {

        Map<String, String> values = new Map<String, String>();
        Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
        System.debug('type: ' + type);

        if ((type == 'PhotovoltaicPanels') || String.isBlank(type)) {
            values.put('PhotovoltaicPanels', caseRts.get('PhotovoltaicPanels').getRecordTypeId());
        }
        if ((type == 'ExternalProduct') || String.isBlank(type)) {
            values.put('Acquisition_Hard_VAS', caseRts.get('Acquisition_Hard_VAS').getRecordTypeId());
            values.put('Termination_Hard_VAS', caseRts.get('Termination_Hard_VAS').getRecordTypeId());
        }
        if ((type == 'PartnerService') || String.isBlank(type)) {
            values.put('Termination_SRV', caseRts.get('Termination_SRV').getRecordTypeId());
            values.put('Activation_SRV', caseRts.get('Activation_SRV').getRecordTypeId());
        }
        if ((type == 'Termination') || String.isBlank(type)) {
            values.put('Termination_ELE', caseRts.get('Termination_ELE').getRecordTypeId());
            values.put('Termination_GAS', caseRts.get('Termination_GAS').getRecordTypeId());
//          please contact @MovsarBekaev if you have to include the Termination_SRV in this list
//          -
//          values.put('Termination_SRV', caseRts.get('Termination_SRV').getRecordTypeId());
        }
        if ((type == 'Activation') || String.isBlank(type)) {
            values.put('Activation_ELE', caseRts.get('Activation_ELE').getRecordTypeId());
            values.put('Activation_GAS', caseRts.get('Activation_GAS').getRecordTypeId());
        }
        if ((type == 'SwitchIn') || String.isBlank(type)) {
            values.put('SwitchIn_ELE',caseRts.get('SwitchIn_ELE').getRecordTypeId());
            values.put('SwitchIn_GAS',caseRts.get('SwitchIn_GAS').getRecordTypeId());
        }
        if ((type == 'SwitchOut') || String.isBlank(type)) {
            values.put('SwitchOut_ELE',caseRts.get('SwitchOut_ELE').getRecordTypeId());
            values.put('SwitchOut_GAS',caseRts.get('SwitchOut_GAS').getRecordTypeId());
        }
        if ((type == 'DecoReco') || String.isBlank(type)) {
            values.put('DecoReco',caseRts.get('DecoReco').getRecordTypeId());
        }
        /* added by Giuseppe Mario Pastore 20/04/2020 */
        if ((type == 'InsolvencyBankruptcy') || String.isBlank(type)) {
            values.put('InsolvencyBankruptcy',caseRts.get('InsolvencyBankruptcy').getRecordTypeId());
        }
        /* added by Giuseppe Mario Pastore 20/04/2020 */
        if ((type == 'Transfer') || String.isBlank(type)) {
            values.put('Transfer_ELE',caseRts.get('Transfer_ELE').getRecordTypeId());
            values.put('Transfer_GAS',caseRts.get('Transfer_GAS').getRecordTypeId());
        }
        if ((type == 'Reactivation') || String.isBlank(type)) {
            values.put('Reactivation_ELE',caseRts.get('Reactivation_ELE').getRecordTypeId());
            values.put('Reactivation_GAS',caseRts.get('Reactivation_GAS').getRecordTypeId());
        }
        if ((type == 'Connection') || String.isBlank(type)) {
            values.put('Connection_ELE',caseRts.get('Connection_ELE').getRecordTypeId());
            values.put('Connection_GAS',caseRts.get('Connection_GAS').getRecordTypeId());
        }

        if ((type == 'BillingProfileChange') || String.isBlank(type)) {
            values.put('BillingProfileChange', caseRts.get('BillingProfileChange').getRecordTypeId());
        }
        if ((type == 'RegistryChange') || String.isBlank(type)) {
            values.put('RegistryChangeFast', caseRts.get('RegistryChangeFast').getRecordTypeId());
            values.put('RegistryChangeSlow', caseRts.get('RegistryChangeSlow').getRecordTypeId());
        }
        if ((type == 'TechnicalDataChange') || String.isBlank(type)) {
            values.put('TechnicalDataChange_ELE', caseRts.get('TechnicalDataChange_ELE').getRecordTypeId());
            values.put('TechnicalDataChange_GAS', caseRts.get('TechnicalDataChange_GAS').getRecordTypeId());
        }
        if ((type == 'MeterChange') || String.isBlank(type)) {
            values.put('MeterChangeEle', caseRts.get('MeterChangeEle').getRecordTypeId());
            values.put('MeterChangeGas', caseRts.get('MeterChangeGas').getRecordTypeId());
        }
        if ((type == 'ContractMerge') || String.isBlank(type)) {
            values.put('ContractMerge', caseRts.get('ContractMerge').getRecordTypeId());
        }
        if ((type == 'ProductChange') || String.isBlank(type)) {
            values.put('ProductChange_ELE',caseRts.get('ProductChange_ELE').getRecordTypeId());
            values.put('ProductChange_GAS',caseRts.get('ProductChange_GAS').getRecordTypeId());
        }
        if ((type == 'ContractualDataChange') || String.isBlank(type)) {
            // 23/04/2020 - INVALID RECORD TYPES - commented by Stefano Porcari
            //values.put('ContractualDataChange_ELE',caseRts.get('ContractualDataChange_ELE').getRecordTypeId());
            //values.put('ContractualDataChange_GAS',caseRts.get('ContractualDataChange_GAS').getRecordTypeId());
            values.put('ContractualDataChange', caseRts.get('ContractualDataChange').getRecordTypeId());

        }
        if ((type == 'MeterReading') || String.isBlank(type)) {
            values.put('Meter_Reading_ELE', caseRts.get('Meter_Reading_Ele').getRecordTypeId());
            values.put('Meter_Reading_GAS', caseRts.get('Meter_Reading_Gas').getRecordTypeId());
        }
        if ((type == 'MeterCheck') || String.isBlank(type)) {
            values.put('MeterCheckEle', caseRts.get('MeterCheckEle').getRecordTypeId());
            values.put('MeterCheckGas', caseRts.get('MeterCheckGas').getRecordTypeId());
        }
        if ((type == 'DemonstratedPayment') || String.isBlank(type)) {
            values.put('DemonstratedPayment', caseRts.get('DemonstratedPayment').getRecordTypeId());
        }
        if ((type == 'TaxChange') || String.isBlank(type)) {
            values.put('TaxChange', caseRts.get('Tax_Change').getRecordTypeId());
        }
        if ((type == 'NonDisconnectablePOD') || String.isBlank(type)) {
            values.put('NonDisconnectablePOD_Ele', caseRts.get('NonDisconnectable_POD_ELE').getRecordTypeId());
            values.put('NonDisconnectablePOD_Gas', caseRts.get('NonDisconnectable_POD_GAS').getRecordTypeId());
        }
        //FF Self Reading #WP3-P2
        if ((type == 'SelfReading') || String.isBlank(type)) {
            values.put('SelfReading_ELE', caseRts.get('SelfReading_ELE').getRecordTypeId());
            values.put('SelfReading_GAS', caseRts.get('SelfReading_GAS').getRecordTypeId());
        }
        //FF Self Reading #WP3-P2
        if ((type == 'RepaymentRequest') || String.isBlank(type)) {
            values.put('RepaymentRequest', caseRts.get('RepaymentRequest').getRecordTypeId());
        }
        if ((type == 'CustomerDataChange') || String.isBlank(type)) {
            values.put('CustomerDataChange', caseRts.get('CustomerDataChange').getRecordTypeId());
        }

        if ((type == 'NLC-CLC Update') || String.isBlank(type)) {
            values.put('NLC_CLC_Update', caseRts.get('NLC_CLC_Update').getRecordTypeId());
        }
        if ((type == 'BailmentManagement') || String.isBlank(type)) {
            values.put('BailmentManagement', caseRts.get('BailmentManagement').getRecordTypeId());
        }
        if ((type == 'RefundRequest') || String.isBlank(type)) {
            values.put('RefundRequest', caseRts.get('RefundRequest').getRecordTypeId());
        }
        if ((type == 'ANRECompensation') || String.isBlank(type)) {
            values.put('ANRECompensation', caseRts.get('ANRECompensation').getRecordTypeId());
        }
        if ((type == 'GDPRRequest') || String.isBlank(type)) {
            values.put('GDPRRequest', caseRts.get('GDPRRequest').getRecordTypeId());
        }
        if ((type == 'Prosumers') || String.isBlank(type)) {
            values.put('Prosumers', caseRts.get('Prosumers').getRecordTypeId());
        }
        if ((type == 'CompensationForDamages') || String.isBlank(type)) {
            values.put('CompensationForDamages', caseRts.get('CompensationForDamages').getRecordTypeId());
        }
        if ((type == 'CustomerCompensation') || String.isBlank(type)) {
            values.put('CustomerCompensation', caseRts.get('CustomerCompensation').getRecordTypeId());
        }
        if ((type == 'ATRExtension') || String.isBlank(type)) {
            values.put('ATRExtension', caseRts.get('ATRExtension').getRecordTypeId());
        }
        if ((type == 'ActivationSoftVas')) {
            values.put('Activation_SRV', caseRts.get('Activation_SRV').getRecordTypeId());
        }
        if ((type == 'TerminationSoftVas')) {
            values.put('Termination_SRV', caseRts.get('Termination_SRV').getRecordTypeId());
        }
        if ((type == 'ActivationHardVas')) {
            values.put('Acquisition_Hard_VAS', caseRts.get('Acquisition_Hard_VAS').getRecordTypeId());
        }
        if ((type == 'TerminationHardVas')) {
            values.put('Termination_Hard_VAS', caseRts.get('Termination_Hard_VAS').getRecordTypeId());
        }
        if ((type == 'ServicePointAddressChange') || String.isBlank(type)) {
            values.put('ServicePointAddressChange',caseRts.get('ServicePointAddressChange').getRecordTypeId());
        }
        if ((type == 'BillingProfileAddressChange') || String.isBlank(type)) {
            values.put('BillingProfileAddressChange',caseRts.get('BillingProfileAddressChange').getRecordTypeId());
        }
        if ((type == 'BankDirectDebit') || String.isBlank(type)) {
            values.put('BankDirectDebit', caseRts.get('BankDirectDebit').getRecordTypeId());
        }
        return values;
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, String> getDossierRecordTypes(String type) {
        Map<String, String> values = new Map<String, String>();
        Map<String, Schema.RecordTypeInfo> dossierRts = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName();
        System.debug('type: ' + type);

        if ((type == 'Acquisition') || String.isBlank(type)) {
            values.put('Acquisition', dossierRts.get('Acquisition').getRecordTypeId());
        }
        if ((type == 'Connection') || String.isBlank(type)) {
            values.put('Connection', dossierRts.get('Connection').getRecordTypeId());
        }
        if ((type == 'Change') || String.isBlank(type)) {
            values.put('Change', dossierRts.get('Change').getRecordTypeId());
        }
        if ((type == 'Change') || String.isBlank(type)) {
            values.put('Change', dossierRts.get('Change').getRecordTypeId());
        }
        if ((type == 'Prosumers') || String.isBlank(type)) {
            values.put('Prosumers', dossierRts.get('Prosumers').getRecordTypeId());
        }
        if ((type == 'Disconnection') || String.isBlank(type)) {
            values.put('Disconnection', dossierRts.get('Disconnection').getRecordTypeId());
        }

        return values;
    }

    public Integer billingFrequencyToNumberOfMonths(String billingFrequency){
        if (billingFrequency == 'Monthly'){
            return 1;
        }

        if (billingFrequency == 'Bimonthly' || billingFrequency == 'Bimonthly-Odd' || billingFrequency == 'Bimonthly-Even'){
            return 2;
        }

        if (billingFrequency == 'Quarterly'){
            return 4;
        }

        System.debug('wrong billing frequency');
        return 0;
    }

    @AuraEnabled(cacheable=true)
    public static MRO_UTL_Constants getAllConstants() {
        return (MRO_UTL_Constants) ServiceLocator.getInstance(MRO_UTL_Constants.class);
    }
}