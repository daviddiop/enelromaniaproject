/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 05, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_FieldsTemplate {

    public static MRO_QR_FieldsTemplate getInstance() {
        return (MRO_QR_FieldsTemplate) ServiceLocator.getInstance(MRO_QR_FieldsTemplate.class);
    }

    public FieldsTemplate__c getById(Id fieldsTemplateId) {
        return [SELECT Name, Code__c, RootObjectType__c FROM FieldsTemplate__c WHERE Id = :fieldsTemplateId];
    }

    public FieldsTemplate__c getByCode(String fieldsTemplateCode) {
        return [SELECT Name, Code__c, RootObjectType__c FROM FieldsTemplate__c WHERE Code__c = :fieldsTemplateCode LIMIT 1];
    }

    public Map<String, FieldsTemplate__c> getByCodes(Set<String> fieldsTemplateCodes) {
        List<FieldsTemplate__c> templates = [SELECT Name, Code__c, RootObjectType__c FROM FieldsTemplate__c WHERE Code__c IN :fieldsTemplateCodes];
        Map<String, FieldsTemplate__c> result = new Map<String, FieldsTemplate__c>();
        for (FieldsTemplate__c template : templates) {
            result.put(template.Code__c, template);
        }
        return result;
    }

    public List<FieldsTemplateSection__c> listSectionsWithDynamicFieldsByTemplateId(Id fieldsTemplateId) {
        return [SELECT Title__c, Columns__c, Order__c, ObjectType__c, ParentRelationPath__c, Template__c, RecordType.DeveloperName, FieldsPrefix__c,
                       (SELECT FieldName__c, Label__c, Order__c, IsReadonly__c, IsRequired__c, IsDisabled__c, InheritsFromField__c, Section__c
                        FROM DynamicFields__r ORDER BY Order__c)
                FROM FieldsTemplateSection__c
                WHERE Template__c = :fieldsTemplateId
                ORDER BY Order__c];
    }
}