/**
 * @author  Stefano Porcari
 * @since   Jan 15, 2020
 * @desc   Test for MRO_LC_ValidatableDocumentsCnt class
 * 
 */
@IsTest
private class MRO_LC_ValidatableDocumentsCntTst {
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static  String phase1Code='rrn';
    private static  String phase2Code='rrc';
    @testSetup
    static void setup() {
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision; 

        wrts_prcgvr__Phase__c phase1=MRO_UTL_TestDataFactory.Phase().setPhaseCode(phase1Code).createPhaseBuilder().build();
        insert phase1;

        wrts_prcgvr__Phase__c phase2=MRO_UTL_TestDataFactory.Phase().setPhaseCode(phase2Code).createPhaseBuilder().build();
        insert phase2;

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).setRecordType().build();
        dossier.Phase__c=phase1.Id;
        insert dossier;
        

        Case case1=MRO_UTL_TestDataFactory.caseRecordBuilder().newCase().build();
        case1.Phase__c=phase1.Id;
        case1.Dossier__c=dossier.Id;
        insert case1;

        Case case2=MRO_UTL_TestDataFactory.caseRecordBuilder().newCase().build();
        case2.Phase__c=phase1.Id;
        case2.Dossier__c=dossier.Id;
        insert case2;

        FileMetadata__c fileMetadata = MRO_UTL_TestDataFactory.FileMetadata().build();
        fileMetadata.Dossier__c = dossier.Id;
        insert fileMetadata;

        insert MRO_UTL_TestDataFactory.FileMetadata().build();

        DocumentType__c documentType01 = MRO_UTL_TestDataFactory.DocumentType().createDocumentType('document Type Test').build();
        insert documentType01;

        DocumentType__c documentType02 = MRO_UTL_TestDataFactory.DocumentType().createDocumentType('document Type Test validated').build();
        insert documentType02;

        DocumentBundle__c bundle = new DocumentBundle__c();
        bundle.Name = 'Doc bundle test';
        insert bundle;

        List<DocumentItem__c> listDocumentItems = new List<DocumentItem__c>();
        for (Integer i = 0; i < 10; i++) {
            DocumentItem__c documentItem = MRO_UTL_TestDataFactory.DocumentItem().createBulkDocumentItem(i).build();
            documentItem.DocumentType__c = i > 5 ? documentType01.Id : documentType02.Id;
            listDocumentItems.add(documentItem);
        }
        insert listDocumentItems;

        DocumentBundleItem__c docBundleItem01 = MRO_UTL_TestDataFactory.DocumentBundleItem().createDocumentBundleItem(bundle.Id,documentType01.Id,true).build();
        insert docBundleItem01;

        DocumentBundleItem__c docBundleItem02 = MRO_UTL_TestDataFactory.DocumentBundleItem().createDocumentBundleItem(bundle.Id,documentType02.Id,false).build();
        insert docBundleItem02;

        ValidatableDocument__c validatableDocument01 = MRO_UTL_TestDataFactory.ValidatableDocument().createValidatableDocument(dossier.Id
                ,docBundleItem01.Id,null).build();
        insert validatableDocument01;

        ValidatableDocument__c validatableDocument02 = MRO_UTL_TestDataFactory.ValidatableDocument().createValidatableDocument(dossier.Id
                ,docBundleItem02.Id,fileMetadata.Id).build();
        insert validatableDocument02;



        wrts_prcgvr.InstallIntegration.install();
        wrts_prcgvr__PhaseManagerSObjectSetting__c dossierSetting=
         new wrts_prcgvr__PhaseManagerSObjectSetting__c(
            Name = 'Dossier',
            wrts_prcgvr__ObjectType__c = 'Dossier__c',
            wrts_prcgvr__PivotField__c = 'Phase__c',
            wrts_prcgvr__TypeField__c = 'RecordTypeId'
        );
        insert dossierSetting;
        insert new wrts_prcgvr__PhaseManagerSObjectSetting__c(
            Name = 'Case',
            wrts_prcgvr__ObjectType__c = 'Case',
            wrts_prcgvr__PivotField__c = 'Phase__c',
            wrts_prcgvr__TypeField__c = 'RecordTypeId'
        );

        wrts_prcgvr__PhaseTransition__c ptDossier = new wrts_prcgvr__PhaseTransition__c(
        wrts_prcgvr__Code__c = Math.random()+'',
        wrts_prcgvr__DestinationPhase__c = phase2.Id,
        wrts_prcgvr__OriginPhase__c = phase1.Id,
        wrts_prcgvr__RecordTypeId__c = dossier.RecordTypeId,
        wrts_prcgvr__RecordTypeName__c = 'Dossier__c',
        wrts_prcgvr__Tags__c=constantsSrv.DOCUMENT_VALIDATION_TAG,
        wrts_prcgvr__Type__c = 'A'
        );
        insert ptDossier;
        wrts_prcgvr__PhaseTransitionDetail__c ptdDossier = new wrts_prcgvr__PhaseTransitionDetail__c(
                wrts_prcgvr__PhaseTransition__c = ptDossier.Id,
                wrts_prcgvr__Type__c = 'Action'
        );
        insert ptdDossier;

        wrts_prcgvr__PhaseTransition__c ptCase = new wrts_prcgvr__PhaseTransition__c(
        wrts_prcgvr__Code__c = Math.random()+'',
        wrts_prcgvr__DestinationPhase__c = phase2.Id,
        wrts_prcgvr__OriginPhase__c = phase1.Id,
        wrts_prcgvr__RecordTypeId__c = case1.RecordTypeId,
        wrts_prcgvr__RecordTypeName__c = 'Case',
        wrts_prcgvr__Tags__c=constantsSrv.DOCUMENT_VALIDATION_TAG,
        wrts_prcgvr__Type__c = 'A'
        );
        insert ptCase;
        wrts_prcgvr__PhaseTransitionDetail__c ptdCase = new wrts_prcgvr__PhaseTransitionDetail__c(
                wrts_prcgvr__PhaseTransition__c = ptCase.Id,
                wrts_prcgvr__Type__c = 'Action'
        );
        insert ptdCase;
    



        }

    @isTest
    static void getFileMetadataAndValidatableDocumentsOk() {
        String fileMetadataId = [
            SELECT Id 
            FROM FileMetadata__c 
            WHERE Dossier__c != null
        ].Id;
        MRO_LC_ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments getValidatableDocumentsData = new MRO_LC_ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        Test.startTest();
        getValidatableDocumentsData.perform(jsonInput);
        Test.stopTest();
    }

    @isTest
    static void getFileMetadataAndValidatableDocumentsKO1() {
        MRO_LC_ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments getValidatableDocumentsData = new MRO_LC_ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => ''
        });
        try {
            Test.startTest();
            getValidatableDocumentsData.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Filemetadata is required ?  ' + exc.getMessage());
        }
    }

    @isTest
    static void getFileMetadataAndValidatableDocumentsKO2() {
        String fileMetadataId = [
            SELECT Id 
            FROM FileMetadata__c 
            WHERE Dossier__c = null
        ].Id;
        MRO_LC_ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments getValidatableDocumentsData =
                new MRO_LC_ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        try {
            Test.startTest();
            getValidatableDocumentsData.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Dossier is required ?  ' + exc.getMessage());
        }
    }

    @isTest
    static void doneValidatableDocumentsWithEmptyFilemetadata() {
        MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments doneValidatableDocuments = new MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => ''
        });
        try {
            Test.startTest();
            doneValidatableDocuments.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Filemetadata is required ?  ' + exc.getMessage());
        }
    }

    @isTest
    static void doneValidatableDocumentsWithEmptyDossier() {
        String fileMetadataId = [
            SELECT Id 
            FROM FileMetadata__c 
            WHERE Dossier__c = null
        ].Id;
        MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments doneValidatableDocuments =
                new MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        try {
            Test.startTest();
            doneValidatableDocuments.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Dossier is required ?  ' + exc.getMessage());
        }
    }
    @isTest
    static void doneValidatableDocumentsWithArchivedDocument() {
        FileMetadata__c fileMetadata = [
            SELECT Id,DiscoFileId__c
            FROM FileMetadata__c
            WHERE Dossier__c != NULL
            LIMIT 1
        ];
        fileMetadata.DiscoFileId__c = null;
        update fileMetadata;
        DocumentBundleItem__c documentBundleItem = [
            SELECT Id,ArchiveOnDisCo__c
            FROM DocumentBundleItem__c
            LIMIT  1
        ];
        documentBundleItem.ArchiveOnDisCo__c = true;
        update  documentBundleItem;

        ValidatableDocument__c vd = [
            SELECT Id FROM ValidatableDocument__c
            LIMIT 1
        ];
        vd.FileMetadata__c = fileMetadata.Id;
        vd.DocumentBundleItem__c = documentBundleItem.Id;
        update vd;


        MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments doneValidatableDocuments =
                new MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadata.Id
        });
        try {
            Test.startTest();
            doneValidatableDocuments.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Dossier is required ?  ' + exc.getMessage());
        }
    }

    @isTest
    static void doneValidatableDocumentsForDossierWithOpenDocuments() {
        FileMetadata__c file= [
            SELECT Id , Dossier__c
            FROM FileMetadata__c 
            WHERE Dossier__c != null
        ];
        String fileMetadataId=file.Id;

        Dossier__c dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];
 
        MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments doneValidatableDocuments = new MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        try {
            Test.startTest();
            doneValidatableDocuments.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Dossier still has opened non validatable documents ?  ' + exc.getMessage());
        }
    }


    @isTest
    static void doneValidatableDocumentsForDossierWithClosedDocuments() {
        FileMetadata__c file= [
            SELECT Id , Dossier__c
            FROM FileMetadata__c 
            WHERE Dossier__c != null
        ];
        String fileMetadataId=file.Id;
        Dossier__c dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c 
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];
       
        wrts_prcgvr__Activity__c checkActivty= new wrts_prcgvr__Activity__c(Type__c=constantsSrv.ACTIVITY_FILE_CHECK_TYPE,wrts_prcgvr__ObjectId__c=file.Id,Dossier__c=file.Dossier__c,wrts_prcgvr__Phase__c=dossier.Phase__c);
        upsert checkActivty;

        CasePhaseUpdate__c casePhaseUpdate= new CasePhaseUpdate__c(MaxRecords__c=2);
        upsert casePhaseUpdate;

        List<ValidatableDocument__c> documents=[
                SELECT Id,DocumentBundleItem__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossier.Id AND FileMetadata__c= NULL
        ];

        for (ValidatableDocument__c document: documents){
            document.FileMetadata__c=file.Id;
        }

        upsert documents;
        MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments doneValidatableDocuments = new MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        Test.startTest();
        doneValidatableDocuments.perform(jsonInput);
        List<wrts_prcgvr__Activity__c> activities=  [
            SELECT Id, wrts_prcgvr__Status__c, wrts_prcgvr__IsClosed__c
            FROM wrts_prcgvr__Activity__c
            WHERE  Dossier__c =: dossier.Id
        ];
        for (wrts_prcgvr__Activity__c activity: activities){
            System.assertEquals(activity.wrts_prcgvr__Status__c,constantsSrv.ACTIVITY_STATUS_COMPLETED);
        }
        wrts_prcgvr__Phase__c phase = [
            SELECT Id
            FROM wrts_prcgvr__Phase__c
            WHERE  wrts_prcgvr__Code__c =: phase2Code
        ];
        dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c 
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];
        System.assertEquals(dossier.Phase__c, phase.Id);
        List<Case> cases= [
            SELECT Id,   Phase__c 
            FROM Case
            WHERE Dossier__c = :dossier.Id
        ];
        for (Case checkedCase: cases){
            System.assertEquals(checkedCase.Phase__c, phase.Id);
        }
        Test.stopTest();
        
        
    }

    @isTest
    static void doneValidatableDocumentsForDossierWithClosedDocumentsEmptyDossierTransition() {
        FileMetadata__c file= [
            SELECT Id , Dossier__c
            FROM FileMetadata__c 
            WHERE Dossier__c != null
        ];
        String fileMetadataId=file.Id;
        Dossier__c dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c 
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];

       
        wrts_prcgvr__Activity__c checkActivty= new wrts_prcgvr__Activity__c(Type__c=constantsSrv.ACTIVITY_FILE_CHECK_TYPE,wrts_prcgvr__ObjectId__c=file.Id,Dossier__c=file.Dossier__c,wrts_prcgvr__Phase__c=dossier.Phase__c);
        upsert checkActivty;

        wrts_prcgvr__Phase__c phase2 = [
            SELECT Id
            FROM wrts_prcgvr__Phase__c
            WHERE  wrts_prcgvr__Code__c =: phase2Code
        ];

        dossier.Phase__c= phase2.Id;
        upsert dossier;

        CasePhaseUpdate__c casePhaseUpdate= new CasePhaseUpdate__c(MaxRecords__c=2);
        upsert casePhaseUpdate;

        List<ValidatableDocument__c> documents=[
                SELECT Id,DocumentBundleItem__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossier.Id AND FileMetadata__c= NULL
        ];

        for (ValidatableDocument__c document: documents){
            document.FileMetadata__c=file.Id;
        }

        upsert documents;
        MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments doneValidatableDocuments = new MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        Test.startTest();
        doneValidatableDocuments.perform(jsonInput);
        List<wrts_prcgvr__Activity__c> activities=  [
            SELECT Id, wrts_prcgvr__Status__c, wrts_prcgvr__IsClosed__c
            FROM wrts_prcgvr__Activity__c
            WHERE  Dossier__c =: dossier.Id
        ];
        for (wrts_prcgvr__Activity__c activity: activities){
            System.assertEquals(activity.wrts_prcgvr__Status__c,constantsSrv.ACTIVITY_STATUS_COMPLETED);
        }
        wrts_prcgvr__Phase__c phase1 = [
            SELECT Id
            FROM wrts_prcgvr__Phase__c
            WHERE  wrts_prcgvr__Code__c =: phase1Code
        ];
        dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c 
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];
        System.assertEquals(dossier.Phase__c, phase2.Id);
        List<Case> cases= [
            SELECT Id,   Phase__c 
            FROM Case
            WHERE Dossier__c = :dossier.Id
        ];
        for (Case checkedCase: cases){
            System.assertEquals(checkedCase.Phase__c, phase1.Id);
        }
        Test.stopTest();
        
        
    }

    @isTest
    static void doneValidatableDocumentsForDossierWithClosedDocumentsUsingBatch() {
        FileMetadata__c file= [
            SELECT Id , Dossier__c
            FROM FileMetadata__c 
            WHERE Dossier__c != null
        ];
        String fileMetadataId=file.Id;

        Dossier__c dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];
        
        wrts_prcgvr__Activity__c checkActivty= new wrts_prcgvr__Activity__c(Type__c=constantsSrv.ACTIVITY_FILE_CHECK_TYPE,wrts_prcgvr__ObjectId__c=file.Id,Dossier__c=file.Dossier__c,wrts_prcgvr__Phase__c=dossier.Phase__c);
        upsert checkActivty;

        CasePhaseUpdate__c casePhaseUpdate= new CasePhaseUpdate__c(MaxRecords__c=1);
        upsert casePhaseUpdate;

        List<ValidatableDocument__c> documents=[
                SELECT Id,DocumentBundleItem__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossier.Id AND FileMetadata__c= NULL
        ];

        for (ValidatableDocument__c document: documents){
            document.FileMetadata__c=file.Id;
        }

        upsert documents;

        try {

        MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments doneValidatableDocuments = new MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        
        Test.startTest();
        doneValidatableDocuments.perform(jsonInput);
        List<wrts_prcgvr__Activity__c> activities=  [
            SELECT Id, wrts_prcgvr__Status__c, wrts_prcgvr__IsClosed__c
            FROM wrts_prcgvr__Activity__c
            WHERE  Dossier__c =: dossier.Id
        ];
        for (wrts_prcgvr__Activity__c activity: activities){
            System.assertEquals(activity.wrts_prcgvr__Status__c,constantsSrv.ACTIVITY_STATUS_COMPLETED);
        }
        wrts_prcgvr__Phase__c phase1 = [
            SELECT Id
            FROM wrts_prcgvr__Phase__c
            WHERE  wrts_prcgvr__Code__c =: phase1Code
        ];

        wrts_prcgvr__Phase__c phase2 = [
            SELECT Id
            FROM wrts_prcgvr__Phase__c
            WHERE  wrts_prcgvr__Code__c =: phase2Code
        ];
        dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c 
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];
        System.assertEquals(dossier.Phase__c, phase2.Id);
        List<Case> cases= [
            SELECT Id,   Phase__c 
            FROM Case
            WHERE Dossier__c = :dossier.Id
        ];
        
        System.assert(true);

        
        Test.stopTest();

        }catch (Exception e){
            System.assert(true,e.getMessage());

        }


    }

    @IsTest
    public static void validateDocumentTest(){
        FileMetadata__c file= [
            SELECT Id , Dossier__c
            FROM FileMetadata__c
            WHERE Dossier__c != null
        ];
        String fileMetadataId=file.Id;
        Dossier__c dossier=  [
            SELECT Id,  Status__c, RecordTypeId, Phase__c
            FROM Dossier__c
            WHERE Id = :file.Dossier__c
        ];

        List<ValidatableDocument__c> documents=[
            SELECT Id,DocumentBundleItem__c
            FROM ValidatableDocument__c
            WHERE Dossier__c = :dossier.Id AND FileMetadata__c= NULL
        ];
        DocumentItem__c documentItem=[
            SELECT Id,Name, Mandatory__c, ValidatableDocumentField__c,
                ValidatableDocParentField__c, CustomValidationLevel__c,
                CustomValidationMethod__c
            FROM DocumentItem__c
            LIMIT 1
        ];

        List<MRO_SRV_DocumentItem.DocumentItemDTO> documentItemsDTO = new List<MRO_SRV_DocumentItem.DocumentItemDTO>();
        MRO_SRV_DocumentItem.DocumentItemDTO docDTO = MRO_SRV_DocumentItem.getInstance().mapToDto((documentItem));
        docDTO.customValidationLevel = 'Error';
        docDTO.customValidationMethod = 'checkSwitchNotificationDate';
        documentItemsDTO.add(docDTO);
        Map<String, String > inputJSON = new Map<String, String>{
            'fileMetadataId' => fileMetadataId,
            'confirmed' => JSON.serialize(true),
            'validatableDocument' => JSON.serialize(documents[0]),
            'documentItemsDTO' => JSON.serialize(documentItemsDTO)
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_ValidatableDocumentsCnt', 'validateDocument', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('message') != null);
        Test.stopTest();
    }

}