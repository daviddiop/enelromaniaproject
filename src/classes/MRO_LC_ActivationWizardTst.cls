/**
 * Created by Boubacar Sow on 13/07/2020.
 */

@IsTest
private class MRO_LC_ActivationWizardTst {

    @TestSetup
    static void setup() {

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        /*User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;
        update user;*/

        List<Account> listAccount = new list<Account>();
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().businessAccount().build());
        insert listAccount;

        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[0].Id;
        insert contact;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        contract.ContractType__c = 'Business';
        insert contract;


        Opportunity opportunity = MRO_UTL_TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        opportunity.ContractId = contract.Id;
        insert opportunity;

        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;


        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;

        OpportunityServiceItem__c opportunityServiceItem = MRO_UTL_TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        insert opportunityServiceItem;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[0].Id;
        insert billingProfile;

        Product2 product2 =MRO_UTL_TestDataFactory.product2().build();
        insert product2;

        PricebookEntry pricebookEntry = MRO_UTL_TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = product2.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = MRO_UTL_TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityID = opportunity.Id;
        opportunityLineItem.Product2ID = product2.Id;
        opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = 'E0120200617144259786';
        insert servicePoint;
        /*BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Connection').getRecordTypeId();
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[0].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            // caseRecord.BillingProfile__c = billingProfile.Id;
//            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Connection_ELE').getRecordTypeId();
            caseRecord.ServicePointCode__c = 'E0120200617144259786';
            caseList.add(caseRecord);
        }
        insert caseList;

        Consumption__c consumption = MRO_UTL_TestDataFactory.consumption().createConsumption()
            .setAccount(listAccount[0].Id)
            .setSupply(supplyList[0].Id)
            .setCase(caseList[0].Id).build();
        insert consumption;


    }
    
    @IsTest
    public static void initializeTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id,Name
            FROM Dossier__c
            LIMIT 1
        ];
        System.debug('### dossier '+dossier);
        CustomerInteraction__c customerInteraction = [
            SELECT Id,Interaction__c
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id,StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => opp.Id,
            'interactionId' => customerInteraction.Interaction__c,
            'connectionDossierId' => dossier.Id
        };


        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('opportunityId') == opp.Id);
    }

    @IsTest
    public static void initializeEmptyOpportunityIdTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        String opportinutyId = '';
        CustomerInteraction__c customerInteraction = [
            SELECT Id,Interaction__c
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => opportinutyId,
            'interactionId ' => customerInteraction.Interaction__c
        };

        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ].Id;
        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivisionId;
        update user;
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('opportunityId') != opportinutyId);
    }

    @IsTest
    public static void initializeExceptionTest(){
        CustomerInteraction__c customerInteraction = [
            SELECT Id,Interaction__c
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id,StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '',
            'opportunityId' => opp.Id,
            'interactionId' => customerInteraction.Interaction__c
        };
        Test.startTest();
        try {
            TestUtils.exec('MRO_LC_ActivationWizard', 'initialize', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }


    @IsTest
    static void updateOsiListTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
            SELECT Id,ServicePointCode__c
            FROM OpportunityServiceItem__c
            LIMIT 10
        ];

        MRO_LC_ActivationWizard.InputData inputData = new MRO_LC_ActivationWizard.InputData();
        inputData.opportunityServiceItems = listOppServiceItem;

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateOsiList', inputData, true);
        Test.stopTest();

        System.assertEquals(true, response != null);
    }

    @IsTest
    static void updateTraderToOsiListTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
            SELECT Id,ServicePointCode__c
            FROM OpportunityServiceItem__c
            LIMIT 10
        ];
        String companyId = [
            SELECT Id
            FROM CompanyDivision__c
        ].Id;


        MRO_LC_ActivationWizard.UpdateTraderToOsiListInput inputData = new MRO_LC_ActivationWizard.UpdateTraderToOsiListInput();
        inputData.opportunityServiceItems = listOppServiceItem;
        inputData.companyDivisionId = companyId;

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateTraderToOsiList', inputData, true);
        Test.stopTest();

        System.assertEquals(true, response != null);
    }

    @IsTest
    static void updateTraderToOsiListExceptionTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
            SELECT Id,ServicePointCode__c
            FROM OpportunityServiceItem__c
            LIMIT 10
        ];
        String companyId = [
            SELECT Id
            FROM CompanyDivision__c
        ].Id;


        MRO_LC_ActivationWizard.UpdateTraderToOsiListInput inputData = new MRO_LC_ActivationWizard.UpdateTraderToOsiListInput();
        inputData.opportunityServiceItems = listOppServiceItem;
        inputData.companyDivisionId = companyId+'fd45';

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateTraderToOsiList', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
    }

    @IsTest
    static void updateCommodityToDossierTest() {
        String dossierId = [
            SELECT Id
            FROM Dossier__c
        ].Id;


        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossierId,
            'commodity' => 'Gas'
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateCommodityToDossier', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
    }

    @IsTest
    static void updateCommodityToDossierExceptionTest() {
        String dossierId = [
            SELECT Id
            FROM Dossier__c
        ].Id;


        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossierId+'hd4',
            'commodity' => 'Gas'
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateCommodityToDossier', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
    }

    @IsTest
    static void updateContractAndContractSignedDateOnOpportunityTest() {
        String opportunityId = [
            SELECT Id
            FROM Opportunity
        ].Id;
        String salesman = [
            SELECT Id
            FROM Contact
        ].Id;
        Contract contractRecord = [
            SELECT Id, ContractType__c, Name
            FROM Contract
            LIMIT 1
        ];


        String userId = UserInfo.getUserId() ;
        String salesChannel = 'Indirect Sales';

        MRO_LC_ActivationWizard.ContractInputData inputData = new MRO_LC_ActivationWizard.ContractInputData();
        inputData.opportunityId = opportunityId ;
        inputData.contractId = contractRecord.Id ;
        inputData.contractType = contractRecord.ContractType__c ;
        inputData.contractName = contractRecord.Name ;
        inputData.companySignedId = userId ;
        inputData.salesChannel = salesChannel ;
        inputData.salesman = salesman ;

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateContractAndContractSignedDateOnOpportunity', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
    }

    @IsTest
    static void insertConsumptionListTest() {
        List<Consumption__c> consumptionList = [
            SELECT Id, Name
            FROM Consumption__c
            LIMIT  1
        ];
        Contract contractRecord = [
            SELECT Id, ContractType__c, Name
            FROM Contract
            LIMIT 1
        ];

        MRO_LC_ActivationWizard.ConsumptionDetails inputData = new MRO_LC_ActivationWizard.ConsumptionDetails();
        inputData.consumptionList = consumptionList ;
        inputData.contractId = contractRecord.Id ;
        inputData.consumptionConventions = true ;

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'insertConsumptionList', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('consumptionList') != null);
    }

    @IsTest
    static void updateCompanyDivisionInOpportunityTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
            SELECT Id
            FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id,
            'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    @IsTest
    static void setChannelAndOriginTest() {
        /*Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;*/
        String opportunityId = [
            SELECT Id
            FROM Opportunity
        ].Id;
        String dossierId = [
            SELECT Id
            FROM Dossier__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunityId,
            'dossierId' => dossierId,
            'origin' => 'Fax',
            'channel' => 'Magazin Enel'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'setChannelAndOrigin', inputJSON, true);
        Test.stopTest();
        //Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, response == false);
    }

    @IsTest
    static void setSubProcessAndExpirationDateTest() {
        String opportunityId = [
            SELECT Id
            FROM Opportunity
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunityId,
            'subProcess' => '' ,
            'expirationDate' => JSON.serialize(System.today())
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'setSubProcessAndExpirationDate', inputJSON, true);
        Test.stopTest();
        //Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, response == false);
    }

    @IsTest
    static void deleteOsisAndOlisByCommodityChangeTest() {
        String opportunityId = [
            SELECT Id
            FROM Opportunity
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunityId,
            'commodity' => 'Gas'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'deleteOsisAndOlisByCommodityChange', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
    }

    @IsTest
    static void deleteOsisAndOlisByCommodityChangeExceptionTest() {
        String opportunityId = [
            SELECT Id
            FROM Opportunity
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunityId+'jf452',
            'commodity' => 'Gas'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'deleteOsisAndOlisByCommodityChange', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
    }

    @IsTest
    static void setRequestedStartDateTest() {
        String opportunityId = [
            SELECT Id
            FROM Opportunity
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunityId,
            'requestedStartDate' => JSON.serialize(System.today())
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'setRequestedStartDate', inputJSON, true);
        Test.stopTest();
        //Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, response == true);
    }

    @IsTest
    static void isBusinessClientTest() {
        String accountId = [
            SELECT Id
            FROM Account
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'isBusinessClient', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isValid') == false);

        inputJSON = new Map<String, String>{
            'accountId' => accountId
        };
        response = TestUtils.exec('MRO_LC_ActivationWizard', 'isBusinessClient', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isValid') == true);

        Test.stopTest();
    }

    @IsTest
    static void checkCompanyDivisionsTest() {
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'checkCompanyDivisions', null, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('enelEnergiaCompanyDivisionId') != null);

        Test.stopTest();
    }

    @IsTest
    static void isNotWorkingDateTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'requestedStartDate' => JSON.serialize(System.today())
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'isNotWorkingDate', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isNotWorkingDate') == false);

        Test.stopTest();
    }

    @IsTest
    static void updateCompanyDivisionInOpportunityExTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
            SELECT Id
            FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => null,
            'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }

    @isTest
    public static void linkOliToOsiTest(){
        List<OpportunityServiceItem__c>  listOsi = [
            SELECT Id, BillingProfile__c
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        OpportunityLineItem oli = [
            SELECT Id,Product2Id
            FROM OpportunityLineItem
            LIMIT 1
        ];
        MRO_LC_ActivationWizard.InputData inputData = new MRO_LC_ActivationWizard.InputData();
        inputData.opportunityServiceItems = listOsi;
        inputData.oli = oli;
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    /*@IsTest
    public static void linkOliToOsiExTest(){
        List<OpportunityServiceItem__c>  listOsi = [
            SELECT Id, BillingProfile__c
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        OpportunityLineItem oli = [
            SELECT Id,Product2Id
            FROM OpportunityLineItem
            LIMIT 1
        ];
        MRO_LC_ActivationWizard.InputData inputData = new MRO_LC_ActivationWizard.InputData();
        inputData.opportunityServiceItems = null;
        inputData.oli = null;
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }*/

    @IsTest
    public static void updateOpportunityTest(){
        Opportunity opp = [
            SELECT Id,StageName
            FROM Opportunity
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' =>opp.Id,
            'stage' => 'Quoted',
            'dossierId' => dossier.Id,
            'cancelReason' => 'cancelReason',
            'detailsReason' => 'detailsReason',
            'privacyChangeId' => ''

        };
        Test.startTest();
        Object response =  TestUtils.exec('MRO_LC_ActivationWizard', 'updateOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('opportunityId') == opp.Id);
    }


    @IsTest
    public static void updateOpportunityExceptionTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' =>'',
            'stage' => 'Closed Won'
        };
        try {
            TestUtils.exec('MRO_LC_ActivationWizard', 'updateOpportunity', inputJSON, true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    public static void CheckOsiTest(){
        OpportunityServiceItem__c osi =[
            SELECT Id, BillingProfile__c, Distributor__c, Trader__c, Account__c
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' =>osi.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ActivationWizard', 'checkOsi', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assert(result.get('opportunityServiceItem') != null);
        Test.stopTest();
    }

    @IsTest
    public static void CheckOsiExceptionTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' =>''
        };
        try {
            TestUtils.exec('MRO_LC_ActivationWizard', 'checkOsi', inputJSON, true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }



}