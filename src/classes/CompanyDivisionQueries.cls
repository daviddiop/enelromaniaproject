/**
 * Created by goudiaby on 02/07/2019.
 */

public with sharing class CompanyDivisionQueries {

    public static CompanyDivisionQueries getInstance() {
        return new CompanyDivisionQueries();
    }

    public List<CompanyDivision__c> getAll(){
        final Integer currentQueryLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
        return [
            SELECT Id, Name, VATNumber__c
            FROM CompanyDivision__c
            LIMIT :currentQueryLimit
        ];
    }

    public CompanyDivision__c getById(String idCompany){
        return [
            SELECT Id, Name, VATNumber__c
            FROM CompanyDivision__c
            WHERE Id =:idCompany
            LIMIT 1
        ];
    }
}