/**
 * MRO_LC_MeterReading
 *
 * @author  INSA BADJI
 * @version 1.0
 * @description Update Cases with
 *              [ENLCRO-116] Meter Reading #WP3-P1
 * 19/11/2019 : INSA - Original
 */

public with sharing class MRO_LC_MeterReading extends ApexServiceLibraryCnt{

    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();

    /**
      * @author  INSA BADJI
      * @description Initialize method to get data at the beginning for Meter Reading
      * @date 19/11/2019
      * @param accountId
      * @param dossierId
      * @param interactionId
      *
      * @return  Map<String, Object> response:
      */
    public with sharing class Initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = '';
            String templateId = params.get('templateId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            Account acc = accountQuery.findAccount(accountId);
            response.put('phoneCustomer',acc.Phone );
            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType,'Meter Reading');
            Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
            if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                dossierSrv.updateParent(dossier.Id, genericRequestId);
                dossier.Parent__c = genericRequestId;
            }

            List<Case> cases ;
                cases = caseQuery.getCasesByDossierId(dossier.Id);
            String code = 'MeterReadingCodeTemplate_1';
            ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
            if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                response.put('templateId', scriptTemplate.Id);
            }

            if(cases.size()>0){
                response.put('caseTile', cases);
                response.put('originSelected', cases[0].Origin);
                response.put('channelSelected', cases[0].Channel__c);
            }
           // Date today = System.today();
           // Date startDate = today.addMonths(-3);

            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('dossierId', dossier.Id);
           // response.put('startDate', startDate);
            //response.put('endDate', today);
            response.put('dossier', dossier);
            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('accountId', accountId);
            response.put('genericRequestId', dossier.Parent__c);
            response.put('meterReadingEle', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Meter_Reading_Ele').getRecordTypeId());
            response.put('meterReadingGas', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Meter_Reading_Gas').getRecordTypeId());
            response.put('error', false);
            return response;
        }
    }
    /**
      * @author  INSA BADJI
      * @description To cancel the  process of dossier and setting the statut to canceled. We won't create a case
      * @date 19/11/2019
      * @param dossierId
      *
      * @return  Map<String, Object> response: returning error message equals to false
      */

    public with sharing class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');
            String companyDivisionId='';
            Savepoint sp = Database.setSavepoint();

            try {
                caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId,'CN010',constantsSrv.CASE_STATUS_CANCELED);
                if (oldCaseList != null && !oldCaseList.isEmpty()) {
                    if (oldCaseList[0] != null) {
                        companyDivisionId = oldCaseList[0].CompanyDivision__c;
                    }
                    dossierSrv.updateDossierCompanyDivisionWithChannel(dossierId, constantsSrv.DOSSIER_STATUS_CANCELED, companyDivisionId, channel, origin);
                } else {
                    dossierSrv.updateDossierWithChannelAndStatusAndSending(dossierId, channel, origin, constantsSrv.DOSSIER_STATUS_CANCELED,'');
                }
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }
    /**
     * @author  INSA BADJI
     * @description Create a case with markedInvoices__c. The Phase__c will be 'MI010' and status of dossier 'New'
     * @date 19/11/2019
     * @param accountId
     * @param dossierId
     * @param invoices
     * @param Supply
     *
     * @return  Map<String, Object> response: returning error message equals to false
     */
    public with sharing class InsertCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            Map<String, String> meterReadingRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('MeterReading');
            List<Supply__c> listSupply = (List<Supply__c>) JSON.deserialize(params.get('supply'), List<Supply__c>.class);
            String recordTypeId = '';
            String companyDivisionId = '';

            try {
                if ((!listSupply.isEmpty()) && (String.isNotBlank(accountId)) && (String.isNotBlank(dossierId))) {
                    recordTypeId = listSupply[0].RecordType.DeveloperName == 'Gas' ? meterReadingRecordTypes.get('Meter_Reading_GAS') : meterReadingRecordTypes.get('Meter_Reading_ELE');
                    companyDivisionId = listSupply[0].CompanyDivision__c;
                    Case caseInvoice = caseSrv.insertCaseWithInvoice(listSupply[0], accountId, dossierId, recordTypeId, '', 'RE010', '', originSelected, channelSelected,listSupply[0].ContractAccount__c,listSupply[0].Contract__c,'');
                    caseInvoice.EffectiveDate__c = caseQuery.getById(caseInvoice.Id).EffectiveDate__c;
                    response.put('caseTile',caseInvoice);
                }
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    /*
    *    @Author: INSA BADJI
    *    Descriptio: task [ENLCRO-462]  Meter Reading - Integrate Account balance and Invoice Inquiry
    *    Date 24/01/2020
    *    @params:startDate
    *    @params:dossierId
    *    @params:oldCaseList
    *    @params:endDate
    *    @params:channelSelected
    *    @params:originSelected
     */
    public with sharing class UpdateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String origin = params.get('originSelected');
            String channel = params.get('channelSelected');
            String note = params.get('note');
            String phone = params.get('phone');
            //Date startDate = (Date) JSON.deserialize(params.get('startDate'), Date.class);
            //Date endDate = (Date) JSON.deserialize(params.get('endDate'), Date.class);
            Date effectiveDate = System.today() ;
            if(String.isNotBlank(params.get('dateProposedForExecution'))){
                effectiveDate = (Date) JSON.deserialize(params.get('dateProposedForExecution'), Date.class);
            }
            Date slaExpirationDate = effectiveDate.addDays(2);
            String companyDivisionId;
            if (oldCaseList[0] != null) {
                oldCaseList[0].AccountPhone__c = phone;
                oldCaseList[0].EffectiveDate__c = effectiveDate;
                oldCaseList[0].SLAExpirationDate__c = slaExpirationDate;
                companyDivisionId = oldCaseList[0].CompanyDivision__c;

                caseSrv.setNewCaseAndDossier(oldCaseList, dossierId, companyDivisionId,note, null, null, origin, channel);
                response.put('error', false);
            }
            return response;
        }
    }
    /*
    *    @Author: INSA BADJI
    *    Descriptio: task [ENLCRO-460]  Get data of selected supply in advanced Seardh
    *    Date 06/01/2020
    *    @params:supplyId
     */
    public with sharing class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            Supply__c supplyRecord = supplyQuery.getById(supplyId);
            response.put('supply', supplyRecord);
            return response;
        }
    }
}