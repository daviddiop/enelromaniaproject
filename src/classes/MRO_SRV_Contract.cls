public with sharing class MRO_SRV_Contract {

    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_User userQuery = MRO_QR_User.getInstance();
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();

    private static MRO_SRV_DatabaseService databaseService = MRO_SRV_DatabaseService.getInstance();
    private static MRO_UTL_Constants constants = MRO_UTL_Constants.getAllConstants();

    public static MRO_SRV_Contract getInstance() {
        return (MRO_SRV_Contract)ServiceLocator.getInstance(MRO_SRV_Contract.class);
    }

    public Contract insertOrGetContractForOpportunity(Id opportunityId){
        Savepoint sp = Database.setSavepoint();
        try {
            Opportunity opportunity = MRO_QR_Opportunity.getInstance().getById(opportunityId);
            Contract contract = null;

            if (opportunity.ContractId == null){
                contract = instantiateForOpportunity(opportunity);
                databaseService.insertSObject(contract);

                opportunity.ContractId = contract.Id;
                databaseService.updateSObject(opportunity);
            } else {
                contract = contractQuery.getById(opportunity.ContractId);
            }

            return contract;
        } catch (Exception ex) {
            Database.rollback(sp);
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());

            return null;
        }
    }

    public Contract instantiateForOpportunity(Opportunity opportunity){
        Contract contract = new Contract(
                AccountId = opportunity.AccountId,
                CompanyDivision__c = opportunity.CompanyDivision__c,
                Pricebook2Id = opportunity.Pricebook2Id,
                Name = opportunity.ContractName__c,
                CompanySignedId = opportunity.CompanySignedBy__c,
                Salesman__c = opportunity.Salesman__c,
                Opportunity__c = opportunity.Id,
                Status = constants.CONTRACT_STATUS_DRAFT,
                ContractType__c = constants.CONTRACTTYPE_RESIDENTIAL,
                ContractTerm = 12
        );
        return contract;
    }

    public Boolean isSuppliesHaveSameProduct(String contractId, List<Case> caseList) {
        Set<String> caseSupplyIds = new Set<String>();
        for (Case theCase : caseList) {
            caseSupplyIds.add(theCase.Supply__c);
        }
        Boolean isProductSame = true;
        List<Supply__c> caseSupplies = supplyQuery.listByIdsOrContractId(caseSupplyIds, contractId);
        for (Supply__c theSupply : caseSupplies) {
            Supply__c firstSupply = caseSupplies.get(0);
            if (firstSupply.Product__c != theSupply.Product__c) {
                isProductSame = false;
                break;
            }
        }
        return isProductSame;
    }

    public void updateConsumptionConventions(String contractId, Boolean consumptionConventions) {
        databaseService.updateSObject(new Contract(
            Id = contractId,
            ConsumptionConventions__c = consumptionConventions
        ));
    }

    public List<Contract> updateContracts(List<Contract> contractList) {
        List<Contract> output = contractList;
        if(contractList!=null && !contractList.isEmpty()) {
            try {
                databaseService.updateSObject(output);
            } catch (Exception e) {
                throw e;
            }
        }
        return output;
    }

    public void assignRelatedCaseAndDossierToEnelX(List<Contract> newContractList, Map<Id, Contract> idToOldContract) {
        List<String> signedContractIds = new List<String>();
        for (Contract newContract : newContractList) {
            Contract oldContract = idToOldContract.get(newContract.Id);
            if (oldContract.Status != constants.CONTRACT_STATUS_SIGNED && newContract.Status == constants.CONTRACT_STATUS_SIGNED) {
                signedContractIds.add(newContract.Id);
            }
        }
        if (signedContractIds.isEmpty()) {
            return;
        }
        List<Case> caseList = caseQuery.listByContractIds(signedContractIds);
        Group enelXQueue = userQuery.getQueueByDeveloperName(MRO_UTL_Constants.ENELX_QUEUE);
        if (caseList.isEmpty() || enelXQueue == null) {
            return;
        }
        List<Case> caseListToReassign = new List<Case>();
        List<Dossier__c> dossierListToReassign = new List<Dossier__c>();
        for (Case theCase : caseList) {
            theCase.OwnerId = enelXQueue.Id;
            caseListToReassign.add(theCase);
            if (String.isNotBlank(theCase.Dossier__c)) {
                dossierListToReassign.add(new Dossier__c(
                    Id = theCase.Dossier__c,
                    OwnerId = enelXQueue.Id
                ));
            }
        }
        databaseService.updateSObject(caseListToReassign);
        databaseService.updateSObject(dossierListToReassign);
    }

    public void handleActivatedContracts(List<Contract> activatedContracts) {
        /*
        Set<Id> contractIds = (new Map<Id, Contract>(activatedContracts)).keySet();
        List<Supply__c> vasSupplies = supplyQuery.listVasSuppliesByCommodityContractIds(contractIds);
        Map<Id, Case> vasActivationCasesToUnsuspend = new Map<Id, Case>();
        Map<Id, Case> vasActivationCasesToPush = new Map<Id, Case>();
        for (Supply__c supply : vasSupplies) {
            if (supply.Status__c == constants.SUPPLY_STATUS_ACTIVATING && supply.Activator__c != null && supply.Activator__r.Status == constants.CASE_STATUS_ONHOLD) {
                Case vasActivationCase = supply.Activator__r;
                vasActivationCase.Status = constants.CASE_STATUS_NEW;
                vasActivationCasesToUnsuspend.put(vasActivationCase.Id, vasActivationCase);
                if (supply.Contract__r.Status == constants.CONTRACT_STATUS_SIGNED) {
                    vasActivationCasesToPush.put(vasActivationCase.Id, vasActivationCase);
                }
            }
        }
        if (!vasActivationCasesToUnsuspend.isEmpty()) {
            update vasActivationCasesToUnsuspend.values();
        }
        if (!vasActivationCasesToPush.isEmpty()) {
            MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
            for (Case c : vasActivationCasesToPush.values()) {
                transitionsUtl.checkAndApplyAutomaticTransitionWithTag(c, constants.DOCUMENT_VALIDATION_TAG);
            }
        }
         */
    }
}