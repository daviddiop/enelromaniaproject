/**
  * @author  Luca Ravicini
  * @since   Feb 23, 2020
  * @desc    Test class for MRO_UTL_Date
  *
  */

@IsTest
private class MRO_UTL_DateTst {
    @IsTest
    static void testCalculateWorkDays() {
        Date dateFrom= date.newInstance(2020,2,4);
        Integer workingDays=10;
        Integer added=MRO_UTL_Date.calculateWorkDays(dateFrom, workingDays);
        System.assert(getTotalDays(dateFrom,workingDays) == added);


        dateFrom= date.newInstance(2020,1,6);
        added=MRO_UTL_Date.calculateWorkDays(dateFrom, workingDays);
        System.assert(getTotalDays(dateFrom,workingDays) == added);

        dateFrom= date.newInstance(2020,1,16);
        workingDays=13;
        added=MRO_UTL_Date.calculateWorkDays(dateFrom, workingDays);
        System.debug('added=='+added +  'total=='+getTotalDays(dateFrom,workingDays));
        System.assert(getTotalDays(dateFrom,workingDays) == added);


        dateFrom= date.newInstance(2020,1,4);
        workingDays=13;
        added=MRO_UTL_Date.calculateWorkDays(dateFrom, workingDays);
        System.debug('added32=='+added +  'total=='+getTotalDays(dateFrom,workingDays));
        System.assert(getTotalDays(dateFrom,workingDays) == added);

        dateFrom= date.newInstance(2020,1,4);
        workingDays=16;
        added=MRO_UTL_Date.calculateWorkDays(dateFrom, workingDays);
        System.debug('added=='+added +  'total=='+getTotalDays(dateFrom,workingDays));
        System.assert(getTotalDays(dateFrom,workingDays) == added);
    }

    private static Integer getTotalDays( Date dateFrom, Integer workingDays){

        Integer totalDays=0;

        Datetime dt=datetime.newInstance(dateFrom.year(), dateFrom.month(), dateFrom.day());
        DateTime endDate=dt;

        for(Integer i=0; i < workingDays; i++){
            endDate=endDate.addDays(1);
            String day=endDate.format('E');

            if(day.equals('Sat') || day.equals('Sun')){
                i--;
            }
            totalDays++;
        }


        return totalDays;
    }
}