/**
 * Created by ferhati on 19/04/2019.
 * modified by Boubacar Sow d on 15/01/2020.
 * modified by Goudiaby on 30/03/2020.
 * @version 1.0
 * @description
 *          [ENLCRO-424] Switch Out - Implement the process to make a switch out Case
 *          [ENLCRO-523] SwitchOut - Integrate Channel & Origin selection
 */

public with sharing class MRO_LC_SwitchOutWizard extends ApexServiceLibraryCnt {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_UTL_SwitchOut switchOutUTL = MRO_UTL_SwitchOut.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
    static String switchOutRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SwitchOut_ELE').getRecordTypeId();
    static List<ApexServiceLibraryCnt.Option> traderPicklistValues = new List<ApexServiceLibraryCnt.Option>();
    private static  MRO_SRV_Case srvCase = MRO_SRV_Case.getInstance();
    static String traderLabel;
    static String effectiveDateLabel;
    static String referenceDateLabel;
    static Date switchOutDate;
    static String accountDistributor = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();

    static List<ApexServiceLibraryCnt.Option> commodityPicklistValues = new List<ApexServiceLibraryCnt.Option>();
    static String commodityLabel;

    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String templateId = params.get('templateId');
            String genericRequestId = params.get('genericRequestId');

            Map<String, Object> response = new Map<String, Object>();
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            //if (String.isNotBlank(interactionId)) {
                /*if (String.isNotBlank(dossierId)) {
                    Dossier__c dossierCheckParentId = dossierQuery.getById(dossierId);

                    if (String.isNotBlank(dossierCheckParentId.Parent__c)) {
                        dossierId = dossierCheckParentId.Id;
                    } else if (dossierCheckParentId.RecordType.DeveloperName == 'GenericRequest') {
                        genericRequestId = dossierId;
                        dossierId = '';
                    }
                }*/
            //}
            try {
                Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'SwitchOut');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                Account acc = accountQuery.findAccount(accountId);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);

                String code = 'SwitchOutWizardCodeTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }

                getSwitchOutDescribe();
                getCommodityDescribe();
                response.put('caseTile', cases);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('accountId', accountId);
                response.put('account', acc);
                response.put('accountPersonRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId());
                response.put('accountPersonProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId());
                response.put('accountBusinessRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId());
                response.put('accountBusinessProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId());
                response.put('traderPicklistValues', traderPicklistValues);
                response.put('traderLabel', traderLabel);
                response.put('effectiveDateLabel', effectiveDateLabel);
                response.put('referenceDateLabel', referenceDateLabel);
                response.put('commodityPicklistValues', commodityPicklistValues);
                response.put('commodityLabel', commodityLabel);
                response.put('commodity', dossier.Commodity__c);
                response.put('switchOutRecordType', switchOutRecordType);
                response.put('effectiveDate', switchOutDate);
                response.put('hasRetroactiveSwitchOutPermission', FeatureManagement.checkPermission('MRO_RetroactiveSwitchOut'));
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public static void getCommodityDescribe() {
        commodityPicklistValues = new List<ApexServiceLibraryCnt.Option>();
        commodityPicklistValues.add(new ApexServiceLibraryCnt.Option('Electric', 'Electric'));
        commodityPicklistValues.add(new ApexServiceLibraryCnt.Option('Gas', 'Gas'));
        commodityLabel = System.Label.commodity;
    }

    public static void getSwitchOutDescribe() {
        traderPicklistValues = new List<ApexServiceLibraryCnt.Option>();
        Schema.DescribeFieldResult trader = Case.Trader__c.getDescribe();
        String traderRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        //Schema.DescribeFieldResult effectiveDate = Case.EffectiveDate__c.getDescribe();
        Schema.DescribeFieldResult referenceDate = Case.ReferenceDate__c.getDescribe();
        List<Account> traders = accQuery.getTraders (traderRecordTypeId);
        if (!traders.isEmpty()) {
            for (Account acc : traders) {
                traderPicklistValues.add(new ApexServiceLibraryCnt.Option(acc.Name, acc.Id));
            }
            traderLabel = Label.New + '' + trader.getLabel();
            effectiveDateLabel = Label.SwitchOutDate; //effectiveDate.getLabel();
            referenceDateLabel = referenceDate.getLabel();
        }
        Date effectiveDate = Date.today().addDays(5);
        while (MRO_UTL_Date.isNotWorkingDate(effectiveDate)) {
            effectiveDate = effectiveDate.addDays(1);
        }
        switchOutDate = effectiveDate;

    }

    public class saveDraftBP extends AuraCallable {
        public override Object perform(final String jsonInput) {
            DraftBillingProfileInput billingProfileParams = (DraftBillingProfileInput) JSON.deserialize(jsonInput, DraftBillingProfileInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = new List<Case>();

            Savepoint sp = Database.setSavepoint();
            response.put('error', false);
            try {
                if ((!billingProfileParams.oldCaseList.isEmpty())) {

                    for (Case caseRecord : billingProfileParams.oldCaseList) {
                        caseList.add(new Case(Id = caseRecord.Id));
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class DraftBillingProfileInput {
        @AuraEnabled
        public List<Case> oldCaseList { get; set; }
        @AuraEnabled
        public String billingProfileId { get; set; }
    }

    public class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CreateCaseInput createCaseParams = (CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);

            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> switchOutRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('SwitchOut');
            List<Case> newCases = new List<Case>();
            String recordTypeId;
            //String caseOrigin = 'Phone';

            response.put('error', false);
            try {
                if (String.isNotBlank(createCaseParams.caseId)) {
                    Case caseRecord = new Case(
                            Id = createCaseParams.caseId,
                            EffectiveDate__c = createCaseParams.effectiveDate,
                            ReferenceDate__c = createCaseParams.referenceDate
                    );
                    newCases.add(caseRecord);

                } else if (String.isNotBlank(createCaseParams.trader)
                        && (!createCaseParams.searchedSupplyFieldsList.isEmpty())
                        && (createCaseParams.effectiveDate != null)
                        && (createCaseParams.referenceDate != null)
                        && (String.isNotBlank(createCaseParams.accountId))
                        && (String.isNotBlank(createCaseParams.dossierId))) {

                    for (Supply__c supply : createCaseParams.searchedSupplyFieldsList) {
                        recordTypeId = supply.RecordType.DeveloperName == 'Gas' ? switchOutRecordTypes.get('SwitchOut_GAS') : switchOutRecordTypes.get('SwitchOut_ELE');
                        Case disconnectedCase = new Case(
                                Supply__c = supply.Id,
                                CompanyDivision__c = supply.CompanyDivision__c,
                                EffectiveDate__c = createCaseParams.effectiveDate,
                                ReferenceDate__c = createCaseParams.referenceDate,
                                Trader__c = createCaseParams.trader,
                                AccountId = createCaseParams.accountId,
                                RecordTypeId = recordTypeId,
                                Dossier__c = createCaseParams.dossierId,
                                Status = 'Draft', Origin = createCaseParams.originSelected,
                                Channel__c = createCaseParams.channelSelected,
                                Reason__c = createCaseParams.reason
                        );
                        newCases.add(disconnectedCase);
                    }
                    /*if (!newCases.isEmpty()) {
                        databaseSrv.insertSObject(newCases);

                        if (!createCaseParams.caseList.isEmpty()) {
                            for (Case oldCaseList : createCaseParams.caseList) {
                                newCases.add(oldCaseList);
                            }
                        }
                        response.put('caseTile', newCases);
                    }*/
                }
                if (!newCases.isEmpty()) {

                    databaseSrv.upsertSObject(newCases);

                    if (!createCaseParams.caseList.isEmpty() && String.isBlank(createCaseParams.caseId)) {
                        for (Case oldCaseList : createCaseParams.caseList) {
                            newCases.add(oldCaseList);
                        }
                    }
                    response.put('caseTile', newCases);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CreateCaseInput {
        @AuraEnabled
        public List<Supply__c> searchedSupplyFieldsList { get; set; }
        @AuraEnabled
        public Date effectiveDate { get; set; }
        @AuraEnabled
        public Date referenceDate { get; set; }
        @AuraEnabled
        public String trader { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String reason { get; set; }
        @AuraEnabled
        public List<Case> caseList { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
    }

    public class updateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            UpdateCaseListInput updateCaseListParams = (UpdateCaseListInput) JSON.deserialize(jsonInput, UpdateCaseListInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> confirmedCaseList = new List<Case>();
            List<Case> canceledCaseList = new List<Case>();
            List<Supply__c> supplyList = new List<Supply__c>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (!updateCaseListParams.oldCaseList.isEmpty() && String.isNotBlank(updateCaseListParams.dossierId)) {
                    //add  by david
                    String companyDivisionId = updateCaseListParams.oldCaseList[0].CompanyDivision__c;
                    //
                    Map<Id, Case> casesMap = new Map<Id, Case>(caseQuery.listByIds((new Map<Id, Case>(updateCaseListParams.oldCaseList)).keySet()));

                    Dossier__c dossier = new Dossier__c (Id = updateCaseListParams.dossierId, Status__c = 'New', CompanyDivision__c = companyDivisionId);
                    if (dossier.Id != null) {
                        databaseSrv.updateSObject(dossier);
                    }
                    for (Case caseRecord : updateCaseListParams.oldCaseList) {

                        Case caseToUpdate = casesMap.get(caseRecord.Id);
                        caseToUpdate.Dossier__c = dossier.Id;
                        caseToUpdate.Status = 'New';

                        if (caseRecord.Reason__c == 'Trader change') {

                            Boolean dateCheck = switchOutUTL.logicDateCheck(caseRecord.Id);
                            System.debug('test1' + caseRecord);
                            if (dateCheck != null) {

                                if (dateCheck) {
                                    //caseToUpdate.Phase__c = 'WT010';
                                    caseToUpdate.CustomerConfirmationDate__c = caseRecord.ReferenceDate__c;
                                    confirmedCaseList.add(caseToUpdate);
                                } else {
                                    //caseToUpdate.Phase__c = 'CANC010';
                                    //caseToUpdate.Status = 'Canceled';
                                    caseToUpdate.CancellationReason__c = 'Switch-out Date <21 days';
                                    canceledCaseList.add(caseToUpdate);
                                }
                            }
                        } else {
                            ///////////
                            //caseToUpdate.Phase__c = 'WT010';
                            confirmedCaseList.add(caseToUpdate);
                        }
                        supplyList.add(new Supply__c(Id = caseRecord.Supply__c, Status__c = 'Terminating', Terminator__c = caseRecord.Id));
                    }
                    if (!supplyList.isEmpty()) {
                        databaseSrv.updateSObject(supplyList);
                    }
                    MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
                    if (!confirmedCaseList.isEmpty()) {
                        databaseSrv.updateSObject(confirmedCaseList);
                        for (Case c : confirmedCaseList) {
                            transitionsUtl.checkAndApplyAutomaticTransitionWithTag(c, CONSTANTS.CONFIRM_TAG);
                        }
                    }
                    if (!canceledCaseList.isEmpty()) {
                        databaseSrv.updateSObject(canceledCaseList);
                        for (Case c : canceledCaseList) {
                            transitionsUtl.checkAndApplyAutomaticTransitionWithTag(c, CONSTANTS.CANCELLATION_TAG);
                        }
                    }
                    if (dossier.Id != null) {
                        //<<<Automatic phase transitions after saving on the wizards
                        //srvCase.applyAutomaticTransitionOnDossierAndCases(dossier.Id);
                        dossier = dossierQuery.getById(dossier.Id);
                        String transitionTag = confirmedCaseList.isEmpty() ? CONSTANTS.CANCELLATION_TAG : CONSTANTS.CONFIRM_TAG;
                        transitionsUtl.checkAndApplyAutomaticTransitionWithTag(dossier, transitionTag);
                        //>>>Automatic phase transitions after saving on the wizards
                    }

                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class UpdateCaseListInput {
        @AuraEnabled
        public List<Case> oldCaseList { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
    }

    public class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CancelProcessInput cancelProcessParams = (CancelProcessInput) JSON.deserialize(jsonInput, CancelProcessInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = new List<Case>();
            List<Supply__c> supplyList = new List<Supply__c>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (!cancelProcessParams.oldCaseList.isEmpty() && String.isNotBlank(cancelProcessParams.dossierId)) {
                    Dossier__c dossier = new Dossier__c (id = cancelProcessParams.dossierId, Status__c = 'Canceled');
                    if (dossier != null) {
                        databaseSrv.upsertSObject(dossier);
                    }

                    for (Case caseRecord : cancelProcessParams.oldCaseList) {
                        caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status = 'Canceled', Phase__c = 'CANC010'));
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    /**
  * @author  Boubacar Sow
  * @description update dossier
  *              [ENLCRO-523] SwitchOut - Integrate Channel & Origin selection
  * @date 13/01/2020
  * @param DossierId,channelSelected,originSelected
  *
  */
    public class UpdateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            dossierSrv.updateDossierChannel(dossierId, channelSelected, originSelected);
            response.put('error', false);
            return response;
        }
    }

    public class getValidDate extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Date effectiveDate = (Date) JSON.deserialize(params.get('effectiveDate'), Date.class);
            Boolean effectiveValid = MRO_UTL_Date.isNotWorkingDate(effectiveDate);
            response.put('error', false);
            System.debug('effectiveValid' + effectiveValid);
            System.debug('effectiveValid' + effectiveDate);
            response.put('effectiveValid', effectiveValid);
            return response;
        }
    }

    public class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            supplyInputIds createCaseParams = (supplyInputIds) JSON.deserialize(jsonInput, supplyInputIds.class);
            List<Supply__c> supplyListRecord = supplyQuery.getSuppliesByIds(createCaseParams.supplyIds);
            response.put('supplies', supplyListRecord);
            return response;
        }
    }

    public class CancelProcessInput {
        @AuraEnabled
        public List<Case> oldCaseList { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
    }
    public class supplyInputIds {
        @AuraEnabled
        public List<Id> supplyIds { get; set; }
    }

    public with sharing class updateCommodityToDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String commodity = params.get('commodity');

            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
            try {
                Dossier__c doss = new Dossier__c();
                doss.Id = dossierId;
                doss.Commodity__c = commodity;
                databaseSrv.upsertSObject(doss);
                response.put('dossierId', doss.Id);
                response.put('commodity', doss.Commodity__c);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class updateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String datesToCheck = params.get('checkEffectiveDate');
            Map<String, Object> response = new Map<String, Object>();
            List<Case> newCases = new List<Case>();
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            Date effectiveDate = (Date) JSON.deserialize(params.get('effectiveDate'), Date.class);
            Date referenceDate = (Date) JSON.deserialize(params.get('referenceDate'), Date.class);
            String recordTypeId;
            //String caseOrigin = 'Phone';

            response.put('error', false);
            try {
                System.debug('newCases2' + params.get('checkEffectiveDate'));
                System.debug('createCaseParams.caseId' + params.get('checkEffectiveDate'));
                if (String.isNotBlank(params.get('caseId'))) {
                    if (!caseList.isEmpty()) {
                        for (Case casetoUpdate : caseList) {
                            Case caseRecord = new Case(
                                    Id = casetoUpdate.Id,
                                    EffectiveDate__c = effectiveDate,
                                    ReferenceDate__c = referenceDate,
                                    Trader__c = params.get('trader'),
                                    Reason__c = params.get('reason')
                            );
                            newCases.add(caseRecord);
                        }
                    }

                }
                if (!newCases.isEmpty()) {

                    databaseSrv.upsertSObject(newCases);
                    response.put('caseTile', newCases);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }


}