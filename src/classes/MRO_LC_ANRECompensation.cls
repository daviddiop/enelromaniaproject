/**
 * Created by Vlad Mocanu on 24/03/2020.
 */

public with sharing class MRO_LC_ANRECompensation extends ApexServiceLibraryCnt {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();


    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String accountId = params.get('accountId');
            String billingProfileId = '';
            String genericRequestDossierId = params.get('genericRequestDossierId');
            String interactionId = params.get('interactionId');
            CustomerInteraction__c customerInteraction;
            List<CustomerInteraction__c> interactionList = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);
            if (interactionList != null && !interactionList.isEmpty()) {
                customerInteraction = interactionList[0];
            }
            Map<String, Object> response = new Map<String, Object>();

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                Dossier__c dossier;
                if (String.isBlank(dossierId)) {
                    dossier = new Dossier__c();
                    dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
                    dossier.RequestType__c = 'ANRECompensation';
                    dossier.Status__c = 'Draft';
                    dossier.Account__c = accountId;

                    if (customerInteraction != null) {
                        dossier.CustomerInteraction__c = customerInteraction.Id;
                        dossier.Origin__c = customerInteraction.Interaction__r.Origin__c;
                        dossier.Channel__c = customerInteraction.Interaction__r.Channel__c;
                    }

                    if (genericRequestDossierId != null) {
                        response.put('parentDossierId', genericRequestDossierId);
                        Dossier__c parentDossier = dossierQuery.getById(genericRequestDossierId);
                        dossier.Origin__c = parentDossier.Origin__c;
                        dossier.Channel__c = parentDossier.Channel__c;
                        dossier.SendingChannel__c = parentDossier.SendingChannel__c;
                        dossier.Account__c = parentDossier.Account__c;
                        dossier.AssignmentProvince__c = parentDossier.AssignmentProvince__c;
                        dossier.Phone__c = parentDossier.Phone__c;
                        dossier.Email__c = parentDossier.Email__c;
                        dossier.SLAExpirationDate__c = parentDossier.SLAExpirationDate__c;
                        dossier.Parent__c = parentDossier.Id;
                    }

                    Date slaExpirationDate = addWorkingDays(Date.today(), 5);
                    dossier.SLAExpirationDate__c = slaExpirationDate;
                    databaseSrv.insertSObject(dossier);
                    dossierId = dossier.Id;
                } else {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
                    dossier = dossierQuery.getById(dossierId);
                    if (!cases.isEmpty()) {
                        billingProfileId = cases[0].BillingProfile__c;

                        if (cases[0].Supply__c != null) {
                            response.put('supplyId', cases[0].Supply__c);
                            response.put('supplyStatus', cases[0].Supply__r.Status__c);
                            response.put('supplyType', getSupplyType(cases[0].Supply__r.RecordType.DeveloperName));
                        }
                        response.put('cases', cases);
                    }


                    response.put('billingProfileId', billingProfileId);
                }


                response.put('dossier', dossier);
                response.put('dossierId', dossierId);
                response.put('accountId', accountId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class SetCaseDatesFromParentCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String parentCaseId = params.get('parentCaseId');
            response.put('error', false);
            try {
                if (String.isNotBlank(parentCaseId)) {
                    Case caseRecord = caseQuery.getById(parentCaseId);
                    response.put('createdDate', caseRecord.CreatedDate);
                    if (caseRecord.StartDate__c != null) {
                        response.put('findingDate', caseRecord.StartDate__c);
                    } else {
                        response.put('findingDate', Date.today());
                    }
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class getSupplyStatusById extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String supplyId = params.get('supplyId');
            Set<Id> supplyIdSet = new Set<Id>();

            supplyIdSet.add(supplyId);

            response.put('error', false);
            try {
                if (String.isNotBlank(supplyId)) {
                    Supply__c supply = supplyQuery.getByIds(supplyIdSet).get(supplyId);
                    response.put('supplyStatus', supply.Status__c);
                    response.put('supplyType', getSupplyType(supply.RecordType.DeveloperName));
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class DeleteCanceledCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String caseId = params.get('caseId');

            response.put('error', false);
            try {
                if (String.isNotBlank(caseId)) {
                    String recordId = caseId;
                    caseSrv.deleteCase(recordId);
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CreateCaseInput createCaseParams = (CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);
            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> anreCompensationRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('ANRECompensation');
            Supply__c supply;
            String supplyType;
            List<Case> newCases = new List<Case>();
            String recordTypeId;

            response.put('error', false);
            try {
                if (String.isNotBlank(createCaseParams.dossierId) && String.isNotBlank(createCaseParams.selectedOrigin) && String.isNotBlank(createCaseParams.selectedChannel)
                        && String.isNotBlank(createCaseParams.supplyId) && String.isNotBlank(createCaseParams.accountId)) {
                    recordTypeId = anreCompensationRecordTypes.get('ANRECompensation');
                    supply = supplyQuery.getById(createCaseParams.supplyId);
                    supplyType = getSupplyType(supply.RecordType.DeveloperName);

                    Case anreCompensationDraftCase = new Case(
                            RecordTypeId = recordTypeId,
                            Dossier__c = createCaseParams.dossierId,
                            Origin = createCaseParams.selectedOrigin,
                            Channel__c = createCaseParams.selectedChannel,
                            Supply__c = createCaseParams.supplyId,
                            SupplyType__c = supplyType,
                            CompanyDivision__c = supply.CompanyDivision__c,
                            AccountId = createCaseParams.accountId,
                            Status = 'Draft');
                    Database.DMLOptions dmo = new Database.DMLOptions();
                    dmo.assignmentRuleHeader.useDefaultRule = true;
                    anreCompensationDraftCase.setOptions(dmo);
                    newCases.add(anreCompensationDraftCase);
                    databaseSrv.upsertSObject(newCases);

                    databaseSrv.upsertSObject(new Dossier__c(
                            Id = createCaseParams.dossierId,
                            SendingChannel__c = supply.ContractAccount__r?.BillingProfile__r?.DeliveryChannel__c,
                            Email__c = supply.ContractAccount__r?.BillingProfile__r?.Email__c
                    ));

                    response.put('cases', newCases);
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class SaveCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String dossierId = params.get('dossierId');
            String billingProfileId = params.get('billingProfileId');

            List<Case> caseList = new List<Case>();
            String selectedChannel = params.get('selectedChannel');
            String selectedOrigin = params.get('selectedOrigin');
            List<Case> newCaseList = new List<Case>();
            Savepoint sp = Database.setSavepoint();

            if (params.get('caseList').length() > 0) {
                caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            }

            response.put('error', false);
            try {
                if (String.isNotBlank(dossierId) && (caseList != null && !caseList.isEmpty())) {
                    for (Case caseRecord : caseList) {
                        Date startDate = getStartDate(caseRecord);
                        Case newCase = new Case(
                                Id = caseRecord.Id,
                                Dossier__c = dossierId,
                                Channel__c = selectedChannel,
                                Origin = selectedOrigin,
                                Status = 'New',
                                CaseTypeCode__c = 'C168',
                                EffectiveDate__c = Date.today(),
                                StartDate__c = startDate,
                                SLAExpirationDate__c = addWorkingDays(startDate, 5)
                        );

                        if (String.isNotEmpty(billingProfileId)) {
                            newCase.BillingProfile__c = billingProfileId;
                        }

                        newCaseList.add(newCase);
                    }
                    if (!newCaseList.isEmpty()) {
                        databaseSrv.upsertSObject(newCaseList);
                    }
                    Dossier__c dossier = new Dossier__c (Id = dossierId, Status__c = 'New', Channel__c = selectedChannel, Origin__c = selectedOrigin);
                    if (dossier != null) {
                        databaseSrv.upsertSObject(dossier);
                        caseSrv.applyAutomaticTransitionOnDossierAndCases(dossierId);
                    }
                    response.put('dossier', dossier);
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String cancelReason = params.get('cancelReason');
            String detailsReason = params.get('detailsReason');
            List<Case> caseList = new List<Case>();
            Savepoint sp = Database.setSavepoint();

            try {
                caseList = caseQuery.getCasesByDossierId(dossierId);
                for (Case caseRecord : caseList) {
                    caseRecord.CancellationReason__c = cancelReason;
                    caseRecord.CancellationDetails__c = detailsReason;
                }


                caseSrv.setCanceledOnCaseAndDossier(caseList, dossierId);

                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class SaveDraftCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Savepoint sp = Database.setSavepoint();
            String dossierId = params.get('dossierId');
            String billingProfileId = params.get('billingProfileId');
            String selectedChannel = params.get('selectedChannel');
            String selectedOrigin = params.get('selectedOrigin');
            List<Case> caseList = new List<Case>();

            if (params.get('caseList').length() > 0) {
                caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            }

            response.put('error', false);
            try {
                for (Case caseRecord : caseList) {
                    caseRecord.Status = 'Draft';
                    caseRecord.BillingProfile__c = billingProfileId;
                    caseRecord.Channel__c = selectedChannel;
                    caseRecord.Origin = selectedOrigin;
                }

                if (!caseList.isEmpty()) {
                    databaseSrv.upsertSObject(caseList);
                }

                Dossier__c dossier = new Dossier__c (
                        Id = dossierId,
                        Status__c = 'Draft',
                        Channel__c = selectedChannel,
                        Origin__c = selectedOrigin
                );

                if (dossier != null) {
                    databaseSrv.upsertSObject(dossier);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class SetChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Savepoint sp = Database.setSavepoint();
            String dossierId = params.get('dossierId');
            String selectedChannel = params.get('selectedChannel');
            String selectedOrigin = params.get('selectedOrigin');

            response.put('error', false);
            try {
                if(String.isNotBlank(dossierId) && String.isNotBlank(selectedChannel) && String.isNotBlank(selectedOrigin)) {
                    dossierSrv.updateDossierChannel(dossierId, selectedChannel, selectedOrigin);
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    private static Date getStartDate(Case authorityCase) {
        Date newStartDate = Date.today();
        Id parentId = authorityCase.ParentId;
        if (parentId != null) {
            Date parentStartDate = caseQuery.getById(parentId).StartDate__c;
            if (parentStartDate != null) {
                newStartDate = parentStartDate;
            }
        }

        return newStartDate;
    }

    private static String getSupplyType(String supplyRecordType) {
        switch on (supplyRecordType) {
            when 'Electric' {
                return 'Electricity';
            }
            when 'Gas' {
                return 'Gas';
            }
            when 'Service' {
                return 'VAS';
            }
            when else {
                return '';
            }
        }
    }

    private static Date addWorkingDays(Date initialDate, Integer businessDaysToAdd) {
        Date finalDate = initialDate;
        Integer direction = businessDaysToAdd < 0 ? -1 : 1;
        while (businessDaysToAdd != 0) {
            finalDate = finalDate.addDays(direction);
            if (!MRO_UTL_Date.isNotWorkingDate(finalDate)) {
                businessDaysToAdd -= direction;
            }
        }

        return finalDate;
    }

    public class CreateCaseInput {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String supplyId { get; set; }
        @AuraEnabled
        public String selectedOrigin { get; set; }
        @AuraEnabled
        public String selectedChannel { get; set; }
        @AuraEnabled
        public String caseList { get; set; }
    }
}