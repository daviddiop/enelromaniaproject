/**
 * Created by tommasobolis on 29/04/2020.
 */
public with sharing class MRO_QR_Token {

    /**
     * Returns and instance of the current class.
     *
     * @return instance of the current class.
     */
    public static MRO_QR_Token getInstance() {

        return (MRO_QR_Token) ServiceLocator.getInstance(MRO_QR_Token.class);
    }

    /**
     * Returns the token identified by the provided value
     * @param value token value
     * @return the token identified by the provided value
     */
    public List<Token__c> getTokenByValue(String value) {

        List<Token__c> tokens =
                [SELECT Id, Value__c, ExpireDateTime__c, IsExpired__c, IsValid__c
                 FROM Token__c
                 WHERE Value__c = :value];

        return tokens;
    }

    /**
     * Returns the token identified by the provided salesforce id
     * @param tokenId token salesforce id
     * @return the token identified by the provided salesforce id
     */
    public List<Token__c> getTokenById(Id tokenId) {

        List<Token__c> tokens =
                [SELECT Id, Value__c, ExpireDateTime__c, IsExpired__c, IsValid__c
                 FROM Token__c
                 WHERE Id = :tokenId];

        return tokens;
    }
}