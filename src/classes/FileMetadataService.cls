public with sharing class FileMetadataService {

    private static FileMetadataQueries fileMetadataQuery = FileMetadataQueries.getInstance();
    private static ContentDocumentQueries contentDocumentQuery = ContentDocumentQueries.getInstance();
    private static ContentDocumentLinkQueries contentDocumentLinkQuery = ContentDocumentLinkQueries.getInstance();

    private static DatabaseService databaseSrv = DatabaseService.getInstance();

    public static FileMetadataService getInstance() {
        return new FileMetadataService();
    }

    public FileMetadataDTO getFileMetadataById(String fileMetadataId) {
        FileMetadata__c fileMetadata = String.isNotBlank(fileMetadataId) ? fileMetadataQuery.findById(fileMetadataId) : null;
        return fileMetadata != null ? new FileMetadataDTO(fileMetadata) : null;
    }

    public FileMetadata__c insertFileMetadata(String dossierId) {
        FileMetadata__c fileMetadata = new FileMetadata__c(Dossier__c = dossierId);
        databaseSrv.insertSObject(fileMetadata);
        return fileMetadata;
    }

    public FileMetadata__c findOrCreateTempFileMetadata() {
        FileMetadata__c fileMetadata = fileMetadataQuery.findByTitle(Label.TempFileMetadataName);
        if (fileMetadata == null) {
            fileMetadata = new FileMetadata__c(
                Title__c = Label.TempFileMetadataName
            );
            databaseSrv.insertSObject(fileMetadata);
        }
        return fileMetadata;
    }

    public void insertFileMetadata(String tempFileMetadataId, String documentId, String dossierId) {
        ContentDocument document = contentDocumentQuery.findById(documentId);
        FileMetadata__c fileMetadata = new FileMetadata__c(
            Dossier__c = dossierId,
            DocumentId__c = documentId,
            Title__c = document.Title
        );
        databaseSrv.insertSObject(fileMetadata);
        List<ContentDocumentLink> linkList = contentDocumentLinkQuery.listByContentDocumentAndEntityId(documentId, tempFileMetadataId);
        List<ContentDocumentLink> linkToCreateList = new List<ContentDocumentLink>();
        for (ContentDocumentLink link : linkList) {
            ContentDocumentLink newLink = link.clone(false, false, false, false);
            newLink.LinkedEntityId = fileMetadata.Id;
            linkToCreateList.add(newLink);
        }
        databaseSrv.deleteSObject(linkList);
        databaseSrv.insertSObject(linkToCreateList);
    }

    public class FileMetadataDTO {
        @AuraEnabled
        public String id { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String dossierName { get; set; }
        @AuraEnabled
        public Datetime createdDate { get; set; }

        public FileMetadataDTO() {
        }

        public FileMetadataDTO(FileMetadata__c fileMetadata) {
            this.Id = fileMetadata.Id;
            this.name = fileMetadata.Name;
            this.dossierId = fileMetadata.Dossier__c;
            this.dossierName = fileMetadata.Dossier__c != null ? fileMetadata.Dossier__r.Name : '';
            this.createdDate = fileMetadata.CreatedDate;
        }
    }
}