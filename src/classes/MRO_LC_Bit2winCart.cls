public with sharing class MRO_LC_Bit2winCart extends ApexServiceLibraryCnt {

    public class createOli extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            List<MRO_SRV_Bit2WinCart.CartItem> cartItemList = (List<MRO_SRV_Bit2WinCart.CartItem>)JSON.deserialize(params.get('cartItemList'), List<MRO_SRV_Bit2WinCart.CartItem>.class);

            MRO_SRV_Bit2WinCart.getInstance().createOLIs(opportunityId, cartItemList);
            return null;
        }
    }
}