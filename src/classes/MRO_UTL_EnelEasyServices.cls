/**
 * Created by David Diop on 13.06.2020.
 */

public with sharing class MRO_UTL_EnelEasyServices {
    private static final CompanyDivisionSettings__c companyDivisionSettingsSettings = CompanyDivisionSettings__c.getOrgDefaults();
    private static MRO_QR_CompanyDivision companyDivisionQuery = MRO_QR_CompanyDivision.getInstance();
    static String subProcessCDC = 'Customer Data Change - Enel Easy';
    static String recordTypeCaseCDC = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CustomerDataChange').getRecordTypeId();
    static Map<String, Street__c> streetMapId;

    public static MRO_UTL_EnelEasyServices getInstance() {
        return (MRO_UTL_EnelEasyServices) ServiceLocator.getInstance(MRO_UTL_EnelEasyServices.class);
    }

    public static final Set<String> ALLOWED_FLUX = new Set<String>{
            'OfferRequest',
            'PrintingRequest',
            'CartCheckoutRequest',
            'Accept'
    };

    public static final Map<String, Map<String, String>> ERROR_CODE_MESSAGES_OFFER = new Map<String, Map<String, String>>{
            'Oferta' => new Map<String, String>{
                    'OF001' => 'Trebuie completate ID_fsd si Serviciu',
                    'OF002' => 'Serviciu neidentificat',
                    'OF003' => 'Trebuie specificat nume agent',
                    'OF004' => 'Trebuie specificata marca agent',
                    'OF005' => 'Trebuie specificata Oferta sau oferta nu este valabilă',
                    'OF006' => '******* TODO******',
                    'OF007' => 'Furnizor neidentificat',
                    'OF008' => 'Trebuie specificat Tip contract',
                    'OF009' => 'Trebuie specificat Furnizor',
                    'OF010' => 'Furnizor neidentificat',
                    'OF011' => 'Trebuie specificata Data accept',
                    'OF012' => 'Data accept nu poate fi in viitor',
                    'OF013' => 'Valabilitate de la >= Valabilitate furnizare',
                    'OF014' => 'Trebuie specificata Valabilitate de la',
                    'OF015' => 'Valabilitate de la nu este in intervalul de furnizare al ofertei',
                    'OF016' => 'Trebuie specificata Valabilitate furnizare',
                    'OF017' => 'Valabilitate furnizare nu este in intervalul de furnizare al ofertei',
                    'OF018' => 'Trebuie specificat Tip canal',
                    'OF019' => 'Trebuie specificat Canal',
                    'OF020' => 'Trebuie specificat Kam',
                    'OF021' => 'Trebuie specificat Cu avans',
                    'OF022' => 'Trebuie specificat Ritm facturare',
                    'OF023' => 'La schimbare furnizor trebuie specificate datele pentru notificare',
                    'OF024' => 'Trebuie completate bifele negociere',
                    'OF025' => 'Trebuie completat pret EA',
                    'OF026' => 'Trebuie completat pret EAZ',
                    'OF027' => 'Trebuie completat pret EANO',
                    'OF028' => 'Trebuie completat pret GAZ'

            },
            'ClienteOferta' => new Map<String, String>{
                    'Clf001' => 'Trebuie completat nume client',
                    'Clf002' => 'Trebuie specificat CNP sau COD_FISCAL',
                    'Clf003' => 'Trebuie specificat fie CNP fie COD_FISCAL',
                    'Clf004' => 'CNP invalid',
                    'Clf005' => 'Trebuie completat prenume client',
                    'Clf006' => 'Cod fiscal invalid',
                    'Clf007' => 'Mail client invalid client.email',
                    'Clf008' => 'Trebuie specificata Strada client',
                    'Clf009' => 'Trebuie specificata Localitate + Tip artera + Nume strada client',
                    'Clf010' => 'Localitate client neidentificata',
                    'Clf011' => 'A aparut o eroare tehnica. Va rugam sa incercati din nou mai tarziu.',
                    'Clf012' => 'Strada client neidentificata',
                    'Clf013' => 'Trebuie specificat Numar strada client'
            },
            'Interlocutor' => new Map<String, String>{
                    'Int01' => 'Trebuie specificat Nume si prenume interlocutor',
                    'Int02' => 'Trebuie specificat Nume si prenume interlocutor',
                    'Int03' => 'Mail Interlocutor invalid interlocutor.email',
                    'Int04' => 'Rol interlocutor neidentificat'
            },
            'Corespondenta' => new Map<String, String>{
                    'Crp01' => 'Trebuie specificata Strada corespondenta',
                    'Crp02' => 'Trebuie specificat Numar strada corespondenta',
                    'Crp03' => 'Trebuie specificata Localitate + Tip artera + Nume strada corespondenta',
                    'Crp04' => 'Localitate corespondenta neidentificata',
                    'Crp05' => 'Strada corespondenta neidentificata'
            },
            'Loc' => new Map<String, String>{
                    'Loc01' => 'Locuri fara NLC completat',
                    'Loc02' => 'Trebuie completata Strada loc consum',
                    'Loc03' => 'Strada loc consum neidentificata',
                    'Loc04' => 'Pentru NLC loc.nlc nu este specificat consum pe marimea consumLoc.marime_masurata',
                    'Loc05' => 'Distributie loc.distribuitor neidentificata',
                    'Loc06' => 'NLC loc.nlc este in derulare. Nu poate fi prins pe alta oferta',
                    'Loc07' => 'Nace loc.NACE neidentificata',
                    'Loc08' => 'Distributor should be in the same zone'
            },
            'ConsumLoc' => new Map<String, String>{
                    'CLc01' => 'La consumuri trebuie specificat NLC',
                    'CLc02' => 'La consumuri trebuie specificata marimea masura',
                    'CLc03' => 'Consumuri pe NLC neidentificat in locuri consumLoc.nr_nlc',
                    'CLc04' => 'Consum constituit pe marime neidentificata consumLoc.marime_masurata',
                    'CLc05' => 'La GAZ rebuie completate conventii consum',
                    'CLc06' => 'La EE trebuie completate consum anual si consum lunar'
            },
            'CFEE' => new Map<String, String>{
                    'CFEE01' => 'Trebuie specificata modalitate corespondenta',
                    'CFEE02' => 'Trebuie specificata modalitate restiturie avans',
                    'CFEE03' => 'Trebuie specificata modalitate corespondenta',
                    'CFEE04' => 'Pentru Modalitate corespondenta MAIL trebuie specificat Email',
                    'CFEE05' => 'Pentru Modalitate corespondenta cfee.mod_corespondenta NU trebuie specificat Email',
                    'CFEE06' => 'Modalitate restiturie avans cfee.mod_avans neidentificata',
                    'CFEE07' => 'Pentru Modalitate restiturie avans BANCA trebuie specificat IBAN',
                    'CFEE08' => 'Pentru Modalitate restiturie avans cfee.mod_avans NU trebuie specificat IBAN',
                    'CFEE09' => 'Pentru Modalitate restiturie avans cfee.mod_avans NU trebuie specificat Eneltel',
                    'CFEE10' => 'Eneltel invalid specificat pentru Modalitate restiturie avans FACTURA',
                    'CFEE11' => 'Mail CFEE invalid cfee.email'
            },
            'GDPR' => new Map<String, String>{
                    'GDPR01' => 'Daca are acord GDPR trebuie specificat cel putin unul din canale',
                    'GDPR02' => 'Mail GDPR invalid gdpr.email',
                    'GDPR03' => 'Trebuie specificate Tip artera + Nume strada GDPR',
                    'GDPR04' => 'Trebuie specificat Numar strada GDPR',
                    'GDPR05' => 'Localitate GDPR neidentificata',
                    'GDPR06' => 'A aparut o eroare tehnica. Va rugam sa incercati din nou mai tarziu.',
                    'GDPR07' => 'Trebuie specificat Numar strada GDPR',
                    'GDPR08' => 'Strada GDPR neidentificata'
            },
            'MISC' => new Map<String, String>{
                    'MISC001' => 'Trebuie transmis document Act proprietate',
                    'MISC002' => 'NLC cu furnizare activa si nu este preluare',
                    'MISC003' => 'POD distributie proprie nu poate fi cu GAZ',
                    'MISC004' => 'POD distributie proprie EDM si alt distribuitor',
                    'MISC005' => 'POD distributie proprie EDD si alt distribuitor',
                    'MISC006' => 'POD distributie proprie EDB si alt distribuitor',
                    'MISC007' => 'POD distributie proprie neidentificat',
                    'MISC008' => 'Distributie proprie si POD invalid',
                    'MISC009' => 'Furnizare energie in judet cu distributie proprie si POD invalid loc.nlc',
                    'MISC010' => 'Eneltel invalid',
                    'MISC011' => 'Cont IBAN invalid',
                    'MISC012' => 'Trebuie specificate locuri consum',
                    'MISC013' => 'Trebuie specificate consumurile de la toate locuri de consum',
                    'MISC014' => 'id_variante_pret is not valid or not exists',
                    'MISC015' => 'Product change and Transfer are not allowed for inactive PODs'
            },
            'Document' => new Map<String, String>{
                    'DOC001' => 'Tipuri document neidentificate',
                    'DOC002' => 'Trebuie transmis document Carte identitate sau Buletin',
                    'DOC003' => 'Trebuie transmis document Certificat fiscal',
                    'DOC004' => 'Trebuie transmis document Act proprietate',
                    'DOC005' => 'Sunt tipuri de document neacceptate'
            },
            'CheckOut' => new Map<String, String>{
                    'CHK001' => 'Opportunity id invalid'
            }
    };

    /**
    * @param servicePointCode
    * @description get ServicePoint by the code.
    * @create 10/06/2020
    * @return ServicePoint__c
    */
    public ServicePoint__c getByCode(String servicePointCode) {
        List<ServicePoint__c> servicePoints = [
                SELECT Id,Name,Trader__c,EstimatedConsumption__c,ContractualPower__c,AvailablePower__c,
                        Distributor__c,Account__c,VoltageLevel__c,PowerPhase__c,Voltage__c,ConversionFactor__c,FirePlacesCount__c,
                        Pressure__c,PressureLevel__c,Code__c,ConsumptionCategory__c,FlowRate__c,
                        PointStreetName__c,PointStreetNumber__c,PointStreetNumberExtn__c,PointPostalCode__c,
                        PointCity__c,PointCountry__c,PointStreetType__c,PointApartment__c,PointBuilding__c,
                        PointLocality__c,PointProvince__c,PointFloor__c,PointAddressNormalized__c,PointAddress__c,
                        PointAddressKey__c,PointStreetId__c,ATRExpirationDate__c,IsNewConnection__c,PointBlock__c,ENELTEL__c,CurrentSupply__c,
                        CurrentSupply__r.ServiceSite__r.NLC__c,CurrentSupply__r.Status__c,CurrentSupply__r.Market__c
                FROM ServicePoint__c
                WHERE Code__c = :servicePointCode
                LIMIT 1
        ];
        return servicePoints.isEmpty() ? null : servicePoints.get(0);
    }

    public List<ServicePoint__c> getListServicePointsByCodes(Set<String> codes) {
        List<ServicePoint__c> servicePoints = [
                SELECT Name,Trader__c,EstimatedConsumption__c,ContractualPower__c,AvailablePower__c,
                        Distributor__c,Account__c,VoltageLevel__c,PowerPhase__c,Voltage__c,ConversionFactor__c,FirePlacesCount__c,
                        Pressure__c,PressureLevel__c,Code__c,ConsumptionCategory__c,FlowRate__c,
                        PointStreetName__c,PointStreetNumber__c,PointStreetNumberExtn__c,PointPostalCode__c,
                        PointCity__c,PointCountry__c,PointStreetType__c,PointApartment__c,PointBuilding__c,
                        PointLocality__c,PointProvince__c,PointFloor__c,PointAddressNormalized__c,PointAddress__c,
                        PointAddressKey__c,PointStreetId__c,ATRExpirationDate__c,IsNewConnection__c,PointBlock__c,ENELTEL__c,CurrentSupply__c,
                        CurrentSupply__r.ServiceSite__r.NLC__c,CurrentSupply__r.Status__c,CurrentSupply__r.Market__c
                FROM ServicePoint__c
                WHERE Code__c IN :codes
                LIMIT 10
        ];
        return /*servicePoints.isEmpty() ? null :*/ servicePoints;
    }

    /**
    * @description Query necessary fields for EnelEasy service
    * @return Account accountObject
    */
    public Contract getContractByIntegrationKey(String integrationKey) {
        List<Contract> contractList = [
                SELECT Id,IntegrationKey__c,StartDate,Salesman__c,CustomerSignedDate,ContractType__c
                FROM Contract
                WHERE IntegrationKey__c = :integrationKey
                LIMIT 1
        ];
        return contractList.isEmpty() ? null : contractList.get(0);
    }

    /**
    * @description Query necessary fields for
    * @return Account accountObject
    */
    public Account getAccountByIntegrationKey(String integrationKey) {
        List<Account> accountList = [
                SELECT Id,VATNumber__c,IntegrationKey__c,IsDisCoENEL__c,Name
                FROM Account
                WHERE IntegrationKey__c = :integrationKey
                LIMIT 1
        ];
        return accountList.isEmpty() ? null : accountList.get(0);
    }

    public Map<String, Account> getListAccountByIntegrationKey(List<String> integrationKeys) {
        Map<String, Account> mapDistributorId = new Map<String, Account>();
        List<Account> accountList = [
                SELECT Id,VATNumber__c,IntegrationKey__c,IsDisCoENEL__c,Name
                FROM Account
                WHERE IntegrationKey__c IN :integrationKeys
                LIMIT 10
        ];
        for (Account distributor : accountList) {
            mapDistributorId.put(distributor.IntegrationKey__c, distributor);
        }
        return mapDistributorId;
    }

    /**
    * @description Query necessary fields for
    * @return Dossier accountObject
    */
    public Dossier__c getDossierById(String dossierId) {
        List<Dossier__c> dossierList = [
                SELECT Id, Name, Status__c, RecordTypeId,
                        RecordType.DeveloperName, Commodity__c,Email__c,
                        Phone__c, SendingChannel__c, RequestType__c, SubProcess__c,
                        Phase__c, Origin__c, Channel__c,Account__c
                FROM Dossier__c
                WHERE Id = :dossierId
                LIMIT 1
        ];
        return dossierList.isEmpty() ? null : dossierList.get(0);
    }

    /**
     * @param streetId street
     * @param cityId city
     *
     * @return Street information
     */
    public Street__c getAddressInfosById(String streetId, String cityId) {
        return [
                SELECT
                        City__r.Province__c,
                        City__r.Name,
                        City__r.Country__c,
                        StreetType__c,
                        Name,
                        PostalCode__c
                FROM Street__c
                WHERE Id = :streetId AND City__r.Id = :cityId
                LIMIT 1
        ];
    }

    /**
    * @param streetIdSet
    * @return Street information
     */
    public Map<String, Street__c> getListAddressInfosById(Set<String> streetIdSet) {
        Map<String, Street__c> streetMapId = new Map<String, Street__c>();
        List<Street__c> streetList = [
                SELECT
                        Id,
                        City__r.Province__c,
                        City__r.Name,
                        City__r.Country__c,
                        StreetType__c,
                        Name,
                        PostalCode__c
                FROM Street__c
                WHERE Id IN :streetIdSet
        ];
        for (Street__c street : streetList) {
            streetMapId.put(street.Id, street);
        }
        return streetMapId;
    }

    //add by David diop
    public Street__c getStreetById(String recordId) {
        return [
                SELECT Id
                FROM Street__c
                WHERE Id = :recordId
        ];
    }
    //add by David diop
    public City__c getCityById(String recordId) {
        return [
                SELECT Id
                FROM City__c
                WHERE Id = :recordId
        ];
    }

    //add by David diop
    public List<ServiceSite__c> getServiceSiteByNlc(String nlcReference) {
        return [
                SELECT Id, Name, Status__c,NLC__c
                FROM ServiceSite__c
                WHERE NLC__c = :nlcReference
        ];
    }

    //add by David diop
    public static Map<String, Object> checkCompanyDivisions() {
        String enelEnergiaMuntenia = companyDivisionSettingsSettings.EnelEnergieMunteniaVAT__c;
        String enelEnergie = companyDivisionSettingsSettings.EnelEnergieMunteniaVAT__c;
        String enelEnergieCompanyDivisionId = '';
        Object munteniaCompanyDivision ;
        Map<String, Object> response = new Map<String, Object>();
        List<CompanyDivision__c> listCompanyDivisions = companyDivisionQuery.getAll();
        for (CompanyDivision__c companyDivision : listCompanyDivisions) {
            if (enelEnergiaMuntenia == companyDivision.VATNumber__c) {
                munteniaCompanyDivision = companyDivision;
            }
            if (enelEnergie == companyDivision.VATNumber__c) {
                enelEnergieCompanyDivisionId = companyDivision.Id;
            }
        }

        response.put('companyDivisionMuntenia', munteniaCompanyDivision);
        response.put('enelEnergiaCompanyDivisionId', enelEnergieCompanyDivisionId);
        return response;
    }
    /**
     * @description instantiate a new OSI from Supply Data
     * @param opportunityId
     * @param supply
     * @param point
     * @param commodity
     * @param applicantNotHolder
     * @param product2Id
     * @return Opportunity service Item
     */
    public OpportunityServiceItem__c instantiateOppServiceItem(String opportunityId, Supply__c supply, ServicePoint__c point, String commodity, Boolean applicantNotHolder, String product2Id) {
        String recordTypeId = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get(commodity).getRecordTypeId();
        OpportunityServiceItem__c osi = new OpportunityServiceItem__c();
        osi.Opportunity__c = opportunityId;
        osi.RecordTypeId = recordTypeId;
        osi.Account__c = supply.Account__c;
        osi.NACEReference__c = supply.NACEReference__c;
        osi.ApplicantNotHolder__c = applicantNotHolder;
        osi.Product__c = product2Id;
        //osi.ServicePoint__c = supply.ServicePoint__c;
        //osi.ServiceSite__c = supply.ServiceSite__c;
        osi.ContractAccount__c = supply.ContractAccount__c;
        //osi.Product__c = supply.Product__c;
        osi.ServicePoint__c = point.Id;
        osi.ServicePointCode__c = point.Code__c;
        osi.PointAddressNormalized__c = point.PointAddressNormalized__c;
        osi.Distributor__c = point.Distributor__c;
        osi.Trader__c = point.Trader__c;
        osi.PointStreetId__c = point.PointStreetId__c;
        osi.PointStreetType__c = point.PointStreetType__c;
        osi.PointStreetName__c = point.PointStreetName__c;
        osi.PointStreetNumber__c = point.PointStreetNumber__c;
        osi.PointStreetNumberExtn__c = point.PointStreetNumberExtn__c;
        osi.PointApartment__c = point.PointApartment__c;
        osi.PointBuilding__c = point.PointBuilding__c;
        osi.PointBlock__c = point.PointBlock__c;
        osi.PointCity__c = point.PointCity__c;
        osi.PointCountry__c = point.PointCountry__c;
        osi.PointFloor__c = point.PointFloor__c;
        osi.PointLocality__c = point.PointLocality__c;
        osi.PointPostalCode__c = point.PointPostalCode__c;
        osi.PointProvince__c = point.PointProvince__c;
        osi.PointAddressNormalized__c = point.PointAddressNormalized__c;
        osi.Voltage__c = point.Voltage__c;
        osi.VoltageSetting__c = point.Voltage__c != null ? String.valueOf((point.Voltage__c * 1000).intValue()) : null;
        osi.VoltageLevel__c = point.VoltageLevel__c;
        osi.PressureLevel__c = point.PressureLevel__c;
        osi.Pressure__c = point.Pressure__c;
        osi.PowerPhase__c = point.PowerPhase__c;
        osi.ConversionFactor__c = point.ConversionFactor__c;
        osi.ContractualPower__c = point.ContractualPower__c;
        osi.AvailablePower__c = point.AvailablePower__c;
        osi.EstimatedConsumption__c = point.EstimatedConsumption__c;
        //FF OSI Edit - Pack2 - Interface Check
        osi.ConsumptionCategory__c = point.ConsumptionCategory__c;
        osi.FlowRate__c = point.FlowRate__c;
        osi.IsNewConnection__c = point.IsNewConnection__c;
        osi.SupplyOperation__c = 'Permanent';

        osi.SiteStreetId__c = point.PointStreetId__c;
        osi.SiteStreetType__c = point.PointStreetType__c;
        osi.SiteStreetName__c = point.PointStreetName__c;
        osi.SiteStreetNumber__c = point.PointStreetNumber__c;
        osi.SiteStreetNumberExtn__c = point.PointStreetNumberExtn__c;
        osi.SiteApartment__c = point.PointApartment__c;
        osi.SiteBuilding__c = point.PointBuilding__c;
        osi.SiteBlock__c = point.PointBlock__c;
        osi.SiteCity__c = point.PointCity__c;
        osi.SiteCountry__c = point.PointCountry__c;
        osi.SiteFloor__c = point.PointFloor__c;
        osi.SiteLocality__c = point.PointLocality__c;
        osi.SitePostalCode__c = point.PointPostalCode__c;
        osi.SiteProvince__c = point.PointProvince__c;
        osi.SiteDescription__c = 'Locuinta';
        osi.SiteAddressKey__c = point.PointAddressKey__c;
        osi.SiteAddressNormalized__c = point.PointAddressNormalized__c;

        if (commodity == 'Electric') {
            osi.NLC__c = point.Code__c;
        } else {
            osi.CLC__c = point.Code__c;
        }
        return osi;
    }
    /**
     * @description instantiate a new dossier for CDC (Customer Data Change)
     * @param accountId
     * @param status
     * @param channel
     * @return Dossier__c
     */
    public Dossier__c instantiateDossierForCDC(String accountId, String status, String channel) {
        String recordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(MRO_UTL_Constants.CHANGE).getRecordTypeId();
        Dossier__c dossier = new Dossier__c();
        dossier.Account__c = accountId;
        dossier.RecordTypeId = recordTypeId;
        dossier.SubProcess__c = subProcessCDC;
        dossier.Status__c = status;
        dossier.Channel__c = channel;
        dossier.Origin__c = MRO_UTL_Constants.ORIGIN_ENEL_EASY;
        dossier.RequestType__c = 'CustomerDataChange';
        return dossier;
    }

    /**
     * @description instantiate a new case for CDC (Customer Data Change)
     * @param account
     * @param status
     * @param dossierId
     * @return Case
     */
    public Case instantiateCaseForCDC(Account account, String status, String dossierId) {
        Case caseRecord = new Case();
        caseRecord.AccountId = account.Id;
        caseRecord.Dossier__c = dossierId;
        caseRecord.RecordTypeId = recordTypeCaseCDC;
        caseRecord.Status = status;
        caseRecord.AccountFirstName__c = account.FirstName;
        caseRecord.AccountLastName__c = account.LastName;
        caseRecord.AccountName__c = account.Name;
        caseRecord.AccountNationalIdentityNumber__c = account.NationalIdentityNumber__pc;
        caseRecord.AccountVATNumber__c = account.VATNumber__c;
        caseRecord.AccountONRCCode__c = account.ONRCCode__c;
        caseRecord.AddressStreetId__c = account.ResidentialStreetId__c;
        caseRecord.AddressStreetName__c = account.ResidentialStreetName__c;
        caseRecord.AddressStreetType__c = account.ResidentialStreetType__c;
        caseRecord.AddressPostalCode__c = account.ResidentialPostalCode__c;
        caseRecord.AddressStreetNumber__c = account.ResidentialStreetNumber__c;
        caseRecord.AddressBlock__c = account.ResidentialBlock__c;
        caseRecord.AddressBuilding__c = account.ResidentialBuilding__c;
        caseRecord.AddressApartment__c = account.ResidentialApartment__c;
        caseRecord.AddressCity__c = account.ResidentialCity__c;
        caseRecord.AddressFloor__c = account.ResidentialFloor__c;
        caseRecord.AccountEmail__c = account.PersonEmail;
        caseRecord.AccountPhone__c = account.Phone;
        return caseRecord;
    }

    public NE__Catalog_Item__c getProduct(String productItemId) {
        List<NE__Catalog_Item__c> catalogItems = [
                SELECT NE__ProductId__c,NE__Prodotto__c,NE__Prodotto__r.Name,
                        NE__ProductId__r.Combo__c, NE__ProductId__r.MarketType__c,NE__ProductId__r.ProductTerm__c
                FROM NE__Catalog_Item__c
                WHERE Id = :productItemId
        ];

        return catalogItems.isEmpty() ? null : catalogItems.get(0);
    }

    public Product2 getProduct2FromCommercialProduct(String commercialProductId) {
        List<Product2> products = [SELECT Id FROM Product2 WHERE CommercialProduct__c = :commercialProductId];

        return products.isEmpty() ? null : products.get(0);
    }

    public void updateChainObjectAfterCheckOut(String oppId, OpportunityLineItem oli) {
        Opportunity myOpp = [
                SELECT Id,ContractId,Pricebook2Id,
                        SalesUnit__c,Salesman__c,RequestedStartDate__c
                FROM Opportunity
                WHERE Id = :oppId
        ];

        Contract myContractToUpdate = new Contract();
        myContractToUpdate.Id = myOpp.ContractId;
        myContractToUpdate.CommodityProduct__c = oli.Product2Id;
        myContractToUpdate.ProductStartDate__c = myOpp.RequestedStartDate__c;
        myContractToUpdate.Pricebook2Id = myOpp.Pricebook2Id;
        myContractToUpdate.SalesUnit__c = myOpp.SalesUnit__c;
        myContractToUpdate.Salesman__c = myOpp.Salesman__c;

        myOpp.StageName = 'Closed Won';
        update myOpp;
        update myContractToUpdate;
    }

    /**
    * @param streetId
    * @return Street information
     */
    public static Street__c getAddressInfosById(String streetId) {
        if (streetMapId == null || !streetMapId.containsKey(streetId)) {
            if (streetMapId == null) {
                streetMapId = new Map<String, Street__c>();
            }

            List<Street__c> streetList = [
                    SELECT
                            Id,
                            City__r.Id,
                            City__r.Province__c,
                            City__r.Name,
                            City__r.Country__c,
                            StreetType__c,
                            Name,
                            PostalCode__c
                    FROM Street__c
                    WHERE Id = :streetId
            ];
            for (Street__c street : streetList) {
                streetMapId.put(street.Id, street);
            }
        }
        return streetMapId.get(streetId);
    }
}