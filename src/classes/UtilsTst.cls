/**
 * Test class for UtilsTst
 *
 * @author Moussa Fofana
 * @version 1.0
 * @description Test class for Utils
 * @uses
 * @code
 * @history
 * 2019-06-12:  Moussa Fofana
 * 2019-29-11:  David Diop
 */
@isTest
public with sharing class UtilsTst {
    @testSetup
   static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        Account myAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        myAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert myAccount;

        Account distributor = new Account();
        distributor.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        distributor.Name = 'distributor';
        distributor.AtoAEnabled__c =false;
        insert distributor;

        Account myTraderAccount = MRO_UTL_TestDataFactory.account().traderAccount().build();
        myTraderAccount.Name = 'Trader interlocutor';
        myTraderAccount.BusinessType__c = 'Commercial areas';
        insert myTraderAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = myAccount.Id;
        insert billingProfile;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        contractAccount.BillingProfile__c = billingProfile.Id;
        insert contractAccount;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder()
                .setAccount(myAccount.Id)
                .setCompany(companyDivision.Id)
                .setContractAccount(contractAccount.Id)
                .build();
        supply.RecordTypeId = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert supply;

        ServicePoint__c sp = MRO_UTL_TestDataFactory.servicePoint().createServicePointEle().build();
        sp.Account__c = myAccount.Id;
        sp.Code__c = 'ServicePointCode';
        sp.Distributor__c = distributor.Id;
        sp.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert sp;

    }

    @isTest
    public static void getAllConstantsTest(){
        Test.startTest();
        Object response = TestUtils.exec(
                'Utils', 'getAllConstants', null, true);
        system.assert(true);
        Test.stopTest();
    }
    @IsTest
    public static void findObjectAPINameTest(){
        Test.startTest();
        Account account = [
            SELECT Id
            FROM Account
            WHERE Name != 'Trader interlocutor'
            LIMIT 1
        ];
            Map<String, String > inputJSON = new Map<String, String>{
        'objId' => account.Id
            };
        Object response = TestUtils.exec(
            'Utils', 'findObjectAPIName', inputJSON, true);
        System.assertEquals(true , response== 'Account');
        Test.stopTest();
    }

    @IsTest
    public static void findObjectAPINameNullTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'objId' => null
        };
        Object response = TestUtils.exec(
            'Utils', 'findObjectAPIName', inputJSON, true);
        System.assertEquals(true , response== null);
        Test.stopTest();
    }

    @IsTest
    public static void findObjectAPINameByIdTest(){
        Test.startTest();
        Account account = [
            SELECT Id
            FROM Account
            WHERE Name != 'Trader interlocutor'
            LIMIT 1
        ];
        String response = Utils.findObjectAPINameById(account.Id);
        System.assertEquals(true , response == 'Account');
        Test.stopTest();
    }
     @IsTest
     public static void findObjectAPINameByIdNullTest(){
         Test.startTest();
         String response = Utils.findObjectAPINameById(null);
         System.assertEquals(true , response== null);
         Test.stopTest();
     }

    @IsTest
    public static void getLookupFieldsTest() {
        Test.startTest();
        String objName = 'Account';
        List<String> response = Utils.getLookupFields(objName);
        System.assertEquals(true, response.size() != 0);
        Test.stopTest();
    }

    @IsTest
    public static void passiveSwitchOutTest() {
        Test.startTest();
        Account account = [
                SELECT Id
                FROM Account
                WHERE Name != 'Trader interlocutor'
                LIMIT 1
        ];
        Account traderAccount = [
                SELECT Id
                FROM Account
                WHERE Name = 'Trader interlocutor'
                LIMIT 1
        ];
        Date dateDay = System.today();
        Map<String, Map<String, Date>> mapServicePointWithTraderIdAndDate = new Map<String, Map<String, Date>>();
        Map<String, Date> mapDate1 = new Map<String, Date>();
        mapDate1.put(traderAccount.Id,dateDay);
        mapServicePointWithTraderIdAndDate.put('',mapDate1);
        Object response = Utils.passiveSwitchOut(account.Id,mapServicePointWithTraderIdAndDate,true);
        System.assertEquals(true, response != null);
        Test.stopTest();
    }

    @IsTest
    public static void passiveSwitchOutNoServicePointTest() {

        Test.startTest();
        Account account = [
                SELECT Id
                FROM Account
                WHERE Name != 'Trader interlocutor'
                LIMIT 1
        ];
        Account traderAccount = [
                SELECT Id
                FROM Account
                WHERE Name = 'Trader interlocutor'
                LIMIT 1
        ];

        ServicePoint__c servPoint = [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,CurrentSupply__r.ContractAccount__r.BillingProfile__c,Code__c,CurrentSupply__r.Account__c,
                        CurrentSupply__r.CompanyDivision__c,CurrentSupply__r.RecordType.DeveloperName
                FROM ServicePoint__c
                    WHERE Code__c = 'ServicePointCode'
                    LIMIT 1
        ];
        System.debug('#--# '+servPoint);
        Date dateDay = System.today();
        Map<String, Map<String, Date>> mapServicePointWithTraderIdAndDate = new Map<String, Map<String, Date>>();
        Map<String, Date> mapDate1 = new Map<String, Date>();
        mapDate1.put(traderAccount.Id,dateDay);
        mapServicePointWithTraderIdAndDate.put(servPoint.Code__c,mapDate1);
        Object response = Utils.passiveSwitchOut(account.Id,mapServicePointWithTraderIdAndDate,true);
        System.assertEquals(true, response != null);
        Test.stopTest();
    }
}