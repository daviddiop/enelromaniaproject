/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 27.03.20.
 */

public with sharing class MRO_LC_GDPRRequestWizard extends ApexServiceLibraryCnt {
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();

    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();

    private static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    private static final String personalRT = Label.GdprPersonalRT;
    private static final String marketingRT = Label.GdprMarketingRT;
    private static final String forgottenRT = Label.GdprForgottenRT;
    private static final String updateRT = Label.GdprUpdateRT;
    private static final String portabilityRT = Label.GdprPortabilityRT;
    private static final String profilesRT = Label.GdprProfilesRT;
    private static final String processingRT = Label.GdprProcessingRT;

    private static final List<String> requestTypeToShowPrivacyChange = new List<String>{ marketingRT, profilesRT };
    private static final List<String> requestTypeToShowCustomerNeeds = new List<String>{ processingRT };

    private static final List<Utils.KeyVal> customerNeedsOptions {
        get {
            List<Utils.KeyVal> values = new List<Utils.KeyVal>();
            for (Schema.PicklistEntry pckEntry : Case.CustomerNeeds__c.getDescribe().getPicklistValues()) {
                values.add(new Utils.KeyVal(pckEntry.getValue(), pckEntry.getLabel()));
            }
            return values;
        }
    }

    private static final List<Utils.KeyVal> requestTypeOptions {
        get {
            List<Utils.KeyVal> values = new List<Utils.KeyVal>();
            values.add(new Utils.KeyVal(personalRT, personalRT));
            values.add(new Utils.KeyVal(marketingRT, marketingRT));
            values.add(new Utils.KeyVal(forgottenRT, forgottenRT));
            values.add(new Utils.KeyVal(updateRT, updateRT));
            values.add(new Utils.KeyVal(portabilityRT, portabilityRT));
            values.add(new Utils.KeyVal(profilesRT, profilesRT));
            values.add(new Utils.KeyVal(processingRT, processingRT));
            return values;
        }
    }


    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                response.put('error', true);
                response.put('errorMsg', System.Label.Account + ' - ' + System.Label.MissingId);
                return response;
            }

            Account acc = accQuery.findAccount(accountId);


            Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'GDPRRequest');

            if(!String.isBlank(genericRequestId)){
                dossierSr.updateParentGenericRequest(dossier.Id, genericRequestId);
            }

            List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
            if (cases.size() > 0) {
                response.put('caseId', cases[0].Id);
                response.put('case', cases[0]);
            }

            response.put('isClosed', dossier.Status__c != constantsSrv.DOSSIER_STATUS_DRAFT);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('customerNeedsOptions', customerNeedsOptions);
            response.put('requestTypeOptions', requestTypeOptions);
            response.put('requestTypeToShowPrivacyChange', requestTypeToShowPrivacyChange);
            response.put('requestTypeToShowCustomerNeeds', requestTypeToShowCustomerNeeds);
            response.put('error', false);

            return response;
        }
    }

    public class RequestParams {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String requestType { get; set; }
        @AuraEnabled
        public String customerNeeds { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
    }

    public class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams channelOriginParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);

            dossierSr.updateDossierChannel(channelOriginParams.dossierId, channelOriginParams.channelSelected, channelOriginParams.originSelected);

            return response;
        }
    }

    public class upsertCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams upsertCaseParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);

            Case newCase;
            Account acc = accQuery.findAccount(upsertCaseParams.accountId);

            if (upsertCaseParams.caseId == null) {
                Map<String, String> caseRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('GDPRRequest');
                newCase = new Case(
                        AccountId = upsertCaseParams.accountId,
                        RecordTypeId = (String) caseRecordTypes.get('GDPRRequest'),
                        Dossier__c = upsertCaseParams.dossierId,
                        Status = constantsSrv.CASE_STATUS_DRAFT,
                        Origin = upsertCaseParams.originSelected,
                        Channel__c = upsertCaseParams.channelSelected,
                        Phase__c = constantsSrv.CASE_START_PHASE,
                        SubProcess__c = upsertCaseParams.requestType,
                        Priority = 'High',

                        AccountName__c = acc.Name,
                        AccountNationalIdentityNumber__c = acc.NationalIdentityNumber__pc,
                        AccountPhone__c = acc.Phone,
                        AccountEmail__c = acc.Email__c,
                        AccountFirstName__c = acc.FirstName,
                        AccountLastName__c = acc.LastName,
                        AccountNACECode__c = acc.NACECode__c,
                        AccountNACEReference__c = acc.NACEReference__c,
                        AccountONRCCode__c = acc.ONRCCode__c,
                        AccountVATNumber__c = acc.VATNumber__c,
                        AddressAddressKey__c = acc.ResidentialAddressKey__c,
                        AddressApartment__c = acc.ResidentialApartment__c,
                        AddressBlock__c = acc.ResidentialBlock__c,
                        AddressBuilding__c = acc.ResidentialBuilding__c,
                        AddressCity__c = acc.ResidentialCity__c,
                        AddressCountry__c = acc.ResidentialCountry__c,
                        AddressFloor__c = acc.ResidentialFloor__c,
                        AddressLocality__c = acc.ResidentialLocality__c,
                        AddressPostalCode__c = acc.ResidentialPostalCode__c,
                        AddressProvince__c = acc.ResidentialProvince__c,
                        AddressStreetId__c = acc.ResidentialStreetId__c,
                        AddressStreetName__c = acc.ResidentialStreetName__c,
                        AddressStreetNumber__c = acc.ResidentialStreetNumber__c,
                        AddressStreetNumberExtn__c = acc.ResidentialStreetNumberExtn__c,
                        AddressStreetType__c = acc.ResidentialStreetType__c
                );
            }else{
                newCase = caseQuery.getById(upsertCaseParams.caseId);
                newCase.SubProcess__c = upsertCaseParams.requestType;
                newCase.CustomerNeeds__c = upsertCaseParams.customerNeeds;
            }

            databaseSrv.upsertSObject(newCase);
            response.put('caseId', newCase.Id);
            response.put('case', caseQuery.getById(newCase.Id));

            return response;
        }
    }

    public class CancelProcessParams {
        @AuraEnabled
        public String dossierId { get; set; }
    }

    public class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CancelProcessParams cancelProcessParams = (CancelProcessParams) JSON.deserialize(jsonInput, CancelProcessParams.class);
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                databaseSrv.upsertSObject(new Dossier__c(
                        Id = cancelProcessParams.dossierId,
                        Status__c = constantsSrv.DOSSIER_STATUS_CANCELED
                ));
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class SaveProcessParams {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public Boolean isDraft { get; set; }
    }

    public class saveProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveProcessParams saveProcessParams = (SaveProcessParams) JSON.deserialize(jsonInput, SaveProcessParams.class);
            Map<String, Object> response = new Map<String, Object>();
            try {
                Dossier__c dossier = new Dossier__c(
                        Id = saveProcessParams.dossierId,
                        Status__c = saveProcessParams.isDraft ? constantsSrv.DOSSIER_STATUS_DRAFT : constantsSrv.DOSSIER_STATUS_NEW
                );
                databaseSrv.upsertSObject(dossier);
                if(saveProcessParams.caseId != null && !saveProcessParams.isDraft){
                    Case c = caseQuery.getById(saveProcessParams.caseId);
                    c.Status = constantsSrv.CASE_STATUS_NEW;
                    databaseSrv.upsertSObject(c);

                    databaseSrv.upsertSObject(new Task(
                            Subject = 'SLA Due Date',
                            WhatId = saveProcessParams.caseId,
                            Priority = 'High',
                            Status = 'Open',
                            ActivityDate = c.SLAExpirationDate__c
                    ));
                    MRO_SRV_PrivacyChange.getInstance().commitPrivacyChange(dossier);
                    if(String.isNotEmpty(saveProcessParams.dossierId)) {
                        MRO_SRV_Case.getInstance().applyAutomaticTransitionOnDossierAndCases(saveProcessParams.dossierId);
                    }
                }
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}