/**
 *
 *
 * @author Salvatore Pignanelli
 * @version 1.0
 * @description
 * @history
 * 01/12/2020 : Salvatore Pignanelli
*/
@IsTest
public with sharing class MRO_QR_ContentVersionTst {

    @TestSetup
    private static void setup() {

        insert TestDataFactory.contentVersion().build();
    }

    @IsTest
    static void testGetInvoiceDetails() {

        List<ContentVersion> cvs = [SELECT Id FROM ContentVersion];
        List<String> cvIds = new List<String>();

        for (ContentVersion cv : cvs ) {

            cvIds.add(String.valueOf(cv.Id));
        }

        Test.startTest();
        MRO_QR_ContentVersion.getInstance().findById(cvIds);
        Test.stopTest();
    }
}