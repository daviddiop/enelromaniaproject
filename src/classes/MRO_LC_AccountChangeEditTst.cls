/**
 * @author  Luca Ravicini
 * @since   May 27, 2020
 * @desc   Test for MRO_LC_AccountChangeEdit class
 *
 */

@IsTest
private class MRO_LC_AccountChangeEditTst {
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();

    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 1000.0,SequenceLength__c = 7.0);
        insert sequencer;
        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder =  MRO_UTL_TestDataFactory.account().personAccount();
        Account acc = AccountBuilder.build();
        Database.SaveResult[] lsr = Database.insert(new Account[]{acc},false);
        Dossier__c dossier1 = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(constantsUtl.REQUEST_TYPE_CUSTOMER_DATA_CHANGE).build();
        insert dossier1;

        Dossier__c dossier2 = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(constantsUtl.REQUEST_TYPE_CUSTOMER_DATA_CHANGE).build();
        insert dossier2;
        Case case1 = MRO_UTL_TestDataFactory.caseRecordBuilder().newCase().build();
        case1.Dossier__c = dossier2.Id;
        insert case1;
    }

    @IsTest
    static void initializeEmptyIds() {
        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => '',
                'dossierId' => ''
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_AccountChangeEdit', 'initialize', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void initializeInvalidIds() {
        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => '1223',
                'dossierId' => '1112'
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_AccountChangeEdit', 'initialize', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void initializeEmptyParameters() {
        Map<String, String> inputJSON = new Map<String, String>{};
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_AccountChangeEdit', 'initialize', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }
    @IsTest
    static void initializeEmptyDossier() {
        try {
            String accountId = [
                    SELECT Id
                    FROM Account
            ].Id;

            Map<String, String> inputJSON = new Map<String, String>{
                    'accountId' => accountId
            };
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_AccountChangeEdit', 'initialize', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void initialize() {
       String accountId = [
                SELECT Id
                FROM Account
        ].Id;

        Case myCase = [
                SELECT Id, Dossier__c
                FROM Case
        ];

        String dossierId = [
                SELECT Id
                FROM Dossier__c WHERE Id !=: myCase.Dossier__c
        ].Id;
        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => accountId,
                'dossierId' => dossierId
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_AccountChangeEdit', 'initialize', inputJSON, true);
            Test.stopTest();
            System.assertEquals(false, response.get('error'));
            System.assertEquals(null, response.get('caseId'));
            Account account = (Account)response.get('account');
            System.assertEquals(accountId, account.Id);
            System.assertEquals(MRO_LC_AccountChangeEdit.VAT_PREFIX, response.get('vatPrefix'));
            System.assertEquals(constantsUtl.CASE_DEFAULT_CASE_ORIGIN, response.get('defaultCaseOrigin'));
            System.assertEquals(constantsUtl.CASE_STATUS_DRAFT, response.get('defaultCaseStatus'));
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void initializeWithCase() {
        String accountId = [
                SELECT Id
                FROM Account
        ].Id;

        Case myCase = [
                SELECT Id, Dossier__c
                FROM Case
        ];

        String dossierId = [
                SELECT Id
                FROM Dossier__c WHERE Id =: myCase.Dossier__c
        ].Id;
        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => accountId,
                'dossierId' => dossierId
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_AccountChangeEdit', 'initialize', inputJSON, true);
            Test.stopTest();
            System.assertEquals(false, response.get('error'));
            System.assertEquals(myCase.Id, response.get('caseId'));
            Account account = (Account)response.get('account');
            System.assertEquals(accountId, account.Id);
            System.assertEquals(MRO_LC_AccountChangeEdit.VAT_PREFIX, response.get('vatPrefix'));
            System.assertEquals(constantsUtl.CASE_DEFAULT_CASE_ORIGIN, response.get('defaultCaseOrigin'));
            System.assertEquals(constantsUtl.CASE_STATUS_DRAFT, response.get('defaultCaseStatus'));
        } catch (Exception exc) {

        }
    }

}