/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
 *         Baba Goudiaby - Refactoring
* @since   28/02/2020
* @desc
* @history
*/
global with sharing class MRO_WS_InternetInterfaceServices {
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();
    static String channelRequest = MRO_UTL_Constants.CHANNEL_ENEL_RO;
    static String originRequest = MRO_UTL_Constants.ORIGIN_WEB;
    static Map<String, String> errorMessagesFormulaSite = MRO_UTL_InternetInterfaceServices.ERROR_CODE_MESSAGES_FORMULA_SITE;
    static Map<String, String> errorMessagesAddDocument = MRO_UTL_InternetInterfaceServices.ERROR_CODE_MESSAGES_ADD_DOCUMENT;
    static Set<String> allowedFlux = MRO_UTL_InternetInterfaceServices.ALLOWED_FLUX;
    static Set<String> allowedLanguage = MRO_UTL_InternetInterfaceServices.ALLOWED_LANGUAGE;
    private static MRO_UTL_InternetInterfaceServices internetInterfaceServices = MRO_UTL_InternetInterfaceServices.getInstance();
    private static final MRO_QR_ReminderQueries reminderQuery = MRO_QR_ReminderQueries.getInstance();
    private static MRO_QR_User userQuery = MRO_QR_User.getInstance();
    private static final MRO_UTL_Constants constants = MRO_UTL_Constants.getAllConstants();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_SRV_TouchPoint touchPointSrv = MRO_SRV_TouchPoint.getInstance();

    webService static wrts_prcgvr.MRR_1_0.MultiResponse post(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest) {
        wrts_prcgvr.MRR_1_0.MultiResponse multiResponse = new wrts_prcgvr.MRR_1_0.MultiResponse();
        multiResponse.responses = new List<wrts_prcgvr.MRR_1_0.Response>();
        wrts_prcgvr.MRR_1_0.Request request = multiRequest.requests[0];

        System.debug('*****MRO_WS_InternetInterfaceServices Request ' + request);
        wrts_prcgvr.MRR_1_0.Response response;
        System.debug('request ******* ' + request.header.requestType);

        Map<String, Object> result;
        String requestType = request.header.requestType;
        if (allowedFlux.contains(requestType) && (requestType.equals('AddOfferDocumentRequest') || requestType.equals('AddContractDocumentRequest'))) {
            result = addDocument (request);
        } else if (allowedFlux.contains(requestType) && requestType.equals('FormularOfertaSiteRequest')) {
            result = formularOfertaSite_x (request);
        } else if (allowedFlux.contains(requestType) && requestType.equals('OfferResult')) {
            result = contractAcceptance (request);
        } else {
            response = new wrts_prcgvr.MRR_1_0.Response();
            response.code = 'KO';
            response.header = request.header;
            response.header.requestTimestamp = String.valueOf(System.now());
            response.description = '999 - Error ' + request.header.requestType ;
        }
        if (response == null) {
            response = (wrts_prcgvr.MRR_1_0.Response) result.get('response');
        }
        multiResponse.responses.add(response);
        Id objectId;
        System.debug('result ******* ' + result);
        if (result != null && !result.isEmpty()) {
            objectId = (Id) result.get('objectId');
            createAndSaveLog(multiRequest, multiResponse, objectId);
        }
        return multiResponse;
    }

    /**
     * Manage request from Internet interface FormulaOferta
     * @param request Request received in CallIn
     * @return result
    */
    static Map<String, Object> formularOfertaSite_x(wrts_prcgvr.MRR_1_0.Request request) {
        System.debug('formularOfertaSite_xx');
        wrts_prcgvr.MRR_1_0.Response response;
        Map<String, Object> result = new Map<String, Object>();
        String commodity = '';
        String pathStringAccount = 'Dossier__r/Account';
        String pathStringServicePoint = 'Case/Supply__r/ServicePoint__r';
        String pathStringInteraction = 'Dossier__r/Interaction__c';
        response = MRO_UTL_MRRMapper.initResponse(request);
        Map<String, String> headerFields = MRO_UTL_MRRMapper.getHeaderFields(request.header);
        String formular = '-1';
        if (!headerFields.isEmpty() && headerFields.containsKey('id_formular') && String.isNotBlank(headerFields.get('id_formular'))) {
            formular = headerFields.get('id_formular');
        }

        Integer idFormular = Integer.valueOf(formular);
        System.debug('formular  : ' + idFormular);
        if (idFormular >= 1 && idFormular <= 9) {
            if (idFormular == 1 || idFormular == 8) {
                commodity = 'Electric';
            } else {
                commodity = 'Gas';
            }
            try {
                Account account;
                Dossier__c dossier;
                Interaction__c interaction;
                ServicePoint__c servicePoint;
                Map<String, SObject> mapObjects = new Map<String, SObject>();
                Boolean isEnel = false;

                if (request.objects.size() > 0) {
                    Dossier__c dossierWo;
                    List<wrts_prcgvr.MRR_1_0.WObject> dossierWObjects = MRO_UTL_MRRMapper.selectWobjects('Dossier__r', request);
                    if (dossierWObjects != null && !dossierWObjects.isEmpty()) {
                        dossierWo = (Dossier__c) MRO_UTL_MRRMapper.wobjectToSObject(dossierWObjects[0]);
                    }
                    dossier = dossierSrv.generateGenericDossier(commodity, channelRequest, originRequest);
                    interaction = MRO_WS_InternetInterfaceServices.getInteractionFormulaOfertaSite(pathStringInteraction, request);
                    mapObjects = MRO_WS_InternetInterfaceServices.getAccountServicePointFormulaOfertaSite(pathStringAccount, pathStringServicePoint,interaction, request);
                    if (!mapObjects.isEmpty()) {
                        account = (Account) mapObjects.get('account');
                        servicePoint = (ServicePoint__c) mapObjects.get('servicePoint');
                        String servicePointAccountId = servicePoint != null && servicePoint.Account__c != null ? servicePoint.Account__c : null;
                        isEnel = servicePoint != null && String.isNotBlank(servicePoint.ENELTEL__c) ? true : false;
                        if (account.Id != servicePointAccountId) {
                            throw new WrtsException('E12');
                        }
                    }

                    if (account != null && String.isNotBlank(account.Id) && interaction != null) {
                        CustomerInteraction__c ci = new CustomerInteraction__c();
                        ci.Customer__c = account.Id;
                        ci.Interaction__c = interaction.Id;
                        databaseSrv.insertSObject(ci);
                        dossierWo.CustomerInteraction__c = ci.Id;
                        dossierWo.Account__c = account.Id;
                    } else {
                        String clientName = interaction.InterlocutorFirstName__c +' '+interaction.InterlocutorLastName__c;
                        dossierWo.Comments__c = String.isNotBlank(clientName) ? dossierWo.Comments__c + '|' + clientName : dossierWo.Comments__c ;
                    }
                    if (interaction != null){
                        dossierWo.Email__c = interaction.InterlocutorEmail__c;
                        dossierWo.SendingChannel__c = dossierWo.Email__c == null ? '' : constantsSrv.SENDING_CHANNEL_EMAIL;
                    }
                    System.debug('Dossier WO: ' + dossierWo);

                    dossierWo.Email__c = interaction.InterlocutorEmail__c;
                    dossierWo.SendingChannel__c = constants.SENDING_CHANNEL_EMAIL;

                    String language = dossierWo.Language__c != null ? String.valueOf(dossierWo.Language__c).toLowerCase() : '';
                    if (String.isBlank(language)) {
                        throw new WrtsException('E6');
                    }else if (!allowedLanguage.contains(language.substring(0,2))){
                        throw new WrtsException('E7');
                    }
                    if(dossierWo.CommercialProduct__c != null || String.isNotBlank(dossierWo.CommercialProduct__c)){
                        NE__Product__c product = internetInterfaceServices.getProductByCode( dossierWo.CommercialProduct__c);
                        System.debug('*** Info ' + dossierWo.CommercialProduct__c + ' ' + product);
                        if(product != null ){
                            dossierWo.CommercialProduct__c = product.Id;
                        }else {
                            dossierWo.CommercialProduct__c = null;
                        }
                    }

                    if (language.substring(0,2) == 'en'){
                        dossierWo.Language__c = 'en_US';
                    }
                    dossierWo.Id = dossier.Id;
                    databaseSrv.updateSObject(dossierWo);
                    String dossierToken = (dossierSrv.generateInternetInterfaceDocumentUploadTouchPoint(dossier.Id, String.valueOf(idFormular), isEnel, language)).Id;
                    if (String.isNotBlank(dossierToken)) {
                        addTokenOnResponse(dossierToken, response.objects);
                    }
                }
            } catch (Exception e) {
                response.code = 'KO';
                if (e.getTypeName() == 'WrtsException') {
                    response.description = e.getMessage();
                } else {
                    response.description = 'E20';
                    System.debug('*** Error ' + e.getMessage() + ' ' + e.getStackTraceString());
                }
            }
        } else {
            response = new wrts_prcgvr.MRR_1_0.Response();
            response.code = 'KO';
            response.header = request.header;
            response.header.requestTimestamp = String.valueOf(System.now());
            response.description = 'E0';
        }
        result.put('response', response);
        return result;
    }

    /**
     * Manage request from Internet interface AddOfferDocument AddContractDocument
     * @param request Request received in CallIn
     * @return result
    */
    static Map<String, Object> addDocument(wrts_prcgvr.MRR_1_0.Request request) {
        wrts_prcgvr.MRR_1_0.Response response;
        Map<String, Object> result = new Map<String, Object>();
        String pathStringDossier = 'Dossier__r';
        String pathStringFileMetadata = 'Dossier__r/FileMetadata__r';
        String pathStringAccessToken = 'Dossier__r/ExternalAccessIdentifier__r';
        String pathStringIndividual = 'Case/Account/Individual';
        response = MRO_UTL_MRRMapper.initResponse(request);
        Savepoint sp = Database.setSavepoint();
        try {
            System.debug('addDocument : ' + request);
            result.put('response', response);
            Dossier__c dossier;
            Individual individual;

            if (request.objects.size() > 0) {
                List<wrts_prcgvr.MRR_1_0.WObject> dossierWObjects = MRO_UTL_MRRMapper.selectWobjects(pathStringDossier, request);
                Dossier__c dossierWo;
                Individual individualWo;
                Token__c token;
                List<wrts_prcgvr.MRR_1_0.WObject> tokenWObjects = MRO_UTL_MRRMapper.selectWobjects(pathStringAccessToken, request);
                if (tokenWObjects != null && !tokenWObjects.isEmpty()) {
                    token = (Token__c) MRO_UTL_MRRMapper.wobjectToSObject(tokenWObjects[0]);
                }
                if (token == null || (token != null && String.isBlank(token.Value__c))) {
                    throw new WrtsException('E1');
                }
                if (dossierWObjects != null && !dossierWObjects.isEmpty()) {
                    dossierWo = (Dossier__c) MRO_UTL_MRRMapper.wobjectToSObject(dossierWObjects[0]);
                }

                dossier = dossierQuery.getByToken(token.Value__c);
                if (dossier == null) {
                    throw new WrtsException('E1');
                }
                /*if (dossier.Account__c == null) {
                    throw new WrtsException('Missing Account');
                }*/
                dossierWo.Id = dossier.Id;
                List<wrts_prcgvr.MRR_1_0.WObject> individualWObjects = MRO_UTL_MRRMapper.selectWobjects(pathStringIndividual, request);
                if (individualWObjects != null && !individualWObjects.isEmpty()) {
                    individualWo = (Individual) MRO_UTL_MRRMapper.wobjectToSObject(individualWObjects[0]);
                }
                if (individualWo != null && dossier.Account__c != null && String.isNotBlank(dossier.Account__r.PersonIndividualId)) {
                    individualWo.Id = dossier.Account__r.PersonIndividualId;
                    databaseSrv.updateSObject(individualWo);
                }

                List<FileMetadata__c> fileMetadataList = new List<FileMetadata__c>();
                List<wrts_prcgvr.MRR_1_0.WObject> fileMetadataWObjects = MRO_UTL_MRRMapper.selectWobjects(pathStringFileMetadata, request);
                Boolean hasOneDocument = false;
                for (wrts_prcgvr.MRR_1_0.WObject file : fileMetadataWObjects) {
                    FileMetadata__c fileMetadata = (FileMetadata__c) MRO_UTL_MRRMapper.wobjectToSObject(file);
                    if (String.isBlank(fileMetadata.FileName__c)) continue;
                    fileMetadata.Dossier__c = dossier.Id;
                    fileMetadata.Title__c = fileMetadata.FileName__c != null ? fileMetadata.FileName__c.substringBefore('.') : '';
                    if (!hasOneDocument) {
                        hasOneDocument = String.isNotBlank(fileMetadata.FileName__c);//&& String.isNotBlank(fileMetadata.ExternalId__c)
                    }
                    fileMetadataList.add(fileMetadata);
                }
                if (!hasOneDocument) {
                    throw new WrtsException('E2');
                }
                if (!fileMetadataList.isEmpty()) {
                    databaseSrv.insertSObject(fileMetadataList);
                }
                List<CustomReminder__c> customReminders = reminderQuery.getById(dossier.Id);
                if (!customReminders.isEmpty()) {
                    databaseSrv.deleteSObject(customReminders);
                }
                Group ownerQueue = userQuery.getQueueByDeveloperName('BackOffice');
                dossierWo.OwnerId = ownerQueue.Id;
                databaseSrv.updateSObject(dossierWo);
                Dossier__c dossierExternalAccessId = [SELECT Id,CommercialProduct__r.ProductCode__c FROM Dossier__c WHERE Id =: dossier.Id];
                wrts_prcgvr.MRR_1_0.WObject wObjectDossier = MRO_UTL_MRRMapper.sObjectToWObject(dossierExternalAccessId, 'Dossier');
                response.objects.add(wObjectDossier);

                //Invalidate Token and remove custom reminders
                invalidateTokenAndRemoveReminders(dossier.Id, dossier.ExternalAccessIdentifier__c);
            }
        } catch (Exception e) {
            response.code = 'KO';
            if (e.getTypeName() == 'WrtsException') {
                response.description = e.getMessage();
            } else {
                response.description = 'E20';
                System.debug('*** Error ' + e.getCause() + ' ' + e.getStackTraceString());
            }
            Database.rollback(sp);
        }
        return result;
    }

    /**
         * Manage request from Internet interface FormulaOferta
         * @param request Request received in CallIn
         * @return result
    */
    static Map<String, Object> contractAcceptance(wrts_prcgvr.MRR_1_0.Request request) {
        wrts_prcgvr.MRR_1_0.Response response;
        Map<String, Object> result = new Map<String, Object>();
        Savepoint sp = Database.setSavepoint();
        response = MRO_UTL_MRRMapper.initResponse(request);

        Map<String, String> headerFields = MRO_UTL_MRRMapper.getHeaderFields(request.header);
        Boolean hasAccepted;
        Boolean termConditions;
        String token, notes = '';


        if (!headerFields.isEmpty() && headerFields.containsKey('ac_of') && String.isNotBlank(headerFields.get('ac_of'))) {
            hasAccepted = Boolean.valueOf (headerFields.get('ac_of'));
        }

        if (!headerFields.isEmpty() && headerFields.containsKey('tc') && String.isNotBlank(headerFields.get('tc'))) {
            termConditions = Boolean.valueOf(headerFields.get('tc'));
        }

        if (!headerFields.isEmpty() && headerFields.containsKey('token') && String.isNotBlank(headerFields.get('token'))) {
            token = String.valueOf(headerFields.get('token'));
        }

        if (!headerFields.isEmpty() && headerFields.containsKey('note') && String.isNotBlank(headerFields.get('note'))) {
            notes = String.valueOf(headerFields.get('note'));
        }

        try {
            if (token == null || String.isBlank(token)) {
                throw new WrtsException('E1');
            }
            Date acceptanceDate = Date.today();
            Map<String, Object> resultAcceptance = touchPointSrv.contractAcceptance(token, (hasAccepted && termConditions), acceptanceDate, notes);
            if (resultAcceptance != null && Boolean.valueOf(resultAcceptance.get('success'))) {
                response.code = 'OK';
                Dossier__c dossier = [SELECT Id,CommercialProduct__r.ProductCode__c FROM Dossier__c WHERE ExternalAccessIdentifier__r.Value__c = :token];
                wrts_prcgvr.MRR_1_0.WObject wObjectDossier = MRO_UTL_MRRMapper.sObjectToWObject(dossier, 'Dossier__r');
                response.objects.add(wObjectDossier);
            } else {
                response.code = 'KO' +' - '+resultAcceptance.get('message');
                response.description = String.valueOf(resultAcceptance.get('message'));
            }
        } catch (Exception e) {
            response.code = 'KO';
            if (e.getTypeName() == 'WrtsException') {
                response.description = e.getMessage();
            } else {
                response.description = 'E20';
                System.debug('*** Error ' + e.getCause() + ' ' + e.getStackTraceString());
            }
            Database.rollback(sp);
        }
        result.put('response', response);
        return result;
    }

    static void createAndSaveLog(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest, wrts_prcgvr.MRR_1_0.MultiResponse multiResponse, Id objectId) {
        wrts_prcgvr.MRR_1_0.Request request = multiRequest.requests[0];
        wrts_prcgvr.MRR_1_0.Response response = multiResponse.responses[0];
        String code = String.isNotBlank(response.description) ? response.code + ' - ' + response.description : response.code;
        String messageString = MRO_UTL_MRRMapper.serializeMultirequest(multiRequest);
        String responseString = MRO_UTL_MRRMapper.serializeMultiResponse(multiResponse);
        MRO_SRV_ExecutionLog.createIntegrationLog('CallIn', request.header.requestId, request.header.requestType, objectId, messageString, code, responseString, null, null, null, 'internetinterface', null);
    }

    /**
     * @author Goudiaby
     * Insert interaction Sobject from Inbound Internet Interface
     * @param pathStringInteraction Path on payload Call
     * @param request Inbound Request
     * @return Interaction__c
    */
    static Interaction__c getInteractionFormulaOfertaSite(String pathStringInteraction, wrts_prcgvr.MRR_1_0.Request request) {
        List<wrts_prcgvr.MRR_1_0.WObject> interactionList = MRO_UTL_MRRMapper.selectWobjects(pathStringInteraction, request);
        Interaction__c interaction;
        if (interactionList != null && !interactionList.isEmpty()) {
            interaction = (Interaction__c) MRO_UTL_MRRMapper.wobjectToSObject(interactionList[0]);
        }
        interaction.Channel__c = MRO_UTL_Constants.CHANNEL_ENEL_RO;
        interaction.Origin__c = MRO_UTL_Constants.ORIGIN_WEB;
        interaction.Status__c = constantsSrv.CASE_STATUS_NEW;
        if (String.isBlank(interaction.InterlocutorFirstName__c) || String.isBlank(interaction.InterlocutorLastName__c)) {
            throw new WrtsException('E1');
        }
        if (String.isBlank(interaction.InterlocutorPhone__c)) {
            throw new WrtsException('E2');
        }
        if (String.isBlank(interaction.InterlocutorEmail__c)) {
            throw new WrtsException('E3');
        }
        if (!MRO_UTL_Validations.isValidRomaniaPhone(interaction.InterlocutorPhone__c)) {
            throw new WrtsException('E4');
        }
        if (!MRO_UTL_Validations.isValidEmail(interaction.InterlocutorEmail__c)) {
            throw new WrtsException('E5');
        }
        databaseSrv.insertSObject(interaction);
        return interaction;
    }

    /**
     * @author Goudiaby
     * Get account IntegrationKey from Inbound Internet Interface
     * @param pathStringAccount Path on Payload Call
     * @param pathStringServicePoint Path service Point on Payload Call
     * @param interaction Interaction Object
     * @param request Inbound Request
     * @return Account
    */
    static Map<String, SObject> getAccountServicePointFormulaOfertaSite(String pathStringAccount, String pathStringServicePoint,Interaction__c interaction, wrts_prcgvr.MRR_1_0.Request request) {
        Account account = new Account();
        ServicePoint__c servicePoint = new ServicePoint__c();
        Map<String, SObject> mapObjects = new Map<String, SObject>();
        List<wrts_prcgvr.MRR_1_0.WObject> accountList = MRO_UTL_MRRMapper.selectWobjects(pathStringAccount, request);
        if (accountList != null && !accountList.isEmpty()) {
            account = (Account) MRO_UTL_MRRMapper.wobjectToSObject(accountList[0]);
        }
        List<wrts_prcgvr.MRR_1_0.WObject> servicePointList = MRO_UTL_MRRMapper.selectWobjects(pathStringServicePoint, request);
        if (servicePointList != null && !servicePointList.isEmpty()) {
            servicePoint = (ServicePoint__c) MRO_UTL_MRRMapper.wobjectToSObject(servicePointList[0]);
        }

        String accountCode = '';
        if (account.IntegrationKey__c != null) {
            accountCode = account.IntegrationKey__c;
        }
        //String enelTel = servicePoint.ENELTEL__c;
        String enelTel = interaction.ENELTEL__c;
        if (String.isNotBlank(enelTel) || String.isNotBlank(accountCode)) {
            if (String.isBlank(accountCode)) {
                throw new WrtsException('E11');
            }
            if (String.isBlank(enelTel)) {
                throw new WrtsException('E10');
            }
            List<Account> myAccounts = [SELECT Id FROM Account WHERE IntegrationKey__c = :accountCode LIMIT 1];
            if (myAccounts.isEmpty()) {
                throw new WrtsException('E12');
            } else {
                account = myAccounts[0];
            }
            List<ServicePoint__c> servicePoints = [SELECT Id,ENELTEL__c,Account__c FROM ServicePoint__c WHERE ENELTEL__c = :enelTel LIMIT 1];
            if (servicePoints.isEmpty()) {
                throw new WrtsException('E12');
            } else {
                servicePoint = servicePoints[0];
            }
            mapObjects.put('account', account);
            mapObjects.put('servicePoint', servicePoint);
        }
        return mapObjects;
    }

    /**
     * @author Goudiaby
     * Add Token URL and Value on response
     * @param dossierId
     * @param objectsResponse
    */
    static void addTokenOnResponse(String dossierId, List<wrts_prcgvr.MRR_1_0.WObject> objectsResponse) {
        Dossier__c dossierExternalAccessId = [SELECT Id,ExternalAccessIdentifier__r.Value__c, ExternalAccessIdentifier__r.TouchPointURL__c FROM Dossier__c WHERE Id = :dossierId];
        wrts_prcgvr.MRR_1_0.WObject wObjectDossier = MRO_UTL_MRRMapper.sObjectToWObject(dossierExternalAccessId, 'Dossier__r');
        objectsResponse.add(wObjectDossier);
    }

    /**
     * @author Goudiaby
     * Add Token URL and Value on response
    */
    static void invalidateTokenAndRemoveReminders (String dossierId, String tokenId) {
        List<CustomReminder__c> customReminders = reminderQuery.getById(dossierId);
        if (!customReminders.isEmpty()) {
            databaseSrv.deleteSObject(customReminders);
        }
        Token__c tokenToBeExpired = new Token__c(Id = tokenId,IsExpired__c = true);
        databaseSrv.updateSObject(tokenToBeExpired);
    }
}