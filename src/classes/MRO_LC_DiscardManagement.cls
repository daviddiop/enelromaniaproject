/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 05, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_LC_DiscardManagement extends ApexServiceLibraryCnt {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id activityId = (Id) params.get('activityId');

            MRO_QR_Activity activityQueries = MRO_QR_Activity.getInstance();
            MRO_SRV_DynamicForm dynamicFormSrv = MRO_SRV_DynamicForm.getInstance();

            wrts_prcgvr__Activity__c activity = activityQueries.getById(activityId);
            if ((activity.Type__c != CONSTANTS.ACTIVITY_DISCARD_MANAGEMENT_TYPE && activity.Type__c != CONSTANTS.ACTIVITY_DATA_ENTRY_TYPE) || activity.FieldsTemplate__c == null) {
                throw new WrtsException(Label.InvalidDiscardManagementActivity);
            }
            response.put('activity', activity);
            response.put('rootRecordId', activity.wrts_prcgvr__ObjectId__c);

            MRO_SRV_DynamicForm.DynamicForm dynamicForm = dynamicFormSrv.buildDynamicForm(activity.FieldsTemplate__c, activity.wrts_prcgvr__ObjectId__c);
            response.put('dynamicForm', dynamicForm);

            return response;
        }
    }

    public class saveDataAndCloseActivity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id activityId = params.get('activityId');
            MRO_SRV_DynamicForm.DynamicForm dynamicForm = (MRO_SRV_DynamicForm.DynamicForm) JSON.deserialize(params.get('dynamicForm'), MRO_SRV_DynamicForm.DynamicForm.class);
            Map<String, Object> records = (Map<String, Object>) JSON.deserializeUntyped(params.get('records'));
            MRO_UTL_Constants constants = MRO_UTL_Constants.getAllConstants();

            System.debug('dynamicForm: '+dynamicForm);
            System.debug('records: '+records);
            Map<Id, Map<String, Object>> formData = new Map<Id, Map<String, Object>>();
            for (String recordId : records.keySet()) {
                formData.put(recordId, (Map<String, Object>) records.get(recordId));
            }

            Savepoint sp = Database.setSavepoint();
            try {
                MRO_QR_Activity activityQueries = MRO_QR_Activity.getInstance();
                wrts_prcgvr__Activity__c activity = activityQueries.getById(activityId);

                MRO_SRV_DynamicForm dynamicFormSrv = MRO_SRV_DynamicForm.getInstance();
                dynamicFormSrv.saveDynamicForm(dynamicForm, formData);

                if (activity.Type__c == CONSTANTS.ACTIVITY_DATA_ENTRY_TYPE) {
                    MRO_LC_DiscardManagement.closeDataEntryActivityAndPushRootRecord(activity, dynamicForm.rootObjectType, dynamicForm.rootRecordId);
                } else {
                    MRO_LC_DiscardManagement.closeDiscardActivityAndUpdateRootRecord(activity, dynamicForm.rootObjectType, dynamicForm.rootRecordId);
                }

                response.put('result', 'OK');
            }
            catch (Exception e) {
                Database.rollback(sp);
                throw e;
            }

            return response;
        }
    }

    public class closeActivity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id activityId = params.get('activityId');

            Savepoint sp = Database.setSavepoint();
            try {
                MRO_QR_Activity activityQueries = MRO_QR_Activity.getInstance();
                wrts_prcgvr__Activity__c activity = activityQueries.getById(activityId);
                Id rootRecordId = (Id) activity.wrts_prcgvr__ObjectId__c;
                String rootObjectType = rootRecordId.getSObjectType().getDescribe().getName();
                MRO_LC_DiscardManagement.closeDiscardActivityAndUpdateRootRecord(activity, rootObjectType, rootRecordId);

                response.put('result', 'OK');
            }
            catch (Exception e) {
                Database.rollback(sp);
                throw e;
            }

            return response;
        }
    }

    private static void closeDiscardActivityAndUpdateRootRecord(wrts_prcgvr__Activity__c discardActivity, String rootObjectType, Id rootRecordId) {
        MRO_SRV_Activity activitySrv = MRO_SRV_Activity.getInstance();
        activitySrv.updateActivitiesStatus(new List<wrts_prcgvr__Activity__c>{discardActivity}, MRO_UTL_Constants.getAllConstants().ACTIVITY_STATUS_COMPLETED);

        SObject rootRecord = Schema.getGlobalDescribe().get(rootObjectType).newSObject(rootRecordId);
        MRO_UTL_ApexContext.FORCE_CALLOUTS_RETRIGGERING = true;
        DatabaseService.getInstance().updateSObject(rootRecord);
    }

    private static void closeDataEntryActivityAndPushRootRecord(wrts_prcgvr__Activity__c discardActivity, String rootObjectType, Id rootRecordId) {
        MRO_SRV_Activity activitySrv = MRO_SRV_Activity.getInstance();
        activitySrv.updateActivitiesStatus(new List<wrts_prcgvr__Activity__c>{discardActivity}, MRO_UTL_Constants.getAllConstants().ACTIVITY_STATUS_COMPLETED);

        MRO_UTL_Transitions transitionUtils = MRO_UTL_Transitions.getInstance();
        if (rootObjectType == 'Case') {
            Case caseRecord = MRO_QR_Case.getInstance().getById(rootRecordId);
            transitionUtils.checkAndApplyAutomaticTransitionWithTag(caseRecord, CONSTANTS.CONFIRM_TAG, true, false);
        } else if (rootObjectType == 'Dossier__c') {
            Dossier__c dossier = MRO_QR_Dossier.getInstance().listDossiersWithCases(new Set<Id>{rootRecordId}, true).get(rootRecordId);
            MRO_SRV_Dossier.getInstance().checkAndApplyAutomaticTransitionToDossierAndCases(dossier, dossier.Cases__r, CONSTANTS.CONFIRM_TAG, true, true, false);
        }
    }
}