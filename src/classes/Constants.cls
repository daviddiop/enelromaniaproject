public with sharing class Constants{

    @AuraEnabled
    public final String CASE_START_PHASE = 'RE010';
    @AuraEnabled
    public final String CASE_DEFAULT_CASE_ORIGIN = 'Phone';
    @AuraEnabled
    public final String CASE_STATUS_DRAFT = 'Draft';
    @AuraEnabled
    public final String CASE_STATUS_NEW = 'New';
    @AuraEnabled
    public final String CASE_STATUS_CANCELED = 'Canceled';

    @AuraEnabled
    public final String DOSSIER_STATUS_DRAFT = 'Draft';
    @AuraEnabled
    public final String DOSSIER_STATUS_NEW = 'New';
    @AuraEnabled
    public final String DOSSIER_STATUS_CANCELED = 'Canceled';


    //Supply
    @AuraEnabled
    public final String SUPPLY_STATUS_ACTIVE = 'Active';
    @AuraEnabled
    public final String SUPPLY_STATUS_ACTIVATING = 'Activating';
    @AuraEnabled
    public final String SUPPLY_STATUS_TERMINATING = 'Terminating';
    @AuraEnabled
    public final String SUPPLY_STATUS_NOTACTIVE = 'Not Active';

    //Contract
    @AuraEnabled
    public final String CONTRACT_STATUS_ACTIVATED = 'Activated';
    @AuraEnabled
    public final String CONTRACT_STATUS_DRAFT = 'Draft';
    @AuraEnabled
    public final String CONTRACT_STATUS_INAPPROVALPROCESS = 'In Approval Process';

    //Billing Profile
    @AuraEnabled
    public final String BILLINGPROFILE_RECORDTYPE_GIRO = 'Giro';

    //Address values
/*
    @AuraEnabled
    public final List<Utils.KeyVal> streetTypes {
        get{
            List<Utils.KeyVal> values = new List<Utils.KeyVal>();
            for(Schema.PicklistEntry pckEntry : Account.ResidentialStreetType__c.getDescribe().getPicklistValues()){
                values.add(new Utils.KeyVal(pckEntry.getValue(),pckEntry.getLabel()));
            }
            return values;
        }
    }

    @AuraEnabled
    public final List<Utils.KeyVal> countries {
        get{
            List<Utils.KeyVal> values = new List<Utils.KeyVal>();
            for(Schema.PicklistEntry pckEntry : Account.ResidentialCountry__c.getDescribe().getPicklistValues()){
                values.add(new Utils.KeyVal(pckEntry.getValue(),pckEntry.getLabel()));
            }
            return values;
        }
    }
*/
    @AuraEnabled
    public final Map<String, String> OSIRecordTypes {
        get{
            Map<String, String> values = new Map<String, String>();
            Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName();
            values.put('Electric', recordTypes.get('Electric').getRecordTypeId());
            values.put('Gas', recordTypes.get('Gas').getRecordTypeId());
            return values;
        }
    }


    @AuraEnabled(cacheable=true)
    public static Map<String, String> getCaseRecordTypes(String type) {

        Map<String, String> values = new Map<String, String>();
        Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
        if ((type == 'Termination') || String.isBlank(type)) {
            values.put('Termination_ELE', caseRts.get('Termination_ELE').getRecordTypeId());
            values.put('Termination_GAS', caseRts.get('Termination_GAS').getRecordTypeId());
            values.put('Termination_SRV', caseRts.get('Termination_SRV').getRecordTypeId());
        }
        if ((type == 'Activation') || String.isBlank(type)) {
            values.put('Activation_ELE', caseRts.get('Activation_ELE').getRecordTypeId());
            values.put('Activation_GAS', caseRts.get('Activation_GAS').getRecordTypeId());
            values.put('Activation_SRV', caseRts.get('Activation_SRV').getRecordTypeId());
        }
        if ((type == 'SwitchIn') || String.isBlank(type)) {
            values.put('SwitchIn_ELE',caseRts.get('SwitchIn_ELE').getRecordTypeId());
            values.put('SwitchIn_GAS',caseRts.get('SwitchIn_GAS').getRecordTypeId());
        }
        if ((type == 'SwitchOut') || String.isBlank(type)) {
            values.put('SwitchOut_ELE',caseRts.get('SwitchOut_ELE').getRecordTypeId());
            values.put('SwitchOut_GAS',caseRts.get('SwitchOut_GAS').getRecordTypeId());
        }
        if ((type == 'DecoReco') || String.isBlank(type)) {
            values.put('DecoReco',caseRts.get('DecoReco').getRecordTypeId());
        }
        if ((type == 'Transfer') || String.isBlank(type)) {
            values.put('Transfer_ELE',caseRts.get('Transfer_ELE').getRecordTypeId());
            values.put('Transfer_GAS',caseRts.get('Transfer_GAS').getRecordTypeId());
        }
        if ((type == 'Reactivation') || String.isBlank(type)) {
            values.put('Reactivation_ELE',caseRts.get('Reactivation_ELE').getRecordTypeId());
            values.put('Reactivation_GAS',caseRts.get('Reactivation_GAS').getRecordTypeId());
        }
        if ((type == 'Connection') || String.isBlank(type)) {
            values.put('Connection_ELE',caseRts.get('Connection_ELE').getRecordTypeId());
            values.put('Connection_GAS',caseRts.get('Connection_GAS').getRecordTypeId());
        }

        if ((type == 'BillingProfileChange') || String.isBlank(type)) {
            values.put('BillingProfileChange', caseRts.get('BillingProfileChange').getRecordTypeId());
        }
        if ((type == 'RegistryChange') || String.isBlank(type)) {
            values.put('RegistryChangeFast', caseRts.get('RegistryChangeFast').getRecordTypeId());
            values.put('RegistryChangeSlow', caseRts.get('RegistryChangeSlow').getRecordTypeId());
        }
        if ((type == 'TechnicalDataChange') || String.isBlank(type)) {
            values.put('TechnicalDataChange_ELE', caseRts.get('TechnicalDataChange_ELE').getRecordTypeId());
            values.put('TechnicalDataChange_GAS', caseRts.get('TechnicalDataChange_GAS').getRecordTypeId());
        }
        if ((type == 'ContractMerge') || String.isBlank(type)) {
            values.put('ContractMerge', caseRts.get('ContractMerge').getRecordTypeId());
        }
        if ((type == 'ProductChange') || String.isBlank(type)) {
            values.put('ProductChange_ELE',caseRts.get('ProductChange_ELE').getRecordTypeId());
            values.put('ProductChange_GAS',caseRts.get('ProductChange_GAS').getRecordTypeId());
        }
        if ((type == 'ContractualDataChange') || String.isBlank(type)) {
            values.put('ContractualDataChange_ELE',caseRts.get('ContractualDataChange_ELE').getRecordTypeId());
            values.put('ContractualDataChange_GAS',caseRts.get('ContractualDataChange_GAS').getRecordTypeId());
        }

        return values;
    }


    @AuraEnabled(cacheable=true)
    public static Constants getAllConstants() {
        return new Constants();
    }
}