/**
 * Updated by BG to implement AddressDTO 16/07/2019
 */
public with sharing class AddressService {
    
    @AuraEnabled
    public static List<Map<String, Object>> normalize(Map<String, Object> address) {
        //TODO call IAddressNormalizationService instance
        List<Map<String, Object>> addresses = new List<Map<String, Object>>();
        
        addresses.add(address);
        return addresses;
    }

    @AuraEnabled
    public static String getExtraFields(String objectApiName, String addressFieldSet) {
        return JSON.serialize(LightningUtils.getFieldSet(objectApiName, addressFieldSet));
    }

    @AuraEnabled
    public static List<AddressField__mdt> getAddressStructure(String addressStructureApiName) {
        List<AddressField__mdt> addrLayout = [SELECT MasterLabel, QualifiedApiName, ColSize__c, Order__c, Required__c, FieldName__c, Hidden__c,
                                            (SELECT MasterLabel, QualifiedApiName FROM AddressPicklistValues__r ORDER BY MasterLabel)
                                            FROM AddressField__mdt
                                            WHERE AddressLayout__r.QualifiedApiName = :addressStructureApiName
                                            ORDER BY Order__c ASC];
        return addrLayout;
    }


    public with sharing class AddressDTO {
        @AuraEnabled
        public String streetNumber {get; set;}
        @AuraEnabled
        public String streetNumberExtn {get; set;}
        @AuraEnabled
        public String streetName {get; set;}
        @AuraEnabled
        public String streetType {get; set;}
        @AuraEnabled
        public String apartment {get; set;}
        @AuraEnabled
        public String building {get; set;}
        @AuraEnabled
        public String city {get; set;}
        @AuraEnabled
        public String country {get; set;}
        @AuraEnabled
        public String floor {get; set;}
        @AuraEnabled
        public String locality {get; set;}
        @AuraEnabled
        public String postalCode {get; set;}
        @AuraEnabled
        public String province {get; set;}
        /*add by david*/
        @AuraEnabled
        public Boolean addressNormalized {get; set;}
        /*add by david*/
        public AddressDTO() {
        }
    }

    // BG commented - unused code
    /*@AuraEnabled
    public static Map<String, Object> validate(List<Map<String,Object>> fields) {
        Map<String, Object> response = new Map<String, Object>();
        try{
            response.put('error', false);
            response.put('isValid', true);
        } catch(Exception ex) {
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> normalizedAddress(List<Map<String,Object>> addressFieldSetValue) {
        Map<String, Object> response = new Map<String, Object>();
        try{

            response.put('normalizedAddress',addressFieldSetValue);
            response.put('error', false);
            response.put('isValid', true);
        } catch(Exception ex) {
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }*/


    @AuraEnabled
    public static Map<String,List<Map<String, Object>>> getAvailableAddresses(String addressLayoutQualifiedApiName, String accountId) {
        Map<String,List<Map<String, Object>>> results = new Map<String,List<Map<String, Object>>>();
        if(String.isBlank(addressLayoutQualifiedApiName)){
            addressLayoutQualifiedApiName = 'DefaultAddressLayout';
        }
        List<AddressField__mdt> addrLayout = [SELECT MasterLabel, QualifiedApiName, ColSize__c, Order__c, Required__c, FieldName__c
                                            FROM AddressField__mdt
                                            WHERE AddressLayout__r.QualifiedApiName = :addressLayoutQualifiedApiName
                                            ORDER BY Order__c ASC];
        Set<String> fields = new Set<String>();
        for(AddressField__mdt field : addrLayout) {
            fields.add(field.FieldName__c);
        }

        List<AddressEntity__mdt> addrEntities = [SELECT MasterLabel, QualifiedApiName, SObjectApiName__c, AddressPrefix__c, QueryRelation__c
                                            FROM AddressEntity__mdt];

        for(AddressEntity__mdt entity : addrEntities) {
            List<Map<String, Object>> addresses = new List<Map<String, Object>>();
            Set<String> entityFields = new Set<String>();
            for(String field : fields) {
                entityFields.add((entity.AddressPrefix__c + field + '__c').toLowerCase());
            }
            entityFields.add((entity.AddressPrefix__c + 'Address__c').toLowerCase());
            entityFields.add((entity.AddressPrefix__c + 'AddressNormalized__c').toLowerCase());
            if(String.isBlank(entity.QueryRelation__c)) {
                entity.QueryRelation__c = 'Id';
            }
            String query = 'SELECT ' + String.join(new List<String>(entityFields), ', ') + ' FROM ' + entity.SObjectApiName__c + ' WHERE ' + entity.QueryRelation__c + ' = :accountId ORDER BY ' + entity.AddressPrefix__c + 'Address__c';
            List<SObject> records = Database.query(query);

            Integer i = 0;
            for(SObject record : records) {
                Map<String, Object> address = new Map<String, Object>();
                for(String field : fields) {
                    address.put(field, record.get(entity.AddressPrefix__c + field + '__c'));
                }
                address.put('formula', record.get(entity.AddressPrefix__c + 'Address__c'));
                address.put('isNormalized', record.get(entity.AddressPrefix__c + 'AddressNormalized__c'));
                address.put('key', entity.MasterLabel + '-' + i);
                Boolean exist = false;
                for(Map<String, Object> addr : addresses) {
                    if(addr.get('formula') == record.get(entity.AddressPrefix__c + 'Address__c')) {
                        exist = true;
                        break;
                    }
                }
                if(exist == false){
                    addresses.add(address);
                }
                i++;
            }
            results.put(entity.MasterLabel, addresses);
        }

        return results;
    }
}