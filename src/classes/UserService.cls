/**
 * Created by goudiaby on 24/07/2019.
 */

public with sharing class UserService {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    public static UserService getInstance() {
        return new UserService();
    }

    //to be removed when all wizards are updated
    public String updateCompanyDivision(String userId,String newCompanyDivision){
        User currentUser = new User(Id= userId,CompanyDivisionId__c = newCompanyDivision);
        databaseSrv.upsertSObject(currentUser);
        return currentUser.Id;
    }

    /**
     * @author: Moussa Fofana
     * @description: this method update the company division on the user with enforced value
     */
    public String updateCompanyDivision(String userId,String newCompanyDivision,Boolean companyDivisionEnforced){
        User currentUser = new User(Id= userId,CompanyDivisionId__c = newCompanyDivision,CompanyDivisionEnforced__c = companyDivisionEnforced );
        databaseSrv.upsertSObject(currentUser);
        return currentUser.Id;
    }
}