/**
 * Created by napoli on 15/10/2019.
 */

global class MRO_SRV_SendMRRcallout implements wrts_prcgvr.Interfaces_1_0.IApexCalloutAction {
    global Object execute(Object args) {
        return execute(args, false);
    }

    global Object execute(Object args, Boolean returnCalloutResponse) {
        return execute(args, returnCalloutResponse, false);
    }

    global Object execute(Object args, Boolean returnCalloutResponse, Boolean skipExecutionLog) {
        Map<String, Object> argsMap = (Map<String, Object>) args;
        System.debug('argsMap: ' + argsMap);
        SObject transition = (SObject) argsMap.get('transition'); //phase transition
        SObject action = (SObject) argsMap.get('action'); //phase transition detail
        String method = (String) argsMap.get('method');
        Sobject obj = (sObject) argsMap.get('sender');
        Object templateMap = argsMap.get('templateMap');
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        String code, responseString;
        Set<String> fluxWithResponse = new Set<String>{'CONN-001'};

        System.debug('***param*** ' + transition);
        System.debug('***action*** ' + action);
        System.debug('***method*** ' + method);
        System.debug('***obj*** ' + obj);
        System.debug('***templateMap*** ' + templateMap);
        System.debug('***parameters*** ' + parameters);

        //MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest;

        //Response
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response = new wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse();

        //MultiResponse
        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse;

        String errorType;
        Boolean isSapMultiPod;

        try {
            if (String.isBlank(method)) return null;
            Type t = Type.forName(method);
            MRO_SRV_SendMRRcallout.IMRRCallout IMRRCallout = (MRO_SRV_SendMRRcallout.IMRRCallout) t.newInstance();

            multiRequest = IMRRCallout.buildMultiRequest(argsMap);

            //ContentType timeout START
            wrts_prcgvr__IntegrationSetting__c CS = wrts_prcgvr__IntegrationSetting__c.getInstance();
            String contentType = String.valueOf(CS.get('wrts_prcgvr__DefaultContentType__c'));
            Integer timeOut = 60000; //Default value
            List<IntegrationSetting__mdt> ac = [SELECT ActionValue__c, TimeOut__c From IntegrationSetting__mdt WHERE DeveloperName = :parameters.get('endPoint')];
            if(!ac.isEmpty()){
                contentType += ac[0].ActionValue__c;
                timeOut = Integer.valueOf(ac[0].TimeOut__c);
            }
            //ContentType timeout END

            argsMap.put('endpoint', 'callout:' + parameters.get('endPoint'));
            argsMap.put('timeout', timeOut);
            argsMap.put('payload', multiRequest);
            argsMap.put('contentType', contentType);

            system.debug('Execute Callout');
            System.debug('multiRequest: ' + multiRequest);

            isSapMultiPod = parameters.get('endPoint') == 'sap' && obj.getSObjectType() == Schema.Case.getSObjectType() && obj.getSObjectType().getDescribe().fields.getMap().keySet().contains('casegroup__c') && obj.get('CaseGroup__c') != null;
            if(isSapMultiPod == false) {
                wrts_prcgvr.Interfaces_1_0.ICalloutClient client = (wrts_prcgvr.Interfaces_1_0.ICalloutClient) wrts_prcgvr.VersionManager.newClassInstance ('CalloutClient');
                calloutResponse = (wrts_prcgvr.MRR_1_0.MultiResponse) client.send(argsMap);
                system.debug('Callout Response: ' + calloutResponse);

                response = IMRRCallout.buildResponse(calloutResponse);
            }


            /*    if(calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
                    if(calloutResponse.responses[0].code == 'OK'){
                        response.success = true;
                        if(fluxWithResponse.contains(parameters.get('requestType'))) IMRRCallout.buildResponse(calloutResponse.responses[0]);
                    }else{
                        response.success = false;
                        errorType = 'Functional error';
                    }
                    response.message = calloutResponse.responses[0].description;
                }*/




        } catch (Exception e) {
            system.debug('Error: ' + e.getMessage() + e.getStackTraceString());
            response.success = false;
            String message = 'KO - ' + e.getMessage();
            response.message = message;
            code = message;
            errorType = 'Technical error';
        } finally {
            if (!skipExecutionLog) {
            // CREATE EXECUTION LOG START
            System.debug('CREATE EXECUTION LOG START');
            if(calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
                code = calloutResponse.responses[0].code + ' ' + calloutResponse.responses[0].description;
                responseString = MRO_UTL_MRRMapper.serializeMultiResponse(calloutResponse);
            }
            //String messageString = MRO_UTL_MRRMapper.serializeMultirequest(multiRequest);
            String messageString = MRO_UTL_MRRMapper.serializeMultiRequest(multiRequest, false);

            String objId = obj != null ? obj.Id : null;
            String type = isSapMultiPod ? 'GroupCallOutItem' : 'CallOut';
            String externalSystem = parameters.containsKey('destination') ? parameters.get('destination') : parameters.get('endPoint');
            MRO_SRV_ExecutionLog.createIntegrationLog(type, multiRequest.requests[0].header.requestId, multiRequest.requests[0].header.requestType, objId, messageString, code, responseString, null, null, null, externalSystem, errorType);
//            //Query Execution Log Settings
//            List<ExecutionLogSetting__mdt> ExecutionLogSettings;
//            ExecutionLogSettings = [
//                    SELECT  LogLevel__c
//                    FROM    executionlogsetting__mdt
//                    WHERE   RequestType__c = :parameters.get('requestType')
//                            AND System__c = :parameters.get('System')
//            ];
//            //Query Execution Log Settings
//            if(ExecutionLogSettings.isEmpty()){
//                ExecutionLogSettings = [
//                        SELECT  LogLevel__c
//                        FROM    executionlogsetting__mdt
//                        WHERE   RequestType__c = '*'
//                        AND System__c = :parameters.get('System')
//                ];
//            }
//            Boolean createIntegrationLog = true;
//            if(!ExecutionLogSettings.isEmpty() && ExecutionLogSettings.size() == 1){
//                /*if(ExecutionLogSettings[0].LogLevel__c == 'Off'){
//                    createIntegrationLog = false;
//                    System.debug('Log level is Off -> It Doesn\'t create Integration log');
//                } else if(ExecutionLogSettings[0].LogLevel__c == 'Full'){
//                    createIntegrationLog = true;
//                    System.debug('Log level is All -> It create Integration log');
//                } else if(ExecutionLogSettings[0].LogLevel__c == 'All Errors' && (errorType == 'Functional error' || errorType == 'Technical error')){
//                    createIntegrationLog = true;
//                    System.debug('Log level is All Errors -> It create Integration log');
//                } else if(ExecutionLogSettings[0].LogLevel__c == 'Technical Errors' && errorType == 'Technical error'){
//                    createIntegrationLog = true;
//                    System.debug('Log level is Technical Errors and error type is Technical error -> It create Integration log');
//                } else if(ExecutionLogSettings[0].LogLevel__c == 'Technical Errors' && errorType == 'Functional error'){
//                    createIntegrationLog = false;
//                    System.debug('Log level is Technical Errors and error type is Functional error -> It Doesn\'t create Integration log');
//                }*/
//
//
//
//                switch on ExecutionLogSettings[0].LogLevel__c {
//                    when 'Off' {
//                        createIntegrationLog = false;
//                        System.debug('Log level is Off -> It Doesn\'t create Integration log');
//                    }
//                    when 'Full' {
//                        createIntegrationLog = true;
//                        System.debug('Log level is All -> It create Integration log');
//                    }
//                    when 'All Errors' {
//                        if(errorType == 'Functional error' || errorType == 'Technical error'){
//                            createIntegrationLog = true;
//                            System.debug('Log level is Technical Errors and error type is Technical error -> It create Integration log');
//                        }
//                    }
//                    when 'Technical Errors' {
//                        if(errorType == 'Technical error'){
//                            createIntegrationLog = true;
//                            System.debug('Log level is Technical Errors and error type is Technical error -> It create Integration log');
//                        } else{
//                            createIntegrationLog = false;
//                            System.debug('Log level is Technical Errors and error type is Functional error -> It Doesn\'t create Integration log');
//                        }
//                    }
//                    when else {
//                        createIntegrationLog = true;
//                    }
//                }
//            }
//            if(createIntegrationLog) MRO_SRV_ExecutionLog.createIntegrationLog('CallOut', multiRequest.requests[0].header.requestId, multiRequest.requests[0].header.requestType, obj.Id, messageString, code, responseString, null, null, null, null. errorType);
//            // CREATE EXECUTION LOG END
            }
            System.debug(returnCalloutResponse);
            if(returnCalloutResponse){
                return calloutResponse;
            }

            return response;
        }
        return null;
    }

    public interface IMRRCallout {
        wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap);
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse);
    }
}