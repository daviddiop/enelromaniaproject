/**
 * Created by David Diop on 12.11.2019.
 */

public with sharing class MRO_QR_Case {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public static MRO_QR_Case getInstance() {
        return (MRO_QR_Case) ServiceLocator.getInstance(MRO_QR_Case.class);
    }

    public List<Case> listByContract(Id contractId) {
        List<Case> caseList = [
            SELECT Id, AssetId
            FROM Case
            WHERE Contract__c = :contractId
        ];
        return caseList;
    }

    public List<Case> listByContractIds(List<String> contractIds) {
        return [
            SELECT Id, Dossier__c
            FROM Case
            WHERE Contract__c IN :contractIds
        ];
    }

    public List<Case> listByContractAndRecordType(Id contractId, Id recordTypeId) {
        List<Case> caseList = [
            SELECT Id,RecordType.DeveloperName, Trader__c, Distributor__c, Voltage__c, PowerPhase__c, Description,
                Dossier__c, Dossier__r.RequestType__c, Dossier__r.Commodity__c, Phase__c, RecordTypeId,
                AvailablePower__c, ContractualPower__c, VoltageLevel__c,
                AddressStreetName__c, AddressStreetType__c, AddressStreetNumber__c, AddressStreetNumberExtn__c, AddressCountry__c, AddressApartment__c,
                AddressBuilding__c,AddressBlock__c,AddressCity__c,AddressFloor__c,AddressLocality__c,AddressAddressNormalized__c,AddressPostalCode__c,AddressProvince__c,
                AddressAddressKey__c, AddressStreetId__c,NonDisconnectable__c,StartDate__c, EndDate__c, MarkedInvoices__c, Reason__c, EffectiveDate__c, ContactId, Contract__c, Supply__c, ServiceSite__c,
                AccountId, Supply__r.ServicePoint__r.Code__c, CustomerNeeds__c, InsolvencyManager__c, SubProcess__c, SLAExpirationDate__c, CreatedDate, ServiceSite__r.NLC__c, ServiceSite__r.CLC__c, ConnectionCertificateNumber__c, CerIssuingDate__c, ANRELicense__c, Batteries__c,
                Supply__r.RecordType.Name, ContractAccount__c, ContractAccount2__c, Contract2__c, BillingProfile__c, BillingProfile2__c,
                Amount__c, IncidentDate__c, ParentId, EvidenceDocumentsReceivedDate__c, DistributorResponseDate__c,
                Contract__r.ConsumptionConventions__c, Contract2__r.ConsumptionConventions__c, CaseNumber, ServicePointCode__c, Dossier__r.Origin__c, Dossier__r.Channel__c, AssetId
            FROM Case
            WHERE Contract__c = :contractId
            AND RecordTypeId = :recordTypeId
        ];
        return caseList;
    }

    public Map<String, List<Case>> listRelatedCases(String accountId, String slowRecordTypeId, String fastRecordTypeId) {
        Map<String, List<Case>> caseList = new Map<String, List<Case>>();
        List<Case> slowCases = [
                SELECT Id,AccountId,RecordTypeId
                FROM Case
                WHERE AccountId = :accountId AND isClosed = :false AND RecordTypeId = :slowRecordTypeId
        ];
        List<Case> fastCases = [
                SELECT Id,AccountId,RecordTypeId
                FROM Case
                WHERE AccountId = :accountId AND isClosed = :false AND RecordTypeId = :fastRecordTypeId
        ];
        System.debug('slowCasesRecord' + slowCases);
        System.debug('fastCasesRecord' + fastCases);
        caseList.put('slowCases', slowCases);
        caseList.put('fastCases', fastCases);
        return caseList;
    }

    public List<Case> getCasesByDossierId(Id dossierId) {

        return getCasesByDossierIds(new Set<Id> {dossierId});
    }

    public List<Case> getCasesByDossierIds(Set<Id> dossierIds) {
        return [
                SELECT Id, CaseNumber, RecordTypeId, RecordType.Name, RecordType.DeveloperName, SubProcess__c, SubType__c,
                        BillingProfile__c, ContractAccount__c, Contract__c, Phase__c,ServicePointCode__c,AccountId,
                                                                                                                        // FF Self Reading #WP3-P2
                        CompanyDivision__c,Status,Supply__c,Supply__r.RecordType.Name, StartDate__c, EndDate__c, MarkedInvoices__c,ContractAccount__r.SelfReadingPeriodEnd__c,
                                                                                                                        // FF Self Reading #WP3-P2
                        EffectiveDate__c, ReferenceDate__c, Origin, Channel__c, CustomerNotes__c, Trader__c, Distributor__c, Distributor__r.IsDisCoENEL__c,
                        Executor__c, ExecutionTerm__c, ExecutionAmount__c, Designer__c, DesignTerm__c, DesignAmount__c,
                        NLCCLC__c, VoltageLevel__c, VoltageSetting__c, Voltage__c, ContractualPower__c, AvailablePower__c, ServiceSite__c, PowerPhase__c,
                        Reason__c,Count__c,Amount__c,ContractAccount__r.StartDate__c,ContractAccount__r.CompanyDivision__c, CompensationType__c, CompensationCode__c, Description,Dossier__c, Supply__r.Contract__c, CustomerNeeds__c,
                        Supply__r.Product__r.CommercialProduct__r.TariffType__c,
                        Supply__r.InENELArea__c, Supply__r.Market__c, Supply__r.ServicePoint__c, Supply__r.ServicePoint__r.ConsumerType__c, Supply__r.ServicePoint__r.Code__c,
                        IncidentDate__c, ParentId, EvidenceDocumentsReceivedDate__c, DistributorResponseDate__c,ContactId,Supply__r.Contract__r.ContractType__c,
                        ContractAccount2__c, TMPPaymentDate__c, Supply__r.ContractAccount__c,Frequency__c,ComplaintsCount__c,Type,Duration__c,ContractAccount__r.Name, ContractAccount2__r.Name,
                        ConnectionCertificateNumber__c, CerIssuingDate__c, ANRELicense__c, Batteries__c, SLAExpirationDate__c, CancellationDetails__c, CancellationReason__c,
                        Supply__r.Status__c, Supply__r.RecordTypeId, Supply__r.RecordType.DeveloperName, Supply__r.ServicePoint__r.ENELTEL__c,Outcome__c, Supply__r.ServiceSite__c,
                        ContractAccount__r.IntegrationKey__c,IsDisCoENEL__c, OwnerId,
                        BillingProfile__r.RefundMethod__c, Account.IsPersonAccount, Supply__r.ServicePoint__r.Distributor__c, Supply__r.ServicePoint__r.Distributor__r.VATNumber__c,
                        Supply__r.ServicePoint__r.VoltageLevel__c, ProsumerPrice__c, SupplyType__c
                FROM Case
                WHERE Dossier__c = :dossierIds
        ];
    }

    /**
      * @author  Boubacar Sow
      * @date 27/01/2020
      * @description  Implement the logic of date check,  Implement the logic of date check, Retention creation
      *              [ENLCRO-425] Switch Out - Implement the logic of date check
      *              [ENLCRO-426] Switch Out - Implement the logic of Validity check
      *              [ENLCRO-428] Switch Out - Retention creation
      *
      * @param caseId
      *
      * @return Case
      */
    public Case getCaseForLogicCheckById(String caseId) {
        List<Case> caseList = [
            SELECT Id,CaseNumber,RecordTypeId, RecordType.DeveloperName,
                Status, EffectiveDate__c, ReferenceDate__c, Origin,
                Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c,
                CancellationReason__c, Phase__c, Trader__c, Reason__c, StartDate__c,
                EndDate__c, Contract__r.EndDate, Account.VIP__c
            FROM Case
            WHERE Id = :caseId
        ];
        if (caseList.isEmpty()) {
            return null;
        }
        return caseList.get(0);
    }

    public List<Case> listCasesForSwitchOutDateCheckByDossierId(Id dossierId) {
        List<Case> caseList = [
            SELECT Id,CaseNumber,RecordTypeId, RecordType.DeveloperName,
                Status, EffectiveDate__c, ReferenceDate__c, Origin,
                Supply__r.ServicePoint__r.Distributor__r.AtoAEnabled__c,
                CancellationReason__c, Phase__c, Trader__c, Reason__c, StartDate__c,
                EndDate__c, Contract__r.EndDate, Account.VIP__c
            FROM Case
            WHERE Dossier__c = :dossierId
        ];
        return caseList;
    }

    public List<Case> getOpenedCase(String accountId, String recordTypeId) {
        return [
                SELECT Id,CaseNumber
                FROM Case
                WHERE AccountId = :accountId AND RecordTypeId = :recordTypeId AND IsClosed =false
        ];

    }

    public Case getById(String caseId) {
        List<Case> caseList = [
                SELECT Id,RecordType.DeveloperName, Trader__c, Distributor__c, Voltage__c, PowerPhase__c, Description,
                        Dossier__c, Dossier__r.RequestType__c, Dossier__r.Commodity__c, Phase__c, RecordTypeId,
                        AvailablePower__c, ContractualPower__c, VoltageLevel__c,
                        AddressStreetName__c, AddressStreetType__c, AddressStreetNumber__c, AddressStreetNumberExtn__c, AddressCountry__c, AddressApartment__c,
                        AddressBuilding__c,AddressBlock__c,AddressCity__c,AddressFloor__c,AddressLocality__c,AddressAddressNormalized__c,AddressPostalCode__c,AddressProvince__c,
	                    AddressAddressKey__c, AddressStreetId__c,NonDisconnectable__c,StartDate__c, EndDate__c, MarkedInvoices__c, Reason__c, EffectiveDate__c, ContactId, Contract__c, Supply__c, ServiceSite__c,
                        AccountId, Supply__r.ServicePoint__r.Code__c, CustomerNeeds__c, InsolvencyManager__c, SubProcess__c, SLAExpirationDate__c, CreatedDate, ServiceSite__r.NLC__c, ServiceSite__r.CLC__c, ConnectionCertificateNumber__c, CerIssuingDate__c, ANRELicense__c, Batteries__c,
                        Supply__r.RecordType.Name, Supply__r.ServicePoint__c, ContractAccount__c, ContractAccount2__c, Contract2__c, BillingProfile__c, BillingProfile2__c,
                        Amount__c, IncidentDate__c, ParentId, EvidenceDocumentsReceivedDate__c, DistributorResponseDate__c,
                        Contract__r.ConsumptionConventions__c, Contract2__r.ConsumptionConventions__c, CaseNumber, ServicePointCode__c, Dossier__r.Origin__c, Dossier__r.Channel__c, AssetId,
                        SupplyType__c, Count__c, Origin, Channel__c, CompanyDivision__r.Code__c, Account.AccountNumber, CaseTypeCode__c, SubType__c, CustomerNotes__c, IsDisCoENEL__c,
                        Dossier__r.Status__c, BillingProfile2__r.DeliveryChannel__c, Account.IsPersonAccount, InvoiceDate__c, Supply__r.ServicePoint__r.ATRExpirationDate__c, Account.Id, Supply__r.ServicePoint__r.VoltageLevel__c,
                        ExternalRequestID__c, Supply__r.ServicePoint__r.Distributor__c, Supply__r.ServicePoint__r.Distributor__r.VATNumber__c, ProsumerPrice__c, Dossier__r.Opportunity__c
                FROM Case
                WHERE Id = :caseId
        ];
        //Tax_Subsidy__c,Tax_Subsidy__r.Id,Tax_Subsidy__r.RecordTypeId,Tax_Subsidy__r.Name,Tax_Subsidy__r.RecordType.Name,
        //                        Tax_Subsidy__r.Supply__r.Id
        if (caseList.size() > 0){
            return caseList.get(0);
        } else {
            return null;
        }
    }

    /**
      * @author  Baba Goudiaby
      * @description Query necessary fields for cancellation request
      * [ENLCRO-43] Request cancellation wp1 p2 - Implementation
      *
      * @return Case caseObject
      */
    public Case getForCancellationById(String caseId) {
        List<Case> caseList = [
                SELECT Id,CaseNumber,RecordTypeId, RecordType.DeveloperName, Status,AccountId,
                        EffectiveDate__c,Origin,NonDisconnectable__c,SubProcess__c,
                        CancellationReason__c,Phase__c,Trader__c,Reason__c,
                        StartDate__c, EndDate__c, MarkedInvoices__c, CaseTypeCode__c, ExternalRequestID__c, SupplyType__c
                FROM Case
                WHERE Id = :caseId
        ];
        return caseList.get(0);
    }

    public List<Case> getCaseList(String caseId) {
        List<Case> caseList = [
                SELECT Id,RecordType.DeveloperName, Trader__c,EffectiveDate__c,
                        AddressStreetName__c, AddressStreetType__c, AddressStreetNumber__c, AddressStreetNumberExtn__c, AddressCountry__c, AddressApartment__c,
                        AddressBuilding__c,AddressBlock__c,AddressCity__c,AddressFloor__c,AddressLocality__c,AddressAddressNormalized__c,AddressPostalCode__c,AddressProvince__c,
	                    AddressAddressKey__c, AddressStreetId__c,
                        NonDisconnectable__c
                FROM Case
                WHERE Id = :caseId
        ];
        System.debug('caseList' + caseList);
        return caseList;
    }

     public List<Case> listByIds(Set<Id> caseIds) {
        List<Case> caseList = [
                SELECT Id, RecordTypeId, RecordType.DeveloperName, AccountId, Supply__c, ServicePointCode__c, Trader__c, Distributor__c,
                        AddressStreetName__c, AddressStreetType__c, AddressStreetNumber__c, AddressStreetNumberExtn__c, AddressCountry__c, AddressApartment__c,
                        AddressBuilding__c,AddressBlock__c,AddressCity__c,AddressFloor__c,AddressLocality__c,AddressAddressNormalized__c,AddressPostalCode__c,AddressProvince__c,
                        AddressAddressKey__c, AddressStreetId__c,EffectiveDate__c, CancellationReason__c, CancellationDetails__c,
                        NonDisconnectable__c, Voltage__c, VoltageLevel__c, PowerPhase__c, Pressure__c, PressureLevel__c, ContractualPower__c, AvailablePower__c,
                        ConversionFactor__c, EstimatedConsumption__c, Dossier__c, Dossier__r.CommercialProduct__c, Dossier__r.Language__c,SubProcess__c,ContactId, Phase__c,
                        NeedsExecutor__c, NeedsDesigner__c, SupplyOperation__c, IsDisCoENEL__c, Status,
                        BillingProfile__c, BillingProfile__r.BillingAddressKey__c, BillingProfile__r.BillingAddressNormalized__c,
                        BillingProfile__r.BillingApartment__c, BillingProfile__r.BillingBlock__c, BillingProfile__r.BillingBuilding__c,
                        BillingProfile__r.BillingCity__c, BillingProfile__r.BillingCountry__c, BillingProfile__r.BillingFloor__c,
                        BillingProfile__r.BillingLocality__c, BillingProfile__r.BillingPostalCode__c, BillingProfile__r.BillingProvince__c,
                        BillingProfile__r.BillingStreetId__c, BillingProfile__r.BillingStreetName__c, BillingProfile__r.BillingStreetNumber__c,
                        BillingProfile__r.BillingStreetNumberExtn__c, BillingProfile__r.BillingStreetType__c,Supply__r.ServicePoint__c, Supply__r.ServicePoint__r.AvailablePower__c, Supply__r.ServicePoint__r.ContractualPower__c,
                        Supply__r.ServicePoint__r.EstimatedConsumption__c, Supply__r.ServicePoint__r.PowerPhase__c,
                        Supply__r.ServicePoint__r.VoltageLevel__c, Supply__r.ServicePoint__r.Voltage__c, Supply__r.ServiceSite__c
                FROM Case
                WHERE Id IN :caseIds
        ];
        System.debug('caseList' + caseList);
        return caseList;
    }

    public List<Case> listByIdsForActivities(Set<Id> caseIds) {
        List<Case> caseList = [
                SELECT Id, OwnerId, RecordTypeId, RecordType.DeveloperName, AccountId, Market__c, IsDisCoENEL__c,
                        Dossier__c, Dossier__r.Opportunity__c, Dossier__r.Opportunity__r.ContractId, Dossier__r.ContractAddition__c,
                        Dossier__r.CommercialProduct__c, Dossier__r.Language__c,
                        Dossier__r.CommercialProduct__r.AnnexATemplateCode__c, Dossier__r.CommercialProduct__r.AnnexATemplateCodeVersion__c,
                        Dossier__r.CommercialProduct__r.ContractTemplateCode__c, Dossier__r.CommercialProduct__r.ContractTemplateCodeVersion__c,
                        Dossier__r.CommercialProduct__r.OfferTemplateCode__c, Dossier__r.CommercialProduct__r.OfferTemplateCodeVersion__c,Dossier__r.CommercialProduct__r.AnnexABisTemplateCode__c,
                        Dossier__r.CommercialProduct__r.Annex1TemplateCode__c,Dossier__r.CommercialProduct__r.Annex2TemplateCode__c,Dossier__r.CommercialProduct__r.Annex3TemplateCode__c,
                        Dossier__r.SendingChannel__c, Dossier__r.Email__c, Dossier__r.Phone__c, Dossier__r.MailChannelType__c,
                        Dossier__r.Origin__c, Dossier__r.Channel__c, Dossier__r.SubProcess__c, Dossier__r.Commodity__c, Dossier__r.PrintBundleSelection__c,
                        Dossier__r.CustomerInteraction__r.Contact__r.Email, Dossier__r.IsDual__c, Dossier__r.RelatedDossier__c, Dossier__r.RelatedDossier__r.Commodity__c,
                        Account.IsPersonAccount, Account.Email__c, Account.PersonEmail,
                        Account.ResidentialAddressKey__c, Account.ResidentialAddressNormalized__c, Account.ResidentialApartment__c,
                        Account.ResidentialBlock__c, Account.ResidentialBuilding__c, Account.ResidentialCity__c, Account.ResidentialCountry__c,
                        Account.ResidentialFloor__c, Account.ResidentialLocality__c, Account.ResidentialPostalCode__c,
                        Account.ResidentialProvince__c, Account.ResidentialStreetId__c, Account.ResidentialStreetName__c,
                        Account.ResidentialStreetNumber__c, Account.ResidentialStreetNumberExtn__c, Account.ResidentialStreetType__c,
                        ContractAccount__c, ContractAccount__r.Market__c, ContractAccount__r.BillingProfile__c,
                        ContractAccount__r.BillingProfile__r.DeliveryChannel__c, ContractAccount__r.BillingProfile__r.Email__c, ContractAccount__r.BillingProfile__r.MobilePhone__c,
                        ContractAccount__r.BillingProfile__r.BillingAddressKey__c, ContractAccount__r.BillingProfile__r.BillingAddressNormalized__c,
                        ContractAccount__r.BillingProfile__r.BillingApartment__c, ContractAccount__r.BillingProfile__r.BillingBlock__c,
                        ContractAccount__r.BillingProfile__r.BillingBuilding__c, ContractAccount__r.BillingProfile__r.BillingCity__c,
                        ContractAccount__r.BillingProfile__r.BillingCountry__c, ContractAccount__r.BillingProfile__r.BillingFloor__c,
                        ContractAccount__r.BillingProfile__r.BillingLocality__c, ContractAccount__r.BillingProfile__r.BillingPostalCode__c,
                        ContractAccount__r.BillingProfile__r.BillingProvince__c, ContractAccount__r.BillingProfile__r.BillingStreetId__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetName__c, ContractAccount__r.BillingProfile__r.BillingStreetNumber__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetNumberExtn__c, ContractAccount__r.BillingProfile__r.BillingStreetType__c,
                        BillingProfile__c, BillingProfile__r.BillingAddressKey__c, BillingProfile__r.BillingAddressNormalized__c,
                        BillingProfile__r.BillingApartment__c, BillingProfile__r.BillingBlock__c, BillingProfile__r.BillingBuilding__c,
                        BillingProfile__r.BillingCity__c, BillingProfile__r.BillingCountry__c, BillingProfile__r.BillingFloor__c,
                        BillingProfile__r.BillingLocality__c, BillingProfile__r.BillingPostalCode__c, BillingProfile__r.BillingProvince__c,
                        BillingProfile__r.BillingStreetId__c, BillingProfile__r.BillingStreetName__c, BillingProfile__r.BillingStreetNumber__c,
                        BillingProfile__r.BillingStreetNumberExtn__c, BillingProfile__r.BillingStreetType__c,
                        Supply__c, Supply__r.InENELArea__c, Supply__r.Market__c, Supply__r.ServicePoint__r.Distributor__r.IsDisCoENEL__c,
                        Supply__r.Contract__c, Supply__r.ContractAccount__c, Supply__r.ContractAccount__r.Market__c,
                        Supply__r.ContractAccount__r.BillingProfile__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.DeliveryChannel__c, Supply__r.ContractAccount__r.BillingProfile__r.Email__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.MobilePhone__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingAddressKey__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingAddressNormalized__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingApartment__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingBlock__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingBuilding__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingCity__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingCountry__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingFloor__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingLocality__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingPostalCode__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingProvince__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetId__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetName__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetNumber__c,
                        Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetNumberExtn__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetType__c,
                        Distributor__c, Distributor__r.IsDisCoENEL__c, Trader__c, Phase__c, CancellationReason__c,
                        NonDisconnectable__c,StartDate__c, EndDate__c, MarkedInvoices__c, Reason__c, EffectiveDate__c, ContactId, Contract__c,
                        SubProcess__c, SLAExpirationDate__c, CreatedDate, ParentId, EvidenceDocumentsReceivedDate__c, DistributorResponseDate__c,
                        CaseNumber, ServicePointCode__c, AssetId, SupplyType__c, Count__c, Origin, Channel__c, NeedsExecutor__c, NeedsDesigner__c, SupplyOperation__c
                FROM Case
                WHERE Id IN :caseIds
        ];
        System.debug('caseList' + caseList);
        return caseList;
    }

    public List<Case> listTaxChangeCasesBySupplyIds(List<Supply__c> suppliesList,String recordType) {
        Set<String> suppliesId = new Set<String>();
        for(Supply__c supply :suppliesList){
            suppliesId.add(supply.Id);
        }
        return [
                SELECT Id,Supply__c,RecordTypeId,Status
                FROM Case
                WHERE Supply__c IN :suppliesId AND RecordTypeId =: recordType AND Status != :CONSTANTS.CASE_STATUS_CANCELED
                AND Status != :CONSTANTS.CASE_STATUS_EXECUTED
        ];
    }



    public List<Case> listConnectionCasesForPreOfferingByDossier(Id connectionDossierId) {
        return [SELECT Id, ServicePointCode__c, SuspendChildren__c,
                       Dossier__r.Account__c, Dossier__r.Origin__c, Dossier__r.Channel__c, Dossier__r.Commodity__c,SubProcess__c
                FROM Case
                WHERE Dossier__c = :connectionDossierId
                    AND RecordType.DeveloperName = 'Connection_ELE'
                    AND Status != :CONSTANTS.CASE_STATUS_CANCELED];
    }

    public List<Case> listTerminationCasesBySupplyId(Id supplyId) {
        return [SELECT Id
                FROM Case
                WHERE (RecordType.DeveloperName = 'Termination_GAS' OR RecordType.DeveloperName = 'Termination_ELE')
                    AND Status != :CONSTANTS.CASE_STATUS_CANCELED
                    AND Status != :CONSTANTS.CASE_STATUS_EXECUTED
                    AND Supply__c = :supplyId
        ];
    }

    public List<Case> listCasesByDossierIdsForGrouping(Set<Id> dossierIds) {
        return [SELECT Id, Dossier__c, CaseGroup__c, CaseGroup__r.IntegrationKey__c, CaseGroup__r.CaseCount__c,
                       Phase__c, Supply__c, Supply__r.ServiceSite__c
                FROM Case
                WHERE Dossier__c IN :dossierIds AND IsClosed = FALSE AND Supply__r.ServiceSite__c != NULL];
    }

    public List<Case> listCasesByCaseGroup(Id caseGroupId) {
        return [SELECT Id, RecordTypeId, Phase__c, IsClosed
                FROM Case WHERE CaseGroup__c = :caseGroupId];
    }

    public List<Case> listCasesByIdsWithCaseGroup(Set<Id> caseIds) {
        return [SELECT CaseGroup__c, CaseGroup__r.CaseCount__c, CaseGroup__r.ExecutionLogCount__c, CaseGroup__r.Status__c
                FROM Case WHERE Id IN :caseIds];
    }

    public List<Case> listCasesByDossierIds(Set<Id> dossierIds) {
        return [SELECT Id, Dossier__c, Status, RecordType.DeveloperName, Phase__c FROM Case WHERE Dossier__c IN :dossierIds];
    }

    public List<Case> listSuspendedChildrenCases(Id parentCaseId) {
        return [SELECT Id, RecordTypeId, NeedsExecutor__c, Phase__c, Status, Dossier__r.Phase__c
                FROM Case WHERE ParentId = :parentCaseId
                    AND Status = :CONSTANTS.CASE_STATUS_ONHOLD];
    }

    public Map<String, Case> getListCaseByIntegrationKey(List<String> IntegrationKeys) {
        Map<String, Case> mapCaseId = new Map<String, Case>();
        List<Case> caseList = [
                SELECT Id,CaseNumber,RecordTypeId, RecordType.DeveloperName, Status,IntegrationKey__c,SubType__c
                FROM Case
                WHERE IntegrationKey__c IN :IntegrationKeys
        ];
        for (Case caseRecord : caseList) {
            mapCaseId.put(caseRecord.IntegrationKey__c, caseRecord);
        }
        return mapCaseId;
    }


    public Map<String, Case> getListCaseByCaseNumber(List<String> caseNumbers) {
        Map<String, Case> mapCaseId = new Map<String, Case>();
        List<Case> caseList = [
                SELECT Id,CaseNumber,RecordTypeId, RecordType.DeveloperName, Status,IntegrationKey__c,SubType__c
                FROM Case
                WHERE CaseNumber IN :caseNumbers
        ];
        for (Case caseRecord : caseList) {
            mapCaseId.put(caseRecord.CaseNumber, caseRecord);
        }
        return mapCaseId;
    }

    public List<Case> listOpenCasesByAccountAndRecordTypeNameandSubProcess(String accountId, String recordTypeName, String subProcess) {
        return [
                SELECT Id
                FROM Case
                WHERE IsClosed = false
                AND AccountId = :accountId
                AND RecordType.DeveloperName = :recordTypeName
                AND SubProcess__c = :subProcess

        ];
    }

    public Map<Id, Case> listCaseAndSupplyByIds(Set<Id> caseIds) {
        return new Map<Id, Case>([
            SELECT Id, RecordTypeId, RecordType.DeveloperName, AccountId, Supply__c, ServicePointCode__c,
                    Supply__r.ServicePoint__r.AvailablePower__c,Supply__r.ServicePoint__r.ContractualPower__c,Supply__r.ServicePoint__r.VoltageLevel__c,
                    Supply__r.ServicePoint__r.Voltage__c,Supply__r.ServicePoint__r.PowerPhase__c,Supply__r.ServicePoint__r.Distributor__c,Supply__r.ServicePoint__r.Trader__c,
                    Supply__r.ServicePoint__r.EstimatedConsumption__c,Supply__r.ServicePoint__r.Code__c,Supply__r.ContractAccount__r.BillingProfile__c
            FROM Case
            WHERE Id IN :caseIds
        ]);
    }

    public List<Case> getCasesByBillingProfile(String billingProfileId) {
        return [
                SELECT Id
                FROM Case
                WHERE BillingProfile__c=:billingProfileId OR ContractAccount__r.BillingProfile__c = :billingProfileId OR Supply__r.ContractAccount__r.BillingProfile__c = :billingProfileId LIMIT 1
        ];
    }

    public List<Case> getCasesByContractAccount(String contractAccountId) {
        return [
                SELECT Id
                FROM Case
                WHERE ContractAccount__c  = :contractAccountId OR  Supply__r.ContractAccount__c  = :contractAccountId LIMIT 1
        ];
    }
    /**
       * @author Boubacar Sow
       * @description [ENLCRO-1101] Repayment Schedule - ZFOFM_SAPQUERY_FILENET_DOC
       * @date  28/07/2020
       * @param caseId Case
       * @param contractAccountId Contract Account
       * @param accountId Account
       *
       * @return Map<Id, Case>
       */
    public Map<Id, Case> getCaseForZfofmSapFilenet(String caseId, String contractAccountId, String accountId ) {
        return new Map<Id, Case>([
                SELECT Id, StartDate__c, EndDate__c, ContractAccount__c, AccountId
                FROM Case
                WHERE Id = :caseId
                AND ContractAccount__c = :contractAccountId
                AND AccountId = :accountId
                LIMIT  999
        ]);
    }

    public List<Case> getPvcCaseListByIds(Set<Id> caseIds) {
        List<Case> caseList = [
                SELECT Id, RecordTypeId, RecordType.DeveloperName, AccountId, Supply__c, Dossier__c
                FROM Case
                WHERE Id IN :caseIds AND (RecordType.DeveloperName = 'TechnicalDataChange_GAS'
                OR RecordType.DeveloperName = 'TechnicalDataChange_ELE')
        ];
        return caseList;
    }

    public Map<Id, Case> getCaseListByParentId(Set<Id> parentCaseIds) {
        Map<Id, Case> mapCase = new Map<Id, Case>();
        List<Case> caseList = [
                SELECT Id, Status, ParentId
                FROM Case WHERE ParentId IN :parentCaseIds
        ];

        for(Case c : caseList){
            mapCase.put(c.ParentId, c);
        }
        return mapCase;
    }
}