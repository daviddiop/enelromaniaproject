/**
 * Created by napoli on 26/09/2019.
 */

@IsTest
private class MRO_TR_IndividualHandlerTst {
    @IsTest
    static void testBehavior() {
        MRO_UTL_TestDataFactory.individualBuilder individualBuilder =  MRO_UTL_TestDataFactory.individual().createIndividual();
        Individual ind = individualBuilder.build();

        Database.SaveResult[] lsr = Database.insert(new Individual[]{ind},false);
        for (Database.SaveResult sr : lsr) {
            System.assertEquals(true, sr.isSuccess());
        }

        individualBuilder.setNationalIdentityNumber('1800101221145');
        lsr = Database.update(new Individual[]{ind},false);
        for (Database.SaveResult sr : lsr) {
            System.assertEquals(false, sr.isSuccess());
        }
    }
}