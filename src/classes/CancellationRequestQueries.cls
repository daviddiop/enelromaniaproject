/**
 * Created by goudiaby on 26/08/2019.
 */

public with sharing class CancellationRequestQueries {
    public static CancellationRequestQueries getInstance() {
        return new CancellationRequestQueries();
    }

    public CancellationRequest__c findCancellationReasons(String objectName, String objectRecordType, String originStage) {
        return [
                SELECT Id, CancellationReasons__c
                FROM CancellationRequest__c
                WHERE Object__c = :objectName AND ObjectRecordType__c = :objectRecordType AND OriginStage__c = :originStage
                LIMIT 1
        ];
    }
}