@isTest
public with sharing class ServicePointCntTst {

    @testSetup
    private static void setup() {
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new list<Account>();
        listAccount.add(TestDataFactory.account().personAccount().build());
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Account accountKey = TestDataFactory.account().setKey('000034').build();

        Account accountTrader = TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'BusinessAccount1';
        accountTrader.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        accountTrader.Key__c = '1324433';
        accountTrader.BusinessType__c = 'Commercial areas';
        insert accountTrader;
        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '01234560001';
        servicePoint.Account__c = listAccount[1].Id;
        servicePoint.Trader__c = accountTrader.Id;
        servicePoint.Distributor__c = listAccount[1].Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        Dossier__c dossier = TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[1].Id;
        insert dossier;
    }


    @isTest
    static void findCodeTest() {
        ServicePoint__c servicePoint = [
            SELECT Id, Code__c
            FROM ServicePoint__c
            LIMIT 1
        ];

        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];        

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'pointCode' => servicePoint.Code__c,
            'companyDivisionId' => companyDivision.Id
        };

        Object response = TestUtils.exec('ServicePointCnt', 'findCode', inputJSON, true);

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('servicePointId') != null);

        Map<String, String > inputJSONError = new Map<String, String>{
            'pointCode' => '',
            'companyDivisionId' => companyDivision.Id
        };

        TestUtils.exec('ServicePointCnt', 'findCode', inputJSONError, false);

        Test.stopTest();
    }

    @isTest
    static void findCodeTm2Test() {
        ServicePoint__c servicePoint = [
            SELECT Id, Code__c
            FROM ServicePoint__c
            LIMIT 1
        ];
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];

        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];
        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().build();
        supply.CompanyDivision__c = companyDivision.Id;
        //supply.ServicePoint__c = servicePoint.Id;
        insert supply;

        servicePoint.Account__c = account.Id;
        update servicePoint;

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'pointCode' => servicePoint.Code__c,
            'accountId' => account.Id,
            'companyDivisionId' => companyDivision.Id,
            'companyDivisionEnforced' => 'true'
        };

        Object response = TestUtils.exec('ServicePointCnt', 'findCodeTm2', inputJSON, true);

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('suppliesByServicePointCode') != null);

        inputJSON = new Map<String, String>{
            'pointCode' => servicePoint.Code__c,
            'accountId' => account.Id,
            'companyDivisionId' => companyDivision.Id,
            'companyDivisionEnforced' => 'false'
        };
        response = TestUtils.exec('ServicePointCnt', 'findCodeTm2', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true,result.get('servicePointId') != null);

       inputJSON = new Map<String, String>{
            'pointCode' => '',
            'accountId' => account.Id,
            'companyDivisionId' => companyDivision.Id,
            'companyDivisionEnforced' => 'true'
        };
        try {
            TestUtils.exec('ServicePointCnt', 'findCodeTm2', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }

        Test.stopTest();
    }

    
    /*@IsTest
    static void findCodeTm2ExcepTest() {

        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'pointCode' => '',
            'accountId' => account.Id,
            'companyDivisionId' => companyDivision.Id,
            'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        try {
            TestUtils.exec('ServicePointCnt', 'findCodeTm2', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }*/

}