/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 16.10.20.
 */

public with sharing class MRO_SRV_SapQueryFilenetDoc extends MRO_SRV_SapQueryHelper {
    private static final String requestTypeQueryFilenetDoc = 'QueryFilenet';

    public static MRO_SRV_SapQueryFilenetDoc getInstance() {
        return new MRO_SRV_SapQueryFilenetDoc();
    }

    public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {
        System.debug('Build request ###########');
        System.debug(argsMap);

        if(argsMap.get('conditions') == null){
            SObject obj = (SObject) argsMap.get('sender');

            Map<String, String> parameters = (Map<String, String>)argsMap.get('parameters');
            MRO_LC_FileNetDoc.RequestParam requestParam = getParams(obj.Id);
            if(parameters.get('documentType') != null){
                requestParam.documentType = parameters.get('documentType');
            }
            Map<String, Object> conditions = requestParam.toMap();

            argsMap.put('conditions', conditions);
            System.debug(parameters);
            System.debug(conditions);
        }

        System.debug(argsMap);
        Set<String> conditionFields = new Set<String>{
                'CompanyDivisionCode', 'ProcessCode', 'DocumentIssuedFrom', 'DocumentIssuedTo', 'RequestId', 'IntegrationId', 'CustomerCode', 'TaxNumber', 'DocumentType'
        };
        wrts_prcgvr.MRR_1_0.Request simpleRequest = buildSimpleRequest(argsMap, conditionFields);
        System.debug(simpleRequest);
        return simpleRequest;
    }

    public List<MRO_LC_FileNetDoc.FileNetDoc> responseToDto(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse) {
        if (calloutResponse == null || calloutResponse.responses.isEmpty() || calloutResponse.responses[0] == null) {
            return null;
        }

        System.debug(calloutResponse.responses[0].objects);
        System.debug(calloutResponse.responses[0].header);

        if(calloutResponse.responses[0].header != null && calloutResponse.responses[0].header.fields != null && !calloutResponse.responses[0].header.fields.isEmpty()){
            for(wrts_prcgvr.MRR_1_0.Field field : calloutResponse.responses[0].header.fields){
                if(field.name == 'ErrorMessage'){
                    throw new WrtsException(field.value);
                }
            }
        }

        if (calloutResponse.responses[0].objects == null || calloutResponse.responses[0].objects.isEmpty()) {
            throw new WrtsException('Service unavailable');
        }

        List<MRO_LC_FileNetDoc.FileNetDoc> fileNetDocs = new List<MRO_LC_FileNetDoc.FileNetDoc>();

        for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {

            if (responseWo.objects != null && responseWo.objects.size() > 0) {
                for (wrts_prcgvr.MRR_1_0.WObject documentWo : responseWo.objects) {
                    Map<String, String> documentWoMap = MRO_UTL_MRRMapper.wobjectToMap(documentWo);

                    MRO_LC_FileNetDoc.FileNetDoc filenetDTO = new MRO_LC_FileNetDoc.FileNetDoc(documentWoMap);

                    fileNetDocs.add(filenetDTO);
                }
            }
        }

        System.debug(fileNetDocs);
        return fileNetDocs;
    }

    public MRO_LC_FileNetDoc.RequestParam getParams(String caseId){
        MRO_LC_FileNetDoc.RequestParam params = new MRO_LC_FileNetDoc.RequestParam();
        List<Case> cases = [SELECT Id, RecordType.DeveloperName, Outcome__c, CompanyDivision__r.Code__c, CaseTypeCode__c, StartDate__c,
                Account.RecordType.DeveloperName, Account.VATNumber__c, Account.NationalIdentityNumber__pc, ContractAccount__r.IntegrationKey__c,
                CaseNumber, Account.AccountNumber FROM Case WHERE Id = :caseId LIMIT 1];
        Case caseRecord = cases[0];

        params.companyDivisionCode = caseRecord.CompanyDivision__r.Code__c;
        params.processCode = caseRecord.CaseTypeCode__c;
        params.documentIssuedFrom = String.valueOf(caseRecord.StartDate__c);
        params.documentIssuedTo = String.valueOf(Date.today());
        params.requestId = caseRecord.CaseNumber;
        params.documentType = '1326_CM_ISU';
        params.customerCode = caseRecord.Account.AccountNumber;

        if(caseRecord.RecordType.DeveloperName == 'AccountSituation') {
            if (caseRecord.Outcome__c == 'CNP/CUI Level') {
                //params.taxNumber = caseRecord.Account.RecordType.DeveloperName == 'Business' ? caseRecord.Account.VATNumber__c : caseRecord.Account.NationalIdentityNumber__pc;
                //params.customerCode = null;
            } else if (caseRecord.Outcome__c == 'Contract Account Level') {
                params.integrationId = caseRecord.ContractAccount__r ?.IntegrationKey__c;
            }
        }

        return params;
    }

    public void createFileMetadata(List<MRO_LC_FileNetDoc.FileNetDoc> filenetDocList) {
        if(filenetDocList == null || filenetDocList.isEmpty()){
            return;
        }

        Set<String> documentTypeCodes = new Set<String>();
        Set<String> caseNumbers = new Set<String>();
        Set<String> filenetDocIds = new Set<String>();

        for(MRO_LC_FileNetDoc.FileNetDoc fnd : filenetDocList){
            documentTypeCodes.add('SAP' + fnd.documentType);
            caseNumbers.add(fnd.requestId);
            filenetDocIds.add(fnd.filenetId);
        }

        List<Case> cases = [SELECT Id, Dossier__c, Phase__c, RecordTypeId FROM Case WHERE CaseNumber IN :caseNumbers];
        if(cases.size() == 0){
            throw new WrtsException('Case not found ' + caseNumbers);
        }

        List<DocumentType__c> documentTypes = [SELECT Id, DocumentTypeCode__c FROM DocumentType__c WHERE DocumentTypeCode__c IN :documentTypeCodes];
        if(documentTypes.size() == 0){
            throw new WrtsException('Document type not found ' + documentTypeCodes);
        }
        Map<String, DocumentType__c> documentTypesMap = new Map<String, DocumentType__c>();
        for(DocumentType__c documentType : documentTypes){
            String dtc = documentType.DocumentTypeCode__c;
            documentTypesMap.put(dtc.substring(3, dtc.length()), documentType);
        }

        Set<Id> caseIds = new Map<Id, Case>(cases).keySet();
        List<FileMetadata__c> existFilemetadata = [SELECT Id, ExternalId__c FROM FileMetadata__c WHERE Case__c IN :caseIds AND ExternalId__c IN :filenetDocIds];
        Map<String, FileMetadata__c> fmdByFilenetIdMap = new Map<String, FileMetadata__c>();
        for(FileMetadata__c fmd : existFilemetadata){
            fmdByFilenetIdMap.put(fmd.ExternalId__c, fmd);
        }

        List<FileMetadata__c> fileMetadataList = new List<FileMetadata__c>();
        for (MRO_LC_FileNetDoc.FileNetDoc filenetDoc : filenetDocList) {
            if(filenetDoc.filenetId == null || fmdByFilenetIdMap.get(filenetDoc.filenetId) != null){
                continue;
            }
            fileMetadataList.add(new FileMetadata__c(
                    Case__c = cases[0].Id,
                    ExternalId__c = filenetDoc.filenetId,
                    Dossier__c = cases[0].Dossier__c,
                    //DocumentId__c = filenetDoc.documentId,
                    DocumentType__c = documentTypesMap.get(filenetDoc.documentType)?.Id
            ));
        }
        System.debug(fileMetadataList);
        if(fileMetadataList.size() > 0){
            MRO_UTL_Transitions.getInstance().checkAndApplyAutomaticTransitionWithTag(cases[0], MRO_UTL_Constants.getAllConstants().PRINT_DONE_TAG);
            MRO_SRV_DatabaseService.getInstance().insertSObject(fileMetadataList);
        }
    }

    public override wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse) {
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response = new wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse();
        response.success = true;
        try {
            List<MRO_LC_FileNetDoc.FileNetDoc> fileNetDocs = responseToDto(calloutResponse);
            createFileMetadata(fileNetDocs);
        }catch (Exception e){
            response.message = e.getMessage();
            System.debug(e.getMessage());
        }
        return response;
    }
}