public inherited sharing class TriggerManager {

	// public static Boolean isOverridden(String settingName) {
	// 	if (!settingName.contains(Utils.getNamespacePrefix()))
	// 		settingName = Utils.getNamespacePrefix() + settingName;

    // 	if (!Test.isRunningTest() &&
    //     	(OverrideSetting__c.getOrgDefaults().get(settingName) <> null) &&
	// 		((DateTime)OverrideSetting__c.getOrgDefaults().get(settingName) > System.now()) &&
	// 		(OverrideSetting__c.getOrgDefaults().User__c <> null) &&
	// 		(OverrideSetting__c.getOrgDefaults().User__c.left(15).equals(UserInfo.getUserId().left(15)))) {
	// 		System.debug(LoggingLevel.INFO, 'Trigger disabled by custom setting');
	// 		return true; 
	// 	}
	// 	return false;
	// }

	public static void handle(ISObjectTriggerHandler handler) {
		try {
			if (Trigger.isBefore) {
				if (Trigger.isInsert)
					handler.beforeInsert();
				else if (Trigger.isUpdate)
					handler.beforeUpdate();
				else if (Trigger.isDelete)
					handler.beforeDelete();
			} else if (Trigger.isAfter) {
		    	if (Trigger.isInsert)
		    		handler.afterInsert();
		    	else if (Trigger.isUpdate)
		    		handler.afterUpdate();
		    	else if (Trigger.isDelete)
		    		handler.afterDelete();
		    	else if (Trigger.isUndelete)
		    		handler.afterUndelete();
			}
		} catch(DmlException e) {
			List<SObject> triggerObjs = Trigger.new;

            if(triggerObjs == null) 
            	triggerObjs = Trigger.old;

        	for(SObject obj : triggerObjs) {
            	obj.addError(e.getdmlMessage(0));
        	}
    	} catch(Exception e) {
            List<SObject> triggerObjs = Trigger.new;

            if(triggerObjs == null) 
            	triggerObjs = Trigger.old;

        	for(SObject obj : triggerObjs) {
            	obj.addError(e.getStackTraceString());
        	}
    	}
	}


    public interface ISObjectTriggerHandler{
        void beforeInsert();
        void beforeUpdate();
        void beforeDelete();
        void afterInsert();
        void afterUpdate();
        void afterDelete();
        void afterUndelete();
    }
}