/**
 * Created by Octavian on 12/2/2019.
 */

public inherited sharing class MRO_QR_Index {

    public static MRO_QR_Index getInstance() {
        return new MRO_QR_Index();
    }

    public Index__c getByCaseIdAndMeterName(String caseId, String meterNumber) {
        Index__c[] indexObjects = [
            SELECT Id, ActiveEnergy__c, ActiveEnergyR1__c, ActiveEnergyR2__c,
                ActiveEnergyR3__c, CapacitiveEnergy__c, InductiveEnergy__c
            FROM Index__c
            WHERE Case__c = :caseId
                AND MeterNumber__c = :meterNumber
        ] ;
        if (indexObjects.size() > 0) {
            return indexObjects[0];
        }
        return null;
    }

   /**
    * @author Luca Ravicini
    * @description  find Index__c via Opportunity service item Ids
    * @param osiIds Opportunity service item Ids
    * @return List<OpportunityServiceItem__c>
    * @since Mar 3, 2020
    */
    public List<Index__c> getByOsiIds(Set<Id> osiIds) {
        return  [
                SELECT Id,MeterNumber__c , ActiveEnergy__c, ActiveEnergyR1__c, ActiveEnergyR2__c,
                        ActiveEnergyR3__c, CapacitiveEnergy__c, InductiveEnergy__c,OpportunityServiceItem__c,
                        Case__c
                FROM Index__c
                WHERE OpportunityServiceItem__c IN : osiIds
        ] ;

    }
// FF Self Reading #WP3-P2
    public List<Index__c> getIndexesByCaseIds(Set<Id> caseIds) {
        return  [
                SELECT Id,MeterNumber__c , ActiveEnergy__c, ActiveEnergyR1__c, ActiveEnergyR2__c,
                        ActiveEnergyR3__c, CapacitiveEnergy__c, InductiveEnergy__c,OpportunityServiceItem__c,
                        Case__c
                FROM Index__c
                WHERE Case__c IN : caseIds
        ] ;

    }
// FF Self Reading #WP3-P2
}