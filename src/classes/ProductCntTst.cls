/**
 * Test class for  ProductCntTest
 *
 * @author Moussa Fofana
 * @version 1.0
 * @description Test class for  ProductCnt
 * @uses
 * @code
 * @history
 * 2019-04-23:  Moussa Fofana
 * 2019-12-02:  David Diop coverage(93%)
 */
@isTest
public with sharing class ProductCntTst {

    @testSetup
    static void setup() {

        CompanyDivision__c companyDivision = TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Pricebook2 pricebook = TestDataFactory.priceBook().build();
        insert pricebook;

        Product2 product1 = TestDataFactory.product2().build();
        insert product1;

        Product2 product2 = TestDataFactory.product2()
            .setFamily('Default')
            .build();
        insert product2;

        insert TestDataFactory.pricebookEntry()
            .setProduct2Id(product1.Id)
            .setPricebook2Id(Test.getStandardPricebookId())
            .build();

        ProductOption__c productOption1 = TestDataFactory.productOption()
            .setRecordType(Schema.SObjectType.ProductOption__c.getRecordTypeInfosByDeveloperName().get(ProductService.VALUE_TYPE).getRecordTypeId())
            .build();
        insert productOption1;

        ProductOption__c productOption2 = TestDataFactory.productOption()
            .setRecordType(Schema.SObjectType.ProductOption__c.getRecordTypeInfosByDeveloperName().get(ProductService.FAMILY_TYPE).getRecordTypeId())
            .build();
        insert productOption2;

        ProductOption__c productOption3 = TestDataFactory.productOption()
                .setRecordType(Schema.SObjectType.ProductOption__c.getRecordTypeInfosByDeveloperName().get(ProductService.VALUE_TYPE).getRecordTypeId())
                .build();
        productOption3.Type__c='Key/Value';
        insert productOption3;

        insert TestDataFactory.productOptionRelation()
            .setProductId(product1.Id)
            .setProductOptionId(productOption1.Id)
            .build();

        insert TestDataFactory.productOptionRelation()
            .setProductId(product1.Id)
            .setProductOptionId(productOption2.Id)
            .build();

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.Pricebook2Id = pricebook.Id;
        insert opportunity;
    }

    @IsTest
    static void getInitDataTest() {
        Opportunity opportunity = [
            SELECT Id,Name
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON =  new Map<String, String>{
            'opportunityId' => opportunity.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('ProductCnt', 'getInitData', inputJSON, true);
        System.assertNotEquals(null, response);
        System.assertNotEquals(null, response.get('opportunity'));
        Pricebook2 pricebook = TestDataFactory.priceBook().build();
        insert pricebook;
        response = (Map<String, Object>) TestUtils.exec('ProductCnt', 'getInitData', inputJSON, true);
        System.assertNotEquals(null, response);
        System.assertNotEquals(null, response.get('opportunity'));
        Test.stopTest();
    }

    @IsTest
    static void getInitDataPricebook2Test() {
        Opportunity opportunity = [
            SELECT Id,Name,Pricebook2Id
            FROM Opportunity
            LIMIT 1
        ];
        opportunity.Pricebook2Id = null;
        update opportunity;
        Map<String, String > inputJSON =  new Map<String, String>{
            'opportunityId' => opportunity.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('ProductCnt', 'getInitData', inputJSON, true);
        System.assertNotEquals(null, response);
        System.assertNotEquals(null, response.get('opportunity'));
        Pricebook2 pricebook = TestDataFactory.priceBook().build();
        insert pricebook;
        response = (Map<String, Object>) TestUtils.exec('ProductCnt', 'getInitData', inputJSON, true);
        System.assertNotEquals(null, response);
        System.assertNotEquals(null, response.get('opportunity'));
        Test.stopTest();
    }

    @IsTest
    static void getInitDataIsBlankOpportunityTest() {
        String opportunityId = null;
        Map<String, String > inputJSON =  new Map<String, String>{
            'opportunityId' => opportunityId
        };
        Test.startTest();
        try {
            TestUtils.exec('ProductCnt', 'getInitData', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    static void getProductListTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'pricebookId' => Test.getStandardPricebookId()
        };
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('ProductCnt', 'getProductList', inputJSON, true);
        Test.stopTest();
        System.assertNotEquals(null, response);
        System.assertEquals(3, response.size());
    }

    @IsTest
    static void getProductListIsBlankProductIdTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'pricebookId' => ''
        };
        Test.startTest();
        try {
            TestUtils.exec('ProductCnt', 'getProductList', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }    

    @IsTest
    static void productsAndFamiliesTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'productId' => productList.get(0).Id
        };
        System.assertEquals(2, productList.size());
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('ProductCnt', 'productsAndFamilies', inputJSON, true);
        Test.stopTest();
        System.assertNotEquals(null, response);
    }

    @IsTest
    static void productsAndFamiliesIsBlankProductIdTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'productId' => ''
        };
        Test.startTest();
        try {
            TestUtils.exec('ProductCnt', 'productsAndFamilies', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }    

    @IsTest
    static void listProductWithOptionsTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        System.assertEquals(2, productList.size());

        Test.startTest();
        List<ProductService.Product> response = (List<ProductService.Product>)TestUtils.exec('ProductCnt', 'listProductWithOptions',
            new Set<String>{productList.get(0).Id}, true);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(1, response.size());
        System.assertEquals(1, response.get(0).productOptions.size());
        List<String> listValues = response.get(0).productOptions.get(0).listValues;
        List<System.SelectOption> mapValues = response.get(0).productOptions.get(0).mapValues;
    }

    @IsTest
    static void listProductWithOptionsIsBlankProductIdsTest() {

        Test.startTest();
        try {
            TestUtils.exec('ProductCnt', 'listProductWithOptions',null, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();

    }

    @IsTest
    static void checkoutTest() {
        List<Product2> productList = [
            SELECT Id
            FROM Product2
            ORDER BY CreatedDate
        ];
        System.assertEquals(2, productList.size());

        Opportunity opportunity = [
            SELECT Id,Name
            FROM Opportunity
            LIMIT 1
        ];

        ProductService.Product product = new ProductService.Product();
        product.id = productList.get(0).Id;
        product.name = 'Test';
        product.description = 'Test';
        product.imageUrl = 'Test';
        product.family = 'Test';
        product.recordTypeName = 'Test';
        product.productOptions = new List<ProductService.ProductOption>();

        Test.startTest();
        Object response = TestUtils.exec('ProductCnt', 'checkout',
            new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'pricebookId' => Test.getStandardPricebookId(),
                'productList' => JSON.serialize(new List<ProductService.Product>{product})
            }, true);
        Test.stopTest();

        System.assertEquals(null, response);
    }

    @IsTest
    static void deserializeTest() {
        Test.startTest();
        StringUtils.tryToDeserialize('Key/Value',Map<String, String>.class);
        Test.stopTest();
    }
}