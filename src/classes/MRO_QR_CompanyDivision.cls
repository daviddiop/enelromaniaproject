/**
 * Created by goudiaby on 02/07/2019.
 */

public with sharing class MRO_QR_CompanyDivision {
    private static final CompanyDivisionSettings__c companyDivisionSettingsSettings = CompanyDivisionSettings__c.getOrgDefaults();

    public static MRO_QR_CompanyDivision getInstance() {
        return new MRO_QR_CompanyDivision();
    }

    public List<CompanyDivision__c> getAll(){
        final Integer currentQueryLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
        return [
            SELECT Id, Name, VATNumber__c
            FROM CompanyDivision__c
            LIMIT :currentQueryLimit
        ];
    }

    public Map<String, CompanyDivision__c> getAllActiveCompanyDivisions(){
        final Integer currentQueryLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
        Map<String, CompanyDivision__c> result = new Map<String, CompanyDivision__c>();
        List<CompanyDivision__c> companyDivisions = [
            SELECT Id, Name, VATNumber__c
            FROM CompanyDivision__c
            WHERE IsActive__c = true
            LIMIT :currentQueryLimit
        ];
        for (CompanyDivision__c cd : companyDivisions) {
            result.put(cd.VATNumber__c, cd);
        }
        return result;
    }

    public CompanyDivision__c getDefaultCompanyDivision(String distributorVATNumber) {
        Map<String, CompanyDivision__c> enabledDivisionsMap = this.getAllActiveCompanyDivisions();
        if (enabledDivisionsMap.isEmpty()) {
            return null;
        }
        if (enabledDivisionsMap.size() == 1) {
            return enabledDivisionsMap.values()[0];
        }
        List<DistributorDefaultDivision__mdt> defaultDivisions = [SELECT CompanyDivisionVATNumber__c
                                                                  FROM DistributorDefaultDivision__mdt
                                                                  WHERE DistributorVATNumber__c = :distributorVATNumber
                                                                  LIMIT 1];
        if (!defaultDivisions.isEmpty() && enabledDivisionsMap.containsKey(defaultDivisions[0].CompanyDivisionVATNumber__c)) {
            return enabledDivisionsMap.get(defaultDivisions[0].CompanyDivisionVATNumber__c);
        }
        return null;
    }

    public CompanyDivision__c getById(String idCompany){
        return [
            SELECT Id, Name, Code__c, VATNumber__c, Trader__c
            FROM CompanyDivision__c
            WHERE Id =:idCompany
            LIMIT 1
        ];
    }

    /**
     * @author Boubacar Sow
     * @date 11/06/2020 at 10:39
     * @description task: [ENLCRO-997] Removal of Enel Trade from Company Division Component
     * @return List<CompanyDivision__c>
     */
    public List<CompanyDivision__c> getAllActive() {
        final Integer currentQueryLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
        return [
            SELECT Id, Name, VATNumber__c
            FROM CompanyDivision__c
            WHERE IsActive__c = TRUE
            LIMIT :currentQueryLimit
        ];
    }
    public CompanyDivision__c getByName(String companyName){
        return [
                SELECT Id, Name, Code__c, VATNumber__c, Trader__c
                FROM CompanyDivision__c
                WHERE Name =:companyName
                LIMIT 1
        ];
    }

    public CompanyDivision__c getName(String companyId){

        List<CompanyDivision__c> companyDivisions = [SELECT Id, Name, Code__c, VATNumber__c, Trader__c
                                                        FROM CompanyDivision__c
                                                        WHERE Id =:companyId
                                                        LIMIT 1];

        if (!companyDivisions.isEmpty()) {
            return companyDivisions[0];
        }
        return null;

    }

    public CompanyDivision__c getCompanyDivisionByTrader(String traderId){

        List<CompanyDivision__c> companyDivisions = [SELECT Id, Name, Code__c, VATNumber__c, Trader__c
        FROM CompanyDivision__c
        WHERE IsActive__c = TRUE AND Trader__c !=:traderId
        LIMIT 1];

        if (!companyDivisions.isEmpty()) {
            return companyDivisions[0];
        }
        return null;

    }



}