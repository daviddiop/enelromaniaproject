global with sharing class MRO_WS_InsolvencyCaseCreationServices {
	private static final wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration PhaseManagerIntegration =
	(wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration');
	private static final MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
	private static final MRO_QR_Supply supplyQry = MRO_QR_Supply.getInstance();
	private static final MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
	private static final MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
	private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

	webservice static wrts_prcgvr.MRR_1_0.MultiResponse post(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest) {
		wrts_prcgvr.MRR_1_0.MultiResponse multiResponse = MRO_UTL_MRRMapper.initMultiResponse();
		String stackTrace;

		Savepoint sp = Database.setSavepoint();

		try {
			for (wrts_prcgvr.MRR_1_0.Request request : multiRequest.requests) {
				Map<String, Object> result = new Map<String, Object>();
				//Case creation process
				result = caseCreation(request);
				wrts_prcgvr.MRR_1_0.Response response = (wrts_prcgvr.MRR_1_0.Response) result.get('response');
				System.debug('response->>'+ response);

				if (result != null && response.code == 'OK' &&  result.get('dossierId') InstanceOf Id) {
					List<Case> listCasesByDossier;
					Dossier__c dossierCreated = dossierQuery.getById((String)result.get('dossierId'));
					listCasesByDossier = caseQuery.getCasesByDossierId(dossierCreated.Id);
					dossierSrv.checkAndApplyAutomaticTransitionToDossierAndCases(
							dossierCreated,
							listCasesByDossier,
							CONSTANTS.CONFIRM_TAG,
							false,
							false,
							false
					);
				}

				multiResponse.responses.add(response);
			}

		} catch (Exception e) {
			Database.rollback(sp);
			wrts_prcgvr.MRR_1_0.Response response = new wrts_prcgvr.MRR_1_0.Response();
			response.code = 'KO';
			response.header = multiRequest.requests[0].header;
			response.header.requestTimestamp = String.valueOf(System.now());
			response.description = e.getMessage();
			multiResponse.responses.add(response);
			stackTrace = e.getStackTraceString();
			system.debug('Error1: ' + e.getMessage() + e.getStackTraceString());
		}


		return multiResponse;
	}


	static Map<String, Object> caseCreation(wrts_prcgvr.MRR_1_0.Request request) {
		wrts_prcgvr.MRR_1_0.Response response = new wrts_prcgvr.MRR_1_0.Response();
		Map<String, Object> result = new Map<String, Object>();

		try{
			System.debug('Request->>>>' + request);
			response = MRO_UTL_MRRMapper.initResponse(request);
			Case caseToInsert, caseWObj = new Case();
			Account acc, insolvencyManager = new Account();

			Map<string, string> accountFieldsMap, insolvencyManagerFieldsMap = new Map<string, string>();

			/*
			 * Account
			 */
			List<wrts_prcgvr.MRR_1_0.WObject> accountWObjects =
					MRO_UTL_MRRMapper.selectWobjects('Case/Account', request);

			if (accountWObjects != null && !accountWObjects.isEmpty()) {
				accountFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(accountWObjects[0]);
				System.debug('accountFieldsMap' + accountFieldsMap);
				if (String.isBlank(accountFieldsMap.get('Key__c'))) throw new WrtsException('Key__c is missing');
			} else throw new WrtsException('Missing Account in MRR');

			//query Account by Key__c
			List<Account> accList = [SELECT Id, Key__c FROM Account
			WHERE Key__c =: accountFieldsMap.get('Key__c')];

			System.debug('accList' + accList);

			if(!accList.isEmpty()){
				acc = accList[0];
			} else {
				accList = [SELECT Id, Key__c FROM Account
				WHERE VATNumber__c =: accountFieldsMap.get('Key__c')];
				acc = accList[0];
			}

			/*
			 * Case
			 */
			List<wrts_prcgvr.MRR_1_0.WObject> caseWObjects =
					MRO_UTL_MRRMapper.selectWobjects('Case', request);

			System.debug('caseWObjects' + caseWObjects);

			if (caseWObjects != null && !caseWObjects.isEmpty()) {
				caseToInsert = (Case) MRO_UTL_MRRMapper.wobjectToSObject(caseWObjects[0]);
				System.debug('caseToInsert 1 ->>>>' + caseToInsert);
			} else throw new WrtsException('Case missing in MRR');

			/*
			 * InsolvencyManager
			 */
			if (caseToInsert.SubProcess__c != 'Insolvency closed') {
				List<wrts_prcgvr.MRR_1_0.WObject> insolvencyManagerWObjects =
						MRO_UTL_MRRMapper.selectWobjects('Case/InsolvencyManager__r', request);
				if (insolvencyManagerWObjects != null && !insolvencyManagerWObjects.isEmpty()) {
					insolvencyManagerFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(insolvencyManagerWObjects[0]);

					System.debug('insolvencyManagerFieldsMap' + insolvencyManagerFieldsMap);

					if (String.isBlank(insolvencyManagerFieldsMap.get('Key__c'))) throw new WrtsException(
							'Key__c on Insolvency Manager is missing');
					if (String.isBlank(insolvencyManagerFieldsMap.get('ONRCCode__c'))) throw new WrtsException(
							'ONRCCode__c on Insolvency Manager is missing');

					//query Insolvency Manager by Key__c and ONRCCode__c
					List<Account> insolvencyManagerList = [SELECT Id, Key__c, ONRCCode__c FROM Account
					WHERE Key__c =: insolvencyManagerFieldsMap.get('Key__c')
					AND ONRCCode__c =: insolvencyManagerFieldsMap.get('ONRCCode__c')];

					System.debug('insolvencyManagerList' + insolvencyManagerList);

					if (!insolvencyManagerList.isEmpty()) {
						insolvencyManager = insolvencyManagerList[0];
					} else throw new WrtsException(
									'Insolvency Manager is missing for this Key__c and ONRCCode__c  -> ' +
									insolvencyManagerFieldsMap.get('Key__c') + ' ' +
									insolvencyManagerFieldsMap.get('ONRCCode__c'));

				} else throw new WrtsException('Missing Insolvency Manager in MRR');
			}


			/*
			 * Case creation process
			 */
			Id dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
			Dossier__c dossier = dossierSrv.generateDossier(null, null, null, null, dossierRecordTypeId, 'Insolvency/Bankruptcy');
			dossier.Origin__c = 'Internal';
			dossier.Channel__c = 'Back Office';
			dossier.Status__c = 'New';
			dossier.Account__c = acc.Id;
			update dossier;

			Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('InsolvencyBankruptcy').getRecordTypeId();
			caseToInsert.RecordTypeId = caseRecordTypeId;
			caseToInsert.Origin = 'Internal';
			caseToInsert.Channel__c = 'Back Office Customer Care';
			caseToInsert.AccountId = acc.Id;
			caseToInsert.Dossier__c = dossier.Id;
			caseToInsert.EffectiveDate__c = System.today();
			//
			caseToInsert.InsolvencyManager__c = insolvencyManager.Id;

			insert caseToInsert;
			System.debug('caseToInsert 2 ->>>>' + caseToInsert);

			result.put('caseId', caseToInsert.Id);
			result.put('dossierId', dossier.Id);

			Case cs = [SELECT CaseNumber, format(CreatedDate), Status, Description FROM Case WHERE Id = :caseToInsert.Id];
			cs.Status = 'Procesat';
			cs.Description = 'Procesat cu success';
			Wrts_prcgvr.MRR_1_0.WObject caseMRR = MRO_UTL_MRRMapper.sObjectToWObject(cs, 'Case');

			response.objects.add(caseMRR);
			result.put('response', response);

		} catch (Exception e){
			response.Code = 'KO';
			String description = e.getMessage() + ' ' + e.getStackTraceString();
			response.header.requestTimestamp = String.valueOf(System.now());
			response.description = description;
			system.debug('Error: ' + description);
			result.put('response', response);
		}

		return result;
	}

}