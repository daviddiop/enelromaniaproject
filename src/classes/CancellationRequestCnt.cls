/**
 * Created by goudiaby on 25/08/2019.
 */
/**
 * Class for cancellation Component
 */
public with sharing class CancellationRequestCnt extends ApexServiceLibraryCnt {
    private static CancellationRequestService cancellationRequestSrv = CancellationRequestService.getInstance();
    private static CaseService caseSrv = CaseService.getInstance();
    private static DossierService dossierSrv = DossierService.getInstance();
    public static final String STATUS_CANCELED = 'Canceled';
    public static final String FIELDSET_API_NAME ='Cancellation';

    /**
     * @description Class to get fields for Fieldset Object's
     */
    public class getFieldset extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String objectName = params.get('objectName');
            response = LightningUtils.getFieldSet(objectName, 'Cancellation');
            return response;
        }
    }

    /**
   * @description Class to get Company Division Data from User id and all company Division
   */
    public with sharing class getCancellationReasons extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String objectName = params.get('objectName');
            String recordType = params.get('recordType');
            String originPhase = params.get('originPhase');
            List<ApexServiceLibraryCnt.Option> cancellationReasonsOptions = cancellationRequestSrv.getListCancellationReasons(objectName,recordType,originPhase);

            response.put('listCancellationReasons', cancellationReasonsOptions);
            return response;
        }
    }

    /**
    * @description Class to update Sobject
    */
    public with sharing class updateSobjectToCanceled extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String objectId = params.get('objectId');
            String objectName = params.get('objectName');
            String cancellationCause = params.get('cancellationReason');

            if (String.isBlank(objectId)) {
                throw new WrtsException(System.Label.Obj+ ' - ' + System.Label.MissingId);
            }

            switch on objectName{
                when 'Case'{
                    caseSrv.updateCaseStage(objectId,cancellationCause,CancellationRequestCnt.STATUS_CANCELED);
                }
                when 'Dossier'{
                    dossierSrv.updateDossierStage(objectId,CancellationRequestCnt.STATUS_CANCELED);
                }
            }

            return response;
        }
    }
}