/**
 * @author  Stefano Porcari
 * @since   Dec 24, 2019
 * @desc   Controller class for ValidatableDocument
 * 
 */
public with sharing class MRO_LC_ValidatableDocumentsCnt extends ApexServiceLibraryCnt {
   private static MRO_SRV_FileMetadata fileMetadataSrv = MRO_SRV_FileMetadata.getInstance();
   private static MRO_SRV_DocumentType documentTypeSrv = MRO_SRV_DocumentType.getInstance();
   private static MRO_SRV_Activity activitySrv = MRO_SRV_Activity.getInstance();
   private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
   private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
   private static MRO_SRV_ValidatableDocument validatableDocumentSrv = MRO_SRV_ValidatableDocument.getInstance();
   private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    public class getFileMetadataAndValidatableDocuments extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String filemetadataId = params.get('filemetadataId');
            if (String.isBlank(filemetadataId)) {
                throw new WrtsException(System.Label.FileMetadata + ' - ' + System.Label.Required);
            }

            MRO_SRV_FileMetadata.FileMetadataDTO fileMetadataDTO = fileMetadataSrv.getFileMetadataById(filemetadataId);
            Id parentRecordId;
            if (fileMetadataDTO.caseId != null) {
                parentRecordId = fileMetadataDTO.caseId;
            }
            else if (fileMetadataDTO.dossierId != null) {
                parentRecordId = fileMetadataDTO.dossierId;
            }
            else {
                throw new WrtsException(System.Label.DossierRequiredSelectedFileMetadata);
            }

            wrts_prcgvr__Activity__c fileCheckActivity = MRO_QR_Activity.getInstance().getByFileMetadataAndType(filemetadataId, constantsSrv.ACTIVITY_FILE_CHECK_TYPE);
            response.put('fileCheckActivity', fileCheckActivity);

            List<MRO_SRV_DocumentType.DocumentTypeDTO> documentTypeDTOS = documentTypeSrv.retrieveDocumentTypesFromParentRecord(parentRecordId);
            response.put('filemetadata', fileMetadataDTO);
            response.put('documentTypes', documentTypeDTOS);

            return response;
        }
    }

    public class validateDocument extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id fileMetadataId = params.get('fileMetadataId');
            Boolean isConfirmed = Boolean.valueOf(params.get('confirmed'));
            ValidatableDocument__c validatableDocument = (ValidatableDocument__c) JSON.deserialize(params.get('validatableDocument'), ValidatableDocument__c.class);
            List<MRO_SRV_DocumentItem.DocumentItemDTO> documentItemsDTO = new List<MRO_SRV_DocumentItem.DocumentItemDTO>();
            if (!String.isBlank(params.get('documentItemsDTO'))) {
                documentItemsDTO = (List<MRO_SRV_DocumentItem.DocumentItemDTO>) JSON.deserialize(params.get('documentItemsDTO'), List<MRO_SRV_DocumentItem.DocumentItemDTO>.class);
            }

            List<MRO_UTL_DocumentValidationChecks.Result> validationResults = validatableDocumentSrv.validateDocument(validatableDocument, fileMetadataId, documentItemsDTO, isConfirmed);
            response.put('result', 'OK');
            String message = '';
            Boolean hasError = false;
            for (MRO_UTL_DocumentValidationChecks.Result res : validationResults) {
                if (res.code == MRO_UTL_DocumentValidationChecks.ResultCode.ERROR) {
                    hasError = true;
                    response.put('result', 'ERROR');
                    if (!String.isBlank(message)) {
                        message += '<br/>';
                    }
                    message += res.message;
                }
                else if (res.code == MRO_UTL_DocumentValidationChecks.ResultCode.WARNING && !hasError) {
                    response.put('result', 'WARNING');
                    if (!String.isBlank(message)) {
                        message += '<br/>';
                    }
                    message += res.message;
                }
            }
            response.put('message', message);

            return response;
        }
    }

    public class doneValidatableDocuments extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String filemetadataId = params.get('filemetadataId');
            if (String.isBlank(filemetadataId)) {
                throw new WrtsException(System.Label.FileMetadata + ' - ' + System.Label.Required);
            }

            MRO_SRV_FileMetadata.FileMetadataDTO fileMetadataDTO = fileMetadataSrv.getFileMetadataById(filemetadataId);
            Id parentRecordId;
            if (fileMetadataDTO.caseId != null) {
                parentRecordId = fileMetadataDTO.caseId;
            }
            else if (fileMetadataDTO.dossierId != null) {
                parentRecordId = fileMetadataDTO.dossierId;
            }
            else {
                throw new WrtsException(System.Label.DossierRequiredSelectedFileMetadata);
            }
            String sObjectType = parentRecordId.getSobjectType().getDescribe().getName();
            Boolean inEnelArea = false;
            Dossier__c dossier = null;
            Case caseRecord;
            switch on sObjectType {
                when 'Case' {
                    caseRecord = MRO_QR_Case.getInstance().listByIdsForActivities(new Set<Id>{parentRecordId})[0];
                }
                when 'Dossier__c' {
                    dossier = MRO_QR_Dossier.getInstance().getByIdsForActivities(new Set<Id>{parentRecordId}).get(parentRecordId);
                    System.debug('MRO_LC_ValidatableDocumentsCnt.doneValidatableDocuments.perform' + dossier);
                    if (!dossier.Cases__r.isEmpty()) {
                        caseRecord = dossier.Cases__r[0];
                    }
                }
                when else {
                    throw new WrtsException('Unsupported parent object for a Validatable Document');
                }
            }
            if (caseRecord != null) {
                inEnelArea = caseRecord.IsDisCoENEL__c
                            || (caseRecord.Distributor__r != null && caseRecord.Distributor__r.IsDisCoENEL__c)
                            || (caseRecord.Supply__r != null && (caseRecord.Supply__r.InENELArea__c
                                                                || (caseRecord.Supply__r.ServicePoint__r.Distributor__r != null && caseRecord.Supply__r.ServicePoint__r.Distributor__r.IsDisCoENEL__c)));
            }

            if (inEnelArea) {
                ValidatableDocument__c toBeArchivedDocument = MRO_QR_ValidatableDocument.getInstance().getDMSArchivableDocumentByFileMetadataId(filemetadataId);
                if (toBeArchivedDocument != null) {
                    wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse archivingResponse = executeArchiveDMS(toBeArchivedDocument.Id);
                    System.debug('archivingResponse: ' + archivingResponse);
                    if (archivingResponse.success != true) {
                        throw new WrtsException(archivingResponse.message);
                    }
                    response.put('awaitingArchiving', true);
                    return response;
                }
                else {
                    response.put('awaitingArchiving', false);
                }
            }

            activitySrv.closeFileCheckActivity(filemetadataId);
            //Id parentRecordId = fileMetadataDTO.caseId != null ? fileMetadataDTO.caseId : fileMetadataDTO.dossierId;
            String phaseTransitionType = constantsSrv.PHASE_AUTOMATIC_TRANSITION;
            String phaseTransitionTag = constantsSrv.DOCUMENT_VALIDATION_TAG;
            MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
            if (parentRecordId == fileMetadataDTO.dossierId) {
                String tokenId = dossier.ExternalAccessIdentifier__c;
                List<wrts_prcgvr__Activity__c> closedValidationActivities;
                if (String.isNotBlank(tokenId)) {
                    closedValidationActivities = activitySrv.closeCompleteValidationActivities(parentRecordId, tokenId);
                } else {
                    closedValidationActivities = activitySrv.closeCompleteValidationActivities(parentRecordId);
                }
                Boolean syncCases = false;
                for (wrts_prcgvr__Activity__c activity : closedValidationActivities) {
                    if (!activity.DoNotSyncCases__c) {
                        syncCases = true;
                        break;
                    }
                }
                List<Case> cases = syncCases ? MRO_QR_Case.getInstance().getCasesByDossierId(dossier.Id): null;
                MRO_SRV_Dossier.getInstance().checkAndApplyAutomaticTransitionToDossierAndCases(dossier, cases, phaseTransitionTag, false, true, false);
            } else {
                List<wrts_prcgvr__Activity__c> closedValidationActivities = activitySrv.closeCompleteValidationActivities(parentRecordId);
                //Case caseRecord = MRO_QR_Case.getInstance().getById(parentRecordId);
                transitionsUtl.checkAndApplyAutomaticTransitionWithTag(caseRecord, phaseTransitionTag, true, false);
            }
            return response;
        }
    }

    private static wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse executeArchiveDMS(Id validatableDocumentId) {
        try {
            String className = 'MRO_SRV_DmsCallOut';
            String flowName = 'dmsUpload';
            String endpointName = 'dms';

            Map<String, Object> argsMap = new Map<String, Object>();
            argsMap.put('method', className);
            argsMap.put('sender', new ValidatableDocument__c(Id = validatableDocumentId));

            Map<String, String> parameters = new Map<String, String>();
            parameters.put('requestType', flowName);
            parameters.put('timeout', '4000');
            parameters.put('endPoint', endpointName);

            argsMap.put('parameters', parameters);

            MRO_SRV_SendMRRcallout instance = new MRO_SRV_SendMRRcallout();
            return (wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse) instance.execute((Object)argsMap);
        } catch (Exception ex) {
            //response.put('error', true);
            //response.put('errorMsg', ex.getMessage());
            //response.put('errorTrace', ex.getStackTraceString());
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            throw ex;
        }
    }
}