global with sharing virtual class ApexServiceLibraryCnt {

//    private static final Logger log = Logger.getInstance(ApiController.class);

    @AuraEnabled
    global static ApexServiceLibraryCnt.Response call(final String className, final String methodName, final String jsonInput) {
        final AuraCallable callableInstance = getClassInstance(className, methodName);
        if (callableInstance == null) {
            return new ApexServiceLibraryCnt.Response('No class found for method ' + methodName);
        }
        return callableInstance.call(jsonInput);
    }

    @AuraEnabled(cacheable=true)
    global static ApexServiceLibraryCnt.Response cacheableCall(final String className, final String methodName, final Map<String,String> input) {
        System.debug('input = ' + input);
        return call(className, methodName, JSON.serialize(input));
    }

    @AuraEnabled(cacheable=false)
    global static ApexServiceLibraryCnt.Response notCacheableCall(final String className, final String methodName, final Map<String,String> input) {
        System.debug('input = ' + input);
        return call(className, methodName, JSON.serialize(input));
    }

    private static AuraCallable getClassInstance(final String className, final String methodName) {
        try {
            System.debug(LoggingLevel.INFO, className + '.' + methodName);
            final String innerClassName = className + '.' + methodName;
            return (AuraCallable) Type.forName(innerClassName).newInstance();
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, 'getClassByMethodName');
            System.debug(LoggingLevel.ERROR, e.getMessage());
            System.debug(LoggingLevel.ERROR, e.getStackTraceString());
//            log.error('getClassByMethodName', e.getMessage(), e.getStackTraceString());
        } finally {
//            log.save();
        }
        return null;
    }

    public static Map<String, String> asMap(String jsonInput) {
        if (jsonInput == 'null' || String.isBlank(jsonInput)) {
            return new Map<String, String>();
        }
        return (Map<String, String>)JSON.deserialize(jsonInput, Map<String, String>.class);
    }

    global with sharing class Response {
        @AuraEnabled
        public String error {get; set;}
        @AuraEnabled
        public String errorStackTrace {get; set;}
        @AuraEnabled
        public Object data {get; set;}
        @AuraEnabled
        public boolean getIsError() {
            return String.isNotEmpty(error);
        }
        global Response() {}
        global Response(String error) {
            this.error = error;
        }
    }

    public with sharing class Option {
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String value {get; set;}

        public Option(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}