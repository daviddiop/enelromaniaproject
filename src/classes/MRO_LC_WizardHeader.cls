/**
 * Created by goudiaby on 23/12/2019.
 */

public with sharing class MRO_LC_WizardHeader extends ApexServiceLibraryCnt {
    private static MRO_QR_Opportunity oppQuery = MRO_QR_Opportunity.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static CompanyDivisionQueries compQuery = CompanyDivisionQueries.getInstance();

    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String opportunityId = params.get('opportunityId');
            String companyDivisionId = params.get('companyDivisionId');
            String dossierId = params.get('dossierId');
            MRO_LC_WizardHeader.ResponseData res = new MRO_LC_WizardHeader.ResponseData();

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                Account acc = accountQuery.findAccount(accountId);
                res.account = acc;
                res.accountId = accountId;
                res.originFromInteraction = false;
                // Handling of CompanyDivision data
                if (String.isNotBlank(companyDivisionId)) {
                    CompanyDivision__c companyDivision = compQuery.getById(companyDivisionId);
                    res.companyDivisionName = companyDivision.Name;
                    res.companyDivisionId = companyDivision.Id;
                }
                // Handling of Dossier data
                Dossier__c dos;
                if (String.isNotBlank(dossierId)) {
                    dos = dossierQuery.getById(dossierId);
                    res.dossier = dos;
                    res.companyDivisionName = dos.CompanyDivision__r.Name;
                    res.companyDivisionId = dos.CompanyDivision__c;
                    // BG-[20200107] [ENLCRO-325] Channel and Origin Selection - Implementation
                    res.originSelected = dos.Origin__c;
                    res.channelSelected = dos.Channel__c;
                    if (dos.CustomerInteraction__c != null && dos.CustomerInteraction__r.Interaction__r != null) {
                        //res.originSelected = dos.CustomerInteraction__r.Interaction__r.Origin__c;
                        //res.channelSelected = dos.CustomerInteraction__r.Interaction__r.Channel__c;
                        res.originFromInteraction = true;
                    }

                }
                // Handling of Opportunity data
                Opportunity opp;
                if (String.isNotBlank(opportunityId)) {
                    opp = oppQuery.getOpportunityById(opportunityId);
                    res.opportunityId = opp.Id;
                    res.opportunity = opp;
                    res.companyDivisionName = opp.CompanyDivision__r.Name;
                    res.companyDivisionId = opp.CompanyDivision__c;
                    // BG-[20200107] [ENLCRO-325] Channel and Origin Selection - Implementation
                    res.originSelected = opp.Origin__c;
                    res.channelSelected = opp.Channel__c;
                    if (opp.CustomerInteraction__c != null && opp.CustomerInteraction__r.Interaction__r != null) {
                        //res.originSelected = opp.CustomerInteraction__r.Interaction__r.Origin__c;
                        //res.channelSelected = opp.CustomerInteraction__r.Interaction__r.Channel__c;
                        res.originFromInteraction = true;
                    }
                }

                res.accountPersonRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId();
                res.accountPersonProspectRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId();
                res.accountBusinessRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId();
                res.accountBusinessProspectRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId();
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            response.put('response', res);
            return response;
        }
    }

    public class ResponseData {
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String companyDivisionId { get; set; }
        @AuraEnabled
        public String companyDivisionName { get; set; }
        @AuraEnabled
        public String accountPersonRT { get; set; }
        @AuraEnabled
        public String accountPersonProspectRT { get; set; }
        @AuraEnabled
        public String accountBusinessRT { get; set; }
        @AuraEnabled
        public String accountBusinessProspectRT { get; set; }
        @AuraEnabled
        public Account account { get; set; }
        @AuraEnabled
        public Opportunity opportunity { get; set; }
        @AuraEnabled
        public Dossier__c dossier { get; set; }
        // BG-[20200107] [ENLCRO-325] Channel and Origin Selection - Implementation
        @AuraEnabled
        public String channelSelected { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public Boolean originFromInteraction { get; set; }
        @AuraEnabled
        public Map<String, List<ApexServiceLibraryCnt.Option>> mapOriginChannelOptions { get; set; }

        public ResponseData() {
        }
    }
}