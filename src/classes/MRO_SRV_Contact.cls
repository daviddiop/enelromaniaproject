public with sharing class MRO_SRV_Contact {

    private static final MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();

    public static MRO_SRV_Contact getInstance() {
        return (MRO_SRV_Contact)ServiceLocator.getInstance(MRO_SRV_Contact.class);
    }

    public Contact create(Individual individualObj, String accountId) {
        Contact contact =  new Contact(

            FirstName = individualObj.FirstName,
            LastName = individualObj.LastName,
            IndividualId = individualObj.Id,
            AccountId = accountId,
                //FF Customer Creation - Pack1/2 - Interface Check
            Gender__c = individualObj.Gender__c,
            BirthCity__c = individualObj.BirthCity__c,
            BirthDate = individualObj.BirthDate,
            ForeignCitizen__c = individualObj.ForeignCitizen__c,
            IDDocumentNr__c 		= individualObj.IDDocumentNr__c,
            IDDocEmissionDate__c  	= individualObj.IDDocEmissionDate__c,
            IDDocValidityDate__c  	= individualObj.IDDocValidityDate__c,
            IDDocumentType__c     	= individualObj.IDDocumentType__c,
            IDDocumentSeries__c   	= individualObj.IDDocumentSeries__c,
            IDDocIssuer__c         	= individualObj.IDDocIssuer__c
                //FF Customer Creation - Pack1/2 - Interface Check

        );
        if (individualObj.TechnicalDetails__c != null) {
            Map<String, String> mapTechnicalDetails = new Map<String, String>();
            mapTechnicalDetails = (Map<String, String>) JSON.deserialize(individualObj.TechnicalDetails__c, Map<String, String>.class);
            contact.ContactChannel__c = mapTechnicalDetails.get('ContactChannel');
            contact.MobilePhone = mapTechnicalDetails.get('Mobile');
            contact.Phone = mapTechnicalDetails.get('Phone');
            contact.Email = mapTechnicalDetails.get('Email');
        }
        return contact;
    }

    public Contact insertForIndividual(Individual individual, String accountId) {
        //System.debug('FF SRV individual.TechnicalDetails__c : ' + individual.TechnicalDetails__c);
        //System.debug('FF1 SRV individual : ' + individual);
        List<Contact> contacts = contactQuery.listContactByIndividualId(individual.Id, true);
        Contact newContact = new Contact();

        if(!contacts.isEmpty()){

            newContact.FirstName = contacts[0].FirstName;
            newContact.LastName = contacts[0].LastName;
            newContact.NationalIdentityNumber__c = contacts[0].NationalIdentityNumber__c;
            newContact.IndividualId = individual.Id;
            newContact.AccountId = accountId;
            newContact.ForeignCitizen__c = contacts[0].ForeignCitizen__c;
            newContact.Gender__c = contacts[0].Gender__c;
            newContact.BirthCity__c = contacts[0].BirthCity__c;
            newContact.BirthDate = contacts[0].BirthDate;
            newContact.IDDocEmissionDate__c = contacts[0].IDDocEmissionDate__c;
            newContact.IDDocValidityDate__c = contacts[0].IDDocValidityDate__c;
            newContact.IDDocumentNr__c = contacts[0].IDDocumentNr__c;
            newContact.IDDocumentType__c = contacts[0].IDDocumentType__c;
            newContact.IDDocumentSeries__c = contacts[0].IDDocumentSeries__c;
            newContact.IDDocIssuer__c =  contacts[0].IDDocIssuer__c;
            newContact.ContactChannel__c = contacts[0].ContactChannel__c;
            newContact.MobilePhone = contacts[0].MobilePhone;
            newContact.Phone = contacts[0].Phone;
            newContact.Email= contacts[0].Email;

        }else{

            Map<String, String> mapTechnicalDetails = individual.TechnicalDetails__c != null ? (Map<String,String>) JSON.deserialize(individual.TechnicalDetails__c, Map<String,String>.class) : null;
            newContact.FirstName = individual.FirstName;
            newContact.LastName = individual.LastName;
            newContact.NationalIdentityNumber__c = individual.NationalIdentityNumber__c;
            newContact.IndividualId = individual.Id;
            newContact.AccountId = accountId;
            newContact.ForeignCitizen__c = individual.ForeignCitizen__c;
            newContact.Gender__c = individual.Gender__c;
            newContact.BirthCity__c = individual.BirthCity__c;
            newContact.BirthDate = individual.BirthDate;
            newContact.IDDocEmissionDate__c = individual.IDDocEmissionDate__c;
            newContact.IDDocValidityDate__c = individual.IDDocValidityDate__c;
            newContact.IDDocumentNr__c = individual.IDDocumentNr__c;
            newContact.IDDocumentType__c = individual.IDDocumentType__c;
            newContact.IDDocumentSeries__c = individual.IDDocumentSeries__c;
            newContact.IDDocIssuer__c =  individual.IDDocIssuer__c;
            if(mapTechnicalDetails != null){
                newContact.ContactChannel__c = mapTechnicalDetails.get('ContactChannel');
                newContact.MobilePhone = mapTechnicalDetails.get('Mobile');
                newContact.Phone = mapTechnicalDetails.get('Phone');
                newContact.Email= mapTechnicalDetails.get('Email');
            }
        }


        databaseSrv.insertSObject(newContact);
        return newContact;
    }
    public List<Contact> searchContact(Interaction__c contactSearch) {
        List<QueryBuilder.Condition> contactConditionList = new List<QueryBuilder.Condition>();
        if (String.isNotBlank(contactSearch.InterlocutorEmail__c)) {
            contactConditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.Email, contactSearch.InterlocutorEmail__c));
        }
        if (String.isNotBlank(contactSearch.InterlocutorPhone__c)) {
            contactConditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.Phone, contactSearch.InterlocutorPhone__c));
        }
            List<QueryBuilder.Condition> conditionList = new List<QueryBuilder.Condition>();
            if (String.isNotBlank(contactSearch.InterlocutorFirstName__c)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.FirstName, contactSearch.InterlocutorFirstName__c));
            }
            if (String.isNotBlank(contactSearch.InterlocutorLastName__c)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.LastName, contactSearch.InterlocutorLastName__c));
            }
            if (String.isNotBlank(contactSearch.InterlocutorNationalIdentityNumber__c)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.NationalIdentityNumber__c, contactSearch.InterlocutorNationalIdentityNumber__c));
            }

            QueryBuilder contactQueryBuilder = QueryBuilder.getInstance()
                    .setObject(Contact.sObjectType)
                    .setFields(new List<SObjectField>{
                            Contact.Id,Contact.Name,Contact.FirstName,Contact.LastName,Contact.NationalIdentityNumber__c,
                            Contact.AccountId,Contact.Email,Contact.Phone
                    })
                    .setCondition(conditionList, QueryBuilder.AND_OPERATOR);

            return contactQueryBuilder.execute();
    }
}