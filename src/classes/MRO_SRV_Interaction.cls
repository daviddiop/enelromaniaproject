/**
 * Created by napoli on 18/11/2019.
 */

public with sharing class MRO_SRV_Interaction {

    private static final MRO_QR_Interaction interactionQuery = MRO_QR_Interaction.getInstance();
//FF Customer Creation - Pack1/2 - Interface Check
    private static final MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();

    private static final MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    //FF Customer Creation - Pack1/2 - Interface Check

    public static MRO_SRV_Interaction getInstance() {
        return (MRO_SRV_Interaction)ServiceLocator.getInstance(MRO_SRV_Interaction.class);
    }

    public List<ApexServiceLibraryCnt.Option> listOptions(SObjectField sObjField) {
        List<ApexServiceLibraryCnt.Option> optionList = new List<ApexServiceLibraryCnt.Option>();
        List<PicklistEntry> picklistEntryList = sObjField.getDescribe().getPicklistValues();
        for (PicklistEntry entry : picklistEntryList) {
            if (!entry.isActive()) {
                continue;
            }
            optionList.add(new ApexServiceLibraryCnt.Option(entry.getLabel(), entry.getValue()));
        }
        return optionList;
    }

    /**
    * @author BG
    * @description Method to update interaction Object particularly Interlocutor field with the new interlocutor
    *
    * @param interactionId Id of interaction to update
    * @param individualId Id of a new individual
    */
    public void updateInteraction(String interactionId, String individualId) {
        //Update individual fields record if Customer exists
        Interaction__c interactionToUpdate = interactionQuery.findInteractionById(interactionId);
        if (interactionToUpdate != null) {
            interactionToUpdate.Interlocutor__c = individualId;
            interactionToUpdate.InterlocutorFirstName__c = null;
            interactionToUpdate.InterlocutorLastName__c = null;
            interactionToUpdate.InterlocutorNationalIdentityNumber__c = null;
            interactionToUpdate.InterlocutorEmail__c = null;
            interactionToUpdate.InterlocutorPhone__c = null;
            updateInteraction(interactionToUpdate);
        }
    }

    public void updateInteraction(Interaction__c interactionToUpdate) {
        databaseSrv.updateSObject(interactionToUpdate);
    }

    public InteractionDTO removeInterlocutor(String interactionId) {
        Interaction__c interactionToUpdate = interactionQuery.findInteractionById(interactionId);
        if (interactionToUpdate != null) {
            List<CustomerInteraction__c> customerInteractions = customerInteractionQuery.listCustomerInteraction(interactionId);
            if (customerInteractions.isEmpty()) {
                if (String.isNotBlank(interactionToUpdate.Interlocutor__c)) {
                    interactionToUpdate.Interlocutor__c = null;
                    interactionToUpdate.InterlocutorFirstName__c = null;
                    interactionToUpdate.InterlocutorLastName__c = null;
                    interactionToUpdate.InterlocutorNationalIdentityNumber__c = null;
                    interactionToUpdate.InterlocutorPhone__c = null;
                    interactionToUpdate.InterlocutorEmail__c = null;
                } else {
                    interactionToUpdate.InterlocutorFirstName__c = null;
                    interactionToUpdate.InterlocutorLastName__c = null;
                    interactionToUpdate.InterlocutorNationalIdentityNumber__c = null;
                    interactionToUpdate.InterlocutorPhone__c = null;
                    interactionToUpdate.InterlocutorEmail__c = null;
                    interactionToUpdate.Institution__c = null;
                }
                databaseSrv.updateSObject(interactionToUpdate);
            } else {
                throw new WrtsException(Label.InterlocutorRemoveDenied);
            }
        }
        return findById(interactionId);
    }

    public InteractionDTO findById(String interactionId) {
        Interaction__c interaction = interactionQuery.findInteractionById(interactionId);
        if (interaction == null) {
            return null;
        }
        InteractionDTO interactionDto = mapToDto(interaction);
        if (interaction.Interlocutor__r != null) {
            interactionDto.interlocutor = MRO_SRV_Individual.mapToDto(interaction.Interlocutor__r);
        }
        interactionDto.isSaveUnIdentifiedInterlocutorAllowed = SettingProvider.isSaveUnIdentifiedInterlocutorAllowed();
        return interactionDto;
    }

    public static InteractionDTO mapToDto(Interaction__c interactionObj) {
        InteractionDTO interaction = new InteractionDTO();
        interaction.id = interactionObj.Id;
        interaction.name = interactionObj.Name;
        interaction.interlocutorType = interactionObj.InterlocutorType__c;
        interaction.interlocutorId = interactionObj.Interlocutor__c;
        interaction.institutionId = interactionObj.Institution__c;
        interaction.status = interactionObj.Status__c;
        interaction.comments = interactionObj.Comments__c;
        interaction.createdDate = interactionObj.CreatedDate;
        interaction.createdDateFormatted = interactionObj.CreatedDate.format('MM/dd/yyyy HH:mm');
        interaction.channel = interactionObj.Channel__c;
        // BG [20200110] - [ENLCRO-325] Channel and Origin Selection - Implementation
        interaction.origin = interactionObj.Origin__c;
        if (interactionObj.CustomerInteractions__r.size() > 0) {
            CustomerInteraction__c customerInteraction = interactionObj.CustomerInteractions__r.get(0);
            interaction.customerId = customerInteraction.Customer__c;
            for (CustomerInteraction__c thisCustomerInteraction : interactionObj.CustomerInteractions__r) {
                interaction.customerInteractionIds.add(thisCustomerInteraction.Id);
            }
        }
        interaction.interlocutorFirstname = interactionObj.InterlocutorFirstName__c;
        interaction.interlocutorLastname = interactionObj.InterlocutorLastName__c;
        interaction.interlocutorNationalId = interactionObj.InterlocutorNationalIdentityNumber__c;
        interaction.interlocutorEmail = interactionObj.InterlocutorEmail__c;
        interaction.interlocutorPhone = interactionObj.InterlocutorPhone__c;
        interaction.institutionName = interactionObj.Institution__r.Name;

        return interaction;
    }

    public static Interaction__c mapFromDto(InteractionDTO interaction) {
        Interaction__c interactionObj = new Interaction__c();
        interactionObj.Id = interaction.id;
        interactionObj.Interlocutor__c = interaction.interlocutorId;
        interactionObj.Status__c = interaction.status;
        interactionObj.Comments__c = interaction.comments;
        interactionObj.Channel__c = interaction.channel;
        interactionObj.Origin__c = interaction.origin;
        return interactionObj;
    }

    public InteractionDTO insertInteraction() {
        Interaction__c interactionObj = new Interaction__c();
        databaseSrv.insertSObject(interactionObj);
        return findById(interactionObj.Id);
    }

    public InteractionDTO upsertInteraction(InteractionDTO interaction) {
        Interaction__c interactionForUpdate = mapFromDto(interaction);
        databaseSrv.upsertSObject(interactionForUpdate);
        return findById(interactionForUpdate.id);
    }

    public void updateIndividualInfo(String interactionId, MRO_SRV_Individual.Interlocutor interlocutorDto) {
        databaseSrv.updateSObject(new Interaction__c(
                Id = interactionId,
                InterlocutorFirstName__c = interlocutorDto.firstName,
                InterlocutorLastName__c = interlocutorDto.lastName,
                InterlocutorNationalIdentityNumber__c = interlocutorDto.nationalId,
                InterlocutorEmail__c = interlocutorDto.email,
                InterlocutorPhone__c = interlocutorDto.phone
        ));
    }

    public Boolean isInteractionClosed(String interactionId) {
        Interaction__c interaction = interactionQuery.findInteractionById(interactionId);
        return interaction.Status__c == 'Closed' || interaction.Status__c == 'Canceled';
    }

    public with sharing class InteractionDTO {
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String interlocutorId {get; set;}
        @AuraEnabled
        public String customerId {get; set;}
        @AuraEnabled
        public String status {get; set;}
        @AuraEnabled
        public String comments {get; set;}
        @AuraEnabled
        public DateTime createdDate {get; set;}
        @AuraEnabled
        public String createdDateFormatted {get; set;}
        @AuraEnabled
        public String channel {get; set;}
        // BG [20200110] - [ENLCRO-325] Channel and Origin Selection - Implementation
        @AuraEnabled
        public String origin {get; set;}
        @AuraEnabled
        public MRO_SRV_Individual.Interlocutor interlocutor {get; set;}

        @AuraEnabled
        public Boolean isSaveUnIdentifiedInterlocutorAllowed {get; set;}

        @AuraEnabled
        public String interlocutorType {get; set;}
        @AuraEnabled
        public String institutionId {get; set;}
        @AuraEnabled
        public String institutionName {get; set;}
        @AuraEnabled
        public String interlocutorFirstname {get; set;}
        @AuraEnabled
        public String interlocutorLastname {get; set;}
        @AuraEnabled
        public String interlocutorNationalId {get; set;}
        @AuraEnabled
        public String interlocutorPhone {get; set;}
        @AuraEnabled
        public String interlocutorEmail {get; set;}
        @AuraEnabled
        public Set<String> customerInteractionIds {get; set;}

        @AuraEnabled
        public String getInterlocutorFullname() {
            if (this.interlocutorFirstname == null) {
                this.interlocutorFirstname = '';
            }
            if (this.interlocutorLastname == null) {
                this.interlocutorLastname = '';
            }
            return this.interlocutorFirstname + ' ' + this.interlocutorLastname;
        }

        @AuraEnabled
        public Boolean getIsClosed() {
            return status == 'Closed' || status == 'Canceled';
        }
        public InteractionDTO() {
            this.customerInteractionIds = new Set<String>();
        }

// TODO: ST not used
//        public InteractionDTO(String id, String interlocutorId, String customerId, String status, String comments,
//                        DateTime createdDate, String channel) {
//            this.id = id;
//            this.interlocutorId = interlocutorId;
//            this.customerId = customerId;
//            this.status = status;
//            this.comments = comments;
//            this.createdDate = createdDate;
//            this.channel = channel;
//            this.customerInteractionIds = new Set<String>();
//        }
    }
}