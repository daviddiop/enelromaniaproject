/**
 * Created by Boubacar Sow on 16/10/2020.
 */

public with sharing class MRO_LC_ConsumptionPlaces extends  ApexServiceLibraryCnt {

    public with sharing class listConsumptionPlaces extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String username = params.get('username');
            System.debug('### username '+username);

            try {
                if (String.isBlank(username)) {
                    throw new WrtsException(System.Label.username+' '+System.Label.IsMissing);
                }
                List<ConsumptionPlaces> consumptionPlaces = MRO_SRV_MyEnelAccountSituationCallout.getInstance().getConsumptionPlacesList(username);
                if (!consumptionPlaces.isEmpty()) {
                    response.put('consumptionPlaces', consumptionPlaces);
                    response.put('error', false);
                    return response;
                }
                consumptionPlaces = new List<ConsumptionPlaces>();
                ConsumptionPlaces.add(new ConsumptionPlaces(
                    'clientCodeclientCodeclientCode',
                    'paymentCodepaymentCodepaymentCode',
                    'enelTelenelTelenelTel',
                    'addressaddressaddressaddressaddressaddressaddressaddressaddressaddressaddressaddress'
                ));
                if (consumptionPlaces.isEmpty()) {
                    response.put('consumptionPlaces', null);
                }
                response.put('consumptionPlaces', consumptionPlaces);
                response.put('error', false);
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                if (e instanceof NullPointerException) {
                    response.put('errorMsg', System.Label.ServiceIsDown);
                }
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }


    public class ConsumptionPlaces {
        @AuraEnabled
        public String clientCode { get; set; }
        @AuraEnabled
        public String paymentCode { get; set; }
        @AuraEnabled
        public String enelTel { get; set; }
        @AuraEnabled
        public String address { get; set; }

        public ConsumptionPlaces(String clientCode, String paymentCode, String enelTel, String address) {
            this.clientCode = clientCode;
            this.paymentCode = paymentCode;
            this.enelTel = enelTel;
            this.address = address;
        }

        public ConsumptionPlaces(Map<String, String> stringMap) {
            this.clientCode = stringMap.get('clientCode');
            this.paymentCode = stringMap.get('paymentCode');
            this.enelTel = stringMap.get('enelTel');
            this.address = stringMap.get('address');
        }

    }

}