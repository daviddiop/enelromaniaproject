/**
 * Created by tommasobolis on 29/06/2020.
 */

public with sharing class MRO_LC_RelatedListCnt {

    public class ColumnWrapper {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public String fieldName {get;set;}
        @AuraEnabled
        public String queryField {get;set;}
        @AuraEnabled
        public String type {get;set;}
        @AuraEnabled
        public Boolean sortable {get;set;}
        /* @AuraEnabled
         public String sortDirection {get;set;}
         @AuraEnabled
         public String showSortRow {get;set;}*/
    }

    public class RecordWrapper{
        @AuraEnabled
        public List<Map<String, Object>> values { get; set; }
        @AuraEnabled
        public Id id {get;set;}

        public RecordWrapper(Id id, List<Map<String, Object>> values){
            this.id = id;
            this.values = values;
        }
    }

    public class RelatedListWrapper {
        @AuraEnabled
        public List<RecordWrapper> records {get;set;}
        @AuraEnabled
        public List<ColumnWrapper> columns {get;set;}


        public RelatedListWrapper(){
            this.records = new List<RecordWrapper>();
            this.columns = new List<ColumnWrapper>();
        }
    }

    private static RelatedListWrapper  relatedList = new RelatedListWrapper();

    @AuraEnabled
    public static Map<String,Object> initialize(String recordId, String title, String sObjectType, String relationshipFieldAPIName, String fieldSet) {

        System.debug('RelatedListCnt.initialize');

        Map<String,Object> response = new Map<String,Object>();
        response.put('error', false);
        try{

            //populate label Map
            Map<String, String> label = new Map<String,String>();
            if(String.isNotBlank(title) && title.contains('$Label.')){
                String[] split = title.split('\\.');
                String userLanguage = UserInfo.getLanguage();
                List<String> translations = MRO_UTL_LabelTranslator.translate(new List<String>{split[1]}, userLanguage);
                label.put('title', translations[0]);

            } else if (String.isNotBlank(title) && !title.contains('$Label.')){
                label.put('title', title);
            }else{

                String pluralSObjectLabel = Schema.getGlobalDescribe().get(sObjectType).getDescribe().getLabelPlural();
                label.put('title', pluralSObjectLabel);
            }

            response = createRelatedList(recordId, sObjectType, fieldSet, relationshipFieldAPIName, null, '');

            //response.put('recordsNumber', this.relatedList.records.size());
            response.put('relatedList', relatedList);
            response.put('label', label);

        }catch(Exception e){
            System.debug(e.getMessage());
            response.put('error', true);
            response.put('errorMessage', e.getMessage());
            response.put('errorStackTraceString', e.getStackTraceString());
        }
        return response;
    }


    @AuraEnabled
    public static Map<String,Object> createRelatedList(String recordId, String sObjectType, String fieldSet, String relationshipFieldAPIName, Integer orderByColumnIndex, String orderByDirection) {
        System.debug('RelatedListCnt.createRelatedList');

        Map<String,Object> response = new Map<String,Object>();
        try {
            response.put('error', false);

            List<String> sObjectFieldsToGet = new List<String>();
            if(String.isNotBlank(fieldSet) && !Test.isRunningTest()) {
                DescribeSObjectResult objResult = Schema.getGlobalDescribe().get(sObjectType).getDescribe();
                Schema.FieldSet fs = objResult.fieldSets.getMap().get(fieldSet); //'RelatedSObjectComponentFieldSet'
                List<Schema.FieldSetMember> fsms = fs.getFields();

                for (Schema.FieldSetMember fsm : fsms) {
                    sObjectFieldsToGet.add(fsm.getFieldPath());
                }
            }else if(Test.isRunningTest()){
                sObjectFieldsToGet.add('FirstName');
                sObjectFieldsToGet.add('LastName');
                sObjectFieldsToGet.add('AccountId');
            }


            fillRelatedListColumns(sObjectType, sObjectFieldsToGet);
            fillRelatedListRecords(recordId, sObjectType, relationshipFieldAPIName, orderByColumnIndex, orderByDirection); //country
            response.put('relatedList', relatedList);

        }catch(Exception ex){
            response.put('error', true);
            response.put('errorMessage', ex.getMessage());
            response.put('errorStackTraceString', ex.getStackTraceString());
        }
        return response;

    }

    private static void fillRelatedListColumns(String sObjectType, List<String> fieldsToGet) {

        if(relatedList.columns.isEmpty()) {

            DescribeSObjectResult objResult = Schema.getGlobalDescribe().get(sObjectType).getDescribe();
            for (String field : fieldsToGet) {
                ColumnWrapper column = new ColumnWrapper();

                DescribeFieldResult fieldResult = objResult.fields.getMap().get(field).getDescribe();
                column.label = fieldResult.getLabel();
                column.fieldName = fieldResult.getName();
                column.type = String.valueOf(fieldResult.getType()).toLowerCase();
                column.sortable = true;

                relatedList.columns.add(column);
            }
        }
    }

    private static void fillRelatedListRecords(String recordId, String sObjectType, String relationshipFieldAPIName, Integer orderByColumnIndex, String orderByDirection) {

        String selectClause = getSelectClause(sObjectType);
        String[] selectClauseSplit = selectClause.split(',');
        //String selectClause = String.join(selectClauseList, ',');

        String sQuery = ' SELECT ' + String.escapeSingleQuotes(selectClause)  +
                ' FROM ' + String.escapeSingleQuotes(sObjectType) +
                ' WHERE '+ String.escapeSingleQuotes(relationshipFieldAPIName) + '= \'' +  String.escapeSingleQuotes(recordId) + '\'';


        if(orderByColumnIndex != null){

            sQuery += ' ORDER BY ' + selectClauseSplit[orderByColumnIndex+1];
            if(String.isNotBlank(orderByDirection)){
                sQuery += ' ' + String.escapeSingleQuotes(orderByDirection);
            }
            sQuery += ' NULLS last';
        }
        else{
            if(!relatedList.columns.isEmpty()) {
                sQuery += ' ORDER BY ' + relatedList.columns[0].fieldName + ' ASC NULLS last';
            }
        }

        System.debug('##SC sQuery: ' + sQuery);
        List<SObject> records = Database.query(sQuery);

        Object fieldValue;
        Id fieldId;
        Id sObjectId;
        //String valueToGet;
        for (SObject record : records) {
            sObjectId = record.Id;
            List<Map<String, Object>> values = new List<Map<String, Object>>();

            for(ColumnWrapper column : relatedList.columns) {

                if (!column.queryField.contains('.')) {

                    fieldValue = record.get(column.queryField);
                } else {
                    SObject so = record;
                    String[] splitField = column.queryField.split('\\.');
                    String sObjType;
                    Integer j;
                    for (j = 0; j < splitField.size() - 1; j++) {

                        if (splitField[j].contains('__r')) {
                            fieldId = (Id) so.get(splitField[j].replace('__r', '__c'));
                            so = so.getSObject(splitField[j]);
                        }
                    }
                    fieldValue = so != null ? so.get(splitField[j]) : so;

                }

                if (fieldValue != null) {
                    values.add(new Map<String, Object> {'id' => fieldId, 'text' => fieldValue});
                } else {
                    values.add(new Map<String, Object> {'id' => null, 'text' => ''});
                }

            }
            relatedList.records.add(new RecordWrapper(
                sObjectId,
                values
            ));

        }
    }

    private static String getSelectClause(String sObjectType) {

        List<String> selectClauseList = new List<String>();
        selectClauseList.add('Id');

        for(ColumnWrapper column : relatedList.columns) {

            if(column.type == 'text' || column.type == 'textarea' || column.type == 'picklist') {

                selectClauseList.add('toLabel(' + column.fieldName + ')');
                column.queryField = column.fieldName;
            } else if(column.type == 'reference') {

                String field = column.fieldName;
                if(field.endsWith('__c')){
                    if(Schema.getGlobalDescribe().get(sObjectType).getDescribe().fields.getMap().get(field).getDescribe().getReferenceTo()[0].getDescribe().getName() != 'Case') {
                        field = field.replace('__c', '__r.Name');
                    }else {
                        field = field.replace('__c', '__r.CaseNumber');
                    }
                }else if(field.endsWith('Id')){
                    if(Schema.getGlobalDescribe().get(sObjectType).getDescribe().fields.getMap().get(field).getDescribe().getReferenceTo()[0].getDescribe().getName() != 'Case') {
                        field = field.replace('Id', '.Name');
                    }else {
                        field = field.replace('Id', '.CaseNumber');
                    }
                }
                column.queryField = field;
                selectClauseList.add(field);
            } else if(String.isNotBlank(column.type)) {

                selectClauseList.add(column.fieldName);
                column.queryField = column.fieldName;
            }
        }

        String selectClause = String.join(selectClauseList, ',');
        return selectClause;
    }
}