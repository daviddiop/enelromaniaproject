/**
 * Created by Boubacar Sow on 28/07/2020.
 */

public with sharing class MRO_LC_ZfofmQuery extends  ApexServiceLibraryCnt{
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();

    public with sharing class listZfofm extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            RequestParam params = (RequestParam) JSON.deserialize(jsonInput, RequestParam.class);
            try {
                if (params.startDate == null) {
                    throw new WrtsException(System.Label.StartDateIsEmpty);
                }
                if (params.endDate == null) {
                    throw new WrtsException(System.Label.EndDateIsEmpty);
                }
                if (params.caseId == null) {
                    throw new WrtsException(System.Label.MissingServiceId);
                }
                List<Zfofm> zfofms = listZfofm(params);
                response.put('zfofms', zfofms);
                response.put('error', false);
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }
    public class RequestParam {
        @AuraEnabled
        public Date startDate { get; set; }
        @AuraEnabled
        public Date endDate { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String contractAccountId { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String cnpCui { get; set; }
        @AuraEnabled
        public String documentType { get; set; }
        @AuraEnabled
        public String c39 { get; set; }
        @AuraEnabled
        public String codCompany { get; set; }

        public RequestParam(Date startDate, Date endDate, String caseId, String contractAccountId, String accountId,
            String cnpCui, String documentType, String c39,  String codCompany) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.caseId = caseId;
            this.contractAccountId = contractAccountId;
            this.accountId = accountId;
            this.cnpCui = cnpCui;
            this.documentType = documentType;
            this.c39 = c39;
            this.codCompany = codCompany;
        }


    }

    public static List<Zfofm> listZfofm(RequestParam params) {
        Integer i = 0;
        Map<Id, Case> caseMap = caseQuery.getCaseForZfofmSapFilenet(params.caseId, params.contractAccountId, params.accountId);
        System.debug('### caseMap '+caseMap);

        List<MRO_LC_ZfofmQuery.Zfofm> ZfofmList = new List<MRO_LC_ZfofmQuery.Zfofm>();

        if (!caseMap.isEmpty()) {
            for(Id caseId: caseMap.keySet()) {
                Case caseRecorde = caseMap.get(caseId);
                ZfofmList.add(new Zfofm(
                    caseRecorde.ContractAccount__c,
                    caseRecorde.AccountId,
                    caseRecorde.Id,
                    'filenet'+i,
                    'documentType'+i,
                    'c39'+i,
                    Date.today().addDays(i)
                ));
            }
        }
        return ZfofmList;
    }

    public class Zfofm {
        @AuraEnabled
        public String contractAccountId { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String filenet { get; set; }
        @AuraEnabled
        public String documentType { get; set; }
        @AuraEnabled
        public String c39 { get; set; }
        @AuraEnabled
        public Date issusDate { get; set; }

        public Zfofm(String contractAccountId, String accountId, String caseId,
            String filenet, String documentType, String c39,  Date issusDate) {
            this.contractAccountId = contractAccountId;
            this.accountId = accountId;
            this.caseId = caseId;
            this.filenet = filenet;
            this.documentType = documentType;
            this.c39 = c39;
            this.issusDate = issusDate;
        }


    }

}