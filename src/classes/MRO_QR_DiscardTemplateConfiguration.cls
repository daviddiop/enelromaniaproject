/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 05, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_DiscardTemplateConfiguration {

    public static MRO_QR_DiscardTemplateConfiguration getInstance() {
        return (MRO_QR_DiscardTemplateConfiguration) ServiceLocator.getInstance(MRO_QR_DiscardTemplateConfiguration.class);
    }

    public DiscardTemplateConfiguration__mdt getBySystemFlowAndError(String externalSystem, String flowName, String errorCode) {
        List<DiscardTemplateConfiguration__mdt> conf = [SELECT FieldsTemplateCode__c FROM DiscardTemplateConfiguration__mdt
                                                        WHERE ExternalSystem__c = :externalSystem AND FlowName__c = :flowName AND ErrorCode__c = :errorCode
                                                        LIMIT 1];
        if (conf != null && !conf.isEmpty()) {
            return conf[0];
        }
        return null;
    }
}