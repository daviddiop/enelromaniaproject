/**
 * @author  Luca Ravicing
 * @since   Mar 19, 2020
 * @desc   Controller class for contact selection
 *
 */

public with sharing class MRO_LC_ContactSelection extends ApexServiceLibraryCnt {
    private static final MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();
    private static final MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();


    public with sharing class getContacts extends AuraCallable{
        public override Object perform (final String jsonInput){
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            try {
                List<Contact> contacts = contactQuery.getContactByAccountId(accountId);
                Account account = accountQuery.findAccount(accountId);
                System.debug('***** account '+account);
                System.debug('***** contacts '+contacts);
                response.put('account', account);
                response.put('contacts', contacts);
                response.put('primaryContact', account.PrimaryContact__c);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

}