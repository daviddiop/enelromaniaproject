public with sharing class MRO_LC_TerminationWizard extends ApexServiceLibraryCnt {

    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static String caseNewStatus = constantsSrv.CASE_STATUS_NEW;
    private static String caseCancelStatus =  constantsSrv.CASE_STATUS_CANCELED;
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseServ = MRO_SRV_Case.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
    static List<String> disconnectionCausePicklistValues = new List<String>();
    static String disconnectionCauseLabel;
    private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    static String disconnectionDateLabel;
    //Add by INSA for task [ENLCRO-369]
    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    
    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String templateId = params.get('templateId');
            String defaultTermination = params.get('defaultDateTermination');
            Datetime effectiveDate;
            Datetime defaultDateTermination= System.today();
            String genericRequestId = params.get('genericRequestId');
            try {
                System.debug('accountId ----- '+accountId);
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                Boolean checkBackOfficePermission = FeatureManagement.checkPermission('MRO_TerminationBackOfficeProfile');
                if(String.isBlank(interactionId)){
                    if(!checkBackOfficePermission){
                        throw new WrtsException(System.Label.AccessDenied);
                    }
                }
                Account acc = accQuery.findAccount(accountId);
                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'Termination');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                if(cases.size()>0 && cases[0].EffectiveDate__c != null){
                   effectiveDate = cases[0].EffectiveDate__c;
                } else {
                    effectiveDate = System.today();
                    if (String.isBlank(dossier.CustomerInteraction__c)) {
                        effectiveDate.addDays(15);
                    } else {
                        for (Integer i = 0; i < 5; i++) {
                            while (MRO_UTL_Date.isNotWorkingDate(effectiveDate)) {
                                effectiveDate = effectiveDate.addDays(1);
                            }
                            effectiveDate = effectiveDate.addDays(1);
                        }
                    }
                }

                String code = 'ContractTermination_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }
                while (MRO_UTL_Date.isNotWorkingDate(effectiveDate)) {
                    effectiveDate = effectiveDate.addDays(1);
                }
                if(defaultTermination == null){
                    defaultDateTermination = effectiveDate;
                } else {
                    defaultDateTermination  = Date.valueOf(defaultTermination);

                }

                response.put('effectiveDate', effectiveDate);
                response.put('defaultDateTermination', defaultDateTermination);
                List<Reason__mdt> reasons = supplyQuery.getReasonsEntry();
                Map<String, Option> reasonValuesMap = MRO_LC_TerminationWizard.getCustomReasonsMap();
                List<Option> listReason = new List<Option>();
                for (Integer i = 0; i < reasons.size(); i++) {
                    if(String.isNotBlank(dossier.CustomerInteraction__c)) {
                        if(String.isNotBlank(reasons[i].CustomPermission__c)){
                            continue;
                        } else {
                            listReason.add(reasonValuesMap.get(reasons[i].Custom_Reason__c));
                        }
                    } else {
                        listReason.add(reasonValuesMap.get(reasons[i].Custom_Reason__c));
                    }
                }
                response.put('customReasons', listReason);
                response.put('defaultEffectiveDate', effectiveDate);
                getDisconnectionDescribe();
                Datetime dateCheckEaster = MRO_UTL_Date.getEasterDate(System.now().year());
                Datetime dateCheckPentecost = MRO_UTL_Date.getPentecostDate(System.now().year());
                response.put('dateCheckEaster', dateCheckEaster);
                response.put('dateCheckPentecost', dateCheckPentecost);
                response.put('caseTile', cases);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('account', acc);
                response.put('accountId', accountId);
                response.put('accountPersonRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId());
                response.put('accountPersonProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId());
                response.put('accountBusinessRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId());
                response.put('accountBusinessProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId());

                response.put('disconnectionCauseLabel', disconnectionCauseLabel);
                response.put('disconnectionDateLabel', disconnectionDateLabel);
                response.put('terminationELE', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_ELE').getRecordTypeId());
                response.put('terminationGAS', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_GAS').getRecordTypeId());
                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('accessDenied', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public static void getDisconnectionDescribe() {
        disconnectionCausePicklistValues = new List<String>();
        Schema.DescribeFieldResult disconnectionCause = Case.Reason__c.getDescribe();
        Schema.DescribeFieldResult disconnectionDate = Case.EffectiveDate__c.getDescribe();
        List<Schema.PicklistEntry> disconnectionCausePicklist = disconnectionCause.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : disconnectionCausePicklist) {
            disconnectionCausePicklistValues.add(pickListVal.getValue());
        }
        disconnectionCauseLabel = disconnectionCause.getLabel();
        disconnectionDateLabel = disconnectionDate.getLabel();
    }
    public with sharing class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();

            Map<String, String> terminationRecordTypes = Constants.getCaseRecordTypes('Termination');
            String recordTypeId;
            Id trader = null;
            Set<String> listPenalities12Months = new Set<String>{'Enel Asistenta','Enel Asistenta +','Enel Asistenta Asociatii Proprietari nelimitat', 'Enel Asistenta Asociatii Proprietari'};
            Set<String> listPenalities24Months = new Set<String>{'Enel Asistenta Gaz','Enel Asistenta Gaz +','Enel Asistenta Gaz IMM', 'Enel Asistenta Gaz IMM +'};
            CreateCaseInput caseDataCaseInput = (CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);

            Date effectiveDate = Date.valueOf(caseDataCaseInput.effectiveDate);
            String accountId = caseDataCaseInput.accountId;
            String dossierId = caseDataCaseInput.dossierId;
            String reason = caseDataCaseInput.reason;
            String originSelected = caseDataCaseInput.originSelected;
            List<Supply__c> searchedSupplyFieldsList = (List<Supply__c>)JSON.deserialize(caseDataCaseInput.supplies, List<Supply__c>.class);
            List<Case> caseList = (List<Case>) JSON.deserialize(caseDataCaseInput.caseTile, List<Case>.class);
            Map<Id, Case> caseMap = new Map<Id, Case>(caseList);

            String channelSelected = caseDataCaseInput.channelSelected;
            List<Case> newCases = new List<Case>();

            List<Case> updateCases = new List<Case>();
            Savepoint sp = Database.setSavepoint();
            try {
                if (MRO_UTL_Date.isNotWorkingDate(effectiveDate)) {
                    throw new WrtsException(System.Label.notWorkingDay);
                }
                if (!String.isBlank(caseDataCaseInput.caseId) && caseMap.containsKey(caseDataCaseInput.caseId)) {
                    Case existingCase = caseMap.get(caseDataCaseInput.caseId);
                    existingCase.Reason__c = reason;
                    existingCase.EffectiveDate__c = effectiveDate;
                    updateCases.add(existingCase);
                } else if ( String.isNotBlank(reason) && (!searchedSupplyFieldsList.isEmpty()) && (effectiveDate != null) && (String.isNotBlank(accountId)) && (String.isNotBlank(dossierId))) {
                    for (Supply__c supply : searchedSupplyFieldsList) {
                        String notePenality = System.Label.NoteForNotAppliedPenalty;
                        recordTypeId = supply.RecordType.DeveloperName == 'Gas' ? terminationRecordTypes.get('Termination_GAS') : supply.RecordType.DeveloperName == 'Electric' ? terminationRecordTypes.get('Termination_ELE') : terminationRecordTypes.get('Termination_Service');

                        Case disconnectedCase = caseServ.createCase(null, supply, accountId, dossierId, recordTypeId, reason, effectiveDate,
                            trader, originSelected, channelSelected);
                        if (supply.Contract__r.EndDate != null ) {
                            Date endDateContract = supply.Contract__r.EndDate;
                            Integer monthDifference = effectiveDate.monthsBetween(endDateContract);
                            if (supply.Product__r != null) {
                                if(listPenalities12Months.contains(supply.Product__r.Name)){
                                    if(monthDifference > 12){
                                        notePenality = System.Label.NoteForAppliedPenalty;
                                    }
                                }else if(listPenalities24Months.contains(supply.Product__r.Name)){
                                    if(monthDifference > 24){
                                        notePenality = System.Label.NoteForAppliedPenalty;
                                    }
                                }
                            }
                        }
                        disconnectedCase.DisCoNotes__c = notePenality;
                        if (disconnectedCase != null) {
                            newCases.add(disconnectedCase);
                        }
                    }
                }
                if (newCases.size()>0) {
                    databaseSrv.insertSObject(newCases);
                    for (Case c : newCases) {
                        caseMap.put(c.Id, c);
                    }
                }
                else if (!updateCases.isEmpty()) {
                    databaseSrv.updateSObject(updateCases);
                    for (Case c : updateCases) {
                        caseMap.put(c.Id, c);
                    }
                }

                response.put('caseTile', caseMap.values());
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                throw ex;
            }
            return response;
        }
    }

    public with sharing class updateCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Date effectiveDate = (Date) JSON.deserialize(params.get('disconnectionDate'), Date.class);
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            String reason = params.get('disconnectionCause');
            String caseId = params.get('caseId');
            Savepoint sp = Database.setSavepoint();
            List<Case> newCases = new List<Case>();

            try {
                if (MRO_UTL_Date.isNotWorkingDate(effectiveDate)) {
                    throw new WrtsException(System.Label.notWorkingDay);
                }
                List<Case> oldCaseList = new List<Case>();
                for(Case cases : caseList){
                    if (cases.Id == caseId){
                        cases.Reason__c = reason;
                        cases.EffectiveDate__c =effectiveDate;
                        oldCaseList.add(cases);
                    } else {
                        newCases.add(cases);
                    }
                }
                List<Case> updatedCases =  caseServ.updateCaseTermination(oldCaseList);
                for (Case c : updatedCases){
                    newCases.add(c);
                }
                response.put('updatedCaseTile', newCases);
                response.put('error', false);

            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String cancelReason = params.get('cancelReason');
            String detailReason = params.get('detailReason');
            Savepoint sp = Database.setSavepoint();

            try {
                for(Case caseRecord: oldCaseList){
                    caseRecord.CancellationReason__c=cancelReason;
                    caseRecord.CancellationDetails__c = detailReason;
                }
                caseServ.setCanceledOnCaseAndDossier(oldCaseList, dossierId, 'RC010',caseCancelStatus);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }
    /**
    *    @Author: INSA BADJI
    *    Descriptio: task [ENLCRO-369]  Get data of selected supply in advanced Seardh
    *    Date 19/12/2019
    *    @params:supplyId
     */
    public Inherited sharing class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            try {
                supplyInputIds createCaseParams = (supplyInputIds)JSON.deserialize(jsonInput, supplyInputIds.class);
                String origin = createCaseParams.origin;
                List<Supply__c> supplyListRecord = supplyQuery.getSuppliesByIds(createCaseParams.supplyId);
                List<Reason__mdt> reasons = supplyQuery.getReasonsEntry();
                System.debug('MRO_LC_TerminationWizard.getSupplyRecord - reasons: ' + reasons);
                Map<String, Option> reasonValuesMap = MRO_LC_TerminationWizard.getCustomReasonsMap();
                System.debug('MRO_LC_TerminationWizard.getSupplyRecord - reasonValuesMap: ' + reasonValuesMap);
                List<Option> listReason = new List<Option>();
                for (Integer i = 0; i < reasons.size(); i++) {

                   if (origin == 'Internal' && reasons[i].Custom_Reason__c != 'Customer request' &&
                           reasons[i].Custom_Reason__c != 'Termination for non-payment') {

                       listReason.add(reasonValuesMap.get(reasons[i].Custom_Reason__c));

                   } else if (origin != 'Internal' && reasons[i].Custom_Reason__c == 'Customer request' &&
                           reasons[i].Custom_Reason__c != 'Termination for non-payment') {

                       listReason.add(reasonValuesMap.get(reasons[i].Custom_Reason__c));
                   }

                }
                response.put('customReasons', listReason);
                response.put('supplies', supplyListRecord);
            } catch (Exception ex) {

                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    /**
   * @author  INSA BADJI
   * @description getCase Termination wizard.
   * @date 17/01/2020
   * @param recordId
   * @return caseRecord
   */

    public with sharing class getCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('recordId');
            Case caseRecord = caseQuery.getById(recordId);
            response.put('caseRecord', caseRecord);
            response.put('error', false);
            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');
            dossierSrv.updateOriginAndChannelOnDossier(dossierId, channel, origin);
            return true;
        }
    }

    /**
    * @author  INSA BADJI
    * @description save Wizard  [ENLCRO-555]
    * Updated by BG - 07/03/2020
    * @date 13/01/2020
    */
    public with sharing class SaveWizard extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            Savepoint sp = Database.setSavepoint();
            try {

                Map<String, String> params = asMap(jsonInput);
                String dossierId = params.get('dossierId');
                String channelSelected = params.get('channelSelected');
                String originSelected = params.get('originSelected');
                String companyDivisionId = String.isNotBlank(params.get('companyDivisionId')) ?
                        params.get('companyDivisionId') : null;
                List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
                List<Supply__c> supplyList = new List<Supply__c>();
                for(Case caseRecord : caseList){
                    caseRecord.Status =caseNewStatus;
                    supplyList.add(new Supply__c(
                            Id = caseRecord.Supply__c,
                            Status__c = constantsSrv.SUPPLY_STATUS_TERMINATING,
                            Terminator__c = caseRecord.Id));
                }

                // Update dossier
                Dossier__c updatedDossier = new Dossier__c(
                        Id = dossierId,
                        SubProcess__c = '',
                        Status__c = constantsSrv.DOSSIER_STATUS_NEW,
                        Channel__c = channelSelected,
                        Origin__c = originSelected,
                        CompanyDivision__c = companyDivisionId);
                databaseSrv.updateSObject(updatedDossier);
                Boolean isUpdated = databaseSrv.updateSObject(updatedDossier);
                if(isUpdated){

                    updatedDossier = dossierQuery.getById(updatedDossier.Id);
                }

                // Update cases
                List<Case> updatedCases = caseServ.updateCaseTermination(caseList);

                // update supplies
                if (!supplyList.isEmpty()) {
                    databaseSrv.updateSObject(supplyList);
                }

                // Automatic phase transitions after saving on the wizards
                dossierSrv.checkAndApplyAutomaticTransitionToDossierAndCases(
                        updatedDossier,
                        updatedCases,
                        constantsSrv.CONFIRM_TAG,
                        true,
                        false,
                        false
                );

            } catch (Exception ex) {

                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    private static Map<String, Option> getCustomReasonsMap() {
        List<Option> reasonValues = MRO_UTL_Utils.getPickListValues('Reason__mdt', 'Custom_Reason__c');
        Map<String, Option> reasonValuesMap = new Map<String, Option>();
        for (Option reasonVal : reasonValues) {
            reasonValuesMap.put(reasonVal.value, reasonVal);
        }
        return reasonValuesMap;
    }

    public class CreateCaseInput {
        @AuraEnabled
        public String supplies { get; set; }
        @AuraEnabled
        public String reason { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String effectiveDate {get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseTile { get; set; }
    }
    public class supplyInputIds {
        @AuraEnabled
        public List<Id> supplyId { get; set; }
        public String origin { get; set; }
    }
}