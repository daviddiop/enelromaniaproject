@IsTest
private class MRO_LC_FisaInquiryTest {

    @IsTest
    static void testListFisa() {
        MRO_LC_FisaInquiry.FisaRequestParam params = new MRO_LC_FisaInquiry.FisaRequestParam('codCompanie', 'codDePlataId', 'cuSold', 'facturiDela',
            'facturiPanala', 'incasatDela', 'incasatPanala', 'partnerId');

        Test.startTest();
        SAPCallouts__c sapCallouts = new SAPCallouts__c(MeterList__c =true);
        insert sapCallouts;
        List<MRO_LC_FisaInquiry.Fisa> fisaList = (List<MRO_LC_FisaInquiry.Fisa>)
            TestUtils.exec('MRO_LC_FisaInquiry','listFisa', params,true);

        Test.stopTest();

        System.assertEquals(null, fisaList);
    }
@IsTest
    static void testListFisa2() {
        MRO_LC_FisaInquiry.FisaRequestParam params = new MRO_LC_FisaInquiry.FisaRequestParam('codCompanie', 'codDePlataId', 'cuSold', 'facturiDela',
            'facturiPanala', 'incasatDela', 'incasatPanala', 'partnerId');

        Test.startTest();
        SAPCallouts__c sapCallouts = new SAPCallouts__c(MeterList__c =false);
        insert sapCallouts;
        Map<String, String> stringMap = new Map<String, String>{
                'DocumentValue' => '4902',
                'DebitTurnover' => '21',
                'CreditTurnover' => '31',
                'Balance' => '0'
        };
        MRO_LC_FisaInquiry.Fisa fisa = new MRO_LC_FisaInquiry.Fisa(stringMap);
        List<MRO_LC_FisaInquiry.Fisa> fisaList = (List<MRO_LC_FisaInquiry.Fisa>)
            TestUtils.exec('MRO_LC_FisaInquiry','listFisa', params,true);

        Test.stopTest();

        System.assert(null != fisaList);
    }
}