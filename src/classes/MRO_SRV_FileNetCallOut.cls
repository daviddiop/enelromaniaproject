/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   25/03/2020
* @desc
* @history
*/
public with sharing class MRO_SRV_FileNetCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout {
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap){
        System.debug('MRO_SRV_FileNetCallOut.buildMultiRequest');

        //Sobject
        SObject obj = (SObject) argsMap.get('sender');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        //Query
        FileMetadata__c document = [SELECT  Id,
                                            DocumentID__c,
                                            Title__c,
                                            ExternalId__c,
                                            Dossier__c
                                    FROM    FileMetadata__c
                                    WHERE   Id = :obj.Id];

        //to WObject
        Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject(document, null);

        //Add mrrObj to the request
        request.objects.add(mrrObj);

        //Add request to Multirequest
        multiRequest.requests.add(request);

        return multiRequest;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        return null;
    }
}