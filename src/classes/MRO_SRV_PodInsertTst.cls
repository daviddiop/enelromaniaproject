/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 18.05.20.
 */

@IsTest
public with sharing class MRO_SRV_PodInsertTst {

    static final String fileTemplateNameElectric = 'PODTemplateElectric';
    static final String fileTemplateNameGas = 'PODTemplateGas';
    static final Integer rowsCountForInsert = 1;
    static final String delimiterString = ';';
    static final String rowDelimiterString = '\n';
    static final String fileExtensionString = '.csv';
    static final String fileTypeString = 'CSV';

    static final String cvTitleEle = 'testPodsFileEle';
    static final String cvTitleGas = 'testPodsFileGas';

    static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    @TestSetup
    private static void setup() {

        Account accountDistributor = TestDataFactory.account().traderAccount().build();
        accountDistributor.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        accountDistributor.Name = 'DistributorAccountEle';
        accountDistributor.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        accountDistributor.BusinessType__c = 'Real estate';
        accountDistributor.IsDisCoENEL__c = true;
        accountDistributor.IsDisCoEle__c = true;
        accountDistributor.Key__c = 'ADE12345678';
        insert accountDistributor;

        Account accountDistributorGas = TestDataFactory.account().traderAccount().build();
        accountDistributorGas.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        accountDistributorGas.Name = 'DistributorAccountGas';
        accountDistributorGas.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        accountDistributorGas.BusinessType__c = 'Real estate';
        accountDistributorGas.IsDisCoENEL__c = true;
        accountDistributorGas.IsDisCoGas__c = true;
        accountDistributorGas.Key__c = 'ADG12345678';
        insert accountDistributorGas;

        Account accountTrader = TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'TraderAccount';
        accountTrader.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.Key__c = 'AT123456789';
        insert accountTrader;

        List<Account> accounts = [SELECT Id, Key__c, Name, RecordTypeId FROM Account];
        Map<Id, Account> accountsMap = new Map<Id, Account>(accounts);
        accountTrader = accountsMap.get(accountTrader.Id);
        accountDistributor = accountsMap.get(accountDistributor.Id);
        accountDistributorGas = accountsMap.get(accountDistributorGas.Id);

        //add file template
        FileTemplate__c ft1 = new FileTemplate__c(
                AllOrNone__c = true,
                BatchSize__c = 200,
                FileDelimiter__c = delimiterString,
                FileExtension__c = fileExtensionString,
                FileTemplateName__c = fileTemplateNameElectric,
                FileType__c = fileTypeString,
                FinishAction__c = 'MRO_SRV_PodInsert',
                MaxSizeMB__c = 8,
                MaxSizeRows__c = 200

        );

        insert ft1;

        FileTemplate__c ft2 = new FileTemplate__c(
                AllOrNone__c = true,
                BatchSize__c = 200,
                FileDelimiter__c = delimiterString,
                FileExtension__c = fileExtensionString,
                FileTemplateName__c = fileTemplateNameGas,
                FileType__c = fileTypeString,
                FinishAction__c = 'MRO_SRV_PodInsert',
                MaxSizeMB__c = 8,
                MaxSizeRows__c = 200

        );

        insert ft2;

        ServicePoint__c sp = new ServicePoint__c(Code__c = '35555555555555');
        insert sp;

        //add file template columns
        List<FileTemplateColumn__c> tcList = new List<FileTemplateColumn__c>();
        
        //Electric
        tcList.add(createColumn(ft1.Id, 'ServicePointCode', true, 1));
        tcList.add(createColumn(ft1.Id, 'Distributor', true, 2));
        tcList.add(createColumn(ft1.Id, 'Trader', true, 3));
        tcList.add(createColumn(ft1.Id, 'AvailablePower', true, 4));
        tcList.add(createColumn(ft1.Id, 'ContractualPower', true, 5));
        tcList.add(createColumn(ft1.Id, 'EstimatedConsumption', true, 6));
        tcList.add(createColumn(ft1.Id, 'VoltageLevel', true, 7));
        tcList.add(createColumn(ft1.Id, 'VoltageSetting', true, 8));
        tcList.add(createColumn(ft1.Id, 'PowerPhase', true, 9));
        tcList.add(createColumn(ft1.Id, 'FlowRate', true, 10));
        tcList.add(createColumn(ft1.Id, 'ConsumptionCategory', false, 11));
        tcList.add(createColumn(ft1.Id, 'EndDate', false, 12));
        tcList.add(createColumn(ft1.Id, 'IsNewConnection', true, 13));
        tcList.add(createColumn(ft1.Id, 'ApplicantNotHolder', true, 14));
        tcList.add(createColumn(ft1.Id, 'FlatRate', true, 15));
        tcList.add(createColumn(ft1.Id, 'NonDisconnectable', true, 16));
        tcList.add(createColumn(ft1.Id, 'NonDisconnectableReason', false, 17));
        tcList.add(createColumn(ft1.Id, 'NLC', false, 18));
        tcList.add(createColumn(ft1.Id, 'CLC', false, 19));
        tcList.add(createColumn(ft1.Id, 'SiteDescription', false, 20));
        tcList.add(createColumn(ft1.Id, 'PointCountry', false, 21));
        tcList.add(createColumn(ft1.Id, 'PointCity', false, 22));
        tcList.add(createColumn(ft1.Id, 'PointProvince', false, 23));
        tcList.add(createColumn(ft1.Id, 'PointPostalCode', false, 24));
        tcList.add(createColumn(ft1.Id, 'PointStreetType', false, 25));
        tcList.add(createColumn(ft1.Id, 'PointStreetName', false, 26));
        tcList.add(createColumn(ft1.Id, 'PointStreetNumber', false, 27));
        tcList.add(createColumn(ft1.Id, 'PointStreetNumberExtn', false, 28));
        tcList.add(createColumn(ft1.Id, 'PointLocality', false, 29));
        tcList.add(createColumn(ft1.Id, 'PointBuilding', false, 30));
        tcList.add(createColumn(ft1.Id, 'PointApartment', false, 31));
        tcList.add(createColumn(ft1.Id, 'PointFloor', false, 32));
        tcList.add(createColumn(ft1.Id, 'SiteCountry', false, 33));
        tcList.add(createColumn(ft1.Id, 'SiteCity', false, 34));
        tcList.add(createColumn(ft1.Id, 'SiteProvince', false, 35));
        tcList.add(createColumn(ft1.Id, 'SitePostalCode', false, 36));
        tcList.add(createColumn(ft1.Id, 'SiteStreetType', false, 37));
        tcList.add(createColumn(ft1.Id, 'SiteStreetName', false, 38));
        tcList.add(createColumn(ft1.Id, 'SiteStreetNumber', false, 39));
        tcList.add(createColumn(ft1.Id, 'SiteStreetNumberExtn', false, 40));
        tcList.add(createColumn(ft1.Id, 'SiteLocality', false, 41));
        tcList.add(createColumn(ft1.Id, 'SiteBuilding', false, 42));
        tcList.add(createColumn(ft1.Id, 'SiteApartment', false, 43));
        tcList.add(createColumn(ft1.Id, 'SiteFloor', false, 44));

        //Gas
        tcList.add(createColumn(ft2.Id, 'ServicePointCode', true, 1));
        tcList.add(createColumn(ft2.Id, 'Distributor', true, 2));
        tcList.add(createColumn(ft2.Id, 'Trader', true, 3));
        tcList.add(createColumn(ft2.Id, 'AvailablePower', true, 4));
        tcList.add(createColumn(ft2.Id, 'ContractualPower', true, 5));
        tcList.add(createColumn(ft2.Id, 'EstimatedConsumption', true, 6));
        tcList.add(createColumn(ft2.Id, 'Pressure', true, 7));
        tcList.add(createColumn(ft2.Id, 'PressureLevel', true, 8));
        tcList.add(createColumn(ft2.Id, 'FlowRate', true, 10));
        tcList.add(createColumn(ft2.Id, 'ConsumptionCategory', false, 11));
        tcList.add(createColumn(ft2.Id, 'EndDate', false, 12));
        tcList.add(createColumn(ft2.Id, 'IsNewConnection', true, 13));
        tcList.add(createColumn(ft2.Id, 'ApplicantNotHolder', true, 14));
        tcList.add(createColumn(ft2.Id, 'FlatRate', true, 15));
        tcList.add(createColumn(ft2.Id, 'NonDisconnectable', true, 16));
        tcList.add(createColumn(ft2.Id, 'NonDisconnectableReason', false, 17));
        tcList.add(createColumn(ft2.Id, 'NLC', false, 18));
        tcList.add(createColumn(ft2.Id, 'CLC', false, 19));
        tcList.add(createColumn(ft2.Id, 'SiteDescription', false, 20));
        tcList.add(createColumn(ft2.Id, 'PointCountry', false, 21));
        tcList.add(createColumn(ft2.Id, 'PointCity', false, 22));
        tcList.add(createColumn(ft2.Id, 'PointProvince', false, 23));
        tcList.add(createColumn(ft2.Id, 'PointPostalCode', false, 24));
        tcList.add(createColumn(ft2.Id, 'PointStreetType', false, 25));
        tcList.add(createColumn(ft2.Id, 'PointStreetName', false, 26));
        tcList.add(createColumn(ft2.Id, 'PointStreetNumber', false, 27));
        tcList.add(createColumn(ft2.Id, 'PointStreetNumberExtn', false, 28));
        tcList.add(createColumn(ft2.Id, 'PointLocality', false, 29));
        tcList.add(createColumn(ft2.Id, 'PointBuilding', false, 30));
        tcList.add(createColumn(ft2.Id, 'PointApartment', false, 31));
        tcList.add(createColumn(ft2.Id, 'PointFloor', false, 32));
        tcList.add(createColumn(ft2.Id, 'SiteCountry', false, 33));
        tcList.add(createColumn(ft2.Id, 'SiteCity', false, 34));
        tcList.add(createColumn(ft2.Id, 'SiteProvince', false, 35));
        tcList.add(createColumn(ft2.Id, 'SitePostalCode', false, 36));
        tcList.add(createColumn(ft2.Id, 'SiteStreetType', false, 37));
        tcList.add(createColumn(ft2.Id, 'SiteStreetName', false, 38));
        tcList.add(createColumn(ft2.Id, 'SiteStreetNumber', false, 39));
        tcList.add(createColumn(ft2.Id, 'SiteStreetNumberExtn', false, 40));
        tcList.add(createColumn(ft2.Id, 'SiteLocality', false, 41));
        tcList.add(createColumn(ft2.Id, 'SiteBuilding', false, 42));
        tcList.add(createColumn(ft2.Id, 'SiteApartment', false, 43));
        tcList.add(createColumn(ft2.Id, 'SiteFloor', false, 44));

        insert tcList;

        City__c city = new City__c(Name = 'SECTOR 1', Country__c = 'ROMANIA', Province__c = 'BUCURESTI');
        insert city;

        Street__c street = new Street__c(Name = 'Sfanta Maria', StreetType__c = 'STRADA', PostalCode__c = '11496', FromStreetNumber__c = 1, ToStreetNumber__c = 50, City__c = city.Id, Verified__c = true);
        insert street;

        //create file
        List<String> rows = new List<String>{
                'ServicePointCode;Distributor;Trader;AvailablePower;ContractualPower;EstimatedConsumption;VoltageLevel;VoltageSetting;PowerPhase;FlowRate;ConsumptionCategory;EndDate;IsNewConnection;ApplicantNotHolder;FlatRate;NonDisconnectable;NonDisconnectableReason;NLC;CLC;SiteDescription;PointCountry;PointCity;PointProvince;PointPostalCode;PointStreetType;PointStreetName;PointStreetNumber;PointStreetNumberExtn;PointLocality;PointBuilding;PointApartment;PointFloor;SiteCountry;SiteCity;SiteProvince;SitePostalCode;SiteStreetType;SiteStreetName;SiteStreetNumber;SiteStreetNumberExtn;SiteLocality;SiteBuilding;SiteApartment;SiteFloor',
                '35555555555555;'+accountDistributor.Key__c+';'+accountTrader.Key__c+';1;1;1;LV;230;Single;0;;;false;false;false;false;;;;;ROMANIA;SECTOR 1;BUCURESTI;11496;STRADA;Sfanta Maria;22;;;;;;ROMANIA;SECTOR 1;BUCURESTI;11496;STRADA;Sfanta Maria;22;;;;;'
        };
        String fileString = String.join(rows, rowDelimiterString) + rowDelimiterString;

        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        Blob myBlob = Blob.valueOf(fileString);
        cv.VersionData = myBlob;
        cv.Title = cvTitleEle;
        cv.PathOnClient = 'testPodsFile.csv';
        insert cv;

        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id].ContentDocumentId;
        cdl.LinkedEntityId = ft1.Id;
        cdl.ShareType = 'V';
        insert cdl;

        //create file gas
        List<String> rowsGas = new List<String>{
                'ServicePointCode;Distributor;Trader;AvailablePower;ContractualPower;EstimatedConsumption;Pressure;PressureLevel;FlowRate;ConsumptionCategory;EndDate;IsNewConnection;ApplicantNotHolder;FlatRate;NonDisconnectable;NonDisconnectableReason;NLC;CLC;SiteDescription;PointCountry;PointCity;PointProvince;PointPostalCode;PointStreetType;PointStreetName;PointStreetNumber;PointStreetNumberExtn;PointLocality;PointBuilding;PointApartment;PointFloor;SiteCountry;SiteCity;SiteProvince;SitePostalCode;SiteStreetType;SiteStreetName;SiteStreetNumber;SiteStreetNumberExtn;SiteLocality;SiteBuilding;SiteApartment;SiteFloor',
                '35555555555555;'+accountDistributorGas.Key__c+';'+accountTrader.Key__c+';1;1;1;12;Low;0;;;false;false;false;false;;12;;;ROMANIA;SECTOR 1;BUCURESTI;11496;STRADA;Sfanta Maria;22;;;;;;ROMANIA;SECTOR 1;BUCURESTI;11496;STRADA;Sfanta Maria;22;;;;;'
        };
        String fileStringGas = String.join(rowsGas, rowDelimiterString) + rowDelimiterString;

        ContentVersion cvGas = new ContentVersion();
        cvGas.ContentLocation = 'S';
        Blob myBlobGas = Blob.valueOf(fileStringGas);
        cvGas.VersionData = myBlobGas;
        cvGas.Title = cvTitleGas;
        cvGas.PathOnClient = 'testPodsFileGas.csv';
        insert cvGas;

        ContentDocumentLink cdlGas = new ContentDocumentLink();
        cdlGas.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cvGas.Id].ContentDocumentId;
        cdlGas.LinkedEntityId = ft2.Id;
        cdlGas.ShareType = 'V';
        insert cdlGas;

        MRO_UTL_TestDataFactory.OpportunityBuilder oppBuilder = MRO_UTL_TestDataFactory.opportunity().createOpportunity();
        Opportunity opp = oppBuilder.build();
        opp.Name = 'Opp Ele';
        insert opp;

        MRO_UTL_TestDataFactory.OpportunityBuilder oppBuilder2 = MRO_UTL_TestDataFactory.opportunity().createOpportunity();
        Opportunity opp2 = oppBuilder2.build();
        opp2.Name = 'Opp Gas';
        insert opp2;

        /*

         String emailRegex = '^[a-zA-Z0-9._}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
         String passwordRegex = '^.*(?=.{8,})(?=.*\\d)(?=.*[a-zA-Z])|(?=.{8,})(?=.*\\d)(?=.*[!@#$%^&])|(?=.{8,})(?=.*[a-zA-Z])(?=.*[!@#$%^&]).*$';
         String dateRegex = '^\\d{4}-\\d{2}-\\d{2}$';
         String ssnRegex = '[0-9]{3}[-]?[0-9]{2}[-]?[0-9]{4}';
         String phoneRegex='^([0-9\\(\\)\\/\\+ \\-]*)$';
         String zipRegex='^\\d{5}(?:[-\\s]?\\d{4})?$';
         String numberRegex = '^[0-9]\\d*(\\.\\d+)?$';
         String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
         */
    }

    private static FileTemplateColumn__c createColumn(String fileTemplateId, String headerName, Boolean mandatory, Integer index) {
        return new FileTemplateColumn__c(
                FileTemplate__c = fileTemplateId,
                HeaderName__c = headerName,
                Index__c = index,
                Mandatory__c = mandatory
        );
    }

    @IsTest
    static void fileImportElectric() {
        createFileImportJobTest(getParams(MRO_UTL_Constants.ELECTRIC));
    }

    @IsTest
    static void fileImportGas() {
        createFileImportJobTest(getParams(MRO_UTL_Constants.GAS));
    }

    static void createFileImportJobTest(Map<String, Object> inputJson) {

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_MassiveHelper', 'createFileImportJob', inputJson, true);
        Test.stopTest();

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));

        String templateId = (String) inputJson.get('templateId');
        List<FileImportJob__c> jobs = [SELECT Id, Status__c FROM FileImportJob__c WHERE FileTemplate__c = :templateId];
        System.assertEquals(1, jobs.size());
        System.assertEquals('Completed', jobs[0].Status__c);

        List<FileImportDataRow__c> rows = [SELECT Id, Status__c, ErrorMessage__c, JSONRow__c FROM FileImportDataRow__c WHERE FileImportJob__c = :jobs[0].Id];
        System.debug(rows);
        System.assertEquals(rowsCountForInsert, rows.size());

        List<OpportunityServiceItem__c> osiList = [SELECT Id FROM OpportunityServiceItem__c];
        System.debug(osiList);
        System.assertEquals(rowsCountForInsert, osiList.size());

        /*Object switchInResponse = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'createPointsFromOpportunity', inputJson, true);

        Map<String, Object> switchInResult = (Map<String, Object>) switchInResponse;
        System.assertEquals(false, switchInResult.get('error'));
        System.debug(switchInResult);*/
    }

    private static Map<String, Object> getParams(String commodity) {
        String ftName = commodity == MRO_UTL_Constants.GAS ? fileTemplateNameGas : fileTemplateNameElectric;
        List<FileTemplate__c> templates = [SELECT Id FROM FileTemplate__c WHERE FileTemplateName__c = :ftName];
        System.assertEquals(1, templates.size());

        String cvTitle = commodity == MRO_UTL_Constants.GAS ? cvTitleGas : cvTitleEle;
        List<ContentVersion> cvList = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Title = :cvTitle];
        System.assertEquals(1, cvList.size());

        String oppName = commodity == MRO_UTL_Constants.GAS ? 'Opp Gas' : 'Opp Ele';
        List<Opportunity> oppList = [SELECT Id FROM Opportunity WHERE Name = :oppName];
        System.assertEquals(1, oppList.size());

        Map<String, String> params = new Map<String, String>{
                'opportunityId' => oppList[0].Id,
                'commodity' => commodity
        };
        Map<String, Object> inputJSON = new Map<String, Object>{
                'documentId' => cvList[0].ContentDocumentId,
                'templateId' => templates[0].Id,
                'params' => JSON.serialize(params),
                'opportunityId' => oppList[0].Id
        };
        return inputJSON;
    }
}