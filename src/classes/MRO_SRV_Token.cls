/**
 * Created by tommasobolis on 29/04/2020.
 */
public with sharing class MRO_SRV_Token {

    /** SERVICE CLASS INSTANCES **/
    /** Database service instance **/
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    /**
     * Returns and instance of the current class.
     *
     * @return instance of the current class.
     */
    public static MRO_SRV_Token getInstance() {

        return (MRO_SRV_Token) ServiceLocator.getInstance(MRO_SRV_Token.class);
    }

    /**
     * Creates a token for a specific touch-point identified by the provided parameters
     * @param source
     * @param type
     * @param sendingChannel
     * @return
     */
    public Token__c insertTouchPointToken(TouchPointConfiguration__mdt touchPointConfiguration, String touchPointUrl) {
        return this.insertTouchPointToken(touchPointConfiguration, touchPointUrl, true);
    }

    public Token__c insertTouchPointToken(TouchPointConfiguration__mdt touchPointConfiguration, String touchPointUrl, Boolean doInsert) {

        // Declare token variable
        Token__c token;
        // Generate guid
        String guid = MRO_UTL_Guid.newGuid();
        // Initialize token
        token = new Token__c(
                Value__c = guid,
                TouchPointUrl__c = touchPointUrl,
                ExpireDateTime__c = touchPointConfiguration.TokenLifetime__c != null ?
                        System.Datetime.now().addHours(Integer.valueOf(touchPointConfiguration.TokenLifetime__c)) : null
            );
        // Insert token
        if (doInsert) {
            databaseSrv.insertSObject(token);
        }
        return token;
    }
}