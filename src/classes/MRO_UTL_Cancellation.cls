/**
 * Created by goudiaby on 21/01/2020.
 */

public with sharing class MRO_UTL_Cancellation {
    public static MRO_QR_Case casesQuery = MRO_QR_Case.getInstance();
    private static MRO_UTL_SwitchOut switchOutUTL = MRO_UTL_SwitchOut.getInstance();

    /**
    * @author  Baba Goudiaby
    * @description [ENLCRO-43] Request Cancellation WP1-P2 - Implementation
    * @date 21/01/2020
    * @param requestId Record id of the request
    * @param processName Name of process
    *
    * @return String errorMessage
    */
    public static Map<String,Object> isCancellable(String requestId) {
        Case caseRecord = casesQuery.getForCancellationById(requestId);
        Map<String,Object> response = new Map<String,Object>();
        List<String> messages = new List<String>();
        Boolean isCancellable = true;
        String newLine = '\n'+'\u2022 ';

        messages.add('\n');
        if (Constants.getCaseRecordTypes('SwitchIn').values().contains(caseRecord.RecordTypeId)) {
            Date effectiveDate = caseRecord.EffectiveDate__c;
            if (effectiveDate != null && Date.today().daysBetween(effectiveDate) < 5) {
                isCancellable = false;
                messages.add(Label.CancellationTooLate);
            }
        }
        if (Constants.getCaseRecordTypes('TechnicalDataChange').values().contains(caseRecord.RecordTypeId)) {

        }
        if (Constants.getCaseRecordTypes('Termination').values().contains(caseRecord.RecordTypeId)) {
            Date today = System.today();
            Date effectiveDate = caseRecord.EffectiveDate__c;
            if(effectiveDate != null && today>effectiveDate){
                isCancellable = false;
                messages.add(Label.CancellationTooLate);
            }

        }
        if (Constants.getCaseRecordTypes('SwitchOut').values().contains(caseRecord.RecordTypeId)) {
            if (String.isNotBlank(requestId)) {
                //Boolean validityCheck = switchOutUTL.logicValidityCheck(requestId);

                Map<String,Boolean > validityCheck = switchOutUTL.logicValidityCheck(requestId);
                System.debug('valiity'+validityCheck);
                if(!validityCheck.isEmpty()){
                    if(validityCheck.get('logic_1') && !(validityCheck.get('allLogic'))){
                        isCancellable = false;
                        messages.add(Label.cancelledAnymore);
                    }
                }
                /*if (validityCheck != null) {
                    if (!validityCheck) {
                        isCancellable = false;
                        messages.add(Label.CancellationTooLate);
                    }
                }*/
            }
        }
        if (Constants.getCaseRecordTypes('ProductChange').values().contains(caseRecord.RecordTypeId)) {

        }
        if (Constants.getCaseRecordTypes('BillingProfileChange').values().contains(caseRecord.RecordTypeId)) {

        }
        if (Constants.getCaseRecordTypes('Activation').values().contains(caseRecord.RecordTypeId)) {

        }
        if (Constants.getCaseRecordTypes('Reactivation').values().contains(caseRecord.RecordTypeId)) {

        }
        if (Constants.getCaseRecordTypes('Transfer').values().contains(caseRecord.RecordTypeId)) {

        }
        if (Constants.getCaseRecordTypes('ContractMerge').values().contains(caseRecord.RecordTypeId)) {

        }

        response.put('isCancellable',isCancellable);
        response.put('messages',String.join(messages,newLine));
        System.debug('map response case '+response);
        return response;
    }
}