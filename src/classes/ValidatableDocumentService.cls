/**
 * @author Baba Goudiaby
 * @version 1.0
 * @description Apex class for all operations of business logic and data manipulation about Validatable Document
 * @history
 * 01/04/2019: Original - BG
 */

public with sharing class ValidatableDocumentService {
    private static ValidatableDocumentQueries validatableDocumentQuery = ValidatableDocumentQueries.getInstance();

    public static ValidatableDocumentService getInstance() {
        return new ValidatableDocumentService();
    }

    /**
     * Get a Map of Validatable documents object with Document Bundle item key from Dossier
     *
     * @param dossierId
     *
     * @return Map Validatable document
     */
    public Map<String, ValidatableDocument__c> getMapDocumentItemBundleValidatableDoc(String dossierId) {
        Map<String, ValidatableDocument__c> mapDocumentItemBundleValidatableDoc = new Map<String, ValidatableDocument__c>();
        for (ValidatableDocument__c validatableDocument : validatableDocumentQuery.findByDossierId(dossierId)) {
            if (validatableDocument.DocumentBundleItem__c != null) {
                mapDocumentItemBundleValidatableDoc.put(
                        String.valueOf(validatableDocument.DocumentBundleItem__c), validatableDocument
                );
            }
        }
        return mapDocumentItemBundleValidatableDoc;
    }
}