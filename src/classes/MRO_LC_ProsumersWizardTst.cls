/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 12.03.20.
 */

@IsTest
public with sharing class MRO_LC_ProsumersWizardTst {

    private static final String DOSSIER_CHANNEL = 'Back Office Sales';
    private static final String DOSSIER_ORIGIN = 'Email';

    @TestSetup
    private static void setup() {

        //Insert Sequencer
        Sequencer__c sequencer = new Sequencer__c(Sequence__c = 1, SequenceLength__c = 1, Type__c = 'CustomerCode');
        insert sequencer;


        //Insert Accounts
        List<Account> listAccount = new List<Account>();
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        listAccount.add(personAccount);

        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.BusinessType__c = 'NGO';
        listAccount.add(businessAccount);

        Account distributorAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        distributorAccount.Name = 'DistributorAccount1';
        distributorAccount.BusinessType__c = 'Real estate';
        distributorAccount.IsDisCoENEL__c = true;
        distributorAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        listAccount.add(distributorAccount);

        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'TraderAccount1';
        accountTrader.VATNumber__c = 'RO3911430';
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.Key__c = '1324433';
        listAccount.add(accountTrader);
        insert listAccount;

        ContractAccount__c contractAccount = new ContractAccount__c();
        insert contractAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = businessAccount.Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, personAccount.Id, contact.Id).build();
        insert customerInteraction;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Account__c = businessAccount.Id;
        servicePoint.Trader__c = accountTrader.Id;
        servicePoint.ENELTEL__c = '123456789';
        servicePoint.Distributor__c = distributorAccount.Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = businessAccount.Id;
        insert contract;

        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = businessAccount.Id;
        billingProfile.PaymentMethod__c = 'Direct Debit';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;

        ServiceSite__c serviceSite = new ServiceSite__c();
        insert serviceSite;

        NE__Product__c commercialProduct = new NE__Product__c(ProductTerm__c = 12);
        insert commercialProduct;

        Product2 product = MRO_UTL_TestDataFactory.product2().build();
        product.CommercialProduct__c = commercialProduct.Id;
        insert product;

        List<Supply__c> supplyList = new List<Supply__c>();
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Account__c = businessAccount.Id;
            supply.ServicePoint__c = servicePoint.Id;
            supply.ServiceSite__c = serviceSite.Id;
            supply.CompanyDivision__c = companyDivision.Id;
            supply.Product__c = product.Id;
            supplyList.add(supply);
        }
        insert supplyList;

        ProcessDateThresholds__c ProcessDateThresholdsSettings = ProcessDateThresholds__c.getOrgDefaults();
        insert ProcessDateThresholdsSettings;

        Dossier__c genericRequest = new Dossier__c(
                RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(MRO_UTL_Constants.GENERIC_REQUEST).getRecordTypeId(),
                Channel__c = DOSSIER_CHANNEL,
                Origin__c = DOSSIER_ORIGIN
        );
        insert genericRequest;

    }

    static Account getAccount(){
        return [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
    }

    static Dossier__c getDossier(String recordTypeName){
        return [
                SELECT Id
                FROM Dossier__c WHERE RecordType.DeveloperName = :recordTypeName
                LIMIT 1
        ];
    }

    static Interaction__c getInteraction() {
        return [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
    }

    static Supply__c getSupply() {
        return [
                SELECT Id
                FROM Supply__c
                LIMIT 1
        ];
    }

    static ContractAccount__c getContractAccount() {
        return [
                SELECT Id
                FROM ContractAccount__c
                LIMIT 1
        ];
    }

    static Map<String, Object> initializeFrom(Map<String, String> inputJSON) {
        Object response = TestUtils.exec(
                'MRO_LC_ProsumersWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        System.assertNotEquals(null, result.get('dossierId'));
        System.assertNotEquals(null, result.get('dossier'));
        System.assertNotEquals(null, result.get('opportunityId'));
        Dossier__c dossier = (Dossier__c)result.get('dossier');
        System.assertEquals(DOSSIER_ORIGIN, dossier.Origin__c);
        System.assertEquals(DOSSIER_CHANNEL, dossier.Channel__c);
        return result;
    }

    @IsTest
    static Map<String, Object> initializeFromInteractionTest() {
        Account account = getAccount();
        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'interactionId' => getInteraction().Id
        };
        return initializeFrom(inputJSON);
    }

    static Map<String, Object> initializeFromGenericRequest() {
        Account account = getAccount();
        Dossier__c genericDossier = getDossier(MRO_UTL_Constants.GENERIC_REQUEST);
        Map<String, String> result = new Map<String, String>{
                'accountId' => account.Id,
                'genericRequestDossierId' => genericDossier.Id
        };
        return initializeFrom(result);
    }

    static Map<String, Object> initialize(Map<String, Object> inputJSON) {
        Object response = TestUtils.exec(
                'MRO_LC_ProsumersWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        return result;
    }

    static Map<String, Object> createCase(Map<String, Object> input) {
        Map<String, Object> requestParams = new Map<String, Object>{
                'dossierId' => (String)input.get('dossierId'),
                'opportunityId' => (String)input.get('opportunityId'),
                'supplyId' => (String)input.get('supplyId'),
                'originSelected' => DOSSIER_ORIGIN,
                'channelSelected' => DOSSIER_CHANNEL
        };
        Object response = TestUtils.exec(
                'MRO_LC_ProsumersWizard', 'createCase', requestParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Case caseRecord = (Case)result.get('caseRecord');
        System.assertNotEquals(null, caseRecord);
        System.assertEquals(requestParams.get('supplyId'), caseRecord.Supply__c);
        return result;
    }

    static void createConsumptionTest(Map<String, Object> input){
        update new Case(Id = (String)input.get('caseId'), AvailablePower__c = 12);

        Object response = TestUtils.exec(
            'MRO_LC_ProsumersWizard', 'createConsumption', input, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    static void setChannelAndOriginTest(Map<String, Object> input) {
        Map<String, Object> requestParams = new Map<String, Object>{
                'dossierId' => (String)input.get('dossierId'),
                'opportunityId' => (String)input.get('opportunityId'),
                'originSelected' => DOSSIER_ORIGIN,
                'channelSelected' => DOSSIER_CHANNEL
        };
        Object response = TestUtils.exec(
                'MRO_LC_ProsumersWizard', 'setChannelAndOrigin', requestParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    static Map<String, Object> updateContractInformationTest(Map<String, Object> input) {
        Object response = TestUtils.exec(
                'MRO_LC_ProsumersWizard', 'updateContractInformation', input, true);
        Map<String, Object> result = (Map<String, Object>) response;
        List<Contract> contracts = [SELECT Id, ContractTerm FROM Contract];
        System.assertEquals(false, result.get('error'));
        System.assertEquals(2, contracts.size());
        System.assertEquals(contracts[0].ContractTerm, contracts[1].ContractTerm);
/*
        Integer prosumersSuppliesCount = [SELECT COUNT() FROM Supply__c WHERE Contract__r.Prosumer__c = TRUE];
        System.assertEquals(1, prosumersSuppliesCount);*/
        return result;
    }

    static Map<String, Object> handleContractAccountSelectionTest(Map<String, Object> input) {
        Object response = TestUtils.exec(
                'MRO_LC_ProsumersWizard', 'handleContractAccountSelection', input, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        return result;
    }

    @IsTest
    static void saveProcessTest() {
        Test.startTest();

        //init
        Map<String, Object> initResult = initializeFromGenericRequest();

        //create dossier and opportunity
        Map<String, String> initializeInput = new Map<String, String>{
                'accountId' => (String)initResult.get('accountId'),
                'dossierId' => (String)initResult.get('dossierId'),
                'opportunityId' => (String)initResult.get('opportunityId')
        };
        Map<String, Object> input = initialize(initializeInput);

        //set channel/origin
        setChannelAndOriginTest(input);

        //create case
        String supplyId = getSupply().Id;
        input.put('supplyId', supplyId);
        Map<String, Object> createCaseResult = createCase(input);
        Case caseRecord = (Case)createCaseResult.get('caseRecord');

        //initialize with case
        initializeInput.put('genericRequestDossierId', null);
        initializeInput.put('caseId', caseRecord.Id);
        input = initialize(initializeInput);

        //update contract information
        Map<String, Object> contractInput = new Map<String, Object>{
                'opportunityId' => input.get('opportunityId'),
                'supplyId' => supplyId,
                'contractType' => createCaseResult.get('contractType'),
                'contractName' => 'Prosumers',
                'salesChannel' => DOSSIER_CHANNEL,
                'contractStartDate' => Date.today().addDays(10)
        };
        Map<String, Object> createContractResult = updateContractInformationTest(contractInput);
        Contract contractRecord = (Contract)createContractResult.get('contract');

        //contract account selection
        Map<String, Object> contractAccountInput = new Map<String, Object>{
                'caseId' => caseRecord.Id,
                'contractAccountId' => getContractAccount().Id
        };
        handleContractAccountSelectionTest(contractAccountInput);

        //create consumption
        Map<String, Object> consumptionInput = new Map<String, Object>{
                'caseId' => caseRecord.Id,
                'contractId' => contractRecord.Id
        };
        createConsumptionTest(consumptionInput);

        //save
        Map<String, Object> saveInput = new Map<String, Object>{
                'caseId' => caseRecord.Id,
                'opportunityId' => input.get('opportunityId'),
                'dossierId' => input.get('dossierId'),
                'isDraft' => false
        };
        Object response = TestUtils.exec(
                'MRO_LC_ProsumersWizard', 'saveProcess', saveInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }
}