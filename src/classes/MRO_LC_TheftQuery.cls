/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 23.09.20.
 */

public with sharing class MRO_LC_TheftQuery extends ApexServiceLibraryCnt {

    public with sharing class listTheft extends AuraCallable {
        public override Object perform(final String jsonInput) {
            TheftRequestParam params = (TheftRequestParam) JSON.deserialize(jsonInput, TheftRequestParam.class);
            if (String.isEmpty(params.startDate) || String.isEmpty(params.endDate) || (String.isEmpty(params.servicePointCode) && String.isEmpty(params.servicePointId))) {
                throw new WrtsException('Missing request parameters');
            }

            if (String.isEmpty(params.servicePointCode)) {
                ServicePoint__c servicePoint = MRO_QR_ServicePoint.getInstance().getById(params.servicePointId);
                params.servicePointCode = servicePoint.Code__c;
            }

            if (MRO_SRV_SapQueryTheftCallOut.checkSapEnabled()) {
                return setDocuments(MRO_SRV_SapQueryTheftCallOut.getInstance().getList(params));
            }
            return listDummyTheft(params);
        }
    }

    public with sharing class getFileFromDMS extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, String> response = new Map<String, String>();

            String documentId = params.get('documentId');
            String objectKey = params.get('objectKey');
            String fileMetadataId = params.get('fileMetadataId');

            System.debug(fileMetadataId);

            if(fileMetadataId != null){
                return MRO_LC_FileRelatedListCnt.retrieveFromDMS(fileMetadataId);
            }
            System.debug('New Filemetadata');

            MRO_QR_FileMetadata qrFileMetadata = MRO_QR_FileMetadata.getInstance();
            Id idDocType = qrFileMetadata.getDocumentTypes('INVOICE')[0].Id;
            FileMetadata__c fileMetadata = new FileMetadata__c(
                    Title__c = documentId,
                    TemplateClass__c = 'ArchiveProcessing',
                    TemplateCode__c = 'ArchiveProduction',
                    TemplateVersion__c = 'v1',
                    DocumentType__c = idDocType,
                    ExternalRequestId__c = objectKey,
                    DiscoFileId__c = documentId
            );
            MRO_SRV_DatabaseService.getInstance().insertSObject(fileMetadata);
            response.put('fileMetadataId', fileMetadata.Id);

            return response;
        }
    }

    private static Object setDocuments(TheftResponse response){
        if(response == null){
            return null;
        }
        if(response.documents.isEmpty()){
            return response;
        }
        List<String> documentIds = new List<String>();
        for(TheftDocument document : response.documents){
            documentIds.add(document.documentId);
        }
        List<FileMetadata__c> fileMetadataList = MRO_QR_FileMetadata.getInstance().findByTitles(documentIds);
        List<String> contentVersionIdList = new List<String>();
        Map<String, String> contentVersionIdByFmdTitleMap = new Map<String, String>();
        if(!fileMetadataList.isEmpty()){
            for(FileMetadata__c fileMetadata : fileMetadataList){
                contentVersionIdByFmdTitleMap.put(fileMetadata.Title__c, fileMetadata.DocumentId__c);
                contentVersionIdList.add(fileMetadata.DocumentId__c);
            }
        }
        List<ContentVersion> contentVersions = MRO_QR_ContentVersion.getInstance().findById(contentVersionIdList);
        Map<String, String> contentDocumentByContentVersionMap = new Map<String, String>();
        for(ContentVersion contentVersion: contentVersions){
            contentDocumentByContentVersionMap.put(contentVersion.Id, contentVersion.ContentDocumentId);
        }
        for(TheftDocument document : response.documents){
            String contentVersionId = contentVersionIdByFmdTitleMap.get(document.documentId);
            if(contentVersionId != null){
                document.contentDocumentId = contentDocumentByContentVersionMap.get(contentVersionId);
            }
        }
        return response;
    }

    public static TheftResponse listDummyTheft(TheftRequestParam params) {
        TheftResponse theftResponse = new TheftResponse();

        for (Integer i = 0; i < 10; i++) {
            theftResponse.documents.add(new TheftDocument(
                    'REF10000' + i,
                    'PROC200' + i,
                    (Math.mod(i, 2) == 0 ? 'Active' : 'Inactive'),
                    'DIS300000' + i,
                    'SP400000' + i,
                    (Math.mod(i, 2) == 0 ? 'Type 1' : 'Type 2'),
                    'DOC500000' + i,
                    (Math.mod(i, 2) == 0 ? 'Type 3' : 'Type 4'),
                    (Math.mod(i, 2) == 0 ? 'Type 5' : 'Type 6'),
                    'OK200' + i
            ));
            theftResponse.processes.add(new TheftProcess(
                    'REF10000' + i,
                    'PROC200' + i,
                    (Math.mod(i, 2) == 0 ? 'Active' : 'Inactive'),
                    'DIS300000' + i,
                    'SP400000' + i,
                    (Math.mod(i, 2) == 0 ? 'Type 1' : 'Type 2'),
                    (Math.mod(i, 2) == 0 ? 'Type 3' : 'Type 4'),
                    Date.today().addDays((i + 1)),
                    (Math.mod(i, 2) == 0 ? 'Type 5' : 'Type 6'),
                    'CN500000' + i,
                    Date.today().addDays((i + 10) * -1),
                    Date.today().addDays((i + 10)),
                    'AC60000' + i,
                    10 + i,
                    100 + i,
                    200 + i,
                    300 + i,
                    400 + i,
                    500 + i
            ));
        }
        return theftResponse;
    }

    public with sharing class listSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SuppliesRequestParam params = (SuppliesRequestParam) JSON.deserialize(jsonInput, SuppliesRequestParam.class);
            if (String.isEmpty(params.serviceSiteId)) {
                throw new WrtsException('Missing request parameters');
            }
            return MRO_QR_Supply.getInstance().getAllSuppliesByServiceSiteId(params.serviceSiteId);
        }
    }

    public class SuppliesRequestParam {
        @AuraEnabled
        public String serviceSiteId { get; set; }
    }

    public class TheftRequestParam {
        @AuraEnabled
        public String servicePointId { get; set; }
        @AuraEnabled
        public String servicePointCode { get; set; }
        @AuraEnabled
        public String startDate { get; set; }
        @AuraEnabled
        public String endDate { get; set; }

        public TheftRequestParam(String servicePointId, String servicePointCode, String startDate, String endDate) {
            this.servicePointId = servicePointId;
            this.servicePointCode = servicePointCode;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }

    public class TheftResponse {
        @AuraEnabled
        public List<TheftDocument> documents { get; set; }
        @AuraEnabled
        public List<TheftProcess> processes { get; set; }

        public TheftResponse() {
            this.documents = new List<MRO_LC_TheftQuery.TheftDocument>();
            this.processes = new List<MRO_LC_TheftQuery.TheftProcess>();
        }
    }

    public class TheftDocument {
        @AuraEnabled
        public String processRef { get; set; }
        @AuraEnabled
        public String processId { get; set; }
        @AuraEnabled
        public String status { get; set; }
        @AuraEnabled
        public String distributorId { get; set; }
        @AuraEnabled
        public String servicePointCode { get; set; }
        @AuraEnabled
        public String requestType { get; set; }
        @AuraEnabled
        public String documentId { get; set; }
        @AuraEnabled
        public String documentType { get; set; }
        @AuraEnabled
        public String objectType { get; set; }
        @AuraEnabled
        public String objectKey { get; set; }
        @AuraEnabled
        public String contentDocumentId { get; set; }

        public TheftDocument() {
        }

        public TheftDocument(String processRef, String processId, String status, String distributorId,
                String servicePointCode, String requestType, String documentId, String documentType,
                String objectType, String objectKey) {
            this.processRef = processRef;
            this.processId = processId;
            this.status = status;
            this.distributorId = distributorId;
            this.servicePointCode = servicePointCode;
            this.requestType = requestType;
            this.documentId = documentId;
            this.documentType = documentType;
            this.objectType = objectType;
            this.objectKey = objectKey;
        }

        public TheftDocument(Map<String, String> stringMap) {
            this.processRef = stringMap.get('ProcessRef');
            this.processId = stringMap.get('ProcessId');
            this.status = stringMap.get('Status');
            this.distributorId = stringMap.get('DistributorId');
            this.servicePointCode = stringMap.get('ServicePointCode');
            this.requestType = stringMap.get('RequestType');
            this.documentId = stringMap.get('DocumentId');
            this.documentType = stringMap.get('Type');
            this.objectType = stringMap.get('ObjectType');
            this.objectKey = stringMap.get('ObjectKey');
        }
    }

    public class TheftProcess {
        @AuraEnabled
        public String processRef { get; set; }
        @AuraEnabled
        public String processId { get; set; }
        @AuraEnabled
        public String status { get; set; }
        @AuraEnabled
        public String distributorId { get; set; }
        @AuraEnabled
        public String servicePointCode { get; set; }
        @AuraEnabled
        public String requestType { get; set; }
        @AuraEnabled
        public String odlType { get; set; }
        @AuraEnabled
        public Date executionDate { get; set; }
        @AuraEnabled
        public String typeRecoveral { get; set; }
        @AuraEnabled
        public String caseNumber { get; set; }
        @AuraEnabled
        public Date dacFrom { get; set; }
        @AuraEnabled
        public Date dacTo { get; set; }
        @AuraEnabled
        public String alteCor { get; set; }
        @AuraEnabled
        public Double activeEnergyF2 { get; set; }
        @AuraEnabled
        public Double activeEnergyF3 { get; set; }
        @AuraEnabled
        public Double indReactiveEnergy { get; set; }
        @AuraEnabled
        public Double indTriplePrice { get; set; }
        @AuraEnabled
        public Double capReactiveEnergy { get; set; }
        @AuraEnabled
        public Double capTriplePrice { get; set; }

        public TheftProcess() {
        }

        public TheftProcess(String processRef, String processId, String status, String distributorId,
                String servicePointCode, String requestType, String odlType, Date executionDate,
                String typeRecoveral, String caseNumber, Date dacFrom, Date dacTo, String alteCor,
                Double activeEnergyF2, Double activeEnergyF3, Double indReactiveEnergy,
                Double indTriplePrice, Double capReactiveEnergy, Double capTriplePrice) {
            this.processRef = processRef;
            this.processId = processId;
            this.status = status;
            this.distributorId = distributorId;
            this.servicePointCode = servicePointCode;
            this.requestType = requestType;
            this.odlType = odlType;
            this.executionDate = executionDate;
            this.typeRecoveral = typeRecoveral;
            this.caseNumber = caseNumber;
            this.dacFrom = dacFrom;
            this.dacTo = dacTo;
            this.alteCor = alteCor;
            this.activeEnergyF2 = activeEnergyF2;
            this.activeEnergyF3 = activeEnergyF3;
            this.indReactiveEnergy = indReactiveEnergy;
            this.indTriplePrice = indTriplePrice;
            this.capReactiveEnergy = capReactiveEnergy;
            this.capTriplePrice = capTriplePrice;
        }

        public TheftProcess(Map<String, String> stringMap) {
            this.processRef = stringMap.get('ProcessRef');
            this.processId = stringMap.get('ProcessId');
            this.status = stringMap.get('Status');
            this.distributorId = stringMap.get('DistributorId');
            this.servicePointCode = stringMap.get('ServicePointCode');
            this.requestType = stringMap.get('RequestType');
            this.odlType = stringMap.get('ODLType');
            this.typeRecoveral = stringMap.get('TypeRecoveral');
            this.caseNumber = stringMap.get('CaseNumber');
            this.alteCor = stringMap.get('AlteCor');
            this.activeEnergyF2 = MRO_SRV_SapQueryTheftCallOut.parseDouble(stringMap.get('ActiveEnergyF2'));
            this.activeEnergyF3 = MRO_SRV_SapQueryTheftCallOut.parseDouble(stringMap.get('ActiveEnergyF3'));
            this.indReactiveEnergy = MRO_SRV_SapQueryTheftCallOut.parseDouble(stringMap.get('IndReactiveEnergy'));
            this.indTriplePrice = MRO_SRV_SapQueryTheftCallOut.parseDouble(stringMap.get('IndTriplePrice'));
            this.capReactiveEnergy = MRO_SRV_SapQueryTheftCallOut.parseDouble(stringMap.get('CapReactiveEnergy'));
            this.capTriplePrice = MRO_SRV_SapQueryTheftCallOut.parseDouble(stringMap.get('CapTriplePrice'));

            try {
                this.executionDate = MRO_SRV_SapQueryTheftCallOut.parseDate(stringMap.get('ExecutionDate'));
                this.dacFrom = MRO_SRV_SapQueryTheftCallOut.parseDate(stringMap.get('DacFrom'));
                this.dacTo = MRO_SRV_SapQueryTheftCallOut.parseDate(stringMap.get('DacTo'));
            } catch (Exception e) {
                System.debug('Invalid Date TheftProcessDTO ' + stringMap.get('ExecutionDate') + ' ' + stringMap.get('DacFrom') + ' ' + stringMap.get('DacTo'));
            }
        }
    }
}