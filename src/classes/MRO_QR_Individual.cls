/**
 * Created by napoli on 19/11/2019.
 */

public with sharing class MRO_QR_Individual {

    public static MRO_QR_Individual getInstance() {
        return new MRO_QR_Individual();
    }

    /**
     * Query to get a list of individuals for the given National Identity Number
     * @param NINumber National Identity Number
     * @return A list of Individuals objects
     */
    public List<Individual> findIndividualsByNINumber(String NINumber) {
        return [
                SELECT Id, Name, FirstName, LastName, NationalIdentityNumber__c, ForeignCitizen__c, Gender__c, BirthCity__c, BirthDate,IDDocEmissionDate__c,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        IDDocValidityDate__c,IDDocumentNr__c,IDDocumentType__c,IDDocumentSeries__c,IDDocIssuer__c,TechnicalDetails__c
                //FF Customer Creation - Pack1/2 - Interface Check
                FROM Individual
                WHERE NationalIdentityNumber__c =:NINumber
        ];
    }

    public Individual findById(String individualId) {
        List<Individual> individualList = [
                SELECT Id, Name, FirstName, LastName, NationalIdentityNumber__c, ForeignCitizen__c, Gender__c, BirthCity__c, BirthDate,IDDocEmissionDate__c,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        IDDocValidityDate__c,IDDocumentNr__c,IDDocumentType__c,IDDocumentSeries__c,IDDocIssuer__c,TechnicalDetails__c
                //FF Customer Creation - Pack1/2 - Interface Check
                FROM Individual
                WHERE Id = :individualId
        ];
        if (individualList.isEmpty()) {
            return null;
        }
        return individualList.get(0);
    }
}