/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   set 10, 2020
 * @desc    
 * @history 
 */

global with sharing class MRO_ACT_PrintAction implements wrts_prcgvr.Interfaces_1_0.IApexAction {

    public Object execute(Object param) {
        System.debug('input : ' + param);

        Map<String, Object> paramsMap = (Map<String, Object>) param;
        PrintAction__c printAction = (PrintAction__c) paramsMap.get('sender');
        String method = (String) paramsMap.get('method');
        System.debug('printAction: ' + printAction);

        switch on method {
            when 'resubmitCallout' {
                this.resubmitCallout(printAction);
            }
            when else {
                throw new WrtsException('Unrecognized method: ' + method);
            }
        }

        return null;
    }

    private void resubmitCallout(PrintAction__c printAction) {
        if (FeatureManagement.checkPermission('MRO_ResubmitCallouts')) {
            wrts_prcgvr__AsyncJob__c pendingCallout = MRO_QR_AsyncJob.getInstance().getPendingCalloutJobByRecordId(printAction.Id);
            if (pendingCallout != null) {
                delete pendingCallout;
            }
            PrintAction__c pa = new PrintAction__c(Id = printAction.Id);
            MRO_UTL_ApexContext.FORCE_CALLOUTS_RETRIGGERING = true;
            update pa;
        } else {
            throw new WrtsException(String.format(Label.PermissionTo, new List<String>{'resubmit callouts'}));
        }
    }

}