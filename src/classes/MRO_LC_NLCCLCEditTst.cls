/**
 * @author  Luca Ravicini
 * @since   Jun 2, 2020
 * @desc   Test for MRO_LC_NLCCLCEdit class
 *
 */

@IsTest
private class MRO_LC_NLCCLCEditTst {
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();

    @testSetup
    static void setup() {
        String dossierRequestType = constantsUtl.REQUEST_TYPE_NLC_CLC_DATA_CHANGE;
        String dossierDraftStatus = constantsUtl.DOSSIER_STATUS_DRAFT;

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 1000.0,SequenceLength__c = 7.0);
        insert sequencer;
        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder1 =  MRO_UTL_TestDataFactory.account().personAccount();
        Account acc1 = AccountBuilder1.build();
        acc1.FirstName = 'TEST';

        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder2 =  MRO_UTL_TestDataFactory.account().personAccount();
        Account acc2 = accountBuilder2.build();

        Database.SaveResult[] lsr = Database.insert(new Account[]{acc1, acc2 }, false);

        Dossier__c dossierWithoutOpenedCases = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(dossierRequestType).build();
        dossierWithoutOpenedCases.Status__c = dossierDraftStatus;
        dossierWithoutOpenedCases.Account__c = acc1.Id;
        insert dossierWithoutOpenedCases;

        Dossier__c dossierWithOpenedCases = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.CHANGE).setRequestType(dossierRequestType).build();
        dossierWithOpenedCases.Status__c = dossierDraftStatus;
        insert dossierWithOpenedCases;

        Case case1 = MRO_UTL_TestDataFactory.caseRecordBuilder().newCase().build();
        case1.Dossier__c = dossierWithOpenedCases.Id;
        case1.AccountId = acc1.Id;

        CompanyDivision__c companyDivision1 = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision1;

        ServiceSite__c serviceSite = MRO_UTL_TestDataFactory.serviceSite().createServiceSite().build();
        serviceSite.Account__c = acc1.Id;
        insert  serviceSite;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePointGas().build();
        servicePoint.Account__c = acc1.Id;
        servicePoint.Code__c = '012345678987654';
        insert  servicePoint;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().build();
        supply.ServiceSite__c = serviceSite.Id;
        supply.Account__c = acc1.Id;
        supply.ServicePoint__c = servicePoint.Id;
        supply.CompanyDivision__c = companyDivision1.Id;
        insert supply;
        case1.Supply__c = supply.Id;
        insert case1;

        servicePoint.CurrentSupply__c = supply.Id;
        update servicePoint;

        Interaction__c interaction1 = MRO_UTL_TestDataFactory.interaction().build();
        insert interaction1;

    }
    @IsTest
    static void getCaseById() {
        String caseId = [SELECT Id, Dossier__c FROM Case LIMIT 1].Id;
        Test.startTest();
        Case myCase = MRO_LC_NLCCLCEdit.getCaseById(caseId);
        Test.stopTest();
        System.assertEquals(caseId, myCase.Id);
    }

    @IsTest
    static void getCaseByIdError() {
        String caseId = '000';
        Test.startTest();
        Case myCase = MRO_LC_NLCCLCEdit.getCaseById(caseId);
        Test.stopTest();
        System.assertEquals(null, myCase);
    }

    @IsTest
    static void getSupplyById() {
        Id supplyId = [SELECT Id FROM Supply__c LIMIT 1].Id;

        Map<String, String> inputJSON = new Map<String, String>{
                'recordId' => supplyId
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'getSupplyById', inputJSON, true);
            Test.stopTest();
            System.assertEquals(false, response.get('error'));
            Supply__c responseSupply = (Supply__c)response.get('supply');
            System.assertEquals(supplyId, responseSupply.Id);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void getSupplyByIdError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'recordId' => '00'
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'getSupplyById', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void checkExistingCaseForSupply() {
        Id supplyId = [SELECT Id FROM Supply__c LIMIT 1].Id;
        Id dossierId = [SELECT Id FROM Dossier__c LIMIT 1].Id;

        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => supplyId,
                'dossierId' => dossierId
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'checkExistingCaseForSupply', inputJSON, true);
            Test.stopTest();
            System.assertEquals(false, response.get('error'));
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void checkExistingCaseForSupplyWithCase() {
        Id supplyId = [SELECT Id FROM Supply__c LIMIT 1].Id;
        Id dossierId = [SELECT Id FROM Dossier__c LIMIT 1].Id;
        Case myCase = [SELECT Id FROM Case LIMIT 1];
        myCase.Dossier__c = dossierId;
        myCase.Supply__c = supplyId;
        update myCase;

        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => supplyId,
                'dossierId' => dossierId
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'checkExistingCaseForSupply', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('message') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void checkExistingCaseForSupplyError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => '00'
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'checkExistingCaseForSupply', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }


    @IsTest
    static void createNewCase() {
        Supply__c supply = [SELECT Id, Account__c, ServiceSite__c, ServicePoint__r.Code__c, CompanyDivision__c FROM Supply__c LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];

        String customerNotes = 'my notes';

        Map<String, String> inputJSON = new Map<String, String>{
                'nlcClcCode' => 'nlcClcCode',
                'oldNlcClc' => 'oldNlcClc',
                'typeLabel' => 'typeLabel',
                'accountId' => supply.Account__c,
                'dossierId' => dossier.Id,
                'supplyId' => supply.Id,
                'serviceSiteId' => supply.ServiceSite__c,
                'companyDivisionId' => supply.CompanyDivision__c,
                'servicePointCode' => supply.ServicePoint__r.Code__c,
                'customerNotes' => customerNotes
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'createNewCase', inputJSON, true);
            System.assertEquals(true, response.get('error') == true);

            Set<String> codes = new Set<String>();
            codes.add(supply.ServicePoint__r.Code__c);
            List<ServicePoint__c> servicePoints =  [SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,CurrentSupply__r.ContractAccount__r.BillingProfile__c,Code__c,CurrentSupply__r.Account__c,
            CurrentSupply__r.CompanyDivision__c,CurrentSupply__r.RecordType.DeveloperName,ConsumptionCategory__c,FlowRate__c, ATRExpirationDate__c, IsNewConnection__c
            FROM ServicePoint__c
            WHERE Code__c IN :codes];
            delete servicePoints;

            response = (Map<String, Object>)TestUtils.exec(
                'MRO_LC_NLCCLCEdit', 'createNewCase', inputJSON, true);
            System.assertEquals(true, response.get('error') == false);
            Case responseCase = (Case)response.get('case');
            System.assert( responseCase != null);

            Test.stopTest();

        } catch (Exception exc) {

        }
    }

    @IsTest
    static void createNewCaseError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => '00'
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'createNewCase', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void UpdateCaseNlcClc() {
        String caseId = [SELECT Id FROM Case LIMIT 1].Id;

        String customerNotes = 'my notes11';
        String typeLabel = 'typeLabel00';
        String nlcClcCode = 'nlcClcCode00';

        Map<String, String> inputJSON = new Map<String, String>{
                'recordId' => caseId,
                'nlcClcCode' => nlcClcCode,
                'typeLabel' => typeLabel,
                'customerNotes' => customerNotes
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'UpdateCaseNlcClc', inputJSON, true);
            Test.stopTest();
            System.assertEquals(false, response.get('error'));
            Case responseCase = (Case)response.get('case');
            System.assert( responseCase != null);
            System.assertEquals(nlcClcCode, responseCase.NLCCLC__c);
            System.assertEquals(customerNotes, responseCase.CustomerNotes__c);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void UpdateCaseNlcClcWithExistingCode() {
        String customerNotes = 'my notes11';
        String typeLabel = 'typeLabel00';
        String nlcClcCode = 'nlcClcCode00';

        String caseId = [SELECT Id FROM Case LIMIT 1].Id;
        ServiceSite__c serviceSite = [SELECT Id, NLC__c FROM ServiceSite__c LIMIT 1];
        serviceSite.NLC__c = nlcClcCode;
        update serviceSite;

        Map<String, String> inputJSON = new Map<String, String>{
                'recordId' => caseId,
                'nlcClcCode' => nlcClcCode,
                'typeLabel' => typeLabel,
                'customerNotes' => customerNotes
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'UpdateCaseNlcClc', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert( response.get('errorMsg') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void UpdateCaseNlcClcError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'recordId' => '00'
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'UpdateCaseNlcClc', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void isNlcClcAlreadyUsedTest() {
        String nlcClcCode = 'nlcClcCode00';
        ServiceSite__c serviceSite = [SELECT Id, NLC__c FROM ServiceSite__c LIMIT 1];


        try {
            Test.startTest();
            MRO_LC_NLCCLCEdit.isNlcClcAlreadyUsed(serviceSite.Id, nlcClcCode);
            /*Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_NLCCLCEdit', 'isNlcClcAlreadyUsed', inputJSON, true);*/
            Test.stopTest();
            /*System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);*/
        } catch (Exception exc) {

        }
    }

}