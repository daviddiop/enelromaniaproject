public with sharing class MRO_QR_ReminderQueries {

	private static MRO_QR_ReminderQueries instance = null;

	public static MRO_QR_ReminderQueries getInstance() {
		if(instance == null) instance = new MRO_QR_ReminderQueries();
		return instance;
	}

	public List<CustomReminder__c> getByListIds(List<ID> IDs){
		return [SELECT ID FROM CustomReminder__c WHERE ID IN :IDs];
	}

	/* public List<CustomReminder__c> getReminderToDay(){
		 return [SELECT ID, Code__c, RecordId__c, Status__c FROM CustomReminder__c WHERE Expiration_Date__c = TODAY];
	 }*/

	public List<CustomReminderConfiguration__mdt> getCfgReminderByCode(List<String> codeList){
		return [SELECT ID, Code__c, ReminderHours__c, ApexClass__c FROM CustomReminderConfiguration__mdt WHERE Code__c IN :codeList];
	}

	public List<CustomReminder__c> getById(String recordId){
		return [SELECT Id,RecordId__c FROM CustomReminder__c WHERE RecordId__c =: recordId ];
	}
}