public with sharing class ServicePointQueries {

    public static ServicePointQueries getInstance() {
        return new ServicePointQueries();
    }

    public List<ServicePoint__c> getPointsWithSupplies(Set<Id> ids) {
        return [
                SELECT Id, Account__c, CurrentSupply__c, CurrentSupply__r.Status__c, CurrentSupply__r.CompanyDivision__c, CurrentSupply__r.Account__c,
                        (SELECT Id, Account__c, Status__c FROM Supplies__r ORDER BY ActivationDate__c DESC NULLS FIRST)
                FROM ServicePoint__c
                WHERE Id in:ids
        ];
    }

    public List<ServicePoint__c> getListServicePoints(String pointCode) {
        return [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,
                        CurrentSupply__r.CompanyDivision__c
                FROM ServicePoint__c
                WHERE Code__c = :pointCode
                LIMIT 1
        ];
    }

    public List<ServicePoint__c> getListServicePointsByAccountId(String pointCode,String accountId) {
        return [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,CurrentSupply__r.ContractAccount__r.BillingProfile__c,
                        CurrentSupply__r.CompanyDivision__c,CurrentSupply__r.RecordType.DeveloperName
                FROM ServicePoint__c
                WHERE Code__c = :pointCode and Account__c = :accountId
                LIMIT 1
        ];
    }

    public List<ServicePoint__c> getListServicePoints(Set<Id> ids) {
        return [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,
                        CurrentSupply__r.CompanyDivision__c
                FROM ServicePoint__c
                WHERE ID = :ids
    ];
    }

    public List<ServicePoint__c> getListServicePointsByCodes(Set<String> codes) {
        return [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,CurrentSupply__r.ContractAccount__r.BillingProfile__c,Code__c,CurrentSupply__r.Account__c,
                        CurrentSupply__r.CompanyDivision__c,CurrentSupply__r.RecordType.DeveloperName
                FROM ServicePoint__c
                WHERE Code__c IN :codes
        ];
    }

    /**
     * Added by Moussa 12/09/2019
     *
     */
    public List<ServicePoint__c> getListServicePointsByCompanyDivision(String id,String companyDivisionId) {
        return [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,
                        CurrentSupply__r.CompanyDivision__c
                FROM ServicePoint__c
                WHERE ID = :id and CurrentSupply__r.CompanyDivision__c=:companyDivisionId
    ];
    }
    /**
     *
     * @Moussa Fofana
     * @param servicePointId
     * @description get ServicePoint by the Id.
     * @create 22/07/2019
     * @return ServicePoint__c
     */
    public ServicePoint__c getById(String servicePointId) {
        return [
            SELECT Trader__c,EstimatedConsumption__c,ContractualPower__c,AvailablePower__c,
                Distributor__c,Account__c,VoltageLevel__c,PowerPhase__c,Voltage__c,ConversionFactor__c,
                Pressure__c,PressureLevel__c,Code__c,
                PointStreetName__c,PointStreetNumber__c,PointStreetNumberExtn__c,PointPostalCode__c,
                PointCity__c,PointCountry__c,PointStreetType__c,PointApartment__c,PointBuilding__c,
                PointLocality__c,PointProvince__c,PointFloor__c,PointAddressNormalized__c,PointAddress__c
            FROM ServicePoint__c
            WHERE id = :servicePointId
            LIMIT 1
        ];
    }
    /**
     *
     * @Moussa Insa
     * @param Set<Id> servicePoint
     * @description get ServicePoint by the Id.
     * @create 22/07/2019
     * @return ServicePoint__c
     */
    public ServicePoint__c getByIds(Set<Id> servicePointId) {
        return [
            SELECT Trader__c,EstimatedConsumption__c,ContractualPower__c,AvailablePower__c,
                Distributor__c,Account__c,VoltageLevel__c,PowerPhase__c,Voltage__c,ConversionFactor__c,
                Pressure__c,PressureLevel__c,PointStreetName__c,PointStreetNumber__c,PointStreetNumberExtn__c,
                PointPostalCode__c,PointCity__c,PointCountry__c,PointStreetType__c,PointApartment__c,PointBuilding__c,
                PointLocality__c,PointProvince__c,PointFloor__c,PointAddressNormalized__c
            FROM ServicePoint__c
            WHERE id IN :servicePointId
            LIMIT 1
        ];
    }

    //BG commmented - unused code
    /**
    * @author BADJI
    * @param Id Service point Id
    * @description vefiry if the id of service point exists
    */
    /*public List<ServicePoint__c> verifyExistIdServicePoint(String id){
        return [SELECT Id, CurrentSupply__c
        FROM ServicePoint__c
        WHERE Id =: id];
    }*/
}