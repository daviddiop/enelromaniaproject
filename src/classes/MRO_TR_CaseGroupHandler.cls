/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 18, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_TR_CaseGroupHandler implements TriggerManager.ISObjectTriggerHandler {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public void beforeInsert() {
    }

    public void beforeUpdate() {
    }

    public void beforeDelete() {
    }

    public void afterInsert() {
    }

    public void afterUpdate() {
        Set<Id> readyCaseGroups = new Set<Id>();
        for (CaseGroup__c cg : (List<CaseGroup__c>) Trigger.new) {
            if (cg.Status__c == CONSTANTS.CASEGROUP_STATUS_READY && Trigger.oldMap.get(cg.Id).get('Status__c') != cg.Status__c) {
                readyCaseGroups.add(cg.Id);
            }
        }
        if (!readyCaseGroups.isEmpty()) {
            if (readyCaseGroups.size() > (Limits.getLimitQueueableJobs() - Limits.getQueueableJobs())) {
                throw new WrtsException('Too many Multi-Point Callouts to be performed');
            }
            else {
                for (Id caseGroupId : readyCaseGroups) {
                    System.enqueueJob(new MRO_SRV_SAPMultiPointCallout(caseGroupId));
                }
            }
        }
    }

    public void afterDelete() {
    }

    public void afterUndelete() {
    }
}