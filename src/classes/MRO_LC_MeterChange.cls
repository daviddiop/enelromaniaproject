/**
 * Created by goudiaby on 02/08/2019.
 */
/**
* @description Class controller for Case components
* @author Boubacar Sow
* @date 04-10-2019
 */
public with sharing class MRO_LC_MeterChange extends ApexServiceLibraryCnt {
    static MRO_UTL_Constants constantSrv = MRO_UTL_Constants.getAllConstants();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Connection').getRecordTypeId();
    private static String caseNewStatus = constantSrv.CASE_STATUS_NEW;
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();

    static List<ApexServiceLibraryCnt.Option> commodityPicklistValues  = new List<ApexServiceLibraryCnt.Option>();
    static String commodityLabel;


    /**
     * Initialize method to get data at the beginning for Meter Change
     */
    public with sharing class InitializeMeterChange extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String genericRequestId = params.get('genericRequestId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String templateId = params.get('templateId');
            //String genericRequestId ='';

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }


            /*if (String.isNotBlank(dossierId)) {
                Dossier__c dossierCheckParentId = dossierQuery.getById(dossierId);
                if (String.isNotBlank(dossierCheckParentId.Parent__c)) {
                    dossierId = dossierCheckParentId.Id;
                } else if (dossierCheckParentId.RecordType.DeveloperName == 'GenericRequest') {
                    genericRequestId = dossierId;
                    dossierId = '';
                }String.isBlank(dossierParent.Parent__c) &&
            }*/


            try {
                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'MeterChange');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                /*Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if (String.isNotBlank(genericRequestId)) {
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                }*/
                if (String.isNotBlank(dossier.Id)) {
                    Contact contactRecord = new Contact();
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                    if (cases.size() > 0) {
                        if (String.isNotBlank(cases[0].ContactId)) {
                            contactRecord = contactQuery.getById(cases[0].ContactId);
                            response.put('contactRecord', contactRecord);
                        }
                    }
                    response.put('caseTile', cases);
                }
                String code = 'MeterChangeTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }
                getCommodityDescribe();
                Account accountRecord = accountQuery.findAccount(accountId);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('accountId', accountId);
                response.put('account', accountRecord);
                response.put('meterChangeRTEle', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterChangeEle').getRecordTypeId());
                response.put('meterChangeRTGas', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterChangeGas').getRecordTypeId());
                response.put('commodityPicklistValues', commodityPicklistValues);
                response.put('commodityLabel', commodityLabel);
                //response.put('commodity', dossier.Commodity__c);
                response.put('commodity',commodityPicklistValues.get(0).label);
                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public static void getCommodityDescribe () {
        Schema.DescribeFieldResult commodityField = Dossier__c.Commodity__c.getDescribe();
        commodityLabel = commodityField.getLabel();
        System.debug('### commodityLabel '+commodityLabel);
        List<Schema.PicklistEntry> commodityFieldPicklist = commodityField.getPicklistValues();
        System.debug('### commodityFieldPicklist '+ commodityFieldPicklist);
        for (Schema.PicklistEntry pickListVal : commodityFieldPicklist) {
            if (System.Label.Electric == pickListVal.getLabel()) {
                System.debug('### pickListVal ' + pickListVal);
                commodityPicklistValues.add(new ApexServiceLibraryCnt.Option(pickListVal.getLabel(), pickListVal.getValue()));
            }
        }
    }

    public class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String contactId = params.get('contactId');
            String companyDivisionId;
            Set<Id> caseSetId = new Set<Id>();
            
            if (oldCaseList[0] != null) {
                companyDivisionId = oldCaseList[0].CompanyDivision__c;
            }
            for (Case caseRecord : oldCaseList) {
                caseSetId.add(caseRecord.Id);
            }
            
            //caseSrv.setNewOnCaseListAndDossier(oldCaseList, dossierId, companyDivisionId,contactId);
            caseSrv.setNewOnCaseListAndDossierMeterChange(oldCaseList,caseSetId, dossierId, companyDivisionId,contactId);
            response.put('error', false);
            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');

            caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId,'RC010',caseNewStatus);
            response.put('error', false);
            return response;
        }
    }
    /**
  * @author  DAVID DIOP
  * @description Get supply record
  * @date 10/01/2020
  * @param list <Id> supplies
  *
  */
    public with sharing class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            Supply__c supplyRecord = supplyQuery.getById(supplyId);
            response.put('supply', supplyRecord);
            response.put('error', false);
            return response;
        }
    }
    /**
  * @author  DAVID DIOP
  * @description update dossier
  * @date 13/01/2020
  * @param DossierId,channelSelected,originSelected
  *
  */
    public class UpdateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            dossierSrv.updateDossierChannel(dossierId,channelSelected,originSelected);
            response.put('error', false);
            return response;
        }
    }

    public with sharing class updateCommodityToDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String commodity = params.get('commodity');
            if (String.isBlank(dossierId)) {
                throw new WrtsException('DossierId - ' + System.Label.MissingId);
            }
            dossierSrv.updateCommodityOnDossier(dossierId,commodity);
            response.put('error', false);
            return response;
        }
    }

}