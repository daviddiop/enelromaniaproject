/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   gen 31, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_UTL_AccountJsonBuilder implements IJsonBuilder {
    public SObject getData(String recordId) {
        return MRO_QR_Account.getInstance().findAccount(recordId);
    }
}