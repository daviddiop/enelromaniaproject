/**
 * @author  Luca Ravicini
 * @since   Mar 17, 2020
 * @desc   Query class for PermissionSet object
 *
 */

public with sharing class MRO_QR_NACE {
    public static MRO_QR_NACE getInstance() {
        return new MRO_QR_NACE();
    }

    public NACE__c getNaceByClass (String clazz){
        List<NACE__c> naceList = [
                SELECT Id
                FROM NACE__c
                WHERE Class__c = :clazz
                LIMIT 1
        ];
        if (naceList.isEmpty()) {
            return null;
        }
        return naceList.get(0);
    }
}