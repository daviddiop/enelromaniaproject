/**
 * Created by david diop on 21.10.2020.
 */

public with sharing class MRO_SRV_MyEnel {
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();
    public static MRO_SRV_MyEnel getInstance() {
        return (MRO_SRV_MyEnel) ServiceLocator.getInstance(MRO_SRV_MyEnel.class);
    }
    public Map<String, Object> instantiateCase(Dossier__c dossier, String accountId,Case caseRecord, String recordTypeId){
        Map<String, Object> result = new Map<String, Object>();
        caseRecord.RecordTypeId = recordTypeId;
        caseRecord.AccountId = accountId;
        caseRecord.Dossier__c = dossier.Id;
        caseRecord.Channel__c = dossier.Channel__c;
        caseRecord.Origin = dossier.Origin__c;
        caseRecord.Status = constantsSrv.CASE_STATUS_NEW;
        caseRecord.SubProcess__c = dossier.SubProcess__c;
        result.put('case', caseRecord);
        return result;
    }
}