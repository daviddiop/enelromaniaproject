public with sharing class MRO_QR_BillingProfile {
	public static MRO_QR_BillingProfile getInstance() {
        return new MRO_QR_BillingProfile();
    }

    public BillingProfile__c getById(String billingProfileId) {
        return [
                SELECT Id, Name, PaymentMethod__c, DeliveryChannel__c, IBAN__c, Fax__c, Email__c, BillingAddress__c, BillingAddressNormalized__c,Bank__c,BankName__c,RefundBank__c,
                                                                                                                                //[ENLCRO-658] Billing Profile - Interface Check - Pack2
                       BillingStreetType__c, BillingStreetName__c, BillingStreetNumber__c, BillingStreetNumberExtn__c, BillingCity__c, BillingPostalCode__c,MobilePhone__c,
                                                                                                                                // [ENLCRO-658] Billing Profile - Interface Check - Pack2
                       BillingCountry__c, BillingBuilding__c, BillingBlock__c, BillingFloor__c, BillingLocality__c, BillingProvince__c, BillingApartment__c, BillingAddressKey__c, BillingStreetId__c, RefundMethod__c, RefundIBAN__c, Account__c
                FROM BillingProfile__c
                WHERE Id = :billingProfileId
        ];
    }

    public List<BillingProfile__c> getByAccountId(String accountId) {
        return [
                SELECT Id, Name, PaymentMethod__c, DeliveryChannel__c, IBAN__c, Fax__c, Email__c, BillingAddress__c, BillingAddressNormalized__c,Bank__c,BankName__c,RefundBank__c,
                                                                                                                                 //[ENLCRO-658] Billing Profile - Interface Check - Pack2
                       BillingStreetType__c, BillingStreetName__c, BillingStreetNumber__c, BillingStreetNumberExtn__c, BillingCity__c, BillingPostalCode__c,MobilePhone__c,
                                                                                                                                    //[ENLCRO-658] Billing Profile - Interface Check - Pack2
                       BillingCountry__c, BillingBuilding__c, BillingBlock__c, BillingFloor__c, BillingLocality__c, BillingProvince__c, BillingApartment__c, BillingAddressKey__c, BillingStreetId__c, RefundMethod__c, RefundIBAN__c
                FROM BillingProfile__c
                WHERE Account__c = :accountId
                ORDER BY Name
        ];
    }

	public Map<Id, BillingProfile__c> getLastBillingAddressesByAccountIds(Set<Id> accountIds) {
		List<BillingProfile__c> billingProfiles = [
                SELECT BillingAddress__c, BillingAddressNormalized__c, BillingStreetType__c, BillingStreetName__c, BillingStreetNumber__c,
	                   BillingStreetNumberExtn__c, BillingCity__c, BillingPostalCode__c, BillingCountry__c, BillingBuilding__c,
	                   BillingBlock__c, BillingFloor__c, BillingLocality__c, BillingProvince__c, BillingApartment__c, BillingAddressKey__c,
	                   BillingStreetId__c, Account__c
                FROM BillingProfile__c
                WHERE Account__c IN :accountIds AND BillingStreetId__c != NULL
			    ORDER BY CreatedDate DESC
        ];
		Map<Id, BillingProfile__c> result = new Map<Id, BillingProfile__c>();
		for (BillingProfile__c bp : billingProfiles) {
			if (!result.containsKey(bp.Account__c)) {
				result.put(bp.Account__c, bp);
			}
		}
		return result;
	}

    public Map<String,Id> getMapBankId() {
        Map<String, Id> mapBankId = new Map<String, Id>();
        List<Bank__c> banklist = new List<Bank__c>([Select Id,BIC__c from Bank__c]);

        for (Bank__c bank : banklist){
            mapBankId.put(bank.BIC__c, bank.Id);
        }

        return mapBankId;
    }

    public Map<Id, BillingProfile__c> getByIds(Set<Id> billingProfileId) {
        return new Map<Id, BillingProfile__c>([
                SELECT Id, Name, PaymentMethod__c, DeliveryChannel__c, IBAN__c, Fax__c, Email__c, BillingAddress__c, BillingAddressNormalized__c,Bank__c,BankName__c,RefundBank__c,RefundIBAN__c,RefundMethod__c,
                        BillingStreetType__c, BillingStreetName__c, BillingStreetNumber__c, BillingStreetNumberExtn__c, BillingCity__c, BillingPostalCode__c,MobilePhone__c,
                        BillingCountry__c, BillingBuilding__c, BillingBlock__c, BillingFloor__c, BillingLocality__c, BillingProvince__c, BillingApartment__c, BillingAddressKey__c, BillingStreetId__c,
	                    Account__c,CertifiedEmail__c,DirectDebitActivationDate__c,SAPStatus__c,UMR__c
                FROM BillingProfile__c
                WHERE Id IN :billingProfileId
                ]);
    }

    public List<ApexServiceLibraryCnt.Option> getListDeliveryChannelBusiness() {
        List<ApexServiceLibraryCnt.Option> businessDeliveryChannels = new List<ApexServiceLibraryCnt.Option>();
        businessDeliveryChannels.add(new ApexServiceLibraryCnt.Option('--None--', ''));
        businessDeliveryChannels.add(new ApexServiceLibraryCnt.Option('Email', 'Email'));
        businessDeliveryChannels.add(new ApexServiceLibraryCnt.Option('Mail', 'Mail'));
        return businessDeliveryChannels;
    }
}