@IsTest
public with sharing class MRO_LC_SwitchInWizardTst {
    @TestSetup
    static void setup() {
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;
        //update user;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0, SequenceLength__c = 9.0);
        insert sequencer;

        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().businessAccount().build());
        insert listAccount;
        Opportunity opportunity = MRO_UTL_TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        opportunity.Channel__c = 'MyEnel';
        opportunity.ContractSignedDate__c = system.today();
        insert opportunity;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[0].Id;
        insert contact;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;

        Account accountDistributor = TestDataFactory.account().businessAccount().build();
        accountDistributor.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        accountDistributor.Name = 'BusinessAccount1';
        accountDistributor.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        accountDistributor.Key__c = '1324433';
        accountDistributor.BusinessType__c = 'Real estate';
        accountDistributor.IsDisCoENEL__c = true;
        accountDistributor.IsDisCoEle__c = true;

        insert accountDistributor;

        OpportunityServiceItem__c opportunityServiceItem = MRO_UTL_TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        opportunityServiceItem.Distributor__c = accountDistributor.Id;
        opportunityServiceItem.ApplicantNotHolder__c = true;
        opportunityServiceItem.RecordTypeId = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId();
        insert opportunityServiceItem;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[0].Id;
        insert billingProfile;
        /*Product2 product2 = MRO_UTL_TestDataFactory.product2().build();
        insert product2;
        PricebookEntry pricebookEntry = MRO_UTL_TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = product2.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        insert pricebookEntry;*/

        Product2 product2 =MRO_UTL_TestDataFactory.product2().build();
        insert product2;

        PricebookEntry pricebookEntry = MRO_UTL_TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = product2.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = MRO_UTL_TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityId = opportunity.Id;
        opportunityLineItem.Product2Id = product2.Id;
        opportunityLineItem.PricebookEntryId = pricebookEntry.Id;
        opportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;

        PrivacyChange__c privacyChange = MRO_UTL_TestDataFactory.privacyChange().createPrivacyChangeBuilder().setOpportunity(opportunity.Id).build();
        privacyChange.Status__c = 'Active';
        insert privacyChange;

        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        opportunity.ContractId = contract.Id;
        update opportunity;

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Acquisition').getRecordTypeId();
        dossier.RequestType__c = 'SwitchIn';
        dossier.Status__c = 'New';
        dossier.Opportunity__c = opportunity.Id ;
        insert dossier;
        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        insert contractAccount;
    }

    @isTest
    static void initializeTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        System.debug('opportunity' + opportunity.Id);
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'opportunityId' => opportunity.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivisionId
                //'dossierId' => dossierId
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        OpportunityServiceItem__c osi = [
                SELECT Id,ApplicantNotHolder__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        osi.ApplicantNotHolder__c = false;
        update osi;

         response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Account distributor = [
                SELECT Id,Name,IsDisCoENEL__c,RecordTypeId
                FROM Account
                WHERE RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId()
                LIMIT 1
        ];
        distributor.IsDisCoENEL__c = false;
        update distributor;

        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        osi.ApplicantNotHolder__c = true;
        update osi;
        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        opportunity.Channel__c = 'Call Center';
        update opportunity;
        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        distributor.IsDisCoENEL__c = true;
        update distributor;

        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        result = (Map<String, Object>) response;
       inputJSON = new Map<String, String>{
                'accountId' => null,
                'opportunityId' => opportunity.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivisionId
                //'dossierId' => dossierId
        };
         response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, false);
        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'opportunityId' => opportunity.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => null
                //'dossierId' => dossierId
        };
        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        Test.stopTest();

    }
    @isTest
    static void initializekoTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'opportunityId' => opportunity.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivisionId
                //'dossierId' => dossierId
        };

        Test.startTest();

        OpportunityServiceItem__c osi = [
                SELECT Id,ApplicantNotHolder__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];

        Account distributor = [
                SELECT Id,Name,IsDisCoENEL__c,RecordTypeId
                FROM Account
                WHERE RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId()
                LIMIT 1
        ];
        opportunity.Channel__c = 'Call Center';
        update opportunity;

        distributor.IsDisCoENEL__c = true;
        update distributor;

        osi.ApplicantNotHolder__c = false;
        update osi;
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);

        distributor.IsDisCoENEL__c = false;
        update distributor;
        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        Test.stopTest();

    }

    @isTest
    static void initializeko2Test() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'opportunityId' => opportunity.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivisionId
                //'dossierId' => dossierId
        };

        Test.startTest();

        OpportunityServiceItem__c osi = [
                SELECT Id,ApplicantNotHolder__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];

        Account distributor = [
                SELECT Id,Name,IsDisCoENEL__c,RecordTypeId
                FROM Account
                WHERE RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId()
                LIMIT 1
        ];
        opportunity.Channel__c = 'Info Kiosk';
        update opportunity;

        distributor.IsDisCoENEL__c = true;
        update distributor;

        osi.ApplicantNotHolder__c = false;
        update osi;
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);

        distributor.IsDisCoENEL__c = false;
        update distributor;
        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        osi.ApplicantNotHolder__c = true;
        update osi;
        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);

        distributor.IsDisCoENEL__c = true;
        update distributor;
        response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        Test.stopTest();

    }

    @IsTest
    static void initializeDossierTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        System.debug('opportunity' + opportunity.Id);
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        System.debug('dossierId record' + dossierId);
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'opportunityId' => opportunity.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivisionId,
                'dossierId' => dossierId
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();

    }
    @IsTest
    static void updateOpportunityTest() {
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        PrivacyChange__c privacyChange = [
                SELECT Id
                FROM PrivacyChange__c
                LIMIT 1
        ];
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;

        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'privacyChangeId' => privacyChange.Id,
                'stage' => 'Quoted',
                'dossierId' => dossierId,
                'companySignedBy' => user.Id,
                'salesUnitId' => account.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();

    }
    @IsTest
    static void isBusinessClientTest() {

        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'isBusinessClient', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }

    @IsTest
    static void isBusinessClientKoTest() {

        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        insert listAccount;

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => listAccount[0].Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'isBusinessClient', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }


    @IsTest
    static void isBusinessClientExTest() {

        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => ''
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'isBusinessClient', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }
    @IsTest
    static void checkCompanyDivisionsTest() {
        Map<String, String > inputJSON = new Map<String, String>();

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'checkCompanyDivisions', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }

    @IsTest
    static void isNotWorkingDateTest() {
        Date workingDate = system.today();
        Map<String, String > inputJSON = new Map<String, String>{
                'requestedStartDate' => JSON.serialize(workingDate)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'isNotWorkingDate', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }

    @IsTest
    static void isNotWorkingDateKOTest() {
        Date workingDate = Date.newInstance(2020,08,22);
        System.debug('workingDate'+workingDate);
        Map<String, String > inputJSON = new Map<String, String>{
                'requestedStartDate' => JSON.serialize(workingDate)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'isNotWorkingDate', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }

    @IsTest
    static void deleteOsisAndOlisByCommodityChangeTest() {
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        String commodity = 'Electric';
        Map<String, String > inputJSON = new Map<String, String>{
                'commodity' => commodity,
                'opportunityId' => opportunity.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'deleteOsisAndOlisByCommodityChange', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
    }
    @IsTest
    static void setChannelAndOriginTest(){
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        String origin = 'Mail';
        String channel = 'MyEnel';
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'dossierId' => dossierId,
                'origin' => origin,
                'channel' => channel
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'setChannelAndOrigin', inputJSON, true);
        Boolean result = (Boolean) response;
        Test.stopTest();
    }

    @IsTest
    static void setSubProcessAndExpirationDateTest(){
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        String subProcess = 'Termination';
        Date expirationDate =  system.today();
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'dossierId' => dossierId,
                'subProcess' => subProcess,
                'expirationDate' => JSON.serialize(expirationDate)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'setSubProcessAndExpirationDate', inputJSON, false);
        Test.stopTest();
    }
    @IsTest
    static void setRequestedStartDateTest(){
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        Date expirationDate =  system.today();
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'requestedStartDate' => JSON.serialize(expirationDate)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'setRequestedStartDate', inputJSON, true);
        Test.stopTest();
    }
    @IsTest
    static void insertConsumptionListTest(){
        Contract contractRecord = [
                SELECT Id
                FROM Contract
                LIMIT 1
        ];
        List<Consumption__c> consumptionList = new List<Consumption__c>();
        Consumption__c consumption = new Consumption__c(Commodity__c = 'Electric', QuantityJanuary__c = 10.0, QuantityFebruary__c= 10.0,
                QuantityMarch__c = 10.0, QuantityApril__c = 10.0, QuantityMay__c = 10.0, QuantityJune__c = 10.0, QuantityJuly__c = 10.0,
                QuantityAugust__c = 10.0, QuantitySeptember__c = 10.0, QuantityOctober__c = 10.0, QuantityNovember__c = 10.0, QuantityDecember__c = 10.0);

        consumptionList.add(consumption);
        insert consumptionList;
        MRO_LC_SwitchInWizard.ConsumptionDetails details = new MRO_LC_SwitchInWizard.ConsumptionDetails();
        details.consumptionConventions = true;
        details.contractId = contractRecord.Id;
        details.consumptionList = consumptionList;
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'insertConsumptionList', details, true);
        Test.stopTest();
    }

    @IsTest
    static void updateContractAndContractSignedDateOnOpportunityTest(){
        Contract contractRecord = [
                SELECT Id
                FROM Contract
                LIMIT 1
        ];
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Contact contactRecord = [
                SELECT Id,Name
                FROM Contact
                LIMIT 1
        ];
        User user = new User();
        user.Id = UserInfo.getUserId() ;
        MRO_LC_SwitchInWizard.ContractInputData inputData = new MRO_LC_SwitchInWizard.ContractInputData();
        inputData.contractName = 'Mario';
        inputData.opportunityId = opportunity.Id;
        inputData.contractId = contractRecord.Id;
        inputData.contractType = 'Residential';
        inputData.salesChannel = 'Indirect Sales';
        inputData.salesUnitId = account.Id;
        inputData.salesman = contactRecord.Id;
        inputData.companySignedId = user.Id;
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateContractAndContractSignedDateOnOpportunity', inputData, true);
        Test.stopTest();
    }
    @IsTest
    static void updateCommodityToDossierTest(){
        String dossierId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'commodity' => 'Electric'
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateCommodityToDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false);
    }


    @IsTest
    static void updateCompanyDivisionInOpportunityTest(){
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'companyDivisionId' =>  companyDivisionId
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false);
    }

    @IsTest
    static void updateCompanyDivisionInOpportunityExTest(){
        Opportunity opportunity = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'companyDivisionId' =>  '4555845ds'
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
    }

    @IsTest
    static void updateOsiListTest(){
        OpportunityServiceItem__c osi = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
        osiList.add(osi);
        MRO_LC_SwitchInWizard.UpdateOsiListInput inputData = new MRO_LC_SwitchInWizard.UpdateOsiListInput();
        inputData.opportunityServiceItems = osiList;
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateOsiList', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false);
    }
    @IsTest
    static void updateTraderToOsiListTest(){
        OpportunityServiceItem__c osi = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'TraderAccount12';
        accountTrader.VATNumber__c = 'RO3911430';
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.Key__c = '1324430';
        insert accountTrader;
        CompanyDivision__c companyDivision = [
                SELECT Id,Name,Trader__c
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        companyDivision.Trader__c = accountTrader.Id;
        update companyDivision;
        List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
        osiList.add(osi);
        MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput inputData = new MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput();
        inputData.opportunityServiceItems = osiList;
        inputData.companyDivisionId = companyDivision.Id;
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateTraderToOsiList', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false);
    }

    @IsTest
    static void updateTraderToOsiListExTest(){
        OpportunityServiceItem__c osi = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'TraderAccount122';
        accountTrader.VATNumber__c = 'RO3911430';
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.Key__c = '1324434';
        insert accountTrader;
        CompanyDivision__c companyDivision = [
                SELECT Id,Name,Trader__c
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        companyDivision.Trader__c = accountTrader.Id;
        update companyDivision;
        List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
        osiList.add(osi);
        MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput inputData = new MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput();
        inputData.opportunityServiceItems = osiList;
        inputData.companyDivisionId = null;
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateTraderToOsiList', inputData, false);
        Test.stopTest();
    }

    static void updateTraderToOsiListExKoTest(){
        OpportunityServiceItem__c osi = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'TraderAccount1';
        accountTrader.VATNumber__c = 'RO3911430';
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.Key__c = '1324433';
        insert accountTrader;
        CompanyDivision__c companyDivision = [
                SELECT Id,Name,Trader__c
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        companyDivision.Trader__c = accountTrader.Id;
        update companyDivision;
        List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
        osiList.add(osi);
        MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput inputData = new MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput();
        inputData.opportunityServiceItems = null;
        inputData.companyDivisionId = companyDivision.Id;
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'updateTraderToOsiList', inputData, false);
        Test.stopTest();
    }

    @IsTest
    static void checkOsiTest(){
        OpportunityServiceItem__c osi = [
                SELECT Id,RecordType.DeveloperName,Distributor__r.IsDisCoENEL__c,ApplicantNotHolder__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        System.debug('OpportunityServiceItem__c1111'+osi.RecordType.DeveloperName);
        System.debug('OpportunityServiceItem__c1111'+osi.Distributor__r.IsDisCoENEL__c);
        System.debug('OpportunityServiceItem__c1111'+osi.ApplicantNotHolder__c);
        List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
        osiList.add(osi);
        MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput inputData = new MRO_LC_SwitchInWizard.UpdateTraderToOsiListInput();
        inputData.opportunityServiceItems = osiList;
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => osi.Id,
                'channel' =>  'EnelRo'
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchInWizard', 'checkOsi', inputJSON, true);
        Test.stopTest();
    }
    @isTest
    public static void linkOliToOsiTest(){
        List<OpportunityServiceItem__c>  listOsi = [
                SELECT Id, BillingProfile__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        OpportunityLineItem oli = [
                SELECT Id,Product2Id
                FROM OpportunityLineItem
                LIMIT 1
        ];
        MRO_LC_SwitchInWizard.LinkToOsiInput inputData = new MRO_LC_SwitchInWizard.LinkToOsiInput();
        inputData.opportunityServiceItems = listOsi;
        inputData.oli = oli;
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_SwitchInWizard', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    @isTest
    public static void linkOliToOsiExTest(){
        List<OpportunityServiceItem__c>  listOsi = [
                SELECT Id, BillingProfile__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        OpportunityLineItem oli = [
                SELECT Id,Product2Id
                FROM OpportunityLineItem
                LIMIT 1
        ];
        MRO_LC_SwitchInWizard.LinkToOsiInput inputData = new MRO_LC_SwitchInWizard.LinkToOsiInput();
        inputData.opportunityServiceItems = null;
        inputData.oli = oli;
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_SwitchInWizard', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }



}