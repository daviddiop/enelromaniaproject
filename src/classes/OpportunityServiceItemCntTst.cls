/**
 * Test class for  OpportunityServiceItemCnt
 *
 * @author Moussa Fofana
 * @version 1.0
 * @description Test class for  OpportunityServiceItemCnt
 * @uses
 * @code
 * @history
 * 2019-07-30:  Moussa Fofana
 * 2019-11-28:  David Diop
 */
@isTest
public with sharing class OpportunityServiceItemCntTst {
    @testSetup
    static void setup() {
        /*
        Account account = TestDataFactory.account().personAccount().build();
        insert account;
        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        insert opportunityServiceItem;
        ServiceSite__c serviceSite = TestDataFactory.serviceSite().createServiceSite().build();
        insert serviceSite;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;

        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().build();
        insert supply;

         */

        Profile p = [SELECT id, Name FROM Profile where name = 'System Administrator' ].get(0);
        User user = TestDataFactory.user().standardUser().build();
        user.profileId = p.id;
        insert user;


        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        user.CompanyDivisionId__c = companyDivision.Id;
        update user;

        Account acc = TestDataFactory.account().setName('BusinessName').build();
        insert acc;

        Account account = TestDataFactory.account().personAccount().build();
        insert account;


        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;

        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = acc.Id;
        contract.CompanyDivision__c = companyDivision.Id;
        insert contract;

        BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=acc.Id;
        insert billingProfile;

        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().build();
        supply.Contract__c=contract.Id;
        supply.CompanyDivision__c = companyDivision.Id;
        supply.ServicePoint__c = servicePoint.Id;
        insert supply;

        Supply__c supplyOld = TestDataFactory.supply().createSupplyBuilder().build();
        supply.Contract__c=contract.Id;
        supply.CompanyDivision__c = companyDivision.Id;
        insert supplyOld;

        servicePoint.CurrentSupply__c = supplyOld.Id;
        update servicePoint;


        ContractAccount__c contractAccount = TestDataFactory.contractAccount().createContractAccount().build();
        insert contractAccount;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;

        OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        opportunityServiceItem.ServicePoint__c = servicePoint.Id;
        insert opportunityServiceItem;

        Dossier__c dossier = TestDataFactory.dossier().build();
        dossier.Opportunity__c = opportunity.Id;
        dossier.Account__c = acc.id;
        dossier.CompanyDivision__c = companyDivision.Id;
        insert dossier;

        ServiceSite__c serviceSite = TestDataFactory.serviceSite().createServiceSite().build();
        insert serviceSite;

    }

    @isTest
    static void getExtraFieldsTest() {
        String extraFieldsFieldSet = 'PointAddress';

        Map<String, String > inputJSON = new Map<String, String>{
                'extraFieldsFieldSet' => extraFieldsFieldSet
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getExtraFields', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assert(result.get('fields') != null);
        system.assert(result.get('label') != null);
        Test.stopTest();
    }

    @isTest
    static void initializeTest() {
        Opportunity opp = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        Supply__c supply = [
            SELECT Id
            FROM Supply__c
            LIMIT 1
        ];
        ServicePoint__c servicePoint = [
            SELECT Id,Code__c,PointStreetNumber__c,PointStreetNumberExtn__c
            FROM ServicePoint__c
            LIMIT 1
        ];
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Test.startTest();
        Map<String, Object> osiAddressWithSupply = OpportunityServiceItemCnt.initialize(supply.Id,servicePoint.Id,servicePoint.Code__c,opp.Id,account.Id) ;
        Map<String, Object> osiAddressNoSupply = OpportunityServiceItemCnt.initialize(null,servicePoint.Id,servicePoint.Code__c,opp.Id,account.Id) ;
        Map<String, Object> result = (Map<String, Object>) osiAddressWithSupply;
        system.assertEquals(null,result.get('error'));
        Map<String, Object> resultNoSupply = (Map<String, Object>) osiAddressNoSupply;
        system.assertEquals(null,resultNoSupply.get('error') );

        Test.stopTest();
    }

   @isTest
    static void getPointCodeFieldsTest() {
        ServicePoint__c servicePoint = [
                SELECT Id,PointStreetName__c
                FROM ServicePoint__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'servicePointId' => servicePoint.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getPointCodeFields', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        system.assert(result.get('error') == false );
        system.assert(result.get('servicePointNotFound') == false );
        Test.stopTest();
    }


    @isTest
    static void getPointCodeFieldsServicePointIdIsBlankTest() {

        Map<String, String > inputJSON = new Map<String, String>{
                'servicePointId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'OpportunityServiceItemCnt', 'getPointCodeFields', inputJSON, false);
        }catch (Exception e){
            system.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void getPointCodeFieldsExceptionTest() {

        ServicePoint__c servicePoint = [
                SELECT Id,PointStreetName__c
                FROM ServicePoint__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'servicePointId' => servicePoint.Id
        };
        delete  servicePoint;
        Test.startTest();
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getPointCodeFields', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assert(result.get('error') == true );
        Test.stopTest();
    }

    @isTest
    static void getOsiAddressFieldTest() {
        OpportunityServiceItem__c opportunityServiceItem = [
                SELECT Id,RecordTypeId
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => opportunityServiceItem.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getOsiAddressField', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        system.assert(result.get('error') == false );
        Test.stopTest();
    }

    @isTest
    static void getOsiAddressFieldOpportunityServiceItemIsBlankTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'OpportunityServiceItemCnt', 'getOsiAddressField', inputJSON, false);
        }catch (Exception e){
            system.assert(true, e.getMessage());
        }

        Test.stopTest();
    }

    @isTest
    static void getOsiAddressFieldExceptionTest() {
        OpportunityServiceItem__c opportunityServiceItem = [
                SELECT Id,RecordTypeId
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => opportunityServiceItem.Id
        };
        delete  opportunityServiceItem;
        Test.startTest();
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getOsiAddressField', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        system.assert(result.get('error') == true );
        Test.stopTest();
    }


    @isTest
    static void getRecordTypesTest() {
        Test.startTest();

        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getRecordTypes', null, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assert(result.get('ss') == false );

        Test.stopTest();
    }

    @isTest
    static void getRecordTypesExceptionTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'electricRecordType' => 'electricTest',
                'gasRecordType' => 'gasTest'
        };
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getRecordTypes', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assert(result.get('ss') == false );

        Test.stopTest();
    }

    @isTest
    static void objectAPINameTest() {
        Test.startTest();
        OpportunityServiceItem__c opportunityServiceItem = [
            SELECT Id,RecordTypeId
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'objId' => opportunityServiceItem.Id
        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'objectAPIName', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assert(result.get('error') == false);
        Test.stopTest();
    }

    @isTest
    static void objectAPINameExceptionTest() {
        Test.startTest();
        Account acc = [
                SELECT Id,RecordTypeId
                FROM Account
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'objId' => 'objIdTest'
        };
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'objectAPIName', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }


    @isTest
    static void objectAPINameExcepTest() {
        Test.startTest();
        OpportunityServiceItem__c opportunityServiceItem = [
            SELECT Id,RecordTypeId
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'objId' => ''
        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'objectAPIName', inputJSON, false);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void serviceSiteOsiAddressSpTest() {
        Test.startTest();
        OpportunityServiceItem__c osi = [
            SELECT Id,RecordTypeId,SiteStreetType__c,SiteStreetName__c,SiteStreetNumberExtn__c,
                SitePostalCode__c,SiteProvince__c,SiteCountry__c,SiteCity__c,SiteApartment__c,
                SiteBuilding__c,SiteLocality__c,SiteFloor__c
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        osi.SiteStreetType__c = 'Street';
        osi.SiteStreetName__c = 'Street';
        osi.SiteStreetNumber__c = '3234';
        osi.SiteStreetNumberExtn__c = '';
        osi.SiteProvince__c = 'SiteProvince';
        osi.SitePostalCode__c = '4546';
        osi.SiteApartment__c = 'SiteApartment';
        osi.SiteBuilding__c = 'SiteBuilding';
        osi.SiteLocality__c = 'SiteLocality';
        osi.SiteFloor__c = 'SiteFloor';
        osi.SiteCountry__c = 'ITALY';
        osi.SiteCity__c = 'SiteCity';
        update osi;
        ServiceSite__c sp = null;
        List<String> siteOsiAddress = OpportunityServiceItemCnt.serviceSiteOsiAddress(sp,osi) ;
        System.assertEquals(true, siteOsiAddress.contains('Street') != false);
        Test.stopTest();
    }

    @isTest
    static void serviceSiteOsiAddressOsiTest() {
        Test.startTest();
        OpportunityServiceItem__c osi = null;
        ServiceSite__c sp = [
            SELECT Id,SiteStreetType__c,SiteStreetName__c,SiteCountry__c,
                SitePostalCode__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,
                SiteCity__c,SiteProvince__c,SiteApartment__c,SiteBuilding__c,
                SiteLocality__c,SiteFloor__c
            FROM ServiceSite__c
            LIMIT 1
        ];
        List<String> siteOsiAddress = OpportunityServiceItemCnt.serviceSiteOsiAddress(sp,osi) ;
        System.assertEquals(true, siteOsiAddress.contains('Street') != false);
        Test.stopTest();
    }

    @isTest
    static void getOsiRecordTypeDevNameTest() {
        Test.startTest();
        OpportunityServiceItem__c opportunityServiceItem = [
            SELECT Id,RecordTypeId
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'recordId' => opportunityServiceItem.Id
        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'getOsiRecordTypeDevName', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assert(result.get('error') == false);
        Test.stopTest();
    }
    @isTest
    static void getOsiRecordTypeDevNameExcepTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'recordId' => ''
        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'getOsiRecordTypeDevName', inputJSON, false);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void getOsiRecordTypeDevNameExceptionTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'recordId' => 'test'
        };
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getOsiRecordTypeDevName', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void getListOSIServiceSiteTest() {
        Test.startTest();
        OpportunityServiceItem__c osi = [
            SELECT Id
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        osi.Account__c = account.Id;
        osi.SiteAddressKey__c = 'SiteAddressKey';
        update osi;
        ServiceSite__c serviceSite = [
            SELECT Id
            FROM ServiceSite__c
            LIMIT 1
        ];
        serviceSite.Account__c = account.Id;
        serviceSite.SiteAddressKey__c = 'SiteAddressKey';
        update serviceSite;
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'recordId' => osi.Id
        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'getListOSIServiceSite', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') != true);
        Test.stopTest();
    }

    @isTest
    static void getListOSIServiceSiteExcepTest() {
        Test.startTest();
        OpportunityServiceItem__c osi = [
            SELECT Id
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '',
            'recordId' => osi.Id

        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'getListOSIServiceSite', inputJSON, false);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void getListOSIServiceSiteExceptionTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => 'testAccount',
                'recordId' => 'test'
        };
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getListOSIServiceSite', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
        Test.stopTest();
    }

    @isTest
    static void getServiceSiteAddressFieldTest() {
        Test.startTest();
        OpportunityServiceItem__c osi = [
            SELECT Id
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' => osi.Id
        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'getServiceSiteAddressField', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void getServiceSiteAddressFieldExceptionTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => 'TestOsi'
        };
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getServiceSiteAddressField', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void setOsiIdToSiteAddressKeyTest() {
        Test.startTest();
        OpportunityServiceItem__c osi = [
            SELECT Id
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' => osi.Id
        };
        Object response = TestUtils.exec(
            'OpportunityServiceItemCnt', 'setOsiIdToSiteAddressKey', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void setOsiIdToSiteAddressKeyIsBlankTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => ''
        };
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'setOsiIdToSiteAddressKey', inputJSON, false);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void setOsiIdToSiteAddressKeyExceptionTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => 'OsiTest'
        };
        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'setOsiIdToSiteAddressKey', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void getOsiAddressTest() {
        Test.startTest();
        OpportunityServiceItem__c osi = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'addressId' => osi.Id
        };

        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getOsiAddress', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

    @isTest
    static void getOsiAddressIsBlankTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'addressId' => ''
        };

        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getOsiAddress', inputJSON, false);
        system.assertEquals(true, response != null);
        Test.stopTest();
    }

    @isTest
    static void getOsiAddressExceptionTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'addressId' => 'addressTest'
        };

        Object response = TestUtils.exec(
                'OpportunityServiceItemCnt', 'getOsiAddress', inputJSON, true);
        system.assertEquals(true,response != null);
        Test.stopTest();
    }

}