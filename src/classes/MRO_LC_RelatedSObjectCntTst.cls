/**
 * Created by tommasobolis on 29/06/2020.
 */
@isTest
public with sharing class MRO_LC_RelatedSObjectCntTst {

    @TestSetup
    static void setup () {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
    }

    @isTest
    public static void testInitialize (){
        String contractId = [
                SELECT id
                FROM Contract
                LIMIT 1
        ].Id;
        Bank__c bank = new Bank__c();
        bank.Name = 'name';
        bank.BIC__c = 'BIC';
        insert bank;
        CompanyDivision__c company = new CompanyDivision__c();
        company.Name = 'Enel';
        company.Code__c='5000';
        company.Bank__c = bank.Id;
        insert company;
        /*Id relationshipSObjectId = bank.Id;
        String sObjectType = relationshipSObjectId.getSobjectType().getDescribe().getName();
        DescribeSObjectResult objResult = Schema.getGlobalDescribe().get(sObjectType).getDescribe();
        System.debug('return fs test '+objResult);
        Schema.FieldSet fs = objResult.fieldSets.getMap().get('Account'); //'RelatedSObjectComponentFieldSet'
        System.debug('return fs test '+fs);
        List<Schema.FieldSetMember> fsms = fs.getFields();
        System.debug('return Test '+fsms);*/

        Map<String, Object> returnValue = new Map<String, Object> ();
        Test.startTest();
        returnValue = MRO_LC_RelatedSObjectCnt.initialize(contractId,'$Label.name', 'Name', 'AccountId');
        System.debug('return value '+returnValue);
        Test.stopTest();
    }

    @isTest
    public static void testInitialize1 (){
        String contractId = [
                SELECT id
                FROM Contract
                LIMIT 1
        ].Id;

        Map<String, Object> returnValue = new Map<String, Object> ();
        Test.startTest();
        returnValue = MRO_LC_RelatedSObjectCnt.initialize(contractId,'name', 'Name', 'AccountId');
        System.debug('return value '+returnValue);
        Test.stopTest();
    }
@isTest
    public static void testInitialize2 (){
        String contractId = [
                SELECT id
                FROM Contract
                LIMIT 1
        ].Id;

        Map<String, Object> returnValue = new Map<String, Object> ();
        Test.startTest();
        returnValue = MRO_LC_RelatedSObjectCnt.initialize(contractId,'', 'Name', 'AccountId');
        System.debug('return value '+returnValue);
        Test.stopTest();
    }

}