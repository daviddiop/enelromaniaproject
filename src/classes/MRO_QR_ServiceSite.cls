public with sharing class MRO_QR_ServiceSite {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

	public static MRO_QR_ServiceSite getInstance() {
        return (MRO_QR_ServiceSite)ServiceLocator.getInstance(MRO_QR_ServiceSite.class);
    }

    public ServiceSite__c getById(Id serviceSiteId) {
        List<ServiceSite__c> serviceSiteList = [
                SELECT Id,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,SiteBuilding__c,SiteBlock__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,
                       SiteLocality__c,SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,SiteAddress__c,SiteStreetId__c,
                       CLC__c, NLC__c, Description__c, ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c, DistributorELE__c, DistributorGAS__c
                FROM ServiceSite__c
                WHERE Id = :serviceSiteId
        ];

        return (serviceSiteList.size() == 0) ? null : serviceSiteList.get(0);
    }

    public List<ServiceSite__c> getListServiceSite(String accountId) {
        return [
                SELECT Id,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,SiteBuilding__c,SiteBlock__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,
                       SiteLocality__c,SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,SiteAddress__c,SiteStreetId__c,
                       CLC__c, NLC__c, Description__c, ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c
                FROM ServiceSite__c
                WHERE Account__c = :accountId
        ];
    }
    /**
     * @author Luca Ravicini
     * @description find service sites for certain account and  site address key
     * @param siteAddressKey the value of SiteAddressKey__c field
     * @param accountId the value of Account__c field
     * @since Feb 27,2020
     *
     */
    public List<ServiceSite__c> getServiceSiteByAddressKey(String siteAddressKey, String accountId) {
        return [
                SELECT Id, CLC__c, NLC__c
                FROM ServiceSite__c
                WHERE SiteAddressKey__c = :siteAddressKey AND Account__c = : accountId
        ];
    }

    /**
     * @author Luca Ravicini
     * @description find service sites for any accounts and  site address keys
     * @param siteAddressKeys
     * @param accountIds
     * @since Mar 5,2020
     *
     */
    public List<ServiceSite__c> getServiceSiteByAddressKey(Set<String> siteAddressKeys, Set<String> accountIds) {
        return [
                SELECT Id, CLC__c, NLC__c,SiteAddressKey__c, Account__c
                FROM ServiceSite__c
                WHERE SiteAddressKey__c IN :siteAddressKeys AND Account__c IN: accountIds
        ];
    }

    public List<ServiceSite__c> listServiceSitesWithSupplies(Set<Id> serviceSiteIds) {
        return [SELECT Id, CLC__c, NLC__c,Description__c, SiteAddressKey__c, Account__c,
                       ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c,
                       (SELECT Id, RecordTypeId, RecordType.DeveloperName, ServicePoint__c, Status__c FROM Supplies__r)
                FROM ServiceSite__c
                WHERE Id IN: serviceSiteIds];
    }

    public List<ServiceSite__c> listServiceSitesByCLCsAndAccounts(Set<String> clcs, Set<Id> accountIds) {
        return [
                SELECT Id, CLC__c, NLC__c,SiteAddressKey__c, Account__c
                FROM ServiceSite__c
                WHERE CLC__c IN :clcs AND Account__c IN: accountIds
        ];
    }

    public List<ServiceSite__c> listServiceSitesByNLCsAndAccounts(Set<String> nlcs, Set<Id> accountIds) {
        return [
                SELECT Id, CLC__c, NLC__c,SiteAddressKey__c, Account__c
                FROM ServiceSite__c
                WHERE NLC__c IN :nlcs AND Account__c IN: accountIds
        ];
    }

    public List<ServiceSite__c> listServiceSitesByCLCorNLC(String clc, String nlc) {
        String query = 'SELECT Id, CLC__c, NLC__c,SiteAddressKey__c, Account__c, ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c '+
                       'FROM ServiceSite__c WHERE ';
        if (!String.isBlank(clc)) {
            query += 'CLC__c = :clc';
            if (!String.isBlank(nlc)) {
                query += ' OR ';
            }
        }
        if (!String.isBlank(nlc)) {
            query += 'NLC__c = :nlc';
        }
        return Database.query(query);
    }

    public List<ServiceSite__c> listServiceSitesByCLCorNLCAndAccountId(String clc, String nlc, Id accountId) {
        String query = 'SELECT Id, CLC__c, NLC__c,SiteAddressKey__c, Account__c, ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c '+
                       'FROM ServiceSite__c WHERE Account__c = :accountId AND (';
        if (!String.isBlank(clc)) {
            query += 'CLC__c = :clc';
            if (!String.isBlank(nlc)) {
                query += ' OR ';
            }
        }
        if (!String.isBlank(nlc)) {
            query += 'NLC__c = :nlc';
        }
        query += ')';
        return Database.query(query);
    }

    public List<ServiceSite__c> listAvailableELEServiceSites(Id accountId, Id distributorId, String nlc) {
        return [SELECT Id, SiteAddressKey__c, SiteAddressNormalized__c, SiteApartment__c, SiteBuilding__c, SiteBlock__c,
                       SiteCity__c, SiteCountry__c, SiteFloor__c, SiteStreetType__c, SiteLocality__c, SitePostalCode__c,
                       SiteProvince__c, SiteStreetName__c, SiteStreetNumber__c, SiteStreetNumberExtn__c, SiteAddress__c, SiteStreetId__c,
                       CLC__c, NLC__c, Account__c, ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c,
                       DistributorELE__c, DistributorELE__r.IsDisCoENEL__c, DistributorGAS__c, Description__c
                FROM ServiceSite__c
                WHERE Account__c = :accountId AND (ActiveSuppliesCountELE__c = 0 OR ActiveSuppliesCountELE__c = NULL) AND
                      ((DistributorELE__c = :distributorId AND (DistributorELE__r.IsDisCoENEL__c = TRUE OR (NLC__c = :nlc OR NLC__c = NULL))) OR
                       (DistributorELE__c = NULL AND NLC__c = NULL))];
    }

    public List<ServiceSite__c> listAvailableGASServiceSites(Id accountId, Id distributorId, String clc) {
        return [SELECT Id, SiteAddressKey__c, SiteAddressNormalized__c, SiteApartment__c, SiteBuilding__c, SiteBlock__c,
                       SiteCity__c, SiteCountry__c, SiteFloor__c, SiteStreetType__c, SiteLocality__c, SitePostalCode__c,
                       SiteProvince__c, SiteStreetName__c, SiteStreetNumber__c, SiteStreetNumberExtn__c, SiteAddress__c, SiteStreetId__c,
                       CLC__c, NLC__c, Account__c, ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c,
                       DistributorELE__c, DistributorELE__r.IsDisCoENEL__c, DistributorGAS__c, Description__c
                FROM ServiceSite__c
                WHERE Account__c = :accountId AND (ActiveSuppliesCountGAS__c = 0 OR ActiveSuppliesCountGAS__c = NULL) AND
                      ((DistributorGAS__c = :distributorId AND (CLC__c = :clc OR CLC__c = NULL)) OR
                       (DistributorGAS__c = NULL AND CLC__c = NULL))];
    }

    /**
     * @author Boubacar Sow
     * @date 13/11/2020
     * @description [ENLCRO-1741] Service Point Address Change - Address change execution at the case closure
     * @param serviceSiteIds
     *
     * @return
     */
    public ServiceSite__c getByIds(Set<Id> serviceSiteIds) {
        List<ServiceSite__c> serviceSites =  [
            SELECT Id, Name,Account__c, SiteStreetType__c, SiteStreetNumber__c, SiteCity__c, SitePostalCode__c,
                SiteCountry__c, SiteAddressNormalized__c, SiteStreetName__c, SiteApartment__c, SiteFloor__c,
                SiteLocality__c, SiteProvince__c, SiteBuilding__c, SiteBlock__c, SiteStreetNumberExtn__c
            FROM ServiceSite__c
            WHERE Id IN :serviceSiteIds
            LIMIT 1000
        ];
        if (serviceSites.isEmpty()) {
            return null;
        }
        return serviceSites.get(0);
    }
    /**
   * @author Giuseppe Mario Pastore
   * @description get Map Id Service Point by Supplies
   * @date 07/07/2020
   * @param Set<Id> serviceSiteIds
   * @return Map<Id, ServiceSite__c>
   */
    public Map<Id, ServiceSite__c> mapServiceSitesByIds(Set<Id> serviceSiteIds) {
        return new Map<Id, ServiceSite__c>([
            SELECT Id,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,SiteBuilding__c,SiteBlock__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,
                SiteLocality__c,SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,SiteAddress__c,SiteStreetId__c,
                CLC__c, NLC__c, Description__c, ActiveSuppliesCountELE__c, ActiveSuppliesCountGAS__c, ActiveSuppliesCountVAS__c
            FROM ServiceSite__c
            WHERE Id IN :serviceSiteIds
        ]);
    }

    public List<ServiceSite__c> listServiceSitesByNLCs(Set<String> nlcs) {
        return [
                SELECT Id, CLC__c, NLC__c,SiteAddressKey__c, Account__c
                FROM ServiceSite__c
                WHERE NLC__c IN :nlcs
        ];
    }
}