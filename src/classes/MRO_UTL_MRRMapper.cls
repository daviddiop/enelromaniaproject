/**
 * @author pes
 */

public with sharing class MRO_UTL_MRRMapper {
    public enum ErrorCode { UNSUPPORTED_FIELD_TYPE }
    public class MRRMapperException extends Exception {
        protected ErrorCode errCode;
        public MRRMapperException(ErrorCode err, String message) {
            this.errCode = err;
            this.setMessage(message);
        }
        public ErrorCode getErrorCode() {
            return this.errCode;
        }
    }

    /**
     * Converts an SObject record with its fields and related records into an MRR WObject structure.
     *
     * @param so            An SObject record that can also reference parent and child records
     * @param overrideName  (Optional) The name to be assigned to the WObject. It empty the SObject type name will be used.
     *
     * @return An MRR WObject instance.
     */
    public static wrts_prcgvr.MRR_1_0.WObject sObjectToWObject(SObject so, String overrideName) {
        return sObjectToWObject(so, overrideName, null);
    }

    /**
     * Converts an SObject record with its fields and related records into an MRR WObject structure.
     *
     * @param so            An SObject record that can also reference parent and child records
     * @param overrideName  (Optional) The name to be assigned to the WObject. It empty the SObject type name will be used.
     * @param fieldsSet     (Optional) The set of fields to include. If empty the getPopulatedFieldsAsMap() method will be used.
     *
     * @return An MRR WObject instance.
     */
    public static wrts_prcgvr.MRR_1_0.WObject sObjectToWObject(SObject so, String overrideName, Set<String> fieldsSet) {
        wrts_prcgvr.MRR_1_0.WObject wo = new wrts_prcgvr.MRR_1_0.WObject();
        wo.id = (String) so.get('Id');
        DescribeSObjectResult objectDescribe = so.getSObjectType().getDescribe();
        wo.objectType = objectDescribe.getName();
        if (!String.isBlank(overrideName)) {
            wo.name = overrideName;
        }
        else {
            wo.name = objectDescribe.getName();
        }
        wo.objects = new List<wrts_prcgvr.MRR_1_0.WObject>();
        if (fieldsSet == null) {
            fieldsSet = so.getPopulatedFieldsAsMap().keySet();
        }
        Map<String, Object> fieldMap = so.getPopulatedFieldsAsMap();
        //for (String fieldName : fieldsSet) {
        for (String fieldName : fieldMap.keySet()) {
            System.debug('FieldName: '+fieldName);
            if (fieldName == 'Id') continue;
            //Object fieldData = so.get(fieldName);
            Object fieldData = fieldMap.get(fieldName);
            if (fieldData instanceof List<Object>) {
                System.debug('Found SObject list '+fieldName);
                List<SObject> childRecords = (List<SObject>) fieldData;
                for (SObject record : childRecords) {
                    wo.objects.add(sObjectToWObject(record, fieldName));
                }
            }
            else if (fieldData instanceof SObject) {
                SObject record = (SObject) fieldData;
                System.debug('Found SObject '+fieldName+': '+record);
                wo.objects.add(sObjectToWObject(record, fieldName));
            }
            else {
                if(fieldData != null) wo.fields.add(createField(fieldName, fieldData));
            }
        }
        return wo;
    }

    /**
     * Creates an MRR Field instance using the provided name and value.
     *
     * @param name  The name of the field.
     * @param data  The value of the field. Supported data types: String, Date, Datetime, Integer, Double, Boolean.
     *
     * @return An MRR Field instance.
     */
    public static wrts_prcgvr.MRR_1_0.Field createField(String name, Object data) {
        wrts_prcgvr.MRR_1_0.Field field = new wrts_prcgvr.MRR_1_0.Field();
        field.name = name;
        if (data instanceof String) {
            field.value = (String) data;
            field.fieldType = 'string';
        }
        else if (data instanceof Date) {
            field.value = String.valueOf((Date) data);
            field.fieldType = 'date';
        }
        else if (data instanceof Datetime) {
            field.value = ((Datetime) data).formatGmt('yyyy-MM-dd hh:mm:ss');
            field.fieldType = 'datetime';
        }
        else if (data instanceof Integer) {
            field.value = String.valueOf((Integer) data);
            field.fieldType = 'integer';
        }
        else if (data instanceof Double) {
            field.value = String.valueOf((Double) data);
            field.fieldType = 'double';
        }
        else if (data instanceof Boolean) {
            field.value = String.valueOf((Boolean) data);
            field.fieldType = 'boolean';
        }
        else {
            throw new MRRMapperException(ErrorCode.UNSUPPORTED_FIELD_TYPE, 'Unable to convert data into MRR field: '+data);
        }
        return field;
    }

    /**
     * Creates a Map FieldName and FieldValue.
     * @description Convert wobject to Map
     * @param wo  WObject instance
     * @author Napoli
     * @return Map<string, string>.
     */
    public static Map<String, String> wobjectToMap(wrts_prcgvr.MRR_1_0.WObject wo) {
        Map<String, String> m = new Map<String, String>();

        if (wo.fields != null) {
            for (wrts_prcgvr.MRR_1_0.Field f : wo.fields) {
                m.put(f.name, f.value);
            }
        }
        return m;
    }

    /**
     * Creates a Map FieldName and FieldValue.
     * @description Get header Fields
     * @param header Header instance
     * @author Goudiaby
     * @return Map<String, String> map name field and value
     */
    public static Map<String, String> getHeaderFields(wrts_prcgvr.MRR_1_0.Header header) {
        Map<String, String> m = new Map<String, String>();

        if (header.fields != null) {
            for (wrts_prcgvr.MRR_1_0.Field f : header.fields ) {
                String nameField = f.name;
                m.put(nameField.toLowerCase(), f.value);
            }
        }
        return m;
    }

    /**
     * Creates a Map FieldName and FieldValue.
     *
     * @param wObject  WObject instance
     * @author Napoli
     * @return An SObject
     */
    public static SObject wobjectToSObject(wrts_prcgvr.MRR_1_0.WObject wo){
        Map<String,String> fMap = wobjectToMap(wo);
        SObject so =  Schema.getGlobalDescribe().get(wo.objectType).newSObject();
        Map<String,Schema.sObjectField> fieldMap=so.getSObjectType().getDescribe().fields.getMap();
        if(String.isNotBlank(wo.id) && wo.Id InstanceOf Id) so.Id = wo.id;
        for(String key: fMap.KeySet()){
            if(key == 'RecordTypeName'){
                try{
                    so.put('RecordTypeId',so.getSObjectType().getDescribe().getRecordTypeInfosByName().get(fMap.get(key)).getRecordTypeId());
                }
                catch(Exception e){
                    continue;
                }
            } else if(key != 'Id' && key !='RecordType' && fieldMap.get(key.toLowerCase()) != null && (fieldMap.get(key.toLowerCase()).getDescribe().isUpdateable() || fieldMap.get(key.toLowerCase()).getDescribe().isCreateable())){
                try{
                    String fieldType = String.ValueOf(fieldMap.get(key.toLowerCase()).getDescribe().getType()).toLowerCase();

                    switch on fieldType {
                        when  'currency', 'double', 'percent', 'decimal' {
                            so.put(key, Double.valueOf(fMap.get(key)));
                        }
                        when 'boolean' {
                            so.put(key, Boolean.valueOf(fMap.get(key)));
                        }
                        when 'date' {
                            Date d = Date.valueOf(fMap.get(key));
                            so.put(key,d);
                        }
                        when 'datetime' {
                            Datetime dt = Datetime.valueOf(fMap.get(key));
                            so.put(key,dt);
                        }
                        when else {
                            so.put(key,fMap.get(key));
                        }
                    }
                }
                catch(Exception e)
                {
                    continue;
                }
            }
        }
        return so;
    }

    /**
     * Creates an MRR Field instance using the provided name and value.
     *
     * @param parentId Id of parent record.
     * @param wObject  WObject instance
     * @param wObjectChild WObject instance
     * @author Napoli
     * @return An MRR WObject instance.
     */
    public static void appendChildRecords(Id parentId, wrts_prcgvr.MRR_1_0.WObject wObject, wrts_prcgvr.MRR_1_0.WObject wObjectChild) {
        if(wObject.Id == parentId) {
            wObject.objects.add(wObjectChild);
        }
        else if(wObject.Id != parentId && !wObject.objects.isEmpty()) {
            for (wrts_prcgvr.MRR_1_0.WObject record : wObject.objects) {
                appendChildRecords(parentId, record, wObjectChild);
            }
        }
    }

    public static List<wrts_prcgvr.MRR_1_0.WObject> selectWobjects(String pathString,  wrts_prcgvr.MRR_1_0.Request request) {
        if(String.isBlank(pathString) || request.objects == null || request.objects.isEmpty()) return null;
        List<wrts_prcgvr.MRR_1_0.WObject> woList = selectWobjects(pathString, request.objects);
        return woList;
    }

    public static List<wrts_prcgvr.MRR_1_0.WObject> selectWobjects(String pathString,  wrts_prcgvr.MRR_1_0.WObject wobject) {
        if(String.isBlank(pathString) || wobject.objects == null || wobject.objects.isEmpty()) return null;
        List<wrts_prcgvr.MRR_1_0.WObject> woList = selectWobjects(pathString, wobject.objects);
        return woList;
    }

    public static List<wrts_prcgvr.MRR_1_0.WObject> selectWobjects(String pathString,  List<wrts_prcgvr.MRR_1_0.WObject> woList) {
        if(String.isBlank(pathString)) return null;
        String objectName = pathString.substringBefore('/').contains('[') ? pathString.substringBefore('[').trim() : pathString.substringBefore('/').trim();
        String objectParam = pathString.substringBefore('/').contains('[') ? pathString.substringBetween('[',']').trim(): null;
        System.debug('pathString: >'+pathString+'<');
        System.debug('objectName: >'+objectName+'<');
        System.debug('objectParam: >'+objectParam+'<');
        List<wrts_prcgvr.MRR_1_0.WObject> wobjectList = new List<wrts_prcgvr.MRR_1_0.WObject>();
        if(woList != null && !woList.isEmpty()){
            for(wrts_prcgvr.MRR_1_0.WObject wo : woList){
                System.debug('***wo.name: '+ wo.name );
                if(wo.name == objectName && String.isBlank(objectParam)){
                    wobjectList.add(wo);
                } else if(wo.name == objectName && String.isNotBlank(objectParam)){
                    if(objectParam instanceof Id && wo.id == objectParam){
                        wobjectList.add(wo);
                    } else if(objectParam.isNumeric()){
                        wobjectList.add(wo);
                    }
                }
            }
        }
        if(!wobjectList.isEmpty()){
            pathString = pathString.substringAfter('/');
            if (String.isBlank(pathString)) {
                return wobjectList;
            }
            else if(wobjectList.size() == 1){
                wobjectList = selectWobjects(pathString, wobjectList[0]);
            } else if(wobjectList.size() > 1 && objectParam.isNumeric()){
                Integer index = Integer.valueOf(objectParam);
                wobjectList = selectWobjects(pathString, wobjectList[index]);
            }
        }
        return wobjectList;
    }

    public static void writeJSONStringOrNullField(String fieldName, String value, JSONGenerator gen) {
        if (value != null) {
            gen.writeStringField(fieldName, value);
        } else {
            gen.writeNullField(fieldName);
        }
    }

    public static void serializeField(wrts_prcgvr.MRR_1_0.Field f, JSONGenerator gen) {
        gen.writeStartObject();
        writeJSONStringOrNullField('name', f.name, gen);
        writeJSONStringOrNullField('fieldType', f.fieldType, gen);
        writeJSONStringOrNullField('value', f.value, gen);
        gen.writeEndObject();
    }

    public static void serializeFields(List<wrts_prcgvr.MRR_1_0.Field> fields, JSONGenerator gen) {
        gen.writeStartArray();
        if (fields != null) {
            for (wrts_prcgvr.MRR_1_0.Field f : fields) {
                serializeField(f, gen);
            }
        }
        gen.writeEndArray();
    }

    public static void serializeWObject(wrts_prcgvr.MRR_1_0.WObject wobj, JSONGenerator gen) {
        gen.writeStartObject();
        writeJSONStringOrNullField('id', wobj.id, gen);
        writeJSONStringOrNullField('name', wobj.name, gen);
        writeJSONStringOrNullField('objectType', wobj.objectType, gen);
        gen.writeFieldName('fields');
        serializeFields(wobj.fields, gen);
        gen.writeFieldName('objects');
        serializeWObjects(wobj.objects, gen);
        gen.writeEndObject();
    }

    public static void serializeHeader(wrts_prcgvr.MRR_1_0.Header h, JSONGenerator gen) {
        gen.writeStartObject();
        writeJSONStringOrNullField('requestId', h.requestId, gen);
        writeJSONStringOrNullField('requestTimestamp', h.requestTimestamp, gen);
        writeJSONStringOrNullField('requestType', h.requestType, gen);
        gen.writeFieldName('fields');
        serializeFields(h.fields, gen);
        gen.writeEndObject();
    }

    public static void serializeRequest(wrts_prcgvr.MRR_1_0.Request req, JSONGenerator gen) {
        gen.writeStartObject();
        gen.writeFieldName('header');
        if (req.header != null) {
            serializeHeader(req.header, gen);
        } else {
            gen.writeNull();
        }
        gen.writeFieldName('objects');
        serializeWObjects(req.objects, gen);
        gen.writeEndObject();
    }

    public static void serializeMultiRequest(wrts_prcgvr.MRR_1_0.MultiRequest mReq, JSONGenerator gen) {
        gen.writeStartObject();
        gen.writeFieldName('header');
        if (mReq.header != null) {
            serializeHeader(mReq.header, gen);
        } else {
            gen.writeNull();
        }
        gen.writeFieldName('requests');
        gen.writeStartArray();
        if (mReq.requests != null) {
            for (wrts_prcgvr.MRR_1_0.Request req : mReq.requests) {
                serializeRequest(req, gen);
            }
        }
        gen.writeEndArray();
        gen.writeEndObject();
    }

    public static void serializeResponse(wrts_prcgvr.MRR_1_0.Response res, JSONGenerator gen) {
        gen.writeStartObject();
        writeJSONStringOrNullField('code', res.code, gen);
        writeJSONStringOrNullField('description', res.description, gen);
        gen.writeFieldName('header');
        serializeHeader(res.header, gen);
        gen.writeFieldName('objects');
        serializeWObjects(res.objects, gen);
        gen.writeEndObject();
    }

    public static void serializeMultiResponse(wrts_prcgvr.MRR_1_0.MultiResponse mRes, JSONGenerator gen) {
        gen.writeStartObject();
        gen.writeFieldName('responses');
        gen.writeStartArray();
        if (mRes.responses != null) {
            for (wrts_prcgvr.MRR_1_0.Response res : mRes.responses) {
                serializeResponse(res, gen);
            }
        }
        gen.writeEndArray();
        gen.writeEndObject();
    }

    public static void serializeWObjects(List<wrts_prcgvr.MRR_1_0.WObject> wObjects, JSONGenerator gen) {
        gen.writeStartArray();
        if (wObjects != null) {
            for (wrts_prcgvr.MRR_1_0.WObject wobj : wObjects) {
                serializeWObject(wobj, gen);
            }
        }
        gen.writeEndArray();
    }

    public static String serializeMultirequest(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest){
        Map<String, Object> myMap = new Map<String, Object>();
        myMap.put('object', multiRequest);
        String jsonString = wrts_prcgvr.MRR_1_0.serializeMultiRequest(myMap);
        System.debug('***json*** ' + jsonString);
        return jsonString;
    }

    public static String serializeMultiRequest(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest, Boolean pretty) {
        JSONGenerator gen = JSON.createGenerator(pretty);
        serializeMultiRequest(multiRequest, gen);
        String jsonString = gen.getAsString();
        System.debug(System.LoggingLevel.FINE, '***json*** ' + jsonString);
        return jsonString;
    }

    public static String serializeMultiResponse(wrts_prcgvr.MRR_1_0.MultiResponse multiResponse){
        Map<String, Object> myMap = new Map<String, Object>();
        myMap.put('object', multiResponse);
        String jsonString = wrts_prcgvr.MRR_1_0.serializeMultiResponse(myMap);
        System.debug('***json*** ' + jsonString);
        return jsonString;
    }

    public static String serializeMultiResponse(wrts_prcgvr.MRR_1_0.MultiResponse multiResponse, Boolean pretty) {
        JSONGenerator gen = JSON.createGenerator(pretty);
        serializeMultiResponse(multiResponse, gen);
        String jsonString = gen.getAsString();
        System.debug(System.LoggingLevel.FINE, '***json*** ' + jsonString);
        return jsonString;
    }

    public static wrts_prcgvr.MRR_1_0.MultiRequest initMultiRequest(){
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = new wrts_prcgvr.MRR_1_0.MultiRequest();
        multiRequest.requests = new List<wrts_prcgvr.MRR_1_0.Request>();
        return multiRequest;
    }

    public static wrts_prcgvr.MRR_1_0.Request initRequest(Map<String, String> parameters) {
        wrts_prcgvr.MRR_1_0.Request request = new wrts_prcgvr.MRR_1_0.Request();
        //Header
        request.header = new wrts_prcgvr.MRR_1_0.Header();
        request.header.requestId = MRO_UTL_Guid.NewGuid();
        request.header.requestTimestamp = String.valueOf(System.now());
        request.header.requestType = parameters.get('requestType');
        //Fields
        request.header.fields = new List<wrts_prcgvr.MRR_1_0.Field>();

        //MyEnel - START
        if(parameters.containsKey('destination')){
            wrts_prcgvr.MRR_1_0.field f = new wrts_prcgvr.MRR_1_0.field();
            f.fieldType = 'String';
            f.name = 'destination';
            f.value = parameters.get('destination');
            request.header.fields.add(f);
        }
        //MyEnel - END

        for(String key : parameters.keySet()){
            if(!key.startsWithIgnoreCase('HDR_')) continue;
            key = key.removeStartIgnoreCase('HDR_');
            request.header.fields.add(createMRRField('String', key,  parameters.get(key)));//
        }

        request.objects = new List<wrts_prcgvr.MRR_1_0.WObject>();
        return request;
    }

    public static wrts_prcgvr.MRR_1_0.MultiResponse initMultiResponse(){
        wrts_prcgvr.MRR_1_0.MultiResponse multiResponse = new wrts_prcgvr.MRR_1_0.MultiResponse();
        multiResponse.responses = new List<wrts_prcgvr.MRR_1_0.Response>();
        return multiResponse;
    }

    public static wrts_prcgvr.MRR_1_0.Response initResponse(wrts_prcgvr.MRR_1_0.Request request) {
        wrts_prcgvr.MRR_1_0.Response response = new wrts_prcgvr.MRR_1_0.Response();
        response.code = 'OK';
        response.description = '';
        response.header = request.header;
        response.header.requestTimestamp = String.valueOf(System.now());
        response.objects = new List<wrts_prcgvr.MRR_1_0.WObject>();
        return response;
    }

    public static wrts_prcgvr.MRR_1_0.Field createMRRField(String type, String name, String value) {
        wrts_prcgvr.MRR_1_0.Field mrrField = new wrts_prcgvr.MRR_1_0.Field();
        mrrField.fieldType = type;
        mrrField.name = name;
        mrrField.value = value;
        return mrrField;
    }

    public static wrts_prcgvr.MRR_1_0.WObject createCustomMRRObject(String mrrObjectType, String mrrObjectName, String id) {
        wrts_prcgvr.MRR_1_0.WObject mrrObject = new wrts_prcgvr.MRR_1_0.WObject();
        mrrObject.id = id;
        mrrObject.objectType = mrrObjectType;
        mrrObject.name = mrrObjectName;
        mrrObject.fields = new List<wrts_prcgvr.MRR_1_0.Field>();
        mrrObject.objects = new List<wrts_prcgvr.MRR_1_0.WObject>();
        return mrrObject;
    }

    public static String getWObjectDebugStructure(wrts_prcgvr.MRR_1_0.WObject obj, String linePrefix, Integer level) {
        String result = linePrefix+' '+'-'.repeat(level)+' '+obj.name+': id: '+obj.id+' - objectType: '+obj.objectType;
        if (obj.objects != null) {
            for (wrts_prcgvr.MRR_1_0.WObject childObj : obj.objects) {
                result += '\n'+getWObjectDebugStructure(childObj, linePrefix, level+1);
            }
        }
        return result;
    }

    public static wrts_prcgvr.MRR_1_0.Field rebuildField(wrts_prcgvr.MRR_1_0.Field originalField) {
        wrts_prcgvr.MRR_1_0.Field resultField = new wrts_prcgvr.MRR_1_0.Field();
        resultField.name = originalField.name;
        resultField.fieldType = originalField.fieldType;
        resultField.value = originalField.value;
        return resultField;
    }

    public static wrts_prcgvr.MRR_1_0.WObject rebuildWObject(wrts_prcgvr.MRR_1_0.WObject originalObj) {
        wrts_prcgvr.MRR_1_0.WObject resultObj = new wrts_prcgvr.MRR_1_0.WObject();
        resultObj.id = originalObj.id;
        resultObj.name = originalObj.name;
        resultObj.objectType = originalObj.objectType;
        if (originalObj.fields != null) {
            resultObj.fields = new List<wrts_prcgvr.MRR_1_0.Field>();
            for (wrts_prcgvr.MRR_1_0.Field f : originalObj.fields) {
                resultObj.fields.add(rebuildField(f));
            }
        }
        if (originalObj.objects != null) {
            resultObj.objects = new List<wrts_prcgvr.MRR_1_0.WObject>();
            for (wrts_prcgvr.MRR_1_0.WObject wo : originalObj.objects) {
                resultObj.objects.add(rebuildWObject(wo));
            }
        }
        return resultObj;
    }

    public static wrts_prcgvr.MRR_1_0.Header rebuildHeader(wrts_prcgvr.MRR_1_0.Header originalHeader) {
        wrts_prcgvr.MRR_1_0.Header resultHeader = new wrts_prcgvr.MRR_1_0.Header();
        resultHeader.requestId = originalHeader.requestId;
        resultHeader.requestType = originalHeader.requestType;
        resultHeader.requestTimestamp = originalHeader.requestTimestamp;
        if (originalHeader.fields != null) {
            resultHeader.fields = new List<wrts_prcgvr.MRR_1_0.Field>();
            for (wrts_prcgvr.MRR_1_0.Field f : originalHeader.fields) {
                resultHeader.fields.add(rebuildField(f));
            }
        }
        return resultHeader;
    }

    public static wrts_prcgvr.MRR_1_0.Request rebuildRequest(wrts_prcgvr.MRR_1_0.Request originalReq) {
        wrts_prcgvr.MRR_1_0.Request resultReq = new wrts_prcgvr.MRR_1_0.Request();
        if (originalReq.header != null) {
            resultReq.header = rebuildHeader(originalReq.header);
        }
        if (originalReq.objects != null) {
            resultReq.objects = new List<wrts_prcgvr.MRR_1_0.WObject>();
            for (wrts_prcgvr.MRR_1_0.WObject wo : originalReq.objects) {
                resultReq.objects.add(rebuildWObject(wo));
            }
        }
        return resultReq;
    }

    public static wrts_prcgvr.MRR_1_0.MultiRequest rebuildMultiRequest(wrts_prcgvr.MRR_1_0.MultiRequest originalMR) {
        wrts_prcgvr.MRR_1_0.MultiRequest resultMR = new wrts_prcgvr.MRR_1_0.MultiRequest();
        if (originalMR.header != null) {
            resultMR.header = rebuildHeader(originalMR.header);
        }
        if (originalMR.requests != null) {
            resultMR.requests = new List<wrts_prcgvr.MRR_1_0.Request>();
            for (wrts_prcgvr.MRR_1_0.Request req : originalMR.requests) {
                resultMR.requests.add(rebuildRequest(req));
            }
        }
        return resultMR;
    }
}