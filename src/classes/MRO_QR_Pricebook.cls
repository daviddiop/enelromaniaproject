/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 23, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_Pricebook {
    public static MRO_QR_Pricebook getInstance() {
        return (MRO_QR_Pricebook) ServiceLocator.getInstance(MRO_QR_Pricebook.class);
    }

    public Pricebook2 getStandardPricebook() {
        List<Pricebook2> priceBookList = [SELECT Id FROM Pricebook2 WHERE IsStandard = TRUE LIMIT 1];
        if (priceBookList.isEmpty()) {
            throw new WrtsException('Standard Price Book does not exist');
        }
        return priceBookList[0];
    }

    public List<Pricebook2> listStandard() {
        return [
            SELECT Id
            FROM Pricebook2
            WHERE IsStandard = true
        ];
    }
}