/**
 * @author  Stefano Porcari
 * @since   Feb 12, 2020
 * @desc    Test class for MRO_LC_CommoditySelection
 *
 */

@IsTest
private class MRO_LC_CommoditySelectionTst {


    @isTest
    static void getCommodityPicklist() {
        MRO_LC_CommoditySelection.getCommodityPicklist getCommodityPicklist = new MRO_LC_CommoditySelection.getCommodityPicklist();
        String jsonInput = JSON.serializePretty(new Map<String, String>());
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>)getCommodityPicklist.perform(jsonInput);
        System.assert(response != null );
        System.assert( response.get('error') == false);
        List<ApexServiceLibraryCnt.Option> commodityPicklistValues=(List<ApexServiceLibraryCnt.Option>)response.get('options');
        System.assert(commodityPicklistValues != null);
        System.assert(!commodityPicklistValues.isEmpty());
        Test.stopTest();

    }
}