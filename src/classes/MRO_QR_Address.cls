/**
 * Created by A701948 on 15/09/2019.
 */

public with sharing class MRO_QR_Address {
    public static MRO_QR_Address getInstance() {
        return new MRO_QR_Address();
    }

    public static Street__c findStreet(String streetId) {
        List<Street__c> streets = [
                SELECT Id, Name, StreetType__c, FromStreetNumber__c, ToStreetNumber__c, Verified__c, PostalCode__c,
                        City__c, Even_Odd__c,City__r.Id, City__r.Name, City__r.Country__c, City__r.Province__c
                FROM Street__c
                WHERE Id = :streetId
                LIMIT 1
        ];
        if (streets.isEmpty()) {
            return null;
        }
        return streets.get(0);
    }

    //    static String ForceAddressPermissionName = 'ForceAddress';

    public static Boolean fieldExist(String field, String value, String table) {
        String query = 'select Id from ';
        switch on table {
            when 'city' {
                query += ' city__c ';
            }
            when 'street' {
                query += ' Street__c ';
            }
        }
        query += ' where ';

        switch on field {
            when 'country' {
                query += ' Country__c ';
            }
            when 'province' {
                query += ' Province__c ';
            }
            when 'city' {
                query += ' Name ';
            }
            when 'streetName' {
                query += ' Name ';
            }
            when 'postalCode' {
                query += ' PostalCode__c ';
            }
            when 'streetType' {
                query += ' StreetType__c ';
            }
        }
        query += ' = \'' + value + '\' ';
        query += 'limit 1';

        return Database.query(query).size() != 0;
    }
    /**
     *  retreive cities by name and state
     *  @param city    city name
     *  @param province   stateName
     *  @return list of matching city objects
     *
     */
    public static City__c getCity(String country, String city, String province) {

        City__c[] cities = [SELECT Id FROM City__c WHERE Country__c = :country AND Name = :city AND Province__c = :province LIMIT 1];
        return cities.isEmpty() ? null : cities[0];
    }


    /**
     *  retreive cities starting with a text
     *
     *  @param starWith     the text with which the city names begin
     *
     *  @return list of matching cities objects with name
     */
    public static List<City__c> getCitiesStartingWith(String startWith) {
        return [
                SELECT Name
                FROM City__c
                WHERE Name LIKE :startWith
                LIMIT 100
        ];
    }

    /**
     *  retreive countries starting with a text
     *
     *  @param starWith     the text with which the country names begin
     *
     *  @return list of matching country objects with country
     */
    public static List<City__c> getCountriesStartingWith(String startWith) {
        return [
                SELECT Country__c
                FROM City__c
                WHERE Country__c LIKE :startWith
                LIMIT 100
        ];
    }

    /**
     *  retreive street starting with a text
     *
     *  @param starWith     the text with which the city names begin
     *
     *  @return list of matching street objects with city name
     */
    public static List<Street__c> getStreetsCityNameStartingWith(String startWith) {
        return [
                SELECT City__r.Name
                FROM Street__c
                WHERE City__r.Name LIKE :startWith
                AND Verified__c = TRUE
                LIMIT 100
        ];
    }

    /**
     *  retreive cities country starting with a text
     *
     *  @param starWith     the text with which the city names begin
     *
     *  @return list of matching city objects with country
     */
    public static List<City__c> getCountryNameByCityName(String name) {
        List<City__c> result = [
                SELECT Country__c
                FROM City__c
                WHERE Name LIKE :name
        ];
        return result;
    }

    /**
     *  retreive cities state starting with a text
     *
     *  @param starWith     the text with which the city names begin
     *
     *  @return list of matching city objects with province
     */
    public static List<City__c> getProvinceByCity(String name) {
        List<City__c> result = [
                SELECT Province__c
                FROM City__c
                WHERE Name LIKE :name
        ];
        return result;
    }

    /**
     *  retreive streets
     *
     *  @param name        street name
     *  @param stateName   state name
     *  @param streetType    street type
     *  @param postalCode    street type
     *  @param evenOdd
     *
     *  @return list of matching street objects
     */
    public static Street__c[] getStreets(City__c city, String name, String streetType, String postalCode, String evenOdd) {

        return [
                SELECT Id
                FROM Street__c
                WHERE City__r.Id = :city.Id AND Name = :name
                AND StreetType__c = :streetType
//AND FromStreetNumber__c=:decimal.valueOf(address.get('fromStreetNumber'))
//AND ToStreetNumber__c =:decimal.valueOf(address.get('toStreetNumber'))
                AND PostalCode__c = :postalCode AND Even_Odd__c = :evenOdd
//                AND Verified__c = TRUE
        ];

    }

    /**
     *  retreive streets starting with a text
     *
     *  @param starWith     the text with which the street names begin
     *
     *  @return list of matching street objects with name
     */
    public static List<Street__c> getStreetsStartingWith(String startWith) {
        return [
                SELECT Name
                FROM Street__c
                WHERE Name LIKE :startWith
                AND Verified__c = TRUE
                LIMIT 20
        ];
    }

    /**
     *  retreive cities starting with a text
     *
     *  @param starWith     the text with which the state names begin
     *
     *  @return list of matching cities objects with state name
     */
    public static List<City__c> getProvincesStartingWith(String startWith) {
        return [
                SELECT Province__c
                FROM City__c
                WHERE Province__c LIKE :startWith
                limit 20
        ];
    }

    /**
     *  retreive streets starting with a text
     *
     *  @param starWith     the text with which the street names begin
     *
     *  @return list of matching street objects with postal code
     */
    public static List<Street__c> getPostalCodesStartingWith(String startWith) {
        return [
                SELECT PostalCode__c
                FROM Street__c
                WHERE PostalCode__c LIKE :startWith
                AND Verified__c = TRUE
                LIMIT 20
        ];
    }

    public static Street__c[] findExactStreetQuery1(MRO_SRV_Address.AddressDTO address_dto) {
        Street__c[] streets = findExactStreet(address_dto);
        Street__c[] streetsVerified = streets.clone();
        Integer index = 0;
        for (Street__c street : streets) {
            if (!street.Verified__c) {
                streetsVerified.remove(index);
            }
            index++;
        }
        return streetsVerified;
    }
    public static Street__c[] findExactStreetQuery2(MRO_SRV_Address.AddressDTO address_dto) {
        Street__c[] streets = findExactStreet2(address_dto);
        Street__c[] streetsVerified = streets.clone();
        Integer index = 0;
        for (Street__c street : streets) {
            if (!street.Verified__c) {
                streetsVerified.remove(index);
            }
            index++;
        }
        return streetsVerified;
    }

/**
 *  Find exact matching address
 *
 * @param address_dto a dto object with all the address fields
 * @param streetNumber street number
 *
 * @return normalised address
 */
    public static Street__c[] findExactStreet(MRO_SRV_Address.AddressDTO address_dto) {
       List<Street__c> streets= new List<Street__c>();
        WithoutStreetNumberValue__c noStreetNumberConstant = WithoutStreetNumberValue__c.getInstance();
        if(noStreetNumberConstant != null && address_dto.streetNumber !=null && address_dto.streetNumber != noStreetNumberConstant.NoStreetNumberString__c) {
            Long streetNumber = Long.valueOf(address_dto.streetNumber);
            String evenOdd = Math.mod(streetNumber, 2) == 0 ? 'E' : 'O';
            System.debug('even_odd' + evenOdd);

            String query = 'SELECT Name,Verified__c, StreetType__c, Even_Odd__c, PostalCode__c, City__r.Country__c, City__r.Name, City__r.Province__c' +
                    ' FROM Street__c' +
                    ' WHERE Name = \'' + address_dto.streetName + '\'' +
                    ' AND StreetType__c = \'' + address_dto.streetType + '\'' +
                    (String.isBlank(address_dto.postalCode) ? '' : ' AND PostalCode__c = \'' + address_dto.postalCode + '\'') +
                    ' AND City__r.Name = \'' + address_dto.city + '\'' +
                    ' AND City__r.Province__c = \'' + address_dto.province + '\'' +
                    ' AND City__r.Country__c = \'' + address_dto.country + '\'' +
//                ' AND Verified__c = TRUE' +
                    ' AND (Even_Odd__c=\'B\' OR Even_Odd__c = \'' + evenOdd + '\')' +
                    ' AND FromStreetNumber__c <= ' + streetNumber +
                    ' AND ToStreetNumber__c >= ' + streetNumber ;
            return (Street__c[]) Database.query(query);
        } else {
            if(noStreetNumberConstant != null && address_dto.streetNumber == noStreetNumberConstant.NoStreetNumberString__c) {
                System.debug('New FN Query');
                String query = 'SELECT Name,Verified__c, StreetType__c, Even_Odd__c, PostalCode__c, City__r.Country__c, City__r.Name, City__r.Province__c' +
                        ' FROM Street__c' +
                        ' WHERE Name LIKE  \'%' + address_dto.streetName + '%\'' +
                        ' AND StreetType__c = \'' + address_dto.streetType + '\'' +
                        (String.isBlank(address_dto.postalCode) ? '' : ' AND PostalCode__c = \'' + address_dto.postalCode + '\'') +
                        ' AND City__r.Name = \'' + address_dto.city + '\'' +
                        ' AND City__r.Province__c = \'' + address_dto.province + '\'' +
                        ' AND City__r.Country__c = \'' + address_dto.country + '\'' +
                        ' AND Even_Odd__c=\'B\'' +
                        ' AND FromStreetNumber__c = ' + null +
                        ' AND ToStreetNumber__c = ' + null ;
                System.debug(query);
                System.debug(query.right(100));
                return (Street__c[]) Database.query(query);
            }
        }
        return streets;

    }

    public static Street__c findExactStreet1(MRO_SRV_Address.AddressDTO address_dto) {
        Street__c[] streets = findExactStreet(address_dto);
        return streets.size() == 0 ? null : streets[0];
    }
    public static Street__c[] findExactStreet2(MRO_SRV_Address.AddressDTO address_dto) {
        List<Street__c> streets= new List<Street__c>();
        WithoutStreetNumberValue__c noStreetNumberConstant = WithoutStreetNumberValue__c.getInstance();
         if(noStreetNumberConstant != null && address_dto.streetNumber !=null && address_dto.streetNumber != noStreetNumberConstant.NoStreetNumberString__c) {
            Long streetNumber = Long.valueOf(address_dto.streetNumber);
            String evenOdd = Math.mod(streetNumber, 2) == 0 ? 'E' : 'O';
            System.debug('even_odd' + evenOdd);

            String query = 'SELECT Name,Verified__c, StreetType__c, Even_Odd__c, PostalCode__c, City__r.Country__c, City__r.Name, City__r.Province__c' +
                    ' FROM Street__c' +
                    ' WHERE Name LIKE  \'%' + address_dto.streetName + '%\'' +
                    ' AND StreetType__c = \'' + address_dto.streetType + '\'' +
                    (String.isBlank(address_dto.postalCode) ? '' : ' AND PostalCode__c = \'' + address_dto.postalCode + '\'') +
                    ' AND City__r.Name = \'' + address_dto.city + '\'' +
                    ' AND City__r.Province__c = \'' + address_dto.province + '\'' +
                    ' AND City__r.Country__c = \'' + address_dto.country + '\'' +
//                ' AND Verified__c = TRUE' +
                    ' AND (Even_Odd__c=\'B\' OR Even_Odd__c = \'' + evenOdd + '\')' +
                    ' AND FromStreetNumber__c <= ' + streetNumber +
                    ' AND ToStreetNumber__c >= ' + streetNumber ;
            System.debug(query);
            System.debug(query.right(100));
            return (Street__c[]) Database.query(query);
        } else {
            if(noStreetNumberConstant != null && address_dto.streetNumber == noStreetNumberConstant.NoStreetNumberString__c) {
                System.debug('New FN Query');
                String query = 'SELECT Name,Verified__c, StreetType__c, Even_Odd__c, PostalCode__c, City__r.Country__c, City__r.Name, City__r.Province__c' +
                        ' FROM Street__c' +
                        ' WHERE Name LIKE  \'%' + address_dto.streetName + '%\'' +
                        ' AND StreetType__c = \'' + address_dto.streetType + '\'' +
                        (String.isBlank(address_dto.postalCode) ? '' : ' AND PostalCode__c = \'' + address_dto.postalCode + '\'') +
                        ' AND City__r.Name = \'' + address_dto.city + '\'' +
                        ' AND City__r.Province__c = \'' + address_dto.province + '\'' +
                        ' AND City__r.Country__c = \'' + address_dto.country + '\'' +
                        ' AND Even_Odd__c=\'B\'' +
                        ' AND FromStreetNumber__c = ' + null +
                        ' AND ToStreetNumber__c = ' + null ;
                System.debug(query);
                System.debug(query.right(100));
                return (Street__c[]) Database.query(query);
            }
        }
        return streets;
    }

/**
 * Find matching address with like operator this is to get address that approach the one of the user
 *
 * @param countries country name
 * @param streetNames street name
 * @param cities   city name
 * @param provinces    state name
 * @param streetTypes   street type
 * @param postalCodes   postal code
 * @param streetNumber  street number
 *
 * @return list of matching addresses
 */
    public static List<Street__c> findStreets(String[] countries, String[] provinces, String[] cities,
            String[] streetTypes, String[] streetNames, String[] postalCodes, Long streetNumber) {
        System.debug('streetNumber#' + streetNumber);

        String evenOdd ;
        if (streetNumber != null) {
            evenOdd = Math.mod(streetNumber, 2) == 0 ? 'E' : 'O';
        } else {
            evenOdd = null;
        }
        System.debug('even_odd' + evenOdd);

        String query = 'SELECT Name, StreetType__c, Even_Odd__c, PostalCode__c,City__r.Country__c, City__r.Name, City__r.Province__c' +
                ' FROM Street__c' +
                ' WHERE  Verified__c = TRUE' ;
        if (evenOdd != null) {
            query += ' AND (Even_Odd__c=\'B\' OR Even_Odd__c = \'' + evenOdd + '\')';
        }
        if (streetNumber != null) {
            query += ' AND FromStreetNumber__c <= ' + streetNumber +
                    ' AND ToStreetNumber__c >= ' + streetNumber ;
        }

        if (countries != null && countries.size() > 0) {
            String[] subWhere = new List<String>();
            for (String v : countries) {
                subWhere.add('  City__r.Country__c  LIKE \'' + v + '\'');
            }
            query += ' AND (' + String.join(subWhere, ' OR ') + ')';
        }
        if (streetNames != null && streetNames.size() > 0) {
            String[] subWhere = new List<String>();
            for (String v : streetNames) {
                subWhere.add(' Name LIKE \'' + v + '\'');
            }
            query += ' AND (' + String.join(subWhere, ' OR ') + ')';
        }
        if (streetTypes != null && streetTypes.size() > 0) {
            String[] subWhere = new List<String>();
            for (String v : streetTypes) {
                subWhere.add(' StreetType__c LIKE \'' + v + '\'') ;
            }
            query += ' AND (' + String.join(subWhere, ' OR ') + ')';
        }
        if (postalCodes != null && postalCodes.size() > 0) {
            String[] subWhere = new List<String>();
            for (String v : postalCodes) {
                subWhere.add(' PostalCode__c LIKE \'' + v + '\'');
            }
            query += ' AND (' + String.join(subWhere, ' OR ') + ')';
        }
        if (cities != null && cities.size() > 0) {
            String[] subWhere = new List<String>();
            for (String v : cities) {
                subWhere.add(' City__r.Name LIKE \'' + v + '\'') ;
            }
            query += ' AND (' + String.join(subWhere, ' OR ') + ')';
        }
        if (provinces != null && provinces.size() > 0) {
            String[] subWhere = new List<String>();
            for (String v : provinces) {
                subWhere.add(' City__r.Province__c LIKE \'' + v + '\'') ;
            }
            query += ' AND (' + String.join(subWhere, ' OR ') + ')';
        }
        query += ' limit 20';
        System.debug('#query# ' + query);
        return (Street__c[]) Database.query(query);

    }

    public Map<String,String> getMapStreetNameBySelfReadingPeriod(String streetId) {
        Map<String, String> mapStreetNameBySelfReadingPeriod = new Map<String, String>();
        List<Street__c> streetNameSelfReadingPeriodList = new List<Street__c>([SELECT Id,SelfReadingPeriodEnd__c FROM Street__c WHERE Id =: streetId]);

        for (Street__c streetNameSelfReadingPeriod : streetNameSelfReadingPeriodList){
            if(String.isNotBlank(streetNameSelfReadingPeriod.SelfReadingPeriodEnd__c)) {
                mapStreetNameBySelfReadingPeriod.put(streetNameSelfReadingPeriod.Id, streetNameSelfReadingPeriod.SelfReadingPeriodEnd__c);
            }
        }
        return mapStreetNameBySelfReadingPeriod;
    }

    public List<ApexServiceLibraryCnt.Option> getListProvinceBySelfReadingPeriod(String pointAddressProvince) {
        List<ApexServiceLibraryCnt.Option> provinceBySelfReadingPeriods = new List<ApexServiceLibraryCnt.Option>();
        Schema.DescribeFieldResult fieldResult = Street__c.SelfReadingPeriodEnd__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        List<Street__c> provinceBySelfReadingPeriodList = new List<Street__c>([SELECT SelfReadingPeriodEnd__c FROM Street__c where City__r.Province__c  =: pointAddressProvince]);
        Set<String> provinceBySelfReadingPeriodSet = new Set<String>();

        for (Street__c selfReadingPeriodListRecord : provinceBySelfReadingPeriodList) {
            if (String.isNotBlank(selfReadingPeriodListRecord.SelfReadingPeriodEnd__c)) {
                provinceBySelfReadingPeriodSet.add(selfReadingPeriodListRecord.SelfReadingPeriodEnd__c);
            }
        }
        for (String selfReadingPeriodRecord : provinceBySelfReadingPeriodSet) {
            for (Schema.PicklistEntry v : values) {
                if ( v.getValue() == selfReadingPeriodRecord){
                    provinceBySelfReadingPeriods.add(new ApexServiceLibraryCnt.Option(v.getLabel(), v.getValue()));
                }
            }
        }
        return provinceBySelfReadingPeriods;
    }

    public List<ApexServiceLibraryCnt.Option> getListSelfReadingPeriodByCompanyDivision(String companyDivisionName,String vatNumber,String market) {
        List<ApexServiceLibraryCnt.Option> provinceBySelfReadingPeriods = new List<ApexServiceLibraryCnt.Option>();
        Schema.DescribeFieldResult fieldResult = ContractAccount__c.SelfReadingPeriodEnd__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        List<String> selfReadingPeriodList = new List<String>();
        if(market.containsIgnoreCase('Free')){
            if(companyDivisionName.containsIgnoreCase('Muntenia') && vatNumber =='RO14507322'){
                selfReadingPeriodList.add('5');
                selfReadingPeriodList.add('15');
                selfReadingPeriodList.add('31');
            }else if((!companyDivisionName.containsIgnoreCase('Muntenia')) && (vatNumber =='RO14500308' || vatNumber =='RO14490379')){
                selfReadingPeriodList.add('10');
                selfReadingPeriodList.add('20');
                selfReadingPeriodList.add('31');
            }else if(companyDivisionName.containsIgnoreCase('Muntenia') && (vatNumber !='RO14507322')){
                selfReadingPeriodList.add('31');
            }else if((!companyDivisionName.containsIgnoreCase('Muntenia')) && (vatNumber =='RO14507322')){
                selfReadingPeriodList.add('31');
            }
        }else if(market.containsIgnoreCase('Regulated')){
            if(companyDivisionName.containsIgnoreCase('Muntenia')){
                selfReadingPeriodList.add('5');
                selfReadingPeriodList.add('15');
                selfReadingPeriodList.add('31');
            }else if(!companyDivisionName.containsIgnoreCase('Muntenia')){
                selfReadingPeriodList.add('10');
                selfReadingPeriodList.add('20');
                selfReadingPeriodList.add('31');
            }
        }


        for (String selfReadingPeriodRecord : selfReadingPeriodList) {
            for (Schema.PicklistEntry v : values) {
                if (v.getValue() == selfReadingPeriodRecord){
                    provinceBySelfReadingPeriods.add(new ApexServiceLibraryCnt.Option(v.getLabel(), v.getValue()));
                }
            }
        }
        return provinceBySelfReadingPeriods;
    }


    public Map<String,String> getMapStreetNameByBillingFrequency(String streetId) {
        Map<String, String> mapStreetNameByBillingFrequency = new Map<String, String>();
        List<Street__c> streetNameBillingFrequencyList = new List<Street__c>([SELECT Id,BillingFrequency__c FROM Street__c WHERE Id =: streetId]);

        for (Street__c streetNameBillingFrequency : streetNameBillingFrequencyList){
            if(String.isNotBlank(streetNameBillingFrequency.BillingFrequency__c)) {
                mapStreetNameByBillingFrequency.put(streetNameBillingFrequency.Id, streetNameBillingFrequency.BillingFrequency__c);
            }
        }
        return mapStreetNameByBillingFrequency;
    }
}