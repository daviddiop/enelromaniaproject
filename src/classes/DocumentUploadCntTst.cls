/**
 * Modified by Moussa on 03/07/2019.
 */

@isTest
public class DocumentUploadCntTst {

    @testSetup
    private static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account myAccount = TestDataFactory.account().personAccount().build();
        insert myAccount;
        insert TestDataFactory.Dossier().setAccount(myAccount.Id).build();
        insert TestDataFactory.contentVersion().build();
    }

    @isTest
    static void getTempFileMetadataTest(){
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
        'DocumentUploadCnt', 'getTempFileMetadata', new Map<String, String>(), true);
        Test.stopTest();

        FileMetadata__c fileMetadata1 = (FileMetadata__c)response.get('fileMetadata');
        Datetime loadDateTime = (Datetime)response.get('loadDateTime');
        System.assertNotEquals(null, fileMetadata1);
        System.assertNotEquals(null, loadDateTime);

        response = (Map<String, Object>)TestUtils.exec(
            'DocumentUploadCnt', 'getTempFileMetadata', new Map<String, String>(), true);

        FileMetadata__c fileMetadata2 = (FileMetadata__c)response.get('fileMetadata');
        System.assertEquals(fileMetadata1.Id, fileMetadata2.Id);

    }

    @isTest
    static void createFileMetadataTest(){
        FileMetadata__c myFileMetadata = TestDataFactory.FileMetadata().setTitle(Label.TempFileMetadataName).build();
        insert myFileMetadata;
        ContentDocument contentDocument = [
            SELECT Id, Title, LatestPublishedVersionId
            FROM ContentDocument
            LIMIT 1
        ];
        List<Dossier__c> dossierList = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, Object> jsonInput = new Map<String, Object>{
            'fileMetadataId' => myFileMetadata.Id,
            'documentId' => contentDocument.Id,
            'dossierId' => dossierList.get(0).Id,
            'loadDateTime' => Datetime.now().addMinutes(-1)
        };

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('DocumentUploadCnt', 'createFileMetadata', jsonInput, true);
        List<FileMetadata__c> fileMetadataList = (List<FileMetadata__c>)result.get('fileMetadataList');
        System.assertEquals(1, fileMetadataList.size());

        jsonInput = new Map<String, String>{
            'fileMetadataId' => myFileMetadata.Id,
            'documentId' => contentDocument.Id,
            'dossierId' => dossierList.get(0).Id,
            'loadDateTime' => null
        };
        result = (Map<String, Object>)TestUtils.exec('DocumentUploadCnt', 'createFileMetadata', jsonInput, true);
        fileMetadataList = (List<FileMetadata__c>)result.get('fileMetadataList');
        System.assertEquals(true, fileMetadataList != null );        

        Test.stopTest();
    }

    @isTest
    static void createFileMetadataEmptyFileMetadataIdTest(){       
        ContentDocument contentDocument = [
            SELECT Id
            FROM ContentDocument
            LIMIT 1
        ];
        Map<String, String> jsonInput = new Map<String, String>{
            'fileMetadataId' => '',
            'documentId' => contentDocument.Id
        };
        Test.startTest();
        try {
            TestUtils.exec('DocumentUploadCnt', 'createFileMetadata', jsonInput, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();

    }

    @isTest
    static void createFileMetadataEmptyDocumentTest(){
        Map<String, String> jsonInput = new Map<String, String>{
            'fileMetadataId' => 'fileMetadataId',
            'documentId' => ''
        };
        Test.startTest();
        try {
            TestUtils.exec('DocumentUploadCnt', 'createFileMetadata', jsonInput, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();

    }
}