/**
 * Created by goudiaby on 23/12/2019.
 */

public with sharing class MRO_LC_BeneficiaryInformation extends ApexServiceLibraryCnt {
    private static MRO_QR_Opportunity oppQuery = MRO_QR_Opportunity.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Asset assetQuery = MRO_QR_Asset.getInstance();
    private static MRO_SRV_VAS vasService = MRO_SRV_VAS.getInstance();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    private static final MRO_UTL_Parsers parsers = MRO_UTL_Parsers.getInstance();
    /*
     * Copies contact information from Account to Asset
     */
    public class copyFromAccount extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            Id accountId = parsers.parseId(params.get('accountId'));

            Savepoint sp = Database.setSavepoint();
            try {
                if (accountId == null){
                    throw new WrtsException('accountId or assetId is null');
                }
                Account currentAccount = accountQuery.findAccount(accountId);

                String name = currentAccount.Name;
                String email = currentAccount.Email__c;
                String phone = currentAccount.Phone;

                response.put('name', name);
                response.put('email', email);
                response.put('phone', phone);
            } catch (Exception ex) {
                Database.rollback(sp);
                System.debug(ex.getMessage());
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}