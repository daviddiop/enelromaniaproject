/**
 * Created by goudiaby on 02/08/2019.
 */
/**
* @description Class controller for Case components
* @author Baba Goudiaby
* @date 02-08-2019
 */
public with sharing class MRO_LC_Case extends ApexServiceLibraryCnt {
    private static DossierService dossierSrv = DossierService.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    static List<ApexServiceLibraryCnt.Option> recordTypePicklistValues  = new List<ApexServiceLibraryCnt.Option>();
    static String recordTypeLabel;
    /**
     * Initialize method to get data at the beginning
     */
    public class InitializeTechnicalDataChange extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType,'TechnicalDataChange');
            if (String.isNotBlank(dossierId)) {
                List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
                response.put('caseTile', cases);
            }
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('accountId', accountId);
            response.put('technicalDataChangeRTEle', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('TechnicalDataChange_ELE').getRecordTypeId());
            response.put('technicalDataChangeRTGas', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('TechnicalDataChange_GAS').getRecordTypeId());
            response.put('error', false);
            return response;
        }
    }

    public class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String companyDivisionId;

            if (oldCaseList[0] != null) {
                companyDivisionId = oldCaseList[0].CompanyDivision__c;
            }

            caseSrv.setNewOnCaseAndDossier(oldCaseList, dossierId, companyDivisionId);
            response.put('error', false);
            return response;
        }
    }

    public class CloneCase extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String caseId = params.get('caseId');
            List<Case> newCaseList = new List<Case>();
            if (String.isBlank(caseId)) {
                throw new WrtsException(System.Label.Case + ' - ' + System.Label.Required);
            }
            try {
                Case caseRecord = caseQuery.getById(caseId);
                if (caseRecord != null) {
                    Case cloneCase = caseRecord.clone();
                    newCaseList.add(cloneCase);
                    response.put('cloneCase', cloneCase);
                }
                caseSrv.insertCaseRecords(newCaseList, new List<Case>());
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');

            caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId);
            response.put('error', false);
            return response;
        }
    }

    public with sharing class getCaseDescribe extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            recordTypePicklistValues = new List<ApexServiceLibraryCnt.Option>();
            recordTypePicklistValues.add(new ApexServiceLibraryCnt.Option('Electric', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Connection_ELE').getRecordTypeId()));
            recordTypePicklistValues.add(new ApexServiceLibraryCnt.Option('Gas', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Connection_GAS').getRecordTypeId()));
            response.put('recordTypePicklistValues', recordTypePicklistValues);
            response.put('error', false);
            return response;
        }
    }

    public static MRO_SRV_Address.AddressDTO copyCaseAddressToDTO(Case caseRecord) {
        MRO_SRV_Address.AddressDTO addressDTO = new MRO_SRV_Address.AddressDTO(caseRecord, 'Address');
        return addressDTO;
    }

    public class getCaseAddressField extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String caseId = params.get('caseId');

            if (String.isBlank(caseId)) {
                throw new WrtsException(System.Label.Case + ' - ' + System.Label.Required);
            }
            try {
                Case caseRecord = caseQuery.getById(caseId);
                if (caseRecord != null) {
                    MRO_SRV_Address.AddressDTO addressFromCase = MRO_LC_Case.copyCaseAddressToDTO(caseRecord);
                    response.put('error', false);
                    response.put('caseAddress', addressFromCase);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }


}