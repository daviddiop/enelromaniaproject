/**
 * Created by tommasobolis on 16/06/2020.
 */

public with sharing class MRO_UTL_LabelTranslator {

    public static final String URL_PARAM_LABEL_SEPARATOR = ';';
    public static final String VFP_LABEL_SEPARATOR = '$sep;$';
    public static final String VFP_LABEL_SEPARATOR_REGEX = '\\$sep;\\$';

    public  static String translate(String labelName, String language){
        try{

            return translate(new List<String> {labelName}, language)[0];
        } catch(Exception ex) {

            return labelName;
        }
    }

    public  static List<String> translate(List<String> labelNames, String language){
        try{
            PageReference r = Page.MRO_LabelTranslator;
            r.getParameters().put('label_lang', language);
            r.getParameters().put('labels', String.join(labelNames, URL_PARAM_LABEL_SEPARATOR));
            List<String> labelValues = new List<String>();
            if(Test.isRunningTest()) {
                labelValues = labelNames;
            } else {
                labelValues = r.getContent().toString()
                        .replaceFirst(VFP_LABEL_SEPARATOR_REGEX,'')
                        .split(VFP_LABEL_SEPARATOR_REGEX);
            }
            return labelValues;
        } catch(Exception ex) {
            return labelNames;
        }
    }
}