public with sharing class MRO_LC_CompanyInsolvency extends ApexServiceLibraryCnt {
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    /**
     * Initialize method to get data at the beginning for CompanyInsolvency
     */
    public class InitializeCompanyInsolvency extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestId = params.get('genericRequestId');
            Account account;
            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'Insolvency/Bankruptcy');
            if(String.isNotBlank(accountId)) {
                account = accountQuery.findAccount(accountId);
            }else {
                //Open Wizard from dossier
                account = accountQuery.findAccount(dossier.Account__c);
                response.put('accountId', account.Id);
            }
            if(!String.isBlank(genericRequestId)){
                dossierSrv.updateParentGenericRequest(dossier.Id,genericRequestId);
            }
            response.put('origin', dossier.Origin__c);
            response.put('channel', dossier.Channel__c);
            response.put('account', account);
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('isPersonAccount', account.IsPersonAccount);
            response.put('recordTypeInsolvencyBankruptcy', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('InsolvencyBankruptcy').getRecordTypeId());

            response.put('error', false);
            return response;
        }
    }

    public class insertCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {
                Date startDateInsolvency = params.get('inputValueStartDate')!= null && params.get('inputValueStartDate')!= ''
                    && params.get('inputValueStartDate')!= '""' ? (Date) JSON.deserialize(params.get('inputValueStartDate'), Date.class) : null;
                Date endDateInsolvency = params.get('inputValueInsolvencyEndDate')!= null && params.get('inputValueInsolvencyEndDate')!= ''
                    && params.get('inputValueInsolvencyEndDate')!= '""'? (Date) JSON.deserialize(params.get('inputValueInsolvencyEndDate'), Date.class) : null;
                Date startDateBankruptcy = params.get('inputValueBankruptcyStartDate')!= null && params.get('inputValueBankruptcyStartDate')!= ''
                    && params.get('inputValueBankruptcyStartDate')!= '""' ? (Date) JSON.deserialize(params.get('inputValueBankruptcyStartDate'), Date.class) : null;
                CreateCaseInput createCaseParams =(CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);
                Case caseInsolvencyBankrupt = caseSrv.insertCaseInsolvencyBankrupt(createCaseParams, startDateInsolvency, endDateInsolvency, startDateBankruptcy);
                Dossier__c dossier = dossierQuery.getById(caseInsolvencyBankrupt.Dossier__c);
                dossier.Status__c = caseInsolvencyBankrupt.Status;
                dossier.Origin__c = caseInsolvencyBankrupt.Origin;
                dossier.Channel__c = caseInsolvencyBankrupt.Channel__c;
                dossier.SLAExpirationDate__c = caseInsolvencyBankrupt.SLAExpirationDate__c;
                update dossier;
                response.put('dossier',dossier);
                System.debug('#### dossier' + dossier);
                System.debug('#### case: ' + caseInsolvencyBankrupt);
                if (caseInsolvencyBankrupt != null && String.isNotBlank(caseInsolvencyBankrupt.CompanyDivision__c)) {
                    dossierSrv.updateDossierCompanyDivision(caseInsolvencyBankrupt.Dossier__c, caseInsolvencyBankrupt.CompanyDivision__c);
                }
                caseSrv.applyAutomaticTransitionOnDossierAndCases(dossier.Id);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            response.put('error', false);
            return response;
        }
    }

    public class CreateCaseInput{
        @AuraEnabled
        public String recordTypeId{get; set;}
        @AuraEnabled
        public String accountId{get; set;}
        @AuraEnabled
        public String dossierId{get; set;}
        @AuraEnabled
        public String companyDivisionId{get; set;}
        @AuraEnabled
        public String inputValueStatus{get; set;}
        @AuraEnabled
        public String originSelected {get; set;}
        @AuraEnabled
        public String channelSelected {get; set;}
        @AuraEnabled
        public String inputValueJudicialAdministrator {get; set;}
        @AuraEnabled
        public String inputValueJudicialLiquidator {get; set;}
        @AuraEnabled
        public String inputdescription {get; set;}
        @AuraEnabled
        public Boolean isDraft {get; set;}
        @AuraEnabled
        public Boolean fromDossier {get;set;}
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            dossierSrv.setCanceledOnDossier(dossierId);
            response.put('error', false);
            return response;
        }
    }


}