@IsTest
private class ProductOptionCntTst {

    @TestSetup
    static void setup() {

        Product2 product2 = TestDataFactory.product2().build();
        insert product2;
        List<RecordType> productOptionRt = [
            SELECT Id, Name, DeveloperName
            FROM RecordType
            WHERE SobjectType = :'ProductOption__c'
        ];        
        ProductOption__c productOption = TestDataFactory.productOption()
            .setRecordType(productOptionRt[0].Id)
            .build();
        insert productOption;
    }    

    @IsTest 
    static void getRecordTypeProductOptionTest() {
        List<RecordType> productOptionRt = [
                SELECT Id, Name, DeveloperName
                FROM RecordType
                WHERE SobjectType = :'ProductOption__c'
        ];
        ProductOption__c productOpt = [
                SELECT Id
                FROM ProductOption__c
                LIMIT 1
        ];

        ProductOptionCnt.getRecordTypeProductOption getRecordProductOption = new ProductOptionCnt.getRecordTypeProductOption();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
                'recordId' => productOpt.Id
        });
        Test.startTest();
        getRecordProductOption.perform(jsonInput);
        Test.stopTest();
    }

    @IsTest
    static void getRecordTypeProductOptionExcepTest() {

        ProductOptionCnt.getRecordTypeProductOption getRecordProductOption = new ProductOptionCnt.getRecordTypeProductOption();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'recordId' =>'232323233434'
        });
        Test.startTest();
        getRecordProductOption.perform(jsonInput);
        Test.stopTest();
    }



    @IsTest
    static void createProdOptRelationTest() {
        Product2 product = [
                SELECT Id
                FROM Product2
                LIMIT 1
        ];
        ProductOption__c productOpt = [
                SELECT Id
                FROM ProductOption__c
                LIMIT 1
        ];
        Map<String, String> jsonInput = new Map<String, String>{
                'productId' => product.Id,
                'productOptionId' => productOpt.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('ProductOptionCnt', 'createProdOptRelation', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false , result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void createProdOptRelationExcepTest() {
        Map<String, String> jsonInput = new Map<String, String>{
            'productId' => '3434343434',
            'productOptionId' =>'34343232'
        };
        Test.startTest();
        Object response = TestUtils.exec('ProductOptionCnt', 'createProdOptRelation', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true , result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void createProdOptRelationExceptionTest() {
        Product2 product = [
            SELECT Id
            FROM Product2
            LIMIT 1
        ];        
        ProductOption__c productOpt = [
            SELECT Id
            FROM ProductOption__c
            LIMIT 1
        ];
        Map<String, String> jsonInput = new Map<String, String>{
                'productId' => product.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('ProductOptionCnt', 'createProdOptRelation', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false , result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void getPicklistFamiliesTest() {
        Test.startTest();
        ProductOptionCnt.getRecordTypeProductOption getRecordProductOption = new ProductOptionCnt.getRecordTypeProductOption();
        Object response = TestUtils.exec('ProductOptionCnt', 'getPicklistFamilies', null, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false , result.get('error'));
        Test.stopTest();
    }
}