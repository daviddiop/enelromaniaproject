/**
 * Create by Moussa Fofana on 29/07/2019.
 */
public with sharing class CaseService {

    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    static Constants constantSrv = Constants.getAllConstants();
    private static String caseOrigin = constantSrv.CASE_DEFAULT_CASE_ORIGIN;
    private static String caseDraftStatus = constantSrv.CASE_STATUS_DRAFT ;
    private static String caseNewStatus = constantSrv.CASE_STATUS_NEW;
    private static String caseCanceledStatus = constantSrv.CASE_STATUS_CANCELED;

    private static String supplyTerminatingStatus = constantSrv.SUPPLY_STATUS_TERMINATING;

    private static String dossierNewStatus = constantSrv.DOSSIER_STATUS_NEW;
    private static String dossierCanceledStatus = constantSrv.DOSSIER_STATUS_CANCELED;

    public static CaseService getInstance() {
        return (CaseService) ServiceLocator.getInstance(CaseService.class);
    }

    public Case createCaseRecord(Supply__c searchedSupply, String accountId, String dossierId, String recordTypeId, String reason, Date effectiveDate, Id trader) {
        Case disconnectedCase ;

        if (searchedSupply != null && String.isNotBlank(recordTypeId) && String.isNotBlank(accountId) && String.isNotBlank(dossierId)) {
            disconnectedCase = new Case(
                    Supply__c = searchedSupply.Id,
                    CompanyDivision__c = searchedSupply.CompanyDivision__c,
                    AccountId = accountId,
                    RecordTypeId = recordTypeId,
                    Dossier__c = dossierId,
                    Status = caseDraftStatus,
                    Origin = caseOrigin,
                    EffectiveDate__c = effectiveDate,
                    Reason__c = reason,
                    Trader__c = trader,
                    Market__c = searchedSupply.Market__c,
                    EstimatedConsumption__c = searchedSupply.ServicePoint__r.EstimatedConsumption__c,
                    ContractAccount__c = searchedSupply.ContractAccount__c,
                    BillingProfile__c = searchedSupply.ContractAccount__r.BillingProfile__c

            );
        }

        return disconnectedCase;
    }

    public Case createCaseForContractMerge(Supply__c supply, String accountId, String dossierId, String recordTypeId) {
        Case mergeContractCase ;
        if (supply != null && String.isNotBlank(recordTypeId) && String.isNotBlank(accountId) && String.isNotBlank(dossierId)) {
            mergeContractCase = new Case(
                    Supply__c = supply.Id,
                    CompanyDivision__c = supply.CompanyDivision__c,
                    AccountId = accountId,
                    RecordTypeId = recordTypeId,
                    Dossier__c = dossierId,
                    Status = caseDraftStatus,
                    Origin = caseOrigin
            );
        }
        return mergeContractCase;
    }

    public List<Case> insertCaseRecords(List<Case> newCases, List<Case> caseList) {
        if (!newCases.isEmpty()) {
            databaseSrv.insertSObject(newCases);
            if (!caseList.isEmpty()) {
                for (Case oldCaseList : caseList) {
                    newCases.add(oldCaseList);
                }
            }
        }
        return newCases;
    }

    public void updateCaseContractAccountAndContractId(List<Case> caseList, String contractAccountId, String contract) {
        List<Case> caseToUpdate = new List<Case>();
        if ((!caseList.isEmpty())) {
            for (Case caseRecord : caseList) {
                caseToUpdate.add(new Case(Id = caseRecord.Id, ContractAccount__c = contractAccountId, Contract__c = contract,Status = caseNewStatus));
            }
            if (!caseToUpdate.isEmpty()) {
                databaseSrv.updateSObject(caseToUpdate);
            }
        }
    }

    public void updateCaseByBillingProfile(List<Case> oldCaseList, String billingProfileId) {
        List<Case> caseList = new List<Case>();
        if (String.isNotBlank(billingProfileId) && (!oldCaseList.isEmpty())) {

            for (Case caseRecord : oldCaseList) {
                caseList.add(new Case(Id = caseRecord.Id, BillingProfile__c = billingProfileId));
            }
            if (!caseList.isEmpty()) {
                databaseSrv.updateSObject(caseList);
            }
        }
    }

    public void updateCaseStage(String caseId, String cancellationReason, String newStage) {
        Case updatedCase = new Case(Id = caseId, Status = newStage, CancellationReason__c = cancellationReason);
        databaseSrv.updateSObject(updatedCase);
    }

    public void setNewOnCaseAndDossier(List<Case> oldCaseList, String dossierId, String billingProfileId, String companyDivisionId) {

        List<Case> caseList = new List<Case>();
        if (!oldCaseList.isEmpty() && String.isNotBlank(dossierId) && String.isNotBlank(billingProfileId)) {

            Dossier__c dossier = new Dossier__c (Id = dossierId, Status__c = dossierNewStatus, CompanyDivision__c = companyDivisionId);
            if (dossier != null) {
                databaseSrv.updateSObject(dossier);
            }

            for (Case caseRecord : oldCaseList) {
                caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status = caseNewStatus, BillingProfile__c = billingProfileId));
            }
            if (!caseList.isEmpty()) {
                databaseSrv.updateSObject(caseList);
            }
        }
    }

    public void setNewOnCaseAndDossier(List<Case> oldCaseList, String dossierId, String companyDivisionId) {
        List<Case> caseList = new List<Case>();
        if (!oldCaseList.isEmpty() && String.isNotBlank(dossierId)) {

            Dossier__c dossier = new Dossier__c (Id = dossierId, Status__c = dossierNewStatus, CompanyDivision__c = companyDivisionId);
            if (dossier != null) {
                databaseSrv.updateSObject(dossier);
            }
            for (Case caseRecord : oldCaseList) {
                caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status = caseNewStatus));
            }
            if (!caseList.isEmpty()) {
                databaseSrv.updateSObject(caseList);
            }
        }
    }

    public void changeStatusOnCaseAndDossierAndSupply(List<Case> oldCaseList, String dossierId, String billingProfileId, String companyDivisionId) {

        List<Case> caseList = new List<Case>();
        List<Supply__c> supplyList = new List<Supply__c>();

        if (!oldCaseList.isEmpty() && String.isNotBlank(dossierId) && String.isNotBlank(billingProfileId)) {

            Dossier__c dossier = new Dossier__c (Id = dossierId, Status__c = dossierNewStatus, CompanyDivision__c = companyDivisionId);
            if (dossier != null) {
                databaseSrv.updateSObject(dossier);
            }

            for (Case caseRecord : oldCaseList) {
                caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status = caseNewStatus, BillingProfile__c = billingProfileId));
                supplyList.add(new Supply__c(Id = caseRecord.Supply__c, Status__c = supplyTerminatingStatus, Terminator__c = caseRecord.Id));
            }
            if (!caseList.isEmpty()) {
                databaseSrv.updateSObject(caseList);
            }
            if (!supplyList.isEmpty()) {
                databaseSrv.updateSObject(supplyList);
            }
        }
    }


    public void changeStatusOnCaseAndDossier(List<Case> oldCaseList, String dossierId, String billingProfileId) {

        List<Case> caseList = new List<Case>();

        if (!oldCaseList.isEmpty() && String.isNotBlank(dossierId) && String.isNotBlank(billingProfileId)) {

            Dossier__c dossier = new Dossier__c (Id = dossierId, Status__c = dossierNewStatus);
            if (dossier != null) {
                databaseSrv.updateSObject(dossier);
            }

            for (Case caseRecord : oldCaseList) {
                caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status = caseNewStatus, BillingProfile__c = billingProfileId));
            }
            if (!caseList.isEmpty()) {
                databaseSrv.updateSObject(caseList);
            }
        }
    }

    public void setCanceledOnCaseAndDossier(List<Case> oldCaseList, String dossierId) {
        if (String.isNotBlank(dossierId)) {
            Dossier__c dossier = new Dossier__c (Id = dossierId, Status__c = dossierCanceledStatus);
            if (dossier != null) {
                databaseSrv.upsertSObject(dossier);
            }
            if (!oldCaseList.isEmpty()) {
                List<Case> caseList = new List<Case>();
                for (Case caseRecord : oldCaseList) {
                    caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossierId, Status = caseCanceledStatus));
                }
                if (!caseList.isEmpty()) {
                    databaseSrv.updateSObject(caseList);
                }
            }
        }
    }

    public static void sendRetentionCases(String jsonRetentionCases) {
        if (String.isNotBlank(jsonRetentionCases)) {
            try {
                List<RetentionEvent__e> retentions = new List<RetentionEvent__e>();
                RetentionEvent__e retentionEvent = new RetentionEvent__e(RetentionCases__c = jsonRetentionCases);
                retentions.add(retentionEvent);

                List<Database.SaveResult> results = EventBus.publish(retentions);
                for (Database.SaveResult result : results) {
                    if (result.isSuccess()) {
                        System.debug('Successfully published event.');
                    } else {
                        for (Database.Error error : result.getErrors()) {
                            System.debug('Error returned: ' + error.getStatusCode() + ' - ' + error.getMessage());
                        }
                    }
                }
            } catch (Exception ex) {
                System.debug('Error returned: ' + ex.getMessage());
            }
        }
    }
}