/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 05, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_DynamicForm {

    public static MRO_SRV_DynamicForm getInstance() {
        return (MRO_SRV_DynamicForm) ServiceLocator.getInstance(MRO_SRV_DynamicForm.class);
    }

    public DynamicForm buildDynamicForm(Id fieldTemplateId, Id rootRecordId) {
        MRO_QR_FieldsTemplate fieldsTemplateQueries = MRO_QR_FieldsTemplate.getInstance();
        FieldsTemplate__c template = fieldsTemplateQueries.getById(fieldTemplateId);
        List<FieldsTemplateSection__c> sections = fieldsTemplateQueries.listSectionsWithDynamicFieldsByTemplateId(template.Id);

        String query = 'SELECT Id';
        for (FieldsTemplateSection__c section : sections) {
            if (section.ParentRelationPath__c != null) {
                query += ', '+section.ParentRelationPath__c+'.Id';
            }
        }
        query += ' FROM '+template.RootObjectType__c+' WHERE Id = :rootRecordId';
        SObject rootRecord = Database.query(query);

        MRO_SRV_DynamicForm.DynamicForm dynamicForm = new MRO_SRV_DynamicForm.DynamicForm(template, sections, rootRecord);
        return dynamicForm;
    }

    public void saveDynamicForm(DynamicForm form, Map<Id, Map<String, Object>> formData) {
        String query = 'SELECT Id';
        for (DynamicFormSection section : form.sections) {
            String parentRelationPath = String.isBlank(section.parentRelationPath) ? '' : section.parentRelationPath+'.';
            for (DynamicField field : section.fields) {
                query += ', '+parentRelationPath+field.fieldName;
            }
        }
        query += ' FROM '+form.rootObjectType+' WHERE Id = \''+form.rootRecordId+'\'';
        System.debug('Root record query: '+query);
        SObject rootRecord = Database.query(query);

        Map<Id, SObject> recordsMap = new Map<Id, SObject>();
        Map<String, SObjectType> globalDescribe = Schema.getGlobalDescribe();
        for (DynamicFormSection section : form.sections) {
            SObjectType objectType = globalDescribe.get(section.objectType);
            SObject record = MRO_UTL_SObject.createSObjectFromFieldsMap(objectType, formData.get(section.recordId), section.recordId);
            System.debug('record '+section.recordId+': '+record);
            recordsMap.put(section.recordId, record);
        }
        Map<Id, DynamicField> fieldsMap = form.getFieldsMap();
        Map<Id, DynamicFormSection> sectionsMap = form.getSectionsMap();
        for (DynamicField field : fieldsMap.values()) {
            if (field.inheritsFromFieldId != null) {
                DynamicField inheritsFromField = fieldsMap.get(field.inheritsFromFieldId);
                if (inheritsFromField != null) {
                    SObject targetRecord = recordsMap.get(sectionsMap.get(field.sectionId).recordId);
                    SObject sourceRecord;
                    if (!inheritsFromField.isReadOnly) {
                        DynamicFormSection section = sectionsMap.get(inheritsFromField.sectionId);
                        if (section != null && section.recordId != null) {
                            sourceRecord = recordsMap.get(section.recordId);
                        }
                    }
                    else {
                        sourceRecord = MRO_UTL_SObject.getParentRecord(rootRecord, sectionsMap.get(inheritsFromField.sectionId).parentRelationPath);
                    }
                    if (sourceRecord != null) {
                        targetRecord.put(field.fieldName, sourceRecord.get(inheritsFromField.fieldName));
                    }
                }
            }
        }
        DatabaseService dbSrv = DatabaseService.getInstance();
        dbSrv.updateSObject(recordsMap.values());
    }

    public with sharing class DynamicForm {
        @AuraEnabled
        public Id templateId {get; set;}
        @AuraEnabled
        public String rootObjectType {get; set;}
        @AuraEnabled
        public Id rootRecordId {get; set;}
        @AuraEnabled
        public List<MRO_SRV_DynamicForm.DynamicFormSection> sections {get; set;}

        public DynamicForm(FieldsTemplate__c template, List<FieldsTemplateSection__c> sections, SObject rootRecord) {
            this.templateId = template.Id;
            this.rootObjectType = template.RootObjectType__c;
            this.rootRecordId = rootRecord.Id;
            this.sections = new List<MRO_SRV_DynamicForm.DynamicFormSection>();
            for (FieldsTemplateSection__c section : sections) {
                SObject targetRecord = MRO_UTL_SObject.getParentRecord(rootRecord, section.ParentRelationPath__c);
                if (targetRecord != null) {
                    this.sections.add(new MRO_SRV_DynamicForm.DynamicFormSection(section, targetRecord.Id));
                }
            }
        }

        public Map<Id, MRO_SRV_DynamicForm.DynamicFormSection> getSectionsMap() {
            Map<Id, MRO_SRV_DynamicForm.DynamicFormSection> result = new Map<Id, MRO_SRV_DynamicForm.DynamicFormSection>();
            if (this.sections != null) {
                for (MRO_SRV_DynamicForm.DynamicFormSection section : this.sections) {
                    result.put(section.sectionId, section);
                }
            }
            return result;
        }

        public Map<Id, MRO_SRV_DynamicForm.DynamicField> getFieldsMap() {
            Map<Id, MRO_SRV_DynamicForm.DynamicField> result = new Map<Id, MRO_SRV_DynamicForm.DynamicField>();
            if (this.sections != null) {
                for (MRO_SRV_DynamicForm.DynamicFormSection section : this.sections) {
                    result.putAll(section.getFieldsMap());
                }
            }
            return result;
        }
    }

    public with sharing class DynamicFormSection {
        @AuraEnabled
        public Id templateId {get; set;}
        @AuraEnabled
        public Id sectionId {get; set;}
        @AuraEnabled
        public String title {get; set;}
        @AuraEnabled
        public Integer columns {get; set;}
        @AuraEnabled
        public Integer order {get; set;}
        @AuraEnabled
        public String objectType {get; set;}
        @AuraEnabled
        public String parentRelationPath {get; set;}
        @AuraEnabled
        public Id recordId {get; set;}
        @AuraEnabled
        public Boolean isStandard {get {
            return this.recordTypeDeveloperName != null && this.recordTypeDeveloperName == 'Standard';
        }}
        @AuraEnabled
        public Boolean isAddress {get {
            return this.recordTypeDeveloperName != null && this.recordTypeDeveloperName == 'Address';
        }}
        @AuraEnabled
        public String recordTypeDeveloperName {get; set;}
        @AuraEnabled
        public String fieldsPrefix {get; set;}
        @AuraEnabled
        public List<MRO_SRV_DynamicForm.DynamicField> fields {get; set;}

        public DynamicFormSection(FieldsTemplateSection__c fts, Id recordId) {
            this.templateId = fts.Template__c;
            this.sectionId = fts.Id;
            this.title = fts.Title__c;
            this.columns = fts.Columns__c.intValue();
            this.order = fts.Order__c.intValue();
            this.objectType = fts.ObjectType__c;
            this.parentRelationPath = fts.ParentRelationPath__c;
            this.recordId = recordId;
            this.recordTypeDeveloperName = fts.RecordType.DeveloperName;
            this.fieldsPrefix = fts.FieldsPrefix__c;
            this.fields = new List<MRO_SRV_DynamicForm.DynamicField>();
            for (DynamicField__c df : fts.DynamicFields__r) {
                this.fields.add(new MRO_SRV_DynamicForm.DynamicField(df));
            }
        }

        public Map<Id, MRO_SRV_DynamicForm.DynamicField> getFieldsMap() {
            Map<Id, MRO_SRV_DynamicForm.DynamicField> result = new Map<Id, MRO_SRV_DynamicForm.DynamicField>();
            if (this.fields != null) {
                for (MRO_SRV_DynamicForm.DynamicField df : this.fields) {
                    result.put(df.fieldId, df);
                }
            }
            return result;
        }
    }

    public with sharing class DynamicField {
        @AuraEnabled
        public Id sectionId {get; set;}
        @AuraEnabled
        public Id fieldId {get; set;}
        @AuraEnabled
        public String fieldName {get; set;}
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public Integer order {get; set;}
        @AuraEnabled
        public Boolean isReadOnly {get; set;}
        @AuraEnabled
        public Boolean isRequired {get; set;}
        @AuraEnabled
        public Boolean isDisabled {get; set;}
        @AuraEnabled
        public Id inheritsFromFieldId {get; set;}

        public DynamicField(DynamicField__c df) {
            this.sectionId = df.Section__c;
            this.fieldId = df.Id;
            this.fieldName = df.FieldName__c;
            this.label = df.Label__c;
            this.order = df.Order__c.intValue();
            this.isReadOnly = df.IsReadonly__c;
            this.isRequired = df.IsRequired__c;
            this.isDisabled = df.IsDisabled__c;
            this.inheritsFromFieldId = df.InheritsFromField__c;
        }
    }
}