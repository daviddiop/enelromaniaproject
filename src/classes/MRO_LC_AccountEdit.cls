/**
 * Created by napoli on 19/11/2019.
 */

public with sharing class MRO_LC_AccountEdit extends ApexServiceLibraryCnt {
    private static final MRO_SRV_Account accountSrv = MRO_SRV_Account.getInstance();
    private static final MRO_QR_Account accountQuerySrv = MRO_QR_Account.getInstance();
    private static final MRO_SRV_Interaction interactionSrv = MRO_SRV_Interaction.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static MRO_QR_Interaction interactionQuery = MRO_QR_Interaction.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static ContactQueries contactQuery = ContactQueries.getInstance();
    private static AccountContactRelationQueries accountContactRelationQuery = AccountContactRelationQueries.getInstance();

    private static CustomerInteractionService customerInteractionSrv = CustomerInteractionService.getInstance();
    private static MRO_SRV_Contact contactSrv = MRO_SRV_Contact.getInstance();
    private static AccountContactRelationService accountContactRelationSrv = AccountContactRelationService.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();

    public with sharing class getOptionList extends AuraCallable {
        public override Object perform ( final String jsonInput ){
            Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
            Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');
            Schema.RecordTypeInfo businessRt = accountRts.get('BusinessProspect');
            return new Map<String,Object> {
                    'personRecordType'=> new Map<String,String>{
                            'label' => personRt.getName(),
                            'value' => personRt.getRecordTypeId()
                    },
                    'businessRecordType'=> new Map<String,String>{
                            'label' => businessRt.getName(),
                            'value' => businessRt.getRecordTypeId()
                    },
                    'roleOptionList' => interactionSrv.listOptions(AccountContactRelation.Roles)
            };
        }
    }

    public with sharing class saveAccount extends AuraCallable {
        public override Object perform ( final String jsonInput ){
            Map<String, String> params = asMap(jsonInput);
            String accString = params.get('account');
            String interactionRecord = params.get('interactionRecord');
            String roles = params.get('roles');
            String recordTypeId = params.get('recordTypeId');
            Account accountObject = (Account)JSON.deserialize(accString, Account.class);
            return accountSrv.createCustomer(accountObject, interactionRecord, roles, recordTypeId);
        }
    }

    public with sharing class retrieveAccount extends AuraCallable{
        public override Object perform (final String jsonInput){
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            return accountQuerySrv.findAccount(accountId);
        }
    }

    public with sharing class updateAccount extends AuraCallable{
        public override  Object perform (final String jsonInput){
            Map<String, String> params = asMap(jsonInput);
            String account = params.get('account');
            Account accountObj = (Account)JSON.deserialize(params.get('account'),Account.class);
            return accountSrv.updateAccount(accountObj);
        }
    }

    public with sharing class isInterlocutorExist extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String customerId = params.get('customerId');
            String interactionId = params.get('interactionId');
            if (String.isBlank(customerId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            Interaction__c interaction = interactionQuery.findInteractionById(interactionId);
            List<CustomerInteraction__c> customerInteractionList = customerInteractionQuery
                    .listCustomerInteractionByInteractionIdAndAccountId(interactionId, customerId);

            String customerInteractionId = null;
            if (!customerInteractionList.isEmpty()) {
                customerInteractionId = customerInteractionList.get(0).Id;
            }

            String contactId;
            if (String.isNotBlank(interaction.Interlocutor__c)) {

                Account myAccount = accountQuery.findAccount(customerId);
                if (myAccount.IsPersonAccount) {
                    if (myAccount.PersonIndividualId == interaction.Interlocutor__c) {
                        contactId = myAccount.PersonContactId;
                    } else {
                        List<Account> individualPersonAccounts = accountQuery.listByIndividualId(interaction.Interlocutor__c);
                        if (individualPersonAccounts.isEmpty()) {
                            Account newPersonAccount = accountSrv.insertPersonalAccount(interaction.Interlocutor__r);
                            newPersonAccount = accountQuery.findAccount(newPersonAccount.Id);
                            contactId = newPersonAccount.PersonContactId;
                            AccountContactRelation relation = accountContactRelationSrv.create(customerId, contactId);
                            databaseSrv.insertSObject(relation);
                        } else {
                            Account existingPersonAccount = individualPersonAccounts[0];
                            contactId = existingPersonAccount.PersonContactId;
                            AccountContactRelation existingRelation = accountContactRelationQuery.findByAccountIdAndContactId(customerId, contactId);
                            if (existingRelation == null) {
                                AccountContactRelation relation = accountContactRelationSrv.create(customerId, contactId);
                                databaseSrv.insertSObject(relation);
                            }
                        }
                    }
                } else {
                    List<Contact> contactList = contactQuery.listContactByIndividualId(interaction.Interlocutor__c, false);
                    if (contactList.isEmpty()) {
                        Contact newContact = contactSrv.insertForIndividual(interaction.Interlocutor__r, customerId);
                        contactId = newContact.Id;
                    } else {
                        Contact individualContact = contactList.get(0);
                        contactId = individualContact.Id;
                        if (individualContact.AccountId == null) {
                            individualContact.AccountId = customerId;
                            databaseSrv.updateSObject(individualContact);
                        } else {
                            AccountContactRelation existingRelation = accountContactRelationQuery.findByAccountIdAndContactId(customerId, contactId);
                            if (existingRelation == null) {
                                AccountContactRelation relation = accountContactRelationSrv.create(customerId, individualContact.Id);
                                databaseSrv.insertSObject(relation);
                            }
                        }
                    }
                }
            }
            if (customerInteractionList.isEmpty()) {
                CustomerInteraction__c customerInteraction = customerInteractionSrv.insertCustomerInteraction(customerId, interactionId, contactId);
                customerInteractionId = customerInteraction.Id;
            }
            String interlocutorIdOrFullName = null;
            if (String.isNotBlank(interaction.Interlocutor__c)) {
                interlocutorIdOrFullName = interaction.Interlocutor__c;
            } else {
                interlocutorIdOrFullName = StringUtils.getEmptyIfNull(interaction.InterlocutorFirstName__c) + StringUtils.getEmptyIfNull(interaction.InterlocutorLastName__c);
            }
            return new Map<String, String>{
                    'customerInteractionId' => customerInteractionId,
                    'interlocutorId' => interlocutorIdOrFullName
            };
        }
    }

//    public with sharing class searchAccount extends AuraCallable {
//        public override Object perform(final String jsonInput) {
//            System.debug(jsonInput);
//            Account accountSearchForm = (Account)JSON.deserialize(jsonInput, Account.class);
//            return accountSrv.searchAccount(accountSearchForm);
//        }
//    }

    public with sharing class InputData {
        @AuraEnabled
        public Account account {get;set;}
        @AuraEnabled
        public String interactionRecord {get;set;}
        @AuraEnabled
        public String roles {get;set;}
        @AuraEnabled
        public String recordTypeId {get;set;}
    }
}