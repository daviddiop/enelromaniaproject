/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   nov 14, 2019
 * @desc    
 * @history 
 */

global with sharing class MRO_TR_PrintActionHandler implements TriggerManager.ISObjectTriggerHandler {
    global void beforeInsert() {
        if (!Test.isRunningTest()) {
            /*
            //Sets Phase description
            ((wrts_prcgvr.Interfaces_1_0.IPhaseManagerUtils) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerUtils'))
                    .bulkSetPhaseDescriptions(new Map<String,Object>{
                        'triggerNew'            =>Trigger.new,
                        'triggerOld'            => Trigger.old,
                        'descriptionFieldName'  => 'PhaseDescription__c'
            });
             */
        }
    }

    global void beforeDelete() {}

    global void beforeUpdate() {
        if (!Test.isRunningTest()) {
            /*
            //Sets Phase description
            ((wrts_prcgvr.Interfaces_1_0.IPhaseManagerUtils) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerUtils'))
                    .bulkSetPhaseDescriptions(new Map<String,Object>{
                        'triggerNew'            =>Trigger.new,
                        'triggerOld'            => Trigger.old,
                        'descriptionFieldName'  => 'PhaseDescription__c'
            });
             */

            //Checks Phase Transition and prepares Activities
            ((wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration'))
                .beforeUpdate(new Map<String, Object>{
                'oldObjects' => Trigger.old,
                'newObjects' => Trigger.new
            });

            //Checks Mandatory Activities
            ((wrts_prcgvr.Interfaces_1_0.IActivityUtils) wrts_prcgvr.VersionManager.newClassInstance('ActivityUtils'))
                .bulkCheckCompleted(new Map<String, Object>{
                'triggerNew' => Trigger.new,
                'triggerOld' => Trigger.old
            });
        }
    }

    global void afterInsert() {}

    global void afterDelete() {}

    global void afterUndelete() {}

    global void afterUpdate() {
        if (!Test.isRunningTest()) {
            //Execute creation Activities
            ((wrts_prcgvr.Interfaces_1_0.IActivityUtils) wrts_prcgvr.VersionManager.newClassInstance('ActivityUtils'))
                .bulkSaveActivityContext(null);

            List<SObject> printActionChangedPhaseNew = new List<SObject>();
            List<SObject> printActionChangedPhaseOld = new List<SObject>();

            for (SObject so : Trigger.new) {
                if (MRO_UTL_ApexContext.FORCE_CALLOUTS_RETRIGGERING || Trigger.oldMap.get(so.Id).get('Phase__c') != so.get('Phase__c')) {
                    printActionChangedPhaseNew.add(so);
                    printActionChangedPhaseOld.add(Trigger.oldMap.get(so.Id));
                }
            }

            //Handles callout (update only)
            ((wrts_prcgvr.Interfaces_1_0.ICalloutUtils) wrts_prcgvr.VersionManager.newClassInstance('CalloutUtils'))
                .bulkSend(new Map<String, Object>{
                'newObjects' => printActionChangedPhaseNew,
                'oldObjects' => printActionChangedPhaseOld
            });
        }
    }
}