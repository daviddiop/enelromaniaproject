/**
 * Created by Moussa Fofana on 14/08/2019.
 */

@isTest
public with sharing class ContractAccountCntTst {
    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        Account account = TestDataFactory.account().personAccount().build();
        insert account;
        
        List<CompanyDivision__c> listCompaniesDivision = new List<CompanyDivision__c>();
        for (Integer i = 0; i < 1; i++) {
            CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(i).build();
            listCompaniesDivision.add(companyDivision);
        }
        insert listCompaniesDivision;
        
        User user = TestDataFactory.user().standardUser().build();
        user.CompanyDivisionId__c = listCompaniesDivision[0].Id;
        insert user;
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '43252435624654';
        servicePoint.Account__c = account.Id;
        servicePoint.Trader__c = account.Id;
        servicePoint.Distributor__c = account.Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;
        
        Contract contract = TestDataFactory.Contract().createContract().build();
        contract.Name = 'Name11062019';
        contract.AccountId = account.Id;
        insert contract;
    
        ContractAccount__c contractAccount = TestDataFactory.contractAccount().createContractAccount().setAccount(account.Id).build();
        insert  contractAccount;
        
        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(listCompaniesDivision[0].Id).build();
        supply.Contract__c = contract.Id;
        supply.Account__c = account.Id;
        //supply.ServicePoint__c = servicePoint.Id;
        insert supply;
    }
    
    @isTest
    static void getActiveSuppliesTest() {
        ContractAccount__c contractAccount = [
        SELECT  Id, Name
        FROM ContractAccount__c
        LIMIT 1
        ];
        
        Map<String, String> jsonInput = new Map<String, String>{
            'contractAccountId' => contractAccount.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('ContractAccountCnt', 'getActiveSupplies', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true , result.get('enableEditButtonCA') == true);
        Test.stopTest();
    
    }
    
    @isTest
    static void getActiveSuppliesExceptionTest() {
        
        Map<String, String> jsonInput = new Map<String, String>{
            'contractAccountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('ContractAccountCnt', 'getActiveSupplies', jsonInput, false);
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    
    }
    
    @isTest
    static void getContractAccountRecordsTest() {
        Account account = [
            SELECT  Id, Name
            FROM Account
            LIMIT 1
        ];
        
        Map<String, String> jsonInput = new Map<String, String>{
            'accountId' => account.Id
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('ContractAccountCnt', 'getContractAccountRecords', jsonInput, false);

        }catch (Exception e){
            System.assert(true , e.getMessage());
        }
        Test.stopTest();
    
    }
    
    @isTest
    static void getContractAccountRecordsExceptionTest() {
        Map<String, String> jsonInput = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('ContractAccountCnt', 'getContractAccountRecords', jsonInput, false);
        
        }catch (Exception e){
            System.assert(true , e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void getContractAccountTest() {
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];
        Map<String, String> jsonInput = new Map<String, String>{
            'accountId' => account.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('ContractAccountCnt', 'getContractAccountRecords', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true , result.get('listContractAccount') != null);
        Test.stopTest();
    }

}