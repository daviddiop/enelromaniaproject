/**
 * Created by goudiaby on 13/03/2019.
 */

public with sharing class IndividualQueries {

    public static IndividualQueries getInstance() {
        return new IndividualQueries();
    }

    /**
     * Query to get a list of individuals for the given National Identity Number
     * @param NINumber National Identity Number
     * @return A list of Individuals objects
     */
    public List<Individual> findIndividualsByNINumber(String NINumber) {
        return [
                SELECT Id, Name, FirstName, LastName, NationalIdentityNumber__c
                FROM Individual
                WHERE NationalIdentityNumber__c =:NINumber
        ];
    }

    public Individual findById(String individualId) {
        List<Individual> individualList = [
            SELECT Id, Name, FirstName, LastName, NationalIdentityNumber__c
            FROM Individual
            WHERE Id = :individualId
        ];
        if (individualList.isEmpty()) {
            return null;
        }
        return individualList.get(0);
    }
}