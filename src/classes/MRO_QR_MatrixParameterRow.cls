public with sharing class MRO_QR_MatrixParameterRow {

    public static MRO_QR_MatrixParameterRow getInstance() {
        return (MRO_QR_MatrixParameterRow)ServiceLocator.getInstance(MRO_QR_MatrixParameterRow.class);
    }

    public List<NE__Matrix_Parameter_Row__c> listByProductCodesWithMultiPriceOrBonus(Set<String> productCodes) {
        return [
            SELECT Id, Name, MPRProductCode__c, ProductCode__c, Multiprice__c, DeliveryPrice__c, InstallationPrice__c, LDNightPrice__c,
                WeekendNightPrice__c, DayPrice__c, MonthlyFee__c, MonthlyCommission__c, PriceValue__c, Subscription__c, BonusValue__c,
                BonusMonth__c, MultipriceStartDate__c, MultipriceEndDate__c
            FROM NE__Matrix_Parameter_Row__c
            WHERE (MPRProductCode__c IN :productCodes OR ProductCode__c IN :productCodes)
                AND (Multiprice__c = true OR BonusValue__c != null)
        ];
    }
}