/**
 * Created by BADJI on 26/05/2019.
 * Modified by Moussa on 21/06/2019.
 */

@isTest
public with sharing class ActivationWizardCntTst {
    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        /*User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;
        update user;*/
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.BusinessType__c = 'NGO';
        List<Account> listAccount = new list<Account>();
        listAccount.add(businessAccount);
        insert listAccount;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        insert opportunity;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        interaction.Channel__c = 'MyEnel';
        insert interaction;

        Contact contact = TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[0].Id;
        contact.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert contact;

        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        City__c city = new City__c(Name = 'JIMBOLIA', Province__c = 'TIMIS', Country__c = 'ROMANIA');
        insert city;
        Street__c street = MRO_UTL_TestDataFactory.street().createBuilder().build();
        street.City__c = city.id;
        insert street;

        OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        opportunityServiceItem.PointStreetId__c = street.id;
        insert opportunityServiceItem;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[0].Id;
        //billingProfile.IBAN__c = 'IT88A0123456789012345678901';
        insert billingProfile;

        Product2 product2 =TestDataFactory.product2().build();
        insert product2;

        PricebookEntry pricebookEntry = TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = product2.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityID = opportunity.Id;
        opportunityLineItem.Product2ID = product2.Id;
        opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;
    }

    /*@isTest
    public static void initializeTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id,StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => opp.Id,
            'customerInteractionId ' => customerInteraction.Id
        };
        User user = new User();
        user.Id = UserInfo.getUserId() ;
        String companyDivisionId = [
            SELECT Id,Name 
            FROM CompanyDivision__c 
            WHERE Name = 'ENEL 1' 
            LIMIT 1
        ].Id;
        user.CompanyDivisionId__c = companyDivisionId;
        update user; 
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('opportunityId') == opp.Id);
    }*/

    @isTest
    public static void initializeTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id,StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => opp.Id,
            'customerInteractionId ' => customerInteraction.Id
        };


            Test.startTest();
            Object response = TestUtils.exec('ActivationWizardCnt', 'initialize', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            Test.stopTest();
            System.assertEquals(true, result.get('opportunityId') == opp.Id);
    }


    @isTest
    public static void initializeEmptyOpportunityIdTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        String opportinutyId = '';
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => opportinutyId,
            'customerInteractionId ' => customerInteraction.Id
        };

        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ].Id;
        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivisionId;
        update user; 
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('opportunityId') != opportinutyId);
    }

	
    @isTest
    public static void initializeExceptionTest(){
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id,StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '', 
            'opportunityId' => opp.Id, 
            'customerInteraction' => customerInteraction.Id  
        };
        Test.startTest();
        try {
            TestUtils.exec('ActivationWizardCnt', 'initialize', inputJSON, false);        
        } catch (Exception e) {
                System.assert(true, e.getMessage());              
        }
        Test.stopTest();
    }

    @isTest
    static void updateOsiListTest() {
        List<OpportunityServiceItem__c> listOppServiceItem = [
                SELECT Id,ServicePointCode__c
                FROM OpportunityServiceItem__c
                LIMIT 10
        ];

        ActivationWizardCnt.InputData inputData = new ActivationWizardCnt.InputData();
        inputData.opportunityServiceItems = listOppServiceItem;

        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'updateOsiList', inputData, true);
        Test.stopTest();

        System.assertEquals(null, response);
    }

    @isTest
    public static void CheckOsiTest(){
        OpportunityServiceItem__c osi =[
            SELECT Id, BillingProfile__c, Distributor__c, Trader__c, Account__c, PointStreetId__c
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' =>osi.Id
        };
        System.debug('##########PointStreetId__c'+osi.PointStreetId__c);
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'CheckOsi', inputJSON, true);
        System.debug('###==========>### '+response);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assert(result.get('opportunityServiceItem') != null);
        Test.stopTest();
    }

    @isTest
    public static void CheckOsiExceptionTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' =>''
        };        
        try {
        TestUtils.exec('ActivationWizardCnt', 'CheckOsi', inputJSON, false);
        } catch (Exception e) {
                System.assert(true, e.getMessage());   
        }
        Test.stopTest();
    }


    @isTest
    public static void updateOpportunityTest(){
        Opportunity opp = [
            SELECT Id,StageName
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' =>opp.Id, 
            'stage' => 'Closed Won'
        };        
        Test.startTest();
        Object response =  TestUtils.exec('ActivationWizardCnt', 'updateOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('opportunityId') == opp.Id);
    }

    @isTest
    public static void updateOpportunityExceptionTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' =>'', 
            'stage' => 'Closed Won'
        };        
        try {
        TestUtils.exec('ActivationWizardCnt', 'updateOpportunity', inputJSON, false);         
        } catch (Exception e) {
                System.assert(true, e.getMessage());   
        }
        Test.stopTest();
    }



    @isTest
    public static void linkOliToOsiTest(){
        List<OpportunityServiceItem__c>  listOsi = [
            SELECT Id, BillingProfile__c
            FROM OpportunityServiceItem__c
            LIMIT 1
        ];
        OpportunityLineItem oli = [
            SELECT Id,Product2Id
            FROM OpportunityLineItem
            LIMIT 1
        ];
        ActivationWizardCnt.InputData inputData = new ActivationWizardCnt.InputData();
        inputData.opportunityServiceItems = listOsi;
        inputData.oli = oli;
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }
    @isTest
    public static void linkOliToOsiExTest(){
        List<OpportunityServiceItem__c>  listOsi = [
                SELECT Id, BillingProfile__c
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];
        OpportunityLineItem oli = [
                SELECT Id,Product2Id
                FROM OpportunityLineItem
                LIMIT 1
        ];
        ActivationWizardCnt.InputData inputData = new ActivationWizardCnt.InputData();
        inputData.opportunityServiceItems = null;
        inputData.oli = null;
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }

    @isTest
    static void updateCompanyDivisionInOpportunityTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }
    @isTest
    static void updateCompanyDivisionInOpportunityExTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => null,
                'companyDivisionId' => companyId
        };
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }
    @isTest
    static void updateContractAndContractSignedDateOnOpportunityTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id,
                'customerSignedDate' => String.valueOf(opportunity.ContractSignedDate__c),
                'contractId' => opportunity.ContractId


        };
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'updateContractAndContractSignedDateOnOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }
    @isTest
    static void updateContractAndContractSignedDateOnOpportunityErrorTest() {
        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;
        String companyId = [
                SELECT Id
                FROM CompanyDivision__c
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => null,
                'customerSignedDate' => String.valueOf(opportunity.ContractSignedDate__c),
                'contractId' => opportunity.ContractId


        };
        Test.startTest();
        Object response = TestUtils.exec('ActivationWizardCnt', 'updateContractAndContractSignedDateOnOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
    }
}