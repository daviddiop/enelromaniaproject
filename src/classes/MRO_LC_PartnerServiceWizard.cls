public with sharing class MRO_LC_PartnerServiceWizard extends ApexServiceLibraryCnt {
    private static MRO_QR_Opportunity opportunityQuery = MRO_QR_Opportunity.getInstance();
    private static OpportunityLineItemQueries oliQuery = OpportunityLineItemQueries.getInstance();
    private static MRO_QR_OpportunityServiceItem osiQuery = MRO_QR_OpportunityServiceItem.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static UserQueries userQuery = UserQueries.getInstance();
    private static MRO_SRV_Opportunity oppService = MRO_SRV_Opportunity.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static PrivacyChangeQueries privacyChangeQuery = PrivacyChangeQueries.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static DossierService dossiersrv = DossierService.getInstance();
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_SRV_VAS vasService = new MRO_SRV_VAS();
    private static MRO_SRV_Case caseService = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static final MRO_UTL_Parsers parsers = MRO_UTL_Parsers.getInstance();

    public class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.cancelEverything(jsonInput);
        }
    }

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.wizardInit(jsonInput, constantsSrv.REQUEST_TYPE_PARTNER_SERVICE);
        }
    }

    public with sharing class processSelectedOli extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.processSelectedOli(jsonInput);
        }
    }

    public with sharing class setInitiativeType extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id dossierId = params.get('dossierId');
            String startedBy = params.get('startedBy');

            Savepoint sp = Database.setSavepoint();
            try {
                Dossier__c dossier = MRO_QR_Dossier.getInstance().getById(dossierId);
                dossier.StartedBy__c = startedBy;
                databaseSrv.updateSObject(dossier);

                response.put('startedBy', startedBy);
                response.put('error', false);
            } catch (Exception ex){
                Database.rollback(sp);
                System.debug(ex.getMessage());
                System.debug(ex.getStackTraceString());

                response.put('error', true);
            }

            return response;
        }
    }

    public with sharing class updateCompanyDivisionInOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.updateCompanyDivision(jsonInput);
        }
    }

    public with sharing class updateContractInfo extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.updateContractInfo(jsonInput);
        }
    }

    public with sharing class afterNewOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.afterOSI(jsonInput, constantsSrv.REQUEST_TYPE_PARTNER_SERVICE);
        }
    }

    public with sharing class checkCommoditySupply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.checkCommoditySupply(jsonInput);
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.setChannelAndOrigin(jsonInput);
        }
    }

    public with sharing class updateCurrentFlow extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.updateCurrentFlow(jsonInput);
        }
    }

    // This is for calls from supplyShortInfo lwc
    public with sharing class getContractById extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id contractId = params.get('contractId');
            System.debug('contractId ' + contractId);

            Contract contract = contractQuery.getById(contractId);
            if (contract == null) {
                throw new WrtsException(System.Label.UnexpectedError + ' in getContractById');
            }

            Integer contractTerm = Integer.valueOf(contract.ContractTerm);
            Date contractEndDate = contract.StartDate.addMonths(contractTerm) - 1;

            response.put('contractEndDate', contractEndDate);
            response.put('contract', contract);
            response.put('error', false);
            return response;
        }
    }

    public with sharing class handleServiceSupplySelection extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.handleServiceSupplySelection(jsonInput);
        }
    }

    private static Id getDistributor(Id contractId){
        System.debug('getDistributor');
        Contract contract = contractQuery.getById(contractId);
        Id commodityProductId = contract.CommodityProduct__c;
        Product2 commodityProduct = ProductQueries.getInstance().getById(commodityProductId);

        System.debug(commodityProduct);
        return commodityProduct.Id;
    }

    private static Asset insertAdditionalInfo(Opportunity opportunity, Supply__c supply, String additionalInfo){
        if (!String.isBlank(additionalInfo) && additionalInfo != 'null') {
            Map<String, Object> additionalInformation = (Map<String, Object>)json.deserializeUntyped(additionalInfo);

            String infoField1 = null;
            Date manufacturingDate = null;

            if (additionalInformation.get('infoFieldDate') != null && String.isNotBlank(additionalInformation.get('infoFieldDate').toString()))
            {
                manufacturingDate = Date.valueOf(additionalInformation.get('infoFieldDate').toString());
                infoField1 = additionalInformation.get('infoField1').toString();
            }

            String infoField2 = additionalInformation.get('infoField2').toString();
            String infoField3 = additionalInformation.get('infoField3').toString();
            String infoField4 = additionalInformation.get('infoField4').toString();
            String assetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();
            Asset asset = new Asset(
                Name = opportunity.Id,
                AccountId = supply.Account__c,
                ContactId = supply.Contact__c,
                NE__Text_Extension_1__c = infoField1,
                NE__Text_Extension_2__c = infoField2,
                NE__Text_Extension_3__c = infoField3,
                NE__Text_Extension_4__c = infoField4,
                IsExternalProduct__c = true,
                ManufacturingDate__c = manufacturingDate,
                RecordTypeId = assetRecordTypeId
            );
            databaseSrv.insertSObject(asset);
            asset = MRO_QR_Asset.getInstance().getById(asset.Id);
            return asset;
        } else {
            return null;
        }
    }

    private static void softVasSubstitution(Id opportunityId, Id dossierId, Map<String, Object> opportunityParams) {
        System.debug('softVasSubstitution');

        Id existingSupplyId = parsers.parseId(opportunityParams.get('selectedToTerminateSupplyId'));
        if (existingSupplyId == null) {
            System.debug(LoggingLevel.ERROR, 'SupplyId is null');
            return;
        }

        Supply__c existingSupply = supplyQuery.getById(existingSupplyId);
        if (existingSupply == null) {
            System.debug(LoggingLevel.ERROR, 'Supply is null');
            return;
        }

        if (existingSupply.Activator__c == null) {
            throw new WrtsException('Selected VAS doesn\'t have a valid activation case');
        }
        Case existingSupplyActivator = MRO_QR_Case.getInstance().getById(existingSupply.Activator__c);
        Dossier__c existingSupplyDossier = dossierQuery.getById(existingSupplyActivator.Dossier__c);

        // Get the newly selected product
        Opportunity opportunity = MRO_QR_Opportunity.getInstance().getById(opportunityId);

        Id contractId = opportunity.ContractId;
        Contract newContract = contractQuery.getById(contractId);

        OpportunityLineItem oli = opportunity.OpportunityLineItems.get(0);
        Id productId = oli.Product2Id;

        // Set the new product Id to OSI
        List<OpportunityServiceItem__c> osiList = osiQuery.getOSIsByOpportunityId(opportunity.Id);
        if (osiList.size() == 0){
            return ;
        }
        OpportunityServiceItem__c osi = osiList.get(0);
        osi.Product__c = productId;
        databaseSrv.updateSObject(osi);

        // Set old contract's status to "Terminating" before linking with the new contract
        Contract oldContract = contractQuery.getById(existingSupply.Contract__c);
        if (oldContract != null) {
            oldContract.Status = constantsSrv.CONTRACT_STATUS_TERMINATED;
        }
        databaseSrv.updateSObject(oldContract);

        Dossier__c dossier = dossierQuery.getById(dossierId);

        Case substitutionCase = caseService.instantiatePrefilled(opportunity, null, newContract.Id, dossierId);
        substitutionCase.RecordTypeId = vasService.getVasCaseRecordTypeId(opportunity);
        substitutionCase.ContactId = (opportunity.CustomerInteraction__r != null) ? opportunity.CustomerInteraction__r.Contact__c : null;
        substitutionCase.Contract2__c = oldContract.Id;
        substitutionCase.Contract__c = newContract.Id;
        substitutionCase.Dossier__c = dossierId;
        substitutionCase.Supply__c = existingSupply.Id;
        substitutionCase.SubProcess__c = constantsSrv.PARTNER_SERVICE_VAS_SUBSTITUTION;
        substitutionCase.CaseTypeCode__c = constantsSrv.CASETYPE_REZ_VAS;
        substitutionCase.SLAExpirationDate__c = Date.today().addDays(constantsSrv.PARTNER_SERVICE_SLA_EXPIRATION_OFFSET);
        substitutionCase.EffectiveDate__c = vasService.computeVasActivationDate();

        // save additional information as an Asset
        Asset asset = insertAdditionalInfo(opportunity, existingSupply, (String)opportunityParams.get('additionalInfo'));
        if (asset != null){
            asset.Name = asset.AssetNumber__c;
            databaseSrv.updateSObject(asset);
            substitutionCase.AssetId = asset.Id;
        }

        databaseSrv.insertSObject(substitutionCase);

        substitutionCase = MRO_QR_Case.getInstance().getById(substitutionCase.Id);

        // Link to the existingSupply new contract and case
        existingSupply.Activator__c = substitutionCase.Id;
        existingSupply.Contract__c = newContract.Id;
        existingSupply.Product__c = productId;
        databaseSrv.updateSObject(existingSupply);

        // Update dossier
        dossier.RecordTypeId = vasService.getVasDossierRecordTypeId(opportunity);
        dossier.Status__c = constantsSrv.DOSSIER_STATUS_NEW;
        dossier.CommercialProduct__c = oli.Product2.CommercialProduct__c;
        dossier.Commodity__c = existingSupplyDossier.Commodity__c;
        databaseSrv.updateSObject(dossier);

        // Update contract
        newContract.ContractTerm = vasService.getContractTerm(productId);
        newContract.Name = substitutionCase.CaseNumber;
        newContract.StartDate = vasService.computeVasActivationDate();
        newContract.ProductStartDate__c = vasService.computeVasActivationDate();
        newContract.Status = constantsSrv.CONTRACT_STATUS_ACTIVATED;
        databaseSrv.updateSObject(newContract);

        caseService.setDistributor(substitutionCase, dossier, existingSupply.ServiceSite__c);
        caseService.applyAutomaticTransitionOnDossierAndCases(dossierId);

        System.debug('softVasSubstitution - ok');
    }

    private static void softVasActivation(Opportunity opportunity, Id dossierId, Map<String, Object> opportunityParams) {
        System.debug('softVasActivation');
        Id contractId = opportunity.ContractId;
        Contract contract = contractQuery.getById(contractId);

        // Check OSI
        List<OpportunityServiceItem__c> osiList = osiQuery.getOSIsByOpportunityId(opportunity.Id);
        if (osiList.size() == 0){
            return;
        }
        OpportunityServiceItem__c osi = osiList.get(0);

        // Check Contract
        if (contract.ContractType__c == null) {
            System.debug(LoggingLevel.ERROR, 'contract fields are not filled');
            System.debug(contract.ContractType__c);
            return;
        }

        List<Supply__c> supplies = supplyQuery.listByContractIds(new Set<Id>{contract.Id});
        if (supplies.size() == 0) {
            System.debug(LoggingLevel.ERROR, 'no supplies have been found for contract ' + contract.Id);
            return;
        }
        Supply__c supply = supplies.get(0);

        // Start date is the first day of next month
        Date contractStartDate = Date.today();
        contractStartDate = contractStartDate.addMonths(1).toStartOfMonth();

        Id productId = opportunity.OpportunityLineItems.get(0).Product2Id;
        Integer contractTerm = vasService.getContractTerm(productId);
        Date contractEndDate = contractStartDate.addMonths(contractTerm);

        if (contractStartDate == null || contractEndDate == null) {
            System.debug(LoggingLevel.ERROR, 'contract dates are null');
            return;
        }

        Supply__c commoditySupply = supplyQuery.getById(osi.Supply__c);

        // Check Case
        Id activatorCaseRecordTypeId = vasService.getVasCaseRecordTypeId(opportunity);
        List<Case> cases = caseQuery.listByContractAndRecordType(contract.Id, activatorCaseRecordTypeId);
        if (cases.isEmpty()) {
            System.debug(LoggingLevel.ERROR, 'listByContractIdAndRecordTypeId is empty');
            return;
        }
        Case activatorCase = cases.get(0);

        activatorCase.SubProcess__c = constantsSrv.PARTNER_SERVICE_VAS_ACTIVATION;
        activatorCase.CaseTypeCode__c = constantsSrv.CASETYPE_ACT_VAS;
        activatorCase.SLAExpirationDate__c = Date.today().addDays(constantsSrv.PARTNER_SERVICE_SLA_EXPIRATION_OFFSET);
        activatorCase.Supply__c = supply.Id;
        activatorCase.CompanyDivision__c = opportunity.CompanyDivision__c;
        activatorCase.EffectiveDate__c = vasService.computeVasActivationDate();
        if (commoditySupply != null && osi.ServiceSite__r != null) {
            if (commoditySupply.RecordType.DeveloperName == constantsSrv.COMMODITY_ELECTRIC) {
                if (osi.ServiceSite__r.ActiveSuppliesCountELE__c == null || osi.ServiceSite__r.ActiveSuppliesCountELE__c < 1) {
                    activatorCase.Status = constantsSrv.CASE_STATUS_ONHOLD;
                }
            } else if (commoditySupply.RecordType.DeveloperName == constantsSrv.COMMODITY_GAS) {
                if (osi.ServiceSite__r.ActiveSuppliesCountGAS__c == null || osi.ServiceSite__r.ActiveSuppliesCountGAS__c < 1) {
                    activatorCase.Status = constantsSrv.CASE_STATUS_ONHOLD;
                }
            }
        }

        // save additional information as an Asset
        Asset asset = insertAdditionalInfo(opportunity, supply, (String)opportunityParams.get('additionalInfo'));
        if (asset != null){
            asset.Name = asset.AssetNumber__c;
            databaseSrv.updateSObject(asset);
            activatorCase.AssetId = asset.Id;
        }
        databaseSrv.updateSObject(activatorCase);

        // Status of this supply will be set to Active once its phase is set to BL010
        supply.Activator__c = activatorCase.Id;
        supply.Market__c = 'Free';
        supply.Status__c = constantsSrv.SUPPLY_STATUS_ACTIVATING;
        supply.ServicePoint__c = null;
        supply.ServiceSite__c = osi.ServiceSite__c;
        supply.Product__c = productId;

        // Copy Nace reference
        if (commoditySupply != null){
            supply.NACEReference__c = commoditySupply.NACEReference__c;
        }

        databaseSrv.updateSObject(supply);

        OpportunityLineItem oli = opportunity.OpportunityLineItems.get(0);

        // Update dossier
        Dossier__c dossier = dossierQuery.getById(dossierId);
        dossier.RecordTypeId = vasService.getVasDossierRecordTypeId(opportunity);
        dossier.Status__c = constantsSrv.DOSSIER_STATUS_NEW;
        dossier.CommercialProduct__c = oli.Product2.CommercialProduct__c;
        databaseSrv.updateSObject(dossier);

        // Update contract info
        // Status of this contract will be set to Activated once its phase is set to BL010
        contract.ContractTerm = contractTerm;
        contract.Name = activatorCase.CaseNumber;
        contract.StartDate = contractStartDate;
        contract.Status = constantssrv.CONTRACT_STATUS_DRAFT;
        contract.ProductStartDate__c = contractStartDate;
        databaseSrv.updateSObject(contract);

        caseService.setDistributor(activatorCase, dossier, supply.ServiceSite__c);
        caseService.applyAutomaticTransitionOnDossierAndCases(dossierId);
        System.debug('softVasActivation - ok');
    }

    private static void softVasTermination(Opportunity opportunity, Id dossierId, Boolean appliedPenalties, Map<String, Object> opportunityParams) {
        System.debug('vasTermination');

        Id selectedToTerminateSupplyId = parsers.parseId(opportunityParams.get('selectedToTerminateSupplyId'));
        if (selectedToTerminateSupplyId == null) {
            System.debug('SupplyId is null');
            return;
        }

        Supply__c supplyToTerminate = supplyQuery.getById(selectedToTerminateSupplyId);
        if (supplyToTerminate == null) {
            System.debug('Supply to terminate is null');
            return;
        }

        if (supplyToTerminate.Activator__c == null) {
            throw new WrtsException('Selected VAS doesn\'t have a valid activation case');
        }
        Case supplyToTerminateActivator = MRO_QR_Case.getInstance().getById(supplyToTerminate.Activator__c);
        Dossier__c supplyToTerminateDossier = dossierQuery.getById(supplyToTerminateActivator.Dossier__c);

        // Get termination reason
        String terminationReason = (String)opportunityParams.get('terminationReason');

        // Create the case
        Case terminator = caseService.instantiatePrefilled(opportunity, null, null, dossierId);

        // Copy relevant info from activation case
        terminator.ContactId = supplyToTerminateActivator.ContactId;

        // Fill the case with necessary information
        terminator.RecordTypeId = vasService.getVasCaseRecordTypeId(opportunity);
        terminator.Dossier__c = dossierId;
        terminator.Reason__c = terminationReason.trim();
        terminator.Supply__c = supplyToTerminate.Id;
        terminator.SubProcess__c = constantsSrv.PARTNER_SERVICE_VAS_TERMINATION;
        terminator.CaseTypeCode__c = constantsSrv.CASETYPE_REZ_VAS;
        terminator.SLAExpirationDate__c = Date.today().addDays(constantsSrv.PARTNER_SERVICE_SLA_EXPIRATION_OFFSET);
        terminator.Penalty__c = appliedPenalties;

        ServiceSite__c serviceSite = MRO_QR_ServiceSite.getInstance().getById(supplyToTerminate.ServiceSite__c);
        // TODO: set Distributor for case

        // Set termination date to last day of this month
        terminator.EffectiveDate__c = vasService.computeVasTerminationDate();

        databaseSrv.insertSObject(terminator);
        System.debug('terminator is inserted');

        // Update terminating supply info
        terminator = MRO_QR_Case.getInstance().getById(terminator.Id);
        supplyToTerminate.Terminator__c = terminator.Id;
        supplyToTerminate.TerminationReason__c = terminationReason;
        supplyToTerminate.Status__c = constantsSrv.SUPPLY_STATUS_TERMINATING;
        databaseSrv.updateSObject(supplyToTerminate);

        // Update contract status
        Contract contract = contractQuery.getById(supplyToTerminate.Contract__c);
        contract.Status = constantsSrv.CONTRACT_STATUS_TERMINATED;
        databaseSrv.updateSObject(contract);

        // Update dossier
        Dossier__c dossier = dossierQuery.getById(dossierId);
        dossier.RecordTypeId = vasService.getVasDossierRecordTypeId(opportunity);
        dossier.Status__c = constantsSrv.DOSSIER_STATUS_NEW;
        dossier.Commodity__c = supplyToTerminateDossier.Commodity__c;
        dossier.CompanyDivision__c = supplyToTerminate.CompanyDivision__c;
        databaseSrv.updateSObject(dossier);

        caseService.setDistributor(terminator, dossier, supplyToTerminate.ServiceSite__c);
        caseService.applyAutomaticTransitionOnDossierAndCases(dossierId);

        System.debug('Vas Termination - Done');
    }

    public with sharing class appliedPenalties extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('appliedPenalties');

            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            response.put('appliedPenalties', false);

            String supplyToTerminateId = params.get('supplyToTerminateId');
            String dossierId = params.get('dossierId');
            String currentFlow = params.get('currentFlow');

            if (currentFlow != constantsSrv.PARTNER_SERVICE_VAS_TERMINATION) {
                System.debug('currentFlow is not temination');
                return response;
            }

            if (String.isBlank(dossierId)) {
                System.debug('dossierId is null');
                return response;
            }

            Dossier__c dossier = dossierQuery.getById(dossierId);
            Supply__c supply = supplyQuery.getById(supplyToTerminateId);
            Contract contract = contractQuery.getById(supply.Contract__c);

            if (String.isBlank(supply.Contract__c)) {
                System.debug('contract id is null');
                return response;
            }

            if (String.isBlank(supply.Product__c)){
                System.debug('supply.Product__c is blank');
                return response;
            }

            Product2 product = MRO_QR_Product.getInstance().getById(supply.Product__c);

            Date contractStartDate = contract.StartDate;
            if (contractStartDate == null) {
                System.debug('contractStartDate is blank');
                return response;
            }

            Integer productTerm = 12;
            Decimal actualProductTerm = product.CommercialProduct__r.ProductTerm__c;
            if (actualProductTerm != null){
                productTerm = Integer.valueOf(actualProductTerm);
            }

            Date contractEndDate = contractStartDate.addMonths(productTerm);
            contractStartDate.addMonths(contract.ContractTerm);

            // e.g. 2020.05.14
            Date firstMonthAfterActivation = Date.newInstance(contractStartDate.year(), contractStartDate.month() + 1, contractStartDate.day());

            // e.g. 2020.06.01
            Date terminationRequestDate = Date.today();

            Id complaintDossierId = dossier.Parent__c;
            if (String.isBlank(complaintDossierId)) {
                if ((terminationRequestDate <= firstMonthAfterActivation) || (terminationRequestDate > contractEndDate)) {
                    // No penalties applied
                    response.put('monthsWithPenalties', 0);
                    System.debug('no penalties');
                    return response;
                }

                if (terminationRequestDate > firstMonthAfterActivation && terminationRequestDate <= contractEndDate) {
                    // Penalties will be applied
                    System.debug('with penalties');

                    response.put('appliedPenalties', true);

                    Integer monthsBetween = vasService.monthsBetween(Date.today(), contractEndDate);
                    response.put('monthsWithPenalties', monthsBetween);
                    return response;
                }
            }

            if (String.isBlank(complaintDossierId)) {
                System.debug('empty complainDossierId');
                return response;
            }

            // Search for complaint case
            List<Case> complaintCases = MRO_QR_Case.getInstance().getCasesByDossierId(complaintDossierId);

            // Only one case must be associated with a complaint dossier
            if (complaintCases.size() != 1) {
                System.debug('complaintCases.size is ' + complaintCases.size());
                return response;
            }

            Case complaintCase = complaintCases[0];
            if (complaintCase.Outcome__c == constantsSrv.CASE_OUTCOME_UNJUSTIFIED) {
                // Penalties will be applied
                return response;
            }

            return response;
        }
    }

    public with sharing class updateOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String opportunityId = params.get('opportunityId');
            String additionalInfo = params.get('additionalInfo');
            String terminationReason = params.get('terminationReason');
            String currentFlow = params.get('currentFlow');
            String dossierId = params.get('dossierId');
            String selectedToTerminateSupplyId = params.get('selectedToTerminateSupplyId');
            String stage = params.get('stage');

            Boolean appliedPenalties = false;
            if (String.isNotBlank(params.get('appliedPenalties'))){
                if (params.get('appliedPenalties') == 'true'){
                    appliedPenalties = true;
                }
            }

            Boolean isActivation = currentFlow == constantsSrv.PARTNER_SERVICE_VAS_ACTIVATION;
            Boolean isTermination = currentFlow == constantsSrv.PARTNER_SERVICE_VAS_TERMINATION;
            Boolean isSubstitution = currentFlow == constantsSrv.PARTNER_SERVICE_VAS_SUBSTITUTION;

            Savepoint sp = Database.setSavepoint();
            try {
                // Update opportunity details
                Opportunity opp = MRO_QR_Opportunity.getInstance().getById(opportunityId);
                opp.StageName = stage;
                opp.Type = 'New Business';
                databaseSrv.updateSObject(opp);

                if (stage == constantsSrv.OPPORTUNITY_STAGE_QUOTED) {
                    Map<String, String> opportunityParams = new Map<String, String>();
                    opportunityParams.put('currentFlow', currentFlow);
                    opportunityParams.put('additionalInfo', additionalInfo);
                    opportunityParams.put('selectedToTerminateSupplyId', selectedToTerminateSupplyId);
                    opportunityParams.put('terminationReason', terminationReason);

                    if (isActivation) {
                        vasService.runAcquisitionChain(opp, dossierId);
                        softVasActivation(opp, dossierId, opportunityParams);
                    }

                    if (isTermination) {
                        softVasTermination(opp, dossierId, appliedPenalties, opportunityParams);
                    }

                    if (isSubstitution) {
                        softVasSubstitution(opportunityId, dossierId, opportunityParams);
                    }
                }
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                System.debug(ex.getMessage());
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}