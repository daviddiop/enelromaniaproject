/**
 * Created by Roberto Tomasoni on 11/07/2019.
 * Updated by Baba Goudiaby on 22/07/2019
 */

public with sharing class SupplyCnt extends ApexServiceLibraryCnt {

    static UserQueries userQuery = UserQueries.getInstance();
    static User userInformation = userQuery.getCompanyDivisionId(UserInfo.getUserId());
    static SupplyService supplyServices = SupplyService.getInstance();
    static SupplyQueries supplyQuery = SupplyQueries.getInstance();

    public static SupplyCnt getInstance() {
        return new SupplyCnt();
    }

/*
    public class getSupplyTile extends AuraCallable{
        public override Object perform(final String jsonInput){
            Map<String, Object> response = new Map<String, Object>();

            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('recordId');
            if (String.isBlank(supplyId)) {
                throw new WrtsException(System.Label.Supply + ' - ' + System.Label.MissingId);
            }
            Supply__c supply = supplyQuery.getBillingProfileBySupplyId(supplyId);
            response.put('supplyData', supply);

            return response;
        }
    }*/
    public with sharing class getDeveloperName extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String caseId = params.get('recordId');
            if (String.isBlank(caseId)) {
                throw new WrtsException(System.Label.Case + ' - ' + System.Label.MissingId);
            }
            List<Case> listRecordType = supplyQuery.getDeveloperNameQuery(caseId);
            response.put('listRecordType', listRecordType);
            return response;
        }
    }

    public with sharing class getSuppliesByAccountId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            response.put('error', false);
            try {
                String accountId = params.get('accountId');
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                List<Supply__c> supplies = supplyQuery.getSupplies(accountId, userInformation);
                response.put('supplies', supplies);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getSuppliesByServicePoint extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            try {
                String servicePointCode = params.get('servicePointCode');
                String accountId = params.get('accountId');
                String status = params.get('status');
                String contractId = params.get('contractId');
                String notOwnAcc = params.get('notOwnAccount');
                String selFields = params.get('selectFields');
                String companyDivisionId = params.get('companyDivisionId');

                List<String> selectFields = (List<String>) JSON.deserialize(selFields, List<String>.class);
                Boolean notOwnAccount = ((Boolean) JSON.deserialize(notOwnAcc, Boolean.class));

                List<Supply__c> supplyFieldsSP = supplyQuery.getSuppliesByServicePoint(servicePointCode, accountId, status, contractId, selectFields, notOwnAccount, companyDivisionId);

                response.put('supplyFieldsSP', supplyFieldsSP);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getSuppliesByContract extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            try {
                String contractCode = params.get('contractCode');
                String contractCustomerCode = params.get('contractCustomerCode');
                String accountId = params.get('accountId');
                String status = params.get('status');
                String contractId = params.get('contractId');
                String notOwnAcc = params.get('notOwnAccount');
                String selFields = params.get('selectFields');
                String companyDivisionId = params.get('companyDivisionId');
                
                List<String> selectFields = (List<String>) JSON.deserialize(selFields, List<String>.class);
                Boolean notOwnAccount = ((Boolean) JSON.deserialize(notOwnAcc, Boolean.class));

                List<Supply__c> supplyFieldsCT = supplyQuery.getSuppliesByContract(contractCode, contractCustomerCode, accountId,
                    status, selectFields, contractId, notOwnAccount, companyDivisionId);

                response.put('supplyFieldsCT', supplyFieldsCT);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getLookupFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            try {

                String objName = params.get('objName');

                List<String> lookupFields = Utils.getLookupFields(objName);
                response.put('lookupFields', lookupFields);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class deleteCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {

                String recordId = params.get('recordId');
                supplyServices.deleteCase(recordId);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}