/**
 * Created by giupastore on 14/05/2020.
 */

@isTest
public with sharing class MRO_SRV_ReminderServiceTst {
	@TestSetup
	private static void setup() {

		CustomReminder__c cr1 = new CustomReminder__c();
		cr1.Code__c = 'test1';
		cr1.RecordId__c = [SELECT ID FROM USER LIMIT 1].ID;
		insert cr1;
	}


	@isTest
	public static void insertReminderTest() {
		CustomReminder__c cr = [SELECT ID, Code__c, RecordId__c, Status__c FROM CustomReminder__c LIMIT 1];
		MRO_SRV_ReminderService.MRO_ReminderServiceRequestWrapper w = new MRO_SRV_ReminderService.MRO_ReminderServiceRequestWrapper();
		w.recordRelatedId = cr.RecordId__c;
		List<String> l = new List<String>();
		l.add('test1');
		l.add('test2');
		w.reminderCodes = l;
		List<MRO_SRV_ReminderService.MRO_ReminderServiceRequestWrapper> wList = new List<MRO_SRV_ReminderService.MRO_ReminderServiceRequestWrapper>();
		wList.add(w);
		w.hashCode();
		MRO_SRV_ReminderService.MRO_ReminderServiceRequestWrapper other = new MRO_SRV_ReminderService.MRO_ReminderServiceRequestWrapper();
		w.equals(other);
		MRO_SRV_ReminderService rms = new MRO_SRV_ReminderService();
		rms.insertReminder(wList);
		rms.insertReminder(null);

	}

	@isTest
	public static void removeReminderTest() {
		CustomReminder__c cr = [SELECT ID, Code__c, RecordId__c, Status__c FROM CustomReminder__c LIMIT 1];
		List<ID> l = new List<ID>();
		l.add(cr.ID);
		MRO_SRV_ReminderService rms = new MRO_SRV_ReminderService();
		rms.removeReminder(l);
		rms.removeReminder(null);
	}
}