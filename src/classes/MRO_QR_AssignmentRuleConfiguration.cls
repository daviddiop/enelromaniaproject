/**
 * Created by tommasobolis on 03/11/2020.
 */

public with sharing class MRO_QR_AssignmentRuleConfiguration {

    public static MRO_QR_AssignmentRuleConfiguration getInstance() {

        return (MRO_QR_AssignmentRuleConfiguration) ServiceLocator.getInstance(MRO_QR_AssignmentRuleConfiguration.class);
    }

    public List<AssignmentRuleConfiguration__mdt> getAssignmentRuleConfigurations(
        String sObjectName,
        String sObjectTriggeringFieldName,
        Set<String> sObjectTriggeringFieldValues
    ) {

        return
            [SELECT
                Id, MasterLabel,
                SObjectName__c, SObjectTriggeringFieldName__c, SObjectTriggeringFieldValue__c,
                SObjectRecordTypeDeveloperName__c, RelatedSObjectsToAssignFieldNames__c,
                SObjectAdditionalTriggeringConditions__c, OwnerTriggeringConditions__c,
                ApexClassName__c, ApexClassMethod__c,
                AssigneeType__c, Assignee__c, Debug__c
             FROM AssignmentRuleConfiguration__mdt
             WHERE
                SObjectName__c = :sObjectName AND
                SObjectTriggeringFieldName__c = :sObjectTriggeringFieldName AND
                SObjectTriggeringFieldValue__c IN :sObjectTriggeringFieldValues
            ];
    }
}