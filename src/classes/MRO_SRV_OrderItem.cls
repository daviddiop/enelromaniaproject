public with sharing class MRO_SRV_OrderItem {

    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_ConfigurationItem configurationItemQuery = MRO_QR_ConfigurationItem.getInstance();
    private static MRO_QR_MatrixParameterRow matrixParameterRowQuery = MRO_QR_MatrixParameterRow.getInstance();

    public static MRO_SRV_OrderItem getInstance() {
        return (MRO_SRV_OrderItem) ServiceLocator.getInstance(MRO_SRV_OrderItem.class);
    }

    public void clone(Set<String> originalOrderItemIds) {
        List<NE__OrderItem__c> orderItemList = configurationItemQuery.listByIds(originalOrderItemIds);
        System.debug('orderItemList = ' + orderItemList);

        Set<String> productCodes = new Set<String>();
        for (NE__OrderItem__c orderItem : orderItemList) {
            if (String.isNotBlank(orderItem.MPRProductCode__c)) {
                productCodes.add(orderItem.MPRProductCode__c);
            } else if (orderItem.NE__ProdId__c == null) {
                productCodes.add(orderItem.NE__ProdId__r.ProductCode__c);
            }
        }
        System.debug('productCodes = ' + productCodes);

        List<NE__Matrix_Parameter_Row__c> mprList = matrixParameterRowQuery.listByProductCodesWithMultiPriceOrBonus(productCodes);
        System.debug('mprList = ' + mprList);
        Map<String, List<NE__Matrix_Parameter_Row__c>> productCodeToMprList = new Map<String, List<NE__Matrix_Parameter_Row__c>>();
        for (NE__Matrix_Parameter_Row__c mpr : mprList) {
            String productCode = String.isNotBlank(mpr.MPRProductCode__c) ? mpr.MPRProductCode__c : mpr.ProductCode__c;
            List<NE__Matrix_Parameter_Row__c> mprs = productCodeToMprList.get(productCode);
            if (mprs == null) {
                mprs = new List<NE__Matrix_Parameter_Row__c>();
                productCodeToMprList.put(productCode, mprs);
            }
            mprs.add(mpr);
        }
        System.debug('productCodeToMprList = ' + productCodeToMprList);

        List<NE__OrderItem__c> clonedOrderItemList = new List<NE__OrderItem__c>();
        List<NE__OrderItem__c> updateOrderItemList = new List<NE__OrderItem__c>();
        for (NE__OrderItem__c orderItem : orderItemList) {
            String productCode = null;
            if (String.isNotBlank(orderItem.MPRProductCode__c)) {
                productCode = orderItem.MPRProductCode__c;
            } else if (orderItem.NE__ProdId__c == null) {
                productCode = orderItem.NE__ProdId__r.ProductCode__c;
            }

            List<NE__Matrix_Parameter_Row__c> orderItemMprs = (List<NE__Matrix_Parameter_Row__c>)productCodeToMprList.get(productCode);
            if (orderItemMprs == null) {
                continue;
            }
            Boolean isMultiPrice = null;
            Integer count = 0;
            for (Integer i = 0; i < orderItemMprs.size(); i++) {
                String guid = MRO_UTL_Guid.newGuid();
                NE__Matrix_Parameter_Row__c mpr = orderItemMprs.get(i);
                if (orderItem.MPRName__c == mpr.Name) {
                    if (mpr.Multiprice__c == true) {
                        setMultiPriceFields(orderItem, mpr, guid);
                    } else if (mpr.BonusValue__c != null) {
                        setBonusFields(orderItem, mpr, guid);
                    }
                    updateOrderItemList.add(orderItem);
                    continue;
                }
                System.debug('mpr = ' + mpr);
                if (mpr.Multiprice__c == true && (isMultiPrice == true || count == 0)) {
                    isMultiPrice = true;
                    NE__OrderItem__c clonedOrderItem = orderItem.clone();
                    setMultiPriceFields(clonedOrderItem, mpr, guid);
                    clonedOrderItemList.add(clonedOrderItem);
                } else if (mpr.BonusValue__c != null && (isMultiPrice == false || count == 0)) {
                    isMultiPrice = false;
                    NE__OrderItem__c clonedOrderItem = orderItem.clone();
                    setBonusFields(clonedOrderItem, mpr, guid);
                    clonedOrderItemList.add(clonedOrderItem);
                }
                count++;
            }
        }
        System.debug('clonedOrderItemList = ' + clonedOrderItemList);
        databaseSrv.insertSObject(clonedOrderItemList);
        databaseSrv.updateSObject(updateOrderItemList);
    }

    public void setMultiPriceFields( NE__OrderItem__c orderItem, NE__Matrix_Parameter_Row__c mpr, String guid) {
        orderItem.MPRName__c = mpr.Name;
        orderItem.DeliveryPrice__c =  mpr.DeliveryPrice__c;
        orderItem.InstallationPrice__c =  mpr.InstallationPrice__c;
        orderItem.LDNightPrice__c =  mpr.LDNightPrice__c;
        orderItem.WeekendNightPrice__c =  mpr.WeekendNightPrice__c;
        orderItem.DayPrice__c =  mpr.DayPrice__c;
        orderItem.MonthlyFee__c =  mpr.MonthlyFee__c;
        orderItem.MonthlyCommission__c =  mpr.MonthlyCommission__c;
        orderItem.NE__BaseOneTimeFee__c =  mpr.PriceValue__c;
        orderItem.NE__BaseRecurringCharge__c =  mpr.Subscription__c;
        orderItem.NE__Configuration_item_update_key__c = orderItem.NE__Configuration_item_update_key__c + '_' + guid;
        orderItem.MultipriceStartDate__c = mpr.MultipriceStartDate__c;
        orderItem.MultipriceEndDate__c = mpr.MultipriceEndDate__c;
    }

    public void setBonusFields( NE__OrderItem__c orderItem, NE__Matrix_Parameter_Row__c mpr, String guid) {
        orderItem.MPRName__c = mpr.Name;
        orderItem.BonusMonth__c =  mpr.BonusMonth__c;
        orderItem.BonusValue__c =  mpr.BonusValue__c;
        orderItem.NE__Configuration_item_update_key__c = orderItem.NE__Configuration_item_update_key__c + '_' + guid;
    }
}