public with sharing class MRO_QR_StartDateValidationSettings {

    public static MRO_QR_StartDateValidationSettings getInstance() {
        return (MRO_QR_StartDateValidationSettings) ServiceLocator.getInstance(MRO_QR_StartDateValidationSettings.class);
    }

    public List<StartDateValidationSettings__mdt> listByProcessOriginCommodity(String process, String origin, String commodity) {
        return [
            SELECT Id, IsEnelArea__c, IsSameOwner__c, SubProcess__c, IsCustomerReading__c, IsMarketChange__c,
                DefaultIncrement__c, IsDefaultWorkingDay__c, ValidationIncrement__c, IsValidationWorkingDay__c
            FROM StartDateValidationSettings__mdt
            WHERE Process__c = :process
                AND ChannelOrigin__c = :origin
                AND Commodity__c = :commodity
        ];
    }
}