/**
 * @author Baba Goudiaby BG
 * @version 1.0
 * @description Apex class for all operations of business logic and data manipulation about Validatable Document
 * @history
 * 01/04/2019: Original - BG
 * 10/06/2019: Add retrieveDocumentTypesFromDossier method - BG
 */

public with sharing class MRO_SRV_DocumentType {
    private static MRO_QR_DocumentBundleItem documentBundleItemQuery = MRO_QR_DocumentBundleItem.getInstance();
    private static MRO_QR_DocumentItem documentItemQuery = MRO_QR_DocumentItem.getInstance();

    private static MRO_SRV_ValidatableDocument validatableDocumentSrv = MRO_SRV_ValidatableDocument.getInstance();
    private static MRO_SRV_DocumentItem documentItemSrv = MRO_SRV_DocumentItem.getInstance();

    public static MRO_SRV_DocumentType getInstance() {
        return new MRO_SRV_DocumentType();
    }

    /**
     * Method to allow us to retrieve all Document Types from Dossier
     *
     * @param parentRecordId
     *
     * @return List of documentType DTO
     */
    public List<MRO_SRV_DocumentType.DocumentTypeDTO> retrieveDocumentTypesFromParentRecord(Id parentRecordId) {
        String sObjectType = parentRecordId.getSobjectType().getDescribe().getName();
        Map<String, ValidatableDocument__c> mapDocumentItemBundleValidatableDoc = validatableDocumentSrv.getMapDocumentItemBundleValidatableDoc(parentRecordId);
        //Map<String, List<MRO_SRV_DocumentType.DocumentTypeDTO>> mapDocumentBundleToDocTypes = new Map<String, List<MRO_SRV_DocumentType.DocumentTypeDTO>>();
        Map<String, List<MRO_SRV_DocumentItem.DocumentItemDTO>> mapDocTypesDocumentItems = new Map<String, List<MRO_SRV_DocumentItem.DocumentItemDTO>>();
        Map<String, MRO_SRV_DocumentType.DocumentTypeDTO> mapDocTypeDtos= new Map<String, MRO_SRV_DocumentType.DocumentTypeDTO>();
        Set<String> idsDocumentTypes = new Set<String>();

        List<DocumentBundleItem__c> listDocumentBundleItems;
        Boolean inEnelArea = false;
        Case caseRecord;
        switch on sObjectType {
            when 'Case' {
                caseRecord = MRO_QR_Case.getInstance().listByIdsForActivities(new Set<Id>{parentRecordId})[0];
                listDocumentBundleItems = documentBundleItemQuery.listDocumentBundleItemsByCaseId(parentRecordId);
            }
            when 'Dossier__c' {
                Dossier__c dossier = MRO_QR_Dossier.getInstance().getByIdsForActivities(new Set<Id>{parentRecordId}).get(parentRecordId);
                if (!dossier.Cases__r.isEmpty()) {
                    caseRecord = dossier.Cases__r[0];
                }
                listDocumentBundleItems = documentBundleItemQuery.listDocumentBundleItemsByDossierId(parentRecordId);
            }
            when else {
                throw new WrtsException('Unsupported parent object for a Validatable Document');
            }
        }
        if (caseRecord != null) {
            inEnelArea = caseRecord.IsDisCoENEL__c
                        || (caseRecord.Distributor__r != null && caseRecord.Distributor__r.IsDisCoENEL__c)
                        || (caseRecord.Supply__r != null && (caseRecord.Supply__r.InENELArea__c
                                                            || (caseRecord.Supply__r.ServicePoint__r.Distributor__r != null && caseRecord.Supply__r.ServicePoint__r.Distributor__r.IsDisCoENEL__c)));
        }
        //List<MRO_SRV_DocumentType.DocumentTypeDTO> listDocumentTypeDTO = new List<MRO_SRV_DocumentType.DocumentTypeDTO>();
        for (DocumentBundleItem__c docBundleItem : listDocumentBundleItems) {
            MRO_SRV_DocumentType.DocumentTypeDTO documentType =
                getDocumentTypeFromDocumentBundleItem(docBundleItem.DocumentType__c, docBundleItem.DocumentType__r.Name, docBundleItem.Mandatory__c, (inEnelArea && docBundleItem.ArchiveOnDisCo__c), docBundleItem.Id);

            Boolean added=idsDocumentTypes.add(docBundleItem.DocumentType__c);
            //listDocumentTypeDTO = new List<MRO_SRV_DocumentType.DocumentTypeDTO>();

           /* if (mapDocumentItemBundleValidatableDoc.containsKey(docBundleItem.Id)) {
                ValidatableDocument__c validatableDocument = mapDocumentItemBundleValidatableDoc.get(docBundleItem.Id);
                documentType.isValidated = String.isNotBlank(validatableDocument.FileMetadata__c) ? true : false;
                documentType.validatableDocument = validatableDocument.Id;
            }*/
            if(added){
                //listDocumentTypeDTO.add(documentType);
                ValidatableDocument__c validatableDocument = mapDocumentItemBundleValidatableDoc.get(docBundleItem.Id);
                documentType.isValidated = String.isNotBlank(validatableDocument.FileMetadata__c) ? true : false;
                documentType.validatableDocument = validatableDocument.Id;
                documentType.isArchivedOnDisCo = String.isNotBlank(validatableDocument.FileMetadata__r.DiscoFileId__c);
                mapDocTypeDtos.put(docBundleItem.DocumentType__c, documentType);
            }
            /*if (!mapDocumentBundleToDocTypes.containsKey(String.valueOf(docBundleItem.DocumentBundle__c))) {
                
                mapDocumentBundleToDocTypes.put(docBundleItem.DocumentBundle__c, new List<MRO_SRV_DocumentType.DocumentTypeDTO>());
            } else {
                mapDocumentBundleToDocTypes.get(docBundleItem.DocumentBundle__c).add(documentType);
            }*/
        }

        //List<DocumentItemService.DocumentItemDTO> listDocumentItemDTO ;
        List<DocumentItem__c> listDocumentItems = documentItemQuery.listDocumentItemsByDocumentTypeId(idsDocumentTypes);
        for (DocumentItem__c docItem : listDocumentItems) {
            MRO_SRV_DocumentItem.DocumentItemDTO docItemDTO = documentItemSrv.mapToDto(docItem);
            //listDocumentItemDTO = new List<DocumentItemService.DocumentItemDTO>();
            if (!mapDocTypesDocumentItems.containsKey(docItem.DocumentType__c)) {
               // listDocumentItemDTO.add(docItemDTO);
                List<MRO_SRV_DocumentItem.DocumentItemDTO> docItems=new List<MRO_SRV_DocumentItem.DocumentItemDTO>();
                docItems.add(docItemDTO);
                mapDocTypesDocumentItems.put(docItem.DocumentType__c, docItems);
            } else {
                List<MRO_SRV_DocumentItem.DocumentItemDTO> docItems=mapDocTypesDocumentItems.get(docItem.DocumentType__c);
                docItems.add(docItemDTO);
            }
        }

        for (String documentType : mapDocTypeDtos.keySet()) {
            mapDocTypeDtos.get(documentType).documentItems = mapDocTypesDocumentItems.get(documentType);
        }

        //List<List<MRO_SRV_DocumentType.DocumentTypeDTO>> listDocumentTypeDTOFromBundle = mapDocumentBundleToDocTypes.values();
        //List<MRO_SRV_DocumentType.DocumentTypeDTO> listDocumentTypeDTO = new List<MRO_SRV_DocumentType.DocumentTypeDTO>() ;
        /*for (List<MRO_SRV_DocumentType.DocumentTypeDTO> listDocTypes : listDocumentTypeDTOFromBundle) {
            for (MRO_SRV_DocumentType.DocumentTypeDTO docTypeDTO : listDocTypes) {
                List<DocumentItemService.DocumentItemDTO> docItemsFromDocType = mapDocTypesDocumentItems.get(docTypeDTO.id);
                if (docItemsFromDocType != null) {
                    docTypeDTO.documentItems = docItemsFromDocType;
                }
                listDocumentTypeDTO.add(docTypeDTO);
            }
        }*/

        System.debug('##### mapDocTypeDtos: '+JSON.serialize(mapDocTypeDtos));
        
        return mapDocTypeDtos.values();
    }

    public DocumentType__c getDocumentTypeObj(String id, String name) {
        return new DocumentType__c(Id = id, Name = name);
    }

    public DocumentTypeDTO getDocumentTypeFromDocumentBundleItem(String idDocType, String name, Boolean isMandatory, String idsDocumentBundleItems) {
        return new DocumentTypeDTO(getDocumentTypeObj(idDocType, name), isMandatory, idsDocumentBundleItems);
    }

    public DocumentTypeDTO getDocumentTypeFromDocumentBundleItem(String idDocType, String name, Boolean isMandatory, Boolean archiveOnDisCo, String idsDocumentBundleItems) {
        return new DocumentTypeDTO(getDocumentTypeObj(idDocType, name), isMandatory, archiveOnDisCo, idsDocumentBundleItems);
    }

    /**
     * @description  Inner class DTO for Document Type
     */
    public class DocumentTypeDTO {
        @AuraEnabled
        public String id { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public Boolean isMandatory { get; set; }
        @AuraEnabled
        public Boolean isValidated { get; set; }
        @AuraEnabled
        public String documentBundleItem { get; set; }
        @AuraEnabled
        public Boolean archiveOnDisCo { get; set; }
        @AuraEnabled
        public Boolean isArchivedOnDisCo { get; set; }
        @AuraEnabled
        public String validatableDocument { get; set; }
        //@AuraEnabled
        public List<MRO_SRV_DocumentItem.DocumentItemDTO> documentItems { get; set; }
        @AuraEnabled
        public String documentItemsDTO {
            get {
                return JSON.serialize(this.documentItems);
            }
        }

        public DocumentTypeDTO() {
        }
        public DocumentTypeDTO(DocumentType__c documentTypeObj, Boolean isMandatory, String documentBundleItem) {
            this(documentTypeObj, isMandatory, false, documentBundleItem);
        }
        public DocumentTypeDTO(DocumentType__c documentTypeObj, Boolean isMandatory, Boolean archiveOnDisCo, String documentBundleItem) {
            this.id = documentTypeObj.Id;
            this.name = documentTypeObj.Name;
            this.isMandatory = isMandatory;
            this.archiveOnDisCo = archiveOnDisCo;
            this.documentBundleItem = documentBundleItem;
        }
    }

}