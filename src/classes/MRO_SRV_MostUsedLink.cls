/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   gen 31, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_MostUsedLink {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_MostUsedLink mostUsedLinkQueries = MRO_QR_MostUsedLink.getInstance();

    public static MRO_SRV_MostUsedLink getInstance() {
        return (MRO_SRV_MostUsedLink) ServiceLocator.getInstance(MRO_SRV_MostUsedLink.class);
    }

    public void upsertMostUsedLink(Id linkId, Id userId) {
        if (String.isBlank(linkId)) {
            throw new WrtsException(System.Label.User + ' - ' + System.Label.MissingId);
        }

        MostUsedLink__c mostUsedLink = new MostUsedLink__c(User__c = userId);
        mostUsedLink.ServiceLink__c = linkId;
        List<MostUsedLink__c> checkIfLinkExists = mostUsedLinkQueries.getMostUsedLink(linkId, userId);

        if (checkIfLinkExists.size() == 0) {
            databaseSrv.insertSObject(mostUsedLink);
        } else {
            mostUsedLink.Id = checkIfLinkExists[0].Id;
            Integer addToCount = (Integer) checkIfLinkExists[0].Count__c + 1;
            mostUsedLink.Count__c = addToCount;
            databaseSrv.updateSObject(mostUsedLink);
        }
    }
}