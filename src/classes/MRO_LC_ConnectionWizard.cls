/**
 * Created by ferhati on 01/10/2019.
 */

public with sharing class MRO_LC_ConnectionWizard extends ApexServiceLibraryCnt {
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_ServicePoint servicePointSrv = MRO_SRV_ServicePoint.getInstance();
    private static MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();
    private static MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    public static final Id CONNECTION_DOSSIER_RECORDTYPE = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Connection').getRecordTypeId();

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String billingProfileId = '';
            String genericRequestId = params.get('genericRequestId');
            Savepoint sp = Database.setSavepoint();
            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }


                Account acc = accQuery.findAccount(accountId);
                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, null,
                        CONNECTION_DOSSIER_RECORDTYPE, 'Connection', constantsUtl.COMMODITY_ELECTRIC);

                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                if (String.isNotBlank(dossierId)) {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                    Contact contactRecord = new Contact();
                    if(cases.size()>0){
                        if(String.isNotBlank(cases[0].ContactId)) {
                            contactRecord = contactQuery.getById(cases[0].ContactId);
                            response.put('contactRecord', contactRecord);
                        }
                    }

                    if (!cases.isEmpty()) {
                        billingProfileId = cases[0].BillingProfile__c;
                    }
                    response.put('caseTile', cases);
                    response.put('billingProfileId', billingProfileId);
                }
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('account', acc);
                response.put('accountId', accountId);
                response.put('genericRequestId', dossier.Parent__c);

                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
                Database.rollback(sp);
            }

            return response;
        }
    }

    public class saveDraftBP extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            List<Case> listCasesToUpdate = new List<Case>();
            String billingProfileId = params.get('billingProfileId');
            String connectionType = params.get('connectionType');
            if (String.isBlank(connectionType)) {
                throw new WrtsException(System.Label.SubProcessCheck);
            }
            Savepoint sp = Database.setSavepoint();

            try {
                if (!oldCaseList.isEmpty()) {
                    for(Case cases : oldCaseList) {
                        if (cases.SubProcess__c != connectionType) {
                            cases.SubProcess__c = connectionType;
                        }
                        listCasesToUpdate.add(cases);
                    }
                }
                if(!listCasesToUpdate.isEmpty()){
                    caseSrv.updateCaseByBillingProfile(listCasesToUpdate, billingProfileId);
                }
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public Inherited sharing class updateDossierConnectionTypeAndChannelOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            String connectionType = params.get('connectionType');
            dossierSrv.updateDossierSubProcessChannelOrigin(dossierId, connectionType, originSelected, channelSelected);
            response.put('error', false);
            return response;
        }
    }


    /**
     * @author Boubacar Sow
     * @description update dossier with the company Division
     * @date 11/05/2020
    */
    public Inherited sharing class updateDossierCompanyDivision extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String companyDivisionId = params.get('companyDivisionId');
            dossierSrv.updateCompanyDivisionByDossierId(dossierId, companyDivisionId);
            response.put('error', false);
            return response;
        }
    }

    public class checkCases extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Id dossierId = (Id) params.get('dossierId');

            List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
            Boolean inENELArea;
            for (Case c : cases) {
                if (inENELArea == null) {
                    inENELArea = c.Distributor__r.IsDisCoENEL__c;
                } else if (inENELArea != c.Distributor__r.IsDisCoENEL__c) {
                    throw new WrtsException(Label.AllDistributersShouldHaveTheSameDiscoEnel);
                }
            }

            response.put('result', 'OK');
            return response;
        }
    }

    public class deleteCloneCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String caseId = params.get('caseId');
            Savepoint sp = Database.setSavepoint();
            try {
                caseSrv.deleteCase(caseId);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class updateChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            dossierSrv.updateDossierChannel(dossierId, channelSelected, originSelected);
            response.put('error', false);
            return response;
        }
    }

    public with sharing class updateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String billingProfileId = params.get('billingProfileId');
            String contactId = params.get('contactId');
            String accountId = params.get('accountId');
            String connectionType = params.get('connectionType');
            if (String.isBlank(connectionType)) {
                throw new WrtsException(System.Label.SubProcessCheck);
            }
            Savepoint sp = Database.setSavepoint();

            try {
                Set<Id> caseIds = (new Map<Id, Case>(oldCaseList)).keySet();
                List<Case> cases = caseQuery.listByIds(caseIds);
                Account acc = accQuery.findAccount(accountId);

                if(contactId != null && String.isNotBlank(contactId) ){
                    Contact contactRecord = contactQuery.getById(contactId);
                    if(String.isBlank(contactRecord.MobilePhone) &&  String.isBlank(contactRecord.Phone)){
                        response.put('phoneError', System.Label.ProcessRequeresPhoneMobile);
                    }
                }
                String accountPhone;
                if(String.isNotBlank(acc.Phone)){
                    accountPhone =  acc.Phone;
                } else if(String.isNotBlank(acc.PersonMobilePhone)){
                    accountPhone =  acc.PersonMobilePhone;
                }else{
                    response.put('phoneError', System.Label.ProcessRequeresPhoneMobile);
                }

                servicePointSrv.createTemporaryServicePointsForConnection(cases);
                caseSrv.generateAddressVerificationActivities(cases, billingProfileId);
                caseSrv.finalizeConnectionCasesAndDossier(cases, dossierId, billingProfileId, contactId,accountPhone, connectionType);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            Savepoint sp = Database.setSavepoint();

            try {
                caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class getAddress extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Case record = new Case();
            MRO_SRV_Address.AddressDTO addressFromCase = new MRO_SRV_Address.AddressDTO() ;
            try {
                addressFromCase = new MRO_SRV_Address.AddressDTO(record, 'Address');
                response.put('osiAddress', addressFromCase);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}