/**
 * Created by Octavian on 11/22/2019.
 */

public with sharing class MRO_LC_MeterTile extends ApexServiceLibraryCnt{
    public class validateQuadrantIndex extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String quadrantName = params.get('QuadrantName');
            String index = params.get('QuadrantIndex');
            String servicePointCode = params.get('ServicePointCode');
            String meterSerialNumber = params.get('MeterSerialNumber');
            String dialId = params.get('DialId');
            String paramsJson = params.get('params');

            Map<String, Object> response = new Map<String, Object>();

            try {
                if (MRO_SRV_SapSelfReadingCallOut.checkSapEnabled() && servicePointCode != null && meterSerialNumber != null && dialId != null) {
                    MRO_SRV_SapSelfReadingCallOut.ReadingDTO reading = new MRO_SRV_SapSelfReadingCallOut.ReadingDTO();
                    if(String.isNotEmpty(paramsJson)){
                        MeterParams meterParams = (MeterParams)JSON.deserialize(paramsJson, MeterParams.class);
                        reading.invoiceNumber = meterParams.invoiceNumber;
                        reading.invoiceSerialNumber = meterParams.invoiceSerialNumber;
                    }
                    reading.dialId = dialId;
                    reading.meterSerialNumber = meterSerialNumber;
                    reading.servicePointCode = servicePointCode;
                    reading.index = Double.valueOf(index);
                    Map<String, String> validationResponse = MRO_SRV_SapSelfReadingCallOut.getInstance().validate(reading);
                    response.put('indexValidated', validationResponse.get('returnCode') == 'OK' || validationResponse.get('returnCode') == '00');
                    response.put('errorMessage', validationResponse.get('returnMessage'));
                    response.put('returnCode', validationResponse.get('returnCode') == 'OK' ? '00' : validationResponse.get('returnCode'));
                    return response;
                }
            }catch (Exception e){
                System.debug(e.getMessage());
            }

            Integer indexValue =  integer.valueOf(index);
            String errorMessage = quadrantName;
            Boolean isValidated = true;
            if(quadrantName == 'Energie Reactiva Inductva'){
                if(indexValue < 100)
                {
                    isValidated = false;
                    errorMessage = 'Index Value has to be grater than 100 for this quadrant';
                }
            }
            if(quadrantName == 'Energie Reactiva Capacitiva'){
                if(indexValue < 50)
                {
                    isValidated = false;
                    errorMessage = 'Index Value has to be grater than 50 for this quadrant';
                }
            }
            response.put('indexValidated',isValidated);
            response.put('errorMessage', errorMessage);
            response.put('returnCode', isValidated ? '00' : '99');
            return response;
        }
    }

    public class MeterParams{
        @AuraEnabled
        public String invoiceSerialNumber { get; set; }
        @AuraEnabled
        public String invoiceNumber { get; set; }
    }

}