/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 13, 2020
 * @desc    
 * @history 
 */

public without sharing class MRO_QR_Sequencer {

    public static MRO_QR_Sequencer getInstance() {
        return new MRO_QR_Sequencer();
    }

    public Sequencer__c getCustomerCodeSequencer() {
        return [SELECT Sequence__c, SequenceLength__c FROM Sequencer__c WHERE Type__c = 'CustomerCode' LIMIT 1 FOR UPDATE];
    }

    public Map<Id, Sequencer__c> getENELTELSequencers(Set<Id> companyDivisionIds) {
        List<Sequencer__c> sequencers = [SELECT Sequence__c, SequenceLength__c, CompanyDivision__c, CompanyPrefix__c FROM Sequencer__c
                                         WHERE Type__c = 'ENELTEL' AND CompanyDivision__c IN :companyDivisionIds
                                         FOR UPDATE];
        Map<Id, Sequencer__c> result = new Map<Id, Sequencer__c>();
        for (Sequencer__c seq : sequencers) {
            result.put(seq.CompanyDivision__c, seq);
        }
        return result;
    }

    public Map<Id, Sequencer__c> getBillingAccountNumberSequencer(Set<Id> companyDivisionIds) {
        List<Sequencer__c> sequencers = [SELECT Sequence__c, SequenceLength__c, CompanyDivision__c, CompanyPrefix__c FROM Sequencer__c
                                         WHERE Type__c = 'BillingAccountNumber' AND CompanyDivision__c IN :companyDivisionIds
                                         FOR UPDATE];
        Map<Id, Sequencer__c> result = new Map<Id, Sequencer__c>();
        for (Sequencer__c seq : sequencers) {
            result.put(seq.CompanyDivision__c, seq);
        }
        return result;
    }
}