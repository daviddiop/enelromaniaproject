/**
 * Created by goudiaby on 02/04/2019.
 */

@IsTest
private class ValidatableDocumentsCntTst {
    @testSetup
    static void setup() {
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision; 

        Dossier__c dossier = TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        insert dossier;

        FileMetadata__c fileMetadata = TestDataFactory.FileMetadata().build();
        fileMetadata.Dossier__c = dossier.Id;
        insert fileMetadata;

        insert TestDataFactory.FileMetadata().build();

        DocumentType__c documentType01 = TestDataFactory.DocumentType().createDocumentType('document Type Test').build();
        insert documentType01;

        DocumentType__c documentType02 = TestDataFactory.DocumentType().createDocumentType('document Type Test validated').build();
        insert documentType02;

        DocumentBundle__c bundle = new DocumentBundle__c();
        bundle.Name = 'Doc bundle test';
        insert bundle;

        List<DocumentItem__c> listDocumentItems = new List<DocumentItem__c>();
        for (Integer i = 0; i < 10; i++) {
            DocumentItem__c documentItem = TestDataFactory.DocumentItem().createBulkDocumentItem(i).build();
            documentItem.DocumentType__c = i > 5 ? documentType01.Id : documentType02.Id;
            listDocumentItems.add(documentItem);
        }
        insert listDocumentItems;

        DocumentBundleItem__c docBundleItem01 = TestDataFactory.DocumentBundleItem().createDocumentBundleItem(bundle.Id,documentType01.Id,true).build();
        insert docBundleItem01;

        DocumentBundleItem__c docBundleItem02 = TestDataFactory.DocumentBundleItem().createDocumentBundleItem(bundle.Id,documentType02.Id,false).build();
        insert docBundleItem02;

        ValidatableDocument__c validatableDocument01 = TestDataFactory.ValidatableDocument().createValidatableDocument(dossier.Id
                ,docBundleItem01.Id,null).build();
        insert validatableDocument01;

        ValidatableDocument__c validatableDocument02 = TestDataFactory.ValidatableDocument().createValidatableDocument(dossier.Id
                ,docBundleItem02.Id,fileMetadata.Id).build();
        insert validatableDocument02;
    }

    @isTest
    static void getFileMetadataAndValidatableDocumentsOk() {
        String fileMetadataId = [
            SELECT Id 
            FROM FileMetadata__c 
            WHERE Dossier__c != null
        ].Id;
        ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments getValidatableDocumentsData = new ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        Test.startTest();
        getValidatableDocumentsData.perform(jsonInput);
        Test.stopTest();
    }

    @isTest
    static void getFileMetadataAndValidatableDocumentsKO1() {
        ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments getValidatableDocumentsData = new ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => ''
        });
        try {
            Test.startTest();
            getValidatableDocumentsData.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Filemetadata is required ?  ' + exc.getMessage());
        }
    }

    @isTest
    static void getFileMetadataAndValidatableDocumentsKO2() {
        String fileMetadataId = [
            SELECT Id 
            FROM FileMetadata__c 
            WHERE Dossier__c = null
        ].Id;
        ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments getValidatableDocumentsData =
                new ValidatableDocumentsCnt.getFileMetadataAndValidatableDocuments();
        String jsonInput = JSON.serializePretty(new Map<String, String>{
            'filemetadataId' => fileMetadataId
        });
        try {
            Test.startTest();
            getValidatableDocumentsData.perform(jsonInput);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Dossier is required ?  ' + exc.getMessage());
        }
    }
}