/**
 * Created by David Diop on 02.12.2019.
 */
@IsTest
public with sharing class ProductChangeWizardCntTst {
    @testSetup
    static void setup() {
        try {
            List<Account> listAccount = new List<Account>();
            listAccount.add(TestDataFactory.account().personAccount().build());
            Account businessAccount = TestDataFactory.account().businessAccount().build();
            businessAccount.Name = 'BusinessAccount1';
            listAccount.add(businessAccount);
            insert listAccount;


            CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
            insert companyDivision;
            User user = new User();
            user.Id = UserInfo.getUserId() ;
            user.CompanyDivisionId__c = companyDivision.Id;
            update user;
            Contact contact = TestDataFactory.contact().createContact().build();
            contact.AccountId = listAccount[1].Id;
            insert contact;
            Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
            insert interaction;

            CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
            insert customerInteraction;

            List<ServicePoint__c> listServicePoint = new List<ServicePoint__c>();
            ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePointGas().build();
            servicePoint.Account__c = listAccount[1].Id;
            insert servicePoint;

            ServicePoint__c servicePoint2 = TestDataFactory.servicePoint().createServicePointEle().build();
            servicePoint.Account__c = listAccount[0].Id;
            insert servicePoint2;

            listServicePoint.add(servicePoint);
            listServicePoint.add(servicePoint2);

            Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
            opportunity.StageName = 'Prospecting';
            opportunity.AccountId = listAccount[1].Id;
            insert opportunity;

            BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile().build();
            billingProfile.Account__c = listAccount[1].Id;
            insert billingProfile;

            Product2 pro = TestDataFactory.product2().build();
            insert pro;

            List<OpportunityServiceItem__c> listOppServiceItem = new List<OpportunityServiceItem__c>();
            for (Integer i = 0; i < 15; i++) {
                OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
                oppServiceItem.BillingProfile__c = billingProfile.Id;
                oppServiceItem.Account__c = listAccount[i].Id;
                oppServiceItem.Opportunity__c = opportunity.Id;
                oppServiceItem.ServicePoint__c = listServicePoint[i].Id;
                listOppServiceItem.add(oppServiceItem);
            }
            insert listOppServiceItem;

            OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
            oppServiceItem.BillingProfile__c = billingProfile.Id;
            oppServiceItem.Account__c = listAccount[0].Id;
            oppServiceItem.Opportunity__c = opportunity.Id;
            oppServiceItem.ServicePoint__c = listServicePoint[0].Id;
            insert oppServiceItem;

            ContractAccount__c contractAccount = TestDataFactory.contractAccount().createContractAccount().setAccount(listAccount[1].Id).setBillingProfile(billingProfile.Id).build();
            insert contractAccount;

            List<Supply__c> supplyList = new List<Supply__c>();
            for (Integer i = 0; i < 20; i++) {
                Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
                supply.Account__c = listAccount[1].Id;
                supply.ServicePoint__c = servicePoint.Id;
                supplyList.add(supply);
            }
            insert supplyList;

            servicePoint.CurrentSupply__c = supplyList[0].Id;
            update servicePoint;

            PricebookEntry pricebookEntry = TestDataFactory.pricebookEntry().build();
            pricebookEntry.Product2Id = pro.Id;
            pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
            insert pricebookEntry;

            OpportunityLineItem opportunityLineItem = TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
            opportunityLineItem.OpportunityID = opportunity.Id;
            opportunityLineItem.Product2ID = pro.Id;
            opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
            OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
            insert opportunityLineItem;

            Contract contract = TestDataFactory.contract().createContract().build();
            contract.AccountId = listAccount[0].Id;
            contract.CompanyDivision__c = companyDivision.Id;
            insert contract;
        } catch (Exception exc) {
            System.assert(true, 'Exception empty data?  ' + exc.getMessage());
        }
    }
    @isTest
    static void initializeTest() {
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];

        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => opportunity.Id,
            'interactionId' => interaction.Id
        };
        Object response = TestUtils.exec(
            'ProductChangeWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId' => '',
            'interactionId' => interaction.Id
        };
         response = TestUtils.exec(
            'ProductChangeWizardCnt', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @isTest
    static void initializeIsBlankAccountIdTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '',
            'opportunityId' => '',
            'interactionId' =>''
        };
        try {
            Object response = TestUtils.exec(
                'ProductChangeWizardCnt', 'initialize', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    static void updateOpportunityTest() {
        Opportunity opp = [
            SELECT Id,StageName
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' =>opp.Id,
            'privacyChangeId' =>'',
            'stage' => 'Closed Won'
        };
        Test.startTest();
        Object response =  TestUtils.exec('ProductChangeWizardCnt', 'updateOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('opportunityId') == opp.Id);
        Test.stopTest();
    }

    @isTest
    static void updateOpportunityExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' =>'',
            'privacyChangeId' =>'',
            'stage' => 'Closed Won'
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                'ProductChangeWizardCnt', 'updateOpportunity', inputJSON, true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    public static void CheckOsiTest(){
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];
        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().build();
        insert supply;

        servicePoint.CurrentSupply__c = supply.Id;
        update servicePoint;

        Opportunity opp = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        ContractAccount__c contractAccount = TestDataFactory.contractAccount().createContractAccount().build();
        insert contractAccount;
        OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        oppServiceItem.ServicePoint__c = servicePoint.Id;
        oppServiceItem.ContractAccount__c = contractAccount.Id;
        oppServiceItem.Opportunity__c = opp.Id;
        insert oppServiceItem;
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' =>oppServiceItem.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('ProductChangeWizardCnt', 'CheckOsi', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assert(result.get('opportunityServiceItem') != null);
        Test.stopTest();
    }

    @isTest
    public static void CheckOsiExceptionTest(){
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' =>''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('ProductChangeWizardCnt', 'CheckOsi', inputJSON, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    public static void linkOliToOsiTest(){
        Product2 pro = TestDataFactory.product2().build();
        insert pro;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;

        PricebookEntry pricebookEntry = TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = pro.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityId = opportunity.Id;
        opportunityLineItem.Product2Id = pro.Id;
        opportunityLineItem.PricebookEntryId = pricebookEntry.Id;
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;
        OpportunityLineItem oli = [
            SELECT Id,Product2Id
            FROM OpportunityLineItem
            LIMIT 1
        ];
        List<OpportunityServiceItem__c>  listOsi = new List<OpportunityServiceItem__c>();
        OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        insert oppServiceItem;
        listOsi.add(oppServiceItem);
        ActivationWizardCnt.InputData inputData = new ActivationWizardCnt.InputData();
        inputData.opportunityServiceItems = listOsi;
        inputData.oli = oli;

        System.debug('response'+listOsi);
        Test.startTest();
        Object response = TestUtils.exec('ProductChangeWizardCnt', 'linkOliToOsi', inputData, true);
        Test.stopTest();
        System.assertEquals(true,response == true);
    }
}