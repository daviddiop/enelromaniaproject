/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   ott 13, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_TR_AccountContactRelationHandler implements TriggerManager.ISObjectTriggerHandler {


    public void beforeInsert() {
    }

    public void beforeUpdate() {
    }

    public void beforeDelete() {
    }

    public void afterInsert() {
        Set<Id> accountIds = new Set<Id>();
        for (AccountContactRelation acr : (List<AccountContactRelation>) Trigger.new) {
            accountIds.add(acr.AccountId);
        }
        Map<Id, Account> accounts = MRO_QR_Account.getInstance().getByIds(accountIds);
        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
        for (AccountContactRelation acr : (List<AccountContactRelation>) Trigger.new) {
            Account acc = accounts.get(acr.AccountId);
            if (!accountsToUpdate.containsKey(acc.Id) && !acc.IsPersonAccount && acc.PrimaryContact__c == null) {
                Account acc2 = new Account(
                    Id = acc.Id,
                    PrimaryContact__c = acr.ContactId
                );
                accountsToUpdate.put(acc2.Id, acc2);
            }
        }
        update accountsToUpdate.values();
    }

    public void afterUpdate() {
    }

    public void afterDelete() {
    }

    public void afterUndelete() {
    }
}