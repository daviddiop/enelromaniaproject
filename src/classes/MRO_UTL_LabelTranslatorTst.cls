/**
 * Created by tommasobolis on 16/06/2020.
 */

@IsTest
private class MRO_UTL_LabelTranslatorTst {
    @IsTest
    static void testTranslate() {
        String s;
        s=MRO_UTL_LabelTranslator.URL_PARAM_LABEL_SEPARATOR;
        s=MRO_UTL_LabelTranslator.VFP_LABEL_SEPARATOR;
        s=MRO_UTL_LabelTranslator.VFP_LABEL_SEPARATOR_REGEX;
        Test.startTest();
        MRO_UTL_LabelTranslator.translate('prova','EN');
        Test.stopTest();
    }
    @IsTest
    static void testTranslateexc() {
        try{
            Test.startTest();
            List<String> myList;
            MRO_UTL_LabelTranslator.translate(myList,null);
            Test.stopTest();
        }
        catch (Exception e){
            System.debug(e.getMessage());
        }
    }
    @IsTest
    static void testTranslateexc2() {
        try{
            Test.startTest();
            String myString;
            MRO_UTL_LabelTranslator.translate(myString,myString);
            Test.stopTest();
        }
        catch (Exception e){
            System.debug(e.getMessage());
        }
    }
}
