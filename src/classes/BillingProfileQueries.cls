public with sharing class BillingProfileQueries {
    public static BillingProfileQueries getInstance() {
        return new BillingProfileQueries();
    }

    public BillingProfile__c getById(String billingProfileId) {
        return [
                SELECT Id, Name, PaymentMethod__c, DeliveryChannel__c, IBAN__c, Fax__c, Email__c, BillingAddress__c, BillingAddressNormalized__c,
                       BillingStreetType__c, BillingStreetName__c, BillingStreetNumber__c, BillingStreetNumberExtn__c, BillingCity__c, BillingPostalCode__c,
                       BillingCountry__c, BillingBuilding__c, BillingFloor__c, BillingLocality__c, BillingProvince__c, BillingApartment__c
                FROM BillingProfile__c
                WHERE Id = :billingProfileId
        ];
    }

    public List<BillingProfile__c> getByAccountId(String accountId) {
        return [
                SELECT Id, Name, PaymentMethod__c, DeliveryChannel__c, IBAN__c, Fax__c, Email__c, BillingAddress__c, BillingAddressNormalized__c,
                       BillingStreetType__c, BillingStreetName__c, BillingStreetNumber__c, BillingStreetNumberExtn__c, BillingCity__c, BillingPostalCode__c,
                       BillingCountry__c, BillingBuilding__c, BillingFloor__c, BillingLocality__c, BillingProvince__c, BillingApartment__c
                FROM BillingProfile__c
                WHERE Account__c = :accountId
                ORDER BY Name
        ];
    }
}