/**
 * Created by david diop on 29.10.2019.
 */

public with sharing class MRO_LC_NonDisconnectablePOD extends ApexServiceLibraryCnt {
    private static DossierService dossierSrv = DossierService.getInstance();
    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    static String dossierChangeRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    /**
    * @author  David Diop
    * @description Initialize NonDisconnectablePOD wizard.
    * @date 29/10/2019
    * @param dossierId
    * @param accountId
    * @param interactionId
    * @return
    */

    public class Initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = '';
            String genericRequestId = params.get('genericRequestId');
            String templateId = params.get('templateId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }


            try {
                Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierChangeRecordType, 'Non Disconnectable POD');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSr.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                if (String.isNotBlank(dossier.Id)) {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                    response.put('caseTile', cases);
                }

                List<String> pickListValuesList = new List<String>();
                Schema.DescribeFieldResult fieldResult = Case.Reason__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for (Schema.PicklistEntry pickListVal : ple) {
                    pickListValuesList.add(pickListVal.getLabel());
                }

                String code = 'NonDisconnectablePODTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }
                response.put('reason', pickListValuesList);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('accountId', accountId);
                response.put('NonDisconnectablePODEle', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NonDisconnectable_POD_ELE').getRecordTypeId());
                response.put('NonDisconnectablePODGas', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NonDisconnectable_POD_GAS').getRecordTypeId());
                response.put('error', false);
            }  catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    /**
    * @author  David Diop
    * @description CancelProcess NonDisconnectablePOD wizard.
    * @date 29/10/2019
    * @param dossierId
    * @param List<Case>
    * @return
    */
    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            CancelInput cancelCaseParams = (CancelInput) JSON.deserialize(jsonInput, CancelInput.class);
            List<Case> oldCaseList = cancelCaseParams.caseList;
            String dossierId = cancelCaseParams.dossierId;

            caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId);

            response.put('error', false);
            return response;
        }
    }
    /**
    * @author  David Diop
    * @description UpdateCaseList NonDisconnectablePOD wizard.
    * @date 29/10/2019
    * @param dossierId
    * @param List<Case>
    * @return
    */
    public class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');
            String companyDivisionId;

            if (oldCaseList[0] != null) {
                companyDivisionId = oldCaseList[0].CompanyDivision__c;
            }
            caseSrv.setOriginAndChannelOnCaseAndDossier(oldCaseList, dossierId, channel, origin);
            response.put('error', false);
            return response;
        }
    }
    /**
    * @author  David Diop
    * @description getCase NonDisconnectablePOD wizard.
    * @date 29/10/2019
    * @param recordId
    * @return caseRecord
    */

    public class getCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('recordId');
            Case caseRecord = caseQuery.getById(recordId);
            response.put('caseRecord', caseRecord);
            response.put('error', false);
            return response;
        }
    }
    /**
    * @author  David Diop
    * @description createOldCase with NonDisconnectablePOD wizard.
    * @date 29/10/2019
    * @param supply
    * @param nonDisconnectable
    * @param reason
    * @param accountId
    * @param recordTypeId
    * @param dossierId
    * @return caseTile
    */

    public class createOldCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            CreateCaseInput createCaseParams = (CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);
            List<Case> newCases = new List<Case>();
            String RecordTypeNonDisconnectableEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NonDisconnectable_POD_ELE').getRecordTypeId();
            String RecordTypeNonDisconnectableGas = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NonDisconnectable_POD_GAS').getRecordTypeId();
            String recordTypeId;
            response.put('error', false);
            try {

                for (Supply__c supply : createCaseParams.searchedSupplyFieldsList) {
                    recordTypeId = supply.RecordType.DeveloperName == 'Gas' ? RecordTypeNonDisconnectableGas : RecordTypeNonDisconnectableEle;
                    Case nonDisconnectableCase = caseSrv.insertCaseForDisconnectable(supply, createCaseParams.nonDisconnectable, createCaseParams.reason, createCaseParams.accountId, recordTypeId, createCaseParams.dossierId, createCaseParams.origin, createCaseParams.channel);
                    newCases.add(nonDisconnectableCase);
                }
                if (createCaseParams.caseTile != null && !createCaseParams.caseTile.isEmpty()) {
                    for (Case oldCaseList : createCaseParams.caseTile) {
                        newCases.add(oldCaseList);
                    }
                }
                dossierSr.updateDossierChannelAndCommodity(createCaseParams.dossierId, createCaseParams.channel, createCaseParams.origin, createCaseParams.commodity);
                response.put('caseTile', newCases);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    /**
    * @author  DAVID DIOP
    * @description Get supply record
    * @date 26/12/2019
    * @param list <Id> supplies
    *
    */
    public class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            //Map<String, String> params = asMap(jsonInput);
            supplyInputIds createCaseParams = (supplyInputIds) JSON.deserialize(jsonInput, supplyInputIds.class);
            List<Supply__c> supplyListRecord = supplyQuery.getSuppliesByIds(createCaseParams.supplyIds);
            response.put('supplies', supplyListRecord);
            return response;
        }
    }
    public class CreateCaseInput {
        @AuraEnabled
        public List<Supply__c> searchedSupplyFieldsList { get; set; }
        @AuraEnabled
        public Boolean nonDisconnectable { get; set; }
        @AuraEnabled
        public String reason { get; set; }
        @AuraEnabled
        public String accountId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public List<Case> caseTile { get; set; }
        @AuraEnabled
        public String origin { get; set; }
        @AuraEnabled
        public String channel { get; set; }
        @AuraEnabled
        public String commodity { get; set; }
    }
    public class CancelInput {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public List<Case> caseList { get; set; }
    }
    public class supplyInputIds {
        @AuraEnabled
        public List<Id> supplyIds { get; set; }
    }
}