public with sharing class AccountContactRelationCnt extends ApexServiceLibraryCnt {

    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static AccountContactRelationQueries accountContactRelationQuery = AccountContactRelationQueries.getInstance();
    private static IndividualQueries individualQuery = IndividualQueries.getInstance();

    private static CustomerInteractionService customerInteractionSrv = CustomerInteractionService.getInstance();
    private static CustomerInteractionService customerInteractionServiceInstance = CustomerInteractionService.getInstance();
    private static IndividualService individualSrv = IndividualService.getInstance();

    public class listAccountContactRelationByAccountId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            List<AccountContactRelationService.AccountContactRelationDTO> resultList = new List<AccountContactRelationService.AccountContactRelationDTO>();

            Account thisAccount = accountQuery.findAccount(accountId);
            List<AccountContactRelation> relationList = accountContactRelationQuery.listByAccountId(accountId);
            List<AccountContactRelationService.AccountContactRelationDTO> relationDtoList = AccountContactRelationService.mapToDto(relationList);
            if (thisAccount.IsPersonAccount) {
                AccountContactRelationService.AccountContactRelationDTO selfRelation = AccountContactRelationService.mapToDto(thisAccount);
                resultList.add(selfRelation);
            }
            if (relationDtoList.size() > 0) {
                resultList.addAll(relationDtoList);
            }
            return new Map<String, Object>{
                //'relationList' => accountContactRelationQuery.listByAccountId(accountId),
                'relationList' => resultList,
                'account' => thisAccount
            };
        }
    }

   /**
    * getAccountContactRelationForIndividual method finds all the account contact relation
    *   records whose Contact related record is a child of the Individual
    * @author Moussa Fofana
    * @param String jsonInput with individualId
    * @since EAP2-119
    */
    public class listAccountContactRelationByIndividualId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String individualId = params.get('individualId');
            if (String.isBlank(individualId)) {
                throw new WrtsException(System.Label.Individual + ' - ' + System.Label.MissingId);
            }
            return new Map<String, Object>{
                //'relations' => individualSrv.listRelatedContact(individualId),
                'relations' => AccountContactRelationService.mapToDto(individualSrv.listRelatedContact(individualId)),
                'individual' => individualQuery.findById(individualId)
            };
        }
    }

    public class assignContactToInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String contactId = params.get('contactId');
            if (String.isBlank(contactId)) {
                throw new WrtsException(System.Label.Contact + ' - ' + System.Label.MissingId);
            }
            String customerInteractionId = params.get('customerInteractionId');
            if (String.isBlank(customerInteractionId)) {

                throw new WrtsException(System.Label.CustomerInteraction + ' - ' + System.Label.MissingId);
            }
            customerInteractionSrv.assignContactToInteraction(contactId, customerInteractionId);
            return true;
        }
    }
    /**
     * addCustomerInteraction is the method to add new customer interaction from AccountContactRelationsListForIndividualLwc
     * @author Moussa Fofana
     * @since  EAP2-119
     */
    public class addCustomerInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String,Object> response = new Map<String,Object>();
            CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = (CustomerInteractionService.CustomerInteractionDTO)
                JSON.deserializeStrict(jsonInput, CustomerInteractionService.CustomerInteractionDTO.class);
            CustomerInteraction__c customerInteraction = customerInteractionServiceInstance.mapFromDto(customerInteractionDTO);
            if (customerInteraction.Interaction__c == null){
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            if (customerInteraction.Customer__c == null){
                throw new WrtsException(System.Label.Customer + ' - ' + System.Label.MissingId);

            }
            if (customerInteraction.Contact__c == null){
                throw new WrtsException(System.Label.Contact + ' - ' + System.Label.MissingId);
            }
            CustomerInteraction__c createdCustomerInteraction = customerInteractionServiceInstance.upsertCustomerInteraction(customerInteraction);
            response.put('customerInteraction', createdCustomerInteraction);
            return response;
        }
    }
}