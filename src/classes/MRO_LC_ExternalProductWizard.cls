public with sharing class MRO_LC_ExternalProductWizard extends ApexServiceLibraryCnt {
    private static MRO_SRV_VAS vasService = new MRO_SRV_VAS();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static MRO_QR_Opportunity opportunityQuery = MRO_QR_Opportunity.getInstance();
    private static MRO_QR_OpportunityLineItem oliQuery = MRO_QR_OpportunityLineItem.getInstance();
    private static MRO_QR_OpportunityServiceItem osiQuery = MRO_QR_OpportunityServiceItem.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static UserQueries userQuery = UserQueries.getInstance();
    private static MRO_SRV_Opportunity oppService = MRO_SRV_Opportunity.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static PrivacyChangeQueries privacyChangeQuery = PrivacyChangeQueries.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static DossierService dossiersrv = DossierService.getInstance();
    private static MRO_QR_Asset assetQuery = MRO_QR_Asset.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_InstallmentPlan installmentPlanQuery = MRO_QR_InstallmentPlan.getInstance();
    private static MRO_QR_ConfigurationItem configurationItemQuery = MRO_QR_ConfigurationItem.getInstance();
    private static final MRO_UTL_Parsers parsers = MRO_UTL_Parsers.getInstance();
    private static MRO_SRV_Case caseService = MRO_SRV_Case.getInstance();

    public static String expandInstallmentPlan(Decimal totalAmount, Decimal numberOfInstallments, Date startDate, Integer billingFrequencyNumber){
        System.debug('expandInstallmentPlan');
        if (totalAmount == null || numberOfInstallments == null || startDate == null || billingFrequencyNumber == null){
            throw new WrtsException('expandInstallmentPlan: Invalid input');
        }

        List<Map<Date, Decimal>> expandedPlan = new List<Map<Date, Decimal>>();
        Decimal valueOfInstallment = totalAmount / numberOfInstallments;
        Date installmentDate = startDate;

        for (Integer i = 0; i < numberOfInstallments; i++){
            Map<Date, Decimal> newInstallment = new Map<Date, Decimal>();
            newInstallment.put(installmentDate, valueOfInstallment);

            expandedPlan.add(newInstallment);
            installmentDate = installmentDate.addMonths(billingFrequencyNumber);
        }

        return Json.serialize(expandedPlan);
    }



    private static Date getLatestInstallmentDate(InstallmentPlan__c installmentPlan){
        // Validate input
        if (installmentPlan == null){
            System.debug(LoggingLevel.ERROR, 'InstallmentPlan is null');
            return null;
        }
        if (installmentPlan.Plan__c == null || installmentPlan.Plan__c.length() < 10){
            System.debug(LoggingLevel.ERROR, 'InstallmentPlan.Plan__c is empty');
            return null;
        }

        List<Map<Date, Decimal>> expandedPlan = (List<Map<Date, Decimal>>)JSON.deserialize(installmentPlan.Plan__c,List<Map<Date, Decimal>>.class);
        if (expandedPlan.size() == 0){
            System.debug(LoggingLevel.ERROR, 'InstallmentPlan.Plan__c is empty');
            return null;
        }

        // Get the latest entry, it should have the latest installment dates
        Map<Date, Decimal> dateDecimalMap = expandedPlan.get(expandedPlan.size() - 1);

        // Get the latest installment date
        Date lastDate = null;
        for (Date currentDate : dateDecimalMap.keySet()){
            if (lastDate == null || currentDate > lastDate){
                lastDate = currentDate;
            }
        }

        return lastDate;
    }

    @TestVisible
    public static void updateInstallmentPlan(Id caseId){
        System.debug('updateInstallmentPlan');
        if (caseId == null){
            throw new WrtsException('Invalid caseId');
        }
        Case activatorCase = caseQuery.getById(caseId);
        List<InstallmentPlan__c> installmentPlans = installmentPlanQuery.listByCaseId(caseId);
        if (installmentPlans.isEmpty()){
            return ;
        }
        InstallmentPlan__c installmentPlan = installmentPlans.get(0);
        Date installationDate = activatorCase.InvoiceDate__c;

        if (installmentPlan.SelfReadingDay__c == null){
            throw new WrtsException('Installment plan\'s self reading day cannot be null');
        }

        if (installationDate == null){
            installationDate = Date.today();
        }

        Decimal totalAmount = installmentPlan.TotalAmount__c;
        Decimal numberOfInstallments = installmentPlan.NumberOfInstallments__c;
        Date startDate = installmentPlan.StartDate__c;
        Integer billingFrequency = constantsSrv.billingFrequencyToNumberOfMonths(installmentPlan.BillingFrequency__c);

        installmentPlan.StartDate__c = computeStartDate(installationDate, installmentPlan.SelfReadingDay__c);
        installmentPlan.Plan__c = expandInstallmentPlan(totalAmount, numberOfInstallments, startDate, BillingFrequency);

        databaseSrv.updateSObject(installmentPlan);
    }

    @TestVisible
    private static Date computeStartDate(Date installationDate, Decimal selfReadingDay){
        System.debug('computeStartDate');

        if (selfReadingDay == 0){
            selfReadingDay = 1;
        }

        Integer daysInMonth = Date.daysInMonth(installationDate.year(), installationDate.month());
        Date startDate = null;
        if (daysInMonth < selfReadingDay){
            startDate = Date.newInstance(installationDate.year(), installationDate.month() + 1, 1);
            return startDate;
        }
        startDate = Date.newInstance(installationDate.year(), installationDate.month(), parsers.parseInt(selfReadingDay));

        if (installationDate.day() >= startDate.day()) {
            startDate = startDate.addMonths(1);
        }

        startDate = startDate.addDays(1);

        return startDate;
    }

    @TestVisible
    private static MAP<String, Object> createInstallmentPlan(String jsonInput){
        System.debug('createInstallmentPlan');
        Map<String, Object> response = new Map<String, Object>();
        Map<String, String> params = ApexServiceLibraryCnt.asMap(jsonInput);
        Id caseId = parsers.parseId(params.get('caseId'));
        Id supplyId = parsers.parseId(params.get('supplyId'));
        Date deliveryDate = (Date)JSON.deserialize(params.get('deliveryDate'), Date.class );

        Savepoint sp = Database.setSavepoint();
        try {
            if (caseId == null || supplyId == null) {
                throw new WrtsException('Exception in createInstallmentPlan: caseId or selectedSupplyId is empty');
            }
            Case activatorCase = caseQuery.getById(caseId);
            Supply__c supply = supplyQuery.getById(supplyId);
            String billingFrequency = supply.ContractAccount__r.BillingFrequency__c;
            Integer selfReadingDay = parsers.parseInt(supply.ContractAccount__r.SelfReadingPeriodEnd__c);
            Decimal totalAmount = activatorCase.Amount__c;
            Decimal numberOfInstallments = activatorCase.Count__c;
            Date startDate = computeStartDate(deliveryDate, selfReadingDay);
            Integer billingFrequencyNumber = constantsSrv.billingFrequencyToNumberOfMonths(billingFrequency);

            InstallmentPlan__c newInstallmentPlan = new InstallmentPlan__c();
            newInstallmentPlan.Case__c = activatorCase.Id;
            newInstallmentPlan.Asset__c = activatorCase.AssetId;
            newInstallmentPlan.Customer__c = activatorCase.AccountId;
            newInstallmentPlan.BillingFrequency__c = billingFrequency;
            newInstallmentPlan.SelfReadingDay__c = selfReadingDay;
            newInstallmentPlan.TotalAmount__c = totalAmount;
            newInstallmentPlan.NumberOfInstallments__c = numberOfInstallments;
            newInstallmentPlan.StartDate__c = startDate;
            newInstallmentPlan.Plan__c = expandInstallmentPlan(totalAmount, numberOfInstallments, startDate, billingFrequencyNumber);
            databaseSrv.insertSObject(newInstallmentPlan);

            System.debug('createInstallmentPlan - ok');
            response.put('error', false);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
            Database.rollback(sp);
        }
        return response;
    }

    @TestVisible
    private static MAP<String, Object> createInstallmentPlan(Id caseId, Id supplyId){
        Map<String, String> inputJSON = new Map<String, String>{
            'caseId' => caseId,
            'supplyId' => supplyId,
            'deliveryDate' => JSON.serialize(Date.today())
        };

        return createInstallmentPlan(JSON.serialize(inputJSON));
    }

    @TestVisible
    private static MAP<String, Object> createInstallmentPlan(Id caseId, Id supplyId, Date deliveryDate){
        if (deliveryDate == null){
            deliveryDate = Date.today();
        }
        Map<String, String> inputJSON = new Map<String, String>{
            'caseId' => caseId,
            'supplyId' => supplyId,
            'deliveryDate' => JSON.serialize(deliveryDate)
        };

        return createInstallmentPlan(JSON.serialize(inputJSON));
    }

    public with sharing class createInstallmentPlan extends AuraCallable {
        public override Object perform(final String jsonInput) {
           return createInstallmentPlan(jsonInput);
        }
    }

    private static void initializeExternallyStartedTermination(MRO_SRV_VAS.InitializeResponseData response, Id caseId){
        Case caseRecord = caseQuery.getById(caseId);
        if (caseRecord == null) {
            return ;
        }
        if (caseRecord.Supply__c == null){
            throw new WrtsException(System.Label.Supply + ' ' + System.Label.IsMissing);
        }
        caseRecord.Status = 'Canceled';
        caseRecord.Phase__c = 'AN010';
        databaseSrv.updateSObject(caseRecord);

        // Retrieve all service supply info
        caseRecord.Supply__r = supplyQuery.getById(caseRecord.Supply__c);

        Id opportunityOfTerminatingCaseId = caseRecord.Dossier__r.Opportunity__c;
        Opportunity opportunityOfTerminatingCase = opportunityQuery.getById(opportunityOfTerminatingCaseId);

        oppService.setChannelAndOrigin(response.opportunityId, caseRecord.Dossier__c, opportunityOfTerminatingCase.Origin__c, opportunityOfTerminatingCase.Channel__c);
        response.opportunity = opportunityQuery.getById(response.opportunityId);
        response.opportunity.SubProcess__c = constantsSrv.OPPORTUNITY_SUB_PROCESS_CANCELLATION;
        databaseSrv.updateSObject(response.opportunity);

        response.dossier.Origin__c = response.opportunity.Origin__c;
        response.dossier.Channel__c = response.opportunity.Channel__c;
        response.dossier.SubProcess__c = response.opportunity.SubProcess__c;
        databaseSrv.updateSObject(response.dossier);

        response.caseToTerminate = caseRecord;
    }

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = ApexServiceLibraryCnt.asMap(jsonInput);
            Id caseToTerminateId = parsers.parseId(params.get('caseToTerminateId'));

            MRO_SRV_VAS.InitializeResponseData result = vasService.wizardInit(jsonInput, constantsSrv.REQUEST_TYPE_EXTERNAL_PRODUCT);

            if (caseToTerminateId != null){
                initializeExternallyStartedTermination(result, caseToTerminateId);
            }

            System.debug('caseToTerminate');
            System.debug(result.caseToTerminate.Supply__c);

            result.numberOfInstallmentsOptions = getNumberOfInstallmentsOptions(result.opportunityLineItems);
            return result;
        }
    }

    private static List<MRO_SRV_VAS.InstallmentOption> getNumberOfInstallmentsOptions(List<OpportunityLineItem> opportunityLineItems){
        if (opportunityLineItems.isEmpty()){
            System.debug('empty opportunityLineItems');
            return null;
        }

        OpportunityLineItem opportunityLineItem = opportunityLineItems.get(0);

        if (opportunityLineItem.Product2Id == null){
            System.debug('empty Product2Id');
            return null;
        }
        Product2 product = MRO_QR_Product.getInstance().getById(opportunityLineItem.Product2Id);

        String rawNumberOfInstallmentsOptions = product.CommercialProduct__r.InstallmentsNumbers__c;
        if (String.isEmpty(rawNumberOfInstallmentsOptions)){
            System.debug('empty number of installments');
            return null;
        }

        List<MRO_SRV_VAS.InstallmentOption> numberOfInstallmentsOptions = new List<MRO_SRV_VAS.InstallmentOption>();
        for(String option : rawNumberOfInstallmentsOptions.split(';')){
                numberOfInstallmentsOptions.add(new MRO_SRV_VAS.InstallmentOption(option, option));
        }

        return numberOfInstallmentsOptions;
    }

    public with sharing class processSelectedOli extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.processSelectedOli(jsonInput);
        }
    }

    public with sharing class checkOliThresholds extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.checkOliThresholds(jsonInput);
        }
    }

    public with sharing class checkCommoditySupply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.checkCommoditySupply(jsonInput);
        }
    }
    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.setChannelAndOrigin(jsonInput);
        }
    }

    public with sharing class updateCurrentFlow extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.updateCurrentFlow(jsonInput);
        }
    }

    public with sharing class updateCompanyDivisionInOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.updateCompanyDivision(jsonInput);
        }
    }

    public with sharing class updateContractInfo extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.updateContractInfo(jsonInput);
        }
    }

    public with sharing class afterNewOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.afterOSI(jsonInput, constantsSrv.REQUEST_TYPE_EXTERNAL_PRODUCT);
        }
    }

    public with sharing class handleServiceSupplySelection extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.handleServiceSupplySelection(jsonInput);
        }
    }

    private static void hardVasCancellation(Opportunity opportunity, Id dossierId, Map<String, Object> opportunityParams){
        System.debug('hardVasCancellation');
        Id selectedToTerminateSupplyId = (Id)opportunityParams.get('selectedToTerminateSupplyId');
        if (selectedToTerminateSupplyId == null){
            throw new WrtsException('SupplyId is null');
        }

        Supply__c supplyToTerminate = MRO_QR_Supply.getInstance().getById(selectedToTerminateSupplyId);
        if (supplyToTerminate == null) {
            throw new WrtsException('supplyToTerminate is null');
        }

        if (supplyToTerminate.Activator__c == null) {
            throw new WrtsException('Selected VAS doesn\'t have a valid activation case');
        }
        Case supplyToTerminateActivator = MRO_QR_Case.getInstance().getById(supplyToTerminate.Activator__c);
        Dossier__c supplyToTerminateDossier = dossierQuery.getById(supplyToTerminateActivator.Dossier__c);

        String terminationReason = (String)opportunityParams.get('terminationReason');
        Case activator = MRO_QR_Case.getInstance().getById(supplyToTerminate.Activator__c);
        Case terminator = MRO_SRV_Case.getInstance().instantiatePrefilled(opportunity, null, null, dossierId);
        terminator.RecordTypeId = vasService.getVasCaseRecordTypeId(opportunity);
        terminator.ContactId = activator.ContactId;
        terminator.AssetId = activator.AssetId;
        terminator.Contract__c = activator.Contract__c;
        terminator.Dossier__c = dossierId;
        terminator.Supply__c = activator.Supply__c;
        terminator.SubProcess__c = opportunity.SubProcess__c;
        terminator.Reason__c = terminationReason.trim();
        terminator.SLAExpirationDate__c = Date.today().addDays(constantsSrv.EXTERNAL_PRODUCT_SLA_EXPIRATION_OFFSET);
        terminator.EffectiveDate__c = Date.today();
        databaseSrv.upsertSObject(terminator);

        terminator = MRO_QR_Case.getInstance().getById(terminator.Id);
        supplyToTerminate.Status__c = constantsSrv.SUPPLY_STATUS_TERMINATING;
        supplyToTerminate.Terminator__c = terminator.Id;
        supplyToTerminate.TerminationReason__c = terminationReason.trim();
        databaseSrv.updateSObject(supplyToTerminate);

        // Update contract status
        Contract contract = MRO_QR_Contract.getInstance().getById(supplyToTerminate.Contract__c);
        contract.Status = constantsSrv.CONTRACT_STATUS_TERMINATED;
        databaseSrv.updateSObject(contract);

        // Update dossier
        Dossier__c dossier = dossierQuery.getById(dossierId);
        dossier.RecordTypeId = vasService.getVasDossierRecordTypeId(opportunity);
        dossier.Status__c = constantsSrv.DOSSIER_STATUS_NEW;
        dossier.Commodity__c = supplyToTerminateDossier.Commodity__c;
        databaseSrv.updateSObject(dossier);

        System.debug('Hard Vas Cancellation - Done');
    }

    private static void hardVasActivation(Opportunity opportunity, Id dossierId, Map<String, Object> opportunityParams){
        System.debug('hardVasActivation');
        Contract contract = contractQuery.getById(opportunity.ContractId);

        // Check OSI
        List<OpportunityServiceItem__c> osiList = osiQuery.getOSIsByOpportunityId(opportunity.Id);
        if (osiList.size() == 0){
            return ;
        }
        OpportunityServiceItem__c osi = osiList.get(0);

        // Check Contract
        if (contract.ContractType__c == null) {
            System.debug(LoggingLevel.ERROR, 'contract fields are not filled');
            System.debug(contract.ContractType__c);
            return;
        }

        List<Supply__c> supplies = supplyQuery.listByContractIds(new Set<Id>{contract.Id});
        if (supplies.size() == 0) {
            System.debug(LoggingLevel.ERROR, 'no supplies have been found for contract ' + contract.Id);
            return;
        }
        Supply__c supply = supplies.get(0);

        Supply__c commoditySupply = supplyQuery.getById(osi.Supply__c);

        Id assetId = null;

        // Retrieve the activation case
        Id activatorCaseRecordTypeId = vasService.getVasCaseRecordTypeId(opportunity);
        List<Case> cases = caseQuery.listByContractAndRecordType(contract.Id, activatorCaseRecordTypeId);
        if (cases.isEmpty()) {
            System.debug(LoggingLevel.ERROR, 'listByContractIdAndRecordTypeId is empty');
            return;
        }
        Case activatorCase = cases.get(0);
        assetId = activatorCase.AssetId;

        Date deliveryDate = null;
        if (String.isNotBlank(String.valueOf(opportunityParams.get('deliveryDate')))){
            deliveryDate = Date.valueOf(opportunityParams.get('deliveryDate').toString());
            contract.StartDate = deliveryDate;
        }
        Integer numberOfPieces = parsers.parseInt(opportunityParams.get('numberOfPieces'));
        Integer numberOfInstallments = parsers.parseInt(opportunityParams.get('numberOfInstallments'));
        Id productId = null;
        Double productPrice = null;
        String productName = null;
        Boolean isPromo = false;
        NE__OrderItem__c configurationItem = null;

        productId = parsers.parseId(opportunityParams.get('productId'));
        if (productId == null){
            System.debug('productId is blank');
            return ;
        }

        OpportunityLineItem oli = null;
        List<OpportunityLineItem> opportunityLineItemList = oliQuery.listByProductAndOpportunity(productId, opportunity.Id);
        if (opportunityLineItemList.size() > 0){
            oli = opportunityLineItemList.get(0);
            oli.Quantity = numberOfPieces;
            databaseSrv.updateSObject(oli);

            productName = oli.Product2.Name;
            productPrice = oli.UnitPrice;
            isPromo = oli.Product2.CommercialProduct__r.NE__IsPromo__c;
            configurationItem = configurationItemQuery.getConfigurationItemsByCommercialProductId(oli.Product2.CommercialProduct__c).get(0);
        }

        Double totalAmount = (numberOfPieces * productPrice);

        Asset asset = assetQuery.getById(assetId);
        asset.Name = productName;
        asset.NE__Delivery_Date__c = deliveryDate;
        asset.Price = productPrice;
        asset.Quantity = numberOfPieces;
        asset.Product2Id = productId;
        asset.NE__IsPromo__c = isPromo;
        asset.NE__BaseOneTimeFee__c = configurationItem.NE__BaseOneTimeFee__c;

        MRO_SRV_Address.AddressDTO assetAddress = new MRO_SRV_Address.AddressDTO(osi, 'Point');
        assetAddress.populateRecordAddressFields(asset, 'Asset');
        asset.NE__Address_Line1__c = osi.PointAddress__c;
        asset.AssetAddressNormalized__c = osi.PointAddressNormalized__c;
        databaseSrv.updateSObject(asset);
        activatorCase.CompanyDivision__c = opportunity.CompanyDivision__c;
        activatorCase.Count__c = numberOfInstallments;
        activatorCase.Amount__c = totalAmount;
        activatorCase.Supply__c = supply.Id;
        activatorCase.SLAExpirationDate__c = Date.today().addDays(constantsSrv.EXTERNAL_PRODUCT_SLA_EXPIRATION_OFFSET);
        activatorCase.EffectiveDate__c = Date.today();
        if (commoditySupply != null && osi.ServiceSite__r != null) {
            if (commoditySupply.RecordType.DeveloperName == constantsSrv.COMMODITY_ELECTRIC) {
                if (osi.ServiceSite__r.ActiveSuppliesCountELE__c == null || osi.ServiceSite__r.ActiveSuppliesCountELE__c < 1) {
                    activatorCase.Status = constantsSrv.CASE_STATUS_ONHOLD;
                }
            } else if (commoditySupply.RecordType.DeveloperName == constantsSrv.COMMODITY_GAS) {
                if (osi.ServiceSite__r.ActiveSuppliesCountGAS__c == null || osi.ServiceSite__r.ActiveSuppliesCountGAS__c < 1) {
                    activatorCase.Status = constantsSrv.CASE_STATUS_ONHOLD;
                }
            }
        }
        databaseSrv.updateSObject(activatorCase);
        System.debug('after activation case has been updated');

        createInstallmentPlan(activatorCase.Id, supply.Id, deliveryDate);

        supply.Activator__c = activatorCase.Id;
        supply.Market__c = constantsSrv.MARKET_FREE;
        supply.Status__c = constantsSrv.SUPPLY_STATUS_ACTIVATING;
        supply.Product__c = productId;
        supply.ServicePoint__c = null;

        databaseSrv.updateSObject(supply);
        System.debug('after supply has been updated');

        Dossier__c dossier = dossierQuery.getById(dossierId);
        dossier.RecordTypeId = vasService.getVasDossierRecordTypeId(opportunity);
        dossier.Status__c = constantsSrv.DOSSIER_STATUS_NEW;
        dossier.CommercialProduct__c = oli.Product2.CommercialProduct__c;
        dossier.ReferenceDate__c = deliveryDate;
        databaseSrv.updateSObject(dossier);

        InstallmentPlan__c installmentPlan = installmentPlanQuery.getByAssetId(assetId);
        contract.StartDate = installmentPlan.StartDate__c;

        // This is to update Contract.EndDate
        Integer contractTerm = calculateContractTerm(installmentPlan);
        contract.ContractTerm = contractTerm;
        databaseSrv.updateSObject(contract);

        // Contract end date can't be sooner than last installment date
//        if (contract.EndDate < getLatestInstallmentDate(installmentPlan)){
//            System.debug('Adding another month to contract term');
//            contract.ContractTerm += 1;
//        }

        contract.Status = constantsSrv.CONTRACT_STATUS_DRAFT;
        databaseSrv.updateSObject(contract);

        System.debug('hard vas activation - ok');
    }

    public static Integer calculateContractTerm(InstallmentPlan__c installmentPlan){
        Date startDate = installmentPlan.StartDate__c;
        Date endDate = getLatestInstallmentDate(installmentPlan);

        if (startDate == null || endDate == null){
            return 0;
        }

        Integer contractTerm = startDate.monthsBetween(endDate);
        return contractTerm + 1;
    }

    public class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return vasService.cancelEverything(jsonInput);
        }
    }

    public with sharing class deleteOlis extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId=params.get('opportunityId');
            Boolean isDeleted = false;
            Map<String, Object> response = new Map<String, Object>();
            try {
                List<OpportunityLineItem> opportunityLineItems = oliQuery.getOLIsByOpportunityId(opportunityId);

                if (opportunityLineItems != null && !opportunityLineItems.isEmpty()) {

                    List<NE__Order__c> orders = oliQuery.getOrdersByOpportunityId(opportunityId);

                    databaseSrv.deleteSObject(opportunityLineItems);
                    if (orders != null && !orders.isEmpty()) {
                        databaseSrv.deleteSObject(orders);
                    }

                    isDeleted = true;
                }
                response.put('isDeleted', isDeleted);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String currentFlow = params.get('currentFlow');
            String selectedCommodity = params.get('selectedCommodity');
            String dossierId = params.get('dossierId');
            String numberOfPieces = params.get('numberOfPieces');
            String numberOfInstallments = params.get('numberOfInstallments');
            String deliveryDate = params.get('deliveryDate');
            String productId = params.get('productId');
            String terminationReason = params.get('terminationReason');
            String stage = params.get('stage');
            String selectedToTerminateSupplyId = params.get('selectedToTerminateSupplyId');
            Map<String, Object> response = new Map<String, Object>();

            Boolean isHardVasCancellation = currentFlow == constantsSrv.OPPORTUNITY_SUB_PROCESS_CANCELLATION;
            Boolean isHardVasInstantDelivery = currentFlow == constantsSrv.OPPORTUNITY_SUB_PROCESS_INSTANT_DELIVERY;
            Boolean isHardVasWithoutInstallation = currentFlow == constantsSrv.OPPORTUNITY_SUB_PROCESS_DISPATCHED_WITHOUT_INSTALLATION;
            Boolean isHardVasWithInstallation = currentFlow == constantsSrv.OPPORTUNITY_SUB_PROCESS_DISPATCHED_WITH_INSTALLATION;

            Savepoint sp = Database.setSavepoint();
            try {
                // Update opportunity details
                Opportunity opp = MRO_QR_Opportunity.getInstance().getById(opportunityId);
                opp.StageName = stage;
                databaseSrv.upsertSObject(opp);

                if (stage == constantsSrv.OPPORTUNITY_STAGE_QUOTED) {
                    Map<String, String> opportunityParams = new Map<String, String>();
                    opportunityParams.put('currentFlow', currentFlow);
                    opportunityParams.put('productId', productId);
                    opportunityParams.put('numberOfPieces', numberOfPieces);
                    opportunityParams.put('numberOfInstallments', numberOfInstallments);
                    opportunityParams.put('deliveryDate', deliveryDate);
                    opportunityParams.put('selectedCommodity', selectedCommodity);
                    opportunityParams.put('terminationReason', terminationReason);
                    opportunityParams.put('selectedToTerminateSupplyId', selectedToTerminateSupplyId);

                    if (!isHardVasCancellation) {
                        vasService.runAcquisitionChain(opp, dossierId);
                        hardVasActivation(opp, dossierId, opportunityParams);
                    } else {
                        hardVasCancellation(opp, dossierId, opportunityParams);
                    }

                    try {
                        caseService.applyAutomaticTransitionOnDossierAndCases(dossierId);
                    } catch (Exception ex){
                        // Temporary solution
                        response.put('warning', true);
                        response.put('warningMsg', 'Automatic phase transitioning failed');

                        System.debug(ex.getMessage());
                    }
                }

                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                System.debug(ex.getMessage());
                System.debug(ex.getStackTraceString()); response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}