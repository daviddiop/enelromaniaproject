/**
 * Created by David Diop on 11.11.2019.
 */
@IsTest
public with sharing class MRO_LC_TaxSubsidyEditTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createCustomerCodeSequencer().build();
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[0].Id;
        insert billingProfile;
        for (Integer i = 0; i < 5; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        TaxSubsidy__c taxSubsidy = MRO_UTL_TestDataFactory.taxSubsidy().build();
        taxSubsidy.Supply__c = supplyList[0].Id;
        insert taxSubsidy;

        for (Integer i = 0; i < 5; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            //caseRecord.Tax_Subsidy__c = taxSubsidy.Id;
            caseList.add(caseRecord);
        }
        insert caseList;


    }
    @IsTest
    public static void InitializeTest() {
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'getSubventionType', '', true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result != null );

        Test.stopTest();
    }



    @IsTest
    public static void getSubventionRecordTest() {
        Test.startTest();
        TaxSubsidy__c taxSubsidy = [
                SELECT Id
                FROM TaxSubsidy__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>();
        inputJSON = new Map<String, String>{
                'recordId' => taxSubsidy.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'getSubventionRecord', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('subventionRecord') != null );
        Test.stopTest();
    }

    @IsTest
    public static void getdeleteRecordTest() {
        Test.startTest();
        TaxSubsidy__c taxSubsidy = [
                SELECT Id
                FROM TaxSubsidy__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>();
        inputJSON = new Map<String, String>{
                'recordId' => taxSubsidy.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'deleteTaxSubsidy', inputJSON, true);
        Test.stopTest();
    }

    @IsTest
    public static void notActiveTaxSubsidyTest(){
        Test.startTest();
        TaxSubsidy__c taxSubsidy = [
                SELECT Id
                FROM TaxSubsidy__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>();
        inputJSON = new Map<String, String>{
                'recordId' => taxSubsidy.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'notActiveTaxSubsidy', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result != null );
        Test.stopTest();
    }
    @IsTest
    public static void notActiveTaxSubsidyExceptionTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>();
        inputJSON = new Map<String, String>{
                'recordId' => 'taxSubsidy'
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'notActiveTaxSubsidy', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    public static void createTaxSubsidyTest(){
        TaxSubsidy__c taxSubsidy = MRO_UTL_TestDataFactory.taxSubsidy().build();
        String field = JSON.serialize(taxSubsidy);
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'fields' => field
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'createTaxSubsidy', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result != null);
        Test.stopTest();

    }

    @IsTest
    public static void createTaxSubsidyExceptionTest(){
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'createTaxSubsidy', null, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();

    }

    @IsTest
    public static void createCaseToTaxSubsidyTest(){
        List<Supply__c> listSupplies =[
                SELECT Id
                FROM Supply__c
                LIMIT 2
        ];

        String caseId  = [
                SELECT Id
                FROM Case
                LIMIT 1
        ].Id;
        String accountId = [
                SELECT Id
                FROM Account
                LIMIT 1
        ].Id;
        String dossId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        String supplies = JSON.serialize(listSupplies);
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'suppliesList' => supplies,
                'parentCaseId' => caseId,
                'accountId' => accountId,
                'dossierId'  => dossId,
                'origin' => selectedOrigin,
                'channel' =>selectedChannel
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'createCaseToTaxSubsidy', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result != null);
        Test.stopTest();
    }

    @IsTest
    public static void createCaseToTaxSubsidyEmptySuppliesTest(){
        List<Supply__c> listSupplies =new List<Supply__c>();

        String caseId  = [
                SELECT Id
                FROM Case
                LIMIT 1
        ].Id;
        String accountId = [
                SELECT Id
                FROM Account
                LIMIT 1
        ].Id;
        String dossId = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ].Id;
        String supplies = JSON.serialize(listSupplies);
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'suppliesList' => supplies,
                'parentCaseId' => caseId,
                'accountId' => accountId,
                'dossierId'  => dossId,
                'origin' => selectedOrigin,
                'channel' =>selectedChannel
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'createCaseToTaxSubsidy', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result != null);
        Test.stopTest();
    }
    @IsTest
    public static void createCaseToTaxSubsidyExeceptionTest(){
        List<Supply__c> listSupplies =new List<Supply__c>();
        String supplies = JSON.serialize(listSupplies);
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'suppliesList' => supplies,
                'parentCaseId' => 'caseId',
                'accountId' => 'accountId',
                'dossierId'  => 'dossId'
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'createCaseToTaxSubsidy', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    public static void getdeleteRecordExcepTest() {
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>();
        inputJSON = new Map<String, String>{
                'recordId' => 'recordid'
        };
        Object response = TestUtils.exec(
                'MRO_LC_TaxSubsidyEdit', 'deleteTaxSubsidy', null, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

}