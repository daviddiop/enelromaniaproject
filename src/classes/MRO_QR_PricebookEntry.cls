/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 23, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_PricebookEntry {
    public static MRO_QR_PricebookEntry getInstance() {
        return (MRO_QR_PricebookEntry) ServiceLocator.getInstance(MRO_QR_PricebookEntry.class);
    }

    public List<PricebookEntry> listPricebookEntriesInPricebookByProductIds(Id pricebookId, Set<Id> productIds) {
        return [SELECT Id, Product2Id, UnitPrice FROM PricebookEntry
                WHERE Product2Id IN :productIds AND Pricebook2Id = :pricebookId];
    }
}