/**
 * Created by Vlad Mocanu on 05/02/2020.
 */

public with sharing class MRO_LC_WarrantiesInquiry extends ApexServiceLibraryCnt {

    private static final Map<String, String> REASON_CODE_MAP = new Map<String, String>{
            '0001' => 'Late Payment',
            '0002' => 'Renters/No propriety documents',
            '0003' => 'Temporary Contract ( Fixed Duration <6 Months)',
            '0004' => 'Electricity Theft'
    };

    private static final Map<String, String> TYPE_CODE_MAP = new Map<String, String>{
            '0001' => 'Bank Warranty Letter',
            '0002' => 'Bank Deposit',
            '0003' => 'Government securities',
            '0004' => 'Deposit certificates',
            '0005' => 'Insurance policy',
            '0006' => 'Amount delivered executor',
            '0007' => 'Amount delivered C.E.C warranty'
    };

    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();

    public class getContractNumber extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String contractId = params.get('contractId');
            Map<String, Object> response = new Map<String, Object>();

            try {
                if (String.isNotBlank(contractId)) {
                    response.put('contractNumber', contractQuery.getById(contractId).ContractNumber);
                    response.put('error', false);
                } else {
                    response.put('error', true);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class listWarranties extends AuraCallable {
        public override Object perform(final String jsonInput) {
            List<String> objectList = new List<String>{'Account', 'ContractAccount__c', 'Contract'};

            WarrantyRequestParam params = (WarrantyRequestParam) JSON.deserialize(jsonInput, WarrantyRequestParam.class);
            String objectName = '';
            if (String.isNotBlank(params.recordId)) {
                Id newRecordId = params.recordId;
                objectName = newRecordId.getSObjectType().getDescribe().getName();
                if(objectName == 'Account'){
                    Account acc = MRO_QR_Account.getInstance().findAccount(params.recordId);
                    if(acc != null){
                        params.NR_EXT_PA = acc.AccountNumber;
                    }
                }
            }
            if (objectList.contains(objectName)) {
                return listWarranty(params);
            } else {
                if (params.contractNumber == null) {
                    throw new WrtsException('Contract Number is missing');
                }
                if (params.contractId == null) {
                    throw new WrtsException('Contract Id is missing');
                }
                if (params.ID_INTEGR_CTR == null) {
                    throw new WrtsException('Contract Integration Key is missing');
                }
                return listWarranty(params);
            }
        }
    }

    public static List<Warranty> listWarranty(WarrantyRequestParam params) {
        if(MRO_SRV_SapQueryWarrantiesCallOut.checkSapEnabled()){
            return MRO_SRV_SapQueryWarrantiesCallOut.getInstance().getList(params);
        }else{
            return listDummyWarranty(params);
        }
    }

    public static List<Warranty> listDummyWarranty(WarrantyRequestParam params) {
        List<MRO_LC_WarrantiesInquiry.Warranty> warrantyList = new List<MRO_LC_WarrantiesInquiry.Warranty>();

        Date today = Date.today();
        List<Date> dateList = new List<Date>();
        List<Date> reverseDateList = new List<Date>();

        for (Integer i = 0; i < 5; i++) {
            dateList.add(today.addDays(-i - 1));
        }

        for (Integer i = dateList.size() - 1; i >= 0; i--) {
            reverseDateList.add(dateList[i]);
        }


        for (Integer i = 0; i < 5; i++) {
            warrantyList.add(new Warranty(
                    '100000' + i,
                    '200000' + i,
                    10000 + i,
                    reverseDateList[i],
                    'Consumator rau platnic',
                    'Certificat de depozit'
            ));
        }
        return warrantyList;
    }

    public class Warranty {
        @AuraEnabled
        public String warrantyId { get; set; }
        @AuraEnabled
        public String contractReference { get; set; }
        @AuraEnabled
        public Double amount { get; set; }
        @AuraEnabled
        public Date startDate { get; set; }
        @AuraEnabled
        public String reason { get; set; }
        @AuraEnabled
        public String type { get; set; }

        public Warranty(String warrantyId, String contractReference, Double amount,
                Date startDate, String reason, String type) {
            this.warrantyId = warrantyId;
            this.contractReference = contractReference;
            this.amount = amount;
            this.startDate = startDate;
            this.reason = reason;
            this.type = type;
        }

        public Warranty(Map<String, String> stringMap) {
            this.warrantyId = stringMap.get('WarrantyID');
            this.contractReference = stringMap.get('SupplyIntegrationKey');
            this.amount = MRO_SRV_SapQueryWarrantiesCallOut.parseDouble(stringMap.get('Amount'));
            this.startDate = MRO_SRV_SapQueryWarrantiesCallOut.parseDate(stringMap.get('StartDate'));
            String reasonCode = stringMap.get('Reason');
            this.reason = reasonCode != null ? REASON_CODE_MAP.get(reasonCode) : reasonCode;
            String typeCode = stringMap.get('Type');
            this.type = typeCode != null ? TYPE_CODE_MAP.get(typeCode) : typeCode;
        }
    }

    public class WarrantyRequestParam {

        @AuraEnabled
        public String contractNumber { get; set; }
        @AuraEnabled
        public String contractId { get; set; }
        @AuraEnabled
        public String ID_INTEGR_CTR { get; set; }
        @AuraEnabled
        public String NR_EXT_PA { get; set; }
        @AuraEnabled
        public String recordId { get; set; }

        public WarrantyRequestParam(String contractNumber, String contractId, String ID_INTEGR_CTR, String NR_EXT_PA, String recordId) {
            this.contractNumber = contractNumber;
            this.contractId = contractId;
            this.ID_INTEGR_CTR = ID_INTEGR_CTR;
            this.NR_EXT_PA = NR_EXT_PA;
            this.recordId = recordId;
        }
    }

}