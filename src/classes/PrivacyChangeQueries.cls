/**
 * Created by ferhati on 05/08/2019.
 */

public with sharing class PrivacyChangeQueries {
    public static PrivacyChangeQueries getInstance() {
        return (PrivacyChangeQueries)ServiceLocator.getInstance(PrivacyChangeQueries.class);
    }

    public PrivacyChange__c getPrivacyChange (String individualId) {
        List<PrivacyChange__c> privacyChange = [
                SELECT Id,Dossier__c,Opportunity__c
                FROM PrivacyChange__c
                WHERE Individual__c = :individualId and Status__c = 'Active'
                LIMIT 1
        ];
        if (privacyChange.isEmpty()) {
            return null;
        }
        return privacyChange.get(0);
    }

    public PrivacyChange__c getPrivacyChangeByOpportunityId (String opportunityId) {
        List<PrivacyChange__c> privacyChange = [
                SELECT Id,Dossier__c,Opportunity__c
                FROM PrivacyChange__c
                WHERE Opportunity__c = :opportunityId
                LIMIT 1
        ];
        if (privacyChange.isEmpty()) {
            return null;
        }
        return privacyChange.get(0);
    }

    public List<PrivacyChange__c> getByIds(Set<Id> ids) {
        return [
                SELECT Id, Dossier__c, Opportunity__c, Status__c, EffectiveDate__c, DossierType__c, Individual__c,
                       HasOptedOutGeoTracking__c, HasOptedOutSolicit__c, HasOptedOutProcessing__c, HasOptedOutProfiling__c,
                       HasOptedOutTracking__c, SendIndividualData__c, ShouldForget__c, CanStorePiiElsewhere__c
                FROM PrivacyChange__c
                WHERE  Dossier__c IN :ids
        ];
    }



    public List<PrivacyChange__c> getPrivacyChangeIndividuals(Set<Id> ids) {
        return [
                SELECT Id, Dossier__c, Opportunity__c, Status__c, EffectiveDate__c, DossierType__c, Individual__c,
                       HasOptedOutGeoTracking__c, HasOptedOutSolicit__c, HasOptedOutProcessing__c, HasOptedOutProfiling__c,
                       HasOptedOutTracking__c, SendIndividualData__c, ShouldForget__c, CanStorePiiElsewhere__c
                FROM PrivacyChange__c
                WHERE Individual__c IN :ids and Status__c='Active'
        ];
    }
}