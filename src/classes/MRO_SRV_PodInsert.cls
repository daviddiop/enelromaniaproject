/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 18.05.20.
 */

public with sharing class MRO_SRV_PodInsert implements MRO_INT_MassiveHelper {

    private static Map<String, Account> accountByKeyMap;
    private static Map<String, String> servicePointCodeToIdMap;

    private static String traderRtId;
    private static String distributorRtId;
    private static Boolean applicantNotHolder;
    private static Boolean isDiscoEnel;

    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    private static FileImportDataRow__c currentRow;

    public Map<String, MRO_BA_MassiveHelperValidation.FileColumnDataDTO> performValidAction(Map<String, MRO_BA_MassiveHelperValidation.FileColumnDataDTO> mapValues) {
        return mapValues;
    }

    public void performFinishAction(List<FileImportDataRow__c> rowsList, Map<String, String> params) {
        List<OsiDTO> osiDtoList = new List<OsiDTO>();
        Set<String> accountKeys = new Set<String>();
        Set<String> servicePointCodes = new Set<String>();

        List<OpportunityServiceItem__c> osiToInsert;
        try {
            String opportunityId = params.get('opportunityId');
            String commodity = params.get('commodity');
            if (commodity == null) {
                throw new WrtsException('Empty commodity');
            }
            String recordTypeId = constantsSrv.OSIRecordTypes.get(commodity);

            List<MRO_SRV_Address.AddressDTO> addressDTOList = new List<MRO_SRV_Address.AddressDTO>();

            List<OpportunityServiceItem__c> osiList = [SELECT Id, ApplicantNotHolder__c, Distributor__r.IsDisCoENEL__c FROM OpportunityServiceItem__c WHERE Opportunity__c = :opportunityId];
            if (osiList.size() > 0) {
                applicantNotHolder = osiList[0].ApplicantNotHolder__c;
                isDiscoEnel = osiList[0].Distributor__r.IsDisCoENEL__c;
            }

            Integer addressDtoKey = 0;
            for (FileImportDataRow__c row : rowsList) {
                currentRow = row;
                OsiDTO osiDto = (OsiDTO) JSON.deserialize(row.JSONRow__c, OsiDTO.class);
                osiDto.fidr = row;

                osiDto.validate(commodity);

                osiDtoList.add(osiDto);
                addressDTOList.add(osiDto.getPointAddressDto(addressDtoKey));
                addressDtoKey++;
                addressDTOList.add(osiDto.getSiteAddressDto(addressDtoKey));
                addressDtoKey++;
                accountKeys.add(osiDto.Trader);
                accountKeys.add(osiDto.Distributor);
                servicePointCodes.add(osiDto.ServicePointCode);
            }

            Map<String, Boolean> normalizedAddresses = normalizeAddress(addressDTOList);

            Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
            traderRtId = accountRts.get('Trader').getRecordTypeId();
            distributorRtId = accountRts.get('Distributor').getRecordTypeId();

            accountByKeyMap = getAccountByKeyMap(accountKeys);
            servicePointCodeToIdMap = getServicePointCodeToIdMap(servicePointCodes);

            osiToInsert = new List<OpportunityServiceItem__c>();
            for (OsiDTO osiDto : osiDtoList) {
                currentRow = osiDto.fidr;
                Boolean pointAddressNormalized = normalizedAddresses.get(osiDto.pointAddressDTO.addressKey);
                Boolean siteAddressNormalized = normalizedAddresses.get(osiDto.siteAddressDTO.addressKey);
                if (pointAddressNormalized != null && siteAddressNormalized != null && pointAddressNormalized && siteAddressNormalized) {
                    osiDto.PointAddressNormalized = pointAddressNormalized;
                    osiDto.SiteAddressNormalized = siteAddressNormalized;

                    osiDto.validateAccounts();

                    OpportunityServiceItem__c osiRecord = osiDto.getOsi(opportunityId, recordTypeId);
                    System.debug(osiRecord.Distributor__c);
                    if (applicantNotHolder != null && osiRecord.ApplicantNotHolder__c != applicantNotHolder) {
                        throw new WrtsException('Invalid Applicant Not Holder ' + applicantNotHolder + ' ' + osiRecord.ApplicantNotHolder__c);
                    }
                    if(osiRecord.ContractualPower__c < 11 && osiRecord.PowerPhase__c == 'Three'){
                        throw new WrtsException('Invalid Power Phase ' + osiRecord.PowerPhase__c + ' for selected Absorbed Power ' + osiRecord.ContractualPower__c);
                    }
                    if (commodity == MRO_UTL_Constants.ELECTRIC && isDiscoEnel != null && osiRecord.Distributor__r.IsDisCoENEL__c != isDiscoEnel) {
                        //throw new WrtsException('Invalid isDiscoEnel ' + commodity + ' ' + isDiscoEnel + ' Distributor IsDisCoENEL:' + osiRecord.Distributor__r.IsDisCoENEL__c);
                    }
                    osiToInsert.add(osiRecord);
                } else {
                    System.debug('############ address invalid');
                    System.debug(osiDto);
                }
            }

            if (osiToInsert.size() > 0) {

                validateOsi(osiToInsert);

                databaseSrv.upsertSObject(osiToInsert);
            }
        }catch (Exception e){
            databaseSrv.deleteSObject(osiToInsert);
            skipJob(e.getMessage(), currentRow);
        }
    }

    private void skipJob(String errorMessage, FileImportDataRow__c row){
        skipJob(errorMessage, row, null);
    }

    private void skipJob(String errorMessage){
        skipJob(errorMessage, null, null);
    }

    private void skipJob(String errorMessage, FileImportDataRow__c row, String jobId){
        FileImportDataRow__c fidr = new FileImportDataRow__c(
                ErrorMessage__c = errorMessage,
                Status__c = 'Rejected'
        );
        if(row != null){
            row.ErrorMessage__c = errorMessage;
            row.Status__c = 'Rejected';
            databaseSrv.upsertSObject(row);
            jobId = row.FileImportJob__c;
        }
        System.debug('######## Job skipped with status: ' + errorMessage);
        databaseSrv.upsertSObject(new FileImportJob__c(Id = jobId, SkipJob__c = true, Status__c = 'Rejected'));
        throw new WrtsException('Job rejected with status: ' + errorMessage);
    }

    private class OsiDTO {
        public String ServicePointCode { get; set; }
        public String Distributor { get; set; }
        public String Trader { get; set; }
        public String Pressure { get; set; }
        public String PressureLevel { get; set; }
        public String AvailablePower { get; set; }
        public String ContractualPower { get; set; }
        public String EstimatedConsumption { get; set; }
        public String VoltageLevel { get; set; }
        public String VoltageSetting { get; set; }
        public String PowerPhase { get; set; }
        public String FlowRate { get; set; }
        public String ConsumptionCategory { get; set; }
        public String EndDate { get; set; }
        public String IsNewConnection { get; set; }
        public String ApplicantNotHolder { get; set; }
        public String FlatRate { get; set; }
        public String NonDisconnectable { get; set; }
        public String NonDisconnectableReason { get; set; }
        public String NLC { get; set; }
        public String CLC { get; set; }
        public String SiteDescription { get; set; }
        public String PointCountry { get; set; }
        public String PointProvince { get; set; }
        public String PointPostalCode { get; set; }
        public String PointStreetType { get; set; }
        public String PointStreetName { get; set; }
        public String PointStreetNumber { get; set; }
        public String PointStreetNumberExtn { get; set; }
        public String PointLocality { get; set; }
        public String PointBuilding { get; set; }
        public String PointApartment { get; set; }
        public String PointFloor { get; set; }
        public String PointCity { get; set; }
        public String SiteCountry { get; set; }
        public String SiteProvince { get; set; }
        public String SitePostalCode { get; set; }
        public String SiteStreetType { get; set; }
        public String SiteStreetName { get; set; }
        public String SiteStreetNumber { get; set; }
        public String SiteStreetNumberExtn { get; set; }
        public String SiteLocality { get; set; }
        public String SiteBuilding { get; set; }
        public String SiteApartment { get; set; }
        public String SiteFloor { get; set; }
        public String SiteCity { get; set; }

        public Boolean PointAddressNormalized { get; set; }
        public Boolean SiteAddressNormalized { get; set; }
        public MRO_SRV_Address.AddressDTO pointAddressDTO  { get; set; }
        public MRO_SRV_Address.AddressDTO siteAddressDTO  { get; set; }
        public FileImportDataRow__c fidr  { get; set; }

        public OsiDTO(Map<String, String> jsonString) {
        }
        
        private Boolean parseBoolean(String var){
            return var != null ? Boolean.valueOf(var) : null;
        }
        private Double parseDouble(String var){
            return var != null ? Double.valueOf(var) : null;
        }

        public OpportunityServiceItem__c getOsi(String opportunityId, String recordTypeId) {
            OpportunityServiceItem__c osi = new OpportunityServiceItem__c(
                    RecordTypeId = recordTypeId,
                    ServicePoint__c = servicePointCodeToIdMap.get(this.ServicePointCode),
                    ServicePointCode__c = this.ServicePointCode,
                    PointAddressNormalized__c = this.PointAddressNormalized,
                    SiteAddressNormalized__c = this.SiteAddressNormalized,
                    Distributor__c = accountByKeyMap.get(this.Distributor).Id,
                    Trader__c = accountByKeyMap.get(this.Trader).Id,
                    Pressure__c = parseDouble(this.Pressure),
                    PressureLevel__c = this.PressureLevel,
                    AvailablePower__c = parseDouble(this.AvailablePower),
                    ContractualPower__c = parseDouble(this.ContractualPower),
                    EstimatedConsumption__c = parseDouble(this.EstimatedConsumption),
                    VoltageLevel__c = this.VoltageLevel,
                    VoltageSetting__c = this.VoltageSetting,
                    Voltage__c = this.VoltageSetting != null ? (Double.valueOf(this.VoltageSetting)/1000) : null,
                    PowerPhase__c = this.PowerPhase,
                    FlowRate__c = parseDouble(this.FlowRate),
                    ConsumptionCategory__c = this.ConsumptionCategory,
                    IsNewConnection__c = parseBoolean(this.IsNewConnection),
                    ApplicantNotHolder__c = parseBoolean(this.ApplicantNotHolder),
                    FlatRate__c = parseBoolean(this.FlatRate),
                    NonDisconnectable__c = parseBoolean(this.NonDisconnectable),
                    NonDisconnectableReason__c = this.NonDisconnectableReason,
                    Opportunity__c = opportunityId,
                    NLC__c = this.NLC,
                    CLC__c = this.CLC,
                    SiteDescription__c = this.SiteDescription,
                    PointCountry__c = this.PointCountry,
                    PointCity__c = this.PointCity,
                    PointProvince__c = this.PointProvince,
                    PointPostalCode__c = this.PointPostalCode,
                    PointStreetType__c = this.PointStreetType,
                    PointStreetName__c = this.PointStreetName,
                    PointStreetNumber__c = this.PointStreetNumber,
                    PointStreetNumberExtn__c = this.PointStreetNumberExtn,
                    PointLocality__c = this.PointLocality,
                    PointBuilding__c = this.PointBuilding,
                    PointApartment__c = this.PointApartment,
                    PointFloor__c = this.PointFloor,
                    SiteCountry__c = this.SiteCountry,
                    SiteCity__c = this.SiteCity,
                    SiteProvince__c = this.SiteProvince,
                    SitePostalCode__c = this.SitePostalCode,
                    SiteStreetType__c = this.SiteStreetType,
                    SiteStreetName__c = this.SiteStreetName,
                    SiteStreetNumber__c = this.SiteStreetNumber,
                    SiteStreetNumberExtn__c = this.SiteStreetNumberExtn,
                    SiteLocality__c = this.SiteLocality,
                    SiteBuilding__c = this.SiteBuilding,
                    SiteApartment__c = this.SiteApartment,
                    SiteFloor__c = this.SiteFloor
            );
            try {
                if (String.isNotEmpty(this.EndDate)) {
                    osi.EndDate__c = Date.parse(this.EndDate);
                }
            } catch (Exception e) {
                System.debug('########## Invalid date ' + this.EndDate);
            }

            return osi;
        }
        
        public MRO_SRV_Address.AddressDTO getPointAddressDto(Integer key){
            MRO_SRV_Address.AddressDTO pointAddressDTO = new MRO_SRV_Address.AddressDTO();
            pointAddressDTO.city = this.PointCity;
            pointAddressDTO.country = this.PointCountry;
            pointAddressDTO.province = this.PointProvince;
            pointAddressDTO.postalCode = this.PointPostalCode;
            pointAddressDTO.streetType = this.PointStreetType;
            pointAddressDTO.streetName = this.PointStreetName;
            pointAddressDTO.streetNumber = this.PointStreetNumber;
            pointAddressDTO.streetNumberExtn = this.PointStreetNumberExtn;
            pointAddressDTO.locality = this.PointLocality;
            pointAddressDTO.building = this.PointBuilding;
            pointAddressDTO.apartment = this.PointApartment;
            pointAddressDTO.floor = this.PointFloor;
            pointAddressDTO.addressKey = String.valueOf(key);
            this.pointAddressDTO = pointAddressDTO;
            return pointAddressDTO;
        }
        
        public MRO_SRV_Address.AddressDTO getSiteAddressDto(Integer key){
            MRO_SRV_Address.AddressDTO siteAddressDTO = new MRO_SRV_Address.AddressDTO();
            siteAddressDTO.city = this.SiteCity;
            siteAddressDTO.country = this.SiteCountry;
            siteAddressDTO.province = this.SiteProvince;
            siteAddressDTO.postalCode = this.SitePostalCode;
            siteAddressDTO.streetType = this.SiteStreetType;
            siteAddressDTO.streetName = this.SiteStreetName;
            siteAddressDTO.streetNumber = this.SiteStreetNumber;
            siteAddressDTO.streetNumberExtn = this.SiteStreetNumberExtn;
            siteAddressDTO.locality = this.SiteLocality;
            siteAddressDTO.building = this.SiteBuilding;
            siteAddressDTO.apartment = this.SiteApartment;
            siteAddressDTO.floor = this.SiteFloor;
            siteAddressDTO.addressKey = String.valueOf(key);
            this.siteAddressDTO = siteAddressDTO;
            return siteAddressDTO;
        }

        public Boolean validate(String commodity){
            if(commodity == MRO_UTL_Constants.ELECTRIC && this.NLC == null || commodity == MRO_UTL_Constants.GAS && this.CLC == null){
                throw new WrtsException('Invalid commodity ' + commodity + ' NLC: ' + this.NLC + ' CLC: ' + this.CLC);
            }
            return true;
        }

        public Boolean validateAccounts(){
            Account distributorAccount = accountByKeyMap.get(this.Distributor);
            Account traderAccount = accountByKeyMap.get(this.Trader);
            if(distributorAccount == null){
                throw new WrtsException('Distributor with key ' + this.Distributor + ' not found');
            }
            if(distributorAccount.RecordTypeId != distributorRtId){
                throw new WrtsException('Distributor with key ' + this.Distributor + ' - invalid record type ' + distributorAccount.RecordTypeId);
            }
            if(traderAccount == null){
                throw new WrtsException('Trader with key ' + this.Trader + ' not found');
            }
            if(traderAccount.RecordTypeId != traderRtId){
                throw new WrtsException('Trader with key ' + this.Trader + ' - invalid record type ' + traderAccount.RecordTypeId);
            }
            return true;
        }
    }

    private Map<String, Account> getAccountByKeyMap(Set<String> accountKeys) {
        List<String> recordTypes = new List<String>{traderRtId, distributorRtId};
        List<Account> accounts = [SELECT Id, Key__c, RecordTypeId FROM Account WHERE Key__c IN :accountKeys AND RecordTypeId IN :recordTypes];
        System.debug(recordTypes);
        System.debug(accountKeys);
        System.debug(accounts);

        Map<String, Account> accountByKeyMap = new Map<String, Account>();
        for (Account account : accounts) {
            accountByKeyMap.put(account.Key__c, account);
        }
        return accountByKeyMap;
    }

    private Map<String, String> getServicePointCodeToIdMap(Set<String> servicePointCodes) {
        List<ServicePoint__c> servicePoints = [SELECT Id, Code__c FROM ServicePoint__c WHERE Code__c IN :servicePointCodes];

        Map<String, String> servicePointCodeToIdMap = new Map<String, String>();
        for (ServicePoint__c servicePoint : servicePoints) {
            servicePointCodeToIdMap.put(servicePoint.Code__c, servicePoint.Id);
        }
        return servicePointCodeToIdMap;
    }

    private Map<String, Boolean> normalizeAddress(List<MRO_SRV_Address.AddressDTO> addressDTOList) {
        Set<String> streetNames = new Set<String>();
        Set<String> streetTypes = new Set<String>();
        for (MRO_SRV_Address.AddressDTO addressDTO : addressDTOList) {
            streetNames.add(addressDTO.streetName);
            streetTypes.add(addressDTO.streetType);
        }
        System.debug(streetNames);
        System.debug(streetTypes);
        Map<String, String> cityMap = getCityMap(addressDTOList);
        List<Street__c> streets = [
                SELECT Name,Verified__c, StreetType__c, Even_Odd__c, PostalCode__c, City__r.Country__c, City__r.Name, City__r.Province__c, FromStreetNumber__c, ToStreetNumber__c
                FROM Street__c
                WHERE Name IN :streetNames AND StreetType__c IN :streetTypes AND City__c IN :cityMap.values() AND Verified__c = TRUE
        ];
        
        Map<String, List<Street__c>> streetsMaps = new Map<String, List<Street__c>>();
        
        for(Street__c street :streets){
            List<Street__c> streetList = streetsMaps.get(getStreetKey(street));
            if(streetList == null){
                streetList = new List<Street__c>();
            }
            streetList.add(street);
            streetsMaps.put(getStreetKey(street), streetList);
        }
        Map<String, Boolean> normalizedAddresses = new Map<String, Boolean>();
        for (MRO_SRV_Address.AddressDTO addressDTO : addressDTOList) {
            String cityId = cityMap.get(addressDTO.city + '_' + addressDTO.province + '_' + addressDTO.country);
            List<Street__c> streetsList = (List<Street__c>)streetsMaps.get(addressDTO.streetName + '_' + addressDTO.streetType + '_' + cityId);
            for(Street__c street : streetsList) {
                if (street.FromStreetNumber__c <= Decimal.valueOf(addressDTO.streetNumber) && street.ToStreetNumber__c >= Decimal.valueOf(addressDTO.streetNumber)) {
                    addressDTO.addressNormalized = true;
                    addressDTO.streetId = street.Id;
                    normalizedAddresses.put(addressDTO.addressKey, addressDTO.addressNormalized);
                }
            }
        }
        return normalizedAddresses;
    }

    private Map<String, String> getCityMap(List<MRO_SRV_Address.AddressDTO> addressDTOList) {
        Set<String> cityNames = new Set<String>();
        Set<String> cityCountries = new Set<String>();
        Set<String> cityProvinces = new Set<String>();
        for (MRO_SRV_Address.AddressDTO addressDTO : addressDTOList) {
            cityNames.add(addressDTO.city);
            cityCountries.add(addressDTO.country);
            cityProvinces.add(addressDTO.province);
        }
        System.debug(cityProvinces);
        System.debug(cityCountries);
        System.debug(cityNames);
        Map<String, String> cityMap = new Map<String, String>();
        List<City__c> cities = [SELECT Id, Name, Country__c, Province__c FROM City__c WHERE Name IN :cityNames AND Province__c IN :cityProvinces AND Country__c IN :cityCountries];
        for (City__c city : cities) {
            cityMap.put(getCityKey(city), city.Id);
        }
        return cityMap;
    }

    private String getCityKey(City__c city) {
        return city.Name + '_' + city.Province__c + '_' + city.Country__c;
    }

    private String getStreetKey(Street__c street) {
        return street.Name + '_' + street.StreetType__c + '_' + street.City__c;
    }

    private Boolean validateOsi(List<OpportunityServiceItem__c> osiList){
        Savepoint spOsi = Database.setSavepoint();
        Savepoint sp;
        try {
            databaseSrv.insertSObject(osiList);

            sp = Database.setSavepoint();
            OpportunityServiceItem__c osi = MRO_QR_OpportunityServiceItem.getInstance().getById(osiList[0].Id);
            Opportunity opp = osi.Opportunity__r;
            List<ServicePoint__c> spList = new List<ServicePoint__c>();
            List<Supply__c> supplyList = new List<Supply__c>();
            List<Case> caseList = new List<Case>();

            Set<String> spCodes = new Set<String>();

            for(OpportunityServiceItem__c osiItem : osiList){
                spCodes.add(osiItem.ServicePointCode__c);
            }

            Map<String, ServicePoint__c> servicePointMap = new Map<String, ServicePoint__c>();
            if(spCodes.size() > 0) {
                List<ServicePoint__c> existingServicePoints = [SELECT Id, Code__c, Voltage__c FROM ServicePoint__c WHERE Code__c IN :spCodes];
                for(ServicePoint__c servicePoint : existingServicePoints){
                    servicePointMap.put(servicePoint.Code__c, servicePoint);
                }
            }

            for(OpportunityServiceItem__c osiItem : osiList){
                ServicePoint__c servicePoint = servicePointMap.get(osiItem.ServicePointCode__c);
                System.debug('##############3 Service Point');
                System.debug(servicePoint);
                if(servicePoint != null) {
                    osi.ServicePoint__c = servicePoint.Id;
                    if(osi.Voltage__c == null){
                        osiItem.Voltage__c = servicePoint.Voltage__c;
                    }
                }
                System.debug(JSON.serialize(spList));
                spList.add(MRO_SRV_Opportunity.instantiateServicePoint(osiItem, osi.Opportunity__r.CompanyDivision__r.Trader__c));
            }
            System.debug(JSON.serialize(spList));
            databaseSrv.upsertSObject(spList);

            for(ServicePoint__c spItem : spList){
                Supply__c supply = MRO_SRV_Opportunity.instantiateSupply(opp, osi);
                supply.ServicePoint__c = spItem.Id;
                supplyList.add(supply);
            }
            databaseSrv.insertSObject(supplyList);

            for(Supply__c supply : supplyList){
                Case caseItem = MRO_SRV_Opportunity.instantiateCase(opp, osi, 'SwitchIn');
                caseItem.Supply__c = supply.Id;
                caseList.add(caseItem);
            }
            databaseSrv.insertSObject(caseList);

        } catch (Exception ex) {
            Database.rollback(spOsi);
            System.debug(ex.getMessage());
            throw new WrtsException('Invalid Service Point ' + ex.getMessage());
        }

        Database.rollback(sp);
        return true;
    }
}