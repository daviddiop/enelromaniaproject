@IsTest
public class MRO_SRV_ContractTst {

    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Sequence__c = 1, SequenceLength__c = 1, Type__c = 'CustomerCode');
        insert sequencer;

        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.BusinessType__c = 'NGO';
        insert businessAccount;

        Contract testContract = MRO_UTL_TestDataFactory.contract().createContract().build();
        testContract.AccountId = businessAccount.Id;
        insert testContract;

        Dossier__c testDossier = MRO_UTL_TestDataFactory.Dossier().setAccount(businessAccount.Id).build();
        insert testDossier;

        Case testCase = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().build();
        testCase.Dossier__c = testDossier.Id;
        testCase.Contract__c = testContract.Id;
        insert testCase;
    }

    @IsTest
    static void assignRelatedCaseAndDossierToEnelXTest() {
        List<Contract> contractList = [SELECT Id, Status FROM Contract];
        System.assertEquals(1, contractList.size());

        Contract testContract = contractList.get(0);
        testContract.Status = MRO_UTL_Constants.getAllConstants().CONTRACT_STATUS_SIGNED;
        update testContract;

        List<Case> caseList = [SELECT Id, OwnerId FROM Case];
        System.assertEquals(1, caseList.size());

        List<Dossier__c> dossierListList = [SELECT Id, OwnerId FROM Dossier__c];
        System.assertEquals(1, dossierListList.size());

        Group enelXQueue = MRO_QR_User.getInstance().getQueueByDeveloperName(MRO_UTL_Constants.ENELX_QUEUE);
        if (enelXQueue != null) {
            System.assertEquals(enelXQueue.Id, caseList.get(0).OwnerId);
            System.assertEquals(enelXQueue.Id, dossierListList.get(0).OwnerId);
        }
    }
}