@isTest
public class MRO_QR_AddressTst {
    @testsetup static void setup(){
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account ac=new Account(Name='account');
        insert ac;
        City__c city = new City__c(Name = 'JIMBOLIA', Province__c = 'TIMIS', Country__c = 'ROMANIA');
        insert city;
        Street__c[] streets = new Street__c[]{
                new Street__c(Name = 'ANTOL', City__c = city.Id, StreetType__c = 'PIATA', PostalCode__c = '123',
                        ToStreetNumber__c = 100, FromStreetNumber__c = 1,Verified__c=false)

        };
        insert streets;
    }
    @isTest static void findStreet(){
        Street__c street=[select Id from Street__c][0];
        Test.startTest();
        MRO_QR_Address.findStreet(street.Id);
        MRO_QR_Address.findStreet('street');
        Test.stopTest();
    }
    @isTest static void fieldExist(){
        Street__c street=[select Id from Street__c][0];
        Test.startTest();
        MRO_QR_Address.fieldExist('country', 'ROMANIA', 'city');
        MRO_QR_Address.fieldExist('province', 'JIMBOLIA', 'city');
        MRO_QR_Address.fieldExist('city', 'TIMIS', 'city');
        MRO_QR_Address.fieldExist('postalCode', '123', 'street');
        MRO_QR_Address.fieldExist('streetType', 'PIATA', 'street');
        MRO_QR_Address.fieldExist('streetName', 'ANTOL', 'street');
      
            
        Test.stopTest();
        
    }
   @isTest static void getCity(){
         Test.startTest();
         MRO_QR_Address.getCity('ROMANIA','JIMBOLIA','TIMIS');
     	 MRO_QR_Address.getCitiesStartingWith('JI');
       	 MRO_QR_Address.getCountriesStartingWith('RO');
         MRO_QR_Address.getCountryNameByCityName('JIMBOLIA');
         MRO_QR_Address.getProvinceByCity('JIMBOLIA');
       MRO_QR_Address.getProvincesStartingWith('TI');
       
        Test.stopTest();
    } 
     @isTest static void street(){
          City__c city=[select Id from City__c][0];
         Test.startTest();
         MRO_QR_Address.getStreets(city, 'ANTOL', 'PIATA', '123', '');
     	 MRO_QR_Address.getStreetsStartingWith('AN');
         MRO_QR_Address.getStreetsCityNameStartingWith('RO');
      	 MRO_QR_Address.getPostalCodesStartingWith('12');    
        Test.stopTest();
    } 
    @isTest static void findExactStreetQuery1(){
         Map<String, String> addressField=new Map<String, String>(); 
        addressField.put('streetId','1az');
       	addressField.put('streetNumber','100');
       	addressField.put('streetNumberExtn','100');
       	addressField.put('streetName','ANTOL');
       	addressField.put('streetType','PIATA'); 
       	addressField.put('apartment','appart');
    	addressField.put('building','address'); 
        addressField.put('city','JIMBOLIA'); 
      	addressField.put('country','ROMANIA'); 
        addressField.put('floor','a'); 
        addressField.put('locality','milan'); 
        addressField.put('postalCode','123');
        addressField.put('province','TIMIS');
        addressField.put('addressNormalized','true');
	    addressField.put('addressKey','11');    
         Test.startTest();
       MRO_SRV_Address.AddressDTO addressDTO1=new MRO_SRV_Address.AddressDTO(addressField);
        MRO_QR_Address.findExactStreetQuery1(addressDTO1);
        MRO_QR_Address.findExactStreetQuery2(addressDTO1);
        MRO_QR_Address.findExactStreet1(addressDTO1);
        
        Test.stopTest();
    } 
    
    @isTest static void findStreets(){
        LIST<String> countries= new List<String>();
        countries.add('ROMANIA');
        LIST<String> provinces= new List<String>();
        provinces.add('TIMIS');
        LIST<String> cities = new List<String>();
        cities.add('JIMBOLIA');
        LIST<String> streetTypes= new List<String>();
        streetTypes.add('PIATA');
        LIST<String> streetNames= new List<String>();
        streetNames.add('ANTOL');
        LIST<String> postalCodes= new List<String>();
        postalCodes.add('123');
        
        
        Test.startTest();
        MRO_QR_Address.findStreets(countries, provinces, cities, streetTypes, streetNames, postalCodes,100 );
        Test.stopTest();
    }
    
    
    

}