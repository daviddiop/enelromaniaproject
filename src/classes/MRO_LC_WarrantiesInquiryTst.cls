/**
 * Created by Vlad Mocanu on 13/02/2020.
 */
@IsTest
public with sharing class MRO_LC_WarrantiesInquiryTst {

    @IsTest
    static void testListWarranty() {
        MRO_LC_WarrantiesInquiry.WarrantyRequestParam params = new MRO_LC_WarrantiesInquiry.WarrantyRequestParam(
                'contractNumber', 'contractId','ID_INTEGR_CTR', 'NR_EXT_PA', '');

        Test.startTest();

        List<MRO_LC_WarrantiesInquiry.Warranty> warrantyList = (List<MRO_LC_WarrantiesInquiry.Warranty>)
                TestUtils.exec('MRO_LC_WarrantiesInquiry', 'listWarranties', params, true);

        Test.stopTest();

        System.assertNotEquals(null, warrantyList);
        System.assertNotEquals(0, warrantyList.size());
        System.assertEquals(5, warrantyList.size());
    }

    @IsTest
    static void getContractNumberTest(){
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account account = MRO_UTL_TestDataFactory.account().businessAccount().build();
        account.Name = 'BusinessAccount1';
        insert account;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = account.Id;
        insert contract;

        Contract c = [ SELECT Id FROM Contract LIMIT 1];
        Map<String, String> jsonInput = new Map<String, String>{
            'contractId' => c.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_WarrantiesInquiry', 'getContractNumber', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true , result.get('error') == false);

        jsonInput = new Map<String, String>{
            'contractId' => account.Id
        };

        response = TestUtils.exec('MRO_LC_WarrantiesInquiry', 'getContractNumber', jsonInput, true);
       result = (Map<String, Object>) response;
        System.assertEquals(true , result.get('error') == true);
        Test.stopTest();
    }
}