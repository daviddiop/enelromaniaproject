/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 18, 2020
 * @desc
 * @history
 */

public with sharing class MRO_SRV_SAPMultiPointCallout implements Queueable, Database.AllowsCallouts {
    private Id caseGroupId;

    public MRO_SRV_SAPMultiPointCallout(Id caseGroupId) {
        this.caseGroupId = caseGroupId;
    }

    public void execute(QueueableContext qContext) {

        List<ExecutionLog__c> calloutItems = [SELECT Id, Message__c, RequestType__c, ExternalSystem__c, Endpoint__c, Case__c, Case__r.CaseGroup__r.IntegrationKey__c, (SELECT Id FROM Attachments ORDER BY CreatedDate DESC LIMIT 1) FROM ExecutionLog__c WHERE CaseGroup__c = :caseGroupId AND Type__c = 'GroupCallOutItem'];
        Map<Id, Attachment> attMap = new Map<Id, Attachment>([SELECT Id, Body FROM Attachment WHERE ParentId IN :calloutItems ]);

        String nomeFlusso = calloutItems[0].RequestType__c;
        String nomeEndPoint = calloutItems[0].ExternalSystem__c.toLowerCase();

        Map<String, Object> argsMap = new Map<String, Object>();

        Map<String, String> parameters = new Map<String, String>();
        parameters.put('requestType', nomeFlusso);
        parameters.put('timeout', '60000');
        parameters.put('endPoint', nomeEndPoint);//

        argsMap.put('parameters', parameters);

        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest;
        wrts_prcgvr.MRR_1_0.Request request;
        List<wrts_prcgvr.MRR_1_0.WObject> supplyOldList, supplyNewList, supplyList; //First Request
        String message;
        for(Integer i = 0; i < calloutItems.size(); i++){
            ExecutionLog__c log = calloutItems[i];
            if(log.Attachments != null && !log.Attachments.isEmpty()){
                message = attMap.get(log.Attachments[0].Id).body.tostring();
            } else if(log.Message__c != null){
                message = log.Message__c;
            }

            wrts_prcgvr.MRR_1_0.MultiRequest multiRequestSingleTemp = (wrts_prcgvr.MRR_1_0.MultiRequest) JSON.deserializeStrict(message, wrts_prcgvr.MRR_1_0.MultiRequest.class);
            wrts_prcgvr.MRR_1_0.MultiRequest multiRequestSingle = MRO_UTL_MRRMapper.rebuildMultiRequest(multiRequestSingleTemp);

            if(i == 0){
                System.debug('multiRequest*** ' + multiRequest);
                System.debug(' multiRequestSingle.requests*** ' +  multiRequestSingle.requests.size());
                multiRequest = multiRequestSingle;
                request = multiRequestSingle.requests[0];

                List<wrts_prcgvr.MRR_1_0.WObject> caseList = MRO_UTL_MRRMapper.selectWobjects('Case', request);
                for(wrts_prcgvr.MRR_1_0.Field field : caseList[0].fields){
                    if(field.name == 'IntegrationKey__c') field.value = log.Case__r.CaseGroup__r.IntegrationKey__c;
                }

                request.header.requestId = MRO_UTL_Guid.NewGuid();
                request.header.requestTimestamp = String.valueOf(System.now());

                supplyOldList = MRO_UTL_MRRMapper.selectWobjects('Case/SupplyOld__r', request);
                supplyNewList = MRO_UTL_MRRMapper.selectWobjects('Case/SupplyNew__r', request);
                supplyList = MRO_UTL_MRRMapper.selectWobjects('Case/Supply__r', request);
            } else{
                wrts_prcgvr.MRR_1_0.Request otherRequest = multiRequestSingle.requests[0];
                //Other Requests
                List<wrts_prcgvr.MRR_1_0.WObject> servicePointOldList = MRO_UTL_MRRMapper.selectWobjects('Case/SupplyOld__r/ServicePointOld__r', otherRequest);
                if(servicePointOldList != null && !servicePointOldList.isEmpty() && supplyOldList != null && !supplyOldList.isEmpty()) supplyOldList[0].objects.addAll(servicePointOldList);

                List<wrts_prcgvr.MRR_1_0.WObject> servicePointNewList = MRO_UTL_MRRMapper.selectWobjects('Case/SupplyNew__r/ServicePointNew__r', otherRequest);
                if(servicePointNewList != null && !servicePointNewList.isEmpty() && supplyNewList != null && !supplyNewList.isEmpty()) supplyNewList[0].objects.addAll(servicePointNewList);

                List<wrts_prcgvr.MRR_1_0.WObject> servicePointList = MRO_UTL_MRRMapper.selectWobjects('Case/Supply__r/ServicePoint__r', otherRequest);
                if(servicePointList != null && !servicePointList.isEmpty() && supplyList != null && !supplyList.isEmpty())  supplyList[0].objects.addAll(servicePointList);
            }
        }

        wrts_prcgvr__IntegrationSetting__c CS = wrts_prcgvr__IntegrationSetting__c.getInstance();
        String contentType = String.valueOf(CS.get('wrts_prcgvr__DefaultContentType__c'));
        Integer timeOut = 60000; //Default value
        List<IntegrationSetting__mdt> ac = [SELECT ActionValue__c, TimeOut__c From IntegrationSetting__mdt WHERE DeveloperName = :nomeEndPoint];
        if(!ac.isEmpty()){
            contentType += ac[0].ActionValue__c;
            timeOut = Integer.valueOf(ac[0].TimeOut__c);
        }

        argsMap.put('endpoint', 'callout:' + nomeEndPoint);
        argsMap.put('timeout', timeOut);
        argsMap.put('payload', multiRequest);
        argsMap.put('contentType', contentType);

        system.debug('Execute Callout');
        System.debug('multiRequest: ' + multiRequest);

        wrts_prcgvr.Interfaces_1_0.ICalloutClient client = (wrts_prcgvr.Interfaces_1_0.ICalloutClient) wrts_prcgvr.VersionManager.newClassInstance ('CalloutClient');
        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = (wrts_prcgvr.MRR_1_0.MultiResponse) client.send(argsMap);

        String code, responseString;
        if(calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
            code = calloutResponse.responses[0].code + ' ' + calloutResponse.responses[0].description;
            responseString = MRO_UTL_MRRMapper.serializeMultiResponse(calloutResponse);
        }
        String messageString = MRO_UTL_MRRMapper.serializeMultirequest(multiRequest);

        MRO_SRV_ExecutionLog.createIntegrationLog('GroupCallOut', multiRequest.requests[0].header.requestId, multiRequest.requests[0].header.requestType, caseGroupId, messageString, code, responseString, null, null, null, nomeEndPoint, null);
    }
}