/**
 * Created by goudiaby on 24/07/2019.
 */

public with sharing class CompanyDivisionService {
    private static CompanyDivisionQueries companyDivisionQuery = CompanyDivisionQueries.getInstance();
    private static UserQueries userQuery = UserQueries.getInstance();

    public static CompanyDivisionService getInstance() {
        return new CompanyDivisionService();
    }

    public CompanyDivision__c getCompanyDivisionInfosFromUser(String userId){
        CompanyDivision__c companyDivision = new CompanyDivision__c();
        User currentUser = userQuery.getCompanyDivisionId(userId);
        if (String.isNotBlank(currentUser.CompanyDivisionId__c)) {
            companyDivision = companyDivisionQuery.getById(currentUser.CompanyDivisionId__c);
        }
        return companyDivision;
    }

    public List<ApexServiceLibraryCnt.Option> getListOptionsCompanyDivision(){
        List<ApexServiceLibraryCnt.Option> companyDivisionOptions = new List<ApexServiceLibraryCnt.Option>();
        List<CompanyDivision__c> allCompanyDivisions = companyDivisionQuery.getAll();
        for (CompanyDivision__c company : allCompanyDivisions) {
            companyDivisionOptions.add(new ApexServiceLibraryCnt.Option(company.Name, company.Id));
        }
        return companyDivisionOptions;
    }

}