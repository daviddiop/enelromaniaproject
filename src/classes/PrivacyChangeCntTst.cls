/**
 * Created by Moussa Fofana on 07/08/2019.
 */

@isTest
public with sharing class PrivacyChangeCntTst {

    @testSetup
    static void setup() {
        /*ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;

        OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        insert opportunityServiceItem;*/

        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.BusinessType__c = 'NGO';
        Account personAccount = TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        List<Account> listAccount = new list<Account>();
        listAccount.add(businessAccount);
        listAccount.add(personAccount);
        insert listAccount;

        Individual individual = TestDataFactory.individual().createIndividual().build();
        individual.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert individual;

        Contact contact = TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[0].Id;
        contact.IndividualId = individual.Id;
        contact.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert contact;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().setInterlocutor(individual.Id).build();
        interaction.Status__c = 'New';
        interaction.Channel__c = 'MyEnel';
        insert interaction;

        CustomerInteraction__c bsnCustomerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id,listAccount[0].Id, contact.Id).build();
        insert bsnCustomerInteraction;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        insert opportunity;

        Dossier__c dossier = TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        PrivacyChange__c privacyChange = TestDataFactory.privacyChange().createPrivacyChangeBuilder().setDossier(dossier.Id).build();
        privacyChange.Status__c = 'Active';
        privacyChange.Individual__c = individual.Id;
        insert privacyChange;
    }

    @isTest
    static void getIndividualTest() {
        PrivacyChangeCnt.getInstance();
        Opportunity opportunity = [
            SELECT Id, Name, AccountId
            FROM  Opportunity
            LIMIT 1
        ];
        PrivacyChange__c privacyChange = [
                SELECT Id, Opportunity__c
                FROM  PrivacyChange__c
                LIMIT 1
        ];
        privacyChange.Opportunity__c = opportunity.Id;
        update  privacyChange;
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id
        };

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('PrivacyChangeCnt', 'getIndividual',
                inputJSON, true);
        system.assertEquals(true, result.get('privacyChange') != null);
        system.assertEquals(true, result.get('individualId') != null);


        privacyChange.Opportunity__c = null;
        update  privacyChange;
        Contact contact = [
            SELECT Id
            FROM  Contact
            LIMIT 1
        ];
        Account accountKey = TestDataFactory.account().setKey('1324433').build();
        /*Account accBusines = TestDataFactory.account().businessAccount().build();
        accBusines.Key__c = accountKey.Key__c;
        insert  accBusines;
        AccountContactRelation accountContactRelation = TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id, accBusines.Id).build();
        accountContactRelation.LegalRepresentative__c = true;
        insert accountContactRelation;*/
        //opportunity.AccountId = accBusines.Id;
        //update  opportunity;
        inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id
        };
        result = (Map<String, Object>) TestUtils.exec('PrivacyChangeCnt', 'getIndividual',
                inputJSON, true);
        //system.assertEquals(true, result.get('privacyChange') != null);
        //system.assertEquals(true, result.get('individualId') != null);


        Account acc = TestDataFactory.account().personAccount().build();
        acc.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        acc.Key__c = '13244346446';
        insert acc;
        opportunity.AccountId = acc.Id;
        update  opportunity;
        inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id
        };
        result = (Map<String, Object>) TestUtils.exec('PrivacyChangeCnt', 'getIndividual',
                inputJSON, true);
        //system.assertEquals(true, result.get('privacyChange') != null);

        Test.stopTest();

    }

    @isTest
    static void customerInteractionNotEmptyTest() {
        Opportunity opportunity = [
                SELECT Id, Name, AccountId
                FROM  Opportunity
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        opportunity.CustomerInteraction__c = customerInteraction.Id;
        update opportunity;
        PrivacyChange__c privacyChange = [
                SELECT Id, Opportunity__c
                FROM  PrivacyChange__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('PrivacyChangeCnt', 'getIndividual',
                inputJSON, true);
        Test.stopTest();

    }

    @isTest
    static void getIndividualExceptionTest() {
        Opportunity opportunity = [
                SELECT Id, Name, AccountId
                FROM  Opportunity
                LIMIT 1
        ];
        PrivacyChange__c privacyChange = [
                SELECT Id, Opportunity__c
                FROM  PrivacyChange__c
                LIMIT 1
        ];
        privacyChange.Opportunity__c = opportunity.Id;
        update  privacyChange;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id
        };
        delete opportunity;
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('PrivacyChangeCnt', 'getIndividual',
                inputJSON, true);
        system.assertEquals(true, result.get('error') == true);
        Test.stopTest();

    }

    @isTest
    static void getIndividualPrivacyRecapTest() {
        Account account = [
            SELECT Id,IsPersonAccount
            FROM Account
            WHERE IsPersonAccount =: true
            LIMIT 1
        ];
            Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id
            };
            Test.startTest();
            Map<String, Object> result = (Map<String, Object>) TestUtils.exec('PrivacyChangeCnt', 'getIndividualPrivacyRecap',
                inputJSON, true);
            Test.stopTest();
        }
    @isTest
    static void getIndividualPrivacyRecapBusinessAccTest() {
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Contact Contact = [
            SELECT Id
            FROM Contact
            LIMIT 1
        ];
        AccountContactRelation accountContactRelation = TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id, account.Id).build();
        accountContactRelation.LegalRepresentative__c = true;
        insert accountContactRelation;
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id
        };
        Test.startTest();
        try {
            TestUtils.exec('PrivacyChangeCnt', 'getIndividualPrivacyRecap',inputJSON, true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void getIndividualPrivacyRecapBusinessAccKOTest() {
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id
        };
        Test.startTest();
        try {
            TestUtils.exec('PrivacyChangeCnt', 'getIndividualPrivacyRecap',inputJSON, true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    static void getIndividualPrivacyRecapExcepTest() {
        Account account = [
            SELECT Id,IsPersonAccount
            FROM Account
            WHERE IsPersonAccount =: true
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('PrivacyChangeCnt', 'getIndividualPrivacyRecap',
            inputJSON, true);
        Map<String, Object> result=(Map<String, Object>)response;
        System.assertEquals(true , result.get('error') != false);
        Test.stopTest();
    }
}