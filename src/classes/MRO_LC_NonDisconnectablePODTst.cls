/**
 * Created by David on 31.10.2019.
 */
@IsTest
public with sharing class MRO_LC_NonDisconnectablePODTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c= 'NonDisconnectablePODTemplate_1';
        insert scriptTemplate;

    }
    @IsTest
    public static void InitializeTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'interactionId' => customerInteraction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_NonDisconnectablePOD', 'Initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => '',
                'interactionId' => customerInteraction.Id
        };
        response = TestUtils.exec(
                'MRO_LC_NonDisconnectablePOD', 'Initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }
    @IsTest
    static void InitializeExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'MRO_LC_NonDisconnectablePOD', 'Initialize', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == true );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @IsTest
    private static void CancelProcessTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                        AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'oldCaseList' => caseListString,
                'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_NonDisconnectablePOD', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void updateCaseListTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                        AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'oldCaseList' => caseListString,
                'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_NonDisconnectablePOD', 'UpdateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    private static void getCaseTest() {
        Case caseObject = [
                SELECT Id
                FROM Case
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'recordId' => caseObject.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_NonDisconnectablePOD', 'getCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    private static void createCaseTest() {
        List<Supply__c> supply = [
                SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c
                FROM Supply__c
                WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Trader__c,AccountId, RecordTypeId
                FROM Case
                LIMIT 2
        ];
        List<Account> myAccount = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Boolean nonDisconnectable = true ;
        String reason = 'Hospital' ;

        Test.startTest();
        MRO_LC_NonDisconnectablePOD.CreateCaseInput inputData =  new MRO_LC_NonDisconnectablePOD.CreateCaseInput();
        inputData.nonDisconnectable = nonDisconnectable;
        inputData.reason = reason ;
        inputData.accountId = myAccount[0].Id;
        inputData.dossierId = dossier.Id ;
        inputData.searchedSupplyFieldsList = supply;
        inputData.caseTile = caseList ;
        Object response = TestUtils.exec('MRO_LC_NonDisconnectablePOD', 'createOldCase', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    private static void createCaseExcepTest() {
        List<Supply__c> supply = [
                SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c
                FROM Supply__c
                WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Trader__c,AccountId, RecordTypeId
                FROM Case
                LIMIT 2
        ];
        List<Account> myAccount = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Boolean nonDisconnectable = true ;
        String reason = 'Fraud';

        Test.startTest();
        MRO_LC_NonDisconnectablePOD.CreateCaseInput inputData =  new MRO_LC_NonDisconnectablePOD.CreateCaseInput();
        inputData.nonDisconnectable = nonDisconnectable;
        inputData.reason = reason ;
        inputData.accountId = myAccount[0].Id;
        inputData.dossierId = dossier.Id ;
        //inputData.searchedSupplyFieldsList = ;
        inputData.caseTile = caseList ;
        Object response = TestUtils.exec('MRO_LC_NonDisconnectablePOD', 'createOldCase', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }
}