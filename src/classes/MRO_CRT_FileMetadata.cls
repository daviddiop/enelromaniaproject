/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   lug 10, 2020
 * @desc    
 * @history 
 */

global with sharing class MRO_CRT_FileMetadata implements wrts_prcgvr.Interfaces_1_2.IApexCriteria {
    global Boolean evaluate(Object param) {
        Map<String, Object> paramsMap = (Map<String, Object>) param;
        FileMetadata__c fm = (FileMetadata__c) paramsMap.get('record');
        String method = (String) paramsMap.get('method');

        switch on method {
            when 'hasFileCheckActivity' {
                return this.hasFileCheckActivity(fm);
            }
            when else {
                throw new WrtsException('Unrecognized method: ' + method);
            }
        }
    }

    private Boolean hasFileCheckActivity(FileMetadata__c fm) {
        wrts_prcgvr__Activity__c checkActivity = MRO_QR_Activity.getInstance().getByFileMetadataAndType(fm.Id, MRO_UTL_Constants.getAllConstants().ACTIVITY_FILE_CHECK_TYPE);
        return (checkActivity != null);
    }
}