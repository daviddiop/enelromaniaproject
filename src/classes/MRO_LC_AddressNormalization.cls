/**
 * Created by tommasobolis on 22/10/2020.
 */

public with sharing class MRO_LC_AddressNormalization {

    private static MRO_QR_Activity activityQry = MRO_QR_Activity.getInstance();

    @AuraEnabled
    public static Map<String, Object> initialize(String recordId) {

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            wrts_prcgvr__Activity__c addressVerificationActivity = activityQry.getById(recordId);
            response.put('activity', addressVerificationActivity);
            if(addressVerificationActivity != null) {
                addressVerificationActivity.SendingCountry__c = 'ROMANIA';
                MRO_SRV_Address.AddressDTO addressFromBilling =
                        new MRO_SRV_Address.AddressDTO(addressVerificationActivity, 'Sending');
                response.put('forcedAddress', addressFromBilling);
            }
        } catch(Exception e) {

            response.put('error', true);
            response.put('errorMessage', e.getMessage());
            response.put('errorStackTrace', e.getStackTraceString());
            System.debug('MRO_LC_AddressNormalization.initialize:  ' + e.getMessage() + e.getStackTraceString());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> updateAddress(String recordId, MRO_SRV_Address.AddressDTO address) {

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            wrts_prcgvr__Activity__c addressVerificationActivity = activityQry.getById(recordId);
            address.populateRecordAddressFields(addressVerificationActivity, 'Sending');
            update addressVerificationActivity;
        } catch(Exception e) {

            response.put('error', true);
            response.put('errorMessage', e.getMessage());
            response.put('errorStackTrace', e.getStackTraceString());
        }
        return response;
    }
}