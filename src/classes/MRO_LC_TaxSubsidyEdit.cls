/**
 * Created by David Diop on 07.11.2019.
 */

public with sharing class MRO_LC_TaxSubsidyEdit extends ApexServiceLibraryCnt {
    private static final MRO_QR_Case caseQuerySrv = MRO_QR_Case.getInstance();
    private static final MRO_QR_Supply supplyQuerySrv = MRO_QR_Supply.getInstance();
    private static final MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static final MRO_QR_TaxSubsidy taxSubsidyQuerySrv = MRO_QR_TaxSubsidy.getInstance();
    private static final MRO_SRV_TaxSubsidy taxSubsidySrv = MRO_SRV_TaxSubsidy.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    static String caseTaxChangeRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Tax_Change').getRecordTypeId();
    static String supplyRecordTypeEle = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId();
    static String institutionRecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Institution').getRecordTypeId();
    static Constants constantSrv = Constants.getAllConstants();
    private static String activatingStatus = constantSrv.SUPPLY_STATUS_ACTIVATING;
    private static String activeStatus = constantSrv.SUPPLY_STATUS_ACTIVE;

    public static MRO_LC_TaxSubsidyEdit getInstance() {
        return (MRO_LC_TaxSubsidyEdit) ServiceLocator.getInstance(MRO_LC_TaxSubsidyEdit.class);
    }
    /**
    * @author  David Diop
    * @description getSubventionType get the record type for TaxSubsidyEdit .
    * @date 07/11/2019
    * @param dossierId
    * @param caseId
    * @return list record type TaxSubsidy and supplyRecordType
    */

    public with sharing class getSubventionType extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, Schema.RecordTypeInfo> taxSubsidy = Schema.SObjectType.TaxSubsidy__c.getRecordTypeInfosByDeveloperName();
            for (String record : taxSubsidy.keySet()) {
                Schema.RecordTypeInfo RecordInfo = taxSubsidy.get(record);
                response.put(record, new Map<String, String>{
                        'label' => RecordInfo.getName(),
                        'value' => RecordInfo.getRecordTypeId()
                });
                response.put('supplyElectricRecord', supplyRecordTypeEle);
                response.put('activatingStatus', activatingStatus);
                response.put('activeStatus', activeStatus);
                List <Account> institutions = accountQuery.listInstitutionsByRecord(institutionRecordType);
                /*[
                        SELECT  Id,Name,ResidentialProvince__c,RecordTypeId,ResidentialLocality__c
                        FROM Account
                        WHERE RecordTypeId =: institutionRecordType
                ];*/

                /*for(Account institution : insts){
                    System.debug('institution'+institution.ResidentialProvince__c);
                }*/
                response.put('institutions',institutions);
            }
            return response;
        }
    }
   
    /**
    * @author  David Diop
    * @description getSubventionRecord  and supply related.
    * @date 07/11/2019
    * @param taxSubsidyId
    * @return subventionRecord,supply
    */
    public with sharing class getSubventionRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('recordId');
            TaxSubsidy__c subventionRecord = taxSubsidyQuerySrv.getTaxSubsidyById(recordId);
            List<Supply__c> supplyRecord = supplyQuerySrv.getSuppliesById(subventionRecord.Supply__c);
            List <Account> institutions = accountQuery.getInstitutionsById(institutionRecordType,subventionRecord.Institution__c);
                    /*[
                    SELECT  Id,Name,ResidentialProvince__c,RecordTypeId,ResidentialLocality__c
                    FROM Account
                    WHERE RecordTypeId =: institutionRecordType AND Id =:subventionRecord.Institution__c
            ];*/
            response.put('subventionRecord', subventionRecord);
            response.put('supply', supplyRecord[0]);
            response.put('institution', institutions);

            return response;
        }
    }
    /**
    * @author  David Diop
    * @description deleteTaxSubsidy
    * @date 07/11/2019
    * @param taxSubsidyId
    * @return
    */
    public with sharing class deleteTaxSubsidy extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {
                String recordId = params.get('recordId');
                TaxSubsidy__c taxRecord = taxSubsidyQuerySrv.getTaxSubsidyById(recordId);
                String parentCase = taxRecord.Case__r.TechnicalDetails__c;
                String caseRecord = taxRecord.Case__c;
                taxSubsidySrv.deleteTaxSubsidy(recordId);
                databaseSrv.deleteSObject(caseRecord);
                response.put('parentCase', parentCase);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class notActiveTaxSubsidy extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {
                String recordId = params.get('recordId');
                TaxSubsidy__c taxSubsidy = new TaxSubsidy__c(Id=params.get('recordId'),Status__c = 'Not Active');
                taxSubsidySrv.updateTaxSubsidy(taxSubsidy);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class createTaxSubsidy extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {
                TaxSubsidy__c taxSubsidy = (TaxSubsidy__c) JSON.deserialize(params.get('fields'), TaxSubsidy__c.class);
                taxSubsidy.Status__c = 'Activating';
                taxSubsidySrv.createTaxSubsidy(taxSubsidy);
                response.put('taxSubsidy',taxSubsidy);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class createCaseToTaxSubsidy extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<Case> casesList = new List<Case>();
            try {
                List<Supply__c> suppliesList = (List<Supply__c>) JSON.deserialize(params.get('suppliesList'), List<Supply__c>.class);
                if (!suppliesList.isEmpty() && suppliesList != null) {
                    for(Supply__c supplyes:suppliesList){
                        Case caseRecord = new Case(
                                Dossier__c = params.get('dossierId'),
                                Supply__c = supplyes.Id,
                                RecordTypeId = caseTaxChangeRecordType,
                                Origin = params.get('origin'),
                                Channel__c = params.get('channel'),
                                AccountId = params.get('accountId')
                        );
                        if(params.get('requestType') == System.Label.UpdateRequest){
                            caseRecord.SubProcess__c = 'Tax Change';
                        }
                        if(params.get('requestType') == System.Label.NewRequest){
                            caseRecord.SubProcess__c = 'New Tax';
                        }
                        casesList.add(caseRecord);
                    }
                    databaseSrv.insertSObject(casesList);
                } else {
                    Case caseRecord = new Case(
                            Dossier__c = params.get('dossierId'),
                            RecordTypeId = caseTaxChangeRecordType,
                            Origin = params.get('origin'),
                            Channel__c = params.get('channel'),
                            AccountId = params.get('accountId')
                    );
                    if (String.isNotBlank(params.get('parentCaseId'))) {
                        caseRecord.TechnicalDetails__c = params.get('parentCaseId');
                    }
                    if (String.isNotBlank(params.get('supplyId'))) {
                        caseRecord.Supply__c = params.get('supplyId');
                    }
                    if(params.get('requestType') == System.Label.UpdateRequest){
                        caseRecord.SubProcess__c = 'Tax Change';
                    }
                    if(params.get('requestType') == System.Label.NewRequest){
                        caseRecord.SubProcess__c = 'New Tax';
                    }
                    databaseSrv.insertSObject(caseRecord);
                    casesList.add(caseRecord);
                }
                response.put('casesList',casesList);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}