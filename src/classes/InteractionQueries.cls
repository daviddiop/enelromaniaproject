public with sharing class InteractionQueries {

    public static InteractionQueries getInstance() {
        return new InteractionQueries();
    }

    public Interaction__c findInteractionById(String interactionId) {
        List<Interaction__c> interactionList = [
            SELECT Id, Name, CreatedDate, Channel__c, Interlocutor__c, Status__c, Comments__c,
                InterlocutorFirstName__c, InterlocutorLastName__c, InterlocutorNationalIdentityNumber__c, InterlocutorEmail__c, InterlocutorPhone__c,
                Interlocutor__r.Id, Interlocutor__r.Name, Interlocutor__r.FirstName, Interlocutor__r.LastName,
                Interlocutor__r.NationalIdentityNumber__c,
                (
                    SELECT Id, Customer__c, Interaction__c, Contact__c
                    FROM CustomerInteractions__r
                )
            FROM Interaction__c
            WHERE Id = :interactionId
        ];
        if (interactionList.isEmpty()) {
            return null;
        }
        return interactionList.get(0);
    }
}