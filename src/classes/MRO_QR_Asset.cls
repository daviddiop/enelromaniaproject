/**
 * Created by Movsar Bekaev on 07.03.2020.
 */

public with sharing class MRO_QR_Asset {
    public static MRO_QR_Asset getInstance() {
        return (MRO_QR_Asset) ServiceLocator.getInstance(MRO_QR_Asset.class);
    }

    public List<Asset> listByAccountIdOrderedByLatestFirst(Id accountId) {
        return [
            SELECT Id, Name, AssetNumber__c, AccountId, ContactId, NE__Text_Extension_1__c, NE__Text_Extension_2__c, NE__Text_Extension_3__c, NE__Text_Extension_4__c, IsExternalProduct__c, ManufacturingDate__c,
                AssetStreetType__c, AssetStreetNumber__c, AssetStreetNumberExtn__c, AssetStreetName__c, AssetProvince__c, AssetPostalCode__c, AssetFloor__c, AssetLocality__c,
                AssetCountry__c, AssetCity__c, AssetBuilding__c, AssetBlock__c, AssetApartment__c, AssetAddressKey__c, AssetStreetId__c, AssetAddressNormalized__c
            FROM Asset
            WHERE AccountId = :accountId
            ORDER BY CreatedDate desc
        ];
    }

    public Asset getById(Id assetId) {
        List<Asset> assetList = [
                SELECT Id, Name, AssetNumber__c, AccountId, ContactId, NE__Text_Extension_1__c, NE__Text_Extension_2__c, NE__Text_Extension_3__c, NE__Text_Extension_4__c,
                        IsExternalProduct__c, ManufacturingDate__c, NE__Address_Line1__c, NE__Address_Line2__c, NE__Address_Line3__c, AssetAddressNormalized__c, Quantity
                FROM Asset
                WHERE Id = :assetId
        ];
        if (assetList.size() > 0){
            return assetList.get(0);
        } else {
            return null;
        }
    }
}