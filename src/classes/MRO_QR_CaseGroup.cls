/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 15, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_CaseGroup {

    public static MRO_QR_CaseGroup getInstance() {
        return (MRO_QR_CaseGroup) ServiceLocator.getInstance(MRO_QR_CaseGroup.class);
    }

    public List<CaseGroup__c> listByIds(Set<Id> caseGroupIds) {
        return [SELECT Name, IntegrationKey__c, ServiceSite__c, Status__c, CaseCount__c, ExecutionLogCount__c
                FROM CaseGroup__c WHERE Id IN :caseGroupIds];
    }

    public Map<Id, CaseGroup__c> listByIdsForUpdate(Set<Id> caseGroupIds) {
        return new Map<Id, CaseGroup__c>([SELECT Name, IntegrationKey__c, ServiceSite__c, Status__c, CaseCount__c, ExecutionLogCount__c
                                          FROM CaseGroup__c WHERE Id IN :caseGroupIds FOR UPDATE]);
    }
}