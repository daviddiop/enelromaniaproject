/**
 * Created by tommasobolis on 03/11/2020.
 */

public with sharing class MRO_ACT_AssignmentRuleConfiguration  implements MRO_ACT_IAssignmentRuleApexAction {

    private static Map<String, String> storeOfficeToQueueDeveloperNameMap = new Map<String, String> {

        // Magazine Enel
        'Timisoara' => 'MRO_ME_B_TM_1',
        'Arad' => 'MRO_ME_B_AR_1',
        'Deva' => 'MRO_ME_B_HD_1',
        'Resita' => 'MRO_ME_B_CS_1',
        'Primaverii' => 'MRO_ME_D_CTP_1',
        'Soveja' => 'MRO_ME_D_CTS_1',
        'Tulcea' => 'MRO_ME_D_TL_1',
        'Calarasi' => 'MRO_ME_D_CL_1',
        'Slobozia' => 'MRO_ME_D_IL_1',
        'Mircea Voda' => 'MRO_ME_M_DT_1',
        'Militari' => 'MRO_ME_M_MI_1',
        'Drumul Taberei' => 'MRO_ME_M_DRT_1',
        'Pantelimon' => 'MRO_ME_M_PA_1',
        'Berceni' => 'MRO_ME_M_BE_1',
        'Nord' => 'MRO_ME_M_NO_1',
        'Giurgiu' => 'MRO_ME_M_GR_1',

        // Magazine Partner
        'Magazin Partener Baia Mare 1' => 'MRO_MP_MM_BM_1',
        'Magazin Partener Satu Mare 1' => 'MRO_MP_SM_SM_1',
        'Magazin Partener Targu Mures 1' => 'MRO_MP_MS_TM_1',
        'Magazin Partener Bacau 1' => 'MRO_MP_BC_BC_1',
        'Magazin Partener Botosani 1' => 'MRO_MP_BT_BT_1',
        'Magazin Partener Focsani 1' => 'MRO_MP_VN_FO_1',
        'Magazin Partener Suceava 1' => 'MRO_MP_SV_SV_1',
        'Magazin Partener Vaslui 1' => 'MRO_MP_VS_VS_1',
        'Magazin Partener Piatra Neamt 1' => 'MRO_MP_NT_PN_1',
        'Magazin Partener Craiova 1' => 'MRO_MP_DJ_CR_1',
        'Magazin Partener Slatina 1' => 'MRO_MP_OT_SL_1',
        'Magazin Partener Alexandria 1' => 'MRO_MP_TR_AL_1',
        'Magazin Partener Targu Jiu 1' => 'MRO_MP_GJ_TJ_1',
        'Magazin Partener Pitesti 1' => 'MRO_MP_AG_PI_1',
        'Magazin Partener Buzau 1' => 'MRO_MP_BZ_BZ_1',
        'Magazin Partener Ploiesti 1' => 'MRO_MP_PH_PL_1',
        'Magazin Partener Baia Mare 1' => 'MRO_MP_MM_BM_2',
        'Magazin Partener Satu Mare 1' => 'MRO_MP_SM_SM_2',
        'Magazin Partener Targu Mures 1' => 'MRO_MP_MS_TM_2',
        'Magazin Partener Bacau 1' => 'MRO_MP_BC_BC_2',
        'Magazin Partener Botosani 1' => 'MRO_MP_BT_BT_2',
        'Magazin Partener Focsani 1' => 'MRO_MP_VN_FO_2',
        'Magazin Partener Suceava 1' => 'MRO_MP_SV_SV_2',
        'Magazin Partener Vaslui 1' => 'MRO_MP_VS_VS_2',
        'Magazin Partener Piatra Neamt 1' => 'MRO_MP_NT_PN_2',
        'Magazin Partener Craiova 1' => 'MRO_MP_DJ_CR_2',
        'Magazin Partener Slatina 1' => 'MRO_MP_OT_SL_2',
        'Magazin Partener Alexandria 1' => 'MRO_MP_TR_AL_2',
        'Magazin Partener Targu Jiu 1' => 'MRO_MP_GJ_TJ_2',
        'Magazin Partener Pitesti 1' => 'MRO_MP_AG_PI_2',
        'Magazin Partener Buzau 1' => 'MRO_MP_BZ_BZ_2',
        'Magazin Partener Ploiesti 1' => 'MRO_MP_PH_PL_2',
        'Magazin Partener Baia Mare 1' => 'MRO_MP_MM_BM_3',
        'Magazin Partener Satu Mare 1' => 'MRO_MP_SM_SM_3',
        'Magazin Partener Targu Mures 1' => 'MRO_MP_MS_TM_3',
        'Magazin Partener Bacau 1' => 'MRO_MP_BC_BC_3',
        'Magazin Partener Botosani 1' => 'MRO_MP_BT_BT_3',
        'Magazin Partener Focsani 1' => 'MRO_MP_VN_FO_3',
        'Magazin Partener Suceava 1' => 'MRO_MP_SV_SV_3',
        'Magazin Partener Vaslui 1' => 'MRO_MP_VS_VS_3',
        'Magazin Partener Piatra Neamt 1' => 'MRO_MP_NT_PN_3',
        'Magazin Partener Craiova 1' => 'MRO_MP_DJ_CR_3',
        'Magazin Partener Slatina 1' => 'MRO_MP_OT_SL_3',
        'Magazin Partener Alexandria 1' => 'MRO_MP_TR_AL_3',
        'Magazin Partener Targu Jiu 1' => 'MRO_MP_GJ_TJ_3',
        'Magazin Partener Pitesti 1' => 'MRO_MP_AG_PI_3',
        'Magazin Partener Buzau 1' => 'MRO_MP_BZ_BZ_3',
        'Magazin Partener Ploiesti 1' => 'MRO_MP_PH_PL_3',

        // Back Office Sales
        'Activarea Si Managementul Clientilor - Zone Enel' => 'MRO_AMC_E_1',
        'Activarea Si Managementul Clientilor - Zone Noi Enel' => 'MRO_AMC_NE_1',
        'Activarea Si Managementul Clientilor - Gaz' => 'MRO_AMC_GAZ_1',
        'Activarea Si Managementul Clientilor - Corporate' => 'MRO_AMC_CC_1'
    };

    public Set<String> getQueueDeveloperNames() {

        Set<String> queueDeveloperNames = new Set<String>();
        queueDeveloperNames.addAll(storeOfficeToQueueDeveloperNameMap.values());
        return queueDeveloperNames;
    }

    public Object execute(Object args) {

        Map<String, Object> params = (Map<String, Object>)args;

        String apexClassMethod = (String)params.get('apexClassMethod');

        SObject sObj = params.containsKey('sObject') ? (SObject)params.get('sObject') : null;
        User owner = params.containsKey('owner') ? (User)params.get('owner') : null;

        if(apexClassMethod == 'getStoreOfficeQueueDeveloperName'&& owner != null) {

            return getStoreOfficeQueueDeveloperName(owner);
        }

        return null;
    }

    private String getStoreOfficeQueueDeveloperName(User owner) {

        String magazineEnelQueueDeveloperName = null;
        if(String.isNotBlank(owner.StoreOffice__c) && storeOfficeToQueueDeveloperNameMap.containsKey(owner.StoreOffice__c)) {

            magazineEnelQueueDeveloperName = storeOfficeToQueueDeveloperNameMap.get(owner.StoreOffice__c);
        }
        return magazineEnelQueueDeveloperName;
    }
}