public with sharing class MRO_QR_ConfigurationItem {

    public static MRO_QR_ConfigurationItem getInstance() {
        return (MRO_QR_ConfigurationItem)ServiceLocator.getInstance(MRO_QR_ConfigurationItem.class);
    }

    public List<NE__OrderItem__c> getConfigurationItemsByCommercialProductId(Id commProdId) {
        return [
            SELECT Id, NE__ProdId__c, NE__IsPromo__c, NE__BaseOneTimeFee__c, NE__BaseRecurringCharge__c,
                NE__Penalty_Fee__c, NE__PromotionId__c, PartnerName__c
            FROM NE__OrderItem__c
            WHERE  NE__ProdId__c =: commProdId
        ];
    }

    public List<NE__OrderItem__c> listConfigurationItemsByConfigurationId(Id configurationId) {
        String queryString = 'SELECT '+MRO_UTL_SObject.getQueryStringAllFields(Schema.NE__OrderItem__c.getSObjectType(), null)
                                    +','+MRO_UTL_SObject.getQueryStringAllFields(Schema.NE__Order__c.getSObjectType(), 'NE__OrderId__r')
                                    +',NE__ProdId__r.NE__Sales_Type__c'
                            +' FROM NE__OrderItem__c WHERE NE__OrderId__c = \''+configurationId+'\'';
        return Database.query(queryString);
    }

    public List<NE__OrderItem__c> listByIds(Set<String> orderItemIds) {
        if (orderItemIds == null || orderItemIds.isEmpty()) {
            return new List<NE__OrderItem__c>();
        }
        String whereClause = '(\'' + String.join(new List<String>(orderItemIds), '\', \'') + '\')';
        String queryString =
            'SELECT ' + MRO_UTL_SObject.getQueryStringAllFields(Schema.NE__OrderItem__c.getSObjectType(), null) + ' , NE__ProdId__r.ProductCode__c '
            + ' FROM NE__OrderItem__c ' +
            + ' WHERE Id IN ' + whereClause;
        return Database.query(queryString);
    }

}