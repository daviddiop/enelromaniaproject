/**
 * Created by goudiaby on 24/07/2019.
 */

public with sharing class MRO_SRV_CompanyDivision {
    private static MRO_QR_CompanyDivision companyDivisionQuery = MRO_QR_CompanyDivision.getInstance();
    private static UserQueries userQuery = UserQueries.getInstance();

    public static MRO_SRV_CompanyDivision getInstance() {
        return new MRO_SRV_CompanyDivision();
    }

    public CompanyDivision__c getCompanyDivisionInfosFromUser(String userId){
        CompanyDivision__c companyDivision = new CompanyDivision__c();
        User currentUser = userQuery.getCompanyDivisionId(userId);
        if (String.isNotBlank(currentUser.CompanyDivisionId__c)) {
            companyDivision = companyDivisionQuery.getById(currentUser.CompanyDivisionId__c);
        }
        return companyDivision;
    }

    public List<ApexServiceLibraryCnt.Option> getListOptionsCompanyDivision(){
        List<ApexServiceLibraryCnt.Option> companyDivisionOptions = new List<ApexServiceLibraryCnt.Option>();
        List<CompanyDivision__c> allCompanyDivisions = companyDivisionQuery.getAll();
        for (CompanyDivision__c company : allCompanyDivisions) {
            companyDivisionOptions.add(new ApexServiceLibraryCnt.Option(company.Name, company.Id));
        }
        return companyDivisionOptions;
    }

    /**
     * @author Boubacar Sow
     * @date 11/06/2020 at 10:48
     * @description task: [ENLCRO-997] Removal of Enel Trade from Company Division Component
     * @return List<ApexServiceLibraryCnt.Option>
     */
    public List<ApexServiceLibraryCnt.Option> getListOptionsActiveCompanyDivision() {
        List<ApexServiceLibraryCnt.Option> companyDivisionOptions = new List<ApexServiceLibraryCnt.Option>();
        List<CompanyDivision__c> allCompanyDivisions = companyDivisionQuery.getAllActive();
        for (CompanyDivision__c company : allCompanyDivisions) {
            companyDivisionOptions.add(new ApexServiceLibraryCnt.Option(company.Name, company.Id));
        }
        return companyDivisionOptions;
    }
}