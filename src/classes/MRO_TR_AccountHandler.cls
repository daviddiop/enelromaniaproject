/**
 * Created by napoli on 26/09/2019.
 */
/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   set 26, 2019
* @desc
* @history
*/
global with sharing class MRO_TR_AccountHandler implements TriggerManager.ISObjectTriggerHandler {
    public static Sequencer__c CUSTOMER_CODE_SEQUENCER;
    private static final Set<Id>  RESIDENTIAL_CUSTOMER_RT_IDS = new Set<Id>{
        Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId(),
        Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId()
    };
    private static final Set<Id>  BUSINESS_CUSTOMER_RT_IDS = new Set<Id>{
        Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId(),
        Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId()
    };
    private DatabaseService databaseSrv = DatabaseService.getInstance();

    global void afterDelete() {}

    global void afterInsert() {
        if (CUSTOMER_CODE_SEQUENCER != null) {
            databaseSrv.updateSObject(CUSTOMER_CODE_SEQUENCER);
        }
    }

    global void afterUndelete() {}

    global void afterUpdate() {}

    global void beforeDelete() {}

    global void beforeInsert() {
        this.checkCNP(Trigger.new, null);
        System.debug('beforeInsert trigger Account 1 - Queries used in this apex code so far: ' + Limits.getQueries());
        this.checkCUI(Trigger.new, null);
        System.debug('beforeInsert trigger Account 2 - Queries used in this apex code so far: ' + Limits.getQueries());

        this.generateCustomerCodes(Trigger.new);
    }

    global void beforeUpdate() {
        this.checkCNP(Trigger.new, (Map<Id, Account>)Trigger.oldMap);
        System.debug('beforeUpdate trigger Account 1 - Queries used in this apex code so far: ' + Limits.getQueries());
        this.checkCUI(Trigger.new, (Map<Id, Account>)Trigger.oldMap);
        System.debug('beforeUpdate trigger Account 2 - Queries used in this apex code so far: ' + Limits.getQueries());
    }

    private void checkCNP(List<Account> accountListNew, Map<Id, Account> accountMapOld) {

        for(Account newAccount : accountListNew) {
            if (newAccount.IsPersonAccount && newAccount.NationalIdentityNumber__pc != null && newAccount.ForeignCitizen__pc != true &&
                    (accountMapOld == null ||
                            (newAccount.NationalIdentityNumber__pc != accountMapOld.get(newAccount.Id).NationalIdentityNumber__pc) ||
                            (newAccount.ForeignCitizen__pc != accountMapOld.get(newAccount.Id).ForeignCitizen__pc))) {
                MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(newAccount.NationalIdentityNumber__pc);
                if(result.outCome == false) newAccount.NationalIdentityNumber__pc.addError(String.format(Label.InvalidCnp, new List<String>{result.errorCode, newAccount.NationalIdentityNumber__pc}));
            }
        }
    }

    private void checkCUI(List<Account> accountListNew, Map<Id, Account> accountMapOld) {

        for(Account newAccount : accountListNew) {
            if (newAccount.VATNumber__c != null && newAccount.ForeignCompany__c != true &&
                    (accountMapOld == null || (newAccount.VATNumber__c != accountMapOld.get(newAccount.Id).VATNumber__c) ||
                            (newAccount.ForeignCompany__c != accountMapOld.get(newAccount.Id).ForeignCompany__c))) {
                MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCUI(newAccount.VATNumber__c);
                if(result.outCome == false) newAccount.VATNumber__c.addError(String.format(Label.InvalidCUI, new List<String>{result.errorCode, newAccount.VATNumber__c}));
            }
        }
    }

    private void generateCustomerCodes(List<Account> accounts) {
        List<Account> newCustomers = new List<Account>();
        for (Account acc : accounts) {
            if ((RESIDENTIAL_CUSTOMER_RT_IDS.contains(acc.RecordTypeId) || BUSINESS_CUSTOMER_RT_IDS.contains(acc.RecordTypeId)) && (acc.AccountNumber == null)) {
                newCustomers.add(acc);
            }
        }
        if (!newCustomers.isEmpty()) {
            MRO_QR_Sequencer sequencerQueries = MRO_QR_Sequencer.getInstance();
            CUSTOMER_CODE_SEQUENCER = sequencerQueries.getCustomerCodeSequencer();

            for (Account acc : newCustomers) {
                CUSTOMER_CODE_SEQUENCER.Sequence__c += 1;
                acc.AccountNumber = 'C'+String.valueOf(CUSTOMER_CODE_SEQUENCER.Sequence__c.intValue()).leftPad(CUSTOMER_CODE_SEQUENCER.SequenceLength__c.intValue(), '0');
                acc.IntegrationKey__c = acc.AccountNumber;
            }
        }
    }
}