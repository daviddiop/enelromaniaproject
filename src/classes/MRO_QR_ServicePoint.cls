public with sharing class MRO_QR_ServicePoint {

    public static MRO_QR_ServicePoint getInstance() {
        return new MRO_QR_ServicePoint();
    }
//FF OSI Edit - Pack2 - Interface Check
    // [ENLCRO-657] OSI Edit - Pack3 - Interface Check
    public List<ServicePoint__c> getPointsWithSupplies(Set<Id> ids) {
        return [
                SELECT Id, Account__c, CurrentSupply__c, CurrentSupply__r.Status__c, CurrentSupply__r.CompanyDivision__c, CurrentSupply__r.Account__c,
                       Distributor__c, ENELTEL__c, Code__c,ConsumptionCategory__c,FlowRate__c,ATRExpirationDate__c, IsNewConnection__c,
                       (SELECT Id, Account__c, Status__c, CompanyDivision__c FROM Supplies__r ORDER BY ActivationDate__c DESC NULLS FIRST)
                FROM ServicePoint__c
                WHERE Id IN :ids
        ];
    }

    public List<ServicePoint__c> getListServicePoints(String pointCode) {
        return [
                SELECT Id, RecordType.DeveloperName, ATRExpirationDate__c, IsNewConnection__c, ConsumptionCategory__c, FlowRate__c,Code__c,CurrentSupply__r.Status__c,
                       CurrentSupply__c, CurrentSupply__r.CompanyDivision__c,CurrentSupply__r.Activator__r.CaseNumber,CurrentSupply__r.Terminator__r.CaseNumber
                FROM ServicePoint__c
                WHERE Code__c = :pointCode
                LIMIT 1
        ];
    }

    public List<ServicePoint__c> getListServicePointsByAccountId(String pointCode,String accountId) {
        return [
                SELECT Id, RecordType.DeveloperName, CurrentSupply__c, CurrentSupply__r.Status__c,CurrentSupply__r.ContractAccount__r.BillingProfile__c, ATRExpirationDate__c, IsNewConnection__c,Code__c,
                       CurrentSupply__r.CompanyDivision__c,CurrentSupply__r.RecordType.DeveloperName,ConsumptionCategory__c,FlowRate__c,CurrentSupply__r.Activator__r.CaseNumber,CurrentSupply__r.Terminator__r.CaseNumber,
                       CurrentSupply__r.ServiceSite__r.Id, CurrentSupply__r.ServiceSite__r.SiteAddressKey__c, CurrentSupply__r.ServiceSite__r.SiteAddressNormalized__c, CurrentSupply__r.ServiceSite__r.SiteApartment__c,
                       CurrentSupply__r.ServiceSite__r.SiteBuilding__c, CurrentSupply__r.ServiceSite__r.SiteBlock__c, CurrentSupply__r.ServiceSite__r.SiteCity__c, CurrentSupply__r.ServiceSite__r.SiteCountry__c,
                       CurrentSupply__r.ServiceSite__r.SiteFloor__c, CurrentSupply__r.ServiceSite__r.SiteStreetType__c, CurrentSupply__r.ServiceSite__r.SiteLocality__c, CurrentSupply__r.ServiceSite__r.SitePostalCode__c,
                       CurrentSupply__r.ServiceSite__r.SiteProvince__c, CurrentSupply__r.ServiceSite__r.SiteStreetName__c, CurrentSupply__r.ServiceSite__r.SiteStreetNumber__c, CurrentSupply__r.ServiceSite__r.SiteStreetNumberExtn__c,
                       CurrentSupply__r.ServiceSite__r.SiteAddress__c, CurrentSupply__r.ServiceSite__r.SiteStreetId__c,
                       CurrentSupply__r.ServiceSite__r.CLC__c, CurrentSupply__r.ServiceSite__r.NLC__c, CurrentSupply__r.ServiceSite__r.Account__c, CurrentSupply__r.ServiceSite__r.ActiveSuppliesCountELE__c,
                       CurrentSupply__r.ServiceSite__r.ActiveSuppliesCountGAS__c, CurrentSupply__r.ServiceSite__r.ActiveSuppliesCountVAS__c, CurrentSupply__r.ServiceSite__r.DistributorELE__c, CurrentSupply__r.ServiceSite__r.DistributorELE__r.IsDisCoENEL__c,
                       CurrentSupply__r.ServiceSite__r.DistributorGAS__c, CurrentSupply__r.ServiceSite__r.Description__c
                FROM ServicePoint__c
                WHERE Code__c = :pointCode and Account__c = :accountId
                LIMIT 1
        ];
    }

    public List<ServicePoint__c> getListServicePoints(Set<Id> ids) {
        return [
                SELECT Id, CurrentSupply__c, ContractualPower__c, ConsumptionCategory__c, FlowRate__c, Account__c, ATRExpirationDate__c, IsNewConnection__c,Code__c,
                    CurrentSupply__r.Status__c, CurrentSupply__r.CompanyDivision__c, CurrentSupply__r.Contract__r.ContractType__c,CurrentSupply__r.Activator__r.CaseNumber,CurrentSupply__r.Terminator__r.CaseNumber
                FROM ServicePoint__c
                WHERE ID = :ids
        ];
    }

    public List<ServicePoint__c> getListServicePointsByCodes(Set<String> codes) {
        return [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,CurrentSupply__r.ContractAccount__r.BillingProfile__c,Code__c,CurrentSupply__r.Account__c,
                        CurrentSupply__r.CompanyDivision__c,CurrentSupply__r.RecordType.DeveloperName,ConsumptionCategory__c,FlowRate__c, ATRExpirationDate__c, IsNewConnection__c,
                        CurrentSupply__r.Activator__r.CaseNumber,CurrentSupply__r.Terminator__r.CaseNumber,
                        PointStreetName__c,PointStreetNumber__c,PointStreetNumberExtn__c,PointPostalCode__c,
                        PointCity__c,PointCountry__c,PointStreetType__c,PointApartment__c,PointBuilding__c,PointBlock__c,
                        PointLocality__c,PointProvince__c,PointFloor__c,PointAddressNormalized__c,PointAddress__c,
                        PointAddressKey__c,PointStreetId__c
                FROM ServicePoint__c
                WHERE Code__c IN :codes
        ];
    }

    /**
     * Added by Moussa 12/09/2019
     *
     */
    public List<ServicePoint__c> getListServicePointsByCompanyDivision(String id,String companyDivisionId) {
        return [
                SELECT Id, CurrentSupply__c, CurrentSupply__r.Status__c,ATRExpirationDate__c, IsNewConnection__c,
                        CurrentSupply__r.CompanyDivision__c,ConsumptionCategory__c,FlowRate__c
                FROM ServicePoint__c
                WHERE ID = :id and CurrentSupply__r.CompanyDivision__c=:companyDivisionId
        ];
    }
    /**
     *
     * @Moussa Fofana
     * @param servicePointId
     * @description get ServicePoint by the Id.
     * @create 22/07/2019
     * @return ServicePoint__c
     */
    public ServicePoint__c getById(String servicePointId) {
        return [
                SELECT  Name, EstimatedConsumption__c,ContractualPower__c,AvailablePower__c,ATRExpirationDate__c, IsNewConnection__c,
                        Trader__c, Distributor__c, Distributor__r.IsDisCoENEL__c, Account__c,VoltageLevel__c,PowerPhase__c,Voltage__c,ConversionFactor__c,FirePlacesCount__c,
                        Pressure__c,PressureLevel__c,Code__c,ConsumptionCategory__c,FlowRate__c,
                        PointStreetName__c,PointStreetNumber__c,PointStreetNumberExtn__c,PointPostalCode__c,
                        PointCity__c,PointCountry__c,PointStreetType__c,PointApartment__c,PointBuilding__c,PointBlock__c,
                        PointLocality__c,PointProvince__c,PointFloor__c,PointAddressNormalized__c,PointAddress__c,
                        PointAddressKey__c,PointStreetId__c, RecordTypeId, RecordType.DeveloperName,
                        CurrentSupply__r.Market__c, CurrentSupply__r.NonDisconnectable__c, CurrentSupply__r.ServiceSite__c, CurrentSupply__r.ServiceSite__r.SiteAddressKey__c, CurrentSupply__r.CompanyDivision__c,
                        CurrentSupply__r.TitleDeedValidity__c, CurrentSupply__r.NACEReference__c,CurrentSupply__r.SupplyOperation__c, CurrentSupply__r.FlatRate__c, CurrentSupply__r.Status__c, ENELTEL__c
                FROM ServicePoint__c
                WHERE id = :servicePointId
                LIMIT 1
        ];
    }
    /**
     *
     * @Moussa Insa
     * @param Set<Id> servicePoint
     * @description get ServicePoint by the Id.
     * @create 22/07/2019
     * @return ServicePoint__c
     */
    public ServicePoint__c getByIds(Set<Id> servicePointId) {
        List<ServicePoint__c> servicePoints =
         [
                SELECT Trader__c,EstimatedConsumption__c,ContractualPower__c,AvailablePower__c,ConsumptionCategory__c,FlowRate__c,ATRExpirationDate__c, IsNewConnection__c,
                        Distributor__c,Account__c,VoltageLevel__c,PowerPhase__c,Voltage__c,ConversionFactor__c, FirePlacesCount__c,
                        Pressure__c,PressureLevel__c,PointStreetName__c,PointStreetNumber__c,PointStreetNumberExtn__c,
                        PointPostalCode__c,PointCity__c,PointCountry__c,PointStreetType__c,PointApartment__c,PointBuilding__c,PointBlock__c,
                        PointLocality__c,PointProvince__c,PointFloor__c,PointAddressNormalized__c,PointAddressKey__c,PointStreetId__c
                FROM ServicePoint__c
                WHERE id IN :servicePointId
                LIMIT 1000
        ];
        if (servicePoints.isEmpty()) {
            return null;
        }
        return servicePoints.get(0);
    }

    public List<ServicePoint__c> listByServicePointCode(Set<String> servicePointCodes) {
        return [SELECT Id, Name, Code__c, RecordTypeId, RecordType.DeveloperName, Trader__c, EstimatedConsumption__c, ContractualPower__c, AvailablePower__c,
                       Distributor__c, Distributor__r.IsDisCoENEL__c, Account__c, VoltageLevel__c, PowerPhase__c, Voltage__c,
                       ConversionFactor__c, FirePlacesCount__c,ATRExpirationDate__c, IsNewConnection__c,
                       Pressure__c, PressureLevel__c, PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c,ConsumptionCategory__c,FlowRate__c,
                       PointPostalCode__c, PointCity__c, PointCountry__c, PointStreetType__c, PointApartment__c, PointBuilding__c, PointBlock__c,
                       PointLocality__c, PointProvince__c, PointFloor__c, PointAddressNormalized__c, PointAddressKey__c, PointStreetId__c
                FROM ServicePoint__c
                WHERE Code__c IN :servicePointCodes];
    }

    /**
    * @author Baba Goudiaby
    * @description get Map Id Service Point by Supplies
    * @date 13/02/2020
    * @param Set<Id> supplyIds
    * @return Map<Id, ServicePoint__c>
    */
    public Map<Id, ServicePoint__c> mapServicePointsByIds(Set<Id> servicePoints) {
        return new Map<Id, ServicePoint__c>([

                SELECT Id, Name, Distributor__c, Trader__c, AvailablePower__c,FlowRate__c,ATRExpirationDate__c, IsNewConnection__c,
                        ContractualPower__c, EstimatedConsumption__c, PowerPhase__c, ConversionFactor__c, Pressure__c, PressureLevel__c,
                        Voltage__c, VoltageLevel__c,ConsumptionCategory__c, RecordTypeId, RecordType.DeveloperName, PointStreetNumber__c,
                        PointStreetNumberExtn__c, PointStreetName__c, PointCity__c, PointCountry__c, PointPostalCode__c, PointStreetType__c,
                        PointApartment__c, PointBuilding__c, PointBlock__c, PointLocality__c, PointProvince__c, PointFloor__c, PointAddressNormalized__c,
                        CurrentSupply__r.Account__c, CurrentSupply__r.Contract__c, CurrentSupply__r.ContractAccount__c, CurrentSupply__r.Product__c,
                        PointAddressKey__c,PointStreetId__c, CurrentSupply__r.Market__c, CurrentSupply__r.NonDisconnectable__c,
                        CurrentSupply__r.ServiceSite__c, CurrentSupply__r.ServiceSite__r.SiteAddressKey__c, CurrentSupply__r.TitleDeedValidity__c,
                        CurrentSupply__r.NACEReference__c, CurrentSupply__r.SupplyOperation__c,CurrentSupply__r.ServiceSite__r.SiteAddressNormalized__c,
                        CurrentSupply__r.ServiceSite__r.SiteApartment__c,CurrentSupply__r.ServiceSite__r.SiteBlock__c,CurrentSupply__r.ServiceSite__r.SiteBuilding__c,
                        CurrentSupply__r.ServiceSite__r.SiteCity__c,CurrentSupply__r.ServiceSite__r.SiteCountry__c,CurrentSupply__r.ServiceSite__r.SiteFloor__c,
                        CurrentSupply__r.ServiceSite__r.SitePostalCode__c,CurrentSupply__r.ServiceSite__r.SiteProvince__c,CurrentSupply__r.ServiceSite__r.SiteStreetId__c,
                        CurrentSupply__r.ServiceSite__r.SiteStreetName__c,CurrentSupply__r.ServiceSite__r.SiteStreetNumber__c,CurrentSupply__r.ServiceSite__r.SiteStreetNumberExtn__c,
                        CurrentSupply__r.ServiceSite__r.SiteStreetType__c,CurrentSupply__r.FlatRate__c
                FROM ServicePoint__c
                WHERE Id IN :servicePoints
        ]);
    }
    // [ENLCRO-657] OSI Edit - Pack3 - Interface Check
    //FF OSI Edit - Pack2 - Interface Check
}