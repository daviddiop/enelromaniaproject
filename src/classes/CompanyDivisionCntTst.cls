/**
 * Created by goudiaby on 10/05/2019.
 * Modified by Moussa on 27/08/2019.
 * Modified by David on 27/11/2019.
 */

@IsTest
private class CompanyDivisionCntTst {


    @testSetup
    static void setup() {
        User user = TestDataFactory.user().standardUser().build();
        insert user;

        List<CompanyDivision__c> listCompaniesDivision = new List<CompanyDivision__c>();
        for (Integer i = 0; i < 10; i++) {
            CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(i).build();
            listCompaniesDivision.add(companyDivision);
        }
        insert listCompaniesDivision;


    }

    @isTest
    static void getCompanyDivisionDataTest() {
        User user = [
                SELECT Id,CompanyDivisionId__c
                FROM User
                WHERE Email = 'userdivision@test.com'
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ].Id;

        user.CompanyDivisionId__c = companyDivisionId;
        update user;

        Map<String, String> jsonInput = new Map<String, String>{
                'userId' => user.Id
        };
        Test.startTest();

        Object response = TestUtils.exec('CompanyDivisionCnt', 'getCompanyDivisionData', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('companyDivision') != null);
    }

    @isTest
    static void getCompanyDivisionDataEmptyUserTest() {
        Map<String, String> jsonInput = new Map<String, String>{
                'userId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('CompanyDivisionCnt', 'getCompanyDivisionData', null, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }


    @isTest
    static void updateUserCompanyDivisionTest() {
        User user = [
                SELECT Id,CompanyDivisionId__c
                FROM User
                WHERE Email = 'userdivision@test.com'
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 2'
                LIMIT 1
        ].Id;

        Map<String, String> jsonInput = new Map<String, String>{
                'userId' => user.Id,
                'companyId' => companyDivisionId,
                'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        Object response = TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('currentUserId') == user.Id);
    }
    // test the method updateCompanyDivision in UserService
    @isTest
    static void updateCompanyDivisionUserTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 2'
            LIMIT 1
        ].Id;

        Test.startTest();
        UserService userSrv = UserService.getInstance();
        String idCurrentUser = userSrv.updateCompanyDivision(user.Id, companyDivisionId);
        Test.stopTest();
        System.assertEquals(true, idCurrentUser == user.Id);
    }

    @isTest
    static void updateUserCompanyDivisionExceptionTest() {
        User user = [
                SELECT Id,CompanyDivisionId__c
                FROM User
                WHERE Email = 'userdivision@test.com'
                LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 2'
                LIMIT 1
        ].Id;

        Map<String, String> jsonInput = new Map<String, String>{
                'userId' => companyDivisionId,
                'companyId' => user.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, false);
        Test.stopTest();
        System.assertEquals(true, response != null);
    }

    @isTest
    static void updateUserCompanyDivisionEmptyUserTest() {
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 2'
                LIMIT 1
        ].Id;

        Map<String, String> jsonInput = new Map<String, String>{
                'userId' => '',
                'companyId' => companyDivisionId,
                'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        try {
            TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, false);

        } catch (Exception e) {
            System.assertEquals(true, e.getMessage() == System.Label.User + ' - ' + System.Label.MissingId);
        }
        Test.stopTest();
    }

    @isTest
    static void updateUserCompanyDivisionEmptyCompanyTest() {
        User user = [
                SELECT Id,CompanyDivisionId__c
                FROM User
                WHERE Email = 'userdivision@test.com'
                LIMIT 1
        ];

        Map<String, String> jsonInput = new Map<String, String>{
                'userId' => user.Id,
                'companyId' => '',
                'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, false);
        }catch (Exception e){
            System.assertEquals(true, e.getMessage() == System.Label.Company + ' - ' + System.Label.MissingId);
        }
        Test.stopTest();
    }

    @isTest
    static void updateUserCompanyDivisiontestUserIsBlankTest() {
        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => '',
            'companyId' => '',
            'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivisiontest', jsonInput, false);
        }catch (Exception e){
            System.assertEquals(true, e.getMessage() == System.Label.User + ' - ' + System.Label.MissingId);
        }
        Test.stopTest();
    }
    
    @isTest
    static void updateUserCompanyDivisiontestCompanyDivisionIsBlankTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => user.Id,
            'companyId' => '',
            'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        try{
            Object response = TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivisiontest', jsonInput, false);
        }catch (Exception e){
            System.assertEquals(true, e.getMessage() == System.Label.Company + ' - ' + System.Label.MissingId);
        }
        Test.stopTest();
    }

    /*@testSetup
    static void setup() {
        User user = TestDataFactory.user().standardUser().build();
        insert user;

        List<CompanyDivision__c> listCompaniesDivision = new List<CompanyDivision__c>();
        for (Integer i = 0; i < 10; i++) {
            CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(i).build();
            listCompaniesDivision.add(companyDivision);
        }
        insert listCompaniesDivision;

        EnergyApp2Settings__c eapSet = new EnergyApp2Settings__c();
        eapSet.Name = 'Default';
        insert eapSet;

    }


    @isTest
    static void getCompanyDivisionDataTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ].Id;

        user.CompanyDivisionId__c = companyDivisionId;
        update user;

        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => user.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('CompanyDivisionCnt', 'getCompanyDivisionData', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('companyDivision') != null);
    }

    @isTest
    static void getCompanyDivisionDataEmptyUserTest() {
        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('CompanyDivisionCnt', 'getCompanyDivisionData', jsonInput, false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void getCurrentCompanyDivisionTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ].Id;

        user.CompanyDivisionId__c = companyDivisionId;
        update user;
               
        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => user.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('CompanyDivisionCnt', 'getCurrentCompanyDivision', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        
        response = TestUtils.exec('CompanyDivisionCnt', 'getCurrentCompanyDivision', jsonInput, true);
        result = (Map<String, Object>) response;
        Test.stopTest();
    }


    @isTest
    static void updateUserCompanyDivisionTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 2'
            LIMIT 1
        ].Id;

        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => user.Id,
            'companyId' => companyDivisionId
        };
        Test.startTest();
        Object response = TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('currentUserId') == user.Id);
    }

    @isTest
    static void updateUserCompanyDivisionExceptionTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 2'
            LIMIT 1
        ].Id;

        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => companyDivisionId,
            'companyId' => user.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, false);
        Test.stopTest();
        System.assertEquals(true, response != null);
    }

    @isTest
    static void updateUserCompanyDivisionEmptyUserTest() {
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 2'
            LIMIT 1
        ].Id;

        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => '',
            'companyId' => companyDivisionId
        };
        Test.startTest();
        try {
            TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, false);

        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void updateUserCompanyDivisionEmptyCompanyTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];

        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => user.Id,
            'companyId' => ''
        };
        Test.startTest();
        try {
            TestUtils.exec('CompanyDivisionCnt', 'updateUserCompanyDivision', jsonInput, false);

        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }*/

}