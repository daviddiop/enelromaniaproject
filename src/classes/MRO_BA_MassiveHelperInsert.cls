/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 06.05.20.
 */

global with sharing class MRO_BA_MassiveHelperInsert implements Database.Batchable<RowWrapper> {

    private static MRO_QR_FileImportJob fijQuery = MRO_QR_FileImportJob.getInstance();
    private static MRO_QR_FileTemplateColumn columnQuery = MRO_QR_FileTemplateColumn.getInstance();
    private static MRO_QR_FileImportDataRow rowsQuery = MRO_QR_FileImportDataRow.getInstance();

    global final String documentId;
    global final FileTemplate__c fileTemplate;
    global FileImportJob__c fileImportJob;
    global Map<String, String> params;

    global with sharing class RowWrapper{
        public Exception e;
        public String dataRow;

        RowWrapper(String dataRow){
            this.dataRow = dataRow;
        }

        RowWrapper(Exception ex){
            this.e = ex;
        }
    }

    global with sharing class CSVIterator implements Iterator<RowWrapper>, Iterable<RowWrapper> {
        private String csvData;
        private String csvRowDelimiter;

        public CSVIterator(String csvData, String csvRowDelimiter) {
            this.csvData = csvData;
            this.csvRowDelimiter = csvRowDelimiter;
        }
        global Boolean hasNext() {
            return csvData != null && csvData.length() > 1 ? true : false;
        }
        global RowWrapper next() {
            try {
                System.debug(csvData.indexOf(csvRowDelimiter));
                Integer delimiterPosition = csvData.indexOf(csvRowDelimiter);

                String row = csvData.substring(0, delimiterPosition);
                csvData = csvData.substring(csvData.indexOf(csvRowDelimiter) + csvRowDelimiter.length(), csvData.length());
                return new RowWrapper(row);
            }catch (Exception e){
                csvData = null;
                return new RowWrapper(e);
            }
        }
        global Iterator<RowWrapper> iterator() {
            return this;
        }
    }

    global MRO_BA_MassiveHelperInsert(FileImportJob__c fileImportJob, String documentId, FileTemplate__c fileTemplate, Map<String, String> params) {
        this.fileImportJob = fileImportJob;
        this.documentId = documentId;
        this.fileTemplate = fileTemplate;
        this.params = params;
    }

    global Iterable<RowWrapper> start(Database.BatchableContext batchableContext) {
        List<ContentVersion> cvList = [SELECT VersionData, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId = :documentId AND IsLatest = TRUE LIMIT 1];
        ContentVersion cv = cvList[0];
        Blob cvFileBody = cv.VersionData;
        String cvString = cvFileBody.toString();

        return new CSVIterator(cvString, '\n');
    }

    global void execute(Database.BatchableContext batchableContext, List<RowWrapper> scope) {
        Integer index = getLastIndex(batchableContext.getJobId());
        System.debug('########## fileImportJob.Id ' + fileImportJob.Id);
        System.debug('########## Index ' + index);
        Boolean skipJob = false;
        try {
            List<FileImportDataRow__c> rows = new List<FileImportDataRow__c>();
            for (RowWrapper rowWrapper : scope) {
                if(rowWrapper.e != null){
                    System.debug(rowWrapper.e.getMessage());
                    throw new WrtsException('Invalid file');
                }
                String row = rowWrapper.dataRow;
                if(index == 0){
                    List<FileTemplateColumn__c> headerColumns = columnQuery.getByFileTemplateId(fileImportJob.FileTemplate__c);
                    checkHeader(row, headerColumns, fileImportJob.FileTemplate__r.FileDelimiter__c);
                }else {
                    rows.add(new FileImportDataRow__c(
                            FileImportJob__c = fileImportJob.Id,
                            Status__c = 'To Be Validated',
                            Index__c = index,
                            RawRow__c = row
                    ));
                }
                index++;
            }
            if (rows.size() > 0) {
                insert rows;
            }
        }catch(Exception e){
            FileImportDataRow__c dataRow = new FileImportDataRow__c(FileImportJob__c = fileImportJob.Id, ErrorMessage__c = e.getMessage());
            MRO_SRV_DatabaseService.getInstance().upsertSObject(dataRow);
            System.debug(e.getMessage());
            if(fileTemplate.AllOrNone__c){
                skipJob = true;
            }
        }
        if(skipJob){
            fileImportJob.Status__c = 'Rejected';
            fileImportJob.SkipJob__c = true;
            update fileImportJob;
            System.abortJob(batchableContext.getJobId());
        }
    }

    global void finish(Database.BatchableContext batchableContext) {
        FileImportJob__c fij = fijQuery.getByJobId(batchableContext.getJobId());
        if(!fij.SkipJob__c) {
            fij.JobID__c = Database.executeBatch(new MRO_BA_MassiveHelperValidation('SELECT Id, FileImportJob__c, FileImportJob__r.FileTemplate__r.FileDelimiter__c, FileImportJob__r.FileTemplate__c,Index__c,ErrorCode__c, ErrorMessage__c, ErrorStackTrace__c, JSONRow__c, RawRow__c, Status__c FROM FileImportDataRow__c WHERE FileImportJob__c=\'' + fij.Id + '\'', 'FileImportDataRow__c', params));
        } else {
            fij.Status__c = 'Rejected';
        }
        update fij;
    }

    private Integer getLastIndex(String apexJobId){
        List<AsyncApexJob> apexJobs = [SELECT Id, JobItemsProcessed FROM AsyncApexJob WHERE Id = :apexJobId];
        System.debug('########### apexJobId ' + apexJobId);
        System.debug(apexJobs);
        if(apexJobs.size() == 0){
            return 0;
        }
        return apexJobs[0].JobItemsProcessed * (Integer)fileTemplate.BatchSize__c;
    }

    private void checkHeader(String headerRow, List<FileTemplateColumn__c> columnsHeader, String delimiter) {
        List<String> headerColumns = headerRow.split(delimiter);
        if(headerColumns.size() < 2){
            throw new WrtsException('Incorrect delimiter');
        }
        System.debug('########## headerColumns ' + headerColumns.size());
        System.debug('########## columnsHeader ' + columnsHeader.size());
        if(headerColumns.size() != columnsHeader.size()) {
            throw new WrtsException('The number of columns does not match. ' + columnsHeader.size() + ' columns expected, ' + headerColumns.size() + ' present');
        }
        for(FileTemplateColumn__c header :columnsHeader) {
            System.debug(headerRow);
            System.debug('########## header.HeaderName__c ' + header.HeaderName__c);
            if(!headerRow.contains(header.HeaderName__c)) {
                throw new WrtsException(header.HeaderName__c + ' column is missing.');
            }
        }
    }
}