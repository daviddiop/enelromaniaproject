public with sharing class AccountService {

    private static final ContactQueries contactQuerySrv = ContactQueries.getInstance();
    private static final AccountQueries accountQuery = AccountQueries.getInstance();
    private static final InteractionQueries interactionQuery = InteractionQueries.getInstance();
    private static final IndividualQueries individualQuery = IndividualQueries.getInstance();

    private static final String PERSON_RT = 'Person';

    static Constants constantSrv = Constants.getAllConstants();

    private static final CustomerInteractionService customerInteractionSrv = CustomerInteractionService.getInstance();
    private static final ContactService contactSrv = ContactService.getInstance();
    private static final AccountContactRelationService accountContactRelationSrv = AccountContactRelationService.getInstance();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    public static AccountService getInstance() {
        return (AccountService)ServiceLocator.getInstance(AccountService.class);
    }

    public List<Account> searchAccount(Account accountObj) {
        List<QueryBuilder.Condition> conditionList = new List<QueryBuilder.Condition>();
        if(String.isNotBlank(accountObj.Name)) {
            conditionList.add(new QueryBuilder.FieldCondition().likeOp(Account.Name, accountObj.Name));
        }
        if(String.isNotBlank(accountObj.Email__c)) {
            conditionList.add(new QueryBuilder.LogicCondition().orOp(new List<QueryBuilder.Condition>{
                new QueryBuilder.FieldCondition().likeOp(Account.Email__c, accountObj.Email__c),
                new QueryBuilder.FieldCondition().likeOp(Account.PersonEmail, accountObj.Email__c)
            }));
        }
        if(String.isNotBlank(accountObj.Key__c)) {
            conditionList.add(new QueryBuilder.FieldCondition().likeOp(Account.Key__c, accountObj.Key__c));
        }
        /*
        if(String.isNotBlank(accountObj.VATNumber__c)) {
            conditionList.add(new QueryBuilder.LogicCondition().orOp(new List<QueryBuilder.Condition>{
                new QueryBuilder.FieldCondition().likeOp(Account.VATNumber__c, accountObj.VATNumber__c),
                new QueryBuilder.FieldCondition().likeOp(Account.NationalIdentityNumber__pc, accountObj.VATNumber__c)
            }));
        }
        */
        if(String.isNotBlank(accountObj.Phone)) {
            conditionList.add(new QueryBuilder.LogicCondition().orOp(new List<QueryBuilder.Condition>{
                new QueryBuilder.FieldCondition().likeOp(Account.Phone, accountObj.Phone),
                new QueryBuilder.FieldCondition().likeOp(Account.PersonMobilePhone, accountObj.Phone)
            }));
        }

        if(conditionList.isEmpty()) {
            return new List<Account>();
        }

        List<Account> accountList = QueryBuilder.getInstance()
            .setObject(Account.SObjectType)
            .setFields(new List<SObjectField>{
                Account.Id, Account.Name, Account.FirstName, Account.LastName, Account.Email__c, Account.PersonEmail,
                Account.Key__c, Account.VATNumber__c, Account.NationalIdentityNumber__pc, Account.Phone,
                Account.PersonIndividualId, Account.PersonMobilePhone, Account.IsPersonAccount, Account.BusinessType__c, Account.Active__c,
                Account.ResidentialCity__c, Account.ResidentialStreetName__c, Account.ResidentialStreetType__c,Account.ResidentialStreetNumber__c
            })
            .setCondition(conditionList, QueryBuilder.AND_OPERATOR)
            .execute();
        return accountList;
    }

    public Account createCustomer(Account accountObj, String interactionId, String role, String recordTypeId) {
        Savepoint savePoint = Database.setSavepoint();
        checkDuplicates(accountObj);

        try {

            Boolean isCustomerAndInterlocutor = false;
            accountObj.RecordTypeId = recordTypeId;
            databaseSrv.insertSObject(accountObj);

            accountObj = accountQuery.findAccount(accountObj.Id);

            Interaction__c interaction = interactionId != null ? interactionQuery.findInteractionById(interactionId) : null;

            if(accountObj.IsPersonAccount) {
                if(interaction != null && interaction.Interlocutor__c != null && interaction.Interlocutor__r.NationalIdentityNumber__c == accountObj.NationalIdentityNumber__pc) {
                    accountObj.PersonIndividualId = interaction.Interlocutor__c;
                    isCustomerAndInterlocutor = true;
                } else {
                    List<Individual> existingIndividuals = individualQuery.findIndividualsByNINumber(accountObj.NationalIdentityNumber__pc);
                    if(existingIndividuals.isEmpty()) {
                        IndividualService.Interlocutor interlocutor = new IndividualService.Interlocutor();
                        interlocutor.firstName = accountObj.FirstName;
                        interlocutor.lastName = accountObj.LastName;
                        interlocutor.nationalId = accountObj.NationalIdentityNumber__pc;
                        Individual newIndividual = IndividualService.getInstance().insertInterlocutor(interlocutor, true);
                        accountObj.PersonIndividualId = newIndividual.Id;
                    } else {
                        accountObj.PersonIndividualId = existingIndividuals[0].Id;
                    }
                }
                databaseSrv.updateSObject(accountObj);
            }

            if(interaction == null) {
                return accountObj;
            }

            Id contactId;

            if(interaction.Interlocutor__c != null) {
                //System.debug('accountObj1111' + accountObj);
                Boolean createAccountContactRelation = true;

                if(accountObj.IsPersonAccount) {
                    if(isCustomerAndInterlocutor) {
                        contactId = accountObj.PersonContactId;
                        createAccountContactRelation = false;
                    } else { //Customer is not the same person as the interlocutor
                        List<Account> individualPersonAccount = accountQuery.listByIndividualId(interaction.Interlocutor__c);
                        if(individualPersonAccount.isEmpty()) {
                            Account newPersonAccount = insertPersonalAccount(interaction.Interlocutor__r);
                            newPersonAccount = accountQuery.findAccount(newPersonAccount.Id);
                            contactId = newPersonAccount.PersonContactId;
                        } else {
                            //Should never exist more than a PersonAccount related to the same Individual. Let's pick the first one from the list:
                            contactId = individualPersonAccount[0].PersonContactId;
                        }
                    }
                } else {
                    List<Contact> contactList = contactQuerySrv.listContactByIndividualId(interaction.Interlocutor__c, false);
                    if(contactList.isEmpty()) {
                        Contact directContact = contactSrv.insertForIndividual(interaction.Interlocutor__r, accountObj.Id);
                        contactId = directContact.Id;
                        createAccountContactRelation = false;
                    } else {
                        //Should never exist more than a Contact (no PersonAccount) related to the same Individual. Let's pick the first one from the list:
                        contactId = contactList[0].Id;
                    }
                }

                if(createAccountContactRelation) {
                    accountContactRelationSrv.addAccountContactRelation(accountObj.Id, contactId, role);
                }
            } else if(accountObj.IsPersonAccount) {
                interaction.Interlocutor__c = accountObj.PersonIndividualId;
                databaseSrv.updateSObject(interaction);
            }

            CustomerInteraction__c customerInteraction = customerInteractionSrv.insertCustomerInteraction(accountObj.Id, interactionId, contactId);

        } catch (Exception exp) {
            Database.rollback(savePoint);
            throw exp;
        }

        return accountObj;
    }

    private void checkDuplicates(Account accountObj) {
        List<Account> accountWithSameKey = new List<Account>();
        String key;
        if (accountObj.IsPersonAccount) {
            key = accountObj.NationalIdentityNumber__pc;
        } else {
           key = accountObj.VATNumber__c;
        }
        accountWithSameKey = accountQuery.listByKey(key);

        if(accountWithSameKey.size() > 0 && accountObj.Id != accountWithSameKey.get(0).Id) {
            String message = '';
            if (accountObj.IsPersonAccount) {
                message = System.Label.NationalIdentityNumber + ' ' + System.Label.AlreadyExist;
            } else {
                message = System.Label.VATNumber + ' ' + System.Label.AlreadyExist;
            }
            throw new WrtsException(message);
        }
    }
    
    public Account updateAccount(Account accountObj){
        checkDuplicates(accountObj);
        accountObj.ResidentialAddressNormalized__c = true;
                databaseSrv.updateSObject(accountObj);
        return accountObj;
    }

    public Account insertPersonalAccount(Individual individualObj) {
        Id personAccountRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(PERSON_RT).getRecordTypeId();
        Account newAcct =  new Account(
            FirstName = individualObj.FirstName,
            LastName = individualObj.LastName,
            NationalIdentityNumber__pc = individualObj.NationalIdentityNumber__c,
            PersonIndividualId = individualObj.Id,
            RecordTypeId = personAccountRtId
        );
        databaseSrv.insertSObject(newAcct);
        return newAcct;
    }

    public Map<String, Object> saveRegistryChange(RegistryChangeWizardCnt.InputData inputData){
        List<Case> caseLists = new List<Case>();
        Map<String, Object> response = new Map<String, Object>();
        Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo registryChangeFast = caseRts.get('RegistryChangeFast');
        Schema.RecordTypeInfo registryChangeSlow = caseRts.get('RegistryChangeSlow');

        Dossier__c dossier = new Dossier__c(
                Account__c = inputData.accountId,
                CompanyDivision__c = inputData.companyDivisionId,
                RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId()
        );
        databaseSrv.insertSObject(dossier);

        if(inputData.fastCase != null){
            inputData.fastCase.RecordTypeId = registryChangeFast.getRecordTypeId();
            inputData.fastCase.AccountId = inputData.accountId;
            inputData.fastCase.Origin  = constantSrv.CASE_DEFAULT_CASE_ORIGIN;
            inputData.fastCase.Dossier__c  = dossier.Id;
            inputData.fastCase.CompanyDivision__c  = dossier.CompanyDivision__c;
            caseLists.add(inputData.fastCase);
        }
        if(inputData.slowCase != null){
            inputData.slowCase.RecordTypeId = registryChangeSlow.getRecordTypeId();
            inputData.slowCase.AccountId = inputData.accountId;
            inputData.slowCase.Origin  = constantSrv.CASE_DEFAULT_CASE_ORIGIN;
            inputData.slowCase.Dossier__c  = dossier.Id;
            inputData.slowCase.CompanyDivision__c  = dossier.CompanyDivision__c;
            inputData.slowCase.AddressAddressNormalized__c  = inputData.residentialAddressForced;
            caseLists.add(inputData.slowCase);
        }

        if(caseLists.size() >0 ){
            databaseSrv.insertSObject(caseLists);
            response.put('caseLists', caseLists);
            response.put('dossierId', dossier.Id);
            response.put('error', false);
        } else {
            response.put('error', true);
        }
        return response;
    }
}