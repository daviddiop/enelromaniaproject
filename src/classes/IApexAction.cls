/**
 * Created by giupastore on 14/05/2020.
 */

public interface IApexAction  {

	/**
      * Execute ApexAction
      * @param args current context object
      * @return Object with some context information
      */
	Object execute(Object args);
}