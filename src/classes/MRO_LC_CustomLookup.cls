public with sharing class MRO_LC_CustomLookup {

    @AuraEnabled
    public static Map<String, Object> initialize (String objectApiName, String queryFields, String queryWhere,
                                                  String filterSearchKeywordField, String filterFields, String primaryDisplayField,
                                                  String secondaryDisplayField, String initialSelectedId) {

        Map<String, Object> returnValue = new Map<String, Object>();
        returnValue.put('error', false);
        try {
            Map<String, Object> returnedQuery = buildQuery('', objectApiName, queryFields, queryWhere, filterSearchKeywordField, filterFields, primaryDisplayField, secondaryDisplayField, '', initialSelectedId);
            String query = '';
            if (returnedQuery.get('error') == false) {
                query = (String) returnedQuery.get('query');
            } else {
                throw new WrtsException('Unable to build the query');
            }
            returnValue.put('query', query);
            SObject initialRecord = Database.query(query);
            returnValue.put('initialRecord', initialRecord);
        } catch (Exception e) {
            System.debug('e.getMessage() ----> ' + e.getMessage());
            System.debug('errorStackTraceString -----> ' + e.getStackTraceString());
            returnValue.put('error', true);
            returnValue.put('errorMessage', e.getMessage());
            returnValue.put('errorStackTraceString', e.getStackTraceString());
        }
        return returnValue;
    }


    @AuraEnabled
    public static Map<String, Object> fetchLookupValues (String searchKeyword, String objectApiName, String queryFields, String queryWhere,
                                                         String filterSearchKeywordField, String filterFields, String primaryDisplayField,
                                                         String secondaryDisplayField, String recordNumberLimit) {
        Map<String, Object> returnValue = new Map<String, Object> ();
        returnValue.put('error', false);
        try {
            String query = '';
            List<sObject> returnList = new List<sObject> ();
            Map<String, Object> returnedQuery = buildQuery(searchKeyword, objectApiName, queryFields, queryWhere, filterSearchKeywordField, filterFields, primaryDisplayField, secondaryDisplayField, recordNumberLimit, '');
            System.debug('query: '+returnedQuery.get('query'));
            if (returnedQuery.get('error') == false) {
                query = (String) returnedQuery.get('query');
            } else {
                throw new WrtsException('Unable to build the query');
            }
            System.debug('query: ' + query);
            returnValue.put('query', query);
            returnList = Database.query(query);
            returnValue.put('returnList', returnList);
        } catch (Exception e) {
            returnValue.put('error', true);
            returnValue.put('errorMessage', e.getMessage());
            returnValue.put('errorStackTraceString', e.getStackTraceString());
        }
        return returnValue;
    }

    private static Map<String, Object> buildQuery (String searchKeyword, String objectApiName, String queryFields, String queryWhere, String filterSearchKeywordField,
                                                   String filterFields, String primaryDisplayField, String secondaryDisplayField,
                                                   String recordNumberLimit, String initialSelectedId) {
        System.debug('searchKeyword ' + searchKeyword + ' objectApiName ' + objectApiName +' queryFields '+ queryFields +' filterSearchKeywordField '+ filterSearchKeywordField +
             ' filterFields ' +filterFields + ' primaryDisplayField ' + primaryDisplayField + ' secondaryDisplayField ' +secondaryDisplayField +
             ' recordNumberLimit ' +recordNumberLimit+ ' initialSelectedId ' + initialSelectedId);
        Map<String, Object> returnValue = new Map<String, Object> ();
        returnValue.put('error', false);
        String query = '';
        Integer recordLimit = 5;
        if (!String.isBlank(recordNumberLimit)) {
            recordLimit = Integer.valueOf(recordNumberLimit);
        }
        try {
            List<String> searchFields = queryFields.split(',');
            for (Integer i = 0; i < searchFields.size(); i++) {
                searchFields[i] = searchFields[i].trim();
            }
            String searchKey = '';
            if (!String.isBlank(primaryDisplayField) && queryFields.contains(primaryDisplayField)) {
                Integer index = queryFields.indexOf(primaryDisplayField);
                queryFields.remove(primaryDisplayField);
            }
            if (!String.isBlank(secondaryDisplayField) && queryFields.contains(secondaryDisplayField)) {
                Integer index = queryFields.indexOf(secondaryDisplayField);
                queryFields.remove(secondaryDisplayField);
            }
            if (!String.isBlank(searchKeyword)) {
                searchKey = '\'%' + searchKeyword + '%\'';
            }
            if (!String.isBlank(queryFields)) {
                //Modified by Giuseppe Mario Pastore on 13/02/2020 because the query resulted in:  SELECT Id, Name, Industry,  FROM Account WHERE Id='0013E00001A0AGbQAN'  ORDER BY Industry ASC LIMIT 5
                queryFields = (String) String.valueOf(queryFields.charAt(queryFields.length()-1)) == ',' ? queryFields.substring(0,queryFields.length()-2) : queryFields;
                System.debug('queryFields after removing final comma -----> ' + queryFields);
                if (!String.isBlank(secondaryDisplayField)) {
                    System.debug('secondary not blank');
                    if (searchFields.contains(secondaryDisplayField)) {
                        //Modified by Giuseppe Mario Pastore, because if primaryDisplayField is blank and is not contained in searchFields,
                        //it will still enters in the subsequent if and will build the string "query = 'SELECT Id, ' + queryFields + ', ' + primaryDisplayField + ' FROM ' + objectApiName;"
                        //with empty primaryDisplay field, resulting in SELECT Id,Name, FROM Account (ERROR)
                        //if (searchFields.contains(primaryDisplayField) &&) { <--- this is the previous code modified to avoid that error
                            if (searchFields.contains(primaryDisplayField) || String.isBlank(primaryDisplayField)) {
                            if (!String.isBlank(searchKey)) {
                                query = 'SELECT Id, ' + queryFields + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField +' LIKE ' + searchKey + ' ';
                                if (!String.isBlank(queryWhere)) {
                                    query += ' AND ' + queryWhere + ' ';
                                }
                            } else {
                                query = 'SELECT Id, ' + queryFields + ' FROM ' + objectApiName;
                            }
                        } else {
                            if (!String.isBlank(searchKey)) {
                                query = 'SELECT Id, ' + queryFields + ', ' + primaryDisplayField + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField +' LIKE ' + searchKey + ' ';
                                if (!String.isBlank(queryWhere)) {
                                    query += ' AND ' + queryWhere + ' ';
                                }
                            } else {
                                query = 'SELECT Id, ' + queryFields + ', ' + primaryDisplayField + ' FROM ' + objectApiName;
                            }
                        }
                    } else {
                        if (!String.isBlank(primaryDisplayField) && searchFields.contains(primaryDisplayField)) {
                            if (!String.isBlank(searchKey)) {
                                query = 'SELECT Id, ' + queryFields + ', ' + secondaryDisplayField + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField + ' LIKE ' + searchKey + ' ';
                                if (!String.isBlank(queryWhere)) {
                                    query += ' AND ' + queryWhere + ' ';
                                }
                            } else {
                                query = 'SELECT Id, ' + queryFields + ', ' + secondaryDisplayField + ' FROM ' + objectApiName;
                            }
                        } else if (!String.isBlank(primaryDisplayField) && !searchFields.contains(primaryDisplayField)) {
                            if (!String.isBlank(searchKey)) {
                                query = 'SELECT Id, ' + queryFields + ', ' + primaryDisplayField + ', ' + secondaryDisplayField + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField + ' LIKE ' + searchKey + ' ';
                                if (!String.isBlank(queryWhere)) {
                                    query += ' AND ' + queryWhere + ' ';
                                }
                            } else {
                                query = 'SELECT Id, ' + queryFields + ', ' + primaryDisplayField + ', ' + secondaryDisplayField + ' FROM ' + objectApiName;
                            }
                        } else {
                            if (!String.isBlank(searchKey)) {
                                query = 'SELECT Id, ' + queryFields + ', ' + secondaryDisplayField + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField + ' LIKE ' + searchKey + ' ';
                                if (!String.isBlank(queryWhere)) {
                                    query += ' AND ' + queryWhere + ' ';
                                }
                            } else {
                                query = 'SELECT Id, ' + queryFields + ', ' + secondaryDisplayField + ' FROM ' + objectApiName;
                            }
                        }
                    }
                } else {
                    if (!String.isBlank(searchKey)) {
                        query = 'SELECT Id, ' + queryFields + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField +' LIKE ' + searchKey + ' ';
                        if (!String.isBlank(queryWhere)) {
                            query += ' AND ' + queryWhere + ' ';
                        }
                    } else {
                        query = 'SELECT Id, ' + queryFields + ' FROM ' + objectApiName;
                    }
                    System.debug('secondary blank ' + query);
                }
            } else {
                if (!String.isBlank(secondaryDisplayField)) {
                    if (!String.isBlank(primaryDisplayField)) {
                        if (!String.isBlank(searchKey)) {
                            query = 'SELECT Id, Name, ' + primaryDisplayField + ', ' + secondaryDisplayField + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField +' LIKE ' + searchKey + ' ';
                            if (!String.isBlank(queryWhere)) {
                                query += ' AND ' + queryWhere + ' ';
                            }
                        } else {
                            query = 'SELECT Id, Name, ' + primaryDisplayField + ', ' + secondaryDisplayField + ' FROM ' + objectApiName;
                        }
                    } else {
                        if (!String.isBlank(searchKey)) {
                            query = 'SELECT Id, Name, ' + secondaryDisplayField + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField +' LIKE ' + searchKey + ' ';
                            if (!String.isBlank(queryWhere)) {
                                query += ' AND ' + queryWhere + ' ';
                            }
                        } else {
                            query = 'SELECT Id, Name, ' + secondaryDisplayField + ' FROM ' + objectApiName;
                        }
                    }
                } else {
                    if (!String.isBlank(primaryDisplayField)) {
                        if (!String.isBlank(searchKey)) {
                            query = 'SELECT Id, Name, ' + primaryDisplayField + ' FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField +' LIKE ' + searchKey + ' ';
                            if (!String.isBlank(queryWhere)) {
                                query += ' AND ' + queryWhere + ' ';
                            }
                        } else {
                            query = 'SELECT Id, Name, ' + primaryDisplayField + ' FROM ' + objectApiName;
                        }
                    } else {
                        if (!String.isBlank(searchKey)) {
                            query = 'SELECT Id, Name FROM ' + objectApiName + ' WHERE ' + filterSearchKeywordField +' LIKE ' + searchKey + ' ';
                            if (!String.isBlank(queryWhere)) {
                                query += ' AND ' + queryWhere + ' ';
                            }
                        } else {
                            query = 'SELECT Id, Name FROM ' + objectApiName;
                        }
                    }
                }
            }
            if (searchKeyword != '') {
                if (filterFields != null && String.isBlank(initialSelectedId)) {
                    List<String> searchKeyWordFieldCell = filterFields.split(',');
                    for (Integer i = 0; i < searchKeyWordFieldCell.size(); i++) {
                        if (i < searchKeyWordFieldCell.size() - 1) {
                            if (!query.contains('WHERE')) {
                                query += ' WHERE ';
                            } else {
                                query += ' OR ';
                            }
                            query += searchKeyWordFieldCell[i] + ' LIKE ' + searchKey + ' OR ';
                        } else {
                            query += searchKeyWordFieldCell[i] + ' LIKE ' + searchKey + ' ';
                        }
                    }
                    if (!String.isBlank(queryWhere)) {
                        query += ' AND ' + queryWhere + ' ';
                    }
                }
            }
            if (!String.isBlank(initialSelectedId)) {
                query += ' WHERE Id=\'' + initialSelectedId +'\' ';
            }
            if (!String.isBlank(primaryDisplayField)) {
                query += ' ORDER BY ' + primaryDisplayField + ' ASC LIMIT ' + recordLimit;
            } else {
                query += ' ORDER BY CreatedDate DESC LIMIT ' + recordLimit;
            }
            System.debug('query from buildQuery: ' + query);
            returnValue.put('query', query);
        } catch (Exception e) {
            returnValue.put('error', true);
            returnValue.put('errorMessage', e.getMessage());
            returnValue.put('errorStackTraceString', e.getStackTraceString());
        }
        return returnValue;
    }
}