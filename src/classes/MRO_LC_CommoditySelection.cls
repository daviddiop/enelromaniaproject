/**
 * @author  Stefano Porcari
 * @since   Jan 30, 2020
 * @desc    Controller class for contract account Commodity Selection
 *
 */

public with sharing class MRO_LC_CommoditySelection extends ApexServiceLibraryCnt {
    private static InteractionService interactionSrv = InteractionService.getInstance();
    public with sharing class getCommodityPicklist extends AuraCallable {
        public override Object perform(final String jsonInput) {
            //Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            List<ApexServiceLibraryCnt.Option> commodityPicklistValues = interactionSrv.listOptions(ContractAccount__c.Commodity__c);
            response.put('options', commodityPicklistValues);
            response.put('error', false);

            return response;
        }
    }
}