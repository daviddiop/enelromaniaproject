/**
 * Created by Boubacar Sow on 15/01/2020.
 */

public with sharing class MRO_UTL_SettingProvider {
    private static final MRO_QR_User userQuery = MRO_QR_User.getInstance();
    User currentUserInfos = userQuery.getUserProfileId(UserInfo.getUserId());
    private static final ProcessDateThresholds__c ProcessDateThresholdsSettings = ProcessDateThresholds__c.getOrgDefaults();
    private static final activityIntegrationTemplate__c activityIntegrationTemplateSetting = activityIntegrationTemplate__c.getOrgDefaults();

    public static Integer getSwOutEffectiveDateHT() {
        Decimal higherThreshold = ProcessDateThresholdsSettings.SwOutEffectiveDateHT__c;
        if (higherThreshold == null) {
            return 0;
        }
        return higherThreshold.intValue();
    }

    public static Integer getSwOutOriginDelay(Boolean isMail) {
        Decimal delay ;
        if (isMail) {
            delay = ProcessDateThresholdsSettings.SwOutOriginMailDelay__c ;
            if (delay == null) {
                return 0;
            }
        }else {
            delay = ProcessDateThresholdsSettings.SwOutOriginOtherDelay__c;
            if (delay == null) {
                return 0;
            }
        }
        return delay.intValue();
    }

    public static Integer getSwOutEffectiveDateLT(Boolean isDistributor) {
        Decimal lower ;
        if (isDistributor) {
            lower = ProcessDateThresholdsSettings.SwOutEffectiveDateEDistrLT__c ;
            if (lower == null) {
                return 0;
            }
        }else {
            lower = ProcessDateThresholdsSettings.SwOutEffectiveDateOtherLT__c;
            if (lower == null) {
                return 0;
            }
        }
        return lower.intValue();
    }
    public static String getTemplateCode() {
        String  templateCode ;
        templateCode = activityIntegrationTemplateSetting.TemplateCode__c;
            if (templateCode == null || templateCode == '') {
                return null;
            }
        return templateCode;
    }

}