/**
 * Created by goudiaby on 08/01/2020.
 * @version 1.0
 * @description
 *          [ENLCRO-325] Channel and Origin Selection - Implementation
 *
 */
@IsTest
private class MRO_LC_OriginChannelSelectionTst {
    @IsTest
    static void OriginChannelOptionsTest() {
        Map<String, String> jsonInput = new Map<String, String>{
                'objectApiName' => 'Dossier__c'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_OriginChannelSelection', 'OriginChannelPicklistOptions', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();

        System.assertEquals(true, result.get('originChannelOptions') != null, 'Why not? ' + result.get('originChannelOptions'));
        System.assertEquals(true, result.get('originChannelEntries') != null, 'Why not? ' + result.get('originChannelEntries'));
    }

    @IsTest
    static void GetPicklistEntriesTest() {
        Map<String, String> jsonInput = new Map<String, String>{};
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_OriginChannelSelection', 'GetPicklistEntries', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('originChannelPicklistEntries') != null, 'Why not? ' + result.get('originChannelPicklistEntries'));
    }

}