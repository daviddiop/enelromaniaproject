/**
 * Created by David Diop on 29.11.2019.
 */
@isTest
public with sharing class ContractCntTst {
    @testSetup
    static void setup() {
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();
        List<Account> listAccount = new list<Account>();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        contract.CompanyDivision__c = companyDivision.Id;
        insert contract;
    }
    @isTest
    static void getContractsByAccountTest() {
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];

        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            LIMIT 1
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Object response = TestUtils.exec(
            'ContractCnt', 'getContractsByAccount', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }
    @isTest
    static void getContractsByAccountIsBlankAccountIdTest() {


        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            LIMIT 1
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' =>'',
            'companyDivisionId' => companyDivision.Id
        };
        Object response = TestUtils.exec(
            'ContractCnt', 'getContractsByAccount', inputJSON, false);

        Test.stopTest();
    }
}