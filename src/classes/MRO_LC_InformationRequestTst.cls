/**
 * Created by Octavian on 4/15/2020.
 */

@IsTest
private class MRO_LC_InformationRequestTst {
    @TestSetup
    static void setup() {
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Dossier__c> dossierList = new List<Dossier__c>();
        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createCustomerCodeSequencer().build();
        insert sequencer;
        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        personAccount.IntegrationKey__c = '000435';
        insert personAccount;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        Account accountDistributor = MRO_UTL_TestDataFactory.account().distributorAccount().build();
        insert accountDistributor;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Dossier__c genericRequestDossier = MRO_UTL_TestDataFactory.dossier().build();
        genericRequestDossier.Account__c = personAccount.Id;
        genericRequestDossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('GenericRequest').getRecordTypeId();
        dossierList.add(genericRequestDossier);

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.ConsumerType__c = 'Residential';
        servicePoint.Trader__c = accountTrader.Id;
        servicePoint.Distributor__c = accountDistributor.Id;
        servicePoint.ENELTEL__c = '123456789';
        insert servicePoint ;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;

        Dossier__c informationRequestDossier = MRO_UTL_TestDataFactory.dossier().build();
        informationRequestDossier.Account__c = personAccount.Id;
        informationRequestDossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('InformationRequest').getRecordTypeId();
        dossierList.add(informationRequestDossier);
        insert dossierList;

        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Status__c = 'Active';
            supply.Market__c = 'Free';
            supply.ServicePoint__c = servicePoint.Id;
            supply.Account__c = listAccount[0].Id;
            supplyList.add(supply);
        }
        insert supplyList;
        servicePoint.CurrentSupply__c = supplyList[0].Id;
        update servicePoint;

        Case caseRecord = new Case();
        caseRecord.AccountId = personAccount.Id;
        caseRecord.Dossier__c = informationRequestDossier.Id;
        caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('InformationRequest').getRecordTypeId();
        insert caseRecord;
    }

    @IsTest
    static void testInitNewDossierWithoutParent() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        String caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('InformationRequest').getRecordTypeId();
        Map<String, String> input = new Map<String, String>{
                'accountId' => account.Id
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_InformationRequest','init', input,true);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.get('dossier'));
        System.assertEquals(account.Id, ((Dossier__c)result.get('dossier')).Account__c);
        System.assertEquals(caseRecordTypeId, result.get('informationRequestRecordTypeId'));
        System.assertEquals(null, ((Dossier__c)result.get('dossier')).Parent__c);

        input = new Map<String, String>{
                'accountId' => ''
        };

        TestUtils.exec('MRO_LC_InformationRequest','init', input,false);
    }

    @IsTest
    static void testInitNewDossierWithParent() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('GenericRequest').getRecordTypeId();
        String caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('InformationRequest').getRecordTypeId();
        Dossier__c genericRequestDossier =  [
                SELECT Id,Name
                FROM Dossier__c
                WHERE RecordTypeId = :dossierRecordTypeId
                LIMIT 1
        ];
        Map<String, String> input = new Map<String, String>{
                'accountId' => account.Id,
                'genericRequestDossierId' => genericRequestDossier.Id
        };

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_InformationRequest','init', input,true);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.get('dossier'));
        System.assertEquals(account.Id, ((Dossier__c)result.get('dossier')).Account__c);
        System.assertEquals(caseRecordTypeId, result.get('informationRequestRecordTypeId'));

        Dossier__c informationRequestDossier =  [
                SELECT Id,Parent__c
                FROM Dossier__c
                WHERE Id = :((Dossier__c)result.get('dossier')).Id
                LIMIT 1
        ];
        System.assertEquals(genericRequestDossier.Id, informationRequestDossier.Parent__c);
    }



    @IsTest
    static void testInitExistingDossierWithCase() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('InformationRequest').getRecordTypeId();
        Dossier__c informationRequestDossier =  [
                SELECT Id,Name
                FROM Dossier__c
                WHERE RecordTypeId = :dossierRecordTypeId
                LIMIT 1
        ];
        Case informationRequestCase =  [
                SELECT Id
                FROM Case
                LIMIT 1
        ];
        String caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('InformationRequest').getRecordTypeId();
        Map<String, String> input = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => informationRequestDossier.Id
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_InformationRequest','init', input,true);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.get('dossier'));
        System.assertEquals(caseRecordTypeId, result.get('informationRequestRecordTypeId'));

        System.assertEquals(informationRequestCase.Id, ((Case)result.get('case')).Id);
    }

    @IsTest
    static void testCancelProcess() {
        String dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('InformationRequest').getRecordTypeId();
        Dossier__c informationRequestDossier =  [
                SELECT Id,Name
                FROM Dossier__c
                WHERE RecordTypeId = :dossierRecordTypeId
                LIMIT 1
        ];
        Map<String, String> input = new Map<String, String>{
                'dossierId' => informationRequestDossier.Id
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_InformationRequest','CancelProcess', input,true);
        Test.stopTest();
        Dossier__c updatedDossier =  [
                SELECT Id,Status__c
                FROM Dossier__c
                WHERE Id = :informationRequestDossier.Id
                LIMIT 1
        ];

        System.assertEquals(updatedDossier.Status__c, 'Canceled');
    }

    @IsTest
    static void testCancelProcessException() {

        Map<String, String> input = new Map<String, String>{
            'dossierId' => 'informationRequestDossierId'
        };
        Test.startTest();
            TestUtils.exec('MRO_LC_InformationRequest','CancelProcess', input,false);
        Test.stopTest();
    }

    @IsTest
    static void testCheckSelectedSupplies(){
        List<Supply__c> listSupplies = [SELECT Id, Status__c, ServicePoint__c FROM Supply__c LIMIT 5];
        List<Id> listSuppliesIds = new List<Id>();
        for (Supply__c s : listSupplies){
            listSuppliesIds.add(s.id);
        }
        Map<String, String> input = new Map<String, String>{
            'selectedSupplyIds' => JSON.serialize(listSuppliesIds)
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InformationRequest','CheckSelectedSupplies', input,true);
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false );
    }

    @IsTest
    static void testcreateCase(){
        List<Supply__c> listSupplies = [ SELECT Id FROM Supply__c LIMIT 5];
        List<Case> listCases = [ SELECT Id FROM Case LIMIT 5];
        Account ac = [SELECT Id FROM Account LIMIT 1];
        Dossier__c dossier = [ SELECT Id FROM Dossier__c LIMIT 1];
            Map<String, String> input = new Map<String, String>{
                'supplies' => JSON.serialize(listSupplies),
                'caseList'=> JSON.serialize(listCases),
                'accountId' =>ac.Id,
                'dossierId' => dossier.Id,
                'channelSelected' => 'Back Office Sales',
                'originSelected' => 'Internal'
            };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InformationRequest','createCase', input,true);
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false );
    }


    @IsTest
    static void testcreateCaseException(){
        List<Supply__c> listSupplies = [ SELECT Id FROM Supply__c LIMIT 5];
        List<Case> listCases = [ SELECT Id FROM Case LIMIT 5];
        Account ac = [SELECT Id FROM Account LIMIT 1];
        Map<String, String> input = new Map<String, String>{
            'supplies' => JSON.serialize(listSupplies),
            'caseList'=> JSON.serialize(listCases),
            'accountId' =>ac.Id,
            'dossierId' => 'dossierId',
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Internal'
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InformationRequest','createCase', input,true);
        Test.stopTest();
        System.assertEquals(true, result.get('error') == true );
    }

    @IsTest
    static void UpdateDossierAndCaseStatusTst(){
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String> input = new Map<String, String>{
                'dossierId' => dossier.Id,
                'originSelected' => 'phone',
                'channelSelected' => 'Back Office Sales'
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_InformationRequest','UpdateDossierAndCaseStatus', input,true);
        Test.stopTest();
    }


    @IsTest
    static void GetAvailableSubTypesTest(){
        Map<String, String> input = new Map<String, String>{
            'informationRequestType' => 'informationRequestType',
            'supplyType' => 'supplyType'
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InformationRequest','GetAvailableSubTypes', input,true);
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false );
    }


    @IsTest
    static void GetAvailableReasonsExceptionTest(){
        Map<String, String> input = new Map<String, String>{
            'caseSubType' => 'caseSubType'
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InformationRequest','GetAvailableReasons', input,true);
        Test.stopTest();
        System.assertEquals(true, result.get('error') == false );
    }

}