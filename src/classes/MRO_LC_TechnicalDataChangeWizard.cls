/**
 * Created by Boubacar Sow  on 18/12/2019.
 *
 * @author Bouabacr Sow
 * @date  18/12/2019
 * @description Class controller for the MRO_LCP_TechnicalDataChangeWizard Component.
 *              [ENLCRO-353] Power Voltage Change  Implementation Wizard.
 */

public with sharing class MRO_LC_TechnicalDataChangeWizard extends ApexServiceLibraryCnt  {
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Connection').getRecordTypeId();
    static List<ApexServiceLibraryCnt.Option> recordTypePicklistValues  = new List<ApexServiceLibraryCnt.Option>();
    static List<ApexServiceLibraryCnt.Option> commodityPicklistValues  = new List<ApexServiceLibraryCnt.Option>();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    static String commodityLabel;
    
    /**
     * Initialize method to get data at the beginning
     */
    public Inherited sharing class InitializeTechnicalDataChange extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String templateId = params.get('templateId');
            String genericRequestId = params.get('genericRequestId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType,'TechnicalDataChange');
            Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
            if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                dossierSrv.updateParent(dossier.Id, genericRequestId);
                dossier.Parent__c = genericRequestId;
            }
            if (String.isNotBlank(dossier.Id)) {
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                Contact contactRecord = new Contact();
                if(cases.size()>0){
                    if(String.isNotBlank(cases[0].ContactId)){
                        contactRecord= contactQuery.getById(cases[0].ContactId);
                        response.put('contactRecord',contactRecord);
                    }
                }
                response.put('caseTile', cases);
            }
            String code = 'TechnicalDataChangeWizardCodeTemplate_1';
            ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
            if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                response.put('templateId', scriptTemplate.Id);
            }

            getCommodityDescribe();
            Account accountRecord = accountQuery.findAccount(accountId);            
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('accountId', accountId);
            response.put('account', accountRecord);            
            response.put('technicalDataChangeRTEle', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('TechnicalDataChange_ELE').getRecordTypeId());
            response.put('technicalDataChangeRTGas', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('TechnicalDataChange_GAS').getRecordTypeId());
            response.put('commodityPicklistValues',commodityPicklistValues);
            response.put('commodityLabel',commodityLabel);
            response.put('genericRequestId', dossier.Parent__c);
            response.put('commodity', commodityPicklistValues.get(0).label);
            response.put('error', false);
            return response;
        }
    }
    
    public static void getCommodityDescribe () {
        Schema.DescribeFieldResult commodityField = Dossier__c.Commodity__c.getDescribe();
        commodityLabel = commodityField.getLabel();
        System.debug('### commodityLabel '+commodityLabel);

        List<Schema.PicklistEntry> commodityFieldPicklist = commodityField.getPicklistValues();
        System.debug('### commodityFieldPicklist '+ commodityFieldPicklist);
        for (Schema.PicklistEntry pickListVal : commodityFieldPicklist) {
            if (System.Label.Electric == pickListVal.getLabel()) {
                System.debug('### pickListVal ' + pickListVal);
                commodityPicklistValues.add(new ApexServiceLibraryCnt.Option(pickListVal.getLabel(), pickListVal.getValue()));
            }
        }
    }    
    
    /**
     * Update the CaseList
     */
    public Inherited sharing class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String contactId = params.get('contactId');
            String companyDivisionId;

            if (oldCaseList[0] != null) {
                companyDivisionId = oldCaseList[0].CompanyDivision__c;
            }

            Savepoint sp = Database.setSavepoint();
            try {
                caseSrv.setNewOnCaseAndDossierPowerVoltage(oldCaseList, dossierId, companyDivisionId, contactId);
            }
            catch (Exception e) {
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMessage', e.getMessage());
            }
            response.put('error', false);
            return response;
        }
    }

    /**
     * Cancel process change status Dossier and Cas to cancel.
     */
    public Inherited sharing class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');

            caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId, 'RC010');
            response.put('error', false);
            return response;
        }
    }

    /**
     * search the supply selected
     */
    public Inherited sharing class getSelectedSypply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');

            if (String.isBlank(supplyId)) {
                throw new WrtsException('Supply - ' + System.Label.MissingId);
            }
            try{
                Supply__c supplyRecord = supplyQuery.getById(supplyId);
                response.put('error', false);
                response.put('selectedSupply', supplyRecord);
            }catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    /**
* @author  DAVID DIOP
* @description update dossier
* @date 28/01/2020
* @param DossierId,channelSelected,originSelected
*
*/
    public Inherited sharing class UpdateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String channelSelected = params.get('channelSelected');
            String originSelected = params.get('originSelected');
            dossierSrv.updateDossierChannel(dossierId,channelSelected,originSelected);
            response.put('error', false);
            return response;
        }
    }


    public with sharing class updateCommodityToDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String commodity = params.get('commodity');
            if (String.isBlank(dossierId)) {
                throw new WrtsException('DossierId - ' + System.Label.MissingId);
            }
            dossierSrv.updateCommodityOnDossier(dossierId,commodity);
            response.put('error', false);
            return response;
        }
    }    
    
}