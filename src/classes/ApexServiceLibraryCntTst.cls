@isTest
public class ApexServiceLibraryCntTst {

    @testSetup
    static void setup() {
        ApexServiceLibraryCnt.Option myOption = new ApexServiceLibraryCnt.Option('label','value');
    }

    @isTest
    static void callTest(){
        String className = 'SupplyCnt';
        String methodName = 'getSuppliesByServicePoint';
        String data = 'inputData';
        Map<String, String> input = new Map<String, String>{
            'jsonInput' => data 
        };
        String jsonInput = JSON.serialize(input);
        Test.startTest();
        ApexServiceLibraryCnt.Response response = ApexServiceLibraryCnt.call(className, methodName, jsonInput);
        Test.stopTest();
        System.assertEquals(false, response.getIsError());
    }

    @isTest
    static void emptyCallTest(){
        String className = '';
        String methodName = '';
        String jsonInput = '';
        Test.startTest();
        ApexServiceLibraryCnt.Response response = ApexServiceLibraryCnt.call(className, methodName, jsonInput);
        Test.stopTest();
        System.assertEquals(true, response.getIsError());        
    }     

    @isTest
    static void cacheableCallTest(){
        String className = 'SupplyCnt';
        String methodName = 'getSuppliesByServicePoint';
        String data = 'inputData';
        Map<String, String> input = new Map<String, String>{
            'jsonInput' => data 
        };
        Test.startTest();
        ApexServiceLibraryCnt.Response response = ApexServiceLibraryCnt.cacheableCall(className, methodName, input);
        Test.stopTest();
        System.assertEquals(false, response.getIsError());
    }  

    @isTest
    static void notCacheableCallTest(){
        String className = 'SupplyCnt';
        String methodName = 'getSuppliesByServicePoint';
        String data = 'inputData';
        Map<String, String> input = new Map<String, String>{
            'jsonInput' => data 
        };
        Test.startTest();
        ApexServiceLibraryCnt.Response response = ApexServiceLibraryCnt.notCacheableCall(className, methodName, input);
        Test.stopTest();
        System.assertEquals(false, response.getIsError());
    } 

    @isTest
    static void asMapTest(){
        String data = 'inputData';
        Map<String, String> input = new Map<String, String>{
            'jsonInput' => data 
        };
        String jsonInput = JSON.serialize(input);
        Test.startTest();
        Map<String, String> response = ApexServiceLibraryCnt.asMap(jsonInput);
        Test.stopTest();
        System.assertNotEquals(null, response);
    } 

    @isTest
    static void asMapBlankTest(){
        String jsonInput = '';
        Test.startTest();
        Map<String, String> response = ApexServiceLibraryCnt.asMap(jsonInput);
        Test.stopTest();
        System.assertNotEquals(null, response);        
    }
}