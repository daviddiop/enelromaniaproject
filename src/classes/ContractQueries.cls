public with sharing class ContractQueries {

    public static ContractQueries getInstance() {
        return (ContractQueries) ServiceLocator.getInstance(ContractQueries.class);
    }

    // commented - unused code
    /*public Contract getById(Id contractId) {
        return [
            SELECT Id, Status 
            FROM Contract
            WHERE Id =:contractId];        
    }
*/

    public List<Contract> getActiveContractsByAccount(String accountId, String companyDivisionId) {
        return [
                SELECT Id,ContractNumber,Status,
                        CompanyDivision__c,StartDate,EndDate,
                        CustomerSignedDate
                FROM Contract
                WHERE AccountId = :accountId AND Status = 'Activated' AND CompanyDivision__c = :companyDivisionId
        ];
    }

    public Map<Id, Contract> getByIds(Set<Id> contractIds) {
        return new Map<Id, Contract>([
                SELECT Id,CompanyDivision__c
                FROM Contract
                WHERE Id IN :contractIds
        ]);
    }

}