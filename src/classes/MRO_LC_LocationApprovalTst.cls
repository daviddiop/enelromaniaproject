/**
 * Created by David DIOP on 10.02.2020.
 */
@IsTest
public with sharing class MRO_LC_LocationApprovalTst {
    @TestSetup
    static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SwitchIn_ELE').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, '');
        insert phaseDI010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        ServiceSite__c serviceSite = TestDataFactory.serviceSite().createServiceSite().build();
        serviceSite.Account__c = listAccount[0].Id;
        insert serviceSite;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Status__c = 'Draft';
        dossier.RequestType__c = 'MeterChange';
        dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            //caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.ReferenceDate__c = System.today();
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterChangeEle').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'LocationApprovalTemplate_1';
        insert scriptTemplate;
    }

    @IsTest
    public static void initialize(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        ScriptTemplate__c scriptTemplate = [
            SELECT Id, Code__c
            FROM ScriptTemplate__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_LocationApproval', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
                'accountId' => '',
                'dossierId' => dossier.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        response = TestUtils.exec(
                'MRO_LC_LocationApproval', 'initialize', inputJSON, false);
        Test.stopTest();
    }

    @IsTest
    public static void createServiceSiteTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        ServiceSite__c serviceSite = TestDataFactory.serviceSite().createServiceSite().build();
        serviceSite.Account__c = account.Id;
        MRO_LC_locationApproval.serviceSiteInput inputServiceSite = new MRO_LC_locationApproval.serviceSiteInput();
        inputServiceSite.serviceSite = serviceSite ;
        String inputServiceSiteString = JSON.serialize(inputServiceSite);
        Map<String, String > inputJSON = new Map<String, String>{
                'serviceSite' => inputServiceSiteString
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_LocationApproval', 'createServiceSite', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }


    @IsTest
    public static void createServiceSiteExcepTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        ServiceSite__c serviceSite = TestDataFactory.serviceSite().createServiceSite().build();
        serviceSite.Account__c = account.Id;
        MRO_LC_locationApproval.serviceSiteInput inputServiceSite = new MRO_LC_locationApproval.serviceSiteInput();
        /*inputServiceSite.serviceSite = serviceSite ;*/
        String inputServiceSiteString = JSON.serialize(inputServiceSite);
        Map<String, String > inputJSON = new Map<String, String>{
                'serviceSite' => inputServiceSiteString
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_LocationApproval', 'createServiceSite', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    public static void createCaseTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Case caseRecord = [
                SELECT Id,ReferenceDate__c
                FROM Case
                LIMIT  1
        ];
        ServiceSite__c serviceSite = [
                SELECT Id
                FROM ServiceSite__c
                LIMIT  1
        ];
        /*BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
                LIMIT  1
        ];*/
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        String originSelected = 'Email';
        String channelSelected = 'Back Office Sales';
        String caseListString = JSON.serialize(caseRecord);
        Map<String, String > inputJSON = new Map<String, String>{
                'caseFields' => caseListString,
                'serviceSiteId' => serviceSite.Id,
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'serviceSiteId' => '',
                'origin' => originSelected,
                'channel' => channelSelected
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_LocationApproval', 'createCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    public static void createCaseExceptionTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Case caseRecord = [
                SELECT Id,ReferenceDate__c
                FROM Case
                LIMIT  1
        ];
        ServiceSite__c serviceSite = [
                SELECT Id
                FROM ServiceSite__c
                LIMIT  1
        ];
        /*BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
                LIMIT  1
        ];*/
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        String originSelected = 'Email';
        String channelSelected = 'Back Office Sales';
        String caseListString = JSON.serialize(caseRecord);
        Map<String, String > inputJSON = new Map<String, String>{
                'caseFields' => caseListString,
                'serviceSiteId' => serviceSite.Id,
                'accountId' => dossier.Id,
                'dossierId' => account.Id,
                'serviceSiteId' => '',
                'origin' => originSelected,
                'channel' => channelSelected
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_LocationApproval', 'createCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    public static void saveDraftRequestTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Case caseRecord = [
            SELECT Id,ReferenceDate__c
            FROM Case
            LIMIT  1
        ];
        ServiceSite__c serviceSite = [
            SELECT Id
            FROM ServiceSite__c
            LIMIT  1
        ];
        BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
                LIMIT  1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        String originSelected = 'Email';
        String channelSelected = 'Back Office Sales';
        String caseListString = JSON.serialize(caseRecord);
        Map<String, String > inputJSON = new Map<String, String>{
            'caseFields' => caseListString,
            'serviceSiteId' => serviceSite.Id,
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'billingId' => billingProfile.Id,
            'serviceSiteId' => '',
            'origin' => originSelected,
            'channel' => channelSelected
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_LocationApproval', 'saveDraftRequest', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    public static void saveDraftRequestExceptionTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Case caseRecord = [
            SELECT Id,ReferenceDate__c
            FROM Case
            LIMIT  1
        ];
        ServiceSite__c serviceSite = [
            SELECT Id
            FROM ServiceSite__c
            LIMIT  1
        ];
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
            LIMIT  1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        String originSelected = 'Email';
        String channelSelected = 'Back Office Sales';
        String caseListString = JSON.serialize(caseRecord);
        Map<String, String > inputJSON = new Map<String, String>{
            'caseFields' => caseListString,
            'serviceSiteId' => serviceSite.Id,
            'accountId' => account.Id,
            'dossierId' => billingProfile.Id,
            'billingId' => dossier.Id,
            'serviceSiteId' => '',
            'origin' => originSelected,
            'channel' => channelSelected
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_LocationApproval', 'saveDraftRequest', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    public static void cancelProcessTest() {
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_LocationApproval', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
}