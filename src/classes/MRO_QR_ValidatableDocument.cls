/**
 * @author  Stefano Porcari
 * @since   Dec 24, 2019
 * @desc   Query class for ValidatableDocument__c object
 * 
 */
public with sharing class MRO_QR_ValidatableDocument {
    public static MRO_QR_ValidatableDocument getInstance() {
        return new MRO_QR_ValidatableDocument();
    }
    public List<ValidatableDocument__c> findByDossierId(String dossierId) {
        return [
                SELECT Id,DocumentBundleItem__c,FileMetadata__c,FileMetadata__r.DiscoFileId__c,Dossier__c,Case__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossierId
        ];
    }

    public List<ValidatableDocument__c> findByCaseId(Id caseId) {
        return [
                SELECT Id,DocumentBundleItem__c,FileMetadata__c,FileMetadata__r.DiscoFileId__c,Dossier__c,Case__c
                FROM ValidatableDocument__c
                WHERE Case__c = :caseId
        ];
    }

    public ValidatableDocument__c getDMSArchivableDocumentByFileMetadataId(Id fileMetadataId) {
        List<ValidatableDocument__c> vd = [SELECT Id FROM ValidatableDocument__c
                                           WHERE FileMetadata__c = :fileMetadataId
                                                AND FileMetadata__r.DiscoFileId__c = NULL
                                                AND DocumentBundleItem__r.ArchiveOnDisCo__c = TRUE
                                           LIMIT 1];
        return vd.isEmpty() ? null : vd[0];
    }

    public List<ValidatableDocument__c> findNonValidatableByDossierId(Id dossierId) {
        return [
                SELECT Id,DocumentBundleItem__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossierId AND FileMetadata__c = NULL
        ];
    }

    public List<ValidatableDocument__c> findNonValidatableByCaseId(Id caseId) {
        return [
                SELECT Id,DocumentBundleItem__c
                FROM ValidatableDocument__c
                WHERE Case__c = :caseId AND FileMetadata__c = NULL
        ];
    }

    public ValidatableDocument__c getById(Id validatableDocumentId) {
        return [
                SELECT Id,DocumentBundleItem__c,FileMetadata__c,Dossier__c,Case__c,ValidationActivity__c
                FROM ValidatableDocument__c
                WHERE Id = :validatableDocumentId
        ];
    }

    public List<ValidatableDocument__c> getByIds(Set<Id> validatableDocumentIds) {
        return [
                SELECT Id,DocumentBundleItem__c,FileMetadata__c,ValidationActivity__c,Dossier__c,Case__c,CustomerSignedDate__c,
                       Dossier__r.Opportunity__r.ContractSignedDate__c, Dossier__r.Opportunity__r.Contract.CustomerSignedDate,
                       Dossier__r.Opportunity__r.Contract.Status
                FROM ValidatableDocument__c
                WHERE Id IN :validatableDocumentIds
        ];
    }

    public List<ValidatableDocument__c> listByValidationActivities(Set<Id> validationActivityIds) {
        return [SELECT Id, ValidationActivity__c, FileMetadata__c, CustomerSignedDate__c, DocumentBundleItem__c,
                       DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c, DocumentBundleItem__r.Mandatory__c
                FROM ValidatableDocument__c WHERE ValidationActivity__c IN :validationActivityIds];
    }

    public ValidatableDocument__c getNotSignedContractValidatableDocumentForDossier(Id dossierId) {
        List<ValidatableDocument__c> vdList = [SELECT Id, ValidationActivity__c, ValidationActivity__r.wrts_prcgvr__Status__c,
                                                      DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c
                                               FROM ValidatableDocument__c
                                               WHERE Dossier__c = :dossierId AND ValidationActivity__r.wrts_prcgvr__IsClosed__c = FALSE
                                                    AND DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c IN ('CTR_ELEC', 'CTR_GAZ', 'CTR_VAS')
                                                    AND CustomerSignedDate__c = NULL
                                               LIMIT 1];
        return vdList.isEmpty() ? null : vdList[0];
    }

    public List<ValidatableDocument__c> listUncheckedValidatableDocumentForDossier(Id dossierId) {
        return [
                SELECT Id, ValidationActivity__c, ValidationActivity__r.wrts_prcgvr__Status__c,
                        DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c, CustomerSignedDate__c,
                        DocumentBundleItem__r.ArchiveOnDisCo__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossierId AND ValidationActivity__r.wrts_prcgvr__IsClosed__c = FALSE
                AND FileMetadata__c = NULL
        ];
    }

    public Map<Id, ValidatableDocument__c> listDiscoValidatableDocumentForDossier(Set<Id> dossierIds) {
        return new Map<Id, ValidatableDocument__c>([
                SELECT Id, ValidationActivity__c, ValidationActivity__r.wrts_prcgvr__Status__c,
                        DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c, CustomerSignedDate__c,
                        DocumentBundleItem__r.ArchiveOnDisCo__c,DocumentBundleItem__r.Mandatory__c,
                        FileMetadata__c,FileMetadata__r.DiscoFileId__c
                FROM ValidatableDocument__c
                WHERE Dossier__c IN :dossierIds AND ValidationActivity__r.wrts_prcgvr__IsClosed__c = FALSE
                AND DocumentBundleItem__r.ArchiveOnDisCo__c = TRUE
        ]);
    }

    // BG commented - unused code
    /**
     * Retrieve a Validatable record by  documentBundleItemId and Dossier
     *
     * @param documentBundleItemId
     * @param dossierId
     *
     * @return ValidatableDocument__c record
     */
    /*public ValidatableDocument__c findByDocumentBundleItemIdIdAndDossierId(String documentBundleItemId, String dossierId) {
        List<ValidatableDocument__c> validatableDocumentList = [
                SELECT Id, DocumentBundleItem__c,Dossier__c,
                        FileMetadata__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossierId AND DocumentBundleItem__c = :documentBundleItemId
                LIMIT 1
        ];
        return validatableDocumentList.isEmpty() ? null : validatableDocumentList.get(0);
    }*/

    public List<ValidatableDocument__c> findByCaseIdAndDocumentType(List<String> caseIdList,
                                                                        List<String> documentTypeCodeList) {
        return [
                SELECT Id,DocumentBundleItem__c,FileMetadata__c,FileMetadata__r.DiscoFileId__c,Dossier__c,Case__c,
                       DocumentBundleItem__r.DocumentType__c, DocumentBundleItem__r.TemplateClass__c,
                        DocumentBundleItem__r.Archive__c, FileMetadata__r.ExternalId__c
                FROM ValidatableDocument__c
                WHERE Case__c IN :caseIdList AND
                    DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c IN :documentTypeCodeList
        ];
    }
}