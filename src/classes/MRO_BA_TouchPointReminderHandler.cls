/**
 * Created by tommasobolis on 19/07/2020.
 */

public with sharing class MRO_BA_TouchPointReminderHandler implements IApexAction{

    private static MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();
    private static MRO_SRV_Dossier dossierservice  = MRO_SRV_Dossier.getInstance();
    public Object execute(Object args) {

        Savepoint sp = Database.setSavepoint();
        try {

            List<CustomReminder__c> reminders = (List<CustomReminder__c>)((Map<String, Object>)args).get('reminders');
            System.debug('MRO_BA_TouchPointReminderHandler.execute - reminders: ' + reminders);
            if(!reminders.isEmpty()) {

                if(reminders[0].Code__c.equals(constantsUtl.CUSTOMREMCONFIGCODE_IIDOCUMENTUPLOADREMINDER)) {

                    processDocumentUploadReminders(reminders);

                } else if(reminders[0].Code__c.equals(constantsUtl.CUSTOMREMCONFIGCODE_IIDOCUMENTUPLOADTOKENEXPIRED)) {

                    processDocumentUploadTokensExpired(reminders);

                } else if(reminders[0].Code__c.equals(constantsUtl.CUSTOMREMCONFIGCODE_IICONTRACTACCEPTANCEREMINDER)) {

                    processContractAcceptanceReminders(reminders);

                } else if(reminders[0].Code__c.equals(constantsUtl.CUSTOMREMCONFIGCODE_IICONTRACTACCEPTANCETOKENEXPIRED)) {

                    processContractAcceptanceTokensExpired(reminders);
                }
                update reminders;
            }
        } catch (Exception e) {

            Database.rollback(sp);
            System.debug('MRO_BA_TouchPointReminderHandler.execute - Exception occured: ' + e.getMessage() + ' ' +e.getStackTraceString());
        }
        return null;
    }

    private void processDocumentUploadReminders(List<CustomReminder__c> reminders) {
        Set<Id> dossierIds = new Set<Id>();
        System.debug('MRO_BA_TouchPointReminderHandler.processDocumentUploadReminders - ENTER');
        for (CustomReminder__c reminder : reminders) {

            reminder.Status__c = 'Executed';
            Id recordId = reminder.RecordId__c;
            String ObjectName = recordId.getSobjectType().getDescribe().getName();
            if (ObjectName == 'Dossier__c') {
                dossierIds.add(reminder.RecordId__c);
            }
        }
        if (!dossierIds.isEmpty()) {
            dossierservice.notifyInternetInterfaceDocumentUploadTouchPointReminder(dossierIds);
        }
    }

    private void processDocumentUploadTokensExpired(List<CustomReminder__c> reminders) {

        Set<Id> dossierIds = new Set<Id>();
        System.debug('MRO_BA_TouchPointReminderHandler.processDocumentUploadTokensExpired - ENTER');
        for(CustomReminder__c reminder : reminders) {

            reminder.Status__c = 'Executed';
            Id recordId = reminder.RecordId__c;
            String ObjectName = recordId.getSobjectType().getDescribe().getName();
            if (ObjectName == 'Dossier__c') {
                dossierIds.add(reminder.RecordId__c);
            }
        }
        if (!dossierIds.isEmpty()) {
            dossierservice.notifyInternetInterfaceDocumentUploadTouchPointExpired(dossierIds);
        }
    }

    private void processContractAcceptanceReminders(List<CustomReminder__c> reminders) {

        System.debug('MRO_BA_TouchPointReminderHandler.processContractAcceptanceReminders - ENTER');
        for(CustomReminder__c reminder : reminders) {

            reminder.Status__c = 'Executed';
        }
    }

    private void processContractAcceptanceTokensExpired(List<CustomReminder__c> reminders) {

        System.debug('MRO_BA_TouchPointReminderHandler.processContractAcceptanceTokensExpired - ENTER');
        for(CustomReminder__c reminder : reminders) {

            reminder.Status__c = 'Executed';
        }
    }
}