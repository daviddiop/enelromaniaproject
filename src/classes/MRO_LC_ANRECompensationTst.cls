/**
 * Created by Vlad Mocanu on 02/04/2020.
 */

@IsTest
public with sharing class MRO_LC_ANRECompensationTst {

    @TestSetup
    private static void setup() {
        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createCustomerCodeSequencer().build();
        insert sequencer;

        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert personAccount;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().build();
        supply.Account__c = personAccount.Id;
        insert supply;

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().build();
        dossier.Status__c = 'Draft';
        insert dossier;
        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = personAccount.Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;

        List<Case> caseList = new List<Case>();
        Case parentCase = new Case(
                Origin = 'Email',
                Channel__c = 'Back Office Sales',
                StartDate__c = Date.today(),
                Supply__c = supply.Id);
        insert parentCase;

        for (Integer i = 0; i <= 3; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().build();
            caseRecord.Status = 'Draft';
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ANRECompensation').getRecordTypeId();
            if (i == 1) {
                caseRecord.ParentId = parentCase.Id;
            }
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    public static void initializeNoDossierTest() {
        Account account = [SELECT Id, Name FROM Account LIMIT 1];
        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => ''
        };
        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, result.get('accountId') == account.Id);
        System.assertEquals(true, result.get('dossierId') != null);

        Test.stopTest();
    }

    @IsTest
    public static void initializeWithDossierTest() {
        Account account = [SELECT Id, Name FROM Account LIMIT 1];
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, result.get('accountId') == account.Id);
        System.assertEquals(true, result.get('dossierId') == dossier.Id);
        System.assertEquals(true, result.get('cases') != null);

        Test.stopTest();
    }

    @IsTest
    public static void initializeWithNoAccount() {
        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == true);
        System.assertEquals(true, result.get('errorMsg') == 'Account - Missing Id');

        Test.stopTest();
    }

    @IsTest
    public static void cancelProcessBothInputsTest() {
        Dossier__c oldDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];
        List<Case> oldCases = [SELECT Id, Status FROM Case WHERE Dossier__c != NULL];
        Integer numberOfOldDraftStatus = 0;

        for (Integer i = 0; i < oldCases.size(); i++) {
            String oldStatus = oldCases[i].Status;
            if (oldStatus == 'Draft') {
                numberOfOldDraftStatus++;
            }
        }

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => oldDossier.Id,
                'caseList' => JSON.serialize(oldCases)
        };

        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c newDossier = [SELECT Id, Status__c FROM Dossier__c WHERE Id = :oldDossier.Id];
        List<Case> newCases = [SELECT Id, Status FROM Case WHERE Dossier__c = :oldDossier.Id];
        Integer numberOfNewDraftStatus = 0;

        for (Integer i = 0; i < newCases.size(); i++) {
            String newStatus = newCases[i].Status;
            if (newStatus == 'Canceled') {
                numberOfNewDraftStatus++;
            }
        }

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, oldDossier.Status__c == 'Draft');
        System.assertEquals(true, numberOfOldDraftStatus == oldCases.size());
        System.assertEquals(true, newDossier.Status__c == 'Canceled');
        System.assertEquals(true, numberOfNewDraftStatus == newCases.size());

        Test.stopTest();
    }

    @IsTest
    public static void cancelProcessNoCaseTest() {
        Dossier__c oldDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => oldDossier.Id,
                'caseList' => ''
        };

        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c newDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, oldDossier.Status__c == 'Draft');
        System.assertEquals(true, newDossier.Status__c == 'Canceled');

        Test.stopTest();
    }

    @IsTest
    public static void cancelProcessNoDossierTest() {
        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => '',
                'cancelReason' => '',
                'detailsReason' => ''
        };

        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c newDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];

        System.assertEquals(true, result.get('error') == true);
        System.assertEquals(true, newDossier.Status__c == 'Draft');

        Test.stopTest();
    }

    @IsTest
    public static void createCaseTest() {
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Account account = [SELECT Id, Name FROM Account LIMIT 1];
        List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE Dossier__c = :dossier.Id];
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';
        Map<String, String> anreCompensationRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('ANRECompensation');

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'accountId' => account.Id,
                'supplyId' => supply.Id,
                'selectedOrigin' => selectedOrigin,
                'selectedChannel' => selectedChannel,
                'caseList' => JSON.serialize(caseList)
        };

        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'createCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Case resultCase = ((List<Case>) result.get('cases'))[0];

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, resultCase.RecordTypeId == anreCompensationRecordTypes.get('ANRECompensation'));
        System.assertEquals(true, resultCase.Dossier__c == dossier.Id);
        System.assertEquals(true, resultCase.Origin == selectedOrigin);
        System.assertEquals(true, resultCase.Channel__c == selectedChannel);
        System.assertEquals(true, resultCase.Supply__c == supply.Id);
        System.assertEquals(true, resultCase.AccountId == account.Id);
        System.assertEquals(true, resultCase.Status == 'Draft');
        System.assertEquals(true, resultCase.Phase__c == 'RE010');

        Test.stopTest();
    }

    @IsTest
    public static void createCaseMissingInputsTest() {
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Account account = [SELECT Id, Name FROM Account LIMIT 1];
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';

        Test.startTest();

        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'accountId' => account.Id,
                'supplyId' => supply.Id,
                'selectedOrigin' => selectedOrigin,
                'selectedChannel' => selectedChannel
        };

        Set<String> inputKeySet = inputJSON.keySet();

        for (String inputKey : inputKeySet) {
            Map<String, String> newMap = inputJSON;
            String inputKeyValue = newMap.get(inputKey);
            newMap.put(inputKey, '');

            Object response = TestUtils.exec(
                    'MRO_LC_ANRECompensation', 'createCase', newMap, true);
            Map<String, Object> result = (Map<String, Object>) response;
            newMap.put(inputKey, inputKeyValue);

            System.assertEquals(true, result.get('error') == true);
        }
        Test.stopTest();
    }

    @IsTest
    public static void saveCaseTest() {
        String dossierId = [SELECT Id FROM Dossier__c LIMIT 1].Id;
        List<Case> caseList = [SELECT Id FROM Case];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'caseList' => JSON.serialize(caseList)
        };

        Object response = TestUtils.exec(
                'MRO_LC_ANRECompensation', 'saveCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c resultDossier = (Dossier__c) result.get('dossier');
        List<Case> savedCases = [SELECT Id, Status, Dossier__c FROM Case];

        System.assertEquals(true, resultDossier.Id == dossierId);
        for (Case savedCase : savedCases) {
            System.assertEquals(true, result.get('error') == false);
            System.assertEquals(true, resultDossier.Status__c == 'New');
            System.assertEquals(true, savedCase.Status == 'New');
            System.assertEquals(true, savedCase.Dossier__c == dossierId);
        }
        Test.stopTest();
    }

    @IsTest
    public static void saveCaseMissingInputsTest() {
        List<Case> caseList = [SELECT Id FROM Case];

        Test.startTest();

        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => 'dossierId',
                'caseList' => JSON.serialize(caseList)
        };
        Set<String> inputKeySet = inputJSON.keySet();

        for (String inputKey : inputKeySet) {
            Map<String, String> newMap = inputJSON;
            String inputKeyValue = newMap.get(inputKey);
            newMap.put(inputKey, '');

            Object response = TestUtils.exec(
                    'MRO_LC_ANRECompensation', 'saveCase', newMap, true);
            Map<String, Object> result = (Map<String, Object>) response;
            newMap.put(inputKey, inputKeyValue);

            System.assertEquals(true, result.get('error') == true);
        }
        Test.stopTest();
    }

    @IsTest
    public static void getSupplyStatusByIdTest() {
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => supply.Id
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'getSupplyStatusById', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();
    }

    @IsTest
    public static void getSupplyStatusByIdBadIdTest() {
        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => ''
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'getSupplyStatusById', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    public static void getSupplyStatusByIdNoStatusTest() {
        Map<String, String> inputJSON = new Map<String, String>{
                'supplyId' => 'notASupplyId'
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'getSupplyStatusById', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    public static void SetCaseDatesFromParentCaseWithParentCaseTest() {
        Case childCase = [SELECT Id, ParentId FROM Case WHERE ParentId != NULL][0];
        Case parentCase = [SELECT Id FROM Case WHERE Id =: childCase.ParentId];
        Map<String, String> inputJSON = new Map<String, String>{
                'parentCaseId' => parentCase.Id
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'SetCaseDatesFromParentCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, result.get('createdDate') != null );
        System.assertEquals(true, result.get('findingDate') != null );

        Test.stopTest();
    }

    @IsTest
    public static void SetCaseDatesFromParentCaseExceptionTest() {
        Map<String, String> inputJSON = new Map<String, String>{
                'parentCaseId' => 'parentCaseId'
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'SetCaseDatesFromParentCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    public static void SetCaseDatesFromParentCaseNoParentCaseTest() {
        Map<String, String> inputJSON = new Map<String, String>{
                'parentCaseId' => ''
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'SetCaseDatesFromParentCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        System.assertEquals(true, result.get('createdDate') == null );
        System.assertEquals(true, result.get('findingDate') == null );

        Test.stopTest();
    }

    @IsTest
    public static void deleteCanceledCaseTest() {
        Case newCase = [SELECT Id, ParentId FROM Case WHERE Dossier__c != NULL][0];

        Map<String, String> inputJSON = new Map<String, String>{
                'caseId' => newCase.Id
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'DeleteCanceledCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();
    }

    @IsTest
    public static void deleteCanceledCaseNoCaseTest() {

        Map<String, String> inputJSON = new Map<String, String>{
                'caseId' => ''
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'DeleteCanceledCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    public static void deleteCanceledCaseExceptionTest() {

        Map<String, String> inputJSON = new Map<String, String>{
                'caseId' => 'badCaseId'
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'DeleteCanceledCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    public static void setChannelAndOriginTest() {
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';
        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'selectedChannel' => selectedChannel,
                'selectedOrigin' => selectedOrigin
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'SetChannelAndOrigin', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();
    }

    @IsTest
    public static void setChannelAndOriginExceptionTest() {
        String dossierId = 'badDossierId';
        String selectedOrigin = 'badOrigin';
        String selectedChannel = 'badChannel';
        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'selectedChannel' => selectedChannel,
                'selectedOrigin' => selectedOrigin
        };

        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'SetChannelAndOrigin', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    public static void setChannelAndOriginNoInputsTest() {
        String dossierId = 'badDossierId';
        String selectedOrigin = 'badOrigin';
        String selectedChannel = 'badChannel';
        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'selectedChannel' => selectedChannel,
                'selectedOrigin' => selectedOrigin
        };

        Set<String> inputKeySet = inputJSON.keySet();
        Test.startTest();

        for (String inputKey : inputKeySet) {
            Map<String, String> newMap = inputJSON;
            String inputKeyValue = newMap.get(inputKey);
            newMap.put(inputKey, '');

            Object response = TestUtils.exec('MRO_LC_ANRECompensation', 'SetChannelAndOrigin', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            newMap.put(inputKey, inputKeyValue);

            System.assertEquals(true, result.get('error') == true);
        }
        Test.stopTest();
    }

}