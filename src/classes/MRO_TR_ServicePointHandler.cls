/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 13, 2020
 * @desc    
 * @history 
 */

global with sharing class MRO_TR_ServicePointHandler implements TriggerManager.ISObjectTriggerHandler {
    public static Boolean FIRST_TIME_UPDATE = true;

    public static Map<Id, Sequencer__c> ENELTEL_SEQUENCERS;

    private DatabaseService databaseSrv = DatabaseService.getInstance();
    private MRO_SRV_ServicePoint servicePointService = MRO_SRV_ServicePoint.getInstance();

    private MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();

    public void beforeInsert() {
        for (ServicePoint__c sp : (List<ServicePoint__c>) Trigger.new) {
            sp.Key__c = sp.Code__c;
        }
        this.generateENELTELCodes(Trigger.new);
    }

    public void beforeUpdate() {
        for (ServicePoint__c sp : (List<ServicePoint__c>) Trigger.new) {
            sp.Key__c = sp.Code__c;
        }
    }

    public void beforeDelete() {
    }

    public void afterInsert() {
        if (ENELTEL_SEQUENCERS != null) {
            this.databaseSrv.updateSObject(ENELTEL_SEQUENCERS.values());
        }
    }

    public void afterUpdate() {
        if (FIRST_TIME_UPDATE) {
            List<ServicePoint__c> newServicePointList = Trigger.new;
            Map<Id, ServicePoint__c> idToOldServicePoint = (Map<Id, ServicePoint__c>)Trigger.oldMap;
            servicePointService.setConsumerType(newServicePointList, idToOldServicePoint);
            FIRST_TIME_UPDATE = false;
        }
    }

    public void afterDelete() {
    }

    public void afterUndelete() {
    }

    private void generateENELTELCodes(List<ServicePoint__c> servicePoints) {
        List<ServicePoint__c> newENELAreaServicePoints = new List<ServicePoint__c>();
        Map<Id, List<ServicePoint__c>> newNonENELAreaServicePoints = new Map<Id, List<ServicePoint__c>>();
        Set<Id> suppliesIds = new Set<Id>();
        Set<Id> distributorIds = new Set<Id>();
        Map<Id, Account> distributors = new Map<Id, Account>();
        for (ServicePoint__c sp : servicePoints) {
            if (sp.ENELTEL__c == null && sp.CurrentSupply__c != null && sp.Distributor__c != null) {
                suppliesIds.add(sp.CurrentSupply__c);
                distributorIds.add(sp.Distributor__c);
            }
        }
        //System.debug('### supplies ids: '+suppliesIds);
        if (!suppliesIds.isEmpty()) {
            distributors = new Map<Id, Account>(this.accountQuery.listDistributorsByIds(distributorIds));

            Map<Id, Supply__c> suppliesMap = new Map<Id, Supply__c>(MRO_QR_Supply.getInstance().getByIds(suppliesIds));
            for (ServicePoint__c sp : servicePoints) {
                Account distributor = distributors.get(sp.Distributor__c);
                if (distributor.IsDisCoENEL__c) {
                    newENELAreaServicePoints.add(sp);
                }
                else {
                    Id companyDivisionId = suppliesMap.get(sp.CurrentSupply__c).CompanyDivision__c;
                    if (!newNonENELAreaServicePoints.containsKey(companyDivisionId)) {
                        newNonENELAreaServicePoints.put(companyDivisionId, new List<ServicePoint__c>{sp});
                    }
                    else {
                        newNonENELAreaServicePoints.get(companyDivisionId).add(sp);
                    }
                }
            }
        }

        if (!newENELAreaServicePoints.isEmpty()) {
            for (ServicePoint__c sp : newENELAreaServicePoints) {
                sp.ENELTEL__c = sp.Code__c.right(9);
            }
        }

        //System.debug('### new non-Enel-area service points: '+newNonENELAreaServicePoints);
        if (!newNonENELAreaServicePoints.isEmpty()) {
            MRO_QR_Sequencer sequencerQueries = MRO_QR_Sequencer.getInstance();
            ENELTEL_SEQUENCERS = sequencerQueries.getENELTELSequencers(newNonENELAreaServicePoints.keySet());
            //System.debug('### eneltel sequencers: '+ENELTEL_SEQUENCERS);

            for (Id companyDivisionId : newNonENELAreaServicePoints.keySet()) {
                Sequencer__c seq = ENELTEL_SEQUENCERS.get(companyDivisionId);
                for (ServicePoint__c sp : newNonENELAreaServicePoints.get(companyDivisionId)) {
                    seq.Sequence__c += 1;
                    String partialCode = String.valueOf(seq.Sequence__c.intValue()).leftPad(seq.SequenceLength__c.intValue(), '0');
                    Integer checkDigit = 0;
                    for (String ch : partialCode.split('')) {
                        checkDigit += Integer.valueOf(ch);
                    }
                    sp.ENELTEL__c = seq.CompanyPrefix__c + '9' + partialCode + Math.mod(checkDigit, 10);
                }
            }
        }
    }
}