public with sharing class TestUtils {

    public static Object exec(String className, String methodName, Object input, Boolean success) {
        ApexServiceLibraryCnt.Response result = ApexServiceLibraryCnt.call(className, methodName, JSON.serialize(input));
        System.assertEquals(!success, result.getIsError(), 'Wrong result');
        return result.getIsError() ? result.error : result.data;
    }

}