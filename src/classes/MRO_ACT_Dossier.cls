/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 03, 2020
 * @desc    
 * @history 
 */

global with sharing class MRO_ACT_Dossier implements wrts_prcgvr.Interfaces_1_0.IApexAction {
    private static MRO_SRV_DatabaseService databaseService = MRO_SRV_DatabaseService.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    global Object execute(Object param) {
        Map<String, Object> paramsMap = (Map<String, Object>) param;
        Dossier__c dossier = (Dossier__c) paramsMap.get('sender');
        String method = (String) paramsMap.get('method');
        wrts_prcgvr__PhaseTransition__c transition = (wrts_prcgvr__PhaseTransition__c) paramsMap.get('transition');
        wrts_prcgvr__PhaseTransitionDetail__c action = (wrts_prcgvr__PhaseTransitionDetail__c) paramsMap.get('action');

        switch on method {
            when 'commitPrivacyChange' {
                this.commitPrivacyChange(dossier);
            }
            when 'copyFilesMetadataFromParentDossier' {
                this.copyFilesMetadataFromParentDossier(dossier);
            }
            when 'noDocumentsAvailable' {
                this.noDocumentsAvailable(dossier);
            }
            when else {
                throw new WrtsException('Unrecognized method: '+method);
            }
        }

        return null;
    }

    private void commitPrivacyChange(Dossier__c dossier) {
        MRO_SRV_PrivacyChange.getInstance().commitPrivacyChange(dossier);
    }

    private void copyFilesMetadataFromParentDossier(Dossier__c dossier) {
        if (dossier.Parent__c == null) {
            throw new WrtsException('This Dossier has no parent!');
        }
        List<FileMetadata__c> parentFilesMetadata = MRO_QR_FileMetadata.getInstance().listByDossierId(dossier.Parent__c);
        if (!parentFilesMetadata.isEmpty()) {
            List<FileMetadata__c> existingChildFilesMetadata = MRO_QR_FileMetadata.getInstance().listByDossierId(dossier.Id);
            List<FileMetadata__c> clonedFilesMetadata = new List<FileMetadata__c>();
            for (FileMetadata__c fm : parentFilesMetadata) {
                Boolean childFileAlreadyExists = false;
                for (FileMetadata__c existingFm : existingChildFilesMetadata) {
                    if ((existingFm.DocumentId__c != null && existingFm.DocumentId__c == fm.DocumentId__c) ||
                        (existingFm.ExternalId__c != null && existingFm.ExternalId__c == fm.ExternalId__c) ||
                        (existingFm.DiscoFileId__c != null && existingFm.DiscoFileId__c == fm.DiscoFileId__c)) {
                        childFileAlreadyExists = true;
                        break;
                    }
                }
                if (!childFileAlreadyExists) {
                    FileMetadata__c clonedFm = fm.clone(false, true);
                    clonedFm.Dossier__c = dossier.Id;
                    clonedFilesMetadata.add(clonedFm);
                }
            }
            databaseService.insertSObject(clonedFilesMetadata);
        }
    }

    private void noDocumentsAvailable(Dossier__c dossier){
        List<Case> cases = MRO_QR_Case.getInstance().getCasesByDossierId(dossier.Id);
        for(Case c : cases){
            MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
            transitionsUtl.checkAndApplyAutomaticTransitionWithTag(c, CONSTANTS.DOCUMENT_VALIDATION_TAG, true, false);
        }
    }
}