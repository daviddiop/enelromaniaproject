/**
 * Created by Moussa Fofana  on 09/08/2019.
 */
@isTest
public with sharing class AddressFormCntTst {
    
    @isTest
    public static void getObjectInfoTest(){
        Map<String, String > inputJSON = new Map<String, String>{
            'objectApiName' => 'Account'
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'AddressFormCnt', 'getObjectInfo', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result != null );
        
        Test.stopTest();
    }
    

}