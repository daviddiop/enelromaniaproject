/**
 * Created by Antonella Loche on 23/01/2020.
 */
    
@IsTest
public with sharing class  MRO_LC_MostUsedLinkTst {
    @TestSetup
    static void setup() {
        User user = TestDataFactory.user().standardUser().build();
        insert user;
        
        wrts_prcgvr__ServiceLink__c serviceLink = new wrts_prcgvr__ServiceLink__c();
        serviceLink.wrts_prcgvr__Label__c = 'Connection';
        insert serviceLink;

        MostUsedLink__c mostUsedLink = new MostUsedLink__c(
            User__c = user.Id,
            ServiceLink__c = serviceLink.Id
        );
        insert mostUsedLink;
         
    }
    
    @isTest
    static void upsertMostUsedLinkTest() {

        wrts_prcgvr__ServiceLink__c link = [
            	SELECT Id
                FROM wrts_prcgvr__ServiceLink__c
                LIMIT 1
        ];
       
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String> {
                'linkId' => link.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_MostUsedLink', 'upsertMostUsedLink', inputJSON, true);
        system.debug('upsertMostUsedLink ***************************'+response);
        //System.assertEquals(true, response == ''); 
        
         //Test for inputJSON = null
  		response = TestUtils.exec(
                'MRO_LC_MostUsedLink', 'upsertMostUsedLink', null, true);
        system.debug('test nullo ***************************'+response);
        Test.stopTest();
    }
    
    @isTest
    static void getListOfMostUsedLinkIdsTest() {      
        User user = [
                SELECT Id
                FROM User
                LIMIT 1
        ];
        
        Test.startTest();
        Map<String, String> inputJSON = new Map<String, String> {
                'userId' => user.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_MostUsedLink', 'getListOfMostUsedLinkIds', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        response = TestUtils.exec(
                'MRO_LC_MostUsedLink', 'getListOfMostUsedLinkIds', null, true);
        result = (Map<String, Object>) response;
        
        // to exec the catch code
        inputJSON = new Map<String, String> {
                'userId' => null
        };
        response = TestUtils.exec(
                'MRO_LC_MostUsedLink', 'getListOfMostUsedLinkIds', null, true);
        system.debug('***************************'+response);
        Test.stopTest();
    }
}