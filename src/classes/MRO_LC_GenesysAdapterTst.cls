/**
 * Created by goudiaby on 19/05/2020.
 */

@IsTest
private class MRO_LC_GenesysAdapterTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Sequence__c = 1, SequenceLength__c = 1, Type__c = 'CustomerCode');
        insert sequencer;

        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert individual;

        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        personAccount.PersonIndividualId = individual.Id;
        insert personAccount;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '43252435624654';
        servicePoint.Account__c = personAccount.Id;
        servicePoint.ENELTEL__c = '123456789';
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().setAccount(personAccount.Id).build();
        insert contractAccount;

        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.Name = 'Name11062019';
        contract.AccountId = personAccount.Id;
        insert contract;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().build();
        supply.Contract__c = contract.Id;
        supply.ServicePoint__c = servicePoint.Id;
        supply.ContractAccount__c = contractAccount.Id;
        insert supply;

        servicePoint.CurrentSupply__c = supply.Id;
        update servicePoint;
    }

    @IsTest
    static void generateCTIInteractionTest() {
        Test.startTest();
        Map<String, String> inputJSON = new Map<String, String>{
                'genesysId' => '072902EF62ABF612',
                'enelTel' => '123456789',
                'blockCode' => ''
        };
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('MRO_LC_GenesysAdapter', 'generateCTIInteraction', inputJSON, true);
        Test.stopTest();
        System.assertNotEquals(null, response.get('interactionId'));
    }

    @IsTest
    static void noServicePointFoundTest() {
        Test.startTest();
        Map<String, String> inputJSON = new Map<String, String>{
                'genesysId' => '072902EF62ABF612',
                'enelTel' => '123',
                'blockCode' => ''
        };
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec('MRO_LC_GenesysAdapter', 'generateCTIInteraction', inputJSON, true);
        Test.stopTest();
        System.assertNotEquals(null, response.get('interactionId'));
    }

}