/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   08/01/2020
* @desc
* @history
*/
public with sharing class MRO_SRV_MyEnelCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout {
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap){
        System.debug('MRO_SRV_MyEnelCallOut.buildMultiRequest');

        //Sobject
        Sobject obj = (sObject) argsMap.get('sender');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        //Query
        Case richiesta = [
                SELECT  //Case
                        AccountId,
                        
                        //Account
                        Account.Email__c,
                        Account.IntegrationKey__c,
                        Account.VATNumber__c,
                        Account.NationalIdentityNumber__pc,
                        Account.Name,
                        Account.ResidentialAddress__c,

                        //Contract
                        Contract__r.CompanyDivision__c,
                        Contract__r.Status,
                        Contract__r.IntegrationKey__c,

                        //ContractAccount
                        ContractAccount__r.IntegrationKey__c,
                        ContractAccount__r.SelfReadingPeriodEnd__c,
                        ContractAccount__r.BillingFrequency__c,
                        ContractAccount__r.Account__r.isPersonAccount,
                        ContractAccount__r.CompanyDivision__c,
//                        ContractAccount__r.PaymentCode__c,

                        //Supply
                        Supply__r.RecordTypeId,

                        //BillingProfile
                        ContractAccount__r.BillingProfile__r.BillingAddress__c,

                        //ServicePoint
                        Supply__r.servicePoint__r.ENELTEL__c,
                        Supply__r.servicePoint__r.IntegrationKey__c,
                        Supply__r.servicePoint__r.PointAddress__c,
                        Supply__r.servicePoint__r.PointProvince__c,

                        //ServiceSite
                        Supply__r.ServiceSite__r.NLC__c
//                        Supply__r.ServiceSite__r.IntegrationKey__c,

                        //ConfigurationItem
//                        Opportunity.Configuration__r.ConfigurationItem__c

                From Case
                where Id = :obj.Id limit 1
        ];

        //to WObject
        Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject(richiesta, null);

        //Add mrrObj to the request
        request.objects.add(mrrObj);

        //Add request to Multirequest
        multiRequest.requests.add(request);

        return multiRequest;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        return null;
    }
}