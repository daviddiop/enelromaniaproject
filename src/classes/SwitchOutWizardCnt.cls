/**
 * Created by ferhati on 19/04/2019.
 */

public with sharing class SwitchOutWizardCnt extends ApexServiceLibraryCnt{
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static CaseQueries caseQuery = CaseQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static DossierQueries dossierQuery = DossierQueries.getInstance();

    static List<ApexServiceLibraryCnt.Option> traderPicklistValues  = new List<ApexServiceLibraryCnt.Option>();
    static String traderLabel;
    static String effectiveDateLabel;

    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String customerInteractionId = params.get('customerInteractionId');
            String companyDivisionId = params.get('companyDivisionId');

            Map<String, Object> response = new Map<String, Object>();
            try {

                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account acc = accountQuery.findAccount(accountId);
                Dossier__c dossier;
                if (String.isBlank(dossierId)) {
                    Id cusId = null;
                    List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, customerInteractionId);
                    if(!cus.isEmpty()){
                        cusId = cus[0].Id;
                    }

                    dossier = new Dossier__c ();
                    dossier.Account__c = accountId;
                    dossier.CompanyDivision__c = String.isNotBlank(companyDivisionId) ? companyDivisionId : null;
                    dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
                    dossier.Status__c = 'Draft';
                    dossier.RequestType__c = 'SwitchOut';

                    if (!cus.isEmpty()) {
                        dossier.CustomerInteraction__c =  cusId;
                    }
                    databaseSrv.insertSObject(dossier);
                    dossierId = dossier.Id;
                } else {
                    dossier = dossierQuery.getById(dossierId);
                    response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                    response.put('companyDivisionId', dossier.CompanyDivision__c);
                }
                List<Case> cases = caseQuery.getCasesByDossierId(dossierId);

                String billingProfileId = '';
                if (!cases.isEmpty()) {
                    billingProfileId = cases[0].BillingProfile__c;
                }

                getSwitchOutDescribe();
                response.put('caseTile',cases);
                response.put('billingProfileId', billingProfileId);
                response.put('dossierId', dossierId);
                response.put('dossier', dossier);
                response.put('accountId', accountId);
                response.put('account', acc);
                response.put('accountPersonRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId());
                response.put('accountPersonProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId());
                response.put('accountBusinessRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId());
                response.put('accountBusinessProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId());
                response.put('traderPicklistValues', traderPicklistValues);
                response.put('traderLabel', traderLabel);
                response.put('effectiveDateLabel', effectiveDateLabel);
                response.put('error', false);


            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public static void getSwitchOutDescribe() {
        traderPicklistValues = new List<ApexServiceLibraryCnt.Option>();
        Schema.DescribeFieldResult trader = Case.Trader__c.getDescribe();
        String traderRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        Schema.DescribeFieldResult effectiveDate = Case.EffectiveDate__c.getDescribe();
        List<Account> traders = accQuery.getTraders (traderRecordTypeId);
        if(!traders.isEmpty()){
            for(Account acc : traders){
                traderPicklistValues.add(new ApexServiceLibraryCnt.Option(acc.Name,acc.Id));
            }
            traderLabel = trader.getLabel();
            effectiveDateLabel = effectiveDate.getLabel();
        }

    }

    public class saveDraftBP extends AuraCallable {
        public override Object perform(final String jsonInput) {
            DraftBillingProfileInput billingProfileParams =(DraftBillingProfileInput) JSON.deserialize(jsonInput, DraftBillingProfileInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = new List<Case>();

            Savepoint sp = Database.setSavepoint();
            response.put('error', false);
            try {
                if (String.isNotBlank(billingProfileParams.billingProfileId) && (!billingProfileParams.oldCaseList.isEmpty())) {

                    for (Case caseRecord : billingProfileParams.oldCaseList) {
                        caseList.add(new Case(Id = caseRecord.Id, BillingProfile__c = billingProfileParams.billingProfileId));
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class DraftBillingProfileInput{
        @AuraEnabled
        public List<Case> oldCaseList{get; set;}
        @AuraEnabled
        public String billingProfileId{get; set;}
    }

    public class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CreateCaseInput createCaseParams =(CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);

            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> switchOutRecordTypes = Constants.getCaseRecordTypes('SwitchOut');
            List<Case> newCases = new List<Case>();
            String recordTypeId;
            String caseOrigin = 'Phone';
            String companyDivisionId = createCaseParams.searchedSupplyFieldsList[0].CompanyDivision__c;

            response.put('error', false);
            try {
                if (String.isNotBlank(createCaseParams.trader) && (!createCaseParams.searchedSupplyFieldsList.isEmpty()) && (createCaseParams.effectiveDate != null) && (String.isNotBlank(createCaseParams.accountId)) && (String.isNotBlank(createCaseParams.dossierId))) {

                    for (Supply__c supply : createCaseParams.searchedSupplyFieldsList) {
                        recordTypeId = supply.RecordType.DeveloperName == 'Gas' ? switchOutRecordTypes.get('SwitchOut_GAS') : switchOutRecordTypes.get('SwitchOut_ELE');
                        Case disconnectedCase = new Case(Supply__c = supply.Id, CompanyDivision__c = supply.CompanyDivision__c, EffectiveDate__c = createCaseParams.effectiveDate, Trader__c = createCaseParams.trader, AccountId = createCaseParams.accountId, RecordTypeId = recordTypeId, Dossier__c = createCaseParams.dossierId,Status='Draft',Origin = caseOrigin);
                        newCases.add(disconnectedCase);
                    }
                    if (!newCases.isEmpty()) {
                        databaseSrv.insertSObject(newCases);

                        if (!createCaseParams.caseList.isEmpty()) {
                            for (Case oldCaseList : createCaseParams.caseList) {
                                newCases.add(oldCaseList);
                            }
                        }
                        response.put('caseTile', newCases);
                    }
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CreateCaseInput{
        @AuraEnabled
        public List<Supply__c> searchedSupplyFieldsList{get; set;}
        @AuraEnabled
        public Date effectiveDate{get; set;}
        @AuraEnabled
        public String trader{get; set;}
        @AuraEnabled
        public String accountId{get; set;}
        @AuraEnabled
        public List<Case> caseList{get; set;}
        @AuraEnabled
        public String dossierId{get; set;}
    }

    public class updateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            UpdateCaseListInput updateCaseListParams =(UpdateCaseListInput) JSON.deserialize(jsonInput, UpdateCaseListInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = new List<Case>();
            List<Supply__c> supplyList = new List<Supply__c>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (!updateCaseListParams.oldCaseList.isEmpty() && String.isNotBlank(updateCaseListParams.dossierId) && String.isNotBlank(updateCaseListParams.billingProfileId)) {
                    //add  by david
                    String companyDivisionId = updateCaseListParams.oldCaseList[0].CompanyDivision__c;
                    //
                    Dossier__c dossier = new Dossier__c (Id= updateCaseListParams.dossierId ,Status__c = 'New' ,CompanyDivision__c = companyDivisionId);
                    if (dossier != null) {
                        databaseSrv.updateSObject(dossier);
                    }

                    for (Case caseRecord : updateCaseListParams.oldCaseList) {
                        caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status ='New', BillingProfile__c = updateCaseListParams.billingProfileId));
                        supplyList.add(new Supply__c(Id = caseRecord.Supply__c, Status__c = 'Terminating', Terminator__c = caseRecord.Id));
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }
                    if (!supplyList.isEmpty()) {
                        databaseSrv.updateSObject(supplyList);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class UpdateCaseListInput{
        @AuraEnabled
        public List<Case> oldCaseList{get; set;}
        @AuraEnabled
        public String dossierId{get; set;}
        @AuraEnabled
        public String billingProfileId{get; set;}
        //add by david
        @AuraEnabled
        public List<Supply__c> searchedSupplyFieldsList{get; set;}
        //
    }

    public class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            CancelProcessInput cancelProcessParams =(CancelProcessInput) JSON.deserialize(jsonInput, CancelProcessInput.class);
            Map<String, Object> response = new Map<String, Object>();
            List<Case> caseList = new List<Case>();
            List<Supply__c> supplyList = new List<Supply__c>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);

            try {
                if (!cancelProcessParams.oldCaseList.isEmpty() && String.isNotBlank(cancelProcessParams.dossierId)) {
                    Dossier__c dossier = new Dossier__c (id= cancelProcessParams.dossierId, Status__c = 'Canceled');
                    if (dossier != null) {
                        databaseSrv.upsertSObject(dossier);
                    }

                    for (Case caseRecord : cancelProcessParams.oldCaseList) {
                        caseList.add(new Case(Id = caseRecord.Id, Dossier__c = dossier.Id, Status ='Canceled'));
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CancelProcessInput{
        @AuraEnabled
        public List<Case> oldCaseList{get; set;}
        @AuraEnabled
        public String dossierId{get; set;}
    }
}