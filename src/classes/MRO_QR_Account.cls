/**
 * Created by napoli on 19/11/2019.
 */

public with sharing class MRO_QR_Account {

    public static MRO_QR_Account getInstance() {
        return (MRO_QR_Account) ServiceLocator.getInstance(MRO_QR_Account.class);
    }

    public List<Account> listAccountByVatNumber(String vatNumber) {
        return [
                SELECT Id, Name
                FROM Account
                WHERE VATNumber__c = :vatNumber
        ];
    }

    public List<Account> listByNationalId(String nationalId) {
        return [
                SELECT Id, Name, Email__c, VATNumber__c,NACECode__c,NACEReference__c,ConsumerType__c,
                        ONRCCode__c, Phone, PersonMobilePhone, PersonEmail, PersonContactId,NationalIdentityNumber__pc, ForeignCitizen__pc,
                        LastName, FirstName, IsPersonAccount, Active__c, PersonIndividualId, RecordTypeId, RecordType.Name, RecordType.DeveloperName,
                        ResidentialStreetId__c,ResidentialStreetNumber__c,
                        ResidentialCity__c,ResidentialStreetType__c,ResidentialStreetName__c,
                        ResidentialStreetNumberExtn__c, ResidentialProvince__c, ResidentialPostalCode__c,
                        ResidentialFloor__c, ResidentialLocality__c, ResidentialCountry__c,
                        ResidentialBuilding__c, ResidentialBlock__c, ResidentialApartment__c,ResidentialAddressNormalized__c,
                        IDDocEmissionDate__pc,IDDocValidityDate__pc,IDDocumentNr__pc,
                        IDDocumentType__pc,IDDocumentSeries__pc,IDDocIssuer__pc,
                        ResidentialAddressKey__c
                FROM Account
                WHERE NationalIdentityNumber__pc = :nationalId
        ];
    }

    public List<Account> listByKey(String key) {
        return [
                SELECT Id, Name, PersonContactId, PersonIndividualId,ConsumerType__c
                FROM Account
                WHERE Key__c = :key
        ];
    }

    public Map<Id, Account> getByIds(Set<Id> accountIds) {
        return new Map<Id, Account>([
                SELECT Id,Name, Email__c, VATNumber__c, Phone, PersonMobilePhone, PersonEmail,NationalIdentityNumber__pc, ForeignCitizen__pc,ConsumerType__c,
                        LastName, FirstName, IsPersonAccount, PersonIndividualId, RecordTypeId,Active__c,PersonContactId,ForeignCompany__c,PrimaryContact__c,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        Fax,Website,VIP__c,VATExempted__c,BusinessType__c,NumberOfEmployees,AnnualRevenue,AnnualRevenueYear__c,ContactChannel__pc,MarketingContactChannel__pc,
                        IDDocEmissionDate__pc,IDDocValidityDate__pc,IDDocumentNr__pc,IDDocumentType__pc,IDDocumentSeries__pc,IDDocIssuer__pc,BirthCity__pc,PersonBirthdate,Gender__pc,
                        //FF Customer Creation - Pack1/2 - Interface Check
                        ResidentialStreetType__c, ResidentialStreetNumber__c, ResidentialStreetNumberExtn__c, ResidentialStreetName__c,
                        ResidentialProvince__c, ResidentialPostalCode__c,ResidentialFloor__c, ResidentialLocality__c,
                        ResidentialCountry__c, ResidentialCity__c, ResidentialBuilding__c, ResidentialBlock__c, ResidentialApartment__c, ResidentialAddressKey__c, ResidentialStreetId__c,
                        NACECode__c, ONRCCode__c, ResidentialAddressNormalized__c,InsolvencyBankruptcyStatus__c,
                        InsolvencyStartDate__c, InsolvencyEndDate__c, BankruptcyDate__c,JudicialAdministrator__c,
                        JudicialLiquidator__c, TraderProvincesDetails__c
                FROM Account
                WHERE Id IN :accountIds
        ]);
    }

    public Account findAccount(String accountId) {
        List<Account> accountList = [
            SELECT Id, Name, Email__c, VATNumber__c,NACECode__c,NACEReference__c,Key__c,PrimaryContact__c,ConsumerType__c,
                ONRCCode__c, Phone, PersonMobilePhone, PersonEmail, PersonContactId,NationalIdentityNumber__pc, ForeignCitizen__pc,ForeignCompany__c,
                LastName, FirstName, IsPersonAccount, Active__c, PersonIndividualId, RecordTypeId, RecordType.Name, RecordType.DeveloperName,
                IntegrationKey__c,JudicialAdministrator__c,JudicialLiquidator__c,
                //FF Customer Creation - Pack1/2 - Interface Check
                Fax,Website,VIP__c,VATExempted__c,BusinessType__c,NumberOfEmployees,AnnualRevenue,AnnualRevenueYear__c,ContactChannel__pc,MarketingContactChannel__pc,
                IDDocEmissionDate__pc,IDDocValidityDate__pc,IDDocumentNr__pc,IDDocumentType__pc,IDDocumentSeries__pc,IDDocIssuer__pc,BirthCity__pc,PersonBirthdate,Gender__pc,
                //FF Customer Creation - Pack1/2 - Interface Check
                ResidentialStreetType__c, ResidentialStreetNumber__c, ResidentialStreetNumberExtn__c, ResidentialStreetName__c, ResidentialProvince__c, ResidentialPostalCode__c,
                ResidentialFloor__c, ResidentialLocality__c, ResidentialCountry__c, ResidentialCity__c, ResidentialBuilding__c, ResidentialBlock__c, ResidentialApartment__c,ResidentialAddressNormalized__c,
                ResidentialAddressKey__c, ResidentialStreetId__c,SelfReadingEnabled__c,
                InsolvencyBankruptcyStatus__c, InsolvencyStartDate__c, InsolvencyEndDate__c, BankruptcyDate__c, ResidentialAddress__c, AccountNumber, RecordTypeDeveloperName__c,UniversalService__c,PrimaryContact__r.Phone, PrimaryContact__r.MobilePhone
            FROM Account
            WHERE Id = :accountId
            LIMIT 1
        ];
        if (accountList.isEmpty()) {
            return null;
        }
        return accountList.get(0);
    }

    public Account getAccountById(String accountId) {
        List<Account> accountList = [
                SELECT Id, Name, Email__c, VATNumber__c,NACECode__c,NACEReference__c,Key__c,PrimaryContact__c,
                        ONRCCode__c, Phone, PersonMobilePhone, PersonEmail, PersonContactId, ForeignCompany__c,
                        LastName, FirstName, IsPersonAccount, Active__c, PersonIndividualId, RecordTypeId, RecordType.Name, RecordType.DeveloperName,
                        IntegrationKey__c,JudicialAdministrator__c,JudicialLiquidator__c,
                        Fax,Website,VIP__c,VATExempted__c,BusinessType__c,NumberOfEmployees,AnnualRevenue,AnnualRevenueYear__c,
                        ResidentialStreetType__c, ResidentialStreetNumber__c, ResidentialStreetNumberExtn__c, ResidentialStreetName__c, ResidentialProvince__c, ResidentialPostalCode__c,
                        ResidentialFloor__c, ResidentialLocality__c, ResidentialCountry__c, ResidentialCity__c, ResidentialBuilding__c, ResidentialBlock__c, ResidentialApartment__c,ResidentialAddressNormalized__c,
                        ResidentialAddressKey__c, ResidentialStreetId__c,SelfReadingEnabled__c,
                        InsolvencyBankruptcyStatus__c, InsolvencyStartDate__c, InsolvencyEndDate__c, BankruptcyDate__c, ResidentialAddress__c, AccountNumber, RecordTypeDeveloperName__c
                FROM Account
                WHERE Id = :accountId
                LIMIT 1
        ];
        if (accountList.isEmpty()) {
            return null;
        }
        return accountList.get(0);
    }

    public List<Account> listByIndividualId(String individualId) {
        return [
                SELECT Id, PersonContactId, PersonIndividualId, Name, RecordType.Name, BusinessType__c, VATNumber__c, NationalIdentityNumber__pc, IsPersonAccount,ConsumerType__c
                FROM Account
                WHERE PersonIndividualId = :individualId
        ];
    }

    public List<Account> listByIds(List<Id> accountListIds) {
        return [
                SELECT Id
                FROM Account
                WHERE Id IN :accountListIds
        ];
    }

    public List<Account> listByIdsWithServicePoints(Set<Id> accountIds, Set<String> servicePointRtDevNames) {
        return [
                SELECT Id, ConsumerType__c, (
                        SELECT Id, ConsumerType__c, RecordType.DeveloperName, CurrentSupply__c, CurrentSupply__r.Status__c
                        FROM ServicePoints__r
                        WHERE RecordType.DeveloperName IN :servicePointRtDevNames
                )
                FROM Account
                WHERE Id IN :accountIds
        ];
    }

    public List<AccountContactRelation> listContacts(String accountId) {
        return [
                SELECT Id,AccountId, Contact.IndividualId,LegalRepresentative__c
                FROM AccountContactRelation
                WHERE AccountId = :accountId AND LegalRepresentative__c = TRUE
        ];
    }

    public List<Account> getTraders(String traderRecordTypeId) {
        return [
                SELECT Id, Name
                FROM Account
                WHERE RecordTypeId = :traderRecordTypeId
        ];
    }

    public List<Account> listDistributorsByIds(Set<Id> distributorIds) {
        return [
                SELECT Name, VATNumber__c, IsDisCoENEL__c, AtoAEnabled__c, ResidentialProvince__c,ConsumerType__c
                FROM Account
                WHERE RecordType.DeveloperName = 'Distributor' AND Id IN :distributorIds
        ];
    }

    public Account getGasDistributorWithCLCPrefix(String clcPrefix) {
        List<Account> distributors = [
                SELECT Name, VATNumber__c, IsDisCoENEL__c, AtoAEnabled__c,ConsumerType__c
                FROM Account
                WHERE RecordType.DeveloperName = 'Distributor' AND IsDisCoGas__c = TRUE
                AND CLCPrefix__c != NULL AND CLCPrefix__c = :clcPrefix
                LIMIT 1
        ];
        return (distributors.isEmpty() ? null : distributors[0]);
    }

    public Account getElectricDistributorWithPointCodePrefix(String pointCodePrefix) {
        List<Account> distributors = [
                SELECT Name, VATNumber__c, IsDisCoENEL__c, AtoAEnabled__c, PointCodePrefix__c, DistributionProvincesDetails__c,ConsumerType__c
                FROM Account
                WHERE RecordType.DeveloperName = 'Distributor' AND IsDisCoEle__c = TRUE
                AND PointCodePrefix__c != NULL AND PointCodePrefix__c = :pointCodePrefix
                LIMIT 1
        ];
        return (distributors.isEmpty() ? null : distributors[0]);
    }
    // [ENLCRO-657] OSI Edit - Pack3 - Interface Check
    public Account getAccountDataByDistributorId(String distributorId) {
        return [
                SELECT Id,IsDisCoENEL__c,ConsumerType__c,SelfReadingEnabled__c,DistributionProvincesDetails__c
                FROM Account
                WHERE RecordType.DeveloperName = 'Distributor' AND Id = :distributorId
                LIMIT 1
        ];
    }

    public Account getAccountDataByIdAndRecordTypeName(String accountId, String recordTypeName) {
        return [
                SELECT Id,VATNumber__c,ConsumerType__c
                FROM Account
                WHERE RecordType.DeveloperName = :recordTypeName AND Id = :accountId
                Limit 1
        ];
    }

    public Account getSalesUnitAccountById(String SalesUnitId) {
        List<Account> accountList = [SELECT Id, SalesDepartment__c, SalesUnitID__c,ConsumerType__c
        FROM Account
        WHERE RecordType.DeveloperName = 'SalesUnit' AND SalesUnitID__c = :SalesUnitId
        LIMIT 1];
        return accountList.isEmpty() ? null : accountList.get(0);
    }

    // [ENLCRO-657] OSI Edit - Pack3 - Interface Check

    /**
    * @author  David diop
    * @description Query necessary fields for Genesys
    * @return Account accountObject
    */
    public Account getAccountByIntegrationKey(String integrationKey) {
        List<Account> accountList = [
                SELECT Id,IntegrationKey__c, ConsumerType__c, PersonMobilePhone, PersonContactId
                FROM Account
                WHERE IntegrationKey__c = :integrationKey
        ];
        if (accountList.isEmpty()) {
            return null;
        }
        return accountList.get(0);
    }
    //tax change
    public List<Account> listInstitutionsByRecord(String RecordTypeString) {
        return [
                SELECT Id,Name,ResidentialProvince__c,RecordTypeId,ResidentialLocality__c,PaymentAgency__c,ConsumerType__c,PaymentAgency__r.Name,PayableSubventions__c,AccountNumber
                FROM Account
                WHERE RecordTypeId = :RecordTypeString
        ];
    }
    public List<Account> getInstitutionsById(String RecordTypeString, String recordId) {
        return [
                SELECT  Id,Name,ResidentialProvince__c,RecordTypeId,ResidentialLocality__c,PaymentAgency__c,ConsumerType__c,PaymentAgency__r.Name,PayableSubventions__c,AccountNumber
                FROM Account
                WHERE RecordTypeId = :RecordTypeString AND Id = :recordId
        ];
    }
    public Map<String, Account> getListAccountByIntegrationKey(List<String> integrationKeys) {
        Map<String, Account> mapAccountId = new Map<String, Account>();
        List<Account> accountList = [
                SELECT Id,IntegrationKey__c,ConsumerType__c
                FROM Account
                WHERE IntegrationKey__c IN :integrationKeys
        ];
        for (Account accountRecord : accountList) {
            mapAccountId.put(accountRecord.IntegrationKey__c, accountRecord);
        }
        return mapAccountId;
    }
    //added by giupastore 30/09/2020
    public List<Account> listByAccountNumber(String accountNumber) {
        return [
                SELECT Id, Name, PersonContactId, PersonIndividualId,ConsumerType__c
                FROM Account
                WHERE AccountNumber = :accountNumber
        ];
    }

    /**
     * @author  Boubacar Sow
     * @date 03/12/2020
     * @description [ENLCRO-1859] Location Approval - MROCRMDEF-3109
     * @return List<Account>
     */
    public List<Account> getDistributorEnel() {
        return [
            SELECT Id, AccountNumber, Name, DistributionProvincesDetails__c
            FROM Account
            WHERE RecordType.DeveloperName = 'Distributor'
            AND IsDisCoENEL__c = TRUE
        ];
    }
}