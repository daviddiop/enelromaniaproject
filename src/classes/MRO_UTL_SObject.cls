public with sharing class MRO_UTL_SObject {

    public static String getSobjectTypeName(final List<sObject> sObjectList) {
        return sObjectList.get(0).getSObjectType().getDescribe().getName();
    }

    public static String getSobjectTypeName(final List<Id> idList) {
        return idList.get(0).getSObjectType().getDescribe().getName();
    }

    public static String getRecordTypeDeveloperName(String objectType, Id recordTypeId) {
        return Schema.getGlobalDescribe().get(objectType).getDescribe().getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
    }

    public static List<String> listNotNullByField(SObject[] sObjectList, Schema.SObjectField field) {
        List<String> values = new List<String>();
        for(SObject sObj : sObjectList) {
            String value = (String) sObj.get(field);
            if(String.isNotBlank(value)) {
                values.add(value);
            }
        }
        return values;
    }

    public static Set<String> setNotNullByField(SObject[] sObjectList, Schema.SObjectField field) {
        Set<String> values = new Set<String>();
        for(SObject sObj : sObjectList) {
            String value = (String) sObj.get(field);
            if(String.isNotBlank(value)) {
                values.add(value);
            }
        }
        return values;
    }

    public static Set<Id> setIdByField(SObject[] sObjectList, Schema.SObjectField field) {
        Set<Id> values = new Set<Id>();
        for(SObject sObj : sObjectList) {
            Id value = (Id) sObj.get(field);
            if(value!=null) {
                values.add(value);
            }
        }
        return values;
    }

    public static Map<String, String> mapTwoFields(SObject[] sObjectList, Schema.SObjectField keyfield, Schema.SObjectField valuefield) {
        Map<String, String> fieldNamesMap = new Map<String, String>();
        for(SObject sObj : sObjectList) {
            String value = (String) sObj.get(keyfield);
            if(!fieldNamesMap.containsKey(value)) {
                fieldNamesMap.put(value, (String)sObj.get(valuefield));
            }
        }
        return fieldNamesMap;
    }

    public static Map<String, SObject> mapByFieldAndSobject(SObject[] sObjectList, Schema.SObjectField field) {
        Map<String, SObject> fieldSobjectMap = new Map<String, SObject>();
        for(SObject sObj : sObjectList) {
            String value = (String) sObj.get(field);
            if(!fieldSobjectMap.containsKey(value)) {
                fieldSobjectMap.put(value, sObj);
            }
        }
        return fieldSobjectMap;
    }

    public static Map<String, SObject[]> mapByFieldAndList(SObject[] sObjectList, Schema.SObjectField field) {
        Map<String, SObject[]> fieldSobjectMap = new Map<String, SObject[]>();
        for(SObject sObj : sObjectList) {
            String key = (String) sObj.get(field);
            SObject[] values = null;
            if(fieldSobjectMap.containsKey(key)) {
                values = fieldSobjectMap.get(key);
            } else {
                values = new List<SObject>();
                fieldSobjectMap.put(key, values);
            }
            values.add(sObj);
        }
        return fieldSobjectMap;
    }

    public static Map<String, Set<String>> mapFieldAndSet(SObject[] sObjectList, Schema.SObjectField keyfield, Schema.SObjectField valuefield) {
        Map<String, Set<String>> fieldNamesMap = new Map<String, Set<String>>();
        for(SObject sObj : sObjectList) {
            String key = (String) sObj.get(keyfield);
            Set<String> values = null;
            if(fieldNamesMap.containsKey(key)) {
                values = fieldNamesMap.get(key);
            } else {
                values = new Set<String>();
                fieldNamesMap.put(key, values);
            }
            values.add((String)sObj.get(valuefield));
        }
        return fieldNamesMap;
    }

    public static SObject getParentRecord(SObject rootRecord, String parentRelationPath) {
        SObject targetRecord = rootRecord.clone(true, true);
        if (String.isNotBlank(parentRelationPath)) {
            List<String> pathElements = parentRelationPath.split('\\.');
            while (!pathElements.isEmpty()) {
                targetRecord = targetRecord.getSObject(pathElements[0]);
                if (targetRecord == null) {
                    return null;
                }
                pathElements.remove(0);
            }
        }
        return targetRecord;
    }

    public static SObject createSObjectFromFieldsMap(SObjectType objectType, Map<String, Object> fieldsMap, Id recordId) {
        SObject record = objectType.newSObject(recordId);
        Map<String, SObjectField> fieldsDescribe = objectType.getDescribe().fields.getMap();
        for (String fieldName : fieldsMap.keySet()) {
            DescribeFieldResult fieldDescribe = fieldsDescribe.get(fieldName).getDescribe();
            Object value = fieldsMap.get(fieldName);
            if (value == null) {
                record.put(fieldName, null);
                continue;
            }
            SoapType type = fieldDescribe.getSoapType();
            switch on type {
                when STRING {
                    record.put(fieldName, value instanceof String ? (String) value : String.valueOf(value));
                }
                when BOOLEAN {
                    record.put(fieldName, value instanceof Boolean ? (Boolean) value : Boolean.valueOf(value));
                }
                when DATE {
                    record.put(fieldName, value instanceof Date ? (Date) value : (value instanceof String ? Date.valueOf((String) value) : Date.valueOf(value)));
                }
                when DATETIME {
                    record.put(fieldName, value instanceof Datetime ? (Datetime) value : (value instanceof String ? Datetime.valueOf((String) value) : Datetime.valueOf(value)));
                }
                when DOUBLE {
                    record.put(fieldName, value instanceof Double ? (Double) value : Double.valueOf(value));
                }
                when INTEGER {
                    record.put(fieldName, value instanceof Integer ? (Integer) value : Integer.valueOf(value));
                }
                when ID {
                    record.put(fieldName, value instanceof Id ? (Id) value : (Id) String.valueOf(value));
                }
                when else {
                    throw new WrtsException('Unsupported field type: '+type);
                }
            }
        }
        return record;
    }

    public static String getQueryStringAllFields(SObjectType objectType, String parentRelationPath) {
        List<String> fieldNames = new List<String>();
        Map<String, SObjectField> fieldsMap = objectType.getDescribe().fields.getMap();
        for (String fieldName : fieldsMap.keySet()) {
            if (fieldName.equalsIgnoreCase('crosidtmp__c')) {
                continue;
            }
            SObjectField field = fieldsMap.get(fieldName);
            fieldNames.add((!String.isBlank(parentRelationPath) ? parentRelationPath+'.' : '')+field.getDescribe().getName());
        }
        return String.join(fieldNames, ',');
    }
}