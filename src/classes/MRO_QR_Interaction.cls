public with sharing class MRO_QR_Interaction {

    public static MRO_QR_Interaction getInstance() {
        return new MRO_QR_Interaction();
    }

    public Interaction__c findInteractionById(String interactionId) {
        List<Interaction__c> interactionList = [
            SELECT Id, Name, CreatedDate, Channel__c, Interlocutor__c, Status__c, Comments__c,Origin__c, Institution__c, Institution__r.Name, InterlocutorType__c,
                InterlocutorFirstName__c, InterlocutorLastName__c, InterlocutorNationalIdentityNumber__c, InterlocutorEmail__c, InterlocutorPhone__c,
                Interlocutor__r.Id, Interlocutor__r.Name, Interlocutor__r.FirstName, Interlocutor__r.LastName,
                Interlocutor__r.NationalIdentityNumber__c, Interlocutor__r.ForeignCitizen__c,
                    //FF Customer Creation - Pack1/2 - Interface Check
                Interlocutor__r.IDDocEmissionDate__c,
                Interlocutor__r.IDDocValidityDate__c,
                Interlocutor__r.IDDocumentNr__c,
                Interlocutor__r.IDDocumentType__c,
                Interlocutor__r.IDDocumentSeries__c,
                Interlocutor__r.IDDocIssuer__c,
                Interlocutor__r.TechnicalDetails__c,
                Interlocutor__r.Gender__c,
                Interlocutor__r.BirthDate,
                Interlocutor__r.BirthCity__c,
                    //FF Customer Creation - Pack1/2 - Interface Check
                (
                    SELECT Id, Customer__c, Interaction__c, Contact__c
                    FROM CustomerInteractions__r
                    )
            FROM Interaction__c
            WHERE Id = :interactionId
        ];
        if (interactionList.isEmpty()) {
            return null;
        }
        return interactionList.get(0);
    }
    /**
    * @author  David diop
    * @description Query necessary fields for EnelEasy
    * @return Interaction__c interactionObject
    */
    public List<Interaction__c> getInteractionByFirstName(String interactionFirstName) {
        List<Interaction__c> interactionList = [
                SELECT Id,InterlocutorFirstName__c,InterlocutorLastName__c,InterlocutorPhone__c,InterlocutorEmail__c
                FROM Interaction__c
                WHERE  InterlocutorFirstName__c = :interactionFirstName
        ];
        return interactionList;
    }

    public Interaction__c getInteractionByCallId(String callId) {
        List<Interaction__c> interactionList = [
                SELECT Id,InterlocutorFirstName__c,InterlocutorLastName__c,InterlocutorPhone__c,InterlocutorEmail__c,CallId__c
                FROM Interaction__c
                WHERE  CallId__c = :callId
        ];
        return interactionList.isEmpty() ? null : interactionList.get(0);
    }

    public Map<String, Interaction__c> getListInteractionByCallId(Set<String> callId) {
        Map<String, Interaction__c> mapinteractionId = new Map<String, Interaction__c>();
        List<Interaction__c> interactionList = [
                SELECT Id,InterlocutorFirstName__c,InterlocutorLastName__c,InterlocutorPhone__c,InterlocutorEmail__c,CallId__c
                FROM Interaction__c
                WHERE CallId__c IN :callId
        ];
        for (Interaction__c interaction : interactionList) {
            mapinteractionId.put(interaction.CallId__c, interaction);
        }
        return mapinteractionId;
    }
}