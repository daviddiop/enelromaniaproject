/**
 * Created by goudiaby on 15/05/2020.
 */

public with sharing class MRO_LC_GenesysAdapter extends ApexServiceLibraryCnt {
    /** SERVICE CLASS INSTANCES **/
    /** Database service instance **/
    private static final MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    public with sharing class generateCTIInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String genesysId = params.get('genesysId');
            String enelTel = params.get('enelTel');
            Interaction__c interaction = createGenericCTIInteraction(genesysId,enelTel);
            List<ServicePoint__c> servicePoints = String.isNotBlank(enelTel) ? getServicePointDataFromEnelTel(enelTel) : null;
            if (servicePoints !=null && !servicePoints.isEmpty()) {
                ServicePoint__c myServicePoint = servicePoints[0];
                String interlocutorId = myServicePoint.Account__r.PersonIndividualId;
                response.put('accountId', myServicePoint.Account__c);
                //4 .Popup for Service Point Page
                response.put('servicePointId', myServicePoint.Id);
                //2 - 3 .Popup for Supply Page
                if (String.isNotBlank(myServicePoint.CurrentSupply__c)) {
                    response.put('supplyId', myServicePoint.CurrentSupply__c);
                    //1 .Popup for Contract Account Page
                    if (String.isNotBlank(myServicePoint.CurrentSupply__r.ContractAccount__c)) {
                        response.put('contractAccountId', myServicePoint.CurrentSupply__r.ContractAccount__c);
                    }
                }
                updateCTIInteraction(interaction.Id, interlocutorId, myServicePoint.Account__c);
            }else {
                response.put('servicePointId', null);
            }
            //5 .Popup for Interaction Page
            response.put('interactionId', interaction.Id);
            return response;
        }
    }

    /**
     * @description Method to create a generic Interaction for Genesys
     * @param callID Genesys connection Id
     * @param enelTel EnelTel value from Genesys
     * @return Interaction
    */
    static Interaction__c createGenericCTIInteraction(String callID,String enelTel) {
        Interaction__c interactionObj = new Interaction__c();
        interactionObj.Channel__c = MRO_UTL_Constants.CHANNEL_CALL_CENTER;
        interactionObj.Origin__c = MRO_UTL_Constants.ORIGIN_PHONE_IN;
        interactionObj.CallId__c = callID;
        interactionObj.ENELTEL__c = enelTel;
        databaseSrv.insertSObject(interactionObj);
        return interactionObj;
    }

    /**
     * @description Update interaction with information from Customer
     * @param idInteraction Interaction Id to be update
     * @param interlocutor Interlocutor related to the Account
     * @param accountId Account id of customer
    */
    static void updateCTIInteraction(String idInteraction, String interlocutor, String accountId) {
        Interaction__c interaction = new Interaction__c(Id = idInteraction, Interlocutor__c = interlocutor);
        CustomerInteraction__c ci = new CustomerInteraction__c();
        ci.Customer__c = accountId;
        ci.Interaction__c = interaction.Id;
        databaseSrv.updateSObject(interaction);
        databaseSrv.insertSObject(ci);
    }

    /**
     * @description Get ServicePoint by EnelTel provided
     * @param enelTel ENELTEL Value from Genesys
     * @return List Service Point
    */
    static List<ServicePoint__c> getServicePointDataFromEnelTel(String enelTel) {
        return [
                SELECT Id, Account__c,Account__r.PersonIndividualId, CurrentSupply__c, CurrentSupply__r.Status__c,
                        CurrentSupply__r.CompanyDivision__c, CurrentSupply__r.ContractAccount__c,
                        Distributor__c, ENELTEL__c, Code__c
                FROM ServicePoint__c
                WHERE ENELTEL__c = :enelTel
                LIMIT 1
        ];
    }

}