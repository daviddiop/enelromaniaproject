/**
 * Created by ferhati on 03/04/2019.
 * Modified by Moussa on 23/07/2019.
 */
@isTest
private class SwitchOutWizardCntTst {

    @testSetup
    private static void setup() {
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();
        List<Account> listAccount = new list<Account>();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        listAccount.add(TestDataFactory.account().personAccount().build());
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;        
        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;         

        Account accountTrader = TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'BusinessAccount1';
        accountTrader.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        accountTrader.Key__c = '1324433';
        accountTrader.BusinessType__c = 'Commercial areas';
        insert accountTrader;




        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePointGas().build();
        servicePoint.Account__c = listAccount[1].Id;
        servicePoint.Trader__c = accountTrader.Id;
        servicePoint.Distributor__c = listAccount[1].Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[1].Id;
        insert contract;

        BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[1].Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        insert billingProfile;


        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Account__c = listAccount[1].Id;
            //supply.ServicePoint__c = servicePoint.Id;
            supply.CompanyDivision__c = companyDivision.Id;
            supplyList.add(supply);
        }
        insert supplyList;

        Dossier__c dossier = TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[1].Id;
        insert dossier;
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_ELE').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }


    @isTest
    static void initializeTest() {
        Account account = [
			SELECT Id,Name
			FROM Account
			LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];        

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];        
        CompanyDivision__c companyDivision = [
            SELECT Id,Name 
            FROM CompanyDivision__c 
            WHERE Name = 'ENEL 1' 
            LIMIT 1
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'customerInteractionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        Object response = TestUtils.exec(
                'SwitchOutWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => '',
                'customerInteractionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id};
        response = TestUtils.exec(
                'SwitchOutWizardCnt', 'initialize', inputJSON, true);
        inputJSON = new Map<String, String>{
                'accountId' => '',
                'dossierId' => dossier.Id,
                'customerInteractionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id};
        response = TestUtils.exec(
                'SwitchOutWizardCnt', 'initialize', inputJSON, true);
        Test.stopTest();
    }
    @isTest
    private static void createCaseTest() {
        List<Supply__c> supply = [
                SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c
                FROM Supply__c
                WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Trader__c,AccountId, RecordTypeId
                FROM Case
                LIMIT 2
        ];
        List<Account> myAccount = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Test.startTest();

        SwitchOutWizardCnt.CreateCaseInput createCaseInput = new SwitchOutWizardCnt.CreateCaseInput();
        createCaseInput.searchedSupplyFieldsList = supply;
        createCaseInput.effectiveDate = caseList[0].EffectiveDate__c;
        createCaseInput.trader = caseList[0].Trader__c;
        createCaseInput.accountId = myAccount[0].Id;
        createCaseInput.caseList = caseList;
        createCaseInput.dossierId = dossier.Id;
        Object response = TestUtils.exec(
                'SwitchOutWizardCnt', 'createCase', createCaseInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        createCaseInput.accountId = 'testInexistentAccountId';
        response = TestUtils.exec(
                'SwitchOutWizardCnt', 'createCase', createCaseInput, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error')== true);
        Test.stopTest();
    }
    @isTest
    private static void updateCaseListTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
        ];

        Test.startTest();
        SwitchOutWizardCnt.UpdateCaseListInput updateCaseListInput = new SwitchOutWizardCnt.UpdateCaseListInput();
        updateCaseListInput.oldCaseList = caseList;
        updateCaseListInput.dossierId = dossier.Id;
        updateCaseListInput.billingProfileId = billingProfile.Id;
        Object response = TestUtils.exec(
                'SwitchOutWizardCnt', 'updateCaseList', updateCaseListInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        updateCaseListInput.oldCaseList = null;
        response = TestUtils.exec(
                'SwitchOutWizardCnt', 'updateCaseList', updateCaseListInput, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @isTest
    private static void cancelProcessTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Test.startTest();
        SwitchOutWizardCnt.CancelProcessInput cancelProcessInput = new SwitchOutWizardCnt.CancelProcessInput();
        cancelProcessInput.oldCaseList = caseList;
        cancelProcessInput.dossierId = dossier.Id;

        Object response = TestUtils.exec(
                'SwitchOutWizardCnt', 'cancelProcess', cancelProcessInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        cancelProcessInput.oldCaseList = null;
        response = TestUtils.exec(
                'SwitchOutWizardCnt', 'cancelProcess', cancelProcessInput, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @isTest
    private static void saveDraftBPTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];

        BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
        ];

        Test.startTest();
        SwitchOutWizardCnt.DraftBillingProfileInput draftBillingProfileInput = new SwitchOutWizardCnt.DraftBillingProfileInput();
        draftBillingProfileInput.oldCaseList = caseList;
        draftBillingProfileInput.billingProfileId = billingProfile.Id;
        Object response = TestUtils.exec(
                'SwitchOutWizardCnt', 'saveDraftBP', draftBillingProfileInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        draftBillingProfileInput.oldCaseList = null;
        draftBillingProfileInput.billingProfileId = 'testInexistentAccountId';
        response = TestUtils.exec(
                'SwitchOutWizardCnt', 'saveDraftBP', draftBillingProfileInput, true);
        Test.stopTest();
    }
}