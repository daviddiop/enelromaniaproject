/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 23, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_CampaignMemberStatusConfig {

    public static MRO_QR_CampaignMemberStatusConfig getInstance() {
        return (MRO_QR_CampaignMemberStatusConfig) ServiceLocator.getInstance(MRO_QR_CampaignMemberStatusConfig.class);
    }

    public Map<String, List<CampaignMemberStatusConfig__mdt>> getCampaignMemberStatusConfigsByCampaignTypes(Set<String> campaignTypes) {
        List<CampaignMemberStatusConfig__mdt> configs = [SELECT Id, CampaignType__c, MasterLabel, IsDefaultStatus__c, HasRespondedStatus__c, SortOrder__c
                                                         FROM CampaignMemberStatusConfig__mdt WHERE CampaignType__c IN :campaignTypes
                                                         ORDER BY CampaignType__c, SortOrder__c];
        Map<String, List<CampaignMemberStatusConfig__mdt>> result = new Map<String, List<CampaignMemberStatusConfig__mdt>>();
        for (CampaignMemberStatusConfig__mdt config : configs) {
            if (result.containsKey(config.CampaignType__c)) {
                result.get(config.CampaignType__c).add(config);
            }
            else {
                result.put(config.CampaignType__c, new List<CampaignMemberStatusConfig__mdt>{config});
            }
        }
        return result;
    }

}