public with sharing class FileMetadataQueries {

    public static FileMetadataQueries getInstance() {
        return new FileMetadataQueries();
    }

    public FileMetadata__c findById(String fileMetadataId) {
        List<FileMetadata__c> fileMetadataList = [
                SELECT
                        Id,Dossier__c,Dossier__r.Name,
                        Name,CreatedDate
                FROM FileMetadata__c
                WHERE Id = :fileMetadataId
                LIMIT 1
        ];
        FileMetadata__c fileMetadata = fileMetadataList.isEmpty() ? null : fileMetadataList.get(0);
        return fileMetadata;
    }

    public List<FileMetadata__c> listRecent(Integer sizeLimit, Datetime startDate) {
        return [
            SELECT Id, Title__c, DocumentId__c, CreatedDate
            FROM FileMetadata__c
            WHERE DocumentId__c != null
                AND CreatedDate > :startDate
            ORDER BY CreatedDate DESC
            LIMIT :sizeLimit
        ];
    }

    public FileMetadata__c findByTitle(String title) {
        List<FileMetadata__c> fileMetadataList = [
            SELECT Id, Name
            FROM FileMetadata__c
            WHERE Title__c = :title
            LIMIT 1
        ];
        return fileMetadataList.isEmpty() ? null : fileMetadataList.get(0);
    }
}