public with sharing class AccountContactRelationService {

    private static final ContactQueries contactQuerySrv = ContactQueries.getInstance();
    private static final AccountQueries accountQuery = AccountQueries.getInstance();
    private static final AccountContactRelationQueries accountContactRelationQuery = AccountContactRelationQueries.getInstance();

    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    public static AccountContactRelationService getInstance() {
        return (AccountContactRelationService)ServiceLocator.getInstance(AccountContactRelationService.class);
    }

    /**
    * @author BG
    * @description Add new Relations through Account and Contact or Person Account
    * @param customerList list of customer interaction
    * @param contact the new contact
    * @param personAcctContact or a new Person Contact Id from Person Account
    */
// TODO: ST Unused
//    public void addAccountContactRelations(List<CustomerInteraction__c> customerList, Contact contact, String personAcctContact) {
//        List<AccountContactRelation> accContRelationsList = new List<AccountContactRelation>();
//        for (Integer i = 1; i < customerList.size(); i++) {
//            AccountContactRelation newAccountContactRelation = new AccountContactRelation();
//            newAccountContactRelation.AccountId = customerList.get(i).Customer__c;
//            newAccountContactRelation.ContactId = contact != null ? contact.Id : personAcctContact;
//            accContRelationsList.add(newAccountContactRelation);
//        }
//        if (!accContRelationsList.isEmpty()) {
//            databaseSrv.insertSObject(accContRelationsList);
//        }
//    }

    public AccountContactRelation addAccountContactRelation(Id accountId, Id contactId, String roles) {
        AccountContactRelation acr = create(accountId, contactId);
        if (String.isBlank(roles)) {
            acr.Roles = roles;
        }
        databaseSrv.insertSObject(acr);
        return acr;
    }

    public AccountContactRelation create(String accountId, String contactId) {
        return new AccountContactRelation(
            AccountId = accountId,
            ContactId = contactId
        );
    }

    public void updateRole(String relationId, String role) {
        AccountContactRelation relation = new AccountContactRelation(
            Id = relationId,
            Roles = role
        );
        databaseSrv.updateSObject(relation);
    }


    public with sharing class AccountContactRelationDTO implements Comparable {
        @AuraEnabled
        public String id {get; set;}

        @AuraEnabled
        public String accountId {get; set;}
        @AuraEnabled
        public String accountName {get; set;}
        @AuraEnabled
        public String accountRecordType {get; set;}
        @AuraEnabled
        public String accountBusinessType {get; set;}
        @AuraEnabled
        public String accountVatOrId {get; set;}
        @AuraEnabled
        public Boolean isPersonAccount {get; set;}

        @AuraEnabled
        public String contactId {get; set;}
        @AuraEnabled
        public String contactName {get; set;}
        @AuraEnabled
        public String contactEmail {get; set;}
        @AuraEnabled
        public String contactPhone {get; set;}
        @AuraEnabled
        public String role {get; set;}

        public Integer compareTo(Object objectToCompareTo) {
            AccountContactRelationDTO relationToCompare = (AccountContactRelationDTO)objectToCompareTo;
            if (relationToCompare == null) {
                return 1;
            }
            if (this.isPersonAccount == relationToCompare.isPersonAccount) {
                return 0;
            } else if (this.isPersonAccount == true && relationToCompare.isPersonAccount == false) {
                return -1;
            } else {
                return 1;
            }
        }
    }
    
    public static AccountContactRelationDTO mapToDto(AccountContactRelation relation) {
        AccountContactRelationDTO relationDto = new AccountContactRelationDTO();
        relationDto.id = relation.Id;
        relationDto.accountId = relation.AccountId;
        relationDto.accountName = relation.Account.Name;
        relationDto.accountRecordType = relation.Account.RecordType.Name;
        relationDto.accountBusinessType = relation.Account.BusinessType__c;
        relationDto.isPersonAccount = relation.Account.IsPersonAccount;
        if (relation.Account.IsPersonAccount) {
            relationDto.accountVatOrId = relation.Account.NationalIdentityNumber__pc;
        } else {
            relationDto.accountVatOrId = relation.Account.VATNumber__c;
        }

        relationDto.contactId = relation.ContactId;
        relationDto.contactEmail = relation.Contact.Email;
        relationDto.contactPhone = relation.Contact.Phone;
        relationDto.contactName = relation.Contact.Name;
        relationDto.role = relation.Roles;
        return relationDto;
    }
    
    public static List<AccountContactRelationDTO> mapToDto(List<AccountContactRelation> relationList) {
        List<AccountContactRelationDTO> dtoList = new List<AccountContactRelationDTO>();
        for (AccountContactRelation relation : relationList) {
            dtoList.add(mapToDto(relation));
        }
        dtoList.sort();
        return dtoList;
    }

    public static AccountContactRelationDTO mapToDto(Account personAccount) {
        if (!personAccount.IsPersonAccount) {
            throw new WrtsException(System.Label.Invalid + ' - ' + System.Label.PersonAccount);
        }
        AccountContactRelationDTO relationDto = new AccountContactRelationDTO();
        relationDto.accountId = personAccount.Id;
        relationDto.accountName = personAccount.Name;
        relationDto.accountRecordType = personAccount.RecordType.Name;
        relationDto.accountBusinessType = personAccount.BusinessType__c;
        relationDto.isPersonAccount = true;
        relationDto.accountVatOrId = personAccount.NationalIdentityNumber__pc;
        relationDto.contactId = personAccount.PersonContactId;
        relationDto.contactEmail = personAccount.PersonEmail;
        relationDto.contactPhone = personAccount.PersonMobilePhone;
        relationDto.contactName = personAccount.Name;
        relationDto.role = Label.Customer;
        return relationDto;
    }

    /**
    *  @author Moussa Fofna
    * @description retrieve the list of contacts and the list of accounts by PersonIndividualId and retrieve
    *              the list of accountContactRelation by accountId and contactId and finish retrieve account
    *              with like Id equal accountContactRelation.AccountId
    * @param Account  the account selected
    * @return List account
    */

    public Static List<Account> listRelatedAccounts(Account account) {
        List<Contact> contacts =  contactQuerySrv.listContactByIndividualId(account.PersonIndividualId);
        Set<String> contactIds = new Set<String>();
        for(Contact contact : contacts ){
            contactIds.add(contact.Id);
        }
        List<Account> accounts = accountQuery.listByIndividualId(account.PersonIndividualId);
        Set<String> accountIds = new Set<String>();
        for(Account accountList : accounts ){
            if(accountList.Id != account.Id)
            accountIds.add(accountList.Id);
        }
        List <AccountContactRelation> accountContactRelations = accountContactRelationQuery.listByContactIdsORAccountIds(contactIds, accountIds);
        List<Id> accountListIds = new List<Id>();
        for(AccountContactRelation accountContactRelation : accountContactRelations ){
            accountListIds.add(accountContactRelation.AccountId);
        }
        List<Account> accounts2 = accountQuery.listByIds(accountListIds);
        return accounts2;
    }
}