/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   17/06/2020
* @desc
* @history
*/
global with sharing class MRO_WS_BankDirectDebitServices {
    private static final wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration PhaseManagerIntegration =
            (wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration');
    private static final MRO_UTL_Constants CONSTANTS = new MRO_UTL_Constants();

    webservice static wrts_prcgvr.MRR_1_0.MultiResponse post(wrts_prcgvr.MRR_1_0.MultiRequest multiRequest) {
        wrts_prcgvr.MRR_1_0.MultiResponse multiResponse = MRO_UTL_MRRMapper.initMultiResponse();
        Id dossierId;
        String stackTrace;
        Savepoint sp = Database.setSavepoint();
        try {
            for (wrts_prcgvr.MRR_1_0.Request request : multiRequest.requests) {
                System.debug('*****MRO_WS_BankDirectDebitServices  Request ' + request);
                wrts_prcgvr.MRR_1_0.Response response;
                Map<String, Object> result;

                result = processRequest(request);

                if (response == null) response = (wrts_prcgvr.MRR_1_0.Response) result.get('response');

                if (result != null && result.get('dossierId') InstanceOf Id) {
                    System.debug('Phase Transition1');
                    dossierId = (Id) result.get('dossierId');
                    String outcome = result.containsKey('outcome') ? String.valueOf(result.get('outcome')) : null;


                    if (outcome == 'KO') {
                        response.code = 'KO';
                        response.description = result.containsKey('errorMessage') ? String.valueOf(result.get('errorMessage')) : null;
                    }

                    if (response != null && response.code == 'OK' && (outcome == null || outcome == 'OK') && dossierId != null) {
                        System.debug('Phase Transition2');

//                        Id objectId = caseId;
                        Schema.SObjectType sobjectType = Schema.Case.getSObjectType();
                        String sobjectName = sobjectType.getDescribe().getName();
                        Set<String> objectFields = sobjectType.getDescribe().fields.getMap().keySet();
                        List<wrts_prcgvr__PhaseManagerSObjectSetting__c> phaseManagerSObjectSettingList = [SELECT wrts_prcgvr__ObjectType__c, wrts_prcgvr__PivotField__c, wrts_prcgvr__TypeField__c From wrts_prcgvr__PhaseManagerSObjectSetting__c where wrts_prcgvr__ObjectType__c = :String.valueof(sobjectType)];
                        if (phaseManagerSObjectSettingList.isEmpty()) continue;
                        List<SObject> objList = Database.query('SELECT ' + String.join((Iterable<String>) objectFields, ',') + ' FROM ' + sobjectName + ' WHERE Dossier__c = :dossierId');
                        for(SObject obj : objList){
                            checkAndApplyAutomaticTransitionWithTagAndContext(obj, CONSTANTS.CONFIRM_TAG, request.header.requestType);
                        }

                    }
                }
                multiResponse.responses.add(response);
            }
        }
        catch (Exception e) {
            Database.rollback(sp);
            wrts_prcgvr.MRR_1_0.Response response = new wrts_prcgvr.MRR_1_0.Response();
            response.code = 'KO';
            response.header = multiRequest.requests[0].header;
            response.header.requestTimestamp = String.valueOf(System.now());
            response.description = e.getMessage();
            multiResponse.responses.add(response);
            stackTrace = e.getStackTraceString();
            system.debug('Error1: ' + e.getMessage() + e.getStackTraceString());
        }
        finally {
            createAndSaveLog(multiRequest, multiResponse, dossierId, stackTrace);
        }
        return multiResponse;
    }

    static Map<String, Object> processRequest (wrts_prcgvr.MRR_1_0.Request request) {
        wrts_prcgvr.MRR_1_0.Response response;
        Map<String, Object> result = new Map<String, Object>();

        try{
            response = MRO_UTL_MRRMapper.initResponse(request);
            result.put('response', response);
            Case c;
            ContractAccount__c contractAccount;
            Map<string, string> billingProfileFieldsMap, bankFieldsMap;
            String billingAccountNumber, iban;
            List<ContractAccount__c> caList = new List<ContractAccount__c>();
            Dossier__c dossier;

            List<wrts_prcgvr.MRR_1_0.WObject> CaseListMRR = MRO_UTL_MRRMapper.selectWobjects('Case', request);
            System.debug('******CaseList ' + CaseListMRR);

            if(CaseListMRR != null && !CaseListMRR.isEmpty()){
                c = (Case) MRO_UTL_MRRMapper.wobjectToSObject(CaseListMRR[0]);
            } else throw new WrtsException('Case in MRR');

            List<wrts_prcgvr.MRR_1_0.WObject> billingProfileList = MRO_UTL_MRRMapper.selectWobjects('Case/BillingProfile__r', request);
            System.debug('******billingProfileList ' + billingProfileList);

            if(billingProfileList != null && !billingProfileList.isEmpty()){
                billingProfileFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(billingProfileList[0]);
                if(String.isBlank(billingProfileFieldsMap.get('IBAN__c'))) throw new WrtsException('Cont Bancar is missing');
                if(String.isBlank(billingProfileFieldsMap.get('BankName__c'))) throw new WrtsException('Bank Name is missing');
                iban = billingProfileFieldsMap.get('IBAN__c');
            } else throw new WrtsException('Missing Billing Profile in MRR');

            List<wrts_prcgvr.MRR_1_0.WObject> bankList = MRO_UTL_MRRMapper.selectWobjects('Case/BillingProfile__r/Bank__r', request);
            System.debug('******bankList ' + bankList);

            if(bankList != null && !bankList.isEmpty()){
                bankFieldsMap = MRO_UTL_MRRMapper.wobjectToMap(bankList[0]);
                if(String.isBlank(bankFieldsMap.get('BIC__c'))) throw new WrtsException('BIC missing');
            } else throw new WrtsException('Missing Bank in MRR');

            List<wrts_prcgvr.MRR_1_0.WObject> contractAccountList = MRO_UTL_MRRMapper.selectWobjects('Case/ContractAccount__r', request);
            System.debug('******contractAccountList ' + contractAccountList);

            if(contractAccountList != null && !contractAccountList.isEmpty()){
                ContractAccount__c ca = (ContractAccount__c) MRO_UTL_MRRMapper.wobjectToSObject(contractAccountList[0]);
                if(String.isBlank(ca.BillingAccountNumber__c)) throw new WrtsException('Cod Plata is missing');
                billingAccountNumber = ca.BillingAccountNumber__c;

                Set<String> queryFields = contractAccountQueryFields();

                String whereCondition = getWhereCondition(c.SubProcess__c);

                String contractAccountQuery =
                        'SELECT ' +
                                String.join(new List<String>(queryFields), ', ') +
                                ' FROM ContractAccount__c' + whereCondition;

                caList = Database.query(contractAccountQuery);

                if(caList.isEmpty()) throw new WrtsException('Contract Account not found for Cod Plata ' + ca.BillingAccountNumber__c);
            } else throw new WrtsException('Missing Contract Account in MRR');

            if(c.SubProcess__c == 'Activation'){
                contractAccount = selectContractAccount(caList);
                dossier = createAndInsertDossier(contractAccount.Account__c, contractAccount.CompanyDivision__c, contractAccount.Commodity__c);
            }

            if(c.SubProcess__c == 'Deactivation'){
                dossier = createAndInsertDossier(caList[0].Account__c, caList[0].CompanyDivision__c, caList[0].Commodity__c);
                List<Case> caseList = new List<Case>();
                for(ContractAccount__c ca : caList){
                    Case deactivationCase = (Case) MRO_UTL_MRRMapper.wobjectToSObject(CaseListMRR[0]);
                    fillCaseField(deactivationCase, dossier.Id, ca.Id, ca.Account__c, ca.CompanyDivision__c, ca.BillingProfile__c);
                    caseList.add(deactivationCase);
                }
                if(!caseList.isEmpty()) insert caseList;
            }
//            else if(c.SubProcess__c == 'Deactivation' && billingProfileFieldsMap.get('IBAN__c') != contractAccount.BillingProfile__r.IBAN__c && bankFieldsMap.get('BIC__c') != contractAccount.BillingProfile__r.Bank__r.BIC__c) throw new WrtsException('NEPROCESAT, reason: not existing IBAN and BIC');
//            else if(c.SubProcess__c == 'Deactivation' && billingProfileFieldsMap.get('IBAN__c') != contractAccount.BillingProfile__r.IBAN__c && bankFieldsMap.get('BIC__c') == contractAccount.BillingProfile__r.Bank__r.BIC__c) throw new WrtsException('NEPROCESAT, reason: not existing IBAN');
//            else if(c.SubProcess__c == 'Deactivation' && billingProfileFieldsMap.get('IBAN__c') == contractAccount.BillingProfile__r.IBAN__c && bankFieldsMap.get('BIC__c') != contractAccount.BillingProfile__r.Bank__r.BIC__c) throw new WrtsException('NEPROCESAT, reason: not existing BIC');

            if(c.SubProcess__c == 'Activation'){
                if(billingProfileFieldsMap.get('IBAN__c') == contractAccount.BillingProfile__r.IBAN__c){
                    fillCaseField(c, dossier.Id, contractAccount.Id, contractAccount.Account__c, contractAccount.CompanyDivision__c, contractAccount.BillingProfile__c);
                } else {
                    BillingProfile__c billingProfile = createAndInsertBillingProfile(contractAccount, billingProfileFieldsMap.get('IBAN__c'), c.StartDate__c, contractAccount.Account__c, bankFieldsMap.get('BIC__c'));
                    fillCaseField(c, dossier.Id, contractAccount.Id, contractAccount.Account__c, contractAccount.CompanyDivision__c, billingProfile.Id);
                }
                insert c;
            }

//            result.put('caseId', c.Id);
            result.put('dossierId', dossier.Id);

            System.debug('Result: ' + result);

//            Case cs = [SELECT CaseNumber, format(CreatedDate), Status, Description FROM Case WHERE Id = :c.Id];
//            cs.Status = 'Procesat';
//            cs.Description = 'Description';
//            if(c.SubProcess__c == 'Activation' && contractAccount.BillingProfile__r.PaymentMethod__c == 'Direct Debit' && contractAccount.BillingProfile__r.IBAN__c == billingProfileFieldsMap.get('IBAN__c')){
//                cs.Status = 'Partial Procesat';
//                cs.Description = 'The option Direct Debit is already active with identical IBAN, DATA is different. Direct Debit deleted and case created/loaded again for activation';
//            }
//            Wrts_prcgvr.MRR_1_0.WObject caseMRR = MRO_UTL_MRRMapper.sObjectToWObject(cs, 'Case');
//            response.objects.add(caseMRR);

            List<Case> csList = [SELECT CaseNumber, format(CreatedDate), Status, Description, Dossier__r.Name FROM Case WHERE Dossier__c = :dossier.Id];
//            for(Case cs : csList){
                Case cs = csList[0];
                cs.Status = 'Procesat';
                cs.Description = '';
                if(c.SubProcess__c == 'Activation' && contractAccount.BillingProfile__r.PaymentMethod__c == 'Direct Debit' && contractAccount.BillingProfile__r.IBAN__c == billingProfileFieldsMap.get('IBAN__c')){
                    cs.Status = 'Partial Procesat';
                    cs.Description = 'The option Direct Debit is already active with identical IBAN, DATA is different. Direct Debit deleted and case created/loaded again for activation';
                }
                Wrts_prcgvr.MRR_1_0.WObject caseMRR = MRO_UTL_MRRMapper.sObjectToWObject(cs, 'Case');
                for(wrts_prcgvr.MRR_1_0.Field field : caseMRR.fields){
                    if(field.name == 'CaseNumber'){
                        field.value = cs.Dossier__r.Name;
                    }
                }
                response.objects.add(caseMRR);
//            }
        } catch (Exception e) {
            response.Code = 'KO';
            String description = e.getMessage() + ' ' + e.getStackTraceString();
            response.description = description;
            system.debug('Error2: ' + e.getMessage() + e.getStackTraceString());
        }
        return result;
    }

    private static void createAndSaveLog (wrts_prcgvr.MRR_1_0.MultiRequest multiRequest, wrts_prcgvr.MRR_1_0.MultiResponse multiResponse, Id dossierId, String stackTrace) {
        wrts_prcgvr.MRR_1_0.Request request = multiRequest.requests[0];
        wrts_prcgvr.MRR_1_0.Response response = multiResponse.responses[0];
        String code = String.isNotBlank(response.description) ? response.code + ' - ' + response.description : response.code;
        String messageString = MRO_UTL_MRRMapper.serializeMultirequest(multiRequest);
        String responseString = MRO_UTL_MRRMapper.serializeMultiResponse(multiResponse);
        if(dossierId == null){
            MRO_SRV_ExecutionLog.createIntegrationLog('CallIn', request.header.requestId, request.header.requestType, null, messageString, stackTrace, code, responseString, null, null, request.header.requestId, 'Tibco Massive Creation', null);
        } else {
            for(Case c : [SELECT Id FROM Case WHERE Dossier__c = :dossierId]){
                MRO_SRV_ExecutionLog.createIntegrationLog('CallIn', request.header.requestId, request.header.requestType, c.Id, messageString, stackTrace, code, responseString, null, null, request.header.requestId, 'Tibco Massive Creation', null);
            }
        }
    }

    private static void checkAndApplyAutomaticTransitionWithTagAndContext(SObject record, String tag, String context) {
        System.debug('### Record: '+record);
        System.debug('### Tag: '+tag);
        System.debug('### Context: '+context);
        List<wrts_prcgvr__PhaseTransition__c> phaseTransitions = (List<wrts_prcgvr__PhaseTransition__c>) PhaseManagerIntegration.getTransitions(new Map<String, Object>{
                'object' => record,
                'type' => 'A',
                'context' => context,
                'tags' => tag
        });
        //get transitions END
        System.debug('### Found transitions: '+phaseTransitions);

        //apply transitions START
        if (phaseTransitions.size() == 1) {
            PhaseManagerIntegration.applyTransition(new Map<String, Object>{
                    'object' => record,
                    'transition' => phaseTransitions[0],
                    'context' => context
            });
        }
    }

    private static Set<String> contractAccountQueryFields(){
        Set<String> queryFields = new Set<String>();
        queryFields.add('Id');
        queryFields.add('Account__c');
        queryFields.add('BillingAccountNumber__c');
        queryFields.add('BillingProfile__r.Bank__c');
        queryFields.add('BillingProfile__r.Bank__r.BIC__c');
        queryFields.add('BillingProfile__r.BillingAddress__c');
        queryFields.add('BillingProfile__r.BillingAddressKey__c');
        queryFields.add('BillingProfile__r.BillingAddressNormalized__c');
        queryFields.add('BillingProfile__r.BillingApartment__c');
        queryFields.add('BillingProfile__r.BillingBlock__c');
        queryFields.add('BillingProfile__r.BillingBuilding__c');
        queryFields.add('BillingProfile__r.BillingCity__c');
        queryFields.add('BillingProfile__r.BillingCountry__c');
        queryFields.add('BillingProfile__r.BillingFloor__c');
        queryFields.add('BillingProfile__r.BillingLocality__c');
        queryFields.add('BillingProfile__r.BillingPostalCode__c');
        queryFields.add('BillingProfile__r.BillingProvince__c');
        queryFields.add('BillingProfile__r.BillingStreetId__c');
        queryFields.add('BillingProfile__r.BillingStreetName__c');
        queryFields.add('BillingProfile__r.BillingStreetNumber__c');
        queryFields.add('BillingProfile__r.BillingStreetNumberExtn__c');
        queryFields.add('BillingProfile__r.BillingStreetType__c');
        queryFields.add('BillingProfile__r.CertifiedEmail__c');
        queryFields.add('BillingProfile__r.DeliveryChannel__c');
        queryFields.add('BillingProfile__r.DirectDebitActivationDate__c');
        queryFields.add('BillingProfile__r.Email__c');
        queryFields.add('BillingProfile__r.Fax__c');
        queryFields.add('BillingProfile__r.IBAN__c');
        queryFields.add('BillingProfile__r.MobilePhone__c');
        queryFields.add('BillingProfile__r.PaymentMethod__c');
        queryFields.add('BillingProfile__r.RefundBank__c');
        queryFields.add('BillingProfile__r.RefundIBAN__c');
        queryFields.add('BillingProfile__r.RefundMethod__c');
        queryFields.add('BillingProfile__r.SAPStatus__c');
        queryFields.add('BillingProfile__r.UMR__c');
        queryFields.add('CompanyDivision__c');
        queryFields.add('Commodity__c');
        queryFields.add('(SELECT Id, Status__c FROM Supplies__r)');
        return queryFields;
    }

    private static String getWhereCondition(String subProcess){
        String whereConditionDeactivation = ' WHERE BillingAccountNumber__c = :billingAccountNumber AND BillingProfile__r.PaymentMethod__c = \'Direct Debit\' AND BillingProfile__r.IBAN__c = :iban';
        String whereConditionActivation = ' WHERE BillingAccountNumber__c = :billingAccountNumber';

        switch on subProcess.toUpperCase() {
            when 'DEACTIVATION' {
                return whereConditionDeactivation;
            }
            when 'ACTIVATION' {
                return whereConditionActivation;
            }
            when else {
                return null;
            }
        }
    }

    private static ContractAccount__c selectContractAccount(List<ContractAccount__c> contractAccountList){
            Map<Id, ContractAccount__c> contractAccountWithActiveSupplies = new  Map<Id, ContractAccount__c>();
            Map<Id, ContractAccount__c> contractAccountWithActivatingSupplies = new  Map<Id, ContractAccount__c>();
            Map<Id, ContractAccount__c> contractAccountWithTerminatingSupplies = new  Map<Id, ContractAccount__c>();

            for(ContractAccount__c contractAccount : contractAccountList){
                System.debug('contractAccount: ' + contractAccount);
                for(Supply__c supply : contractAccount.Supplies__r){
                    System.debug('supply.Status__c: ' + supply.Status__c);
                    switch on supply.Status__c {
                        when 'Active' {
                            contractAccountWithActiveSupplies.put(contractAccount.Id, contractAccount);
                        }
                        when 'Activating' {
                            contractAccountWithActivatingSupplies.put(contractAccount.Id, contractAccount);
                        }
                        when 'Terminating' {
                            contractAccountWithTerminatingSupplies.put(contractAccount.Id, contractAccount);
                        }
                    }
                }
            }
            System.debug('contractAccountWithActiveSupplies: ' + contractAccountWithActiveSupplies);
            System.debug('contractAccountWithActivatingSupplies: ' + contractAccountWithActivatingSupplies);
            System.debug('contractAccountWithTerminatingSupplies: ' + contractAccountWithTerminatingSupplies);
            if(!contractAccountWithActiveSupplies.isEmpty()) return contractAccountWithActiveSupplies.values()[0];
            if(!contractAccountWithActivatingSupplies.isEmpty()) return contractAccountWithActivatingSupplies.values()[0];
            if(!contractAccountWithTerminatingSupplies.isEmpty()) return contractAccountWithTerminatingSupplies.values()[0];

        return null;
    }

    private static Dossier__c createAndInsertDossier(Id accountId, Id companyDivisionId, String commodity){
        Id dossierRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('BankDirectDebit').getRecordTypeId();
        MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
        Dossier__c dossier = dossierSrv.generateDossier(accountId, null, null, companyDivisionId, dossierRecordTypeId, 'DirectDebit', commodity);
        dossier.Origin__c = MRO_UTL_Constants.ORIGIN_INTERNAL;
        dossier.Channel__c = MRO_UTL_Constants.CHANNEL_BO_CUCA;
        dossier.Status__c = 'New';
        update dossier;
        return dossier;
    }

    private static void fillCaseField(Case c, Id dossierId, Id contractAccountId, Id accountId, Id companyDivisionId, Id billingProfileId){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BankDirectDebit').getRecordTypeId();
        c.Dossier__c = dossierId;
        c.RecordTypeId = caseRecordTypeId;
        c.ReferenceDate__c = Date.today();
        c.EffectiveDate__c = Date.today();
        c.SLAExpirationDate__c = Date.today().addDays(60);
        c.CaseTypeCode__c = 'C35';
        c.Origin = MRO_UTL_Constants.ORIGIN_INTERNAL;
        c.Channel__c = MRO_UTL_Constants.CHANNEL_BO_CUCA;
        c.ContractAccount__c = contractAccountId;
        c.AccountId = accountId;
        c.CompanyDivision__c = companyDivisionId;
        c.BillingProfile__c = billingProfileId;
    }

    private static BillingProfile__c createAndInsertBillingProfile(ContractAccount__c contractAccount, String iban, Date startDate, Id AccountId, String bic){
        BillingProfile__c billingProfile = contractAccount.BillingProfile__r.clone(false, true);
        billingProfile.IBAN__c = iban;
        billingProfile.PaymentMethod__c = 'Direct Debit';
        billingProfile.DirectDebitActivationDate__c = startDate;
        billingProfile.Account__c = AccountId;
        billingProfile.SAPStatus__c = 'New';
        billingProfile.Bank__c = null;
        List<Bank__c> banks = [SELECT Id, BIC__c, Name From Bank__c where BIC__c = :bic];

        if(!banks.isEmpty()) billingProfile.Bank__c = banks[0].Id;
        else throw new WrtsException('Missing Bank in MRO CRM database');

        insert billingProfile;
        return billingProfile;
    }
}