/**
 * @author  Stefano Porcari
 * @since   Jan 7, 2020
 * @desc   Batch class for updating phase trasition of Case sObject
 * 
 */
global with sharing class MRO_BA_CasePhaseUpdate implements Database.Batchable<SObject> {
    private Id dossierId;
    private Set<Id> caseIds;
    public MRO_UTL_Transitions.PhaseTransitionDTO  phaseTransitionDTO;
    private Boolean checkActivities = false;
    private Boolean handleDependencies = false;

    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    public MRO_BA_CasePhaseUpdate(MRO_UTL_Transitions.PhaseTransitionDTO  phaseTransitionDTO) {
        this(phaseTransitionDTO, false, false);
    }

    public MRO_BA_CasePhaseUpdate(MRO_UTL_Transitions.PhaseTransitionDTO  phaseTransitionDTO, Boolean checkActivities, Boolean handleDependencies) {
        this.phaseTransitionDTO = phaseTransitionDTO;
        this.dossierId = phaseTransitionDTO.objectId;
        this.checkActivities = checkActivities;
        this.handleDependencies = handleDependencies;
    }

    public MRO_BA_CasePhaseUpdate(Set<Id> caseIds, MRO_UTL_Transitions.PhaseTransitionDTO  phaseTransitionDTO) {
        this(caseIds, phaseTransitionDTO, false, false);
    }

    public MRO_BA_CasePhaseUpdate(Set<Id> caseIds, MRO_UTL_Transitions.PhaseTransitionDTO  phaseTransitionDTO, Boolean checkActivities, Boolean handleDependencies) {
        this.phaseTransitionDTO = phaseTransitionDTO;
        this.caseIds = caseIds;
        this.checkActivities = checkActivities;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, ' +
                            'CaseGroup__c, ' +
                            'CaseNumber, ' +
                            'CaseTypeCode__c, ' +
                            'Channel__c, ' +
                            'CompanyDivision__c, ' +
                            'EffectiveDate__c, ' +
                            'ExternalRequestID__c, ' +
                            'IntegrationKey__c, ' +
                            'IsDisCoENEL__c, ' +
                            'Market__c, ' +
                            'NeedsDesigner__c, ' +
                            'NeedsExecutor__c, ' +
                            'Origin, ' +
                            'Outcome__c, ' +
                            'Phase__c, ' +
                            'Reason__c, ' +
                            'RecordTypeId, ' +
                            'ReferenceDate__c, ' +
                            'RequiredForConnection__c, ' +
                            'SLAExpirationDate__c, ' +
                            'StartDate__c, ' +
                            'Status, ' +
                            'SubProcess__c, ' +
                            'SubType__c, ' +
                            'SuspendChildren__c, ' +
                            'Type ' +
                       'FROM Case ';
        if (this.caseIds != null) {
            query += 'WHERE Id IN :caseIds';
        }
        else {
            query += 'WHERE Dossier__c =\'' + phaseTransitionDTO.objectId + '\'';
        }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
        //wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration PhaseManagerIntegration = (wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration)
        //        wrts_prcgvr.VersionManager.newClassInstance(constantsSrv.PHASE_MANAGER_INTEGRATION_CLASS);
        for (SObject caseObject : scope) {
            Case checkedCase = (Case) caseObject;
            Map<String, Object> fieldsUpdateMap=phaseTransitionDTO.fieldsUpdateMap;
            if(fieldsUpdateMap !=null){
                for (String field: fieldsUpdateMap.keySet()){
                    checkedCase.put(field, fieldsUpdateMap.get(field));
                }
            }
            Map<String, Object> transitionResult = transitionsUtl.checkAndApplyAutomaticTransitionWithTag(caseObject, this.phaseTransitionDTO.transitionTags, this.checkActivities, this.handleDependencies);
            /*
            if (this.checkActivities) {
                Map<Id, List<wrts_prcgvr__Activity__c>> mandatoryActivities = MRO_QR_Activity.getInstance().listOpenMandatoryActivitiesByParentIds(new Set<Id>{checkedCase.Id});
                System.debug('Mandatory open activities: '+mandatoryActivities);
                if (mandatoryActivities.containsKey(checkedCase.Id)) {
                    continue;
                }
            }
            Map<String, Object> transQueryMap = new Map<String, Object>{
                    constantsSrv.PHASE_QRY_MAP_KEY_OBJECT => checkedCase,
                    constantsSrv.PHASE_QRY_MAP_KEY_TYPE => phaseTransitionDTO.transitionType,
                    constantsSrv.PHASE_QRY_MAP_KEY_TAGS => phaseTransitionDTO.transitionTags
            };
            List<wrts_prcgvr__PhaseTransition__c> transList = (List<wrts_prcgvr__PhaseTransition__c>) PhaseManagerIntegration.getTransitions(transQueryMap);
            if (!transList.isEmpty()) {
                wrts_prcgvr__PhaseTransition__c transition = transList.get(0);
                Map<String, Object> appliedTransMap = new Map<String, Object>{
                        constantsSrv.PHASE_TRANS_APPLY_MAP_KEY_OBJECT => checkedCase,
                        constantsSrv.PHASE_TRANS_APPLY_MAP_KEY_TRANS => transition
                };
                Map<String, Object> transResultMap = (Map<String, Object>) PhaseManagerIntegration.applyTransition(appliedTransMap);
            }
             */
        }
    }
    global void finish(Database.BatchableContext BC) {

    }

}