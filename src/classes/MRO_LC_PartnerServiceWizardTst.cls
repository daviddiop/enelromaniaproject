@isTest
public class MRO_LC_PartnerServiceWizardTst {
    final static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();

    @testSetup
    static void setup() {
        //Insert Sequencer
        Sequencer__c sequencer = new Sequencer__c(Sequence__c = 1, SequenceLength__c = 1, Type__c = 'CustomerCode');
        insert sequencer;

        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = companyDivision.Id;
        user.CompanyDivisionEnforced__c = true;
        update user;

        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert individual;

        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().setInterlocutor(individual.Id).build();
        interaction.Status__c = 'New';
        insert interaction;

        Account account = MRO_UTL_TestDataFactory.account().businessAccount().build();
        account.Name = 'BusinessAccount1';
        account.BusinessType__c = 'Embassy';
        insert account;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = account.Id;
        opportunity.RequestType__c = constantsSrv.REQUEST_TYPE_PARTNER_SERVICE;
        insert opportunity;

        Dossier__c dossier = TestDataFactory.dossier().setAccount(account.Id).build();
        dossier.RequestType__c = opportunity.RequestType__c;
        dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Acquisition').getRecordTypeId();
        dossier.Opportunity__c = opportunity.Id;
        insert dossier;

        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = account.Id;
        contact.IndividualId = individual.Id;
        insert contact;

        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
        supply.Status__c = 'Active';
        supply.Key__c = 'supply';
        supply.RecordTypeId = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId();
        supply.Account__c = account.Id;
        supply.CompanyDivision__c = companyDivision.Id;
        insert supply;
    }

    static OpportunityServiceItem__c createOpportunityServiceItem(Id opportunityId, Id accountId) {
        OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Account__c = accountId;
        opportunityServiceItem.Opportunity__c = opportunityId;
        opportunityServiceItem.PointStreetType__c = 'STRADA';
        opportunityServiceItem.PointStreetName__c = 'Sfanta Maria';
        opportunityServiceItem.PointStreetNumber__c = '2';
        opportunityServiceItem.PointStreetNumberExtn__c = '1';
        opportunityServiceItem.PointCity__c = 'SECTOR 1';
        opportunityServiceItem.PointProvince__c = 'BUCURESTI';
        opportunityServiceItem.PointPostalCode__c = '11496';
        opportunityServiceItem.PointCountry__c = 'ROMANIA';
        opportunityServiceItem.RecordTypeId = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();
        return opportunityServiceItem;
    }

    static OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id product2Id, PricebookEntry pricebookEntry) {
        OpportunityLineItem opportunityLineItem = TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityID = opportunityId;
        opportunityLineItem.Product2ID = product2Id;
        opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        return opportunityLineItem;
    }

    static Contract createContract(Id opportunityId, Id accountId) {
        Contract contract = MRO_UTL_TestDataFactory.Contract().build();
        contract.Opportunity__c = opportunityId;
        contract.AccountId = accountId;
        contract.ContractType__c = 'Business';
        contract.ContractTerm = 12;
        contract.StartDate = Date.today();
        return contract;
    }

    static PricebookEntry createPriceBookEntry(Id product2Id) {
        PricebookEntry myPicebookEntry = TestDataFactory.pricebookEntry().build();
        myPicebookEntry.Product2Id = product2Id;
        myPicebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        return myPicebookEntry;
    }

    static Map<String, Object> vasActivation() {
        Account account = [
            SELECT Id, Name
            FROM Account
            LIMIT 1
        ];

        Opportunity initialOpportunity = [
            SELECT Id, StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];
        System.debug('initialOpportunity ' + initialOpportunity.Id);

        Contract initialContract = createContract(initialOpportunity.Id, account.Id);
        insert initialContract;

        initialOpportunity.SubProcess__c = constantsSrv.PARTNER_SERVICE_VAS_ACTIVATION;
        initialOpportunity.ContractId = initialContract.Id;
        update initialOpportunity;



        OpportunityServiceItem__c opportunityServiceItem = createOpportunityServiceItem(initialOpportunity.Id, account.Id);
        insert opportunityServiceItem;

        Product2 product2 = TestDataFactory.product2().build();
        product2.Name = 'Test Product';
        insert product2;

        PricebookEntry pricebookEntry = createPriceBookEntry(product2.Id);
        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = createOpportunityLineItem(initialOpportunity.Id, product2.Id, pricebookEntry);
        insert opportunityLineItem;

        Dossier__c initialDossier = [
            SELECT Id, Status__c, SubProcess__c
            FROM Dossier__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
        'currentFlow' => constantsSrv.PARTNER_SERVICE_VAS_ACTIVATION,
        'opportunityId' => initialOpportunity.Id,
        'contractId' => initialContract.Id,
        'dossierId' => initialDossier.Id,
        'additionalInfo' => 'null',
        'commodity' => 'Electric',
        'stage' => 'Closed Won'
        };
        return (Map<String, Object>) TestUtils.exec('MRO_LC_PartnerServiceWizard', 'updateOpportunity', inputJSON, true);
    }

    static Map<String, Object> vasTermination() {
        Account account = [
            SELECT Id, Name
            FROM Account
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];

        Opportunity initialOpportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        initialOpportunity.AccountId = account.Id;
        initialOpportunity.RequestType__c = constantsSrv.REQUEST_TYPE_PARTNER_SERVICE;
        initialOpportunity.SubProcess__c = constantsSrv.PARTNER_SERVICE_VAS_TERMINATION;
        insert initialOpportunity;

        Dossier__c initialDossier = TestDataFactory.dossier().setAccount(account.Id).build();
        initialDossier.RequestType__c = initialOpportunity.RequestType__c;
        initialDossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
        initialDossier.Opportunity__c = initialOpportunity.Id;
        insert initialDossier;

        Supply__c supplyToTerminate = [
            SELECT Id, Status__c, RecordType.DeveloperName, Contract__c, Activator__c
            FROM Supply__c
            WHERE RecordType.DeveloperName = 'Service'
        ];

        Map<String, String > inputJSON = new Map<String, String>{
        'opportunityId' => initialOpportunity.Id,
        'currentFlow' => 'Termination',
        'dossierId' => initialDossier.Id,
        'additionalInfo' => 'null',
        'selectedToTerminateSupplyId' => supplyToTerminate.Id,
        'terminationReason' => Label.FinancialReason,
        'commodity' => '',
        'stage' => 'Closed Won'
        };

        return (Map<String, Object>) TestUtils.exec('MRO_LC_PartnerServiceWizard', 'updateOpportunity', inputJSON, true);
    }

    static Map<String, Object> vasSubstitution() {
        Account account = [
            SELECT Id, Name
            FROM Account
            LIMIT 1
        ];

        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];

        Opportunity substitutionOpportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        substitutionOpportunity.AccountId = account.Id;
        substitutionOpportunity.RequestType__c = constantsSrv.REQUEST_TYPE_PARTNER_SERVICE;
        substitutionOpportunity.SubProcess__c = constantsSrv.PARTNER_SERVICE_VAS_SUBSTITUTION;
        insert substitutionOpportunity;

        Dossier__c substitutionDossier = TestDataFactory.dossier().setAccount(account.Id).build();
        substitutionDossier.RequestType__c = substitutionOpportunity.RequestType__c;
        substitutionDossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Acquisition').getRecordTypeId();
        substitutionDossier.Opportunity__c = substitutionOpportunity.Id;
        insert substitutionDossier;

        Contract substitutionContract = createContract(substitutionOpportunity.Id, account.Id);
        insert substitutionContract;

        Product2 product2 = TestDataFactory.product2().build();
        product2.Name = 'New Product';
        insert product2;

        PricebookEntry pricebookEntry = createPriceBookEntry(product2.Id);
        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = createOpportunityLineItem(substitutionOpportunity.Id, product2.Id, pricebookEntry);
        insert opportunityLineItem;

        List<Supply__c> supplies = [
            SELECT Id, Opportunity__c, Status__c, RecordType.DeveloperName, Contract__c, Activator__c
            FROM Supply__c
            WHERE RecordType.DeveloperName = 'Service'
        ];
        Supply__c supplyToUpdate = supplies.get(0);

        Map<String, String > inputJSON = new Map<String, String>{
        'opportunityId' => substitutionOpportunity.Id,
        'selectedToTerminateSupplyId' => supplyToUpdate.Id,
        'contractId' => substitutionContract.Id,
        'dossierId' => substitutionDossier.Id,
        'currentFlow' => 'Substitution',
        'additionalInfo' => 'null',
        'terminationReason' => Label.FinancialReason,
        'commodity' => '',
        'stage' => 'Closed Won'
        };

        return (Map<String, Object>) TestUtils.exec('MRO_LC_PartnerServiceWizard', 'updateOpportunity', inputJSON, true);
    }

    @isTest
    static void testVasActivation() {
        Test.startTest();
        Map<String, Object> result = vasActivation();
        Test.stopTest();

        System.assertEquals(false, result.get('error'));

        Dossier__c resultingDossier = [
            SELECT Id, Status__c, SubProcess__c
            FROM Dossier__c
        ];

        Case resultingCase = [
            SELECT Id, Supply__c, EffectiveDate__c, Reason__c
            FROM Case
            WHERE Dossier__c =: resultingDossier.Id
        ];

        Supply__c resultingSupply = [
            SELECT Id, Status__c, RecordType.DeveloperName, Contract__c, Activator__c
            FROM Supply__c
            WHERE id = :resultingCase.Supply__c
            LIMIT 1
        ];

        List<Contract> contracts = [
            SELECT Id, Status
            FROM Contract
            WHERE Id = :resultingSupply.Contract__c
        ];

        System.assertEquals(1, contracts.size());
        Contract resultingContract = contracts.get(0);

        // Check dossier
        System.assertEquals('New', resultingDossier.Status__c);
        System.assertEquals(constantsSrv.PARTNER_SERVICE_VAS_ACTIVATION, resultingDossier.SubProcess__c);

        // Check contract
        System.assertEquals(constantsSrv.CONTRACT_STATUS_ACTIVATED, resultingContract.Status);

        // Check supply
        System.assertEquals(constantsSrv.SUPPLY_STATUS_ACTIVATING, resultingSupply.Status__c);
        System.assertEquals(resultingContract.Id, resultingSupply.Contract__c);
        // TODO: check for product on supply

    }

    @isTest
    static void testVasTermination() {
        Map<String, Object> vasActivationResult = vasActivation();
        System.assertEquals(false, vasActivationResult.get('error'));

        Test.startTest();
        Map<String, Object> resultFromTermination = vasTermination();
        Test.stopTest();
        System.assertEquals(false, resultFromTermination.get('error'));

        Opportunity terminationOpportunity = [
            SELECT Id
            FROM Opportunity
            WHERE SubProcess__c = :constantsSrv.PARTNER_SERVICE_VAS_TERMINATION
        ];
        System.debug('terminationOpportunity ' + terminationOpportunity.Id);

        Dossier__c resultingDossier = [
            SELECT Id, Status__c, SubProcess__c, RecordTypeId, RecordType.DeveloperName
            FROM Dossier__c
            WHERE Opportunity__c = :terminationOpportunity.Id
        ];

        List<Supply__c> resultingSupplies = [
            SELECT Id, Opportunity__c, Status__c, RecordType.DeveloperName, Contract__c, Activator__c
            FROM Supply__c
            WHERE RecordType.DeveloperName = 'Service'
        ];
        Supply__c resultingSupply = resultingSupplies.get(0);

        Case resultingCase = [
            SELECT Id, RecordType.DeveloperName, Reason__c
            FROM Case
            WHERE Dossier__c = :resultingDossier.Id
        ];

        // Contract created on activation, now should be updated with a new status
        Contract resultingContract = [
            SELECT Id, Opportunity__c, Status
            FROM Contract
        ];

        // Check dossier
        Id dossierDisconnectionRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
        System.assertEquals('New', resultingDossier.Status__c);
        System.assertEquals(constantsSrv.PARTNER_SERVICE_VAS_TERMINATION, resultingDossier.SubProcess__c);
        System.assertEquals(dossierDisconnectionRecordTypeId, resultingDossier.RecordTypeId);

        // Check contract
        System.assertEquals(constantsSrv.CONTRACT_STATUS_TERMINATED, resultingContract.Status);

        // Check supply
        System.assertEquals(constantsSrv.SUPPLY_STATUS_TERMINATING, resultingSupply.Status__c);
        System.assertEquals(resultingContract.Id, resultingSupply.Contract__c);

        // Check case
        System.assertEquals(Label.FinancialReason, resultingCase.Reason__c);
        System.assertEquals('Termination_SRV', resultingCase.RecordType.DeveloperName);
    }

    @isTest
    static void testVasSubstitution() {
        Map<String, Object> vasActivationResult = vasActivation();
        System.assertEquals(false, vasActivationResult.get('error'));

        OpportunityLineItem opportunityLineItemBefore = [
            SELECT Id, Product2Id, Product2.Name
            FROM OpportunityLineItem
        ];

        Supply__c supplyBefore = [
            SELECT Id, Status__c, RecordType.DeveloperName, Contract__c, Activator__c,Product__r.Name, Opportunity__c
            FROM Supply__c
            WHERE  Activator__c != null
        ];

        System.debug(supplyBefore.Product__r.Name);

        Test.startTest();
        Map<String, Object> resultFromSubstitution = vasSubstitution();
        Test.stopTest();
        System.assertEquals(false, resultFromSubstitution.get('error'));

        Opportunity substitutionOpportunity = [
            SELECT Id
            FROM Opportunity
            WHERE SubProcess__c = :constantsSrv.PARTNER_SERVICE_VAS_SUBSTITUTION
        ];

        OpportunityLineItem opportunityLineItemAfter = [
            SELECT Id, OpportunityId, Product2Id, Product2.Name
            FROM OpportunityLineItem
            WHERE OpportunityId =:substitutionOpportunity.Id
        ];

        Supply__c supplyAfter = [
            SELECT Id, Status__c, RecordType.DeveloperName, Contract__c, Activator__c,Product__r.Name, Opportunity__c
            FROM Supply__c
            WHERE  Activator__c != null
        ];

        System.debug(supplyAfter.Product__r.Name);

        System.assertEquals('New Product', supplyAfter.Product__r.Name);
    }
}