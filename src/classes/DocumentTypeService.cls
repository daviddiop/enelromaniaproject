/**
 * @author Baba Goudiaby BG
 * @version 1.0
 * @description Apex class for all operations of business logic and data manipulation about Validatable Document
 * @history
 * 01/04/2019: Original - BG
 * 10/06/2019: Add retrieveDocumentTypesFromDossier method - BG
 */

public with sharing class DocumentTypeService {
    private static DocumentBundleItemQueries documentBundleItemQuery = DocumentBundleItemQueries.getInstance();
    private static DocumentItemQueries documentItemQuery = DocumentItemQueries.getInstance();

    private static ValidatableDocumentService validatableDocumentSrv = ValidatableDocumentService.getInstance();
    private static DocumentItemService documentItemSrv = DocumentItemService.getInstance();

    public static DocumentTypeService getInstance() {
        return new DocumentTypeService();
    }

    /**
     * Method to allow us to retrieve all Document Types from Dossier
     *
     * @param dossierId
     *
     * @return List of documentType DTO
     */
    public List<DocumentTypeService.DocumentTypeDTO> retrieveDocumentTypesFromDossier(String dossierId){
        Map<String, ValidatableDocument__c> mapDocumentItemBundleValidatableDoc = validatableDocumentSrv.getMapDocumentItemBundleValidatableDoc(dossierId);
        Map<String, DocumentTypeService.DocumentTypeDTO> mapDocumentBundleToDocTypes = new Map<String, DocumentTypeService.DocumentTypeDTO>();
        Map<String, List<DocumentItemService.DocumentItemDTO>> mapDocTypesDocumentItems = new Map<String, List<DocumentItemService.DocumentItemDTO>>();
        Set<String> idsDocumentTypes = new Set<String>();

        List<DocumentBundleItem__c>  listDocumentBundleItems = documentBundleItemQuery.listDocumentBundleItemsFromValidatableDocs(dossierId);
        for (DocumentBundleItem__c docBundleItem : listDocumentBundleItems) {
            DocumentTypeService.DocumentTypeDTO documentType = getDocumentTypeFromDocumentBundleItem(docBundleItem.DocumentType__c, docBundleItem.DocumentType__r.Name,
                    docBundleItem.Mandatory__c, docBundleItem.Id
            );

            idsDocumentTypes.add(docBundleItem.DocumentType__c);

            if (mapDocumentItemBundleValidatableDoc.containsKey(docBundleItem.Id)) {
                ValidatableDocument__c validatableDocument = mapDocumentItemBundleValidatableDoc.get(docBundleItem.Id);
                documentType.isValidated = String.isNotBlank(validatableDocument.FileMetadata__c) ? true : false;
                documentType.validatableDocument = validatableDocument.Id;
            }

            if (!mapDocumentBundleToDocTypes.containsKey(String.valueOf(docBundleItem.DocumentBundle__c))) {
                mapDocumentBundleToDocTypes.put(docBundleItem.DocumentBundle__c, documentType);
            }
        }

        List<DocumentItemService.DocumentItemDTO> listDocumentItemDTO ;
        List<DocumentItem__c> listDocumentItems = documentItemQuery.listDocumentItemsByDocumentTypeId(idsDocumentTypes);
        for (DocumentItem__c docItem : listDocumentItems) {
            DocumentItemService.DocumentItemDTO docItemDTO = documentItemSrv.mapToDto(docItem);
            listDocumentItemDTO = new List<DocumentItemService.DocumentItemDTO>();
            if (!mapDocTypesDocumentItems.containsKey(docItem.DocumentType__c)) {
                listDocumentItemDTO.add(docItemDTO);
                mapDocTypesDocumentItems.put(docItem.DocumentType__c, listDocumentItemDTO);
            } else {
                mapDocTypesDocumentItems.get(docItem.DocumentType__c).add(docItemDTO);
            }
        }

        List<DocumentTypeService.DocumentTypeDTO> listDocumentTypeDTO = mapDocumentBundleToDocTypes.values();
        for (DocumentTypeService.DocumentTypeDTO docTypeDTO : listDocumentTypeDTO) {
            docTypeDTO.documentItems = mapDocTypesDocumentItems.get(docTypeDTO.id);
        }
        return listDocumentTypeDTO;
    }

    public DocumentType__c getDocumentTypeObj(String id, String name) {
        return new DocumentType__c( Id = id,Name = name);
    }

    public DocumentTypeDTO getDocumentTypeFromDocumentBundleItem(String idDocType,String name,Boolean isMandatory,String idsDocumentBundleItems){
        return new DocumentTypeDTO(getDocumentTypeObj(idDocType,name),isMandatory,idsDocumentBundleItems);
    }

    /**
     * @description  Inner class DTO for Document Type
     */
    public class DocumentTypeDTO {
        @AuraEnabled
        public String id { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public Boolean isMandatory { get; set; }
        @AuraEnabled
        public Boolean isValidated { get; set; }
        @AuraEnabled
        public String documentBundleItem { get; set; }
        @AuraEnabled
        public String validatableDocument { get; set; }
        @AuraEnabled
        public List<DocumentItemService.DocumentItemDTO> documentItems { get; set; }

        public DocumentTypeDTO(){}
        public DocumentTypeDTO(DocumentType__c documentTypeObj,Boolean isMandatory,String documentBundleItem) {
            this.id = documentTypeObj.Id;
            this.name = documentTypeObj.Name;
            this.isMandatory = isMandatory;
            this.documentBundleItem = documentBundleItem;
        }
    }

}