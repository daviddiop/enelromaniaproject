/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 14, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_CaseGroup {
    public static MRO_SRV_CaseGroup getInstance() {
        return (MRO_SRV_CaseGroup) ServiceLocator.getInstance(MRO_SRV_CaseGroup.class);
    }

    public Map<CaseGroup__c, List<Case>> makeCaseGroups(List<Case> candidateCaseGroupMembers) {
        Set<Id> candidateCaseIds = new Set<Id>();
        Set<Id> dossierIds = new Set<Id>();
        Set<Id> supplyIds = new Set<Id>();
        List<Case> allCases = new List<Case>();
        for (Case candidate : candidateCaseGroupMembers) {
            if (candidate.Supply__c == null || candidate.Dossier__c == null) {
                continue;
            }
            dossierIds.add(candidate.Dossier__c);
            supplyIds.add(candidate.Supply__c);
            allCases.add(candidate);
            if (candidate.Id != null) {
                candidateCaseIds.add(candidate.Id);
            }
        }
        Map<Id, Supply__c> suppliesMap = supplyIds.isEmpty() ? new Map<Id, Supply__c>() : MRO_QR_Supply.getInstance().getByIds(supplyIds);

        //Adding all existing cases related to the same Dossiers
        List<Case> allDossierCases = MRO_QR_Case.getInstance().listCasesByDossierIdsForGrouping(dossierIds);
        for (Case c : allDossierCases) {
            if (!candidateCaseIds.contains(c.Id)) {
                allCases.add(c);
            }
        }

        //Structure: - Dossier
        //             - ServiceSite
        //               - Cases
        Map<Id, Map<Id, List<Case>>> casesByDossierIdAndServiceSiteId = new Map<Id, Map<Id, List<Case>>>();
        for (Case c : allCases) {
            Map<Id, List<Case>> casesByServiceSite;
            if (!casesByDossierIdAndServiceSiteId.containsKey(c.Dossier__c)) {
                casesByServiceSite = new Map<Id, List<Case>>();
                casesByDossierIdAndServiceSiteId.put(c.Dossier__c, casesByServiceSite);
            } else {
                casesByServiceSite = casesByDossierIdAndServiceSiteId.get(c.Dossier__c);
            }
            Id serviceSiteId = c.Supply__r == null ? suppliesMap.get(c.Supply__c).ServiceSite__c : c.Supply__r.ServiceSite__c;
            System.debug('## serviceSiteId: '+serviceSiteId);
            if (serviceSiteId != null) {
                if (casesByServiceSite.containsKey(serviceSiteId)) {
                    casesByServiceSite.get(serviceSiteId).add(c);
                }
                else {
                    casesByServiceSite.put(serviceSiteId, new List<Case>{c});
                }
            }
        }

        Map<String, CaseGroup__c> caseGroups = new Map<String, CaseGroup__c>();
        Map<String, List<Case>> casesByGroupKey = new Map<String, List<Case>>();
        List<Case> otherCasesToUpsert = new List<Case>();

        for (Id dossierId : casesByDossierIdAndServiceSiteId.keySet()) {
            for (Id serviceSiteId : casesByDossierIdAndServiceSiteId.get(dossierId).keySet()) {
                List<Case> cases = casesByDossierIdAndServiceSiteId.get(dossierId).get(serviceSiteId);
                if (cases.size() < 2) continue;
                CaseGroup__c caseGroup;
                for (Case c : cases) {
                    if (c.CaseGroup__c != null) {
                        caseGroup = c.CaseGroup__r;
                        caseGroup.CaseCount__c = cases.size();
                        break;
                    }
                }
                if (caseGroup == null) {
                    caseGroup = new CaseGroup__c(
                        IntegrationKey__c = MRO_UTL_Guid.newGuid(), //Temporary, it will be overwritten with the Case Group Number
                        ServiceSite__c = serviceSiteId,
                        CaseCount__c = cases.size()
                    );
                }
                caseGroups.put(caseGroup.IntegrationKey__c, caseGroup);
                casesByGroupKey.put(caseGroup.IntegrationKey__c, cases);
                for (Case c : cases) {
                    if (c.Id != null && !candidateCaseIds.contains(c.Id) && (c.CaseGroup__c == null || c.CaseGroup__r.IntegrationKey__c != caseGroup.IntegrationKey__c)) {
                        otherCasesToUpsert.add(c);
                    }
                    c.CaseGroup__c = caseGroup.Id;
                    c.CaseGroup__r = caseGroup;
                }
            }
        }

        upsert caseGroups.values();
        Set<Id> caseGroupIds = new Set<Id>();
        for (CaseGroup__c cg : caseGroups.values()) {
            caseGroupIds.add(cg.Id);
        }
        Map<Id, CaseGroup__c> caseGroupsByIds = MRO_QR_CaseGroup.getInstance().listByIdsForUpdate(caseGroupIds);

        Map<CaseGroup__c, List<Case>> result = new Map<CaseGroup__c, List<Case>>();
        for (String key : casesByGroupKey.keySet()) {
            CaseGroup__c cg = caseGroups.get(key);
            cg = caseGroupsByIds.get(cg.Id);
            cg.IntegrationKey__c = cg.Name;
            for (Case c : casesByGroupKey.get(key)) {
                c.CaseGroup__c = c.CaseGroup__r.Id;
            }
            result.put(cg, casesByGroupKey.get(key));
        }
        upsert otherCasesToUpsert;

        update caseGroupsByIds.values();

        return result;
    }

    public void removeCancelledCasesFromGroups(List<Case> cancelledCases) {
        Set<Id> caseGroupIds = new Set<Id>();
        for (Case c : cancelledCases) {
            caseGroupIds.add(c.CaseGroup__c);
        }
        Map<Id, CaseGroup__c> caseGroupsMap = new Map<Id, CaseGroup__c>(MRO_QR_CaseGroup.getInstance().listByIds(caseGroupIds));
        List<CaseGroup__c> caseGroupsToDelete = new List<CaseGroup__c>();
        for (Case c : cancelledCases) {
            CaseGroup__c cg = caseGroupsMap.get(c.CaseGroup__c);
            if (cg.Status__c == MRO_UTL_Constants.getAllConstants().CASEGROUP_STATUS_STACKING) {
                cg.CaseCount__c -= 1;
                c.CaseGroup__c = null;
                if (cg.CaseCount__c < 2) {
                    caseGroupsToDelete.add(cg);
                }
            } else {
                c.addError(Label.CancelCaseGroupItemError);
            }
        }
        if (!caseGroupsToDelete.isEmpty()) {
            delete caseGroupsToDelete;
            for (CaseGroup__c dcg : caseGroupsToDelete) {
                caseGroupsMap.remove(dcg.Id);
            }
        }
        if (!caseGroupsMap.isEmpty()) {
            update caseGroupsMap.values();
        }
    }

    public void addLogsToCaseGroups(List<ExecutionLog__c> caseGroupLogs) {
        Set<Id> caseIds = new Set<Id>();
        for (ExecutionLog__c log : caseGroupLogs) {
            caseIds.add(log.Case__c);
        }
        Map<Id, Case> casesMap = new Map<Id, Case>(MRO_QR_Case.getInstance().listCasesByIdsWithCaseGroup(caseIds));
        Set<Id> caseGroupIds = new Set<Id>();
        for (Case c : casesMap.values()) {
            if (c.CaseGroup__c != null) {
                caseGroupIds.add(c.CaseGroup__c);
            }
        }
        Map<Id, CaseGroup__c> caseGroupsMap = MRO_QR_CaseGroup.getInstance().listByIdsForUpdate(caseGroupIds);
        List<CaseGroup__c> caseGroupsToUpdate = new List<CaseGroup__c>();
        for (ExecutionLog__c log : caseGroupLogs) {
            Case c = casesMap.get(log.Case__c);
            if (c.CaseGroup__c == null) {
                continue;
            }
            CaseGroup__c caseGroup = caseGroupsMap.get(c.CaseGroup__c);
            if (caseGroup.Status__c != MRO_UTL_Constants.getAllConstants().CASEGROUP_STATUS_STACKING) {
                log.addError(Label.GroupCallOutOverflowError);
                continue;
            }
            log.CaseGroup__c = caseGroup.Id;
            caseGroup.ExecutionLogCount__c += 1;
            if (caseGroup.ExecutionLogCount__c == caseGroup.CaseCount__c) {
                caseGroup.Status__c = MRO_UTL_Constants.getAllConstants().CASEGROUP_STATUS_READY;
            }
            caseGroupsToUpdate.add(caseGroup);
        }
        update caseGroupsToUpdate;
    }
}