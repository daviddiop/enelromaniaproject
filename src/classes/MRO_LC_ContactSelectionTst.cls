/**
 * @author  Luca Ravicini
 * @since   Jun 2, 2020
 * @desc   Test for MRO_LC_ContactSelection class
 *
 */

@IsTest
private class MRO_LC_ContactSelectionTst {
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();

    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 1000.0,SequenceLength__c = 7.0);
        insert sequencer;

        MRO_UTL_TestDataFactory.AccountBuilder accountBuilder2 =  MRO_UTL_TestDataFactory.account().businessAccount();
        Account acc = AccountBuilder2.build();
        insert acc;
    }

    @IsTest
    static void getContacts() {
        Account account = [
                SELECT Id
                FROM Account
        ];
        Contact contact1 = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact1.AccountId = account.Id;
        insert contact1;

        account.PrimaryContact__c = contact1.Id;
        update account;

        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => account.Id
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_ContactSelection', 'getContacts', inputJSON, true);
            Test.stopTest();
            System.assertEquals(false, response.get('error'));
            List<Contact> contacts = (List<Contact>)response.get('contacts');
            System.assertEquals(1, contacts.size());
            System.assertEquals(contact1.Id, response.get('primaryContact'));
        } catch (Exception exc) {

        }
    }

    @IsTest
    static void getContactsError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_ContactSelection', 'getContacts', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }
}