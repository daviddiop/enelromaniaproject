public with sharing class BillingProfileChangeWizardCnt  extends ApexServiceLibraryCnt  {

    private static DossierService dossierServ = DossierService.getInstance();
    private static CaseService caseServ = CaseService.getInstance();
    private static CaseQueries caseQuery = CaseQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    public class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String billingProfileId = '';

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account acc = accQuery.findAccount(accountId); 
                Dossier__c dossier = dossierServ.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType,'BillingProfileChange');
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);

                if (!cases.isEmpty()) {
                    billingProfileId = cases[0].BillingProfile__c;
                }

                response.put('caseTile',cases);
                response.put('billingProfileId', billingProfileId);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('accountId', accountId);
                response.put('account', acc);
                response.put('accountPersonRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId());
                response.put('accountPersonProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId());
                response.put('accountBusinessRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId());
                response.put('accountBusinessProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId());

                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return  response;
        }
    }

    public class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String recordTypeId;
            String reason = '';
            Date effectiveDate = null;
            Id trader = null;

            Map<String, String>  billingProfileChangeRecordTypes = Constants.getCaseRecordTypes('BillingProfileChange');
            recordTypeId =  billingProfileChangeRecordTypes.get('BillingProfileChange');

            List<Supply__c> searchedSupplyFieldsList = (List<Supply__c>) JSON.deserialize(params.get('searchedSupplyFieldsList'), List<Supply__c>.class);
            String accountId = params.get('accountId');
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            List<Case> newCases =  new List<Case>();

            try {
                for (Supply__c supply : searchedSupplyFieldsList) {
                    Case changeCase = caseServ.createCaseRecord(supply,accountId,dossierId,recordTypeId, reason, effectiveDate, trader);
                    newCases.add(changeCase);
                }

                List<Case> updatedCases = caseServ.insertCaseRecords(newCases,caseList);
                response.put('caseTile', updatedCases);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public class saveDraftBP extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String billingProfileId = params.get('billingProfileId');
            Savepoint sp = Database.setSavepoint();

            try {
                caseServ.updateCaseByBillingProfile(oldCaseList, billingProfileId);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;        
        }
    }

    public class updateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String billingProfileId = params.get('billingProfileId');
            String companyDivisionId; // Add by bsow
            Savepoint sp = Database.setSavepoint();

            try {
                //Add by bsow
                if (oldCaseList[0] != null) {
                    companyDivisionId = oldCaseList[0].CompanyDivision__c;
                }
                // End add

                caseServ.setNewOnCaseAndDossier(oldCaseList, dossierId, billingProfileId,companyDivisionId);
                response.put('error', false);

            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            Savepoint sp = Database.setSavepoint();

            try {
                caseServ.setCanceledOnCaseAndDossier(oldCaseList, dossierId);
                response.put('error', false);  
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;         
        }
    }

}