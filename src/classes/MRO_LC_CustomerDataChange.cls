/**
 * Created by goudiaby on 02/08/2019.
 */
/**
* @description Class controller for Case components
* @author Boubacar Sow
* @date 04-10-2019
 */
public with sharing class MRO_LC_CustomerDataChange extends ApexServiceLibraryCnt {
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static DossierService dossierSrv = DossierService.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();

    /**
     * Initialize method to get data at the beginning for Meter Change
     */
    public class InitializeCustomerDataChange extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Account account = accountQuery.findAccount(accountId);
//            List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'CustomerDataChange');
//            if (String.isNotBlank(dossierId)) {
//                List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
//                response.put('caseTile', cases);
//            }
//            if (cases != null && !cases.isEmpty()) {
//                for (Case aCase : cases) {
//                    if (aCase.RecordType.DeveloperName == 'CustomerDataChange') {
//                        response.put('caseId', aCase.Id);
//                        response.put('case', aCase);
//                        break;
//                    }
//                }
//            }

            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('accountId', accountId);
            response.put('account', account);
            response.put('isPersonAccount', account.IsPersonAccount);
            response.put('recordTypeCustomerDataChange', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CustomerDataChange').getRecordTypeId());

            response.put('error', false);
            return response;
        }
    }


    public class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            String companyDivisionId;

            if (oldCaseList[0] != null) {
                companyDivisionId = oldCaseList[0].CompanyDivision__c;
            }

            caseSrv.setNewOnCaseAndDossier(oldCaseList, dossierId, companyDivisionId);
            response.put('error', false);
            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');

            caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId);
            response.put('error', false);
            return response;
        }
    }
}