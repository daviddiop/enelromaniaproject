/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 27, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_CampaignMember {
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_QR_Activity activityQueries = MRO_QR_Activity.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public static MRO_SRV_CampaignMember getInstance() {
        return (MRO_SRV_CampaignMember) ServiceLocator.getInstance(MRO_SRV_CampaignMember.class);
    }

    public void cancelTacitRenewalCampaignMembers(List<wrts_prcgvr__Activity__c> lostTacitRenewalActivities) {
        Set<Id> currentActivityIds = new Set<Id>();
        Set<Id> campaignIds = new Set<Id>();
        Map<Id, CampaignMember> campaignMembersMap = new Map<Id, CampaignMember>();
        for (wrts_prcgvr__Activity__c act : lostTacitRenewalActivities) {
            currentActivityIds.add(act.Id);
            if (act.wrts_prcgvr__ObjectId__c != null) {
                campaignMembersMap.put(act.wrts_prcgvr__ObjectId__c, new CampaignMember(
                    Id = act.wrts_prcgvr__ObjectId__c,
                    Status = act.wrts_prcgvr__Status__c == CONSTANTS.ACTIVITY_STATUS_CANCELLED ? 'Cancelled' : 'Refused'
                ));
            }
            if (act.Campaign__c != null) {
                campaignIds.add(act.Campaign__c);
            }
        }

        List<wrts_prcgvr__Activity__c> otherActivities = activityQueries.listOtherTacitRenewalActivities(campaignIds, campaignMembersMap.keySet(), currentActivityIds);
        for (wrts_prcgvr__Activity__c act : otherActivities) {
            campaignMembersMap.remove(act.wrts_prcgvr__ObjectId__c);
        }

        databaseSrv.updateSObject(campaignMembersMap.values());
    }

    public void acceptTacitRenewalCampaignMembers(Map<Id, Opportunity> wonTacitRenewalOpportunities) {
        //Map<Id, wrts_prcgvr__Activity__c> activities = new Map<Id, wrts_prcgvr__Activity__c>();
        //for (Id campaignId : successfulRenewalOpportunitiesByCampaignId.keySet()) {
        //    activities.putAll(activityQueries.getTacitRenewalActivitiesByOpportunities(successfulRenewalOpportunitiesByCampaignId.get(campaignId), campaignId));
        //}
        Set<Id> campaignIds = new Set<Id>();
        for (Opportunity opp : wonTacitRenewalOpportunities.values()) {
            if (opp.CampaignId != null) {
                campaignIds.add(opp.CampaignId);
            }
        }
        Map<Id, wrts_prcgvr__Activity__c> activities = activityQueries.getTacitRenewalActivitiesByOpportunities(wonTacitRenewalOpportunities.keySet(), campaignIds);
        Set<Id> campaignMemberIds = new Set<Id>();
        for (wrts_prcgvr__Activity__c act : activities.values()) {
            campaignMemberIds.add(act.wrts_prcgvr__ObjectId__c);
        }
        List<wrts_prcgvr__Activity__c> otherActivities = activityQueries.listOtherTacitRenewalActivities(wonTacitRenewalOpportunities.keySet(), campaignMemberIds, activities.keySet());
        for (wrts_prcgvr__Activity__c act : otherActivities) {
            campaignMemberIds.remove(act.wrts_prcgvr__ObjectId__c);
        }

        List<CampaignMember> campaignMembersToUpdate = new List<CampaignMember>();
        for (Id campaignMemberId : campaignMemberIds) {
            campaignMembersToUpdate.add(new CampaignMember(
                Id = campaignMemberId,
                Status = 'Accepted'
            ));
        }

        databaseSrv.updateSObject(campaignMembersToUpdate);
    }

}