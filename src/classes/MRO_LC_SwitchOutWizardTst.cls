/**
 * Created by Boubacar Sow d on 15/01/2020.
 * @version 1.0
 * @description
 *          [ENLCRO-424] Switch Out - Implement the process to make a switch out Case
 *
 */
@IsTest
public with sharing class MRO_LC_SwitchOutWizardTst {

    @TestSetup
    private static void setup() {

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SwitchOut_ELE').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SwitchOut_ELE', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'SwitchOut_ELE', recordTypeSwiEle, '');
        insert phaseDI010ToRC010;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createSequencer().setCompany(companyDivision.Id).build();
        insert sequencer;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'SwitchOutWizardCodeTemplate_1';
        insert scriptTemplate;


        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();
        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        Account distributorAccount = MRO_UTL_TestDataFactory.account().distributorAccount().build();
        listAccount.add(distributorAccount);
        insert listAccount;

        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;

        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'BusinessAccount1';
        accountTrader.BusinessType__c = 'Commercial areas';

        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
       insert servicePoint;

        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[1].Id;
        insert contract;
        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;
        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[1].Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;

        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supply.Account__c = listAccount[1].Id;
            supply.ServicePoint__c = servicePoint.Id;
            supply.CompanyDivision__c = companyDivision.Id;
            supplyList.add(supply);
        }
        insert supplyList;

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[1].Id;
        insert dossier;
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SwitchOut_ELE').getRecordTypeId();
            caseRecord.Origin = 'Fax';
            caseRecord.Channel__c = 'Back Office Sales';
            //caseRecord.ReferenceDate__c = Date.today();
            caseRecord.EffectiveDate__c = Date.newInstance(2020,02,03);
            caseRecord.ReferenceDate__c = Date.newInstance(2020,02,04);


            System.debug('#### caseRecord '+caseRecord);
            caseList.add(caseRecord);
        }
        insert caseList;

        ProcessDateThresholds__c ProcessDateThresholdsSettings = ProcessDateThresholds__c.getOrgDefaults();
        insert ProcessDateThresholdsSettings;

    }

    @IsTest
    static void initializeTest() {
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'customerInteractionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_SwitchOutWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => '',
            'customerInteractionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id};
        response = TestUtils.exec(
            'MRO_LC_SwitchOutWizard', 'initialize', inputJSON, true);
        inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => dossier.Id,
            'customerInteractionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id};
        response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'initialize', inputJSON, false);
        Test.stopTest();
    }

    @IsTest
    private static void createCaseTest() {
        List<Supply__c> supply = [
            SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c
            FROM Supply__c
            WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Trader__c,
                    ReferenceDate__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        List<Account> myAccount = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        Test.startTest();

        MRO_LC_SwitchOutWizard.CreateCaseInput createCaseInput = new MRO_LC_SwitchOutWizard.CreateCaseInput();
        createCaseInput.searchedSupplyFieldsList = supply;
        createCaseInput.effectiveDate = caseList[0].EffectiveDate__c;
        createCaseInput.referenceDate = caseList[0].ReferenceDate__c;
        createCaseInput.trader = caseList[0].Trader__c;
        createCaseInput.accountId = myAccount[0].Id;
        createCaseInput.caseList = caseList;
        createCaseInput.dossierId = dossier.Id;
        Object response = TestUtils.exec(
            'MRO_LC_SwitchOutWizard', 'createCase', createCaseInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        //System.assertEquals(true, result.get('caseTile') != null);
        createCaseInput.searchedSupplyFieldsList = supply;
        createCaseInput.effectiveDate = caseList[0].EffectiveDate__c;
        createCaseInput.referenceDate = caseList[0].ReferenceDate__c;
        createCaseInput.trader = caseList[0].Trader__c;
        createCaseInput.accountId = myAccount[0].Id;
        createCaseInput.caseList = caseList;
        createCaseInput.caseId = caseList[0].Id;
        createCaseInput.dossierId = dossier.Id; response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'createCase', createCaseInput, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        createCaseInput.caseId = 'testInexistentAccountId';
        response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'createCase', createCaseInput, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    private static void updateCaseListTest() {
        List<Case> caseList = [
            SELECT Id, Supply__c, EffectiveDate__c, Reason__c, AccountId,
                    ReferenceDate__c, RecordTypeId,CompanyDivision__c, Origin
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        Test.startTest();
        MRO_LC_SwitchOutWizard.UpdateCaseListInput updateCaseListInput = new MRO_LC_SwitchOutWizard.UpdateCaseListInput();
        updateCaseListInput.oldCaseList = caseList;
        updateCaseListInput.dossierId = dossier.Id;
        Object response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'updateCaseList', updateCaseListInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        updateCaseListInput.oldCaseList = null;
        response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'updateCaseList', updateCaseListInput, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    private static void cancelProcessTest() {
        List<Case> caseList = [
            SELECT Id, Supply__c, EffectiveDate__c, Reason__c, AccountId,
                    ReferenceDate__c, RecordTypeId, CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        Test.startTest();
        MRO_LC_SwitchOutWizard.CancelProcessInput cancelProcessInput = new MRO_LC_SwitchOutWizard.CancelProcessInput();
        cancelProcessInput.oldCaseList = caseList;
        cancelProcessInput.dossierId = dossier.Id;

        Object response = TestUtils.exec(
            'MRO_LC_SwitchOutWizard', 'cancelProcess', cancelProcessInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        cancelProcessInput.oldCaseList = null;
        response = TestUtils.exec(
            'MRO_LC_SwitchOutWizard', 'cancelProcess', cancelProcessInput, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    private static void saveDraftBPTest() {
        List<Case> caseList = [
            SELECT Id, Supply__c, EffectiveDate__c, Reason__c, AccountId,
                    ReferenceDate__c, RecordTypeId, CompanyDivision__c
            FROM Case
            LIMIT 1
        ];

        Test.startTest();
        MRO_LC_SwitchOutWizard.DraftBillingProfileInput draftBillingProfileInput = new MRO_LC_SwitchOutWizard.DraftBillingProfileInput();
        draftBillingProfileInput.oldCaseList = caseList;
        Object response = TestUtils.exec(
            'MRO_LC_SwitchOutWizard', 'saveDraftBP', draftBillingProfileInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        draftBillingProfileInput.oldCaseList = null;
        draftBillingProfileInput.billingProfileId = 'testInexistentAccountId';
        response = TestUtils.exec(
            'MRO_LC_SwitchOutWizard', 'saveDraftBP', draftBillingProfileInput, true);
        Test.stopTest();
    }


    @IsTest
    private static void UpdateDossierTest() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'channelSelected' => 'Enel',
                'originSelected' => 'Mail'
        };

        Object response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'UpdateDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    private static void getSupplyRecordTest() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        List<Id> supplyIds = new List<Id>{
                [
                        SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c
                        FROM Supply__c
                        LIMIT 1
                ].Id
        };
        Test.startTest();
        MRO_LC_SwitchOutWizard.supplyInputIds supplyInputIds = new MRO_LC_SwitchOutWizard.supplyInputIds();
        supplyInputIds.supplyIds = supplyIds;
        Object response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'getSupplyRecord', supplyInputIds, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('supplies') != null);
        Test.stopTest();
    }

    @IsTest
    private static void updateCommodityToDossierTest() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'commodity' => 'Electric'
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'updateCommodityToDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    private static void updateCaseTest() {
        List<Case> caseList = [
                SELECT Id, Supply__c, EffectiveDate__c, Reason__c, AccountId,
                        ReferenceDate__c, RecordTypeId,CompanyDivision__c, Origin
                FROM Case
                LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
                'caseList' => JSON.serialize(caseList),
                'caseId' => caseList[0].Id,
                'effectiveDate' => JSON.serialize(caseList[0].EffectiveDate__c),
                'referenceDate' => JSON.serialize(caseList[0].ReferenceDate__c)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_SwitchOutWizard', 'updateCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

}