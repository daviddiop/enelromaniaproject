@isTest
private class MRO_LC_RelatedListCntTst {

    @testSetup
    static void setup() {
        Account a = TestDataFactory.account().businessAccount().build();
        insert a;

        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(LastName='contactTest', AccountId=a.Id));
        contacts.add(new Contact(LastName='contactTest2', AccountId=a.Id));
        insert contacts;

        List<Case> cases = new List<Case>();
        cases.add(new Case(Status='New', Origin='Web', AccountId = a.Id));
        cases.add(new Case(Status='New', Origin='Phone', AccountId = a.Id));
        cases.add(new Case(Status='New', Origin='Email', AccountId = a.Id));
        insert cases;
    }

    @IsTest
    static void initializeContactTest() {
        Account a = [SELECT Id FROM Account LIMIT 1];
        MRO_LC_RelatedListCnt.initialize(a.Id, 'Contacts', 'Contact', 'AccountId', '');
    }
    @IsTest
    static void initializeCaseWithLabelTest() {
        Account a = [SELECT Id FROM Account LIMIT 1];
        MRO_LC_RelatedListCnt.initialize(a.Id, '$Label.Cases', 'Case', 'AccountId', '');
    }
    @IsTest
    static void initializeCaseWithoutLabelTest() {
        Account a = [SELECT Id FROM Account LIMIT 1];
        MRO_LC_RelatedListCnt.initialize(a.Id, 'Cases', 'Case', 'AccountId', '');
    }
    @IsTest
    static void initializeCatchTest() {
        Account a = [SELECT Id FROM Account LIMIT 1];
        MRO_LC_RelatedListCnt.initialize('', '', '', '', '');
    }
    @IsTest
    static void createRelatedListTest() {
        Account a = [SELECT Id FROM Account LIMIT 1];
        MRO_LC_RelatedListCnt.createRelatedList(a.Id, 'Contact', '', 'AccountId', 1, 'DESC');
    }

}