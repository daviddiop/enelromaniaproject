/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.04.20.
 */

public with sharing class MRO_LC_ATRExtensionWizard extends ApexServiceLibraryCnt {


    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();

    private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();

    private static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                response.put('error', true);
                response.put('errorMsg', System.Label.Account + ' - ' + System.Label.MissingId);
                return response;
            }

            Account acc = accQuery.findAccount(accountId);

            Dossier__c dossier = dossierSr.generateDossier(accountId, dossierId, interactionId, null, dossierRecordType, 'ATR Extension');

            if(!String.isBlank(genericRequestId)){
                dossierSr.updateParentGenericRequest(dossier.Id, genericRequestId);
            }

            List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);

            if (cases.size() > 0) {
                response.put('caseRecord', cases.get(0));
            }

            response.put('isClosed', dossier.Status__c != constantsSrv.DOSSIER_STATUS_DRAFT);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('error', false);

            return response;
        }
    }


    public class RequestParams {
        @AuraEnabled
        public Id supplyId { get; set; }
        @AuraEnabled
        public Id caseId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
    }

    public with sharing class upsertCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams upsertCaseParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);
            Supply__c supply = supplyQuery.getSupplyWithServicePointById(upsertCaseParams.supplyId);

            if(upsertCaseParams.supplyId != null && upsertCaseParams.caseId == null){
                List<Case> terminationCases = caseQuery.listTerminationCasesBySupplyId(upsertCaseParams.supplyId);
                Boolean showAlert = terminationCases.size() > 0;
                response.put('showAlert', showAlert);
            }

            Map<String, String> recordTypes = MRO_UTL_Constants.getCaseRecordTypes('ATRExtension');
            Account acc = accQuery.findAccount(supply.Account__c);
            Case newCase = new Case(
                    Id = upsertCaseParams.caseId,
                    Supply__c = supply.Id,
                    CompanyDivision__c = supply.CompanyDivision__c,
                    AccountId = supply.Account__c,
                    AccountName__c = acc.Name,
                    RecordTypeId = (String) recordTypes.get('ATRExtension'),
                    Dossier__c = upsertCaseParams.dossierId,
                    Status = constantsSrv.CASE_STATUS_DRAFT,
                    Origin = upsertCaseParams.originSelected,
                    Channel__c = upsertCaseParams.channelSelected,
                    Phase__c = constantsSrv.CASE_START_PHASE,
                    ServiceSite__c = supply.ServiceSite__c,
                    Distributor__c = supply.ServicePoint__r.Distributor__c,
                    VoltageLevel__c = supply.ServicePoint__r.VoltageLevel__c,
                    Voltage__c = supply.ServicePoint__r.Voltage__c,
                    VoltageSetting__c = String.valueOf((supply.ServicePoint__r.Voltage__c * 1000).round(System.RoundingMode.DOWN)),
                    ContractualPower__c = supply.ServicePoint__r.ContractualPower__c,
                    AvailablePower__c = supply.ServicePoint__r.AvailablePower__c,
                    PowerPhase__c = supply.ServicePoint__r.PowerPhase__c,
                    Pressure__c = supply.ServicePoint__r.Pressure__c,
                    PressureLevel__c = supply.ServicePoint__r.PressureLevel__c,
                    ConversionFactor__c = supply.ServicePoint__r.ConversionFactor__c,
                    EstimatedConsumption__c = supply.ServicePoint__r.EstimatedConsumption__c,
                    FirePlacesCount__c = supply.ServicePoint__r.FirePlacesCount__c
            );
            databaseSrv.upsertSObject(newCase);
            response.put('caseRecord', newCase);
            response.put('supplyRecord', supply);
            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams channelOriginParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);

            dossierSr.updateDossierChannel(channelOriginParams.dossierId, channelOriginParams.channelSelected, channelOriginParams.originSelected);

            return response;
        }
    }

    public class SaveProcessParams {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public Boolean isDraft { get; set; }
        @AuraEnabled
        public String contactId { get; set; }
        @AuraEnabled
        public String companyDivisionId { get; set; }
        @AuraEnabled
        public String supplyId { get; set; }
        @AuraEnabled
        public String billingProfileId { get; set; }
    }

    public with sharing class saveProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveProcessParams saveProcessParams = (SaveProcessParams) JSON.deserialize(jsonInput, SaveProcessParams.class);
            Map<String, Object> response = new Map<String, Object>();
            try {
                databaseSrv.upsertSObject(new Dossier__c(
                        Id = saveProcessParams.dossierId,
                        Status__c = saveProcessParams.isDraft ? constantsSrv.DOSSIER_STATUS_DRAFT : constantsSrv.DOSSIER_STATUS_NEW,
                        CompanyDivision__c = saveProcessParams.companyDivisionId
                ));
                if (saveProcessParams.caseId != null && !saveProcessParams.isDraft) {
                    databaseSrv.upsertSObject(new Case(
                            Id = saveProcessParams.caseId,
                            Status = constantsSrv.CASE_STATUS_NEW,
                            ContactId = saveProcessParams.contactId,
                            BillingProfile__c = saveProcessParams.billingProfileId,
                            CaseTypeCode__c = 'CCT'
                    ));
                    MRO_SRV_Case.getInstance().applyAutomaticTransitionOnDossierAndCases(saveProcessParams.dossierId);
                }
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getBillingProfile extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String billingProfileId = params.get('billingProfileId');
            Map<String, Object> response = new Map<String, Object>();
            try {
                BillingProfile__c billingProfile = billingProfileQuery.getById(billingProfileId);
                response.put('billingProfile', billingProfile);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}