/**
 * Created by BADJI on 13/07/2020.
 */
@IsTest
public with sharing class MRO_LC_CompanyDivisionTst {

    @TestSetup
    private static void setup() {
        User user = TestDataFactory.user().standardUser().build();
        insert user;

        List<CompanyDivision__c> listCompaniesDivision = new List<CompanyDivision__c>();
        for (Integer i = 0; i < 10; i++) {
            CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(i).build();
            listCompaniesDivision.add(companyDivision);
        }
        insert listCompaniesDivision;
    }


    @IsTest
    static void getCompanyDivisionDataTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        String companyDivisionId = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ].Id;

        user.CompanyDivisionId__c = companyDivisionId;
        update user;

        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => user.Id
        };
        Test.startTest();

        Object response = TestUtils.exec('MRO_LC_CompanyDivision', 'getCompanyDivisionData', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('companyDivision') != null);

    }


    @isTest
    static void updateUserCompanyDivisionTest() {
        User user = [
            SELECT Id,CompanyDivisionId__c
            FROM User
            WHERE Email = 'userdivision@test.com'
            LIMIT 1
        ];
        String companyDivisionId = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 2'
                LIMIT 1
        ].Id;

        Map<String, String> jsonInput = new Map<String, String>{
            'userId' => user.Id,
            'companyId' => companyDivisionId,
            'companyDivisionEnforced' => 'true'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_CompanyDivision', 'updateUserCompanyDivision', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Test.stopTest();
        System.assertEquals(true, result.get('currentUserId') == user.Id);
    }

}