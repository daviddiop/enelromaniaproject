/**
 * Modified by goudiaby on 09/01/2020.
 * @version 1.0
 * @description Class Query Service for Opportunity Object
 *
 */
public with sharing class MRO_QR_Opportunity {

    public static MRO_QR_Opportunity getInstance() {
        return (MRO_QR_Opportunity) ServiceLocator.getInstance(MRO_QR_Opportunity.class);
    }

    public Opportunity getById(String OpportunityId) {
        List<Opportunity> opportunityList = [
            SELECT Id, Name, Pricebook2Id, Type, AccountId, Account.Name, StageName, CloseDate, CompanyDivision__c, CompanyDivision__r.Trader__c, CustomerInteraction__c,RequestedStartDate__c,
                CustomerInteraction__r.Contact__c,RequestType__c,ContractSignedDate__c,ContractName__c,ContractType__c,CompanySignedBy__c,Salesman__c,SalesUnit__c, SalesUnit__r.SalesDepartment__c,
                SubProcess__c,Origin__c,Channel__c, LostReason__c,ExpirationDate__c, ContactId, SalesSupportUser__c, ConsumptionConventions__c, IsDual__c, ContractAddition__c, MarketSegment__c,
                ContractId, Contract.MarketSegment__c,
                (
                    SELECT Id, ServicePointCode__c, RecordType.DeveloperName, Product__c,Account__c,FlatRate__c,StartDate__c,EndDate__c,ApplicantNotHolder__c,NACEReference__c, NACEReference__r.Class__c,
                        Distributor__c, BillingProfile__c, ContractAccount__c, Trader__c, Supply__c, Supply__r.Contract__c,
                        ContractAccount__r.BillingProfile__r.BillingAddressNormalized__c, ContractAccount__r.BillingProfile__r.BillingStreetType__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetId__c, ContractAccount__r.BillingProfile__r.BillingAddressKey__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetName__c, ContractAccount__r.BillingProfile__r.BillingStreetNumber__c,
                        ContractAccount__r.BillingProfile__r.BillingStreetNumberExtn__c, ContractAccount__r.BillingProfile__r.BillingApartment__c,
                        ContractAccount__r.BillingProfile__r.BillingBuilding__c, ContractAccount__r.BillingProfile__r.BillingBlock__c,
                        ContractAccount__r.BillingProfile__r.BillingCity__c, ContractAccount__r.BillingProfile__r.BillingCountry__c,
                        ContractAccount__r.BillingProfile__r.BillingFloor__c,ContractAccount__r.BillingProfile__r.BillingPostalCode__c,
                        ContractAccount__r.BillingProfile__r.BillingProvince__c, ContractAccount__r.BillingProfile__r.BillingLocality__c,
                        PointAddressNormalized__c, PointStreetType__c,PointStreetId__c,PointAddressKey__c,
                        PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c, PointApartment__c, PointBuilding__c, PointBlock__c, PointCity__c,
                        PointCountry__c, PointFloor__c, Market__c, NonDisconnectable__c, PointLocality__c,ContractAccount__r.Market__c,
                        PointPostalCode__c, PointProvince__c, Voltage__c, VoltageLevel__c, PressureLevel__c,ConsumptionCategory__c,NonDisconnectableReason__c, IsNewConnection__c,
                        Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c, CLC__c, NLC__c, SiteDescription__c,
                        ServiceSite__c, SiteAddressKey__c,SiteStreetId__c,
                        SiteAddressNormalized__c, SiteApartment__c, SiteBuilding__c, SiteBlock__c, SiteCity__c, SiteCountry__c, SiteFloor__c,
                        SiteStreetType__c, SiteLocality__c, SitePostalCode__c, SiteProvince__c, SiteStreetName__c, SiteStreetNumber__c,
                        SiteStreetNumberExtn__c, AvailablePower__c, EstimatedConsumption__c, ServicePoint__r.CurrentSupply__c,
                        ServicePoint__r.CurrentSupply__r.CompanyDivision__c,ServicePoint__r.CurrentSupply__r.Account__c,ServicePoint__r.CurrentSupply__r.Market__c,ServicePoint__r.CurrentSupply__r.Contract__c,
                        ServiceSite__r.CLC__c, ServiceSite__r.NLC__c, ServiceSite__r.SiteAddressKey__c,Distributor__r.VATNumber__c,SupplyOperation__c,Distributor__r.IsDisCoENEL__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c
                    FROM OpportunityServiceItems__r
                ),
                (
                    SELECT Id, Product2Id, ProductCode,
                        Product2.Id, Product2.Name, Product2.RecordType.DeveloperName, Product2.RecordType.Name, Product2.Description,
                        Product2.Image__c, Product2.Family, Product2.CommercialProduct__r.ProductTerm__c,Product2.CommercialProduct__c
                    FROM OpportunityLineItems
                )
            FROM Opportunity
            WHERE Id = :OpportunityId
        ];

        return opportunityList.get(0);
    }
    // FF Activation/SwitchIn - Interface Check - Pack2
    /**
    * @author INSA BADJI
    * @description  Select an Opportunity by Id
    *
    * @param opportunityId Id of Opportunity
    * Modified by BG to add Interaction__r
    */
    // FF Activation/SwitchIn - Interface Check - Pack2
    //[ENLCRO-646] Switch-In - Check Interface - Pack3
    public Opportunity getOpportunityById(String opportunityId) {
        return [
            SELECT Id, Name, StageName, RequestType__c, ContractSignedDate__c, Origin__c, Channel__c,
                CompanyDivision__c, CompanyDivision__r.Name, CustomerInteraction__c, RequestedStartDate__c,
                CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                CustomerInteraction__r.Interaction__r.Interlocutor__c, CustomerInteraction__r.Contact__c,
                AccountId, Account.IsPersonAccount, Account.PersonIndividualId, Account.PersonContactId, Account.Name,
                ContractId, Contract.Status, Contract.AdditionalActCounter__c, SubProcess__c, ExpirationDate__c, IsDual__c, ContractAddition__c,
                ContractName__c, ContractType__c, CompanySignedBy__c, Salesman__c, SalesUnit__c, SalesUnit__r.SalesDepartment__c, SalesSupportUser__c,
                Contract.StartDate
            FROM Opportunity
            WHERE Id = :opportunityId
        ];
    }
    //[ENLCRO-646] Switch-In - Check Interface - Pack3
    /**
 * @author DAVID DIOP
 * @Created 29/01/2020
 * @description: retreive the map of Id and Opportunity by the opportunity Ids.
 * @param opportunitiesIds
 */
    public Map<Id, Opportunity> getByIds(Set<Id> opportunityIds) {
        return new Map<Id, Opportunity>([
                SELECT Id, ContractId
                FROM Opportunity
                WHERE Id IN :opportunityIds
        ]);
    }

    public Map<Id, Opportunity> getByIdsWithOSIs(Set<Id> opportunityIds) {
        return new Map<Id, Opportunity>([
                SELECT Id, ContractId,
                    (SELECT Id, Supply__c, Supply__r.Contract__c, ServicePoint__c, ServicePoint__r.CurrentSupply__r.Contract__c
                     FROM OpportunityServiceItems__r)
                FROM Opportunity
                WHERE Id IN :opportunityIds
        ]);
    }

    public Map<Id, Opportunity> getByContractIds(Set<Id> contractIds) {
        return new Map<Id, Opportunity>([
            SELECT Id
            FROM Opportunity
            WHERE Contract.Id IN :contractIds
        ]);
    }

    public List<NE__OrderItem__c> getOrderItems(String orderId){
        return [
                SELECT Id, NE__ProdId__r.NE__Sales_Type__c, DistributionPriceGas__c, TransportPrice__c, DistributionPriceElectricIT__c,
                        DistributionPriceElectricJT__c, DistributionPriceElectricMT__c, TransportPriceElectricTG__c, TransportPriceElectricTL__c,
                        TransportPriceElectricTS__c
                FROM NE__OrderItem__c
                WHERE NE__OrderId__c = :orderId
        ];
    }

    public NE__Order__c getOrder (String opportunityId) {
        List<NE__Order__c> orders = [Select Id from NE__Order__c where NE__OptyId__c = :opportunityId Limit 1];
        if (!orders.isEmpty()) {
            return orders[0];
        }
        return null;
    }

    public List<RegulatedTariff__c> getEleRegulatedTariff(String commodity, String vatNumber){
        return [
                SELECT Type__c, VoltageLevel__c, PriceValue__c
                FROM RegulatedTariff__c
                WHERE Commodity__c = :commodity AND (DistributorVATNumber__c = :vatNumber OR DistributorVATNumber__c = NULL)
        ];
    }

    public List<RegulatedTariff__c> getGasRegulatedTariff(String commodity, String vatNumber, String consumptionCategory){
        return [
                SELECT Type__c, PriceValue__c
                FROM RegulatedTariff__c
                WHERE Commodity__c = :commodity AND (DistributorVATNumber__c = :vatNumber OR DistributorVATNumber__c = NULL) AND (ConsumptionCategory__c = :consumptionCategory OR ConsumptionCategory__c = NULL)
        ];
    }

    public Opportunity getOpportunityDossiersById(String opportunityId) {
        List<Opportunity> opportunityList = [
                SELECT Id, Name, StageName, RequestType__c, ContractSignedDate__c, Origin__c, Channel__c,
                        ContractName__c, ContractType__c, CompanySignedBy__c, Salesman__c ,
                (SELECT Id FROM Dossiers__r ORDER BY CreatedDate DESC LIMIT 1)
                FROM Opportunity
                WHERE Id = :opportunityId
        ];
        return opportunityList.isEmpty() ? null : opportunityList[0];
    }

    public RegulatedTariff__c getProsumerPrice(String voltageLevel, String vatNumber){
        List<RegulatedTariff__c> regulatedTariffList = [
                SELECT Id, PriceValue__c
                FROM RegulatedTariff__c
                WHERE Type__c = 'Prosumer' AND DistributorVATNumber__c =: vatNumber AND VoltageLevel__c =: voltageLevel
        ];

        if (regulatedTariffList.isEmpty()) {
            return null;
        }
        return regulatedTariffList.get(0);
    }
}