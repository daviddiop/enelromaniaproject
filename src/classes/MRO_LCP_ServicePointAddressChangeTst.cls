/**
 * Created by BADJI on 28/08/2020.
 */

@IsTest
public with sharing class MRO_LCP_ServicePointAddressChangeTst {

    @TestSetup
    static void Setup(){
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SwitchIn_ELE').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'SwitchIn_ELE', recordTypeSwiEle, '');
        insert phaseDI010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;
        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'ServicePointChangeCodeTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;

        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterChangeEle').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    public static void InitializeServicePointAddressChangeTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CustomerInteraction__c interaction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => interaction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LCP_ServicePointAddressChange', 'InitializeServicePointAddressChange', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.debug('***** result '+result);
        System.assertEquals(true, result.get('accountId') !=null );
        Test.stopTest();
    }

    @IsTest
    public static void getServicePointTest(){
        ServicePoint__c sp = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'servicePointId' => sp.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LCP_ServicePointAddressChange', 'getServicePoint', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );


        inputJSON = new Map<String, String>{
            'servicePointId' => 'sptestId'
        };
        response = TestUtils.exec('MRO_LCP_ServicePointAddressChange', 'getServicePoint', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void UpdateCaseListTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LCP_ServicePointAddressChange', 'UpdateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    public static void CancelProcessTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        List<Case> caseList = [
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LCP_ServicePointAddressChange', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void updateDossierTest() {
        String dossierId = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossierId,
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Internal'
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LCP_ServicePointAddressChange', 'UpdateDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

}