/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 26, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_TR_OpportunityHandler implements TriggerManager.ISObjectTriggerHandler {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public void beforeInsert() {
        for (Opportunity opportunityNew : (List<Opportunity>) Trigger.new) {
            opportunityNew.NE__Channel__c = opportunityNew.Channel__c;
        }
    }

    public void beforeUpdate() {
        for (Opportunity opportunityNew : (List<Opportunity>) Trigger.new) {
            opportunityNew.NE__Channel__c = opportunityNew.Channel__c;
        }
    }

    public void beforeDelete() {
    }

    public void afterInsert() {
    }

    public void afterUpdate() {
        Map<Id, Opportunity> tacitRenewalOpportunities = new Map<Id, Opportunity>();
        for (Opportunity opp : (List<Opportunity>) Trigger.new) {
            Opportunity oldOpp = (Opportunity) Trigger.oldMap.get(opp.Id);
            if (opp.Type == 'Tacit Renewal' && opp.StageName == CONSTANTS.OPPORTUNITY_STAGE_CLOSED_WON && oldOpp.StageName != CONSTANTS.OPPORTUNITY_STAGE_CLOSED_WON) {
                tacitRenewalOpportunities.put(opp.Id, opp);
            }
        }

        if (!tacitRenewalOpportunities.isEmpty()) {
            Set<Id> successfulOpportunityIds = MRO_SRV_Dossier.getInstance().generateTacitRenewalDossiersAndCases(tacitRenewalOpportunities);
            Map<Id, Opportunity> successfulRenewalOpportunities = new Map<Id, Opportunity>();
            for (Id oppId : successfulOpportunityIds) {
                successfulRenewalOpportunities.put(oppId, tacitRenewalOpportunities.get(oppId));
            }
            MRO_SRV_CampaignMember.getInstance().acceptTacitRenewalCampaignMembers(successfulRenewalOpportunities);
        }
    }

    public void afterDelete() {
    }

    public void afterUndelete() {
    }
}