/**
 * Created by Octavian on 1/29/2020.
 */

public with sharing class MRO_LC_ConnectionTypeSelection extends ApexServiceLibraryCnt {
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    public with sharing class getConnectionTypeList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String dossierId = params.get('dossierId');
            if (String.isBlank(dossierId)) {
                throw new WrtsException(System.Label.User + ' - ' + System.Label.MissingId);
            }
            Map<String, String> pickListValues = picklistValues('Dossier__c','SubProcess__c');
            Dossier__c dossier = dossierQuery.getById(dossierId);
            response.put('pickListValues', pickListValues);
            response.put('initialValue', dossier.SubProcess__c);

            return response;
        }

        public Map<String, String> picklistValues(String objectName, String fieldName) {
            Map<String, String> values = new Map<String, String>{};

            List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(new List<String>{
                    objectName
            });
            for (Schema.DescribeSobjectResult res : results) {
                for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                    if (entry.isActive()) {
                        values.put(entry.getValue(), entry.getLabel());
                    }
                }
            }
            return values;

        }

    }

}
