/**
 * Created by giupastore on 14/05/2020.
 * Modified by tommaso.bolis on 19/07/2020
 */
global with sharing class MRO_BA_ReminderBatch implements Schedulable, Database.Batchable<sObject> {

	global MRO_BA_ReminderBatch() {

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {

		DateTime dateTimeToCompare = System.now().addMinutes(59);

		return Database.getQueryLocator(
			'SELECT ID, Code__c, RecordId__c, Status__c ' +
			'FROM CustomReminder__c ' +
			'WHERE ExecutionDateTime__c <=: dateTimeToCompare AND Status__c=\'Pending\'');
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		try {

			Map<String, List<CustomReminder__c>> reminderCodeToReminderListMap = new Map<String, List<CustomReminder__c>>();
			for(CustomReminder__c customReminder : (List<CustomReminder__c>)scope) {

				if(!reminderCodeToReminderListMap.containsKey(customReminder.Code__c)) {

					reminderCodeToReminderListMap.put(customReminder.Code__c, new List<CustomReminder__c>());
				}
				reminderCodeToReminderListMap.get(customReminder.Code__c).add(customReminder);
			}

			List<CustomReminderConfiguration__mdt> customReminderConfigurations =
					MRO_QR_ReminderQueries.getInstance().getCfgReminderByCode(new List<String>(reminderCodeToReminderListMap.keySet()));

			String apexClass;
			for(CustomReminderConfiguration__mdt customReminderConfiguration :customReminderConfigurations) {

				if(String.isBlank(customReminderConfiguration.ApexClass__c)) {

					continue;
				}
				if(!test.isRunningTest()){

					IApexAction ri = (IApexAction) Type.forName(customReminderConfiguration.ApexClass__c).newInstance();
					ri.execute(
						new Map<String, Object> {
							'reminders' => new List<CustomReminder__c>( reminderCodeToReminderListMap.get(customReminderConfiguration.Code__c))
						}
					);
				}
			}

		} catch(Exception e) {

			System.debug('MRO_BA_ReminderBatch.execute - Exception occured: ' + e.getMessage() + ' ' +e.getStackTraceString());
		}
	}

	global void finish(Database.BatchableContext BC) {

	}

	global void execute(SchedulableContext sc){

		Database.executeBatch(new MRO_BA_ReminderBatch());
	}
}