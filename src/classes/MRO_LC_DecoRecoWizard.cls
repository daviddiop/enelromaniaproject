/**
 * Created by giupastore on 28/02/2020.
 * I based the creation of this Apex Class on other class
 * from other Process Click instances, such as Switch Out, New Connection and so on
 *
 */


public with sharing class MRO_LC_DecoRecoWizard extends ApexServiceLibraryCnt {
	private static DatabaseService databaseSrv = DatabaseService.getInstance();
	private static MRO_SRV_Dossier dossierSr = MRO_SRV_Dossier.getInstance();
	private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
	private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
	private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
	private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
	private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
	private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
	private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
	static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
	static String decoRecoRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('DecoReco').getRecordTypeId();
	private static MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();
	private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
	private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
	private static String dossierDraftStatus = constantsSrv.DOSSIER_STATUS_DRAFT;

	static List<ApexServiceLibraryCnt.Option> traderPicklistValues  = new List<ApexServiceLibraryCnt.Option>();
	//static String accountDistributor = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();

	static List<ApexServiceLibraryCnt.Option> commodityPicklistValues  = new List<ApexServiceLibraryCnt.Option>();
	static String commodityLabel;

	public class initialize extends AuraCallable {
		public override Object perform(final String jsonInput) {
			Map<String, String> params = asMap(jsonInput);
			String accountId = params.get('accountId');
			String dossierId = params.get('dossierId');
			String interactionId = params.get('interactionId');
			String companyDivisionId = params.get('companyDivisionId');
			String templateId = params.get('templateId');
			String genericRequestId = params.get('genericRequestId');
			String parentDecoCaseId = params.get('parentDecoCaseId');
			String parentDecoCaseOrigin = params.get('parentDecoCaseOrigin');
			String parentDecoCaseChannel = params.get('parentDecoCaseChannel');

			Map<String, Object> response = new Map<String, Object>();
			try {
				List<Case> cases = new List<Case>();

				if (String.isBlank(accountId)) {
					throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
				}

				Account acc = accountQuery.findAccount(accountId);

				if (String.isBlank(companyDivisionId)) {
					companyDivisionId = null;
				}

				Dossier__c dossier;
				if (String.isBlank(dossierId)) {
					Id cusId = null;
					List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);
					String interactionOrigin;
					String interactionChannel;
					if (!cus.isEmpty()) {
						cusId = cus[0].Id;
						interactionOrigin = cus[0].Interaction__r.Origin__c;
						interactionChannel = cus[0].Interaction__r.Channel__c;
					}
					if (genericRequestId != null) {
						Dossier__c genericRequestDossier = dossierQuery.getById(genericRequestId);
						dossier = genericRequestDossier.clone();
						dossier.Parent__c = genericRequestId;
					} else {
						dossier = new Dossier__c();
						if (parentDecoCaseId != null) {
							dossier.Origin__c = parentDecoCaseOrigin;
							dossier.Channel__c = parentDecoCaseChannel;
						} else {
							dossier.Origin__c = interactionOrigin;
							dossier.Channel__c = interactionChannel;
						}
					}
					Map<String, String> dossierRecordTypes = MRO_UTL_Constants.getDossierRecordTypes('Disconnection');
					dossier.Account__c = accountId;
					dossier.CompanyDivision__c = String.isNotBlank(companyDivisionId) ? companyDivisionId : null;
					dossier.RecordTypeId = dossierRecordTypes.get('Disconnection');
					dossier.Status__c = dossierDraftStatus;
					dossier.RequestType__c= 'Deco-Reco';
					if (!cus.isEmpty()) {
						dossier.CustomerInteraction__c = cusId;
					}
					databaseSrv.insertSObject(dossier);
					dossierId = dossier.Id;
					/*if (!String.isBlank(genericRequestId)) {
						dossierSrv.updateParentGenericRequest(dossier.Id, genericRequestId);
					}*/
				} else {
					dossier = dossierQuery.getById(dossierId);
					cases = caseQuery.getCasesByDossierId(dossier.Id);
				}

				response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
				response.put('companyDivisionId', dossier.CompanyDivision__c);

				String code = 'DisconnectionReconnectionWizardCodeTemplate_1';
				ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
				if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
					response.put('templateId', scriptTemplate.Id);
				}

				getCommodityDescribe();
				response.put('caseTile',cases);
				response.put('dossierId', dossier.Id);
				response.put('dossier', dossier);
				response.put('accountId', accountId);
				response.put('account', acc);
				response.put('accountPersonRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId());
				response.put('accountPersonProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId());
				response.put('accountBusinessRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId());
				response.put('accountBusinessProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId());
				response.put('commodityPicklistValues',commodityPicklistValues);
				response.put('commodityLabel',commodityLabel);
				response.put('commodity', dossier.Commodity__c);
				response.put('decoRecoRecordType', decoRecoRecordType);
				response.put('error', false);


			} catch (Exception ex) {
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
			}
			return response;
		}
	}
	public static void getCommodityDescribe () {
		commodityPicklistValues = new List<ApexServiceLibraryCnt.Option>();
		commodityPicklistValues.add(new ApexServiceLibraryCnt.Option('Electric', 'Electric'));
		commodityPicklistValues.add(new ApexServiceLibraryCnt.Option('Gas', 'Gas'));
		commodityLabel = System.Label.commodity;
	}

	public class saveDraftBP extends AuraCallable {
		public override Object perform(final String jsonInput) {
			DraftBillingProfileInput billingProfileParams =(DraftBillingProfileInput) JSON.deserialize(jsonInput, DraftBillingProfileInput.class);
			Map<String, Object> response = new Map<String, Object>();
			List<Case> caseList = new List<Case>();

			Savepoint sp = Database.setSavepoint();
			response.put('error', false);
			try {
				if ((!billingProfileParams.oldCaseList.isEmpty())) {

					for (Case caseRecord : billingProfileParams.oldCaseList) {
						caseList.add(new Case(Id = caseRecord.Id));
					}
					if (!caseList.isEmpty()) {
						databaseSrv.updateSObject(caseList);
					}
				}
			} catch (Exception ex) {
				Database.rollback(sp);
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
			}
			return response;
		}
	}

	public class DraftBillingProfileInput{
		@AuraEnabled
		public List<Case> oldCaseList{get; set;}
		@AuraEnabled
		public String billingProfileId{get; set;}
	}

	public class createCase extends AuraCallable {

		public override Object perform(final String jsonInput) {
			System.debug('jsonInput **********************> ' + JSON.serialize('jsonInput *********> ' + jsonInput));
			CreateCaseInput createCaseParams =(CreateCaseInput) JSON.deserialize(jsonInput, CreateCaseInput.class);
			System.debug('11111');
			Map<String, Object> response = new Map<String, Object>();
			Map<String, String> decoRecoRecordTypes = Constants.getCaseRecordTypes('DecoReco');
			List<Case> newCases = new List<Case>();
			String recordTypeId;
			//String companyDivisionId = createCaseParams.searchedSupplyFieldsList[0].CompanyDivision__c;

			response.put('error', false);
			try {
				if (/*String.isNotBlank(createCaseParams.trader) && (createCaseParams.effectiveDate != null) && (createCaseParams.referenceDate != null) && */
					(!createCaseParams.searchedSupplyFieldsList.isEmpty()) &&
					(String.isNotBlank(createCaseParams.accountId)) && (String.isNotBlank(createCaseParams.dossierId))) {
					System.debug('22222');
					for (Supply__c supply : createCaseParams.searchedSupplyFieldsList) {
						recordTypeId = decoRecoRecordTypes.get('DecoReco');
						Case disconnectedCase;
						if(createCaseParams.durationValue != null ) {
							disconnectedCase = new Case(
								Supply__c = supply.Id,
								Supply__r = supply,
								ServiceSite__c = supply.ServiceSite__c,
								Duration__c = createCaseParams.durationValue,
								//Description = createCaseParams.noteValue,
								CustomerNotes__c = createCaseParams.noteValue,
								StartDate__c = createCaseParams.startDateValue, //createCaseParams.startDateValue.addMonths((Integer) Integer.valueOf(createCaseParams.durationValue)),
								Type = createCaseParams.requestTypeLabel,
								CompanyDivision__c = supply.CompanyDivision__c,
								AccountId = createCaseParams.accountId,
								RecordTypeId = recordTypeId,
								Dossier__c = createCaseParams.dossierId,
								Status = 'Draft',
								Origin = createCaseParams.originSelected,
								Channel__c = createCaseParams.channelSelected
							);
						} else {
							System.debug('33333');
								disconnectedCase = new Case(
								Supply__c = supply.Id,
								Supply__r = supply,
								Duration__c = null,
								ServiceSite__c = supply.ServiceSite__c,
								//Description = createCaseParams.noteValue,
                                CustomerNotes__c = createCaseParams.noteValue,
								StartDate__c = createCaseParams.startDateValue,
								Type = createCaseParams.requestTypeLabel,
								CompanyDivision__c = supply.CompanyDivision__c,
								AccountId = createCaseParams.accountId,
								RecordTypeId = recordTypeId,
								Dossier__c = createCaseParams.dossierId,
								Status = 'Draft',
								Origin = createCaseParams.originSelected,
								Channel__c = createCaseParams.channelSelected
							);
						}
						newCases.add(disconnectedCase);
					}
					if (!newCases.isEmpty()) {
						databaseSrv.insertSObject(newCases);

						if (!createCaseParams.caseList.isEmpty()) {
							for (Case oldCaseList : createCaseParams.caseList) {
								newCases.add(oldCaseList);
							}
						}
						response.put('caseTile', newCases);
					}
				}
			} catch (Exception ex) {
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
				System.debug('response createCase *********> ' + JSON.serialize(response));
			}

			System.debug('response createCase*********> ' + JSON.serialize(response));
			return response;
		}
	}

	public class CreateRecoCase extends AuraCallable {
		public override Object perform(final String jsonInput) {
			Map<String, String> params = asMap(jsonInput);
			Map<String, Object> response = new Map<String, Object>();
			Map<String, String> decoRecoRecordTypes = Constants.getCaseRecordTypes('DecoReco');
			List<Case> newCases = new List<Case>();
			String recordTypeId;
			String accountId = params.get('accountId');
			String dossierId = params.get('dossierId');
			String requestTypeLabel = params.get('requestTypeLabel');
			String supplyId = params.get('supplyId');
			String parentCaseId = params.get('parentCaseId');
			String customerNotes = params.get('customerNotes');
			Supply__c supply = supplyQuery.getById(supplyId);
			Case parentCase = caseQuery.getById(parentCaseId);

			response.put('error', false);
			try {
				if ((String.isNotBlank(accountId)) && (String.isNotBlank(dossierId))) {
					recordTypeId = decoRecoRecordTypes.get('DecoReco');
					Case reconnectionCase;
					reconnectionCase = new Case(
							Supply__c = supply.Id,
							Supply__r = supply,
							ServiceSite__c = supply.ServiceSite__c,
							Type = requestTypeLabel,
							CompanyDivision__c = supply.CompanyDivision__c,
							StartDate__c = Date.today(),
							CustomerNotes__c = customerNotes,
							AccountId = accountId,
							RecordTypeId = recordTypeId,
							Dossier__c = dossierId,
							Parent = parentCase,
							ParentId = parentCaseId,
							Status = 'Draft',
							Origin = parentCase.Origin,
							Channel__c = parentCase.Channel__c
					);
					newCases.add(reconnectionCase);
					if (!newCases.isEmpty()) {
						databaseSrv.insertSObject(newCases);
						response.put('caseTile', newCases);
					}
				}
			} catch (Exception ex) {
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
			}
			return response;
		}
	}

	public class CreateCaseInput{
		@AuraEnabled
		public List<Supply__c> searchedSupplyFieldsList{get; set;}
		@AuraEnabled
		public Date startDateValue{get; set;}
		@AuraEnabled
		public Decimal durationValue {get; set;}
		@AuraEnabled
		public String requestTypeLabel{get; set;}
		@AuraEnabled
		public String noteValue{get; set;}
		@AuraEnabled
		public String accountId{get; set;}
		@AuraEnabled
		public List<Case> caseList{get; set;}
		@AuraEnabled
		public String dossierId{get; set;}
		@AuraEnabled
		public String channelSelected{get; set;}
		@AuraEnabled
		public String originSelected{get; set;}
	}

	public class updateCaseList extends AuraCallable {
		public override Object perform(final String jsonInput) {
			UpdateCaseListInput updateCaseListParams =(UpdateCaseListInput) JSON.deserialize(jsonInput, UpdateCaseListInput.class);
			Map<String, Object> response = new Map<String, Object>();
			List<Case> caseList = new List<Case>();
			List<Supply__c> supplyList = new List<Supply__c>();
			Savepoint sp = Database.setSavepoint();
			response.put('error', false);

			try {
				if (!updateCaseListParams.oldCaseList.isEmpty() && String.isNotBlank(updateCaseListParams.dossierId)) {
					//add  by david
					String companyDivisionId = updateCaseListParams.oldCaseList[0].CompanyDivision__c;
					//

					Dossier__c dossier = new Dossier__c (Id= updateCaseListParams.dossierId ,Status__c = 'New' ,CompanyDivision__c = companyDivisionId);
					if (dossier != null) {
						databaseSrv.updateSObject(dossier);
					}
					for (Case caseRecord : updateCaseListParams.oldCaseList) {

						Case caseToUpdate = new Case(
							Id = caseRecord.Id,
							Dossier__c = dossier.Id,
							Status ='New',
							CaseTypeCode__c = 'C44',
							EffectiveDate__c = caseRecord.StartDate__c,
							SLAExpirationDate__c =System.today().addDays(MRO_UTL_Date.calculateWorkDays(System.today(),5)),
							SubProcess__c = caseRecord.Type
						);

						caseList.add(caseToUpdate);
						//supplyList.add(new Supply__c(Id = caseRecord.Supply__c, Terminator__c = caseRecord.Id));
					}
					/*if (!supplyList.isEmpty()) {
						databaseSrv.updateSObject(supplyList);
					}*/
					if (!caseList.isEmpty()) {
						databaseSrv.updateSObject(caseList);
					}

					MRO_SRV_Case.getInstance().applyAutomaticTransitionOnDossierAndCases(updateCaseListParams.dossierId);
				}
			} catch (Exception ex) {
				Database.rollback(sp);
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
				System.debug('updateCaseList ***** ' + response);
			}
			return response;
		}
	}

	public class UpdateCaseListInput{
		@AuraEnabled
		public List<Case> oldCaseList{get; set;}
		@AuraEnabled
		public String dossierId{get; set;}
	}

	public class CancelProcess extends AuraCallable {
		public override Object perform(final String jsonInput) {
			Map<String, String> params = asMap(jsonInput);
			Map<String, Object> response = new Map<String, Object>();
			String dossierId = params.get('dossierId');
			String cancelReason = params.get('cancelReason');
			String cancelDetails = params.get('detailsReason');

			List<Case> caseList = caseQuery.getCasesByDossierId(dossierId);
			Dossier__c dossier = dossierQuery.getById(dossierId);
			dossier.CancellationDetails__c = cancelDetails;
			dossier.CancellationReason__c = cancelReason;

			for (Case caseRecord : caseList) {
				caseRecord.CancellationReason__c = cancelReason;
				caseRecord.CancellationDetails__c = cancelDetails;
			}

			Savepoint sp = Database.setSavepoint();
			try {
				databaseSrv.updateSObject(dossier);
				caseSrv.setCanceledOnCaseAndDossier(caseList, dossierId);
				response.put('error', false);
			}catch (Exception ex) {
				Database.rollback(sp);
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
			}
			return new Map<String, Object>();
		}
	}

	/**
	 * @author  Boubacar Sow
	 * @description update dossier
	 *              [ENLCRO-523] SwitchOut - Integrate Channel & Origin selection
	 * @date 13/01/2020
	 * @param DossierId,channelSelected,originSelected
	 *
	 */
	public class UpdateDossier extends AuraCallable {
		public override Object perform(final String jsonInput) {
			Map<String, Object> response = new Map<String, Object>();
			Map<String, String> params = asMap(jsonInput);
			String dossierId = params.get('dossierId');
			String channelSelected = params.get('channelSelected');
			String originSelected = params.get('originSelected');

			dossierSrv.updateDossierChannel(dossierId,channelSelected,originSelected);

			response.put('error', false);
			return response;
		}
	}

	public class getSupplyRecord extends AuraCallable {
		public override Object perform(final String jsonInput) {
			Map<String, Object> response = new Map<String, Object>();
			supplyInputIds createCaseParams = (supplyInputIds) JSON.deserialize(jsonInput, supplyInputIds.class);

			List<Case> currentCases = (List<Case>)JSON.deserialize(createCaseParams.caseList, List<Case>.class);
			Set<Id> currentCaseIds = new Set<Id>();
			if(currentCases != null){
				currentCaseIds = new Map<Id, Case>(currentCases).keySet();
			}
			Integer existCasesCount = [SELECT COUNT() FROM Case WHERE Supply__c IN :createCaseParams.supplyIds AND Status != 'Canceled' AND AccountId = :createCaseParams.accountId AND SubProcess__c = :createCaseParams.subProcess AND Id NOT IN :currentCaseIds LIMIT :currentCaseIds.size() + 1];
			if(existCasesCount > 0){
				response.put('error', true);
				response.put('errorMessage', 'A request is already open for selected POD');
				return response;
			}

			List<Supply__c> supplyListRecord = supplyQuery.getSuppliesByIds(createCaseParams.supplyIds);
			response.put('supplies', supplyListRecord);
			return response;
		}
	}

	public class CancelProcessInput{
		@AuraEnabled
		public List<Case> oldCaseList{get; set;}
		@AuraEnabled
		public String dossierId{get; set;}
	}
	public class supplyInputIds {
		@AuraEnabled
		public List<Id> supplyIds { get; set; }
		@AuraEnabled
		public String subProcess { get; set; }
		@AuraEnabled
		public String accountId { get; set; }
		@AuraEnabled
		public String caseList { get; set; }
	}

	public with sharing class updateCommodityToDossier extends AuraCallable {
		public override Object perform(final String jsonInput) {
			Map<String, String> params = asMap(jsonInput);
			String dossierId = params.get('dossierId');
			String commodity = params.get('commodity');

			Map<String, Object> response = new Map<String, Object>();
			//Savepoint sp = Database.setSavepoint();
			try {
				Dossier__c doss = new Dossier__c();
				doss.Id = dossierId;
				doss.Commodity__c = commodity;
				databaseSrv.upsertSObject(doss);
				response.put('dossierId', doss.Id);
				response.put('commodity', doss.Commodity__c);
				response.put('error', false);
			} catch (Exception ex) {
				//Database.rollback(sp);
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
			}
			return response;
		}
	}


	public with sharing class deleteCases extends AuraCallable {
		public override Object perform(final String jsonInput) {
			CancelProcessInput cancelProcessParams =(CancelProcessInput) JSON.deserialize(jsonInput, CancelProcessInput.class);
			List<Case> caseToDelete = new List<Case>();
			Map<String, Object> response = new Map<String, Object>();
			Savepoint sp = Database.setSavepoint();
			response.put('error', false);
			try {
				if (!cancelProcessParams.oldCaseList.isEmpty()) {
					for (Case caseRecord : cancelProcessParams.oldCaseList) {
						caseToDelete.add(new Case(Id = caseRecord.Id));
					}
					delete caseToDelete;
				}
			} catch (Exception ex) {
				System.debug(ex.getStackTraceString());
				Database.rollback(sp);
				response.put('error', true);
				response.put('errorMsg', ex.getMessage());
				response.put('errorTrace', ex.getStackTraceString());
			}
			return response;
		}
	}

	public with sharing class setChannelAndOrigin extends AuraCallable {
		public override Object perform(final String jsonInput) {
			Map<String, String> params = asMap(jsonInput);
			String dossierId = params.get('dossierId');
			String origin = params.get('origin');
			String channel = params.get('channel');
			dossierSr.setChannelAndOrigin(dossierId,origin,channel);
			return true;
		}
	}

}