/**
 * Created by napoli on 28/10/2019.
 */

@isTest
private class MRO_SRV_ExecutionLogTst {
    @isTest
    static void unit_test_1() {
        Test.StartTest();
        try{
            Integer i = Integer.valueof('a');
        }catch(Exception e) {
            MRO_SRV_ExecutionLog.createDebugLog(e, 'test','test','recordId');
        }
        Test.StopTest();
        System.assertEquals(1, [select count() from ExecutionLog__c]);
    }

    @isTest
    static void unit_test_2() {
        Test.StartTest();
        String message = generateRandomString (140000);
        MRO_SRV_ExecutionLog.createDebugLog('Apex Exception', 'subject', message, message, 'test', 'recordId');
        Test.StopTest();
        System.assertEquals(1, [select count() from ExecutionLog__c]);
        System.assertEquals(2, [select count() from Attachment]);
    }

    @isTest
    static void unit_test_3() {
        String message = generateRandomString (140000);
        Test.StartTest();
        MRO_SRV_ExecutionLog.createIntegrationLog('CallOut', '32132-321-3213-321321', 'SWI-001', '50032434342', message, 'KO', 'errorTest', 'test', 'test', null, '', null);
        MRO_SRV_ExecutionLog.createIntegrationLog('CallIn', '32132-321-3213-321321', 'SWI-002', '50032434342', message, 'KO', 'errorTest', 'test', 'test', '32132-321-3213-321321', '', null);
        Test.StopTest();
        System.assertEquals(1, [select count() from ExecutionLog__c where ParentLog__c != null]);
        System.assertEquals(1, [select count() from ExecutionLog__c where ParentLog__c = null]);
        System.assertEquals(2, [select count() from Attachment]);
    }

    @isTest
    static void unit_test_4() {
        Test.StartTest();
        MRO_SRV_ExecutionLog.createDebugLog(null, 'test', 'test', 'recordId');
        Test.StopTest();
        System.assertEquals(0, [select count() from ExecutionLog__c]);
        System.assertEquals(0, [select count() from Attachment]);
    }

    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr;
    }
}