/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   nov 11, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_Bit2WinCart extends ApexServiceLibraryCnt {

    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();
    private static final MRO_QR_Pricebook pricebookQuery = MRO_QR_Pricebook.getInstance();
    private static final MRO_QR_Product productQuery = MRO_QR_Product.getInstance();
    private static MRO_QR_Opportunity opportunityQuery = MRO_QR_Opportunity.getInstance();

    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    private static final MRO_SRV_OrderItem orderItemService = MRO_SRV_OrderItem.getInstance();

    public static MRO_SRV_Bit2WinCart getInstance() {
        return (MRO_SRV_Bit2WinCart) ServiceLocator.getInstance(MRO_SRV_Bit2WinCart.class);
    }

    public class CartItem {
        @AuraEnabled
        public String configurationItemId {get;set;}
        @AuraEnabled
        public String productName {get;set;}
        @AuraEnabled
        public String productId {get;set;}
        @AuraEnabled
        public String salesType {get;set;}
        @AuraEnabled
        public Decimal quantity {get;set;}
        @AuraEnabled
        public Decimal price {get;set;}
        @AuraEnabled
        public Decimal recurringPrice {get;set;}

        public CartItem(String configurationItemId, String productName, String productId, String salesType, Decimal quantity, Decimal price, Decimal recurringPrice) {
            this.configurationItemId = configurationItemId;
            this.productName = productName;
            this.productId = productId;
            this.salesType = salesType;
            this.quantity = quantity;
            this.price = price;
            this.recurringPrice = recurringPrice;
        }

        public CartItem(NE__OrderItem__c configurationItem) {
            this.configurationItemId = configurationItem.Id;
            this.productName = configurationItem.NE__ProdName__c;
            this.productId = configurationItem.NE__ProdId__c;
            this.salesType = configurationItem.NE__ProdId__r.NE__Sales_Type__c;
            this.quantity = configurationItem.NE__Qty__c;
            this.price = configurationItem.NE__BaseOneTimeFee__c;
            this.recurringPrice = configurationItem.NE__BaseRecurringCharge__c;
        }

        public Decimal getPrice() {
            if (this.price != null && this.price != 0) {
                return this.price;
            } else if(this.recurringPrice != null) {
                return this.recurringPrice;
            } else {
                return 0;
            }
        }
    }

    public List<OpportunityLineItem> createOLIs(Id opportunityId, List<CartItem> cartItemList) {
        // TODO: For Demo purpose ONLY
        List<Pricebook2> priceBookList = pricebookQuery.listStandard();
        if (priceBookList.isEmpty()) {
            throw new WrtsException('Standard Price Book do not exist');
        }

        Opportunity myOpportunity = new Opportunity(Id = opportunityId, Pricebook2Id = priceBookList.get(0).Id);
        databaseSrv.updateSObject(myOpportunity);

        Set<String> configurationItemIds = new Set<String>();
        CartItem electricityCartItem, gasCartItem, serviceCartItem;
        Map<String, MRO_SRV_Bit2winCart.CartItem> productIdToCartItem = new Map<String, MRO_SRV_Bit2winCart.CartItem>();
        for (MRO_SRV_Bit2winCart.CartItem item : cartItemList) {
            productIdToCartItem.put(item.productId, item);
            configurationItemIds.add(item.configurationItemId);
            if (String.isBlank(item.salesType)) {
                throw new WrtsException(String.format('{0} {1} ({2})', new List<String>{Label.SalesTypeIsBlank, item.productName, item.productId}));
            }
            if (item.salesType == 'Electricity') {
                electricityCartItem = item;
            } else if (item.salesType == 'Gas') {
                gasCartItem = item;
            } else if (item.salesType == CONSTANTS.SALES_TYPE_HARD_VAS || item.salesType == CONSTANTS.SALES_TYPE_SOFT_VAS) {
                serviceCartItem = item;
            }
        }

        List<OpportunityServiceItem__c> osis = MRO_QR_OpportunityServiceItem.getInstance().getOSIsByOpportunityId(opportunityId);
        for (OpportunityServiceItem__c osi : osis) {
            System.debug(osi.RecordType.DeveloperName);
            if (osi.RecordType.DeveloperName == CONSTANTS.COMMODITY_ELECTRIC && electricityCartItem != null) {
                osi.ConfigurationItem__c = electricityCartItem.configurationItemId;
            } else if (osi.RecordType.DeveloperName == CONSTANTS.COMMODITY_GAS && gasCartItem != null) {
                osi.ConfigurationItem__c = gasCartItem.configurationItemId;
            } else if (osi.RecordType.DeveloperName == CONSTANTS.VAS && serviceCartItem != null) {
                osi.ConfigurationItem__c = serviceCartItem.configurationItemId;
            }
        }
        databaseSrv.updateSObject(osis);

        List<Product2> product2List = productQuery.listByCommercialProductIds(productIdToCartItem.keySet());
        Map<String, String> commercialProductIdToId = SobjectUtils.mapTwoFields(product2List, Product2.CommercialProduct__c, Product2.Id);
        List<Product2> product2Insert = new List<Product2>();
        for (String productId : productIdToCartItem.keySet()) {
            if (!commercialProductIdToId.containsKey(productId)) {
                CartItem item = productIdToCartItem.get(productId);
                product2Insert.add(new Product2(
                    Name = item.productName,
                    CommercialProduct__c = item.productId,
                    Key__c = String.valueOf(System.currentTimeMillis()),
                    IsActive = true
                ));
            }
        }
        databaseSrv.insertSObject(product2Insert);

        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        for (Product2 myProduct : product2Insert) {
            pricebookEntryList.add(new PricebookEntry(
                Pricebook2Id = priceBookList.get(0).Id,
                Product2Id = myProduct.Id,
                IsActive = true,
                UnitPrice = 1
            ));
            commercialProductIdToId.put(myProduct.Name, myProduct.Id);
        }
        databaseSrv.insertSObject(pricebookEntryList);
        // For Demo purpose ONLY

        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        for(CartItem item : cartItemList) {
            OpportunityLineItem oli = new OpportunityLineItem();
            oli.Product2Id = commercialProductIdToId.get(item.productId);
            oli.OpportunityId = opportunityId;
            oli.Quantity = item.quantity;
            oli.UnitPrice = item.getPrice();
            oliList.add(oli);
        }
        databaseSrv.insertSObject(oliList);

        Opportunity currentOpportunity = opportunityQuery.getOpportunityById(opportunityId);
        List<String> acquisitionRequestTypes = new List<String>{'SwitchIn', 'Activation', 'Connection', 'Transfer', 'ProductChange'};
        if (acquisitionRequestTypes.contains(currentOpportunity.RequestType__c)) {
            orderItemService.clone(configurationItemIds);
        }

        return oliList;
    }
}