/**
 * Created by tommasobolis on 20/10/2020.
 */

public with sharing class MRO_LC_PreliminaryDataCheck {

    private static MRO_QR_Account accountQry = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQry = MRO_QR_Case.getInstance();
    private static MRO_QR_Dossier dossierQry = MRO_QR_Dossier.getInstance();
    private static MRO_QR_CustomerInteraction customerInteractionQry = MRO_QR_CustomerInteraction.getInstance();
    private static MRO_QR_Opportunity opportunityQry = MRO_QR_Opportunity.getInstance();
    public static MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();

    @AuraEnabled
    public static Map<String, Object> doPreliminaryDataCheck(Id recordId){

        Map<String, Object> returnValue = new Map<String, Object> ();
        returnValue.put('error', false);
        try {

            List<String> blocking = new List<String>();
            List<String> warning = new List<String>();

            String sObjectType = recordId.getSobjectType().getDescribe().getName(); //retrieve the SObject type of the SObject where the component 'mlm_lwc-relatedSObject' is placed
            System.debug('MRO_LC_PreliminaryDataCheck.doPreliminaryDataCheck - sObjectType: ' + sObjectType);

            if(recordId.getSobjectType().getDescribe().getName() == Schema.SObjectType.Opportunity.name) {

                Opportunity opp = opportunityQry.getOpportunityById(recordId);

            } else if(recordId.getSobjectType().getDescribe().getName() == Schema.SObjectType.Dossier__c.name) {

                Dossier__c dos = dossierQry.getById(recordId);
                Map<String,List<String>> accountCheckResults = checkAccount(dos.Account__r,
                        Schema.SObjectType.Dossier__c.name, dos);
                blocking.addAll(accountCheckResults.get('blocking'));
                warning.addAll(accountCheckResults.get('warning'));

            } else if(recordId.getSobjectType().getDescribe().getName() == Schema.SObjectType.Case.name) {

                Case cas = caseQry.getById(recordId);

            } else if(recordId.getSobjectType().getDescribe().getName() == Schema.SObjectType.CustomerInteraction__c.name) {

                CustomerInteraction__c cin = customerInteractionQry.findById(recordId);
                Map<String, List<String>> accountCheckResults = checkAccount(cin.Customer__r,
                        Schema.SObjectType.CustomerInteraction__c.name);
                blocking.addAll(accountCheckResults.get('blocking'));
                warning.addAll(accountCheckResults.get('warning'));
            }

            returnValue.put('blocking', blocking);
            returnValue.put('warning', warning);

        }catch(Exception e){

            returnValue.put('error', true);
            returnValue.put('errorMessage', e.getMessage());
            returnValue.put('errorStackTraceString', e.getStackTraceString());
        }
        return returnValue;
    }

    private static Map<String,List<String>> checkAccount(Account account, String sObjectName) {

        return checkAccount(account, sObjectName, null);
    }

    private static Map<String,List<String>> checkAccount(Account account, String sObjectName, SObject record) {

        System.debug('MRO_LC_PreliminaryDataCheck.checkAccount - account: ' + account);
        System.debug('MRO_LC_PreliminaryDataCheck.checkAccount - record: ' + record);
        Map<String,List<String>> accountCheckResults = new Map<String,List<String>>();
        accountCheckResults.put('blocking', new List<String>());
        accountCheckResults.put('warning', new List<String>());
        Id recordTypeId = record != null ? (Id)record.get('RecordTypeId') : null;
        // Dossier
        if(sObjectName == Schema.SObjectType.Dossier__c.name) {

            Dossier__c dos = (Dossier__c) record;
            // Power Voltage Change, Connection and Meter Change for person account
            if( MRO_UTL_Constants.getDossierRecordTypes('Connection').values().contains(recordTypeId) &&
                (dos.RequestType__c == constantsUtl.DOSSIER_REQUESTTYPE_CONNECTION||
                    dos.RequestType__c == constantsUtl.DOSSIER_REQUESTTYPE_METERCHANGE||
                    dos.RequestType__c == constantsUtl.DOSSIER_REQUESTTYPE_TECHNICALDATACHANGE)) {

                if (account.IsPersonAccount && String.isBlank(account.PersonMobilePhone) && String.isBlank(account.Phone)) {
                    accountCheckResults.get('blocking').add('For this customer, the contact phone number is missing! For this process the phone number is mandatory! Please check!');
                }else if(!account.IsPersonAccount && account.PrimaryContact__c == null){
                    accountCheckResults.get('blocking').add('There is no primary contact specified for this customer!');
                }else if(!account.IsPersonAccount && account.PrimaryContact__c != null && account.PrimaryContact__r.MobilePhone == null && account.PrimaryContact__r.Phone == null){
                    accountCheckResults.get('blocking').add('There is no phone number specified for primary contact! For this process the phone number is mandatory! Please check!');
                }
            }

            // Acquisition processes
            if(MRO_UTL_Constants.getDossierRecordTypes('Acquisition').values().contains(recordTypeId)) {

                if(String.isNotBlank(account.Description)  &&
                        (account.Description.toLowerCase().contains('lipsa') || account.Description.toLowerCase().contains('multiplu'))) {

                    accountCheckResults.get('blocking').add(account.Name + ' - ' + account.Description);
                }
                if(account.IsPersonAccount && String.isNotBlank(account.NationalIdentityNumber__pc) &&
                        account.NationalIdentityNumber__pc.toLowerCase().startsWith('f')) {

                    accountCheckResults.get('blocking').add('A valid national identity number is missing on the account');
                }
            // Change processes
            } else if(MRO_UTL_Constants.getDossierRecordTypes('Change').values().contains(recordTypeId)){

                if(String.isNotBlank(dos.SubProcess__c) &&
                        dos.SubProcess__c != constantsUtl.SUB_PROCESS_PRODCHNG_SAMEMARK_APINAME &&
                        account.IsPersonAccount && String.isNotBlank(account.NationalIdentityNumber__pc) &&
                        account.NationalIdentityNumber__pc.toLowerCase().startsWith('f')) {

                    accountCheckResults.get('blocking').add('A valid national identity number is missing on the account');

                } else if (dos.RequestType__c == constantsUtl.DOSSIER_REQUESTTYPE_METERCHECK) {


                    if ((account.IsPersonAccount && String.isBlank(account.PersonMobilePhone)) ||
                            (!account.IsPersonAccount && String.isBlank(account.Phone))) {

                        accountCheckResults.get('blocking').add('A valid mobile phone number is missing on the account');
                    }
                }

            // Other processes
            } else {

                if(String.isNotBlank(account.Description)  &&
                        (account.Description.toLowerCase().contains('lipsa') || account.Description.toLowerCase().contains('multiplu'))) {

                    accountCheckResults.get('warning').add(account.Name + ' - ' + account.Description);
                }
            }

        // Interaction
        } else if(sObjectName == Schema.SObjectType.CustomerInteraction__c.name) {

            if(String.isNotBlank(account.Description)  &&
                    (account.Description.toLowerCase().contains('lipsa') || account.Description.toLowerCase().contains('multiplu'))) {

                accountCheckResults.get('warning').add(account.Name + ' - ' + account.Description);
            }
        }
        return accountCheckResults;
    }
}