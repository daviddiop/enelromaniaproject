/**
 * Created by Vlad Mocanu on 24/10/2019.
 */

public with sharing class OpportunityJsonBuilder implements IJsonBuilder {
    public SObject getData(String recordId) {
        return OpportunityQueries.getInstance().getById(recordId);
    }
}