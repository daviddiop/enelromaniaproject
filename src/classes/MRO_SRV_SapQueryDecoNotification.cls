/**
 * Created by giupastore on 01/09/2020.
 */

public with sharing class MRO_SRV_SapQueryDecoNotification extends MRO_SRV_SapQueryHelper {

	private static final String requestTypeQueryDisconnectionNotifications = 'QueryDisconnectionNotifications';

	public static MRO_SRV_SapQueryDecoNotification getInstance() {
		return new MRO_SRV_SapQueryDecoNotification();
	}

	public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {

		Set<String> conditionFields = new Set<String>{
			 'StartDate', 'EndDate', 'CustomerCode'
		};
		return buildSimpleRequest(argsMap, conditionFields);
	}

	public List<MRO_LC_DecoRecoNotification.DisconnectionNotification> getDecoRecoList (MRO_LC_DecoRecoNotification.DisconnectionNotificationParam params) {
		Map<String, Object> conditions = new Map<String, Object>();
		try{
			 conditions.put('StartDate', Date.parse(params.startDate));
			 conditions.put('EndDate', Date.parse(params.endDate));
		 }catch (Exception e){
			 System.debug('Invalid Date InvoiceRequestParam ' + params.startDate + ' ' + params.endDate);
		 }

		if (params.customerCode != null) {
			conditions.put('CustomerCode', params.customerCode);
		}

		wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeQueryDisconnectionNotifications, conditions, 'MRO_SRV_SapQueryDecoNotification');

		if (calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null && calloutResponse.responses[0].objects != null && !calloutResponse.responses[0].objects.isEmpty()) {

			List<MRO_LC_DecoRecoNotification.DisconnectionNotification> notificationList = new List<MRO_LC_DecoRecoNotification.DisconnectionNotification>();

			for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {
				Map<String, String> responseWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);

						Map<String, String> notificationWoMap = MRO_UTL_MRRMapper.wobjectToMap(responseWo);

						MRO_LC_DecoRecoNotification.DisconnectionNotification notificationDTO = new MRO_LC_DecoRecoNotification.DisconnectionNotification(notificationWoMap);

						notificationList.add(notificationDTO);
			}

			return notificationList;
		}
		return null;
	}

}