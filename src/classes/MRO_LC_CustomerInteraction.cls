public with sharing class MRO_LC_CustomerInteraction extends ApexServiceLibraryCnt {

    private static final MRO_SRV_CustomerInteraction customerInteractionSrv = MRO_SRV_CustomerInteraction.getInstance();
    private static final InteractionService interactionSrv = InteractionService.getInstance();
    private static final AccountContactRelationService accountContactRelationSrv = AccountContactRelationService.getInstance();

    public class initData extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            return new Map<String, Object>{
                'rolesList' => interactionSrv.listOptions(AccountContactRelation.Roles),
                'customerInteractionList' => customerInteractionSrv.listByInteractionId(params.get('interactionId')),
                'isInteractionClosed' => interactionSrv.isInteractionClosed(params.get('interactionId'))
            };
        }
    }

    public class updateRole extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            MRO_SRV_CustomerInteraction.CustomerInteractionDTO customerInteractionDto = (MRO_SRV_CustomerInteraction.CustomerInteractionDTO)JSON.deserialize(params.get('customerInteraction'), MRO_SRV_CustomerInteraction.CustomerInteractionDTO.class);
            return updateRole(customerInteractionDto);
        }
    }

    @AuraEnabled(cacheable=false)
    public static List<MRO_SRV_CustomerInteraction.CustomerInteractionDTO> updateRole(MRO_SRV_CustomerInteraction.CustomerInteractionDTO customerInteraction) {
        if (String.isBlank(customerInteraction.relationId)) {
            throw new WrtsException(System.Label.Relation + ' - ' + System.Label.MissingId);
        } else if (String.isBlank(customerInteraction.role)) {
            throw new WrtsException(System.Label.Roles + ' - ' + System.Label.Required);
        }
        accountContactRelationSrv.updateRole(customerInteraction.relationId, customerInteraction.role);
        return customerInteractionSrv.listByInteractionId(customerInteraction.interactionId);
    }

    public class deleteCustomerInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            // MRO_SRV_CustomerInteraction.CustomerInteractionDTO customerInteractionDto = (MRO_SRV_CustomerInteraction.CustomerInteractionDTO)JSON.deserialize(jsonInput, MRO_SRV_CustomerInteraction.CustomerInteractionDTO.class);
            Map<String, String> params = asMap(jsonInput);
            MRO_SRV_CustomerInteraction.CustomerInteractionDTO customerInteractionDto = (MRO_SRV_CustomerInteraction.CustomerInteractionDTO)JSON.deserialize(params.get('customerInteraction'), MRO_SRV_CustomerInteraction.CustomerInteractionDTO.class);
            return deleteCustomerInteraction(customerInteractionDto);
        }
    }

    @AuraEnabled
    public static List<MRO_SRV_CustomerInteraction.CustomerInteractionDTO> deleteCustomerInteraction(MRO_SRV_CustomerInteraction.CustomerInteractionDTO customerInteraction) {
        if (String.isBlank(customerInteraction.id)) {
            throw new WrtsException(System.Label.CustomerInteraction + ' - ' + System.Label.MissingId);
        }
        customerInteractionSrv.deleteById(customerInteraction.id);
        return  customerInteractionSrv.listByInteractionId(customerInteraction.interactionId);
    }
}