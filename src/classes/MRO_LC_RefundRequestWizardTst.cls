/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 18.03.20.
 */

@IsTest
public with sharing class MRO_LC_RefundRequestWizardTst {

    @TestSetup
    private static void setup() {

        //Insert Sequencer
        Sequencer__c sequencer = new Sequencer__c(Sequence__c = 1, SequenceLength__c = 1, Type__c = 'CustomerCode');
        insert sequencer;


        //Insert Accounts
        List<Account> listAccount = new List<Account>();
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        listAccount.add(personAccount);

        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.BusinessType__c = 'NGO';
        listAccount.add(businessAccount);

        Account distributorAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        distributorAccount.Name = 'DistributorAccount1';
        distributorAccount.BusinessType__c = 'Real estate';
        distributorAccount.IsDisCoENEL__c = true;
        distributorAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        listAccount.add(distributorAccount);

        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.Name = 'TraderAccount1';
        accountTrader.VATNumber__c = 'RO3911430';
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.Key__c = '1324433';
        listAccount.add(accountTrader);
        insert listAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = businessAccount.Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, personAccount.Id, contact.Id).build();
        insert customerInteraction;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Account__c = businessAccount.Id;
        servicePoint.Trader__c = accountTrader.Id;
        servicePoint.ENELTEL__c = '123456789';
        servicePoint.Distributor__c = distributorAccount.Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = businessAccount.Id;
        insert contract;

        Bank__c bank = new Bank__c(Name = 'Test Bank', BIC__c = '5555');
        insert bank;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = businessAccount.Id;
        billingProfile.PaymentMethod__c = 'Direct Debit';
        billingProfile.Bank__c = bank.Id;
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
        billingProfile.RefundIBAN__c = 'IT60X0542811101000000123456';
        insert billingProfile;

        List<Supply__c> supplyList = new List<Supply__c>();
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Account__c = businessAccount.Id;
            supply.ServicePoint__c = servicePoint.Id;
            supply.CompanyDivision__c = companyDivision.Id;
            supplyList.add(supply);
        }
        insert supplyList;

        ProcessDateThresholds__c ProcessDateThresholdsSettings = ProcessDateThresholds__c.getOrgDefaults();
        insert ProcessDateThresholdsSettings;

    }

    static Map<String, Object> initialize() {
        return initialize(false);
    }

    static Map<String, Object> initialize(Boolean withEmptyAccount) {
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        List<Dossier__c> dossiers = [
                SELECT Id, CancellationDetails__c
                FROM Dossier__c
                LIMIT 1
        ];
        String interactionId;
        String companyDivisionId;
        String dossierId;
        if (dossiers.size() > 0 && dossiers[0].CancellationDetails__c == null) {
            Interaction__c interaction = [
                    SELECT Id
                    FROM Interaction__c
                    LIMIT 1
            ];
            interactionId = interaction.Id;
            CompanyDivision__c companyDivision = [
                    SELECT Id
                    FROM CompanyDivision__c
                    LIMIT 1
            ];
            companyDivisionId = companyDivision.Id;
            dossierId = dossiers[0].Id;

        }
        Map<String, String> inputJSON = new Map<String, String>{
                'interactionId' => interactionId,
                'companyDivisionId' => companyDivisionId,
                'dossierId' => dossierId
        };
        if (!withEmptyAccount) {
            inputJSON.put('accountId', account.Id);
        }
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        if(!withEmptyAccount) {
            System.assertEquals(false, result.get('error'));
            System.assertNotEquals(result.get('dossierId'), null);
        }
        return result;
    }

    static Map<String, Object> createCase() {
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'createCase', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        List<Case> cases = (List<Case>) result.get('caseTile');
        System.assertEquals(1, cases.size());
        Case caseRecord = cases[0];
        System.assertEquals(Date.today(), caseRecord.StartDate__c);
        System.assertEquals(Date.today(), caseRecord.EffectiveDate__c);
        System.assertEquals(MRO_UTL_Date.addWorkingDays(Date.today(), 15), caseRecord.SLAExpirationDate__c);
        return result;
    }

    @IsTest
    static Map<String, Object> initializeFromGenericRequest() {
        Dossier__c genericDossier = new Dossier__c(RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(MRO_UTL_Constants.GENERIC_REQUEST).getRecordTypeId(), Origin__c = 'Email', Channel__c = 'Back Office Sales');
        insert genericDossier;
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Map<String, String> inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'genericRequestId' => genericDossier.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(false, result.get('error'));
        System.assertNotEquals(result.get('dossierId'), null);
        String dossierId = (String)result.get('dossierId');

        Dossier__c dossier = [SELECT Origin__c, Channel__c FROM Dossier__c WHERE Id = :dossierId LIMIT 1];
        System.assertEquals(genericDossier.Origin__c, dossier.Origin__c);
        System.assertEquals(genericDossier.Channel__c, dossier.Channel__c);

        return result;
    }

    @IsTest
    static void initializeWithEmptyAccountTest() {
        Test.startTest();
        Map<String, Object> result = initialize(true);
        System.assertEquals(true, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void setChannelAndOriginTest() {
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'setChannelAndOrigin', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void handleBillingProfileSelectionTest() {
        Map<String, Object> createCaseResult = createCase();

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'handleBillingProfileSelection', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void handleBillingProfileSelectionErrorTest() {
        createCase();
        Map<String, Object> requestParams = getRequestParams();
        requestParams.put('caseId', '');
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'handleBillingProfileSelection', requestParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void cancelBeforeSupplySelectionProcessTest() {
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'cancelProcess', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void cancelProcessTest() {
        createCase();
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'cancelProcess', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void cancelProcessErrorTest() {
        createCase();
        Map<String, Object> requestParams = getRequestParams();
        requestParams.put('dossierId', '');

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'cancelProcess', requestParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void saveProcessTest() {
        createCase();
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'saveProcess', getRequestParams(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void saveProcessErrorTest() {
        createCase();
        Map<String, Object> requestParams = getRequestParams();
        requestParams.put('dossierId', '');

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'saveProcess', requestParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void saveDraftProcessTest() {
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_RefundRequestWizard', 'saveProcess', getRequestParamsForDraft(), true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
        Test.stopTest();
    }

    static Map<String, Object> getRequestParamsForDraft() {
        return getRequestParams(true);
    }

    static Map<String, Object> getRequestParams() {
        return getRequestParams(false);
    }

    static Map<String, Object> getRequestParams(Boolean isDraft) {
        Map<String, Object> initResult = initialize();
        List<Case> caseTile = (List<Case>) initResult.get('caseTile');
        String caseId = caseTile != null && caseTile.size() > 0 ? caseTile[0].Id : null;

        Supply__c supply = [
                SELECT Id,Name
                FROM Supply__c
                LIMIT 1
        ];

        BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
                LIMIT 1
        ];


        Map<String, Object> inputJSON = new Map<String, Object>{
                'caseId' => caseId,
                'supplyId' => supply.Id,
                'dossierId' => (String) initResult.get('dossierId'),
                'billingProfileId' => billingProfile.Id,
                'originSelected' => 'Email',
                'channelSelected' => 'Back Office Sales',
                'salesman' => null,
                'salesChannel' => null,
                'cancelReason' => '',
                'detailsReason' => null,
                'isDraft' => isDraft
        };
        return inputJSON;
    }
}