/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 26.11.20.
 */

public with sharing class MRO_SRV_SapQueryIvrGazCallOut extends MRO_SRV_SapQueryHelper {
    private static final String requestType = 'QueryIVRGaz';

    public static MRO_SRV_SapQueryIvrGazCallOut getInstance() {
        return new MRO_SRV_SapQueryIvrGazCallOut();
    }

    public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {

        Set<String> conditionFields = new Set<String>{
                'ENELTEL', 'StartDate', 'EndDate'
        };

        return buildSimpleRequest(argsMap, conditionFields);
    }

    public MRO_LC_IvrGaz.IvrGazResponse getList(MRO_LC_IvrGaz.IvrGazRequestParam params) {
        Map<String, Object> conditions = new Map<String, Object>{
                'ENELTEL' => params.enelTel,
                'StartDate' => params.startDate,
                'EndDate' => params.endDate
        };

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestType, conditions, 'MRO_SRV_SapQueryIvrGazCallOut');

        if (calloutResponse == null || calloutResponse.responses.isEmpty() || calloutResponse.responses[0] == null) {
            return null;
        }
        System.debug(calloutResponse.responses[0].code);
        System.debug(calloutResponse.responses[0].description);

        if(calloutResponse.responses[0].code == 'NOK'){
            throw new WrtsException(calloutResponse.responses[0].description != null ? calloutResponse.responses[0].description : 'Unexpected error');
        }

        if (calloutResponse.responses[0].objects == null || calloutResponse.responses[0].objects.isEmpty()) {
            return null;
        }
        System.debug(calloutResponse.responses[0].objects);

        MRO_LC_IvrGaz.IvrGazResponse ivrGazResponse = new MRO_LC_IvrGaz.IvrGazResponse();

        for (wrts_prcgvr.MRR_1_0.WObject childWo : calloutResponse.responses[0].objects) {
            Map<String, String> childWoMap = MRO_UTL_MRRMapper.wobjectToMap(childWo);
            MRO_LC_IvrGaz.Index indexDto = new MRO_LC_IvrGaz.Index(childWoMap);
            ivrGazResponse.indexes.add(indexDto);
        }

        System.debug(ivrGazResponse);
        return ivrGazResponse;
    }
}