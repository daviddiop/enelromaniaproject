@IsTest
private class MRO_LC_InvoicesInquiryTst {

    @TestSetup
    static void setup () {

        Id businessAccountRTId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Business' AND IsActive = TRUE].Id;
        Account acc = new Account(Name = 'Business Account', Email__c = 'Business@it.com',
                RecordTypeId = businessAccountRTId);
        insert acc;

        ContractAccount__c ca = new ContractAccount__c(Account__c = acc.Id);
        insert ca;

        SAPCallouts__c newSapCallout = new SAPCallouts__c(MeterList__c = false);
        insert newSapCallout;

        insert new DocumentType__c(Name = 'Factura', DocumentTypeCode__c = 'INVOICE');

        String fileString = String.join(new List<String>{'test test'}, '\n');

        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        Blob myBlob = Blob.valueOf(fileString);
        cv.VersionData = myBlob;
        cv.Title = 'testFile';
        cv.PathOnClient = 'testFile.csv';
        insert cv;

    }

    @IsTest
    static void testListInvoice() {

        Account acc = [SELECT Id FROM Account WHERE Name = 'Business Account'];

        MRO_LC_InvoicesInquiry.InvoiceRequestParam params = new MRO_LC_InvoicesInquiry.InvoiceRequestParam(
            Date.today().format(), Date.today().addDays(1).format(), 'billingAccountNumber', acc.Id);

        Test.startTest();

        Map<String, Object> returnValueFromController = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InvoicesInquiry','listInvoice', params,true);

        Test.stopTest();

        System.assertNotEquals(null, returnValueFromController.get('invoiceList'));
        System.assertNotEquals(0, ((List<MRO_LC_InvoicesInquiry.Invoice>)returnValueFromController.get('invoiceList')).size());
    }

    @IsTest
    static void testListInvoice2() {

        Account acc = [SELECT Id FROM Account WHERE Name = 'Business Account'];
        DocumentType__c docType = [SELECT Id FROM DocumentType__c WHERE Name = 'Factura'];
        ContentVersion contentVersion = [SELECT Id FROM ContentVersion WHERE Title = 'testFile'];

        insert new FileMetadata__c(
                Title__c = '1000001',
                TemplateClass__c = 'ArchiveProcessing',
                TemplateCode__c = 'ArchiveProduction',
                TemplateVersion__c = 'v1',
                DocumentType__c = DocType.Id,
                DocumentId__c = contentVersion.Id,
                ExternalId__c = 'fileNetId0',
                Account__c = acc.Id);

        MRO_LC_InvoicesInquiry.InvoiceRequestParam params = new MRO_LC_InvoicesInquiry.InvoiceRequestParam(
                Date.today().format(), Date.today().addDays(1).format(), 'billingAccountNumber', acc.Id);

        Test.startTest();

        Map<String, Object> returnValueFromController = (Map<String, Object>)
                TestUtils.exec('MRO_LC_InvoicesInquiry','listInvoice', params,true);

        Test.stopTest();

        System.assertNotEquals(null, returnValueFromController.get('invoiceList'));
        System.assertNotEquals(0, ((List<MRO_LC_InvoicesInquiry.Invoice>)returnValueFromController.get('invoiceList')).size());
    }

    @IsTest
    static void testGetInvoiceDetails() {
        Test.startTest();

            Map<String, Object> invoiceDetailMap = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InvoicesInquiry','getInvoiceDetails',
            new Map<String, String>{'invoiceId' => 'invoiceId'},true);

        Test.stopTest();

        System.assertNotEquals(null, invoiceDetailMap);
        System.assertNotEquals(0, ((List<Object>) invoiceDetailMap.get('invoiceDetailList')).size());
    }

    @IsTest
    static void testgetInvoiceFromFilenet() {

        Account acc = [SELECT Id FROM Account WHERE Name = 'Business Account'];
        DocumentType__c docType = [SELECT Id FROM DocumentType__c WHERE Name = 'Factura'];
        ContentVersion contentVersion = [SELECT Id FROM ContentVersion WHERE Title = 'testFile'];
        FileMetadata__c newFileMetadata = new FileMetadata__c(
                Title__c = 'invoiceId',
                TemplateClass__c = 'ArchiveProcessing',
                TemplateCode__c = 'ArchiveProduction',
                TemplateVersion__c = 'v1',
                DocumentType__c = DocType.Id,
                DocumentId__c = contentVersion.Id,
                ExternalId__c = 'fileNetId0',
                Account__c = acc.Id);
        insert newFileMetadata;

        Test.startTest();

        Map<String, Object> invoicesFromFilenet = (Map<String, Object>)
            TestUtils.exec('MRO_LC_InvoicesInquiry','getInvoiceFromFilenet',
            new Map<String, String>{'invoiceId' => 'invoiceId'},true);

        Test.stopTest();

        System.assertNotEquals(null, invoicesFromFilenet);
        System.assertNotEquals(invoicesFromFilenet.get('code'), 'OK');
    }

    @IsTest
    static void testClasses() {

        Map<String, String> stringMap = new Map<String, String>{
                'MeterSerialNumber' => 'MeterSerialNumber',
                'DialId' => '',
                'NewIndex' => '',
                'OldIndex' => '',
                'Quantity' => '',
                'AverageConsumption' => ''
        };

        Map<String, String> stringBilldetailsMap = new Map<String, String>{
                'Article' => '',
                'InvoicedQuantity' => '',
                'Amount' => '',
                'VATAmout' => ''
        };

        Map<String, String> stringInvoiceMap = new Map<String, String>{
                'InvoiceId' => '',
                'InvoiceNumber' => '',
                'InvoiceSerialNumber' => '',
                'Type' => '',
                'IssuedDate' => '',
                'DueDate' => '',
                'VATAmount' => '',
                'TotalAmount' => '',
                'FileNetId' => '',
                'Balance' => '',
                'Claimed' => '',
                'Rescheduling' => ''
        };

        Test.startTest();
        MRO_LC_InvoicesInquiry.MeterDetails meterDetails = new MRO_LC_InvoicesInquiry.MeterDetails(stringMap);
        MRO_LC_InvoicesInquiry.BillDetails billDetails = new MRO_LC_InvoicesInquiry.BillDetails(stringBilldetailsMap);
        MRO_LC_InvoicesInquiry.Invoice invoice = new MRO_LC_InvoicesInquiry.Invoice(stringInvoiceMap);
        MRO_LC_InvoicesInquiry.Invoice newInvoice = new MRO_LC_InvoicesInquiry.Invoice('', '' ,'Unknown',
                '', null, null, 100, '', null, null, '',
                '', new List<MRO_LC_InvoicesInquiry.Payment>());
        newInvoice.getIsMarked();

        Test.stopTest();
    }
}