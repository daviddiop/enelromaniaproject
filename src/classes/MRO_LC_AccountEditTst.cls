/**
 * Created by napoli on 22/11/2019.
 */
@IsTest
public with sharing class MRO_LC_AccountEditTst {

    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        listAccount.add(MRO_UTL_TestDataFactory.account().businessAccount().build());
        insert listAccount;

        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert individual;

        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        interaction.Interlocutor__c = individual.Id;
        insert interaction;

        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        contact.IndividualId = individual.Id;
        insert contact;

        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id,listAccount[0].Id,contact.Id).build();
        insert customerInteraction;
    }

    @isTest
    static void getOptionListTest() {
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit','getOptionList',null,true);
        Map<String,Object> createdOptionList = (Map<String,Object>)response;
        System.assertEquals(true, createdOptionList.get('roleOptionList') != null);
        Test.stopTest();
    }

    @isTest
    static void saveBusinessAccountTest() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                lIMIT 1
        ];
        Account account = MRO_UTL_TestDataFactory.account().businessAccount().build();
        String vat = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        Account account1 = MRO_UTL_TestDataFactory.account().setVatNumber(vat).build();
        account.VATNumber__c = account1.VATNumber__c;
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo businessRt = accountRts.get('BusinessProspect');
        MRO_LC_AccountEdit.InputData inputData = new MRO_LC_AccountEdit.InputData();
        inputData.account = account;
        inputData.interactionRecord= interaction.Id;
        inputData.roles = 'Influenzatore';
        inputData.recordTypeId = businessRt.getRecordTypeId();
        String accountInput = JSON.serialize(account);
        Map<String, String > inputJSON = new Map<String, String>{
                'account' => accountInput,
                'interactionRecord' => interaction.Id,
                'roles' => 'Influenzatore',
                'recordTypeId' => businessRt.getRecordTypeId()
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit','saveAccount',inputJSON,true);
        Account createdAccount=(Account)response;
        Test.stopTest();

        System.assert(createdAccount.Id!=null);
    }

    @IsTest
    static void savePersonAccountTest() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                lIMIT 1
        ];
        Map<String,String> technicalDetailMap = new Map<String, String>();
        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        technicalDetailMap.put('test','technical detail test');
        individual.TechnicalDetails__c = JSON.serialize(technicalDetailMap);
        insert individual;

        interaction.Interlocutor__c =individual.Id;
        update interaction;
        
        String vat = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        String NationalIdentityNumber = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();

        Account account = MRO_UTL_TestDataFactory.account().personAccount()
                .setNationalIdentityNumber(NationalIdentityNumber)
                .setVatNumber(vat).build();
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');
        MRO_LC_AccountEdit.InputData inputData = new MRO_LC_AccountEdit.InputData();
        inputData.account = account;
        inputData.interactionRecord= interaction.Id;
        inputData.roles = 'Influenzatore';
        inputData.recordTypeId = personRt.getRecordTypeId();
        String accountInput = JSON.serialize(account);
        Map<String, String > inputJSON = new Map<String, String>{
                'account' => accountInput,
                'interactionRecord' => interaction.Id,
                'roles' => 'Influenzatore',
                'recordTypeId' => personRt.getRecordTypeId()
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit','saveAccount',inputJSON,true);
        Account createdAccount=(Account)response;
        Test.stopTest();
        System.assert(createdAccount.Id!=null);
    }

    @IsTest
    static void savePersonAccountWithDuplicatesTest() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                lIMIT 1
        ];
        Account myExistAccount = [
                SELECT Id, NationalIdentityNumber__pc
                FROM Account
                WHERE IsPersonAccount = true
                lIMIT 1
        ];
        System.assertNotEquals(null, myExistAccount);

        Account account = MRO_UTL_TestDataFactory.account().personAccount().setNationalIdentityNumber(myExistAccount.NationalIdentityNumber__pc).build();
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');

        MRO_LC_AccountEdit.InputData inputData = new MRO_LC_AccountEdit.InputData();
        inputData.account = account;
        inputData.interactionRecord= interaction.Id;
        inputData.roles = 'Influenzatore';
        inputData.recordTypeId = personRt.getRecordTypeId();

        Map<String, String > inputJSON = new Map<String, String>{
                'account' => JSON.serialize(account),
                'interactionRecord' => interaction.Id,
                'roles' => 'Influenzatore',
                'recordTypeId' => personRt.getRecordTypeId()
        };

        Test.startTest();
        String message = (String)TestUtils.exec('MRO_LC_AccountEdit','saveAccount',inputJSON,false);
        Test.stopTest();

        System.assert(message.contains('DUPLICATE_VALUE'));
    }

    @IsTest
    static void savePersonAccountWithNullInterlocutorTest() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                lIMIT 1
        ];

        interaction.Interlocutor__c=null;
        update interaction;

        /*Account account = MRO_UTL_TestDataFactory.account().personAccount()
                .setNationalIdentityNumber('1223188')
                .setVatNumber('125685').build();*/
        Account account = MRO_UTL_TestDataFactory.account().personAccount()
                .setNationalIdentityNumber(MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber())
                .setVatNumber(MRO_UTL_TestDataFactory.CreateFakeVatNumber()).build();
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');
        MRO_LC_AccountEdit.InputData inputData = new MRO_LC_AccountEdit.InputData();
        inputData.account = account;
        inputData.interactionRecord= interaction.Id;
        inputData.roles = 'Influenzatore';
        inputData.recordTypeId = personRt.getRecordTypeId();
        String accountInput = JSON.serialize(account);
        Map<String, String > inputJSON = new Map<String, String>{
                'account' => accountInput,
                'interactionRecord' => interaction.Id,
                'roles' => 'Influenzatore',
                'recordTypeId' => personRt.getRecordTypeId()
        };

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit','saveAccount',inputJSON,true);
        Account createdAccount=(Account)response;
        Test.stopTest();
        System.assert(createdAccount.Id!=null);
    }

    @isTest
    static void retrieveAccountTest() {
        Id accountId = [
                SELECT Id, Name,IsPersonAccount,FirstName,LastName,PersonEmail,
                        NationalIdentityNumber__pc,PersonMobilePhone,Email__c,VATNumber__c,
                        Phone,BusinessType__c,Active__c
                FROM Account
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => accountId
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit','retrieveAccount',inputJSON,true);
        Account createdAccount = (Account) response;
        System.assert(createdAccount.Id==accountId);
        Map<String, String > inputJSON1 = new Map<String, String>{
                'accountId' => ''
        };
        response = TestUtils.exec('MRO_LC_AccountEdit','retrieveAccount',inputJSON1,true);
        system.assertEquals(response,null);
        Test.stopTest();
    }

    @isTest
    static void updateAccountTest(){
        Account account = [
                SELECT Id, Name,IsPersonAccount,FirstName,LastName,
                        PersonEmail,NationalIdentityNumber__pc,PersonMobilePhone,
                        Email__c,VATNumber__c,Phone,BusinessType__c,Active__c
                FROM Account
                LIMIT 1
        ];
        Account account1 = MRO_UTL_TestDataFactory.account().setFirstName('business').build();
        account.FirstName = account1.FirstName;
        String accountInput = JSON.serialize(account);
        Map<String, String > inputJSON = new Map<String, String>{
                'account' => accountInput
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit','updateAccount',inputJSON,true);
        Account createdAccount = (Account) response;
        System.assert(createdAccount.FirstName == 'business');
        Test.stopTest();
    }

    @isTest
    static void isInterlocutorExistTest(){
        Account account = [
                SELECT Id,PersonIndividualId
                FROM Account
                LIMIT 1
        ];
        Individual individual = [
                SELECT  Id
                FROM Individual
                LIMIT 1
        ];
        account.PersonIndividualId = individual.Id;
        update account;
        Interaction__c interaction = [
                SELECT Id,InterlocutorFirstName__c
                FROM Interaction__c
                LIMIT 1
        ];
        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit', 'isInterlocutorExist', jsonInput, true);
        Map<String, String> mapOptions=(Map<String, String>)response;
        System.assert(mapOptions.get('customerInteractionId') != null);
        String interlocutorFirstNameEmpty = StringUtils.getEmptyIfNull(interaction.InterlocutorFirstName__c);
        System.assertEquals(interlocutorFirstNameEmpty, '');
        interaction.InterlocutorFirstName__c = 'TestName';
        update interaction;
        String interlocutorFirstName = StringUtils.getEmptyIfNull(interaction.InterlocutorFirstName__c);
        System.assertEquals(interlocutorFirstName, 'TestName');
        Test.stopTest();
    }

    @isTest
    static void isInterlocutorExistTest2(){
        Account myAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        insert myAccount;

        Map<String,String> technicalDetailMap = new Map<String, String>();
        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        technicalDetailMap.put('test','technical detail test');
        individual.TechnicalDetails__c = JSON.serialize(technicalDetailMap);
        insert individual;

        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        interaction.Interlocutor__c = individual.Id;
        insert interaction;

        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => myAccount.Id,
                'interactionId'=>interaction.Id
        };

        Test.startTest();
        Map<String, String> result = (Map<String, String>)TestUtils.exec('MRO_LC_AccountEdit', 'isInterlocutorExist', jsonInput, true);
        Test.stopTest();

        System.assert(result.get('customerInteractionId') != null);
    }

    @isTest
    static void isInterlocutorExistIsBlankinteractionIdTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>''
        };
        Test.startTest();
        String interactionId = null;
        Object response = TestUtils.exec('MRO_LC_AccountEdit', 'isInterlocutorExist', jsonInput, false);
        system.assertEquals(response,'Interaction - Missing Id');
        Test.stopTest();
    }
    @isTest
    static void isInterlocutorExistIsBlankcustomerIdTest(){
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => '',
                'interactionId'=>interaction.Id
        };
        Test.startTest();
        String customerId = null;
        Object response = TestUtils.exec('MRO_LC_AccountEdit', 'isInterlocutorExist', jsonInput, false);
        system.assertEquals(response,'Account - Missing Id');
        Test.stopTest();
    }

    @isTest
    static void isInterlocutorExistIsNotBlankInterlocutorTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Map<String,String> technicalDetailMap = new Map<String, String>();
        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        technicalDetailMap.put('test','technical detail test');
        individual.TechnicalDetails__c = JSON.serialize(technicalDetailMap);
        insert individual;

        interaction.Interlocutor__c =individual.Id;
        update interaction;

        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };

        Test.startTest();
        Map<String, String> mapOptions = (Map<String, String>)TestUtils.exec('MRO_LC_AccountEdit', 'isInterlocutorExist', jsonInput, true);
        System.assert(mapOptions.get('interlocutorId') != null);

        List<AccountContactRelation> accountContactRelationList = [
                SELECT Id
                FROM AccountContactRelation
        ];
        System.assert(accountContactRelationList.size() == 2);

        Contact contact = [
                SELECT Id,Name,IndividualId,IsPersonAccount
                FROM Contact
                LIMIT 1
        ];
        contact.IndividualId = null;
        update contact;

        jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };
        mapOptions = (Map<String, String>)TestUtils.exec('MRO_LC_AccountEdit', 'isInterlocutorExist', jsonInput, true);

        System.assert(mapOptions.get('interlocutorId') != null);
        Test.stopTest();
    }

    @isTest
    static void isInterlocutorExistIsEmptycustomerInteractionListTest(){
        Account account = [
                SELECT Id,Name,IsPersonAccount
                FROM Account
                WHERE IsPersonAccount = false
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Individual individual = [
                SELECT  Id
                FROM Individual
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_AccountEdit', 'isInterlocutorExist', inputJSON, true);
        Map<String, String> mapOptions=(Map<String, String>)response;
        System.assert(mapOptions.get('customerInteractionId') != null);
        List<CustomerInteraction__c> listCustomerInteractions = [
                SELECT Id
                FROM CustomerInteraction__c
        ];
        System.assert(listCustomerInteractions.size() == 2);
        Test.stopTest();
    }

  /*  @isTest
    static void searchAccountTest() {
        Account account = [
                SELECT Id,Email__c,Name,Phone,VATNumber__c,BusinessType__c
                FROM Account where Email__c='Business@it.com'
                LIMIT 1
        ];
        Object response=TestUtils.exec('MRO_LC_AccountEdit','searchAccount',account,true);
        Test.startTest();
        List<Account> accountList=(List<Account>)response;
        System.assert(accountList.size()== 1);
        Test.stopTest();
    }*/

 /*   @isTest
    static void searchAccountTestNoRow() {
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Object response=TestUtils.exec('MRO_LC_AccountEdit','searchAccount',account,true);
        Test.startTest();
        List<Account> accountList=(List<Account>)response;
        System.assert(accountList.size()== 0);
        Test.stopTest();
    }*/

    @isTest
    private static void getByIdsTest(){
        Account myAccount = [SELECT Id FROM Account LIMIT 1];
        Set<Id> accountSetIds = new Set<Id>();
        accountSetIds.add(myAccount.Id);
        AccountQueries myAQ = AccountQueries.getInstance();
        Test.startTest();
        Map<Id, Account> response =  myAQ.getByIds(accountSetIds);
        Test.stopTest();
        System.assertEquals(true ,  response.size() > 0 );
    }

    @isTest
    private static void listByIdsTest(){
        Account myAccount = [SELECT Id FROM Account LIMIT 1];
        List<Id> accountListIds = new List<Id>();
        accountListIds.add(myAccount.Id);
        AccountQueries myAQ = AccountQueries.getInstance();
        Test.startTest();
        List<Account> response =  myAQ.listByIds(accountListIds);
        Test.stopTest();
        System.assertEquals(true ,  response.size() > 0 );
    }
}