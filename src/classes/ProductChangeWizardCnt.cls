/**
 * Created by ferhati on 08/10/2019.
 */

public with sharing class ProductChangeWizardCnt extends ApexServiceLibraryCnt{

    private static OpportunityQueries opportunityQuery = OpportunityQueries.getInstance();
    private static OpportunityLineItemQueries oliQuery = OpportunityLineItemQueries.getInstance();
    private static OpportunityServiceItemQueries osiQuery = OpportunityServiceItemQueries.getInstance();
    private static SupplyQueries supplyQuery = SupplyQueries.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static final UserQueries userQuery = UserQueries.getInstance();
    private static OpportunityService oppService = OpportunityService.getInstance();
    private static Constants constantsSrv = new Constants();
    private static DatabaseService databaseSrv = new DatabaseService();
    static Constants constantSrve = Constants.getAllConstants();


    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String opportunityId = params.get('opportunityId');
            String interactionId = params.get('interactionId');
            //String companyDivisionId = params.get('companyDivisionId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Account acc = accQuery.findAccount(accountId);

            Opportunity opp;
            if (String.isBlank(opportunityId)) {
                Id cusId = null;

                List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);


                if(!cus.isEmpty()){
                    cusId = cus[0].Id;
                }
                opp = oppService.insertOpportunityByCustomerInteraction(cusId, accountId, 'ProductChange', null);
            } else {
                opp = opportunityQuery.getOpportunityById(opportunityId);
            }
            List<OpportunityLineItem> olis = oliQuery.getOLIsByOpportunityId(opp.Id);
            List<OpportunityServiceItem__c> osis = osiQuery.getOSIsByOpportunityId(opp.Id);
            response.put('opportunityId', opp.Id);
            response.put('opportunity', opp);
            response.put('opportunityLineItems', olis);
            response.put('opportunityServiceItems', osis);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('stage', opp.StageName);
            response.put('error', false);

            return response;
        }
    }

    public with sharing class updateOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String privacyChangeId = params.get('privacyChangeId');
            String stage = params.get('stage');
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                Opportunity opp = new Opportunity();
                if (String.isBlank(opportunityId)) {
                    throw new WrtsException(System.Label.Opportunity + ' - ' + System.Label.MissingId);
                }
                opp.Id = opportunityId;
                opp.StageName = stage;
                databaseSrv.upsertSObject(opp);

                if (stage == 'Closed Won') {
                    Contract myContract = new Contract();
                    OpportunityService.generateAcquisitionChain(opp.Id, 'ProductChange', myContract, privacyChangeId);
                }
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class checkOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String osiId = params.get('osiId');

            Savepoint sp = Database.setSavepoint();

            try {
                if (String.isBlank(osiId)) {
                    throw new WrtsException(System.Label.OpportunityServiceItem + ' - ' + System.Label.MissingId);
                }

                OpportunityServiceItem__c osi = osiQuery.getById(osiId);
                Opportunity opp = osi.Opportunity__r;

                ServicePoint__c point = OpportunityService.instantiateServicePoint(osi);

                Supply__c supply = OpportunityService.instantiateSupply(opp, osi);
                supply.ServicePoint__c = point.Id;
                databaseSrv.insertSObject(supply);

                Case myCase = OpportunityService.instantiateCase(opp, osi, 'ProductChange');
                databaseSrv.insertSObject(myCase);

                if (osi.ServicePoint__r.CurrentSupply__c != null) {
                    String idOldSupply = osi.ServicePoint__r.CurrentSupply__c;
                    Supply__c oldSupply = supplyQuery.getById(idOldSupply);
                    oldSupply.Status__c = constantsSrv.SUPPLY_STATUS_TERMINATING;

                    databaseSrv.updateSObject(oldSupply);
                }
                response.put('opportunityServiceItem', osi);
                Database.rollback(sp);
            }
            catch (Exception e) {
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                Database.rollback(sp);
                throw e;
            }

            return response;
        }
    }
    public with sharing class linkOliToOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();

            InputData inputData = (InputData) JSON.deserialize(jsonInput, InputData.class);
            for (OpportunityServiceItem__c osi : inputData.opportunityServiceItems) {
                osiList.add(new OpportunityServiceItem__c(Id = osi.Id, Product__c = inputData.oli.Product2Id));
            }
            databaseSrv.updateSObject(osiList);
            return true;
        }
    }

    public with sharing class InputData {
        @AuraEnabled
        public String billingProfileId { get; set; }
        @AuraEnabled
        public String contractAccountId { get; set; }
        @AuraEnabled
        public String product2Id { get; set; }
        @AuraEnabled
        public List<OpportunityServiceItem__c> opportunityServiceItems { get; set; }
        @AuraEnabled
        public OpportunityLineItem oli { get; set; }
    }


}