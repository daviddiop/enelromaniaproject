@IsTest
private class MRO_LC_ComplaintsTst {
    @TestSetup
    private static void setup() {

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Complaint').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SwitchOut_ELE', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        Account distributorAccount = MRO_UTL_TestDataFactory.account().distributorAccount().build();
        insert  distributorAccount;
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '43252435624654';
        servicePoint.Account__c = listAccount[1].Id;
        servicePoint.Trader__c = listAccount[1].Id;
        servicePoint.ENELTEL__c = '123456789';
        servicePoint.Distributor__c = distributorAccount.Id;
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supply.ServicePoint__c = servicePoint.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Complaint').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    static void GetAvailableSubTypesTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'complaintType' => 'For commercial activity',
                'supplyType' => 'Electricity'
        };
        Object response = TestUtils.exec(
                'MRO_LC_Complaints', 'GetAvailableSubTypes', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void CreateIndexTest(){
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        List<Case> caseObj = [
                SELECT Id, Supply__c
                FROM Case
                LIMIT 2
        ];
        List<MRO_LC_Complaints.MeterQuadrant> listQuadrants = new List<MRO_LC_Complaints.MeterQuadrant>();
        MRO_LC_Complaints.MeterQuadrant quadrant = new MRO_LC_Complaints.MeterQuadrant();
        quadrant.quadrantName = 'Energie Activa - Rata Unica';
        quadrant.index = 5000;
        quadrant.meterNumber='5';
        listQuadrants.add(quadrant);
        String caseObject = JSON.serialize(caseObj);
        Map<String, String> input = new Map<String, String>{
                'accountId' => acc.Id,
                'caseList' => caseObject,
                'meterInfo' => JSON.serialize(listQuadrants)
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_Complaints', 'CreateIndex', input, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();


    }

    @IsTest
    static void GetAvailableReasonsTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'caseSubType' => 'caseSubType'
        };
        Object response = TestUtils.exec(
                'MRO_LC_Complaints', 'GetAvailableReasons', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void testInit() {
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Map<String, String> input = new Map<String, String>{
                'accountId' => acc.Id,
                'dossierId' => dossier.Id
        };

        Test.startTest();

        Object response = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','init', input,true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result != null);
        Test.stopTest();

    }

    @IsTest
    static void testSaveComplaint() {
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        List<Case> caseObj = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        String caseObject = JSON.serialize(caseObj);
        Map<String, String> input = new Map<String, String>{
                'accountId' => acc.Id,
                'dossierId' => dossier.Id,
                'markedInvoiceIds' => invoiceIds,
                'caseList' => caseObject,
                'caseObject' => JSON.serialize(caseObj[0]),
                'correctionType' =>'Automatic',
                'SLAExpirationDate' => '2020-01-31'
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','SaveComplaint', input,true);

        Test.stopTest();
        System.assertEquals(true, result.get('error') == false);
    }
    @IsTest
    static void testSaveComplainManualt() {
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        List<Case> caseObj = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        String caseObject = JSON.serialize(caseObj);
        Map<String, String> input = new Map<String, String>{
                'accountId' => acc.Id,
                'dossierId' => dossier.Id,
                'markedInvoiceIds' => invoiceIds,
                'caseList' => caseObject,
                'caseObject' => JSON.serialize(caseObj[0]),
                'correctionType' =>'Manual',
                'SLAExpirationDate' => '2020-01-31'
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','SaveComplaint', input,true);

        Test.stopTest();
        System.assertEquals(true, result.get('error') == false);
    }

    @IsTest
    static void testSaveComplaintException() {
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        List<Case> caseObj = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        String invoiceIds = '';
        String caseObject = JSON.serialize(caseObj);
        Map<String, String> input = new Map<String, String>{
                'accountId' => acc.Id,
                'dossierId' => 'dossier',
                'markedInvoiceIds' => invoiceIds,
                'caseList' => caseObject,
                'caseObject' => JSON.serialize(caseObj[0]),
                'correctionType' =>'Automatic',
                'SLAExpirationDate' => '2020-01-31'
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','SaveComplaint', input,true);

        Test.stopTest();
        System.assertEquals(true, result.get('error') == true);
    }

    @IsTest
    static void testCancelProcess() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        List<Case> caseObj = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        Map<String, String> input = new Map<String, String>{
                'dossierId' => dossier.Id,
                'caseList' => JSON.serialize(caseObj)
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','CancelProcess', input,true);

        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());

        Dossier__c updatedDossier = [
                SELECT Id, Account__c, Status__c
                FROM dossier__c
                WHERE Id = :dossier.Id
        ];
        System.assertEquals(Constants.getAllConstants().DOSSIER_STATUS_CANCELED, updatedDossier.Status__c);
    }
    @IsTest
    static void testCancelProcessException() {
        List<Case> caseObj = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        Map<String, String> input = new Map<String, String>{
                'dossierId' => 'dossierId',
                'caseList' => JSON.serialize(caseObj)
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','CancelProcess', input,true);
        Test.stopTest();

        System.assertEquals(true, result !=null);

    }

    @IsTest
    static void CheckSelectedSuppliesTest(){
        Supply__c supplies = [
                SELECT Id
                FROM Supply__c
                LIMIT 1
        ];
        List<Id> listIds = new List<Id>();
        listIds.add(supplies.Id);
        Map<String, String> input = new Map<String, String>{
                'selectedSupplyIds' => JSON.serialize(listIds)
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','CheckSelectedSupplies', input,true);
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void CheckBillTest(){
        String invoiceIds = '["1000000","1000001","1000002","1000004","1000004","1000005"]';
        Map<String, String> input = new Map<String, String>{
                'markedInvoiceId' => invoiceIds
        };

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','CheckBill', input,true);
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void GetMetersTest(){
        Map<String, String> input = new Map<String, String>{};

        Test.startTest();

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','GetMeters', input,true);
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void createCaseTest(){
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        List<Case> caseObj = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        List<Supply__c> supplies = [
                SELECT Id
                FROM Supply__c
                LIMIT 2
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'accountId' => acc.Id,
                'channelSelected' => 'Back Office Sales',
                'originSelected' => 'Fax',
                'supplies' =>JSON.serialize(supplies),
                'caseList' =>JSON.serialize(caseObj)
        };

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','createCase', inputJSON,true);
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void createCaseExceptionTest(){
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Account acc = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        List<Case> caseObj = [
                SELECT Id
                FROM Case
                LIMIT 2
        ];
        List<Supply__c> supplies = [
                SELECT Id
                FROM Supply__c
                LIMIT 2
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => 'dossierId',
                'accountId' => acc.Id,
                'channelSelected' => 'Back Office Sales',
                'originSelected' => 'Fax',
                'supplies' =>JSON.serialize(supplies),
                'caseList' =>JSON.serialize(caseObj)
        };

        Map<String, Object> result = (Map<String, Object>)
                TestUtils.exec('MRO_LC_Complaints','createCase', inputJSON,true);
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }
}