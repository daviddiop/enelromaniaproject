public with sharing class ContractAccountQueries {
    public static ContractAccountQueries getInstance() {
        return new ContractAccountQueries();
    }
/*
    public ContractAccount__c getById(String contractAccountId) {
        return [
                SELECT Id, Name, BillingFrequency__c, PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c,
                       StartDate__c, EndDate__c, Key__c,
                       BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c,BillingProfile__r.PaymentMethod__c,
                       BillingProfile__r.Fax__c, BillingProfile__r.Name, BillingProfile__r.Email__c,
                       BillingProfile__r.BillingStreetType__c, BillingProfile__r.BillingStreetName__c,
                       BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c,
                       BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c
                FROM ContractAccount__c
                WHERE Id = :contractAccountId
        ];
    }

    public List<ContractAccount__c> getByAccountId(String accountId) {
        return [
                SELECT Id, Name, BillingFrequency__c, PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c,
                       StartDate__c, EndDate__c, Key__c,
                       BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c,BillingProfile__r.PaymentMethod__c,
                       BillingProfile__r.Fax__c, BillingProfile__r.Name, BillingProfile__r.Email__c,
                       BillingProfile__r.BillingStreetType__c, BillingProfile__r.BillingStreetName__c,
                       BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c,
                       BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c
                FROM ContractAccount__c
                WHERE Account__c = :accountId
                ORDER BY Name
        ];
    }
*/
    /**
     * Created by Moussa Fofana
     * 13/09/2019
     */
    public List<ContractAccount__c> getByAccountAndCompanyDivisionId(String accountId,String companyDivisionId) {
        return [
                SELECT Id, Name, BillingFrequency__c, PaymentTerms__c, BillingProfile__c, DirectDebitAuth__c,
                       StartDate__c, EndDate__c, Key__c,
                       BillingProfile__r.IBAN__c, BillingProfile__r.DeliveryChannel__c,BillingProfile__r.PaymentMethod__c,
                       BillingProfile__r.Fax__c, BillingProfile__r.Name, BillingProfile__r.Email__c,
                       BillingProfile__r.BillingStreetType__c, BillingProfile__r.BillingStreetName__c,
                       BillingProfile__r.BillingStreetNumber__c, BillingProfile__r.BillingStreetNumberExtn__c,
                       BillingProfile__r.BillingCity__c, BillingProfile__r.BillingPostalCode__c
                FROM ContractAccount__c
                WHERE Account__c = :accountId and CompanyDivision__c=:companyDivisionId
                ORDER BY Name
        ];
    }

}