/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 18, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_ExecutionLog {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public static MRO_QR_ExecutionLog getInstance() {
        return (MRO_QR_ExecutionLog) ServiceLocator.getInstance(MRO_QR_ExecutionLog.class);
    }

    public List<ExecutionLog__c> listGroupCalloutItemsByCaseGroupId(Id caseGroupId) {
        return [SELECT Id, Message__c, RequestType__c, ExternalSystem__c, Endpoint__c, Case__c
                FROM ExecutionLog__c WHERE CaseGroup__c = :caseGroupId AND Type__c = 'GroupCallOutItem'];
    }
}