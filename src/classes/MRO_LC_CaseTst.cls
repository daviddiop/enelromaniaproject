/**
 * Created by BADJI on 10/07/2020.
 */

@IsTest
public with sharing class MRO_LC_CaseTst {

    @TestSetup
    private static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeSwiEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Complaint').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SwitchOut_ELE', recordTypeSwiEle, 'Cancellation');
        insert phaseRE010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;

        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        Account distributorAccount = MRO_UTL_TestDataFactory.account().distributorAccount().build();
        insert  distributorAccount;
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '43252435624654';
        servicePoint.Account__c = listAccount[1].Id;
        servicePoint.Trader__c = listAccount[1].Id;
        servicePoint.ENELTEL__c = '123456789';
        servicePoint.Distributor__c = distributorAccount.Id;
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supply.ServicePoint__c = servicePoint.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Complaint').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    static void InitializeTechnicalDataChangeTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            LIMIT 1
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'companyDivisionId' => companyDivision.Id,
            'dossierId' => dossier.Id,
            'companyDivisionId' => interaction.Id

        };
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'InitializeTechnicalDataChange', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void UpdateCaseListTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        List<Case> caseObj = [
            SELECT Id, Supply__c
            FROM Case
            LIMIT 2
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            LIMIT 1
        ];
        caseObj[0].CompanyDivision__c = companyDivision.Id;
        update caseObj[0];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => JSON.serialize(caseObj),
            'dossierId' => dossier.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'UpdateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void CloneCaseTest(){
        Case caseObj = [
            SELECT Id, Supply__c
            FROM Case
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'caseId' => caseObj.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'CloneCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('cloneCase') != null);
        Test.stopTest();
    }

    @IsTest
    static void CloneCaseExceptionTest(){

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'caseId' => 'caseObjId'
        };
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'CloneCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    static void CancelProcessTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        List<Case> caseObj = [
            SELECT Id, Supply__c
            FROM Case
            LIMIT 2
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => JSON.serialize(caseObj),
            'dossierId' => dossier.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }
    @IsTest
    static void getCaseDescribeTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{};
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'getCaseDescribe', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void getCaseAddressFieldTest(){
        Case caseObj = [
            SELECT Id, Supply__c
            FROM Case
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'caseId' => caseObj.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'getCaseAddressField', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    static void getCaseAddressFieldExceptionTest(){

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'caseId' => 'caseObjId'
        };
        Object response = TestUtils.exec(
            'MRO_LC_Case', 'getCaseAddressField', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        Test.stopTest();
    }
}