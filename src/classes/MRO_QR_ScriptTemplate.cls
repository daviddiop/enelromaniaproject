public inherited sharing class MRO_QR_ScriptTemplate {

    private static MRO_QR_ScriptTemplate instance = null;

    public static MRO_QR_ScriptTemplate getInstance() {
        if(instance == null) instance = new MRO_QR_ScriptTemplate();
        return instance;
    }

    public List<ScriptTemplateElement__c> getByScriptTemplateId(String templateId) {
        return [
            SELECT Id, HTMLContent__c, Name, RecordTypeId,
                    RecordType.DeveloperName, Variable__c
            FROM ScriptTemplateElement__c 
            WHERE ScriptTemplate__c=:templateId 
            ORDER BY Order__c
        ];
    }

    /**
     * @author Luca Ravicini
     * @description get ScriptTemplateElement__c by ScriptTemplate__c Code__c
     * @param  templateCode the ScriptTemplate__c Code__c
     * @since Mar 16,2020
     *
    */
    public List<ScriptTemplateElement__c> getByScriptTemplateByCode(String templateCode) {
        return [
                SELECT Id, HTMLContent__c, Name, RecordTypeId,
                        RecordType.DeveloperName, Variable__c
                FROM ScriptTemplateElement__c
                WHERE ScriptTemplate__r.Code__c=:templateCode
                ORDER BY Order__c
        ];
    }

    //add by david
    public List<ScriptTemplate__c> getScriptTemplate() {
        return [
            SELECT Id,CreatedById
            FROM ScriptTemplate__c
            LIMIT 1
        ];
    }

    /**
     * @author Boubacar Sow
     * @date 23/12/2019
     * @description To get the ScriptTemplate by the Id
     *              [ENLCRO-357] Power Voltage Change Implementation Script Template
     * @param templateId
     *
     * @return ScriptTemplate__c
     */
    public ScriptTemplate__c getScriptTemplateById(String templateId) {
        return [
            SELECT Id,CreatedById
            FROM ScriptTemplate__c
            WHERE Id =:templateId
            LIMIT 1
        ];
    }

    public ScriptTemplate__c getScriptTemplateByCode(String code) {
        List<ScriptTemplate__c> templates = [
                SELECT Id,Code__c
                FROM ScriptTemplate__c
                WHERE Code__c =:code
                LIMIT 1
        ];
        return templates.isEmpty() ? null : templates[0];
    }
}