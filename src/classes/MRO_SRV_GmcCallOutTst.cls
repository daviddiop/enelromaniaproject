/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   20/12/2019
* @desc
* @history
*/
@IsTest
private class MRO_SRV_GmcCallOutTst {
     private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    @IsTest
    static void testBehavior() {
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Map<String, Object> argsMap = new Map<String, Object>();
        Case c = new Case();
        insert c;
        DocumentBundle__c bundle = new DocumentBundle__c();
        bundle.Name = 'Doc bundle test';
        insert bundle;
        
        
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        insert dossier;
        
        FileMetadata__c fileMetadata = MRO_UTL_TestDataFactory.FileMetadata().build();
        fileMetadata.Dossier__c = dossier.Id;
        insert fileMetadata;
        
        wrts_prcgvr__Activity__c checkActivty= new wrts_prcgvr__Activity__c(Type__c=constantsSrv.ACTIVITY_FILE_CHECK_TYPE,wrts_prcgvr__ObjectId__c=fileMetadata.Id,Dossier__c=fileMetadata.Dossier__c,wrts_prcgvr__Phase__c=dossier.Phase__c);
        upsert checkActivty;
        
        PrintAction__c printAction = new PrintAction__c();
        printAction.DocumentBundle__c = bundle.Id;
        printAction.PrintActivity__c = checkActivty.Id;
        insert printAction;
        
        argsMap.put('sender', printAction);
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('requestType','test');
        argsMap.put('parameters', parameters);

        MRO_SRV_SendMRRcallout.IMRRCallout IMRRCallout = (MRO_SRV_SendMRRcallout.IMRRCallout) MRO_SRV_GMCCallOut.class.newInstance();
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = IMRRCallout.buildMultiRequest(argsMap);
        String cn = [SELECT CaseNumber FROM Case WHERE Id = :c.Id].CaseNumber;
        for(wrts_prcgvr.MRR_1_0.Field f : multiRequest.requests[0].objects[0].fields){
            If(f.name == 'CaseNumber') System.assert(f.value == cn);
        }
    }
}