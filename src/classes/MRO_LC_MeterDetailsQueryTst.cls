/**
 * Created by Boubacar Sow  on 21/04/2020.
 */

@IsTest
public with sharing class MRO_LC_MeterDetailsQueryTst {

    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        ServiceSite__c serviceSite = MRO_UTL_TestDataFactory.serviceSite().createServiceSite().build();
        insert serviceSite;

        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        businessAccount.Key__c = '1324433';
        Id accBusinessRtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Business' and IsActive = TRUE LIMIT 1].Id;
        businessAccount.RecordTypeId = accBusinessRtId;

        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        accountTrader.BusinessType__c = 'Commercial areas';
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[1].Id;
        insert contract;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.ServicePoint__c = servicePoint.Id;
            supply.ServiceSite__c = serviceSite.Id;
            supplyList.add(supply);
        }
        insert supplyList;

        SAPCallouts__c sapCallouts = new SAPCallouts__c(MeterList__c =false);
        insert sapCallouts;

    }

    @IsTest
    static void testListMeterDetailsQueryIndex() {
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];

        Supply__c supply = [
            SELECT Id
            FROM Supply__c
            WHERE ServicePoint__c = :servicePoint.Id
            LIMIT 1
        ];
        MRO_LC_MeterDetailsQuery.MeterDetailsRequestParam params = new MRO_LC_MeterDetailsQuery.MeterDetailsRequestParam(
                String.valueOf(Date.today().format()),
                String.valueOf(Date.today().addDays(1).format()),
                servicePoint.Id,
                'true',
                String.valueOf(supply.Id)
        );

        Test.startTest();

        Map<String, List<Object>> meterDetailResponse = (Map<String, List<Object>>)
            TestUtils.exec('MRO_LC_MeterDetailsQuery','listMeterDetails', params,true);

        Test.stopTest();

        System.assertNotEquals(null, meterDetailResponse);
        System.assertNotEquals(0, meterDetailResponse.size());
    }

    @IsTest
    static void testListMeterDetailsIvrGas() {
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];

        Supply__c supply = [
            SELECT Id
            FROM Supply__c
            WHERE ServicePoint__c = :servicePoint.Id
            LIMIT 1
        ];
        MRO_LC_MeterDetailsQuery.MeterDetailsRequestParam params = new MRO_LC_MeterDetailsQuery.MeterDetailsRequestParam(
                String.valueOf(Date.today().format()),
                String.valueOf(Date.today().addDays(1).format()),
                servicePoint.Id,
                'false',
                String.valueOf(supply.Id)
        );

        Test.startTest();

        Map<String, List<Object>> meterDetailResponse = (Map<String, List<Object>>)
            TestUtils.exec('MRO_LC_MeterDetailsQuery','listMeterDetails', params,true);

        Test.stopTest();

        System.assertNotEquals(null, meterDetailResponse);
        System.assertNotEquals(0, meterDetailResponse.size());
    }

    @IsTest
    static void testGetDetails() {
        Test.startTest();

        List<MRO_LC_MeterDetailsQuery.Details> detailsList = (List<MRO_LC_MeterDetailsQuery.Details>)
            TestUtils.exec('MRO_LC_MeterDetailsQuery','getDetails',
                new Map<String, String>{'meterDetailsId' => 'meterDetailsId'},true);

        Test.stopTest();

        System.assertNotEquals(null, detailsList);
        System.assertNotEquals(0, detailsList.size());
    }

    @IsTest
    static void testListSupplies() {

        ServiceSite__c serviceSite = [
                SELECT Id
                FROM ServiceSite__c
                LIMIT 1
        ];

        MRO_LC_MeterDetailsQuery.SuppliesRequestParam param = new MRO_LC_MeterDetailsQuery.SuppliesRequestParam();
        param.serviceSiteId = serviceSite.Id;

        Test.startTest();

        List<Supply__c> supplyList = (List<Supply__c>)
            TestUtils.exec('MRO_LC_MeterDetailsQuery','listSupplies',param,true);

        Test.stopTest();

        System.assertNotEquals(null, supplyList);
        System.assertNotEquals(0, supplyList.size());
    }
}