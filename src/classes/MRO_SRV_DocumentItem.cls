/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 17, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_DocumentItem {
    public static MRO_SRV_DocumentItem getInstance() {
        return (MRO_SRV_DocumentItem)ServiceLocator.getInstance(MRO_SRV_DocumentItem.class);
    }

    public DocumentItemDTO mapToDto(DocumentItem__c documentItemObj){
        DocumentItemDTO docItemDTO = new DocumentItemDTO(documentItemObj);
        return docItemDTO;
    }

    public class DocumentItemDTO {
        @AuraEnabled
        public String id { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public Boolean isMandatory { get; set; }
        @AuraEnabled
        public String validatableDocumentField { get; set; }
        @AuraEnabled
        public String validatableDocumentParentField {get; set;}
        @AuraEnabled
        public String customValidationLevel {get; set;}
        @AuraEnabled
        public String customValidationMethod {get; set;}

        public DocumentItemDTO(){}
        public DocumentItemDTO(DocumentItem__c documentItemObj) {
            this.id = documentItemObj.Id;
            this.name = documentItemObj.Name;
            this.isMandatory = documentItemObj.Mandatory__c;
            this.validatableDocumentField = documentItemObj.ValidatableDocumentField__c;
            this.validatableDocumentParentField = documentItemObj.ValidatableDocParentField__c;
            this.customValidationLevel = documentItemObj.CustomValidationLevel__c;
            this.customValidationMethod = documentItemObj.CustomValidationMethod__c;
        }
    }
}