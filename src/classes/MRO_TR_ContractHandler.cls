public with sharing class MRO_TR_ContractHandler implements TriggerManager.ISObjectTriggerHandler {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public void beforeInsert() {}

    public void beforeUpdate() {}

    public void beforeDelete() {}

    public void afterInsert() {}

    public void afterUpdate() {
        MRO_SRV_Contract contractSrv = MRO_SRV_Contract.getInstance();
        contractSrv.assignRelatedCaseAndDossierToEnelX(Trigger.new, (Map<Id, Contract>)Trigger.oldMap);

        List<Contract> activatedContracts = new List<Contract>();
        for (Contract ctr : (List<Contract>) Trigger.new) {
            if (ctr.Status == CONSTANTS.CONTRACT_STATUS_ACTIVATED && Trigger.oldMap.get(ctr.Id).get('Status') != CONSTANTS.CONTRACT_STATUS_ACTIVATED) {
                activatedContracts.add(ctr);
            }
        }
        if (!activatedContracts.isEmpty()) {
            contractSrv.handleActivatedContracts(activatedContracts);
        }
    }

    public void afterDelete() {}

    public void afterUndelete() {}

}
