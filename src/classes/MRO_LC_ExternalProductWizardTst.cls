/**
 * Created by BADJI on 21/07/2020.
 */
@IsTest
public with sharing class MRO_LC_ExternalProductWizardTst {
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();

    @TestSetup
    static void setup(){
        // Insert Sequencer
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        // Insert Company Division
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        // Insert Account
        Account accont = MRO_UTL_TestDataFactory.account().personAccount().build();
        accont.NationalIdentityNumber__pc = null;
        insert accont;

        // Insert dossier
        Dossier__c dossier0 = MRO_UTL_TestDataFactory.Dossier().setRecordType(MRO_UTL_Constants.GENERIC_REQUEST).setRequestType('ExternalProduct').build();
        dossier0.Commodity__c = 'Electric';
        insert dossier0;

        // Insert contract
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = accont.Id;
        contract.ContractType__c = 'Residential';
        insert contract;

        // Insert Opportunity
        Opportunity opp = MRO_UTL_TestDataFactory.opportunity().createOpportunity().build();
        opp.AccountId = accont.Id;
        opp.ContractId = contract.Id;
        opp.RequestType__c = 'ExternalProduct';
        opp.SubProcess__c = 'Instant Delivery';
        insert opp;

        // Insert interaction
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;

        // Insert Service Point
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePointGas().build();
        servicePoint.Account__c = accont.Id;
        insert servicePoint;

        // Insert OSI
        Id osiServiceRecordTypeId = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();

        OpportunityServiceItem__c oppServiceItem = MRO_UTL_TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        oppServiceItem.Account__c = accont.Id;
        oppServiceItem.RecordTypeId = osiServiceRecordTypeId;
        insert oppServiceItem;

        NE__Product__c commercialProduct = new NE__Product__c();
        commercialProduct.InstallmentsNumbers__c = '36';
        insert commercialProduct;

        Product2 pro = MRO_UTL_TestDataFactory.product2().build();
        pro.CommercialProduct__c = commercialProduct.Id ;
        insert pro;

        PricebookEntry pricebookEntry = MRO_UTL_TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = pro.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();

        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = MRO_UTL_TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityID = opp.Id;
        opportunityLineItem.Product2ID = pro.Id;
        opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;

        ProductOption__c productOption = new ProductOption__c();
        insert productOption;

        ProductOptionRelation__c productOptionRelation = MRO_UTL_TestDataFactory.productOptionRelation().setProductId(pro.Id).build();
        productOptionRelation.ProductOption__c = productOption.Id;
        insert productOptionRelation;

        Asset asset = new Asset();
        asset.Name = 'asset name';
        asset.AccountId = accont.Id;
        asset.NE__Delivery_Date__c = Date.today() - 1;
        asset.Price = 100;
        asset.Quantity = 15;
        asset.Product2Id = pro.Id;
        insert asset;

        Id hardVasActivationRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Acquisition_Hard_VAS').getRecordTypeId();
        Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
        caseRecord.InvoiceDate__c = Date.today();
        caseRecord.Contract__c = contract.Id;
        caseRecord.AssetId = asset.Id;
        caseRecord.Dossier__c = dossier0.Id;
        caseRecord.RecordTypeId = hardVasActivationRecordTypeId;
        insert caseRecord;

        InstallmentPlan__c installmentPlan = new InstallmentPlan__c();
        installmentPlan.TotalAmount__c = 100;
        installmentPlan.NumberOfInstallments__c = 500;
        installmentPlan.StartDate__c = Date.today();
        installmentPlan.BillingFrequency__c = 'Monthly';
        installmentPlan.SelfReadingDay__c = 12;
        installmentPlan.Case__c = caseRecord.Id;
        insert installmentPlan;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        contractAccount.SelfReadingPeriodEnd__c = '31';
        insert contractAccount;

        List<Supply__c> supplyList = new List<Supply__c>();
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Account__c = accont.Id;
            supply.ServicePoint__c = servicePoint.Id;
            supply.Opportunity__c = opp.Id;
            supply.Product__c = pro.Id;
            supply.Contract__c = contract.Id;
            supply.Activator__c = caseRecord.Id;
            supply.ContractAccount__c = contractAccount.Id;
            supplyList.add(supply);
        }
        insert supplyList;
    }

    @IsTest
    static void expandInstallmentPlanTest(){
        String response = MRO_LC_ExternalProductWizard.expandInstallmentPlan(100, 500, Date.today(), 3);
        System.debug('#--# '+  response );
        System.assert(null != response);
    }
    @IsTest
    static void updateInstallmentPlanTest(){
        String caseId = [
                SELECT Id
                FROM Case
                LIMIT 1
        ].Id;
        Test.startTest();
        MRO_LC_ExternalProductWizard.updateInstallmentPlan(caseId);
        Test.stopTest();
    }
    @IsTest
    static void initializeTest(){
        Account account = [
            SELECT Id, IsPersonAccount
            FROM Account
            LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id, ContractId
            FROM Opportunity
            LIMIT 1
        ];
        Id genericRecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(MRO_UTL_Constants.GENERIC_REQUEST).getRecordTypeId();
        Dossier__c parentDossier = [
            SELECT Id, Commodity__c, StartedBy__c, Opportunity__c
            FROM Dossier__c WHERE RecordTypeId =: genericRecordTypeId
        ];
        parentDossier.Account__c = account.Id;
        parentDossier.Commodity__c = 'Electric';
        parentDossier.StartedBy__c = 'Operator Proposal';
        parentDossier.Opportunity__c = opp.Id;
        parentDossier.Status__c = 'NEW';
        update parentDossier;

        Map<String, String> inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'opportunityId'=> opp.Id,
            'genericRequestId' => parentDossier.Id
        };
        Test.startTest();
        MRO_SRV_VAS.InitializeResponseData response = (MRO_SRV_VAS.InitializeResponseData) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'initialize', inputJSON, true);
        System.assertEquals(false, response.opportunityLineItems.isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void processSelectedOliTest(){
        OpportunityLineItem oli=[
            SELECT Id, Product2Id
            FROM OpportunityLineItem
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id, Commodity__c
            FROM Dossier__c
        ];
        Opportunity opp = [
            SELECT Id, ContractId
            FROM Opportunity
            LIMIT 1
        ];
        Map<String, String> inputJSON = new Map<String, String>{
            'opportunityLineItemId' => oli.Id,
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'processSelectedOli', inputJSON, true);
        System.assertEquals(false, response.get('error'));

        inputJSON = new Map<String, String>{
            'opportunityLineItemId' => 'oliId',
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id
        };
        response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'processSelectedOli', inputJSON, true);
        System.assertEquals(true, response.get('error'));
        Test.stopTest();
    }

    @IsTest
    static void checkCommoditySupply_shouldWork(){
        Supply__c supply = [ SELECT Id FROM Supply__c LIMIT 1 ];
        Opportunity opp =[ SELECT Id, AccountId FROM Opportunity LIMIT 1 ];
        Dossier__c dossier= [ SELECT Id FROM Dossier__c LIMIT 1];
        Contract contract = new Contract(Opportunity__c = opp.Id, AccountId = opp.AccountId);
        insert contract;

        Map<String, String> inputJSON = new Map<String, String>{
            'contractId' => contract.Id,
            'currentFlow'=> 'Instant Delivery',
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id,
            'supplyId' =>supply.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'checkCommoditySupply', inputJSON, true);
        //System.assertEquals(false, response.get('error'));
        System.assertEquals(false, response.get('commodityProduct') == null);

        Supply__c newSupply = new Supply__c();
        insert newSupply;
        inputJSON = new Map<String, String>{
            'contractId' => contract.Id,
            'opportunityId'=> opp.Id,
            'currentFlow'=> 'Instant Delivery',
            'dossierId' => dossier.Id,
            'supplyId' =>supply.Id
        };
        response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'checkCommoditySupply', inputJSON, true);
        System.debug('**** '+response.get('commodityProduct'));
        System.assertEquals(false, response.get('commodityProduct')==null);

        inputJSON = new Map<String, String>{
            'contractId' => contract.Id,
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id,
            'supplyId' =>'supplyId'
        };
        response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'checkCommoditySupply', inputJSON, true);
        System.assertEquals(true, response.get('error'));

        Test.stopTest();
    }

    @IsTest
    static void setChannelAndOriginTest(){
        Opportunity opp =[ SELECT Id FROM Opportunity LIMIT 1 ];
        Dossier__c dossier= [ SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String> inputJSON = new Map<String, String>{
            'origin' => 'Email',
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id,
            'channel' => 'Back Office Sales'
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'setChannelAndOrigin', inputJSON, true);
        System.assertEquals(false, response.get('error'));
        Test.stopTest();
    }

    /*@IsTest
    static void updateCurrentFlowTest(){
        Opportunity opp =[ SELECT Id FROM Opportunity LIMIT 1 ];
        Dossier__c dossier= [ SELECT Id FROM Dossier__c LIMIT 1];
        Supply__c suppy = [ SELECT Id, Opportunity__c FROM Supply__c LIMIT 1];
        Map<String, String> inputJSON = new Map<String, String>{
            'currentFlow' => 'Instant Delivery',
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'updateCurrentFlow', inputJSON, true);
        System.assertEquals(true, response.get('error')== false);
        Test.stopTest();
    }*/


    @IsTest
    static void updateCompanyDivisionInOpportunity(){
        Opportunity opp =[ SELECT Id FROM Opportunity LIMIT 1 ];
        Dossier__c dossier= [ SELECT Id FROM Dossier__c LIMIT 1];
        CompanyDivision__c companyDivision= [ SELECT Id FROM CompanyDivision__c LIMIT 1];
        Map<String, String> inputJSON = new Map<String, String>{
            'companyDivisionId' => companyDivision.Id,
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        System.assertEquals(true, response.get('error')== false);

        inputJSON = new Map<String, String>{
            'companyDivisionId' => 'companyDivisionId',
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id
        };

        response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'updateCompanyDivisionInOpportunity', inputJSON, true);
        System.assertEquals(true, response.get('error') == true);
        Test.stopTest();
    }

    @IsTest
    static void updateContractInfo_shouldWork(){
        Contract c = [ SELECT Id, ContractTerm, Name, ContractType__c FROM Contract LIMIT 1];
        Opportunity opp = [ SELECT Id, StageName FROM Opportunity LIMIT 1];

        System.debug('contractId in test');
        Contract contract = MRO_QR_Contract.getInstance().getById(c.Id);
        System.debug(contract.Id);

        Map<String, string> listFields = new Map<String, string>{
            'ContractTerm'=>'12',
            'name' => 'test',
            'type' => 'Residential'
        };

        Map<String, String> inputJSON = new Map<String, String>{
            'contractId' => c.Id,
            'opportunityId'=> opp.Id,
            'contractFields'=>JSON.serialize(listFields)
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'updateContractInfo', inputJSON, true);
        System.assertEquals(true, response.get('error')== false);

        inputJSON = new Map<String, String>{
            'contractId' => 'cId',
            'contractFields'=>JSON.serialize(listFields)
        };
        response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'updateContractInfo', inputJSON, true);
        System.assertEquals(true, response.get('error')== true);
        Test.stopTest();
    }

    @IsTest
    static void afterNewOsiTest(){
        OpportunityServiceItem__c osi =[ SELECT Id FROM OpportunityServiceItem__c LIMIT 1 ];
        Supply__c suppy = [ SELECT Id, Opportunity__c FROM Supply__c LIMIT 1];
        Map<String, String> inputJSON = new Map<String, String>{
            'commoditySupplyId' => suppy.Id,
            'osiId'=> osi.Id,
            'dossierId' => '',
            'opportunityId'=>''

        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'afterNewOsi', inputJSON, true);
        System.assertEquals(true, response.get('error')== false);
        Test.stopTest();
    }
    @IsTest
    static void handleServiceSupplySelectionTest(){
        Contract c = [ SELECT Id, ContractTerm, Name, ContractType__c FROM Contract LIMIT 1];
        Opportunity opp =[ SELECT Id FROM Opportunity LIMIT 1 ];
        Supply__c suppy = [ SELECT Id, Contract__c FROM Supply__c LIMIT 1];

        Map<String, String> inputJSON = new Map<String, String>{
            'serviceSupplyId' => suppy.Id,
            'contractId'=> c.Id,
            'opportunityId'=>opp.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'handleServiceSupplySelection', inputJSON, true);
        System.assertEquals(true, response.get('error')== false);
        Test.stopTest();
    }

    @IsTest
    static void updateOpportunityTest(){
        Contract c = [ SELECT Id, ContractTerm, Name FROM Contract LIMIT 1];
       // c.ContractType__c = 'Business';
       // update c;
        Supply__c suppy = [ SELECT Id, Activator__c FROM Supply__c LIMIT 1];
        Opportunity opp = [ SELECT Id, StageName FROM Opportunity LIMIT 1];
        opp.ContractType__c = 'Residential';
        //opp = MRO_SRV_VAS.getVasCaseRecordTypeId(opp);
        update opp;
        Dossier__c dossier= [ SELECT Id FROM Dossier__c LIMIT 1];
        Product2 pro = [ SELECT Id FROM Product2 LIMIT 1];
        OpportunityServiceItem__c oppServiceItem =[SELECT Id FROM OpportunityServiceItem__c LIMIT 1];
        oppServiceItem.Opportunity__c = opp.Id;
        update oppServiceItem;
        Map<String, String> inputJSON = new Map<String, String>{
            'currentFlow' => 'Instant Delivery',
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id,
            'selectedCommodity'=>suppy.Id,
            'numberOfPieces' => '100',
            'numberOfInstallments'=>'30',
            'deliveryDate' => '2020-09-01',
            'productId' => pro.Id,
            'stage' => 'Closed Won',
            'selectedToTerminateSupplyId'=>suppy.Id,
            'terminationReason'=>'Wrong case'
        };
        System.debug('Date today '+Date.today());
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'updateOpportunity', inputJSON, true);
        System.assertEquals(false, response.get('error')== false);
        Test.stopTest();
    }

    @IsTest
    static void updateOpportunityTest2(){
        Contract c = [ SELECT Id, ContractTerm, Name FROM Contract LIMIT 1];
       // c.ContractType__c = 'Business';
       // update c;
        Supply__c suppy = [ SELECT Id, Activator__c FROM Supply__c LIMIT 1];
        Opportunity opp = [ SELECT Id, StageName FROM Opportunity LIMIT 1];
        opp.ContractType__c = 'Residential';
        //opp = MRO_SRV_VAS.getVasCaseRecordTypeId(opp);
        update opp;
        Dossier__c dossier= [ SELECT Id FROM Dossier__c LIMIT 1];
        Product2 pro = [ SELECT Id FROM Product2 LIMIT 1];
        OpportunityServiceItem__c oppServiceItem =[SELECT Id FROM OpportunityServiceItem__c LIMIT 1];
        oppServiceItem.Opportunity__c = opp.Id;
        update oppServiceItem;
        Map<String, String> inputJSON = new Map<String, String>{
            'currentFlow' => 'Cancellation',
            'opportunityId'=> opp.Id,
            'dossierId' => dossier.Id,
            'selectedCommodity'=>suppy.Id,
            'numberOfPieces' => '100',
            'numberOfInstallments'=>'30',
            'deliveryDate' => '2020-09-01',
            'productId' => pro.Id,
            'stage' => 'Closed Won',
            'selectedToTerminateSupplyId'=>suppy.Id,
            'terminationReason'=>'Not mention a reason'
        };
        System.debug('Date today '+Date.today());
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
            'MRO_LC_ExternalProductWizard', 'updateOpportunity', inputJSON, true);
        System.assertEquals(false, response.get('error')== false);
        Test.stopTest();
    }

    @IsTest
    static void deleteOlisTest(){
        Opportunity opp = [ SELECT Id, StageName FROM Opportunity LIMIT 1];
        Map<String, String> inputJSON = new Map<String, String>{
                'opportunityId'=> opp.Id
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
                'MRO_LC_ExternalProductWizard', 'deleteOlis', inputJSON, true);
        System.assertEquals(true, response.get('error')== false);
        Test.stopTest();
    }
    @IsTest
    static void updateCurrentFlowTest(){
        Opportunity opp = [ SELECT Id, StageName FROM Opportunity LIMIT 1];
        Dossier__c dossier= [ SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String> inputJSON = new Map<String, String>{
                'opportunityId'=> opp.Id,
                'dossierId'=> dossier.Id,
                'currentFlow'=> 'Termination of an existing Soft VAS'
        };
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>) TestUtils.exec(
                'MRO_LC_ExternalProductWizard', 'updateCurrentFlow', inputJSON, true);
        System.assertEquals(true, response != null);
        Test.stopTest();
    }

    @IsTest
    static void createInstallmentPlanTest(){
        Case caserec = [ SELECT Id FROM Case LIMIT 1];
        Supply__c supply= [ SELECT Id FROM Supply__c LIMIT 1];

        Test.startTest();
        MAP<String, Object> installmentPlanResult = MRO_LC_ExternalProductWizard.createInstallmentPlan(caserec.Id, supply.Id);

        System.assertEquals(true, installmentPlanResult != null);
        Test.stopTest();
    }

    @IsTest
    static void computeStartDate_shouldWork(){
        Test.startTest();

        Date actual = MRO_LC_ExternalProductWizard.computeStartDate(Date.today(), 12);
        Date expected = Date.newInstance(2020, 12, 13);
        System.assertEquals(expected, actual);

        actual = MRO_LC_ExternalProductWizard.computeStartDate(Date.today(), 30);
        expected = Date.newInstance(2020, 12, 1);
        System.assertEquals(expected, actual);

        actual = MRO_LC_ExternalProductWizard.computeStartDate(Date.today(), 31);
        expected = Date.newInstance(2020, 12, 1);
        System.assertEquals(expected, actual);

        actual = MRO_LC_ExternalProductWizard.computeStartDate(Date.newInstance(2021, 2, 2), 30);
        expected = Date.newInstance(2021, 3, 1);
        System.assertEquals(expected, actual);

        actual = MRO_LC_ExternalProductWizard.computeStartDate(Date.newInstance(2020, 12, 21), 31);
        expected = Date.newInstance(2021, 1, 1);
        System.assertEquals(expected, actual);
        Test.stopTest();
    }
}