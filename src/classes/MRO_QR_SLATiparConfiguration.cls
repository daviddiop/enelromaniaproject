/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   giu 08, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_SLATiparConfiguration {

    public static MRO_QR_SLATiparConfiguration getInstance() {
        return (MRO_QR_SLATiparConfiguration) ServiceLocator.getInstance(MRO_QR_SLATiparConfiguration.class);
    }

    public SLATiparConfiguration__mdt getByRecordTypeSubProcessAndReason(String recordTypeDevName, String subProcess, String reason) {
        List<SLATiparConfiguration__mdt> configurations = [SELECT CaseTypeCode__c, SLADays__c, SLACalendarDays__c FROM SLATiparConfiguration__mdt
                                                           WHERE CaseRecordTypeDevName__c = :recordTypeDevName AND SubProcess__c = :subProcess AND Reason__c = :reason
                                                           LIMIT 1];
        return configurations.isEmpty() ? null : configurations[0];
    }

    public SLATiparConfiguration__mdt getByRecordTypeAndTypeCode(String recordTypeDevName, String typeCode) {
        List<SLATiparConfiguration__mdt> configurations = [SELECT CaseTypeCode__c, SLADays__c, SLACalendarDays__c FROM SLATiparConfiguration__mdt
                                                           WHERE CaseRecordTypeDevName__c = :recordTypeDevName AND CaseTypeCode__c = :typeCode
                                                           LIMIT 1];
        return configurations.isEmpty() ? null : configurations[0];
    }
}