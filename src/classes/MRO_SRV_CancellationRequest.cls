/**
 * Created by goudiaby on 26/08/2019.
 */

public with sharing class MRO_SRV_CancellationRequest {

    private static CancellationRequestQueries cancellationRequestQuery = CancellationRequestQueries.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Dossier dossierQry = MRO_QR_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_UTL_Transitions transitionsUtl = MRO_UTL_Transitions.getInstance();
    private static MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();

    public static MRO_SRV_CancellationRequest getInstance() {
        return new MRO_SRV_CancellationRequest();
    }

    public List<ApexServiceLibraryCnt.Option> getListCancellationReasons(String objectName, String objectRecordType, String originStage) {
        List<ApexServiceLibraryCnt.Option> cancellationReasonsOptions = new List<ApexServiceLibraryCnt.Option>();
        CancellationRequest__c cancellationRequest = cancellationRequestQuery.findCancellationReasons(objectName, objectRecordType, originStage);
        if (cancellationRequest != null){
            String cancellationReasons = cancellationRequest.CancellationReasons__c;
            if (String.isNotBlank(cancellationReasons)) {
                for (String reason : cancellationReasons.split(';')) {
                    cancellationReasonsOptions.add(new ApexServiceLibraryCnt.Option(reason, reason));
                }
            }
        }
        return cancellationReasonsOptions;
    }

    /**
     * @param sObjectId
     * @param cancellationCause
     * @param cancellationDetails
     *
     * @return
     */
    public Map<String, Object> cancelSObject(Id sObjectId, String cancellationCause, String cancellationDetails) {

        Map<String, Object> response = new Map<String, Object>();
        switch on sObjectId.getSobjectType().getDescribe().name {

            when 'Case' {

                Case myCase = caseQuery.getForCancellationById(sObjectId);
                if (myCase == null || myCase.Status == 'Canceled') {

                    throw new WrtsException('MRO_SRV_CancellationRequest.cancelSObject - Request already canceled.');
                } else {

                    // Retrieve cancellation transition
                    List <wrts_prcgvr__PhaseTransition__c> transitions =
                            transitionsUtl.getTransitions(myCase, 'Cancellation', 'A');
                    if (transitions.isEmpty() || transitions.size() > 1) {

                        String template = Label.NotFound + '.{1}';
                        List<Object> parameters = new List<Object>{
                                Label.CancellationTransition, Label.ContactSalesforceAdministrator
                        };
                        throw new WrtsException('MRO_SRV_CancellationRequest.MRO_SRV_CancellationRequest - ' + String.format(template, parameters));
                    } else {

                        response = caseSrv.cancelCase(myCase, cancellationCause, cancellationDetails, transitions[0]);
                    }
                }
            }
            when 'Dossier__c' {

                Dossier__c dossier = dossierQry.getById(sObjectId);
                response = dossierSrv.cancelDossierAndCases(dossier, cancellationCause, cancellationDetails);
            }
        }
        return response;
    }

    public void cancelSObjectsByRetentionActivities(List<wrts_prcgvr__Activity__c> activities) {

        Set<Id> dossierIds = new Set<Id>();
        Set<Id> caseIds = new Set<Id>();
        for(wrts_prcgvr__Activity__c activity : activities) {

            if(activity.Type__c == constantsUtl.ACTIVITY_DOSSIER_ELE_RETENTION_TYPE ||
                    activity.Type__c == constantsUtl.ACTIVITY_DOSSIER_GAS_RETENTION_TYPE) {

                dossierIds.add(activity.Case__c);

            } else if(activity.Type__c == constantsUtl.ACTIVITY_CASE_RETENTION_TYPE) {

                caseIds.add(activity.Case__c);

            }
        }
        List<Dossier__c> dossiers = new List<Dossier__c>();
        List<Case> cases = new List<Case>();

        MRO_UTL_Transitions.PhaseTransitionDTO phaseTransitionDTO = new MRO_UTL_Transitions.PhaseTransitionDTO();
        Map<String, Object> fieldsUpdateMap = new Map<String, Object>();
        fieldsUpdateMap.put('CancellationReason__c', 'Retention accepted');
        fieldsUpdateMap.put('CancellationDetails__c', '');

        phaseTransitionDTO.fieldsUpdateMap = fieldsUpdateMap;

        if(!dossierIds.isEmpty()) {

            dossiers = dossierQry.getByIdList(dossierIds);
            List<Dossier__c> cancellableDossiers = new List<Dossier__c>();
            Map<String, wrts_prcgvr__PhaseTransition__c> dossierTransitions = new Map<String, wrts_prcgvr__PhaseTransition__c>();

            for(Dossier__c dossier : dossiers) {
                if (phaseTransitionDTO.fieldsUpdateMap != null){
                    for (String field: phaseTransitionDTO.fieldsUpdateMap.keySet()){
                        dossier.put(field, phaseTransitionDTO.fieldsUpdateMap.get(field));
                    }
                }
                Map<String, Object> transQueryMap = new Map<String, Object>{
                    constantsUtl.PHASE_QRY_MAP_KEY_OBJECT => dossier,
                    constantsUtl.PHASE_QRY_MAP_KEY_TYPE => constantsUtl.PHASE_AUTOMATIC_TRANSITION,
                    constantsUtl.PHASE_QRY_MAP_KEY_TAGS => constantsUtl.CANCELLATION_TAG
                };
                List <wrts_prcgvr__PhaseTransition__c> transitions = transitionsUtl.getTransitions(transQueryMap);
                if (!transitions.isEmpty()) {

                    cancellableDossiers.add(dossier);
                    dossierTransitions.put(dossier.Id, transitions[0]);
                }
            }
            if(!cancellableDossiers.isEmpty()) {

                dossierSrv.applyBulkTransitions(cancellableDossiers, dossierTransitions, phaseTransitionDTO);
            }

            // Retrieve all cases linked to dossier
            cases.addAll(caseQuery.getCasesByDossierIds(dossierIds));
        }
        if(!caseIds.isEmpty() || !cases.isEmpty()) {

            if(!caseIds.isEmpty()) cases.addAll(caseQuery.listByIds(caseIds));
            List<Case> cancellableCases = new List<Case>();
            Map<String, wrts_prcgvr__PhaseTransition__c> caseTransitions = new Map<String, wrts_prcgvr__PhaseTransition__c>();

            for(Case c : cases) {
                if (phaseTransitionDTO.fieldsUpdateMap != null){
                    for (String field: phaseTransitionDTO.fieldsUpdateMap.keySet()){
                        c.put(field, phaseTransitionDTO.fieldsUpdateMap.get(field));
                    }
                }
                Map<String, Object> transQueryMap = new Map<String, Object>{
                    constantsUtl.PHASE_QRY_MAP_KEY_OBJECT => c,
                    constantsUtl.PHASE_QRY_MAP_KEY_TYPE => constantsUtl.PHASE_AUTOMATIC_TRANSITION,
                    constantsUtl.PHASE_QRY_MAP_KEY_TAGS => constantsUtl.CANCELLATION_TAG
                };
                List <wrts_prcgvr__PhaseTransition__c> transitions = transitionsUtl.getTransitions(transQueryMap);
                if (!transitions.isEmpty()) {

                    cancellableCases.add(c);
                    caseTransitions.put(c.Id, transitions[0]);
                }
            }
            if(!cancellableCases.isEmpty()) {

                caseSrv.applyBulkTransitions(cancellableCases, caseTransitions, phaseTransitionDTO);
            }
        }
    }
 }