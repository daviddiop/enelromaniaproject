/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 23, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_TR_CampaignHandler implements TriggerManager.ISObjectTriggerHandler {
    private static MRO_SRV_CampaignMemberStatus campaignMemberStatusSrv = MRO_SRV_CampaignMemberStatus.getInstance();

    public void beforeInsert() {
    }

    public void beforeUpdate() {
    }

    public void beforeDelete() {
    }

    public void afterInsert() {
        campaignMemberStatusSrv.configureCampaignMemberStatuses(Trigger.new);
    }

    public void afterUpdate() {
    }

    public void afterDelete() {
    }

    public void afterUndelete() {
    }
}