/**
 * Created by ferhati on 02/07/2019.
 */

public with sharing class DossierQueries {
    public static DossierQueries getInstance() {
        return new DossierQueries();
    }

    public Map<Id, Dossier__c> getByIds(Set<Id> dossierId) {
        return new Map<Id, Dossier__c>([
                SELECT Id, Opportunity__c, CompanyDivision__c
                FROM Dossier__c
                WHERE Id IN :dossierId
        ]);
    }

    /**
     * @author Moussa Fofana
     * @description get dossier by Id
     * @created at 25/07/2019
     */
    public Dossier__c getById(String dossierId){
        return [
            SELECT Id, Name,CustomerInteraction__c,CustomerInteraction__r.Interaction__r.Channel__c,CustomerInteraction__r.Interaction__r.Origin__c,CompanyDivision__c, CompanyDivision__r.Name, Status__c,Channel__c,Origin__c
            FROM Dossier__c
            WHERE Id = :dossierId
        ];
    }
}