/**
 * Created by David diop on 14.04.2020.
 */

public with sharing class MRO_LC_ConnectionRepresentant extends ApexServiceLibraryCnt {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();


    public with sharing class createContactAccountRelations extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {
                Account accountRecord = (Account) JSON.deserialize(params.get('fields'), Account.class);
                accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId();

                Individual individualRecord = new Individual();
                individualRecord.FirstName = accountRecord.FirstName;
                individualRecord.LastName = accountRecord.LastName;
                individualRecord.NationalIdentityNumber__c = accountRecord.NationalIdentityNumber__pc;
                insert  individualRecord;

                accountRecord.PersonIndividualId = individualRecord.Id;
                databaseSrv.insertSObject(accountRecord);
                Account accountRecordT = accountQuery.findAccount(accountRecord.Id);
                AccountContactRelation accountContactRelation = new AccountContactRelation(
                        AccountId = params.get('accountId'),
                        ContactId = accountRecordT.PersonContactId
                );
                databaseSrv.insertSObject(accountContactRelation);
                response.put('contactId', accountRecordT.PersonContactId);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateContact extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {
                Account accountRecord = (Account) JSON.deserialize(params.get('accountRecord'), Account.class);
                Contact contactToUpdate = (Contact) JSON.deserialize(params.get('contactRecord'), Contact.class);

                if(String.isBlank(contactToUpdate.NationalIdentityNumber__c)) {
                    throw new WrtsException(System.Label.NationalIdentityNumber + ' - ' + System.Label.Required);
                }
                Contact contactRecord = contactQuery.getById(contactToUpdate.Id);
                response.put('contactRecord', contactRecord);

                response.put('contactRecord', contactRecord);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getContactById extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {

                String contactId = params.get('contactId');
                Contact contactRecord = contactQuery.getById(contactId);
                response.put('contactRecord', contactRecord);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
}