/**
 * Created by David diop on 26.10.2020.
 */

public with sharing class MRO_SRV_MyEnelGDPRCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout{
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap){
        System.debug('MRO_SRV_MyEnelCampaignCallOut.buildMultiRequest');

        //Sobject
        SObject obj = (SObject) argsMap.get('sender');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        //Query
        Case myGDPR = [
                SELECT  //Case
                        AccountId,IntegrationKey__c,ReferenceDate__c,

                        //Account
                        Account.IntegrationKey__c,
                        Account.MarketingContactChannel__pc,
                        Account.MarketingEmail__pc,
                        Account.MarketingPhone__pc,
                        Account.MarketingMobilePhone__pc,

                        Account.PersonIndividual.HasOptedOutProcessing,
                        Account.PersonIndividual.HasOptedOutPartners__c

                From Case
                where Id = :obj.Id limit 1
        ];
        ContractAccount__c myContractAccount = [
                SELECT Id,BillingAccountNumber__c
                FROM ContractAccount__c
                WHERE Account__c =: myGDPR.AccountId AND ActiveSuppliesCount__c != 0 LIMIT 1
        ];


        //to WObject
        Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject((SObject) myGDPR,null);
        Wrts_prcgvr.MRR_1_0.WObject mrrContractAccount = MRO_UTL_MRRMapper.sObjectToWObject(myContractAccount, 'ContractAccount__c');

        MRO_UTL_MRRMapper.appendChildRecords(myGDPR.Id,mrrObj,mrrContractAccount);

        //Add mrrObj to the request
        request.objects.add(mrrObj);


        //Add request to Multirequest
        multiRequest.requests.add(request);
        return multiRequest;
    }
    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response = new wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse();
        if(calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
            if(calloutResponse.responses[0].code == 'OK'){
                response.success = true;
            }else{
                response.success = false;
                //            errorType = 'Functional error';
            }
            response.message = calloutResponse.responses[0].description;
        }
        return response;
    }
}