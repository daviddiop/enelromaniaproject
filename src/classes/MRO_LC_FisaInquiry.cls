public with sharing class MRO_LC_FisaInquiry extends ApexServiceLibraryCnt {

    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();

    private static final String ACCOUNT_LEVEL = 'AccountSituation_AC';
    private static final String CNPCUI_LEVEL = 'AccountSituation_CC';
    private static final String CONTRACTCOUNT_LEVEL = 'AccountSituation_CA';

    private static final String INVOICES_ONLY = 'Invoices only';
    private static final String INVOICES_WITH_PAYMENTS = 'Invoices with payments';
    private static final String SOLD_STATUS = 'Sold Status';

    public with sharing class listFisa extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            FisaRequestParam params = (FisaRequestParam) JSON.deserialize(jsonInput, FisaRequestParam.class);

            try {
                List<Fisa> fisaList ;
                if (MRO_SRV_SapSelfReadingCallOut.checkSapEnabled()) {
                    if (String.isEmpty(params.taxNum) && String.isEmpty(params.partnerId)) {
                        if (String.isEmpty(params.accountId)) {
                            throw new WrtsException('PartnerId is empty');
                        }
                        Account acc = accountQuery.findAccount(params.accountId);
                        if (acc != null && acc.AccountNumber != null) {
                            params.partnerId = acc.AccountNumber;
                            params.taxNum = acc.RecordType.DeveloperName == 'Business' ? acc.VATNumber__c.replaceAll('\\D','') : acc.NationalIdentityNumber__pc.replaceAll('\\D', '');
                        }
                    }
                    if(params.subProcess != INVOICES_WITH_PAYMENTS){
                        params.incasatDela = null;
                        params.incasatPanala = null;
                        params.cuSold = params.subProcess == SOLD_STATUS ? '1' : '0';
                    }
                    if(String.isNotEmpty(params.codCompanie)){
                        if(params.codCompanie.length() > 4){
                            CompanyDivision__c companyDivision = MRO_QR_CompanyDivision.getInstance().getById(params.codCompanie);
                            params.codCompanie = companyDivision.Code__c;
                        }
                    }

                    if(params.processName == ACCOUNT_LEVEL){ // Customer Code + Company Division
                        params.codDePlataId = null;
                        params.taxNum = null;
                    }else if(params.processName == CNPCUI_LEVEL){ // Tax Num + Company Division
                        params.codDePlataId = null;
                        params.partnerId = null;
                    }else if(params.processName == CONTRACTCOUNT_LEVEL){ // Billing Account Number + Customer Code + Company Division
                        params.taxNum = null;
                    }else{
                        params.taxNum = null;
                    }

                    fisaList = MRO_SRV_SapQueryFisaCallOut.getInstance().getList(params);
                    if(fisaList == null){
                        response.put('fisaList', fisaList);
                        response.put('error', false);
                        return response;
                    }

                    if(params.processName == 'RepaymentSchedule'){
                        List<Fisa> filteredFisa = new List<Fisa>();
                        for(Fisa fisaRecord : fisaList){
                            if(fisaRecord.hip != 0){
                                filteredFisa.add(fisaRecord);
                            }
                        }
                        response.put('fisaList', filteredFisa);
                        response.put('error', false);
                        return response;
                    }

                    response.put('fisaList', fisaList);
                    response.put('error', false);
                    return response;
                }

                fisaList = listDummyFisa(params);
                response.put('fisaList', fisaList);
                response.put('error', false);
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                if (e instanceof NullPointerException || e instanceof ListException) {
                    response.put('errorMsg', System.Label.ServiceIsDown);
                }
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }

    public static List<Fisa> listDummyFisa(FisaRequestParam params) {
        List<Fisa> fisaList = new List<Fisa>();
        for (Integer i = 0; i < 10; i++) {
            Double debtorTurnover = 0.111224 + 21 + i;

            fisaList.add(new Fisa(
                    'a114E000005X465' ,
                    '106255313',
                    '100000' + i,
                    String.valueOf(1200 + i),
                    Date.today().addDays(i),
                    Date.today().addDays(5+i),
                    4902 + i,
                    debtorTurnover,
                    31 + i,
                    0 + i,
                    '201' + i,
                    'IN_CA' + i,
                    Date.today().addDays(i),
                    100 + i,
                    'BCR' + i,
                    Date.today().addDays(i)
            ));
        }
        return fisaList;
    }

    public class FisaRequestParam {
        @AuraEnabled
        public String codCompanie { get; set; }
        @AuraEnabled
        public String codDePlataId { get; set; }
        @AuraEnabled
        public String cuSold { get; set; }
        @AuraEnabled
        public String facturiDela { get; set; }
        @AuraEnabled
        public String facturiPanala { get; set; }
        @AuraEnabled
        public String incasatDela {get; set;}
        @AuraEnabled
        public String incasatPanala {get; set;}
        @AuraEnabled
        public String partnerId {get; set;}
        @AuraEnabled
        public String dataSold {get; set;}
        @AuraEnabled
        public String taxNum {get; set;}
        @AuraEnabled
        public String accountId {get; set;}
        @AuraEnabled
        public String subProcess {get; set;}
        @AuraEnabled
        public String processName {get; set;}

        public FisaRequestParam(String codCompanie, String codDePlataId, String cuSold, String facturiDela, String facturiPanala, String incasatDela,
                String incasatPanala, String partnerId) {
            this.codCompanie = codCompanie;
            this.codDePlataId = codDePlataId;
            this.cuSold = cuSold;
            this.facturiDela = facturiDela;
            this.facturiPanala = facturiPanala;
            this.incasatDela = incasatDela;
            this.incasatPanala = incasatPanala;
            this.partnerId = partnerId;
        }
    }

    public class Fisa {
        @AuraEnabled
        public String fisaId { get; set; }
        @AuraEnabled
        public String paymentCode { get; set; }
        @AuraEnabled
        public String invoiceNumber { get; set; }
        @AuraEnabled
        public String invoiceID { get; set; }
        @AuraEnabled
        public Date invoiceDate { get; set; }
        @AuraEnabled
        public Date dueDate { get; set; }
        @AuraEnabled
        public Double invoiceValue { get; set; }
        @AuraEnabled
        public Double debtorTurnover { get; set; }
        @AuraEnabled
        public Double creditorTurnover { get; set; }
        @AuraEnabled
        public Double hip { get; set; }
        @AuraEnabled
        public String nrCollectionDocument { get; set; }
        @AuraEnabled
        public String collectionDocument { get; set; }
        @AuraEnabled
        public Date dateOfReceiptDocument { get; set; }
        @AuraEnabled
        public Double receiptDocumentValue { get; set; }
        @AuraEnabled
        public String collectionPartner { get; set; }
        @AuraEnabled
        public Date collectionOperationDate { get; set; }
        @AuraEnabled
        public String fileNetId { get; set; }


        public Fisa(String fisaId, String paymentCode, String invoiceNumber,
                String invoiceID, Date invoiceDate, Date dueDate, Double invoiceValue,
                Double debtorTurnover, Double creditorTurnover, Double hip,
                String nrCollectionDocument, String collectionDocument, Date dateOfReceiptDocument,
                Double receiptDocumentValue, String collectionPartner, Date collectionOperationDate) {
            this.fisaId = fisaId;
            this.paymentCode = paymentCode;
            this.invoiceNumber = invoiceNumber;
            this.invoiceID = invoiceID;
            this.invoiceDate = invoiceDate;
            this.dueDate = dueDate;
            this.invoiceValue = invoiceValue;
            this.debtorTurnover = debtorTurnover;
            this.creditorTurnover = creditorTurnover;
            this.hip = hip;
            this.nrCollectionDocument = nrCollectionDocument;
            this.collectionDocument = collectionDocument;
            this.dateOfReceiptDocument = dateOfReceiptDocument;
            this.receiptDocumentValue = receiptDocumentValue;
            this.collectionPartner = collectionPartner;
            this.collectionOperationDate = collectionOperationDate;
        }

        public Fisa(Map<String, String> stringMap) {
            this.fisaId = stringMap.get('ClaimedInvoiceNumber');
            this.paymentCode = stringMap.get('BillingAccountNumber');
            this.invoiceNumber = stringMap.get('ClassificationKey');
            this.invoiceID = stringMap.get('ClaimedInvoiceNumber');

            try {
                this.invoiceDate = MRO_SRV_SapQueryFisaCallOut.parseDate(stringMap.get('DocumentDate'));
                this.dueDate = MRO_SRV_SapQueryFisaCallOut.parseDate(stringMap.get('InvoiceDueDate'));
                this.dateOfReceiptDocument = MRO_SRV_SapQueryFisaCallOut.parseDate(stringMap.get('ReceiptDocumentDate'));
                this.collectionOperationDate = MRO_SRV_SapQueryFisaCallOut.parseDate(stringMap.get('CollectionOperationDate'));
            } catch (Exception e) {
                System.debug('Invalid Date FisaDTO ' +
                        stringMap.get('DocumentDate') + ' ' +
                        stringMap.get('InvoiceDueDate') + ' ' +
                        stringMap.get('ReceiptDocumentDate') + ' ' +
                        stringMap.get('CollectionOperationDate'));
            }

            this.invoiceValue = Double.valueOf(stringMap.get('DocumentValue'));
            this.debtorTurnover = Double.valueOf(stringMap.get('DebitTurnover'));
            this.creditorTurnover = Double.valueOf(stringMap.get('CreditTurnover'));
            this.hip = Double.valueOf(stringMap.get('Balance'));
            this.nrCollectionDocument = stringMap.get('CollectionDocumentNumber');
            this.collectionDocument = stringMap.get('CollectionDocument');
            this.receiptDocumentValue = MRO_SRV_SapQueryFisaCallOut.parseDouble(stringMap.get('ReceiptDocumentValue'));
            this.collectionPartner =stringMap.get('CollectionParthner');

            if(this.invoiceNumber == null && this.nrCollectionDocument != null){
                this.fisaId = this.nrCollectionDocument;
            }
        }
    }
}