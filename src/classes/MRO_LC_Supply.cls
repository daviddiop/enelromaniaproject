/**
 * Created by BADJI on 13/11/2019.
 */

public with sharing class MRO_LC_Supply extends ApexServiceLibraryCnt {

    static UserQueries userQuery = UserQueries.getInstance();
    static User userInformation = userQuery.getCompanyDivisionId(UserInfo.getUserId());
    static MRO_SRV_Supply supplyServices = MRO_SRV_Supply.getInstance();
    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();

    public static MRO_LC_Supply getInstance() {
        return new MRO_LC_Supply();
    }


    public with sharing class getSupplyTile extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();

            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('recordId');
            if (String.isBlank(supplyId)) {
                throw new WrtsException(System.Label.Supply + ' - ' + System.Label.MissingId);
            }
            Supply__c supply = supplyQuery.getBillingProfileBySupplyId(supplyId);
            response.put('supplyData', supply);

            return response;
        }
    }
    public with sharing class getDeveloperName extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String caseId = params.get('recordId');
            if (String.isBlank(caseId)) {
                throw new WrtsException(System.Label.Case + ' - ' + System.Label.MissingId);
            }
            List<Case> listRecordType = supplyQuery.getDeveloperNameQuery(caseId);
            response.put('listRecordType', listRecordType);
            return response;
        }
    }

    public with sharing class getSuppliesByAccountId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {
                String accountId = params.get('accountId');
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                List<Supply__c> supplies = supplyQuery.getSupplies(accountId, userInformation);
                System.debug('supplies >>> ' + supplies);
                response.put('supplies', supplies);
            } catch (Exception ex) {
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getSuppliesByServicePoint extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            try {
                String servicePointCode = params.get('servicePointCode');
                String accountId = params.get('accountId');
                String status = params.get('status');
                String contractId = params.get('contractId');
                String notOwnAcc = params.get('notOwnAccount');
                String selFields = params.get('selectFields');
                String companyDivisionId = params.get('companyDivisionId');

                List<String> selectFields = (List<String>) JSON.deserialize(selFields, List<String>.class);
                Boolean notOwnAccount = ((Boolean) JSON.deserialize(notOwnAcc, Boolean.class));

                List<Supply__c> supplyFieldsSp = supplyQuery.getSuppliesByServicePoint(servicePointCode, accountId, status, contractId, selectFields, notOwnAccount, companyDivisionId);

                response.put('supplyFieldsSP', supplyFieldsSp);

            } catch (Exception ex) {
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getSuppliesByContract extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            try {
                String contractCode = params.get('contractCode');
                String contractCustomerCode = params.get('contractCustomerCode');
                String accountId = params.get('accountId');
                String status = params.get('status');
                String contractId = params.get('contractId');
                String notOwnAcc = params.get('notOwnAccount');
                String selFields = params.get('selectFields');
                String companyDivisionId = params.get('companyDivisionId');

                List<String> selectFields = (List<String>) JSON.deserialize(selFields, List<String>.class);
                Boolean notOwnAccount = ((Boolean) JSON.deserialize(notOwnAcc, Boolean.class));

                List<Supply__c> supplyFieldsCt = supplyQuery.getSuppliesByContract(contractCode, contractCustomerCode, accountId,
                        status, selectFields, contractId, notOwnAccount, companyDivisionId);

                response.put('supplyFieldsCT', supplyFieldsCt);

            } catch (Exception ex) {
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getLookupFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            try {

                String objName = params.get('objName');

                List<String> lookupFields = supplyServices.getLookupFields(objName);
                response.put('lookupFields', lookupFields);

            } catch (Exception ex) {
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class deleteCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            try {

                String recordId = params.get('recordId');
                supplyServices.deleteCase(recordId);

            } catch (Exception ex) {
                System.debug(ex.getStackTraceString());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class getSupplyDeveloperNameById extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            if (String.isBlank(supplyId)) {
                throw new WrtsException(System.Label.Supply + ' - ' + System.Label.MissingId);
            }
            Supply__c supply = supplyQuery.getById(supplyId);

            response.put('supplyRecordType', supply.RecordType.DeveloperName);
            return response;
        }
    }
}