/**
 * Created by tommasobolis on 20/11/2020.
 */

public with sharing class MRO_LC_ActivityActions {

    public static MRO_QR_Activity activityQry =  MRO_QR_Activity.getInstance();
    public static MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();

    @AuraEnabled
    public static Map<String, Object> refuseRetentionActivity(String activityId) {

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            List<wrts_prcgvr__Activity__c>  activitiesToUpdate = new List<wrts_prcgvr__Activity__c>();
            Set<Id> activityIds = new Set<Id>();
            activityIds.add(activityId);
            for(wrts_prcgvr__Activity__c activity :
                    activityQry.getServicePointRetentionActivitiesByParentIds(activityIds)) {

                if(!activity.wrts_prcgvr__IsClosed__c) {

                    activity.wrts_prcgvr__Status__c = constantsUtl.ACTIVITY_STATUS_CLOSED_LOST;
                    activity.wrts_prcgvr__IsClosed__c = true;
                    activitiesToUpdate.add(activity);
                }
            }
            activitiesToUpdate.add(new wrts_prcgvr__Activity__c(
                Id = activityId,
                wrts_prcgvr__Status__c = constantsUtl.ACTIVITY_STATUS_CLOSED_LOST,
                wrts_prcgvr__IsClosed__c = true)
            );
            if(!activitiesToUpdate.isEmpty()) {

                update activitiesToUpdate;
            }

        } catch (Exception ex) {

            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> acceptRetentionActivity(String activityId) {

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            List<wrts_prcgvr__Activity__c>  activitiesToUpdate = new List<wrts_prcgvr__Activity__c>();
            Set<Id> activityIds = new Set<Id>();
            activityIds.add(activityId);
            for(wrts_prcgvr__Activity__c activity :
                    activityQry.getServicePointRetentionActivitiesByParentIds(activityIds)) {

                if(!activity.wrts_prcgvr__IsClosed__c) {

                    activity.wrts_prcgvr__Status__c = constantsUtl.ACTIVITY_STATUS_CLOSED_WON;
                    activity.wrts_prcgvr__IsClosed__c = true;
                    activitiesToUpdate.add(activity);
                }
            }
            if(!activitiesToUpdate.isEmpty()) {

                update activitiesToUpdate;
            }

        } catch (Exception ex) {

            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> cancelActivity(Id activityId) {
        System.debug('### activityId: '+activityId);
        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {
            wrts_prcgvr__Activity__c activity = new wrts_prcgvr__Activity__c(
                Id = activityId,
                wrts_prcgvr__Status__c = constantsUtl.ACTIVITY_STATUS_CANCELLED
            );

            update activity;
        } catch (Exception ex) {

            response.put('error', true);
            response.put('errorMsg', ex.getMessage());
            response.put('errorTrace', ex.getStackTraceString());
        }
        return response;
    }
}