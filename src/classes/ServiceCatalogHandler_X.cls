global with sharing class ServiceCatalogHandler_X implements wrts_prcgvr.Interfaces_1_2.IServiceCatalogHandler {
    private MRO_QR_MostUsedLink mostUsedLinkQueries = MRO_QR_MostUsedLink.getInstance();

    public Set<Id> getQuickLinks(Object params) {
        Set<Id> linkIds = new Set<Id>();
        try {
            List<MostUsedLink__c> selectedMostUsedLinks = mostUsedLinkQueries.getMostUsedLinksByUser(UserInfo.getUserId(), 5);
            for (Integer i = 0; i < selectedMostUsedLinks.size(); i++) {
                linkIds.add(selectedMostUsedLinks[i].ServiceLink__c);
            }
        } catch (Exception ex) {
            throw new WrtsException(ex.getMessage());
        }
        return linkIds;
    }
}