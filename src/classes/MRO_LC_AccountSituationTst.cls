/* MRO_LC_AccountSituationTst
*
* @author  INSA BADJI
* @version 1.0
* @description Update Cases with
*              [ENLCRO-117] Account Situation #WP3-P1
* 12/11/2019 : INSA - Original
*/

@IsTest
public with sharing class MRO_LC_AccountSituationTst {

    @TestSetup
    static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'MeterCheckEle', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'MeterCheckEle', recordTypeEle, '');
        insert phaseDI010ToRC010;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'AccountSituationWizardTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
        supply.Contract__c = contract.Id;
        insert supply;
        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        insert contractAccount;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;
        Case cases = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().build();
        cases.Dossier__c = dossier.Id;
        insert cases;


    }

    @IsTest
    public static void initializeTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];

        Dossier__c dossier = [
                SELECT Id, Status__c
                FROM Dossier__c
                LIMIT 1
        ];

        Test.startTest();

        //With Statuc__c Dossier equals to SaveDraft
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'interactionId' => customerInteraction.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_AccountSituation', 'Initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        //With Account Id equals to null
        inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id
        };
        response = TestUtils.exec(
            'MRO_LC_AccountSituation', 'Initialize', inputJSON, false);
        Test.stopTest();
    }

    @IsTest
    public static void insertCaseTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id,CompanyDivision__c
                FROM Dossier__c
                LIMIT 1
        ];
        ContractAccount__c contractAccount = [
                SELECT Id
                FROM ContractAccount__c
                LIMIT 1
        ];
        Test.startTest();
        String selectedFisa = '["1000000","1000001","1000002","1000003"]';
        String recordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AccountSituation').getRecordTypeId();
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'recordTypeId' => recordType,
                'companyDivisionId' => dossier.CompanyDivision__c,
                'startDate' => '2019-11-05',
                'endDate' => '2019-11-22',
                'contractAccountId' => contractAccount.Id,
                'selectedFisa' => selectedFisa
        };

        Object response = TestUtils.exec(
                'MRO_LC_AccountSituation', 'InsertCase', inputJSON, true);
        System.debug('test for response *** ' + response);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false, 'Error is found');

        // EXception for InsertCase
        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'recordTypeId' => recordType,
                'companyDivisionId' => dossier.CompanyDivision__c,
                'startDate' => '20-11-05',
                'endDate' => '20-11-22',
                'contractAccountId' => contractAccount.Id,
                'selectedFisa' => selectedFisa
        };


        response = TestUtils.exec(
                'MRO_LC_AccountSituation', 'InsertCase', inputJSON, true);
        System.debug('test for response *** ' + response);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false, 'Error is found');
        Test.stopTest();
    }
    @IsTest
    private static void cancelProcessTest() {

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_AccountSituation', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }
    @IsTest
    private static void updateOriginAndChannelOnDossierTest() {

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'channelSelected' => 'Back Office Sales',
                'originSelected' => 'Internal'
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_AccountSituation', 'updateOriginAndChannelOnDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    private static void updateDossierCompanyDivisionTest() {

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'companyDivisionId' => companyDivision.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_AccountSituation', 'updateDossierCompanyDivision', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @IsTest
    private static void retrieveCompanyDivisionCodeTest() {
        CompanyDivision__c companyDivision = [
                SELECT Id
                FROM CompanyDivision__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'companyDivisionId' => companyDivision.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_AccountSituation', 'retrieveCompanyDivisionCode', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isValid') == true);
        Test.stopTest();
    }

}