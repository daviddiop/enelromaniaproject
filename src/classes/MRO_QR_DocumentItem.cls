/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 17, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_DocumentItem {
    public static MRO_QR_DocumentItem getInstance() {
        return (MRO_QR_DocumentItem) ServiceLocator.getInstance(MRO_QR_DocumentItem.class);
    }

    public List<DocumentItem__c> listDocumentItemsByDocumentTypeId(Set<String> documentTypesIds) {
        return [
                SELECT Id,Name,DocumentType__c,ValidatableDocumentField__c,ValidatableDocParentField__c,Mandatory__c,
                       CustomValidationLevel__c,CustomValidationMethod__c,
                        DocumentType__r.Name
                FROM DocumentItem__c
                WHERE DocumentType__c IN :documentTypesIds
        ];
    }
}