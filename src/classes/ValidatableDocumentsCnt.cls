public with sharing class ValidatableDocumentsCnt extends ApexServiceLibraryCnt {
    private static FileMetadataService fileMetadataSrv = FileMetadataService.getInstance();
    private static DocumentTypeService documentTypeSrv = DocumentTypeService.getInstance();

    /**
   * @description Class for get Document Items and related Document Type from Document Bundle
   */
    public class getFileMetadataAndValidatableDocuments extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String filemetadataId = params.get('filemetadataId');
            if (String.isBlank(filemetadataId)) {
                throw new WrtsException(System.Label.FileMetadata + ' - ' + System.Label.Required);
            }

            FileMetadataService.FileMetadataDTO fileMetadataDTO = fileMetadataSrv.getFileMetadataById(filemetadataId);
            if (fileMetadataDTO.dossierId == null) {
                throw new WrtsException(System.Label.DossierRequiredSelectedFileMetadata);
            }

            List<DocumentTypeService.DocumentTypeDTO> documentTypeDTOS = documentTypeSrv.retrieveDocumentTypesFromDossier(fileMetadataDTO.dossierId);
            response.put('filemetadata', fileMetadataDTO);
            response.put('documentTypes', documentTypeDTOS);

            return response;
        }
    }
}