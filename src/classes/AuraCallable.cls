public abstract class AuraCallable {

    public ApexServiceLibraryCnt.Response call(final String jsonInput) {
        System.debug(LoggingLevel.ERROR, jsonInput);
        ApexServiceLibraryCnt.Response result = new ApexServiceLibraryCnt.Response();
        try {
            result.data = perform(jsonInput);
        } catch (Exception e) {
            //                log.error('call', e.getMessage(), e.getTypeName() + '\n' + e.getStackTraceString());
            result.error = e.getMessage();
            result.errorStackTrace = e.getStackTraceString();
            System.debug(LoggingLevel.ERROR, e.getMessage());
            System.debug(LoggingLevel.ERROR, e.getStackTraceString());
        } finally {
            //                log.save();
        }
        return result;
    }

    protected abstract Object perform(final String jsonInput);
}