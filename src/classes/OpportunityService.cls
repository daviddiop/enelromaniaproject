public with sharing class OpportunityService extends ApexServiceLibraryCnt {

    private static final OpportunityQueries opportunityQuery = OpportunityQueries.getInstance();
    private static final UserQueries userQuery = UserQueries.getInstance();
    private static final AccountQueries accountQuery = AccountQueries.getInstance();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    private static final OpportunityServiceItemQueries osiQuery = OpportunityServiceItemQueries.getInstance();
    private static final ServiceSiteQueries ssQuery = ServiceSiteQueries.getInstance();
    private static String opportunityKeyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();

    static Constants constantSrv = Constants.getAllConstants();
    static String pointGasId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
    static String pointElectricId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId();
    static String supplyGasId = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
    static String supplyElectricId = Schema.SObjectType.Supply__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId();
    private static String dossierNewStatus = constantSrv.DOSSIER_STATUS_NEW;

    public static OpportunityService getInstance() {
        return (OpportunityService) ServiceLocator.getInstance(OpportunityService.class);
    }

    public class OpportunityException extends Exception {
    }
    public class getOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            response.put('error', false);
            try {
                if (String.isBlank(opportunityId) || !opportunityId.startsWith(opportunityKeyPrefix)) {
                    throw new OpportunityException('Invalid Opportunity Id');
                }
                response.put('opportunity', JSON.serialize(opportunityQuery.getById(opportunityId)));
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public Opportunity insertOpportunityByCustomerInteraction(Id cusId, String accountId, String typeOpp) {
        Account acc = accountQuery.findAccount(accountId);
        User currentUserInfos = userQuery.getCompanyDivisionId(UserInfo.getUserId());
        Opportunity opp = new Opportunity();
        opp.RequestType__c = typeOpp;
        opp.AccountId = accountId;
        opp.StageName = 'Prospecting';
        opp.Name = acc.Name + ' - ' + opp.RequestType__c + ' - ' + System.now().format('YYMMdd');
        opp.CloseDate = System.today().addDays(28);
        if (currentUserInfos != null && String.isNotBlank(currentUserInfos.CompanyDivisionId__c)) {
            opp.CompanyDivision__c = currentUserInfos.CompanyDivisionId__c;
        }
        opp.CustomerInteraction__c = cusId;

        databaseSrv.insertSObject(opp);
        return opp;
    }

    public Opportunity insertOpportunityByCustomerInteraction(Id cusId, String accountId, String typeOpp, String companyDivisionId) {
        Account acc = accountQuery.findAccount(accountId);
        User currentUserInfos = userQuery.getCompanyDivisionId(UserInfo.getUserId());
        Opportunity opp = new Opportunity();
        opp.RequestType__c = typeOpp;
        opp.AccountId = accountId;
        opp.StageName = 'Prospecting';
        opp.Name = acc.Name + ' - ' + opp.RequestType__c + ' - ' + System.now().format('YYMMdd');
        opp.CloseDate = System.today().addDays(28);

        if (String.isNotBlank(companyDivisionId)) {
            opp.CompanyDivision__c = companyDivisionId;
        }
        opp.CustomerInteraction__c = cusId;

        databaseSrv.insertSObject(opp);
        return opp;
    }

    public static void generateAcquisitionChain(String opportunityId, String typeWizard, Contract contract, String privacyChangeId) {
        Opportunity opp = opportunityQuery.getById(opportunityId);
        String contractId = contract.Id;
	    Savepoint sp = Database.setSavepoint();
	    try {
		    if (typeWizard != 'ProductChange') {
			    if (String.isEmpty(contract.Id)) {
				    contract.AccountId = opp.AccountId;
				    contract.CompanyDivision__c = opp.CompanyDivision__c;
				    contract.Status = 'Draft';
				    contract.Pricebook2Id = opp.Pricebook2Id;
				    databaseSrv.insertSObject(contract);
			    }
			    contractId = contract.Id;
			    opp.ContractId = contractId;
			    databaseSrv.updateSObject(opp);
		    }

		    Dossier__c dossier = new Dossier__c(
			    Opportunity__c = opp.Id,
			    Account__c = opp.AccountId,
			    CompanyDivision__c = opp.CompanyDivision__c,
			    CustomerInteraction__c = opp.CustomerInteraction__c,
			    Status__c = dossierNewStatus,
			    RequestType__c = typeWizard,
			    RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Acquisition').getRecordTypeId()
		    );
		    databaseSrv.insertSObject(dossier);
		    System.debug('** Dossier ' + dossier);

		    if (String.isNotBlank(privacyChangeId)) {
			    PrivacyChange__c privacyChange = new PrivacyChange__c (
				    Id = privacyChangeId,
				    Dossier__c = dossier.Id,
				    DossierType__c = typeWizard,
				    EffectiveDate__c = System.today()
			    );
			    databaseSrv.updateSObject(privacyChange);
		    }

		    Map<String, ServicePoint__c> osiToPointMap = new Map<String, ServicePoint__c>();
		    for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
			    ServicePoint__c tmp = instantiateServicePoint(osi);
			    // Only new ServicePoint will be inserted at this time, the SP that needs to be updated, will be updated in the Commit phase (when the Case will be closed with success)
			    if (tmp.Id == null) {
				    osiToPointMap.put(osi.Id, tmp);
			    }
		    }
		    if (!osiToPointMap.isEmpty()) {
			    databaseSrv.insertSObject(osiToPointMap.values());
		    }

		    Map<String, Supply__c> osiToSupplyMap = new Map<String, Supply__c>();
		    for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
			    Supply__c supply = instantiateSupply(opp, osi);
			    supply.Contract__c = String.isNotBlank(contractId) ? contractId : null;
			    osiToSupplyMap.put(osi.Id, supply);
		    }
		    databaseSrv.insertSObject(osiToSupplyMap.values());

		    Map<String, Case> osiToCaseMap = new Map<String, Case>();
		    for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
			    Case myCase = instantiateCase(opp, osi, typeWizard);
			    myCase.ContactId = opp.CustomerInteraction__r.Contact__c;
			    myCase.Contract__c = String.isNotBlank(contractId) ? contractId : null;
			    myCase.Dossier__c = dossier.Id;
			    myCase.Supply__c = osiToSupplyMap.get(osi.Id).Id;

			    osiToCaseMap.put(osi.Id, myCase);
		    }
		    databaseSrv.insertSObject(osiToCaseMap.values());

		    Map<Id, OpportunityServiceItem__c> osiList = new Map<Id, OpportunityServiceItem__c>();

		    // recupero tutti i servicesite dell'account utilizzando la chiave "logica" SiteAddressKey__c
		    Map<String, ServiceSite__c> existingSSAccount = new Map<String, ServiceSite__c>();
		    for (ServiceSite__c ss : ssQuery.getListServiceSite(opp.AccountId)) {
			    existingSSAccount.put(ss.SiteAddressKey__c, ss);
		    }

		    Map<String, ServiceSite__c> addedSSAccount = new Map<String, ServiceSite__c>();

		    for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
			    if (String.isBlank(osi.ServiceSite__c)) {
				    if (existingSSAccount.containsKey(osi.SiteAddressKey__c)) {
					    osi.ServiceSite__c = existingSSAccount.get(osi.SiteAddressKey__c).Id;
					    osiList.put(osi.Id, osi);
				    }
				    else {
					    addedSSAccount.put(osi.SiteAddressKey__c, instantiateServiceSite(osi));
				    }
			    }
		    }
		    if (!addedSSAccount.isEmpty()) {
			    databaseSrv.insertSObject(addedSSAccount.values());
			    for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
				    if (String.isBlank(osi.ServiceSite__c)) {
					    if (addedSSAccount.containsKey(osi.SiteAddressKey__c)) {
						    osi.ServiceSite__c = addedSSAccount.get(osi.SiteAddressKey__c).Id;
						    if (!osiList.containsKey(osi.Id)) {
							    osiList.put(osi.Id, osi);
						    }
					    }
				    }
			    }
		    }


		    for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
			    if (osiToCaseMap.containsKey(osi.Id)) {
				    osiToSupplyMap.get(osi.Id).Activator__c = osiToCaseMap.get(osi.Id).Id;
			    }
			    if (osiToPointMap.containsKey(osi.Id)) {
				    osi.ServicePoint__c = osiToPointMap.get(osi.Id).Id;

				    if (!osiList.containsKey(osi.Id)) {
					    osiList.put(osi.Id, osi);
				    }
			    }
			    osiToSupplyMap.get(osi.Id).ServicePoint__c = osi.ServicePoint__c;
			    osiToSupplyMap.get(osi.Id).ServiceSite__c = osi.ServiceSite__c;
		    }

		    databaseSrv.updateSObject(osiToSupplyMap.values());
		    databaseSrv.updateSObject(osiList.values());

		    if (typeWizard == 'Transfer' || typeWizard == 'ProductChange') {
			    Map<String, Id> OldSuppliesIdMap = new Map<String, Id>();
			    Set<Id> OldSuppliesId = new Set<Id>();

			    for (OpportunityServiceItem__c osi : opp.OpportunityServiceItems__r) {
				    if (osi.ServicePoint__r.CurrentSupply__c != null) {
					    OldSuppliesIdMap.put(osi.Id, osi.ServicePoint__r.CurrentSupply__c);
					    OldSuppliesId.add(osi.ServicePoint__r.CurrentSupply__c);
				    }
			    }

			    Map<Id, Supply__c> osiToOldSupplyMap = SupplyQueries.getByIdsStatic(OldSuppliesId);
			    for (String osiId : OldSuppliesIdMap.keySet()) {
				    osiToOldSupplyMap.get(OldSuppliesIdMap.get(osiId)).Terminator__c = osiToCaseMap.get(osiId).Id;
				    if (typeWizard == 'ProductChange') {
					    System.debug('entara');
					    osiToCaseMap.get(osiId).CompanyDivision__c = osiToOldSupplyMap.get(OldSuppliesIdMap.get(osiId)).CompanyDivision__c;
					    osiToSupplyMap.get(osiId).CompanyDivision__c = osiToOldSupplyMap.get(OldSuppliesIdMap.get(osiId)).CompanyDivision__c;
					    osiToCaseMap.get(osiId).Contract__c = osiToOldSupplyMap.get(OldSuppliesIdMap.get(osiId)).Contract__c;
					    osiToSupplyMap.get(osiId).Contract__c = osiToOldSupplyMap.get(OldSuppliesIdMap.get(osiId)).Contract__c;
				    }
				    osiToOldSupplyMap.get(OldSuppliesIdMap.get(osiId)).Status__c = constantSrv.SUPPLY_STATUS_TERMINATING;
			    }
			    databaseSrv.updateSObject(osiToOldSupplyMap.values());

			    if (typeWizard == 'ProductChange') {
				    databaseSrv.updateSObject(osiToCaseMap.values());
				    databaseSrv.updateSObject(osiToSupplyMap.values());
			    }

		    }
	    }
	    catch (Exception e) {
		    Database.rollback(sp);
		    System.debug('*******'+e.getMessage());
		    Database.rollback(sp);
		    throw e;
	    }
    }

    public static ServicePoint__c instantiateServicePoint(OpportunityServiceItem__c osi) {
        ServicePoint__c point = new ServicePoint__c();
        point.Name = osi.ServicePointCode__c;
        point.Id = osi.ServicePoint__c;
        point.Code__c = osi.ServicePointCode__c;
        point.RecordTypeId = osi.RecordType.DeveloperName == 'Electric' ? pointElectricId : pointGasId;
        point.Account__c = osi.Account__c ;
        point.Distributor__c = osi.Distributor__c;
        point.Trader__c = osi.Trader__c;
        point.PointAddressNormalized__c = osi.PointAddressNormalized__c;
        point.PointStreetId__c = osi.PointStreetId__c;
	    point.PointAddressKey__c = osi.PointAddressKey__c;
        point.PointStreetType__c = osi.PointStreetType__c;
        point.PointStreetName__c = osi.PointStreetName__c;
        point.PointStreetNumber__c = osi.PointStreetNumber__c;
        point.PointStreetNumberExtn__c = osi.PointStreetNumberExtn__c;
        point.PointApartment__c = osi.PointApartment__c;
        point.PointBuilding__c = osi.PointBuilding__c;
        point.PointCity__c = osi.PointCity__c;
        point.PointCountry__c = osi.PointCountry__c;
        point.PointFloor__c = osi.PointFloor__c;
        point.PointLocality__c = osi.PointLocality__c;
        point.PointPostalCode__c = osi.PointPostalCode__c;
        point.PointProvince__c = osi.PointProvince__c;
        point.Voltage__c = osi.Voltage__c;
        point.VoltageLevel__c = osi.VoltageLevel__c;
        point.PressureLevel__c = osi.PressureLevel__c;
        point.Pressure__c = osi.Pressure__c;
        point.PowerPhase__c = osi.PowerPhase__c;
        point.ConversionFactor__c = osi.ConversionFactor__c;
        point.ContractualPower__c = osi.ContractualPower__c;
        point.AvailablePower__c = osi.AvailablePower__c;
        point.EstimatedConsumption__c = osi.EstimatedConsumption__c;
        return point;
    }

    public static ServiceSite__c instantiateServiceSite(OpportunityServiceItem__c osi) {
        ServiceSite__c serviceSite = new ServiceSite__c ();
        serviceSite.Account__c = osi.Account__c;
        serviceSite.SiteAddressKey__c = osi.SiteAddressKey__c;
        serviceSite.SiteAddressNormalized__c = osi.SiteAddressNormalized__c;
        serviceSite.SiteStreetId__c = osi.SiteStreetId__c;
        serviceSite.SiteStreetType__c = osi.SiteStreetType__c;
        serviceSite.SiteStreetName__c = osi.SiteStreetName__c;
        serviceSite.SiteStreetNumber__c = osi.SiteStreetNumber__c;
        serviceSite.SiteStreetNumberExtn__c = osi.SiteStreetNumberExtn__c;
        serviceSite.SiteApartment__c = osi.SiteApartment__c;
        serviceSite.SiteBuilding__c = osi.SiteBuilding__c;
        serviceSite.SiteCity__c = osi.SiteCity__c;
        serviceSite.SiteCountry__c = osi.SiteCountry__c;
        serviceSite.SiteFloor__c = osi.SiteFloor__c;
        serviceSite.SiteLocality__c = osi.SiteLocality__c;
        serviceSite.SitePostalCode__c = osi.SitePostalCode__c;
        serviceSite.SiteProvince__c = osi.SiteProvince__c;
        return serviceSite;
    }

    public static Supply__c instantiateSupply(Opportunity opp, OpportunityServiceItem__c osi) {
        Supply__c supply = new Supply__c();
        supply.RecordTypeId = osi.RecordType.DeveloperName == 'Electric' ? supplyElectricId : supplyGasId;
        supply.Product__c = osi.Product__c;
        supply.Account__c = opp.AccountId;
        supply.ContractAccount__c = osi.ContractAccount__c;
        supply.ServicePoint__c = osi.ServicePoint__c;
        supply.Market__c = osi.Market__c;
        supply.NonDisconnectable__c = osi.NonDisconnectable__c;
        supply.Status__c = 'Activating';
        supply.CompanyDivision__c = opp != null ? opp.CompanyDivision__c : null ;
        return supply;
    }

    public static Case instantiateCase(Opportunity opp, OpportunityServiceItem__c osi, String type) {
        Map<String, String> caseRecordTypes = Constants.getCaseRecordTypes(type);
        Case myCase = new Case();
        myCase.Phase__c = constantSrv.CASE_START_PHASE;
        myCase.AccountId = opp.AccountId;
        myCase.Origin = constantSrv.CASE_DEFAULT_CASE_ORIGIN;
        myCase.CompanyDivision__c = opp != null ? opp.CompanyDivision__c : null ;
        if (type == 'Activation') {
            myCase.RecordTypeId = osi.RecordType.DeveloperName == 'Electric' ? caseRecordTypes.get('Activation_ELE') : caseRecordTypes.get('Activation_GAS');
        } else if (type == 'SwitchIn') {
            myCase.RecordTypeId = osi.RecordType.DeveloperName == 'Electric' ? caseRecordTypes.get('SwitchIn_ELE') : caseRecordTypes.get('SwitchIn_GAS');
        } else if (type == 'Transfer') {
            myCase.CompanyDivision__c = osi.ServicePoint__r.CurrentSupply__r.CompanyDivision__c;
            myCase.RecordTypeId = osi.RecordType.DeveloperName == 'Electric' ? caseRecordTypes.get('Transfer_ELE') : caseRecordTypes.get('Transfer_GAS');
        } else if (type == 'Reactivation') {
            myCase.RecordTypeId = osi.RecordType.DeveloperName == 'Electric' ? caseRecordTypes.get('Reactivation_ELE') : caseRecordTypes.get('Reactivation_GAS');
        } else if (type == 'ProductChange') {
            myCase.RecordTypeId = osi.RecordType.DeveloperName == 'Electric' ? caseRecordTypes.get('ProductChange_ELE') : caseRecordTypes.get('ProductChange_GAS');
        }
        return myCase;
    }
}