/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   set 10, 2020
 * @desc    
 * @history 
 */

global with sharing class MRO_CRT_PrintAction implements wrts_prcgvr.Interfaces_1_2.IApexCriteria {

    global Boolean evaluate(Object param) {
        Map<String, Object> paramsMap = (Map<String, Object>) param;
        PrintAction__c printAction = (PrintAction__c) paramsMap.get('record');
        String method = (String) paramsMap.get('method');
        switch on method {
            when 'canResubmitCallout' {
                return this.canResubmitCallout(printAction);
            }
            when else {
                throw new WrtsException('Unrecognized method: '+method);
            }
        }
    }

    private Boolean canResubmitCallout(PrintAction__c printAction) {
        if (FeatureManagement.checkPermission('MRO_ResubmitCallouts')) {
            Map<String, Object> params = new Map<String, Object>{
                'triggerNew' => new List<PrintAction__c>{printAction}
            };
            wrts_prcgvr.Interfaces_1_2.IPhaseManagerUtils pmUtils = (wrts_prcgvr.Interfaces_1_2.IPhaseManagerUtils) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerUtils');
            Map<SObject, wrts_prcgvr__PhaseTransition__c> calloutsMap = (Map<SObject, wrts_prcgvr__PhaseTransition__c>) pmUtils.bulkCheckCalloutForSobject(params);
            return !calloutsMap.isEmpty();
        }
        return false;
    }
}