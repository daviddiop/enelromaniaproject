public with sharing class MRO_LC_InvoicesInquiry extends ApexServiceLibraryCnt {

    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_FileMetadata fileMetadataQuery = MRO_QR_FileMetadata.getInstance();
    private static MRO_QR_ContentVersion contentVersionQuery = MRO_QR_ContentVersion.getInstance();
    private static MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();
    private static final String CLAIMED = 'Claimed';
    private static final String MARKED = 'Marked';
    private static List<String> markedStatuses = new List<String>{CLAIMED, MARKED};
    private static Map<String, String> sapStatusToStatus = new Map<String, String>{
            'R' => CLAIMED,
            'D' => MARKED,
            'I' => 'Temporary Marked'
    };

    public with sharing class listInvoice extends AuraCallable {
        public override Object perform(final String jsonInput) {
//            Map<String, Object> response = new Map<String, Object>();
            InvoiceRequestParam params = (InvoiceRequestParam) JSON.deserialize(jsonInput, InvoiceRequestParam.class);
            Map<String, Object> returnValue = new Map<String, Object>();
            returnValue.put('error', false);
            List<Invoice> invoices = new List<Invoice>();
            List<Invoice> filteredInvoices = new List<Invoice>();
            try {
                if (params.startDate == null) {
                    throw new WrtsException('Start date is empty');
                }
                if (params.endDate == null) {
                    throw new WrtsException('End date is empty');
                }

                String accountId;

                if (MRO_SRV_SapQueryBillsCallOut.checkSapEnabled()) {
                    if (params.customerCode == null && params.billingAccountNumber == null) {
                        throw new WrtsException('CustomerCode or BillingAccountNumber is empty');
                    }
                    if (params.customerCode != null) {
                        Account acc = accountQuery.findAccount(params.customerCode);
                        if (acc != null && acc.AccountNumber != null) {
                            accountId = acc.Id;
                            params.customerCode = acc.AccountNumber;
                        }

                        else if (acc == null){
                            params.customerCode = null;
                        }

                    }

                    if (params.billingAccountNumber != null && MRO_UTL_Utils.isId(params.billingAccountNumber, ContractAccount__c.getSObjectType())) {
                        ContractAccount__c contractAcc = contractAccountQuery.getById(params.billingAccountNumber);
                        if (contractAcc != null && contractAcc.IntegrationKey__c != null) {
                            params.billingAccountNumber = contractAcc.IntegrationKey__c;
                            accountId = contractAcc.Account__c;
                            params.customerCode = contractAcc.Account__r.AccountNumber;
                        } else {
                            throw new WrtsException('CustomerCode or BillingAccountNumber is empty');
                        }
                    }else{
                        params.billingAccountNumber = null;
                    }

                    invoices = MRO_SRV_SapQueryBillsCallOut.getInstance().getList(params);
                    for (Invoice invoice : invoices) {
                        Boolean hasInvoiceNumber = String.isNotBlank(invoice.invoiceNumber);
                        Boolean notSoldCreditor = invoice.invoiceType != 'Sold creditor (avans)';

                        if (params.subProcess == 'DemonstratedPayment' && hasPaymentsWithPositiveBalance(invoice)) {
                            invoice.isClaimed = invoice.claimed == 'D';
                            filteredInvoices.add(invoice);
                        }

                        if ((params.subProcess == 'InvoiceDuplicate' || params.subProcess == 'MeterCheck') && invoice.fileNetId != null && hasInvoiceNumber && invoice.invoiceSerialNumber != null && invoice.invoiceIssueDate != null && notSoldCreditor) {
                            filteredInvoices.add(invoice);
                        }

                        if (params.subProcess == 'Complaints' && hasInvoiceNumber) {
                            invoice.isClaimed = invoice.claimed.indexOf('R') != -1;
                            filteredInvoices.add(invoice);
                        }

                        if (params.subProcess == 'CustomerCompensation' && notSoldCreditor) {
                            invoice.isClaimed = invoice.claimed.indexOf('R') != -1;
                            filteredInvoices.add(invoice);
                        }
                    }
                }
                //[ENLCRO-1811 - management of FileMetadata added]
                if (Test.isRunningTest()) {

                    accountId = accountQuery.findAccount(params.customerCode).Id;
                    List<Invoice> dummyList = listDummyInvoice(params);
                    Map<String, String> existingContentVersionMap = checkExistanceFileMetadata(dummyList, accountId);
                    returnValue.put('invoiceList', dummyList);
                    returnValue.put('contentVersionMap', existingContentVersionMap);
                    return returnValue;
                }
                Map<String, String> existingContentVersionMap = new Map<String, String>();
                if (!filteredInvoices.isEmpty()) {

                    existingContentVersionMap = checkExistanceFileMetadata(filteredInvoices, accountId);
                    returnValue.put('invoiceList', filteredInvoices);
                    returnValue.put('contentVersionMap', existingContentVersionMap);
                    return returnValue;
                }

                existingContentVersionMap = checkExistanceFileMetadata(invoices, accountId);
                returnValue.put('invoiceList', invoices);
                returnValue.put('contentVersionMap', existingContentVersionMap);
                return returnValue;

            } catch (Exception e) {
                returnValue.put('error', true);
                returnValue.put('errorMsg', e.getMessage());
                if (e instanceof NullPointerException || e instanceof ListException) {
                    returnValue.put('errorMsg', System.Label.ServiceIsDown);
                }
                returnValue.put('errorTrace', e.getStackTraceString());
            }
            return returnValue;
        }
    }

    //[ENLCRO-1811 - Method that check the existance of FileMetadata ]
    private static Map<String, String> checkExistanceFileMetadata (List<Invoice> invoiceList, String accountId) {

        //{externalId: ContentDocumentId}
        Map<String, String> returnValue = new Map<String, String>();

        Map<String, Invoice> invoiceIdToInvoiceMap = new Map<String, Invoice>();
        List<String> invoiceIdList = new List<String>();
        for (Invoice inv : invoiceList) {

            invoiceIdToInvoiceMap.put(inv.invoiceId, inv);
            if (String.isNotBlank(inv.fileNetId)) {
                invoiceIdList.add(inv.invoiceId);
            }
        }

        List<FileMetadata__c> existingFileMetadataList = new List<FileMetadata__c>();
        if (!invoiceIdList.isEmpty()) {

            existingFileMetadataList = fileMetadataQuery.findByTitles(invoiceIdList);
        }

//        if (existingFileMetadataList.size() == externalIdToInvoiceMap.keySet().size()) {
//            return null;
//        }

        List<Id> contentVersionIdList = new List<Id>();
        List<String> invoiceIdFileMetadataRetrieved = new List<String>();
        for (FileMetadata__c fileMetadata : existingFileMetadataList) {

            invoiceIdFileMetadataRetrieved.add(fileMetadata.Title__c);
            contentVersionIdList.add(fileMetadata.DocumentId__c);
        }

        List<ContentVersion> existingContentVersionList = new List<ContentVersion>();
        if (!contentVersionIdList.isEmpty()) {

            existingContentVersionList = contentVersionQuery.findById(contentVersionIdList);
            Map<String, String> documentIdToExternalIdMap = new Map<String, String>();
            for (ContentVersion contentVersion : existingContentVersionList) {
                documentIdToExternalIdMap.put(contentVersion.Id, contentVersion.ContentDocumentId);
            }

            for (FileMetadata__c fileMetadata : existingFileMetadataList) {

                returnValue.put(fileMetadata.Title__c, documentIdToExternalIdMap.get(fileMetadata.DocumentId__c));
            }
        }

        MRO_QR_FileMetadata qrFileMetadata = MRO_QR_FileMetadata.getInstance();
        Id idDocType = qrFileMetadata.getDocumentTypes('INVOICE')[0].Id;

        List<FileMetadata__c> fileMetadataToInsert = new List<FileMetadata__c>();
        for (String invoiceId : invoiceIdToInvoiceMap.keySet()) {

            if (!invoiceIdFileMetadataRetrieved.contains(invoiceId)) {

                Invoice inv = invoiceIdToInvoiceMap.get(invoiceId);
                fileMetadataToInsert.add(new FileMetadata__c(
                        Title__c = inv.invoiceId,
                        TemplateClass__c = 'ArchiveProcessing',
                        TemplateCode__c = 'ArchiveProduction',
                        TemplateVersion__c = 'v1',
                        DocumentType__c = idDocType,
                        ExternalId__c = inv.fileNetId,
                        Account__c = String.isNotBlank(accountId) ? accountId : ''
//                        Dossier__c = updatedDossier.Id,
//                        Case__c = listCases[0].Id
                ));
            }
        }

        if (!fileMetadataToInsert.isEmpty()) {

            DatabaseService databaseSrv = DatabaseService.getInstance();
            databaseSrv.insertSObject(fileMetadataToInsert);
        }
        return returnValue;
    }

    //[ENLCRO-1811 - Callout to Filenet ]
    public with sharing class getInvoiceFromFilenet extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, String> params = asMap(jsonInput);
            String invoiceId = params.get('invoiceId');
            String fileMetadataId = fileMetadataQuery.findByTitle(invoiceId).Id;
            System.debug('fileMetadataId: ' + fileMetadataId);
            if (String.isBlank(fileMetadataId)) {
                throw new WrtsException(Label.InvoiceId + ' is empty');
            }
            return MRO_LC_FileRelatedListCnt.retrieveFromDMS(fileMetadataId);
        }

    }

    private static Boolean hasPaymentsWithPositiveBalance(Invoice invoice){
        Payment paymentWithZeroOrNegativeFinalBalance;
        for (Payment paymentObject : invoice.paymentList) {
            if (paymentObject.finalBalance <= 0) {
                paymentWithZeroOrNegativeFinalBalance = paymentObject;
            }
        }
        return paymentWithZeroOrNegativeFinalBalance == null && invoice.total > 0;
    }

    public with sharing class getInvoiceDetails extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String invoiceId = params.get('invoiceId');
            try {
                if (String.isBlank(invoiceId)) {
                    throw new WrtsException(Label.InvoiceId + ' is empty');
                }

                List<InvoiceDetails> invoiceDetails;
                if(MRO_SRV_SapQueryBillsCallOut.checkSapEnabled()){
                    invoiceDetails = MRO_SRV_SapQueryBillsCallOut.getInstance().getDetails(invoiceId);
                    response.put('invoiceDetailList',invoiceDetails);
                    response.put('error', false);
                    return response;
                }
                invoiceDetails = listDummyInvoiceDetails(invoiceId);
                response.put('invoiceDetailList',invoiceDetails);
                response.put('error', false);
            }catch (Exception e){
                response.put('error', true);
                response.put('errorMsg', e.getMessage());
                if (e instanceof NullPointerException || e instanceof ListException) {
                    response.put('errorMsg', System.Label.ServiceIsDown);
                }
                response.put('errorTrace', e.getStackTraceString());
            }
            return response;
        }
    }

    public static List<Invoice> listDummyInvoice(InvoiceRequestParam params) {
        List<MRO_LC_InvoicesInquiry.Invoice> invoiceList = new List<MRO_LC_InvoicesInquiry.Invoice>();

        for (Integer i = 0; i < 10; i++) {
            invoiceList.add(new Invoice(
                    '100000' + i,
                    '100000' + i,
                    Math.mod(i, 2) == 0 ? '19MI' : '20MI',
                    getDummyStatus(i),
                    'Type' + i,
                    Date.today().addDays(i),
                    Date.today().addDays(i),
                    (i + 1) * 1000,
                    'fileNetId' + i,
                    19,
                    1,
                    Math.mod(i, 2) == 0 ? 'RD' : 'NU',
                    Math.mod(i, 2) == 0 ? 'DA' : 'NU',
                    listPayments()
            ));
        }
        return invoiceList;
    }

    public static List<MRO_LC_InvoicesInquiry.Payment> listPayments() {
        List<MRO_LC_InvoicesInquiry.Payment> paymentList = new List<Payment>();
        for (Integer i = 0; i < 5; i++) {
            paymentList.add(new Payment(
                    Date.today().addDays(1),
                    100 + i,
                    'Type' + i,
                    10000 + i,
                    Date.today().addDays(10),
                    'DocNum' + i
            ));
        }
        return paymentList;
    }

    public static List<InvoiceDetails> listDummyInvoiceDetails(String invoiceId) {
        List<MRO_LC_InvoicesInquiry.InvoiceDetails> invoiceDetailsList = new List<MRO_LC_InvoicesInquiry.InvoiceDetails>();
        for (Integer i = 0; i < 10; i++) {
            invoiceDetailsList.add(new InvoiceDetails(
                    '10000' + i,
                    10000 + i,
                    20000 + i,
                    30000 + i,
                    5 + i,
                    2 + i,
                    listBillDetails(),
                    listMeterDetails()
            ));
        }
        return invoiceDetailsList;
    }

    public static List<MRO_LC_InvoicesInquiry.BillDetails> listBillDetails() {
        List<MRO_LC_InvoicesInquiry.BillDetails> billDetailList = new List<MRO_LC_InvoicesInquiry.BillDetails>();
        for (Integer i = 0; i < 5; i++) {
            billDetailList.add(new MRO_LC_InvoicesInquiry.BillDetails(
                    '10000' + i,
                    'Item' + i,
                    10 + i,
                    543 + i,
                    'vat0' + i,
                    2 + i,
                    Date.today().addDays(i),
                    Date.today().addDays(i + 5)
            ));
        }
        return billDetailList;
    }

    public static List<MRO_LC_InvoicesInquiry.MeterDetails> listMeterDetails() {
        List<MRO_LC_InvoicesInquiry.MeterDetails> meterDetailList = new List<MRO_LC_InvoicesInquiry.MeterDetails>();
        for (Integer i = 0; i < 5; i++) {
            meterDetailList.add(new MRO_LC_InvoicesInquiry.MeterDetails(
                    '10000' + i,
                    '10000' + i,
                    'dial' + i,
                    getDummyReading(i),
                    getDummyReading(i + 1),
                    100 + i,
                    12 + i,
                    'oldIndex' + i,
                    'newIndex' + i,
                    Date.today().addDays(i),
                    Date.today().addDays(i + 5)

            ));
        }
        return meterDetailList;
    }

    private static String getDummyStatus(Integer index) {
        if (Math.mod(index, 2) == 0) {
            return '';
        } else if (Math.mod(index, 3) == 0) {
            return 'D';
        } else if (Math.mod(index, 5) == 0) {
            return 'R';
        } else if (Math.mod(index, 7) == 0) {
            return 'I';
        }
        return '';
    }

    private static String getDummyReading(Integer index) {
        if (Math.mod(index, 2) == 0) {
            return '710/CI';
        } else if (Math.mod(index, 3) == 0) {
            return '820/AU';
        }
        return '900/ES';
    }

    public class CheckBillParams {
        @AuraEnabled
        public String invoiceNumber { get; set; }
        @AuraEnabled
        public String invoiceSerialNumber { get; set; }

        public CheckBillParams(String invoiceNumber, String invoiceSerialNumber){
            this.invoiceNumber = invoiceNumber;
            this.invoiceSerialNumber = invoiceSerialNumber;
        }
    }

    public class CheckBillResponse {
        @AuraEnabled
        public String code { get; set; }
        @AuraEnabled
        public String returnMessage { get; set; }
        @AuraEnabled
        public List<CheckBillDetail> details { get; set; }

        public CheckBillResponse(Map<String, String> stringMap){
            this.code = stringMap.get('Code');
            this.returnMessage = stringMap.get('ReturnMessage');
            this.details = new List<MRO_LC_InvoicesInquiry.CheckBillDetail>();
        }

        public Set<String> getCodes(){
            Set<String> codes = new Set<String>();
            for(CheckBillDetail detail : this.details){
                codes.add('COD ' + detail.code);
            }
            return codes;
        }
    }

    public class CheckBillDetail {
        @AuraEnabled
        public String code { get; set; }
        @AuraEnabled
        public String message { get; set; }

        public CheckBillDetail(Map<String, String> stringMap){
            this.code = stringMap.get('Code');
            this.message = stringMap.get('Message');
        }
    }

    public class InvoiceRequestParam {
        @AuraEnabled
        public String startDate { get; set; }
        @AuraEnabled
        public String endDate { get; set; }
        @AuraEnabled
        public String billingAccountNumber { get; set; }
        @AuraEnabled
        public String customerCode { get; set; }
        @AuraEnabled
        public String subProcess { get; set; }

        public InvoiceRequestParam(String startDate, String endDate, String billingAccountNumber, String customerCode) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.billingAccountNumber = billingAccountNumber;
            this.customerCode = customerCode;
        }
    }

    public class Invoice {
        @AuraEnabled
        public String invoiceId { get; set; }
        @AuraEnabled
        public String invoiceNumber { get; set; }
        @AuraEnabled
        public String invoiceSerialNumber { get; set; }
        @AuraEnabled
        public String sapStatus { get; set; }
        @AuraEnabled
        public String invoiceType { get; set; }
        @AuraEnabled
        public Date invoiceIssueDate { get; set; }
        @AuraEnabled
        public Date invoiceDate { get; set; }
        @AuraEnabled
        public Double total { get; set; }
        @AuraEnabled
        public String fileNetId { get; set; }
        @AuraEnabled
        public Double tva { get; set; }
        @AuraEnabled
        public Double balance { get; set; }
        @AuraEnabled
        public String claimed { get; set; }
        @AuraEnabled
        public String rescheduling { get; set; }
        @AuraEnabled
        public Boolean isClaimed {get ; set; }
        @AuraEnabled
        public Double valueWithoutVAT { get; set; }
        @AuraEnabled
        public Double penalty { get; set; }
        @AuraEnabled
        public String recoveryAction { get; set; }
        @AuraEnabled
        public List<Payment> paymentList { get; set; }

        @AuraEnabled
        public String getStatus() {
            if (String.isNotBlank(sapStatus)) {
                String status = sapStatusToStatus.get(sapStatus);
                if (status == null) {
                    status = 'Unknown';
                }
                return status;
            }
            return sapStatus;
        }

        @AuraEnabled
        public Boolean getIsMarked() {
            return markedStatuses.contains(getStatus());
        }

        private Double calculateValueWithoutVAT(Double total) {
            return (total / 1.19).setScale(2);
        }

        public Invoice(String invoiceId, String invoiceNumber, String sapStatus, String invoiceType, Date invoiceIssueDate, Date invoiceDate, Double total, String fileNetId, Double tva, Double balance, String claimed, String rescheduling, List<MRO_LC_InvoicesInquiry.Payment> paymentList) {
            this(invoiceId, invoiceNumber, null, sapStatus, invoiceType, invoiceIssueDate, invoiceDate, total, fileNetId, tva, balance, claimed, rescheduling, paymentList);
        }

        public Invoice(String invoiceId, String invoiceNumber, String invoiceSerialNumber, String sapStatus, String invoiceType, Date invoiceIssueDate, Date invoiceDate, Double total,
                String fileNetId, Double tva, Double balance, String claimed, String rescheduling,  List<MRO_LC_InvoicesInquiry.Payment> paymentList) {
            this.invoiceId = invoiceId;
            this.invoiceNumber = invoiceNumber;
            this.invoiceSerialNumber = invoiceSerialNumber;
            this.sapStatus = sapStatus;
            this.invoiceType = invoiceType;
            this.invoiceIssueDate = invoiceIssueDate;
            this.invoiceDate = invoiceDate;
            this.total = total;
            this.fileNetId = fileNetId;
            this.tva = tva;
            this.balance = balance;
            this.claimed = claimed;
            this.rescheduling = rescheduling;
            this.valueWithoutVAT = calculateValueWithoutVAT(total);
            this.paymentList = paymentList;
        }

        public Invoice(Map<String, String> stringMap) {
            this.invoiceId = stringMap.get('InvoiceId');
            this.invoiceNumber = stringMap.get('InvoiceNumber');
            this.invoiceSerialNumber = stringMap.get('InvoiceSerialNumber');
            this.invoiceType = stringMap.get('Type');
            this.invoiceIssueDate = MRO_SRV_SapQueryBillsCallOut.parseDate(stringMap.get('IssuedDate'));
            this.invoiceDate = MRO_SRV_SapQueryBillsCallOut.parseDate(stringMap.get('DueDate'));
            this.tva = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('VATAmount'));
            this.total = MRO_SRV_SapQueryBillsCallOut.parseDecimal(stringMap.get('TotalAmount'));
            this.fileNetId = stringMap.get('FileNetId');
            this.balance = MRO_SRV_SapQueryBillsCallOut.parseDecimal(stringMap.get('Balance'));
            this.claimed = stringMap.get('Claimed');
            this.rescheduling = stringMap.get('Rescheduling');
            this.valueWithoutVAT = MRO_SRV_SapQueryBillsCallOut.parseDecimal(stringMap.get('AmountWithoutVAT'));
            this.penalty = MRO_SRV_SapQueryBillsCallOut.parseDecimal(stringMap.get('Penalty'));
            this.recoveryAction = stringMap.get('RecoveryAction');
            this.paymentList = new List<MRO_LC_InvoicesInquiry.Payment>();
        }
    }

    public class Payment {
        @AuraEnabled
        public Date paymentDate { get; set; }
        @AuraEnabled
        public Double paymentValue { get; set; }
        @AuraEnabled
        public String paymentType { get; set; }
        @AuraEnabled
        public Double finalBalance { get; set; }
        @AuraEnabled
        public Date processingDate { get; set; }
        @AuraEnabled
        public String documentNumber { get; set; }

        public Payment(Date paymentDate, Double paymentValue, String paymentType, Double finalBalance, Date processingDate, String documentNumber) {
            this.paymentDate = paymentDate;
            this.paymentValue = paymentValue;
            this.paymentType = paymentType;
            this.finalBalance = finalBalance;
            this.processingDate = processingDate;
            this.documentNumber = documentNumber;
        }

        public Payment(Map<String, String> stringMap) {
            this.paymentType = stringMap.get('DocumentType');
            this.paymentValue = MRO_SRV_SapQueryBillsCallOut.parseDecimal(stringMap.get('Amount'));
            this.finalBalance = MRO_SRV_SapQueryBillsCallOut.parseDecimal(stringMap.get('FinalDueAmount'));
            this.paymentDate = MRO_SRV_SapQueryBillsCallOut.parseDate(stringMap.get('PaymentDate'));
            this.processingDate = MRO_SRV_SapQueryBillsCallOut.parseDate(stringMap.get('ProcessingDate'));
            this.documentNumber = stringMap.get('DocumentNumber');
        }
    }

    public class InvoiceDetails {
        @AuraEnabled
        public String invoiceDetailId { get; set; }
        @AuraEnabled
        public Double current { get; set; }
        @AuraEnabled
        public Double total { get; set; }
        @AuraEnabled
        public Double overdue { get; set; }
        @AuraEnabled
        public Integer penalties { get; set; }
        @AuraEnabled
        public Integer ACTRecup { get; set; }
        @AuraEnabled
        public Boolean showBillDetails { get; set; }
        @AuraEnabled
        public List<BillDetails> billDetailList { get; set; }
        @AuraEnabled
        public List<MeterDetails> meterDetailList { get; set; }

        public InvoiceDetails(String invoiceDetailId, Double current, Double total, Double overdue, Integer penalties, Integer ACTRecup,
            List<MRO_LC_InvoicesInquiry.BillDetails> billDetailList, List<MRO_LC_InvoicesInquiry.MeterDetails> meterDetailList) {
            this.invoiceDetailId = invoiceDetailId;
            this.current = current;
            this.total = total;
            this.overdue = overdue;
            this.penalties = penalties;
            this.ACTRecup = ACTRecup;
            this.billDetailList = billDetailList;
            this.meterDetailList = meterDetailList;
        }

        public InvoiceDetails(Map<String, String> stringMap) {
            this.current = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('InvoicedAmount'));
            this.total = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('TotalAmount'));
            this.overdue = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('OverdueAmount'));
            this.billDetailList = new List<BillDetails>();
            this.meterDetailList = new List<MeterDetails>();
        }
    }

    public class BillDetails {
        @AuraEnabled
        public String billDetailId { get; set; }
        @AuraEnabled
        public String item { get; set; }
        @AuraEnabled
        public Double quantity { get; set; }
        @AuraEnabled
        public Double value { get; set; }
        @AuraEnabled
        public String vat { get; set; }
        @AuraEnabled
        public Integer total { get; set; }
        @AuraEnabled
        public Date fromDate { get; set; }
        @AuraEnabled
        public Date toDate { get; set; }

        public BillDetails(String billDetailId, String item, Double quantity, Double value, String vat, Integer total, Date fromDate, Date toDate) {
            this.billDetailId = billDetailId;
            this.item = item;
            this.quantity = quantity;
            this.value = value;
            this.vat = vat;
            this.total = total;
            this.fromDate = fromDate;
            this.toDate = toDate;
        }

        public BillDetails(Map<String, String> stringMap) {
            //this.billDetailId = Double.valueOf(stringMap.get('InvoicedAmount'));
            this.item = stringMap.get('Article');
            this.quantity = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('InvoicedQuantity'));
            this.value = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('Amount'));
            this.vat = stringMap.get('VATAmout');
            this.fromDate = MRO_SRV_SapQueryBillsCallOut.parseDate(stringMap.get('From'));
            this.toDate = MRO_SRV_SapQueryBillsCallOut.parseDate(stringMap.get('To'));
        }
    }

    public class MeterDetails {
        @AuraEnabled
        public String meterDetailId { get; set; }
        @AuraEnabled
        public String serialNumber { get; set; }
        @AuraEnabled
        public String dial { get; set; }
        @AuraEnabled
        public String oldReading { get; set; }
        @AuraEnabled
        public String newReading { get; set; }
        @AuraEnabled
        public Double quantity { get; set; }
        @AuraEnabled
        public Double averageConsumption { get; set; }
        @AuraEnabled
        public String oldIndex { get; set; }
        @AuraEnabled
        public String oldIndexType { get; set; }
        @AuraEnabled
        public String newIndex { get; set; }
        @AuraEnabled
        public String newIndexType { get; set; }
        @AuraEnabled
        public Date fromDate { get; set; }
        @AuraEnabled
        public Date toDate { get; set; }

        public MeterDetails(String meterDetailId, String serialNumber, String dial, String oldReading, String newReading, Double quantity,
                Double averageConsumption, String oldIndex, String newIndex, Date fromDate, Date toDate) {
            this.meterDetailId = meterDetailId;
            this.serialNumber = serialNumber;
            this.dial = dial;
            this.oldReading = oldReading;
            this.newReading = newReading;
            this.quantity = quantity;
            this.averageConsumption = averageConsumption;
            this.oldIndex = oldIndex;
            this.newIndex = newIndex;
            this.fromDate = fromDate;
            this.toDate = toDate;

        }

        public MeterDetails(Map<String, String> stringMap) {
            //this.meterDetailId = Double.valueOf(stringMap.get('InvoicedAmount'));
            this.serialNumber = stringMap.get('MeterSerialNumber');
            this.dial = stringMap.get('DialId');
            this.newIndex = stringMap.get('NewIndex');
            this.newIndexType = stringMap.get('NewIndexType');
            this.oldIndex = stringMap.get('OldIndex');
            this.oldIndexType = stringMap.get('OldIndexType');
            this.quantity = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('Quantity'));
            this.averageConsumption = MRO_SRV_SapQueryBillsCallOut.parseDouble(stringMap.get('AverageConsumption'));
            this.fromDate = parseStringDate(stringMap.get('ConsumptionPeriodFrom'));
            this.toDate = parseStringDate(stringMap.get('ConsumptionPeriodTo'));
        }
    }

    private static Date parseStringDate(String stringDate){
        if(String.isEmpty(stringDate) || stringDate.length() < 8){
            return null;
        }
        Integer year = Integer.valueOf(stringDate.substring(0, 4));
        Integer month = Integer.valueOf(stringDate.substring(4, 6));
        Integer day = Integer.valueOf(stringDate.substring(6, 8));
        return Date.newInstance(year, month, day);
    }

}