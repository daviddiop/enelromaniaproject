/**
 * Created by Giuseppe Mario Pastore on 03-04-2020
 */

@IsTest
public with sharing class MRO_LC_DecoRecoWizardTst {
	@TestSetup
	static void setup() {
        
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('DecoReco').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'MeterCheckEle', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'MeterCheckEle', recordTypeEle, '');
        insert phaseDI010ToRC010;

		ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
		scriptTemplate.Code__c = 'DisconnectionReconnectionWizardCodeTemplate_1';
		insert scriptTemplate;
		ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
		scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
		insert scriptTemplateElement;
		Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
		insert sequencer;

        
		List<Case> caseList = new List<Case>();
		List<Supply__c> supplyList = new List<Supply__c>();
         System.debug('Inizio');

        
        Sequencer__c seq = new Sequencer__c();
        seq.Type__c = 'CustomerCode';
        seq.SequenceLength__c=9;
        seq.Sequence__c=2504;
        Insert seq;
        System.debug('SEQUENCE');

        Account account = new Account();
        account.Name = 'PersonAccount1';
        insert account;
        System.debug('00000');

        ScriptTemplate__c sctpTmpl = new ScriptTemplate__c();
        sctpTmpl.Code__c='TestCode__c';
        insert sctpTmpl;

		Account accountTrader = TestDataFactory.account().traderAccount().build();
		accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
		accountTrader.Name = 'TraderAccount1';
		accountTrader.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
		accountTrader.Key__c = '1324433';
        accountTrader.BusinessType__c = 'Commercial areas';
        accountTrader.IsDisCoENEL__c = true;
		insert accountTrader;
        System.debug('1111');

        /*Account accountDistributor = TestDataFactory.account().traderAccount().build();
		accountDistributor.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
		accountDistributor.Name = 'TraderAccount1';
		accountDistributor.VATNumber__c= MRO_UTL_TestDataFactory.CreateFakeVatNumber();
		accountDistributor.Key__c = '1324433';
        accountDistributor.BusinessType__c = 'State Institution';
        accountDistributor.IsDisCoENEL__c = true;
		insert accountDistributor;*/
        System.debug('1111-bbbbbbbbb');

		CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
		insert companyDivision;
        System.debug('2222');

		Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
		dossier.Account__c = account.Id;
		insert dossier;
        System.debug('333');

		Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
		insert individual;
        System.debug('444');

		Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
		interaction.Interlocutor__c = individual.Id;
		insert interaction;
        System.debug('555');

		wrts_prcgvr__ActivityTemplate__c template = new wrts_prcgvr__ActivityTemplate__c();
		insert template;
        System.debug('666');

		ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
		servicePoint.Account__c = account.Id;
		servicePoint.Trader__c = accountTrader.Id;
//		servicePoint.Distributor__c = accountDistributor.Id;
        servicePoint.Code__c = 'TestCode__c';
		servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
		insert servicePoint;
        System.debug('777');

		Contract contract = TestDataFactory.contract().createContract().build();
		contract.AccountId = account.Id;
		insert contract;
        System.debug('8888');
        
        Bank__c bank = new Bank__c();
		bank.Name = 'RO2255550000000012345678';
		bank.BIC__c = '5555';
		insert bank;

		Bank__c refundBank = new Bank__c();
		refundBank.Name = 'RO2255550000000012345678';
		refundBank.BIC__c = '5555';
		insert refundBank;

		BillingProfile__c billingProfile = new BillingProfile__c();
		billingProfile.Account__c = account.Id;
		billingProfile.Bank__c = bank.Id;
		billingProfile.RefundBank__c = refundBank.Id;
		billingProfile.RefundIBAN__c = 'RO2255550000000012345678';
		billingProfile.PaymentMethod__c = 'Direct Debit';
        billingProfile.IBAN__c = 'IT60X0542811101000000123456';
		insert billingProfile;
        System.debug('9999');

		for (Integer i = 0; i < 20; i++) {
			Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
			supply.Contract__c = contract.Id;
			supply.Account__c = account.Id;
			supply.ServicePoint__c = servicePoint.Id;
			supply.CompanyDivision__c = companyDivision.Id;
			supplyList.add(supply);
		}
		insert supplyList;
        System.debug('10-10-10');

		for (Integer i = 0; i < 10; i++) {
			Case caseRecord = TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
			caseRecord.AccountId = account.Id;
			caseRecord.Supply__c = supplyList[0].Id;
			caseRecord.Channel__c = 'MyEnel';
			caseRecord.Origin = 'Web';
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('DecoReco').getRecordTypeId();
			//caseRecord.BillingProfile__c = billingProfile.Id;
			caseRecord.Trader__c = accountTrader.Id;
			caseRecord.Dossier__c = dossier.Id;
			caseList.add(caseRecord);
		}
		insert caseList;
        System.debug('11-11-11');

	}

	@IsTest
	public static void initializeTest() {
		Account account = [SELECT id FROM Account LIMIT 1];
		Dossier__c dossier = [SELECT id, Status__c, Phase__c FROM Dossier__c LIMIT 1];
		CompanyDivision__c companyDivision = [SELECT id FROM CompanyDivision__c LIMIT 1];
		ScriptTemplate__c sctpTmpl =  [SELECT id FROM ScriptTemplate__c LIMIT 1];

		Map<String, Object> argsMap = new Map<String, Object>();
		argsMap.put('accountId', account.Id);
		argsMap.put('dossierId', dossier.Id);
		argsMap.put('companyDivisionId', companyDivision.Id);
		argsMap.put('templateId', sctpTmpl.Id);


		Test.startTest();
		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'initialize', argsMap, true);
		Test.stopTest();
		Map<String, Object> result = (Map<String, Object>)response;
		system.assert(result != null);
	}

	@IsTest
	public static void initializeErrorNoAccountTest() {
		Dossier__c dossier = [SELECT id, Status__c, Phase__c FROM Dossier__c LIMIT 1];
		CompanyDivision__c companyDivision = [SELECT id FROM CompanyDivision__c LIMIT 1];
		ScriptTemplate__c sctpTmpl =  [SELECT id FROM ScriptTemplate__c LIMIT 1];

		Map<String, Object> argsMap = new Map<String, Object>();
		argsMap.put('accountId', null);
		argsMap.put('dossierId', dossier.Id);
		argsMap.put('companyDivisionId', companyDivision.Id);
		argsMap.put('templateId', sctpTmpl.Id);


		Test.startTest();
		try {
			Object response = TestUtils.exec(
				'MRO_LC_DecoRecoWizard', 'initialize', argsMap, true);
		} catch (Exception e) {
			System.assert(true, e.getMessage());
		}
		Test.stopTest();
	}

	@IsTest
	public static void initializeErrorTest() {
		Map<String, Object> argsMap = new Map<String, Object>();
		Dossier__c dossier = [SELECT id, Status__c, Phase__c FROM Dossier__c LIMIT 1];
		Account account = [SELECT id FROM Account LIMIT 1];
		argsMap.put('accountId', account.Id);
		argsMap.put('dossierId', '');
		argsMap.put('companyDivisionId', '');
		argsMap.put('genericRequestId', dossier.Id);


		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'initialize', argsMap, true);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);
		Test.stopTest();
	}

	@IsTest
	public static void saveDraftBPTest(){
		List <Case> caseList = [
			SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c, Status, Phase__c, Dossier__c
			FROM Case
			LIMIT 1
		];

		BillingProfile__c billingProfile = [
			SELECT Id
			FROM BillingProfile__c
		];

		Test.startTest();
		MRO_LC_DecoRecoWizard.DraftBillingProfileInput draftBillingProfileInput = new MRO_LC_DecoRecoWizard.DraftBillingProfileInput();
		draftBillingProfileInput.oldCaseList = caseList;
		draftBillingProfileInput.billingProfileId = billingProfile.Id;

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'saveDraftBP', draftBillingProfileInput, true);

		system.debug('2 *** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);
		Test.stopTest();
	}

	/*@IsTest
	public static void saveDraftBPErrorTest(){
		Test.startTest();
		MRO_LC_DecoRecoWizard.DraftBillingProfileInput draftBillingProfileInput = new MRO_LC_DecoRecoWizard.DraftBillingProfileInput();
		draftBillingProfileInput.oldCaseList = (List<Case>)null;
		draftBillingProfileInput.billingProfileId = '';

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'saveDraftBP', draftBillingProfileInput, true);

		system.debug('2 *** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == true);
		Test.stopTest();
	}*/

	@IsTest
	public static void createCaseTest() {
		CompanyDivision__c companyDivision = [
			SELECT Id,Name
			FROM CompanyDivision__c
			LIMIT 1
		];
		Account account = [
			SELECT Id
			FROM Account
			LIMIT 1
		];
		Account trader = [
			SELECT Id
			FROM Account
			WHERE Name = 'TraderAccount1'
			LIMIT 1
		];
		Dossier__c dossier = [
			SELECT Id
			FROM Dossier__c
			LIMIT 1
		];
		List<Case> caseList = [SELECT Id, Supply__c, Status, CancellationReason__c, Phase__c, Dossier__c,
			EffectiveDate__c, StartDate__c, AccountId, Trader__c, Reason, Channel__c, Origin
		FROM Case];
		List<Supply__c> supply = [SELECT Id, Name, Account__c, RecordTypeId, RecordType.DeveloperName, CompanyDivision__c,
			BillingProfile__c, Contact__c, Contract__c
		FROM Supply__c
		];

		MRO_LC_DecoRecoWizard.CreateCaseInput createCaseInput = new MRO_LC_DecoRecoWizard.CreateCaseInput();
		//createCaseInput.effectiveDate = Date.today();
		//createCaseInput.referenceDate = Date.today();
		createCaseInput.accountId = account.Id;
		createCaseInput.dossierId = dossier.Id;
		//createCaseInput.trader = trader.Id;
		createCaseInput.searchedSupplyFieldsList = supply;
		createCaseInput.caseList = caseList;
		//createCaseInput.reason = 'reason';
		createCaseInput.channelSelected = 'Back Office Sales';
		createCaseInput.originSelected = 'phone';


		Test.startTest();

		/* MRO_LC_DecoRecoWizard drw = new MRO_LC_DecoRecoWizard();
		 Object response = drw.createCase(createCaseInputString);*/
		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'createCase', createCaseInput, true);
		system.debug('createCaseTest ***** '+response);
		//Map<String, Object> result = (Map<String, Object>) response;
		//System.assertEquals(true, result.get('error')== true);
		Test.stopTest();

	}

	@IsTest
	public static void createCaseDurationTest() {
		CompanyDivision__c companyDivision = [
				SELECT Id,Name
				FROM CompanyDivision__c
				LIMIT 1
		];
		Account account = [
				SELECT Id
				FROM Account
				LIMIT 1
		];
		Account trader = [
				SELECT Id
				FROM Account
				WHERE Name = 'TraderAccount1'
				LIMIT 1
		];
		Dossier__c dossier = [
				SELECT Id
				FROM Dossier__c
				LIMIT 1
		];
		List<Case> caseList = [SELECT Id, Supply__c, Status, CancellationReason__c, Phase__c, Dossier__c,
				EffectiveDate__c, StartDate__c, AccountId, Trader__c, Reason, Channel__c, Origin
		FROM Case];
		List<Supply__c> supply = [SELECT Id, Name, Account__c, RecordTypeId, RecordType.DeveloperName, CompanyDivision__c,
				BillingProfile__c, Contact__c, Contract__c
		FROM Supply__c
		];

		MRO_LC_DecoRecoWizard.CreateCaseInput createCaseInput = new MRO_LC_DecoRecoWizard.CreateCaseInput();
		//createCaseInput.effectiveDate = Date.today();
		//createCaseInput.referenceDate = Date.today();
		createCaseInput.accountId = account.Id;
		createCaseInput.dossierId = dossier.Id;
		//createCaseInput.trader = trader.Id;
		createCaseInput.searchedSupplyFieldsList = supply;
		createCaseInput.caseList = caseList;
		createCaseInput.durationValue = 2;
		createCaseInput.channelSelected = 'Back Office Sales';
		createCaseInput.originSelected = 'phone';


		Test.startTest();

		/* MRO_LC_DecoRecoWizard drw = new MRO_LC_DecoRecoWizard();
		 Object response = drw.createCase(createCaseInputString);*/
		Object response = TestUtils.exec(
				'MRO_LC_DecoRecoWizard', 'createCase', createCaseInput, true);
		system.debug('createCaseTest ***** '+response);
		//Map<String, Object> result = (Map<String, Object>) response;
		//System.assertEquals(true, result.get('error')== true);
		Test.stopTest();

	}

	@IsTest
	public static void updateCaseListTest() {
		List<Case> caseList = [SELECT Id, Supply__c, Status, CancellationReason__c, Phase__c, Dossier__c
		FROM Case];
		Dossier__c dossier = [
			SELECT Id
			FROM Dossier__c
			LIMIT 1
		];
		system.debug('updateCaseListTest dossier ***** '+dossier);
		system.debug('updateCaseListTest caseList ***** '+caseList);
		MRO_LC_DecoRecoWizard.UpdateCaseListInput updateCaseListInput = new MRO_LC_DecoRecoWizard.UpdateCaseListInput();
		updateCaseListInput.oldCaseList = caseList;
		updateCaseListInput.dossierId = dossier.Id;

		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'updateCaseList', updateCaseListInput, true);

		Test.stopTest();
		system.debug('updateCaseListTest ***** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);
	}

	@IsTest
	public static void updateCaseListTraderChangeTest() {
		Case myCase = [SELECT Id, Supply__c, Status, CancellationReason__c, Phase__c, Dossier__c, Reason__c
		FROM Case limit 1];
		myCase.Reason__c = 'Trader change';
		List<Case> caseList = new List<Case>();
		caseList.add(myCase);
		Dossier__c dossier = [
			SELECT Id
			FROM Dossier__c
			LIMIT 1
		];
		MRO_LC_DecoRecoWizard.UpdateCaseListInput updateCaseListInput = new MRO_LC_DecoRecoWizard.UpdateCaseListInput();
		updateCaseListInput.oldCaseList = caseList;
		updateCaseListInput.dossierId = dossier.Id;

		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'updateCaseList', updateCaseListInput, true);

		Test.stopTest();
		system.debug('update ***** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);
	}

	@IsTest
	public static void updateCaseListErrorTest() {
		MRO_LC_DecoRecoWizard.UpdateCaseListInput updateCaseListInput = new MRO_LC_DecoRecoWizard.UpdateCaseListInput();
		updateCaseListInput.oldCaseList = (List<Case>)null;
		updateCaseListInput.dossierId = '';

		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'updateCaseList', updateCaseListInput, true);

		Test.stopTest();

		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == true);
	}

	@IsTest
	public static void cancelProcessTest() {
		List<Case> caseList = [SELECT Id, Supply__c, Status, CancellationReason__c, Phase__c, Dossier__c
		FROM Case];
		Dossier__c dossier = [
			SELECT Id, Status__c
			FROM Dossier__c
			LIMIT 1
		];
		system.debug('cancelProcessTest dossier ***** '+dossier);
		system.debug('cancelProcessTest caseList ***** '+caseList);

		MRO_LC_DecoRecoWizard.CancelProcessInput cancelProcessInput = new MRO_LC_DecoRecoWizard.CancelProcessInput();
		cancelProcessInput.oldCaseList = caseList;
		cancelProcessInput.dossierId = dossier.Id;

		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'cancelProcess', cancelProcessInput, true);

		Test.stopTest();
		system.debug('cancelProcessTest response ***** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);

	}

	@IsTest
	public static void cancelProcessErrorTest() {
		MRO_LC_DecoRecoWizard.CancelProcessInput cancelProcessInput = new MRO_LC_DecoRecoWizard.CancelProcessInput();
		cancelProcessInput.oldCaseList = (List<Case>)null;
		cancelProcessInput.dossierId = '';

		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'cancelProcess', cancelProcessInput, true);

		Test.stopTest();
		system.debug('cancel ***** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == true);

	}

	@IsTest
	public static void UpdateDossierTest() {
		Dossier__c dossier = [
			SELECT Id, Status__c
			FROM Dossier__c
			LIMIT 1
		];
		Map<String, String> updateDossier = new Map<String, String> {
			'dossierId' => dossier.Id,
			'channelSelected' => 'web',
			'originSelected'  => 'phone'
		};
		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'UpdateDossier', updateDossier, true);

		Test.stopTest();
		system.debug('UpdateDossier ***** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);
	}

	@IsTest
	public static void getSupplyRecordTest() {
		List<Supply__c> supplyList = [SELECT Id FROM Supply__c limit 1];
		List<Id> supplyListId = new List<Id>();
		supplyListId.add(supplyList[0].Id);

		MRO_LC_DecoRecoWizard.supplyInputIds sii = new MRO_LC_DecoRecoWizard.supplyInputIds();
		sii.supplyIds = supplyListId;

		Test.startTest();
		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'getSupplyRecord', sii, true);
		Test.stopTest();

		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('supplies') != null);
	}


	@IsTest
	public static void updateCommodityToDossierTest() {
		Dossier__c dossier = [
			SELECT Id, Status__c
			FROM Dossier__c
			LIMIT 1
		];
		Map<String, String> jsonInput = new Map<String, String> {
			'dossierId' => dossier.Id,
			'commodity' => 'Electric'
		};
		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'updateCommodityToDossier', jsonInput, true);

		Test.stopTest();

		system.debug('updateCommodityToDossier  ***** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);
	}

	@IsTest
	public static void setChannelAndOriginTest() {
		Dossier__c dossier = [
				SELECT Id, Status__c
				FROM Dossier__c
				LIMIT 1
		];
		Map<String, String> jsonInput = new Map<String, String> {
				'dossierId' => dossier.Id,
				'origin' => 'phone',
				'channel' => 'Back Office Sales'
		};
		Test.startTest();

		Object response = TestUtils.exec(
				'MRO_LC_DecoRecoWizard', 'setChannelAndOrigin', jsonInput, true);

		Test.stopTest();
	}

	@IsTest
	public static void updateCommodityToDossierErrorTest() {
		Map<String, String> jsonInput = new Map<String, String> {
			'dossierId' => '',
			'commodity' => ''
		};
		Test.startTest();

		Object response = TestUtils.exec(
			'MRO_LC_DecoRecoWizard', 'updateCommodityToDossier', jsonInput, true);

		Test.stopTest();

		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == true);
	}

	@IsTest
	public static void deleteCasesTest() {
		List<Case> caseList = [SELECT Id, Supply__c, Status, CancellationReason__c, Phase__c, Dossier__c
		FROM Case];
		Dossier__c dossier = [
				SELECT Id, Status__c
				FROM Dossier__c
				LIMIT 1
		];

		MRO_LC_DecoRecoWizard.CancelProcessInput cancelProcessInput = new MRO_LC_DecoRecoWizard.CancelProcessInput();
		cancelProcessInput.oldCaseList = caseList;
		cancelProcessInput.dossierId = dossier.Id;

		Test.startTest();

		Object response = TestUtils.exec(
				'MRO_LC_DecoRecoWizard', 'deleteCases', cancelProcessInput, true);

		Test.stopTest();
		system.debug('cancelProcessTest response ***** '+response);
		Map<String, Object> result = (Map<String, Object>)response;
		System.assert(result.get('error') == false);

	}
}