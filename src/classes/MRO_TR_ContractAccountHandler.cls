/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 13, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_TR_ContractAccountHandler implements TriggerManager.ISObjectTriggerHandler {
    public static Map<Id, Sequencer__c> BILLING_ACCOUNT_NUMBER_SEQUENCERS;
    private DatabaseService databaseSrv = DatabaseService.getInstance();

    public void beforeInsert() {
        this.generateBillingAccountNumbers(Trigger.new);
    }

    public void beforeUpdate() {
    }

    public void beforeDelete() {
    }

    public void afterInsert() {

        if (BILLING_ACCOUNT_NUMBER_SEQUENCERS != null) {
            this.databaseSrv.updateSObject(BILLING_ACCOUNT_NUMBER_SEQUENCERS.values());
        }
        List<ContractAccount__c> contractAccountsToUpdate = new List<ContractAccount__c>();
        for(ContractAccount__c ca : (List<ContractAccount__c>)Trigger.new) {

            ContractAccount__c newCa = new ContractAccount__c(Id = ca.Id);
            if (ca.Key__c == null) {
                newCa.Key__c = ca.Name.substring(3);
            }
            if (ca.IntegrationKey__c == null) {
                newCa.IntegrationKey__c = ca.Name.substring(3);
            }
            contractAccountsToUpdate.add(newCa);
        }
        update contractAccountsToUpdate;
    }

    public void afterUpdate() {
    }

    public void afterDelete() {
    }

    public void afterUndelete() {
    }

    private void generateBillingAccountNumbers(List<ContractAccount__c> contractAccounts) {
        //System.debug('### generateBillingAccountNumbers');
        Map<Id, List<ContractAccount__c>> newContractAccounts = new Map<Id, List<ContractAccount__c>>();
        for (ContractAccount__c ca : contractAccounts) {
            if (ca.BillingAccountNumber__c == null && ca.CompanyDivision__c != null) {
                if (!newContractAccounts.containsKey(ca.CompanyDivision__c)) {
                    newContractAccounts.put(ca.CompanyDivision__c, new List<ContractAccount__c>{ca});
                }
                else {
                    newContractAccounts.get(ca.CompanyDivision__c).add(ca);
                }
            }
        }
        //System.debug('### newContractAccounts: '+newContractAccounts);

        if (!newContractAccounts.isEmpty()) {
            MRO_QR_Sequencer sequencerQueries = MRO_QR_Sequencer.getInstance();
            BILLING_ACCOUNT_NUMBER_SEQUENCERS = sequencerQueries.getBillingAccountNumberSequencer(newContractAccounts.keySet());
            //System.debug('### sequencers: '+BILLING_ACCOUNT_NUMBER_SEQUENCERS);

            for (Id companyDivisionId : newContractAccounts.keySet()) {
                Sequencer__c seq = BILLING_ACCOUNT_NUMBER_SEQUENCERS.get(companyDivisionId);
                for (ContractAccount__c ca : newContractAccounts.get(companyDivisionId)) {
                    seq.Sequence__c += 1;
                    String partialCode = String.valueOf(seq.Sequence__c.intValue()).leftPad(seq.SequenceLength__c.intValue(), '0');
                    Integer checkDigit = 0;
                    for (String ch : partialCode.split('')) {
                        checkDigit += Integer.valueOf(ch);
                    }
                    ca.BillingAccountNumber__c = seq.CompanyPrefix__c + partialCode + Math.mod(checkDigit, 10);
                }
            }
        }
    }
}