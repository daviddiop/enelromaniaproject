/**
 * Created by tommasobolis on 16/06/2020.
 */

public with sharing class MRO_LC_FileRelatedListCnt {

    public class ColumnWrapper {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String fieldName { get; set; }
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public Boolean sortable { get; set; }
        /* @AuraEnabled
         public String sortDirection {get;set;}
         @AuraEnabled
         public String showSortRow {get;set;}*/
    }

    public class RecordWrapper {
        @AuraEnabled
        public List<Map<String, Object>> values { get; set; }
        @AuraEnabled
        public Id id { get; set; }
        @AuraEnabled
        public Id documentId { get; set; }
        @AuraEnabled
        public String documentIdSobjectType { get; set; }
        @AuraEnabled
        public String externalId { get; set; }
        @AuraEnabled
        public String discoFileId { get; set; }
        @AuraEnabled
        public String documentUrl { get; set; }

        public RecordWrapper(Id id, Id documentId, String externalId, String discoFileId, String documentUrl, List<Map<String, Object>> values) {
            this.id = id;
            this.documentId = documentId;
            this.documentIdSobjectType = String.isNotBlank(documentId) ? documentId.getSObjectType().getDescribe().getName() : null;
            this.externalId = externalId;
            this.discoFileId = discoFileId;
            this.documentUrl = documentUrl;
            this.values = values;
        }
    }

    public class RelatedListWrapper {
        @AuraEnabled
        public List<RecordWrapper> records { get; set; }
        @AuraEnabled
        public List<ColumnWrapper> columns { get; set; }


        public RelatedListWrapper() {
            this.records = new List<RecordWrapper>();
            this.columns = new List<ColumnWrapper>();
        }
    }

    private static RelatedListWrapper relatedList = new RelatedListWrapper();

    @AuraEnabled
    public static Map<String, Object> initialize(String recordId, String title, String relationshipFieldAPIName, String fieldSet) {

        System.debug('MRO_LC_FileRelatedListCnt.initialize');

        String sObjectType = 'FileMetadata__c';

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            //populate label Map
            Map<String, String> label = new Map<String, String>();
            if (String.isNotBlank(title) && title.contains('$Label.')) {
                String[] split = title.split('\\.');
                String userLanguage = UserInfo.getLanguage();
                List<String> translations = MRO_UTL_LabelTranslator.translate(new List<String>{
                        split[1]
                }, userLanguage);
                label.put('title', translations[0]);

            } else if (String.isNotBlank(title) && !title.contains('$Label.')) {
                label.put('title', title);
            } else {

                String pluralSObjectLabel = Schema.getGlobalDescribe().get(sObjectType).getDescribe().getLabelPlural();
                label.put('title', pluralSObjectLabel);
            }

            response = createRelatedList(recordId, sObjectType, fieldSet, relationshipFieldAPIName, null, '');

            //response.put('recordsNumber', this.relatedList.records.size());
            response.put('relatedList', relatedList);
            response.put('label', label);

        } catch (Exception e) {
            System.debug(e.getMessage());
            response.put('error', true);
            response.put('errorMessage', e.getMessage());
            response.put('errorStackTraceString', e.getStackTraceString());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> createRelatedList(String recordId, String relationshipFieldAPIName, String fieldSet, Integer orderByColumnIndex, String orderByDirection) {

        return createRelatedList(recordId, relationshipFieldAPIName, fieldSet, 'Case__c', orderByColumnIndex, orderByDirection);
    }

    public static Map<String, Object> createRelatedList(String recordId, String sObjectType, String fieldSet, String relationshipFieldAPIName, Integer orderByColumnIndex, String orderByDirection) {
        System.debug('MRO_LC_FileRelatedListCnt.createRelatedList');

        Map<String, Object> response = new Map<String, Object>();
        try {
            response.put('error', false);

            List<String> sObjectFieldsToGet = new List<String>();
            if (String.isNotBlank(fieldSet) && !Test.isRunningTest()) {
                DescribeSObjectResult objResult = Schema.getGlobalDescribe().get(sObjectType).getDescribe();
                Schema.FieldSet fs = objResult.fieldSets.getMap().get(fieldSet); //'RelatedSObjectComponentFieldSet'
                List<Schema.FieldSetMember> fsms = fs.getFields();

                for (Schema.FieldSetMember fsm : fsms) {
                    sObjectFieldsToGet.add(fsm.getFieldPath());
                }
            } else if (Test.isRunningTest()) {
                sObjectFieldsToGet.add('FirstName');
                sObjectFieldsToGet.add('LastName');
                sObjectFieldsToGet.add('AccountId');
            }

            fillRelatedListColumns(sObjectFieldsToGet);
            fillRelatedListRecords(recordId, relationshipFieldAPIName, orderByColumnIndex, orderByDirection); //country
            response.put('relatedList', relatedList);

        } catch (Exception ex) {
            response.put('error', true);
            response.put('errorMessage', ex.getMessage());
            response.put('errorStackTraceString', ex.getStackTraceString());
        }
        return response;

    }

    private static void fillRelatedListColumns(List<String> fieldsToGet) {

        if (relatedList.columns.isEmpty()) {

            DescribeSObjectResult objResult = Schema.getGlobalDescribe().get('FileMetadata__c').getDescribe();
            for (String field : fieldsToGet) {
                ColumnWrapper column = new ColumnWrapper();

                DescribeFieldResult fieldResult = objResult.fields.getMap().get(field).getDescribe();
                column.label = fieldResult.getLabel();
                column.fieldName = fieldResult.getName();
                column.type = String.valueOf(fieldResult.getType()).toLowerCase();
                column.sortable = true;

                relatedList.columns.add(column);
            }
        }
    }

    private static void fillRelatedListRecords(String recordId, String relationshipFieldAPIName, Integer orderByColumnIndex, String orderByDirection) {

        String selectClause = getSelectClause();
        String[] selectClauseSplit = selectClause.split(',');
        //String selectClause = String.join(selectClauseList, ',');

        String sQuery = ' SELECT ' + String.escapeSingleQuotes(selectClause) +
                ' FROM FileMetadata__c ' +
                ' WHERE ' + String.escapeSingleQuotes(relationshipFieldAPIName) + '= \'' + String.escapeSingleQuotes(recordId) + '\'';


        if (orderByColumnIndex != null) {

            sQuery += ' ORDER BY ' + selectClauseSplit[orderByColumnIndex + 1];
            if (String.isNotBlank(orderByDirection)) {
                sQuery += ' ' + String.escapeSingleQuotes(orderByDirection);
            }
            sQuery += ' NULLS last';
        } else {
            if (!relatedList.columns.isEmpty()) {
                sQuery += ' ORDER BY ' + relatedList.columns[0].fieldName + ' ASC NULLS last';
            }
        }

        System.debug('##SC sQuery: ' + sQuery);
        List<FileMetadata__c> records = Database.query(sQuery);

	    Set<Id> contentVersionIds = new Set<Id>();
	    Map<Id, ContentVersion> contentVersionsMap = new Map<Id, ContentVersion>();
	    for (FileMetadata__c fm : records) {
		    if (fm.DocumentId__c != null && ((Id)fm.DocumentId__c).getSObjectType().getDescribe().getName() == 'ContentVersion') {
			    contentVersionIds.add(fm.DocumentId__c);
		    }
	    }
	    if (!contentVersionIds.isEmpty()) {
		    contentVersionsMap = new Map<Id, ContentVersion>([SELECT ContentDocumentId FROM ContentVersion WHERE Id IN :contentVersionIds]);
	    }

        Object fieldValue;
        Id fieldId;
        Id sObjectId;
        //String valueToGet;
        for (FileMetadata__c record : records) {
            sObjectId = record.Id;
            List<Map<String, Object>> values = new List<Map<String, Object>>();

            for (Integer i = 1; i < selectClauseSplit.size(); i++) {

                if (!selectClauseSplit[i].contains('.')) {

                    fieldValue = record.get(selectClauseSplit[i]);
                } else {
                    SObject so = record;
                    String[] splitField = selectClauseSplit[i].split('\\.');
                    String sObjType;
                    Integer j;
                    for (j = 0; j < splitField.size() - 1; j++) {

                        if (splitField[j].contains('__r')) {
                            fieldId = (Id) so.get(splitField[j].replace('__r', '__c'));
                            so = so.getSObject(splitField[j]);
                        }
                    }
                    fieldValue = so != null ? so.get(splitField[j]) : so;

//                    for (j = 0; j < splitField.size() - 1; j++) {
//                        if (splitField[j].contains('__r')) {
//                            splitField[j] = splitField[j].replace('__r', '__c');
//                            Id rId = (Id) so.get(splitField[j]);
//                            sObjType = rId.getSobjectType().getDescribe().getName();
//                            System.debug('MRO_LC_FileRelatedListCnt.fillRelatedListRecords - rId: ' + rId);
//                            System.debug('MRO_LC_FileRelatedListCnt.fillRelatedListRecords - sObjType: ' + sObjType);
//                        } else {
//                            sObjType = splitField[j];
//                        }
//                        System.debug('MRO_LC_FileRelatedListCnt.fillRelatedListRecords - so: ' + so);
//                        so = so.getSObject(sObjType);
//                    }
//                    recordValue = so.get(splitField[j]);

                }

                if (fieldValue != null) {
                    values.add(new Map<String, Object> {'id' => fieldId, 'text' => fieldValue});
                } else {
                    values.add(new Map<String, Object> {'id' => null, 'text' => ''});
                }

            }
	        Id documentId = (Id) record.get('DocumentId__c');
	        if (contentVersionsMap.containsKey(documentId)) {
		        documentId = contentVersionsMap.get(documentId).ContentDocumentId;
	        }
            relatedList.records.add(new RecordWrapper(
                    sObjectId,
                    documentId,
                    (String) record.get('ExternalId__c'),
                    (String) record.get('DiscoFileId__c'),
                    (String) record.get('DocumentUrl__c'),
                    values));

        }
    }

    private static String getSelectClause() {

        List<String> selectClauseList = new List<String>();
        selectClauseList.add('Id');

        for (ColumnWrapper column : relatedList.columns) {

            if (column.type == 'text' || column.type == 'textarea' || column.type == 'picklist') {
                selectClauseList.add('toLabel(' + column.fieldName + ')');
            } else if (column.type == 'reference') {
                String field = column.fieldName;
                if (field.endsWith('__c')) {
                    if (Schema.getGlobalDescribe().get('FileMetadata__c').getDescribe().fields.getMap().get(field).getDescribe().getReferenceTo()[0].getDescribe().getName() != 'Case') {
                        field = field.replace('__c', '__r.Name');
                    } else {
                        field = field.replace('__c', '__r.CaseNumber');
                    }
                } else if (field.endsWith('Id')) {
                    if (Schema.getGlobalDescribe().get('FileMetadata__c').getDescribe().fields.getMap().get(field).getDescribe().getReferenceTo()[0].getDescribe().getName() != 'Case') {
                        field = field.replace('Id', '.Name');
                    } else {
                        field = field.replace('Id', '.CaseNumber');
                    }
                }
                selectClauseList.add(field);
            } else if (String.isNotBlank(column.type)) {
                selectClauseList.add(column.fieldName);
            }
        }

        if (!selectClauseList.contains('DocumentId__c')) selectClauseList.add('DocumentId__c');
        if (!selectClauseList.contains('ExternalId__c')) selectClauseList.add('ExternalId__c');
        if (!selectClauseList.contains('DiscoFileId__c')) selectClauseList.add('DiscoFileId__c');
        if (!selectClauseList.contains('DocumentUrl__c')) selectClauseList.add('DocumentUrl__c');

        String selectClause = String.join(selectClauseList, ',');
        System.debug('MRO_LC_FileRelatedListCnt.getSelectClause - selectClause: ' + selectClause);
        return selectClause;
    }

    @AuraEnabled
    public static Map<String, Object> retrieveFromFilenet(String recordId) {

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            String nomeClasse = 'MRO_SRV_FileNetCallOut';
            String nomeFlusso = 'filenetPreview';
            String nomeEndPoint = 'filenet';

            Map<String, Object> argsMap = new Map<String, Object>();
            argsMap.put('method', nomeClasse);
            argsMap.put('sender', new FileMetadata__c (Id = recordId));

            Map<String, String> parameters = new Map<String, String>();
            parameters.put('requestType', nomeFlusso);
            parameters.put('timeout', '60000');
            parameters.put('endPoint', nomeEndPoint);

            argsMap.put('parameters', parameters);

            MRO_SRV_SendMRRcallout instance = new MRO_SRV_SendMRRcallout();
            instance.execute((Object)argsMap);

            List<ExecutionLog__c> executionLogs =
                    [SELECT Id, Code__c, Response__c
                     FROM ExecutionLog__c
                     WHERE
                        FileMetadata__c = :recordId AND
                        RequestType__c = :nomeFlusso AND
                        CreatedById = :UserInfo.getUserId()
                    ORDER BY CreatedDate DESC];

            if(!executionLogs.isEmpty() && String.isNotBlank(executionLogs[0].Response__c)) {

                Map<String, Object> payload = (Map<String, Object>)JSON.deserializeUntyped(executionLogs[0].Response__c);
                List<Object> payloadResponses = (List<Object>)payload.get('responses');
                Map<String, Object> payloadResponse = (Map<String, Object>) payloadResponses[0];
                response.put('code', payloadResponse.get('code'));
                response.put('description', payloadResponse.get('description'));
            } else {

                response.put('code', 'KO');
                response.put('description', '');
            }

        } catch (Exception e) {

            System.debug(e.getMessage());
            response.put('error', true);
            response.put('errorMessage', e.getMessage());
            response.put('errorStackTraceString', e.getStackTraceString());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> retrieveFromDMS(String recordId) {

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            FileMetadata__c fileMetadata =
                [SELECT Id, ExternalId__c, DiscoFileId__c, ExternalRequestID__c FROM FileMetadata__c WHERE Id = :recordId];

            String nomeClasse = fileMetadata.ExternalId__c != null ? 'MRO_SRV_FileNetCallOut' : 'MRO_SRV_DMSCallOut';
            String nomeFlusso = fileMetadata.ExternalId__c != null ? 'filenetPreview' : 'dmsPreview';
            String nomeEndPoint = fileMetadata.ExternalId__c != null ? 'filenet' : 'dms';

            Map<String, Object> argsMap = new Map<String, Object>();
            argsMap.put('method', nomeClasse);
            argsMap.put('sender', new FileMetadata__c (Id = recordId));

            Map<String, String> parameters = new Map<String, String>();
            parameters.put('requestType', nomeFlusso);
            parameters.put('timeout', '60000');
            parameters.put('endPoint', nomeEndPoint);

            argsMap.put('parameters', parameters);

            MRO_SRV_SendMRRcallout instance = new MRO_SRV_SendMRRcallout();
            instance.execute((Object)argsMap);

            List<ExecutionLog__c> executionLogs =
            [SELECT Id, Code__c, Response__c
            FROM ExecutionLog__c
            WHERE
            FileMetadata__c = :recordId AND
            RequestType__c = :nomeFlusso AND
            CreatedById = :UserInfo.getUserId()
            ORDER BY CreatedDate DESC];

            if(!executionLogs.isEmpty() && String.isNotBlank(executionLogs[0].Response__c)) {

                Map<String, Object> payload = (Map<String, Object>)JSON.deserializeUntyped(executionLogs[0].Response__c);
                List<Object> payloadResponses = (List<Object>)payload.get('responses');
                Map<String, Object> payloadResponse = (Map<String, Object>) payloadResponses[0];
                response.put('code', payloadResponse.get('code'));
                response.put('description', payloadResponse.get('description'));
            } else {

                response.put('code', 'KO');
                response.put('description', '');
            }

        } catch (Exception e) {

            System.debug(e.getMessage());
            response.put('error', true);
            response.put('errorMessage', e.getMessage());
            response.put('errorStackTraceString', e.getStackTraceString());
        }
        return response;
    }
}