/**
 * Created by David on 19.07.2019.
 */
public with sharing class AddressFormCnt extends ApexServiceLibraryCnt{
    
    public with sharing class getObjectInfo extends  AuraCallable{
        public override  Object perform ( final String jsonInput ){
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            SObjectType objToken = Schema.getGlobalDescribe().get(params.get('objectApiName'));
            Map<String, Schema.SObjectField> objFieldMap = objToken.getDescribe().fields.getMap();
            for(String fieldName:objFieldMap.keySet()){
                response.put(fieldName,objFieldMap.get(fieldName).getDescribe().getLabel());
            }
            return response;

        }
    }
}