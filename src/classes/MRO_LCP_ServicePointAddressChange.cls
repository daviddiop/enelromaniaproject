/**
* @description Class controller for Service Point components
* @author goudiaby
* @date 20-03-2020
 */
public with sharing class MRO_LCP_ServicePointAddressChange extends ApexServiceLibraryCnt {
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_ServicePoint servicePointQuery = MRO_QR_ServicePoint.getInstance();
    private static MRO_QR_Supply mroQrSupply = MRO_QR_Supply.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();

    /**
     * Initialize method to get data at the beginning for Meter Change
     */
    public class InitializeServicePointAddressChange extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String genericRequestId = params.get('genericRequestId');

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'ServicePointAddressChange');
            Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
            if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                dossierSrv.updateParent(dossier.Id, genericRequestId);
                dossier.Parent__c = genericRequestId;
            }
            if (String.isNotBlank(dossier.Id)) {
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                response.put('caseTile', cases);
            }

            String code = 'ServicePointChangeCodeTemplate_1';
            ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
            if (String.isNotBlank(scriptTemplate.Id)) {
                response.put('templateId', scriptTemplate.Id);
            }
            response.put('dossierId', dossier.Id);
            response.put('dossier', dossier);
            response.put('genericRequestId', dossier.Parent__c);
            response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
            response.put('companyDivisionId', dossier.CompanyDivision__c);
            response.put('accountId', accountId);
            response.put('recordTypeServicePointAddressChange', Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ServicePointAddressChange').getRecordTypeId());
            response.put('error', false);
            return response;
        }
    }

    public class getServicePoint extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String servicePointId = params.get('servicePointId');

            if (String.isBlank(servicePointId)) {
                throw new WrtsException(System.Label.ServicePoint + ' - ' + System.Label.MissingId);
            }
            try {
                ServicePoint__c servicePoint = servicePointQuery.getById(servicePointId);
                MRO_SRV_Address.AddressDTO addressFromServicePoint = MRO_LC_OpportunityServiceItem.copyServicePointAddressToDTO(servicePoint);
                response.put('servicePointAddress', addressFromServicePoint);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    /**
     * @author Boubacar Sow
     * @date 26/11/2020
     * @description [ENLCRO-1805] Service Point  Address Change  - treating multi POD service sites situation
     *
     */
    public class OtherSupplyRelatedToServiceSite extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String serviceSiteId = params.get('serviceSiteId');
            String supplyId = params.get('supplyId');

            if (String.isBlank(serviceSiteId)) {
                throw new WrtsException(System.Label.ServiceSite + ' - ' + System.Label.MissingId);
            }
            if (String.isBlank(supplyId)) {
                throw new WrtsException(System.Label.Supply + ' - ' + System.Label.MissingId);
            }
            try {
                List<Supply__c> supplies = mroQrSupply.getSuppliesByServiceSite(supplyId,serviceSiteId);
                response.put('supplies', supplies);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class updateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');
            Dossier__c dossier = new Dossier__c (Id = dossierId,Channel__c = channel, Origin__c = origin);
            databaseSrv.updateSObject(dossier);
            response.put('error', false);
            return response;
        }
    }

    public class UpdateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {

            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            Savepoint sp = Database.setSavepoint();
            try {

                Map<String, String> params = asMap(jsonInput);
                List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
                List<Supply__c> otherSupplies = (List<Supply__c>) JSON.deserialize(params.get('otherSupplies'), List<Supply__c>.class);
                String dossierId = params.get('dossierId');
                String companyDivisionId;
                if (oldCaseList[0] != null) {

                    companyDivisionId = oldCaseList[0].CompanyDivision__c;
                }
                List<Case> caseList = new List<Case>();
                if (!otherSupplies.isEmpty() && !oldCaseList.isEmpty()) {
                    Case caseAddress = caseQuery.getCaseList(oldCaseList[0].Id)[0];
                    for (Supply__c supply : otherSupplies){
                        caseList.add(
                            new Case(
                                AccountId = supply.Account__c,
                                Dossier__c = dossierId,
                                Supply__c = supply.Id,
                                CompanyDivision__c = supply.CompanyDivision__c,
                                Distributor__c = supply.ServicePoint__r.Distributor__c,
                                ServicePointCode__c = supply.ServicePoint__r.Name,
                                RecordTypeId = oldCaseList[0].RecordTypeId,
                                Channel__c = oldCaseList[0].Channel__c,
                                Origin = oldCaseList[0].Origin,
                                BillingProfile__c = oldCaseList[0].BillingProfile__c,
                                Status = oldCaseList[0].Status,
                                DisCoNotes__c = oldCaseList[0].DisCoNotes__c,
                                AddressStreetType__c = caseAddress.AddressStreetType__c,
                                AddressStreetNumber__c = caseAddress.AddressStreetNumber__c,
                                AddressCity__c = caseAddress.AddressCity__c,
                                AddressPostalCode__c = caseAddress.AddressPostalCode__c,
                                AddressCountry__c = caseAddress.AddressCountry__c,
                                AddressAddressNormalized__c = caseAddress.AddressAddressNormalized__c,
                                AddressStreetName__c = caseAddress.AddressStreetName__c,
                                AddressApartment__c = caseAddress.AddressApartment__c,
                                AddressFloor__c = caseAddress.AddressFloor__c,
                                AddressLocality__c = caseAddress.AddressLocality__c,
                                AddressProvince__c = caseAddress.AddressProvince__c,
                                AddressBuilding__c = caseAddress.AddressBuilding__c,
                                AddressBlock__c = caseAddress.AddressBlock__c,
                                AddressStreetNumberExtn__c = caseAddress.AddressStreetNumberExtn__c,
                                AddressStreetId__c = caseAddress.AddressStreetId__c,
                                AddressAddressKey__c = caseAddress.AddressAddressKey__c
                            )
                        );
                    }
                    if (!caseList.isEmpty()) {
                        databaseSrv.insertSObject(caseList);
                        for (Case aCase : caseList){
                            oldCaseList.add(aCase);
                        }
                    }
                }
                caseSrv.generateAddressVerificationActivities(caseQuery.listByIds((new Map<Id,Case>(oldCaseList)).keySet()), null);
                caseSrv.setNewOnCaseAndDossierServicePointAddress(oldCaseList, dossierId, companyDivisionId);

            } catch (Exception ex) {

                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');

            caseSrv.setCanceledOnCaseAndDossier(oldCaseList, dossierId);
            response.put('error', false);
            return response;
        }
    }
}