public with sharing class ProductOptionQueries {

    public static ProductOptionQueries getInstance() {
        return (ProductOptionQueries)ServiceLocator.getInstance(ProductOptionQueries.class);
    }

    public List<ProductOptionRelation__c> listPORelationByProductIds(String productId) {
        return listPORelationByProductIds(new Set<String>{productId});
    }

    public List<ProductOptionRelation__c> listPORelationByProductIds(Set<String> productIds) {
        return [
            SELECT Id, Product__c, Order__c,
                Product__r.Name, Product__r.Family, Product__r.Description, Product__r.Image__c, Product__r.RecordType.Name,
                ProductOption__r.Accessibility__c, ProductOption__r.BundleProduct__c, ProductOption__r.DefaultValue__c,
                ProductOption__r.Description__c, ProductOption__r.HelpText__c, ProductOption__r.Key__c, ProductOption__r.Label__c,
                ProductOption__r.Max__c, ProductOption__r.Min__c, ProductOption__r.Required__c, ProductOption__r.Selected__c,
                ProductOption__r.Type__c, ProductOption__r.ValueSet__c, ProductOption__r.RecordType.Name,
                ProductOption__r.RecordType.DeveloperName
            FROM ProductOptionRelation__c
            WHERE Product__c IN :productIds
                AND ProductOption__r.Accessibility__c != 'Hidden'
            ORDER BY Order__c
        ];
    }

    public ProductOption__c getProductById(String recordId) {
        return [
            SELECT RecordTypeId, Type__c, DefaultValue__c, Family__c, Min__c,Max__c, ValueSet__c
            FROM ProductOption__c
            WHERE id=: recordId
        ];
    }
}