/**
 * Created by ferhati on 03/04/2019.
 * Modified by Moussa on 23/07/2019.
 */
@isTest
private class TerminationWizardCntTst {
    @testSetup
    private static void setup() {
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Case> caseList = new List<Case>();
        List<Account> listAccount = new list<Account>();
        listAccount.add(TestDataFactory.account().personAccount().build());
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        Contact contact = TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;        

        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[1].Id;
        insert contract;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        insert interaction;

        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;        

        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;        

        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Account__c = listAccount[1].Id;
        servicePoint.Trader__c = listAccount[1].Id;
        servicePoint.Distributor__c = listAccount[1].Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[1].Id;
        billingProfile.PaymentMethod__c = 'Giro';
        insert billingProfile;
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Account__c = listAccount[1].Id;
            supply.ServicePoint__c = servicePoint.Id;
            supply.CompanyDivision__c = companyDivision.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[1].Id;
        insert dossier;
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[i].Id;
            caseRecord.CompanyDivision__c = supplyList[i].CompanyDivision__c;
            caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
    }


    @isTest
    static void initializeTest() {
        Account account = [
			SELECT Id,Name
			FROM Account
			LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];        
        CompanyDivision__c companyDivision = [
            SELECT Id,Name 
            FROM CompanyDivision__c 
            WHERE Name = 'ENEL 1' 
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            //'companyDivisionId' => companyDivision.Id
            'companyDivisionId' => null
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => '',
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        response = TestUtils.exec(
                'TerminationWizardCnt', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }

    @isTest
    static void initializeExceptionTest() {

        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => '',
            'interactionId' => '',
            'companyDivisionId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();

    }

    @isTest
    private static void createCaseTest() {
        List<Supply__c> supply = [
            SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c
            FROM Supply__c
            WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        List<Account> myAccount = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String supplyListString = JSON.serialize(supply);
        String caseListString = JSON.serialize(caseList);
        String effectiveDateString = JSON.serialize(caseList[0].EffectiveDate__c);

        Map<String, String > inputJSON = new Map<String, String>{
            'searchedSupplyFieldsList' => supplyListString,
            'accountId' => myAccount[0].Id,
            'caseList' => caseListString,
            'dossierId' => dossier.Id,
            'disconnectionDate' => effectiveDateString,
            'disconnectionCause' => caseList[0].Reason__c
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'createCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();

    }

    @isTest
    private static void emptyCreateCaseTest() {
        List<Supply__c> supply = [
            SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName
            FROM Supply__c
            WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        String supplyListString = JSON.serialize(supply);
        String caseListString = JSON.serialize(caseList);
        String effectiveDateString = JSON.serialize(caseList[0].EffectiveDate__c);

        Map<String, String > inputJSON = new Map<String, String>{
                'searchedSupplyFieldsList' => supplyListString,
                'accountId' => 'testInexistentAccountId',
                'caseList' => caseListString,
                'dossierId' => dossier.Id,
                'disconnectionDate' => effectiveDateString,
                'disconnectionCause' => caseList[0].Reason__c
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'createCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );

        Test.stopTest();

    }


    @isTest
    private static void updateBillingProfileTest() {
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
        ];
  
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'billingProfileId' => billingProfile.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'saveDraftBP', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }  


    @isTest
    private static void emptyUpdateBillingProfileTest() {
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
        ];
  
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'billingProfileId' => caseList[0].Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'saveDraftBP', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();

    }        

    @isTest
    private static void updateCaseListTest() {      
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        List<Supply__c> listSupplies = [
            SELECT Id, CompanyDivision__c,ServicePoint__c
            FROM Supply__c
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
        ];
        String caseListString = JSON.serialize(caseList);
        String listSuppliesString = JSON.serialize(listSupplies);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'searchedSupplyFieldsList' => listSuppliesString,
            'dossierId' => dossier.Id,
            'billingProfileId' => billingProfile.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'updateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }

    @isTest
    private static void emptyUpdateCaseListTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        BillingProfile__c billingProfile = [
                SELECT Id
                FROM BillingProfile__c
        ];
        List<Supply__c> listSupplies = [
            SELECT Id, CompanyDivision__c,ServicePoint__c
            FROM Supply__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        String listSuppliesString = JSON.serialize(listSupplies);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id,
            'billingProfileId' => caseList[0].Id,
            'searchedSupplyFieldsList' => listSuppliesString
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'updateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @isTest
    private static void cancelProcessTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'oldCaseList' => caseListString,
                'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'cancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @isTest
    private static void cancelProcessExceptionTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'oldCaseList' => caseListString,
                'dossierId' => caseList[0].Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'TerminationWizardCnt', 'cancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }


    /*@isTest
    private static void emptyCancelProcessTest() {      
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];         

        Test.startTest();
        Map<String, Object> response = TerminationWizardCnt.cancelProcess(caseList, caseList[0].Id);
        System.assertEquals(true, response.get('error'));
        Test.stopTest();
    }*/
}