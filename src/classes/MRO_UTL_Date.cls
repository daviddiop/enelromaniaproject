/**
 * Created by BADJI on 23/12/2019.
 */

public with sharing class MRO_UTL_Date extends ApexServiceLibraryCnt{

    /**
     * Method class that allows to get the system's constants on Lightning Component
     */
    public with sharing class getValidDate extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String datesToCheck = params.get('checkEffectiveDate');
            Date effectiveDate = (Date) JSON.deserialize(datesToCheck, Date.class);
            if (isNotWorkingDate(effectiveDate)) {
                response.put('isInvalid',true);
            } else {
                response.put('isInvalid',false);
            }
            return response;
        }
    }
    /*
    * @Description : Check if the date Contract Termination is valid or Working day
    * @author: INSA BADJI
    *  @date 23/12/2019
    * @param Datetime datetimeToCheck
    */
    public static Boolean isNotWorkingDate(Datetime dateToCheck) {
        if (checkStaticHolidays(dateToCheck) || chackDynamicHolidays(dateToCheck)) {
            return true;
        } else {
            return false;
        }
    }
    /**
      * @author  INSA BADJI
      * @description Check the static Holidays in Roumania
      * @date 23/12/2019
      * @param Datetime datetimeToCheck
      *
      * @return Return a Boolean
      */
    public static Boolean checkStaticHolidays(Datetime datetimeToCheck) {
        if ((datetimeToCheck.format('E') == 'Sat') ||
            (datetimeToCheck.format('E') == 'Sun') ||
            (datetimeToCheck.format('dd/MM') == '01/01') ||
            (datetimeToCheck.format('dd/MM') == '02/01') ||
            (datetimeToCheck.format('dd/MM') == '24/01') ||
            (datetimeToCheck.format('dd/MM') == '26/04') ||
            (datetimeToCheck.format('dd/MM') == '28/04') ||
            (datetimeToCheck.format('dd/MM') == '29/04') ||
            (datetimeToCheck.format('dd/MM') == '01/05') ||
            (datetimeToCheck.format('dd/MM') == '01/06') ||
            (datetimeToCheck.format('dd/MM') == '15/08') ||
            (datetimeToCheck.format('dd/MM') == '30/11') ||
            (datetimeToCheck.format('dd/MM') == '01/12') ||
            (datetimeToCheck.format('dd/MM') == '25/12') ||
            (datetimeToCheck.format('dd/MM') == '26/12')) {
            return true;
        } else {
            return false;
        }
    }
    /**
      * @author  INSA BADJI
      * @description Check the Dynamic Holidays in Roumania for each year
      * @date 23/12/2019
      * @param Datetime datetimeToCheck
      *
      * @return Return a Boolean
  */

    public static Boolean chackDynamicHolidays(Datetime dateToCheck) {
        Datetime esterDate = getEasterDate(dateToCheck.year());
        Datetime pentcostDate = getPentecostDate(dateToCheck.year());
        if ((esterDate == dateToCheck) ||
            ((esterDate + 1) == dateToCheck) ||
            ((esterDate - 2) == dateToCheck) ||
            (pentcostDate) == dateToCheck ||
            ((pentcostDate + 1) == dateToCheck)) {
            return true;
        } else {
            return false;
        }
    }
    /**
      * @author  INSA BADJI
      * @description Check the static Holidays in Roumania
      * @date 23/12/2019
      * @param Datetime datetimeToCheck
      *
      * @return Return a Boolean
  */


    public static Datetime getEasterDate(Integer yearToCheck) {
        Integer a, b, c, d, e, M, N;
        Integer easterDay, easterMonth;
        Datetime newDates = System.now();
        Integer year = newDates.year();
        M = 15;
        N = 6;
        a = Math.mod(year, 19);
        b = Math.mod(year, 4);
        c = Math.mod(year, 7);
        d = Math.mod(19 * a + M, 30);
        e = Math.mod(2 * b + 4 * c + 6 * d + N, 7);

        if (d + e < 10) {
            easterDay = d + e + 22;
            easterMonth = 3;
        } else {
            easterDay = d + e - 9;
            easterMonth = 4;
        }
        Date newDate = Date.newInstance(yearToCheck, easterMonth, easterDay);
        return newDate.addDays(13);
    }
    /**
      * @author  INSA BADJI
      * @description Pentecost Date
      * @date 23/12/2019
      * @param Integer yearToCheck
      *
      * @return Datetime
  */

    public static Datetime getPentecostDate(Integer yearToCheck) {
        Datetime easterDate = getEasterDate(yearToCheck);
        return easterDate.addDays(49);

    }

    /**
    * @author Luca Ravicini
    * @description calculate woring days
    * @param  fromDate start date
    * @param days number of days should be added
    * @return number of total days as off days have been ignored
    */
    public static Integer calculateWorkDays (Date fromDate, Integer days){
        Datetime dt = datetime.newInstance(fromDate.year(), fromDate.month(), fromDate.day());

        DateTime endDate=dt;

        Integer offDays=0;

        for(Integer i=0; i < days; i++){
            endDate=endDate.addDays(1);
            String day=endDate.format('E');

            if(day.equals('Sat') || day.equals('Sun')){
                offDays++;
                i--;
            }

        }
        return days + offDays ;
    }
    /**
     * @author Luca Ravicini
     * @description comparing two dates
     * @param  date1 first date to be compared
     * @param date2 second date to compare date1 to
     * @since May 5,2020
     * @return Integer value based on date1 if it's after date2 returns 1.
     * if it's  before date2 returns -1. if the same returns 0.
     */
    public static Integer compareDates(Date date1, Date date2){
        if(date1.year() > date2.year()){
            return 1;
        }else if(date1.year() == date2.year()){
            if(date1.month() > date2.month()){
                return 1;
            }else if(date1.month() == date2.month()){
                if(date1.day() > date2.day()){
                    return 1;
                } else if(date1.day() < date2.day()){
                    return -1;
                }  else {
                    return 0;
                }
            }else {
                return -1;
            }
        } else {
            return -1;
        }

    }

    public static Date addWorkingDays(Date initialDate, Integer businessDaysToAdd) {
        Date finalDate = initialDate;
        Integer direction = businessDaysToAdd < 0 ? -1 : 1;
        while (businessDaysToAdd != 0) {
            finalDate = finalDate.addDays(direction);
            if (!MRO_UTL_Date.isNotWorkingDate(finalDate)) {
                businessDaysToAdd -= direction;
            }
        }
        return finalDate;
    }

}