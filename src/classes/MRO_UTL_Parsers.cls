public with sharing class MRO_UTL_Parsers extends ApexServiceLibraryCnt{
    private static final MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();

    public static MRO_UTL_Parsers getInstance() {
        return (MRO_UTL_Parsers) ServiceLocator.getInstance(MRO_UTL_Parsers.class);
    }

    public Integer parseInt(Object input){
        if (input == null){
            return null;
        }

        try {
            if (Pattern.compile('[0-9]+').matcher(String.valueOf(input)).matches()) {
                return Integer.valueOf(input);
            }
        } catch ( Exception e ){
            // Invalid id
        }

        // ID is not valid
        return null;
    }

    public Id parseId(Object sfdcId){
        try {
            String sfdcIdStr = String.valueOf(sfdcId);
            if (Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(sfdcIdStr).matches()){
                // Try to assign it to an Id before checking the type
                Id id = Id.valueOf(sfdcIdStr);

                // If the tests passed, it's valid
                return id;
            }
        } catch ( Exception e ){
            // Invalid id
        }

        // ID is not valid
        return null;
    }

}