public with sharing class SettingProvider {

    static Map<String, EnergyApp2Settings__c> energyApp2Settings = EnergyApp2Settings__c.getAll();

    public static Boolean isSaveUnIdentifiedInterlocutorAllowed() {
        EnergyApp2Settings__c settings = energyApp2Settings.get('Default');
        if (settings == null) {
            return false;
        }
        return settings.AllowSaveUnIdentifiedInterlocutor__c;
    }
}