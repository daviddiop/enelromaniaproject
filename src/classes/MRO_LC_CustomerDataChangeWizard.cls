/**
 * @author  Luca Ravicini
 * @since   Mar 9, 2020
 * @desc   Controller class for CustomerDataChangeWizard
 *
 */

public with sharing class MRO_LC_CustomerDataChangeWizard extends ApexServiceLibraryCnt {
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Contact contactQuery = MRO_QR_Contact.getInstance();
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static final String REQUEST_TYPE = constantsSrv.REQUEST_TYPE_CUSTOMER_DATA_CHANGE;
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_CompanyDivision companyQuery = MRO_QR_CompanyDivision.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            try {
                String accountId = params.get('accountId');
                String interactionId = params.get('interactionId');
                String dossierId = params.get('dossierId');
                String companyDivisionId = params.get('companyDivisionId');
                String genericRequestId = params.get('genericRequestId');
                String gDPRParentDossierId = params.get('gDPRParentDossierId');
                String templateId = params.get('templateId');

                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Schema.RecordTypeInfo caseRecordTypeInfo = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(REQUEST_TYPE);
                Id caseRecordTypeId = caseRecordTypeInfo.getRecordTypeId();
                List<Case> cases = caseQuery.getOpenedCase(accountId,caseRecordTypeId);

                if(!cases.isEmpty()){
                    response.put('error', true);
                    String msg= String.format(System.Label.AccountOpenCase, new List<String> {cases.get(0).CaseNumber});
                    response.put('errorMsg',  msg );
                    response.put('openCase', true);
                    return response;
                }
                Account account = accQuery.findAccount(accountId);
                String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(MRO_UTL_Constants.CHANGE).getRecordTypeId();

                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, REQUEST_TYPE);
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);

                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                    response.put('genericRequestId', dossier.Parent__c);
                }

                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(gDPRParentDossierId)) {
                    dossierSrv.updateParent(dossier.Id, gDPRParentDossierId);
                }

                String contactId;
                Contact contact = contactQuery.getByAccountId(accountId);

                if(contact != null) {
                    contactId = contact.Id;
                }

                String code = 'CustomerDataChangeWizardCodeTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);

                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }

                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('accountId', accountId);
                response.put('contactId', contactId);
                response.put('account', account);
                response.put('isPersonAccount', account.IsPersonAccount);
                response.put('recordTypeCustomerDataChange', caseRecordTypeId);
                response.put('subProcessChangeAccName',constantsSrv.SUB_PROCESS_CHNG_ACC_NAME);
                response.put('subProcessChangeCompanyName',constantsSrv.SUB_PROCESS_CHNG_COMPANY_NAME);
                response.put('subProcessChangeContact',constantsSrv.SUB_PROCESS_CHNG_CONTACT);
                response.put('subProcessChangeCAEN',constantsSrv.SUB_PROCESS_CHNG_CAEN);
                response.put('subProcessChangeCNP',constantsSrv.SUB_PROCESS_CHNG_CNP);
                response.put('subProcessChangeCUI',constantsSrv.SUB_PROCESS_CHNG_CUI);
                response.put('subProcessChangeONRC',constantsSrv.SUB_PROCESS_CHNG_ONRC);
                response.put('subProcessChangeResidentialAddress',constantsSrv.SUB_PROCESS_CHNG_RESIDENTIAL_ADDRESS);
                response.put('subProcessChangeOtherFields',constantsSrv.SUB_PROCESS_CHNG_OTHER_FIELDS);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            try {
                Map<String, String> params = asMap(jsonInput);
                String dossierId = params.get('dossierId');
                String origin = params.get('origin');
                String channel = params.get('channel');
                dossierSrv.updateDossierChannel( dossierId, channel, origin);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public with sharing class validateSubProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String subProcess =  params.get('subProcess');
            String accountId = params.get('accountId');
            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                if (String.isBlank(dossierId)) {
                    throw new WrtsException(System.Label.Dossier + ' - ' + System.Label.MissingId);
                }

                String  permissionName;
                Boolean authorized=true;
                Boolean isChangeCnp=subProcess == constantsSrv.SUB_PROCESS_CHNG_CNP;
                Boolean isChangeCui=false;
                Boolean isChangeCompanyName=false;

                if(isChangeCnp){
                    permissionName=constantsSrv.CUSTOM_PERMISSION_MDFY_CNP;
                } else if (subProcess == constantsSrv.SUB_PROCESS_CHNG_CUI){
                    isChangeCui=true;
                    permissionName=constantsSrv.CUSTOM_PERMISSION_MDFY_VAT_NUMBER;
                }else if(subProcess == constantsSrv.SUB_PROCESS_CHNG_COMPANY_NAME) {
                    isChangeCompanyName=true;
                    permissionName=constantsSrv.CUSTOM_PERMISSION_MDFY_ACC_NAME;
                }

                if (!String.isBlank(permissionName)) {
                    authorized = FeatureManagement.checkPermission(permissionName);
                }

                if(authorized){
                    Account account = accQuery.findAccount(accountId);
                    Boolean isPersonalAccount=account.IsPersonAccount;
                    String accountType= (isPersonalAccount)? System.Label.PersonAccount: System.Label.BusinessAccount;
                    if(isPersonalAccount){
                        authorized = !(isChangeCui || isChangeCompanyName
                                || subProcess == constantsSrv.SUB_PROCESS_CHNG_ONRC || subProcess == constantsSrv.SUB_PROCESS_CHNG_CAEN);


                    } else {
                        authorized =!( isChangeCnp || subProcess == constantsSrv.SUB_PROCESS_CHNG_ACC_NAME);
                    }

                    if(!authorized){
                        String msg= String.format(System.Label.AllowedFor, new List<String> {subProcess,accountType});
                        response.put('errorMsg',  msg );
                    } else {
                        if(isPersonalAccount){
                            if(authorized){
                                String nationalNumber=account.NationalIdentityNumber__pc;
                                Boolean fakeAccount=nationalNumber!=null && nationalNumber.toLowerCase().startsWith('f');
                                if(isChangeCnp ){
                                    if(!fakeAccount){
                                        authorized=false;
                                        response.put('errorMsg',  System.Label.NotAllowedModifyCNP );
                                    }
                                }else if(fakeAccount){
                                    response.put('warnMsg',  System.Label.NationalNumberFake );
                                }
                            }
                        } else {
                            if(isChangeCui){
                                Boolean foreignCompany=account.ForeignCompany__c;
                                if(foreignCompany != null && foreignCompany){
                                    authorized = false;
                                    response.put('errorMsg',  System.Label.ForeignCompanyExemption);
                                } /*else  {
                                    String key = account.Key__c;
                                    if(Key != null && key.indexOf('_') != -1){
                                        authorized = false;
                                        String msg= String.format(System.Label.InvalidCUIKey, new List<String> {key});
                                        response.put('errorMsg',  msg);
                                    }

                                }*/

                            }
                        }

                    }
                } else {
                    String msg= String.format(System.Label.PermissionTo, new List<String> {subProcess});
                    response.put('errorMsg',  msg);
                }
                if(authorized){

                    String casePhase = constantsSrv.CASE_START_PHASE;
                    String caseStatus = constantsSrv.CASE_STATUS_NEW;
                    String dossierStatus = constantsSrv.DOSSIER_STATUS_NEW;
                  /*  if(subProcess == constantsSrv.SUB_PROCESS_CHNG_CONTACT){
                        List<Contract> activeContracts= contractQuery.getActiveContractsByAccount(accountId);
                        Boolean hasActiveContract= !activeContracts.isEmpty();
                        if(hasActiveContract){
                            casePhase = constantsSrv.CASE_SEND_TO_SAP_PHASE;
                        }else {
                            casePhase = constantsSrv.CASE_END_PHASE;
                            caseStatus = constantsSrv.CASE_STATUS_EXECUTED;
                            dossierStatus = constantsSrv.DOSSIER_STATUS_CLOSED;
                        }
                    }else if (subProcess == constantsSrv.SUB_PROCESS_CHNG_OTHER_FIELDS){
                        casePhase = constantsSrv.CASE_END_PHASE;
                        caseStatus = constantsSrv.CASE_STATUS_EXECUTED;
                        dossierStatus = constantsSrv.DOSSIER_STATUS_CLOSED;
                    }  */
                    response.put('casePhase', casePhase);
                    response.put('caseStatus', caseStatus);
                    response.put('dossierStatus', dossierStatus);
                    dossierSrv.updateDossierSubprocess(dossierId, subProcess);
                }
                response.put('authorized',authorized);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class validateKey extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            try{
                Map<String, String> params = asMap(jsonInput);
                String key =  params.get('key');
                if (String.isBlank(key)) {
                    throw new WrtsException(System.Label.Key + ' - ' + System.Label.MissingParameter);
                }
                String subProcess =  params.get('subProcess');
                List<Account> accounts=accountQuery.listByKey(key);
                Boolean valid = accounts.isEmpty();
                response.put('valid', valid);
                if(!valid){
                    String msg;
                    if(subProcess == constantsSrv.SUB_PROCESS_CHNG_CNP){
                        msg=System.Label.UniqueCNP;
                    }/*else {
                        msg=System.Label.UniqueCUI;
                    }*/
                    response.put('errorMsg', msg);
                }

                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }


    /*public with sharing class validateNaceCode extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            try{
                Map<String, String> params = asMap(jsonInput);
                String naceCode = params.get('naceCode');
                NACE__c nace=naceQuery.getNaceByClass(naceCode);
                response.put('valid', (nace !=null));
                if(nace !=null){
                    response.put('naceId', nace.Id);
                }else{
                    response.put('errorMsg', System.Label.NACECodeDoesNotExist);
                }
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }*/


    public class updateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            try {
                Map<String, String> params = asMap(jsonInput);
                List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
                String dossierId = params.get('dossierId');
                String dossierStatus = params.get('dossierStatus');
                String changedPhone = params.get('changedPhone');
                String changedEmail = params.get('changedEmail');
                String changedMobile = params.get('changedMobile');
                Id companyDivisionId;
                List<Case> caseToUpdateList = new List<Case>();

                String accid = dossierQuery.getById(dossierId).Account__c;
                Supply__c supply = supplyQuery.getCustomerDataChangeSupply(accid);
                if(supply != null){
                    companyDivisionId = supply.CompanyDivision__c;
                }
                /*List<CompanyDivision__c> companyList = companyQuery.getAllActive();
                if(!companyList.isEmpty()) {
                    for (Integer i = 0; i < companyList.size(); i++) {
                        companyDivisionId = companyList[i].Name == 'Enel Energie' ? companyList[i].Id : null;
                    }
                    companyDivisionId = companyDivisionId != null ? companyDivisionId : companyList[0].Id;
                }*/

                /*if (oldCaseList[0] != null) {
                    companyDivisionId = oldCaseList[0].CompanyDivision__c;
                }*/
                for(Case caseToUpdate : oldCaseList){
                    caseToUpdate.CompanyDivision__c = companyDivisionId;
                    caseToUpdate.EffectiveDate__c = System.today();
                    caseToUpdateList.add(caseToUpdate);
                }
                caseSrv.updateCases(caseToUpdateList);
                dossierSrv.updateDossierCompanyDivision(dossierId,companyDivisionId,dossierStatus);

                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class createCaseComment extends AuraCallable {
        Map<Id, Account> accountMap = new Map<Id, Account>();
        String contactPhone;
        String contactMobile;
        String contactEmail;
        String accountWebsite;
        String accountAnnualRevenue;
        String accountAnnualRevenueYear;
        String accountNumbOfEmployees;

        public override Object perform(final String jsonInput) {
            System.debug('Class : MRO_LC_CustomerDataChangeWizard -- inner : createCaseComment --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();

            try {
                Map<String, String> params = asMap(jsonInput);
                List<Case> cases = (List<Case>) JSON.deserialize(params.get('cases'), List<Case>.class);
                contactPhone = params.get('contactPhone');
                contactMobile = params.get('contactMobile');
                contactEmail = params.get('contactEmail');
                accountWebsite = params.get('accountWebsite');
                accountAnnualRevenue = params.get('accountAnnualRevenue');
                accountAnnualRevenueYear = params.get('accountAnnualRevenueYear');
                accountNumbOfEmployees = params.get('accountNumbOfEmployees');


                System.debug('cases : ' + cases);
                List<Case> caseList = caseQuery.listByIds(this.getInvolvedObjIds(cases).get('CASE_IDS'));
                Map<String,Set<Id>> involvedObjMap = this.getInvolvedObjIds(caseList);
                accountMap = accountQuery.getByIds(involvedObjMap.get('ACCOUNT_IDS'));

                Map<Id,String> comments = new Map<Id,String>();
                for(Case c : caseList){
                    System.debug('caseiD : '+c.id);
                    String message = this.generateComment(c, c.SubProcess__c);
                    comments.put(c.Id,message);
                }

                List<CaseComment> caseCommentsList = new List<CaseComment>();
                if(!comments.isEmpty())
                    caseCommentsList = this.writeComments(comments);

                if(this.caseCommentRecordsCreated(caseCommentsList)){
                    response.put('caseComments', caseCommentsList);
                    response.put('error', false);
                }
                else {
                    response.put('error', true);
                    response.put('errorMsg', System.Label.ErrorInCaseCommentCreating);
                }

            } catch (Exception ex) {
                Database.rollback(sp);
                System.debug(ex.getStackTraceString() + ' message : ' + ex.getMessage());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }

        private String generateComment(Case c, String subProcess){
            String subProcessLabel=MRO_UTL_Utils.getFieldLabel('SubProcess__c', 'Case');
            String separator=' : ';
            String fieldSeparator = ' - ';
            String message = subProcessLabel + separator + subProcess + ' - Old Value ';
            String objectAccount='Account';

            if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_CAEN)) {
                Account a = accountMap.get(c.AccountId);
                String naceLabel = MRO_UTL_Utils.getFieldLabel('NACECode__c', objectAccount);
                message += naceLabel + separator + a.NACECode__c;
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_ACC_NAME)) {
                Account a = accountMap.get(c.AccountId);
                String firstNameLabel = MRO_UTL_Utils.getFieldLabel('FirstName', objectAccount);
                String lastNameLabel = MRO_UTL_Utils.getFieldLabel('LastName', objectAccount);
                message += firstNameLabel + separator + a.FirstName + fieldSeparator;
                message += lastNameLabel + separator + a.LastName;
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_CUI)) {
                Account a = accountMap.get(c.AccountId);
                Boolean oldCheck = a.VATExempted__c;
                String vatNumberLabel = MRO_UTL_Utils.getFieldLabel('VATNumber__c', objectAccount);
                String vatExemptedLabel = MRO_UTL_Utils.getFieldLabel('VATExempted__c', objectAccount);
                message += vatNumberLabel + separator + a.VATNumber__c + fieldSeparator;
                message += vatExemptedLabel + separator + (oldCheck ? 'Checked' : 'Unchecked');
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_COMPANY_NAME)) {
                Account a = accountMap.get(c.AccountId);
                String nameLabel = MRO_UTL_Utils.getFieldLabel('Name', objectAccount);
                message += nameLabel + separator + a.Name;
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_ONRC)) {
                Account account = accountMap.get(c.AccountId);
                if(account != null){
                    String onrcCodeLabel = MRO_UTL_Utils.getFieldLabel('ONRCCode__c', objectAccount);
                    message +=  onrcCodeLabel + separator + account.ONRCCode__c;
                }
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_RESIDENTIAL_ADDRESS)) {
                Account account = accountMap.get(c.AccountId);
                /*message += ' Residential Street : ' + a.ResidentialStreetType__c +' ' + a.ResidentialStreetName__c +
                        ' ' + a.ResidentialStreetNumber__c + ' - ';*/
                if(account != null) {
                    String streetTypeLabel = MRO_UTL_Utils.getFieldLabel('ResidentialStreetType__c', objectAccount);
                    String streetNameLabel = MRO_UTL_Utils.getFieldLabel('ResidentialStreetName__c', objectAccount);
                    String streetNumberLabel = MRO_UTL_Utils.getFieldLabel('ResidentialStreetNumber__c', objectAccount);
                    String streetNumberExtLabel = MRO_UTL_Utils.getFieldLabel('ResidentialStreetNumberExtn__c', objectAccount);
                    String blockLabel = MRO_UTL_Utils.getFieldLabel('ResidentialBlock__c', objectAccount);
                    String apartmentLabel = MRO_UTL_Utils.getFieldLabel('ResidentialApartment__c', objectAccount);
                    String buildingLabel = MRO_UTL_Utils.getFieldLabel('ResidentialBuilding__c', objectAccount);
                    String floorLabel = MRO_UTL_Utils.getFieldLabel('ResidentialFloor__c', objectAccount);
                    String cityLabel = MRO_UTL_Utils.getFieldLabel('ResidentialCity__c', objectAccount);
                    String provinceLabel = MRO_UTL_Utils.getFieldLabel('ResidentialProvince__c', objectAccount);
                    String countryLabel = MRO_UTL_Utils.getFieldLabel('ResidentialCountry__c', objectAccount);
                    String localityLabel = MRO_UTL_Utils.getFieldLabel('ResidentialLocality__c', objectAccount);
                    String postalCodeLabel = MRO_UTL_Utils.getFieldLabel('ResidentialPostalCode__c', objectAccount);
                    String normalizedLabel = MRO_UTL_Utils.getFieldLabel('ResidentialAddressNormalized__c', objectAccount);
                    message += streetTypeLabel + separator + account.ResidentialStreetType__c + fieldSeparator;
                    message += streetNameLabel + separator + account.ResidentialStreetName__c + fieldSeparator;
                    message += streetNumberLabel + separator + account.ResidentialStreetNumber__c + fieldSeparator;
                    message += streetNumberExtLabel + separator + account.ResidentialStreetNumberExtn__c + fieldSeparator;
                    message += blockLabel + separator + account.ResidentialBlock__c + fieldSeparator;
                    message += apartmentLabel + separator + account.ResidentialApartment__c + fieldSeparator;
                    message += buildingLabel + separator + account.ResidentialBuilding__c + fieldSeparator;
                    message += floorLabel + separator + account.ResidentialFloor__c + fieldSeparator;
                    message += cityLabel + separator + account.ResidentialCity__c + fieldSeparator;
                    message += provinceLabel + separator + account.ResidentialProvince__c + fieldSeparator;
                    message += countryLabel + separator + account.ResidentialCountry__c + fieldSeparator;
                    message += localityLabel + separator + account.ResidentialLocality__c + fieldSeparator;
                    message += postalCodeLabel + separator + account.ResidentialPostalCode__c + fieldSeparator;
                    message += normalizedLabel + separator + account.ResidentialAddressNormalized__c ;
                }
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_CNP)) {
                Account a = accountMap.get(c.AccountId);
                String nationalIdentityNumberLabel = MRO_UTL_Utils.getFieldLabel('NationalIdentityNumber__pc', objectAccount);
                message += nationalIdentityNumberLabel + separator + a.NationalIdentityNumber__pc;
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_CONTACT)){
                Account account = accountMap.get(c.AccountId);
                if(account != null) {
                    Boolean isPersonalAccount = account.IsPersonAccount;
                    String objectModified = isPersonalAccount ? objectAccount : 'Contact';
                    String mobileFieldName = isPersonalAccount ? 'PersonMobilePhone' : 'MobilePhone';
                    String phoneFieldName = 'Phone' ;
                    String emailFieldName = isPersonalAccount ? 'PersonEmail' : 'Email';

                    String phoneLabel = MRO_UTL_Utils.getFieldLabel(phoneFieldName, objectModified);
                    String mobileLabel = MRO_UTL_Utils.getFieldLabel(mobileFieldName, objectModified);
                    String emailLabel = MRO_UTL_Utils.getFieldLabel(emailFieldName, objectModified);
                    message += phoneLabel + separator + (contactPhone != null ? contactPhone : '') + fieldSeparator;
                    message += mobileLabel + separator + (contactMobile != null ? contactMobile : '') + fieldSeparator;
                    message += emailLabel + separator + (contactEmail != null ? contactEmail : '');
                }
            } else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_OTHER_FIELDS)) {
                Account account = accountMap.get(c.AccountId);
                String webSiteLabel = MRO_UTL_Utils.getFieldLabel('Website', objectAccount);
                String annualRevenueLabel = MRO_UTL_Utils.getFieldLabel('AnnualRevenue', objectAccount);
                String annualRevenueYearLabel = MRO_UTL_Utils.getFieldLabel('AnnualRevenueYear__c', objectAccount);
                String numbOfEmployeesLabel = MRO_UTL_Utils.getFieldLabel('NumberOfEmployees', objectAccount);
                message += webSiteLabel + separator + (accountWebsite != null ? accountWebsite : '')  + fieldSeparator;
                message += annualRevenueLabel + separator + (accountAnnualRevenue!=null ? accountAnnualRevenue : '')  + fieldSeparator;
                message += numbOfEmployeesLabel + separator + (accountNumbOfEmployees!=null ? accountNumbOfEmployees : '') + fieldSeparator;
                message+= annualRevenueYearLabel + separator + (accountAnnualRevenueYear!=null ? accountAnnualRevenueYear : '');
            }
            return message;
        }

        private List<CaseComment> writeComments(Map<Id,String> comments){
            List<CaseComment> caseComments = new List<CaseComment>();
            try {
                caseComments = caseSrv.createCaseComments(comments);
                System.debug('output : Case Comments created ' + comments);
            } catch(Exception e){
                throw e;
            }
            return caseComments;
        }

        private Map<String,Set<Id>> getInvolvedObjIds(List<Case> cases){
            Map<String,Set<Id>> output = new Map<String,Set<Id>>();

            Set<Id> accountIds = new Set<Id>();
            Set<Id> caseIds = new Set<Id>();

            for(Case c : cases){
                if(c.Id!=null && String.isNotEmpty(c.Id))
                    caseIds.add(c.Id);
                if(c.AccountId!=null && String.isNotEmpty(c.AccountId))
                    accountIds.add(c.AccountId);
            }

            output.put('ACCOUNT_IDS',accountIds);
            output.put('CASE_IDS',caseIds);

            return output;

        }

        private Boolean caseCommentRecordsCreated(List<CaseComment> caseComments){
            return !caseComments.isEmpty();
        }

    }


    public with sharing class getOldData extends AuraCallable {

        public override Object perform(final String jsonInput) {
            System.debug('Class : MRO_LC_CustomerDataChangeWizard -- inner : getContactOldData --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            try {
                Map<String, String> params = asMap(jsonInput);
                String objectName = params.get('objectApiName');
                string objectId= params.get('objectId');
                if(objectName=='Account') {
                    Account account = accountQuery.findAccount(objectId);
                    response.put('accountWebsite', account.Website);
                    response.put('accountAnnualRevenue', account.AnnualRevenue);
                    response.put('accountAnnualRevenueYear', account.AnnualRevenueYear__c);
                    response.put('accountNumbOfEmployees', account.NumberOfEmployees);
                }else if(objectName=='Contact') {
                    Contact c = contactQuery.getById(objectId);
                    if(c!=null){
                        response.put('contactPhone',c.Phone);
                        response.put('contactMobile',c.MobilePhone);
                        response.put('contactEmail',c.Email);
                    }

                }
            }
            catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }



}