/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 12.05.20.
 */

public interface MRO_INT_MassiveHelper {

    Map<String, MRO_BA_MassiveHelperValidation.FileColumnDataDTO> performValidAction(Map<String, MRO_BA_MassiveHelperValidation.FileColumnDataDTO> mapValues);

    void performFinishAction(List<FileImportDataRow__c> rowsList, Map<String, String> params);
}