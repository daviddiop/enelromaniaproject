/**
 * Created by tommasobolis on 29/04/2020.
 */
public with sharing class MRO_SRV_TouchPoint {
    private static final MRO_QR_Dossier dossierQueries = MRO_QR_Dossier.getInstance();
    private static final MRO_QR_FileMetadata fileMetadataQueries = MRO_QR_FileMetadata.getInstance();
    private static final MRO_QR_ValidatableDocument validatableDocumentQueries = MRO_QR_ValidatableDocument.getInstance();
    private static final MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    /**
     * Returns and instance of the current class.
     *
     * @return instance of the current class.
     */
    public static MRO_SRV_TouchPoint getInstance() {

        return (MRO_SRV_TouchPoint) ServiceLocator.getInstance(MRO_SRV_TouchPoint.class);
    }

    public Map<String, Object> contractAcceptance(String tokenVal, Boolean accepted, Date acceptanceDate, String customerNotes) {
        Boolean success = true;
        String message;
        Savepoint sp = Database.setSavepoint();
        try {
            Dossier__c dossier = dossierQueries.getByToken(tokenVal);
            if (dossier == null) {
                success = false;
                message = 'Request not found';
            }
            else if (!dossier.ExternalAccessIdentifier__r.IsValid__c) {
                success = false;
                message = 'Invalid or expired token';
            }
            else {
                List<ValidatableDocument__c> validatableDocuments = validatableDocumentQueries.listUncheckedValidatableDocumentForDossier(dossier.Id);
                if (validatableDocuments.isEmpty()) {
                    success = false;
                    message = 'Validatable documents to be accepted not found';
                }
                else {
                    if (accepted) {
                        if (acceptanceDate == null) {
                            acceptanceDate = Date.today();
                        }
                        Set<String> contractDocumentTypeCodes = new Set<String>{'CTR_ELEC', 'CTR_GAZ', 'CTR_VAS'};
                        Set<String> documentTypeCodes = new Set<String>();
                        for (ValidatableDocument__c vd : validatableDocuments) {
                            documentTypeCodes.add(vd.DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c);

                        }
                        Map<String, FileMetadata__c> printedFMMap = fileMetadataQueries.mapPrintedDocumentByTypeCodes(documentTypeCodes, dossier.Id);

                        Map<Id, ValidatableDocument__c> validatableDocumentsToUpdate = new Map<Id, ValidatableDocument__c>();
                        Map<Id, Boolean> candidateActivitiesToClose = new Map<Id, Boolean>();
                        for (ValidatableDocument__c vd : validatableDocuments) {
                            if (printedFMMap.containsKey(vd.DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c)) {
                                vd.FileMetadata__c = printedFMMap.get(vd.DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c).Id;
                                if (contractDocumentTypeCodes.contains(vd.DocumentBundleItem__r.DocumentType__r.DocumentTypeCode__c)) {
                                    vd.CustomerSignedDate__c = acceptanceDate;
                                }
                                validatableDocumentsToUpdate.put(vd.Id, vd);
                                candidateActivitiesToClose.put(vd.ValidationActivity__c, true);
                            }
                        }
                        if (!validatableDocumentsToUpdate.isEmpty()) {
                            update validatableDocumentsToUpdate.values();
                        }

                        for (ValidatableDocument__c otherDoc : validatableDocumentQueries.listByValidationActivities(candidateActivitiesToClose.keySet())) {
                            if (!validatableDocumentsToUpdate.containsKey(otherDoc.Id) && otherDoc.FileMetadata__c == null && otherDoc.DocumentBundleItem__r.Mandatory__c) {
                                candidateActivitiesToClose.put(otherDoc.ValidationActivity__c, false);
                            }
                        }

                        List<wrts_prcgvr__Activity__c> activitiesToClose = new List<wrts_prcgvr__Activity__c>();
                        for (Id activityId : candidateActivitiesToClose.keySet()) {
                            if (candidateActivitiesToClose.get(activityId) == true) {
                                activitiesToClose.add(new wrts_prcgvr__Activity__c(
                                    Id = activityId,
                                    wrts_prcgvr__Status__c = CONSTANTS.ACTIVITY_STATUS_COMPLETED
                                ));
                            }
                        }
                        if (!activitiesToClose.isEmpty()) {
                            update activitiesToClose;

                            List<Case> cases = MRO_QR_Case.getInstance().getCasesByDossierId(dossier.Id);
                            dossierSrv.checkAndApplyAutomaticTransitionToDossierAndCases(dossier, cases, CONSTANTS.DOCUMENT_VALIDATION_TAG, false, true, false);
                        }
                    } else {
                        Map<Id, wrts_prcgvr__Activity__c> activitiesToCancel = new Map<Id, wrts_prcgvr__Activity__c>();
                        for (ValidatableDocument__c vd : validatableDocuments) {
                            if (!activitiesToCancel.containsKey(vd.ValidationActivity__c)) {
                                activitiesToCancel.put(vd.ValidationActivity__c, new wrts_prcgvr__Activity__c(
                                    Id = vd.ValidationActivity__c,
                                    wrts_prcgvr__Status__c = CONSTANTS.ACTIVITY_STATUS_CANCELLED
                                ));
                            }
                        }
                        if (!activitiesToCancel.isEmpty()) {
                            update activitiesToCancel.values();
                        }

                        Map<String, Object> cancelResult = dossierSrv.cancelDossierAndCases(dossier, 'Customer request', customerNotes);
                        if (!String.isBlank((String) cancelResult.get('error'))) {
                            success = false;
                            message = (String) cancelResult.get('error');
                        }
                    }

                    if (success) {
                        dossier.ExternalAccessIdentifier__r.IsExpired__c = true;
                        update dossier.ExternalAccessIdentifier__r;
                    }
                }
            }
        }
        catch (Exception e) {
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
            success = false;
            message = e.getMessage();
        }
        return new Map<String, Object>{
            'success' => success,
            'message' => message
        };
    }
}