/**
 * Created by tommasobolis on 30/11/2020.
 */

public with sharing class MRO_QR_Configuration {

    public static MRO_QR_Configuration getInstance() {

        return (MRO_QR_Configuration)ServiceLocator.getInstance(MRO_QR_Configuration.class);
    }

    public NE__Order__c getConfigurationByOpportunityId(Id opportunityId) {

        String queryString =
                'SELECT '+
                        MRO_UTL_SObject.getQueryStringAllFields(Schema.NE__Order__c.getSObjectType(),
                                null) + ' ' +
                'FROM NE__Order__c ' +
                'WHERE NE__OptyId__c = \'' + opportunityId + '\'';
        List<NE__Order__c> orderList = (List<NE__Order__c>)Database.query(queryString);
        if (orderList.size() > 0) {
            return orderList.get(0);
        }
        return null;
    }
}