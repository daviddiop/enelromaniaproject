@isTest
public with sharing class CustomerInteractionCntTst {
    @testSetup
    static void setup() {
        List<Account> listAccount = new list<Account>();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        listAccount.add(TestDataFactory.account().personAccount().build());
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        Individual individual = TestDataFactory.individual().createIndividual().build();
        insert individual;

        List<Interaction__c> listinteractions = new list<Interaction__c>();
        listinteractions.add( TestDataFactory.interaction().createInteraction().build());
        Interaction__c interaction2 = TestDataFactory.interaction().createInteraction().build();
        interaction2.Status__c = 'Closed';
        listinteractions.add(interaction2);
        insert listinteractions;

        Contact contact = TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;

        CustomerInteraction__c personCustomerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(listinteractions[0].Id,listAccount[0].Id,contact.Id).build();
        contact.FirstName = 'updateContact';
        update contact;
        CustomerInteraction__c bsnCustomerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(listinteractions[1].Id,listAccount[1].Id,contact.Id).build();

        List<CustomerInteraction__c> customerInteractions = new List<CustomerInteraction__c>();
        customerInteractions.add(personCustomerInteraction);
        customerInteractions.add(bsnCustomerInteraction);
        insert customerInteractions;

        AccountContactRelation accountContactRelation = TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id,listAccount[0].Id).build();
        AccountContactRelation accountContactRelation2 = TestDataFactory.AccountContactRelation().setRoles('Utente business').build();
        accountContactRelation.Roles = accountContactRelation2.Roles;
        insert accountContactRelation;
    }
    @isTest
    static void initDataTest() {
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        Test.startTest();
        Object response=TestUtils.exec('CustomerInteractionCnt','initData',new Map<String, String>{'interactionId'=>interaction.Id},true);
        if(response instanceof Map<String,Object>){
            Map<String,Object> createdOptionList=(Map<String,Object>)response;
            System.assert(createdOptionList.get('customerInteractionList')!=null);
        }
        Test.stopTest();
    }
    @isTest
    static void updateRoleTest() {
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        AccountContactRelation accountContactRelation2 = TestDataFactory.AccountContactRelation().setRoles('Utente business').build();
        accountContactRelation.Roles = accountContactRelation2.Roles;
        update accountContactRelation;
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO CustomerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        CustomerInteractionDTO.relationId = accountContactRelation.Id;
        CustomerInteractionDTO.role = accountContactRelation.Roles ;
        CustomerInteractionDTO.interactionId = interaction.Id;
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Object response=TestUtils.exec('CustomerInteractionCnt','updateRole',jsonInput,true);
        Test.startTest();
        System.assertNotEquals(response , new List<CustomerInteractionService.CustomerInteractionDTO>());
        AccountContactRelation accountContactRelation3 = [
            SELECT Id,Roles
            FROM AccountContactRelation
            WHERE Id =:accountContactRelation.Id
        ];
        System.assertEquals(accountContactRelation3.Roles,'Utente business');
        Test.stopTest();
    }
    @isTest
    static void updateRoleTestIsBlankRelationId() {
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        AccountContactRelation accountContactRelation2 = TestDataFactory.AccountContactRelation().setRoles('Utente business').build();
        accountContactRelation.Roles = accountContactRelation2.Roles;
        update accountContactRelation;
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.relationId = '';
        customerInteractionDTO.role = accountContactRelation.Roles ;
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Test.startTest();
        try {
            Object response=TestUtils.exec('CustomerInteractionCnt','updateRole',jsonInput,false);
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
        
    }
    @isTest
    static void updateRoleTestIsBlankRole() {
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.relationId = accountContactRelation.Id;
        customerInteractionDTO.role = '';
        
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Test.startTest();
        try {
            Object response=TestUtils.exec('CustomerInteractionCnt','updateRole',jsonInput,false);
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
        
    }
    
    @isTest
    static void deleteCustomerInteractionTest() {
        List<CustomerInteraction__c> customerInteractionList1 = [
            SELECT Id
            FROM CustomerInteraction__c
        ];
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.relationId = accountContactRelation.Id;
        customerInteractionDTO.role = accountContactRelation.Roles;
        customerInteractionDTO.interactionId = interaction.Id;
        customerInteractionDTO.id = customerInteractionList1[0].Id;
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Object response=TestUtils.exec('CustomerInteractionCnt','deleteCustomerInteraction',jsonInput,true);
        List<CustomerInteraction__c> customerInteractionList2 = [
            SELECT Id
            FROM CustomerInteraction__c
        ];
        Test.startTest();
        system.assertEquals(customerInteractionList2.size() , customerInteractionList1.size()-1 );
        Test.stopTest();
    }
    @isTest
    static void deleteCustomerInteractionTestIsBlankId() {
        AccountContactRelation accountContactRelation = [
            SELECT Id,Roles
            FROM AccountContactRelation
            LIMIT 1
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];
        CustomerInteractionService.CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionService.CustomerInteractionDTO();
        customerInteractionDTO.Id = '';
        customerInteractionDTO.relationId = accountContactRelation.Id;
        customerInteractionDTO.role = accountContactRelation.Roles;
        customerInteractionDTO.interactionId = interaction.Id;
        String customerInteractionString = JSON.serialize(CustomerInteractionDTO);
        Map<String, Object> jsonInput = new Map<String, Object>{
            'customerInteraction' => customerInteractionString
        };
        Test.startTest();
        try {
            Object response=TestUtils.exec('CustomerInteractionCnt','deleteCustomerInteraction',jsonInput,false);
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
        
    }
}