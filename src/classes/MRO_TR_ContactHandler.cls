/**
 * Created by napoli on 14/11/2019.
 */
/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   nov 14, 2019
* @desc
* @history
*/
global with sharing class MRO_TR_ContactHandler implements TriggerManager.ISObjectTriggerHandler {

    global void afterDelete() {}

    global void afterInsert() {}

    global void afterUndelete() {}

    global void afterUpdate() {}

    global void beforeDelete() {}

    global void beforeInsert() {
        this.checkCNP(Trigger.new, null);
        System.debug('beforeInsert trigger Contact 1 - Queries used in this apex code so far: ' + Limits.getQueries());
    }

    global void beforeUpdate() {
        this.checkCNP(Trigger.new, (Map<Id, Contact>)Trigger.oldMap);
        System.debug('beforeUpdate trigger Contact 1 - Queries used in this apex code so far: ' + Limits.getQueries());
    }

    private void checkCNP(List<Contact> contactListNew, Map<Id, Contact> contactMapOld) {

        for (Contact newContact : contactListNew) {
            if (!newContact.IsPersonAccount && newContact.NationalIdentityNumber__c != null && newContact.ForeignCitizen__c != true &&
                    (contactMapOld == null ||
                        (newContact.NationalIdentityNumber__c != contactMapOld.get(newContact.Id).NationalIdentityNumber__c) ||
                        (newContact.ForeignCitizen__c != contactMapOld.get(newContact.Id).ForeignCitizen__c))) {
                MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(newContact.NationalIdentityNumber__c);
                if(result.outCome == false) newContact.NationalIdentityNumber__c.addError(String.format(Label.InvalidCnp, new List<String>{result.errorCode, newContact.NationalIdentityNumber__c}));
            }
        }
    }
}