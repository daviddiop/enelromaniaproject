/**
 * Created by goudiaby on 31/12/2019.
 * @description Utils Class for Transitions
 */

public with sharing class MRO_UTL_Transitions {
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    /**
     * @description get class instance
     *
     * @return
     */
    public static MRO_UTL_Transitions getInstance() {
        return (MRO_UTL_Transitions) ServiceLocator.getInstance(MRO_UTL_Transitions.class);
    }
    /**
     *  Retrieve transitions available for a specific record
     *
     * @param object: SObject record to evaluate;
     * @param tags: String that specifies the wrts_prcgvr__Tag__c of the Phase Transition, it is used to filter the available transition;
     * @param type: String that specifies the phase transitions type to filter (A or M);
     * @param context : String that specifies a context value
     *
     * @return List<wrts_prcgvr__PhaseTransition__c>
     */
    public List<wrts_prcgvr__PhaseTransition__c> getTransitions(SObject myObject, String tags, String type) {
        wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration phaseManagerIntegration = (wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration');
        Map<String, Object> params = new Map<String, Object>{
                'object' => myObject,
                'type' => type,
                'tags' => tags
        };
        return (List<wrts_prcgvr__PhaseTransition__c>) phaseManagerIntegration.getTransitions(params);
    }

    /**
     *  Retrieve transitions available for a specific record
     *
     * @param Map Object: Map to query transitions;
     *
     * @return List<wrts_prcgvr__PhaseTransition__c>
     */
    public List<wrts_prcgvr__PhaseTransition__c> getTransitions(Map<String,Object> params) {
        wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration phaseManagerIntegration = (wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration');
        return (List<wrts_prcgvr__PhaseTransition__c>) phaseManagerIntegration.getTransitions(params);
    }

    /**
     * Applies a specific phase transition on a single record,
     *
     * @param object: SObject record to evaluate;
     * @param transition: the wrts_prcgvr__PhaseTransition__c record to apply
     * @param context : String that specifies a context value
     *
     * @return Map<String,Object>
     *          isSuccess: a Boolean, true when the procedure completed successfully;
 *              isDependant: a Boolean, true if the procedure found dependencies that you must handle; the dependency result to handle is provided in the inspectResult entry;
     *          inspectResult: result of the inspect invocation of the Dependency Manager;
     *          message: a String that contains the error message, if any;
     *          stackTraceString: a String that contains the error stacktrace, if any.
     */
    public Map<String,Object> applyTransitions(SObject myObject, wrts_prcgvr__PhaseTransition__c transition) {
        return this.applyTransitions(myObject, transition, false);
    }

    public Map<String,Object> applyTransitions(SObject myObject, wrts_prcgvr__PhaseTransition__c transition, Boolean handleDependencies) {
        wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration phaseManagerIntegration = (wrts_prcgvr.Interfaces_1_2.IPhaseManagerIntegration) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerIntegration');
        Map<String, Object> params = new Map<String, Object>{
                'object' => myObject,
                'transition' => transition
        };
        Map<String, Object> transitionResult = (Map<String, Object>) phaseManagerIntegration.applyTransition(params);
        if (transitionResult != null && handleDependencies) {
            this.handleDependencies(transitionResult);
        }
        return transitionResult;
    }

    public Map<String,Object> checkAndApplyAutomaticTransitionWithTag(SObject record, String tag) {
        return this.checkAndApplyAutomaticTransitionWithTag(record, tag, false, false);
    }

    public Map<String,Object> checkAndApplyAutomaticTransitionWithTag(SObject record, String tag, Boolean checkMandatoryActivities, Boolean handleDependencies) {
        List<wrts_prcgvr__PhaseTransition__c> transitions = this.getTransitions(record, tag, CONSTANTS.PHASE_AUTOMATIC_TRANSITION);

        if (transitions.size() > 1 && tag == '') {
            List<wrts_prcgvr__PhaseTransition__c> nullTransitions = new List<wrts_prcgvr__PhaseTransition__c>();
            for (wrts_prcgvr__PhaseTransition__c transition : transitions) {
                if (transition.wrts_prcgvr__Tags__c == null) {
                    nullTransitions.add(transition);
                }
                transitions = nullTransitions;
            }
        }

        if (transitions.size() == 1) {
            if (checkMandatoryActivities) {
                Map<Id, List<wrts_prcgvr__Activity__c>> mandatoryActivities = MRO_QR_Activity.getInstance().listOpenMandatoryActivitiesByParentIds(new Set<Id>{record.Id});
                System.debug('Mandatory open activities: '+mandatoryActivities);
                if (mandatoryActivities.containsKey(record.Id)) {
                    return null;
                }
            }
            Map<String, Object> transitionResult = (Map<String, Object>) this.applyTransitions(record, transitions[0], handleDependencies);
            return transitionResult;
        } else {
            return null;
        }
    }

    public void handleDependencies(Map<String, Object> transitionResult) {
        if (transitionResult.get('isDependant') == true) {
            wrts_prcgvr.DependencyUtils_1_0.InspectResult inspectResult = (wrts_prcgvr.DependencyUtils_1_0.InspectResult) transitionResult.get('inspectResult');
            inspectResult.handle();
        }
    }

    public wrts_prcgvr.DependencyUtils_1_0.InspectResult inspectAndHandleDependencies(SObject record) {
        wrts_prcgvr.Interfaces_1_0.IDependencyIntegration depUtils = (wrts_prcgvr.Interfaces_1_0.IDependencyIntegration)wrts_prcgvr.VersionManager.newClassInstance('DependencyUtils');
        wrts_prcgvr.DependencyUtils_1_0.InspectResult inspectResult = (wrts_prcgvr.DependencyUtils_1_0.InspectResult) depUtils.inspect(new Map<String,Object>{ 'sObject' => record });
        inspectResult.handle();
        return inspectResult;
    }

    public with sharing class PhaseTransitionDTO {
        @AuraEnabled
        public String objectId {get; set;}
        @AuraEnabled
        public String transitionType {get; set;}
        @AuraEnabled
        public String transitionTags {get; set;}
        @AuraEnabled
        public Map<String, Object> fieldsUpdateMap {get; set;}
    }

}