/**
 * Created by tommasobolis on 01/06/2020.
 */

public with sharing class MRO_QR_DocumentBundle {

    private static MRO_QR_DocumentBundle singleton;

    public static MRO_QR_DocumentBundle getInstance() {

        if (singleton == null) {

            singleton = new MRO_QR_DocumentBundle();
        }
        return singleton;
    }

    /**
     * Return document bundle by code
     *
     * @param documentBundleCode the document bundle code.
     *
     * @return document bundle by code
     */
    public DocumentBundle__c getDocumentBundleByCode(String documentBundleCode) {

        return
            [SELECT Id, Name, DocumentBundleCode__c
             FROM DocumentBundle__c
             WHERE DocumentBundleCode__c = :documentBundleCode];
    }

    public List<DocumentBundle__c> findByIds (Set<Id> documentBundleIdList) {

        return [SELECT Id, Name, DocumentBundleCode__c, Recipient__c
                FROM DocumentBundle__c
                WHERE Id IN :documentBundleIdList];
    }
}