/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 12, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_FileMetadata {
    public static MRO_QR_FileMetadata getInstance() {
        return (MRO_QR_FileMetadata) ServiceLocator.getInstance(MRO_QR_FileMetadata.class);
    }

    public FileMetadata__c findById(String fileMetadataId) {
        List<FileMetadata__c> fileMetadataList = [
                SELECT Id, Dossier__c, Dossier__r.Name, Case__c, Name,CreatedDate
                FROM FileMetadata__c
                WHERE Id = :fileMetadataId
                LIMIT 1
        ];
        FileMetadata__c fileMetadata = fileMetadataList.isEmpty() ? null : fileMetadataList.get(0);
        return fileMetadata;
    }

    public List<FileMetadata__c> listRecent(Integer sizeLimit, Datetime startDate) {
        return [
            SELECT Id, Title__c, DocumentId__c, CreatedDate
            FROM FileMetadata__c
            WHERE DocumentId__c != null
                AND CreatedDate > :startDate
            ORDER BY CreatedDate DESC
            LIMIT :sizeLimit
        ];
    }

    public FileMetadata__c findByTitle(String title) {
        List<FileMetadata__c> fileMetadataList = [
            SELECT Id, Name
            FROM FileMetadata__c
            WHERE Title__c = :title
            LIMIT 1
        ];
        return fileMetadataList.isEmpty() ? null : fileMetadataList.get(0);
    }

    public FileMetadata__c findPrintedDocumentByTypeCode(String typeCode, Id parentId) {
        List<FileMetadata__c> fmList = [SELECT Id
                                        FROM FileMetadata__c
                                        WHERE (Dossier__c = :parentId OR Case__c = :parentId)
                                            AND PrintAction__c != NULL
                                            AND ExternalId__c != NULL
                                            AND DocumentType__r.DocumentTypeCode__c = :typeCode
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        return fmList.isEmpty() ? null : fmList[0];
    }

    public Map<String, FileMetadata__c> mapPrintedDocumentByTypeCodes(Set<String> typeCodes, Id parentId) {
        List<FileMetadata__c> fmList = [SELECT Id, DocumentType__r.DocumentTypeCode__c
                                        FROM FileMetadata__c
                                        WHERE (Dossier__c = :parentId OR Case__c = :parentId)
                                            AND PrintAction__c != NULL
                                            AND ExternalId__c != NULL
                                            AND DocumentType__r.DocumentTypeCode__c IN :typeCodes
                                        ORDER BY CreatedDate DESC];
        Map<String, FileMetadata__c> result = new Map<String, FileMetadata__c>();
        for (FileMetadata__c fm : fmList) {
            result.put(fm.DocumentType__r.DocumentTypeCode__c, fm);
        }
        return result;
    }

    public Map<Id, Map<String, List<FileMetadata__c>>> listExistingDocumentsByCaseIds(Set<Id> caseIds) {
        List<FileMetadata__c> existingDocuments = [SELECT Id, Case__c, DocumentType__r.DocumentTypeCode__c, ExternalId__c
                                                   FROM FileMetadata__c
                                                   WHERE Case__c IN :caseIds AND DocumentType__r.DocumentTypeCode__c != NULL
                                                        AND (ExternalId__c != NULL OR DiscoFileId__c != NULL)
                                                   ORDER BY CreatedDate];
        Map<Id, Map<String, List<FileMetadata__c>>> result = new Map<Id, Map<String, List<FileMetadata__c>>>();
        for (FileMetadata__c fm : existingDocuments) {
            if (result.containsKey(fm.Case__c)) {
                if (result.get(fm.Case__c).containsKey(fm.DocumentType__r.DocumentTypeCode__c)) {
                    result.get(fm.Case__c).get(fm.DocumentType__r.DocumentTypeCode__c).add(fm);
                } else {
                    result.get(fm.Case__c).put(fm.DocumentType__r.DocumentTypeCode__c, new List<FileMetadata__c>{fm});
                }
            } else {
                result.put(fm.Case__c, new Map<String, List<FileMetadata__c>>{ fm.DocumentType__r.DocumentTypeCode__c => new List<FileMetadata__c>{fm} });
            }
        }
        return result;
    }
    public List<DocumentType__c> getDocumentTypes(String codeDocument){
        return [
            SELECT Id, Name, DocumentTypeCode__c  FROM DocumentType__c WHERE DocumentTypeCode__c =: codeDocument
        ];
    }

    public Map<Id, Map<String, List<FileMetadata__c>>> listExistingDocumentsByDossierIds(Set<Id> dossierIds) {
        List<FileMetadata__c> existingDocuments = [SELECT Id, Dossier__c, DocumentType__r.DocumentTypeCode__c, ExternalId__c
                                                   FROM FileMetadata__c
                                                   WHERE Dossier__c IN :dossierIds AND DocumentType__r.DocumentTypeCode__c != NULL
                                                        AND (ExternalId__c != NULL OR DiscoFileId__c != NULL) AND Case__c = NULL
                                                   ORDER BY CreatedDate];
        Map<Id, Map<String, List<FileMetadata__c>>> result = new Map<Id, Map<String, List<FileMetadata__c>>>();
        for (FileMetadata__c fm : existingDocuments) {
            if (result.containsKey(fm.Dossier__c)) {
                if (result.get(fm.Dossier__c).containsKey(fm.DocumentType__r.DocumentTypeCode__c)) {
                    result.get(fm.Dossier__c).get(fm.DocumentType__r.DocumentTypeCode__c).add(fm);
                } else {
                    result.get(fm.Dossier__c).put(fm.DocumentType__r.DocumentTypeCode__c, new List<FileMetadata__c>{fm});
                }
            } else {
                result.put(fm.Dossier__c, new Map<String, List<FileMetadata__c>>{ fm.DocumentType__r.DocumentTypeCode__c => new List<FileMetadata__c>{fm} });
            }
        }
        return result;
    }

    public List<FileMetadata__c> listByDossierId(Id dossierId) {
        return [SELECT Id, DocumentType__c, DocumentType__r.DocumentTypeCode__c, ExternalId__c, DiscoFileId__c, DocumentId__c,
                       Title__c, FileName__c, StatusCode__c, StatusDetail__c, DocumentURL__c, ContentType__c
                FROM FileMetadata__c
                WHERE Dossier__c = :dossierId
                AND (ExternalId__c != NULL OR DiscoFileId__c != NULL OR DocumentId__c != NULL)
        ];
    }

    public Map<String, FileMetadata__c> mapDocumentTypeCodes(Id parentId) {
        List<FileMetadata__c> fmList = [
                SELECT Id, DocumentType__r.DocumentTypeCode__c,PrintAction__c
                FROM FileMetadata__c
                WHERE (Dossier__c = :parentId OR Case__c = :parentId)
                AND ExternalId__c != NULL
                ORDER BY CreatedDate DESC
        ];
        Map<String, FileMetadata__c> result = new Map<String, FileMetadata__c>();
        for (FileMetadata__c fm : fmList) {
            if (fm.DocumentType__c != null && String.isNotBlank(fm.DocumentType__r.DocumentTypeCode__c)){
                result.put(fm.DocumentType__r.DocumentTypeCode__c, fm);
            }
        }
        return result;
    }

    //[ENLCRO-1811]
    public List<FileMetadata__c> findByExternalId (List<String> externalIdToFind) {

        return [
                SELECT Id, Title__c, DocumentId__c, CreatedDate, DocumentType__c, ExternalId__c
                FROM FileMetadata__c
                WHERE ExternalId__c IN :externalIdToFind
        ];
    }
    public List<FileMetadata__c> findByTitles(List<String> titleList) {
        return [
                SELECT Id, Name, Title__c, DocumentId__c
                FROM FileMetadata__c
                WHERE Title__c IN :titleList
        ];
    }
}