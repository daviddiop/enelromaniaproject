/**
 * Created by DAVID DIOP on 21.10.2020.
 */

public with sharing class MRO_UTL_MyEnelServices {
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    public static MRO_UTL_MyEnelServices getInstance() {
        return (MRO_UTL_MyEnelServices) ServiceLocator.getInstance(MRO_UTL_MyEnelServices.class);
    }

    public static final Set<String> ALLOWED_FLUX = new Set<String>{
            'ExtraCRMFlowRequest',
            'MyEnelFlowRequest',
            'SyncGDPR',
            'RegisterGDPRContest',
            'CfeInitiationStatus',
            'Contact'
    };
    public static final Map<String, Map<String, String>> URL_TIPCONTACT_MAP  = new Map<String, Map<String, String>>{
            'G001' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'A031' => new Map<String, String>{
                    'Complaint' => 'CustomerCare'
            },
            'G002' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G003' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G004' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G005' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'B012' => new Map<String, String>{
                    'Complaint' => 'CustomerCare'
            },
            'G006' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G007' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G008' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G009' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G010' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G011' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G012' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G013' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G014' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G015' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G016' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G017' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G018' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G019' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G020' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G021' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G022' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G023' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G024' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G025' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G026' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G027' => new Map<String, String>{
                    'Generic' => 'BackOffice'
            },
            'G028' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G029' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G030' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G031' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G032' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G033' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G034' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G035' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G036' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G037' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G038' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G039' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G040' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G041' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'G042' => new Map<String, String>{
                    'Generic' => 'CustomerCare'
            },
            'A011' => new Map<String, String>{
                    'Complaint' => 'CustomerCare'
            },
            'S02' => new Map<String, String>{
                    'DemonstratedPayment' => 'CustomerCare'
            }
    };

    public ContractAccount__c getContractAccount( String billingAccountNumber){
        List<ContractAccount__c> contractAccountList = [
                SELECT Id,BillingAccountNumber__c
                FROM ContractAccount__c
                WHERE BillingAccountNumber__c  = :billingAccountNumber
        ];

        return contractAccountList.isEmpty() ? null : contractAccountList.get(0);
    }

    public List<Supply__c> getSupplyList(String codPlata) {
        List<Supply__c> suppliesList = [
                SELECT Id,ContractAccount__c,CompanyDivision__c,Status__c,ContractAccount__r.BillingAccountNumber__c,ContractAccount__r.BillingProfile__c,BillingProfile__c,Account__c,
                        Account__r.IntegrationKey__c, Account__r.NationalIdentityNumber__pc
                FROM Supply__c
                WHERE ContractAccount__r.BillingAccountNumber__c = :codPlata AND Status__c = 'ACTIVE'
                LIMIT 1
        ];

        return suppliesList;
    }
    public List<BillingProfile__c> getBillingByIds(Set<String> setBillingProfileIds){
        List<BillingProfile__c> billingProfilesList = [
                SELECT Id,BillingAddress__c,BillingAddressKey__c,BillingAddressNormalized__c,BillingApartment__c,BillingBlock__c,
                        BillingBuilding__c,BillingCity__c,BillingCountry__c,BillingFloor__c,
                        BillingLocality__c,BillingPostalCode__c,BillingProvince__c,BillingStreetId__c,
                        BillingStreetName__c,BillingStreetNumber__c,BillingStreetNumberExtn__c,BillingStreetType__c
                FROM BillingProfile__c
                WHERE Id IN: setBillingProfileIds LIMIT 1
        ];
        return billingProfilesList;
    }

    public Case getCaseByExternalId(String externalId){
        return [
                SELECT Id,IntegrationKey__c
                FROM  Case
                WHERE IntegrationKey__c = :externalId
                LIMIT 1
        ];
    }
    public Supply__c getSupplyWithEnelTel(String enelTel ) {
        return [SELECT Id, CompanyDivision__c,RecordTypeId, RecordType.Name, RecordType.DeveloperName,ServicePoint__r.ENELTEL__c
        FROM Supply__c
        WHERE ServicePoint__r.ENELTEL__c = :enelTel AND  CompanyDivision__c != NULL
        LIMIT 1];
    }
    public NE__Product__c getProductByCode( String productCode ) {
        List<NE__Product__c> productList = [
                SELECT Id,Name,ProductCode__c
                FROM NE__Product__c
                WHERE ProductCode__c  = :productCode
        ];
        return productList.isEmpty() ? null : productList.get(0);
    }
    public  Map<String, Object> generateCaseWithContactRequest(Dossier__c dossier, Account account,Case caseRecord, String recordTypeId,Supply__c supply){
        Map<String, Object> result = new Map<String, Object>();
        caseRecord.Dossier__c = dossier.Id;
        caseRecord.AccountId = account.Id;
        if(supply.CompanyDivision__c != null) {
            caseRecord.CompanyDivision__c = supply.CompanyDivision__c;
        }
        //caseRecord.SupplyType__c = supply.RecordType.DeveloperName;
        caseRecord.RecordTypeId = recordTypeId;
        caseRecord.Status = constantsSrv.CASE_STATUS_NEW;
        caseRecord.Channel__c = MRO_UTL_Constants.CHANNEL_MY_ENEL;
        caseRecord.Origin = MRO_UTL_Constants.ORIGIN_WEB;
        caseRecord.Phase__c = constantsSrv.CASE_START_PHASE;
        databaseSrv.insertSObject(caseRecord);
        result.put('caseRecord',caseRecord);
        return result;
    }
    public Map<String, Account> getListAccountByIntegrationKey(Set<String> integrationKeys) {
        Map<String, Account> mapAccountId = new Map<String, Account>();
        List<Account> accountList = [
                SELECT Id,IntegrationKey__c,ConsumerType__c
                FROM Account
                WHERE IntegrationKey__c IN :integrationKeys
        ];
        for (Account accountRecord : accountList) {
            mapAccountId.put(accountRecord.IntegrationKey__c, accountRecord);
        }
        return mapAccountId;
    }
}