/**
 * @author  Stefano Porcari
 * @since   Dec 24, 2019
 * @desc   Query class for Dossier__c object
 *
 */
public with sharing class MRO_QR_Dossier {
    public static MRO_QR_Dossier getInstance() {
        return new MRO_QR_Dossier();
    }

    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public Map<Id, Dossier__c> getByIds(Set<Id> dossierId) {
        return new Map<Id, Dossier__c>([
                SELECT
                        Id, Name, Status__c, RecordTypeId, RecordType.DeveloperName, Commodity__c,Comments__c,
                        Email__c, Phone__c, SendingChannel__c, RequestType__c, SubProcess__c, Phase__c, Origin__c, Channel__c,
                        Account__c, Account__r.IsPersonAccount, Account__r.PersonIndividualId, Account__r.PersonContactId, Account__r.Name,
                        Account__r.ResidentialAddress__c, Account__r.Email__c, Account__r.PersonEmail, Account__r.Phone, Account__r.PersonHomePhone,
                        CustomerInteraction__c, CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                        CustomerInteraction__r.Interaction__r.Interlocutor__c,Opportunity__c,
                        CustomerInteraction__r.Contact__c, CustomerInteraction__r.Contact__r.Email, CustomerInteraction__r.Contact__r.Phone,
                        CompanyDivision__c, CompanyDivision__r.Name,OwnerId,CompanyDivision__r.Code__c,
                        HasJustifyingDocuments__c,SelfReadingProvided__c, StartedBy__c, MailChannelType__c, CustomerInteraction__r.Contact__r.MobilePhone,
                        Account__r.PersonMobilePhone, SLAExpirationDate__c,Parent__c,CommercialProduct__c,AssignmentProvince__c,
                        IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c,RelatedDossier__r.Opportunity__r.ContractId, Opportunity__r.ContractAddition__c,
                        RelatedDossier__r.Opportunity__r.Contract.RelatedContract__c, Language__c, ContractAddition__c
                FROM Dossier__c
                WHERE Id IN :dossierId
        ]);
    }

    public Map<Id, Dossier__c> getByIdsForActivities(Set<Id> dossierId) {
        return new Map<Id, Dossier__c>([
                SELECT Id, OwnerId, Account__c, ExternalAccessIdentifier__c, Opportunity__c, Opportunity__r.ContractId, Status__c, RecordTypeId, RecordType.DeveloperName, RequestType__c, Commodity__c,
                        Language__c, CommercialProduct__c, CommercialProduct__r.AnnexATemplateCode__c, CommercialProduct__r.AnnexATemplateCodeVersion__c,
                        CommercialProduct__r.ContractTemplateCode__c, CommercialProduct__r.ContractTemplateCodeVersion__c,
                        CommercialProduct__r.OfferTemplateCode__c, CommercialProduct__r.OfferTemplateCodeVersion__c,CommercialProduct__r.AnnexABisTemplateCode__c,
                        CommercialProduct__r.Annex1TemplateCode__c,CommercialProduct__r.Annex2TemplateCode__c,CommercialProduct__r.Annex3TemplateCode__c,
                        SendingChannel__c, Email__c, Phone__c, MailChannelType__c, Origin__c, Channel__c, SubProcess__c, PrintBundleSelection__c,
                        Phase__c, SLAExpirationDate__c, Parent__c, CompanyDivision__c, CompanyDivision__r.Name, CompanyDivision__r.Code__c,
                        HasJustifyingDocuments__c, SelfReadingProvided__c, StartedBy__c, ContractAddition__c,
                        Account__r.IsPersonAccount, Account__r.RecordTypeId, Account__r.RecordType.DeveloperName, Account__r.Email__c, Account__r.PersonEmail,
                        Account__r.ResidentialAddressKey__c, Account__r.ResidentialAddressNormalized__c, Account__r.ResidentialApartment__c,
                        Account__r.ResidentialBlock__c, Account__r.ResidentialBuilding__c, Account__r.ResidentialCity__c, Account__r.ResidentialCountry__c,
                        Account__r.ResidentialFloor__c, Account__r.ResidentialLocality__c, Account__r.ResidentialPostalCode__c,
                        Account__r.ResidentialProvince__c, Account__r.ResidentialStreetId__c, Account__r.ResidentialStreetName__c,
                        Account__r.ResidentialStreetNumber__c, Account__r.ResidentialStreetNumberExtn__c, Account__r.ResidentialStreetType__c,
                        CustomerInteraction__r.Contact__r.Email,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c, (
                        SELECT Id, RecordTypeId, RecordType.DeveloperName, Phase__c, CancellationReason__c,
                                ContractAccount__c, ContractAccount__r.BillingProfile__c, ContractAccount__r.Market__c,
                                ContractAccount__r.BillingProfile__r.DeliveryChannel__c, ContractAccount__r.BillingProfile__r.Email__c, ContractAccount__r.BillingProfile__r.MobilePhone__c,
                                ContractAccount__r.BillingProfile__r.BillingAddressKey__c, ContractAccount__r.BillingProfile__r.BillingAddressNormalized__c,
                                ContractAccount__r.BillingProfile__r.BillingApartment__c, ContractAccount__r.BillingProfile__r.BillingBlock__c,
                                ContractAccount__r.BillingProfile__r.BillingBuilding__c, ContractAccount__r.BillingProfile__r.BillingCity__c,
                                ContractAccount__r.BillingProfile__r.BillingCountry__c, ContractAccount__r.BillingProfile__r.BillingFloor__c,
                                ContractAccount__r.BillingProfile__r.BillingLocality__c, ContractAccount__r.BillingProfile__r.BillingPostalCode__c,
                                ContractAccount__r.BillingProfile__r.BillingProvince__c, ContractAccount__r.BillingProfile__r.BillingStreetId__c,
                                ContractAccount__r.BillingProfile__r.BillingStreetName__c, ContractAccount__r.BillingProfile__r.BillingStreetNumber__c,
                                ContractAccount__r.BillingProfile__r.BillingStreetNumberExtn__c, ContractAccount__r.BillingProfile__r.BillingStreetType__c,
                                BillingProfile__c, BillingProfile__r.BillingAddressKey__c, BillingProfile__r.BillingAddressNormalized__c,
                                BillingProfile__r.BillingApartment__c, BillingProfile__r.BillingBlock__c, BillingProfile__r.BillingBuilding__c,
                                BillingProfile__r.BillingCity__c, BillingProfile__r.BillingCountry__c, BillingProfile__r.BillingFloor__c,
                                BillingProfile__r.BillingLocality__c, BillingProfile__r.BillingPostalCode__c, BillingProfile__r.BillingProvince__c,
                                BillingProfile__r.BillingStreetId__c, BillingProfile__r.BillingStreetName__c, BillingProfile__r.BillingStreetNumber__c,
                                BillingProfile__r.BillingStreetNumberExtn__c, BillingProfile__r.BillingStreetType__c,
                                Supply__c, Supply__r.InENELArea__c, Supply__r.Market__c, Supply__r.ServicePoint__r.Distributor__r.IsDisCoENEL__c,
                                Supply__r.Contract__c, Supply__r.ContractAccount__c, Supply__r.ContractAccount__r.Market__c,
                                Supply__r.ContractAccount__r.BillingProfile__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.DeliveryChannel__c, Supply__r.ContractAccount__r.BillingProfile__r.Email__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.MobilePhone__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingAddressKey__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingAddressNormalized__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingApartment__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingBlock__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingBuilding__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingCity__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingCountry__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingFloor__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingLocality__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingPostalCode__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingProvince__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetId__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetName__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetNumber__c,
                                Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetNumberExtn__c, Supply__r.ContractAccount__r.BillingProfile__r.BillingStreetType__c,
                                Distributor__c, Distributor__r.IsDisCoENEL__c, Market__c, IsDisCoENEL__c, NeedsExecutor__c, NeedsDesigner__c,
                                SupplyOperation__c, SupplyType__c, Contract__c
                        FROM Cases__r
                        LIMIT 1
                )
                FROM Dossier__c
                WHERE Id IN :dossierId
        ]);
    }

    /**
     * @author Baba Goudiaby
     * @description get dossier by Id
     * @created at 15/01/2019
     */
    // FF Activation/SwitchIn - Interface Check - Pack2
    public Dossier__c getById(String dossierId) {
        return getByIdList(new Set<Id>{
                (Id) dossierId
        })[0];
    }

    public List<Dossier__c> getByIdList(Set<Id> dossierIds) {
        return [
                SELECT Id, Name, Status__c, RecordTypeId, RecordType.DeveloperName, Commodity__c,Comments__c,
                        Email__c, Phone__c, SendingChannel__c, RequestType__c, SubProcess__c, Phase__c, Origin__c, Channel__c,
                        Account__c, Account__r.NationalIdentityNumber__pc,
                        Account__r.PrimaryContact__r.MobilePhone, Account__r.PrimaryContact__r.Phone, Account__r.PrimaryContact__c,
                        Account__r.Description, Account__r.IsPersonAccount, Account__r.PersonIndividualId, Account__r.PersonContactId, Account__r.Name,
                        Account__r.ResidentialAddress__c, Account__r.Email__c, Account__r.PersonEmail, Account__r.Phone, Account__r.PersonHomePhone,
                        CustomerInteraction__c, CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                        CustomerInteraction__r.Interaction__r.Interlocutor__c,Opportunity__c, ContractAddition__c,
                        CustomerInteraction__r.Contact__c, CustomerInteraction__r.Contact__r.Email, CustomerInteraction__r.Contact__r.Phone,
                        CompanyDivision__c, CompanyDivision__r.Name,OwnerId,CompanyDivision__r.Code__c,
                        HasJustifyingDocuments__c,SelfReadingProvided__c, StartedBy__c, MailChannelType__c, CustomerInteraction__r.Contact__r.MobilePhone,
                        Account__r.PersonMobilePhone, SLAExpirationDate__c,Parent__c,CommercialProduct__c,AssignmentProvince__c,
                        IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c,RelatedDossier__r.Opportunity__r.ContractId,
                        RelatedDossier__r.Opportunity__r.Contract.RelatedContract__c,ExternalAccessIdentifier__c,
                        PumaUrgency__c, FileNumber__c, EnelRegistrationDateAndNumber__c, Parent__r.RequestType__c, (
                        SELECT Id, ContractAccount__c,IsDisCoENEL__c
                        FROM Cases__r
                        LIMIT 1
                )
                FROM Dossier__c
                WHERE Id IN :dossierIds
        ];
    }

    public List<Dossier__c> getParentDossiers(List<Dossier__c> dossierList) {
        List<Dossier__c> parentDossiers = new List<Dossier__c>();

        for (Dossier__c regularDossier : dossierList) {
            if (String.isNotBlank(regularDossier.Parent__c)) {
                Dossier__c parentDossier = [
                        SELECT Id, Name, Status__c, RecordTypeId, RecordType.DeveloperName, Commodity__c, Parent__c, SLAExpirationDate__c,
                                Email__c, Phone__c, SendingChannel__c, RequestType__c, SubProcess__c, Phase__c, Origin__c, Channel__c,
                                Account__c, Account__r.IsPersonAccount, Account__r.PersonIndividualId, Account__r.PersonContactId, Account__r.Name,
                                Account__r.ResidentialAddress__c, Account__r.Email__c, Account__r.PersonEmail, Account__r.Phone, Account__r.PersonHomePhone,
                                CustomerInteraction__c, CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                                CustomerInteraction__r.Interaction__r.Interlocutor__c,
                                CustomerInteraction__r.Contact__c, CustomerInteraction__r.Contact__r.Email, CustomerInteraction__r.Contact__r.Phone,
                                //FF Account Situation #WP3-P2
                                CompanyDivision__c, CompanyDivision__r.Name,OwnerId, AssignmentProvince__c, Comments__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c
                        //FF Account Situation #WP3-P2
                        FROM Dossier__c
                        WHERE Id = :regularDossier.Parent__c
                ];
                parentDossiers.add(parentDossier);
            } else {
                continue;
            }
        }
        return parentDossiers;
    }

    public List<Dossier__c> getChildDossiers(List<Dossier__c> parentDossierList) {
        List<Dossier__c> childDossiers = new List<Dossier__c>();

        for (Dossier__c parentDossier : parentDossierList) {
            List<Dossier__c> childrenOfParent = [
                    SELECT Id, Name, Status__c, RecordTypeId, RecordType.DeveloperName, Commodity__c, Parent__c, SLAExpirationDate__c,
                            Email__c, Phone__c, SendingChannel__c, RequestType__c, SubProcess__c, Phase__c, Origin__c, Channel__c,
                            Account__c, Account__r.IsPersonAccount, Account__r.PersonIndividualId, Account__r.PersonContactId, Account__r.Name,
                            Account__r.ResidentialAddress__c, Account__r.Email__c, Account__r.PersonEmail, Account__r.Phone, Account__r.PersonHomePhone,
                            CustomerInteraction__c, CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                            CustomerInteraction__r.Interaction__r.Interlocutor__c,
                            CustomerInteraction__r.Contact__c, CustomerInteraction__r.Contact__r.Email, CustomerInteraction__r.Contact__r.Phone,
                            //FF Account Situation #WP3-P2
                            CompanyDivision__c, CompanyDivision__r.Name,OwnerId, AssignmentProvince__c, Comments__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c
                    //FF Account Situation #WP3-P2
                    FROM Dossier__c
                    WHERE Parent__c = :parentDossier.Id
            ];
            childDossiers.addAll(childrenOfParent);
        }
        return childDossiers;
    }

    public List<Dossier__c> getChildDossiers(String dossierId) {
        return [
                SELECT Id, Name, Status__c, RecordTypeId, RecordType.DeveloperName, Commodity__c, Parent__c, SLAExpirationDate__c,
                        Email__c, Phone__c, SendingChannel__c, RequestType__c, SubProcess__c, Phase__c, Origin__c, Channel__c,
                        Account__c, Account__r.IsPersonAccount, Account__r.PersonIndividualId, Account__r.PersonContactId, Account__r.Name,
                        Account__r.ResidentialAddress__c, Account__r.Email__c, Account__r.PersonEmail, Account__r.Phone, Account__r.PersonHomePhone,
                        CustomerInteraction__c, CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                        CustomerInteraction__r.Interaction__r.Interlocutor__c,
                        CustomerInteraction__r.Contact__c, CustomerInteraction__r.Contact__r.Email, CustomerInteraction__r.Contact__r.Phone,
                        //FF Account Situation #WP3-P2
                        CompanyDivision__c, CompanyDivision__r.Name,OwnerId, AssignmentProvince__c, Comments__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c
                //FF Account Situation #WP3-P2
                FROM Dossier__c
                WHERE Parent__c = :dossierId
        ];
    }

    public Dossier__c getByIdWithOpportunity(String dossierId) {
        return [
                SELECT Id, Name, CustomerInteraction__c,Origin__c,Channel__c,Commodity__c, SubProcess__c, ContractAddition__c,
                        CustomerInteraction__r.Interaction__r.Channel__c,CustomerInteraction__r.Interaction__r.Origin__c,
                        CompanyDivision__c, CompanyDivision__r.Name, Status__c, RecordTypeId, RequestType__c, Phase__c,
                        Opportunity__c, Opportunity__r.RequestedStartDate__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c
                FROM Dossier__c
                WHERE Id = :dossierId
        ];
    }
    // FF Activation/SwitchIn - Interface Check - Pack2
    public Map<Id, Dossier__c> getDossierByIds(Set<Id> dossierIds) {
        return new Map<Id, Dossier__c> ([
                SELECT Id, Name, CustomerInteraction__c,Origin__c,Channel__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c, ContractAddition__c,
                        CustomerInteraction__r.Interaction__r.Channel__c,CustomerInteraction__r.Interaction__r.Origin__c, CompanyDivision__c, CompanyDivision__r.Name, Status__c, RecordTypeId, Phase__c
                FROM Dossier__c
                WHERE Id IN :dossierIds
        ]);
    }
    public Map<Id, Dossier__c> listDossiersWithCases(Set<Id> dossierIds, Boolean onlyOpenCases) {
        String query = 'SELECT Id, Parent__c, Account__c, Opportunity__c, CompanyDivision__c, Origin__c, Channel__c, Status__c, RecordTypeId, RequestType__c, SubProcess__c, Commodity__c, ContractAddition__c, Phase__c, ' +
                '(SELECT Id, CaseNumber, RecordTypeId, SubProcess__c, Status, IsClosed, Phase__c, NeedsExecutor__c, NeedsDesigner__c, SupplyOperation__c FROM Cases__r';
        if (onlyOpenCases) {
            query += ' WHERE IsClosed = false';
        }
        query += ') FROM Dossier__c WHERE Id IN :dossierIds';
        return new Map<Id, Dossier__c>((List<Dossier__c>) Database.query(query));
    }
    // FF Activation/SwitchIn - Interface Check - Pack2
    public List<Dossier__c> listDossiersByOpportunity(Id opportunityId) {
        return [
                SELECT Id, Name, CustomerInteraction__c,Origin__c,Channel__c,Commodity__c,Subprocess__c,ContractAddition__c,
                        CustomerInteraction__r.Interaction__r.Channel__c,CustomerInteraction__r.Interaction__r.Origin__c,
                        CompanyDivision__c, CompanyDivision__r.Name, Status__c, RecordTypeId, RequestType__c, Phase__c,
                        SelfReadingProvided__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c
                FROM Dossier__c
                WHERE Opportunity__c = :opportunityId
                ORDER BY LastModifiedDate DESC
        ];
    }
    // FF Activation/SwitchIn - Interface Check - Pack2

    public Dossier__c getDraftActivationDossierByConnectionOpportunityId(Id opportunityId) {
        List<Dossier__c> dossiers = [
                SELECT Id, Name, CustomerInteraction__c,Origin__c,Channel__c,Commodity__c,Subprocess__c,ContractAddition__c,
                        CustomerInteraction__r.Interaction__r.Channel__c,CustomerInteraction__r.Interaction__r.Origin__c,
                        CompanyDivision__c, CompanyDivision__r.Name, Status__c, RecordTypeId, RequestType__c, Phase__c,
                        SelfReadingProvided__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c
                FROM Dossier__c
                WHERE Opportunity__c = :opportunityId AND RequestType__c = 'Activation'
                AND Status__c = :CONSTANTS.DOSSIER_STATUS_DRAFT
                ORDER BY LastModifiedDate DESC
                LIMIT 1
        ];
        if (!dossiers.isEmpty()) {
            return dossiers[0];
        }
        return null;
    }

    /**
     * @description Get Dossier by Token
     * @param tokenValue
     * @return Dossier
    */
    public Dossier__c getByToken(String tokenValue) {
        List<Dossier__c> dossierList = [
                SELECT Id,Name, Status__c, RecordTypeId, RecordType.DeveloperName, Commodity__c,Comments__c,
                        Email__c, Phone__c, SendingChannel__c, RequestType__c, SubProcess__c, Phase__c, Origin__c, Channel__c,
                        Account__c, Account__r.IsPersonAccount, Account__r.PersonIndividualId, Account__r.PersonContactId, Account__r.Name,
                        Account__r.ResidentialAddress__c, Account__r.Email__c, Account__r.PersonEmail, Account__r.Phone, Account__r.PersonHomePhone,
                        CustomerInteraction__c, CustomerInteraction__r.Interaction__r.Channel__c, CustomerInteraction__r.Interaction__r.Origin__c,
                        CustomerInteraction__r.Interaction__r.Interlocutor__c,Opportunity__c,ContractAddition__c,
                        CustomerInteraction__r.Contact__c, CustomerInteraction__r.Contact__r.Email, CustomerInteraction__r.Contact__r.Phone,
                        CompanyDivision__c, CompanyDivision__r.Name,OwnerId,CompanyDivision__r.Code__c,
                        HasJustifyingDocuments__c,SelfReadingProvided__c, StartedBy__c, MailChannelType__c, CustomerInteraction__r.Contact__r.MobilePhone,
                        Account__r.PersonMobilePhone, SLAExpirationDate__c,Parent__c,CommercialProduct__c,AssignmentProvince__c,
                        IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c,RelatedDossier__r.Opportunity__r.ContractId,
                        RelatedDossier__r.Opportunity__r.Contract.RelatedContract__c, Language__c,
                        ExternalAccessIdentifier__c,ExternalAccessIdentifier__r.IsValid__c,ExternalAccessIdentifier__r.IsExpired__c,
                        Opportunity__r.Contract.Status, Opportunity__r.Contract.CustomerSignedDate
                FROM Dossier__c
                WHERE ExternalAccessIdentifier__r.Value__c = :tokenValue
        ];
        if (dossierList.isEmpty()) {
            return null;
        }
        return dossierList.get(0);
    }

    public Dossier__c findActiveByOpportunityId(Id opportunityId) {
        List<Dossier__c> result = [
                SELECT Id, Name, CustomerInteraction__c,Origin__c,Channel__c,Commodity__c,Subprocess__c,ContractAddition__c,
                        CustomerInteraction__r.Interaction__r.Channel__c,CustomerInteraction__r.Interaction__r.Origin__c,
                        CompanyDivision__c, CompanyDivision__r.Name, Status__c, RecordTypeId, RequestType__c, Phase__c,
                        SelfReadingProvided__c, StartedBy__c,IsDual__c,RelatedDossier__c,RelatedDossier__r.Commodity__c
                FROM Dossier__c
                WHERE Opportunity__c = :opportunityId
                AND Status__c NOT IN ('Closed', 'Canceled')
        ];

        return (result.size() != 0) ? result.get(0) : null;
    }

    public Dossier__c getDossierByActivityId(Id activityId) {

        List<Dossier__c> result = [
                SELECT
                        Id, Name, Commodity__c,Origin__c,Channel__c,RequestType__c, Account__r.IsPersonAccount, (SELECT Supply__r.Market__c FROM Cases__r)
                FROM
                        Dossier__c
                WHERE
                        Id IN (SELECT Dossier__c FROM wrts_prcgvr__Activity__c WHERE Id = :activityId)
        ];

        return (result.size() != 0) ? result.get(0) : null;
    }
    public List<Dossier__c> getListByIds(Set<Id> dosssIds) {
        List<Dossier__c> dossierList = [SELECT Id,ExternalAccessIdentifier__c, Parent__c, Parent__r.SLAExpirationDate__c FROM Dossier__c WHERE Id IN :dosssIds];
        return dossierList;
    }
    /* Author: simon.matei@accenture.com 02122020
       Date: 02/12/2020
       Description: query 
    */
    public List<Dossier__c> getDossierbyRelated(Id dossier, Id relatedDossier){
        return [SELECT Id, Opportunity__r.ContractId
                FROM Dossier__c  
                WHERE Id = :dossier OR Id = :relatedDossier];
    }
}