/**
 * @author  Luca Ravicing
 * @since   Mar 25, 2020
 * @desc   Apex Criteria class for Case
 *
 */

global with sharing class MRO_CRT_Case implements wrts_prcgvr.Interfaces_1_2.IApexCriteria {
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();
    private static MRO_UTL_Constants constants = MRO_UTL_Constants.getAllConstants();


    global Boolean evaluate(Object param) {
        Map<String, Object> paramsMap = (Map<String, Object>) param;
        Case myCase = (Case) paramsMap.get('record');
        String method = (String) paramsMap.get('method');
        switch on method {
            when 'NotActiveContract' {
                return !this.hasActiveContracts(myCase);
            }
            when 'ActiveContract' {
                return this.hasActiveContracts(myCase);
            }
            when 'canResubmitCallout' {
                return this.canResubmitCallout(myCase);
            }
            when 'isCreditManagementQueue' {
                return this.isCreditManagementQueue(myCase);
            }
            when 'isNotCreditManagementQueue' {
                return !this.isCreditManagementQueue(myCase);
            }
            when 'isCustomerCareQueue' {
                return this.isCustomerCareQueue(myCase);
            }
            when 'isNotCustomerCareQueue' {
                return !this.isCustomerCareQueue(myCase);
            }
            when 'isDossierNotDraft' {
                return !this.isDossierDraft(myCase);
            }
            when 'isDossierDraft' {
                return this.isDossierDraft(myCase);
            }
            when 'notSMSDeliveryChannel' {
                return this.notSMSDeliveryChannel(myCase);
            }
            when 'accountSituationMetadataExist' {
                return this.accountSituationMetadataExist(myCase);
            }
            when 'noAccountSituationMetadataExist' {
                return !this.accountSituationMetadataExist(myCase);
            }
            when 'isVoltageLevelChanged' {
                return this.isVoltageLevelChanged(myCase);
            }
            when 'isVoltageLevelUnchanged' {
                return !this.isVoltageLevelChanged(myCase);
            }
            when else {
                throw new WrtsException('Unrecognized method: '+method);
            }
        }
    }

    private Boolean hasActiveContracts(Case caseRecord) {
        Id accountId= caseRecord.AccountId;
        List<Contract> activeContracts= contractQuery.getActiveContractsByAccount(accountId);
        return !activeContracts.isEmpty();
    }

    private Boolean canResubmitCallout(Case caseRecord) {
        if (FeatureManagement.checkPermission('MRO_ResubmitCallouts')) {
            Map<String, Object> params = new Map<String, Object>{
                    'triggerNew' => new List<Case>{caseRecord}
            };
            wrts_prcgvr.Interfaces_1_2.IPhaseManagerUtils pmUtils = (wrts_prcgvr.Interfaces_1_2.IPhaseManagerUtils) wrts_prcgvr.VersionManager.newClassInstance('PhaseManagerUtils');
            Map<SObject, wrts_prcgvr__PhaseTransition__c> calloutsMap = (Map<SObject, wrts_prcgvr__PhaseTransition__c>) pmUtils.bulkCheckCalloutForSobject(params);
            return !calloutsMap.isEmpty();
        }
        return false;
    }

    private Boolean isCreditManagementQueue(Case caseRecord) {
        Group creditManagementQueue = MRO_QR_User.getInstance().getQueueByDeveloperName('CreditManagementQueue');
        if(creditManagementQueue != null) {
            return caseRecord.OwnerId == creditManagementQueue.Id;
        }
        return false;
    }

    private Boolean isCustomerCareQueue(Case caseRecord) {
        Group customerCareQueue = MRO_QR_User.getInstance().getQueueByDeveloperName('CustomerCare');
        if(customerCareQueue != null) {
            return caseRecord.OwnerId == customerCareQueue.Id;
        }
        return false;
    }

    private Boolean isDossierDraft(Case c) {
        Case caseRecord = MRO_QR_Case.getInstance().getById(c.Id);
        return caseRecord.Dossier__r.Status__c == 'Draft';
    }

    private Boolean notSMSDeliveryChannel(Case c) {
        Case caseRecord = MRO_QR_Case.getInstance().getById(c.Id);
        return caseRecord.SubProcess__c != constants.SUB_PROCESS_CHNG_BILLING_CHANNEL || caseRecord.BillingProfile2__r?.DeliveryChannel__c != constants.BILLING_PROFILE_DELIVERY_CHANNEL_SMS;
    }

    private Boolean accountSituationMetadataExist(Case c) {
        List<FileMetadata__c> fmList = [SELECT Id FROM FileMetadata__c WHERE Case__c = :c.Id AND DocumentType__r.DocumentTypeCode__c = 'SAP1326_CM_ISU' LIMIT 1];
        return fmList.size() > 0;
    }

    private Boolean isVoltageLevelChanged(Case c) {
        Case caseRecord = MRO_QR_Case.getInstance().getById(c.Id);
        return MRO_UTL_PowerVoltageChange.getInstance().logicIsCaseChanged(caseRecord);
    }
}