public with sharing class MRO_LC_ServicePointSearch extends ApexServiceLibraryCnt {
    static MRO_QR_ServicePoint servicePointQuery = MRO_QR_ServicePoint.getInstance();
    static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    static MRO_QR_OpportunityServiceItem osiQuery = MRO_QR_OpportunityServiceItem.getInstance();

    /**
     * @description Class to find service point data
     */
    public with sharing class findCode extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String pointCode = params.get('pointCode');
            String accountId = params.get('accountId');
            String companyDivisionId = params.get('companyDivisionId');
            if (String.isBlank(pointCode)) {
                throw new WrtsException(System.Label.Invalid + ' - ' + System.Label.Code);
            }

            List<ServicePoint__c> myServicePoint = String.isNotBlank(accountId) ? servicePointQuery.getListServicePointsByAccountId(pointCode,accountId) : servicePointQuery.getListServicePoints(pointCode);
            List<Supply__c> suppliesByServicePointCode = supplyQuery.getSuppliesByServicePointCode(pointCode);
            response.put('suppliesByServicePointCode', suppliesByServicePointCode);

            if (!myServicePoint.isEmpty()) {
                response.put('servicePointId', myServicePoint[0].Id);
                response.put('servicePoint', myServicePoint[0]);
            }
            return response;
        }
    }

    /**
     * @description Class to find service point data according to companyDivision when companyDivisionEnforced
     */
    public with sharing class findCodeTm2 extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String pointCode = params.get('pointCode');
            String accountId = params.get('accountId');
            String companyDivisionId = params.get('companyDivisionId');
            //added by moussa
            Boolean companyDivisionEnforced=Boolean.valueOf(params.get('companyDivisionEnforced'));
            //end add
            if (String.isBlank(pointCode)) {
                throw new WrtsException(System.Label.Invalid + ' - ' + System.Label.Code);
            }
            List<ServicePoint__c> myServicePoint;
            List<Supply__c> suppliesByServicePointCode;
            if(companyDivisionEnforced){
                myServicePoint = String.isNotBlank(accountId) ? servicePointQuery.getListServicePointsByAccountId(pointCode,accountId) : servicePointQuery.getListServicePointsByCompanyDivision(pointCode,companyDivisionId);
                suppliesByServicePointCode = supplyQuery.getSuppliesByServicePointCodeAndCompanyDivision(pointCode,companyDivisionId);
            } else {
                myServicePoint = String.isNotBlank(accountId) ? servicePointQuery.getListServicePointsByAccountId(pointCode,accountId) : servicePointQuery.getListServicePoints(pointCode);
                suppliesByServicePointCode = supplyQuery.getSuppliesByServicePointCode(pointCode);
            }
            response.put('suppliesByServicePointCode', suppliesByServicePointCode);

            if (!myServicePoint.isEmpty()) {
                response.put('servicePointId', myServicePoint[0].Id);
                response.put('servicePoint', myServicePoint[0]);
            }
            return response;
        }
    }
}