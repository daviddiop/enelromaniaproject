/**
 * Created by Goudiaby 14/01/2020.
 *
 */
@IsTest
public with sharing class MRO_LC_WizardHeaderTst {
    @TestSetup
    private static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new List<Account>();
        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        listAccount.add(personAccount);
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        Opportunity opportunity = MRO_UTL_TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        opportunity.CustomerInteraction__c = customerInteraction.Id;
        insert opportunity;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        dossier.CustomerInteraction__c = customerInteraction.Id;
        insert dossier;
    }

    @IsTest
    public static void initializeTest(){
        Account account = [
			SELECT Id,Name
			FROM Account
			LIMIT 1
        ];
        Opportunity opp = [
            SELECT Id,StageName, AccountId
            FROM Opportunity
            LIMIT 1
        ];        

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];        
        CompanyDivision__c companyDivision = [
            SELECT Id,Name 
            FROM CompanyDivision__c 
            WHERE Name = 'ENEL 1' 
            LIMIT 1
        ];        
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'opportunityId' => opp.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_WizardHeader', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('response') != null );
        Test.stopTest();

    }

    @IsTest
    public static void initializeExceptionTest(){        
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => '',
            'opportunityId' => '',
            'companyDivisionId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_WizardHeader', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }     
}