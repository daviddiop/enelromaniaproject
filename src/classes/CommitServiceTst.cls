@isTest
private with sharing class CommitServiceTst {
    
    @testSetup
    static void setup() {
        Profile p = [SELECT id, Name FROM Profile where name = 'System Administrator' ].get(0);  
        User user = TestDataFactory.user().standardUser().build();
        user.profileId = p.id;
        insert user;

        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();        
        insert companyDivision;

        user.CompanyDivisionId__c = companyDivision.Id;
        update user;

        Account acc = TestDataFactory.account().setName('BusinessName').build();
        insert acc;

        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;

        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = acc.Id;
        contract.CompanyDivision__c = companyDivision.Id;
        insert contract;

        BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=acc.Id;
        insert billingProfile;

        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().build();
        supply.Contract__c=contract.Id;
        supply.CompanyDivision__c = companyDivision.Id;
        supply.ServicePoint__c = servicePoint.Id;               
        insert supply;

        Supply__c supplyOld = TestDataFactory.supply().createSupplyBuilder().build();
        supply.Contract__c=contract.Id;
        supply.CompanyDivision__c = companyDivision.Id;        
        insert supplyOld;

        servicePoint.CurrentSupply__c = supplyOld.Id;
        update servicePoint;


        ContractAccount__c contractAccount = TestDataFactory.contractAccount().createContractAccount().build();
        insert contractAccount;

        Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().build();
        insert opportunity;

        OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        opportunityServiceItem.ServicePoint__c = servicePoint.Id;
        insert opportunityServiceItem;

        Dossier__c dossier = TestDataFactory.dossier().build();
        dossier.Opportunity__c = opportunity.Id;
        dossier.Account__c = acc.id;
        dossier.CompanyDivision__c = companyDivision.Id;
        insert dossier;

        Case caseNew = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'SwitchIn_ELE', 'Phone', acc.Id, dossier.Id,contractAccount.Id ,supply.Id, contract.Id);
        caseNew.CompanyDivision__c = companyDivision.Id;
        insert caseNew;

        Case caseNewTermination = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Termination_GAS', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseNewTermination.CompanyDivision__c = companyDivision.Id;
        insert caseNewTermination;

        Case caseNewSO = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'SwitchOut_GAS', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseNewSO.CompanyDivision__c = companyDivision.Id;
        insert caseNewSO;
        Case caseActivation = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Activation_ELE', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseActivation.CompanyDivision__c = companyDivision.Id;
        insert caseActivation;

        Case caseReactivation = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Reactivation_ELE', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseReactivation.CompanyDivision__c = companyDivision.Id;
        insert caseReactivation;

        Case caseTransfer = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Transfer_ELE', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseTransfer.CompanyDivision__c = companyDivision.Id;
        insert caseTransfer;

        List<Case> caseLists = new List<Case>();
        Case caseRegistryChangeFast = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'RegistryChangeFast', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseRegistryChangeFast.CompanyDivision__c = companyDivision.Id;
        caseLists.add(caseRegistryChangeFast);


        Case caseConnectionEE = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Connection_ELE', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        insert caseConnectionEE;

        Case caseConnectionGAS = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Connection_GAS', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        insert caseConnectionGAS;

        Case caseProductChange = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'ProductChange_ELE', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        insert caseProductChange;

        Case caseContractMerge = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'ContractMerge', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseContractMerge.CompanyDivision__c = companyDivision.Id;
        insert caseContractMerge;

        Case caseTechnicalDataEE = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'TechnicalDataChange_ELE', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        insert caseTechnicalDataEE;

        Case caseTechnicalDataGas = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'TechnicalDataChange_GAS', 'Phone', acc.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        insert caseTechnicalDataGas;

        Individual individual = TestDataFactory.individual().createIndividual().build();
        insert individual;

        Individual individualPrivacyChange = TestDataFactory.individual().createIndividual().build();
        insert individualPrivacyChange;


        Account personAccount = TestDataFactory.account().personAccount().setPersonIndividual(individual.Id).build();
        insert personAccount;


        PrivacyChange__c privacyChange = TestDataFactory.privacyChange().createPrivacyChangeBuilder().build();
        //privacyChange.Name = 'Privacy';
        privacyChange.Dossier__c = dossier.Id;
        privacyChange.Opportunity__c = opportunity.Id;
        privacyChange.Individual__c = individual.Id;
        privacyChange.Status__c = 'Active';
        insert privacyChange;

        PrivacyChange__c privacyChangeNotActive = TestDataFactory.privacyChange().createPrivacyChangeBuilder().build();
        //privacyChange.Name = 'Privacy';
        privacyChangeNotActive.Dossier__c = dossier.Id;
        privacyChangeNotActive.Opportunity__c = opportunity.Id;
        privacyChangeNotActive.Individual__c = individualPrivacyChange.Id;
        insert privacyChangeNotActive;




        Case caseRegistryChangeSlow = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'RegistryChangeSlow', 'Phone', personAccount.Id, dossier.Id,contractAccount.Id, supply.Id, contract.Id);
        caseRegistryChangeSlow.CompanyDivision__c = companyDivision.Id;
        caseLists.add(caseRegistryChangeSlow);
    
        /*Case caseCommitRecordTypeElectric = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Electric', 'Phone', acc.Id, dossier.Id, supply.Id, contract.Id);
        caseCommitRecordTypeElectric.CompanyDivision__c = companyDivision.Id;
        caseLists.add(caseCommitRecordTypeElectric);
    
        Case caseCommitRecordTypeGas = TestDataFactory.createCaseRecords(billingProfile.id, 'New', 'RC010', 'Gas', 'Phone', acc.Id, dossier.Id, supply.Id, contract.Id);
        caseCommitRecordTypeGas.CompanyDivision__c = companyDivision.Id;
        caseLists.add(caseCommitRecordTypeGas);*/
    
        insert caseLists;        
    }
    @isTest
    static void switchInTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
                List<Case> caseNew = [
                SELECT Id,Status,Contract__c,Supply__c
                FROM Case
                WHERE Recordtype.Developername='SwitchIn_ELE'
            ];
            caseNew[0].Status = 'Executed';
            update caseNew;
            Supply__c supp = [
                SELECT Id,Status__c,Activator__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            Contract cnt = [
                SELECT Id,Status
                FROM Contract
                WHERE Id =: caseNew[0].Contract__c
            ];
            List<Case> cases = [
                SELECT id,status,EffectiveDate__c
                FROM case
                WHERE Recordtype.Developername='SwitchIn_ELE'];
            System.assertEquals(supp.Status__c, 'Active',true);
            System.assertEquals(supp.Activator__c, caseNew[0].Id,true);
            System.assertEquals(cnt.Status, 'Activated',true);
            System.assertEquals(cases[0].EffectiveDate__c, System.today(), true);
            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppDE = [
                SELECT id,Status__c,Activator__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(suppDE.Status__c, 'Not Active',true);
            System.assertEquals(cases[0].EffectiveDate__c, System.today(), true);
        }
        Test.stopTest();
    }
    @isTest
    static void executeTerminationInTest() {
        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
        ];
        User myUser = [
            SELECT Id
            FROM User
            WHERE CompanyDivisionId__c = :companyDivision.Id
        ];

        Test.startTest();
        System.runAs(myUser) {
            List<Case> caseNewTermination = [
                SELECT Id, Status, Supply__c, Contract__c
                FROM Case
                WHERE Recordtype.Developername = 'Termination_GAS'
            ];
            caseNewTermination[0].Status = 'Executed';
            update caseNewTermination;
            Supply__c supp = [
                SELECT Id, Status__c, Terminator__c
                FROM Supply__c
                WHERE Id =:caseNewTermination[0].Supply__c
            ];
            System.assertEquals('Not Active', supp.Status__c);
        }
        Test.stopTest();
    }
    @isTest
    static void cancelTerminationInTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNewTermination = [
                SELECT id,status,Supply__c,Contract__c
                FROM case
                WHERE recordtype.developername='Termination_GAS'
            ];
            caseNewTermination[0].Status='Canceled';
            update caseNewTermination;
            Supply__c supp = [
                SELECT id, Status__c
                FROM Supply__c
                WHERE Id =:caseNewTermination[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
        }
        Test.stopTest();
    }

    @isTest
    static void executeTechnicalDataEETest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNewSO = [
                    SELECT Id,status,Supply__c,Contract__c
                    FROM case
                    WHERE Recordtype.developername='TechnicalDataChange_ELE'
            ];
            caseNewSO[0].Status = 'Executed';
            update caseNewSO;
            Supply__c supp = [
                    SELECT Id, Status__c,Terminator__c
                    FROM Supply__c
                    WHERE id =:caseNewSO[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
        }
        Test.stopTest();
    }


    @isTest
    static void executeTechnicalDataGasTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNewSO = [
                    SELECT Id,status,Supply__c,Contract__c
                    FROM case
                    WHERE Recordtype.developername='TechnicalDataChange_GAS'
            ];
            caseNewSO[0].Status = 'Executed';
            update caseNewSO;
            Supply__c supp = [
                    SELECT Id, Status__c,Terminator__c
                    FROM Supply__c
                    WHERE id =:caseNewSO[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
        }
        Test.stopTest();
    }

    @isTest
    static void executeContractMergeTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNewSO = [
                    SELECT Id,status,Supply__c,Contract__c,CompanyDivision__c
                    FROM case
                    WHERE Recordtype.developername='ContractMerge'
            ];
            caseNewSO[0].Status = 'Executed';
            update caseNewSO;
            Supply__c supp = [
                    SELECT Id, Status__c,Terminator__c,CompanyDivision__c
                    FROM Supply__c
                    WHERE id =:caseNewSO[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
        }
        Test.stopTest();
    }


    @isTest
    static void executeSwitchOutTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNewSO = [
                SELECT Id,status,Supply__c,Contract__c
                FROM case
                WHERE Recordtype.developername='SwitchOut_GAS'
            ];
            caseNewSO[0].Status = 'Executed';
            update caseNewSO;
            Supply__c supp = [
                SELECT Id, Status__c,Terminator__c
                FROM Supply__c
                WHERE id =:caseNewSO[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Not Active',true);
        }
        Test.stopTest();
     }

    @isTest
    static void cancelSwitchOutTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNewSO = [
                SELECT Id,Status,Supply__c,Contract__c
                FROM Case
                WHERE Recordtype.Developername='SwitchOut_GAS'
            ];
            caseNewSO[0].Status = 'Canceled';
            update caseNewSO;
            Supply__c supp = [
                SELECT id, Status__c,Terminator__c
                FROM Supply__c
                WHERE id =:caseNewSO[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
        }
        Test.stopTest();
    }
/*
    @isTest
    static void executeBillingProfile() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNewBP = [
                SELECT Id,Status,Supply__c,BillingProfile__c,Contract__c
                FROM Case
                WHERE Recordtype.Developername='BillingProfileChange'];
            caseNewBP[0].Status = 'Executed';
            update caseNewBP;
            Supply__c supp = [
                SELECT Id, Status__c,BillingProfile__c
                FROM Supply__c
                WHERE id =:caseNewBP[0].Supply__c
            ];
            List<Case> cases = [
                SELECT id,status,EffectiveDate__c
                FROM case
                WHERE Recordtype.Developername='BillingProfileChange'
            ];
            System.assertEquals(supp.BillingProfile__c, caseNewBP[0].BillingProfile__c,true);
            System.assertEquals(cases[0].EffectiveDate__c, System.today(), true);
        }
        Test.stopTest();
    }
    */
    @isTest
    static void activationTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNew = [
                SELECT Id,Status,Contract__c,Supply__c
                FROM Case
                WHERE Recordtype.Developername='Activation_ELE'
            ];
            caseNew[0].Status = 'Executed';
            update caseNew;
            Supply__c supp = [
                SELECT Id,Status__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppDE = [
                SELECT id,Status__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            List<Case> cases = [
                SELECT id,status,EffectiveDate__c
                FROM case
                WHERE Recordtype.Developername='Activation_ELE'
            ];
            System.assertEquals(suppDE.Status__c, 'Not Active',true);
            System.assertEquals(cases[0].EffectiveDate__c, System.today(), true);
        }
        Test.stopTest();
    }
    @isTest
    static void reactivationTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNew = [
                SELECT Id,Status,Contract__c,Supply__c
                FROM Case
                WHERE Recordtype.Developername='Reactivation_ELE'
            ];
            caseNew[0].Status = 'Executed';
            update caseNew;
            Supply__c supp = [
                SELECT Id,Status__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppDE = [
                SELECT id,Status__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            List<Case> cases = [
                SELECT id,status,EffectiveDate__c
                FROM case
                WHERE Recordtype.Developername='Reactivation_ELE'
            ];
            System.assertEquals(suppDE.Status__c, 'Not Active',true);
            System.assertEquals(cases[0].EffectiveDate__c, System.today(), true);
        }
        Test.stopTest();
    }
    @isTest
    static void TransferTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNew = [
                SELECT Id,Status,Contract__c,Supply__c,Dossier__c
                FROM Case
                WHERE Recordtype.Developername='Transfer_ELE'
            ];
            caseNew[0].Status = 'Executed';
            update caseNew;
            Supply__c supp = [
                SELECT Id,Status__c,ServicePoint__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
            ServicePoint__c service = [
                SELECT Id,CurrentSupply__c
                FROM ServicePoint__c
                WHERE id = :supp.ServicePoint__c
            ];
            Supply__c suppOld = [
                SELECT Id,Status__c
                FROM Supply__c
                WHERE id = :service.CurrentSupply__c
            ];
            Contract cnt = [
                SELECT Id,Status
                FROM Contract
                WHERE Id =: caseNew[0].Contract__c
            ];
            System.assertEquals(suppOld.Status__c, 'Not Active',true);
            System.assertEquals(cnt.Status, 'Activated',true);
            //Test Canceled
            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppACT = [
                SELECT id,Status__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(suppACT.Status__c, 'Not Active',true);
            ServicePoint__c srv = [
                SELECT Id,CurrentSupply__c
                FROM ServicePoint__c
                WHERE id = :supp.ServicePoint__c
            ];
            Supply__c spDE = [
                SELECT Id,Status__c
                FROM Supply__c
                WHERE id = :srv.CurrentSupply__c
            ];
            List<Case> cases = [
                SELECT id,status,EffectiveDate__c
                FROM case
                WHERE Recordtype.Developername='Transfer_ELE'
            ];
            System.assertEquals(cases[0].EffectiveDate__c, System.today(), true);
        }
        Test.stopTest();
    }

    @isTest
    static void productChangeTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNew = [
                    SELECT Id,Status,Contract__c,Supply__c
                    FROM Case
                    WHERE Recordtype.Developername='ProductChange_ELE'
            ];
            caseNew[0].Status = 'Executed';
            update caseNew;
            Supply__c supp = [
                    SELECT Id,Status__c
                    FROM Supply__c
                    WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppDE = [
                    SELECT id,Status__c
                    FROM Supply__c
                    WHERE id =:caseNew[0].Supply__c
            ];
            List<Case> cases = [
                    SELECT id,status,EffectiveDate__c
                    FROM case
                    WHERE Recordtype.Developername='ProductChange_ELE'
            ];
            System.assertEquals(suppDE.Status__c, 'Not Active',true);
            System.assertEquals(cases[0].EffectiveDate__c, System.today(), true);
        }
        Test.stopTest();
    }

    @isTest
    static void commitRegistryChangeTest() {
        Test.startTest();                
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNew = [
                SELECT Id,Status,Contract__c,Supply__c 
                FROM Case 
                WHERE Recordtype.Developername='RegistryChangeFast'
            ];  
            List<Case> caseNew1 = [
                SELECT Id,Status,Contract__c,Supply__c 
                FROM Case 
                WHERE Recordtype.Developername='RegistryChangeSlow'
            ];                       
            caseNew[0].Status = 'Executed';
            caseNew1[0].Status = 'Executed';
            update caseNew;
            update caseNew1;
            Supply__c supp = [
                SELECT Id,Status__c,ServicePoint__c 
                FROM Supply__c  
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
            ServicePoint__c service = [
                SELECT Id,CurrentSupply__c
                FROM ServicePoint__c  
                WHERE id = :supp.ServicePoint__c
            ];
            Supply__c suppOld = [
                SELECT Id,Status__c 
                FROM Supply__c  
                WHERE id = :service.CurrentSupply__c
            ];
            Contract cnt = [
                SELECT Id,Status
                FROM Contract
                WHERE Id =: caseNew[0].Contract__c
            ];
            System.assertEquals(suppOld.Status__c, 'Active',true);
            System.assertEquals(cnt.Status, 'Draft',true);
            //Test Canceled
            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppACT = [
                SELECT id,Status__c
                FROM Supply__c  
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(suppACT.Status__c, 'Active',true);
            ServicePoint__c srv = [
                SELECT Id,CurrentSupply__c
                FROM ServicePoint__c  
                WHERE id = :supp.ServicePoint__c
            ];
            Supply__c spDE = [
                SELECT Id,Status__c 
                FROM Supply__c  
                WHERE id = :srv.CurrentSupply__c
            ];
            System.assertEquals(spDE.Status__c, 'Active',true);
        }    
        Test.stopTest();          
    }
    
   /* @isTest
    static void commitRecordTypeTest() {
        Test.startTest();
        CompanyDivision__c cmp = [SELECT Id FROM CompanyDivision__c];
        User u = [SELECT Id FROM User WHERE CompanyDivisionId__c=:cmp.Id];
        System.runAs(u) {
            List<Case> caseNew = [
                SELECT Id,Status,Contract__c,Supply__c
                FROM Case
                WHERE Recordtype.Developername='Electric'
            ];
            List<Case> caseNew1 = [
                SELECT Id,Status,Contract__c,Supply__c
                FROM Case
                WHERE Recordtype.Developername='Gas'
            ];
            caseNew[0].Status = 'Executed';
            caseNew1[0].Status = 'Executed';
            update caseNew;
            update caseNew1;
            Supply__c supp = [
                SELECT Id,Status__c,ServicePoint__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(supp.Status__c, 'Active',true);
            ServicePoint__c service = [
                SELECT Id,CurrentSupply__c
                FROM ServicePoint__c
                WHERE id = :supp.ServicePoint__c
            ];
            Supply__c suppOld = [
                SELECT Id,Status__c
                FROM Supply__c
                WHERE id = :service.CurrentSupply__c
            ];
            Contract cnt = [
                SELECT Id,Status
                FROM Contract
                WHERE Id =: caseNew[0].Contract__c
            ];
            System.assertEquals(suppOld.Status__c, 'Active',true);
            System.assertEquals(cnt.Status, 'Draft',true);
            //Test Canceled
            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppACT = [
                SELECT id,Status__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(suppACT.Status__c, 'Active',true);
            ServicePoint__c srv = [
                SELECT Id,CurrentSupply__c
                FROM ServicePoint__c
                WHERE id = :supp.ServicePoint__c
            ];
            Supply__c spDE = [
                SELECT Id,Status__c
                FROM Supply__c
                WHERE id = :srv.CurrentSupply__c
            ];
            System.assertEquals(spDE.Status__c, 'Active',true);
        }
        Test.stopTest();
    }*/
    @isTest
    static void ConnectionEETest() {
        Test.startTest();
            List<Case> caseNew = [
                    SELECT Id,Status,Contract__c,Supply__c
                    FROM Case
                    WHERE Recordtype.Developername='Connection_ELE'
            ];
            caseNew[0].Status = 'Executed';
            update caseNew;
            Supply__c supp = [
                    SELECT Id,Status__c,Activator__c
                    FROM Supply__c
                    WHERE id =:caseNew[0].Supply__c
            ];
            Contract cnt = [
                    SELECT Id,Status
                    FROM Contract
                    WHERE Id =: caseNew[0].Contract__c
            ];

            System.assertEquals(supp.Status__c, 'Active',false);
            System.assertEquals(cnt.Status, 'Draft',true);

            caseNew[0].Status = 'Canceled';
            update caseNew;
            Supply__c suppDE = [
                    SELECT id,Status__c,Activator__c
                    FROM Supply__c
                    WHERE id =:caseNew[0].Supply__c
            ];
            System.assertEquals(suppDE.Status__c, 'Active',true);

        Test.stopTest();
    }

    @isTest
    static void ConnectionGASTest() {
        Test.startTest();
        List<Case> caseNew = [
                SELECT Id,Status,Contract__c,Supply__c
                FROM Case
                WHERE Recordtype.Developername='Connection_GAS'
        ];
        caseNew[0].Status = 'Executed';
        update caseNew;
        Supply__c supp = [
                SELECT Id,Status__c,Activator__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
        ];
        Contract cnt = [
                SELECT Id,Status
                FROM Contract
                WHERE Id =: caseNew[0].Contract__c
        ];

        System.assertEquals(supp.Status__c, 'Active',false);
        System.assertEquals(cnt.Status, 'Draft',true);

        caseNew[0].Status = 'Canceled';
        update caseNew;
        Supply__c suppDE = [
                SELECT id,Status__c,Activator__c
                FROM Supply__c
                WHERE id =:caseNew[0].Supply__c
        ];
        System.assertEquals(suppDE.Status__c, 'Active',true);

        Test.stopTest();
    }
    @isTest
    public static void commitPrivacyChangeTest() {
        Test.startTest();
        CommitService commService = new CommitService();
        Date dateDay = System.today();
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Map<String, Date> inputMap = new Map<String, Date>();
        inputMap.put(dossier.Id,dateDay);
        commService.commitPrivacyChange(inputMap);
        System.assertEquals(true, inputMap != null);
        Test.stopTest();

    }

}