/**
 * Created by napoli on 26/09/2019.
 */
/**
* @author  Giuseppe Napoli (giuseppe.napoli@webresults.it)
* @since   set 26, 2019
* @desc
* @history
*/
global with sharing class MRO_TR_IndividualHandler implements TriggerManager.ISObjectTriggerHandler {

    global void afterDelete() {}

    global void afterInsert() {}

    global void afterUndelete() {}

    global void afterUpdate() {}

    global void beforeDelete() {}

    global void beforeInsert() {
        this.checkCNP(Trigger.new, null);
        System.debug('beforeInsert trigger Individual 1 - Queries used in this apex code so far: ' + Limits.getQueries());
    }

    global void beforeUpdate() {
        this.checkCNP(Trigger.new, (Map<Id, Individual>)Trigger.oldMap);
        System.debug('beforeUpdate trigger Individual 1 - Queries used in this apex code so far: ' + Limits.getQueries());
    }

    private void checkCNP(List<Individual> individualListNew, Map<Id, Individual> individualMapOld) {

        for (Individual newIndividual : individualListNew) {
            if (newIndividual.NationalIdentityNumber__c != null && newIndividual.ForeignCitizen__c != true &&
                    (individualMapOld == null ||
                        (newIndividual.NationalIdentityNumber__c != individualMapOld.get(newIndividual.Id).NationalIdentityNumber__c) ||
                        (newIndividual.ForeignCitizen__c != individualMapOld.get(newIndividual.Id).ForeignCitizen__c))) {
                MRO_UTL_Validations.Result result = MRO_UTL_Validations.checkCNP(newIndividual.NationalIdentityNumber__c);
                if(result.outCome == false) newIndividual.NationalIdentityNumber__c.addError(String.format(Label.InvalidCnp, new List<String>{result.errorCode, newIndividual.NationalIdentityNumber__c}));
            }
        }
    }
}