/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 28.07.20.
 */

public with sharing class MRO_SRV_SapQueryWarrantiesCallOut extends MRO_SRV_SapQueryHelper {

    private static final String requestTypeQueryWarranties = 'QueryWarranties';

    public static MRO_SRV_SapQueryWarrantiesCallOut getInstance() {
        return new MRO_SRV_SapQueryWarrantiesCallOut();
    }

    public override wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap) {

        Set<String> conditionFields = new Set<String>{
                'SupplyIntegrationKey', 'CustomerCode'
        };

        return buildSimpleRequest(argsMap, conditionFields);
    }

    public List<MRO_LC_WarrantiesInquiry.Warranty> getList(MRO_LC_WarrantiesInquiry.WarrantyRequestParam params) {
        Map<String, Object> conditions = new Map<String, Object>{
                //'SupplyIntegrationKey' => params.ID_INTEGR_CTR,
                'CustomerCode' => params.NR_EXT_PA
        };

        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = executeCallout(this.endpoint, requestTypeQueryWarranties, conditions, 'MRO_SRV_SapQueryWarrantiesCallOut');

        if (calloutResponse == null || calloutResponse.responses.isEmpty() || calloutResponse.responses[0] == null) {
            return null;
        }
        System.debug(calloutResponse.responses[0].code);
        System.debug(calloutResponse.responses[0].description);

        if (calloutResponse.responses[0].objects == null || calloutResponse.responses[0].objects.isEmpty()) {
            return null;
        }
        System.debug(calloutResponse.responses[0].objects);

        List<MRO_LC_WarrantiesInquiry.Warranty> warranties = new List<MRO_LC_WarrantiesInquiry.Warranty>();

        for (wrts_prcgvr.MRR_1_0.WObject responseWo : calloutResponse.responses[0].objects) {
            if (responseWo.objects != null && responseWo.objects.size() != 0) {
                for (wrts_prcgvr.MRR_1_0.WObject childWo : responseWo.objects) {
                    Map<String, String> childWoMap = MRO_UTL_MRRMapper.wobjectToMap(childWo);

                    MRO_LC_WarrantiesInquiry.Warranty warrantyDTO = new MRO_LC_WarrantiesInquiry.Warranty(childWoMap);

                    warranties.add(warrantyDTO);
                }
            }
        }

        System.debug(warranties);
        return warranties;
    }
}