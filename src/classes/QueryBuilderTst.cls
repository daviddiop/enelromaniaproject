@IsTest
private class QueryBuilderTst {
    @IsTest
    static void testChildQuery() {
        QueryBuilder childQuery = QueryBuilder.getInstance()
            .setObject(Contact.sObjectType)
            .setFields(new List<SObjectField>{Contact.Id, Contact.Name})
            .addField(Contact.Id, Account.Id);

        QueryBuilder baseQueryBuilder = QueryBuilder.getInstance()
            .setObject(Account.sObjectType)
            .setFields(new List<SObjectField>{Account.Name, Account.Id})
            .setSubQuery(childQuery)
            .setCondition(new QueryBuilder.FieldCondition().eqOp(Account.Name, 'Test'));

        final String queryString = 'SELECT Name, Id, (SELECT Id, Name FROM Contacts ) FROM Account WHERE Name = \'Test\'';
        System.assertEquals(queryString, baseQueryBuilder.build());

        List<QueryBuilder.Condition> conditions = new List<QueryBuilder.Condition>();
        conditions.add(new QueryBuilder.FieldCondition().eqOp(Account.Name, 'Test'));
        conditions.add(new QueryBuilder.FieldCondition().likeOp(Account.Name, 'Test'));

        baseQueryBuilder = QueryBuilder.getInstance()
            .setObject(Account.sObjectType)
            .setFields(new List<SObjectField>{Account.Name, Account.Id})
            .setSubQuery(childQuery)
            .setCondition(conditions, 'AND');
        baseQueryBuilder = QueryBuilder.getInstance()
            .setObject(Account.sObjectType)
            .setFields(new List<SObjectField>{Account.Name, Account.Id})
            .setSubQuery(childQuery)
            .setCondition(conditions, 'OR');
        List<QueryBuilder.Condition> conditionsIsEmpty = new List<QueryBuilder.Condition>();
        baseQueryBuilder = QueryBuilder.getInstance()
            .setObject(Account.sObjectType)
            .setFields(new List<SObjectField>{Account.Name, Account.Id})
            .setSubQuery(childQuery)
            .setCondition(conditionsIsEmpty, 'OR');
        QueryBuilder.LogicCondition logCon  = new QueryBuilder.LogicCondition();
        logCon.orOp(conditions);
        logCon.getExpression();

    }
}