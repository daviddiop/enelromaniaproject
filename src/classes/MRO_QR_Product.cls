/**
 * @author  Luca Ravicini
 * @since   May 7, 2020
 * @desc   Query class for Product and related objects
 *
 */
public with sharing class MRO_QR_Product {
    public static MRO_QR_Product getInstance() {
        return (MRO_QR_Product)ServiceLocator.getInstance(MRO_QR_Product.class);
    }

    public List<Pricebook2> listActivePricebooks() {
        return [
                SELECT Id, Name, IsStandard
                FROM Pricebook2
                WHERE IsActive = true
        ];
    }

    public List<PricebookEntry> listActivePricebookEntryByPricebookId(String pricebookId) {
        return [
                SELECT Id, Name, ProductCode, Product2.Name, Product2.Family, Product2.Description, Product2.Image__c,
                        Product2.RecordType.Name, Product2.RecordType.DeveloperName
                FROM PricebookEntry
                WHERE Pricebook2Id = :pricebookId
                AND IsActive = true
                AND Product2.IsActive = true
        ];
    }

    public List<Product2> listProductByFamily(Set<String> familySet) {
        return [
                SELECT Id, Name, Family, Description, Image__c, RecordType.Name, RecordType.DeveloperName
                FROM Product2
                WHERE Family IN :familySet
        ];
    }


    public Product2 getById(String productId) {
        List<Product2> prdList = this.getByIds(new Set<String>{productId});
        System.assertEquals(1, prdList.size());
        return prdList.get(0);
    }

    public List<Product2> getByIds(Set<String> productIds) {
        return [
                SELECT Id, Name, Family, Description, Image__c, RecordType.Name, CommercialProduct__r.RateType__c, CommercialProduct__r.ProductTerm__c, CommercialProduct__r.InstallmentsNumbers__c,
                (SELECT ProductOption__r.Accessibility__c, ProductOption__r.BundleProduct__c, ProductOption__r.DefaultValue__c,
                        ProductOption__r.Description__c, ProductOption__r.HelpText__c, ProductOption__r.Key__c, ProductOption__r.Label__c,
                        ProductOption__r.Max__c, ProductOption__r.Min__c, ProductOption__r.Required__c, ProductOption__r.Selected__c,
                        ProductOption__r.Type__c, ProductOption__r.ValueSet__c, ProductOption__r.RecordType.Name,
                        ProductOption__r.RecordType.DeveloperName
                FROM Options__r)
                FROM Product2
                WHERE Id IN :productIds
        ];
    }

    public List<Product2> listByCommercialProductIds(Set<String> commercialProductIds) {
        return [
            SELECT Id, Name, CommercialProduct__c
            FROM Product2
            WHERE CommercialProduct__c IN :commercialProductIds
        ];
    }
}