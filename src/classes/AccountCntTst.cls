@IsTest
public with sharing class AccountCntTst {

    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.BusinessType__c = 'NGO';
        Account personAccount = TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        List<Account> listAccounts = new List<Account>();
        listAccounts.add(businessAccount);
        listAccounts.add(personAccount);
        insert listAccounts;

        Individual individual = TestDataFactory.individual().createIndividual().build();
        individual.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert individual;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        interaction.Interlocutor__c = individual.Id;
        interaction.Channel__c = 'MyEnel';
        insert interaction;

        Contact contact = TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccounts[0].Id;
        contact.IndividualId = individual.Id;
        contact.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert contact;

        CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id,listAccounts[0].Id,contact.Id).build();
        insert customerInteraction;
    }

    @isTest
    static void getOptionListTest() {
        Test.startTest();
        Object response = TestUtils.exec('AccountCnt','getOptionList',null,true);
        Map<String,Object> createdOptionList = (Map<String,Object>)response;
        System.assertEquals(true, createdOptionList.get('roleOptionList') != null);
        Test.stopTest();
    }

    @isTest
    static void saveBusinessAccountTest() {
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            lIMIT 1
        ];
        Account account = TestDataFactory.account().businessAccount().build();
        account.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        account.Name = 'Business Prospect';
        account.Key__c = '132326';
        account.BusinessType__c = 'Embassy';
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo businessRt = accountRts.get('BusinessProspect');
        AccountCnt.InputData inputData = new AccountCnt.InputData();
        inputData.account = account;
        inputData.interactionRecord= interaction.Id;
        inputData.roles = 'Influenzatore';
        inputData.recordTypeId = businessRt.getRecordTypeId();
        String accountInput = JSON.serialize(account);
        Map<String, String > inputJSON = new Map<String, String>{
            'account' => accountInput,
            'interactionRecord' => interaction.Id,
            'roles' => 'Influenzatore',
            'recordTypeId' => businessRt.getRecordTypeId()
        };

        Test.startTest();
        Object response = TestUtils.exec('AccountCnt','saveAccount',inputJSON,true);
        Account createdAccount=(Account)response;
        Test.stopTest();

        System.assert(createdAccount.Id!=null);
    }

    @IsTest
    static void savePersonAccountTest() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                lIMIT 1
        ];

        Account personAccount = TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        personAccount.FirstName = 'Person1';
        personAccount.LastName = 'Account';
        personAccount.PersonEmail = 'Person1Email@it.com';
        personAccount.Key__c = '132328';
        personAccount.PersonMobilePhone = '233239';
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');
        String accountInput = JSON.serialize(personAccount);
        Map<String, String > inputJSON = new Map<String, String>{
                'account' => accountInput,
                'interactionRecord' => interaction.Id,
                'roles' => 'Influenzatore',
                'recordTypeId' => personRt.getRecordTypeId()
        };

        Test.startTest();
        Object response = TestUtils.exec('AccountCnt','saveAccount',inputJSON,true);
        Account createdAccount=(Account)response;
        Test.stopTest();
        System.assert(createdAccount.Id!=null);
    }

    @IsTest
    static void savePersonAccountWithDuplicatesTest() {
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            lIMIT 1
        ];
        Account myExistAccount = [
            SELECT Id, NationalIdentityNumber__pc
            FROM Account
            WHERE IsPersonAccount = true
            lIMIT 1
        ];
        System.assertNotEquals(null, myExistAccount);

        Account account = TestDataFactory.account().personAccount().setNationalIdentityNumber(myExistAccount.NationalIdentityNumber__pc).build();
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');

        Map<String, String > inputJSON = new Map<String, String>{
            'account' => JSON.serialize(account),
            'interactionRecord' => interaction.Id,
            'roles' => 'Influenzatore',
            'recordTypeId' => personRt.getRecordTypeId()
        };

        Test.startTest();
        String message = (String)TestUtils.exec('AccountCnt','saveAccount',inputJSON,false);
        Test.stopTest();
    }

    @IsTest
    static void savePersonAccountWithNullInterlocutorTest() {
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                lIMIT 1
        ];

        interaction.Interlocutor__c=null;
        update interaction;

        Account personAccount = TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        personAccount.FirstName = 'Person1';
        personAccount.LastName = 'Account';
        personAccount.PersonEmail = 'Person1Email@it.com';
        personAccount.Key__c = '132328';
        personAccount.PersonMobilePhone = '233239';
        Map<String, Schema.RecordTypeInfo> accountRts = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo personRt = accountRts.get('PersonProspect');
        String accountInput = JSON.serialize(personAccount);
        Map<String, String > inputJSON = new Map<String, String>{
                'account' => accountInput,
                'interactionRecord' => interaction.Id,
                'roles' => 'Influenzatore',
                'recordTypeId' => personRt.getRecordTypeId()
        };

        Test.startTest();
        Object response = TestUtils.exec('AccountCnt','saveAccount',inputJSON,true);
        Account createdAccount=(Account)response;
        Test.stopTest();
        System.assert(createdAccount.Id!=null);
    }

    @isTest
    static void retrieveAccountTest() {
        Id accountId = [
                SELECT Id, Name,IsPersonAccount,FirstName,LastName,PersonEmail,
                        NationalIdentityNumber__pc,PersonMobilePhone,Email__c,VATNumber__c,
                        Phone,BusinessType__c,Active__c
                FROM Account
                LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => accountId
        };
        Test.startTest();
        Object response = TestUtils.exec('AccountCnt','retrieveAccount',inputJSON,true);
        Account createdAccount = (Account) response;
        System.assert(createdAccount.Id==accountId);
        Map<String, String > inputJSON1 = new Map<String, String>{
                'accountId' => ''
        };
        response = TestUtils.exec('AccountCnt','retrieveAccount',inputJSON1,true);
        system.assertEquals(response,null);
        Test.stopTest();
    }

    @isTest
    static void updateAccountTest(){
        Account account = [
                SELECT Id, Name,IsPersonAccount,FirstName,LastName,
                        PersonEmail,NationalIdentityNumber__pc,PersonMobilePhone,
                        Email__c,VATNumber__c,Phone,BusinessType__c,Active__c
                FROM Account
                LIMIT 1
        ];
        System.debug('###-Account-### '+account);
        //Account account1 = TestDataFactory.account().setFirstName('business').build();
        account.Name = 'Business interlocutor';
        String accountInput = JSON.serialize(account);
        Map<String, String > inputJSON = new Map<String, String>{
                'account' => accountInput
        };
        Test.startTest();
        Object response = TestUtils.exec('AccountCnt','updateAccount',inputJSON,true);
        Account createdAccount = (Account) response;
        System.assert(createdAccount.Name == 'Business interlocutor');
        Test.stopTest();
    }

    @isTest
    static void isInterlocutorExistTest(){
        Account account = [
                SELECT Id,PersonIndividualId
                FROM Account
                LIMIT 1
        ];
        Individual individual = [
                SELECT  Id
                FROM Individual
                LIMIT 1
        ];
        //account.PersonIndividualId = individual.Id;
        update account;
        Interaction__c interaction = [
                SELECT Id,InterlocutorFirstName__c
                FROM Interaction__c
                LIMIT 1
        ];
        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, true);
        Map<String, String> mapOptions=(Map<String, String>)response;
        System.assert(mapOptions.get('customerInteractionId') != null);
        String interlocutorFirstNameEmpty = StringUtils.getEmptyIfNull(interaction.InterlocutorFirstName__c);
        System.assertEquals(interlocutorFirstNameEmpty, '');
        interaction.InterlocutorFirstName__c = 'TestName';
        update interaction;
        String interlocutorFirstName = StringUtils.getEmptyIfNull(interaction.InterlocutorFirstName__c);
        System.assertEquals(interlocutorFirstName, 'TestName');
        Test.stopTest();
    }

    @isTest
    static void isInterlocutorExistTest2(){
        Account businessAccount = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];

        Individual individual = TestDataFactory.individual().createIndividual().build();
        individual.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert individual;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        interaction.Interlocutor__c = individual.Id;
        interaction.Channel__c = 'MyEnel';
        insert interaction;

        Map<String,Object> jsonInput = new Map<String, String>{
            'customerId' => businessAccount.Id,
            'interactionId'=>interaction.Id
        };

        Test.startTest();
        Map<String, String> result = (Map<String, String>)TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, true);
        Test.stopTest();

        System.assert(result.get('customerInteractionId') != null);
    }

    @isTest
    static void isInterlocutorExistIsBlankinteractionIdTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>''
        };
        Test.startTest();
        Object response = TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, false);
        system.assertEquals(response,System.Label.Interaction + ' - ' + System.Label.MissingId);
        Test.stopTest();
    }
    @isTest
    static void isInterlocutorExistIsBlankcustomerIdTest(){
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => '',
                'interactionId'=>interaction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, false);
        system.assertEquals(response,System.Label.Account + ' - ' + System.Label.MissingId);
        Test.stopTest();
    }


    @isTest
    static void isInterlocutorExistNoAccountContactRelationTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];

        Individual individual = TestDataFactory.individual().createIndividual().build();
        individual.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert individual;

        interaction.Interlocutor__c =null;
        update interaction;

        Map<String,Object> jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };

        Test.startTest();
        Map<String, String> mapOptions = (Map<String, String>)TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, true);
        System.assert(mapOptions.get('interlocutorId') != null);

        List<AccountContactRelation> accountContactRelationList = [
                SELECT Id
                FROM AccountContactRelation
        ];
        System.debug('accountContactRelationList ' + accountContactRelationList);
        System.assert(accountContactRelationList.size() == 1);

        Contact contact = [
                SELECT Id,Name,IndividualId,IsPersonAccount
                FROM Contact
                LIMIT 1
        ];
        contact.IndividualId = null;
        update contact;

        jsonInput = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };
        mapOptions = (Map<String, String>)TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, true);

        System.assert(mapOptions.get('interlocutorId') != null);
        Test.stopTest();
    }




    @isTest
    static void isInterlocutorExistIsNotBlankInterlocutorTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            LIMIT 1
        ];

        Individual individual = TestDataFactory.individual().createIndividual().build();
        individual.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        insert individual;

        interaction.Interlocutor__c =individual.Id;
        update interaction;

        Map<String,Object> jsonInput = new Map<String, String>{
            'customerId' => account.Id,
            'interactionId'=>interaction.Id
        };

        Test.startTest();
        Map<String, String> mapOptions = (Map<String, String>)TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, true);
        System.assert(mapOptions.get('interlocutorId') != null);

        List<AccountContactRelation> accountContactRelationList = [
            SELECT Id
            FROM AccountContactRelation
        ];
        System.assert(accountContactRelationList.size() == 2);

        Contact contact = [
            SELECT Id,Name,IndividualId,IsPersonAccount
            FROM Contact
            LIMIT 1
        ];
        contact.IndividualId = null;
        update contact;

        jsonInput = new Map<String, String>{
            'customerId' => account.Id,
            'interactionId'=>interaction.Id
        };
        mapOptions = (Map<String, String>)TestUtils.exec('AccountCnt', 'isInterlocutorExist', jsonInput, true);

        System.assert(mapOptions.get('interlocutorId') != null);
        Test.stopTest();
    }

    @isTest
    static void isInterlocutorExistIsEmptycustomerInteractionListTest(){
        Account account = [
                SELECT Id,Name,IsPersonAccount
                FROM Account
                WHERE IsPersonAccount = false
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Individual individual = [
                SELECT  Id
                FROM Individual
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'customerId' => account.Id,
                'interactionId'=>interaction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('AccountCnt', 'isInterlocutorExist', inputJSON, true);
        Map<String, String> mapOptions=(Map<String, String>)response;
        System.assert(mapOptions.get('customerInteractionId') != null);
        List<CustomerInteraction__c> listCustomerInteractions = [
                SELECT Id
                FROM CustomerInteraction__c
        ];
        System.debug('### '+listCustomerInteractions.size());
        System.assert(listCustomerInteractions.size() == 1);
        Test.stopTest();
    }

    @isTest
    static void searchAccountTest() {
        Account account = [
                SELECT Id,Email__c,Name,Phone,VATNumber__c,BusinessType__c
                FROM Account where Email__c='Business@it.com'
                LIMIT 1
        ];
        Object response=TestUtils.exec('AccountCnt','searchAccount',account,true);
        Test.startTest();
        List<Account> accountList=(List<Account>)response;
        System.assert(accountList.size()== 1);
        Test.stopTest();
    }

    @isTest
    static void searchAccountTestNoRow() {
        Account account = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Object response=TestUtils.exec('AccountCnt','searchAccount',account,true);
        Test.startTest();
        List<Account> accountList=(List<Account>)response;
        System.assert(accountList.size()== 0);
        Test.stopTest();
    }

    @isTest
    private static void getByIdsTest(){
        Account myAccount = [SELECT Id FROM Account LIMIT 1];
        Set<Id> accountSetIds = new Set<Id>();
        accountSetIds.add(myAccount.Id);
        AccountQueries myAQ = AccountQueries.getInstance();
        Test.startTest();
        Map<Id, Account> response =  myAQ.getByIds(accountSetIds);
        Test.stopTest();
        System.assertEquals(true ,  response.size() > 0 );
    }

    @isTest
    private static void listByIdsTest(){
        Account myAccount = [SELECT Id FROM Account LIMIT 1];
        List<Id> accountListIds = new List<Id>();
        accountListIds.add(myAccount.Id);
        AccountQueries myAQ = AccountQueries.getInstance();
        Test.startTest();
        List<Account> response =  myAQ.listByIds(accountListIds);
        Test.stopTest();
        System.assertEquals(true ,  response.size() > 0 );
    }

    @isTest
    private static void ContactServiceCreateTest(){
        Account account = [
                SELECT Id,Name,LastName,FirstName
                FROM Account
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Individual individual = [
                SELECT  Id
                FROM Individual
                LIMIT 1
        ];
        Test.startTest();
        ContactService contactServ = new ContactService();
        Contact response =  contactServ.create(account.FirstName,account.LastName,individual.Id,account.Id);
        Test.stopTest();
        System.assertEquals(true, response != null);
    }
}