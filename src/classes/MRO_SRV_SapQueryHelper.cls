/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 27.07.20.
 */

public with sharing abstract class MRO_SRV_SapQueryHelper implements MRO_SRV_SendMRRcallout.IMRRCallout{

    public String endpoint = 'sapquery';

    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap) {

        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        wrts_prcgvr.MRR_1_0.Request request = buildRequest(argsMap);

        multiRequest.requests.add(request);

        System.debug(multiRequest);

        return multiRequest;
    }

    public abstract wrts_prcgvr.MRR_1_0.Request buildRequest(Map<String, Object> argsMap);

    public wrts_prcgvr.MRR_1_0.Request buildSimpleRequest(Map<String, Object> argsMap, Set<String> conditionFields){
        Map<String, Object> conditions = (Map<String, Object>) argsMap.get('conditions');
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        String requestType = parameters.get('requestType');

        wrts_prcgvr.MRR_1_0.WObject wo;

        wo = new wrts_prcgvr.MRR_1_0.WObject();
        wo.id = '';
        wo.name = requestType;
        wo.objectType = requestType;

        for (String fieldName : conditionFields) {
            Object fieldValue = conditions.get(fieldName);
            if (fieldValue != null) {
                wo.fields.add(MRO_UTL_MRRMapper.createField(fieldName, fieldValue));
            }
        }
        request.objects.add(wo);

        return request;
    }

    public virtual wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse) {
        return null;
    }

    public static Boolean checkSapEnabled() {
        List<SAPCallouts__c> sapCalloutsCustomSetting = [SELECT MeterList__c FROM SAPCallouts__c LIMIT 1];
        if(sapCalloutsCustomSetting.size() == 0){
            return false;
        }
        return sapCalloutsCustomSetting[0].MeterList__c;
    }

    public wrts_prcgvr.MRR_1_0.MultiResponse executeCallout(String endPoint, String requestType, Map<String, Object> conditions, String className) {

        Map<String, Object> argsMap = new Map<String, Object>();
        argsMap.put('method', className);
        argsMap.put('conditions', conditions);

        Map<String, String> parameters = new Map<String, String>();
        parameters.put('requestType', requestType);
        parameters.put('timeout', '60000');
        parameters.put('endPoint', endPoint);
        argsMap.put('parameters', parameters);

        MRO_SRV_SendMRRcallout instance = new MRO_SRV_SendMRRcallout();
        Object executionResult = instance.execute((Object) argsMap, true);
        System.debug(executionResult);
        if (executionResult instanceof wrts_prcgvr.MRR_1_0.MultiResponse) {
            return (wrts_prcgvr.MRR_1_0.MultiResponse) executionResult;
        }
        return null;
    }

    public static Double parseDouble(String value){
        if(String.isEmpty(value)){
            return null;
        }
        return Double.valueOf(value);
    }

    public static Date parseDate(String value){
        if(String.isEmpty(value)){
            return null;
        }
        try {
            return Date.valueOf(value);
        }catch(Exception e){
            return null;
        }

    }

    public static Integer parseInteger(String value){
        if(String.isEmpty(value)){
            return null;
        }
        return Integer.valueOf(value);
    }

    public static Decimal parseDecimal(String value){
        if(String.isEmpty(value)){
            return null;
        }
        return Decimal.valueOf(value);
    }
}