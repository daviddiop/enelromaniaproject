/**
 * @author  Stefano Porcari
 * @since   Dec 24, 2019
 * @desc    Service class for wrts_prcgvr__Activity__c object
 *
 */
public with sharing class MRO_SRV_Activity {

    private static MRO_QR_Activity mroQrActivity = MRO_QR_Activity.getInstance();
    public static MRO_SRV_Address addressSrv = MRO_SRV_Address.getInstance();
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();

    static Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();

    public static MRO_SRV_Activity getInstance() {
        return (MRO_SRV_Activity) ServiceLocator.getInstance(MRO_SRV_Activity.class);
    }

    /**
    * @author  Baba Goudiaby
    * @description Get Activity for Product Change process
    * @see [ENLCRO-601] Customer feedback to be corrected
    * @date 14/02/2020
    * @param activityId
    * @return List<wrts_prcgvr__Activity__c> list of activities
    */
    public List<wrts_prcgvr__Activity__c> getActivityRecords(String activityId) {
        List<wrts_prcgvr__Activity__c> activityList = new List<wrts_prcgvr__Activity__c>();
        wrts_prcgvr__Activity__c activity = mroQrActivity.getById(activityId);
        if (activity.Case__c != null && activity.Case__r.Status != 'Canceled') {
            activityList.add(activity);
        } else {
            if (String.valueOf(activity.Type__c).contains('Dossier Retention')) {
                activityList = mroQrActivity.getActivitiesByParentId(activityId);
            }
            else if(String.valueOf(activity.Type__c).contains('Dossier Product Change')){
                activityList = mroQrActivity.getActivityListByParentId(activityId);
            }
        }
        for (Integer i = 0; i < activityList.size(); i++) {
            if (activityList.get(i).wrts_prcgvr__Status__c == 'Product change initiated') {
                activityList.remove(i);
            }
        }
        return activityList;
    }

    /**
    * @author  Baba Goudiaby
    * @description instantiate Service Point activity
    * @date 21/02/2020
    * @param caseRecord
    * @return wrts_prcgvr__Activity__c
    */
    public wrts_prcgvr__Activity__c instantiateServicePointActivity(Case caseRecord, String type) {
        wrts_prcgvr__Activity__c activityRetention = new wrts_prcgvr__Activity__c();
        activityRetention.Case__c = caseRecord.Id;
        activityRetention.Account__c = caseRecord.Account.Id;
        if (caseRecord.EffectiveDate__c != null && type != constantsUtl.SERVICE_POINT_PRODUCT_CHANGE) {
            Date swiOutDate = caseRecord.EffectiveDate__c;
            activityRetention.wrts_prcgvr__DueDate__c = swiOutDate.addDays(-5);
        }
        activityRetention.Dossier__c = caseRecord.Dossier__c;
        return activityRetention;
    }

    public Boolean updateActivitiesStatus(List<wrts_prcgvr__Activity__c> activities, String newStatus) {
        for (wrts_prcgvr__Activity__c activity : activities) {
            activity.wrts_prcgvr__Status__c = newStatus;
        }
        if (!activities.isEmpty()) {
            upsert activities;
        }
        return true;
    }

    public Boolean closeFileCheckActivity(String fileMetadataId) {
        wrts_prcgvr__Activity__c checkActivity = mroQrActivity.getByFileMetadataAndType(fileMetadataId, constantsUtl.ACTIVITY_FILE_CHECK_TYPE);
        if (checkActivity != null) {
            return this.updateActivitiesStatus(new List<wrts_prcgvr__Activity__c>{checkActivity}, constantsUtl.ACTIVITY_STATUS_COMPLETED);
        }
        return true;
    }

    public List<wrts_prcgvr__Activity__c> closeCompleteValidationActivities(Id parentRecordId) {
        return this.closeCompleteValidationActivities(parentRecordId, '');
    }

    public List<wrts_prcgvr__Activity__c> closeCompleteValidationActivities(Id parentRecordId, String tokenId) {
        List<wrts_prcgvr__Activity__c> openActivities = mroQrActivity.findOpenValidationActivitiesByParentRecordId(parentRecordId);
        List<wrts_prcgvr__Activity__c> completeValidationActivities = new List<wrts_prcgvr__Activity__c>();
        for (wrts_prcgvr__Activity__c activity : openActivities) {
            Boolean canBeClosed = true;
            if (activity.Validatable_Documents__r != null) {
                for (ValidatableDocument__c vd : activity.Validatable_Documents__r) {
                    if (vd.FileMetadata__c == null && vd.DocumentBundleItem__r.Mandatory__c == true) {
                        canBeClosed = false;
                        break;
                    }
                }
            }
            if (canBeClosed) {
                completeValidationActivities.add(activity);
            }
        }
        if (String.isNotBlank(tokenId) && !completeValidationActivities.isEmpty()) {
            Token__c dossierToken = new Token__c(Id = tokenId, IsExpired__c = true);
            update dossierToken;
        }
        this.updateActivitiesStatus(completeValidationActivities, constantsUtl.ACTIVITY_STATUS_COMPLETED);
        return completeValidationActivities;
    }

    /**
    * @author  David DIOP
    * @description create customer activity
    * Updated by BG - 10/02/2020
    * @see [ENLCRO-601] Customer feedback to be corrected  - BG
    * @date 24/01/2020
    * @param caseRecord , type
    * @return wrts_prcgvr__Activity__c
    */
    public wrts_prcgvr__Activity__c insertCustomerActivity(Case caseRecord, String type) {
        wrts_prcgvr__Activity__c activityRetention = new wrts_prcgvr__Activity__c();
        activityRetention.Account__c = caseRecord.AccountId;
        activityRetention.Type__c = type;
        //activityRetention.Dossier__c = caseRecord.Dossier__c;
        activityRetention.wrts_prcgvr__ObjectId__c = caseRecord.AccountId;
        databaseSrv.insertSObject(activityRetention);
        System.debug('#########activityRetention1 ' + activityRetention);
        return activityRetention;
    }

    /**
    * @author  David DIOP
    * @description create child activity
    * @date 24/01/2020
    * @date 22/05/2020 - Updated by Goudiaby
    * @param caseList
    * @param type
    * @param dossierRetentionId
    * @return List<wrts_prcgvr__Activity__c>
    */
    public List<wrts_prcgvr__Activity__c> insertChildActivity(List<Case> caseList, String type, Id dossierRetentionId) {
        List<wrts_prcgvr__Activity__c> activityRetentionList = new List<wrts_prcgvr__Activity__c>();
        if (!caseList.isEmpty() && String.isNotBlank(type)) {
            for (Case caseRecord : caseList) {
                wrts_prcgvr__Activity__c activityRetention = instantiateServicePointActivity(caseRecord, type);
                activityRetention.Type__c = type;
                activityRetention.wrts_prcgvr__ObjectId__c = caseRecord.Id;
                activityRetention.Parent__c = dossierRetentionId;
                activityRetentionList.add(activityRetention);
            }
            databaseSrv.insertSObject(activityRetentionList);
            System.debug('#########activityRetention2 ' + activityRetentionList);
        }
        return activityRetentionList;
    }

    /**
    * @author  Baba GOUDIABY
    * @description create dossier activity
    *
    * @see [ENLCRO-601] Customer feedback to be corrected  - BG
    * @date 07/04/2020
    * @param caseRecord, type
    * @return wrts_prcgvr__Activity__c
    */
    public wrts_prcgvr__Activity__c insertDossierActivity(Case caseRecord, Id customerActivity, String type) {
        wrts_prcgvr__Activity__c activityRetention = new wrts_prcgvr__Activity__c();
        activityRetention.Account__c = caseRecord.AccountId;
        activityRetention.Type__c = type;
        activityRetention.Parent__c = customerActivity;
        activityRetention.Dossier__c = caseRecord.Dossier__c;
        activityRetention.wrts_prcgvr__ObjectId__c = caseRecord.Dossier__c;
        databaseSrv.insertSObject(activityRetention);
        System.debug('#########activityRetention1 ' + activityRetention);
        return activityRetention;
    }

    /**
    * @author  Baba Goudiaby
    * @description Delete Child Activity
    *
    * @date 07/0/2020
    * @param caseIds List Id of Old deleted Cases
    */
    public void deleteChildActivities(List<Id> caseIds) {
        List<Id> activityRetentionListToBeDelete = new List<Id>();
        System.debug('on delete child activities Cases Ids >>> ' + caseIds);
        Map<Id, wrts_prcgvr__Activity__c> retentionActivities = mroQrActivity.getByCaseId(new Set<Id>(caseIds));
        System.debug('on delete child activities retention Activities >>> ' + retentionActivities);
        for (wrts_prcgvr__Activity__c activity : retentionActivities.values()) {
            if (activity.Type__c == constantsUtl.SERVICE_POINT_RETENTION) {
                activityRetentionListToBeDelete.add(activity.Id);
            }
        }
        if (!activityRetentionListToBeDelete.isEmpty()) {
            databaseSrv.deleteSObject(activityRetentionListToBeDelete);
        }
    }

    /**
    * @author  Baba GOUDIABY
    * @description update child activities from dossier activity
    *
    * @date 07/04/2020
    * @param parentId, refusalReason,refusalReasonDetails
    * @return update result Boolean
    */
    @AuraEnabled
    public static Boolean updateChildActivitiesCloseStatusAndReason(String parentId, String refusalReason, String refusalReasonDetails) {
        List<wrts_prcgvr__Activity__c> activityRetentionList = new List<wrts_prcgvr__Activity__c>();
        if (String.isNotBlank(parentId)) {
            activityRetentionList = mroQrActivity.getActivitiesByParentId(parentId);
        }
        for (Integer i = 0; i < activityRetentionList.size(); i++) {
            activityRetentionList.get(i).wrts_prcgvr__Status__c = 'Cancelled';
            activityRetentionList.get(i).wrts_prcgvr__IsClosed__c = true;
            activityRetentionList.get(i).CancellationReason__c = refusalReason;
            activityRetentionList.get(i).CancellationDetails__c = refusalReasonDetails;
        }
        return databaseSrv.updateSObject(activityRetentionList);
    }

    public wrts_prcgvr__Activity__c insertDiscardManagementActivity(Id parentRecordId, String externalSystem, String flowName, String errorCode, String errorMessage) {
        return this.insertDiscardManagementActivity(parentRecordId, UserInfo.getUserId(), externalSystem, flowName, errorCode, errorMessage);
    }

    public wrts_prcgvr__Activity__c insertDiscardManagementActivity(Id parentRecordId, Id ownerId, String externalSystem, String flowName, String errorCode, String errorMessage) {
        MRO_QR_DiscardTemplateConfiguration confQueries = MRO_QR_DiscardTemplateConfiguration.getInstance();
        DiscardTemplateConfiguration__mdt conf = confQueries.getBySystemFlowAndError(externalSystem, flowName, errorCode);
        if (conf != null) {
            FieldsTemplate__c template = new FieldsTemplate__c(Code__c = conf.FieldsTemplateCode__c);
            wrts_prcgvr__Activity__c discardActivity = new wrts_prcgvr__Activity__c(
                    Type__c = constantsUtl.ACTIVITY_DISCARD_MANAGEMENT_TYPE,
                    wrts_prcgvr__ObjectId__c = parentRecordId,
                    wrts_prcgvr__Description__c = errorMessage,
                    wrts_prcgvr__IsRequired__c = true,
                    FieldsTemplate__r = template,
                    OwnerId = ownerId
            );
            databaseSrv.insertSObject(discardActivity);
            return discardActivity;
        } else {
            return this.insertGenericDiscardManagementActivity(parentRecordId, ownerId, externalSystem, flowName, errorMessage);
        }
    }

    public wrts_prcgvr__Activity__c insertGenericDiscardManagementActivity(Id parentRecordId, Id ownerId, String externalSystem, String flowName, String errorMessage) {
        wrts_prcgvr__Activity__c discardActivity = new wrts_prcgvr__Activity__c(
                Type__c = constantsUtl.ACTIVITY_DISCARD_MANAGEMENT_TYPE,
                wrts_prcgvr__ObjectId__c = parentRecordId,
                wrts_prcgvr__Description__c = errorMessage,
                wrts_prcgvr__IsRequired__c = true,
                OwnerId = ownerId
        );
        databaseSrv.insertSObject(discardActivity);
        return discardActivity;
    }

    public wrts_prcgvr__Activity__c insertDataEntryActivity(Id parentRecordId, Id ownerId, String description, String fieldsTemplateCode, Boolean required) {
        FieldsTemplate__c template = new FieldsTemplate__c(Code__c = fieldsTemplateCode);
        wrts_prcgvr__Activity__c dataEntryActivity = new wrts_prcgvr__Activity__c(
                Type__c = constantsUtl.ACTIVITY_DATA_ENTRY_TYPE,
                wrts_prcgvr__ObjectId__c = parentRecordId,
                wrts_prcgvr__Description__c = description,
                wrts_prcgvr__IsRequired__c = required,
                FieldsTemplate__r = template,
                OwnerId = ownerId
        );
        databaseSrv.insertSObject(dataEntryActivity);
        return dataEntryActivity;
    }

    /**
    *
    *
    * @param parentRecordId
    * @param description
    * @param tokenId
    * @param documentBundleId
    *
    * @return
    */
    public wrts_prcgvr__Activity__c insertTouchPointActivity(Id parentRecordId, String description,
            Id tokenId, Id documentBundleId) {

        return insertTouchPointActivity(parentRecordId, description, null,
                tokenId, documentBundleId);
    }

    /**
    *
    *
    * @param parentRecordId
    * @param description
    * @param touchPointConfigurationDeveloperName
    * @param tokenId
    * @param documentBundleId
    *
    * @return
    */
    public wrts_prcgvr__Activity__c insertTouchPointActivity(Id parentRecordId, String description,
            String touchPointConfigurationDeveloperName, Id tokenId, Id documentBundleId) {

        wrts_prcgvr__Activity__c touchPointActivity = new wrts_prcgvr__Activity__c(
                Type__c = constantsUtl.ACTIVITY_TOUCHPOINT_TYPE,
                wrts_prcgvr__ObjectId__c = parentRecordId,
                wrts_prcgvr__Description__c = description,
                ExternalAccessIdentifier__c = tokenId,
                DocumentBundle__c = documentBundleId,
                wrts_prcgvr__IsRequired__c = false
        );
        System.debug(touchPointActivity);
        databaseSrv.insertSObject(touchPointActivity);
        return touchPointActivity;
    }

    /**
   *
   *
   * @param parentRecordId
   * @param description
   * @param tokenId
   * @param documentBundleId
   *
   * @return touchPointActivity
   */
    public wrts_prcgvr__Activity__c instantiateTouchPointActivity(Dossier__c dossierRecord, String description, Id tokenId, Id documentBundleId) {
        wrts_prcgvr__Activity__c touchPointActivity = new wrts_prcgvr__Activity__c(
                Type__c = constantsUtl.ACTIVITY_PRINT_TYPE,
                wrts_prcgvr__ObjectId__c = dossierRecord.Id,
                wrts_prcgvr__Description__c = description,
                ExternalAccessIdentifier__c = tokenId,
                DocumentBundle__c = documentBundleId,
                wrts_prcgvr__IsRequired__c = false,
                SendingChannel__c = dossierRecord.SendingChannel__c,
                Email__c = dossierRecord.Email__c
        );
        return touchPointActivity;
    }

    /**
     * @param parentRecordId
     * @param recordWithAddress
     * @param description
     * @return
     */
    public wrts_prcgvr__Activity__c instantiateAddressVerificationActivity(
            Id parentObjectId,
            SObject record,
            String fieldsPrefix,
            String description) {

        wrts_prcgvr__Activity__c addressVerificationActivity = new wrts_prcgvr__Activity__c(
                Type__c = constantsUtl.ACTIVITY_ADDRESS_VERIFICATION_TYPE,
                AddressType__c = fieldsPrefix,
                AddressVerificationObjectId__c = record.Id,
                wrts_prcgvr__ObjectId__c = parentObjectId,
                wrts_prcgvr__Description__c = String.isNotBlank(description) ? description :
                        'Address Verification',
                wrts_prcgvr__IsRequired__c = true
        );

        MRO_SRV_Address.copyAddress(
                record, //sourceRecord
                fieldsPrefix, //sourceRecordFieldsPrefix
                addressVerificationActivity, //destinationRecord
                'Sending' //destinationRecordFieldsPrefix
        );

        System.debug(addressVerificationActivity);

        return addressVerificationActivity;

    }


    /**
     * @param activity
     * @return void
     */
    public  void updateActivityStatus(wrts_prcgvr__Activity__c activity, String status){

        if(activity != null){
            activity.wrts_prcgvr__Status__c = status;
            databaseSrv.updateSObject(activity);
        }
    }
}