/**
* Trigger handler class for Supply Object
* @author Guerini Ivano
* 
*/
public with sharing class SupplyTrgHnd implements TriggerManager.ISObjectTriggerHandler {
    static SupplyService supplySrv = SupplyService.getInstance();
    
    public void beforeInsert() {}
    public void beforeDelete() {}
    public void beforeUpdate() {}
    
    public void afterInsert() {
        Set<Id> servicePointInvolved = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.new) {
            if (supply.ServicePoint__c != null) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
    }

    public void afterUpdate() {
        Set<Id> servicePointInvolved = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.new) {
            if ( (supply.ServicePoint__c != null)
              && ( (supply.Status__c != ((Supply__c)Trigger.oldMap.get(supply.Id)).Status__c)
              || (supply.ServicePoint__c != ((Supply__c)Trigger.oldMap.get(supply.Id)).ServicePoint__c) )
               ) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
    }

    public void afterDelete() {
        Set<Id> servicePointInvolved = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.old) {
            if (supply.ServicePoint__c != null) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
    }

    public void afterUndelete() {
        Set<Id> servicePointInvolved = new Set<Id>();
        for (Supply__c supply : (List<Supply__c>)Trigger.new) {
            if (supply.ServicePoint__c != null) {
                servicePointInvolved.add(supply.ServicePoint__c);
            }
        }
        supplySrv.actualizeSupplyOnServicePoint(servicePointInvolved);
    }


}