/**
 * Created by napoli on 18/11/2019.
 */

public with sharing class MRO_SRV_Individual {

    private static final MRO_SRV_Contact contactSrv = MRO_SRV_Contact.getInstance();
    private static final AccountContactRelationService accountContactRelationSrv = AccountContactRelationService.getInstance();
    private static final MRO_SRV_Account accountSrv = MRO_SRV_Account.getInstance();
    private static final MRO_SRV_Interaction interactionSrv = MRO_SRV_Interaction.getInstance();
    private static final MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();

    private static final ContactQueries contactQuery = ContactQueries.getInstance();
    private static final MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static final MRO_QR_CustomerInteraction customerInteractionQuery = MRO_QR_CustomerInteraction.getInstance();
    private static final AccountContactRelationQueries accountContactRelationQuery = AccountContactRelationQueries.getInstance();
    private static final MRO_QR_Individual individualQuery = MRO_QR_Individual.getInstance();

    public static MRO_SRV_Individual getInstance() {
        return (MRO_SRV_Individual)ServiceLocator.getInstance(MRO_SRV_Individual.class);
    }

    public List<Individual> searchIndividual(Interaction__c interaction) {
//        interactionSrv.updateInteraction(interaction);
        List<QueryBuilder.Condition> contactConditionList = new List<QueryBuilder.Condition>();
        if (String.isNotBlank(interaction.InterlocutorEmail__c)) {
            contactConditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.Email, interaction.InterlocutorEmail__c));
        }
        if (String.isNotBlank(interaction.InterlocutorPhone__c)) {
            contactConditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.Phone, interaction.InterlocutorPhone__c));
        }
        QueryBuilder contactQueryBuilder = QueryBuilder.getInstance()
                .setFields(new List<SObjectField>{Contact.Email, Contact.Phone, Contact.IndividualId})
                .addField(Contact.IndividualId, Individual.Name).addField(Contact.IndividualId, Individual.NationalIdentityNumber__c)
                .addField(Contact.IndividualId, Individual.FirstName).addField(Contact.IndividualId, Individual.LastName)
                .setObject(Contact.sObjectType)
                .setCondition(contactConditionList, QueryBuilder.AND_OPERATOR);

        List<Schema.SObjectType> searchBy = searchBy(interaction);
        if (searchBy.contains(Individual.sObjectType)) {
            List<QueryBuilder.Condition> conditionList = new List<QueryBuilder.Condition>();
            if (String.isNotBlank(interaction.InterlocutorFirstName__c)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Individual.FirstName, interaction.InterlocutorFirstName__c));
            }
            if (String.isNotBlank(interaction.InterlocutorLastName__c)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Individual.LastName, interaction.InterlocutorLastName__c));
            }
            if (String.isNotBlank(interaction.InterlocutorNationalIdentityNumber__c)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Individual.NationalIdentityNumber__c, interaction.InterlocutorNationalIdentityNumber__c));
            }

            QueryBuilder individualQueryBuilder = QueryBuilder.getInstance()
                    .setObject(Individual.sObjectType)
                    .setFields(new List<SObjectField>{
                            Individual.Id, Individual.Name, Individual.FirstName, Individual.LastName, Individual.NationalIdentityNumber__c
                    })
                    .setCondition(conditionList, QueryBuilder.AND_OPERATOR);

            if (searchBy.contains(Contact.sObjectType)) {
                individualQueryBuilder.setSubQuery(contactQueryBuilder);
            }
            return individualQueryBuilder.execute();
        } else if (searchBy.contains(Contact.sObjectType)) {
            List<Contact> contactList = contactQueryBuilder.execute();
            return extractFromContact(contactList);
        }
        return new List<Individual>();
    }
    //FF Customer Creation - Pack1/2 - Interface Check
    public List<ApexServiceLibraryCnt.Option> listOptions(SObjectField sObjField) {
        List<ApexServiceLibraryCnt.Option> optionList = new List<ApexServiceLibraryCnt.Option>();
        List<PicklistEntry> picklistEntryList = sObjField.getDescribe().getPicklistValues();
        for (PicklistEntry entry : picklistEntryList) {
            if (!entry.isActive()) {
                continue;
            }
            optionList.add(new ApexServiceLibraryCnt.Option(entry.getLabel(), entry.getValue()));
        }
        return optionList;
    }
    //FF Customer Creation - Pack1/2 - Interface Check

    /*
    public List<Individual> searchIndividual(Contact contactObj) {
        List<QueryBuilder.Condition> contactConditionList = new List<QueryBuilder.Condition>();
        if (String.isNotBlank(contactObj.Email)) {
            contactConditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.Email, contactObj.Email));
        }
        if (String.isNotBlank(contactObj.Phone)) {
            contactConditionList.add(new QueryBuilder.FieldCondition().likeOp(Contact.Phone, contactObj.Phone));
        }
        QueryBuilder contactQueryBuilder = QueryBuilder.getInstance()
            .setFields(new List<SObjectField>{Contact.Email, Contact.Phone, Contact.IndividualId})
            .addField(Contact.IndividualId, Individual.Name).addField(Contact.IndividualId, Individual.NationalIdentityNumber__c)
            .addField(Contact.IndividualId, Individual.FirstName).addField(Contact.IndividualId, Individual.LastName)
            .setObject(Contact.sObjectType)
            .setCondition(contactConditionList, QueryBuilder.AND_OPERATOR);

        List<Schema.SObjectType> searchBy = searchBy(contactObj);
        if (searchBy.contains(Individual.sObjectType)) {
            List<QueryBuilder.Condition> conditionList = new List<QueryBuilder.Condition>();
            if (String.isNotBlank(contactObj.FirstName)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Individual.Name, contactObj.FirstName));
            }
            if (String.isNotBlank(contactObj.NationalIdentityNumber__c)) {
                conditionList.add(new QueryBuilder.FieldCondition().likeOp(Individual.NationalIdentityNumber__c, contactObj.NationalIdentityNumber__c));
            }

            QueryBuilder individualQueryBuilder = QueryBuilder.getInstance()
                .setObject(Individual.sObjectType)
                .setFields(new List<SObjectField>{
                   Individual.Id, Individual.Name, Individual.FirstName, Individual.LastName, Individual.NationalIdentityNumber__c
                })
                .setCondition(conditionList, QueryBuilder.AND_OPERATOR);

            if (searchBy.contains(Contact.sObjectType)) {
                individualQueryBuilder.setSubQuery(contactQueryBuilder);
            }
            return individualQueryBuilder.execute();
        } else if (searchBy.contains(Contact.sObjectType)) {
            List<Contact> contactList = contactQueryBuilder.execute();
            return extractFromContact(contactList);
        }
        return new List<Individual>();
    }
    */

    /**
     * @author  BG
     * @description Method to find a duplicate individuals by NationalIdentityNumber
     * @param nationalIdentityNumber
     *
     * @return ids of duplicate individuals
     */
// TODO: ST unused
//    public String findDuplicateIndividual(String nationalIdentityNumber) {
//        List<Individual> duplicateIndividuals = individualQuery.findIndividualsByNINumber(nationalIdentityNumber);
//        String listIds = '';
//        if (!duplicateIndividuals.isEmpty()) {
//            for (Integer i = 0; i < duplicateIndividuals.size(); i++) {
//                listIds += duplicateIndividuals[i].Id + ',';
//            }
//        }
//        return listIds;
//    }

    /**
     * @author  MF
     * @description Method to find a duplicate individuals by NationalIdentityNumber for existing individual
     * @param nationalIdentityNumber
     *
     * @return ids of duplicate individuals
     */
    public Boolean checkIndividualDuplicate(MRO_SRV_Individual.Interlocutor interlocutor) {
        Individual individualItem=individualQuery.findById(interlocutor.id);
        List<Individual> duplicateIndividuals = individualQuery.findIndividualsByNINumber(interlocutor.nationalId);
        return (interlocutor.id!=duplicateIndividuals.get(0).id);
    }

    /**
     * @author BG
     * @description Method to update individual Object particularly TechnicalDetails that contains Phone and email information
     *
     * @param individual Map fields of individual object
     * @param hasCustomer true if at least one customer exists
     * @return new Individual Id
     */
    public Individual insertInterlocutor(MRO_SRV_Individual.Interlocutor individual, Boolean hasCustomer) {
        Individual newIndividual = new Individual(
            FirstName = individual.firstName,
            LastName = individual.lastName,
            NationalIdentityNumber__c = individual.nationalId,
            ForeignCitizen__c = individual.ForeignCitizen,
            Gender__c = individual.gender,
            BirthCity__c = individual.birthCity,
            BirthDate = individual.birthDate,
                //FF Customer Creation - Pack1/2 - Interface Check
            IDDocEmissionDate__c = individual.iDDocEmissionDate,
            IDDocValidityDate__c = individual.iDDocValidityDate,
            IDDocumentNr__c = individual.iDDocumentNr,
            IDDocumentType__c = individual.iDDocumentType,
            IDDocumentSeries__c = individual.iDDocumentSeries,
            IDDocIssuer__c =  individual.iDDocIssuer
                //FF Customer Creation - Pack1/2 - Interface Check
        );

            newIndividual.TechnicalDetails__c = JSON.serializePretty(new Map<String, String>{

                    'Phone' => individual.phone,
                    'Email' => individual.email,
                    //FF Customer Creation - Pack1/2 - Interface Check
                    'Mobile' => individual.mobile,
                    'ContactChannel' => individual.contactChannel
                    //FF Customer Creation - Pack1/2 - Interface Check
            });
        system.debug('gn****' + newIndividual);
        databaseSrv.insertSObject(newIndividual);
        return newIndividual;
    }

    /**
     * @author MF
     * @description Method to update individual Object particularly TechnicalDetails that contains Phone and email information
     *
     * @param individual Map fields of individual object
     * @param hasCustomer true if at least one customer exists
     * @return new Individual Id
     */
    public String updateIndividual(MRO_SRV_Individual.Interlocutor individual, Boolean hasCustomer) {
        Individual oldIndividual = new Individual(Id=individual.Id, FirstName = individual.firstName, LastName = individual.lastName, NationalIdentityNumber__c = individual.nationalId);
        Map<String, String> fields=new Map<String, String>();
        if(String.isNotBlank(individual.phone)){
            fields.put('Phone', individual.phone);
        }
        if(String.isNotBlank(individual.email)){
            fields.put('Email', individual.email);
        }
        if (!hasCustomer) {
            oldIndividual.TechnicalDetails__c = JSON.serializePretty(fields);
        }
        databaseSrv.updateSObject(oldIndividual);
        return oldIndividual.Id;
    }
/*
    private List<Interlocutor> mapFrom(List<Individual> individualList, List<SObjectType> searchBy) {
        List<Interlocutor> interlocutorList = new List<Interlocutor>();
        for (Individual individualObj : individualList) {
            for (Contact individualContact  : individualObj.Contacts) {
                interlocutorList.add(new Interlocutor(individualObj, individualContact));
            }
            if (!searchBy.contains(Contact.sObjectType)) {
                interlocutorList.add(new Interlocutor(individualObj, null));
            }
        }
        return interlocutorList;
    }*/

    private List<Individual> extractFromContact(List<Contact> contactList) {
        Map<String, Individual> individualList = new Map<String, Individual>();
        for (Contact contactObj : contactList) {
            individualList.put(contactObj.IndividualId, contactObj.Individual);
        }
        return individualList.values();
    }

    public List<SObjectType> searchBy(Interaction__c searchForm) {
        List<Schema.SObjectType> searchByObjectList = new List<SObjectType>();
        if (String.isNotBlank(searchForm.InterlocutorFirstName__c) || String.isNotBlank(searchForm.InterlocutorLastName__c) || String.isNotBlank(searchForm.InterlocutorNationalIdentityNumber__c)) {
            searchByObjectList.add(Individual.sObjectType);
        }
        if (String.isNotBlank(searchForm.InterlocutorEmail__c) || String.isNotBlank(searchForm.InterlocutorPhone__c)) {
            searchByObjectList.add(Contact.sObjectType);
        }
        return searchByObjectList;
    }
// TODO: ST Unused
//    public List<SObjectType> searchBy(Contact searchForm) {
//        List<Schema.SObjectType> searchByObjectList = new List<SObjectType>();
//        if (String.isNotBlank(searchForm.FirstName) || String.isNotBlank(searchForm.LastName) || String.isNotBlank(searchForm.NationalIdentityNumber__c)) {
//            searchByObjectList.add(Individual.sObjectType);
//        }
//        if (String.isNotBlank(searchForm.Email) || String.isNotBlank(searchForm.Phone)) {
//            searchByObjectList.add(Contact.sObjectType);
//        }
//        return searchByObjectList;
//    }

    public Boolean findOrCreateSameContact(String individualId, String interactionId) {
        List<CustomerInteraction__c> customerInteractionList = customerInteractionQuery.listCustomerInteraction(interactionId);
        if (customerInteractionList.isEmpty()) {
            List<AccountContactRelation> relations = accountContactRelationQuery.listByIndividualId(individualId);
            if (relations.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
        Individual individualObj = individualQuery.findById(individualId);
        if (individualObj == null) {
            throw new WrtsException(System.Label.Invalid + ' - ' + System.Label.Individual + ' ' + individualId);
        }
        createRelations(customerInteractionList, individualObj);
        return true;
    }

    public Map<String, Object> saveIndividual(Interlocutor individualDTO, String interactionId, Boolean isInterlocutorUpgrade) {
        Map<String, Object> response = new Map<String, Object>();
        Savepoint sp = Database.setSavepoint();
        try {
            List<CustomerInteraction__c> customerInteractionList = new List<CustomerInteraction__c>();
            if (String.isNotBlank(interactionId)) {
                customerInteractionList = customerInteractionQuery.listByInteractionId(interactionId);
            }
            Boolean hasCustomer = !customerInteractionList.isEmpty();

            Boolean isPartSaveAllowed = SettingProvider.isSaveUnIdentifiedInterlocutorAllowed();
            if (String.isNotBlank(interactionId) && isPartSaveAllowed && !individualDTO.isRequiredFieldsFilled() && !isInterlocutorUpgrade) {
                interactionSrv.updateIndividualInfo(interactionId, individualDTO);
                response.put('hasCustomer', hasCustomer);
                return response;
            }

            List<Individual> existingIndividuals = individualQuery.findIndividualsByNINumber(individualDTO.nationalId);
            if (existingIndividuals.size() > 0) {
                response.put('duplicateIndividuals', existingIndividuals);
                return response;
            }

            Individual individual = insertInterlocutor(individualDTO, hasCustomer);
            individual = individualQuery.findById(individual.Id);
            individualDTO.id = individual.Id;

            interactionSrv.updateInteraction(interactionId, individualDTO.id);
            if (hasCustomer) {
                createRelations(customerInteractionList, individual);
            }
            response.put('individualId', individualDTO.id);
            response.put('hasCustomer', hasCustomer);

        } catch (Exception exc) {
            Database.rollback(sp);
            throw exc;
        }
        return response;
    }

    public void createRelations(List<CustomerInteraction__c> customerInteractionList,  Individual individualObj) {
        List<Contact> individualContactList = contactQuery.listContactByIndividualId(individualObj.Id);
        Set<String> individualContactIds = SobjectUtils.setNotNullByField(individualContactList, Contact.Id);

        List<Account> individualAccountList = accountQuery.listByIndividualId(individualObj.Id);
        if (individualAccountList.size() == 0) {
            individualAccountList = accountQuery.listByNationalId(individualObj.NationalIdentityNumber__c);
        }
        Set<String> individualPersonContactIds = SobjectUtils.setNotNullByField(individualAccountList, Account.PersonContactId);

        Set<String> allContactIds = new Set<String>();
        if (individualContactIds.size() > 0) {
            allContactIds.addAll(individualContactIds);
        }
        if (individualPersonContactIds.size() > 0) {
            allContactIds.addAll(individualPersonContactIds);
        }
        System.debug('allContactIds = ' + allContactIds);

        Map<String, sObject> accountToCInteraction = SobjectUtils.mapByFieldAndSobject(customerInteractionList, CustomerInteraction__c.Customer__c);
        List<AccountContactRelation> relationList = accountContactRelationQuery.listByAccountIds(accountToCInteraction.keySet());
        System.debug('relationList = ' + relationList);
        System.debug('AccountContactRelation.AccountId = ' + AccountContactRelation.AccountId);

        Map<String, List<sObject>> accountIdToRelationList = SobjectUtils.mapByFieldAndList(relationList, AccountContactRelation.AccountId);
        List<CustomerInteraction__c> customerInteractionToUpdate = new List<CustomerInteraction__c>();
        List<Account> personalAccountList = new List<Account>();
        List<String> businessAccountIds = new List<String>();

        for (String accountId : accountToCInteraction.keySet()) {
            List<AccountContactRelation> relationForAccount = (List<AccountContactRelation>)accountIdToRelationList.get(accountId);
            CustomerInteraction__c customerInteraction = (CustomerInteraction__c)accountToCInteraction.get(accountId);

            if (relationForAccount != null && relationForAccount.size() > 0) {
                Boolean found = false;
                for (AccountContactRelation relation : relationForAccount) {
                    if (allContactIds.contains(relation.ContactId)) {
                        customerInteraction.Contact__c = relation.ContactId;
                        customerInteractionToUpdate.add(customerInteraction);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    if (customerInteraction.Customer__r.IsPersonAccount) {
                        personalAccountList.add(customerInteraction.Customer__r);
                    } else {
                        businessAccountIds.add(customerInteraction.Customer__c);
                    }
                }
            } else {
                if (customerInteraction.Customer__r.IsPersonAccount) {
                    personalAccountList.add(customerInteraction.Customer__r);
                } else {
                    businessAccountIds.add(customerInteraction.Customer__c);
                }
            }
        }

        List<AccountContactRelation> acrList = new List<AccountContactRelation>();
        List<Account> personAccountToUpdate = new List<Account>();
        String contactId = null;
        Boolean createAcr = true;
        for (String businessAccountId : businessAccountIds) {
            if (contactId == null) {
                if (individualContactList.size() > 0) {
                    //TODO: it may be Private Contact without AccountId
                    contactId = individualContactList.get(0).Id;
                } else {
                    //FF Customer Creation - Pack1/2 - Interface Check
                    Contact newContact = contactSrv.create(individualObj,businessAccountId);
                    System.debug('newContact:' + newContact);
                    //FF Customer Creation - Pack1/2 - Interface Check
                    databaseSrv.insertSObject(newContact);
                    contactId = newContact.Id;
                    createAcr = false;
                }
            }
            if (createAcr) {
                acrList.add(accountContactRelationSrv.create(businessAccountId, contactId));
                createAcr = true;
            }
            CustomerInteraction__c customerInteraction = (CustomerInteraction__c)accountToCInteraction.get(businessAccountId);
            customerInteraction.Contact__c = contactId;
            customerInteractionToUpdate.add(customerInteraction);
        }

        String personalContactId = null;
        for (Account personAccount : personalAccountList) {
            if (personalContactId == null) {
                if (individualAccountList.size() > 0) {
                    Account firstPersonAccount = individualAccountList.get(0);
                    if (firstPersonAccount.PersonIndividualId == null) {
                        firstPersonAccount.PersonIndividualId = individualObj.Id;
                        databaseSrv.updateSObject(firstPersonAccount);
                    }
                    personalContactId = firstPersonAccount.PersonContactId;
                } else {
                    Account personalAccount = accountSrv.insertPersonalAccount(individualObj);
                    personalContactId = accountQuery.findAccount(personalAccount.Id).PersonContactId;
                }
            }
            if (personAccount.NationalIdentityNumber__pc == individualObj.NationalIdentityNumber__c) {
                personAccount.PersonIndividualId = individualObj.Id;
                personAccountToUpdate.add(personAccount);
                personalContactId = personAccount.PersonContactId;
            } else {
                acrList.add(accountContactRelationSrv.create(personAccount.Id, personalContactId));
            }
            CustomerInteraction__c customerInteraction = (CustomerInteraction__c)accountToCInteraction.get(personAccount.Id);
            customerInteraction.Contact__c = personalContactId;
            customerInteractionToUpdate.add(customerInteraction);
        }
        if (acrList.size() > 0) {
            databaseSrv.insertSObject(acrList);
        }
        if (personAccountToUpdate.size() > 0) {
            databaseSrv.updateSObject(personAccountToUpdate);
        }
        if (customerInteractionToUpdate.size() > 0) {
            databaseSrv.updateSObject(customerInteractionToUpdate);
        }
    }

    /*private String getSameContactId(List<Contact> contactList, Set<String> contactIds) {
        String sameContactId = null;
        Set<String> accountContactIds = new Set<String>();
        Set<String> individualContactIds = new Set<String>();
        for (Contact theContact : contactList) {
            if (String.isNotBlank(theContact.AccountId) && String.isNotBlank(theContact.IndividualId)) {
                sameContactId = theContact.Id;
                break;
            } else if (String.isNotBlank(theContact.AccountId)) {
                accountContactIds.add(theContact.Id);
            } else if (String.isNotBlank(theContact.IndividualId)) {
                individualContactIds.add(theContact.Id);
            }
        }

        if (sameContactId != null) {
            return sameContactId;
        }

        accountContactIds.addAll(contactIds);

        for (String individualContactId : individualContactIds) {
            if (accountContactIds.contains(individualContactId)) {
                sameContactId = individualContactId;
            }
        }

        if (sameContactId != null) {
            return sameContactId;
        } else {
            return null;
        }
    }*/
    // TODO: method not used in IndividualCnt
    public List<AccountContactRelation> listRelatedContact(String individualId) {
        List<AccountContactRelation> relations = accountContactRelationQuery.listByIndividualId(individualId);
        List<Account> personAccountList = accountQuery.listByIndividualId(individualId);
        for (Account personAccount : personAccountList) {
            relations.add(new AccountContactRelation(
                    Account = personAccount,
                    AccountId = personAccount.Id,
                    ContactId = personAccount.PersonContactId,
                    Roles = Label.Customer
            ));
        }
        return relations;
    }

//    public Interlocutor findUnidentified(String interactionId) {
//        Interaction__c interaction = interactionQuery.findInteractionById(interactionId);
//        return mapToDto(interaction);
//    }

    /* public static Individual mapFromDto(Interlocutor interlocutorDTO){
         Individual individuObj = new Individual();
         individuObj.FirstName = interlocutorDTO.firstname;
         individuObj.LastName = interlocutorDTO.lastname;
         individuObj.NationalIdentityNumber__c = interlocutorDTO.nationalId;
         individuObj.email = interlocutorDTO.email;
         individuObj.phone =  interlocutorDTO.Phone;
         return individuObj;
     }*/
    // TODO: method not used in IndividualCnt
    public static Interlocutor mapToDto(Individual individualObj){
        Interlocutor individualDto = new Interlocutor();
        individualDto.id = individualObj.Id;
        individualDto.name = individualObj.Name;
        individualDto.firstName = individualObj.FirstName;
        individualDto.lastName = individualObj.LastName;
        individualDto.nationalId = individualObj.NationalIdentityNumber__c;
        return individualDto;
    }

// TODO: ST Unused
//    public static Interlocutor mapToDto(Interaction__c interactionObj){
//        Interlocutor individualDto = new Interlocutor();
//        individualDto.firstName = interactionObj.InterlocutorFirstName__c;
//        individualDto.lastName = interactionObj.InterlocutorLastName__c;
//        individualDto.nationalId = interactionObj.InterlocutorNationalIdentityNumber__c;
//        individualDto.email = interactionObj.InterlocutorEmail__c;
//        individualDto.phone = interactionObj.InterlocutorPhone__c;
//        return individualDto;
//    }

    public with sharing class Interlocutor {
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String firstName {get; set;}
        @AuraEnabled
        public String lastName {get; set;}
        @AuraEnabled
        public String nationalId {get; set;}
        @AuraEnabled
        public String email {get; set;}
        @AuraEnabled
        public String phone {get; set;}
        @AuraEnabled
        public Boolean ForeignCitizen {get; set;}
        @AuraEnabled
        public String gender {get; set;}
        @AuraEnabled
        public Date birthDate {get; set;}
        @AuraEnabled
        public String birthCity {get; set;}
        //FF Customer Creation - Pack1/2 - Interface Check
        @AuraEnabled
        public Date iDDocEmissionDate {get; set;}
        @AuraEnabled
        public Date iDDocValidityDate {get; set;}
        @AuraEnabled
        public String iDDocumentNr {get; set;}
        @AuraEnabled
        public String iDDocumentType {get; set;}
        @AuraEnabled
        public String iDDocumentSeries {get; set;}
        @AuraEnabled
        public String iDDocIssuer {get; set;}
        @AuraEnabled
        public String contactChannel {get; set;}
        @AuraEnabled
        public String mobile {get; set;}
        //FF Customer Creation - Pack1/2 - Interface Check


        public Boolean isRequiredFieldsFilled() {
            return String.isNotBlank(firstName)
                    && String.isNotBlank(lastName)
                    && String.isNotBlank(nationalId);
        }

        // TODO: method not used in IndividualCnt
        public Interlocutor() {}
// TODO: ST Unused
//        public Interlocutor(Individual individualObj, Contact contactObj) {
//            this.id = individualObj.Id;
//            this.name = individualObj.Name;
//            this.firstName = individualObj.FirstName;
//            this.lastName = individualObj.LastName;
//            this.nationalId = individualObj.NationalIdentityNumber__c;
//            if (contactObj != null) {
//                this.email = contactObj.Email;
//                this.Phone = contactObj.Phone;
//            }
//        }
    }
}