/**
 * Created by Boubacar Sow  on 01/04/2020.
 */

public with sharing class MRO_LC_MeterDetailsQuery extends ApexServiceLibraryCnt {

    private static MRO_QR_Supply supplyQR = MRO_QR_Supply.getInstance();

    public with sharing class listMeterDetails extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            MeterDetailsRequestParam params = (MeterDetailsRequestParam) JSON.deserialize(jsonInput, MeterDetailsRequestParam.class);
            if (params.startDate == null) {
                throw new WrtsException(System.Label.StartDateIsEmpty);
            }
            if (params.endDate == null) {
                throw new WrtsException(System.Label.EndDateIsEmpty);
            }
            if (params.recordId == null) {
                throw new WrtsException(System.Label.MissingServiceId);
            }
            return listMeterDetails(params);
        }
    }

    public static Map<String, List<Object>> listMeterDetails(MeterDetailsRequestParam params) {

        Map<String, List<Object>> returnMap = new Map<String, List<Object>>();
        String recordId = params.recordId;
        List<Id> newIdList = new List<Id>();
        newIdList.add(Id.valueOf(params.selectedSupplyId));
        List<Supply__c> supplyList = supplyQR.getSuppliesByIds(newIdList);
        Supply__c selectedSupply = supplyList.isEmpty() ? null : supplyList[0];
        System.debug('### selectedSupply '+ selectedSupply);
        Integer i = 0;

        List<MRO_LC_MeterDetailsQuery.MeterDetails> meterDetailsList = new List<MRO_LC_MeterDetailsQuery.MeterDetails>();

        MRO_QR_ServicePoint spQuery = MRO_QR_ServicePoint.getInstance();
        ServicePoint__c servicePoint = spQuery.getById(recordId);

        if (params.isQueryIndex.equals('true')) {
            System.debug('$$$ SM >> QUERY INDEX...');

            Date startDate = parseStringDate(params.startDate);
            Date endDate = parseStringDate(params.endDate);

            MRO_SRV_SapSelfReadingCallOut.QueryIndexResponseDTO queryIndexResponseDTO = new MRO_SRV_SapSelfReadingCallOut.QueryIndexResponseDTO();

            if (MRO_SRV_SapSelfReadingCallOut.checkSapEnabled()) {

                queryIndexResponseDTO = MRO_SRV_SapSelfReadingCallOut.getInstance().getQueryIndexResponse(servicePoint.ENELTEL__c, startDate, endDate);
                System.debug('$$$ SM >> queryIndexResponseDTO: ' + queryIndexResponseDTO);

            } else {

                queryIndexResponseDTO = MRO_SRV_SapSelfReadingCallOut.getInstance().getQueryIndexResponseDummy(servicePoint.ENELTEL__c, startDate, endDate);
                System.debug('$$$ SM >> queryIndexResponseDummy: ' + queryIndexResponseDTO);

            }
            meterDetailsList.add(new MeterDetails(
                    queryIndexResponseDTO.MeterSerialNumber,
                    selectedSupply.ServicePoint__c,
                    selectedSupply.ServicePoint__r.Code__c,
                    selectedSupply.ServicePoint__r.ENELTEL__c,
                    queryIndexResponseDTO.ReadingRoute,
                    selectedSupply.Status__c,
                    queryIndexResponseDTO.LastInvoiceStatus,
                    selectedSupply.Name,
                    queryIndexResponseDTO.MeterType,
                    queryIndexResponseDTO.SmartMeter,
                    queryIndexResponseDTO.LastInvoiceDate,
                    queryIndexResponseDTO.LastInvoicedReading,
                    queryIndexResponseDTO.LastInvoicedReadingDate,
                    queryIndexResponseDTO.readings
            ));
            System.debug('$$$ SM >> meterDetailsList: ' + meterDetailsList);

            List<Readings> readingsList = new List<Readings>();

            if (!meterDetailsList.isEmpty()) {

                for (MRO_SRV_SapSelfReadingCallOut.ReadingDTO readingDTO : meterDetailsList[0].readings) {

                    readingsList.add(new Readings(
                            readingDTO.dialId,
                            readingDTO.readingDate,
                            readingDTO.type,
                            readingDTO.indexIntegerPart,
                            readingDTO.index,
                            readingDTO.invoiced,
                            readingDTO.amount,
                            readingDTO.imageLink
                    ));
                }
            }
            System.debug('$$$ SM >> readingsList: ' + readingsList);
            returnMap.put('firstTableData' , meterDetailsList);
            returnMap.put('secondTableData' , readingsList);

        } else {

            System.debug('$$$ SM >> QUERY IVR GAS...');

            MRO_LC_IvrGaz.IvrGazRequestParam calloutParams = new MRO_LC_IvrGaz.IvrGazRequestParam();
            calloutParams.enelTel = servicePoint.ENELTEL__c;
            calloutParams.startDate = params.startDate;
            calloutParams.endDate = params.endDate;

            List<MRO_LC_IvrGaz.Index> indexes = new List<MRO_LC_IvrGaz.Index>();

            if (MRO_SRV_SapQueryIvrGazCallOut.checkSapEnabled()) {

                MRO_LC_IvrGaz.IvrGazResponse ivrGazResponse = MRO_SRV_SapQueryIvrGazCallOut.getInstance().getList(calloutParams);

                for (MRO_LC_IvrGaz.Index index : ivrGazResponse.indexes) {

                    if (index.meterSerialNumber != null) {
                        indexes.add(index);
                    }
                }
            } else {

                for (MRO_LC_IvrGaz.Index index : MRO_LC_IvrGaz.listDummyIndex(calloutParams).indexes) {

                    if (index.meterSerialNumber != null) {
                        indexes.add(index);
                    }
                }
            }
            returnMap.put('firstTableData' , indexes);
        }
        return returnMap;
    }

    public with sharing class getDetails extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String meterDetailsId = params.get('meterDetailsId');
            if (String.isBlank(meterDetailsId)) {
                throw new WrtsException('meterDetailsId' + ' is empty');
            }
            return listDetails(meterDetailsId);
        }
    }
    public static List<Details> listDetails(String meterDetailsId) {
        List<MRO_LC_MeterDetailsQuery.Details> detailsList = new List<MRO_LC_MeterDetailsQuery.Details>();
        for (Integer i = 0; i < 5; i++) {
            detailsList.add(new Details(
                    '###0000' + i,
                    '###0000' + i,
                    Math.mod(i, 2) == 0,
                    Date.today().addDays(i),
                    Label.LastIndexInvoiced+''+ i,
                    Date.today().addDays(i)
            ));
        }
        return detailsList;
    }

    // SM >> [ENLCRO-1841] Implementing the Meter Details query at Service Site object level
    public with sharing class listSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SuppliesRequestParam params = (SuppliesRequestParam) JSON.deserialize(jsonInput, SuppliesRequestParam.class);
            if (String.isEmpty(params.serviceSiteId)) {
                throw new WrtsException('Missing request parameters');
            }
            List<Supply__c> supplies = MRO_QR_Supply.getInstance().getActiveSuppliesByServiceSiteIds(new Set<String>{params.serviceSiteId});
            return supplies;
        }
    }
    public class SuppliesRequestParam {
        @AuraEnabled
        public String serviceSiteId { get; set; }
    }
    // SM << [ENLCRO-1841] Implementing the Meter Details query at Service Site object level

    private static Date parseStringDate(String stringDate){
        if(stringDate.length() == 8){
            String year = stringDate.substring(0, 4);
            System.debug(year);
            String month = stringDate.substring(4, 6);
            System.debug(month);
            String day = stringDate.substring(6, 8);
            System.debug(day);
            return Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
        }
        return MRO_SRV_SapQueryHelper.parseDate(stringDate);
    }


    public class MeterDetailsRequestParam {
        @AuraEnabled
        public String startDate { get; set; }
        @AuraEnabled
        public String endDate { get; set; }
        @AuraEnabled
        public String recordId { get; set; }
        @AuraEnabled
        public String isQueryIndex { get; set; }
        @AuraEnabled
        public String selectedSupplyId { get; set; }

        public MeterDetailsRequestParam(String startDate, String endDate, String recordId, String isQueryIndex, String selectedSupplyId) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.recordId = recordId;
            this.isQueryIndex = isQueryIndex;
            this.selectedSupplyId = selectedSupplyId;
        }
    }

    public class MeterDetails {
        @AuraEnabled
        public String meterDetailsId { get; set; }
        @AuraEnabled
        public String servicePointId { get; set; }
        @AuraEnabled
        public String servicePointCode { get; set; }
        @AuraEnabled
        public String enelTel { get; set; }
        @AuraEnabled
        public String readingRoute { get; set; }
        @AuraEnabled
        public String status { get; set; }
        @AuraEnabled
        public String lastInvoiceStatus { get; set; }
        @AuraEnabled
        public String supplyName { get; set; }
        @AuraEnabled
        public String meterType { get; set; }
        @AuraEnabled
        public String smartMeter { get; set; }
        @AuraEnabled
        public String lastInvoiceDate { get; set; }
        @AuraEnabled
        public String lastInvoicedReading { get; set; }
        @AuraEnabled
        public String lastInvoicedReadingDate { get; set; }
        @AuraEnabled
        public List<MRO_SRV_SapSelfReadingCallOut.ReadingDTO> readings { get; set; }

        public MeterDetails(String meterDetailsId, String servicePointId, String servicePointCode, String enelTel, String readingRoute, String status, String lastInvoiceStatus,
                String supplyName, String meterType, String smartMeter, String lastInvoiceDate, String lastInvoicedReading, String lastInvoicedReadingDate,
                List<MRO_SRV_SapSelfReadingCallOut.ReadingDTO> readings) {
            this.meterDetailsId = meterDetailsId;
            this.servicePointId = servicePointId;
            this.servicePointCode = servicePointCode;
            this.enelTel = enelTel;
            this.readingRoute = readingRoute;
            this.status = status;
            this.lastInvoiceStatus = lastInvoiceStatus;
            this.supplyName = supplyName;
            this.meterType = meterType;
            this.smartMeter = smartMeter;
            this.lastInvoiceDate = lastInvoiceDate;
            this.lastInvoicedReading = lastInvoicedReading;
            this.lastInvoicedReadingDate = lastInvoicedReadingDate;
            this.readings = readings;
        }
    }

    public class Details {
        @AuraEnabled
        public String detailsId { get; set; }
        @AuraEnabled
        public String dialId { get; set; }
        @AuraEnabled
        public Boolean smartMeter { get; set; }
        @AuraEnabled
        public Date lastInvoiceDate { get; set; }
        @AuraEnabled
        public String LastInvoiceReading  { get; set; }
        @AuraEnabled
        public Date LastInvoiceReadingDate { get; set; }

        public Details(String detailsId, String dialId, Boolean smartMeter, Date lastInvoiceDate,
                String LastInvoiceReading, Date LastInvoiceReadingDate) {
            this.detailsId = detailsId;
            this.dialId = dialId;
            this.smartMeter = smartMeter;
            this.lastInvoiceDate = lastInvoiceDate;
            this.LastInvoiceReading = LastInvoiceReading;
            this.LastInvoiceReadingDate = LastInvoiceReadingDate;
        }
    }

    public class Readings {
        @AuraEnabled
        public String dial { get; set; }
        @AuraEnabled
        public String readingDate { get; set; }
        @AuraEnabled
        public String readingType { get; set; }
        @AuraEnabled
        public String indexIntegerPart { get; set; }
        @AuraEnabled
        public Double index { get; set; }
        @AuraEnabled
        public String invoiced { get; set; }
        @AuraEnabled
        public String amount { get; set; }
        @AuraEnabled
        public String imageLink { get; set; }

        public Readings(String dial, String readingDate, String readingType, String indexIntegerPart, Double index, String invoiced, String amount, String imageLink) {
            this.dial = dial;
            this.readingDate = readingDate;
            this.readingType = readingType;
            this.indexIntegerPart = indexIntegerPart;
            this.index = index;
            this.invoiced = invoiced;
            this.amount = amount;
            this.imageLink = imageLink;
        }
    }


}