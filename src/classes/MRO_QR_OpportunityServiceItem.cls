public inherited sharing class MRO_QR_OpportunityServiceItem {
    private static final MRO_UTL_Constants constantsUtl = MRO_UTL_Constants.getAllConstants();

    public static MRO_QR_OpportunityServiceItem getInstance() {
        return (MRO_QR_OpportunityServiceItem)ServiceLocator.getInstance(MRO_QR_OpportunityServiceItem.class);
    }

    public List<OpportunityServiceItem__c> listByServiceSiteAndProduct(Id serviceSite, Id accountId, Id productId){
        return [
            SELECT Id, Opportunity__c, Opportunity__r.ContractId
            FROM OpportunityServiceItem__c
            WHERE ServiceSite__c = :serviceSite
            AND Account__c = :accountId
            AND Product__c = :productId
            AND Opportunity__r.ContractId != null
            AND Opportunity__r.RequestType__c IN ('Partner Service', 'PartnerService')
            AND Opportunity__r.StageName = 'Closed Won'
        ];
    }

    public List<OpportunityServiceItem__c> listByOpportunityAndServicePoint(Id opportunityId, Id servicePointId, String servicePointName){
        return [
            SELECT Id, ServicePoint__c, ServicePointCode__c
            FROM OpportunityServiceItem__c
            WHERE Opportunity__c =: opportunityId AND (ServicePoint__c=: servicePointId OR ServicePointCode__c =: servicePointName)
        ];
    }

    /**
    * @author Luca Ravicini
    * @description get List of OpportunityServiceItem__c by opportunity
    * and Service Point Codes or Service Point Names
    * @since May 22, 2020
    * @param opportunityId
    * @param servicePointIds
    * @param servicePointNames
    * @return List<OpportunityServiceItem__c>
    */
    public List<OpportunityServiceItem__c> listByOpportunityAndServicePoints(Id opportunityId, Set<Id> servicePointIds, Set<String> servicePointNames){
        return [
                SELECT Id, ServicePoint__c, ServicePointCode__c
                FROM OpportunityServiceItem__c
                WHERE Opportunity__c =: opportunityId AND (ServicePoint__c IN: servicePointIds OR ServicePointCode__c IN: servicePointNames)
        ];
    }

    public List<OpportunityServiceItem__c> getOSIsByServicePointId(Id servicePointId) {
        return [
            SELECT Id, Name, RecordTypeId, RecordType.DeveloperName, RecordType.Name, Account__c,PointStreetId__c,PointAddress__c,Trader__c,
                ServicePointCode__c, ServicePoint__c, ContractAccount__c, BillingProfile__c,ServicePoint__r.CurrentSupply__c,
                ServicePoint__r.CurrentSupply__r.CompanyDivision__c, EstimatedConsumption__c,Distributor__c,Distributor__r.IsDisCoENEL__c,Distributor__r.VATNumber__c,
                Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c
            FROM OpportunityServiceItem__c
            WHERE ServicePoint__c = :servicePointId
        ];
    }

    public List<OpportunityServiceItem__c> getOSIsByOpportunityId(Id opportunityId) {
        return [
                SELECT Id, Name, RecordTypeId, RecordType.DeveloperName, RecordType.Name, Account__c, PointStreetId__c, Contract__c,PointProvince__c,PointStreetName__c,Trader__c,
                        ContractAccount__c, ContractAccount__r.Market__c, ContractAccount__r.ContractType__c, BillingProfile__c, ServicePointCode__c, ServicePoint__c,
                        ServicePoint__r.CurrentSupply__c, ServicePoint__r.CurrentSupply__r.Id, ServicePoint__r.CurrentSupply__r.Account__c,
                        ServicePoint__r.CurrentSupply__r.Contract__c, ServicePoint__r.CurrentSupply__r.Contract__r.ContractType__c, ServicePoint__r.CurrentSupply__r.ContractAccount__c,
                        ServicePoint__r.CurrentSupply__r.ServiceSite__c, ServicePoint__r.CurrentSupply__r.Market__c, ServicePoint__r.CurrentSupply__r.CompanyDivision__c,ServicePoint__r.CurrentSupply__r.InENELArea__c,
                        EstimatedConsumption__c, Product__c, ServiceSite__c, ServiceSite__r.ActiveSuppliesCountELE__c, ServiceSite__r.ActiveSuppliesCountGAS__c, ServiceSite__r.ActiveSuppliesCountVAS__c,
                        VoltageSetting__c, StartDate__c, EndDate__c,ApplicantNotHolder__c,
                        Distributor__r.IsDisCoENEL__c,Distributor__r.SelfReadingEnabled__c, CLC__c, NLC__c, SiteDescription__c, Product__r.CommercialProduct__r.RateType__c,Distributor__c,
                        NACEReference__c, PointAddress__c, Market__c, Contract__r.ContractType__c,Distributor__r.VATNumber__c,SupplyOperation__c,
                        PointStreetNumber__c, PointStreetNumberExtn__c,PointCountry__c, PointApartment__c,
                        PointStreetType__c, PointBuilding__c, PointCity__c, PointAddressNormalized__c, PointAddressKey__c, PointBlock__c,
                        PointFloor__c, PointLocality__c, PointPostalCode__c,ServicePoint__r.ENELTEL__c, Supply__c, ConsumptionCategory__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, IsPlaceholder__c
                FROM OpportunityServiceItem__c
                WHERE Opportunity__c = :opportunityId
        ];
    }

    public Map<Id, List<OpportunityServiceItem__c>> getOSIsByOpportunityIds(Set<Id> opportunityIds) {
        List<OpportunityServiceItem__c> osis = [
                SELECT Id, Name, RecordTypeId, RecordType.DeveloperName, RecordType.Name, Opportunity__c, Account__c, PointStreetId__c, Contract__c,Trader__c,
                        ContractAccount__c, ContractAccount__r.Market__c, ContractAccount__r.ContractType__c, BillingProfile__c, ServicePointCode__c, ServicePoint__c,
                        Supply__c, Supply__r.Status__c, Supply__r.Product__c,
                        ServicePoint__r.CurrentSupply__r.Id, ServicePoint__r.CurrentSupply__r.Account__c, ServicePoint__r.CurrentSupply__c,
                        ServicePoint__r.CurrentSupply__r.Status__c, ServicePoint__r.CurrentSupply__r.Product__c,
                        ServicePoint__r.CurrentSupply__r.Market__c, ServicePoint__r.CurrentSupply__r.CompanyDivision__c,
                        EstimatedConsumption__c, Product__c, ServiceSite__c, VoltageSetting__c, StartDate__c, EndDate__c,
                        Distributor__r.IsDisCoENEL__c, CLC__c, NLC__c, SiteDescription__c,ConsumptionCategory__c,ApplicantNotHolder__c,Distributor__r.VATNumber__c,SupplyOperation__c,Distributor__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c,
                        Product__r.CommercialProduct__c, Product__r.CommercialProduct__r.ProductTerm__c
                FROM OpportunityServiceItem__c
                WHERE Opportunity__c IN :opportunityIds
        ];
        Map<Id, List<OpportunityServiceItem__c>> result = new Map<Id, List<OpportunityServiceItem__c>>();
        for (Id oppId : opportunityIds) {
            result.put(oppId, new List<OpportunityServiceItem__c>());
        }
        for (OpportunityServiceItem__c osi : osis) {
            result.get(osi.Opportunity__c).add(osi);
        }
        return result;
    }

    /**
    * @author Luca Ravicini
    * @description get List of OpportunityServiceItem__c by Ids
    * @since Mar 6, 2020
    * @param osiIds
    * @return List<OpportunityServiceItem__c>
    */
    public List<OpportunityServiceItem__c> getOSIsByIds(Set<Id> osiIds) {
        return [
                SELECT Id, ServicePointCode__c, RecordType.DeveloperName, Product__c, Account__c,FlatRate__c,StartDate__c,EndDate__c,
                        Distributor__c, ContractAccount__c, Trader__c, Market__c, NonDisconnectable__c,NACEReference__c,
                        PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c,PointCountry__c, PointApartment__c,
                        PointStreetType__c, PointBuilding__c, PointCity__c,ApplicantNotHolder__c,
                        PointAddressNormalized__c, PointAddressKey__c, PointStreetId__c,
                        PointFloor__c, PointLocality__c, PointPostalCode__c,PointAddress__c,
                        PointProvince__c, Voltage__c,VoltageSetting__c, VoltageLevel__c, PressureLevel__c,
                        Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c,
                        AvailablePower__c, EstimatedConsumption__c,NonDisconnectableReason__c, IsNewConnection__c,
                        Opportunity__c, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Pricebook2Id,
                        Opportunity__r.Type, Opportunity__r.Account.Name,Opportunity__r.RequestedStartDate__c,
                        Opportunity__r.Origin__c,Opportunity__r.Channel__c, Opportunity__r.RequestType__c, Opportunity__r.SubProcess__c,
                        Opportunity__r.StageName, Opportunity__r.CloseDate, Opportunity__r.CompanyDivision__c,
                        ServicePoint__c, ServicePoint__r.CurrentSupply__c,ServicePoint__r.CurrentSupply__r.Account__c, BillingProfile__c,
                        ServiceSite__c,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,
                        SiteBuilding__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,SiteLocality__c,
                        SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,
                        ServicePoint__r.CurrentSupply__r.CompanyDivision__c,ServicePoint__r.CurrentSupply__r.InENELArea__c,
                        Distributor__r.IsDisCoENEL__c,Distributor__r.SelfReadingEnabled__c,
                        ConsumptionCategory__c,ContractAccount__r.Market__c,Distributor__r.VATNumber__c,SupplyOperation__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c
                FROM OpportunityServiceItem__c
                WHERE Id In :osiIds
        ];
    }

    public List<OpportunityServiceItem__c> getListOSIServiceSite(String accountId) {
	    List<AggregateResult> aggregateOSIs = [
                SELECT SiteAddressKey__c, MIN(Id) Id
                FROM OpportunityServiceItem__c
                WHERE (Account__c = :accountId) AND ServiceSite__c = NULL AND SiteAddressKey__c != NULL
                      AND (CLC__c != NULL OR NLC__c != NULL) AND Opportunity__r.StageName != :constantsUtl.OPPORTUNITY_STAGE_CLOSED_LOST
                GROUP BY SiteAddressKey__c, CLC__c, NLC__c
        ];
	    Set<Id> osiIds = new Set<Id>();
	    for (AggregateResult ar : aggregateOSIs) {
		    osiIds.add((Id) ar.get('Id'));
	    }
	    return this.getOSIServiceSites(osiIds);
    }

    public List<OpportunityServiceItem__c> listAvailableEleOSIServiceSites(Id opportunityId, Id distributorId, String nlc) {
	    List<AggregateResult> aggregateOSIs = [
                SELECT SiteAddressKey__c, MIN(Id) Id
                FROM OpportunityServiceItem__c
                WHERE Opportunity__c = :opportunityId AND ServiceSite__c = NULL AND SiteAddressKey__c != NULL AND
                      Opportunity__r.StageName != :constantsUtl.OPPORTUNITY_STAGE_CLOSED_LOST AND
                      ((Distributor__c = :distributorId AND (NLC__c = :nlc OR Distributor__r.IsDisCoENEL__c = TRUE)) OR NLC__c = NULL)
                GROUP BY SiteAddressKey__c, NLC__c, CLC__c
        ];
	    Set<Id> osiIds = new Set<Id>();
	    for (AggregateResult ar : aggregateOSIs) {
		    osiIds.add((Id) ar.get('Id'));
	    }
	    return this.getOSIServiceSites(osiIds);
    }

    public List<OpportunityServiceItem__c> listAvailableGasOSIServiceSites(Id opportunityId, Id distributorId, String clc) {
	    List<AggregateResult> aggregateOSIs = [
                SELECT SiteAddressKey__c, MIN(Id) Id
                FROM OpportunityServiceItem__c
                WHERE Opportunity__c = :opportunityId AND ServiceSite__c = NULL AND SiteAddressKey__c != NULL AND
                      Opportunity__r.StageName != :constantsUtl.OPPORTUNITY_STAGE_CLOSED_LOST AND
                      ((Distributor__c = :distributorId AND CLC__c = :clc) OR CLC__c = NULL)
                GROUP BY SiteAddressKey__c, NLC__c, CLC__c
        ];
	    Set<Id> osiIds = new Set<Id>();
	    for (AggregateResult ar : aggregateOSIs) {
		    osiIds.add((Id) ar.get('Id'));
	    }
	    return this.getOSIServiceSites(osiIds);
    }

    private List<OpportunityServiceItem__c> getOSIServiceSites(Set<Id> osiIds) {
        return [SELECT Id, ServiceSite__c, SiteAddress__c, SiteAddressKey__c, SiteAddressNormalized__c, SiteApartment__c,PointStreetId__c, PointAddress__c,
                       SiteBuilding__c, SiteBlock__c, SiteCity__c, SiteCountry__c, SiteFloor__c, SiteStreetType__c, SiteLocality__c, SitePostalCode__c,
                       SiteProvince__c, SiteStreetId__c, SiteStreetName__c, SiteStreetNumber__c, SiteStreetNumberExtn__c,Trader__c,
                       CLC__c, NLC__c, SiteDescription__c,Distributor__c,Distributor__r.IsDisCoENEL__c,Distributor__r.VATNumber__c,
                       Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c
                FROM OpportunityServiceItem__c
	            WHERE Id IN :osiIds];
    }
/*
    public List<OpportunityServiceItem__c> getListOsiBySiteAddressKey(Set<Id> osiIds) {
        return [
                SELECT Id, ServiceSite__c, SiteAddressKey__c
                FROM OpportunityServiceItem__c
                WHERE SiteAddressKey__c in :osiIds
        ];
    }
*/
    public List<OpportunityServiceItem__c> getByIds(Set<String> siteAddressKeyIds) {
        return [
                SELECT Id, ServiceSite__c, SiteAddressKey__c, SiteApartment__c, Account__c,SiteAddressNormalized__c,PointStreetId__c,PointAddress__c,Trader__c,
                        SiteBuilding__c, SiteBlock__c, SiteCity__c, SiteCountry__c, SiteFloor__c, SiteStreetType__c, SiteLocality__c, StartDate__c, EndDate__c,Distributor__r.VATNumber__c,
                        SitePostalCode__c, SiteProvince__c, SiteStreetId__c, SiteStreetName__c, SiteStreetNumber__c, SiteStreetNumberExtn__c,Distributor__c,Distributor__r.IsDisCoENEL__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c
                FROM OpportunityServiceItem__c
                WHERE Id in :siteAddressKeyIds
        ];
    }

    public OpportunityServiceItem__c getOsiAddress(String addressId) {
        return [
                SELECT Id, SiteApartment__c, SiteAddressNormalized__c, SiteAddressKey__c,PointStreetId__c,PointAddress__c,Trader__c,
                        SiteBuilding__c, SiteBlock__c, SiteCity__c, SiteCountry__c, SiteFloor__c, SiteStreetType__c, SiteLocality__c,Distributor__r.VATNumber__c,
                        SitePostalCode__c, SiteProvince__c, SiteStreetId__c, SiteStreetName__c, SiteStreetNumber__c, SiteStreetNumberExtn__c,Distributor__c,Distributor__r.IsDisCoENEL__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c
                FROM OpportunityServiceItem__c
                WHERE Id = :addressId Limit 1
        ];
    }

    public OpportunityServiceItem__c getById(String OsiId) {
        List<OpportunityServiceItem__c> osiList = [
                SELECT Id, ServicePointCode__c, RecordType.DeveloperName, Product__c, Account__c,FlatRate__c,StartDate__c,EndDate__c,
                        Distributor__c, Contract__c, ContractAccount__c, Trader__c, Market__c, NonDisconnectable__c,NACEReference__c, RecordType.Name,
                        PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c,PointCountry__c, PointApartment__c,
                        PointStreetType__c, PointBuilding__c, PointBlock__c, PointCity__c,ApplicantNotHolder__c,
                        PointAddressNormalized__c, PointAddressKey__c, PointStreetId__c,PointAddress__c,
                        PointFloor__c, PointLocality__c, PointPostalCode__c,
                        PointProvince__c, Voltage__c,VoltageSetting__c, VoltageLevel__c, PressureLevel__c,
                        Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c,
                        AvailablePower__c, EstimatedConsumption__c,NonDisconnectableReason__c, IsNewConnection__c,
                        Opportunity__c, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Pricebook2Id, Opportunity__r.RequestType__c,
                        Opportunity__r.Type, Opportunity__r.Account.Name,ServicePoint__r.CurrentSupply__r.InENELArea__c,
                        Opportunity__r.StageName, Opportunity__r.CloseDate, Opportunity__r.CompanyDivision__c, Opportunity__r.CompanyDivision__r.Trader__c,
                        Opportunity__r.SubProcess__c, Opportunity__r.RequestedStartDate__c, Opportunity__r.Origin__c, Opportunity__r.Channel__c,
                        ServicePoint__c, ServicePoint__r.CurrentSupply__c,ServicePoint__r.CurrentSupply__r.Account__c, BillingProfile__c,
                        ServiceSite__c,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,
                        SiteBuilding__c,SiteBlock__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,SiteLocality__c,
                        SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,
                        ServicePoint__r.CurrentSupply__r.CompanyDivision__c, Distributor__r.IsDisCoENEL__c,ContractAccount__r.Market__c,ConsumptionCategory__c,
                        ServicePoint__r.CurrentSupply__r.Market__c,Distributor__r.VATNumber__c,SupplyOperation__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c,ServicePoint__r.CurrentSupply__r.Status__c,SiteDescription__c,
                        IsPlaceholder__c
                FROM OpportunityServiceItem__c
                WHERE Id = :OsiId
        ];
        return osiList.get(0);
    }


    public OpportunityServiceItem__c getOsiId(String servicePointId,String opportunityId) {
        List<OpportunityServiceItem__c> osiList = [
                SELECT Id, ServicePointCode__c, RecordType.DeveloperName, Product__c, Account__c,FlatRate__c,StartDate__c,EndDate__c,
                        Distributor__c, Contract__c, ContractAccount__c, Trader__c, Market__c, NonDisconnectable__c,NACEReference__c,
                        PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c, PointApartment__c,
                        PointStreetType__c, PointBuilding__c, PointBlock__c, PointCity__c,PointAddressNormalized__c,
                        PointCountry__c, PointFloor__c, PointLocality__c, PointPostalCode__c,
                        PointProvince__c, PointAddressKey__c, PointStreetId__c,ContractAccount__r.Market__c,ApplicantNotHolder__c, PointAddress__c,
                                                                //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
	                    Voltage__c, VoltageLevel__c, PressureLevel__c,NonDisconnectableReason__c,IsNewConnection__c,
                                                                //[ENLCRO-657] OSI Edit - Pack3 - Interface Check
                        Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c,
                        AvailablePower__c, EstimatedConsumption__c,
                        Opportunity__c, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Pricebook2Id,
                        Opportunity__r.Type, Opportunity__r.Account.Name,
                        Opportunity__r.StageName, Opportunity__r.CloseDate, Opportunity__r.CompanyDivision__c,
                        ServicePoint__c, ServicePoint__r.CurrentSupply__c, BillingProfile__c,
                        ServiceSite__c,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,
                        SiteBuilding__c,SiteBlock__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,SiteLocality__c,Distributor__r.IsDisCoENEL__c,
                        SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,ConsumptionCategory__c,Distributor__r.VATNumber__c,SupplyOperation__c,
                        Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c
                FROM OpportunityServiceItem__c
                WHERE ServicePoint__c= :servicePointId AND Opportunity__c = :opportunityId
        ];

        return osiList.get(0);
    }

    public List<OpportunityServiceItem__c> listActivationOSIByServicePointCodes(Set<String> servicePointCodes, Id accountId) {
        return [SELECT Id, ServicePointCode__c,ServicePoint__c, Opportunity__c, RecordType.DeveloperName, Product__c,Trader__c,
                       ApplicantNotHolder__c, NACEReference__c, ContractAccount__c, Market__c, NonDisconnectable__c,
                       FlatRate__c, StartDate__c, NonDisconnectableReason__c, IsNewConnection__c, SupplyOperation__c
                FROM OpportunityServiceItem__c
                WHERE ServicePointCode__c IN :servicePointCodes
                    AND Opportunity__r.RequestType__c = 'Connection'
                    AND Opportunity__r.StageName != :constantsUtl.OPPORTUNITY_STAGE_CLOSED_LOST
                    AND Opportunity__r.AccountId = :accountId];
    }

    public List<OpportunityServiceItem__c> listOSIByServicePoints(Set<String> servicePointIds) {
        return [SELECT Id, ServicePointCode__c,ServicePoint__c, Opportunity__c, RecordType.DeveloperName, Product__c,
                       ApplicantNotHolder__c, Trader__c, Distributor__c, Distributor__r.IsDisCoENEL__c, ContractAccount__c,
                       Market__c, NonDisconnectable__c, FlatRate__c, StartDate__c, NonDisconnectableReason__c,
                       IsNewConnection__c, NACEReference__c, SupplyOperation__c
        FROM OpportunityServiceItem__c
        WHERE ServicePoint__c IN :servicePointIds
        AND Opportunity__r.StageName != :constantsUtl.OPPORTUNITY_STAGE_CLOSED_LOST];
    }

    public List<OpportunityServiceItem__c> listOSIByAccountIdAndCLCorNLC(Id accountId, String clc, String nlc) {
        String query = 'SELECT Id, CLC__c, NLC__c, SiteAddressKey__c '+
                       'FROM OpportunityServiceItem__c WHERE Opportunity__r.AccountId = :accountId '+
                            'AND Opportunity__r.StageName != \''+constantsUtl.OPPORTUNITY_STAGE_CLOSED_LOST+'\' '+
                            'AND ServiceSite__c = NULL AND (';
        if (!String.isBlank(clc)) {
            query += 'CLC__c = :clc';
            if (!String.isBlank(nlc)) {
                query += ' OR ';
            }
        }
        if (!String.isBlank(nlc)) {
            query += 'NLC__c = :nlc';
        }
        query += ')';
        return Database.query(query);
    }

    public Map<String,String> getMapKWhByConsumptionName() {
        Map<String, String> mapElectricConsumption = new Map<String, String>();
        List<ElectricAnnualConsumption__mdt> electricAnnualConsumptionList = new List<ElectricAnnualConsumption__mdt>([Select Label,EstimatedConsumption__c from ElectricAnnualConsumption__mdt]);

        for (ElectricAnnualConsumption__mdt electricConsumptionRecord : electricAnnualConsumptionList){
            mapElectricConsumption.put(electricConsumptionRecord.Label, electricConsumptionRecord.EstimatedConsumption__c);
        }
        return mapElectricConsumption;
    }

    public List<OpportunityServiceItem__c> getOSIListByOpportunityId(string opportunityId) {
        return [SELECT Id, ServicePointCode__c,ServicePoint__c, Opportunity__c, RecordType.DeveloperName, Product__c,ApplicantNotHolder__c,
                ContractAccount__c, Market__c, NonDisconnectable__c, FlatRate__c, StartDate__c, NonDisconnectableReason__c, IsNewConnection__c,
                Distributor__r.IsDisCoENEL__c,ConsumptionCategory__c,NACEReference__c,SupplyOperation__c,Trader__c,Distributor__r.VATNumber__c,
                Distributor__r.IsDisCoEle__c, Distributor__r.IsDisCoGas__c, Distributor__r.SelfReadingEnabled__c, EstimatedConsumption__c
        FROM OpportunityServiceItem__c
        WHERE Opportunity__c = :opportunityId
        ];
    }

}