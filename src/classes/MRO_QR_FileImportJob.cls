/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 08.05.20.
 */

public with sharing class MRO_QR_FileImportJob {

    public static MRO_QR_FileImportJob getInstance() {
        return new MRO_QR_FileImportJob();
    }

    public FileImportJob__c getById(String fijId) {
        List<FileImportJob__c> jobs = [
                SELECT Id, FileTemplate__c, FileTemplate__r.FileDelimiter__c, FileTemplate__r.AllOrNone__c, Status__c, SkipJob__c
                FROM FileImportJob__c
                WHERE Id = :fijId
        ];

        return jobs.size() > 0 ? jobs[0] : null;
    }

    public FileImportJob__c getByJobId(String jobId) {
        List<FileImportJob__c> jobs = [SELECT Id, SkipJob__c, FileTemplate__c, FileTemplate__r.AllOrNone__c, FileTemplate__r.FinishAction__c FROM FileImportJob__c WHERE JobID__c = :jobId];
        return jobs.size() > 0 ? jobs[0] : null;
    }

    public FileImportJob__c getByDocumentId(String documentId) {
        List<FileImportJob__c> jobs = [SELECT Id, SkipJob__c, FileTemplate__c, FileTemplate__r.AllOrNone__c, FileTemplate__r.FinishAction__c, Status__c, JobID__c FROM FileImportJob__c WHERE ContentDocumentId__c = :documentId];
        return jobs.size() > 0 ? jobs[0] : null;
    }

    public FileImportJob__c getByDossierId(String dossierId) {
        List<FileImportJob__c> jobs = [SELECT Id, SkipJob__c, FileTemplate__c, FileTemplate__r.AllOrNone__c, FileTemplate__r.FinishAction__c, Status__c, JobID__c FROM FileImportJob__c WHERE Dossier__c = :dossierId ORDER BY CreatedDate DESC LIMIT 1];
        return jobs.size() > 0 ? jobs[0] : null;
    }
}