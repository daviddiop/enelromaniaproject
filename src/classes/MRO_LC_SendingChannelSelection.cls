/**
 * Created by David DIOP on 28.02.2020.
 */

public with sharing class MRO_LC_SendingChannelSelection extends ApexServiceLibraryCnt{
    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    private static MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();
    private static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();
    private static MRO_QR_Account accQuery = MRO_QR_Account.getInstance();

    public with sharing class getDossierRecord extends AuraCallable {
        public override Object perform ( final String jsonInput ){
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            return dossierQuery.getById(dossierId);
        }
    }

    public with sharing class getBillingProfileAddress extends AuraCallable {
        public override Object perform (final String jsonInput){
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String contractAccountId = params.get('contractAccountId');
            String billingProfileId = params.get('billingProfileId');
            String billingProfileAddress = '';

            if(String.isNotBlank(contractAccountId)){
                ContractAccount__c contractAccountRecord = contractAccountQuery.getById(contractAccountId);
                if(contractAccountRecord != null){
                    billingProfileAddress = contractAccountRecord.BillingProfile__r.BillingAddress__c;
                }
            }else if(String.isNotBlank(billingProfileId)){
                BillingProfile__c billingProfileRecord = billingProfileQuery.getById(billingProfileId);
                if(billingProfileRecord != null){
                    billingProfileAddress = billingProfileRecord.BillingAddress__c;
                }
            }
            response.put('billingProfileAddress', billingProfileAddress);
            return response;
        }
    }

    public with sharing class getBillingProfileAddressByAccountId extends AuraCallable {
        public override Object perform (final String jsonInput){
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String billingProfileAddress = '';

            if(String.isNotBlank(accountId)){
                Map<Id, BillingProfile__c> billingProfileMap = billingProfileQuery.getLastBillingAddressesByAccountIds(new Set<Id>{accountId});
                if (!billingProfileMap.isEmpty()) {
                    billingProfileAddress = billingProfileMap.get(accountId).BillingAddress__c;
                }
            }
            response.put('billingProfileAddress', billingProfileAddress);
            return response;
        }
    }
    public class isBusinessClient extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.BillingProfile + ' - ' + System.Label.MissingId);
            }
            Account account = accQuery.findAccount(accountId);
            if (account != null) {
                if(account.RecordType.DeveloperName == 'Business' || account.RecordType.DeveloperName == 'BusinessProspect'){
                    response.put('isBusinessClient', true);
                }
                else{
                    response.put('isBusinessClient', false);
                }
            }
            return response;
        }
    }
}