/**
 * Created by goudiaby on 07/01/2020.
 * @version 1.0
 * @description Class Query Service for Origin Channel Selection
 *          [ENLCRO-325] Channel and Origin Selection - Implementation
 *
 */
public with sharing class MRO_QR_OriginChannel {

    /**
    * @description Get Class instance
    * @return instance MRO_QR_OriginChannel Class
    */
    public static MRO_QR_OriginChannel getInstance() {
        return (MRO_QR_OriginChannel) ServiceLocator.getInstance(MRO_QR_OriginChannel.class);
    }

    /**
    * @author  Baba GOUDIABY
    * @description get a List custom metadata Origin Channel Dependency from Org
    * [ENLCRO-325] Channel and Origin Selection - Implementation
    * @param objectApiName
    * @return List<OriginChannelDependency__mdt> List custom metadata Origin Channel Dependency
    */
    public List<OriginChannelDependency__mdt> getOriginChannelDependencies(String objectApiName) {
        return [
                SELECT Id,MasterLabel,ObjectApiName__c,
                        ChannelPicklistEntry__r.Label, ChannelPicklistEntry__r.CustomPermission__c,
                        OriginPicklistEntry__r.Label,OriginPicklistEntry__r.CustomPermission__c
                FROM OriginChannelDependency__mdt
                WHERE ObjectApiName__c = '' OR ObjectApiName__c = :objectApiName
                ORDER BY MasterLabel ASC
        ];
    }

    /**
    * @author  Baba GOUDIABY
    * @description get a List custom metadata Origin Channel Entries from Org
    * [ENLCRO-325] Channel and Origin Selection - Implementation
    * @return List<OriginChannelPicklistEntry__mdt> List custom metadata Origin Channel Picklist Entry
    */
    public List<OriginChannelPicklistEntry__mdt> getOriginChannelEntries() {
        return [
                SELECT Id,MasterLabel,QualifiedApiName,
                        CustomPermission__c,isNotSelectable__c
                FROM OriginChannelPicklistEntry__mdt
                ORDER BY MasterLabel ASC
        ];
    }

    public List<String> getWebChannels() {
        List<String> webChannelList = new List<String>();
        for (OriginChannelPicklistEntry__mdt webChannel : [
                SELECT MasterLabel
                FROM OriginChannelPicklistEntry__mdt
                WHERE QualifiedApiName
                        IN ('EnelRo','EnelEasy','MyEnel')
                LIMIT 999
        ]){
            webChannelList.add(webChannel.MasterLabel);
        }
        return webChannelList;
    }

    public List<String> getShopAndSalesChannels() {
        List<String> shopAndSalesChannelList = new List<String>();
        for (OriginChannelPicklistEntry__mdt shopAndSalesChannel : [
                SELECT MasterLabel
                FROM OriginChannelPicklistEntry__mdt
                WHERE QualifiedApiName
                        IN ('MagazinEnel','MagazinEnelPartner','InfoKiosk','BackOfficeSales','BackOfficeCustomerCare','DirectSales')
                LIMIT 999
        ]){
            shopAndSalesChannelList.add(shopAndSalesChannel.MasterLabel);
        }
        return shopAndSalesChannelList;
    }

    public List<String> getPartnerAndCallCenterChannels() {
        List<String> partnerAndCallCenterChannelList = new List<String>();
        for (OriginChannelPicklistEntry__mdt partnerAndCallCenterChannel : [
                SELECT MasterLabel
                FROM OriginChannelPicklistEntry__mdt
                WHERE QualifiedApiName
                        IN ('IndirectSales','CallCenter')
                LIMIT 999
        ]){
            partnerAndCallCenterChannelList.add(partnerAndCallCenterChannel.MasterLabel);
        }
        return partnerAndCallCenterChannelList;
    }
}