/**
    * Test class for  TransferWizardCnt
    *
    * @author Baba Goudiaby
    * @modified by Moussa
    * @version 1.0
    * @description Test class for TransferWizardCnt
    * @status Completed
    * @uses
    * @code
    * @history
    * @date
    * 2019-06-05:  Baba Goudiaby - Code coverage 70%
    * 2019-06-06:  Baba Goudiaby - Code coverage 100%
    * 2019-11-29:  David Diop -
    *
*/
@IsTest
public with sharing class TransferWizardCntTst {

    @testSetup
    static void setup() {
        try {
            Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
            insert sequencer;
            Account businessAccount = TestDataFactory.account().businessAccount().build();
            businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
            businessAccount.Name = 'BusinessAccount1';
            businessAccount.BusinessType__c = 'NGO';
            Account personAccount = TestDataFactory.account().personAccount().build();
            personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
            List<Account> listAccount = new List<Account>();
            listAccount.add(businessAccount);
            listAccount.add(personAccount);
            insert listAccount;

            CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
            insert companyDivision;
            User user = new User();
            user.Id = UserInfo.getUserId() ;
            user.CompanyDivisionId__c = companyDivision.Id;
            update user;

            Contact contact = TestDataFactory.contact().createContact().build();
            contact.AccountId = listAccount[0].Id;
            contact.NationalIdentityNumber__c = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
            insert contact;

            Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
            interaction.Channel__c = 'MyEnel';
            insert interaction;

            CustomerInteraction__c customerInteraction = TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
            insert customerInteraction;        

            List<ServicePoint__c> listServicePoint = new List<ServicePoint__c>();
            ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePointGas().build();
            servicePoint.Account__c = listAccount[1].Id;
            insert servicePoint;

            ServicePoint__c servicePoint2 = TestDataFactory.servicePoint().createServicePointEle().build();
            servicePoint.Account__c = listAccount[0].Id;
            insert servicePoint2;

            listServicePoint.add(servicePoint);
            listServicePoint.add(servicePoint2);

            Opportunity opportunity = TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
            opportunity.StageName = 'Prospecting';
            opportunity.AccountId = listAccount[1].Id;
            insert opportunity;

            BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
            billingProfile.Account__c = listAccount[1].Id;
            insert billingProfile;

            Product2 pro = TestDataFactory.product2().build();
            insert pro;

            City__c city = new City__c(Name = 'JIMBOLIA', Province__c = 'TIMIS', Country__c = 'ROMANIA');
            insert city;
            Street__c street = MRO_UTL_TestDataFactory.street().createBuilder().build();
            street.City__c = city.id;
            insert street;

            List<OpportunityServiceItem__c> listOppServiceItem = new List<OpportunityServiceItem__c>();
            for (Integer i = 0; i < 15; i++) {
                OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
                oppServiceItem.BillingProfile__c = billingProfile.Id;
                oppServiceItem.Account__c = listAccount[i].Id;
                oppServiceItem.Opportunity__c = opportunity.Id;
                oppServiceItem.ServicePoint__c = listServicePoint[i].Id;
                oppServiceItem.PointStreetId__c = street.id;
                listOppServiceItem.add(oppServiceItem);
            }
            insert listOppServiceItem;

            OpportunityServiceItem__c opportunityServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
            opportunityServiceItem.Opportunity__c = opportunity.Id;
            opportunityServiceItem.PointStreetId__c = street.id;
            insert opportunityServiceItem;
            List<Supply__c> supplyList = new List<Supply__c>();
            for (Integer i = 0; i < 20; i++) {
                Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
                supply.Account__c = listAccount[1].Id;
                supply.ServicePoint__c = servicePoint.Id;
                supplyList.add(supply);
            }
            insert supplyList;

            servicePoint.CurrentSupply__c = supplyList[0].Id;
            update servicePoint;

            PricebookEntry pricebookEntry = TestDataFactory.pricebookEntry().build();
            pricebookEntry.Product2Id = pro.Id;
            pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
            insert pricebookEntry;

            OpportunityLineItem opportunityLineItem = TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
            opportunityLineItem.OpportunityID = opportunity.Id;
            opportunityLineItem.Product2ID = pro.Id;
            opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
            OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
            insert opportunityLineItem;

            Contract contract = TestDataFactory.contract().createContract().build();
            contract.AccountId = listAccount[0].Id;
            contract.CompanyDivision__c = companyDivision.Id;
            insert contract;

        } catch (Exception exc) {
            System.assert(true, 'Exception empty data?  ' + exc.getMessage());
        }
    }

    @IsTest
    static void initializeTest() {
        Account acc = [
			SELECT Id
			FROM Account
			WHERE Name = 'BusinessAccount1'
			LIMIT 1
        ];
        Opportunity opp = [
			SELECT Id
			FROM Opportunity
			LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
			SELECT Id
			FROM CustomerInteraction__c
			LIMIT 1
        ];           
        Map<String, String > inputJSON = new Map<String, String>{
			'accountId' => acc.Id,
			'opportunityId' => opp.Id,
			'customerInteractionId ' => customerInteraction.Id
        };


        Test.startTest();
        Object response = TestUtils.exec(
                'TransferWizardCnt', 'initialize', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    @IsTest
    static void initializeExceptionTest() {
        try {
            Test.startTest();
            Map<String, String > inputJSON = new Map<String, String>{
                    'accountId' => null
            };
            Object response = TestUtils.exec(
                    'TransferWizardCnt', 'initialize', inputJSON, false);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Exception empty data?  ' + exc.getMessage());
        }
    }

    @IsTest
    static void initializeEmptyOpportunity() {
        Account acc = [
			SELECT Id
			FROM Account
			WHERE Name = 'BusinessAccount1'
			LIMIT 1
        ];

        CustomerInteraction__c customerInteraction = [
			SELECT Id
			FROM CustomerInteraction__c
			LIMIT 1
        ]; 

        User user = new User();
        user.Id = UserInfo.getUserId() ;
        String companyDivisionId = [
            SELECT Id,Name 
            FROM CompanyDivision__c 
            WHERE Name = 'ENEL 1' 
            LIMIT 1
        ].Id;        
        user.CompanyDivisionId__c = companyDivisionId;
        update user; 

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
			'accountId' => acc.Id, 
			'opportunityId' => null,
			'customerInteractionId ' => customerInteraction.Id

        };
        Object response = TestUtils.exec(
                'TransferWizardCnt', 'initialize', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));
    }

    @IsTest
    static void initializeNoHasCompanyDivisionTest() {
        Account acc = [
			SELECT Id
			FROM Account
			WHERE Name = 'BusinessAccount1'
			LIMIT 1
        ];

        CustomerInteraction__c customerInteraction = [
			SELECT Id
			FROM CustomerInteraction__c
			LIMIT 1
        ]; 
        User user = new User();
        user.Id = UserInfo.getUserId() ;
        user.CompanyDivisionId__c = '';
        update user; 

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
			'accountId' => acc.Id, 
			'opportunityId' => '',
			'customerInteractionId ' => ''
        };

        Object response = TestUtils.exec(
                'TransferWizardCnt', 'initialize', inputJSON, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(null, result.get('hasCompanyDivision'));
    }


    @IsTest
    static void updateOpportunityTest() {
        Opportunity opp = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id, 'stage' => 'Closed Won'
        };
        Object response = TestUtils.exec(
                'TransferWizardCnt', 'updateOpportunity', inputJSON, true);
        Test.stopTest();
        Map<String, Object> opportunityData = (Map<String, Object>) response;
        String updatedStage = [
            SELECT Id,StageName 
            FROM Opportunity 
            WHERE Id = :(String) opportunityData.get('opportunityId')
        ].StageName;
        System.assertEquals(updatedStage, 'Closed Won', 'Why not? ' + updatedStage);
    }

    @IsTest
    static void updateOpportunityExceptionTest() {
        try {
            Opportunity opp = [
                SELECT Id
                FROM Opportunity
                LIMIT 1
            ];

            Test.startTest();
            Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => null,
                'stage' => 'Closed Won'
            };
            Object response = TestUtils.exec(
                    'TransferWizardCnt', 'updateOpportunity', inputJSON, true);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Exception empty data?  ' + exc.getMessage());
        }
    }

    @IsTest
    static void checkOsiTest() {
        BillingProfile__c billingProfile = [
            SELECT Id
            FROM BillingProfile__c
            LIMIT 1
        ];
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];
        OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        oppServiceItem.BillingProfile__c = billingProfile.Id;
        oppServiceItem.Account__c = account.Id;
        oppServiceItem.Opportunity__c = opportunity.Id;
        oppServiceItem.ServicePoint__c = servicePoint.Id;
        insert oppServiceItem;

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'osiId' => oppServiceItem.Id
        };
        Object response = TestUtils.exec(
                'TransferWizardCnt', 'checkOsi', inputJSON, true);
        Test.stopTest();
        Map<String, Object> opportunityServiceItemData = (Map<String, Object>) response;
        System.assert(opportunityServiceItemData.get('opportunityServiceItem') != null, 'Why not? ' + opportunityServiceItemData);
    }

    @IsTest
    static void checkOsiExceptionTest() {
        try {
            OpportunityServiceItem__c oppServiceItem = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
            ];

            Test.startTest();
            Map<String, String > inputJSON = new Map<String, String>{
                    'osiId' => null
            };
            Object response = TestUtils.exec(
                    'TransferWizardCnt', 'checkOsi', inputJSON, false);
            Test.stopTest();
        } catch (Exception exc) {
            System.assert(true, 'Exception empty data?  ' + exc.getMessage());
        }
    }

    @IsTest
    static void updateOsiListTest() {

        String billingProfileId = [
            SELECT Id
            FROM BillingProfile__c
        ].Id;
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];
        OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        oppServiceItem.BillingProfile__c = billingProfileId;
        oppServiceItem.Account__c = account.Id;
        oppServiceItem.Opportunity__c = opportunity.Id;
        oppServiceItem.ServicePoint__c = servicePoint.Id;
        insert oppServiceItem;

        List <OpportunityServiceItem__c> listOppServiceItem = new List<OpportunityServiceItem__c>();
        listOppServiceItem.add(oppServiceItem);

        TransferWizardCnt.InputData inputData = new TransferWizardCnt.InputData();
        inputData.opportunityServiceItems = listOppServiceItem;
        inputData.billingProfileId = billingProfileId;

        Test.startTest();
        Boolean response = (Boolean) TestUtils.exec(
                'TransferWizardCnt', 'updateOsiList', inputData, true);
        Test.stopTest();

        System.assertEquals(response, true);
    }


    @IsTest
    static void linkOliToOsiTest() {

        String productId = [
            SELECT Id
            FROM Product2
        ].Id;
        String billingProfileId = [
            SELECT Id
            FROM BillingProfile__c
        ].Id;
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];
        OpportunityServiceItem__c oppServiceItem = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        oppServiceItem.BillingProfile__c = billingProfileId;
        oppServiceItem.Account__c = account.Id;
        oppServiceItem.Opportunity__c = opportunity.Id;
        oppServiceItem.ServicePoint__c = servicePoint.Id;
        insert oppServiceItem;

        List <OpportunityServiceItem__c> listOppServiceItem = new List<OpportunityServiceItem__c>();
        listOppServiceItem.add(oppServiceItem);

        TransferWizardCnt.InputData inputData = new TransferWizardCnt.InputData();
        inputData.opportunityServiceItems = listOppServiceItem;
        inputData.product2Id = productId;

        Test.startTest();
        Boolean response = (Boolean) TestUtils.exec(
            'TransferWizardCnt', 'linkOliToOsi', inputData, true);
        Test.stopTest();

        System.assertEquals(response, true);
    }
    @IsTest
    static void checkSelectedSypply() {
        Account acc = [
            SELECT Id
            FROM Account
            WHERE Name = 'BusinessAccount1'
            LIMIT 1
        ];

        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().build();
        supply.Account__c = acc.Id;
        insert supply ;

        Test.startTest();
        Map<String, Object> response = TransferWizardCnt.checkSelectedSypply(supply.Id,acc.Id,opportunity.Id);
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
    }

    @IsTest
    static void checkSelectedSypplyIsNotBlankOsi() {
        Account acc = [
            SELECT Id
            FROM Account
            WHERE Name = 'BusinessAccount1'
            LIMIT 1
        ];

        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        ServicePoint__c servicePoint = [
            SELECT Id
            FROM ServicePoint__c
            LIMIT 1
        ];
        OpportunityServiceItem__c osi = TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        osi.ServicePoint__c = servicePoint.Id;
        osi.Opportunity__c = opportunity.Id;
        insert osi;
        Supply__c supply = TestDataFactory.supply().createSupplyBuilder().build();
        supply.ServicePoint__c = servicePoint.Id;
        insert supply ;

        Test.startTest();
        Map<String, Object> response = TransferWizardCnt.checkSelectedSypply(supply.Id,acc.Id,opportunity.Id);
        Test.stopTest();
        System.assertEquals(false, response.get('error'));
    }
    @IsTest
    static void updateContractAndContractSignedDateOnOpportunityTst() {

        Opportunity opportunity = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        Account acc = [
            SELECT Id
            FROM Account
            WHERE Name = 'BusinessAccount1'
            LIMIT 1
        ];

        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = acc.Id;
        insert contract;

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id,
            'customerSignedDate' => '',
            'contractId' => contract.Id
        };
        Object response = TestUtils.exec(
            'TransferWizardCnt', 'updateContractAndContractSignedDateOnOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    static void updateContractAndContractSignedDateOnOpportunityExcepTst() {

        Account acc = [
            SELECT Id
            FROM Account
            WHERE Name = 'BusinessAccount1'
            LIMIT 1
        ];

        Contract contract = TestDataFactory.contract().createContract().build();
        contract.AccountId = acc.Id;
        insert contract;

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => '',
            'customerSignedDate' => '',
            'contractId' => contract.Id
        };
        Object response = TestUtils.exec(
            'TransferWizardCnt', 'updateContractAndContractSignedDateOnOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }
}