/**
 * Created by BADJI on 31/12/2019.
 */

@IsTest
public with sharing class MRO_LC_TerminationWizardTst {

    @TestSetup
    static void setup() {
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('CN010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_ELE').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'Termination_ELE', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('CN010', 'RC010', 'Termination_ELE', recordTypeEle, '');
        insert phaseDI010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        Product2 product1 = TestDataFactory.product2().build();
        insert product1;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        //contract.EndDate= Date.newInstance(2030, 02, 17);
        insert contract;
        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Product__c = product1.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        dossier.Phase__c = 'RE010';
        dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('GenericRequest').getRecordTypeId();

        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_ELE').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c= 'ContractTermination_1';
        insert scriptTemplate;
    }

    @IsTest
    public static void InitializeTest() {
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ];
        ScriptTemplate__c scriptTemplate = [
            SELECT Id
            FROM ScriptTemplate__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            'templateId' => scriptTemplate.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'Initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
         System.assertEquals(true, result.get('error') == false);

        inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => '',
            'interactionId' => customerInteraction.Id,
            'templateId' => scriptTemplate.Id
        };
        response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'Initialize', inputJSON, true);
        result = (Map<String, Object>) response;

        Test.stopTest();
    }

    @IsTest
    static void InitializeExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                'MRO_LC_TerminationWizard', 'Initialize', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    private static void createCaseTest() {
        List<Supply__c> supply = [
            SELECT Id,Name,Account__c,Product__c, Product__r.Name, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c,Contract__c,
                Contract__r.StartDate,Contract__r.EndDate, Contract__r.ContractTerm
            FROM Supply__c
            WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        Account myAccount = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        supply[0].Contract__r.StartDate = Date.newInstance(2018, 03, 04);
        supply[0].Contract__r.ContractTerm = 50;
        supply[0].Product__r.Name = 'Enel Asistenta';
        String supplyListString = JSON.serialize(supply);
        String caseListString = JSON.serialize(caseList);
        Date dToday = Date.newInstance(2020, 03, 04);
        String effectiveDateString = dToday.year() + '-' + dToday.month() + '-' + dToday.day();

        Map<String, String > inputJSON = new Map<String, String>{
            'supplies' => supplyListString,
            'accountId' => myAccount.Id,
            'caseList' => caseListString,
            'dossierId' => dossier.Id,
            'effectiveDate' => effectiveDateString,
            'caseTile'=> caseListString,
            'reason' => caseList[0].Reason__c
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'createCase', inputJSON, true);
        System.debug('--- '+response);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();

    }

    @IsTest
    private static void emptyCreateCaseTest() {
        List<Supply__c> supply = [
            SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName
            FROM Supply__c
            WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        String supplyListString = JSON.serialize(supply);
        String caseListString = JSON.serialize(caseList);
        Date dToday = Date.newInstance(2020, 03, 04);
        String effectiveDateString = dToday.year() + '-' + dToday.month() + '-' + dToday.day();
        MRO_LC_TerminationWizard.CreateCaseInput caseInputData = new MRO_LC_TerminationWizard.CreateCaseInput();
        caseInputData.supplies =supplyListString;
        caseInputData.accountId = 'inexistantaccount';
        caseInputData.caseTile = caseListString;
        caseInputData.effectiveDate = effectiveDateString;
        caseInputData.reason = caseList[0].Reason__c;

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'createCase', caseInputData, true);
        Test.stopTest();

    }

    @IsTest
    private static void updateCaseListTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Reason__mdt> listRason= [SELECT Id, Custom_Reason__c FROM Reason__mdt];
        String caseListString = JSON.serialize(caseList);
        caseList[0].EffectiveDate__c = Date.newInstance(2020, 02, 17);
        String effectiveDateString = JSON.serialize(caseList[0].EffectiveDate__c);
        String reason = listRason[0].Custom_Reason__c;
        System.debug('reason '+reason);
        Map<String, String > inputJSON = new Map<String, String>{
            'caseList' => caseListString,
            'disconnectionCause' => reason ,
            'disconnectionDate' => effectiveDateString,
            'dossierId' => dossier.Id,
            'caseId' => caseList[0].Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'updateCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }

    @IsTest
    private static void emptyUpdateCaseListTest() {

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        List<Supply__c> listSupplies = [
            SELECT Id, CompanyDivision__c,ServicePoint__c
            FROM Supply__c
            LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        List<Reason__mdt> reason= [SELECT Id, Custom_Reason__c FROM Reason__mdt];
        String caseListString = JSON.serialize(caseList);
        String listSuppliesString = JSON.serialize(listSupplies);
        caseList[0].EffectiveDate__c = Date.newInstance(2020, 01, 03);
        String effectiveDateString = JSON.serialize(caseList[0].EffectiveDate__c);

        Map<String, String > inputJSON = new Map<String, String>{
            'caseList' => caseListString,
            'disconnectionDate'=> effectiveDateString,
            'disconnectionCause' => JSON.serialize(reason[0]),
            'dossierId' => dossier.Id,
            'searchedSupplyFieldsList' => listSuppliesString
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'updateCase', inputJSON, true);
        System.debug('response* *** '+response);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == true );

        caseList[0].EffectiveDate__c = Date.newInstance(2020, 09, 12);
        effectiveDateString = JSON.serialize(caseList[0].EffectiveDate__c);
        inputJSON = new Map<String, String>{
                'caseList' => caseListString,
                'disconnectionDate'=> effectiveDateString,
                'disconnectionCause' => JSON.serialize(reason[0]),
                'dossierId' => dossier.Id,
                'searchedSupplyFieldsList' => listSuppliesString
        };
        response = TestUtils.exec(
                'MRO_LC_TerminationWizard', 'updateCase', inputJSON, true);

        Test.stopTest();
    }

    @IsTest
    private static void cancelProcessTest() {
        List<Case> caseList = [
            SELECT Id,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,Supply__c,Supply__r.Contract__r.Status
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'cancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void cancelProcessExceptionTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => caseList[0].Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'cancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    private static void getSupplyRecordTest(){
        Supply__c supply = [
            SELECT Id
            FROM Supply__c
            LIMIT 1
        ];
        Test.startTest();
        List<Id> ids = new List<Id>();
        MRO_LC_TerminationWizard.supplyInputIds inputData = new MRO_LC_TerminationWizard.supplyInputIds();

        ids.add(supply.Id);
        inputData.supplyId =ids;
        inputData.origin = 'Internal';
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'getSupplyRecord', inputData, true);
        System.debug('*** '+response);
        Map<String, Object> result = (Map<String, Object>) response;
        //system.assertNotEquals(result.size(),0);
        Test.stopTest();
    }

    @IsTest
    private static void getValidDatetest(){
        Date myDate = Date.newInstance(2019, 12, 31);
        Map<String, String> inputJSON = new Map<String, String>{
            'checkEffectiveDate' =>myDate.format()
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_UTL_Date', 'getValidDate', inputJSON, false
        );

        // FOR INVALID DAY
        Date myDate1 = Date.newInstance(2020, 01, 01);
        Map<String, String> inputJSON1 = new Map<String, String>{
            'checkEffectiveDate' =>myDate1.format()
        };
        TestUtils.exec(
            'MRO_UTL_Date', 'getValidDate', inputJSON1, false
        );
        Test.stopTest();
    }

    @IsTest
    private static void updateDossierTest() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'channelSelected' => 'Back Office Sales',
                'originSelected' => 'Internal',
                'caseList' => caseListString
        };

        Test.startTest();
        Object response =  TestUtils.exec(
                'MRO_LC_TerminationWizard', 'SaveWizard', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    private static void getCaseTest() {
        Case caseObject = [
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'recordId' => caseObject.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TerminationWizard', 'getCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void setChannelAndOriginTest() {
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'channelSelected' => 'Back Office Sales',
                'originSelected' => 'Internal'
        };

        Test.startTest();
        Object response =  TestUtils.exec(
                'MRO_LC_TerminationWizard', 'setChannelAndOrigin', inputJSON, true);
        Test.stopTest();
    }
}