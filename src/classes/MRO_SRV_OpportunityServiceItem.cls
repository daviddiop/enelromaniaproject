/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 10, 2020
 * @desc
 * @history
 */

public with sharing class MRO_SRV_OpportunityServiceItem {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_ServicePoint queryServicePoint = MRO_QR_ServicePoint.getInstance();
    private static MRO_SRV_Supply supplyService= MRO_SRV_Supply.getInstance();
    private static MRO_SRV_Case caseService= MRO_SRV_Case.getInstance();
    private static MRO_SRV_Activity activityService= MRO_SRV_Activity.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static Map<String, Schema.RecordTypeInfo> osiRecordTypes = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();

    public static MRO_SRV_OpportunityServiceItem getInstance() {
        return (MRO_SRV_OpportunityServiceItem) ServiceLocator.getInstance(MRO_SRV_OpportunityServiceItem.class);
    }

    public List<OpportunityServiceItem__c> createOSIWithServicePoints(List<ServicePoint__c> servicePoints, Id opportunityId, Map<String, Case> mapCasesByServicePointId) {
        List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
        Set<String> rtDevNames = new Set<String>{constantsSrv.COMMODITY_ELECTRIC, constantsSrv.COMMODITY_GAS};
        Map<String, RecordType> recordTypesMap = MRO_QR_RecordType.getInstance().getRecordTypesByObjectAndDeveloperNames('OpportunityServiceItem__c', rtDevNames);
        String supplyOperation;
        for (ServicePoint__c servicePoint : servicePoints) {

            if(mapCasesByServicePointId.containsKey(servicePoint.Code__c) && mapCasesByServicePointId.get(servicePoint.Code__c).SubProcess__c == constantsSrv.SUBPROCESS_CONNECTION_PERMANENT){
                supplyOperation = 'Permanent';
            }else if((mapCasesByServicePointId.containsKey(servicePoint.Code__c) && (mapCasesByServicePointId.get(servicePoint.Code__c).SubProcess__c == constantsSrv.SUBPROCESS_CONNECTION_TEMPORARY_DEFINITIVE_SOLUTION || mapCasesByServicePointId.get(servicePoint.Code__c).SubProcess__c == constantsSrv.SUBPROCESS_CONNECTION_TEMPORARY_SOLUTION))){
                supplyOperation = 'Temporary';
            }else{
                supplyOperation = '';
            }

            OpportunityServiceItem__c osi = new OpportunityServiceItem__c(
                Opportunity__c = opportunityId,
                RecordTypeId = recordTypesMap.get(servicePoint.RecordType.DeveloperName).Id,
                ServicePoint__c = servicePoint.Id,
                ServicePointCode__c = servicePoint.Code__c,
                Distributor__c = servicePoint.Distributor__c,
                Distributor__r = servicePoint.Distributor__r,
                Trader__c = servicePoint.Trader__c,
                Voltage__c = servicePoint.Voltage__c,
                VoltageSetting__c = servicePoint.Voltage__c != null ? String.valueOf((servicePoint.Voltage__c*1000).intValue()) : null,
                VoltageLevel__c = servicePoint.VoltageLevel__c,
                PowerPhase__c = servicePoint.PowerPhase__c,
                AvailablePower__c = servicePoint.AvailablePower__c,
                ContractualPower__c = servicePoint.ContractualPower__c,
                Pressure__c = servicePoint.Pressure__c,
                PressureLevel__c = servicePoint.PressureLevel__c,
                ConversionFactor__c = servicePoint.ConversionFactor__c,
                EstimatedConsumption__c = servicePoint.EstimatedConsumption__c,
                FirePlacesCount__c = servicePoint.FirePlacesCount__c,
                ConsumptionCategory__c = servicePoint.ConsumptionCategory__c,
                FlowRate__c = servicePoint.FlowRate__c,
                IsNewConnection__c = servicePoint.IsNewConnection__c,
                SupplyOperation__c = supplyOperation
            );
            osi.RecordType = recordTypesMap.get(servicePoint.RecordType.DeveloperName);
            MRO_SRV_Address.AddressDTO pointAddress = new MRO_SRV_Address.AddressDTO(servicePoint, 'Point');
            pointAddress.populateRecordAddressFields(osi, 'Point');
            pointAddress.populateRecordAddressFields(osi, 'Site');
            if (servicePoint.RecordType.DeveloperName == 'Electric') {
                osi.NLC__c = servicePoint.Code__c;
            } else if (servicePoint.RecordType.DeveloperName == 'Gas') {
                osi.CLC__c = servicePoint.Code__c;
            }
            osiList.add(osi);
        }
        databaseSrv.insertSObject(osiList);
        return osiList;
    }

    /**
    * @author Luca Ravicini
    * @description  create OSI via service point data
    * @param servicePointIds
    * @return List<OpportunityServiceItem__c>
    */
    public List<OpportunityServiceItem__c> initOsis(Set<Id> servicePointIds, String opportunityId) {
        Map<Id, ServicePoint__c> servicePointsMap = queryServicePoint.mapServicePointsByIds(servicePointIds);
        return this.initOsis(servicePointsMap, opportunityId);
    }

    public List<OpportunityServiceItem__c> initOsis(Map<Id, ServicePoint__c> servicePointsMap, String opportunityId) {
        Map<String,Map<String,String>> voltagePicklistMap = MRO_UTL_Utils.getDependentPickList('OpportunityServiceItem__c','VoltageLevel__c', 'VoltageSetting__c');
        List<OpportunityServiceItem__c> osis= new List<OpportunityServiceItem__c>();
        for (ServicePoint__c point: servicePointsMap.values()){
            OpportunityServiceItem__c osi = new OpportunityServiceItem__c();
            osi.Opportunity__c= opportunityId;
            osi.RecordTypeId = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get(point.RecordType.DeveloperName).getRecordTypeId() ;
            osi.ServicePoint__r = point;
            osi.ServicePoint__c = point.Id;
            osi.ServicePointCode__c = point.Name;
            osi.AvailablePower__c = point.AvailablePower__c;
            osi.ConsumptionCategory__c = point.ConsumptionCategory__c;
            osi.ContractualPower__c = point.ContractualPower__c;
            osi.ConversionFactor__c = point.ConversionFactor__c;
            osi.Distributor__c = point.Distributor__c;
            //osi.EndDate__c = point.ATRExpirationDate__c;
            osi.EstimatedConsumption__c = point.EstimatedConsumption__c;
            osi.FlowRate__c = point.FlowRate__c;
            osi.IsNewConnection__c = point.IsNewConnection__c;
            osi.PointAddressKey__c = point.PointAddressKey__c;
            osi.PointStreetId__c = point.PointStreetId__c;
            osi.PointStreetNumber__c = point.PointStreetNumber__c;
            osi.PointStreetNumberExtn__c = point.PointStreetNumberExtn__c;
            osi.PointStreetName__c = point.PointStreetName__c;
            osi.PointCity__c = point.PointCity__c;
            osi.PointCountry__c = point.PointCountry__c;
            osi.PointPostalCode__c = point.PointPostalCode__c;
            osi.PointStreetType__c = point.PointStreetType__c;
            osi.PointApartment__c = point.PointApartment__c;
            osi.PointBuilding__c = point.PointBuilding__c;
            osi.PointLocality__c = point.PointLocality__c;
            osi.PointProvince__c = point.PointProvince__c;
            osi.PointFloor__c = point.PointFloor__c;
            osi.PointAddressNormalized__c = point.PointAddressNormalized__c;
            osi.PowerPhase__c = point.PowerPhase__c;
            osi.Pressure__c = point.Pressure__c;
            osi.PressureLevel__c = point.PressureLevel__c;
            osi.Trader__c = point.Trader__c;
            osi.Voltage__c = point.Voltage__c;
            osi.VoltageLevel__c = point.VoltageLevel__c;
            if(point.RecordType.DeveloperName.equals(constantsSrv.COMMODITY_ELECTRIC) ){
                Decimal volt= point.Voltage__c;
                String voltageLevel= point.VoltageLevel__c;
                if(voltageLevel !=null && volt != null){
                    String voltageSetting=String.valueOf(Integer.valueOf(volt * 1000));
                    if (voltageSetting != null) {
                        String voltLabel=voltagePicklistMap.get(voltageLevel).get(voltageSetting);
                        if(voltLabel != null ){
                            osi.VoltageSetting__c=voltageSetting;
                        }
                    }
                }

            }
            if(point.CurrentSupply__c != null){
                osi.Account__c = point.CurrentSupply__r.Account__c;
                osi.Contract__c = point.CurrentSupply__r.Contract__c;
                osi.ContractAccount__c = point.CurrentSupply__r.ContractAccount__c;
                osi.EndDate__c = point.CurrentSupply__r.TitleDeedValidity__c;
                osi.Market__c = point.CurrentSupply__r.Market__c;
                osi.NACEReference__c = point.CurrentSupply__r.NACEReference__c;
                osi.NonDisconnectable__c = point.CurrentSupply__r.NonDisconnectable__c;
                osi.SupplyOperation__c = point.CurrentSupply__r.SupplyOperation__c;
                osi.Product__c = point.CurrentSupply__r.Product__c;
                osi.FlatRate__c = point.CurrentSupply__r.FlatRate__c;
                if(point.CurrentSupply__r.ServiceSite__c != null){
                    osi.ServiceSite__c = point.CurrentSupply__r.ServiceSite__c;
                    osi.SiteAddressKey__c = point.CurrentSupply__r.ServiceSite__r.SiteAddressKey__c;
                    osi.SiteAddressNormalized__c = point.CurrentSupply__r.ServiceSite__r.SiteAddressNormalized__c;
                    osi.SiteApartment__c = point.CurrentSupply__r.ServiceSite__r.SiteApartment__c;
                    osi.SiteBlock__c = point.CurrentSupply__r.ServiceSite__r.SiteBlock__c;
                    osi.SiteBuilding__c = point.CurrentSupply__r.ServiceSite__r.SiteBuilding__c;
                    osi.SiteCity__c = point.CurrentSupply__r.ServiceSite__r.SiteCity__c;
                    osi.SiteCountry__c = point.CurrentSupply__r.ServiceSite__r.SiteCountry__c;
                    osi.SiteFloor__c = point.CurrentSupply__r.ServiceSite__r.SiteFloor__c;
                    osi.SitePostalCode__c = point.CurrentSupply__r.ServiceSite__r.SitePostalCode__c;
                    osi.SiteProvince__c = point.CurrentSupply__r.ServiceSite__r.SiteProvince__c;
                    osi.SiteStreetId__c = point.CurrentSupply__r.ServiceSite__r.SiteStreetId__c;
                    osi.SiteStreetName__c = point.CurrentSupply__r.ServiceSite__r.SiteStreetName__c;
                    osi.SiteStreetNumber__c = point.CurrentSupply__r.ServiceSite__r.SiteStreetNumber__c;
                    osi.SiteStreetNumberExtn__c = point.CurrentSupply__r.ServiceSite__r.SiteStreetNumberExtn__c;
                    osi.SiteStreetType__c = point.CurrentSupply__r.ServiceSite__r.SiteStreetType__c;
                }
            }
            osis.add(osi);
        }

        return osis;
    }

    /**
     * @author BG
     * @description  copy new OSI taken from Supply on Activity
     * @param opportunityId
     * @param activityId
     *
     * @return List<OpportunityServiceItem__c>
     */
    public List<OpportunityServiceItem__c> createOsiFromActivity(String opportunityId, String activityId) {
        List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
        List<wrts_prcgvr__Activity__c> activities = new List<wrts_prcgvr__Activity__c>();
        activities = activityService.getActivityRecords(activityId);
        List<Supply__c> supplies = supplyService.listSuppliesFromActivity(activities);
        List<Case> cases = caseService.listCasesFromActivity(activities);
        Map<String, Case> mapCasesByServicePointId = new Map<String, Case>();
        Set<Id> servicePointIds = new Set<Id>();
        Set<Id> serviceSiteIds = new Set<Id>();
        String commodity = 'Electric';

        if(!cases.isEmpty()) {
            for (Case c : cases) {
                if (c != null && c.Supply__c != null && c.Supply__r.ServicePoint__c != null) {
                    mapCasesByServicePointId.put(c.Supply__r.ServicePoint__c, c);
                    serviceSiteIds.add(c.Supply__r.ServiceSite__c);
                }
            }
        }

        if(!serviceSiteIds.isEmpty()){
            supplies = supplyQuery.getSuppliesByServiceSiteAndStatus(serviceSiteIds, commodity, new Set<String>{
                    constantsSrv.SUPPLY_STATUS_ACTIVE,
                    constantsSrv.SUPPLY_STATUS_ACTIVATING,
                    constantsSrv.SUPPLY_STATUS_TERMINATING});
        }

        for (Supply__c supply : supplies) {
            if(supply.ServicePoint__c != null) {
                if(!mapCasesByServicePointId.isEmpty()){
                    osiList.add(instantiateOppServiceItem(opportunityId, supply, true));
                }else{
                    osiList.add(instantiateOppServiceItem(opportunityId, supply));
                }

                servicePointIds.add(supply.ServicePoint__c);
            }
        }

        Map<Id, ServicePoint__c> servicePoints = queryServicePoint.mapServicePointsByIds(servicePointIds);
        if (!servicePoints.isEmpty()) {
            for (OpportunityServiceItem__c osi : osiList) {
                copyDataFromServicePoint(osi, servicePoints.get(osi.ServicePoint__c));
                if(!mapCasesByServicePointId.isEmpty() && mapCasesByServicePointId.containsKey(osi.ServicePoint__c)){
                    osi.Voltage__c = mapCasesByServicePointId.get(osi.ServicePoint__c).Voltage__c;
                    osi.AvailablePower__c = mapCasesByServicePointId.get(osi.ServicePoint__c).AvailablePower__c;
                    osi.ContractualPower__c = mapCasesByServicePointId.get(osi.ServicePoint__c).ContractualPower__c;
                    osi.EstimatedConsumption__c = mapCasesByServicePointId.get(osi.ServicePoint__c).EstimatedConsumption__c;
                    osi.PowerPhase__c = mapCasesByServicePointId.get(osi.ServicePoint__c).PowerPhase__c;
                    osi.VoltageLevel__c = mapCasesByServicePointId.get(osi.ServicePoint__c).VoltageLevel__c;
                }
            }
        }
        databaseSrv.insertSObject(osiList);
        if(!activities.isEmpty()) {

            for(wrts_prcgvr__Activity__c activity : activities) {

                activity.wrts_prcgvr__Status__c = 'Product change initiated';
            }
        }
        activities.add(new wrts_prcgvr__Activity__c(Id = activityId, wrts_prcgvr__Status__c = 'Product change initiated'));
        databaseSrv.updateSObject(activities);
        return osiList;
    }

    public OpportunityServiceItem__c instantiateOppServiceItem(String opportunityId, Supply__c supply, Boolean isPvcActivity) {

        OpportunityServiceItem__c osi = this.instantiateOppServiceItem(opportunityId, supply);
        if(isPvcActivity){
            osi.Supply__c = supply.Id;
        }
        return osi;
    }

    /**
     * @author BG
     * @description instantiate a new OSI from Supply Data
     * @param osi
     * @param point
     * @return Opportunity service Item
     */
    public OpportunityServiceItem__c instantiateOppServiceItem(String opportunityId, Supply__c supply) {
        OpportunityServiceItem__c osi = new OpportunityServiceItem__c();
        osi.Opportunity__c = opportunityId;
        osi.RecordTypeId = osiRecordTypes.get(supply.RecordType.DeveloperName).getRecordTypeId();
        osi.Account__c = supply.Account__c;
        osi.ServicePoint__c = supply.ServicePoint__c;
        osi.ServiceSite__c = supply.ServiceSite__c;
        osi.ContractAccount__c = supply.ContractAccount__c;
        osi.Contract__c = supply.Contract__c;
        osi.Product__c = supply.Product__c;
        osi.NACEReference__c = supply.NACEReference__c;
        osi.FlatRate__c = supply.FlatRate__c;
        osi.NonDisconnectable__c = supply.NonDisconnectable__c;
        osi.NonDisconnectableReason__c = supply.NonDisconnectableReason__c;
        osi.SupplyOperation__c = supply.SupplyOperation__c;
        osi.EndDate__c = supply.TitleDeedValidity__c;
        return osi;
    }

    /**
     * @author BG
     * @description copy data from Service Point
     * @date 17/02/2020
     * @param osi
     * @param point Service Point on Active supply
     */
    public void copyDataFromServicePoint(OpportunityServiceItem__c osi, ServicePoint__c point) {
        osi.PointAddressNormalized__c = point.PointAddressNormalized__c;
        osi.Distributor__c = point.Distributor__c;
        osi.Trader__c = point.Trader__c;
        osi.PointStreetType__c = point.PointStreetType__c;
        osi.PointStreetName__c = point.PointStreetName__c;
        osi.PointStreetNumber__c = point.PointStreetNumber__c;
        osi.PointStreetNumberExtn__c = point.PointStreetNumberExtn__c;
        osi.PointApartment__c = point.PointApartment__c;
        osi.PointBuilding__c = point.PointBuilding__c;
        osi.PointBlock__c = point.PointBlock__c;
        osi.PointCity__c = point.PointCity__c;
        osi.PointCountry__c = point.PointCountry__c;
        osi.PointFloor__c = point.PointFloor__c;
        osi.PointLocality__c = point.PointLocality__c;
        osi.PointPostalCode__c = point.PointPostalCode__c;
        osi.PointProvince__c = point.PointProvince__c;
        osi.PointAddressNormalized__c = point.PointAddressNormalized__c;
        osi.Voltage__c = point.Voltage__c;
        osi.VoltageLevel__c = point.VoltageLevel__c;
        osi.PressureLevel__c = point.PressureLevel__c;
        osi.Pressure__c = point.Pressure__c;
        osi.PowerPhase__c = point.PowerPhase__c;
        osi.ConversionFactor__c = point.ConversionFactor__c;
        osi.ContractualPower__c = point.ContractualPower__c;
        osi.AvailablePower__c = point.AvailablePower__c;
        osi.EstimatedConsumption__c = point.EstimatedConsumption__c;
        //FF OSI Edit - Pack2 - Interface Check
        osi.ConsumptionCategory__c = point.ConsumptionCategory__c;
        osi.FlowRate__c = point.FlowRate__c;
        //FF OSI Edit - Pack2 - Interface Check
        // [ENLCRO-657] OSI Edit - Pack3 - Interface Check
        //osi.EndDate__c = point.ATRExpirationDate__c;
        osi.IsNewConnection__c = point.IsNewConnection__c;
        // [ENLCRO-657] OSI Edit - Pack3 - Interface Check
    }

    public void copyDataFromServiceSite(OpportunityServiceItem__c osi, ServiceSite__c serviceSite) {
        osi.ServiceSite__c = serviceSite.Id;
        osi.SiteAddressKey__c = serviceSite.SiteAddressKey__c;
        osi.SiteStreetId__c = serviceSite.SiteStreetId__c;
        osi.SiteAddressNormalized__c = serviceSite.SiteAddressNormalized__c;
        osi.SiteStreetType__c = serviceSite.SiteStreetType__c;
        osi.SiteStreetName__c = serviceSite.SiteStreetName__c;
        osi.SiteStreetNumber__c = serviceSite.SiteStreetNumber__c;
        osi.SiteStreetNumberExtn__c = serviceSite.SiteStreetNumberExtn__c;
        osi.SiteApartment__c = serviceSite.SiteApartment__c;
        osi.SiteBuilding__c = serviceSite.SiteBuilding__c;
        osi.SiteBlock__c = serviceSite.SiteBlock__c;
        osi.SiteCity__c = serviceSite.SiteCity__c;
        osi.SiteCountry__c = serviceSite.SiteCountry__c;
        osi.SiteFloor__c = serviceSite.SiteFloor__c;
        osi.SiteLocality__c = serviceSite.SiteLocality__c;
        osi.SitePostalCode__c = serviceSite.SitePostalCode__c;
        osi.SiteProvince__c = serviceSite.SiteProvince__c;
        osi.CLC__c = serviceSite.CLC__c;
        osi.NLC__c = serviceSite.NLC__c;
        osi.SiteDescription__c = serviceSite.Description__c;
    }
}