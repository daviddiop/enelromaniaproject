public with sharing class InteractionCnt  extends ApexServiceLibraryCnt {

    private static final InteractionService interactionSrv = InteractionService.getInstance();

    public with sharing class listOptions extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return new Map<String, Object>{
                'statusList' => interactionSrv.listOptions(Interaction__c.Status__c)
            };
        }
    }

    public with sharing class findById extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            return interactionSrv.findById(interactionId);
        }
    }

    public with sharing class insertInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return interactionSrv.insertInteraction();
        }
    }

    public with sharing class upsertInteraction extends AuraCallable {
        public override Object perform(final String jsonInput) {
            InteractionService.InteractionDTO interactionDto = (InteractionService.InteractionDTO)JSON.deserialize(jsonInput, InteractionService.InteractionDTO.class);
            return interactionSrv.upsertInteraction(interactionDto);
        }
    }

    public with sharing class removeInterlocutor extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String interactionId = params.get('interactionId');
            if (String.isBlank(interactionId)) {
                throw new WrtsException(System.Label.Interaction + ' - ' + System.Label.MissingId);
            }
            return interactionSrv.removeInterlocutor(interactionId);
        }
    }
}