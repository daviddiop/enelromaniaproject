public with sharing class ScriptTemplateService extends ApexServiceLibraryCnt {
    private static ScriptTemplateQueries scriptElementQuery = ScriptTemplateQueries.getInstance();
    private static String scriptTemplateKeyPrefix = ScriptTemplate__c.sObjectType.getDescribe().getKeyPrefix();

    public with sharing class getScriptElements extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String templateId = params.get('templateId');

            if (String.isBlank(templateId) || !templateId.startsWith(scriptTemplateKeyPrefix)) {
                throw new ScriptException(System.Label.ScriptTemplate +' - '+ System.Label.MissingId);

            }
            return scriptElementQuery.getByScriptTemplateId(templateId);
        }
    }

    public class ScriptException extends Exception {
    }

}