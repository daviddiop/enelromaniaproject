/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 04, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_Contact {

    public static MRO_QR_Contact getInstance() {
        return (MRO_QR_Contact)ServiceLocator.getInstance(MRO_QR_Contact.class);
    }

    public List<Contact> listContactByIndividualId(String individualId) {
        return [
            SELECT Id, Name, IndividualId, AccountId, IsPersonAccount
            FROM Contact
            WHERE IndividualId = :individualId
        ];
    }


    public List<Contact> listContactByIndividualId(String individualId, Boolean includePersonAccounts) {
        String query =
        'SELECT Id, Name, IndividualId, AccountId, IsPersonAccount, FirstName, LastName, NationalIdentityNumber__c,ForeignCitizen__c, ' +
                'Gender__c,BirthCity__c,BirthDate,IDDocEmissionDate__c, IDDocValidityDate__c, IDDocumentNr__c, IDDocumentType__c, IDDocumentSeries__c, IDDocIssuer__c, ' +
                'ContactChannel__c, MobilePhone, Phone, Email ' +
        'FROM Contact ' +
        'WHERE IndividualId = :individualId ';

        if (includePersonAccounts) {
            query += 'AND IsPersonAccount = true';
        } else {
            query += 'AND IsPersonAccount = false';
        }

        return Database.query(query);
    }

    public Contact getById(Id contactId) {
        List<Contact> contactList = [
            SELECT Id,Name, IsPersonAccount, AccountId, FirstName, LastName, Gender__c, HomePhone, MobilePhone, Email,Phone,
                   MarketingContactChannel__c, MarketingPhone__c, MarketingMobilePhone__c, MarketingEmail__c,
                   IndividualId, Individual.HasOptedOutTracking, Individual.HasOptedOutProfiling, Individual.HasOptedOutProcessing,
                   Individual.HasOptedOutSolicit, Individual.HasOptedOutGeoTracking, Individual.HasOptedOutPartners__c,
                   Individual.ShouldForget, Individual.SendIndividualData, Individual.CanStorePiiElsewhere,NationalIdentityNumber__c
            FROM Contact
            WHERE Id = :contactId
            LIMIT 1
        ];
        if (contactList.isEmpty()) {
            return null;
        }
        return contactList.get(0);
    }

    public List<Contact> getContactByAccountId(String accountId) {
        return [
                SELECT Id,Name, Phone,MobilePhone,Email
                FROM Contact
                WHERE Id IN (SELECT ContactId FROM AccountContactRelation WHERE  AccountId = :accountId)
        ];

    }

    public Contact getByAccountId(Id accountId) {
        List<Contact> contactList = [
                SELECT Id, IsPersonAccount, AccountId, FirstName, LastName, Gender__c, HomePhone, MobilePhone, Email,
                        MarketingContactChannel__c, MarketingPhone__c, MarketingMobilePhone__c, MarketingEmail__c,
                        IndividualId, Individual.HasOptedOutTracking, Individual.HasOptedOutProfiling, Individual.HasOptedOutProcessing,
                        Individual.HasOptedOutSolicit, Individual.HasOptedOutGeoTracking, Individual.HasOptedOutPartners__c,
                        Individual.ShouldForget, Individual.SendIndividualData, Individual.CanStorePiiElsewhere
                FROM Contact
                WHERE AccountId = :accountId
                LIMIT 1
        ];
        if (contactList.isEmpty()) {
            return null;
        }
        return contactList.get(0);
    }

    public Contact findById(String contactId) {
        List<Contact> contactList = [
                SELECT Id, Name, IndividualId
                FROM Contact
                WHERE Id = :contactId
                LIMIT 1
        ];
        if (contactList.isEmpty()) {
            return null;
        }
        return contactList.get(0);
    }

}