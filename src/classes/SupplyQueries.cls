public inherited sharing class SupplyQueries {

    public static SupplyQueries getInstance() {
        return new SupplyQueries();
    }

    public List<Case> getDeveloperNameQuery(String caseId){
        return new List<Case>(
            [
                SELECT Id, CaseNumber, RecordTypeId, RecordType.Name, RecordType.DeveloperName
                FROM Case
                WHERE Id = :caseId
            ]
        );
    }

    public static List<String> getIntersectList(List<List<String>> listOfList){
        Set<String> dataSet = new Set<String>();
        for(List<String> alist : listOfList) {
            dataSet.addAll(alist);
        }

        List<String> mergedList = new List<String>();
        mergedList.addAll(dataSet);
        return mergedList;
    }

    public static String getQuery(List<String> selectFields) {
        List<String> defaultFields = new List<String>{'RecordType.DeveloperName', 'NonDisconnectable__c','CompanyDivision__c','Market__c','ServiceSite__c','ServicePoint__r.CurrentSupply__c','Contract__r.Name','Contract__c','ContractAccount__c','ContractAccount__r.BillingProfile__c','ServicePoint__r.Code__c','Product__c','ServicePoint__r.EstimatedConsumption__c'};

        List<List<String>> listOfList = new List<List<String>>();
        listOfList.add(defaultFields);
        listOfList.add(selectFields);

        List<String> mergedFields = getIntersectList(listOfList);

        String query = 'SELECT ' + String.join(mergedFields, ', ')+ ' FROM Supply__c ';
        return query;
    }

    public Map<Id, Supply__c> getByIds(Set<Id> supplyId) {
        return new Map<Id, Supply__c>([
                SELECT Id, Activator__c, Status__c,CompanyDivision__c,
                        Contract__c, Terminator__c, ServicePoint__c
                FROM Supply__c
                WHERE Id IN :supplyId
        ]);
    }

    public static Map<Id, Supply__c> getByIdsStatic(Set<Id> supplyId) {
        return new Map<Id, Supply__c>([
                SELECT Id, Activator__c, Status__c,CompanyDivision__c,
                        Contract__c, Terminator__c, Terminator__r.RecordType.DeveloperName, ServicePoint__c
                FROM Supply__c
                WHERE Id IN :supplyId
        ]);
    }

    public Supply__c getById(Id supplyId) {
        return [
                SELECT Id, Status__c, CompanyDivision__c, Account__c, ServicePoint__c
                FROM Supply__c
                WHERE Id = :supplyId
        ];
    }

    public List<Supply__c> getActiveSuppliesByBillingProfile(String billingProfileId) {
        return [
                SELECT Id
                FROM Supply__c
                WHERE Status__c in ('Active', 'Terminating') AND ContractAccount__r.BillingProfile__c = :billingProfileId
        ];
    }

    public List<Supply__c> getActiveSuppliesByContractAccount(String contractAccountId) {
        return [
                SELECT Id
                FROM Supply__c
                WHERE Status__c in ('Active', 'Terminating') AND ContractAccount__c = :contractAccountId
        ];
    }

    public Supply__c getById(Id supplyId, String companyDivision) {
        return [
                SELECT Id, Status__c,CompanyDivision__c
                FROM Supply__c
                WHERE Id = :supplyId AND CompanyDivision__c = :companyDivision
        ];
    }
    /*public Supply__c getActiveSupplies(Id supply, String sp){
        return [
            SELECT Id, ServicePoint__r.Code__c, Status__c
            FROM Supply__c
            WHERE Id =: supply AND Status__c =: 'Active' AND ServicePoint__r.Code__c = :sp
        ];
    }*/


    public List<Supply__c> getSuppliesByServicePointCode(String pointCode) {
        return [
                SELECT Id, ServicePoint__r.Code__c,ServicePoint__c, CompanyDivision__c
                FROM Supply__c
                WHERE ServicePoint__r.Code__c = :pointCode
        ];
    }

    /**
     * Cretaed by Moussa
     * 12/09/2019
     */
    public List<Supply__c> getSuppliesByServicePointCodeAndCompanyDivision(String pointCode,String companyDivisionId) {
        return [
                SELECT Id, ServicePoint__r.Code__c,ServicePoint__c, CompanyDivision__c
                FROM Supply__c
                WHERE ServicePoint__r.Code__c = :pointCode and CompanyDivision__c=:companyDivisionId
        ];
    }

    public List<Supply__c> getSupplies(String accountId, User userInformation) {
        String query = 'SELECT Id,Status__c, ServicePoint__r.Code__c,ContractAccount__r.BillingProfile__c, Product__c, Product__r.Name, ' +
                            'ActivationDate__c,ServiceSite__c, TerminationDate__c, CompanyDivision__c, Market__c, ServicePoint__r.EstimatedConsumption__c, ' +
                            'RecordType.Name,RecordType.DeveloperName ' +
                        'FROM Supply__c ';

        List<String> conditions = new List<String>();
        String companyDivisionId;
        if (String.isNotBlank(accountId)) {
            conditions.add('Account__c = \'' + accountId + '\'');
        }

        if (userInformation.CompanyDivisionEnforced__c) {
            companyDivisionId = userInformation.CompanyDivisionId__c;
            String companyDivisionIdFormatted  = String.isNotBlank(companyDivisionId) ? '\''+ companyDivisionId +'\'' : null;
            conditions.add('CompanyDivision__c = ' + companyDivisionIdFormatted );
        }

        if (!conditions.isEmpty()) {
            query +=' WHERE ' + String.join(conditions, ' AND ');
        }

        return Database.query(query);
    }

    public List<Supply__c> getSuppliesByServicePoint(String servicePointCode, String accountId, String status, string contractId, List<String> selectFields, Boolean notOwnAccount, String companyDivisionId) {
        List<Supply__c> supplyFieldsSP;
        List<String> conditions = new List<String>();
        String query;

        if (String.isNotBlank(servicePointCode)) {
            conditions.add('ServicePoint__r.Code__c = \'' + servicePointCode + '\'');
        }

        if (String.isNotBlank(accountId)) {
            if (notOwnAccount) {
                conditions.add('Account__c != \'' + accountId + '\'');
            } else {
                conditions.add('Account__c = \'' + accountId + '\'');
            }
        }

        if (String.isNotBlank(status)) {
            conditions.add('Status__c = \'' + status + '\'');
        }

        if (String.isNotBlank(contractId)) {
            conditions.add('Contract__c = \'' + contractId + '\'');
        }

        if (String.isNotBlank(companyDivisionId)) {
            conditions.add('CompanyDivision__c = \'' + companyDivisionId + '\'');
        }

        if (!conditions.isEmpty()) {
            query = getQuery(selectFields) + ' WHERE ' + String.join(conditions, ' AND ') + ' LIMIT 999';
        }

        supplyFieldsSP = Database.query(query);


        if (supplyFieldsSP.isEmpty()) {
            throw new WrtsException.GenericException(System.Label.NoResults);
        }

        return supplyFieldsSP;
    }

    public List<Supply__c> getSuppliesByContract(String contractCode, String contractCustomerCode, String accountId, String status, List<String> selectFields, String contractId, Boolean notOwnAccount, String companyDivisionId) {

        List<Supply__c> supplyFieldsCT;
        List<String> conditions = new List<String>();
        String query;

        if (String.isNotBlank(contractCode)) {
            conditions.add('Contract__r.Name = \'' + contractCode + '\'');
        }

        if (String.isNotBlank(contractCustomerCode)) {
            conditions.add('Contract__r.ContractNumber like \'%' + contractCustomerCode + '%\'');
        }

        if (String.isNotBlank(accountId)) {
            if (notOwnAccount) {
                conditions.add('Account__c != \'' + accountId + '\'');
            } else {
                conditions.add('Account__c = \'' + accountId + '\'');
            }
        }

        if (String.isNotBlank(contractId)) {
            conditions.add('Contract__c = \'' + contractId + '\'');
        }

        if (String.isNotBlank(companyDivisionId)) {
            conditions.add('CompanyDivision__c = \'' + companyDivisionId + '\'');
        }

        if (String.isNotBlank(status)) {
            conditions.add('Status__c = \'' + status + '\'');
        }

        if (!conditions.isEmpty()) {
            query = getQuery(selectFields) + ' WHERE ' + String.join(conditions, ' AND ') + ' LIMIT 999';
        }

        supplyFieldsCT = Database.query(query);

        if (supplyFieldsCT.isEmpty()) {
            throw new WrtsException.GenericException(System.Label.NoResults);
        }

        return supplyFieldsCT;
    }
}