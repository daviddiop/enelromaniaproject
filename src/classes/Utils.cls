/**
 * Created by Roberto Tomasoni on 16/01/2019.
 *
 * Contains various utility methods used in all project
 *
 * @author Roberto Tomasoni
 * @version 1.0
 * @code 001
 */

public with sharing class Utils extends ApexServiceLibraryCnt {
    public static boolean firstTimeEntry = false;
    public static ServicePointQueries servicePointQuery = ServicePointQueries.getInstance();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    /**
     * Utility class for SelectOptions in lightning
     */
    public with sharing class KeyVal {
        @AuraEnabled
        public String key { get; set; }
        @AuraEnabled
        public Object value { get; set; }
        @AuraEnabled
        public Object label { get; set; }

        public KeyVal(String key, Object value) {
            this.key = key;
            this.value = value;
        }
        public KeyVal(String key, Object value, Object label) {
            this.key = key;
            this.value = value;
            this.label = label;
        }
    }

    /**
     * Method class that allows to get the system's constants on Lightning Component
     */
    public with sharing class getAllConstants extends AuraCallable {
        public override Object perform(final String jsonInput) {
            return Constants.getAllConstants();
        }
    }

    public with sharing class findObjectAPIName extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Id recId = params.get('objId');
            if (recId == null) {
                return null;
            }
            return recId.getSobjectType().getDescribe().getName();
        }
    }

    public static String findObjectAPINameById(Id recId){
        if(recId == null){
            return null;
        }
        return recId.getSobjectType().getDescribe().getName();
    }

    public static List<String> getLookupFields(String objName) {
        List<String> lookupFields = new List<String> ();
        //map<string, Map<String, Schema.SObjectField>> objectFieldsMap = new map<string, Map<String, Schema.SObjectField>>(); //Object field schema map
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();

        for (Schema.SObjectField fld : objectFields.values()) {

            schema.describeFieldResult dfield = fld.getDescribe();
            String fldType = String.valueOf(dfield.getType());

            if (fldType == 'REFERENCE') { // Lookup field!
                lookupFields.add(String.valueOf(dfield.Name));
            }
        }
        return lookupFields;
    }

    public static  Map<String, Map<String, Boolean>> passiveSwitchOut (String accountId, Map<String, Map<String, Date>> mapServicePointWithTraderIdAndDate,Boolean abortOnFirstError){

        Map<String, String> switchOutRecordTypes = Constants.getCaseRecordTypes('SwitchOut');
        Dossier__c  dossier = new Dossier__c ();
        Map<String, Case> newCases = new Map<String, Case>();

        Map<String, Map<String, Boolean>> servicePointResults = new Map<String, Map<String, Boolean>> ();

        Set<String> servicePointSet = new Set<String>();
        servicePointSet.addAll(mapServicePointWithTraderIdAndDate.keySet());
        List<ServicePoint__c> servicePoints = servicePointQuery.getListServicePointsByCodes(servicePointSet);
        Set<String> servicePointFoundSet = new Set<String>();
        for (ServicePoint__c servPoint : servicePoints) {
            servicePointFoundSet.add(servPoint.Code__c);
        }

        for(String servicePointCode : servicePointSet) {
            if(!servicePointFoundSet.contains(servicePointCode)) {
                servicePointResults.put(servicePointCode, new Map<String,Boolean>{System.Label.ServicePointDoesNotExist => false});
                continue;
            }

            ServicePoint__c myServicePoint = new ServicePoint__c();
            for (ServicePoint__c servicePoint : servicePoints) {
                if (servicePoint.Code__c == servicePointCode) {
                    myServicePoint = servicePoint;
                }
            }

            if (String.isBlank(myServicePoint.CurrentSupply__c)) {
                servicePointResults.put(myServicePoint.Code__c, new Map<String,Boolean>{System.Label.CurrentSupplyNotSetOnServicePoint => false});
                continue;
            }

            if (myServicePoint.CurrentSupply__r.Account__c != accountId) {
                servicePointResults.put(myServicePoint.Code__c, new Map<String,Boolean>{System.Label.ServicePointBelongsToAnotherAccount => false});
                continue;
            }

            if (myServicePoint.CurrentSupply__r.Status__c != 'Active') {
                servicePointResults.put(myServicePoint.Code__c, new Map<String,Boolean>{System.Label.CurrentSupplyIsNotActive => false});
                continue;
            }

            Case caseRecord = new Case(
                    AccountId = accountId,
                    RecordTypeId = myServicePoint.CurrentSupply__r.RecordType.DeveloperName == 'Gas' ? switchOutRecordTypes.get('SwitchOut_GAS') : switchOutRecordTypes.get('SwitchOut_ELE'),
                    CompanyDivision__c = myServicePoint.CurrentSupply__r.CompanyDivision__c,
                    BillingProfile__c = myServicePoint.CurrentSupply__r.ContractAccount__r.BillingProfile__c,
                    Supply__c = myServicePoint.CurrentSupply__c,
                    Trader__c = new List<String>(mapServicePointWithTraderIdAndDate.get(myServicePoint.Code__c).keySet())[0],
                    EffectiveDate__c = mapServicePointWithTraderIdAndDate.get(myServicePoint.Code__c).values()[0],
                    Status = 'New',
                    Origin = 'Phone'
            );
            newCases.put(myServicePoint.Code__c, caseRecord);
        }

        if ((abortOnFirstError) && (!servicePointResults.isEmpty())) {
            for (String code : newCases.keySet()) {
                servicePointResults.put(code, new Map<String,Boolean>{System.Label.LockedForErrorsOnOtherServicePointsAbortOnFirstError => false});
            }
        }

        if ((servicePointResults.isEmpty()) || (!abortOnFirstError)) {
            List<String> caseRetention = new List<String>();
            if (!newCases.isEmpty()) {
                dossier.Account__c = accountId;
                dossier.CompanyDivision__c = String.isNotBlank(newCases.values()[0].CompanyDivision__c) ? newCases.values()[0].CompanyDivision__c : null;
                dossier.RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
                dossier.Status__c = 'New';
                dossier.RequestType__c = 'SwitchOut';

                databaseSrv.insertSObject(dossier);

                for(Case c : newCases.values()) {
                    c.Dossier__c = dossier.Id;
                }
                databaseSrv.insertSObject(newCases.values());

                for (String code : newCases.keySet()) {
                    servicePointResults.put(code, new Map<String,Boolean>{newCases.get(code).Id => true});
                    caseRetention.add(newCases.get(code).Id);
                }
                caseService.sendRetentionCases(JSON.serialize(caseRetention));
            }
        }
        return servicePointResults;
    }

}