/**
 * Created by ferhati on 11/02/2019.
 */

public with sharing class OpportunityServiceItemEditCnt extends ApexServiceLibraryCnt {

    static OpportunityServiceItemQueries osiQueries = OpportunityServiceItemQueries.getInstance();
    static ServicePointQueries spQueries = ServicePointQueries.getInstance();

    public class getExtraFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String extraFieldsFieldSet = params.get('extraFieldsFieldSet');
            response = LightningUtils.getFieldSet('OpportunityServiceItem__c', extraFieldsFieldSet);
            return  response;
        }
    }

    public class getPointCodeFields extends AuraCallable{
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String servicePointId = params.get('servicePointId');

            if (String.isBlank(servicePointId)) {
                throw new WrtsException(System.Label.ServicePoint + ' - ' + System.Label.MissingId);
            }
            try {
                response.put('servicePointNotFound', true);

                ServicePoint__c servicePoint = spQueries.getById(servicePointId);
                AddressCnt.AddressDTO addressFromServicePoint = OpportunityServiceItemEditCnt.copyServicePointAddressToDTO(servicePoint);

                response.put('servicePointNotFound', (servicePoint == null));
                WrapperServicePoint WrapServicePoint = new WrapperServicePoint(servicePoint);
                response.put('servicePoint', WrapServicePoint);
                response.put('servicePointAddress', addressFromServicePoint);

                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;

        }

    }
    public class getOsiAddressField extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String osiId = params.get('osiId');

            if (String.isBlank(osiId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem + ' - ' + System.Label.Required);
            }
            try {
                OpportunityServiceItem__c osi = osiQueries.getById(osiId);
                if (osi != null) {
                    AddressCnt.AddressDTO addressFromOsi = OpportunityServiceItemEditCnt.copyOsiAddressToDTO(osi);
                    response.put('error', false);
                    response.put('osiAddress', addressFromOsi);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class WrapperServicePoint {
        @AuraEnabled
        public String VoltageLevel {get; set;}
        @AuraEnabled
        public String Voltage {get; set;}
        @AuraEnabled
        public String PowerPhase {get; set;}

        @AuraEnabled
        public String ConversionFactor {get; set;}
        @AuraEnabled
        public String PressureLevel {get; set;}
        @AuraEnabled
        public String Pressure {get; set;}
        @AuraEnabled
        public String Account {get; set;}
        @AuraEnabled
        public String Distributor {get; set;}
        @AuraEnabled
        public String AvailablePower {get; set;}
        @AuraEnabled
        public String EstimatedConsumption {get; set;}
        @AuraEnabled
        public String ContractualPower {get; set;}
        @AuraEnabled
        public String Trader {get; set;}
        @AuraEnabled
        public String Address {get; set;}

        public WrapperServicePoint(ServicePoint__c sp) {
            this.VoltageLevel = String.valueOf(sp.VoltageLevel__c);
            this.Voltage = String.valueOf(sp.Voltage__c);
            this.PowerPhase = String.valueOf(sp.PowerPhase__c);
            this.ConversionFactor = String.valueOf(sp.ConversionFactor__c);
            this.PressureLevel = String.valueOf(sp.PressureLevel__c);
            this.Pressure = String.valueOf(sp.Pressure__c);
            this.Account = sp.Account__c;
            this.Distributor = sp.Distributor__c;
            this.AvailablePower = String.valueOf(sp.AvailablePower__c);
            this.EstimatedConsumption = String.valueOf(sp.EstimatedConsumption__c);
            this.ContractualPower = String.valueOf(sp.ContractualPower__c);
            this.Trader = sp.Trader__c;


            String TempAddress = '';

            if (String.isNotBlank(sp.PointStreetName__c)) {
                TempAddress = TempAddress + sp.PointStreetName__c;
            }
            if (String.isNotBlank(sp.PointStreetNumber__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointStreetNumber__c;
                if (String.isNotBlank(sp.PointStreetNumberExtn__c)) {
                    TempAddress = TempAddress + sp.PointStreetNumberExtn__c;
                }
            }
            if (String.isNotBlank(sp.PointPostalCode__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointPostalCode__c;
            }
            if (String.isNotBlank(sp.PointCity__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointCity__c;
            }
            if (String.isNotBlank(sp.PointCountry__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointCountry__c;
            }

            this.Address = TempAddress;
        }
    }
    public class getRecordTypes extends  AuraCallable {
        public override Object perform(final  String jsonInput){
            Map<String,Object> response = new Map<String,Object>();
            try {
                Map<String, Schema.RecordTypeInfo> osiRts = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName();
                Schema.RecordTypeInfo electricRt = osiRts.get('Electric');
                Schema.RecordTypeInfo gasRt = osiRts.get('Gas');
                response.put('electricRecordType', new Map<String,String>{'label'=>electricRt.getName(),'value'=>electricRt.getRecordTypeId()});
                response.put('gasRecordType', new Map<String,String>{'label'=>gasRt.getName(),'value'=>gasRt.getRecordTypeId()});

                response.put('ss', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public static AddressCnt.AddressDTO copyOsiAddressToDTO(OpportunityServiceItem__c osi) {
        AddressCnt.AddressDTO addressDTO = new AddressCnt.AddressDTO();
        addressDTO.streetNumber = osi.PointStreetNumber__c;
        addressDTO.streetNumberExtn = osi.PointStreetNumberExtn__c;
        addressDTO.streetName = osi.PointStreetName__c;
        addressDTO.streetType = osi.PointStreetType__c;
        addressDTO.apartment = osi.PointApartment__c;
        addressDTO.building = osi.PointBuilding__c;
        addressDTO.city = osi.PointCity__c;
        addressDTO.country = osi.PointCountry__c;
        addressDTO.floor = osi.PointFloor__c;
        addressDTO.locality = osi.PointLocality__c;
        addressDTO.postalCode = osi.PointPostalCode__c;
        addressDTO.province = osi.PointProvince__c;
        addressDTO.addressNormalized = osi.PointAddressNormalized__c;

        return addressDTO;
    }

    public static AddressCnt.AddressDTO copyServicePointAddressToDTO(ServicePoint__c sp) {
        AddressCnt.AddressDTO addressDTO = new AddressCnt.AddressDTO();
        addressDTO.streetNumber = sp.PointStreetNumber__c;
        addressDTO.streetNumberExtn = sp.PointStreetNumberExtn__c;
        addressDTO.streetName = sp.PointStreetName__c;
        addressDTO.city = sp.PointCity__c;
        addressDTO.country = sp.PointCountry__c;
        addressDTO.postalCode = sp.PointPostalCode__c;
        addressDTO.streetType = sp.PointStreetType__c;
        addressDTO.apartment = sp.PointApartment__c;
        addressDTO.building = sp.PointBuilding__c;
        addressDTO.locality = sp.PointLocality__c;
        addressDTO.province = sp.PointProvince__c;
        addressDTO.floor = sp.PointFloor__c;
        addressDTO.addressNormalized = sp.PointAddressNormalized__c;

        return addressDTO;
    }
}