public with sharing class OpportunityServiceItemQueries {
    //static List<String> recordTypeWithDevName;
    public static OpportunityServiceItemQueries getInstance() {
        return (OpportunityServiceItemQueries)ServiceLocator.getInstance(OpportunityServiceItemQueries.class);
    }

    public List<OpportunityServiceItem__c> getOSIsByOpportunityId(Id opportunityId) {
        return [
                SELECT Id, Name, RecordTypeId, RecordType.Name, Account__c,
                        ServicePointCode__c, ServicePoint__c, ContractAccount__c, BillingProfile__c,ServicePoint__r.CurrentSupply__c,
                        ServicePoint__r.CurrentSupply__r.CompanyDivision__c
                FROM OpportunityServiceItem__c
                WHERE Opportunity__c = :opportunityId
        ];
    }

    public List<AggregateResult> getListOSIServiceSite(String accountId) {
        return [
                SELECT SiteAddressKey__c, MIN(Id) Id, MIN(ServiceSite__c) ServiceSite__c,
                        MIN(SiteApartment__c) SiteApartment__c, MIN(SiteBuilding__c) SiteBuilding__c, MIN(SiteCity__c) SiteCity__c, MIN(SiteCountry__c) SiteCountry__c,
                        MIN(SiteFloor__c) SiteFloor__c, MIN(SiteStreetType__c) SiteStreetType__c, MIN(SiteLocality__c) SiteLocality__c, MIN(SitePostalCode__c) SitePostalCode__c,
                        MIN(SiteProvince__c) SiteProvince__c, MIN(SiteStreetName__c) SiteStreetName__c, MIN(SiteStreetNumber__c) SiteStreetNumber__c, MIN(SiteStreetNumberExtn__c) SiteStreetNumberExtn__c
                FROM OpportunityServiceItem__c
                WHERE (Account__c = :accountId) AND (ServiceSite__c = null)
                GROUP BY SiteAddressKey__c
        ];
    }
/*
    public List<OpportunityServiceItem__c> getListOsiBySiteAddressKey(Set<Id> osiIds) {
        return [
                SELECT Id, ServiceSite__c, SiteAddressKey__c
                FROM OpportunityServiceItem__c
                WHERE SiteAddressKey__c in :osiIds
        ];
    }
*/
    public List<OpportunityServiceItem__c> getByIds(Set<String> siteAddressKeyIds) {
        return [
                SELECT Id, ServiceSite__c, SiteAddressKey__c, SiteApartment__c, Account__c,SiteAddressNormalized__c,
                        SiteBuilding__c, SiteCity__c, SiteCountry__c, SiteFloor__c, SiteStreetType__c, SiteLocality__c,
                        SitePostalCode__c, SiteProvince__c, SiteStreetName__c, SiteStreetNumber__c, SiteStreetNumberExtn__c
                FROM OpportunityServiceItem__c
                WHERE Id in :siteAddressKeyIds
        ];
    }

    public OpportunityServiceItem__c getOsiAddress(String addressId) {
        return [
                SELECT Id,  SiteApartment__c,SiteAddressNormalized__c,
                        SiteBuilding__c, SiteCity__c, SiteCountry__c, SiteFloor__c, SiteStreetType__c, SiteLocality__c,
                        SitePostalCode__c, SiteProvince__c, SiteStreetName__c, SiteStreetNumber__c, SiteStreetNumberExtn__c
                FROM OpportunityServiceItem__c
                WHERE Id = :addressId Limit 1
        ];
    }

    public OpportunityServiceItem__c getById(String OsiId) {
        List<OpportunityServiceItem__c> osiList = [
                SELECT Id, ServicePointCode__c, RecordType.DeveloperName, Product__c, Account__c,
                        Distributor__c, ContractAccount__c, Trader__c, Market__c, NonDisconnectable__c,
                        PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c,PointCountry__c, PointApartment__c,
                        PointStreetType__c, PointBuilding__c, PointCity__c,
                        PointAddressNormalized__c,
                        PointFloor__c, PointLocality__c, PointPostalCode__c,
                        PointProvince__c, Voltage__c, VoltageLevel__c, PressureLevel__c,
                        Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c,
                        AvailablePower__c, EstimatedConsumption__c,
                        Opportunity__c, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Pricebook2Id,
                        Opportunity__r.Type, Opportunity__r.Account.Name,
                        Opportunity__r.StageName, Opportunity__r.CloseDate, Opportunity__r.CompanyDivision__c,
                        ServicePoint__c, ServicePoint__r.CurrentSupply__c, BillingProfile__c,
                        ServiceSite__c,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,
                        SiteBuilding__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,SiteLocality__c,
                        SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c,
                        ServicePoint__r.CurrentSupply__r.CompanyDivision__c
                FROM OpportunityServiceItem__c
                WHERE Id = :OsiId
        ];
        return osiList.get(0);
    }


    public OpportunityServiceItem__c getOsiId(String servicePointId,String opportunityId) {
        List<OpportunityServiceItem__c> osiList = [
                SELECT Id, ServicePointCode__c, RecordType.DeveloperName, Product__c, Account__c,
                        Distributor__c, ContractAccount__c, Trader__c, Market__c, NonDisconnectable__c,
                        PointStreetName__c, PointStreetNumber__c, PointStreetNumberExtn__c, PointApartment__c,
                        PointStreetType__c, PointBuilding__c, PointCity__c,PointAddressNormalized__c,
                        PointCountry__c, PointFloor__c, PointLocality__c, PointPostalCode__c,
                        PointProvince__c, Voltage__c, VoltageLevel__c, PressureLevel__c,
                        Pressure__c, PowerPhase__c, ConversionFactor__c, ContractualPower__c,
                        AvailablePower__c, EstimatedConsumption__c,
                        Opportunity__c, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Pricebook2Id,
                        Opportunity__r.Type, Opportunity__r.Account.Name,
                        Opportunity__r.StageName, Opportunity__r.CloseDate, Opportunity__r.CompanyDivision__c,
                        ServicePoint__c, ServicePoint__r.CurrentSupply__c, BillingProfile__c,
                        ServiceSite__c,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,
                        SiteBuilding__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,SiteLocality__c,
                        SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c
                FROM OpportunityServiceItem__c
                WHERE ServicePoint__c= :servicePointId AND Opportunity__c = :opportunityId
        ];

        return osiList.get(0);
    }
}