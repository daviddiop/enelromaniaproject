public with sharing class MRO_SRV_ServicePoint {
    //FF OSI Edit - Pack2 - Interface Check
	private static MRO_QR_ServicePoint servicePointQuery = MRO_QR_ServicePoint.getInstance();
    private MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();

    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    //FF OSI Edit - Pack2 - Interface Check
	private static MRO_SRV_Address addressSrv = MRO_SRV_Address.getInstance();
	public static final Id SERVICE_POINT_ELE_RT = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId();
	public static final Id SERVICE_POINT_GAS_RT = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();

    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

	public static MRO_SRV_ServicePoint getInstance() {
        return (MRO_SRV_ServicePoint) ServiceLocator.getInstance(MRO_SRV_ServicePoint.class);
    }

    public static final Set<String> SUPPLY_STATUSES = new Set<String>{'Active', 'Terminating'};

    public Map<String, Object> findCode(String pointCode, String companyDivisionId) {

        Map<String, Object> response = new Map<String, Object>();
        if (String.isBlank(pointCode)) {
            return new Map<String, Object>{
                    'errorMsg' => 'Invalid code point',
                    'error' => true
            };
        }

        List<ServicePoint__c> myServicePoint = servicePointQuery.getListServicePoints(pointCode);

        //Check if the list is empty
        if(!myServicePoint.isEmpty()){
            response.put('servicePointId', myServicePoint[0].Id);
            response.put('servicePoint', myServicePoint[0]);
        }
        response.put('error', false);

        return response;
    }

    public List<ServicePoint__c> createTemporaryServicePointsForConnection(List<Case> connectionCases) {
        List<ServicePoint__c> servicePoints = new List<ServicePoint__c>();
        List<ServiceSite__c> serviceSites = new List<ServiceSite__c>();
        Id connectionEleRT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Connection_ELE').getRecordTypeId();

        Integer sequence = 0;
        for (Case connectionCase : connectionCases) {
            sequence++;
            Id servicePointRecordType;
            String pointCode = 'TMP';
            if (connectionCase.RecordTypeId == connectionEleRT) {
                servicePointRecordType = SERVICE_POINT_ELE_RT;
                pointCode += 'E';
            } else {
                throw new WrtsException('Wrong Case record type');
            }
            pointCode += String.valueOf(sequence).leftPad(2, '0') + DateTime.now().format('yyyyMMddHHmmssSSS');
            connectionCase.ServicePointCode__c = pointCode;

            ServicePoint__c servicePoint = new ServicePoint__c(
                RecordTypeId = servicePointRecordType,
                Name = pointCode,
                Code__c = pointCode,
                Account__c = connectionCase.AccountId,
                Distributor__c = connectionCase.Distributor__c,
                Voltage__c = connectionCase.Voltage__c,
                VoltageLevel__c = connectionCase.VoltageLevel__c,
                PressureLevel__c = connectionCase.PressureLevel__c,
                Pressure__c = connectionCase.Pressure__c,
                PowerPhase__c = connectionCase.PowerPhase__c,
                ConversionFactor__c = connectionCase.ConversionFactor__c,
                ContractualPower__c = connectionCase.ContractualPower__c,
                AvailablePower__c = connectionCase.AvailablePower__c,
                EstimatedConsumption__c = connectionCase.EstimatedConsumption__c,
                IsNewConnection__c = true
            );
            MRO_SRV_Address.AddressDTO caseAddress = new MRO_SRV_Address.AddressDTO(connectionCase, 'Address');
            caseAddress.populateRecordAddressFields(servicePoint, 'Point');
            ServiceSite__c serviceSite = new ServiceSite__c(
                    Account__c = connectionCase.AccountId,
                    NLC__c = pointCode
            );
            caseAddress.populateRecordAddressFields(serviceSite, 'Site');
            servicePoints.add(servicePoint);
            serviceSites.add(serviceSite);
        }
        databaseSrv.insertSObject(servicePoints);
        databaseSrv.insertSObject(serviceSites);
        return servicePoints;
    }
    /**********************************************************************
     * @author Boubacar Sow
     * @modified-date 26/12/2019
     * @description  Update the service point by the new case record.
     *              [ENLCRO-359] Power Voltage Change Implementation Commit
     * @param servicePoint
     * @param caseRecord
     *
     * @return ServicePoint__c
     */
    public  ServicePoint__c updateServicePoint( ServicePoint__c servicePoint, Case caseRecord){
        String strRecordDevName = Schema.SObjectType.Case.getRecordTypeInfosById().get(caseRecord.RecordTypeId).getDeveloperName();

        servicePoint.AvailablePower__c = caseRecord.AvailablePower__c;
        servicePoint.ContractualPower__c = caseRecord.ContractualPower__c;
        servicePoint.EstimatedConsumption__c = caseRecord.EstimatedConsumption__c;
        if (strRecordDevName == 'TechnicalDataChange_ELE' || strRecordDevName == 'Connection_ELE') {
            servicePoint.PowerPhase__c = caseRecord.PowerPhase__c;
            servicePoint.VoltageLevel__c = caseRecord.VoltageLevel__c;
            servicePoint.Voltage__c = caseRecord.Voltage__c;
        } else if( strRecordDevName == 'TechnicalDataChange_GAS'){
            servicePoint.ConversionFactor__c = caseRecord.ConversionFactor__c;
            servicePoint.PressureLevel__c = caseRecord.PressureLevel__c;
            servicePoint.Pressure__c = caseRecord.Pressure__c;
        }
        if (strRecordDevName == 'Connection_ELE') {
            MRO_SRV_Address.copyAddress(caseRecord, 'Address', servicePoint, 'Point');
        }
        return servicePoint;
    }

    public void setConsumerType(List<ServicePoint__c> newServicePointList, Map<Id, ServicePoint__c> idToOldServicePoint) {
        Set<Id> servicePointIds = new Set<Id>();
        for (ServicePoint__c newServicePoint : newServicePointList) {
            ServicePoint__c oldServicePoint = idToOldServicePoint.get(newServicePoint.Id);
            Boolean currentSupplyChanged = newServicePoint.CurrentSupply__c != oldServicePoint.CurrentSupply__c;
            Boolean currentSupplyNotEmpty = newServicePoint.CurrentSupply__c != null;
            Boolean isElectric = newServicePoint.RecordTypeId == SERVICE_POINT_ELE_RT;
            if (currentSupplyChanged && currentSupplyNotEmpty && isElectric) {
                servicePointIds.add(newServicePoint.Id);
            }
        }

        if (servicePointIds.isEmpty()) {
            return;
        }

        List<ServicePoint__c> servicePointList = servicePointQuery.getListServicePoints(servicePointIds);
        for (ServicePoint__c servicePoint : servicePointList) {
            //if (!SUPPLY_STATUSES.contains(servicePoint.CurrentSupply__r.Status__c)) {
            //    continue;
            //}
            if (servicePoint.CurrentSupply__r.Contract__r.ContractType__c == CONSTANTS.CONTRACTTYPE_RESIDENTIAL) {
                servicePoint.ConsumerType__c = CONSTANTS.CONSUMERTYPE_RESIDENTIAL;
            } else if (servicePoint.CurrentSupply__r.Contract__r.ContractType__c == CONSTANTS.CONTRACTTYPE_BUSINESS) {
                if (servicePoint.ContractualPower__c > 100) {
                    servicePoint.ConsumerType__c = CONSTANTS.CONSUMERTYPE_LARGE_BUSINESS;
                }
                else {
                    servicePoint.ConsumerType__c = CONSTANTS.CONSUMERTYPE_SMALL_BUSINESS;
                }
            }
        }
        databaseSrv.updateSObject(servicePointList);
    }

    public List<ServicePoint__c> updateServicePoints(List<ServicePoint__c> servicePointList) {
        List<ServicePoint__c> output = servicePointList;
        if(servicePointList!=null && !servicePointList.isEmpty()) {
            try {
                databaseSrv.updateSObject(output);
            } catch (Exception e) {
                throw e;
            }
        }
        return output;
    }

    public  ServicePoint__c updateServicePointAddress( ServicePoint__c servicePoint, Case caseRecord){
        servicePoint.PointStreetType__c = caseRecord.AddressStreetType__c;
        servicePoint.PointStreetNumber__c = caseRecord.AddressStreetNumber__c;
        servicePoint.PointCity__c = caseRecord.AddressCity__c;
        servicePoint.PointPostalCode__c = caseRecord.AddressPostalCode__c;
        servicePoint.PointCountry__c = caseRecord.AddressCountry__c;
        servicePoint.PointAddressNormalized__c = caseRecord.AddressAddressNormalized__c;
        servicePoint.PointStreetName__c = caseRecord.AddressStreetName__c;
        servicePoint.PointApartment__c = caseRecord.AddressApartment__c;
        servicePoint.PointFloor__c = caseRecord.AddressFloor__c;
        servicePoint.PointLocality__c = caseRecord.AddressLocality__c;
        servicePoint.PointProvince__c = caseRecord.AddressProvince__c;
        servicePoint.PointBuilding__c = caseRecord.AddressBuilding__c;
        servicePoint.PointBlock__c = caseRecord.AddressBlock__c;
        servicePoint.PointStreetNumberExtn__c = caseRecord.AddressStreetNumberExtn__c;
        servicePoint.PointStreetId__c = caseRecord.AddressStreetId__c;
        return servicePoint;
    }
}