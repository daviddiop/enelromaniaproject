public with sharing class ContactService {

    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    public static ContactService getInstance() {
        return (ContactService)ServiceLocator.getInstance(ContactService.class);
    }

    public Contact create(String firstName, String lastName, String individualId, String accountId) {
        return new Contact(
            FirstName = firstName,
            LastName = lastName,
            IndividualId = individualId,
            AccountId = accountId
        );
    }

    public Contact insertForIndividual(Individual individual, String accountId) {
        Contact newContact = new Contact(
            FirstName = individual.FirstName,
            LastName = individual.LastName,
            NationalIdentityNumber__c = individual.NationalIdentityNumber__c,
            IndividualId = individual.Id,
            AccountId = accountId
        );
        databaseSrv.insertSObject(newContact);
        return newContact;
    }
}