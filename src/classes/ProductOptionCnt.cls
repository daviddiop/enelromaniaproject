/**
* @description Class for productOption
* @author Lili / Baba
* @creationDate 15/03/2019
* @date  29/10/2019 - Refactoring BG
*/
public with sharing class ProductOptionCnt extends ApexServiceLibraryCnt {
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    static ProductOptionQueries productOptionQuery = ProductOptionQueries.getInstance();
    static Map<Id, String> recordTypes = new Map<Id, String>();
    static Map<String, Id> recordTypeWithDevName = new Map<String, Id>();
    static final String OBJECT_NAME = 'ProductOption__c';

    /**
     * Method for retrieve Picklist Values
     *
     * @param void
     *
     * @return All record type object's
     */
    public class getPicklistFamilies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            try {
                List<String> pickListValuesList = new List<String>();
                Schema.DescribeFieldResult fieldResult = Product2.Family.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for (Schema.PicklistEntry pickListVal : ple) {
                    pickListValuesList.add(pickListVal.getLabel());
                }
                response.put('familiesOptionList', pickListValuesList);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    /**
     * Method for retrieve Record type of Product Option Sobject
     *
     * @param void
     *
     * @return All record type object's
     */
    public class getRecordTypeProductOption extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('recordId');
            response.put('error', false);
            try {
                fillRecordTypeMaps(OBJECT_NAME);
                if (!String.isBlank(recordId)) {
                    ProductOption__c product = productOptionQuery.getProductById(recordId);
                    response.put('recordTypeIdValue', product.RecordTypeId);
                    response.put('familiesValue', product.Family__c);
                    response.put('minValue', product.Min__c);
                    response.put('maxValue', product.Max__c);
                    response.put('typeValue', product.Type__c);
                    response.put('dfValue', product.DefaultValue__c);
                    response.put('valueSetValue', product.ValueSet__c);
                }
                response.put('recordTypes', recordTypes);
                response.put('valueRecordTypeId', recordTypeWithDevName.get('Value'));
                response.put('productRecordTypeId', recordTypeWithDevName.get('Product'));
                response.put('familyRecordTypeId', recordTypeWithDevName.get('Family'));
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class createProdOptRelation extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String productOptionId = params.get('productOptionId');
            String productId = params.get('productId');
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            response.put('error', false);
            try {
                if (String.isNotBlank(productOptionId) && String.isNotBlank(productId)) {
                    ProductOptionRelation__c productOptionRelation = new ProductOptionRelation__c(
                            ProductOption__c = productOptionId,
                            Product__c = productId
                    );
                    databaseSrv.insertSObject(productOptionRelation);
                }
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    /**
    * fill map recordtype map that have couple key = Name and value recordtype Id
    *
    * @param: Object Name
    */
    private static void fillRecordTypeMaps(String sobjectType) {
        for (RecordType rt : [SELECT Id,Name, DeveloperName FROM RecordType WHERE SobjectType = :sobjectType]) {
            recordTypes.put(rt.Id, rt.Name);
            recordTypeWithDevName.put(rt.DeveloperName, rt.Id);
        }
    }
}