public with sharing class DatabaseService {

    public static DatabaseService getInstance() {
        return (DatabaseService)ServiceLocator.getInstance(DatabaseService.class);
    }

    private static final String MAX_NUMBER_RECORDS_EXCEPTION = 'You can pass a maximum of 10,000 sObject records to a single DML operation.';
    private static final String PERMISSION_EXCEPTION = 'User {0} does not have permission to perform an operation on object type {1}';

    private final List<sObject> sObjectList = new List<sObject>();

// TODO: ST No usages
//    public void addSobject(final sObject sObj) {
//        this.sObjectList.add(sObj);
//    }

    public Boolean insertSObject(SObject sObj) {
        return insertSObject(new List<sObject>{sObj});
    }

    public Boolean insertSObject(List<SObject> sObjectList) {
        if (checkBeforeDml(sObjectList, DmlOperation.DML_INSERT)) {
            return true;
        }
        List<Database.SaveResult> insertResult = Database.insert(sObjectList, true);
        //TODO: handle result
        return true;
    }

    public Boolean updateSObject(SObject sObj) {
        return updateSObject(new List<sObject>{sObj});
    }

    public Boolean updateSObject(List<SObject> sObjectList) {
        if (checkBeforeDml(sObjectList, DmlOperation.DML_UPDATE)) {
            return true;
        }
        List<Database.SaveResult> updateResult = Database.update(sObjectList, true);
        return true;
    }

    public Boolean upsertSObject(SObject sObj) {
        return upsertSObject(new List<sObject>{sObj});
    }

    public Boolean upsertSObject(List<SObject> sObjectList) {
        if (checkBeforeDml(sObjectList, DmlOperation.DML_UPSERT)) {
            return true;
        }
        List<sObject> toInsert = new List<SObject>();
        List<sObject> toUpdate = new List<SObject>();
        for (sObject sObj : sObjectList) {
            if (String.isNotBlank(sObj.id)) {
                toUpdate.add(sObj);
            } else {
                toInsert.add(sObj);
            }
        }
        insertSObject(toInsert);
        updateSObject(toUpdate);
        return true;
    }
// TODO: ST NO usages
//    public Boolean deleteSObject(SObject sObj) {
//        return deleteSObject(new List<sObject>{sObj});
//    }

    public Boolean deleteSObject(Id sObjId) {
        return deleteSObject(new List<Id>{sObjId});
    }

    public Boolean deleteSObject(List<SObject> sObjectList) {
        if (checkBeforeDml(sObjectList, DmlOperation.DML_DELETE)) {
            return true;
        }
        List<Database.DeleteResult> deleteResult = Database.delete(sObjectList, true);
        return true;
    }

    public Boolean deleteSObject(List<Id> sObjectIds) {
        if (checkBeforeDml(sObjectIds, DmlOperation.DML_DELETE)) {
            return true;
        }
        List<Database.DeleteResult> deleteResult = Database.delete(sObjectIds, true);
        return true;
    }

    private Boolean checkBeforeDml(List<SObject> sObjectList, DmlOperation operation) {
        if (sObjectList.isEmpty()) {
            return true;
        }
        if (isDmlNotAllowed(sObjectList.size())) {
            throw new WrtsException.DatabaseException(MAX_NUMBER_RECORDS_EXCEPTION);
        }
        if (!checkDmlPermissions(sObjectList, operation)) {
            throw new WrtsException.DatabaseException(String.format(
                PERMISSION_EXCEPTION, new List<String> {UserInfo.getUserId(), SobjectUtils.getSobjectTypeName(sObjectList)}));
        }
        return false;
    }

    private Boolean checkBeforeDml(List<Id> idList, DmlOperation operation) {
        if (idList.isEmpty()) {
            return true;
        }
        if (isDmlNotAllowed(idList.size())) {
            throw new WrtsException.DatabaseException(MAX_NUMBER_RECORDS_EXCEPTION);
        }
        if (!checkDmlPermissions(idList, operation)) {
            throw new WrtsException.DatabaseException(String.format(
                PERMISSION_EXCEPTION, new List<String> {UserInfo.getUserId(), SobjectUtils.getSobjectTypeName(idList)}));
        }
        return false;
    }

    private Boolean isDmlNotAllowed(Integer recordsNumber) {
        return (Limits.getDMLRows() + recordsNumber) > Limits.getLimitDMLRows();
    }

    private Boolean checkDmlPermissions(List<SObject> sObjectList, DmlOperation operation) {
        for (sObject sObj : sObjectList) {
            Boolean isPermissionGranted = checkDmlPermission(sObj.getSObjectType(), operation);
            if (!isPermissionGranted) {
                return false;
            }
        }
        return true;
    }

    private Boolean checkDmlPermissions(List<Id> idList, DmlOperation operation) {
        for (Id theId : idList) {
            Boolean isPermissionGranted = checkDmlPermission(theId.getSObjectType(), operation);
            if (!isPermissionGranted) {
                return false;
            }
        }
        return true;
    }

    private Boolean checkDmlPermission(SObjectType sobjType, DmlOperation operation) {
        DescribeSObjectResult sObjectDescribe = sobjType.getDescribe();
        if (operation == DmlOperation.DML_INSERT) {
            return sObjectDescribe.isCreateable();
        } else if (operation == DmlOperation.DML_UPDATE) {
            return sObjectDescribe.isUpdateable();
        } else if (operation == DmlOperation.DML_UPSERT) {
            return sObjectDescribe.isCreateable() && sObjectDescribe.isUpdateable();
        } else if (operation == DmlOperation.DML_DELETE) {
            return sObjectDescribe.isDeletable();
        }
        return false;
    }

    enum DmlOperation {
        DML_INSERT,
        DML_UPDATE,
        DML_UPSERT,
        DML_DELETE
    }
}