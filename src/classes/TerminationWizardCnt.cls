public with sharing class TerminationWizardCnt extends ApexServiceLibraryCnt {

    private static CaseQueries caseQuery = CaseQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static DossierService dossierServ = DossierService.getInstance();
    private static CaseService caseServ = CaseService.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Disconnection').getRecordTypeId();
    static List<String> disconnectionCausePicklistValues = new List<String>();
    static String disconnectionCauseLabel;
    static String disconnectionDateLabel;

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            String billingProfileId = '';

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account acc = accQuery.findAccount(accountId);
                Dossier__c dossier = dossierServ.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType,'Termination');
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);

                if (!cases.isEmpty()) {
                    billingProfileId = cases[0].BillingProfile__c;
                }

                getDisconnectionDescribe();

                response.put('caseTile', cases);
                response.put('billingProfileId', billingProfileId);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('companyDivisionName', dossier.CompanyDivision__r.Name);
                response.put('companyDivisionId', dossier.CompanyDivision__c);
                response.put('account', acc);
                response.put('accountId', accountId);
                response.put('accountPersonRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Person').getRecordTypeId());
                response.put('accountPersonProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonProspect').getRecordTypeId());
                response.put('accountBusinessRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business').getRecordTypeId());
                response.put('accountBusinessProspectRT', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessProspect').getRecordTypeId());
                response.put('disconnectionCausePicklistValues', disconnectionCausePicklistValues);
                response.put('disconnectionCauseLabel', disconnectionCauseLabel);
                response.put('disconnectionDateLabel', disconnectionDateLabel);

                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }
    public static void getDisconnectionDescribe() {
        disconnectionCausePicklistValues = new List<String>();
        Schema.DescribeFieldResult disconnectionCause = Case.Reason__c.getDescribe();
        Schema.DescribeFieldResult disconnectionDate = Case.EffectiveDate__c.getDescribe();
        List<Schema.PicklistEntry> disconnectionCausePicklist = disconnectionCause.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : disconnectionCausePicklist) {
            disconnectionCausePicklistValues.add(pickListVal.getValue());
        }
        disconnectionCauseLabel = disconnectionCause.getLabel();
        disconnectionDateLabel = disconnectionDate.getLabel();
    }
    public with sharing class createCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            Map<String, String> terminationRecordTypes = Constants.getCaseRecordTypes('Termination');
            String recordTypeId;
            Id trader = null;

            List<Supply__c> searchedSupplyFieldsList = (List<Supply__c>) JSON.deserialize(params.get('searchedSupplyFieldsList'), List<Supply__c>.class);
            List<Case> caseList = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
            Date effectiveDate = (Date) JSON.deserialize(params.get('disconnectionDate'), Date.class);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String reason = params.get('disconnectionCause');
            List<Case> newCases = new List<Case>();
            try {
                if (String.isNotBlank(reason) && (!searchedSupplyFieldsList.isEmpty()) && (effectiveDate != null) && (String.isNotBlank(accountId)) && (String.isNotBlank(dossierId))) {
                    for (Supply__c supply : searchedSupplyFieldsList) {
                        recordTypeId = supply.RecordType.DeveloperName == 'Gas' ? terminationRecordTypes.get('Termination_GAS') : supply.RecordType.DeveloperName == 'Electric' ? terminationRecordTypes.get('Termination_ELE') : terminationRecordTypes.get('Termination_SRV');
                        Case disconnectedCase = caseServ.createCaseRecord(supply, accountId, dossierId, recordTypeId, reason, effectiveDate, trader);
                        newCases.add(disconnectedCase);
                    }
                    List<Case> updatedCases = caseServ.insertCaseRecords(newCases, caseList);
                    response.put('caseTile', updatedCases);
                    response.put('error', false);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class saveDraftBP extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String billingProfileId = params.get('billingProfileId');
            Savepoint sp = Database.setSavepoint();

            try {
                caseServ.updateCaseByBillingProfile(oldCaseList, billingProfileId);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class updateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);

            List<Supply__c> listSupplies = (List<Supply__c>) JSON.deserialize(params.get('searchedSupplyFieldsList'), List<Supply__c>.class);
            String companyDivisionId;
            String dossierId = params.get('dossierId');
            String billingProfileId = params.get('billingProfileId');
            Savepoint sp = Database.setSavepoint();

            try {
                if (oldCaseList[0] != null) {
                    companyDivisionId = oldCaseList[0].CompanyDivision__c;
                }

                caseServ.changeStatusOnCaseAndDossierAndSupply(oldCaseList, dossierId, billingProfileId, companyDivisionId);
                response.put('error', false);

            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            Savepoint sp = Database.setSavepoint();

            try {
                caseServ.setCanceledOnCaseAndDossier(oldCaseList, dossierId);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }
}