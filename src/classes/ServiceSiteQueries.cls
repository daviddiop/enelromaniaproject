/**
 * Created by ferhati on 05/09/2019.
 */

public inherited sharing class ServiceSiteQueries {

    public static ServiceSiteQueries getInstance() {
        return (ServiceSiteQueries)ServiceLocator.getInstance(ServiceSiteQueries.class);
    }


    public List<ServiceSite__c> getListServiceSite(String accountId) {
        return [
                SELECT Id,SiteAddressKey__c,SiteAddressNormalized__c,SiteApartment__c,SiteBuilding__c,SiteCity__c,SiteCountry__c,SiteFloor__c,SiteStreetType__c,
                        SiteLocality__c,SitePostalCode__c,SiteProvince__c,SiteStreetName__c,SiteStreetNumber__c,SiteStreetNumberExtn__c
                FROM ServiceSite__c
                WHERE Account__c = :accountId
        ];
    }

}