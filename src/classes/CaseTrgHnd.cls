/**
 * Created by Roberto Tomasoni on 21/03/2019.
 */

global without sharing class CaseTrgHnd implements TriggerManager.ISObjectTriggerHandler {    
    global void beforeInsert() {}
    global void beforeDelete() {}
    global void beforeUpdate() {}
    global void afterInsert() {}
    global void afterDelete() {}
    global void afterUndelete() {} 
    global void afterUpdate() {        
        //>>>EAP-168 LC20190506 Process Commit
        System.debug('1 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());        
        //Add list case for each recordType will be process commit or cancellation
        CaseService caseServ = CaseService.getInstance();
        List<Case> caseSwitchInExecute = new List<Case>();
        List<Case> caseSwitchInCancel = new List<Case>();
        List<Case> caseTerminationExecute = new List<Case>();
        List<Case> caseTerminationCancel = new List<Case>();
        List<Case> casesSwitchOutExecute = new List<Case>();
        List<Case> caseSwitchOutCancel = new List<Case>();
        List<Case> casesBillingExecute = new List<Case>();
        List<Case> caseBillingCancel = new List<Case>();
        List<Case> casesConnectionExecute = new List<Case>();
        List<Case> casesActivationExecute = new List<Case>();
        List<Case> casesActivationCancel = new List<Case>();
        List<Case> casesReactivationExecute = new List<Case>();
        List<Case> casesReactivationCancel = new List<Case>();
        List<Case> casesTransferExecute = new List<Case>();
        List<Case> casesTransferCancel = new List<Case>();
        List<Case> casesProductChangeExecute = new List<Case>();
        List<Case> casesProductChangeCancel = new List<Case>();
        List<Case> casesRegistryChangeExecute = new List<Case>();
        List<Case> casesRegistryChangeCancel = new List<Case>();
        List<Case> casesTechnicalDataChangesExecute = new List<Case>();
        List<Case> casesContractMergeExecute = new List<Case>();

        for(Case caseRecord : (List<Case>)Trigger.new) {
            if (Constants.getCaseRecordTypes('SwitchIn').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    caseSwitchInExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    caseSwitchInCancel.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('Termination').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    caseTerminationExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    caseTerminationCancel.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('SwitchOut').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesSwitchOutExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    caseSwitchOutCancel.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('ProductChange').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesProductChangeExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    casesProductChangeCancel.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('BillingProfileChange').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesBillingExecute.add(caseRecord);
                }/*else if(caseRecord.Status=='Canceled'){
                    caseBillingCancel.add(caseRecord);
                }*/
            }
            if (Constants.getCaseRecordTypes('Connection').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesConnectionExecute.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('Activation').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesActivationExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    casesActivationCancel.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('Reactivation').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesReactivationExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    casesReactivationCancel.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('Transfer').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesTransferExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    casesTransferCancel.add(caseRecord);
                }
            }
            if (Constants.getCaseRecordTypes('RegistryChange').values().contains(caseRecord.RecordTypeId)) {
                if(caseRecord.Status=='Executed') {
                    casesRegistryChangeExecute.add(caseRecord);
                }else if(caseRecord.Status=='Canceled'){
                    casesRegistryChangeCancel.add(caseRecord);
                }
            }

            if(Constants.getCaseRecordTypes('TechnicalDataChange').values().contains(caseRecord.RecordTypeId)){
                if (caseRecord.Status == 'Executed') {
                    casesTechnicalDataChangesExecute.add(caseRecord);
                }
            }

            if(Constants.getCaseRecordTypes('ContractMerge').values().contains(caseRecord.RecordTypeId)){
                if (caseRecord.Status == 'Executed') {
                    casesContractMergeExecute.add(caseRecord);
                }
            }
        }

        //Update RecordType Electric or Gas
        if (!casesTechnicalDataChangesExecute.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitTechnicalData(casesTechnicalDataChangesExecute);
        }

        //ContractMerge
        if (!casesContractMergeExecute.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitContractMerge(casesContractMergeExecute);
        }

        //SwitchIn
        if(!caseSwitchInExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitSwitchIn(caseSwitchInExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!caseSwitchInCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelSwitchIn(caseSwitchInCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        //Termination
        if(!caseTerminationExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitTermination(caseTerminationExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!caseTerminationCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelTermination(caseTerminationCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        //SwitchOut
        if(!casesSwitchOutExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitSwitchOut(casesSwitchOutExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!caseSwitchOutCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelSwitchOut(caseSwitchOutCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        //Product Change
        if(!casesProductChangeExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitProductChange(casesProductChangeExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!casesProductChangeCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelProductChange(casesProductChangeCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        //BillingProfile
        if(!casesBillingExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            //cmtSrv.commitBillingProfileChange(casesBillingExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        /*if(!caseBillingCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelBillingProfileChange(caseBillingCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }*/
        //<<<EAP-168 LC20190506 Process Commit

        //Connection
        if(!casesConnectionExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitConnection(casesConnectionExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }

        if(!casesActivationExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitActivation(casesActivationExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!casesActivationCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelActivation(casesActivationCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!casesReactivationExecute.isEmpty()){
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitReactivation(casesReactivationExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!casesReactivationCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelReactivation(casesReactivationCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!casesTransferExecute.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitTransfer(casesTransferExecute);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!casesTransferCancel.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.cancelTransfer(casesTransferCancel);
            System.debug('2 Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        }
        if(!casesRegistryChangeExecute.isEmpty()) {
            CommitService cmtSrv = new CommitService();
            cmtSrv.commitRegistryChange(casesRegistryChangeExecute);
        }
    }   
}