@isTest
public with sharing class MRO_LC_CustomLookupTst {

    @testSetup
    static void setup () {
        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createCustomerCodeSequencer().build();
        insert sequencer;

        List<Account> accountList = new List<Account>();
        for (Integer i = 0; i < 5; i++) {
            Account accountTest = new Account();
            accountTest.Name = 'Test '+i;
            accountList.add(accountTest);
        }
        insert accountList;
    }

    @isTest
    public static void testInitialize () {

        Account acc = [SELECT Id, Name FROM Account WHERE Name='Test 0'];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.initialize('Account',
                'Name', '', 'Name', 'BillingState, Industry',
                '', 'Name', acc.Id);
        Test.stopTest();
        Account returnedAccount = (Account)returnedMap.get('initialRecord');
        System.assertEquals(acc.Id, returnedAccount.Id);
    }

    @isTest
    public static void testInitializeWithPrimaryDisplayField () {

        Account acc = [SELECT Id, Name FROM Account WHERE Name='Test 0'];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.initialize('Account',
                'Name, Industry', '', 'Name', 'BillingState, Industry',
                'Industry', 'BillingAddress', acc.Id);
        Test.stopTest(  );
        Account returnedAccount = (Account)returnedMap.get('initialRecord');
        System.assertEquals(acc.Id, returnedAccount.Id);
    }

    @isTest
    public static void testInitialize2 () {

//        This method tests the initialize method with the queryFields param which includes the primaryDisplayField and secondaryDisplayField
        Account acc = [SELECT Id, Name FROM Account WHERE Name='Test 0'];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.initialize('Account',
                'Name, Industry ', '', 'Name', 'BillingState, Industry',
                'Industry', 'Name', acc.Id);
        Test.stopTest();
        Account returnedAccount = (Account)returnedMap.get('initialRecord');
        System.assertEquals(acc.Id, returnedAccount.Id);
    }

    @isTest
    public static void testInitialize3 () {

//        This method tests the initialize method with the queryFields param which does not contains the primaryDisplayField and the secondaryDisplayField
        Account acc = [SELECT Id, Name FROM Account WHERE Name='Test 0'];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.initialize('Account',
                'Name', '', 'Name', 'BillingState, Industry',
                'Industry', '', acc.Id);
        Test.stopTest(  );
        Account returnedAccount = (Account)returnedMap.get('initialRecord');
        System.assertEquals(acc.Id, returnedAccount.Id);
    }

    @isTest
    public static void testInitialize4 () {

//        This method tests the initialize method with the queryFields param which includes only the secondaryDisplayField
        Account acc = [SELECT Id, Name FROM Account WHERE Name='Test 0'];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.initialize('Account',
                'Name ', '', 'Name', 'BillingState, Industry',
                'Industry', 'BillingAddress', acc.Id);
        Test.stopTest(  );
        Account returnedAccount = (Account)returnedMap.get('initialRecord');
        System.assertEquals(acc.Id, returnedAccount.Id);
    }

    @isTest
    public static void testInitializeError () {

        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.initialize(null, null, null, null, null, null, null, null);
        Test.stopTest();
        System.assertEquals(true, returnedMap.get('error'));
    }

    @isTest
    static void testFetchLookupValues () {

        List<Account> accountList = [SELECT Id, Name
                                     FROM Account];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                                                    'Name', '', 'Name', 'BillingState, Industry',
                                           '', '', '5');
        Test.stopTest();
        List<Account> returnedAccounts = (List<Account>) returnedMap.get('returnList');
        System.assertEquals(accountList, returnedAccounts);
    }

    @isTest
    static void testFetchLookupValuesWithoutFields () {

        List<Account> accountList = [SELECT Id, Name
                                     FROM Account];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                                                    '', '', 'Name', 'BillingState, Industry',
                                           'BillingAddress', 'Name', '5');

        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                                                        '', 'Test', 'Name', 'BillingState, Industry',
                                                        '', '', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                                                        '', 'Test', 'Name', 'BillingState, Industry',
                                                        'BillingAddress', 'Name', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('', 'Account',
                                                        '', '', 'Name', 'BillingState, Industry',
                                                        '', '', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                                                    '', '', 'Name', 'BillingState, Industry',
                                           '', '', '5');
        Test.stopTest();
        List<Account> returnedAccounts = (List<Account>) returnedMap.get('returnList');
        System.assertEquals(accountList, returnedAccounts);
    }

    @isTest
    static void testFetchLookupValuesWithoutRecordDescription () {

        List<Account> accountList = [SELECT Id, Name
        FROM Account];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                                                    'Name', '', 'Name', 'BillingState, Industry', '',
                                           '', '5');
        Test.stopTest();
        List<Account> returnedAccounts = (List<Account>) returnedMap.get('returnList');
        System.assertEquals(accountList, returnedAccounts);
    }

    @isTest
    static void testFetchLookupValuesWithoutFieldsAndRecordDescription () {

        List<Account> accountList = [SELECT Id, Name FROM Account];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                                                    '', '', 'Name', 'BillingState, Industry', '',
                                           '', '5');
        Test.stopTest();
        List<Account> returnedAccounts = (List<Account>) returnedMap.get('returnList');
        System.assertEquals(accountList, returnedAccounts);
    }

    @isTest
    static void testFetchLookupValues2 () {

//        This method tests the fetchLookupValues with an empty search keyword and without primaryDisplayField
        List<Account> accountList = [SELECT Id, Name FROM Account];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('', 'Account',
                'Name', '', 'Name', 'BillingState, Industry',
                '', 'Name', '5');
        Test.stopTest();
        List<Account> returnedAccounts = (List<Account>) returnedMap.get('returnList');
        System.assertEquals(accountList, returnedAccounts);
    }

    @isTest
    static void testFetchLookupValues3 () {

//        This method tests the fetchLookupValues with empty queryFields param
        List<Account> accountList = [SELECT Id, Name FROM Account ORDER BY Industry ASC LIMIT 5];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                '', '', 'Name', 'BillingState, Industry',
                'Industry', '', '5');
        Test.stopTest();
        List<Account> returnedAccounts = (List<Account>) returnedMap.get('returnList');
        for (Account acc : accountList) {

//            System.assertEquals(accountList, returnedAccounts);
            System.assert(returnedAccounts.contains(acc));
        }
    }

    @isTest
    static void testFetchLookupValues4 () {

//        This method tests the fetchLookupValues with empty queryFields and secondaryDisplayField empty
        List<Account> accountList = [SELECT Id, Name FROM Account];
        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                '', '', 'Name', 'BillingState, Industry',
                'Industry', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                '', '', 'Name', 'BillingState, Industry',
                '', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                '', '', 'Name', 'BillingState, Industry',
                '', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('', 'Account',
                'BillingAddress,Name', '', 'Name', 'BillingState, Industry',
                'Name', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                'BillingAddress', '', 'Name', 'BillingState, Industry',
                'Name', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                'BillingAddress', '', 'Name', 'BillingState, Industry',
                'Name', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                'BillingCity', '', 'Name', 'BillingState, Industry',
                'Name', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                'BillingCity,BillingAddress,Name', 'Test', 'Name', 'BillingState, Industry',
                'Name', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                'BillingCity', 'Test', 'Name', 'BillingState, Industry',
                '', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('Test', 'Account',
                'BillingCity,Name', 'Test', 'Name', 'BillingState, Industry',
                'Name', 'BillingAddress', '5');
        returnedMap.clear();
        returnedMap = MRO_LC_CustomLookup.fetchLookupValues('', 'Account',
                '', 'Test', 'Name', 'BillingState, Industry',
                'Industry', '', '5');
        Test.stopTest();
        List<Account> returnedAccounts = (List<Account>) returnedMap.get('returnList');
        System.assertEquals(accountList, returnedAccounts);
    }

    @isTest
    static void testFetchLookupValuesError () {

        Test.startTest();
        Map<String, Object> returnedMap = MRO_LC_CustomLookup.fetchLookupValues('', null,
                                                '', '', '', '', '', '', '');
        Test.stopTest();
        System.assertEquals(true, returnedMap.get('error'));
    }

}