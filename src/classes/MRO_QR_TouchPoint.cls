/**
 * Created by tommasobolis on 30/04/2020.
 */
public with sharing class MRO_QR_TouchPoint {

    /**
     * Returns and instance of the current class.
     *
     * @return instance of the current class.
     */
    public static MRO_QR_TouchPoint getInstance() {

        return (MRO_QR_TouchPoint) ServiceLocator.getInstance(MRO_QR_TouchPoint.class);
    }

    /**
     * Returns the touch-point configuration with the provided developer name
     * @param developerName the touch-point configuration developerName
     * @return he touch-point configuration with the provided developer name
     */
    public List<TouchPointConfiguration__mdt> getTouchPointConfigurationByDeveloperName(String developerName) {

        List<TouchPointConfiguration__mdt> touchPointConfigurations =
        [SELECT Id, MasterLabel, DeveloperName, Source__c, Type__c, SendingChannel__c, TokenLifetime__c, URLPath__c
        FROM TouchPointConfiguration__mdt
        WHERE DeveloperName = :developerName];

        return touchPointConfigurations;
    }

    /**
     * Returns the touch-point configuration with the provided source, type and sending channel
     * @param source the touch-point source
     * @param type the touch-point type
     * @param sendingChannel the touch-point sending channel
     * @return he touch-point configuration with the provided source, type and sending channel
     */
    public List<TouchPointConfiguration__mdt> getTouchPointConfiguration(String source, String type, String sendingChannel) {

        List<TouchPointConfiguration__mdt> touchPointConfigurations =
        [SELECT Id, MasterLabel, DeveloperName, Source__c, Type__c, SendingChannel__c, TokenLifetime__c, URLPath__c
        FROM TouchPointConfiguration__mdt
        WHERE Source__c = :source AND Type__c = :type AND SendingChannel__c = :sendingChannel];

        return touchPointConfigurations;
    }
}