/**
 * Created by goudiaby on 01/04/2019.
 */

public with sharing class ValidatableDocumentQueries {
    public static ValidatableDocumentQueries getInstance() {
        return new ValidatableDocumentQueries();
    }

    public List<ValidatableDocument__c> findByDossierId(String dossierId) {
        return [
                SELECT Id,DocumentBundleItem__c,FileMetadata__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossierId
        ];
    }

    // BG commented - unused code
    /**
     * Retrieve a Validatable record by  documentBundleItemId and Dossier
     *
     * @param documentBundleItemId
     * @param dossierId
     *
     * @return ValidatableDocument__c record
     */
    /*public ValidatableDocument__c findByDocumentBundleItemIdIdAndDossierId(String documentBundleItemId, String dossierId) {
        List<ValidatableDocument__c> validatableDocumentList = [
                SELECT Id, DocumentBundleItem__c,Dossier__c,
                        FileMetadata__c
                FROM ValidatableDocument__c
                WHERE Dossier__c = :dossierId AND DocumentBundleItem__c = :documentBundleItemId
                LIMIT 1
        ];
        return validatableDocumentList.isEmpty() ? null : validatableDocumentList.get(0);
    }*/
}