/**
 * Modified by Moussa Fofana on 18/07/2019.
 */
@IsTest
private class SobjectUtilsTst {

    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.VATNumber__c = MRO_UTL_TestDataFactory.CreateFakeVatNumber();
        businessAccount.BusinessType__c = 'NGO';
        Account personAccount = TestDataFactory.account().personAccount().build();
        personAccount.NationalIdentityNumber__pc = MRO_UTL_TestDataFactory.CreateFakeNationalIdentityNumber();
        List<Account> listAccounts = new List<Account>();
        listAccounts.add(businessAccount);
        listAccounts.add(personAccount);
        insert listAccounts;   

    }    

    @IsTest
    static void extractValuesByFieldNameTest() {
        List<Account> accountList = listAccount();
        List<Id> idList = new List<Id>();
        idList.add(accountList[0].Id);
        List<String> response = SobjectUtils.extractValuesByFieldName(accountList,'Name');
        System.debug('========> '+response);
        System.assertEquals(2, response.size());
    }

@IsTest
    static void getSobjectTypeNameTest() {
        List<Account> accountList = listAccount();
        List<Id> idList = new List<Id>();
        idList.add(accountList[0].Id);
        String response = SobjectUtils.getSobjectTypeName(accountList);
        response = SobjectUtils.getSobjectTypeName(idList);
        System.assert(true, response);
    }


    @IsTest
    static void testListNotNullByField() {
        List<Account> accountList = listAccount();
        List<String> names = SobjectUtils.listNotNullByField(accountList, Account.Name);
        System.assertEquals(accountList.size(), names.size());
    }

    @IsTest
    static void testSetNotNullByField() {
        List<Account> accountList = listAccount();
        Set<String> names = SobjectUtils.setNotNullByField(accountList, Account.Name);
        System.assertEquals(accountList.size(), names.size());
    }

    @IsTest
    static void testSetIdByField() {
        List<Account> accountList = listAccount();
        Set<Id> names = SobjectUtils.setIdByField(accountList, Account.Id);
        System.assertEquals(accountList.size(), names.size());
    }

    @IsTest
    static void testMapTwoFields() {
        List<Account> accountList = listAccount();
        Map<String, String> nameToPhone = SobjectUtils.mapTwoFields(accountList, Account.Name, Account.Phone);
        System.assertEquals(accountList.size(), nameToPhone.size());
    }

    @IsTest
    static void testMapByFieldAndSobject() {
        List<Account> accountList = listAccount();
        Map<String, Object> nameToAccount = SobjectUtils.mapByFieldAndSobject(accountList, Account.Name);
        System.assertEquals(accountList.size(), nameToAccount.size());
    }

    @IsTest
    static void testMapByFieldAndList() {
        List<Account> accountList = listAccount();
        Map<String, Object> nameToAccounts = SobjectUtils.mapByFieldAndList(accountList, Account.Name);
        System.assertEquals(accountList.size(), nameToAccounts.size());
    }

    @IsTest
    static void testMapFieldAndSet() {
        List<Account> accountList = listAccount();
        Map<String, Set<String>> nameToPhoneSet = SobjectUtils.mapFieldAndSet(accountList, Account.Name, Account.Phone);
        System.assertEquals(accountList.size(), nameToPhoneSet.size());
    }

    static List<Account> listAccount() {
        /*return new List<Account>{
            new Account(Id = '1', Name = 'Test1', Phone = '1'),
            new Account(Id = '2', Name = 'Test2', Phone = '2'),
            new Account(Id = '3', Name = 'Test3', Phone = '3')
        };*/
        return [
            SELECT Id, Name, Phone
            FROM Account
            LIMIT 999
        ];
    }
}