public with sharing class ContactQueries {

    public static ContactQueries getInstance() {
        return (ContactQueries)ServiceLocator.getInstance(ContactQueries.class);
    }

    public List<Contact> listContactByIndividualId(String individualId) {
        return [
            SELECT Id, Name, IndividualId, AccountId, IsPersonAccount
            FROM Contact
            WHERE IndividualId = :individualId
        ];
    }


    public List<Contact> listContactByIndividualId(String individualId, Boolean includePersonAccounts) {
        String query =
        'SELECT Id, Name, IndividualId, AccountId, IsPersonAccount ' +
        'FROM Contact ' +
        'WHERE IndividualId = :individualId ';
        if (!includePersonAccounts) {
            query += 'AND IsPersonAccount = false';
        }
        return Database.query(query);
    }

    //Mf Not used in Contsct Service
    public Contact findById(String contactId) {
        List<Contact> contactList = [
            SELECT Id, Name, IndividualId
            FROM Contact
            WHERE Id = :contactId
            LIMIT 1
        ];
        if (contactList.isEmpty()) {
            return null;
        }
        return contactList.get(0);
    }
}