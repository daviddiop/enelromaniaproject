/**
 * @author  Stefano Porcari
 * @since   Feb 5, 2020
 * @desc   Query class for Contract object
 *
 */
public with sharing class MRO_QR_Contract {
    public static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public static MRO_QR_Contract getInstance() {
        return (MRO_QR_Contract)ServiceLocator.getInstance(MRO_QR_Contract.class);
    }

    public Contract getById(Id contractId) {
        List<Contract> contractList = [
            SELECT Id,AccountId, BillingAddress, CompanyDivision__c ,CompanySignedId, CompanySignedDate,
                   ConsumptionConventions__c, EndDate, Name,ContractNumber,StartDate,ContractTerm,ContractType__c,
                   CustomerSignedId,CustomerSignedDate,CustomerSignedTitle,Description,
                   Key__c,LastModifiedById,Opportunity__c,OwnerExpirationNotice,
                   Pricebook2Id,Salesman__c,ShippingAddress,SpecialTerms,Status,CompanyDivision__r.Name,ProductStartDate__c,
                   CommodityProduct__c, SalesUnit__c, AdditionalActCounter__c,
                   RelatedContract__c, RelatedContract__r.Status
            FROM Contract
            WHERE Id =:contractId
        ];
        if (contractList.isEmpty()) {
            return null;
        }
        return contractList.get(0);
    }

    public List<Contract> getContractsByAccount(String accountId) {
        return [
                SELECT Id, Name, ContractNumber, Status, CompanyDivision__c, StartDate, EndDate, ContractTerm,
                        CustomerSignedDate, CompanySignedId, Salesman__c, ContractType__c, ConsumptionConventions__c
                FROM Contract
                WHERE AccountId = :accountId
        ];
    }

    public List<Contract> getActiveContractsByAccount(String accountId, String companyDivisionId) {
        return [
            SELECT Id, Name, ContractNumber, Status, CompanyDivision__c, StartDate, EndDate, ContractTerm, CustomerSignedDate, CompanySignedId,
                Salesman__c, ContractType__c, ConsumptionConventions__c, CompanyDivision__r.Name
            FROM Contract
            WHERE AccountId = :accountId AND (Status = 'Activated' OR Status = 'Draft') AND CompanyDivision__c = :companyDivisionId
                AND Id IN (
                    SELECT Contract__c FROM Supply__c WHERE  Market__c='Free'
                )
        ];
    }

    public List<Contract> getActiveContractsByAccount(String accountId) {
        return [
                SELECT Id, Name, ContractNumber, Status, CompanyDivision__c, StartDate, EndDate, ContractTerm,
                        CustomerSignedDate, CompanySignedId, Salesman__c, ContractType__c, ConsumptionConventions__c
                FROM Contract
                WHERE AccountId = :accountId AND Status = 'Activated'
        ];
    }

    public List<Contract> getActiveContractsByAccount(String accountId, String companyDivisionId, String commodity, String market, Boolean includeDraft) {
        return this.getActiveContractsByAccount(accountId, companyDivisionId, null, commodity, market, null, includeDraft, new Set<String>());
    }

    public List<Contract> getActiveContractsByAccount(String accountId, String companyDivisionId, String contractType, String commodity, String market, String tariffType, Boolean includeDraft, Set<String> excludeContractIds) {
        return getActiveContractsByAccount(accountId, companyDivisionId, contractType, commodity, market, tariffType, includeDraft, excludeContractIds, '', '');
    }

    public List<Contract> getActiveContractsByAccount(String accountId, String companyDivisionId, String contractType, String commodity, String market, String tariffType, Boolean includeDraft, Set<String> excludeContractIds, String commodityToExclude, String marketToExclude) {
        Set<String> statusFilter = new Set<String>{'Activated'};
        if (includeDraft) {
            statusFilter.add('Draft');
            statusFilter.add('Signed');
        }
        String query = 'SELECT Id, Name, ContractNumber, Status, CompanyDivision__c, StartDate, EndDate, ContractTerm,'+
                       'SalesUnit__c, CustomerSignedDate, CompanySignedId, Salesman__c, ContractType__c, ConsumptionConventions__c , CompanyDivision__r.Name';

        if (!String.isBlank(commodity) || !String.isBlank(market) || !String.isBlank(tariffType)) {
            query += ', (SELECT Id, Market__c FROM Supplies__r WHERE ';
            List<String> conditions = new List<String>();
            if (!String.isBlank(marketToExclude)) {
                conditions.add('Market__c != :marketToExclude');
            }
            if (!String.isBlank(market)) {
                conditions.add('Market__c = :market');
            }
            if (!String.isBlank(commodity)) {
                conditions.add('RecordType.DeveloperName = :commodity');
            }
            if (!String.isBlank(commodityToExclude)) {
                conditions.add('RecordType.DeveloperName != :commodityToExclude');
            }
            if (!String.isBlank(tariffType)) {
                conditions.add('Product__c != null AND Product__r.CommercialProduct__c != null AND Product__r.CommercialProduct__r.TariffType__c = :tariffType');
            }
            query += String.join(conditions, ' AND ')+') ';
        } else {
            query+= ', (SELECT Market__c FROM Supplies__r LIMIT 1)';
        }
        query += 'FROM Contract WHERE (EndDate > TODAY OR EndDate = NULL)  AND AccountId = :accountId AND CompanyDivision__c = :companyDivisionId AND Status IN :statusFilter';
        if (!String.isBlank(contractType)) {
            query += ' AND ContractType__c = :contractType';
        }
        if (excludeContractIds != null && excludeContractIds.size() > 0) {
            List<String> contractIds = new List<String>();
            for (String currentContractAccountId: excludeContractIds){
                contractIds.add('\''+currentContractAccountId+'\'');
            }
            query += ' AND Id NOT IN (' + String.join(contractIds, ',') + ')';
            System.debug('getActiveContractsByAccount query = ' + query);
        }
        System.debug('### contracts query: '+query);
        List<Contract> contracts = Database.query(query);
        System.debug('### contracts before: '+contracts);
        if (!String.isBlank(commodity) || !String.isBlank(market)|| !String.isBlank(tariffType)) {
            List<Contract> result = new List<Contract>();
            for (Contract c : contracts) {
                System.debug('### c.ContractNumber: '+c.ContractNumber);
                System.debug('### c.Supplies__r.size: '+c.Supplies__r.size());
                if (c.Supplies__r != null && !c.Supplies__r.isEmpty()) {
                    result.add(c);
                }
            }
            System.debug('### result: '+result);
            return result;
        }
        System.debug('### contracts after: '+contracts);
        return contracts;
    }

    //AS [ENLCRO-669] Contract Account Selection - Check Interface
	public List<Contract> getActiveContractsByAccountAndContractTypeAndMarket(String accountId, String companyDivisionId, String market, String contractType) {
		return this.getActiveContractsByAccount(accountId, companyDivisionId, contractType, null, market, null, true, new Set<String>());
	}
	//AS [ENLCRO-669] Contract Account Selection - Check Interface

    public List<Contract> getActiveContractsByAccountAndTariffType(String accountId, String companyDivisionId, String tariffType) {
        return [
            SELECT Id, Name, ContractNumber, Status, CompanyDivision__c, StartDate, EndDate, ContractTerm, CustomerSignedDate, CompanySignedId,
                Salesman__c, ContractType__c, ConsumptionConventions__c, CompanyDivision__r.Name
            FROM Contract
            WHERE AccountId = :accountId AND (Status = 'Activated' OR Status = 'Draft') AND CompanyDivision__c = :companyDivisionId
            AND Id IN (
                SELECT Contract__c
                FROM Supply__c
                WHERE Market__c = 'Free'
                    AND Product__c != null
                    AND Product__r.CommercialProduct__c != null
                    AND Product__r.CommercialProduct__r.TariffType__c = :tariffType
            )
        ];
    }

	//AS [ENLCRO-669] Contract Account Selection - Check Interface
	public List<Contract> getActiveContractsByAccountAndContractTypeAndMarket1(String accountId, String companyDivisionId, String market, String contractType) {
		String query = 'SELECT Id, ContractNumber, ContractType__c, Status, CompanyDivision__c, StartDate, EndDate, CustomerSignedDate, ConsumptionConventions__c FROM Contract';
		List<Contract> contracts;
		List<String> conditions = new List<String>();
		conditions.add('Status = \'Activated\'');

		if (String.isNotBlank(accountId)) {
			conditions.add('AccountId = \'' + accountId + '\'');
		}
		if (String.isNotBlank(companyDivisionId)) {
			conditions.add('CompanyDivision__c = \'' + companyDivisionId + '\'');
		}
		if (String.isNotBlank(contractType)) {
			conditions.add('ContractType__c = \'' + contractType + '\'');
		}
		System.debug('###conditions '+conditions);
		if (!conditions.isEmpty()) {
			query +=' WHERE ' + String.join(conditions, ' AND ');
		}
		if (String.isNotBlank(market)) {
			query +=' AND Id IN (SELECT Contract__c FROM Supply__c WHERE  Market__c = \'' + market + '\')';
		}

		System.debug('###Query: '+ query);
		contracts = Database.query(query);
		return contracts;
	}
    //AS [ENLCRO-669] Contract Account Selection - Check Interface

    public List<Contract> getActiveContractsByAccountAndIds(String accountId, List<String> contractIdList) {
        return [
                SELECT Id, ContractNumber, Name, CompanyDivision__c, CompanyDivision__r.Name,
                        ContractType__c, StartDate, EndDate, ContractTerm, CustomerSignedDate,
                        CompanySignedId, ConsumptionConventions__c, Salesman__c
                FROM Contract
                WHERE AccountId = :accountId AND Status = 'Activated' AND Id IN :contractIdList
        ];
    }

    public Map<Id, Contract> getByIds(Set<Id> contractIds) {
        return new Map<Id, Contract>([
                SELECT Id, Name, ContractNumber, Status, CompanyDivision__c, StartDate, EndDate, ContractTerm,
                       CustomerSignedDate, CompanySignedId, Salesman__c, ContractType__c, ConsumptionConventions__c,
                       IntegrationKey__c, IsLinkedCombo__c, CommodityProduct__c, CommodityProduct__r.CommercialProduct__c,
                       RelatedContract__c, RelatedContract__r.CommodityProduct__c, RelatedContract__r.CommodityProduct__r.CommercialProduct__c,
                       (SELECT Id, Status__c, RecordTypeId, RecordType.DeveloperName, OpportunityServiceItem__r.Opportunity__c FROM Supplies__r)
                FROM Contract
                WHERE Id IN :contractIds
        ]);
    }

    public Map<Id, Contract> getByIdsWithActiveSupplies(Set<Id> contractIds, Set<String> supplyRecordTypeDevNames) {
        return new Map<Id, Contract>([
                SELECT Id, Name, ContractNumber, Status, AccountId, Account.Name, CompanyDivision__c, StartDate, EndDate, ContractTerm,
                       CustomerSignedDate, CompanySignedId, Salesman__c, ContractType__c, ConsumptionConventions__c,
                       (SELECT Id, Product__c, Market__c, ContractAccount__c, NonDisconnectable__c, NonDisconnectableReason__c,
                               FlatRate__c, SupplyOperation__c, TitleDeedValidity__c, NACEReference__c,
                               ServicePoint__c, ServicePoint__r.Name, ServicePoint__r.Code__c, ServicePoint__r.Distributor__c,
                               ServicePoint__r.Trader__c, ServicePoint__r.AvailablePower__c,
                               ServicePoint__r.FlowRate__c, ServicePoint__r.ATRExpirationDate__c, ServicePoint__r.IsNewConnection__c,
                               ServicePoint__r.ContractualPower__c, ServicePoint__r.EstimatedConsumption__c, ServicePoint__r.PowerPhase__c,
                               ServicePoint__r.ConversionFactor__c, ServicePoint__r.Pressure__c, ServicePoint__r.PressureLevel__c,
                               ServicePoint__r.Voltage__c, ServicePoint__r.VoltageLevel__c, ServicePoint__r.ConsumptionCategory__c,
                               ServicePoint__r.RecordTypeId, ServicePoint__r.RecordType.DeveloperName, ServicePoint__r.PointStreetNumber__c,
                               ServicePoint__r.PointStreetNumberExtn__c, ServicePoint__r.PointStreetName__c, ServicePoint__r.PointCity__c,
                               ServicePoint__r.PointCountry__c, ServicePoint__r.PointPostalCode__c, ServicePoint__r.PointStreetType__c,
                               ServicePoint__r.PointApartment__c, ServicePoint__r.PointBuilding__c, ServicePoint__r.PointBlock__c,
                               ServicePoint__r.PointLocality__c, ServicePoint__r.PointProvince__c, ServicePoint__r.PointFloor__c,
                               ServicePoint__r.PointAddressNormalized__c, ServicePoint__r.PointAddressKey__c, ServicePoint__r.PointStreetId__c,
                               ServicePoint__r.CurrentSupply__r.Market__c, ServicePoint__r.CurrentSupply__r.NonDisconnectable__c,
                               ServicePoint__r.CurrentSupply__r.Account__c, ServicePoint__r.CurrentSupply__r.Contract__c,
                               ServicePoint__r.CurrentSupply__r.ContractAccount__c, ServicePoint__r.CurrentSupply__r.TitleDeedValidity__c,
                               ServicePoint__r.CurrentSupply__r.NACEReference__c, ServicePoint__r.CurrentSupply__r.Product__c,
                               ServicePoint__r.CurrentSupply__r.SupplyOperation__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__c, ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteAddressKey__c,
                               ServiceSite__c, ServiceSite__r.CLC__c, ServiceSite__r.NLC__c, ServiceSite__r.Description__c, ServiceSite__r.SiteStreetNumber__c,
                               ServiceSite__r.SiteStreetNumberExtn__c, ServiceSite__r.SiteStreetName__c, ServiceSite__r.SiteCity__c,
                               ServiceSite__r.SiteCountry__c, ServiceSite__r.SitePostalCode__c, ServiceSite__r.SiteStreetType__c,
                               ServiceSite__r.SiteApartment__c, ServiceSite__r.SiteBuilding__c, ServiceSite__r.SiteBlock__c,
                               ServiceSite__r.SiteLocality__c, ServiceSite__r.SiteProvince__c, ServiceSite__r.SiteFloor__c,
                               ServiceSite__r.SiteAddressNormalized__c, ServiceSite__r.SiteAddressKey__c, ServiceSite__r.SiteStreetId__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteAddressNormalized__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteApartment__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteBlock__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteBuilding__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteCity__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteCountry__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteFloor__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SitePostalCode__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteProvince__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteStreetId__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteStreetName__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteStreetNumber__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteStreetNumberExtn__c,
                               ServicePoint__r.CurrentSupply__r.ServiceSite__r.SiteStreetType__c,
                               ServicePoint__r.CurrentSupply__r.FlatRate__c
                        FROM Supplies__r
                        WHERE Status__c = :CONSTANTS.SUPPLY_STATUS_ACTIVE AND RecordType.DeveloperName IN :supplyRecordTypeDevNames
                              AND ServicePoint__c != NULL AND ServiceSite__c != NULL)
                FROM Contract
                WHERE Id IN :contractIds
        ]);
    }

    public List<Contract> getWithSuppliesByIds(Set<String> contractIds, String status) {
        return [
            SELECT Id, Name, ContractNumber, Status, CompanyDivision__c, StartDate, EndDate, ContractTerm,
                CustomerSignedDate, CompanySignedId, Salesman__c, ContractType__c, ConsumptionConventions__c,
                (
                    SELECT Id, Status__c
                    FROM Supplies__r
                    WHERE Status__c = :status
                )
            FROM Contract
            WHERE Id IN :contractIds
        ];
    }

    public List<Contract> getWithActiveSuppliesByIds(Set<Id> contractIds) {
        return [
            SELECT Id, Name, ContractNumber, Status, AccountId, CompanyDivision__c, StartDate, EndDate, ContractTerm,
                CustomerSignedDate, CompanySignedId, Salesman__c, ContractType__c, ConsumptionConventions__c,
                (
                    SELECT Id, Status__c, Contract__c, RecordType.DeveloperName, InENELArea__c, Market__c,
                           ContractAccount__c, ContractAccount__r.LatePaymentPenalty__c, ContractAccount__r.PaymentTerms__c
                    FROM Supplies__r
                    WHERE Status__c != 'Not Active'
                )
            FROM Contract
            WHERE Id IN :contractIds
        ];
    }
}