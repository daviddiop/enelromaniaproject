public with sharing class ServicePointService {
    static ServicePointQueries servicePointQuery = ServicePointQueries.getInstance();

    @AuraEnabled
    public static Map<String, Object> findCode(String pointCode, String companyDivisionId) {

        Map<String, Object> response = new Map<String, Object>();
        if (String.isBlank(pointCode)) {
            return new Map<String, Object>{
                    'errorMsg' => 'Invalid code point',
                    'error' => true
            };
        }

        List<ServicePoint__c> myServicePoint = servicePointQuery.getListServicePoints(pointCode);

        //Check if the list is empty
        if(!myServicePoint.isEmpty()){
            response.put('servicePointId', myServicePoint[0].Id);
            response.put('servicePoint', myServicePoint[0]);
        }
        response.put('error', false);

        return response;
    }
}