public with sharing class ProductService {

    public static final String VALUE_TYPE = 'Value';
    public static final String PRODUCT_TYPE = 'Product';
    public static final String FAMILY_TYPE = 'Family';

    private static final ProductOptionQueries productOptionQuery = ProductOptionQueries.getInstance();
    private static final ProductQueries productQuery = ProductQueries.getInstance();

    public static ProductService getInstance() {
        return (ProductService)ServiceLocator.getInstance(ProductService.class);
    }

    public Map<String, Object> productsAndFamilies(String productId) {
        List<ProductOptionRelation__c> poRelation = productOptionQuery.listPORelationByProductIds(productId);
        Product2 prd = productQuery.getById(productId);
        Product currentProduct = new Product(prd);

        Set<String> bundleProductIds = new Set<String>();
        Map<String, MinMax> productFamilyToMinMax = new Map<String, MinMax>();
        for (ProductOptionRelation__c por : poRelation) {
            if (currentProduct == null) {
                currentProduct = new Product(por.Product__r);
            }
            ProductOption__c productOptionObj = por.ProductOption__r;
            if (productOptionObj.RecordType.DeveloperName == VALUE_TYPE) {
                currentProduct.productOptions.add(new ProductOption(productOptionObj));
            } else if (productOptionObj.RecordType.DeveloperName == PRODUCT_TYPE) {
                bundleProductIds.add(productOptionObj.BundleProduct__c);
            } else if (productOptionObj.RecordType.DeveloperName == FAMILY_TYPE) {
                productFamilyToMinMax.put(productOptionObj.DefaultValue__c, new MinMax(productOptionObj.Min__c, productOptionObj.Max__c));
            }
        }

        List<ProductFamily> productFamilyList = new List<ProductFamily>();
        if (!productFamilyToMinMax.isEmpty()) {
            List<Product2> productSObjList = productQuery.listProductByFamily(productFamilyToMinMax.keySet());
            Map<String, List<Product>> familyToProductList = new Map<String, List<Product>>();
            for (Product2 product : productSObjList) {
                List<Product> productList = familyToProductList.get(product.Family);
                if (productList == null) {
                    productList = new List<Product>();
                    familyToProductList.put(product.Family, productList);
                }
                productList.add(new Product(product));
            }
            for (String family : familyToProductList.keySet()) {
                MinMax minAndMax = productFamilyToMinMax.get(family);
                productFamilyList.add(new ProductFamily(family, familyToProductList.get(family), minAndMax.min, minAndMax.max));
            }
        }
        List<ProductService.Product> productList = new List<Product>();
        if (currentProduct != null) {
            productList.add(currentProduct);
        }
        List<Product> productWithOptionsList = listProductWithOptions(bundleProductIds);
        if (!productList.isEmpty()) {
            productList.addAll(productWithOptionsList);
        }
        return new Map<String, Object>{
            'productList' => productList,
            'productFamilyList' => productFamilyList
        };
    }

    public List<Product> listProductWithOptions(Set<String> productIds) {
        List<Product> productList = new List<Product>();
        if (productIds.isEmpty()) {
            return productList;
        }
        List<Product2> prdList = productQuery.getByIds(productIds);

        for(Product2 prd : prdList) {
            Product prdTmp = new Product(prd);
            List<ProductOption> poList = new List<ProductOption>();
            for(ProductOptionRelation__c por : prd.Options__r) {
                if (por.ProductOption__r.RecordType.DeveloperName != VALUE_TYPE) {
                    continue;
                }
                poList.add(new ProductOption(por.ProductOption__r));
            }
            prdTmp.productOptions = poList;
            productList.add(prdTmp);
        }

        return productList;
    }

    public with sharing class ProductFamily {
        @AuraEnabled
        public String family {get; set;}
        @AuraEnabled
        public List<Product> productList {get; set;}
        @AuraEnabled
        public Integer min {get; set;}
        @AuraEnabled
        public Integer max {get; set;}
        @AuraEnabled
        public Boolean rendered {get; set;}

        public ProductFamily() {}

        public ProductFamily(String family, List<Product> productList, Integer min, Integer max) {
            this.family = family;
            this.productList = productList;
            this.min = min;
            this.max = max;
            this.rendered = true;
        }
    }

    public with sharing class Product {
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String description {get; set;}
        @AuraEnabled
        public String imageUrl {get; set;}
        @AuraEnabled
        public String family {get; set;}
        @AuraEnabled
        public String recordTypeName {get; set;}
        @AuraEnabled
        public Boolean selected {get; set;}
        @AuraEnabled
        public String oliId {get; set;}
        @AuraEnabled
        public List<ProductOption> productOptions {get; set;}

        public Product() {}
// ST commented due unused
//        public Product(String name, String description, String imageUrl, String family, String recordTypeName) {
//            this.name = name;
//            this.description = description;
//            this.imageUrl = imageUrl;
//            this.family = family;
//            this.recordTypeName = recordTypeName;
//            this.productOptions = new List<ProductOption>();
//        }
        public Product(Product2 sfProduct) {
            this.id = sfProduct.Id;
            this.name = sfProduct.Name;
            this.description = sfProduct.Description;
            this.imageUrl = sfProduct.Image__c;
            this.family = sfProduct.Family;
            this.recordTypeName = sfProduct.RecordType.Name;
            this.selected = false;
            this.productOptions = new List<ProductOption>();
        }
        public Product(OpportunityLineItem oli) {
            this(oli.Product2);
            this.oliId = oli.Id;
        }
    }

    public with sharing class ProductOption {
        @AuraEnabled
        public String productId {get; set;}
        @AuraEnabled
        public String recordTypeName {get; set;}
        @AuraEnabled
        public String accessibility {get; set;}
        @AuraEnabled
        public String bundleProductId {get; set;}
        @AuraEnabled
        public String defaultValue {get; set;}
        @AuraEnabled
        public String description {get; set;}
        @AuraEnabled
        public String helpText {get; set;}
        @AuraEnabled
        public String key {get; set;}
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public Integer max {get; set;}
        @AuraEnabled
        public Integer min {get; set;}
        @AuraEnabled
        public Boolean required {get; set;}
        @AuraEnabled
        public Boolean selected {get; set;}
        @AuraEnabled
        public String dataType {get; set;}
        @AuraEnabled
        public String valueSet {get; set;}

        @AuraEnabled
        public List<String> listValues { 
            get{
               if (dataType == 'List') {
                    if (String.isNotBlank(valueSet)) {
                        Object result = StringUtils.tryToDeserialize(valueSet, List<String>.class);
                        if (result == null) {
                            return new List<String>();
                        }
                        return (List<String>)result;
                    } else {
                        return new List<String>();
                    }
                } 
                return null;
            }
        }

        @AuraEnabled
        public List<SelectOption> mapValues { 
            get {
                if (dataType == 'Key/Value') {
                    if (String.isNotBlank(valueSet)) {
                        Object result = StringUtils.tryToDeserialize(valueSet, Map<String, String>.class);
                        if (result == null) {
                            return new List<SelectOption>();
                        }
                        Map<String, String> keyToValue = (Map<String, String>)result;
                        List<SelectOption> selectOptionList = new List<SelectOption>();
                        for (String key : keyToValue.keySet()) {
                            selectOptionList.add(new SelectOption(key, keyToValue.get(key)));
                        }
                        return selectOptionList;
                    } else {
                        return new List<SelectOption>();
                    }
                }
                return null;
            }
        }

        public ProductOption(ProductOption__c productOptionObj) {
            this.recordTypeName = productOptionObj.RecordType.Name;
            this.accessibility = productOptionObj.Accessibility__c;
            this.bundleProductId = productOptionObj.BundleProduct__c;
            this.defaultValue = productOptionObj.Defaultvalue__c;
            this.description = productOptionObj.Description__c;
            this.helpText = productOptionObj.Helptext__c;
            this.key = productOptionObj.Key__c;
            this.label = productOptionObj.Label__c;
            this.max = Integer.valueOf(productOptionObj.max__c);
            this.min = Integer.valueOf(productOptionObj.min__c);
            this.required = productOptionObj.Required__c;
            this.selected = productOptionObj.Selected__c;
            this.dataType = productOptionObj.Type__c;
            this.valueSet = productOptionObj.ValueSet__c;
        }
// ST commented due unused
//        public ProductOption(String productId, ProductOption__c productOptionObj) {
//            this(productOptionObj);
//            this.productId = productId;
//        }
    }
    class MinMax {
        public Integer min;
        public Integer max;
        public MinMax(Decimal min, Decimal max) {
            this.min = Integer.valueOf(min);
            this.max = Integer.valueOf(max);
        }
    }
}