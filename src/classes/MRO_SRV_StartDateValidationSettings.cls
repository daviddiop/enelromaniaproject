public with sharing class MRO_SRV_StartDateValidationSettings {

    private static MRO_SRV_OriginChannel originChannelService = MRO_SRV_OriginChannel.getInstance();
    private static MRO_QR_StartDateValidationSettings startDateValidationSettingsQuery = MRO_QR_StartDateValidationSettings.getInstance();

    public static MRO_SRV_StartDateValidationSettings getInstance() {
        return (MRO_SRV_StartDateValidationSettings) ServiceLocator.getInstance(MRO_SRV_StartDateValidationSettings.class);
    }

    public StartDates getDefaultAndValidateStartDate(Opportunity myOpportunity, OpportunityServiceItem__c osi) {
        return getDefaultAndValidateStartDate(myOpportunity, osi, null, null);
    }

    public StartDates getDefaultAndValidateStartDate(Opportunity myOpportunity, OpportunityServiceItem__c osi, Boolean isCustomerReading) {
        return getDefaultAndValidateStartDate(myOpportunity, osi, isCustomerReading, null);
    }

    public StartDates getDefaultAndValidateStartDate(Opportunity myOpportunity, OpportunityServiceItem__c osi, Boolean isCustomerReading, Boolean isMarketChange) {
        System.debug('myOpportunity.Channel__c = ' + myOpportunity.Channel__c);
        System.debug('myOpportunity.RequestType__c = ' + myOpportunity.RequestType__c);
        System.debug('myOpportunity.SubProcess__c = ' + myOpportunity.SubProcess__c);
        System.debug('osi.RecordType.DeveloperName = ' + osi.RecordType.DeveloperName);
        System.debug('osi.Distributor__r.IsDisCoENEL__c = ' + osi.Distributor__r.IsDisCoENEL__c);
        System.debug('osi.ApplicantNotHolder__c = ' + osi.ApplicantNotHolder__c);
        System.debug('isCustomerReading = ' + isCustomerReading);
        System.debug('isMarketChange = ' + isMarketChange);

        StartDateValidationSettings__mdt exactSetting = null;
        StartDateValidationSettings__mdt commonSetting = null;

        String origin = originChannelService.getOriginByChannelLabel(myOpportunity.Channel__c);
        List<StartDateValidationSettings__mdt> startDateSettingList = startDateValidationSettingsQuery
            .listByProcessOriginCommodity(myOpportunity.RequestType__c, origin, osi.RecordType.DeveloperName);

        System.debug('startDateSettingList = ' + startDateSettingList);

        for (StartDateValidationSettings__mdt setting : startDateSettingList) {
            List<String> subprocesses = String.isBlank(setting.SubProcess__c) ? new List<String>() : setting.SubProcess__c.split(';');
            Set<String> subProcessValues = new Set<String>();
            for (String subprocess : subprocesses) {
                subProcessValues.add(subprocess.trim());
            }
            Boolean isEnelAreaSame = setting.IsEnelArea__c == String.valueOf(osi.Distributor__r.IsDisCoENEL__c);
            Boolean isSameOwnerSame = setting.IsSameOwner__c == String.valueOf(!osi.ApplicantNotHolder__c);
            Boolean isSubProcessSame = String.isNotBlank(setting.SubProcess__c) && String.isNotBlank(myOpportunity.SubProcess__c)
                    && subProcessValues.contains(myOpportunity.SubProcess__c);
// We use SubProcess fullName now
//                || (String.isNotBlank(myOpportunity.SubProcess__c) && String.isNotBlank(setting.SubProcess__c) && myOpportunity.SubProcess__c.contains(setting.SubProcess__c));
            Boolean isCustomerReadingSame = isCustomerReading == null || setting.IsCustomerReading__c == String.valueOf(isCustomerReading);
            Boolean isMarketChangeSame = isMarketChange == null || setting.IsMarketChange__c == String.valueOf(isMarketChange);

            Boolean isEnelAreaEmpty = String.isBlank(setting.IsEnelArea__c);
            Boolean isSameOwnerEmpty = String.isBlank(setting.IsSameOwner__c);
            Boolean isSubProcessEmpty = String.isBlank(setting.SubProcess__c);
            Boolean isCustomerReadingEmpty = String.isBlank(setting.IsCustomerReading__c);
            Boolean isMarketChangeEmpty = String.isBlank(setting.IsMarketChange__c);

            if (isEnelAreaSame && isSameOwnerSame && isSubProcessSame && isCustomerReadingSame && isMarketChangeSame) {
                exactSetting = setting;
            }
            if ((isEnelAreaSame || isEnelAreaEmpty) && (isSameOwnerSame || isSameOwnerEmpty) && (isSubProcessSame || isSubProcessEmpty)
                    && (isCustomerReadingSame || isCustomerReadingEmpty) && (isMarketChangeSame || isMarketChangeEmpty)) {
                commonSetting = setting;
            }
        }

        exactSetting = exactSetting == null ? commonSetting : exactSetting;

        System.debug('exactSetting = ' + exactSetting);
        System.debug('commonSetting = ' + commonSetting);

        Date defaultStartDate = System.today();
        Date validationStartDate = System.today();
        Boolean isWorkingDay = exactSetting != null ? exactSetting.IsValidationWorkingDay__c : false;

        try {
            if (exactSetting != null && exactSetting.DefaultIncrement__c != null && exactSetting.ValidationIncrement__c != null) {
                System.debug('exactSetting.DefaultIncrement__c = ' + exactSetting.DefaultIncrement__c);
                System.debug('exactSetting.ValidationIncrement__c = ' + exactSetting.ValidationIncrement__c);
                if (exactSetting.IsDefaultWorkingDay__c) {
                    Integer addedValue = MRO_UTL_Date.calculateWorkDays(defaultStartDate, Integer.valueOf(exactSetting.DefaultIncrement__c));
                    defaultStartDate = defaultStartDate.addDays(addedValue);
                    while(MRO_UTL_Date.isNotWorkingDate(defaultStartDate)){
                        defaultStartDate = defaultStartDate.addDays(1);
                    }
                } else {
                    defaultStartDate = defaultStartDate.addDays(Integer.valueOf(exactSetting.DefaultIncrement__c));
                }
                if (exactSetting.IsValidationWorkingDay__c) {
                    Integer addedValue = MRO_UTL_Date.calculateWorkDays(validationStartDate, Integer.valueOf(exactSetting.ValidationIncrement__c));
                    validationStartDate = validationStartDate.addDays(addedValue);
                    while(MRO_UTL_Date.isNotWorkingDate(validationStartDate)){
                        validationStartDate = validationStartDate.addDays(1);
                    }
                } else {
                    validationStartDate = validationStartDate.addDays(Integer.valueOf(exactSetting.ValidationIncrement__c));
                }
            }
        } catch (Exception exp) {
            System.debug('getDefaultAndValidateStartDate = ' + exp.getTypeName());
            System.debug(exp.getMessage());
            System.debug(exp.getStackTraceString());
        }

        System.debug('defaultStartDate = ' + defaultStartDate);
        System.debug('validationStartDate = ' + validationStartDate);

        return new StartDates(defaultStartDate, validationStartDate, isWorkingDay);
    }

    public class StartDates {
        @AuraEnabled
        public Date defaultStartDate {get; set;}
        @AuraEnabled
        public Date validationStartDate {get; set;}
        @AuraEnabled
        public Boolean isWorkingDay {get; set;}

        public StartDates(Date defaultStartDate, Date validationStartDate, Boolean isWorkingDay) {
            this.defaultStartDate = defaultStartDate;
            this.validationStartDate = validationStartDate;
            this.isWorkingDay = isWorkingDay;
        }
    }
}