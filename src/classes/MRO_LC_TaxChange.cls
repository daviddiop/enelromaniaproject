/**
 * Created by David Diop on 07.11.2019.
 * @description Update Cases with [ENLCRO-192] Tax Change
 */

public with sharing class MRO_LC_TaxChange extends ApexServiceLibraryCnt {
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_SRV_Case caseSrv = MRO_SRV_Case.getInstance();
    private static MRO_QR_Case caseQuerySrv = MRO_QR_Case.getInstance();
    private static MRO_QR_TaxSubsidy taxSubsidyQuery = MRO_QR_TaxSubsidy.getInstance();
    private static MRO_SRV_TaxSubsidy taxSubsidySrv = MRO_SRV_TaxSubsidy.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static CaseQueries caseQuery = CaseQueries.getInstance();
    private static AccountQueries accountQuery = AccountQueries.getInstance();
    private static MRO_QR_User userQuery = MRO_QR_User.getInstance();
    private static MRO_UTL_Constants constantSrv = new MRO_UTL_Constants();
    public static String caseDraftStatus = constantSrv.CASE_STATUS_DRAFT ;
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId();
    static String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Tax_Change').getRecordTypeId();
    static String heatingSubventionRecordType = Schema.SObjectType.TaxSubsidy__c.getRecordTypeInfosByDeveloperName().get('Heating_subvention').getRecordTypeId();

    private static MRO_QR_Dossier dossierQuery = MRO_QR_Dossier.getInstance();
    /**
    * @author  David Diop
    * @description initializeTaxChange wizard .
    * @history 18/11/2019: David Diop - Original
    * @param dossierId
    * @param accountId
    * @param caseId
    * @param interactionId
    * @return dossierId,
    */
    public with sharing class initializeTaxChange extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String templateId = params.get('templateId');
            String genericRequestId = params.get('genericRequestId');
            String companyDivisionId = '';
            Group creditManagementGroup = userQuery.getQueueByDeveloperName('CreditManagementQueue');
            String creditManagementId = creditManagementGroup != null ? creditManagementGroup.Id : '';
            User currentUserInfos = userQuery.getUserProfileId(UserInfo.getUserId());
            Boolean hasPermission = false;
            MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
            try {
                if (String.isNotBlank(creditManagementId)) {
                    GroupMember groupMember = userQuery.getGroupMemberByGroupIdAndUserId(creditManagementId, currentUserInfos.Id);
                    hasPermission = groupMember != null ? true : false;
                }
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, companyDivisionId, dossierRecordType, 'Tax Change');
                Dossier__c dossierParent = dossierQuery.getById(dossier.Id);
                if(String.isBlank(dossierParent.Parent__c) && String.isNotBlank(genericRequestId)){
                    dossierSrv.updateParent(dossier.Id, genericRequestId);
                    dossier.Parent__c = genericRequestId;
                }
                 /*
                if (dossier.OwnerId == creditManagementId && !hasPermission) {
                    response.put('redirectToDossier', true);
                    throw new WrtsException(System.Label.AccessDenied);
                }
                */
                List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);
                if (cases.size() != 0) {
                    System.debug('cases[0].OwnerId: '+cases[0].OwnerId);
                    System.debug('creditManagementId: '+creditManagementId);
                    System.debug('hasPermission: '+hasPermission);
                    response.put('cases', cases[0]);
                    if (cases[0].OwnerId == creditManagementId && !hasPermission) {
                        response.put('redirectToDossier', true);
                        response.put('dossierId', dossier.Id);
                        throw new WrtsException(System.Label.TaxChangeAccessDenied);
                    }
                }

                if (String.isNotBlank(dossier.Id)) {
                    List<TaxSubsidy__c> taxSubsidies = taxSubsidyQuery.getTaxSubsidyByDossier(dossier.Id);
                    List<TaxSubsidy__c> taxSubsidiesUpdate = new List<TaxSubsidy__c>();
                    List<TaxSubsidy__c> taxSubsidiesUpdateToFilter = taxSubsidyQuery.getTaxSubsidyByAccountId(accountId);
                    for (TaxSubsidy__c taxSubsidiesRecord : taxSubsidiesUpdateToFilter) {
                        if   (taxSubsidiesRecord.StartDate__c < taxSubsidiesRecord.EndDate__c){
                             taxSubsidiesUpdate.add(taxSubsidiesRecord);
                        }
                    }
                    List<TaxSubsidy__c> taxSubsidiesToUpdate;
                    Set<Id> parentIds = new Set<Id>();
                    taxSubsidiesToUpdate = taxSubsidyQuery.getTaxSubsidyByParentId(parentIds,accountId,dossier.Id);

                    /*if(taxSubsidiesUpdate != null && !taxSubsidiesUpdate.isEmpty()) {
                        for (TaxSubsidy__c taxSubsidyRecord : taxSubsidiesUpdate) {
                            parentIds.add(taxSubsidyRecord.Case__c);
                        }

                    }*/
                    response.put('subsidyTiles', taxSubsidies);
                    response.put('subsidyTilesUpdate', taxSubsidiesUpdate);
                    response.put('subsidyTilesToUpdate', taxSubsidiesToUpdate);
                    response.put('heatingSubventionRecordType', heatingSubventionRecordType);
            }


                String code = 'TaxChangeTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }
                Account accountRecord = accountQuery.findAccount(accountId);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('genericRequestId', dossier.Parent__c);
                response.put('accountId', accountId);
                response.put('account', accountRecord);
                response.put('creditManagementId', creditManagementId);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    /**
    * @author  David Diop
    * @description UpdateCaseList.
    * @history 19/11/2019 David diop - Original
    * @param dossierId
    * @param companyDivisionId
    * @param caseId
    * @param supplyId
    * @return
    */
    public with sharing class updateCaseListAndTax extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String dossierId = params.get('dossierId');
            String supplyId = params.get('supplyId');
            String companyDivisionId = params.get('companyDivisionId');
            String subventionType = params.get('subventionType');
            String requestType = params.get('requestType');
            String origin = params.get('origin');
            String channel = params.get('channel');
            List<TaxSubsidy__c> taxSubsidiesList = (List<TaxSubsidy__c>) JSON.deserialize( params.get('taxSubsidiesList'),List<TaxSubsidy__c>.class);
            List<TaxSubsidy__c> taxSubsidiesListToUpdate = (List<TaxSubsidy__c>) JSON.deserialize( params.get('taxSubsidiesListToUpdate'),List<TaxSubsidy__c>.class);
            List<Case> caseToUpdate;
            List<Case> caseToTerminating;
            List<TaxSubsidy__c> subventionToActive;
            if(taxSubsidiesList != null && !taxSubsidiesList.isEmpty()) {
                Set<Id> caseIds = new Set<Id>();
                Set<Id> subventionIds = new Set<Id>();
                Set<Id> caseToUpdateIds = new Set<Id>();
                for (TaxSubsidy__c taxSubsidyRecord : taxSubsidiesList) {
                    caseIds.add(taxSubsidyRecord.Case__c);
                    subventionIds.add(taxSubsidyRecord.Id);
                    caseToUpdateIds.add(taxSubsidyRecord.Case__r.TechnicalDetails__c);
                }
                subventionToActive = taxSubsidyQuery.getTaxSubsidyByIds(subventionIds);
                caseToUpdate = caseQuerySrv.listByIds(caseIds);
                caseToTerminating = caseQuerySrv.listByIds(caseToUpdateIds);
            }
            caseSrv.setNewCaseAndDossier(caseToUpdate, dossierId,supplyId, companyDivisionId,subventionType,origin,channel);
            if (requestType == System.Label.UpdateRequest) {
                if (taxSubsidiesListToUpdate != null && !taxSubsidiesListToUpdate.isEmpty()) {
                    List<Case> caseList = new List<Case>();
                    List<TaxSubsidy__c> taxSubsidyList = new List<TaxSubsidy__c>();
                    for (TaxSubsidy__c subventionRecord : taxSubsidiesListToUpdate) {
                        for(TaxSubsidy__c taxSubsidies: taxSubsidiesList){
                            if(subventionRecord.Case__c == taxSubsidies.Case__r.TechnicalDetails__c){
                                //caseList.add(new Case(Id = subventionRecord.Case__c, Status = caseDraftStatus));
                                if (subventionRecord.Status__c != 'Not Active') {
                                    taxSubsidyList.add(new TaxSubsidy__c(Id = subventionRecord.Id, Status__c = 'Terminating'));
                                }
                            }
                        }
                    }
                    if (!taxSubsidyList.isEmpty()) {
                        databaseSrv.updateSObject(taxSubsidyList);
                    }
                    /*if (!caseList.isEmpty()) {
                        databaseSrv.updateSObject(caseList);
                    }*/
                }
            }
            response.put('error', false);
            return response;
        }
    }

    public with sharing class updateCaseAndDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            Group  creditManagement = userQuery.getQueueByDeveloperNameAndType('Queue','CreditManagementQueue');
            String creditManagementId = creditManagement.Id;
            String dossierId = params.get('dossierId');
            String supplyId = params.get('supplyId');
            String companyDivisionId = params.get('companyDivisionId');
            String subventionType = params.get('subventionType');
            String requestType = params.get('requestType');
            String origin = params.get('origin');
            String channel = params.get('channel');
            String notes = params.get('notes');
            String distributorNotes = params.get('distributorNotes');
            Boolean isRetroActiveDate = (Boolean)JSON.deserialize( params.get('isRetroActiveDate'),Boolean.class);
            List<TaxSubsidy__c> taxSubsidiesList = (List<TaxSubsidy__c>) JSON.deserialize( params.get('taxSubsidiesList'),List<TaxSubsidy__c>.class);
            List<Case> caseToUpdate;
            if(taxSubsidiesList != null && !taxSubsidiesList.isEmpty()) {
                Set<Id> caseIds = new Set<Id>();
                for (TaxSubsidy__c taxSubsidyRecord : taxSubsidiesList) {
                    caseIds.add(taxSubsidyRecord.Case__c);
                }
                caseToUpdate = caseQuerySrv.listByIds(caseIds);
            }
            caseSrv.setNewCaseAndDossierTaxChange(caseToUpdate, dossierId,supplyId, companyDivisionId,subventionType,origin,channel,isRetroActiveDate,creditManagementId,notes,distributorNotes);


            response.put('error', false);
            return response;
        }
    }

  /*  public with sharing class ChangeStatusAndExecuteTransition extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');

            if (dossierId != null) {
                dossierSrv.updateDossierStatus(dossierId);
                response.put('error', false);
            } else {
                response.put('error', true);
            }

            return response;
        }
    }  */

    /**
    * @author  David Diop
    * @description CancelProcess.
    * @date 19/11/2019
    * @param dossierId
    * @param caseId
    * @return
    */
    public with sharing class CancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String dossierId = params.get('dossierId');
            String caseId = params.get('caseRecord');
            List<TaxSubsidy__c> taxSubsidiesList = (List<TaxSubsidy__c>) JSON.deserialize( params.get('taxSubsidiesList'),List<TaxSubsidy__c>.class);
            List<Case> caseToCancel;
            if(taxSubsidiesList != null && !taxSubsidiesList.isEmpty()) {
                Set<Id> caseIds = new Set<Id>();
                for (TaxSubsidy__c taxSubsidyRecord : taxSubsidiesList) {
                    caseIds.add(taxSubsidyRecord.Case__c);
                }
                caseToCancel = caseQuerySrv.listByIds(caseIds);
            }
            System.debug('#### '+caseToCancel);
            System.debug('#### dossierId '+dossierId);
            if(caseToCancel != null){
                caseSrv.setCanceledOnCaseAndDossier(caseToCancel, dossierId,'RC010');
            }
            response.put('error', false);
            return response;
        }
    }

    public with sharing class updateDossier extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String origin = params.get('origin');
            String channel = params.get('channel');
            Dossier__c dossier = new Dossier__c (Id = dossierId,Channel__c = channel, Origin__c = origin);
            databaseSrv.updateSObject(dossier);
            response.put('error', false);
            return response;
        }
    }

    public Inherited sharing class updateTaxSubsidyStatut extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String taxSubsidyId = params.get('taxSubsidyId');
            TaxSubsidy__c taxSubsidy = new TaxSubsidy__c (Id = taxSubsidyId, Status__c = 'Terminating');
            taxSubsidySrv.updateTaxSubsidy(taxSubsidy);
            response.put('error', false);
            return response;
        }
    }

    /**
     *
     */
    public Inherited sharing class updateOldTaxSubsidyStatut extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String parentCaseId = params.get('parentCaseId');
            TaxSubsidy__c oldTaxSubsidy = taxSubsidyQuery.getByCaseId(parentCaseId);
            TaxSubsidy__c taxSubsidy = new TaxSubsidy__c (Id = oldTaxSubsidy.Id, Status__c = 'Active');
            taxSubsidySrv.updateTaxSubsidy(taxSubsidy);
            response.put('error', false);
            return response;
        }
    }


    /**
    * @author  David Diop
    * @description getSupplyRecord .
    * @date 19/11/2019
    * @param supplyId
    * @return
    */

  /*  public class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            Supply__c supplyRecord = supplyQuery.getById(supplyId);
            response.put('supply', supplyRecord);
            response.put('error', false);
            return response;
        }
    }*/

    public with sharing class getSupplyRecord extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            supplyInputIds createCaseParams = (supplyInputIds) JSON.deserialize(jsonInput, supplyInputIds.class);
            List<Supply__c> supplyListRecord = supplyQuery.getSuppliesByIds(createCaseParams.supplyIds);
            Set<Id> serviceSiteIds = new Set<Id>();
            Boolean existeCasePerSupplies = false;
            try {
                if (!supplyListRecord.isEmpty()) {
                    for (Supply__c supply : supplyListRecord) {
                        if (supply.ServiceSite__c != null) {
                            serviceSiteIds.add(supply.ServiceSite__c);
                        }
                    }
                    if (!serviceSiteIds.isEmpty()) {
                        Map<Id, Supply__c> oldSuppliesRelatedToServiceSite = supplyQuery.getSupplyByServiceSiteIds(serviceSiteIds);
                        if (!oldSuppliesRelatedToServiceSite.isEmpty()) {
                            List <Case> oldCaseRelatedToSupplies = caseQuerySrv.listTaxChangeCasesBySupplyIds(oldSuppliesRelatedToServiceSite.values(), caseRecordType);
                            if (!oldCaseRelatedToSupplies.isEmpty()) {
                                existeCasePerSupplies = true;
                            }
                        }
                    }
                }
                response.put('supplies', supplyListRecord);
                response.put('existeCasePerSupplies', existeCasePerSupplies);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class supplyInputIds {
        @AuraEnabled
        public List<Id> supplyIds { get; set; }
    }
}