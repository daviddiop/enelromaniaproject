/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 23.04.20.
 */

public with sharing class MRO_LC_CompensationForDamagesWizard extends ApexServiceLibraryCnt {

    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static MRO_QR_Case caseQuery = MRO_QR_Case.getInstance();
    private static MRO_QR_Supply supplyQuery = MRO_QR_Supply.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();
    private static MRO_SRV_Dossier dossierSrv = MRO_SRV_Dossier.getInstance();
    private static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('CommercialRequest').getRecordTypeId();

    public static final String caseRecordType = 'CompensationForDamages';

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String dossierId = params.get('dossierId');
            String accountId = params.get('accountId');
            String parentCaseId = params.get('parentCaseId');
            String interactionId = params.get('interactionId');
            Map<String, Object> response = new Map<String, Object>();

            try {
                if (String.isBlank(accountId)) {
                    response.put('error', true);
                    response.put('errorMsg', System.Label.Account + ' - ' + System.Label.MissingId);
                    return response;
                }

                Account accountRecord = accountQuery.findAccount(accountId);
                if(accountRecord.RecordType.DeveloperName != 'Person'){
                    response.put('isNotPerson', true);
                    return response;
                }

                Dossier__c dossier = dossierSrv.generateDossier(accountId, dossierId, interactionId, null, dossierRecordType, caseRecordType, null, null);
                Case parentCase = caseQuery.getById(parentCaseId);
                dossier.Origin__c = parentCase.Origin;
                dossier.Channel__c = parentCase.Channel__c;
                dossier.Parent__c = parentCase.Dossier__c;
                databaseSrv.upsertSObject(dossier);

                RequestParams createCaseParams = new RequestParams();
                createCaseParams.dossierId = dossier.Id;
                createCaseParams.originSelected = parentCase.Origin;
                createCaseParams.channelSelected = parentCase.Channel__c;

                CaseUpdate caseUpdate = new CaseUpdate(createCaseParams, parentCaseId);
                Case caseRecord = caseUpdate.initialize();
                response.put('caseRecord', caseRecord);
                response.put('billingProfileId', caseRecord.BillingProfile__c);
                if(caseUpdate.parentCase != null){
                    response.put('parentSupplyId', caseUpdate.parentCase.Supply__c);
                    response.put('parentCase', caseUpdate.parentCase);
                }

                if(parentCase.DistributorResponseDate__c != null){
                    response.put('distributorResponseDate', parentCase.DistributorResponseDate__c.addDays(5));
                }

                response.put('isClosed', dossier.Status__c != constantsSrv.DOSSIER_STATUS_DRAFT);
                response.put('dossierId', dossier.Id);
                response.put('dossier', dossier);
                response.put('accountId', accountId);
                response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class RequestParams {
        @AuraEnabled
        public Id supplyId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String billingProfileId { get; set; }
        @AuraEnabled
        public String originSelected { get; set; }
        @AuraEnabled
        public String channelSelected { get; set; }
    }

    private with sharing class CaseUpdate{
        RequestParams createCaseParams;
        String parentCaseId;
        Case parentCase;

        public CaseUpdate(RequestParams createCaseParams){
            this.createCaseParams = createCaseParams;
        }

        public CaseUpdate(RequestParams createCaseParams, String parentCaseId){
            this.createCaseParams = createCaseParams;
            this.parentCaseId = parentCaseId;
            this.parentCase = caseQuery.getById(parentCaseId);
        }

        public Case initialize(){
            List<Case> cases = caseQuery.getCasesByDossierId(this.createCaseParams.dossierId);
            Case caseRecord;
            if (cases.size() > 0) {
                caseRecord = cases[0];
            }else{
                Map<String, String> recordTypes = MRO_UTL_Constants.getCaseRecordTypes(caseRecordType);
                caseRecord = new Case();
                caseRecord.Status = constantsSrv.CASE_STATUS_DRAFT;
                caseRecord.Phase__c = constantsSrv.CASE_START_PHASE;
                caseRecord.RecordTypeId = (String) recordTypes.get(caseRecordType);
                caseRecord.Dossier__c = this.createCaseParams.dossierId;
                caseRecord.ParentId = this.parentCaseId;
                caseRecord.ExternalRequestID__c = this.parentCase.ExternalRequestID__c;
                caseRecord.EvidenceDocumentsReceivedDate__c = Date.today();
                caseRecord = setOriginChannel(caseRecord);

                if(this.parentCase != null){
                    if(this.parentCase.Supply__c != null) {
                        this.createCaseParams.supplyId = this.parentCase.Supply__c;
                        caseRecord = setSupply(caseRecord);
                    }
                    caseRecord.DistributorResponseDate__c = this.parentCase.DistributorResponseDate__c;
                }

                databaseSrv.upsertSObject(caseRecord);
            }
            return caseRecord;
        }

        public Case upsertCase(){
            Case caseRecord = new Case(Id = this.createCaseParams.caseId);
            caseRecord = setSupply(caseRecord);
            caseRecord = setOriginChannel(caseRecord);
            databaseSrv.upsertSObject(caseRecord);
            Case updatedCase = caseQuery.getById(caseRecord.Id);

            return updatedCase;
        }

        private Case setOriginChannel(Case caseRecord){
            caseRecord.Origin = createCaseParams.originSelected;
            caseRecord.Channel__c = createCaseParams.channelSelected;
            caseRecord.BillingProfile__c = createCaseParams.billingProfileId;
            return caseRecord;
        }

        private Case setSupply(Case caseRecord){
            if(this.createCaseParams.supplyId != null){
                Supply__c supply = supplyQuery.getSupplyWithServicePointById(this.createCaseParams.supplyId);
                Account acc = accountQuery.findAccount(supply.Account__c);
                caseRecord.Supply__c = supply.Id;
                caseRecord.CompanyDivision__c = supply.CompanyDivision__c;
                caseRecord.AccountId = supply.Account__c;
                caseRecord.AccountName__c = acc.Name;
                caseRecord.ServiceSite__c = supply.ServiceSite__c;
                if(supply.ServicePoint__c != null) {
                    caseRecord.Distributor__c = supply.ServicePoint__r.Distributor__c;
                }
            }
            return caseRecord;
        }
    }

    public with sharing class upsertCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams createCaseParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);
            if (createCaseParams.caseId == null) {
                response.put('error', true);
                response.put('errorMsg', 'Supply or Case not found');
                return response;
            }

            CaseUpdate caseUpdate = new CaseUpdate(createCaseParams);
            Case caseRecord = caseUpdate.upsertCase();

            response.put('caseRecord', caseRecord);
            return response;
        }
    }

    public class HasJustifyingDocumentsParams {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public Boolean hasJustifyingDocuments { get; set; }
    }

    public with sharing class updateHasJustifyingDocuments extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            HasJustifyingDocumentsParams params = (HasJustifyingDocumentsParams) JSON.deserialize(jsonInput, HasJustifyingDocumentsParams.class);

            Dossier__c dossier = new Dossier__c(Id = params.dossierId, HasJustifyingDocuments__c = params.hasJustifyingDocuments);
            databaseSrv.upsertSObject(dossier);
            return response;
        }
    }

    public with sharing class refundMethodCheck extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            Boolean isInvoice = false;
            RequestParams params = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);

            isInvoice = billingProfileQuery.getById(params.billingProfileId).RefundMethod__c == constantsSrv.BILLING_PROFILE_REFUND_METHOD_INVOICE ? true : false;

            response.put('isInvoice', isInvoice);
            return response;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            RequestParams channelOriginParams = (RequestParams) JSON.deserialize(jsonInput, RequestParams.class);

            dossierSrv.updateDossierChannel(channelOriginParams.dossierId, channelOriginParams.channelSelected, channelOriginParams.originSelected);

            return response;
        }
    }

    public class SaveProcessParams {
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String caseId { get; set; }
        @AuraEnabled
        public String caseSupply { get; set; }
        @AuraEnabled
        public Boolean isDraft { get; set; }
    }

    public with sharing class saveProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            SaveProcessParams saveProcessParams = (SaveProcessParams) JSON.deserialize(jsonInput, SaveProcessParams.class);
            Map<String, Object> response = new Map<String, Object>();
            Date startDate = Date.today();
            Supply__c supply;
            String supplyType;

            if (String.isNotEmpty(saveProcessParams.caseSupply)) {
                supply = supplyQuery.getById(saveProcessParams.caseSupply);
                supplyType = setSupplyType(supply.RecordType.DeveloperName);
            }

            try {
                if (!saveProcessParams.isDraft) {
                    Case caseRecord = new Case(
                            Id = saveProcessParams.caseId,
                            Status = constantsSrv.CASE_STATUS_NEW,
                            EffectiveDate__c = startDate,
                            StartDate__c = startDate,
                            SLAExpirationDate__c = MRO_UTL_Date.addWorkingDays(startDate, 15),
                            CompensationType__c ='Distributie',
                            CompensationCode__c = 'D001 - Compensation for burnt equipment',
                            SupplyType__c = supplyType,
                            CaseTypeCode__c = 'C63'
                    );
                    Dossier__c dossier = new Dossier__c(Id = saveProcessParams.dossierId, Status__c = constantsSrv.DOSSIER_STATUS_NEW);
                    databaseSrv.upsertSObject(caseRecord);
                    databaseSrv.upsertSObject(dossier);
                }
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    private static String setSupplyType(String supplyRecordType) {
        switch on (supplyRecordType) {
            when 'Electric' {
                return 'Electricity';
            }
            when 'Gas' {
                return 'Gas';
            }
            when 'Service' {
                return 'VAS';
            }
            when else {
                return '';
            }
        }
    }
}