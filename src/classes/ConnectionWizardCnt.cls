/**
 * Created by ferhati on 01/10/2019.
 */

public with sharing class ConnectionWizardCnt extends ApexServiceLibraryCnt {
    private static CaseQueries caseQuery = CaseQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static DossierService dossierServ = DossierService.getInstance();
    private static CaseService caseServ = CaseService.getInstance();
    static String dossierRecordType = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Connection').getRecordTypeId();

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String accountId = params.get('accountId');
            String dossierId = params.get('dossierId');
            String interactionId = params.get('interactionId');
            String billingProfileId = '';

            try {
                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account acc = accQuery.findAccount(accountId);
                Dossier__c dossier = dossierServ.generateDossier(accountId, dossierId, interactionId, null, dossierRecordType,'Connection');

                if (String.isNotBlank(dossierId)) {
                    List<Case> cases = caseQuery.getCasesByDossierId(dossier.Id);

                    if (!cases.isEmpty()) {
                        billingProfileId = cases[0].BillingProfile__c;
                    }
                    response.put('caseTile', cases);
                    response.put('billingProfileId', billingProfileId);
                }



                    response.put('dossierId', dossier.Id);
                    response.put('dossier', dossier);
                    response.put('account', acc);
                    response.put('accountId', accountId);

                    response.put('error', false);

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public class saveDraftBP extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String billingProfileId = params.get('billingProfileId');
            Savepoint sp = Database.setSavepoint();

            try {
                caseServ.updateCaseByBillingProfile(oldCaseList, billingProfileId);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class updateCaseList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);


            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);

            //List<Supply__c> listSupplies = (List<Supply__c>) JSON.deserialize(params.get('searchedSupplyFieldsList'), List<Supply__c>.class);
            //String companyDivisionId;
            String dossierId = params.get('dossierId');
            String billingProfileId = params.get('billingProfileId');
            Savepoint sp = Database.setSavepoint();

            try {

                caseServ.changeStatusOnCaseAndDossier(oldCaseList, dossierId, billingProfileId);
                response.put('error', false);

            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class cancelProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            List<Case> oldCaseList = (List<Case>) JSON.deserialize(params.get('oldCaseList'), List<Case>.class);
            String dossierId = params.get('dossierId');
            Savepoint sp = Database.setSavepoint();

            try {
                caseServ.setCanceledOnCaseAndDossier(oldCaseList, dossierId);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }
}