/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 12.05.20.
 */

@IsTest
public with sharing class MRO_LC_MassiveHelperTst {
    static final String fileTemplateName = 'Template1';
    static final Integer rowsCountForInsert = 10;

    @TestSetup
    private static void setup() {
        final String delimiterString = ',';
        final String rowDelimiterString = '\n';

        final String dossierCancellationDetailsHeader = 'Dossier Cancellation Details';
        final String dossierEmailHeader = 'Dossier Email';

        //add file template
        FileTemplate__c ft1 = new FileTemplate__c(
                AllOrNone__c = false,
                BatchSize__c = 200,
                FileDelimiter__c = delimiterString,
                FileExtension__c = '.csv',
                FileTemplateName__c = fileTemplateName,
                FileType__c = 'CSV',
                FinishAction__c = '',
                MaxSizeMB__c = 8,
                MaxSizeRows__c = 200

        );

        insert ft1;

        //add file template columns
        List<FileTemplateColumn__c> tcList = new List<FileTemplateColumn__c>();

        FileTemplateColumn__c tc1 = new FileTemplateColumn__c(
                FileTemplate__c = ft1.Id,
                HeaderName__c = dossierCancellationDetailsHeader,
                Index__c = 1,
                Mandatory__c = true
        );
        tcList.add(tc1);

        FileTemplateColumn__c tc2 = new FileTemplateColumn__c(
                FileTemplate__c = ft1.Id,
                HeaderName__c = dossierEmailHeader,
                Index__c = 2,
                Mandatory__c = true,
                ValidationRegex__c = '^[a-zA-Z0-9._}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'

        );
        tcList.add(tc2);
        insert tcList;

        //create file
        List<String> rows = new List<String>{
                dossierCancellationDetailsHeader + delimiterString + dossierEmailHeader
        };
        for (Integer i = 0; i < rowsCountForInsert; i++) {
            rows.add('Test ' + i + delimiterString + 'test' + i + '@example.com');
        }
        String fileString = String.join(rows, rowDelimiterString) + rowDelimiterString;

        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        Blob myBlob = Blob.valueOf(fileString);
        cv.VersionData = myBlob;
        cv.Title = 'testFile';
        cv.PathOnClient = 'testFile.csv';
        insert cv;

        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id].ContentDocumentId;
        cdl.LinkedEntityId = ft1.Id;
        cdl.ShareType = 'V';
        insert cdl;

        /*

         String emailRegex = '^[a-zA-Z0-9._}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
         String passwordRegex = '^.*(?=.{8,})(?=.*\\d)(?=.*[a-zA-Z])|(?=.{8,})(?=.*\\d)(?=.*[!@#$%^&])|(?=.{8,})(?=.*[a-zA-Z])(?=.*[!@#$%^&]).*$';
         String dateRegex = '^\\d{4}-\\d{2}-\\d{2}$';
         String ssnRegex = '[0-9]{3}[-]?[0-9]{2}[-]?[0-9]{4}';
         String phoneRegex='^([0-9\\(\\)\\/\\+ \\-]*)$';
         String zipRegex='^\\d{5}(?:[-\\s]?\\d{4})?$';
         String numberRegex = '^[0-9]\\d*(\\.\\d+)?$';
         String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
         */
    }

    @IsTest
    static void createFileImportJobTest() {
        Map<String, Object> inputJson = getParams();
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_MassiveHelper', 'createFileImportJob', inputJson, true);
        Test.stopTest();

        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error'));

        String templateId = (String) inputJson.get('templateId');
        List<FileImportJob__c> jobs = [SELECT Id, Status__c FROM FileImportJob__c WHERE FileTemplate__c = :templateId];
        System.assertEquals(1, jobs.size());
        System.assertEquals('Validated', jobs[0].Status__c);

        List<FileImportDataRow__c> rows = [SELECT Id, Status__c FROM FileImportDataRow__c WHERE FileImportJob__c = :jobs[0].Id];
        System.assertEquals(rowsCountForInsert, rows.size());
    }

    static Map<String, Object> getParams(){
        List<FileTemplate__c> templates = [SELECT Id FROM FileTemplate__c WHERE FileTemplateName__c = :fileTemplateName];
        System.assertEquals(1, templates.size());

        List<ContentDocument> documents = [SELECT Id FROM ContentDocument];
        System.assertEquals(1, documents.size());

        String documentId = documents[0].Id;
        String templateId = templates[0].Id;
        Map<String, Object> inputJSON = new Map<String, Object>{
                'documentId' => documentId,
                'templateId' => templateId
        };
        return inputJSON;
    }
}