/**
 * Created by vincenzo.scolavino on 22/04/2020.
 */

public with sharing class MRO_SRV_BillingProfile {
    private static MRO_SRV_DatabaseService databaseService = MRO_SRV_DatabaseService.getInstance();

    public static MRO_SRV_BillingProfile getInstance() {
        return (MRO_SRV_BillingProfile)ServiceLocator.getInstance(MRO_SRV_BillingProfile.class);
    }

    public List<BillingProfile__c> updateBillingProfiles(List<BillingProfile__c> billingProfilesList) {
        List<BillingProfile__c> output = billingProfilesList;
        if(billingProfilesList!=null && !billingProfilesList.isEmpty()) {
            try {
                databaseService.updateSObject(output);
            } catch (Exception e) {
                throw e;
            }
        }
        return output;
    }
    public  BillingProfile__c updateBillingProfileAddress( BillingProfile__c billingProfile, Case caseRecord){
        billingProfile.BillingStreetType__c = caseRecord.AddressStreetType__c;
        billingProfile.BillingStreetNumber__c = caseRecord.AddressStreetNumber__c;
        billingProfile.BillingCity__c = caseRecord.AddressCity__c;
        billingProfile.BillingPostalCode__c = caseRecord.AddressPostalCode__c;
        billingProfile.BillingCountry__c = caseRecord.AddressCountry__c;
        billingProfile.BillingAddressNormalized__c = caseRecord.AddressAddressNormalized__c;
        billingProfile.BillingStreetName__c = caseRecord.AddressStreetName__c;
        billingProfile.BillingApartment__c = caseRecord.AddressApartment__c;
        billingProfile.BillingFloor__c = caseRecord.AddressFloor__c;
        billingProfile.BillingLocality__c = caseRecord.AddressLocality__c;
        billingProfile.BillingProvince__c = caseRecord.AddressProvince__c;
        billingProfile.BillingBuilding__c = caseRecord.AddressBuilding__c;
        billingProfile.BillingBlock__c = caseRecord.AddressBlock__c;
        billingProfile.BillingStreetId__c = caseRecord.AddressStreetId__c;
        billingProfile.BillingStreetNumberExtn__c = caseRecord.AddressStreetNumberExtn__c;
        return billingProfile;
    }
}