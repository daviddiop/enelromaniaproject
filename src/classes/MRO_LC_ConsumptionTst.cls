/**
 * @author  Luca Ravicini
 * @since   Jun 2, 2020
 * @desc   Test for MRO_LC_Consumption class
 *
 */

@IsTest
private class MRO_LC_ConsumptionTst {
    private static MRO_UTL_Constants constantsUtl = new MRO_UTL_Constants();


    @testSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 1000.0, SequenceLength__c = 7.0);
        insert sequencer;

        Account account = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert account;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePointGas().build();
        servicePoint.Account__c = account.Id;
        insert servicePoint;

        Opportunity opportunity = MRO_UTL_TestDataFactory.opportunity().createOpportunity().build();
        opportunity.StageName = 'Prospecting';
        opportunity.AccountId = account.Id;
        insert opportunity;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = account.Id;
        billingProfile.IBAN__c = null;
        insert billingProfile;

        OpportunityServiceItem__c oppServiceItem = MRO_UTL_TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        oppServiceItem.BillingProfile__c = billingProfile.Id;
        oppServiceItem.Account__c = account.Id;
        oppServiceItem.Opportunity__c = opportunity.Id;
        oppServiceItem.ServicePoint__c = servicePoint.Id;
        insert oppServiceItem;
    }

    @IsTest
    static void updateEstimatedConsumption() {
        OpportunityServiceItem__c opportunityServiceItem = [
                SELECT Id
                FROM OpportunityServiceItem__c
        ];

        Decimal estimatedConsumption = 300.0;
        Map<String, String> inputJSON = new Map<String, String>{
                'osiId' => opportunityServiceItem.Id,
                'estimatedConsumption' => JSON.serializePretty(estimatedConsumption)
        };

        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_Consumption', 'updateEstimatedConsumption', inputJSON, true);
            Test.stopTest();

            System.assertEquals(false, response.get('error'));
            opportunityServiceItem = [
                    SELECT Id, EstimatedConsumption__c
                    FROM OpportunityServiceItem__c
            ];
            System.assertEquals(estimatedConsumption, opportunityServiceItem.EstimatedConsumption__c);

        } catch (Exception exc) {

        }
    }

    @IsTest
    static void updateEstimatedConsumptionError() {
        Map<String, String> inputJSON = new Map<String, String>{
                'osiId' => '000'
        };
        try {
            Test.startTest();
            Map<String, Object> response = (Map<String, Object>)TestUtils.exec(
                    'MRO_LC_Consumption', 'updateEstimatedConsumption', inputJSON, true);
            Test.stopTest();
            System.assertEquals(true, response.get('error'));
            System.assert(response.get('errorMsg') != null);
            System.assert(response.get('errorTrace') != null);
        } catch (Exception exc) {

        }
    }
}