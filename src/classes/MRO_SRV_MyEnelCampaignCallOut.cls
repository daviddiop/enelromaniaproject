/**
 * Created by David diop on 21.10.2020.
 */

public with sharing class MRO_SRV_MyEnelCampaignCallOut implements MRO_SRV_SendMRRcallout.IMRRCallout {
    public wrts_prcgvr.MRR_1_0.MultiRequest buildMultiRequest(Map<String, Object> argsMap){
        System.debug('MRO_SRV_MyEnelCampaignCallOut.buildMultiRequest');

        //Sobject
        SObject obj = (SObject) argsMap.get('sender');

        //Init MultiRequest
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = MRO_UTL_MRRMapper.initMultiRequest();

        //Init Request
        Map<String, String> parameters = (Map<String, String>) argsMap.get('parameters');
        wrts_prcgvr.MRR_1_0.Request request = MRO_UTL_MRRMapper.initRequest(parameters);

        //Query
        Campaign myCampaign = [
                SELECT Id,Name,Description,StartDate,EndDate,ContractType__c,Status
                FROM Campaign
                WHERE Id = :obj.Id
                LIMIT 1
        ];

        //to WObject
        Wrts_prcgvr.MRR_1_0.WObject mrrObj = MRO_UTL_MRRMapper.sObjectToWObject((SObject) myCampaign,null);

        //Add mrrObj to the request
        request.objects.add(mrrObj);

        //Add request to Multirequest
        multiRequest.requests.add(request);
        return multiRequest;
    }

    public wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse buildResponse(wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse){
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response = new wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse();
        if(calloutResponse != null && !calloutResponse.responses.isEmpty() && calloutResponse.responses[0] != null) {
            if(calloutResponse.responses[0].code == 'OK'){
                response.success = true;
            }else{
                response.success = false;
                //            errorType = 'Functional error';
            }
            response.message = calloutResponse.responses[0].description;
        }
        return response;
    }
}