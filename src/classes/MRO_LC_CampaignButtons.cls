/**
 * Created by tommasobolis on 04/06/2020.
 */

public with sharing class MRO_LC_CampaignButtons extends ApexServiceLibraryCnt {

    public inherited sharing class loadTarget extends AuraCallable {

        public override Object perform(final String jsonInput) {
            System.debug('jsonInput: '+jsonInput);
            CampaignButtonsParam params = (CampaignButtonsParam) JSON.deserialize(jsonInput, CampaignButtonsParam.class);
            Map<String, Object> response = new Map<String, Object>();
            try {
                HttpRequest req = new HttpRequest();
                req.setEndpoint('callout:symphony');
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('orgId', UserInfo.getOrganizationId().left(15));
                req.setHeader('orgToken', IntegrationSettings__c.getInstance().SymphonyToken__c);
                String reqBody = JSON.serialize(params);
                System.debug('reqBody: '+reqBody);
                req.setBody(reqBody);
                Http http = new Http();
                HttpResponse res = http.send(req);
                System.debug('result: '+res.getBody());
                response.put('data', res);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public class CampaignButtonsParam {

        @AuraEnabled
        public String recordId { get; set; }
        @AuraEnabled
        public String companyDivisionId { get; set; }
        @AuraEnabled
        public String contractType { get; set; }
        @AuraEnabled
        public String status { get; set; }
        @AuraEnabled
        public String limitDate { get; set; }

        public CampaignButtonsParam(String recordId, String companyDivisionId, String contractType, String status, String limitDate) {
            this.recordId = recordId;
            this.companyDivisionId = companyDivisionId;
            this.contractType = contractType;
            this.status = status;
            this.limitDate = limitDate;
        }
    }
}