/**
 * @author  Luca Ravicini
 * @since   Apr 10, 2020
 * @desc   Controller class for Consumption
 *
 */

public with sharing class MRO_LC_Consumption extends ApexServiceLibraryCnt {
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_UTL_Constants constantsSrv = new MRO_UTL_Constants();


    public class updateEstimatedConsumption extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String osiId = params.get('osiId');
            try {
                Decimal estimatedConsumption =(Decimal)JSON.deserialize(params.get('estimatedConsumption'),Decimal.class);
                estimatedConsumption = estimatedConsumption.setScale(2);
                OpportunityServiceItem__c osi = new OpportunityServiceItem__c(Id=osiId, EstimatedConsumption__c=estimatedConsumption);
                databaseSrv.updateSObject(osi);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

}