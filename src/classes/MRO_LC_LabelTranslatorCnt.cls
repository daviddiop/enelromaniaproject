/**
 * Created by tommasobolis on 16/06/2020.
 */

public with sharing class MRO_LC_LabelTranslatorCnt {

    public String label_lang {get;set;}
    public List<String> labels {get;set;}
    public String vfpLabelSeparator {get { return MRO_UTL_LabelTranslator.VFP_LABEL_SEPARATOR; }}

    public  MRO_LC_LabelTranslatorCnt() {

        Map<String, String> reqParams = ApexPages.currentPage().getParameters();
        label_lang = reqParams.get('label_lang');
        labels = reqParams.get('labels').split(MRO_UTL_LabelTranslator.URL_PARAM_LABEL_SEPARATOR);
    }
}