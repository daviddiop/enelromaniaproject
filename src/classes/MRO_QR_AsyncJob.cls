/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   set 07, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_QR_AsyncJob {
    public static MRO_QR_AsyncJob getInstance() {
        return (MRO_QR_AsyncJob) ServiceLocator.getInstance(MRO_QR_AsyncJob.class);
    }

    public wrts_prcgvr__AsyncJob__c getPendingCalloutJobByRecordId(Id parentRecordId) {
        List<wrts_prcgvr__AsyncJob__c> pendingJobs = [SELECT Id, wrts_prcgvr__Status__c, wrts_prcgvr__NumberOfRetries__c,
                                                             wrts_prcgvr__CompletionDate__c, wrts_prcgvr__JobType__c, wrts_prcgvr__ObjectType__c,
                                                             wrts_prcgvr__ApexClassName__c, wrts_prcgvr__MethodName__c
                                                      FROM wrts_prcgvr__AsyncJob__c
                                                      WHERE wrts_prcgvr__JobItem__c = :parentRecordId AND wrts_prcgvr__Status__c = 'Queued'
                                                            AND wrts_prcgvr__JobType__c = 'Callout'
                                                      LIMIT 1];
        if (!pendingJobs.isEmpty()) {
            return pendingJobs[0];
        }
        return null;
    }
}