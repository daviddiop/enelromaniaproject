/**
 * Created by tommasobolis on 18/05/2020.
 */

global with sharing class MRO_UTL_ApexCriteriaHandler implements wrts_prcgvr.Interfaces_1_2.IApexCriteria{

    public static Boolean evaluate(Object args) {

        Map<String, Object> argsMap = (Map<String, Object>)args;
        Boolean hasPermission = false;
        String customPermissionName = (String)argsMap.get('method');
        hasPermission = FeatureManagement.checkPermission(customPermissionName);
        return hasPermission;
    }
}