/**
 * Created by Vlad Mocanu on 19/03/2020.
 */

@IsTest
public with sharing class MRO_LC_BailmentManagementTst {

    @TestSetup
    private static void setup() {
        Sequencer__c sequencer = MRO_UTL_TestDataFactory.sequencer().createCustomerCodeSequencer().build();
        insert sequencer;

        Account personAccount = MRO_UTL_TestDataFactory.account().personAccount().build();
        personAccount.IntegrationKey__c = '000435';
        insert personAccount;

        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = personAccount.Id;
        contract.IntegrationKey__c = '000116';
        insert contract;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().build();
        supply.Contract__c = contract.Id;
        supply.Account__c = personAccount.Id;
        insert supply;

        List<Dossier__c> dossiers = new List<Dossier__c>();

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().build();
        dossier.Status__c = 'Draft';
        Dossier__c dossierGen = MRO_UTL_TestDataFactory.Dossier().build();
        dossierGen.Status__c = 'New';
        dossiers.add(dossier);
        dossiers.add(dossierGen);
        insert dossiers;

        Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().build();
        caseRecord.Status = 'Draft';
        caseRecord.Dossier__c = dossiers[0].Id;
        insert caseRecord;
    }

    @IsTest
    public static void initializeNoDossierTest() {
        Account account = [SELECT Id, Name FROM Account LIMIT 1];
        Case caseRecorde = [SELECT Id FROM Case LIMIT 1];
        List<Dossier__c> dossiers = [SELECT Id, Name FROM Dossier__c LIMIT 2];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => '',
                'parentCaseId' => caseRecorde.Id,
                'genericRequestDossierId' => dossiers[1].Id

        };
        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, result.get('accountId') == account.Id);
        System.assertEquals(true, result.get('dossierId') != null);

        Test.stopTest();
    }

    @IsTest
    public static void initializeWithDossierTest() {
        Account account = [SELECT Id, Name FROM Account LIMIT 1];
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, result.get('accountId') == account.Id);
        System.assertEquals(true, result.get('dossierId') == dossier.Id);
        System.assertEquals(true, result.get('caseTile') != null);

        Test.stopTest();
    }

    @IsTest
    public static void initializeWithNoAccount() {
        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == true);
        System.assertEquals(true, result.get('errorMsg') == 'Account - Missing Id');

        Test.stopTest();
    }

    @IsTest
    public static void getContractIdBySupplyIdTest() {
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Account account = [SELECT Id, Name, IntegrationKey__c FROM Account LIMIT 1];
        Contract contract = [SELECT Id, ContractNumber, IntegrationKey__c FROM Contract LIMIT 1];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'supplyId' => supply.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'getContractIdBySupplyId', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, result.get('contractId') == contract.Id);
        System.assertEquals(true, result.get('contractNumber') == contract.ContractNumber);
        System.assertEquals(true, result.get('contractIntegrationKey') == contract.IntegrationKey__c);
        System.assertEquals(true, result.get('accountIntegrationKey') == account.IntegrationKey__c);

        Test.stopTest();
    }

    @IsTest
    public static void getContractIdBySupplyIdNoSupplyIdTest() {
        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'supplyId' => ''
        };
        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'getContractIdBySupplyId', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == true);
        System.assertEquals(true, result.get('errorMsg') == 'Supply - Missing Id');

        Test.stopTest();
    }

    @IsTest
    public static void cancelDossierAndCaseBothInputsTest() {
        Dossier__c oldDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];
        Case oldCase = [SELECT Id, Status FROM Case LIMIT 1];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => oldDossier.Id,
                'cancelReason' => 'cancelReason',
                'detailsReason' => 'detailsReason'
        };

        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c newDossier = [SELECT Id, Status__c FROM Dossier__c WHERE Id = :oldDossier.Id];
        Case newCase = [SELECT Id, Status FROM Case WHERE Id = :oldCase.Id];

        //System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, oldDossier.Status__c == 'Draft');
        System.assertEquals(true, oldCase.Status == 'Draft');
        System.assertEquals(true, newDossier.Status__c == 'Canceled');
//        System.assertEquals(true, newCase.Status == 'Canceled');

        Test.stopTest();
    }

    @IsTest
    public static void cancelDossierAndCaseNoCaseTest() {
        Dossier__c oldDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];
        Case oldCase = [SELECT Id, Status FROM Case LIMIT 1];

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => oldCase.Id
        };

        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c newDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];

        System.assert(true);

        Test.stopTest();
    }

    /*@IsTest
    public static void cancelDossierAndCaseNoDossierTest() {
        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => ''
        };

        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'cancelDossierAndCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c newDossier = [SELECT Id, Status__c FROM Dossier__c LIMIT 1];

        System.assertEquals(true, result.get('error') == true);
        System.assertEquals(true, result.get('errorMsg') == 'Invalid id: ');
        System.assertEquals(true, newDossier.Status__c == 'Draft');

        Test.stopTest();
    }*/

    @IsTest
    public static void createCaseTest() {
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Account account = [SELECT Id, Name, IntegrationKey__c FROM Account LIMIT 1];
        Contract contract = [SELECT Id, ContractNumber, IntegrationKey__c FROM Contract LIMIT 1];
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';
        Map<String, String> bailmentManagementRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('BailmentManagement');

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'contractId' => contract.Id,
                'accountId' => account.Id,
                'supplyId' => supply.Id,
                'selectedOrigin' => selectedOrigin,
                'selectedChannel' => selectedChannel
        };
        MRO_LC_BailmentManagement.CreateCaseInput createCaseParams = new MRO_LC_BailmentManagement.CreateCaseInput();
        createCaseParams.dossierId = dossier.Id;
        createCaseParams.contractId = contract.Id;
        createCaseParams.accountId = account.Id;
        createCaseParams.supplyId = supply.Id;
        createCaseParams.selectedOrigin = selectedOrigin;
        createCaseParams.selectedChannel = selectedChannel;

        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'createCase', createCaseParams, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Case resultCase = ((List<Case>) result.get('caseList'))[0];

        System.assertEquals(true, result.get('error') == false);
        System.assertEquals(true, resultCase.RecordTypeId == bailmentManagementRecordTypes.get('BailmentManagement'));
        System.assertEquals(true, resultCase.Dossier__c == dossier.Id);
        System.assertEquals(true, resultCase.Contract__c == contract.Id);
        System.assertEquals(true, resultCase.Origin == selectedOrigin);
        System.assertEquals(true, resultCase.Channel__c == selectedChannel);
        System.assertEquals(true, resultCase.Supply__c == supply.Id);
        System.assertEquals(true, resultCase.AccountId == account.Id);
        System.assertEquals(true, resultCase.Status == 'Draft');
        System.assertEquals(true, resultCase.Phase__c == 'RE010');

        Test.stopTest();
    }

    @IsTest
    public static void createCaseMissingInputsTest() {
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Account account = [SELECT Id, Name, IntegrationKey__c FROM Account LIMIT 1];
        Contract contract = [SELECT Id, ContractNumber, IntegrationKey__c FROM Contract LIMIT 1];
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';

        Test.startTest();

        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'contractId' => contract.Id,
                'accountId' => account.Id,
                'supplyId' => supply.Id,
                'selectedOrigin' => selectedOrigin,
                'selectedChannel' => selectedChannel
        };
        Set<String> inputKeySet = inputJSON.keySet();
        MRO_LC_BailmentManagement.CreateCaseInput createCaseParams = new MRO_LC_BailmentManagement.CreateCaseInput();
        createCaseParams.dossierId = dossier.Id;
        createCaseParams.contractId = contract.Id;
        createCaseParams.accountId = account.Id;
        createCaseParams.supplyId = supply.Id;
        createCaseParams.selectedOrigin = selectedOrigin;
        createCaseParams.selectedChannel = selectedChannel;

        for (String inputKey : inputKeySet) {
            Map<String, String> newMap = inputJSON;
            String inputKeyValue = newMap.get(inputKey);
            newMap.put(inputKey, '');

            Object response = TestUtils.exec(
                    'MRO_LC_BailmentManagement', 'createCase', createCaseParams, true);
            Map<String, Object> result = (Map<String, Object>) response;
            newMap.put(inputKey, inputKeyValue);

            System.assert(true);
        }
        Test.stopTest();
    }


    @IsTest
    public static void saveDraftCaseTest() {
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Account account = [SELECT Id, Name, IntegrationKey__c FROM Account LIMIT 1];
        Contract contract = [SELECT Id, ContractNumber, IntegrationKey__c FROM Contract LIMIT 1];
        Case caseRecorde = [SELECT Id FROM Case LIMIT 1];
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';

        Test.startTest();

        MRO_LC_BailmentManagement.SaveCaseInput saveDraftCaseParams = new MRO_LC_BailmentManagement.SaveCaseInput();
        saveDraftCaseParams.dossierId = dossier.Id;
        saveDraftCaseParams.caseId = caseRecorde.Id;
        saveDraftCaseParams.listOfWarranties = 'listOfWarranties';
        saveDraftCaseParams.withdrawReason = '' ;
        saveDraftCaseParams.selectedOrigin = selectedOrigin;
        saveDraftCaseParams.selectedChannel = selectedChannel;


            Object response = TestUtils.exec(
                    'MRO_LC_BailmentManagement', 'saveDraftCase', saveDraftCaseParams, true);
            Map<String, Object> result = (Map<String, Object>) response;

            System.assertEquals(true, result.get('error') == false);

        Test.stopTest();
    }

    @IsTest
    public static void saveDraftCaseExceptionTest() {
        Dossier__c dossier = [SELECT Id, Name FROM Dossier__c LIMIT 1];
        Supply__c supply = [SELECT Id, Name FROM Supply__c LIMIT 1];
        Account account = [SELECT Id, Name, IntegrationKey__c FROM Account LIMIT 1];
        Contract contract = [SELECT Id, ContractNumber, IntegrationKey__c FROM Contract LIMIT 1];
        Case caseRecorde = [SELECT Id FROM Case LIMIT 1];
        String selectedOrigin = 'Email';
        String selectedChannel = 'Back Office Sales';

        Test.startTest();

        MRO_LC_BailmentManagement.SaveCaseInput saveDraftCaseParams = new MRO_LC_BailmentManagement.SaveCaseInput();
        saveDraftCaseParams.dossierId = dossier.Id+'DF4';
        saveDraftCaseParams.caseId = caseRecorde.Id;
        saveDraftCaseParams.listOfWarranties = 'listOfWarranties';
        saveDraftCaseParams.withdrawReason = '' ;
        saveDraftCaseParams.selectedOrigin = selectedOrigin;
        saveDraftCaseParams.selectedChannel = selectedChannel;


            Object response = TestUtils.exec(
                    'MRO_LC_BailmentManagement', 'saveDraftCase', saveDraftCaseParams, true);
            Map<String, Object> result = (Map<String, Object>) response;

            System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }

    @IsTest
    public static void saveCaseTest() {
        String dossierId = [SELECT Id FROM Dossier__c LIMIT 1].Id;
        String caseId = [SELECT Id FROM Case LIMIT 1].Id;
        String listOfWarranties = '[100001, 100002, 100005]';
        String withdrawReason = 'Contract End';

        Test.startTest();

        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'caseId' => caseId,
                'listOfWarranties' => listOfWarranties,
                'withdrawReason' => withdrawReason
        };

        Object response = TestUtils.exec(
                'MRO_LC_BailmentManagement', 'saveCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        Dossier__c resultDossier = (Dossier__c) result.get('dossier');
        Case savedCase = [
                SELECT Id, Dossier__c, Status, Phase__c, SelectedWarranties__c, Description
                FROM Case
                WHERE Id = :caseId
        ];

       /* System.assertEquals(true, result.get('error') == true);
        System.assertEquals(true, resultDossier.Id == dossierId);
        System.assertEquals(true, resultDossier.Status__c == 'New');
        System.assertEquals(true, savedCase.Id == caseId);
        System.assertEquals(true, savedCase.Status == 'New');
        System.assertEquals(true, savedCase.Dossier__c == dossierId);
        System.assertEquals(true, savedCase.Phase__c == 'RE010');
        System.assertEquals(true, savedCase.SelectedWarranties__c == listOfWarranties);
        System.assertEquals(true, savedCase.Description == withdrawReason);*/

        Test.stopTest();
    }

    @IsTest
    public static void cancelEverythingTest() {
        String dossierId = [SELECT Id FROM Dossier__c LIMIT 1].Id;
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'cancelReason' => '',
                'detailsReason' => 'detailsReason'
        };
        String inputJSONString = JSON.serialize(inputJSON);

        Map<String, Object> result = new MRO_LC_BailmentManagement().cancelEverything(inputJSONString);

        System.assertEquals(true, result.get('error') == false);

        Test.stopTest();
    }

    /*@IsTest
    public static void cancelEverythingExceptionTest() {
        String dossierId = [SELECT Id FROM Dossier__c LIMIT 1].Id;
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossierId+'E74',
                'cancelReason' => '',
                'detailsReason' => 'detailsReason'
        };
        String inputJSONString = JSON.serialize(inputJSON);

        Map<String, Object> result = new MRO_LC_BailmentManagement().cancelEverything(inputJSONString);

        System.assertEquals(true, result.get('error') == true);

        Test.stopTest();
    }*/

    @IsTest
    public static void saveCaseMissingInputsTest() {
        String dossierId = [SELECT Id FROM Dossier__c LIMIT 1].Id;
        String caseId = [SELECT Id FROM Case LIMIT 1].Id;
        String listOfWarranties = '[100001, 100002, 100005]';
        String withdrawReason = 'Financial Difficulties';

        Test.startTest();

        Map<String, String> inputJSON = new Map<String, String>{
                'dossierId' => dossierId,
                'caseId' => caseId,
                'listOfWarranties' => listOfWarranties,
                'withdrawReason' => withdrawReason
        };
        Set<String> inputKeySet = inputJSON.keySet();

        for (String inputKey : inputKeySet) {
            Map<String, String> newMap = inputJSON;
            String inputKeyValue = newMap.get(inputKey);
            newMap.put(inputKey, '');

            Object response = TestUtils.exec(
                    'MRO_LC_BailmentManagement', 'saveCase', newMap, true);
            Map<String, Object> result = (Map<String, Object>) response;
            newMap.put(inputKey, inputKeyValue);

            System.assertEquals(true, result.get('error') == true);
        }
        Test.stopTest();
    }

}