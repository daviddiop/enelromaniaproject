/**
 * Created by BADJI on 30/06/2020.
 */
/**
 * MRO_LC_SelfReadingWizardTst
 *
 * @author  INSA BADJI
 * @version 1.0
 * @description Update Cases with
 *              [ENLCRO-116] Self Reading
 * 30/06/2020: INSA - Original
 */

@IsTest
public with sharing class MRO_LC_SelfReadingWizardTst {

    @TestSetup
    static void setup(){
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('BL010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SelfReading_ELE').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'SelfReading_ELE', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'SelfReading_ELE', recordTypeEle, '');
        insert phaseDI010ToRC010;

        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().build();
        contractAccount.Account__c = listAccount[0].Id;
        contractAccount.SelfReadingPeriodEnd__c = '19';
        insert contractAccount;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
        supply.Contract__c=contract.Id;
        supply.ServicePoint__c = servicePoint.Id;
        insert supply;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supply.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.ContractAccount__c = contractAccount.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SelfReading_ELE').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c= 'SelfReadingWizardCodeTemplate_1';
        insert scriptTemplate;

        Index__c index = MRO_UTL_TestDataFactory.index().createIndex().build();
        index.Case__c = caseList[0].Id;
        index.MeterNumber__c= '200';
        index.ReadingDate__c = Date.newInstance(2030, 02, 17);
        insert  index;
    }

    @IsTest
    public static void InitializeMeterReadingTst(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        ScriptTemplate__c scriptTemplate = [
            SELECT Id
            FROM ScriptTemplate__c
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            'templateId' => scriptTemplate.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @IsTest
    static void InitializeExceptionTest() {
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => 'dossier.Id'
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true );

        Test.stopTest();
    }

    @IsTest
    static void checkSelectedSuppliesTest(){
        List<Supply__c> listSupplies= [
            SELECT Id FROM Supply__c LIMIT 999
        ];
        List<Id> listIds = new List<Id>();
        for (Supply__c s : listSupplies){
            listIds.add(s.Id);
        }
        String listSupplyIds = JSON.serialize(listIds);
        Map<String, String > inputJSON = new Map<String, String>{
            'selectedSupplyIds' => listSupplyIds
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'checkSelectedSupplies', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        //Exception
        List<Id> listIdsError = new List<Id>();
        //listIdsError.add('testerror');
        String listSupplyIdsError = JSON.serialize(listIdsError);
        Map<String, String > inputJSONError = new Map<String, String>{
            'selectedSupplyIds' => listSupplyIdsError
        };
         TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'checkSelectedSupplies', inputJSONError, false);
        Test.stopTest();

    }

    @IsTest
    static void getSelfReadingIntervalTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'getSelfReadingInterval', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @IsTest
    static void getSavedMetersTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'getSavedMeters', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Map<String, String > inputJSONError = new Map<String, String>{
            'dossierId' => ''
        };
        Object responseError = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'getSavedMeters', inputJSONError, true);
        Map<String, Object> resultError = (Map<String, Object>) responseError;
        system.assertEquals(true, resultError.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    static void getMetersTest(){
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            '' => ''
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'getMeters', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    static void createIndexTest(){
        List<Supply__c> supply = [
            SELECT Id,Name,Account__c,Product__c, Product__r.Name, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c,Contract__c,
                Contract__r.StartDate,Contract__r.EndDate, Contract__r.ContractTerm, ServicePoint__c
            FROM Supply__c
            LIMIT 10
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        MRO_LC_SelfReadingWizard.MeterQuadrant selfReadingWizard = new MRO_LC_SelfReadingWizard.MeterQuadrant();
        List<MRO_LC_SelfReadingWizard.MeterQuadrant> meterQuadrantsList = new List<MRO_LC_SelfReadingWizard.MeterQuadrant>();
       selfReadingWizard.quadrantName = 'Energie Reactiva Inductva';
        selfReadingWizard.meterNumber = '200';
        selfReadingWizard.index = 200;
        meterQuadrantsList.add(selfReadingWizard);

        String meterInfo = JSON.serialize(meterQuadrantsList);
        String selectedSupply =JSON.serialize(supply);
        String caseLisString = JSON.serialize(caseList);

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'meterInfo' => meterInfo,
            'selectedSupply' => selectedSupply,
            'caseList' => caseLisString
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'createIndex', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();


    }

    @IsTest
    static  void cancelProcessTest(){
        MRO_LC_SelfReadingWizard.CancelProcessInput selfreCancelProcessInput = new MRO_LC_SelfReadingWizard.CancelProcessInput();
         List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        Dossier__c doss = [
            SELECT Id,Status__c
            FROM Dossier__c
            LIMIT 1
        ];
        selfreCancelProcessInput.oldCaseList = caseList;
        selfreCancelProcessInput.dossierId = doss.Id;

        Test.startTest();

        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'cancelProcess', selfreCancelProcessInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );


        // Exception error
        String cancelProcessInput = JSON.serialize(selfreCancelProcessInput);
        Map<String, String > inputJSON = new Map<String, String>{
            'cancelProcess' => cancelProcessInput
        };
        TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'cancelProcess', inputJSON, true);
        Test.stopTest();
    }

    @IsTest
    static  void SaveDraftTest(){
        MRO_LC_SelfReadingWizard.CancelProcessInput selfreCancelProcessInput = new MRO_LC_SelfReadingWizard.CancelProcessInput();
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        Dossier__c doss = [
            SELECT Id,Status__c
            FROM Dossier__c
            LIMIT 1
        ];
        selfreCancelProcessInput.oldCaseList = caseList;
        selfreCancelProcessInput.dossierId = doss.Id;

        Test.startTest();

        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'SaveDraft', selfreCancelProcessInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );


        // Exception error
        String cancelProcessInput = JSON.serialize(selfreCancelProcessInput);
        Map<String, String > inputJSON = new Map<String, String>{
            'cancelProcess' => cancelProcessInput
        };
        TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'SaveDraft', inputJSON, true);
        Test.stopTest();
    }


    @IsTest
    static void createCaseTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Supply__c> supply = [
            SELECT Id,Name,Account__c,Product__c, Product__r.Name, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c,Contract__c,
                Contract__r.StartDate,Contract__r.EndDate, Contract__r.ContractTerm, ServicePoint__c
            FROM Supply__c
            LIMIT 10
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        String supplyies = JSON.serialize(supply);
        String listCaseString = JSON.serialize(caseList);

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'caseList'=>listCaseString,
            'supplies'=>supplyies,
            'accountId'=> account.Id,
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Fax'

        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'createCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Map<String, String > inputJSONError = new Map<String, String>{
            'dossierId' => 'dosssId',
            'caseList'=>null,
            'supplies'=>supplyies,
            'accountId'=> account.Id,
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Fax'
        };
        Object responseError = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'createCase', inputJSONError, true);
        Map<String, Object> resultError = (Map<String, Object>) responseError;
        system.assertEquals(true, resultError.get('error') == true );
        Test.stopTest();
    }

    @IsTest
    static void UpdateCaseListTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId
            FROM Case
            LIMIT 2
        ];
        String listCaseString = JSON.serialize(caseList);
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'oldCaseList'=>listCaseString
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'UpdateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }

    @IsTest
    static void updateOriginAndChannelOnDossierTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Fax'
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'updateOriginAndChannelOnDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    static void updateCompanyDivisionAndCommodityOnDossierTest(){
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id
            FROM CompanyDivision__c
            LIMIT 1
        ];
        String recordTypeSelfReadingEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('SelfReading_ELE').getRecordTypeId();
        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'caseRecordTypeId' => recordTypeSelfReadingEle,
            'companyDivisionId' => companyDivision.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_SelfReadingWizard', 'updateCompanyDivisionAndCommodityOnDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
}