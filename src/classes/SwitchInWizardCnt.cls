public with sharing class SwitchInWizardCnt extends ApexServiceLibraryCnt {

    private static OpportunityQueries opportunityQuery = OpportunityQueries.getInstance();
    private static OpportunityLineItemQueries oliQuery = OpportunityLineItemQueries.getInstance();
    private static OpportunityServiceItemQueries osiQuery = OpportunityServiceItemQueries.getInstance();
    private static CustomerInteractionQueries customerInteractionQuery = CustomerInteractionQueries.getInstance();
    private static AccountQueries accQuery = AccountQueries.getInstance();
    private static final UserQueries userQuery = UserQueries.getInstance();

    private static OpportunityService oppService = OpportunityService.getInstance();
    private static Constants constantsSrv = new Constants();
    private static DatabaseService databaseSrv = DatabaseService.getInstance();
    private static PrivacyChangeQueries privacyChangeQuery = PrivacyChangeQueries.getInstance();

    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            String accountId = params.get('accountId');
            String opportunityId = params.get('opportunityId');
            String interactionId = params.get('interactionId');
            String companyDivisionId = params.get('companyDivisionId');
            User currentUserInfos = userQuery.getCompanyDivisionId(UserInfo.getUserId());

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            Account acc = accQuery.findAccount(accountId);

            if (String.isBlank(companyDivisionId)) {
                companyDivisionId = null;
            }

            Opportunity opp;
            if (String.isBlank(opportunityId)) {
                Id cusId = null;
                List<CustomerInteraction__c> cus = customerInteractionQuery.listByInteractionIdAndAccountId(accountId, interactionId);

                if (!cus.isEmpty()) {
                    cusId = cus[0].Id;
                }
                opp = oppService.insertOpportunityByCustomerInteraction(cusId, accountId, 'SwitchIn', companyDivisionId);
            } else {
                opp = opportunityQuery.getOpportunityById(opportunityId);
                //String contractId = opp.ContractId != null ? opp.ContractId : '';
                response.put('companyDivisionId', opp.CompanyDivision__c);
                response.put('companyDivisionName', opp.CompanyDivision__r.Name);
                response.put('contractIdFromOpp', opp.ContractId);
                response.put('customerSignedDate', opp.ContractSignedDate__c);
                response.put('opportunityCompanyDivisionId', opp.CompanyDivision__c);
            }
            List<OpportunityLineItem> olis = oliQuery.getOLIsByOpportunityId(opp.Id);
            List<OpportunityServiceItem__c> osis = osiQuery.getOSIsByOpportunityId(opp.Id);

            String contractAccountId = '';
            if (!osis.isEmpty()) {
                contractAccountId = osis[0].ContractAccount__c;
            }
            response.put('opportunityId', opp.Id);
            response.put('opportunity', opp);
            response.put('opportunityLineItems', olis);
            response.put('opportunityServiceItems', osis);
            response.put('accountId', accountId);
            response.put('account', acc);
            response.put('contractAccountId', contractAccountId);
            response.put('stage', opp.StageName);
            response.put('user', currentUserInfos);
            response.put('error', false);

            return response;
        }
    }

    public with sharing class updateOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String privacyChangeId = params.get('privacyChangeId');
            String contractId = params.get('contractId');
            String stage = params.get('stage');
            Opportunity opps;
            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
            try {
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.StageName = stage;
                //opp.ContractId = String.isBlank(contractId) ? null : contractId;
                databaseSrv.upsertSObject(opp);

                if (stage == 'Closed Won') {
                    Contract myContract = new Contract();
                    opps = opportunityQuery.getOpportunityById(opportunityId);
                    myContract.Id = String.isBlank(opps.ContractId) ? null : opps.ContractId;
                    if (opps.ContractSignedDate__c != null) {
                        myContract.CustomerSignedDate = opps.ContractSignedDate__c;
                    }
                    OpportunityService.generateAcquisitionChain(opp.Id, 'SwitchIn', myContract, privacyChangeId);
                }
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class updateOsiList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            UpdateOsiListInput updateOsiListParams = (UpdateOsiListInput) JSON.deserialize(jsonInput, UpdateOsiListInput.class);

            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
                for (OpportunityServiceItem__c osi : updateOsiListParams.opportunityServiceItems) {
                    osiList.add(new OpportunityServiceItem__c(Id = osi.Id, ContractAccount__c = updateOsiListParams.contractAccountId));
                }
                databaseSrv.updateSObject(osiList);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class checkOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String osiId = params.get('osiId');

            Savepoint sp = Database.setSavepoint();
            Map<String, Object> response = new Map<String, Object>();
            try {
                OpportunityServiceItem__c osi = osiQuery.getById(osiId);
                response.put('opportunityServiceItem', osi);
                Opportunity opp = osi.Opportunity__r;
                ServicePoint__c point = OpportunityService.instantiateServicePoint(osi);
                databaseSrv.upsertSObject(point);

                Supply__c supply = OpportunityService.instantiateSupply(opp, osi);
                supply.ServicePoint__c = point.Id;
                databaseSrv.insertSObject(supply);

                Case tkt = OpportunityService.instantiateCase(opp, osi, 'SwitchIn');
                tkt.Supply__c = supply.Id;
                databaseSrv.insertSObject(tkt);

                response.put('opportunityServiceItem', osi);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            Database.rollback(sp);
            return response;
        }
    }

    public with sharing class linkOliToOsi extends AuraCallable {
        public override Object perform(final String jsonInput) {
            LinkToOsiInput linkOliToOsiParams = (LinkToOsiInput) JSON.deserialize(jsonInput, LinkToOsiInput.class);

            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                List<OpportunityServiceItem__c> osiList = new List<OpportunityServiceItem__c>();
                for (OpportunityServiceItem__c osi : linkOliToOsiParams.opportunityServiceItems) {
                    osiList.add(new OpportunityServiceItem__c(Id = osi.Id, Product__c = linkOliToOsiParams.oli.Product2Id));
                }
                databaseSrv.updateSObject(osiList);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    // Classes for deserialize JSON into Objects
    public class UpdateOsiListInput {
        @AuraEnabled
        public List<OpportunityServiceItem__c> opportunityServiceItems { get; set; }
        @AuraEnabled
        public String contractAccountId { get; set; }
    }

    public class LinkToOsiInput {
        @AuraEnabled
        List<OpportunityServiceItem__c> opportunityServiceItems { get; set; }
        @AuraEnabled
        OpportunityLineItem oli { get; set; }
    }

    public with sharing class updateCompanyDivisionInOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String companyDIvisionId = params.get('companyDivisionId');

            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
            try {
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.companyDivision__c = companyDIvisionId;
                databaseSrv.upsertSObject(opp);
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }
    public with sharing class updateContractAndContractSignedDateOnOpportunity extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String opportunityId = params.get('opportunityId');
            String customerSignedDate = params.get('customerSignedDate');
            String contractId = params.get('contractId');

            Map<String, Object> response = new Map<String, Object>();
            //Savepoint sp = Database.setSavepoint();
            try {
                Opportunity opp = new Opportunity();
                opp.Id = opportunityId;
                opp.ContractId = String.isBlank(contractId) ? null : contractId;
                opp.ContractSignedDate__c = String.isBlank(customerSignedDate) ? null : Date.valueOf(customerSignedDate);
                databaseSrv.upsertSObject(opp);
                response.put('opportunityId', opp.Id);
                response.put('error', false);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

}