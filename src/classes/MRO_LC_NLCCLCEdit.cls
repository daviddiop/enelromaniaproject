/**
 * Created by vincenzo.scolavino on 23/03/2020.
 */

public with sharing class MRO_LC_NLCCLCEdit extends ApexServiceLibraryCnt  {
    private static MRO_QR_Supply        supplyQuery              = MRO_QR_Supply.getInstance();
    private static MRO_QR_Case          caseQuery                = MRO_QR_Case.getInstance();
    private static MRO_QR_ServiceSite   serviceSiteQuery         = MRO_QR_ServiceSite.getInstance();
    private static MRO_QR_Account       accountQuery             = MRO_QR_Account.getInstance();
    private static MRO_SRV_Case         caseService              = MRO_SRV_Case.getInstance();
    private static MRO_UTL_Constants    constantService          = MRO_UTL_Constants.getAllConstants();
    private static MRO_QR_ServicePoint  servicePointQuery        = MRO_QR_ServicePoint.getInstance();

    @AuraEnabled
    public static Case getCaseById(String recordId){
        Case result;
        try{
            result = caseQuery.getById(recordId);
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }
        return result;
    }

    /* Search for specific Commodity Type */
    public static Boolean isNlcClcAlreadyUsed(String serviceSiteId, String nlcClcCode){
        System.debug('Class : MRO_LC_NLCCLCEdit -- method : isNlcClcAlreadyUsed');
        System.debug('input - serviceSiteId' + serviceSiteId + ' nlcClcCode ' + nlcClcCode);
        try {
            ServiceSite__c ss = serviceSiteQuery.getById(serviceSiteId);
            String commodityType = !String.isBlank(ss.NLC__c) ? 'ELECTRIC' : (!String.isBlank(ss.CLC__c) ? 'GAS' : '');
            switch on(commodityType) {
                when 'ELECTRIC' {
                    return serviceSiteQuery.listServiceSitesByCLCorNLC('', nlcClcCode).size() > 0;
                }
                when 'GAS' {
                    return serviceSiteQuery.listServiceSitesByCLCorNLC(nlcClcCode, '').size() > 0;
                }
                when else {
                    throw new WrtsException('Invalid Commodity');
                }
            }
        } catch(Exception ex){
            throw ex;
        }

    }

    /* Search for all Commodity Types */
    public static Boolean isNlcClcAlreadyUsed(String nlcClcCode){
        System.debug('Class : MRO_LC_NLCCLCEdit - method : isNlcClcAlreadyUsed');
        System.debug('input - serviceSiteId' + ' nlcClcCode ' + nlcClcCode);
        try {
            return serviceSiteQuery.listServiceSitesByCLCorNLC(nlcClcCode, nlcClcCode).size() > 0;
        } catch(Exception ex){
            throw ex;
        }

    }

    /* Search for all Commodity Types */
    public static Boolean isServicePointAlreadyUsed(String servicePointCode){
        System.debug('Class : MRO_LC_NLCCLCEdit - method : isServicePointAlreadyUsed');
        System.debug('input - servicePointCode' + servicePointCode);
        try {
            Set<String> servicePointStrings = new Set<String>();
            servicePointStrings.add(servicePointCode);
            return servicePointQuery.getListServicePointsByCodes(servicePointStrings).size() > 0;
        } catch(Exception ex){
            System.debug('Error message: ' + ex.getMessage() + ' at line: ' + ex.getStackTraceString());
            throw ex;
        }

    }


    public with sharing class getSupplyById extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('Class : MRO_LC_NLCCLCEdit -- inner : getSupplyById --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            try {
                Map<String, String> params = asMap(jsonInput);
                String supplyId = params.get('recordId');
                Supply__c result = supplyQuery.getSupplyWithCLCNLCById(supplyId);
                System.debug('output : ' + result);
                response.put('supply', result);
                response.put('error', false);
            }catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }


    public with sharing class createNewCase extends AuraCallable {
        public override Object perform(final String jsonInput) {

            System.debug('Class : MRO_LC_NLCCLCEdit -- inner : createNewCase --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            try {
                Map<String, String> params = asMap(jsonInput);
                Map<String, String> caseRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('NLC-CLC Update');
                String newNlcClc         =  params.get('nlcClcCode');
                String oldNlcClc         =  params.get('oldNlcClc');
                String typeLabel         =  params.get('typeLabel');
                String newServicePointCode = params.get('servicePointCode');
                String oldServicePointCode = params.get('oldServicePointCode');
                Id recordTypeId          =  caseRecordTypes.get('NLC_CLC_Update');
                String comment           = 'Old Value '+ typeLabel + ' : ' + oldNlcClc + '. Old Value POD ' + ' : ' + oldServicePointCode;

                System.debug('recordType id : ' + recordTypeId);

                if(MRO_LC_NLCCLCEdit.isNlcClcAlreadyUsed(newNlcClc)){
                    System.debug('The value ' + newNlcClc + ' entered for '+ typeLabel + ' is already present in the system');
                    response.put('error', true);
                    response.put('errorMsg', 'The value' + newNlcClc + ' entered for '+ typeLabel + ' is already present in the system');
                }
                else if(newServicePointCode != oldServicePointCode && MRO_LC_NLCCLCEdit.isServicePointAlreadyUsed(newServicePointCode)){
                    System.debug('The value ' + newServicePointCode + ' entered for POD  is already present in the system');
                    response.put('error', true);
                    response.put('errorMsg', 'The value ' + newServicePointCode + ' entered for POD  is already present in the system');
                } else {
                    Case newCase = this.createCase(params);
                    if(newCase != null) {
                        CaseComment caseComment = this.createCaseComment(newCase.id, comment);
                        if(this.recordCreated(newCase,caseComment)) {
                            response.put('case', newCase);
                            response.put('error', false);
                        }
                    }
                    else
                        response.put('error', true);

                }

            }catch(Exception ex){
                System.debug(ex.getStackTraceString() + ' message : ' + ex.getMessage());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }

        private Case createCase(Map<String,String> params){
            Map<String, String> caseRecordTypes = MRO_UTL_Constants.getCaseRecordTypes('NLC-CLC Update');
            String accountId         =  params.get('accountId');
            String dossierId         =  params.get('dossierId');
            String supplyId          =  params.get('supplyId');
            String serviceSiteId     =  params.get('serviceSiteId');
            String companyDivisionId =  params.get('companyDivisionId');
            String newNlcClc         =  params.get('nlcClcCode');
            String servicePointCode  =  params.get('servicePointCode');
            String customerNotes     =  params.get('customerNotes');
            String oldNlcClc         =  params.get('oldNlcClc');
            String oldServicePointCode = params.get('oldServicePointCode');
            Id recordTypeId          =  caseRecordTypes.get('NLC_CLC_Update');
            String caseInitStatus    = constantService.CASE_STATUS_DRAFT;
            String caseInitPhase     = constantService.CASE_START_PHASE;
            Case newCase = new Case();

            try {
                newCase = caseService
                        .insertCaseForNclClcChange(accountId, dossierId, supplyId,
                                serviceSiteId, companyDivisionId,
                                newNlcClc, servicePointCode,
                                recordTypeId, caseInitStatus,
                                caseInitPhase, customerNotes, oldNlcClc, oldServicePointCode);
                System.debug('output : new Case created ' + newCase);
            } catch(Exception e){
                throw e;
            }
            return newCase;

        }

        private CaseComment createCaseComment(String newCaseId, String comment){
            CaseComment caseComment = new CaseComment();
            try {
                if (newCaseId != null && !String.isBlank(newCaseId)) {
                    caseComment = caseService.createCaseComment(newCaseId, comment);
                    System.debug('output : new Case Comment created ' + caseComment + ' related to case id ' + caseComment.ParentId);
                }
            } catch(Exception e){
                throw e;
            }
            return caseComment;
        }

        private Boolean recordCreated(Case newCase, CaseComment caseComment){
            return newCase.Id!=null && !String.isBlank(newCase.Id) &&
                    caseComment.Id!=null && !String.isBlank(caseComment.Id);
        }


    }


    public with sharing class UpdateCaseNlcClc extends AuraCallable {
        public override Object perform(final String jsonInput) {

            System.debug('Class : MRO_LC_NLCCLCEdit -- inner : UpdateCaseNlcClc --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            try {
                Map<String, String> params = asMap(jsonInput);
                String recordId         =  params.get('recordId');
                String nlcClcCode       =  params.get('nlcClcCode');
                String customerNotes    =  params.get('customerNotes');
                String typeLabel        =  params.get('typeLabel');
                String servicePointCode =  params.get('servicePointCode');
                String oldServicePointCode = params.get('oldServicePointCode');

                if(MRO_LC_NLCCLCEdit.isNlcClcAlreadyUsed(nlcClcCode)){
                    System.debug('The value' + nlcClcCode + ' entered for '+ typeLabel + ' is already present in the system');
                    response.put('error', true);
                    response.put('errorMsg', 'The value' + nlcClcCode + ' entered for '+ typeLabel + ' is already present in the system');
                }else if(servicePointCode != oldServicePointCode && MRO_LC_NLCCLCEdit.isServicePointAlreadyUsed(servicePointCode)){
                    System.debug('The value ' + servicePointCode + ' entered for POD  is already present in the system');
                    response.put('error', true);
                    response.put('errorMsg', 'The value ' + servicePointCode + ' entered for POD  is already present in the system');
                } else {
                    Case editedCase = caseQuery.getById(recordId);
                    editedCase.NLCCLC__c = nlcClcCode;
                    editedCase.CustomerNotes__c = customerNotes;
                    editedCase.ServicePointCode__c = servicePointCode;
                    editedCase = caseService.updateCaseNlcClc(editedCase);

                    System.debug('output : updated case ' + editedCase);
                    response.put('case', editedCase);
                    response.put('error', false);
                }
            }catch(Exception ex){
                System.debug(ex.getStackTraceString() + ' message : ' + ex.getMessage());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }


    public with sharing class checkExistingCaseForSupply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            System.debug('Class : MRO_LC_NLCCLCEdit -- inner : createNewCase --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            try {
                Map<String, String> params = asMap(jsonInput);
                String supplyId = params.get('supplyId');
                String dossierId = params.get('dossierId');

                List<Case> cases = caseQuery.getCasesByDossierId(dossierId);
                for(Case c : cases){
                    if(c.Supply__c.equals(supplyId)){
                        response.put('error', true);
                        response.put('message', System.Label.TheSelectedSupplyIsAlreadyRegisteredToTheCurrentCustomer);
                        return response;
                    }
                }
                response.put('error', false);
            }catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }

    public with sharing class GetServicePointDistributor extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();

            try {
                Map<String, String> params = asMap(jsonInput);
                String supplyId = params.get('supplyId');
                Supply__c supply = supplyQuery.getById(supplyId);
                Account distributor = accountQuery.findAccount(supply.ServicePoint__r.Distributor__c);

                response.put('error', false);
                response.put('distributor', distributor);
            } catch(Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }

}