/**
 * Created by Boubacar Sow on 07/11/2019.
 */
@IsTest
public with sharing class MRO_LC_MeterCheckTst {
    @TestSetup
    static void setup() {

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'MeterCheckEle', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'MeterCheckEle', recordTypeEle, '');
        insert phaseDI010ToRC010;

        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c = 'MeterCheckTemplate_1';
        insert scriptTemplate;
        ScriptTemplateElement__c scriptTemplateElement = MRO_UTL_TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        scriptTemplateElement.ScriptTemplate__c = scriptTemplate.Id;
        insert scriptTemplateElement;
        
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;
        
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        /*BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;
        
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
           // caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }
    
    @IsTest
    public static void InitializeMeterCheckOK(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];
        
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
            SELECT Id,Name
            FROM CompanyDivision__c
            WHERE Name = 'ENEL 1'
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterCheck', 'InitializeMeterCheck', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false, 'Error is found');
        
        inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => '',
            'interactionId' => customerInteraction.Id,
            'companyDivisionId' => companyDivision.Id
        };
        response = TestUtils.exec(
            'MRO_LC_MeterCheck', 'InitializeMeterCheck', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false,'Error is found');
        
        Test.stopTest();
    }
    
    @IsTest
    static void InitializeMeterCheckKO() {
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                'MRO_LC_MeterCheck', 'InitializeMeterCheck', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == true,'Error is found');
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    
    @IsTest
    public static void CreateCaseOK(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Supply__c> supply = [
            SELECT Id,Name,CompanyDivision__c
            FROM Supply__c
            LIMIT 1
        ];
        String supplyString = JSON.serialize(supply);
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'supply' => supplyString,
            'invoiceIds' => invoiceIds
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterCheck', 'CreateCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false,'Error is found');
        
        Test.stopTest();
    }
    
    @IsTest
    public static void CreateCaseKO(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Supply__c> supply = new List<Supply__c>();/* [
            SELECT Id,Name,CompanyDivision__c
            FROM Supply__c
            LIMIT 1
        ];*/
        String supplyString = JSON.serialize(supply);
        String invoiceIds = '["1000000","1000001","1000002","1000003","1000004","1000005","1000006","1000007","1000008","1000009"]';
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'supply' => supplyString,
            'invoiceIds' => invoiceIds
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterCheck', 'CreateCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true,'Error is found');
        Test.stopTest();
    }

    @IsTest
    private static void getSupplyRecordTest(){
        String supplyId = [
            SELECT Id
            FROM Supply__c
            LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'supplyId' => supplyId
        };
        Test.startTest();
        Object response = TestUtils.exec( 'MRO_LC_MeterCheck', 'getSupplyRecord', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        //system.assertNotEquals(result.size(),0);
        Test.stopTest();
    }
    
    
    @IsTest
    private static void CancelProcess() {
        
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id
        };
        
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterCheck', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false,'Error is found');
        Test.stopTest();
    }
}