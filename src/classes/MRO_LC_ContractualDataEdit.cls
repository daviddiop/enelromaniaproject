/**
 * Created by vincenzo.scolavino on 15/04/2020.
 */

public with sharing class MRO_LC_ContractualDataEdit extends ApexServiceLibraryCnt {
    private static MRO_QR_Supply                  supplyQuery              = MRO_QR_Supply.getInstance();
    private static MRO_QR_ContractAccount         contractAccountQuery     = MRO_QR_ContractAccount.getInstance();
    private static MRO_QR_Contract                contractQuery            = MRO_QR_Contract.getInstance();
    private static MRO_QR_Case                caseQuery            = MRO_QR_Case.getInstance();
    private static MRO_QR_Consumption consumptionQuery = MRO_QR_Consumption.getInstance();
    private static MRO_QR_OpportunityServiceItem osiQuery = MRO_QR_OpportunityServiceItem.getInstance();
    private static MRO_UTL_Constants constantsSrv = MRO_UTL_Constants.getAllConstants();
    private static MRO_SRV_Consumption consumptionService = MRO_SRV_Consumption.getInstance();
    private static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();

    public with sharing class getContractData extends AuraCallable {
        protected override Object perform(String jsonInput) {
            System.debug('Class : MRO_LC_ContractualDataEdit -- inner : getContractData --- method : perform');
            System.debug('input : ' + jsonInput);

            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String supplyId = params.get('supplyId');
            String caseId = params.get('caseId');
            String subProcess = params.get('subProcess');
            try {
                String contractAccountId ;
                String contractId ;
                if(!String.isBlank(caseId)){
                   Case myCase= caseQuery.getById(caseId);
                    response.put('case', myCase);
                    supplyId = myCase.Supply__c;
                    contractAccountId = myCase.ContractAccount__c;
                    contractId = myCase.Contract__c;
                }
                Supply__c selectedSupply;
                if(!String.isBlank(supplyId)){
                    selectedSupply = supplyQuery.getById(supplyId);
                    contractAccountId = selectedSupply.ContractAccount__c;
                    contractId = selectedSupply.Contract__c;
                    response.put('supply', selectedSupply);
                }
                if(!String.isBlank(contractId)){
                    Contract selectedContract = contractQuery.getById(contractId);
                    response.put('contract', selectedContract);

                }
                if(!String.isBlank(contractAccountId)){
                    ContractAccount__c selectedContractAccount = contractAccountQuery.getById(contractAccountId);
                    response.put('contractAccount', selectedContractAccount);
                }

                if(subProcess == constantsSrv.SUB_PROCESS_CHNG_CONTRACTED_QUANTITIES ){
                    Set<Id> supplyIds= new Set<Id>();
                    supplyIds.add(selectedSupply.Id);
                    List<Consumption__c> consumptionList = consumptionQuery.listBySupplyIds(supplyIds);
                    Set<Id> osiIds= new Set<Id>();
                    for(Consumption__c consumption : consumptionList){
                        osiIds.add(consumption.OpportunityServiceItem__c);
                        if(String.isBlank(caseId)){
                            consumption.Id = null;
                        }
                    }
                    List<OpportunityServiceItem__c> osis = osiQuery.getOSIsByIds(osiIds);
                    if(!osis.isEmpty()){
                        response.put('opportunityServiceItems', osis);
                    }
                    response.put('consumptionList', consumptionList);

                }

                String casePhase = constantsSrv.CASE_START_PHASE;
                String caseStatus = constantsSrv.CASE_STATUS_DRAFT;
                String contractStatus=constantsSrv.CONTRACT_STATUS_DRAFT;
                casePhase = constantsSrv.CASE_START_PHASE;

                response.put('casePhase', casePhase);
                response.put('caseStatus', caseStatus);
                response.put('contractStatus',contractStatus);

                response.put('error', false);
            }catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }

    public with sharing class getMapBankNameWithId extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, Id> mapBankId = billingProfileQuery.getMapBankId();
            response.put('mapBankId', mapBankId);
            return response;
        }
    }


    public with sharing class insertConsumptionList extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();

            try {
                Map<String, String> params = asMap(jsonInput);
                String caseId=params.get('caseId');
                String supplyId=params.get('supplyId');
                List<Consumption__c> consumptionList    = (List<Consumption__c>) JSON.deserialize(params.get('consumptionList'), List<Consumption__c>.class);
                //ConsumptionDetails details = (ConsumptionDetails) JSON.deserialize(jsonInput, ConsumptionDetails.class);
                //List<Consumption__c> consumptionList=details.consumptionList;
                for(Consumption__c consumption : consumptionList){
                    if(caseId != consumption.Case__c){
                        consumption.Id = null;
                    }
                    consumption.Case__c = caseId;
                    consumption.Status__c = constantsSrv.CONSUMPTION_STATUS_ACTIVATING;
                    consumption.Supply__c = supplyId;
                }
                consumptionService.upsertConsumption(consumptionList);
                response.put('error', false);
            }catch(Exception ex){
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }

}