/**
 * Created by Antonella Loche on 31/03/2020.
 */
    
@IsTest
public with sharing class MRO_SRV_GenesysCallOutTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account account = MRO_UTL_TestDataFactory.account().businessAccount().build();
        account.Name = 'BusinessAccount1';
        insert account;
        
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;  
        
        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '43252435624654';
        servicePoint.Account__c = account.Id;
        servicePoint.Trader__c = account.Id;
        servicePoint.Distributor__c = account.Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;
        
        Contact contact = MRO_UTL_TestDataFactory.Contact().createContact().build();
        contact.AccountId = account.Id;
        insert contact;
        
        Contract contract = TestDataFactory.Contract().createContract().build();
        contract.Name = 'Name11062019';
        contract.AccountId = account.Id;
        insert contract;

        
        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
        supply.Contract__c = contract.Id;
        supply.Account__c = account.Id;
        //supply.ServicePoint__c = servicePoint.Id;
        supply.Status__c = 'Active';
        insert supply;
        
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        interaction.InterlocutorPhone__c = '34566778899';
        insert interaction;
        
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, account.Id, contact.Id).build();
        insert customerInteraction; 

        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = account.Id;
        dossier.CustomerInteraction__c = customerInteraction.id;
        insert dossier;
        
        Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
        caseRecord.AccountId = account.Id;
        caseRecord.Dossier__c = dossier.id;
        insert caseRecord;
    }

    @IsTest
    public static void buildMultiRequestTest(){
        Case tempCase = [select id, dossier__c, Dossier__r.customerinteraction__c from case limit 1];
        system.debug('Dossier__c '+ tempCase.Dossier__r.CustomerInteraction__c);
        
    	Interaction__c intRecord = [SELECT Name, Comments__c, 
                                    (select Customer__c, Contact__c,
										Customer__r.IntegrationKey__c 
										//Contact__r.MarketingPhone__c
										from CustomerInteractions__r),
									LastModifiedById
								FROM Interaction__c
        						LIMIT 1];
        system.debug('intRecord '+ intRecord);
        Map<String, Object> argsMap = new Map<String, Object>();
        argsMap.put('sender', tempCase);
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('requestType','test');
        argsMap.put('parameters', parameters);
        system.debug('argsMap ***** '+ argsMap);
        MRO_SRV_SendMRRcallout.IMRRCallout IMRRCallout = (MRO_SRV_SendMRRcallout.IMRRCallout) MRO_SRV_GenesysCallOut.class.newInstance();
        wrts_prcgvr.MRR_1_0.MultiRequest multiRequest = IMRRCallout.buildMultiRequest(argsMap);
        system.debug('multirequest '+multiRequest);
        
        //String cn = [SELECT CaseNumber FROM Case WHERE Id = :intRecord.Id].CaseNumber;
        for(wrts_prcgvr.MRR_1_0.Field f : multiRequest.requests[0].objects[0].fields){
            If(f.name == 'Name') System.assert(f.value == intRecord.id);
        }
 
    }

    @IsTest
    public static void calloutResponseTest(){
        wrts_prcgvr.MRR_1_0.MultiResponse calloutResponse = new wrts_prcgvr.MRR_1_0.MultiResponse();
        MRO_SRV_SendMRRcallout.IMRRCallout IMRRCallout = (MRO_SRV_SendMRRcallout.IMRRCallout) MRO_SRV_GenesysCallOut.class.newInstance();
        wrts_prcgvr.ApexActionIntegration_1_0.CalloutResponse response  = IMRRCallout.buildResponse(calloutResponse);
    }
}