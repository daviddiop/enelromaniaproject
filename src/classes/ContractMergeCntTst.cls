/**
 * Created by goudiaby on 02/10/2019.
 */

@isTest
public with sharing class ContractMergeCntTst {

    @testSetup
    private static void setup() {
        List<Supply__c> supplyList = new List<Supply__c>();
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new List<Account>();
        listAccount.add(TestDataFactory.account().personAccount().build());
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;

        Contract contract = TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[1].Id;
        insert contract;

        CompanyDivision__c companyDivision = TestDataFactory.companyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Interaction__c interaction = TestDataFactory.interaction().createInteraction().build();
        insert interaction;

        ServicePoint__c servicePoint = TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Account__c = listAccount[1].Id;
        servicePoint.Trader__c = listAccount[1].Id;
        servicePoint.Distributor__c = listAccount[1].Id;
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        BillingProfile__c billingProfile = TestDataFactory.billingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c = listAccount[1].Id;
        billingProfile.PaymentMethod__c = 'Postal Order';
        insert billingProfile;

        ContractAccount__c contractAccount = TestDataFactory.contractAccount().createContractAccount().setAccount(listAccount[1].Id).setBillingProfile(billingProfile.Id).build();
        insert contractAccount;

        for (Integer i = 0; i < 20; i++) {
            Supply__c supply = TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c = contract.Id;
            supply.Account__c = listAccount[1].Id;
            //supply.ServicePoint__c = servicePoint.Id;
            supplyList.add(supply);
        }
        insert supplyList;

        Dossier__c dossier = TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[1].Id;
        insert dossier;

        List<Case> caseList = new List<Case>();
        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Contract__c = contract.Id;
            caseRecord.ContractAccount__c = contractAccount.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Termination_ELE').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @isTest
    static void initializeTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Interaction__c interaction = [
                SELECT Id
                FROM Interaction__c
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'interactionId' => interaction.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'ContractMergeCnt', 'Initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @isTest
    static void InitializeContractMergeExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'ContractMergeCnt', 'Initialize', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            System.assertEquals(true, result.get('error') == true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    private static void CreateCaseTest() {
        List<Supply__c> supply = [
                SELECT Id,Name,Account__c, RecordTypeId,RecordType.DeveloperName,CompanyDivision__c
                FROM Supply__c
                WHERE Status__c = 'Active'
        ];
        List<Case> caseList = [
                SELECT Id,Supply__c,AccountId
                FROM Case
                LIMIT 2
        ];
        List<Account> myAccount = [
                SELECT Id
                FROM Account
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        String supplyListString = JSON.serialize(supply);
        String caseListString = JSON.serialize(caseList);

        Map<String, String > inputJSON = new Map<String, String>{
                'supplies' => supplyListString,
                'accountId' => myAccount[0].Id,
                'caseList' => caseListString,
                'dossierId' => dossier.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'ContractMergeCnt', 'CreateCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @isTest
    static void updateCasesTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,CompanyDivision__c
                FROM Case
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        ContractAccount__c contractAccount = [
                SELECT Id
                FROM ContractAccount__c
        ];

        Contract contract = [
                SELECT Id
                FROM Contract
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'caseList' => caseListString,
                'dossierId' => dossier.Id,
                'contractAccountId' => contractAccount.Id,
                'contract' => contract.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'ContractMergeCnt', 'UpdateCases', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }

    @isTest
    static void updateCasesExceptionTest() {
        List<Case> caseList = [
                SELECT Id,Supply__c,CompanyDivision__c
                FROM Case
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        ContractAccount__c contractAccount = [
                SELECT Id
                FROM ContractAccount__c
        ];

        Contract contract = [
                SELECT Id
                FROM Contract
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'caseList' => caseListString,
                'dossierId' => dossier.Id,
                'contractAccountId' => null,
                'contract' => null
        };
        Test.startTest();
        Object  response = TestUtils.exec(
                'ContractMergeCnt', 'UpdateCases', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == true);
        //System.assertEquals(System.Label.AtLeastOneOfTheFollowingRequestsMustBeSelected, response);
        Test.stopTest();
    }

    @isTest static void CancelProcessTest() {
        List<Case> caseList = [
                SELECT Id, RecordTypeId,CompanyDivision__c
                FROM Case
                LIMIT 1
        ];
        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
                'caseList' => caseListString,
                'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'ContractMergeCnt', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false);
        Test.stopTest();
    }
}