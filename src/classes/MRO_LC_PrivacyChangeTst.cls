/**
 * Created by Moussa Fofana on 07/08/2019.
 */

@isTest
public with sharing class MRO_LC_PrivacyChangeTst {

    @testSetup
    static void setup() {
        /*ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;

        OpportunityServiceItem__c opportunityServiceItem = MRO_UTL_TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        insert opportunityServiceItem;*/
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        List<Account> listAccount = new list<Account>();
        listAccount.add(MRO_UTL_TestDataFactory.account().businessAccount().build());
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        insert listAccount;

        Individual individual = MRO_UTL_TestDataFactory.individual().createIndividual().build();
        insert individual;

        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[0].Id;
        contact.IndividualId = individual.Id;
        insert contact;

        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().setInterlocutor(individual.Id).build();
        interaction.Status__c = 'New';
        insert interaction;

        CustomerInteraction__c bsnCustomerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id,listAccount[0].Id, contact.Id).build();
        insert bsnCustomerInteraction;

        Opportunity opportunity = MRO_UTL_TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        insert opportunity;

        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        PrivacyChange__c privacyChange = MRO_UTL_TestDataFactory.privacyChange().createPrivacyChangeBuilder().setDossier(dossier.Id).build();
        privacyChange.Status__c = 'Active';
        privacyChange.Individual__c = individual.Id;
        insert privacyChange;
    }

    @isTest
    static void getIndividualTest() {
        MRO_LC_PrivacyChange.getInstance();
        Opportunity opportunity = [
            SELECT Id, Name, AccountId
            FROM  Opportunity
            LIMIT 1
        ];
        PrivacyChange__c privacyChange = [
                SELECT Id, Opportunity__c
                FROM  PrivacyChange__c
                LIMIT 1
        ];

        privacyChange.Opportunity__c = opportunity.Id;
        update  privacyChange;
        Contact contact = [
            SELECT Id
            FROM  Contact
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id,
            'dossierId'=> dossier.Id,
            'contactId' => contact.Id
        };

        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividual',
                inputJSON, true);
        system.assertEquals(true, result.get('privacyChange') != null);
        system.assertEquals(true, result.get('individualId') != null);


        privacyChange.Opportunity__c = null;
        update  privacyChange;
        Account accountKey = MRO_UTL_TestDataFactory.account().setKey('1324433').build();
        /*Account accBusines = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accBusines.Key__c = accountKey.Key__c;
        insert  accBusines;
        AccountContactRelation accountContactRelation = MRO_UTL_TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id, accBusines.Id).build();
        accountContactRelation.LegalRepresentative__c = true;
        insert accountContactRelation;*/
        //opportunity.AccountId = accBusines.Id;
        //update  opportunity;
        inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id
        };
        result = (Map<String, Object>) TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividual',
                inputJSON, true);
        //system.assertEquals(true, result.get('privacyChange') != null);
        //system.assertEquals(true, result.get('individualId') != null);


        Account acc = MRO_UTL_TestDataFactory.account().personAccount().build();
        acc.NationalIdentityNumber__pc = '1630615123457';
        acc.Key__c = '13244346446';
        insert acc;
        opportunity.AccountId = acc.Id;
        update  opportunity;
        inputJSON = new Map<String, String>{
            'opportunityId' => opportunity.Id
        };
        result = (Map<String, Object>) TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividual',
                inputJSON, true);
        //system.assertEquals(true, result.get('privacyChange') != null);

        Test.stopTest();

    }

    @isTest
    static void customerInteractionNotEmptyTest() {
        Opportunity opportunity = [
                SELECT Id, Name, AccountId
                FROM  Opportunity
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];
        opportunity.CustomerInteraction__c = customerInteraction.Id;
        update opportunity;
        PrivacyChange__c privacyChange = [
                SELECT Id, Opportunity__c
                FROM  PrivacyChange__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id
        };
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividual',
                inputJSON, true);
        Test.stopTest();

    }

    @isTest
    static void getIndividualExceptionTest() {
        Opportunity opportunity = [
                SELECT Id, Name, AccountId
                FROM  Opportunity
                LIMIT 1
        ];
        PrivacyChange__c privacyChange = [
                SELECT Id, Opportunity__c
                FROM  PrivacyChange__c
                LIMIT 1
        ];
        privacyChange.Opportunity__c = opportunity.Id;
        update  privacyChange;
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opportunity.Id
        };
        delete opportunity;
        Test.startTest();
        Map<String, Object> result = (Map<String, Object>)TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividual',
                inputJSON, true);
        system.assertEquals(true, result.get('error') == true);
        Test.stopTest();

    }

    @isTest
    static void getIndividualPrivacyRecapTest() {
        Account account = [
            SELECT Id,IsPersonAccount
            FROM Account
            WHERE IsPersonAccount =: true
            LIMIT 1
        ];
            Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id
            };
            Test.startTest();
            Map<String, Object> result = (Map<String, Object>) TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividualPrivacyRecap',
                inputJSON, true);
            Test.stopTest();
        }
    @isTest
    static void getIndividualPrivacyRecapBusinessAccTest() {
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Contact Contact = [
            SELECT Id
            FROM Contact
            LIMIT 1
        ];
        AccountContactRelation accountContactRelation = MRO_UTL_TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id, account.Id).build();
        accountContactRelation.LegalRepresentative__c = true;
        insert accountContactRelation;
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id
        };
        Test.startTest();
        try {
            TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividualPrivacyRecap',inputJSON, true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void getIndividualPrivacyRecapBusinessAccKOTest() {
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id
        };
        Test.startTest();
        try {
            TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividualPrivacyRecap',inputJSON, true);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    static void getIndividualPrivacyRecapExcepTest() {
        Account account = [
            SELECT Id,IsPersonAccount
            FROM Account
            WHERE IsPersonAccount =: true
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_PrivacyChange', 'getIndividualPrivacyRecap',
            inputJSON, true);
        Map<String, Object> result=(Map<String, Object>)response;
        System.assertEquals(true , result.get('error') != false);
        Test.stopTest();
    }
}