/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 06.05.20.
 */

public with sharing class MRO_LC_MassiveHelper extends ApexServiceLibraryCnt {
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static MRO_QR_FileImportJob jobQuery = MRO_QR_FileImportJob.getInstance();
    private static MRO_QR_FileTemplateColumn columnQuery = MRO_QR_FileTemplateColumn.getInstance();

    public class CreateFileImportJobParams {
        @AuraEnabled
        public String documentId { get; set; }
        @AuraEnabled
        public String templateId { get; set; }
        @AuraEnabled
        public String dossierId { get; set; }
        @AuraEnabled
        public String params { get; set; }

        public Map<String, String> paramsMap {
            get {
                return (Map<String, String>) JSON.deserialize(params, Map<String, String>.class);
            }
        }
    }

    public with sharing class loadJob extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            CreateFileImportJobParams params = (CreateFileImportJobParams) JSON.deserialize(jsonInput, CreateFileImportJobParams.class);
            if(params.dossierId != null){
                FileImportJob__c job = jobQuery.getByDossierId(params.dossierId);
                if(job.Status__c != 'Completed' && job.Status__c != 'Rejected'){
                    response.put('jobId', job.Id);
                }
            }
            return response;
        }
    }

    public with sharing class createFileImportJob extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            CreateFileImportJobParams params = (CreateFileImportJobParams) JSON.deserialize(jsonInput, CreateFileImportJobParams.class);

            List<FileTemplate__c> templates = [SELECT Id, BatchSize__c, AllOrNone__c FROM FileTemplate__c WHERE Id = :params.templateId];
            FileTemplate__c template = templates[0];
            Integer batchSize = (Integer) template.BatchSize__c;
            if (batchSize == null) {
                batchSize = 200;
            }

            try {

                FileImportJob__c fileImportJob = new FileImportJob__c(
                        Status__c = 'To Be Validated',
                        FileTemplate__c = params.templateId,
                        ContentDocumentId__c = params.documentId
                );

                if(params.dossierId != null){
                    fileImportJob.Dossier__c = params.dossierId;
                }

                databaseSrv.insertSObject(fileImportJob);
                fileImportJob = jobQuery.getById(fileImportJob.Id);
                Id batchJobId = Database.executeBatch(new MRO_BA_MassiveHelperInsert(fileImportJob, params.documentId, template, params.paramsMap), batchSize);
                fileImportJob.JobID__c = batchJobId;
                databaseSrv.updateSObject(fileImportJob);
            } catch (Exception e) {
                response.put('error', true);
                response.put('errorMessage', e.getMessage());
            }

            return response;
        }
    }

    public with sharing class getProgress extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            CreateFileImportJobParams params = (CreateFileImportJobParams) JSON.deserialize(jsonInput, CreateFileImportJobParams.class);

            FileImportJob__c job = params.documentId != null ? jobQuery.getByDocumentId(params.documentId) : jobQuery.getByDossierId(params.dossierId);

            if (job == null) {
                response.put('progress', 5);
                response.put('status', 'Creating File Import Job');
                return response;
            }

            if (job.Status__c == 'Rejected') {
                String errorMessage = MRO_QR_FileImportDataRow.getInstance().lastErrorFromJob(job.Id);
                response.put('progress', 100);
                response.put('status', errorMessage == null ? 'Job Rejected' : errorMessage);
                response.put('error', true);
                return response;
            }

            if (job.Status__c == 'Completed') {
                response.put('progress', 100);
                response.put('status', 'Completed');
                return response;
            }

            if (job.JobID__c == null) {
                response.put('progress', 10);
                response.put('status', 'Creating Batch Job');
                return response;
            }

            List<AsyncApexJob> apexJobs = [SELECT Id, JobItemsProcessed, TotalJobItems, ApexClassId FROM AsyncApexJob WHERE Id = :job.JobID__c];
            if (apexJobs.size() == 0) {
                response.put('progress', 10);
                response.put('status', 'Creating Batch Job');
                return response;
            }
            AsyncApexJob apexJob = apexJobs[0];
            ApexClass apexClassValidation = [SELECT Id FROM ApexClass WHERE Name = 'MRO_BA_MassiveHelperValidation'];
            Double progress = 0;
            if (apexJob.TotalJobItems > 0) {
                Double itemsProceed = (Double) apexJob.JobItemsProcessed;
                response.put('job', apexJob);
                progress = itemsProceed / apexJob.TotalJobItems;
            }

            response.put('jobsprogress', progress.round());

            if (apexJob.ApexClassId == apexClassValidation.Id) {
                progress = 40 + 30 * progress;
                response.put('progress', progress.round());
                response.put('status', 'Validation Data Row items');
            } else {
                progress = 10 + 30 * progress;
                response.put('progress', progress.round());
                response.put('status', 'Creating Data Row items');
            }
            return response;
        }
    }

    public with sharing class getSample extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            response.put('error', false);
            CreateFileImportJobParams params = (CreateFileImportJobParams) JSON.deserialize(jsonInput, CreateFileImportJobParams.class);

            List<FileTemplateColumn__c> columns = columnQuery.getByFileTemplateIdOrderASC(params.templateId);
            String fileDelimiterEncoded = EncodingUtil.urlEncode(columns[0].FileTemplate__r.FileDelimiter__c, 'UTF-8');

            String sampleLinkHref = 'data:application/octet-stream,';
            List<String> headerLine = new List<String>();
            List<String> sampleLine = new List<String>();
            for (FileTemplateColumn__c col : columns) {
                headerLine.add(col.HeaderName__c);
                sampleLine.add(col.SampleData__c);
            }
            sampleLinkHref += String.join(headerLine, fileDelimiterEncoded) + '\n' + String.join(sampleLine, fileDelimiterEncoded);

            response.put('sampleLinkHref', sampleLinkHref);
            response.put('fileTemplateName', columns[0].FileTemplate__r.FileTemplateName__c);
            return response;
        }
    }

}