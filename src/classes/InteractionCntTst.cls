@isTest
public with sharing class InteractionCntTst {

    @testSetup
    static void setup(){
        Individual newIndividual = TestDataFactory.individual().createIndividual().build();
        insert newIndividual;

        Interaction__c interaction = TestDataFactory.interaction()
            .createInteraction()
            .setInterlocutor(newIndividual.Id)
            .build();
        insert interaction;
    }

    @isTest
    static void listOptionsTest(){
        Test.startTest();
        Map<String, Object> response = (Map<String, Object>)TestUtils.exec('InteractionCnt','listOptions',null,true);
        Test.stopTest();

        System.assertNotEquals(null, response.get('statusList'));
    }

    @isTest
    static void findByIdTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
        ];

        Test.startTest();
        InteractionService.InteractionDTO resultInteraction = (InteractionService.InteractionDTO)TestUtils.exec(
        'InteractionCnt','findById',new Map<String, String>{'interactionId' => interaction.Id},true);
        System.assertEquals(resultInteraction.status, 'New' );

        Object responseException = TestUtils.exec('InteractionCnt','findById',new Map<String, String>{'interactionId'=>''},false);
        System.assertEquals(responseException, System.Label.Interaction + ' - ' + System.Label.MissingId );
        Test.stopTest();
    }

    @isTest
    static void insertInteractionTest(){
        Test.startTest();
        InteractionService.InteractionDTO resultInteraction = (InteractionService.InteractionDTO)TestUtils.exec(
        'InteractionCnt','insertInteraction', null, true );
        Test.stopTest();

        System.assertEquals(resultInteraction.status, 'New' );
    }

    @isTest
    static void upsertInteractionTest(){
        InteractionService.InteractionDTO interactionDTO = new InteractionService.InteractionDTO();
        interactionDTO.comments = 'Test';

        Test.startTest();
        InteractionService.InteractionDTO resultInteraction = (InteractionService.InteractionDTO)TestUtils.exec(
        'InteractionCnt','upsertInteraction', interactionDTO, true );
        Test.stopTest();

        System.assertNotEquals(null, resultInteraction);
        System.assertEquals('Test', resultInteraction.comments);
    }

    @isTest
    static void removeInterlocutorTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];

        Test.startTest();
        InteractionService.InteractionDTO resultInteraction = (InteractionService.InteractionDTO)TestUtils.exec(
        'InteractionCnt','removeInterlocutor',new Map<String, String>{'interactionId'=>interaction.Id},true);
        Interaction__c updatedInteraction = [
            SELECT Id, Interlocutor__c
            FROM Interaction__c
            WHERE Id = :interaction.Id
        ];
        System.assertEquals(null, updatedInteraction.Interlocutor__c);

        String error = (String)TestUtils.exec('InteractionCnt','removeInterlocutor',new Map<String, String>{'interactionId'=>''},false);
        System.assertEquals(System.Label.Interaction + ' - ' + System.Label.MissingId, error);
        Test.stopTest();
    }

    @isTest
    static void removeInterlocutorEmptyIdTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        interaction.Interlocutor__c = null;
        interaction.InterlocutorFirstName__c = 'Test';
        update interaction;

        Test.startTest();
        InteractionService.InteractionDTO resultInteraction = (InteractionService.InteractionDTO)TestUtils.exec(
            'InteractionCnt','removeInterlocutor',new Map<String, String>{'interactionId'=>interaction.Id},true);
        Test.stopTest();

        System.assertEquals(null, resultInteraction.interlocutorFirstname);
        System.assertEquals(' ', resultInteraction.getInterlocutorFullname());
        System.assertEquals(false, resultInteraction.getIsClosed());
    }

    @isTest
    static void removeInterlocutorWithCustomerInteractionTest(){
        Interaction__c interaction = [
            SELECT Id
            FROM Interaction__c
            WHERE Interlocutor__c != NULL
        ];
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        insert businessAccount;
        Contact contact = TestDataFactory.contact().createContact().build();
        insert contact;
        insert TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, businessAccount.Id, contact.Id).build();

        Test.startTest();
        InteractionService.InteractionDTO resultInteraction = (InteractionService.InteractionDTO)TestUtils.exec(
            'InteractionCnt','findById',new Map<String, String>{'interactionId'=>interaction.Id},true);
        System.assertNotEquals(null, resultInteraction.customerId);
        System.assertNotEquals(null, resultInteraction.customerInteractionIds);

        String error = (String)TestUtils.exec(
            'InteractionCnt','removeInterlocutor',new Map<String, String>{'interactionId'=>interaction.Id},false);
        System.assertEquals(Label.InterlocutorRemoveDenied, error);
        Test.stopTest();
    }
}