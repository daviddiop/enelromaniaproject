/**
 * Created by napoli on 29/10/2019.
 */

@isTest
private class MRO_UTL_GuidTst {
    @isTest
    static void unit_test_1() {
        Pattern p = Pattern.compile('[\\w]{8}-[\\w]{4}-4[\\w]{3}-[89ab][\\w]{3}-[\\w]{12}');
        for(Integer x = 0; x < 100; x++) {
            Matcher m = p.matcher(MRO_UTL_Guid.NewGuid());
            System.assert(m.matches() == true);
        }
    }
}