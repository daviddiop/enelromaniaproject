/**
 * Created by BADJI on 01/09/2020.
 */

@IsTest
public with sharing class MRO_LC_ProductChangeWizardTst {

    @TestSetup
    static void Setup(){

        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('DI010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MeterCheckEle').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'MeterCheckEle', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('DI010', 'RC010', 'MeterCheckEle', recordTypeEle, '');
        insert phaseDI010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = 'E0120200617144259786';
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        contract.ContractType__c = 'Business';
        insert contract;


        Opportunity opportunity = MRO_UTL_TestDataFactory.opportunity().createOpportunity().setCompany(companyDivision.Id).build();
        opportunity.AccountId = listAccount[0].Id;
        opportunity.ContractId = contract.Id;
        opportunity.Channel__c ='Magazin Enel';
        //opportunity.RequestType__c = 'ProductChange';
        //opportunity.SubProcess__c = 'Change Product within the same Market plus New Contract';
        insert opportunity;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        List<ContractAccount__c> listContractAccount = new List<ContractAccount__c>();
        for (Integer i = 0; i< 5; i++){
            ContractAccount__c contractAccount =  MRO_UTL_TestDataFactory.contractAccount().build();
            listContractAccount.add(contractAccount);
        }
        insert listContractAccount;
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supply.Market__c = 'Free';
            supplyList.add(supply);
        }
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;
        insert supplyList;
        DocumentType__c documentType01 = MRO_UTL_TestDataFactory.DocumentType().createDocumentType('document Type Test').build();
        insert documentType01;
        DocumentBundle__c bundle = new DocumentBundle__c();
        bundle.Name = 'Doc bundle test';
        insert bundle;
        /*Account trader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert trader;*/
        OpportunityServiceItem__c opportunityServiceItem = MRO_UTL_TestDataFactory.opportunityServiceItem().createOpportunityServiceItem().build();
        opportunityServiceItem.ServicePointCode__c = servicePoint.Id;
        opportunityServiceItem.Opportunity__c = opportunity.Id;
        //opportunityServiceItem.Trader__c = trader.Id;

        insert opportunityServiceItem;

        DocumentBundleItem__c docBundleItem01 = MRO_UTL_TestDataFactory.DocumentBundleItem().createDocumentBundleItem(bundle.Id,documentType01.Id,true).build();
        insert docBundleItem01;

        BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;
        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().setAccount(listAccount[0].Id).build();
        contractAccount.SelfReadingPeriodEnd__c = '4';
        insert  contractAccount;

        NE__Product__c p0 = new NE__Product__c();
        p0.RateType__c ='Single Rate';
        insert p0;
        Product2 product2 =MRO_UTL_TestDataFactory.product2().build();
        product2.CommercialProduct__c = p0.Id;
        insert product2;

        PricebookEntry pricebookEntry = MRO_UTL_TestDataFactory.pricebookEntry().build();
        pricebookEntry.Product2Id = product2.Id;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        insert pricebookEntry;
        OpportunityLineItem opportunityLineItem = MRO_UTL_TestDataFactory.opportunityLineItem().createOpportunityLineItem().build();
        opportunityLineItem.OpportunityID = opportunity.Id;
        opportunityLineItem.Product2ID = product2.Id;
        opportunityLineItem.PricebookEntryID = pricebookEntry.Id;
        OpportunityLineItem.TotalPrice = opportunityLineItem.Quantity * pricebookEntry.UnitPrice;
        insert opportunityLineItem;


        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
            caseRecord.Status = 'New';
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CustomerCompensation').getRecordTypeId();
            caseList.add(caseRecord);
        }
        insert caseList;

        wrts_prcgvr__Activity__c activity =new wrts_prcgvr__Activity__c();
        // activity.FieldsTemplate__c = fieldsTemplate.Id;
        activity.DocumentBundle__c = bundle.Id;
        activity.Type__c = 'Dossier Retention Electric';
        // activity.wrts_prcgvr__Status__c = 'Product change initiated';
        activity.wrts_prcgvr__ObjectId__c = dossier.Id;
        activity.Case__c = caseList[0].Id;
        insert activity;


    }


    @IsTest
    public static void isBusinessClientTest(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => acc.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'isBusinessClient', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isValid') ==true );

        // Exception
        inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'isBusinessClient', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isValid') ==false );
        Test.stopTest();
    }

    @IsTest
    public static void initializeTest(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        wrts_prcgvr__Activity__c activity = [ SELECT Id FROM wrts_prcgvr__Activity__c LIMIT 1];
        Interaction__c interaction = [ SELECT Id FROM Interaction__c LIMIT 1];

        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => acc.Id,
                'opportunityId' => '',
                'interactionId' => interaction.Id,
                'activityId' => activity.Id,
                'dossierId' => ''
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        // Exception
        inputJSON = new Map<String, String>{
                'accountId' => acc.Id,
                'opportunityId' => opp.Id,
                'interactionId' => interaction.Id,
                'activityId' => activity.Id,
                'dossierId' => ''
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'initialize', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );
        Test.stopTest();
    }

    @IsTest
    public static void setChannelAndOriginTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'origin' => 'Email',
                'channel' => 'Back Office Sales',
                'dossierId' => dossier.Id
        };
        Test.startTest();
        TestUtils.exec('MRO_LC_ProductChangeWizard', 'setChannelAndOrigin', inputJSON, true);
        Test.stopTest();

    }

    @IsTest
    public static void setSubProcessAndExpirationDateTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'expirationDate' => JSON.serialize(System.today()),
                'dossierId' => dossier.Id,
                'clear' => 'true'
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'setSubProcessAndExpirationDate', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        inputJSON = new Map<String, String>{
                'opportunityId' => 'test',
                'expirationDate' => JSON.serialize(System.today()),
                'dossierId' => dossier.Id
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'setSubProcessAndExpirationDate', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void updateSubProcessTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'dossierId' => dossier.Id
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'updateSubProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        inputJSON = new Map<String, String>{
                'opportunityId' => 'test',
                'dossierId' => dossier.Id
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'updateSubProcess', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void commodityChangeActionsTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'dossierId' => dossier.Id,
                'clear' => 'true',
                'commodity'=>'Electric'
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'commodityChangeActions', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        inputJSON = new Map<String, String>{
                'opportunityId' => 'test',
                'dossierId' => dossier.Id,
                'commodity'=>'Electric'
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'commodityChangeActions', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void updateCommodityToDossierTest(){
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'dossierId' => dossier.Id,
                'commodity'=>'Electric'
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'updateCommodityToDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        inputJSON = new Map<String, String>{
                'dossierId' => 'test',
                'commodity'=>'Electric'
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'updateCommodityToDossier', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void getSuppliesTest(){
        List<Case> listCases = [SELECT Id FROM Case LIMIT 5];
        List<Id> listIds = new List<Id>();
        for (Case c: listCases){
            listIds.add(c.Id);
        }
        Map<String, String > inputJSON = new Map<String, String>{
                'selectedSupplyIds' => JSON.serialize(listIds)
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'getSupplies', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        List<Id> listIdsError = new List<Id>();
        inputJSON = new Map<String, String>{
                'selectedSupplyIds' => JSON.serialize(listIdsError)
        };
        TestUtils.exec('MRO_LC_ProductChangeWizard', 'getSupplies', inputJSON, false);
        Test.stopTest();
    }

    @IsTest
    public static void createOsisTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        List<Case> listCases = [SELECT Id FROM Case LIMIT 5];
        Map<String, String > inputJSON = new Map<String, String>{
                'supplies' => JSON.serialize(listCases),
                'opportunityId' => opp.Id,
                'subProcess' => 'ChangeMarketFromFreeToRegulated',
                'channel' => 'EnelEasy',
                'commodity'=>'Electric'
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'createOsis', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        //shopAndSalesChannels

        inputJSON = new Map<String, String>{
                'supplies' => JSON.serialize(listCases),
                'opportunityId' => opp.Id,
                'subProcess' => 'ChangeMarketFromFreeToRegulated',
                'channel' => 'MagazinEnelPartner',
                'commodity'=>'Electric'
        };
        response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'createOsis', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        //partnerAndCallCenterChannels
        inputJSON = new Map<String, String>{
                'supplies' => JSON.serialize(listCases),
                'opportunityId' => opp.Id,
                'subProcess' => 'ChangeMarketFromFreeToRegulated',
                'channel' => 'IndirectSales',
                'commodity'=>'Electric'
        };
        response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'createOsis', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );
        Test.stopTest();
    }

    @IsTest
    public static void checkOsisListTet(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'subProcess' => 'ChangeMarketFromFreeToRegulated'
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'checkOsisList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        //ChangeProductWithinTheSameMarket
        inputJSON = new Map<String, String>{
                'opportunityId' => 'test',
                'subProcess' => 'ChangeProductWithinTheSameMarket'
        };
        response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'checkOsisList', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void setRequestedStartDateTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'requestedStartDate' => JSON.serialize(System.today())
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'setRequestedStartDate', inputJSON, true);
        Boolean result = (Boolean) response;
        System.assertEquals(true, result==true );
        Test.stopTest();
    }

    @IsTest
    public static void isNotWorkingDateTest(){
        Map<String, String > inputJSON = new Map<String, String>{
                'requestedStartDate' => JSON.serialize(System.today())
        };
        Test.startTest();
        TestUtils.exec('MRO_LC_ProductChangeWizard', 'isNotWorkingDate', inputJSON, true);

        Test.stopTest();
    }

    @IsTest
    public static void saveCompanyDivisionTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        CompanyDivision__c companyDivision = [SELECT ID FROM CompanyDivision__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'dossierId' => dossier.Id,
                'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response =TestUtils.exec('MRO_LC_ProductChangeWizard', 'saveCompanyDivision', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        inputJSON = new Map<String, String>{
                'opportunityId' => 'test',
                'dossierId' => dossier.Id
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'saveCompanyDivision', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    static void checkOsiTest(){
        OpportunityServiceItem__c osi = [
                SELECT Id
                FROM OpportunityServiceItem__c
                LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
                'osiId' => osi.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'checkOsi', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error') ==false );
        Test.stopTest();
    }

    @IsTest
    public static void getOsiListTest(){
        List<OpportunityServiceItem__c> listOsis = [SELECT Id FROM OpportunityServiceItem__c LIMIT 5];
        List<String> osiIds = new List<String>();
        for (OpportunityServiceItem__c osi: listOsis){
            osiIds.add(osi.Id);
        }
        MRO_LC_ProductChangeWizard.InputData inputData = new MRO_LC_ProductChangeWizard.InputData();
        inputData.osiIds = osiIds;

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'getOsiList', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        //inputData = new MRO_LC_ProductChangeWizard.InputData();
        inputData.osiIds = null;

        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'getOsiList', inputData, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();

    }

    @IsTest
    public static void updateOsiListTest(){
        List<OpportunityServiceItem__c> listOsis = [SELECT Id FROM OpportunityServiceItem__c LIMIT 5];
        MRO_LC_ProductChangeWizard.InputData inputData = new MRO_LC_ProductChangeWizard.InputData();
        inputData.opportunityServiceItems = listOsis;
        Test.startTest();
        TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'updateOsiList', inputData, true);
        Test.stopTest();
    }

    @IsTest
    public static void getContractAccountDataTest(){
        ContractAccount__c  contractAccount = [SELECT Id FROM ContractAccount__c LIMIT 1];
        Map<String, String > inputJSON = new Map<String, String>{
                'contractAccountId' => contractAccount.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'getContractAccountData', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isValid') ==true );

        //Exception
        inputJSON = new Map<String, String>{
                'contractAccountId' => ''
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'getContractAccountData', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('isValid') ==false );
        Test.stopTest();
    }

    @IsTest
    public static void updateContractInformationOnOpportunityTest(){
        List<OpportunityServiceItem__c> listOsis = [SELECT Id FROM OpportunityServiceItem__c LIMIT 5];
        Opportunity opp =[SELECT Id FROM Opportunity LIMIT 1];
        Contract contract = [SELECT Id, ContractType__c, Name  FROM Contract LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];

        MRO_LC_ProductChangeWizard.ContractInputData inputData = new MRO_LC_ProductChangeWizard.ContractInputData();
        inputData.opportunityServiceItems = listOsis;
        inputData.salesChannel ='Indirect Sales';
        inputData.opportunityId = opp.Id;
        inputData.contractId = contract.Id;
        inputData.contractType = contract.ContractType__c;
        inputData.contractName = contract.Name;
        //
        Test.startTest();
        TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'updateContractInformationOnOpportunity', inputData, true);

        //Exception
        inputData.salesman ='errorId';
        Object response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'updateContractInformationOnOpportunity', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(false, result.get('error') ==false );
        Test.stopTest();
    }

    @IsTest
    public static void updateOpportunityTest(){
        Opportunity opp =[SELECT Id FROM Opportunity LIMIT 1];
        Contract contract = [SELECT Id, ContractType__c, Name  FROM Contract LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];

        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'contractId' => contract.Id,
                'dossierId' => dossier.Id,
                'stage' =>'Quoted',
                'privacyChangeId'=>''
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'updateOpportunity', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        //Exception
        inputJSON = new Map<String, String>{
                'opportunityId' =>'errorId',
                'contractId' =>'errorId',
                'dossierId' => 'errorId',
                'stage' =>'Quoted',
                'privacyChangeId'=>''
        };
        response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'updateOpportunity', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void linkOliToOsiTest(){
        List<OpportunityServiceItem__c> listOsis = [SELECT Id FROM OpportunityServiceItem__c LIMIT 5];
        OpportunityLineItem oli = [SELECT Id FROM OpportunityLineItem LIMIT 1];
        // listOsis[0].RecordType.DeveloperName = 'Gas';
        update listOsis;
        MRO_LC_ProductChangeWizard.InputData inputData = new MRO_LC_ProductChangeWizard.InputData();
        inputData.opportunityServiceItems = listOsis;
        inputData.oli = oli;

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'linkOliToOsi', inputData, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );

        //Exception
        response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'linkOliToOsi', null, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();

    }

    @IsTest
    public static void deleteOlisTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'deleteOlis', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );


        //Exception
        inputJSON = new Map<String, String>{
                'opportunityId' => 'tester'
        };
        response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'deleteOlis', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void deleteOsiTest(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        OpportunityServiceItem__c Osi = [SELECT Id FROM OpportunityServiceItem__c LIMIT 1];

        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => opp.Id,
                'dossierId' => dossier.Id,
                'osiId' =>osi.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'deleteOsi', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );


        //Exception
        response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'deleteOsi', null, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void insertConsumptionListTest(){
        List<Consumption__c> consumptionList = [
                SELECT Id, Name
                FROM Consumption__c
                LIMIT  1
        ];
        Contract contractRecord = [
                SELECT Id, ContractType__c, Name
                FROM Contract
                LIMIT 1
        ];

        MRO_LC_ProductChangeWizard.ConsumptionDetails inputData = new MRO_LC_ProductChangeWizard.ConsumptionDetails();
        inputData.consumptionList = consumptionList ;
        inputData.contractId = contractRecord.Id ;
        inputData.consumptionConventions = true ;

        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ProductChangeWizard', 'insertConsumptionList', inputData, true);
        Test.stopTest();
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('consumptionList') != null);
    }

    @IsTest
    public static void getMetersTest(){

        OpportunityServiceItem__c osi = [SELECT Id, Opportunity__c  FROM OpportunityServiceItem__c LIMIT 1];
        Index__c index = MRO_UTL_TestDataFactory.index().createIndex().build();
        index.OpportunityServiceItem__c = osi.Id;
        index.MeterNumber__c = '12334FF';
        index.ReadingDate__c = System.today();
        insert  index;

        Map<String, String > inputJSON = new Map<String, String>{
                'opportunityId' => osi.Opportunity__c
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'getMeters', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==false );


        //Exception
        inputJSON = new Map<String, String>{
                'opportunityId' => 'tester'
        };
        response = TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'getMeters', inputJSON, true);
        result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') ==true );
        Test.stopTest();
    }

    @IsTest
    public static void createIndexTest(){
        Opportunity opp = [SELECT Id  FROM Opportunity LIMIT 1];
        Account acc = [SELECT Id  FROM Account LIMIT 1];
        Dossier__c dossier = [SELECT Id FROM Dossier__c LIMIT 1];
        List<MRO_SRV_Index.MeterQuadrant> listQuadrants = new List<MRO_SRV_Index.MeterQuadrant>();
        MRO_SRV_Index.MeterQuadrant  quadrant = new MRO_SRV_Index.MeterQuadrant();
        quadrant.quadrantName = 'Energie Activa - Rata Unica';
        quadrant.index = 5000;
        quadrant.meterNumber='5';
        listQuadrants.add(quadrant);
        List<List<MRO_SRV_Index.MeterQuadrant>> listOfLisMeterQuadrants = new List<List<MRO_SRV_Index.MeterQuadrant>>();
        listOfLisMeterQuadrants.add(listQuadrants);
        MRO_LC_ProductChangeWizard.MetersDetails masterDetails = new MRO_LC_ProductChangeWizard.MetersDetails();
        masterDetails.accountId = acc.Id;
        masterDetails.opportunityId = opp.Id;
        masterDetails.dossierId =dossier.Id;
        masterDetails.selfReadingProvided = true;
        masterDetails.skipMetersValidation = true;
        masterDetails.metersInfo = listOfLisMeterQuadrants;
        Test.startTest();
        TestUtils.exec(
                'MRO_LC_ProductChangeWizard', 'createIndex', masterDetails, true);


        Test.stopTest();

    }
}