/**
 * Created by Boubacar Sow on 18/12/2019.
 * @author Bouabacr Sow
 * @date  18/12/2019
 * @description Class test for the MRO_LC_TechnicalDataChangeWizard Controller.
 *              [ENLCRO-353] Power Voltage Change  Implementation Wizard.
 * 
 */
@IsTest
public with sharing class MRO_LC_TechnicalDataChangeWizardTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        //businessAccount.VATNumber__c= '1343324';
        businessAccount.Key__c = '1324433';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().businessAccount().build();
        accountTrader.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Trader').getRecordTypeId();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;
        /*BillingProfile__c billingProfile = MRO_UTL_TestDataFactory.BillingProfileBuilder().createBillingProfile().build();
        billingProfile.Account__c=listAccount[0].Id;
        insert billingProfile;*/
        for(Integer i = 0; i < 20; i++) {
            Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
            supply.Contract__c=contract.Id;
            supplyList.add(supply);
        }
        insert supplyList;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supplyList[0].Id;
           // caseRecord.BillingProfile__c = billingProfile.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseList.add(caseRecord);
        }
        insert caseList;
    }

    @IsTest
    public static void InitializeTechnicalDataChangeTest(){
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
                SELECT Id
                FROM CustomerInteraction__c
                LIMIT 1
        ];

        Dossier__c dossier = [
                SELECT Id
                FROM Dossier__c
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT Id,Name
                FROM CompanyDivision__c
                WHERE Name = 'ENEL 1'
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => dossier.Id,
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TechnicalDataChangeWizard', 'InitializeTechnicalDataChange', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
                'accountId' => account.Id,
                'dossierId' => '',
                'interactionId' => customerInteraction.Id,
                'companyDivisionId' => companyDivision.Id
        };
        response = TestUtils.exec(
                'MRO_LC_TechnicalDataChangeWizard', 'InitializeTechnicalDataChange', inputJSON, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }
    @IsTest
    public static void InitializeTechnicalDataChangeExceptionTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'MRO_LC_TechnicalDataChangeWizard', 'InitializeTechnicalDataChange', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }
    @IsTest
    public static void updateCaseListTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                    AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TechnicalDataChangeWizard', 'updateCaseList', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    public static void cancelProcessTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,
                    AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TechnicalDataChangeWizard', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    public static void getSelectedSypplyTest() {
        Supply__c supply = [
                SELECT Id
                FROM Supply__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'supplyId' => supply.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_TechnicalDataChangeWizard', 'getSelectedSypply', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }
    @IsTest
    public static void getSelectedSypplyIsBlankTest() {
        Map<String, String > inputJSON = new Map<String, String>{
            'supplyId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'MRO_LC_TechnicalDataChangeWizard', 'getSelectedSypply', inputJSON, false);
            Map<String, Object> result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true, 'Erreur found' );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }

        Test.stopTest();
    }
    @IsTest
    public static void getSelectedSypplyExceptionTest() {
        Supply__c supply = [
                SELECT Id
                FROM Supply__c
                LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
                'supplyId' => supply.Id+'124KJFD'
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec(
                    'MRO_LC_TechnicalDataChangeWizard', 'getSelectedSypply', inputJSON, true);
            Map<String, Object> result = (Map<String, Object>) response;
            system.assertEquals(true, result.get('error') == true, 'Erreur found' );
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }

        Test.stopTest();
    }
    @IsTest
    private static void updateDossierTest() {
        String dossierId = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ].Id;
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossierId,
            'channelSelected' => 'Back Office Sales',
            'originSelected' => 'Internal'
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TechnicalDataChangeWizard', 'UpdateDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void updateCommodityToDossierTest(){
        Dossier__c dossier = [
            SELECT Id, Commodity__c
            FROM Dossier__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'dossierId' => dossier.Id,
            'commodity' => dossier.Commodity__c
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_TechnicalDataChangeWizard', 'updateCommodityToDossier', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }

}