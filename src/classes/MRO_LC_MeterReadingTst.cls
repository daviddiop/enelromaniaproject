/**
 * MRO_LC_MeterReading
 *
 * @author  INSA BADJI
 * @version 1.0
 * @description Update Cases with
 *              [ENLCRO-116] Meter Reading #WP3-P1
 * 05/11/2019: INSA - Original
 */

@IsTest
public with sharing class MRO_LC_MeterReadingTst {
    @TestSetup
    static void setup(){
        wrts_prcgvr.InstallIntegration.install();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingCase();
        insert MRO_UTL_TestDataFactory.createPhaseManagerSettingDossier();
        insert MRO_UTL_TestDataFactory.createPhase('RE010');
        insert MRO_UTL_TestDataFactory.createPhase('BL010');
        insert MRO_UTL_TestDataFactory.createPhase('RC010');

        String recordTypeEle = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Meter_Reading_Ele').getRecordTypeId();
        SObject phaseRE010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('RE010', 'RC010', 'Meter_Reading_Ele', recordTypeEle, 'Cancellation');
        insert phaseRE010ToRC010;

        SObject phaseDI010ToRC010 = MRO_UTL_TestDataFactory.createPhaseTransition('BL010', 'RC010', 'Meter_Reading_Ele', recordTypeEle, '');
        insert phaseDI010ToRC010;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;


        List<Supply__c> supplyList = new List<Supply__c>();
        List<Account> listAccount = new list<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        Contact contact = MRO_UTL_TestDataFactory.contact().createContact().build();
        contact.AccountId = listAccount[1].Id;
        insert contact;
        Interaction__c interaction = MRO_UTL_TestDataFactory.interaction().createInteraction().build();
        insert interaction;
        CustomerInteraction__c customerInteraction = MRO_UTL_TestDataFactory.CustomerInteraction().createCustomerInteraction(interaction.Id, listAccount[0].Id, contact.Id).build();
        insert customerInteraction;
        Account accountTrader = MRO_UTL_TestDataFactory.account().traderAccount().build();
        insert accountTrader;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        insert servicePoint;
        Contract contract = MRO_UTL_TestDataFactory.contract().createContract().build();
        contract.AccountId = listAccount[0].Id;
        insert contract;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(companyDivision.Id).build();
        supply.Contract__c=contract.Id;
        insert supply;
        Dossier__c dossier = MRO_UTL_TestDataFactory.dossier().setCompany(companyDivision.Id).build();
        dossier.Account__c = listAccount[0].Id;
        insert dossier;

        for (Integer i = 0; i < 10; i++) {
            Case caseRecord = MRO_UTL_TestDataFactory.caseRecordBuilder().createCaseBuilder().setCompany(companyDivision.Id).build();
            caseRecord.AccountId = listAccount[1].Id;
            caseRecord.Supply__c = supply.Id;
            caseRecord.Trader__c = accountTrader.Id;
            caseRecord.Dossier__c = dossier.Id;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Meter_Reading_Ele').getRecordTypeId();
            caseList.add(caseRecord);
        }
        System.debug('******* list case '+caseList);
        insert caseList;
        ScriptTemplate__c scriptTemplate = MRO_UTL_TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        scriptTemplate.Code__c= 'MeterReadingCodeTemplate_1';
        insert scriptTemplate;
    }

    @IsTest
    public static void insertCaseTest(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        List<Supply__c> supply = [
            SELECT Id,Name,CompanyDivision__c
            FROM Supply__c
            LIMIT 1
        ];
        String supplies = JSON.serialize(supply);
        String invoiceIds = '["1000000","1000001","1000002","1000003"]';
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'supply' => supplies,
            'invoiceIds' => invoiceIds
        };
        Map<String, String > inputJSONNonIdDossier = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => '',
            'supply' => supplies,
            'invoiceIds' => invoiceIds
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterReading', 'InsertCase', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false,'Error is found');


        Object response1 = TestUtils.exec(
            'MRO_LC_MeterReading', 'InsertCase', inputJSONNonIdDossier, true);
        Map<String, Object> result1 = (Map<String, Object>) response1;
        system.assertEquals(true, result1.get('error') == false,'Error is found');
        Test.stopTest();
    }


    @IsTest
    public static void InitializeMeterReadingTst(){
        Account account = [
            SELECT Id,Name
            FROM Account
            LIMIT 1
        ];
        CustomerInteraction__c customerInteraction = [
            SELECT Id
            FROM CustomerInteraction__c
            LIMIT 1
        ];

        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        ScriptTemplate__c scriptTemplate = [
            SELECT Id
            FROM ScriptTemplate__c
            LIMIT 1
        ];

        Test.startTest();
        Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id,
            'dossierId' => dossier.Id,
            'interactionId' => customerInteraction.Id,
            'templateId' => scriptTemplate.Id
        };
        Object response = TestUtils.exec(
            'MRO_LC_MeterReading', 'Initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );

        inputJSON = new Map<String, String>{
            'accountId' => '',
            'dossierId' => '',
            'interactionId' => customerInteraction.Id
        };
        response = TestUtils.exec(
            'MRO_LC_MeterReading', 'Initialize', inputJSON, false);
        System.debug('response empty daata '+response);
        System.assertEquals(true, result.get('error') == false );

        Test.stopTest();
    }
    @IsTest
    private static void CancelProcessTest() {

        List<Case> caseList = [
            SELECT Id,Supply__c,EffectiveDate__c,Reason__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'dossierId' => dossier.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterReading', 'CancelProcess', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false );
        Test.stopTest();
    }

    @IsTest
    private static void getSupplyRecordTest(){
        Supply__c supply = [
            SELECT Id
            FROM Supply__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'supplyId' => supply.Id
        };

        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterReading', 'getSupplyRecord', inputJSON, true);
        System.debug('*** '+response);
        Map<String, Object> result = (Map<String, Object>) response;
        //system.assertNotEquals(result.size(),0);
        Test.stopTest();
    }

    @IsTest
    private static void updateDossierTest() {
        List<Case> caseList = [
            SELECT Id,Supply__c,StartDate__c,EndDate__c,AccountId, RecordTypeId,CompanyDivision__c
            FROM Case
            LIMIT 1
        ];
        List<Supply__c> listSupplies = [
            SELECT Id, CompanyDivision__c,ServicePoint__c
            FROM Supply__c
            LIMIT 1
        ];
        Dossier__c dossier = [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        String caseListString = JSON.serialize(caseList);
        String listSuppliesString = JSON.serialize(listSupplies);
        Map<String, String > inputJSON = new Map<String, String>{
            'oldCaseList' => caseListString,
            'startDate' => '2020-01-27',
            'endDate' => '2020-01-31',
            'sLAExpirationDate' => '2020-01-31',
            'dossierId' => dossier.Id,
            'note' => 'note',
            'selectedOrigin' => 'Internal',
            'channelSelected' => 'Back Office Sales'
        };
        Test.startTest();
        Object response = TestUtils.exec(
            'MRO_LC_MeterReading', 'UpdateDossier', inputJSON, true);
        System.debug('tester ****** '+response);
        Map<String, Object> result = (Map<String, Object>) response;

        System.assertEquals(true, result.get('error') == false );
        Test.stopTest();

    }
}