/**
 * Created by goudiaby on 26/08/2019.
 */

public with sharing class CancellationRequestService {
    private static CancellationRequestQueries cancellationRequestQuery = CancellationRequestQueries.getInstance();

    public static CancellationRequestService getInstance() {
        return new CancellationRequestService();
    }

    public List<ApexServiceLibraryCnt.Option> getListCancellationReasons(String objectName, String objectRecordType, String originStage) {
        List<ApexServiceLibraryCnt.Option> cancellationReasonsOptions = new List<ApexServiceLibraryCnt.Option>();
        CancellationRequest__c cancellationRequest = cancellationRequestQuery.findCancellationReasons(objectName, objectRecordType, originStage);
        String cancellationReasons = cancellationRequest.CancellationReasons__c;
        if (String.isNotBlank(cancellationReasons)) {
            for (String reason : cancellationReasons.split(';')) {
                cancellationReasonsOptions.add(new ApexServiceLibraryCnt.Option(reason, reason));
            }
        }
        return cancellationReasonsOptions;
    }

}