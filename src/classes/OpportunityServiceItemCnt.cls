/**
 * Created by ferhati on 11/02/2019.
 */

public with sharing class OpportunityServiceItemCnt extends ApexServiceLibraryCnt {

    static OpportunityServiceItemQueries osiQueries = OpportunityServiceItemQueries.getInstance();
    static ServicePointQueries spQueries = ServicePointQueries.getInstance();
    static ServiceSiteQueries servSiteQueries = ServiceSiteQueries.getInstance();
    static List<ApexServiceLibraryCnt.Option> serviceSitePicklistValues  = new List<ApexServiceLibraryCnt.Option>();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    @AuraEnabled
    public static Map<String, Object> initialize(String supplyId, String servicePointId, String servicePointCode, String opportunityId, String accountId) {
        OpportunityServiceItem__c osi = new OpportunityServiceItem__c();
        AddressCnt.AddressDTO addressFromServicePoint;
        Map<String, Object> response = new Map<String, Object>();
        osi.Account__c = accountId;
        osi.Opportunity__c = opportunityId;

        if(String.isBlank(supplyId) && String.isBlank(servicePointId)) {
            osi.ServicePointCode__c = servicePointCode;
        }else if(String.isNotBlank(supplyId)) {
            Supply__c supply = [SELECT Id, ServicePoint__c, ServicePoint__r.Name, ServicePoint__r.Distributor__c, ServicePoint__r.Trader__c, ServicePoint__r.AvailablePower__c,
                    ServicePoint__r.ContractualPower__c, ServicePoint__r.EstimatedConsumption__c, ServicePoint__r.PowerPhase__c, ServicePoint__r.ConversionFactor__c,
                    ServicePoint__r.Pressure__c, ServicePoint__r.PressureLevel__c, ServicePoint__r.Voltage__c, ServicePoint__r.VoltageLevel__c,
                    Market__c, NonDisconnectable__c, RecordTypeId, RecordType.DeveloperName, ServicePoint__r.PointStreetNumber__c,
                    ServicePoint__r.PointStreetNumberExtn__c, ServicePoint__r.PointStreetName__c, ServicePoint__r.PointCity__c,
                    ServicePoint__r.PointCountry__c, ServicePoint__r.PointPostalCode__c, ServicePoint__r.PointStreetType__c,
                    ServicePoint__r.PointApartment__c, ServicePoint__r.PointBuilding__c, ServicePoint__r.PointLocality__c,
                    ServicePoint__r.PointProvince__c, ServicePoint__r.PointFloor__c, ServicePoint__r.PointAddressNormalized__c, ServiceSite__c
                        FROM Supply__c
                        WHERE Id = :supplyId];
            osi.RecordTypeId = supply.RecordType.DeveloperName == 'Electric' ? Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId() : Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
            //osi.RecordType.DeveloperName = supply.RecordType.DeveloperName;
            osi.ServicePointCode__c = supply.ServicePoint__r.Name;
            osi.ServicePoint__c = Supply.ServicePoint__c;
            osi.Distributor__c = supply.ServicePoint__r.Distributor__c;
            osi.Trader__c = supply.ServicePoint__r.Trader__c;
            osi.AvailablePower__c = supply.ServicePoint__r.AvailablePower__c;
            osi.ContractualPower__c = supply.ServicePoint__r.ContractualPower__c;
            osi.EstimatedConsumption__c = supply.ServicePoint__r.EstimatedConsumption__c;
            osi.PowerPhase__c = supply.ServicePoint__r.PowerPhase__c;
            osi.ConversionFactor__c = supply.ServicePoint__r.ConversionFactor__c;
            osi.Pressure__c = supply.ServicePoint__r.Pressure__c;
            osi.PressureLevel__c = supply.ServicePoint__r.PressureLevel__c;
            osi.Voltage__c = supply.ServicePoint__r.Voltage__c;
            osi.VoltageLevel__c = supply.ServicePoint__r.VoltageLevel__c;
            osi.Market__c = supply.Market__c;
            osi.NonDisconnectable__c = supply.NonDisconnectable__c;
            osi.PointStreetNumber__c = supply.ServicePoint__r.PointStreetNumber__c;
            osi.PointStreetNumberExtn__c = supply.ServicePoint__r.PointStreetNumberExtn__c;
            osi.PointStreetName__c = supply.ServicePoint__r.PointStreetName__c;
            osi.PointCity__c = supply.ServicePoint__r.PointCity__c;
            osi.PointCountry__c = supply.ServicePoint__r.PointCountry__c;
            osi.PointPostalCode__c = supply.ServicePoint__r.PointPostalCode__c;
            osi.PointStreetType__c = supply.ServicePoint__r.PointStreetType__c;
            osi.PointApartment__c = supply.ServicePoint__r.PointApartment__c;
            osi.PointBuilding__c = supply.ServicePoint__r.PointBuilding__c;
            osi.PointLocality__c = supply.ServicePoint__r.PointLocality__c;
            osi.PointProvince__c = supply.ServicePoint__r.PointProvince__c;
            osi.PointFloor__c = supply.ServicePoint__r.PointFloor__c;
            osi.PointAddressNormalized__c = supply.ServicePoint__r.PointAddressNormalized__c;
            osi.ServiceSite__c = supply.ServiceSite__c;
            addressFromServicePoint = OpportunityServiceItemCnt.copyServicePointAddressToDTO(supply.ServicePoint__r);
        } else if(String.isNotBlank(servicePointId)) {
            ServicePoint__c point = [SELECT Id, Name, Distributor__c, Trader__c, AvailablePower__c, ContractualPower__c,
                        EstimatedConsumption__c, PowerPhase__c, ConversionFactor__c, Pressure__c, PressureLevel__c, Voltage__c, VoltageLevel__c,
                        CurrentSupply__r.Market__c, CurrentSupply__r.NonDisconnectable__c, RecordTypeId, RecordType.DeveloperName,
                        PointStreetNumber__c, PointStreetNumberExtn__c, PointStreetName__c, PointCity__c, PointCountry__c,
                        PointPostalCode__c, PointStreetType__c, PointApartment__c, PointBuilding__c, PointLocality__c,
                        PointProvince__c, PointFloor__c, PointAddressNormalized__c, CurrentSupply__r.ServiceSite__c
                        FROM ServicePoint__c
                        WHERE Id = :servicePointId];
            osi.RecordTypeId = point.RecordType.DeveloperName == 'Electric' ? Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get('Electric').getRecordTypeId() : Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
            //osi.RecordType.DeveloperName = point.RecordType.DeveloperName;
            osi.ServicePointCode__c = point.Name;
            osi.ServicePoint__c = point.Id;
            osi.Distributor__c = point.Distributor__c;
            osi.Trader__c = point.Trader__c;
            osi.AvailablePower__c = point.AvailablePower__c;
            osi.ContractualPower__c = point.ContractualPower__c;
            osi.EstimatedConsumption__c = point.EstimatedConsumption__c;
            osi.PowerPhase__c = point.PowerPhase__c;
            osi.ConversionFactor__c = point.ConversionFactor__c;
            osi.Pressure__c = point.Pressure__c;
            osi.PressureLevel__c = point.PressureLevel__c;
            osi.Voltage__c = point.Voltage__c;
            osi.VoltageLevel__c = point.VoltageLevel__c;
            osi.Market__c = point.CurrentSupply__r.Market__c;
            osi.NonDisconnectable__c = point.CurrentSupply__r.NonDisconnectable__c;
            osi.PointStreetNumber__c = point.PointStreetNumber__c;
            osi.PointStreetNumberExtn__c = point.PointStreetNumberExtn__c;
            osi.PointStreetName__c = point.PointStreetName__c;
            osi.PointCity__c = point.PointCity__c;
            osi.PointCountry__c = point.PointCountry__c;
            osi.PointPostalCode__c = point.PointPostalCode__c;
            osi.PointStreetType__c = point.PointStreetType__c;
            osi.PointApartment__c = point.PointApartment__c;
            osi.PointBuilding__c = point.PointBuilding__c;
            osi.PointLocality__c = point.PointLocality__c;
            osi.PointProvince__c = point.PointProvince__c;
            osi.PointFloor__c = point.PointFloor__c;
            osi.PointAddressNormalized__c = point.PointAddressNormalized__c;
            osi.ServiceSite__c = point.CurrentSupply__r.ServiceSite__c;
            addressFromServicePoint = OpportunityServiceItemCnt.copyServicePointAddressToDTO(point);
        }
        response.put('osi', osi);
        if(String.isNotBlank(osi.RecordTypeId)) {
            response.put('recordType', Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosById().get(osi.RecordTypeId).DeveloperName);
        }
        response.put('osiAddress', addressFromServicePoint);
        return response;
    }


    public class getExtraFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String extraFieldsFieldSet = params.get('extraFieldsFieldSet');
            response = LightningUtils.getFieldSet('OpportunityServiceItem__c', extraFieldsFieldSet);
            return  response;
        }
    }

    public class getPointCodeFields extends AuraCallable{
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String servicePointId = params.get('servicePointId');

            if (String.isBlank(servicePointId)) {
                throw new WrtsException(System.Label.ServicePoint+ ' - ' + System.Label.MissingId);
            }
            try {
                response.put('servicePointNotFound', true);

                ServicePoint__c servicePoint = spQueries.getById(servicePointId);
                AddressCnt.AddressDTO addressFromServicePoint = OpportunityServiceItemCnt.copyServicePointAddressToDTO(servicePoint);

                response.put('servicePointNotFound', (servicePoint == null));
                WrapperServicePoint WrapServicePoint = new WrapperServicePoint(servicePoint);
                response.put('servicePoint', WrapServicePoint);
                response.put('servicePointAddress', addressFromServicePoint);

                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;

        }

    }

    public class getOsiRecordTypeDevName extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('recordId');

            if (String.isBlank(recordId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem+ ' - ' + System.Label.MissingId);
            }

            try {
                OpportunityServiceItem__c osi = osiQueries.getById(recordId);
                response.put('error', false);
                response.put('osiRecordTypeDevName', osi.RecordType.DeveloperName);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public class getListOSIServiceSite extends AuraCallable{
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            List<String> osiAddress = new List<String>();
            List<String> sitesAddress = new List<String>();
            serviceSitePicklistValues = new List<ApexServiceLibraryCnt.Option>();
            String accountId = params.get('accountId');
            String recordId = params.get('recordId');
            String serviceSiteField;
            String siteAddressKeyField;

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.ServicePoint+ ' - ' + System.Label.MissingId);
            }
            try {
                if (String.isNotBlank(recordId)) {
                    OpportunityServiceItem__c osi = osiQueries.getById(recordId);
                    serviceSiteField = osi.ServiceSite__c;
                    siteAddressKeyField = osi.SiteAddressKey__c;
                }

                List<ServiceSite__c> serviceSite = servSiteQueries.getListServiceSite(accountId);
                List<OpportunityServiceItem__c> siteAddress = new List<OpportunityServiceItem__c>();
                List<AggregateResult> siteAddressAggregateResult = osiQueries.getListOSIServiceSite(accountId);
                Map<Id, ServiceSite__c> allSS = new Map<Id, ServiceSite__c>();
                Map<Id, OpportunityServiceItem__c> allOSI = new Map<Id, OpportunityServiceItem__c>();
                for(AggregateResult aggResult : siteAddressAggregateResult){
                    OpportunityServiceItem__c osi = new OpportunityServiceItem__c(
                            SiteAddressKey__c = aggResult.get('SiteAddressKey__c') != null ? (String)aggResult.get('SiteAddressKey__c'): null,
                            Id = aggResult.get('Id') != null ? (ID)aggResult.get('Id'): null,
                            ServiceSite__c = aggResult.get('ServiceSite__c') != null ? (ID)aggResult.get('ServiceSite__c'): null,
                            SiteStreetType__c = aggResult.get('SiteStreetType__c') != null ? (String)aggResult.get('SiteStreetType__c'): null,
                            SiteApartment__c = aggResult.get('SiteApartment__c') != null ? (String)aggResult.get('SiteApartment__c'): null,
                            SiteStreetName__c = aggResult.get('SiteStreetName__c') != null ? (String)aggResult.get('SiteStreetName__c'): null,
                            SiteStreetNumber__c = aggResult.get('SiteStreetNumber__c') != null ? (String)aggResult.get('SiteStreetNumber__c'): null,
                            SiteStreetNumberExtn__c  = aggResult.get('SiteStreetNumberExtn__c') != null ? (String)aggResult.get('SiteStreetNumberExtn__c'):null,
                            SiteCountry__c = aggResult.get('SiteCountry__c') != null ? (String)aggResult.get('SiteCountry__c'): null,
                            SiteCity__c = aggResult.get('SiteCity__c') != null ? (String)aggResult.get('SiteCity__c'): null,
                            SitePostalCode__c = aggResult.get('SitePostalCode__c') != null ? (String)aggResult.get('SitePostalCode__c') : null,
                            SiteProvince__c = aggResult.get('SiteProvince__c') != null ? (String)aggResult.get('SiteProvince__c'): null,
                            SiteBuilding__c = aggResult.get('SiteBuilding__c') != null ? (String)aggResult.get('SiteBuilding__c'): null,
                            SiteFloor__c = aggResult.get('SiteFloor__c') != null ? (String)aggResult.get('SiteFloor__c'): null,
                            SiteLocality__c = aggResult.get('SiteLocality__c') != null ? (String)aggResult.get('SiteLocality__c'): null
                    );
                    siteAddress.add(osi);
                }
                //siteAddress=siteAddressAggregateResult;
                        serviceSitePicklistValues.add(new ApexServiceLibraryCnt.Option(System.Label.CreateNewServiceSite,'new'));

                if(!siteAddress.isEmpty()){
                    for(OpportunityServiceItem__c osi : siteAddress ){
                        if((osi.SiteAddressKey__c != null) && (osi.Id != null)){
                            osiAddress = OpportunityServiceItemCnt.serviceSiteOsiAddress(null,osi);
                            if(recordId == osi.Id ){
                                serviceSitePicklistValues[0].label = String.join(osiAddress, '  ');
                            }else{

                                serviceSitePicklistValues.add(new ApexServiceLibraryCnt.Option(String.join(osiAddress, '  '),osi.SiteAddressKey__c));
                                allOSI.put(osi.SiteAddressKey__c,osi);

                            }
                        }
                    }
                }

                if(!serviceSite.isEmpty()) {
                    for (ServiceSite__c servSite : serviceSite) {
                        if((servSite.SiteAddressKey__c != null) && (servSite.Id != null)) {
                            sitesAddress = OpportunityServiceItemCnt.serviceSiteOsiAddress(servSite, null);
                            serviceSitePicklistValues.add(new ApexServiceLibraryCnt.Option(String.join(sitesAddress, '  '), servSite.Id));
                            allSS.put(servSite.Id, servSite);
                        }
                    }
                }

                response.put('siteAddressKeyField',siteAddressKeyField);
                response.put('serviceSiteField',serviceSiteField);
                response.put('serviceSitePicklistValues', serviceSitePicklistValues);
                response.put('allOSI',allOSI);
                response.put('allSS',allSS);


                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;

        }

    }

    public class getOsiAddressField extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String osiId = params.get('osiId');

            if (String.isBlank(osiId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem+ ' - ' + System.Label.Required);
            }
            try {
                OpportunityServiceItem__c osi = osiQueries.getById(osiId);
                if (osi != null) {
                    AddressCnt.AddressDTO addressFromOsi = OpportunityServiceItemCnt.copyOsiAddressToDTO(osi);
                    response.put('error', false);
                    response.put('osiAddress', addressFromOsi);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }


    public class getServiceSiteAddressField extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String osiId = params.get('osiId');

            if (String.isBlank(osiId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem+ ' - ' + System.Label.Required);
            }
            try {
                OpportunityServiceItem__c osi = osiQueries.getById(osiId);
                if (osi != null) {
                    AddressCnt.AddressDTO addressFromOsi = OpportunityServiceItemCnt.copyServiceSiteAddressToDTO(osi);
                    response.put('error', false);
                    response.put('serviceSiteAddress', addressFromOsi);
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public class getOsiAddress extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String addressId = params.get('addressId');


            if (String.isBlank(addressId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem+ ' - ' + System.Label.Required);
            }
            try {
                OpportunityServiceItem__c osiAddress = osiQueries.getOsiAddress(addressId);
                if(osiAddress != null) {
                    response.put('error', false);
                    response.put('osiAddress', JSON.serialize(osiAddress));
                }
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }


    public class setOsiIdToSiteAddressKey extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String osiId = params.get('osiId');

            if (String.isBlank(osiId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem+ ' - ' + System.Label.Required);
            }
            try {
                OpportunityServiceItem__c osi = osiQueries.getById(osiId);
                if (osi != null) {
                    osi.SiteAddressKey__c = osi.Id;
                    databaseSrv.updateSObject(osi);
                }


            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }

            return response;
        }
    }

    public with sharing class WrapperServicePoint {
        @AuraEnabled
        public String ServicePointCode {get; set;}

        @AuraEnabled
        public String VoltageLevel {get; set;}
        @AuraEnabled
        public String Voltage {get; set;}
        @AuraEnabled
        public String PowerPhase {get; set;}
        @AuraEnabled
        public String ConversionFactor {get; set;}
        @AuraEnabled
        public String PressureLevel {get; set;}
        @AuraEnabled
        public String Pressure {get; set;}
        @AuraEnabled
        public String Account {get; set;}
        @AuraEnabled
        public String Distributor {get; set;}
        @AuraEnabled
        public String AvailablePower {get; set;}
        @AuraEnabled
        public String EstimatedConsumption {get; set;}
        @AuraEnabled
        public String ContractualPower {get; set;}
        @AuraEnabled
        public String Trader {get; set;}
        @AuraEnabled
        public String Address {get; set;}

        public WrapperServicePoint(ServicePoint__c sp) {
            this.ServicePointCode = sp.Code__c;
            this.VoltageLevel = String.valueOf(sp.VoltageLevel__c);
            this.Voltage = String.valueOf(sp.Voltage__c);
            this.PowerPhase = String.valueOf(sp.PowerPhase__c);
            this.ConversionFactor = String.valueOf(sp.ConversionFactor__c);
            this.PressureLevel = String.valueOf(sp.PressureLevel__c);
            this.Pressure = String.valueOf(sp.Pressure__c);
            this.Account = sp.Account__c;
            this.Distributor = sp.Distributor__c;
            this.AvailablePower = String.valueOf(sp.AvailablePower__c);
            this.EstimatedConsumption = String.valueOf(sp.EstimatedConsumption__c);
            this.ContractualPower = String.valueOf(sp.ContractualPower__c);
            this.Trader = sp.Trader__c;

            String TempAddress = '';

            if (String.isNotBlank(sp.PointStreetName__c)) {
                TempAddress = TempAddress + sp.PointStreetName__c;
            }
            if (String.isNotBlank(sp.PointStreetNumber__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointStreetNumber__c;
                if (String.isNotBlank(sp.PointStreetNumberExtn__c)) {
                    TempAddress = TempAddress + sp.PointStreetNumberExtn__c;
                }
            }
            if (String.isNotBlank(sp.PointPostalCode__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointPostalCode__c;
            }
            if (String.isNotBlank(sp.PointCity__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointCity__c;
            }
            if (String.isNotBlank(sp.PointCountry__c)) {
                if (String.isNotBlank(TempAddress)) { TempAddress = TempAddress +', '; }
                TempAddress = TempAddress + sp.PointCountry__c;
            }

            this.Address = TempAddress;
        }
    }
    public class getRecordTypes extends  AuraCallable {
        public override Object perform(final  String jsonInput){
            Map<String,Object> response = new Map<String,Object>();
            try {
                Map<String, Schema.RecordTypeInfo> osiRts = Schema.SObjectType.OpportunityServiceItem__c.getRecordTypeInfosByDeveloperName();
                Schema.RecordTypeInfo electricRt = osiRts.get('Electric');
                Schema.RecordTypeInfo gasRt = osiRts.get('Gas');
                response.put('electricRecordType', new Map<String,String>{'label'=>electricRt.getName(),'value'=>electricRt.getRecordTypeId()});
                response.put('gasRecordType', new Map<String,String>{'label'=>gasRt.getName(),'value'=>gasRt.getRecordTypeId()});

                response.put('ss', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public static AddressCnt.AddressDTO copyServiceSiteAddressToDTO(OpportunityServiceItem__c osi) {
        AddressCnt.AddressDTO addressDTO = new AddressCnt.AddressDTO();
        addressDTO.streetNumber = osi.SiteStreetNumber__c;
        addressDTO.streetNumberExtn = osi.SiteStreetNumberExtn__c;
        addressDTO.streetName = osi.SiteStreetName__c;
        addressDTO.streetType = osi.SiteStreetType__c;
        addressDTO.apartment = osi.SiteApartment__c;
        addressDTO.building = osi.SiteBuilding__c;
        addressDTO.city = osi.SiteCity__c;
        addressDTO.country = osi.SiteCountry__c;
        addressDTO.floor = osi.SiteFloor__c;
        addressDTO.locality = osi.SiteLocality__c;
        addressDTO.postalCode = osi.SitePostalCode__c;
        addressDTO.province = osi.SiteProvince__c;
        addressDTO.addressNormalized = osi.SiteAddressNormalized__c;

        return addressDTO;
    }

    public static AddressCnt.AddressDTO copyOsiAddressToDTO(OpportunityServiceItem__c osi) {
        AddressCnt.AddressDTO addressDTO = new AddressCnt.AddressDTO();
        addressDTO.streetNumber = osi.PointStreetNumber__c;
        addressDTO.streetNumberExtn = osi.PointStreetNumberExtn__c;
        addressDTO.streetName = osi.PointStreetName__c;
        addressDTO.streetType = osi.PointStreetType__c;
        addressDTO.apartment = osi.PointApartment__c;
        addressDTO.building = osi.PointBuilding__c;
        addressDTO.city = osi.PointCity__c;
        addressDTO.country = osi.PointCountry__c;
        addressDTO.floor = osi.PointFloor__c;
        addressDTO.locality = osi.PointLocality__c;
        addressDTO.postalCode = osi.PointPostalCode__c;
        addressDTO.province = osi.PointProvince__c;
        addressDTO.addressNormalized = osi.PointAddressNormalized__c;

        return addressDTO;
    }

    public static AddressCnt.AddressDTO copyServicePointAddressToDTO(ServicePoint__c sp) {
        AddressCnt.AddressDTO addressDTO = new AddressCnt.AddressDTO();
        addressDTO.streetNumber = sp.PointStreetNumber__c;
        addressDTO.streetNumberExtn = sp.PointStreetNumberExtn__c;
        addressDTO.streetName = sp.PointStreetName__c;
        addressDTO.city = sp.PointCity__c;
        addressDTO.country = sp.PointCountry__c;
        addressDTO.postalCode = sp.PointPostalCode__c;
        addressDTO.streetType = sp.PointStreetType__c;
        addressDTO.apartment = sp.PointApartment__c;
        addressDTO.building = sp.PointBuilding__c;
        addressDTO.locality = sp.PointLocality__c;
        addressDTO.province = sp.PointProvince__c;
        addressDTO.floor = sp.PointFloor__c;
        addressDTO.addressNormalized = sp.PointAddressNormalized__c;

        return addressDTO;
    }

    public static List<String> serviceSiteOsiAddress(ServiceSite__c sp,OpportunityServiceItem__c osi) {
        List<String> address = new List<String>();

        if(osi == null) {

            if (String.isNotBlank(sp.SiteStreetType__c)) {
                address.add(sp.SiteStreetType__c);
            }
            if (String.isNotBlank(sp.SiteStreetName__c)) {
                address.add(sp.SiteStreetName__c);
            }
       
            if (String.isNotBlank(sp.SiteStreetNumber__c)) {

                if(String.isBlank(sp.SiteStreetNumberExtn__c)){
                    address.add(sp.SiteStreetNumber__c);
                }else {
                    address.add(sp.SiteStreetNumber__c + sp.SiteStreetNumberExtn__c);
                }
            }
            if (String.isNotBlank(sp.SiteCountry__c)) {
                address.add(sp.SiteCountry__c);
            }
            if (String.isNotBlank(sp.SiteCity__c)) {
                address.add(sp.SiteCity__c);
            }
            if (String.isNotBlank(sp.SitePostalCode__c)) {
                address.add(sp.SitePostalCode__c);
            }
            if (String.isNotBlank(sp.SiteProvince__c)) {
                address.add(sp.SiteProvince__c);
            }

            if (String.isNotBlank(sp.SiteApartment__c)) {
                address.add(sp.SiteApartment__c);
            }
            if (String.isNotBlank(sp.SiteBuilding__c)) {
                address.add(sp.SiteBuilding__c);
            }
            if (String.isNotBlank(sp.SiteLocality__c)) {
                address.add(sp.SiteLocality__c);
            }
            if (String.isNotBlank(sp.SiteFloor__c)) {
                address.add(sp.SiteFloor__c);
            }

        }

        if(sp == null){

            if(String.isNotBlank(osi.SiteStreetType__c)){
                address.add(osi.SiteStreetType__c);
            }
            if(String.isNotBlank(osi.SiteStreetName__c)){
                address.add(osi.SiteStreetName__c);
            }
            if (String.isNotBlank(osi.SiteStreetNumber__c)) {

                if(String.isBlank(osi.SiteStreetNumberExtn__c)){
                    address.add(osi.SiteStreetNumber__c);
                }else {
                    address.add(osi.SiteStreetNumber__c + osi.SiteStreetNumberExtn__c);
                }
            }
            if(String.isNotBlank(osi.SiteCountry__c)){
                address.add(osi.SiteCountry__c);
            }
            if(String.isNotBlank(osi.SiteCity__c)){
                address.add(osi.SiteCity__c);
            }
            if(String.isNotBlank(osi.SitePostalCode__c)){
                address.add(osi.SitePostalCode__c);
            }
            if(String.isNotBlank(osi.SiteProvince__c)){
                address.add(osi.SiteProvince__c);
            }

            if(String.isNotBlank(osi.SiteApartment__c)){
                address.add(osi.SiteApartment__c);
            }
            if(String.isNotBlank(osi.SiteBuilding__c)){
                address.add(osi.SiteBuilding__c);
            }
            if(String.isNotBlank(osi.SiteLocality__c)){
                address.add(osi.SiteLocality__c);
            }
            if(String.isNotBlank(osi.SiteFloor__c)){
                address.add(osi.SiteFloor__c);
            }
        }

        return address;
    }

    public class objectAPIName extends  AuraCallable{
        protected override Object perform(String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('objId');

            if (String.isBlank(recordId)) {
                throw new WrtsException(System.Label.OpportunityServiceItem+ ' - ' + System.Label.Required);
            }
            
            try {
                response.put('error', false);
                response.put('objName',Utils.findObjectAPINameById(recordId));

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

}