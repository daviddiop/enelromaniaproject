/*
 * Test class for RegistryChangeWizardCnt
 *
 * @author Moussa Fofana
 * @version 1.0
 * @description Test class for RegistryChangeWizardCnt
 * @uses
 * @code
 * @history
 * 2019-05-31:  Moussa Fofana
 * 2019-11-27:  David diop
 */

@isTest
public with sharing class RegistryChangeWizardCntTst {
    @testSetup
    static void setup() {
        Individual individual = TestDataFactory.individual().createIndividual().build();
        insert individual;
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        List<Account> listAccount = new list<Account>();
        Account personAccount = TestDataFactory.account().personAccount().setPersonIndividual(individual.Id).build();
        listAccount.add(personAccount);
        /*Account personAccount2 = TestDataFactory.account().personAccount().setPersonIndividual(individual.Id).build();
        listAccount.add(personAccount2);*/
        Account businessAccount = TestDataFactory.account().businessAccount().build();
        listAccount.add(businessAccount);
        insert listAccount;
        CompanyDivision__c companyDivision = TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;

        Contact contact = TestDataFactory.contact().createContact().setAccount(listAccount[1].Id).setIndividual(individual.Id).build();
        insert contact;
        AccountContactRelation accountContactRelation = TestDataFactory.AccountContactRelation().createAccountContactRelation(contact.Id, listAccount[0].Id).build();
        insert accountContactRelation;
    }

    @isTest
    static void getOptionListTest() {
        Account account = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
         Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => account.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('RegistryChangeWizardCnt','getOptionList',inputJSON,true);
        Test.stopTest();
        Map<String,Object> OptionList = (Map<String,Object>) response;
        System.assertEquals(true, OptionList.get('personRecordType')!=null);
        System.assertEquals(true, OptionList.get('businessRecordType')!=null);
    }

    @isTest
    static void getOptionListExceptionTest() {
         Map<String, String > inputJSON = new Map<String, String>{
            'accountId' => ''
        };        
        Test.startTest();
        try {
            TestUtils.exec('RegistryChangeWizardCnt','getOptionList',inputJSON,false);
        } catch (Exception e) {
            System.assert(true, e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void retrieveAccountTest() {
        Id accountId = [
            SELECT Id, Name,IsPersonAccount,FirstName,LastName,PersonEmail,
                    NationalIdentityNumber__pc,PersonMobilePhone,Email__c,VATNumber__c,
                    Phone,BusinessType__c,Active__c
            FROM Account
            LIMIT 1
        ].Id;
        Test.startTest();
        Object response = TestUtils.exec('RegistryChangeWizardCnt','retrieveAccount',accountId,true);
        Account createdAccount = (Account) response;
        System.assert(createdAccount.Id == accountId);
        Id accountId2 = null;
        response = TestUtils.exec('AccountCnt','retrieveAccount',accountId2,true);
        system.assertEquals(response,null);
        Test.stopTest();
    }

    @isTest
    static void getCaseListTest() {
        Account account = [
            SELECT Id, PersonIndividualId
            FROM Account
            LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('RegistryChangeWizardCnt','getCaseList',account.Id,true);
        Map<String , List<Case>> result = (Map<String , List<Case>>) response;
        System.assertEquals(true, result.get('slowCases').size() != null );
        Test.stopTest();
    }


    @isTest
    static void getContactListTest() {
        Account account = [
            SELECT Id, PersonIndividualId
            FROM Account
            LIMIT 1
        ];
        Test.startTest();
        Object response = TestUtils.exec('RegistryChangeWizardCnt','getContactList',account,true);
        List<Account> accountList = (List<Account>) response;
        System.assertEquals(accountList.size(),2);
        Test.stopTest();
    }

    @isTest
    static void saveNewRegistryTest() {
        Account account = [
            SELECT Id, PersonIndividualId
            FROM Account
            LIMIT 1
        ];
        RegistryChangeWizardCnt.InputData inputData = new RegistryChangeWizardCnt.InputData();
        inputData.slowcase = TestDataFactory.slowCaseBuilder().createCaseBuilder().build();
        inputData.fastcase = TestDataFactory.fastCaseBuilder().createCaseBuilder().build();
        inputData.accountId = account.Id;
        inputData.residentialAddressForced = true;
        Test.startTest();
        Object response = TestUtils.exec('RegistryChangeWizardCnt','saveNewRegistry',inputData,true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(result.get('error'),false);
        Test.stopTest();
    }
}