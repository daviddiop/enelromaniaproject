/**
 * Created by BADJI on 20/07/2020.
 */
@IsTest
public with sharing class MRO_LC_DiscardManagementTst {

    @TestSetup
    static void setup(){
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(1).build();
        insert companyDivision;
        List<Account> listAccount = new List<Account>();
        List<Case> caseList = new List<Case>();
        listAccount.add(MRO_UTL_TestDataFactory.account().personAccount().build());
        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        listAccount.add(businessAccount);
        insert listAccount;
        Dossier__c dossier = MRO_UTL_TestDataFactory.Dossier().setCompany(companyDivision.Id).setRecordType().build();
       // dossier.Phase__c=phase1.Id;
        insert dossier;

        FieldsTemplate__c fieldsTemplate = new FieldsTemplate__c();
        fieldsTemplate.Name ='fields template';
        fieldsTemplate.Code__c ='123R568FG7ohfg2517h';
        fieldsTemplate.RootObjectType__c ='Dossier__c';
        insert fieldsTemplate;

        FileMetadata__c fileMetadata = MRO_UTL_TestDataFactory.FileMetadata().build();
        fileMetadata.Dossier__c = dossier.Id;
        insert fileMetadata;

        insert MRO_UTL_TestDataFactory.FileMetadata().build();

        DocumentType__c documentType01 = MRO_UTL_TestDataFactory.DocumentType().createDocumentType('document Type Test').build();
        insert documentType01;

        DocumentType__c documentType02 = MRO_UTL_TestDataFactory.DocumentType().createDocumentType('document Type Test validated').build();
        insert documentType02;

        DocumentBundle__c bundle = new DocumentBundle__c();
        bundle.Name = 'Doc bundle test';
        insert bundle;

        List<DocumentItem__c> listDocumentItems = new List<DocumentItem__c>();
        for (Integer i = 0; i < 10; i++) {
            DocumentItem__c documentItem = MRO_UTL_TestDataFactory.DocumentItem().createBulkDocumentItem(i).build();
            documentItem.DocumentType__c = i > 5 ? documentType01.Id : documentType02.Id;
            listDocumentItems.add(documentItem);
        }
        insert listDocumentItems;

        DocumentBundleItem__c docBundleItem01 = MRO_UTL_TestDataFactory.DocumentBundleItem().createDocumentBundleItem(bundle.Id,documentType01.Id,true).build();
        insert docBundleItem01;
        wrts_prcgvr__Activity__c activity =new wrts_prcgvr__Activity__c();
        activity.Type__c = 'Discard';
        activity.wrts_prcgvr__ObjectId__c = dossier.Id;
        activity.FieldsTemplate__c = fieldsTemplate.Id;
        activity.DocumentBundle__c = bundle.Id;
        activity.wrts_prcgvr__Status__c = 'Not Started';
        insert activity;
    }

    @IsTest
    static void initializeTest(){
        wrts_prcgvr__Activity__c  activity = [
            SELECT Id, Type__c,FieldsTemplate__c
            FROM wrts_prcgvr__Activity__c
            LIMIT 1
        ];
        Map<String, String > inputJSON = new Map<String, String>{
            'activityId' => activity.Id
        };
        Test.startTest();
        Object response= TestUtils.exec(
            'MRO_LC_DiscardManagement', 'initialize', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('activity') != null);
        Test.stopTest();
    }

    @IsTest
    static void saveDataAndCloseActivityTest(){
        wrts_prcgvr__Activity__c  activity = [
            SELECT Id, Type__c,FieldsTemplate__c,wrts_prcgvr__Status__c
            FROM wrts_prcgvr__Activity__c
            LIMIT 1
        ];
        Dossier__c dossier= [
            SELECT Id
            FROM Dossier__c
            LIMIT 1
        ];
        FieldsTemplate__c fieldsTemplate=[
            SELECT Id, RootObjectType__c
            FROM FieldsTemplate__c
            LIMIT 1
        ];

        FieldsTemplateSection__c fieldsTemplateSection = new FieldsTemplateSection__c();
        fieldsTemplateSection.ObjectType__c='Dossier__c';
        fieldsTemplateSection.Order__c=5;
        fieldsTemplateSection.Template__c=fieldsTemplate.Id;
        fieldsTemplateSection.Columns__c = 3;

        insert fieldsTemplateSection;
        Map<String, Object> mapObject =new Map<String, Object>();
        //mapObject.put('recordId',dossier.Id);
        List<FieldsTemplateSection__c>listFieldsTemplateSection = new List<FieldsTemplateSection__c>();
        MRO_SRV_DynamicForm.DynamicFormSection  dynamicFormSection = new MRO_SRV_DynamicForm.DynamicFormSection(fieldsTemplateSection,dossier.Id);
        dynamicFormSection.recordId =dossier.Id;
        dynamicFormSection.templateId = activity.FieldsTemplate__c;
        listFieldsTemplateSection.add(fieldsTemplateSection);
        MRO_SRV_DynamicForm.DynamicForm dynamicForm = new MRO_SRV_DynamicForm.DynamicForm(fieldsTemplate, listFieldsTemplateSection, dossier);


        dynamicForm.templateId = activity.FieldsTemplate__c;


        Map<String, String > inputJSON = new Map<String, String>{
            'activityId' => activity.Id,
            'dynamicForm' => JSON.serialize(dynamicForm),
            'records' =>JSON.serialize(mapObject)
        };

        Test.startTest();
        Object response= TestUtils.exec(
            'MRO_LC_DiscardManagement', 'saveDataAndCloseActivity', inputJSON, false);
        System.debug('#response '+response);
        //Map<String, Object> result = (Map<Stri ng, Object>) response;
       // System.assertEquals(true, result.get('result') != null);
        Test.stopTest();


    }
    @IsTest
    static void closeActivityTest(){
        wrts_prcgvr__Activity__c  activity = [
            SELECT Id, Type__c,FieldsTemplate__c,wrts_prcgvr__ObjectId__c,DocumentBundle__c,wrts_prcgvr__Status__c
            FROM wrts_prcgvr__Activity__c
            LIMIT 1
        ];

        Map<String, String > inputJSON = new Map<String, String>{
            'activityId' => activity.Id
        };

        Test.startTest();
        Object response= TestUtils.exec(
            'MRO_LC_DiscardManagement', 'closeActivity', inputJSON, false);
        System.debug('#response close activity '+response);
        //Map<String, Object> result = (Map<String, Object>) response;
       // System.assertEquals(true, result.get('result') != null);
        Test.stopTest();


    }
}