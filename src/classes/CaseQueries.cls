/**
 * Created by Moussa FOFANA on 17.07.2019.
 */
public with sharing class CaseQueries {
    public static CaseQueries getInstance() {
        return (CaseQueries) ServiceLocator.getInstance(CaseQueries.class);
    }
    public Map<String, List<Case>> listRelatedCases(String accountId, String slowRecordTypeId, String fastRecordTypeId) {
        Map<String, List<Case>> caseList = new Map<String, List<Case>>();
        List<case> slowCases = [
                SELECT Id,AccountId,RecordTypeId
                FROM Case
                WHERE AccountId = :accountId AND isClosed = :false AND RecordTypeId = :slowRecordTypeId
        ];
        List<case> fastCases = [
                SELECT Id,AccountId,RecordTypeId
                FROM Case
                WHERE AccountId = :accountId AND isClosed = :false AND RecordTypeId = :fastRecordTypeId
        ];
        caseList.put('slowCases', slowCases);
        caseList.put('fastCases', fastCases);
        return caseList;
    }
    public List<Case> getCasesByDossierId(Id dossierId) {
        return [
            SELECT Id, CaseNumber, RecordTypeId, RecordType.Name, Supply__r.ContractAccount__c,
                    BillingProfile__c, ContractAccount__c, Contract__c,
                    CompanyDivision__c,Status,Supply__c
            FROM Case
            WHERE Dossier__c = :dossierId
        ];
    }

    public Case getById(String caseId) {
        List<Case> caseList = [
                SELECT Id,RecordType.DeveloperName, Trader__c,Supply__r.ContractAccount__c,
                        AddressStreetName__c, AddressStreetType__c, AddressStreetNumber__c, AddressStreetNumberExtn__c, AddressCountry__c, AddressApartment__c,
                        AddressBuilding__c,AddressCity__c,AddressFloor__c,AddressLocality__c,AddressAddressNormalized__c,AddressPostalCode__c,AddressProvince__c
                FROM Case
                WHERE Id = :caseId
        ];

        return caseList.get(0);
    }
}