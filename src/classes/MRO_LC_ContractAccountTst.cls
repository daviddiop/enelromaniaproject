/**
 * @author Created by Boubacar Sow  on 25/11/2019.
 * @description  [ENLCRO-196] Integrate ContractAccountSelection in the DemostratedPaymentWizard

 */
@IsTest
public with sharing class MRO_LC_ContractAccountTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;
        Account account = MRO_UTL_TestDataFactory.account().personAccount().build();
        insert account;

        List<CompanyDivision__c> listCompaniesDivision = new List<CompanyDivision__c>();
        for (Integer i = 0; i < 1; i++) {
            CompanyDivision__c companyDivision = MRO_UTL_TestDataFactory.CompanyDivision().createBulkCompanyDivision(i).build();
            listCompaniesDivision.add(companyDivision);
        }
        insert listCompaniesDivision;

        User user = MRO_UTL_TestDataFactory.user().standardUser().build();
        user.CompanyDivisionId__c = listCompaniesDivision[0].Id;
        insert user;

        ServicePoint__c servicePoint = MRO_UTL_TestDataFactory.servicePoint().createServicePoint().build();
        servicePoint.Code__c = '43252435624654';
        servicePoint.Account__c = account.Id;
        servicePoint.Trader__c = account.Id;
        servicePoint.Distributor__c = account.Id;
        servicePoint.ENELTEL__c = '123456789';
        servicePoint.RecordTypeId = Schema.SObjectType.ServicePoint__c.getRecordTypeInfosByDeveloperName().get('Gas').getRecordTypeId();
        insert servicePoint;

        Contract contract = MRO_UTL_TestDataFactory.Contract().createContract().build();
        contract.Name = 'Name11062019';
        contract.AccountId = account.Id;
        insert contract;

        ContractAccount__c contractAccount = MRO_UTL_TestDataFactory.contractAccount().createContractAccount().setAccount(account.Id).build();
        insert  contractAccount;

        Supply__c supply = MRO_UTL_TestDataFactory.supply().createSupplyBuilder().setCompany(listCompaniesDivision[0].Id).build();
        supply.Contract__c = contract.Id;
        supply.Account__c = account.Id;
        supply.ServicePoint__c = servicePoint.Id;
        insert supply;
        City__c c= new City__c(
                Name = 'SAG',
                Country__c = 'ROMANIA'
        );
        insert c ;

        Street__c street = MRO_UTL_TestDataFactory.street().createBuilder().build();
        street.City__c = c.Id;
        insert street;
    }

    @IsTest
    static void getActiveSuppliesTest() {
        ContractAccount__c contractAccount = [
                SELECT  Id, Name
                FROM ContractAccount__c
                LIMIT 1
        ];

        Map<String, String> jsonInput = new Map<String, String>{
                'contractAccountId' => contractAccount.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ContractAccount', 'getActiveSupplies', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true , result.get('enableEditButtonCA') == true);
        Test.stopTest();

    }

    @IsTest
    static void getActiveSuppliesExceptionTest() {

        Map<String, String> jsonInput = new Map<String, String>{
                'contractAccountId' => ''
        };
        Test.startTest();
        try {
            Object response = TestUtils.exec('MRO_LC_ContractAccount', 'getActiveSupplies', jsonInput, false);
        }catch (Exception e){
            System.assert(true, e.getMessage());
        }
        Test.stopTest();

    }

    @IsTest
    static void getContractAccountRecordsTest() {
        Account account = [
                SELECT  Id, Name
                FROM Account
                LIMIT 1
        ];
        CompanyDivision__c companyDivision = [
                SELECT  Id, Name
                FROM CompanyDivision__c
                LIMIT 1
        ];
        MRO_LC_ContractAccount.Filter filter = new MRO_LC_ContractAccount.Filter();
        filter.dataType = 'String';
        filter.field = 'Name';
        filter.value = 'contractAccName';
        filter.operator = 'equals';
        String filterToString = JSON.serialize(filter);
        Map<String, String> jsonInput = new Map<String, String>{
                'accountId' => account.Id,
                'companyDivisionId' => companyDivision.Id,
                'filter' =>filterToString
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ContractAccount', 'getContractAccountRecords', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true , result.get('listContractAccount') != null);
        System.debug('#### listContractAccount '+result.get('listContractAccount'));
        system.assertEquals(true, result.get('error') == false, 'Error is found');


        jsonInput = new Map<String, String>{
                'accountId' => account.Id,
                'companyDivisionId' => ''
        };
        response = TestUtils.exec(
                'MRO_LC_ContractAccount', 'getContractAccountRecords', jsonInput, true);
        result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == false, 'Error is found');
        Test.stopTest();

    }

    @IsTest
    static void getContractAccountRecordsExceptionTest() {
        Map<String, String> jsonInput = new Map<String, String>{
                'accountId' => 'acccountId'
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ContractAccount', 'getContractAccountRecords', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result.get('error') == true, 'Error is found');
        Test.stopTest();
    }

    @IsTest
    static void getMapStreetNameBySelfReadingPeriodAndBillingFrequencyTest(){
        Street__c street = [
                SELECT Id
                FROM Street__c
                LIMIT 1
        ];

        Map<String, String> jsonInput = new Map<String, String>{
                'streetId' => street.Id
        };
        Test.startTest();
        Object response = TestUtils.exec('MRO_LC_ContractAccount', 'getMapStreetNameBySelfReadingPeriodAndBillingFrequency', jsonInput, true);
        Map<String, Object> result = (Map<String, Object>) response;
        system.assertEquals(true, result != null);
        Test.stopTest();

    }

}