/**
 * Created by moustapha on 04/09/2020.
 */

@IsTest
private class MRO_LC_BeneficiaryInformationTst {
    @TestSetup
    static void setup() {
        Sequencer__c sequencer = new Sequencer__c(Type__c = 'CustomerCode', Sequence__c = 2000.0,SequenceLength__c = 9.0);
        insert sequencer;

        Account businessAccount = MRO_UTL_TestDataFactory.account().businessAccount().build();
        businessAccount.Name = 'BusinessAccount1';
        insert businessAccount;

    }

    @IsTest
    public static void copyFromAccountTest() {
        Account account = [
                SELECT Id,Name
                FROM Account
                LIMIT 1
        ];
        Test.startTest();

        //With Account Id not  null
        Map<String, String > inputJSON = new Map<String, String>{
                'accountId' => account.Id
        };
        Object response = TestUtils.exec(
                'MRO_LC_BeneficiaryInformation', 'copyFromAccount', inputJSON, true);
        Map<String, Object> result = (Map<String, Object>) response;
        System.assertEquals(true, result.get('name') == 'BusinessAccount1');
        //With Account Id equals to null
        inputJSON = new Map<String, String>{
                'accountId' => ''
        };
        response = TestUtils.exec(
                'MRO_LC_BeneficiaryInformation', 'copyFromAccount', inputJSON, true);
        Test.stopTest();
    }
}