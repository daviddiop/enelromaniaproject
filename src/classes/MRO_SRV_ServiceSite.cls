/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   apr 07, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_ServiceSite {
    private static MRO_QR_Supply supplyQueries = MRO_QR_Supply.getInstance();
    private static MRO_SRV_DatabaseService databaseSrv = MRO_SRV_DatabaseService.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public static MRO_SRV_ServiceSite getInstance() {
        return (MRO_SRV_ServiceSite) ServiceLocator.getInstance(MRO_SRV_ServiceSite.class);
    }

    public void updateActiveSuppliesCounts(Set<Id> serviceSiteIds) {
        Set<String> supplyStatuses = new Set<String>{CONSTANTS.SUPPLY_STATUS_ACTIVE, CONSTANTS.SUPPLY_STATUS_ACTIVATING, CONSTANTS.SUPPLY_STATUS_TERMINATING};
        Map<Id, List<Supply__c>> activeSuppliesByServiceSite = supplyQueries.getSuppliesByServiceSites(serviceSiteIds, supplyStatuses);
        Map<Id, ServiceSite__c> serviceSitesToUpdate = new Map<Id, ServiceSite__c>();
        Map<Id, List<Supply__c>> vasSuppliesToTerminateByCustomerId = new Map<Id, List<Supply__c>>();
        Map<Id, List<Supply__c>> vasSuppliesToActivateByServiceSiteId = new Map<Id, List<Supply__c>>();
        List<Supply__c> activatingVASSuppliesToUnsuspend = new List<Supply__c>();
        for (Id serviceSiteId : activeSuppliesByServiceSite.keySet()) {
            ServiceSite__c serviceSite = activeSuppliesByServiceSite.get(serviceSiteId)[0].ServiceSite__r;
            Integer oldELESuppliesCount = (serviceSite.ActiveSuppliesCountELE__c != null) ? serviceSite.ActiveSuppliesCountELE__c.intValue() : 0;
            Integer oldGASSuppliesCount = (serviceSite.ActiveSuppliesCountGAS__c != null) ? serviceSite.ActiveSuppliesCountGAS__c.intValue() : 0;
            Integer oldVASSuppliesCount = (serviceSite.ActiveSuppliesCountVAS__c != null) ? serviceSite.ActiveSuppliesCountVAS__c.intValue() : 0;
            Integer newELESuppliesCount = 0, newGASSuppliesCount = 0, newVASSuppliesCount = 0;
            List<Supply__c> activeVASEleSupplies = new List<Supply__c>();
            List<Supply__c> activeVASGasSupplies = new List<Supply__c>();
            for (Supply__c supply : activeSuppliesByServiceSite.get(serviceSiteId)) {
                if (supply.Status__c == CONSTANTS.SUPPLY_STATUS_ACTIVE || supply.Status__c == CONSTANTS.SUPPLY_STATUS_TERMINATING) {
                    if (supply.RecordType.DeveloperName == CONSTANTS.COMMODITY_ELECTRIC) {
                        newELESuppliesCount++;
                    }
                    else if (supply.RecordType.DeveloperName == CONSTANTS.COMMODITY_GAS) {
                        newGASSuppliesCount++;
                    }
                    else {
                        newVASSuppliesCount++;
                        if (supply.Status__c == CONSTANTS.SUPPLY_STATUS_ACTIVE) {
                            if (supply.ContractAccount__r.Commodity__c == CONSTANTS.COMMODITY_ELECTRIC) {
                                activeVASEleSupplies.add(supply);
                            }
                            else if (supply.ContractAccount__r.Commodity__c == CONSTANTS.COMMODITY_GAS) {
                                activeVASGasSupplies.add(supply);
                            }
                        }
                    }
                } else if (supply.Status__c == CONSTANTS.SUPPLY_STATUS_ACTIVATING && supply.RecordType.DeveloperName == 'Service') {
                    if (!vasSuppliesToActivateByServiceSiteId.containsKey(serviceSiteId)) {
                        vasSuppliesToActivateByServiceSiteId.put(serviceSiteId, new List<Supply__c>{supply});
                    } else {
                        vasSuppliesToActivateByServiceSiteId.get(serviceSiteId).add(supply);
                    }
                }
            }
            if (newELESuppliesCount != oldELESuppliesCount || newGASSuppliesCount != oldGASSuppliesCount || newVASSuppliesCount != oldVASSuppliesCount) {
                serviceSite.ActiveSuppliesCountELE__c = newELESuppliesCount;
                serviceSite.ActiveSuppliesCountGAS__c = newGASSuppliesCount;
                serviceSite.ActiveSuppliesCountVAS__c = newVASSuppliesCount;
                if (newELESuppliesCount == 0 && oldELESuppliesCount > 0) {
                    if (!vasSuppliesToTerminateByCustomerId.containsKey(serviceSite.Account__c)) {
                        vasSuppliesToTerminateByCustomerId.put(serviceSite.Account__c, activeVASEleSupplies);
                    } else {
                        vasSuppliesToTerminateByCustomerId.get(serviceSite.Account__c).addAll(activeVASEleSupplies);
                    }
                } else if (newELESuppliesCount > 0 && oldELESuppliesCount == 0) {
                    if (activeSuppliesByServiceSite.containsKey(serviceSiteId)) {
                        for (Supply__c vasSupply : activeSuppliesByServiceSite.get(serviceSiteId)) {
                            if (vasSupply.ContractAccount__r.Commodity__c == CONSTANTS.COMMODITY_ELECTRIC) {
                                activatingVASSuppliesToUnsuspend.add(vasSupply);
                            }
                        }
                    }
                }
                if (newGASSuppliesCount == 0 && oldGASSuppliesCount > 0) {
                    if (!vasSuppliesToTerminateByCustomerId.containsKey(serviceSite.Account__c)) {
                        vasSuppliesToTerminateByCustomerId.put(serviceSite.Account__c, activeVASGasSupplies);
                    } else {
                        vasSuppliesToTerminateByCustomerId.get(serviceSite.Account__c).addAll(activeVASGasSupplies);
                    }
                } else if (newGASSuppliesCount > 0 && oldGASSuppliesCount == 0) {
                    if (activeSuppliesByServiceSite.containsKey(serviceSiteId)) {
                        for (Supply__c vasSupply : activeSuppliesByServiceSite.get(serviceSiteId)) {
                            if (vasSupply.ContractAccount__r.Commodity__c == CONSTANTS.COMMODITY_GAS) {
                                activatingVASSuppliesToUnsuspend.add(vasSupply);
                            }
                        }
                    }
                }
                serviceSitesToUpdate.put(serviceSite.Id, serviceSite);
            }
        }
        serviceSiteIds.removeAll(activeSuppliesByServiceSite.keySet());
        for (Id missingServiceSiteId : serviceSiteIds) {
            serviceSitesToUpdate.put(missingServiceSiteId, new ServiceSite__c(
                Id = missingServiceSiteId,
                ActiveSuppliesCountELE__c = 0,
                ActiveSuppliesCountGAS__c = 0,
                ActiveSuppliesCountVAS__c = 0
            ));
        }
        if (!serviceSitesToUpdate.isEmpty()) {
            databaseSrv.updateSObject(serviceSitesToUpdate.values());
        }
        if (!vasSuppliesToTerminateByCustomerId.isEmpty()) {
            List<Case> vasTerminationCases = MRO_SRV_VAS.getInstance().generateVASTerminationCases(vasSuppliesToTerminateByCustomerId);
            if (!vasTerminationCases.isEmpty()) {
                Set<Id> caseIds = new Map<Id, Case>(vasTerminationCases).keySet();
                MRO_SRV_Case.getInstance().applyAutomaticTransitionOnCases(caseIds, CONSTANTS.DOCUMENT_VALIDATION_TAG);
            }
        }
        if (!activatingVASSuppliesToUnsuspend.isEmpty()) {
            MRO_SRV_Supply.getInstance().unsuspendVASActivationCases(activatingVASSuppliesToUnsuspend);
        }
    }

    /**
     * @author Boubacar Sow
     * @date 13/11/200
     * @description [ENLCRO-1741] Service Point Address Change - Address change execution at the case closure
     * @param serviceSite
     * @param caseRecord
     *
     * @return
     */
    public  ServiceSite__c updateServiceSiteAddress( ServiceSite__c serviceSite, Case caseRecord){
        serviceSite.SiteStreetType__c = caseRecord.AddressStreetType__c;
        serviceSite.SiteStreetNumber__c = caseRecord.AddressStreetNumber__c;
        serviceSite.SiteCity__c = caseRecord.AddressCity__c;
        serviceSite.SitePostalCode__c = caseRecord.AddressPostalCode__c;
        serviceSite.SiteCountry__c = caseRecord.AddressCountry__c;
        serviceSite.SiteAddressNormalized__c = caseRecord.AddressAddressNormalized__c;
        serviceSite.SiteStreetName__c = caseRecord.AddressStreetName__c;
        serviceSite.SiteApartment__c = caseRecord.AddressApartment__c;
        serviceSite.SiteFloor__c = caseRecord.AddressFloor__c;
        serviceSite.SiteLocality__c = caseRecord.AddressLocality__c;
        serviceSite.SiteProvince__c = caseRecord.AddressProvince__c;
        serviceSite.SiteBuilding__c = caseRecord.AddressBuilding__c;
        serviceSite.SiteBlock__c = caseRecord.AddressBlock__c;
        serviceSite.SiteStreetNumberExtn__c = caseRecord.AddressStreetNumberExtn__c;
        serviceSite.SiteStreetId__c = caseRecord.AddressStreetId__c;
        return serviceSite;
    }
}