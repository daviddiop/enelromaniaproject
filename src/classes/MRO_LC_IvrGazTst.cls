/**
 * Created by a.maksimov (alexey.maksimov@truesalessoft.com) on 26.11.20.
 */

@IsTest
private class MRO_LC_IvrGazTst {

    @IsTest
    static void getDummyList() {
        SAPCallouts__c sapCallouts = new SAPCallouts__c(MeterList__c = false);
        insert sapCallouts;

        MRO_LC_IvrGaz.IvrGazRequestParam params = new MRO_LC_IvrGaz.IvrGazRequestParam();
        params.enelTel = '1234';
        params.startDate = '2020-11-01';
        params.endDate = '2020-11-20';

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_IvrGaz', 'listIndex', params, true);
        Test.stopTest();

        System.assertNotEquals(null, response);
    }

    @IsTest
    static void getList() {
        SAPCallouts__c sapCallouts = new SAPCallouts__c(MeterList__c = true);
        insert sapCallouts;

        MRO_LC_IvrGaz.IvrGazRequestParam params = new MRO_LC_IvrGaz.IvrGazRequestParam();
        params.enelTel = '1234';
        params.startDate = '2020-11-01';
        params.endDate = '2020-11-20';

        Test.startTest();
        Object response = TestUtils.exec(
                'MRO_LC_IvrGaz', 'listIndex', params, true);
        Test.stopTest();

        System.assertEquals(null, response);
    }

    @IsTest
    static void invalidRequestData() {
        SAPCallouts__c sapCallouts = new SAPCallouts__c(MeterList__c = false);
        insert sapCallouts;

        MRO_LC_IvrGaz.IvrGazRequestParam params = new MRO_LC_IvrGaz.IvrGazRequestParam();
        params.enelTel = '1234';

        Test.startTest();
        TestUtils.exec(
                'MRO_LC_IvrGaz', 'listIndex', params, false);
        Test.stopTest();
    }

    @IsTest
    static void testParsingSapResponse() {
        Map<String, String> stringMap = new Map<String, String>{
                'MeterSerialNumber' => '1234',
                'ReadingDate' => '2020-11-01',
                'Index' => '100'
        };
        MRO_LC_IvrGaz.Index index = new MRO_LC_IvrGaz.Index(stringMap);
        System.assertNotEquals(null, index.readingDate);
        System.assertNotEquals(null, index.meterSerialNumber);
        System.assertNotEquals(null, index.index);
    }
}