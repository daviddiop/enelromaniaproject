public with sharing class LightningUtils {

    @AuraEnabled(cacheable=true)
    public static Map<String,Object> getFieldSet(String objectType, String extraFieldsFieldSet) {
        Map<String,Object> response = new Map<String,Object>();
        List<Map<String,Object>> fields = new List<Map<String,Object>>();
        Schema.FieldSet fieldSet = Schema.getGlobalDescribe().get(objectType).getDescribe().fieldSets.getMap().get(extraFieldsFieldSet);
        if (fieldSet != null) {
            response.put('label', fieldSet.getLabel());
            for (Schema.FieldSetMember f : fieldSet.getFields()) {
                Map<String, Object> fieldDetail = new Map<String, Object>();
                fieldDetail.put('dbRequired', f.getDBRequired());
                fieldDetail.put('fieldPath', f.getFieldPath());
                fieldDetail.put('label', f.getLabel());
                fieldDetail.put('required', f.getRequired() || f.getDBRequired());
                fieldDetail.put('type', f.getType().name());
                fields.add(fieldDetail);
            }
            response.put('fields', fields);
        }
        return response;
    }

}