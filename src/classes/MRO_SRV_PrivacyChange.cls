/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mar 11, 2020
 * @desc
 * @history
 */

public with sharing class MRO_SRV_PrivacyChange {
    private static final MRO_SRV_DatabaseService DATABASE_SRV = MRO_SRV_DatabaseService.getInstance();
    private static final MRO_UTL_Constants CONSTANTS = MRO_UTL_Constants.getAllConstants();

    public static MRO_SRV_PrivacyChange getInstance() {
        return (MRO_SRV_PrivacyChange) ServiceLocator.getInstance(MRO_SRV_PrivacyChange.class);
    }

    public void commitPrivacyChange(Dossier__c dossier) {
        MRO_QR_PrivacyChange privacyChangeQueries = MRO_QR_PrivacyChange.getInstance();
        Savepoint sp = Database.setSavepoint();
        try {
            PrivacyChange__c privacyChange = privacyChangeQueries.getPendingPrivacyChangeByDossierId(dossier.Id);
            if (privacyChange != null) {
                if (privacyChange.Individual__c != null) {
                    List<PrivacyChange__c> otherPrivacyChanges = privacyChangeQueries.getPrivacyChangesByIndividual(privacyChange.Individual__c);
                    List<PrivacyChange__c> privacyChangesToUpdate = new List<PrivacyChange__c>();
                    for (PrivacyChange__c pc : otherPrivacyChanges) {
                        if (pc.Id != privacyChange.Id) {
                            if (pc.CreatedDate > privacyChange.CreatedDate && (pc.Status__c == CONSTANTS.PRIVACYCHANGE_STATUS_ACTIVE || pc.Status__c == CONSTANTS.PRIVACYCHANGE_STATUS_OLD)) {
                                privacyChange.Status__c = CONSTANTS.PRIVACYCHANGE_STATUS_SKIPPED;
                            }
                            else if (pc.CreatedDate <= privacyChange.CreatedDate && pc.Status__c == CONSTANTS.PRIVACYCHANGE_STATUS_ACTIVE) {
                                pc.Status__c = CONSTANTS.PRIVACYCHANGE_STATUS_OLD;
                                privacyChangesToUpdate.add(pc);
                            }
                        }
                    }
                    if (privacyChange.Status__c == CONSTANTS.PRIVACYCHANGE_STATUS_PENDING) {
                        Individual ind = new Individual(
                            Id = privacyChange.Individual__c,
                            HasOptedOutTracking = privacyChange.HasOptedOutTracking__c,
                            HasOptedOutProfiling = privacyChange.HasOptedOutProfiling__c,
                            HasOptedOutProcessing = privacyChange.HasOptedOutProcessing__c,
                            HasOptedOutGeoTracking = privacyChange.HasOptedOutGeoTracking__c,
                            HasOptedOutSolicit = privacyChange.HasOptedOutSolicit__c,
                            HasOptedOutPartners__c = privacyChange.HasOptedOutPartners__c,
                            ShouldForget = privacyChange.ShouldForget__c,
                            SendIndividualData = privacyChange.SendIndividualData__c,
                            CanStorePiiElsewhere = privacyChange.CanStorePiiElsewhere__c
                        );
                        DATABASE_SRV.updateSObject(ind);

                        if (privacyChange.Contact__c != null) {
                            Contact cont = new Contact(
                                Id = privacyChange.Contact__c,
                                MarketingContactChannel__c = privacyChange.MarketingChannel__c,
                                MarketingEmail__c = privacyChange.MarketingEmail__c,
                                MarketingMobilePhone__c = privacyChange.MarketingMobilePhone__c,
                                MarketingPhone__c = privacyChange.MarketingPhone__c
                            );
                            DATABASE_SRV.updateSObject(cont);
                        }

                        privacyChange.Status__c = CONSTANTS.PRIVACYCHANGE_STATUS_ACTIVE;
                    }

                    privacyChange.EffectiveDate__c = Date.today();
                    privacyChangesToUpdate.add(privacyChange);

                    DATABASE_SRV.updateSObject(privacyChangesToUpdate);
                }
                else {
                    throw new WrtsException('Cannot commit Privacy Change record: missing Individual!');
                }
            }
        }
        catch (Exception e) {
            Database.rollback(sp);
            throw e;
        }
    }

    public static PrivacyChange__c updatePrivacyChangeDossier(String privacyChangeId, String dossierId){
        PrivacyChange__c pc = new PrivacyChange__c();
        try {
            if (String.isNotBlank(privacyChangeId) && String.isNotBlank(dossierId)) {
                pc.Id = privacyChangeId;
                pc.Dossier__c = dossierId;
                DATABASE_SRV.updateSobject(pc);
            }
        } catch(Exception e){
            throw e;
        }
        return pc;
    }
}