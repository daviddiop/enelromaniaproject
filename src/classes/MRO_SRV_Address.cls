/**
 * Updated by BG to implement AddressDTO 16/07/2019
 */
public with sharing class MRO_SRV_Address {
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    public static MRO_SRV_Address getInstance() {
        return (MRO_SRV_Address) ServiceLocator.getInstance(MRO_SRV_Address.class);
    }

    @AuraEnabled
    public static List<Map<String, Object>> normalize(Map<String, Object> address) {
        //TODO call IAddressNormalizationService instance
        List<Map<String, Object>> addresses = new List<Map<String, Object>>();

        addresses.add(address);
        return addresses;
    }

    @AuraEnabled
    public static String getExtraFields(String objectApiName, String addressFieldSet) {
        return JSON.serialize(LightningUtils.getFieldSet(objectApiName, addressFieldSet));
    }

    @AuraEnabled
    public static List<AddressField__mdt> getAddressStructure(String addressStructureApiName) {
        List<AddressField__mdt> addrLayout = [SELECT MasterLabel, QualifiedApiName, ColSize__c, Order__c, Required__c, FieldName__c, Hidden__c,SourceTable__c,SourceField__c,ParentsFields__c,
                                            (SELECT MasterLabel, QualifiedApiName FROM AddressPicklistValues__r ORDER BY MasterLabel)
                                                FROM AddressField__mdt
                                            WHERE AddressLayout__r.QualifiedApiName = :addressStructureApiName
                                            ORDER BY Order__c ASC];
        return addrLayout;
    }

    public static Boolean isForced(SObject record, String fieldsPrefix) {

        return (!(Boolean) record.get(fieldsPrefix+'AddressNormalized__c')) && String.isNotBlank((String) record.get(fieldsPrefix+'StreetId__c'));
    }
//    @AuraEnabled
//    public static List<Map<String, Object>> getAddressStructure(String addressStructureApiName) {
//        List<AddressField__mdt> addrLayout = [
//                SELECT MasterLabel, QualifiedApiName, ColSize__c, Order__c, Required__c, FieldName__c, Hidden__c
//                FROM AddressField__mdt
//                WHERE AddressLayout__r.QualifiedApiName = 'DefaultAddressLayout'
//                ORDER BY Order__c ASC
//        ];
//
//        List<AddressPicklistValue__mdt> addressPicklistValues = [SELECT AddressField__c, MasterLabel, QualifiedApiName FROM AddressPicklistValue__mdt ORDER BY MasterLabel];
//        System.debug('addressPicklistValues size ' + addressPicklistValues.size());
//        Map<Id, List<AddressPicklistValue__mdt>> addressPicklistValueMap = new Map<Id, List<AddressPicklistValue__mdt>>();
//        for (AddressPicklistValue__mdt addressPicklistValue : addressPicklistValues) {
//            if (!addressPicklistValueMap.containsKey(addressPicklistValue.AddressField__c)) {
//                addressPicklistValueMap.put(addressPicklistValue.AddressField__c, new List<AddressPicklistValue__mdt>());
//            }
//            List<AddressPicklistValue__mdt>addressPicklistValues2 = addressPicklistValueMap.get(addressPicklistValue.AddressField__c);
//            addressPicklistValues2.add(addressPicklistValue);
//        }
//        System.debug('key ' + addressPicklistValueMap.keySet());
//
//        List<Map<String, Object>> addrLayoutJson = new List<Map<String, Object>>();
//        System.debug('JSON.serialize(addrLayout) = '+JSON.serialize(addrLayout));
//        for (Map<String, Object> addressField : (List<Map<String, Object>>) JSON.deserialize(JSON.serialize(addrLayout), List<Map<String, Object>>.class)) {
////            System.debug(' addressField.AddressPicklistValues__r' + addressField.AddressPicklistValues__r);
//            List<AddressPicklistValue__mdt> addressPicklistValues2 = addressPicklistValueMap.get(addressField.get('Id').toString());
//            System.debug('xx = ' + addressPicklistValues2);
//
//            if (addressPicklistValues2 != null) addressField.put('AddressPicklistValues__r', addressPicklistValues2);
//            addrLayoutJson.add(addressField);
//        }
//        return addrLayoutJson;
//    }


    public with sharing class AddressDTO {
	    @AuraEnabled
        public String streetId { get; set; }
        @AuraEnabled
        public String streetNumber { get; set; }
        @AuraEnabled
        public String streetNumberExtn { get; set; }
        @AuraEnabled
        public String streetName { get; set; }
        @AuraEnabled
        public String streetType { get; set; }
        @AuraEnabled
        public String apartment { get; set; }
        @AuraEnabled
        public String building { get; set; }
        @AuraEnabled
        public String block { get; set; }
        @AuraEnabled
        public String city { get; set; }
        @AuraEnabled
        public String country { get; set; }
        @AuraEnabled
        public String floor { get; set; }
        @AuraEnabled
        public String locality { get; set; }
        @AuraEnabled
        public String postalCode { get; set; }
        @AuraEnabled
        public String province { get; set; }
        @AuraEnabled
        public Boolean addressNormalized { get; set; }
        @AuraEnabled
	    public String addressKey { get; set; }

	    public AddressDTO() {
        }

	    public AddressDTO(SObject record, String fieldsPrefix) {
		    this.streetId = (String) record.get(fieldsPrefix+'StreetId__c');
		    this.streetNumber = (String) record.get(fieldsPrefix+'StreetNumber__c');
	        this.streetNumberExtn = (String) record.get(fieldsPrefix+'StreetNumberExtn__c');
	        this.streetName = (String) record.get(fieldsPrefix+'StreetName__c');
	        this.streetType = (String) record.get(fieldsPrefix+'StreetType__c');
	        this.apartment = (String) record.get(fieldsPrefix+'Apartment__c');
	        this.building = (String) record.get(fieldsPrefix+'Building__c');
            this.block = (String) record.get(fieldsPrefix+'Block__c');
	        this.city = (String) record.get(fieldsPrefix+'City__c');
	        this.country = (String) record.get(fieldsPrefix+'Country__c');
	        this.floor = (String) record.get(fieldsPrefix+'Floor__c');
	        this.locality = (String) record.get(fieldsPrefix+'Locality__c');
	        this.postalCode = (String) record.get(fieldsPrefix+'PostalCode__c');
	        this.province = (String) record.get(fieldsPrefix+'Province__c');
	        this.addressNormalized = (Boolean) record.get(fieldsPrefix+'AddressNormalized__c');
		    this.addressKey = (String) record.get(fieldsPrefix+'AddressKey__c');
	    }

	    public AddressDTO(Map<String, String> addressFields) {
		    for (String field : addressFields.keySet()) {
			    switch on field {
				    when 'streetId' {
					    this.streetId = addressFields.get(field);
				    }
				    when 'streetNumber' {
					    this.streetNumber = addressFields.get(field);
				    }
				    when 'streetNumberExtn' {
					    this.streetNumberExtn = addressFields.get(field);
				    }
				    when 'streetName' {
					    this.streetName = addressFields.get(field);
				    }
				    when 'streetType' {
					    this.streetType = addressFields.get(field);
				    }
				    when 'apartment' {
					    this.apartment = addressFields.get(field);
				    }
				    when 'building' {
					    this.building = addressFields.get(field);
				    }
                    when 'block' {
                        this.block = addressFields.get(field);
                    }
				    when 'city' {
					    this.city = addressFields.get(field);
				    }
				    when 'country' {
					    this.country = addressFields.get(field);
				    }
				    when 'floor' {
					    this.floor = addressFields.get(field);
				    }
				    when 'locality' {
					    this.locality = addressFields.get(field);
				    }
				    when 'postalCode' {
					    this.postalCode = addressFields.get(field);
				    }
				    when 'province' {
					    this.province = addressFields.get(field);
				    }
				    when 'addressNormalized' {
					    if (addressFields.get(field) != null) {
						    this.addressNormalized = Boolean.valueOf(addressFields.get(field));
					    }
					    else {
						    this.addressNormalized = false;
					    }
				    }
				    when 'addressKey' {
					    this.addressKey = addressFields.get(field);
				    }
				    when else {
					    throw new AddressException('Unrecognized address field: '+field);
				    }
			    }
		    }
	    }

	    public void populateRecordAddressFields(SObject record, String fieldsPrefix) {
		    record.put(fieldsPrefix+'StreetId__c', this.streetId);
		    record.put(fieldsPrefix+'StreetNumber__c', this.streetNumber);
	        record.put(fieldsPrefix+'StreetNumberExtn__c', this.streetNumberExtn);
	        record.put(fieldsPrefix+'StreetName__c', this.streetName);
	        record.put(fieldsPrefix+'StreetType__c', this.streetType);
	        record.put(fieldsPrefix+'Apartment__c', this.apartment);
	        record.put(fieldsPrefix+'Building__c', this.building);
            record.put(fieldsPrefix+'Block__c', this.block);
	        record.put(fieldsPrefix+'City__c', this.city);
	        record.put(fieldsPrefix+'Country__c', this.country);
	        record.put(fieldsPrefix+'Floor__c', this.floor);
	        record.put(fieldsPrefix+'Locality__c', this.locality);
	        record.put(fieldsPrefix+'PostalCode__c', this.postalCode);
	        record.put(fieldsPrefix+'Province__c', this.province);
	        record.put(fieldsPrefix+'AddressNormalized__c', this.addressNormalized);
		    record.put(fieldsPrefix+'AddressKey__c', this.addressKey);
	    }
    }

	@AuraEnabled
	public static void copyAddress(SObject sourceRecord, String sourceRecordFieldsPrefix, SObject destinationRecord, String destinationRecordFieldsPrefix) {
		AddressDTO addressDTO = new AddressDTO(sourceRecord, sourceRecordFieldsPrefix);
		addressDTO.populateRecordAddressFields(destinationRecord, destinationRecordFieldsPrefix);
	}

    @AuraEnabled
    public static Map<String, List<Map<String, Object>>> getAvailableAddresses(String addressLayoutQualifiedApiName, String accountId) {
        return getAvailableAddressesCountryRestricted(addressLayoutQualifiedApiName, accountId, null);
    }

    @AuraEnabled
    public static Map<String, List<Map<String, Object>>> getAvailableAddressesCountryRestricted(String addressLayoutQualifiedApiName, String accountId, String restrictToCountry) {
        Map<String, List<Map<String, Object>>> results = new Map<String, List<Map<String, Object>>>();
        if (String.isBlank(addressLayoutQualifiedApiName)) {
            addressLayoutQualifiedApiName = 'DefaultAddressLayout';
        }
        List<AddressField__mdt> addrLayout = [
                SELECT MasterLabel, QualifiedApiName, ColSize__c, Order__c, Required__c, FieldName__c
                FROM AddressField__mdt
                WHERE AddressLayout__r.QualifiedApiName = :addressLayoutQualifiedApiName
                ORDER BY Order__c ASC
        ];
        Set<String> fields = new Set<String>();
        for (AddressField__mdt field : addrLayout) {
            fields.add(field.FieldName__c);
        }

        List<AddressEntity__mdt> addrEntities = [
                SELECT MasterLabel, QualifiedApiName, SObjectApiName__c, AddressPrefix__c, QueryRelation__c, QueryCriteria__c
                FROM AddressEntity__mdt
        ];

        for (AddressEntity__mdt entity : addrEntities) {
            List<Map<String, Object>> addresses = new List<Map<String, Object>>();
            String prefix = entity.AddressPrefix__c;
            Set<String> entityFields = new Set<String>();
            for (String field : fields) {
                entityFields.add((prefix + field + '__c').toLowerCase());
            }
            entityFields.add((prefix + 'Address__c').toLowerCase());
            entityFields.add((prefix + 'AddressNormalized__c').toLowerCase());
            entityFields.add((prefix + 'StreetId__c').toLowerCase());
            if (String.isBlank(entity.QueryRelation__c)) {
                entity.QueryRelation__c = 'Id';
            }
            String query = 'SELECT ' + String.join(new List<String>(entityFields), ', ') + ' FROM ' + entity.SObjectApiName__c +
                            ' WHERE ' + entity.QueryRelation__c + ' = :accountId AND ' + prefix + 'AddressKey__c != null AND ' + prefix + 'StreetId__c != null ';
            if (!String.isBlank(restrictToCountry)) {
                query += 'AND '+(prefix + 'Country__c')+' = :restrictToCountry ';
            }
            if (!String.isBlank(entity.QueryCriteria__c)) {
                query += 'AND '+entity.QueryCriteria__c+' ';
            }
            query += 'ORDER BY ' + entity.AddressPrefix__c + 'Address__c';
            List<SObject> records = Database.query(query);
            System.debug('##### Found addresses: '+records);

            Integer i = 0;
            for (SObject record : records) {
                Map<String, Object> address = new Map<String, Object>();
                for (String field : fields) {
                    Object fieldValue = record.get(prefix + field + '__c');
                    address.put(field, fieldValue != null ? fieldValue : '');
                }
                address.put('formula', record.get(prefix + 'Address__c'));
                address.put('isNormalized', record.get(prefix + 'AddressNormalized__c'));
                address.put('key', record.get(prefix + 'AddressKey__c'));
                address.put('streetId', record.get(prefix + 'StreetId__c'));
                Boolean exist = false;
                for (Map<String, Object> addr : addresses) {
                    if (addr.get('key') == record.get(prefix + 'AddressKey__c')) {
                        exist = true;
                        break;
                    }
                }
                if (exist == false) {
                    addresses.add(address);
                }
                i++;
            }
            System.debug('##### Deduplicated addresses: '+addresses);
            results.put(entity.MasterLabel, addresses);
        }

        return results;
    }

    public City__c insertCity(Map<String, String> address) {

        City__c city = MRO_QR_Address.getCity(address.get('country'), address.get('city'), address.get('province'));

        if (city == null) {
            city = new City__c(
                Name = address.get('city'),
                Province__c = address.get('province'),
                Country__c = address.get('country')
            );
            try {
                System.debug('create or update city ' + city);
                databaseSrv.upsertSObject(city);
                System.debug('done with city ' + city);
            }
            catch (Exception e) {
                System.debug('Error: ' + e);
                String error = e.getMessage();
                if (error.contains('Province__c')) {
                    error = 'Invalid Province value';
                }
                throw new SObjectException(error);
            }
        }
        return city;
    }

    public Street__c insertStreet(City__c city, Map<String, String> address, Boolean isForceAddress) {
        if (isForceAddress == null) {
            isForceAddress = false;
        }
        Street__c street = new Street__c(City__c = city.Id);
        if (String.isNotEmpty(address.get('id'))) {
            street.Id = address.get('id');
        }
        street.Name = address.get('streetName');
        street.PostalCode__c = address.get('postalCode');
        street.StreetType__c = address.get('streetType');
        if (!isForceAddress) {
            street.Even_Odd__c = address.get('evenOdd');
            street.FromStreetNumber__c = Decimal.valueOf(address.get('fromStreetNumber'));
            street.ToStreetNumber__c = Decimal.valueOf(address.get('toStreetNumber'));
        } else {
            street.Even_Odd__c = 'B';
            street.FromStreetNumber__c = 1;
            street.ToStreetNumber__c = 999999999;
        }
        street.Verified__c = !isForceAddress;
        try {
            databaseSrv.upsertSObject(street);
        } catch (Exception e) {
            System.debug('Error: ' + e.getMessage());
            String error = e.getMessage();

            if (error.contains('StreetType__c')) {
                error = 'Invalid Street Type value' ;
            }

            if (error.contains('Even_Odd__c')) {
                error = 'Invalid Even Odd value' ;
            }

            throw new SObjectException(error);
        }
        return street;
    }

    public List<String> getAddressFields(String fieldsPrefix) {

        List<String> addressFields = new List<String>();
        addressFields.add(fieldsPrefix+'StreetId__c');
        addressFields.add(fieldsPrefix+'StreetNumber__c');
        addressFields.add(fieldsPrefix+'StreetNumberExtn__c');
        addressFields.add(fieldsPrefix+'StreetName__c');
        addressFields.add(fieldsPrefix+'StreetType__c');
        addressFields.add(fieldsPrefix+'Apartment__c');
        addressFields.add(fieldsPrefix+'Building__c');
        addressFields.add(fieldsPrefix+'Block__c');
        addressFields.add(fieldsPrefix+'City__c');
        addressFields.add(fieldsPrefix+'Country__c');
        addressFields.add(fieldsPrefix+'Floor__c');
        addressFields.add(fieldsPrefix+'Locality__c');
        addressFields.add(fieldsPrefix+'PostalCode__c');
        addressFields.add(fieldsPrefix+'Province__c');
        addressFields.add(fieldsPrefix+'AddressNormalized__c');
        addressFields.add(fieldsPrefix+'AddressKey__c');
        return addressFields;
    }

	public class AddressException extends Exception {


    }
}