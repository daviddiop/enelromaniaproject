public with sharing class AccountQueries {

    public static AccountQueries getInstance() {
        return (AccountQueries)ServiceLocator.getInstance(AccountQueries.class);
    }

    public List<Account> listAccountByVatNumber(String vatNumber) {
        return [
            SELECT Id, Name
            FROM Account
            WHERE VATNumber__c = :vatNumber
        ];
    }

    public List<Account> listByNationalId(String nationalId) {
        return [
            SELECT Id, Name, PersonContactId, PersonIndividualId
            FROM Account
            WHERE NationalIdentityNumber__pc = :nationalId
        ];
    }

    public List<Account> listByKey(String key) {
        return [
            SELECT Id, Name, PersonContactId, PersonIndividualId
            FROM Account
            WHERE Key__c = :key
        ];
    }

    public Map<Id, Account> getByIds(Set<Id> accountIds) {
        return new Map<Id, Account>([
            SELECT Id,Name, Email__c, VATNumber__c, Phone, PersonMobilePhone, PersonEmail,NationalIdentityNumber__pc,
                    LastName, FirstName, isPersonAccount, PersonIndividualId, RecordTypeId,Active__c,PersonContactId,
                    ResidentialStreetType__c, ResidentialStreetNumber__c, ResidentialStreetNumberExtn__c, ResidentialStreetName__c,
                    ResidentialProvince__c, ResidentialPostalCode__c,ResidentialFloor__c, ResidentialLocality__c,
                    ResidentialCountry__c, ResidentialCity__c, ResidentialBuilding__c, ResidentialApartment__c
            FROM Account
            WHERE Id IN :accountIds
        ]);
    }

    public Account findAccount(String accountId) {
        List<Account> accountList = [
            SELECT Id, Name, Email__c, VATNumber__c, Phone, PersonMobilePhone, PersonEmail, PersonContactId,NationalIdentityNumber__pc,
                    LastName, FirstName, isPersonAccount, Active__c, BusinessType__c, PersonIndividualId, RecordTypeId, RecordType.Name,
                    ResidentialStreetType__c, ResidentialStreetNumber__c, ResidentialStreetNumberExtn__c, ResidentialStreetName__c, ResidentialProvince__c, ResidentialPostalCode__c,
                    ResidentialFloor__c, ResidentialLocality__c, ResidentialCountry__c, ResidentialCity__c, ResidentialBuilding__c, ResidentialApartment__c,ResidentialAddressNormalized__c
            FROM Account
            WHERE Id = :accountId
            LIMIT 1
        ];
        if (accountList.isEmpty()) {
            return null;
        }
        return accountList.get(0);
    }

    public List<Account> listByIndividualId(String individualId) {
        return [
            SELECT Id, PersonContactId, PersonIndividualId, Name, RecordType.Name, BusinessType__c, VATNumber__c, NationalIdentityNumber__pc, IsPersonAccount
            FROM Account
            WHERE PersonIndividualId = :individualId
        ];
    }

    public List<Account> listByIds(List<Id> accountListIds) {
        return [
            SELECT Id
            FROM Account
            WHERE Id IN :accountListIds
        ];
    }

    public List<AccountContactRelation> listContacts (String accountId) {
        return [
                SELECT Id,AccountId, Contact.IndividualId,LegalRepresentative__c
                FROM AccountContactRelation
                WHERE AccountId = :accountId and LegalRepresentative__c = true
        ];
    }

    public List<Account> getTraders (String traderRecordTypeId) {
       return [
                SELECT Id, Name
                FROM Account
                WHERE RecordTypeId = :traderRecordTypeId
        ];
    }
}