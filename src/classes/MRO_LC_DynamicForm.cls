/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   mag 06, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_LC_DynamicForm extends ApexServiceLibraryCnt {

    public static MRO_SRV_Address addressSrv = MRO_SRV_Address.getInstance();

    @AuraEnabled(Cacheable=false)
    public static Map<String, Object> parseLabels(final Map<String, String> labels) {
        //System.debug(jsonInput);
        Map<String, Object> response = new Map<String, Object>();
        //Map<String, String> labels = asMap(jsonInput);

        /*
        for (String label : labels.keySet()) {
            Component.Apex.OutputText output = new Component.Apex.OutputText();
            output.expressions.value = '{!$Label.' + label + '}';
            labels.put(label, String.valueOf(output.value));
        }
        */
        List<String> labelsList = new List<String>(labels.keySet());
        List<String> translatedLabels = MRO_UTL_LabelTranslator.translate(labelsList, UserInfo.getLanguage());

        for (Integer i = 0; i < labelsList.size(); i++) {
            labels.put(labelsList[i], translatedLabels[i]);
        }

        response.put('labels', labels);

        return response;
    }

    @AuraEnabled(Cacheable=false)
    public static Map<String, Object> getAddress(String recordId, String objectType, String fieldsPrefix) {

        Map<String, Object> response = new Map<String, Object>();
        response.put('error', false);
        try {

            String queryString =
                'SELECT ' + String.join(addressSrv.getAddressFields(fieldsPrefix), ',') + ' ' +
                'FROM ' + objectType + ' ' +
                'WHERE Id = \'' + recordId + '\' ' +
                'LIMIT 1';
            System.debug('MRO_LC_DynamicForm.getAddress - queryString: ' + queryString);
            SObject sObj = Database.query(queryString);
            if(sObj != null) {
                sObj.put(fieldsPrefix +'Country__c','ROMANIA');
                MRO_SRV_Address.AddressDTO addressDTO =
                        new MRO_SRV_Address.AddressDTO(sObj, fieldsPrefix);
                response.put('address', addressDTO);
            }
        } catch(Exception e) {

            response.put('error', true);
            response.put('errorMessage', e.getMessage());
            response.put('errorStackTrace', e.getStackTraceString());
            System.debug('MRO_LC_DynamicForm.getAddress:  ' + e.getMessage() + e.getStackTraceString());
        }
        return response;
    }
}