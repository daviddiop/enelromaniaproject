/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   ott 23, 2020
 * @desc    
 * @history 
 */

global with sharing class MRO_ACT_StaticQueueAssignment implements wrts_prcgvr.Interfaces_1_0.IApexAction {

    public Object execute(Object param) {
        System.debug('Class: MRO_ACT_QueueAssignment -- method : execute');
        System.debug('input: ' + param);

        Map<String, Object> paramsMap = (Map<String, Object>) param;
        SObject record = (SObject) paramsMap.get('sender');
        String queueDeveloperName = (String) paramsMap.get('method');
        System.debug('record: ' + record);
        System.debug('queueDeveloperName: '+queueDeveloperName);

        Group queue = [SELECT Id FROM Group WHERE DeveloperName = :queueDeveloperName AND Type = 'Queue'];

        if(record.get('Dossier__c') != null){
            update new Dossier__c(Id = (Id)record.get('Dossier__c'), OwnerId = queue.Id);
        }

        record.put('OwnerId', queue.Id);
        paramsMap.put('sender', record);

        return paramsMap;
    }
}