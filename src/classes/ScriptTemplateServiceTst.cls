/**
 * Created by Moussa Fofana  on 05/08/2019.
 */
@isTest
public with sharing class ScriptTemplateServiceTst {
    @testSetup
    private static void setup() {
        ScriptTemplate__c myST = TestDataFactory.scriptTemplate().createScriptTemplateBuilder().build();
        insert myST;

        ScriptTemplateElement__c mySTE = TestDataFactory.scriptTemplateElement().createScriptTemplateElementBuilder().build();
        mySTE.ScriptTemplate__c	= myST.id;
        insert mySTE;
    }

    @isTest
    private static void getScriptElementsTest(){
        ScriptTemplate__c myST = [SELECT Id FROM ScriptTemplate__c LIMIT 1];
        Test.startTest();
        Map<String, String> jsonInput = new Map<String, String>{
            'templateId' => myST.Id
        };
        Object response = TestUtils.exec('ScriptTemplateService', 'getScriptElements', jsonInput, true);
        Test.stopTest();
        //System.assertEquals(true, response.size() > 0 );
    }

    @isTest
    private static void getScriptElementsExceptionTest(){
        try {
            Test.startTest();
            Map<String, String> jsonInput = new Map<String, String>{
                'templateId' => ''
            };
            Object response = TestUtils.exec('ScriptTemplateService', 'getScriptElements', jsonInput, false);
            Test.stopTest();
        } catch (Exception e) {
            System.assert(true, e);
        }
    }

    @isTest
    private static void getByScriptTemplateIdTest(){
        ScriptTemplateQueries mySTQ = ScriptTemplateQueries.getInstance();
        Test.startTest();
        List<ScriptTemplateElement__c> response = mySTQ.getByScriptTemplateId('templateId');
        Test.stopTest();
        System.assertEquals(true, response.size() == 0 );
    }


    @isTest
    private static void jsonByObjectNameAndIdTest(){

        try {
            Test.startTest();
            Map<String, String> jsonInput = new Map<String, String>{
                    'objectJsonBuilder' => 'ddd',
                    'recordId' => 'ddddd'
            };
            Object response = TestUtils.exec('PickJsonBuilder', 'getJsonByObjectNameAndId', jsonInput, false);
            Test.stopTest();
        } catch (Exception e) {
            System.assert(true, e);
        }
    }

    @isTest
    private static void jsonByObjectNameAndIdExcepTest(){

        try {
            Test.startTest();
            Map<String, String> jsonInput = new Map<String, String>{
                    'objectJsonBuilder' => null,
                    'recordId' => null
            };
            Object response = TestUtils.exec('PickJsonBuilder', 'getJsonByObjectNameAndId', jsonInput, false);
            Test.stopTest();
        } catch (Exception e) {
            System.assert(true, e);
        }
    }

    @isTest
    private static void opportunityJsonBuilderTest(){

        try {
            Test.startTest();
            OpportunityJsonBuilder a = new  OpportunityJsonBuilder();
            a.getData('recordTest');
            Test.stopTest();
        } catch (Exception e) {
            System.assert(true, e);
        }
    }

}