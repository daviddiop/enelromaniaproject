/**
 * Created by napoli on 18/11/2019.
 */

public with sharing class MRO_SRV_Account {

    private static final ContactQueries contactQuerySrv = ContactQueries.getInstance();
    private static final MRO_QR_Account accountQuery = MRO_QR_Account.getInstance();
    private static final MRO_QR_Interaction interactionQuery = MRO_QR_Interaction.getInstance();
    private static final IndividualQueries individualQuery = IndividualQueries.getInstance();
    private static final MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();

    private static final String PERSON_RT = 'Person';

    static MRO_UTL_Constants constantSrv = MRO_UTL_Constants.getAllConstants();

    private static final CustomerInteractionService customerInteractionSrv = CustomerInteractionService.getInstance();
    private static final MRO_SRV_Contact contactSrv = MRO_SRV_Contact.getInstance();
    private static final AccountContactRelationService accountContactRelationSrv = AccountContactRelationService.getInstance();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();

    public static MRO_SRV_Account getInstance() {
        return (MRO_SRV_Account)ServiceLocator.getInstance(MRO_SRV_Account.class);
    }

    public List<Account> searchAccount(MRO_SRV_Account.AccountDTO accountObj) {
        List<QueryBuilder.Condition> conditionList = new List<QueryBuilder.Condition>();
        if(String.isNotBlank(accountObj.Name)) {
            conditionList.add(new QueryBuilder.FieldCondition().likeOp(Account.Name, accountObj.Name));
        }
        if(String.isNotBlank(accountObj.Email)) {
            conditionList.add(new QueryBuilder.LogicCondition().orOp(new List<QueryBuilder.Condition>{
                new QueryBuilder.FieldCondition().likeOp(Account.Email__c, accountObj.Email),
                new QueryBuilder.FieldCondition().likeOp(Account.PersonEmail, accountObj.Email)
            }));
        }
        if(String.isNotBlank(accountObj.Key)) {
            conditionList.add(new QueryBuilder.FieldCondition().likeOp(Account.Key__c, accountObj.Key));
        }
        if(String.isNotBlank(accountObj.Phone)) {
            conditionList.add(new QueryBuilder.LogicCondition().orOp(new List<QueryBuilder.Condition>{
                new QueryBuilder.FieldCondition().likeOp(Account.Phone, accountObj.Phone),
                new QueryBuilder.FieldCondition().likeOp(Account.PersonMobilePhone, accountObj.Phone)
            }));
        }
        if(String.isNotBlank(accountObj.AccountNumber)) {
            conditionList.add(new QueryBuilder.FieldCondition().likeOp(Account.IntegrationKey__c , accountObj.AccountNumber));
        }

        if(String.isNotBlank(accountObj.RecordTypeDeveloperName)) {
            conditionList.add(new QueryBuilder.FieldCondition().eqOp(Account.RecordTypeId , Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(accountObj.RecordTypeDeveloperName).getRecordTypeId()));
        }

        Boolean additionalConditionApplied = false;
        Set<Id> accountIds = new Set<Id>();
        if (String.isNotBlank(accountObj.billingAccountNumber)) {
            additionalConditionApplied = true;
            List<ContractAccount__c> contractAccountList = contractAccountQuery.listByBillingAccountNumber(accountObj.billingAccountNumber);
            accountIds = SobjectUtils.setIdByField(contractAccountList, ContractAccount__c.Account__c);
        }

        List<QueryBuilder.Condition> servicePointConditionList = new List<QueryBuilder.Condition>();
        if (String.isNotBlank(accountObj.eneltel)) {
            additionalConditionApplied = true;
            servicePointConditionList.add(new QueryBuilder.FieldCondition().eqOp(ServicePoint__c.ENELTEL__c, accountObj.eneltel));
        }
        if (String.isNotBlank(accountObj.servicePointCode)) {
            additionalConditionApplied = true;
            servicePointConditionList.add(new QueryBuilder.FieldCondition().eqOp(ServicePoint__c.Code__c, accountObj.servicePointCode));
        }
        if (servicePointConditionList.size() > 0) {
            List<ServicePoint__c> servicePointList = QueryBuilder.getInstance()
                .setObject(ServicePoint__c.SObjectType)
                .setFields(new List<SObjectField>{ServicePoint__c.Account__c})
                .setCondition(servicePointConditionList, QueryBuilder.AND_OPERATOR)
                .execute();
            if (servicePointList.size() > 0) {
                accountIds.addAll(SobjectUtils.setIdByField(servicePointList, ServicePoint__c.Account__c));
            }
        }

        if (accountIds.size() > 0) {
            conditionList.add(new QueryBuilder.FieldCondition().InOp(Account.Id, accountIds));
        }

        if (conditionList.isEmpty() || (additionalConditionApplied && accountIds.isEmpty())) {
            return new List<Account>();
        }

        List<Account> accountList = QueryBuilder.getInstance()
            .setObject(Account.SObjectType)
            .setFields(new List<SObjectField>{
                Account.Id, Account.Name, Account.FirstName, Account.LastName, Account.Email__c, Account.PersonEmail,
                Account.Key__c, Account.VATNumber__c, Account.NationalIdentityNumber__pc, Account.Phone,
                Account.PersonIndividualId, Account.PersonMobilePhone, Account.IsPersonAccount, Account.BusinessType__c, Account.Active__c,
                Account.ResidentialCity__c, Account.ResidentialStreetName__c, Account.ResidentialStreetType__c,Account.ResidentialStreetNumber__c
            })
            .setCondition(conditionList, QueryBuilder.AND_OPERATOR)
            .execute();
        return accountList;
    }

    public Account createCustomer(Account accountObj, String interactionId, String role, String recordTypeId) {
        Savepoint savePoint = Database.setSavepoint();
        checkDuplicates(accountObj);

        try {

            Boolean isCustomerAndInterlocutor = false;
            System.debug('accountObj = ' + JSON.serialize(accountObj));
            System.debug('recordTypeId ' + recordTypeId);
            accountObj.RecordTypeId = recordTypeId;
            databaseSrv.insertSObject(accountObj);

            System.debug('accountObj = ' + JSON.serialize(accountObj));

            accountObj = accountQuery.findAccount(accountObj.Id);

            Interaction__c interaction = interactionId != null ? interactionQuery.findInteractionById(interactionId) : null;

            if(accountObj.IsPersonAccount) {
                if(interaction != null && interaction.Interlocutor__c != null && interaction.Interlocutor__r.NationalIdentityNumber__c == accountObj.NationalIdentityNumber__pc) {
                    accountObj.PersonIndividualId = interaction.Interlocutor__c;
                    isCustomerAndInterlocutor = true;
                } else {
                    List<Individual> existingIndividuals = individualQuery.findIndividualsByNINumber(accountObj.NationalIdentityNumber__pc);
                    if(existingIndividuals.isEmpty()) {
                        MRO_SRV_Individual.Interlocutor interlocutor = new MRO_SRV_Individual.Interlocutor();
                        interlocutor.firstName = accountObj.FirstName;
                        interlocutor.lastName = accountObj.LastName;
                        interlocutor.nationalId = accountObj.NationalIdentityNumber__pc;
                        interlocutor.foreignCitizen = accountObj.ForeignCitizen__pc;
                        //FF Customer Creation - Pack1/2 - Interface Check
                        interlocutor.gender = accountObj.Gender__pc;
                        interlocutor.birthDate = accountObj.PersonBirthdate;
                        interlocutor.birthCity = accountObj.BirthCity__pc;
                        interlocutor.phone = accountObj.Phone;
                        interlocutor.email = accountObj.Email__c;
                        interlocutor.contactChannel = accountObj.ContactChannel__pc;
                        interlocutor.mobile = accountObj.PersonMobilePhone;
                        interlocutor.iDDocEmissionDate = accountObj.IDDocEmissionDate__pc;
                        interlocutor.iDDocValidityDate = accountObj.IDDocValidityDate__pc;
                        interlocutor.iDDocumentNr = accountObj.IDDocumentNr__pc;
                        interlocutor.iDDocumentType = accountObj.IDDocumentType__pc;
                        interlocutor.iDDocumentSeries = accountObj.IDDocumentSeries__pc;
                        interlocutor.iDDocIssuer = accountObj.IDDocIssuer__pc;

                        //FF Customer Creation - Pack1/2 - Interface Check


                        Individual newIndividual = MRO_SRV_Individual.getInstance().insertInterlocutor(interlocutor, true);
                        accountObj.PersonIndividualId = newIndividual.Id;
                    } else {
                        accountObj.PersonIndividualId = existingIndividuals[0].Id;
                    }
                }
                databaseSrv.updateSObject(accountObj);
            }

            if(interaction == null) {
                return accountObj;
            }

            Id contactId;

            if(interaction.Interlocutor__c != null) {
                //System.debug('accountObj1111' + accountObj);
                Boolean createAccountContactRelation = true;

                if(accountObj.IsPersonAccount) {
                    if(isCustomerAndInterlocutor) {
                        contactId = accountObj.PersonContactId;
                        createAccountContactRelation = false;
                    } else { //Customer is not the same person as the interlocutor
                        List<Account> individualPersonAccount = accountQuery.listByIndividualId(interaction.Interlocutor__c);
                        if(individualPersonAccount.isEmpty()) {
                            Account newPersonAccount = insertPersonalAccount(interaction.Interlocutor__r);
                            newPersonAccount = accountQuery.findAccount(newPersonAccount.Id);
                            contactId = newPersonAccount.PersonContactId;
                        } else {
                            //Should never exist more than a PersonAccount related to the same Individual. Let's pick the first one from the list:
                            contactId = individualPersonAccount[0].PersonContactId;
                        }
                    }
                } else {
                    List<Contact> contactList = contactQuerySrv.listContactByIndividualId(interaction.Interlocutor__c, false);
                    if(contactList.isEmpty()) {
                        Contact directContact = contactSrv.insertForIndividual(interaction.Interlocutor__r, accountObj.Id);
                        contactId = directContact.Id;
                        createAccountContactRelation = false;
                    } else {
                        //Should never exist more than a Contact (no PersonAccount) related to the same Individual. Let's pick the first one from the list:
                        contactId = contactList[0].Id;
                    }
                }

                if(createAccountContactRelation) {
                    accountContactRelationSrv.addAccountContactRelation(accountObj.Id, contactId, role);
                }
            } else if(accountObj.IsPersonAccount) {
                interaction.Interlocutor__c = accountObj.PersonIndividualId;
                databaseSrv.updateSObject(interaction);
            }

            CustomerInteraction__c customerInteraction = customerInteractionSrv.insertCustomerInteraction(accountObj.Id, interactionId, contactId);

        } catch (Exception exp) {
            Database.rollback(savePoint);
            throw exp;
        }

        return accountObj;
    }

    private void checkDuplicates(Account accountObj) {
        List<Account> accountWithSameKey = new List<Account>();
        String key = accountObj.Key__c != null ? accountObj.Key__c : (accountObj.IsPersonAccount ? accountObj.NationalIdentityNumber__pc : accountObj.VATNumber__c);
        accountWithSameKey = accountQuery.listByKey(key);
        if(accountWithSameKey.size() > 0 && accountObj.Id != accountWithSameKey.get(0).Id) {
            String message = '';
            if (accountObj.IsPersonAccount) {
                message = System.Label.NationalIdentityNumber + ' ' + System.Label.AlreadyExist;
            } else {
                message = System.Label.VATNumber + ' ' + System.Label.AlreadyExist;
            }
            throw new WrtsException(message);
        }
    }

    public Account updateAccount(Account accountObj){
        checkDuplicates(accountObj);
        accountObj.ResidentialAddressNormalized__c = true;
        databaseSrv.updateSObject(accountObj);
        return accountObj;
    }

    public Account insertPersonalAccount(Individual individualObj) {
        Map<String, String> mapTechnicalDetails = new Map<String, String>();
        if (individualObj.TechnicalDetails__c != null) {
             mapTechnicalDetails = (Map<String, String>) JSON.deserialize(individualObj.TechnicalDetails__c, Map<String, String>.class);
        }
        Id personAccountRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(PERSON_RT).getRecordTypeId();
        Account newAcct =  new Account(
            FirstName = individualObj.FirstName,
            LastName = individualObj.LastName,
            NationalIdentityNumber__pc = individualObj.NationalIdentityNumber__c,
            PersonIndividualId = individualObj.Id,
            RecordTypeId = personAccountRtId,
                //FF Customer Creation - Pack1/2 - Interface Check
            Gender__pc = individualObj.Gender__c,
            BirthCity__pc = individualObj.BirthCity__c,
            PersonBirthdate = individualObj.BirthDate,
            /*ContactChannel__pc = mapTechnicalDetails.get('ContactChannel'),
            PersonMobilePhone = mapTechnicalDetails.get('Mobile'),
            Phone = mapTechnicalDetails.get('Phone'),
            PersonEmail = mapTechnicalDetails.get('Email'),*/
            ForeignCitizen__pc = individualObj.ForeignCitizen__c,
            IDDocEmissionDate__pc = individualObj.IDDocEmissionDate__c,
            IDDocValidityDate__pc = individualObj.IDDocValidityDate__c,
            IDDocumentNr__pc      = individualObj.IDDocumentNr__c,
            IDDocumentType__pc    = individualObj.IDDocumentType__c,
            IDDocumentSeries__pc  = individualObj.IDDocumentSeries__c,
            IDDocIssuer__pc       = individualObj.IDDocIssuer__c
                //FF Customer Creation - Pack1/2 - Interface Check
        );
        if(!mapTechnicalDetails.isEmpty()){
            newAcct.ContactChannel__pc = mapTechnicalDetails.get('ContactChannel');
            newAcct.PersonMobilePhone = mapTechnicalDetails.get('Mobile');
            newAcct.Phone = mapTechnicalDetails.get('Phone');
            newAcct.PersonEmail = mapTechnicalDetails.get('Email');
        }
        databaseSrv.insertSObject(newAcct);
        return newAcct;
    }

    public Map<String, Object> saveRegistryChange(RegistryChangeWizardCnt.InputData inputData){
        List<Case> caseLists = new List<Case>();
        Map<String, Object> response = new Map<String, Object>();
        Map<String, Schema.RecordTypeInfo> caseRts = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo registryChangeFast = caseRts.get('RegistryChangeFast');
        Schema.RecordTypeInfo registryChangeSlow = caseRts.get('RegistryChangeSlow');

        Dossier__c dossier = new Dossier__c(
                Account__c = inputData.accountId,
                CompanyDivision__c = inputData.companyDivisionId,
                RecordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get('Change').getRecordTypeId()
        );
        databaseSrv.insertSObject(dossier);
        System.debug('slowCase'+inputData.slowCase);
        System.debug('fastCase'+inputData.fastCase);
        if(inputData.fastCase != null){
            inputData.fastCase.RecordTypeId = registryChangeFast.getRecordTypeId();
            inputData.fastCase.AccountId = inputData.accountId;
            inputData.fastCase.Origin  = constantSrv.CASE_DEFAULT_CASE_ORIGIN;
            inputData.fastCase.Dossier__c  = dossier.Id;
            inputData.fastCase.CompanyDivision__c  = dossier.CompanyDivision__c;
            caseLists.add(inputData.fastCase);
        }
        if(inputData.slowCase != null){
            inputData.slowCase.RecordTypeId = registryChangeSlow.getRecordTypeId();
            inputData.slowCase.AccountId = inputData.accountId;
            inputData.slowCase.Origin  = constantSrv.CASE_DEFAULT_CASE_ORIGIN;
            inputData.slowCase.Dossier__c  = dossier.Id;
            inputData.slowCase.CompanyDivision__c  = dossier.CompanyDivision__c;
            inputData.slowCase.AddressAddressNormalized__c  = inputData.residentialAddressForced;
            caseLists.add(inputData.slowCase);
        }
        System.debug('slowCase2'+inputData.slowCase);
        System.debug('fastCase2'+inputData.fastCase);
        if(caseLists.size() >0 ){
            databaseSrv.insertSObject(caseLists);
            response.put('caseLists', caseLists);
            response.put('dossierId', dossier.Id);
            response.put('error', false);
        } else {
            response.put('error', true);
        }
        return response;
    }

    public void updateConsumerTypeOnAccounts(Set<Id> accountIds) {
        if (!accountIds.isEmpty()) {
            List<Account> accounts = accountQuery.listByIdsWithServicePoints(accountIds, new Set<String>{constantSrv.COMMODITY_ELECTRIC});
            List<Account> accountsToUpdate = new List<Account>();
            for (Account acc : accounts) {
                Integer largeBusinessCount = 0;
                Integer smallBusinessCount = 0;
                String newConsumerType;
                for (ServicePoint__c servicePoint : acc.ServicePoints__r) {
                    if (servicePoint.CurrentSupply__c != null &&
                        (servicePoint.CurrentSupply__r.Status__c == constantSrv.SUPPLY_STATUS_ACTIVE || servicePoint.CurrentSupply__r.Status__c == constantSrv.SUPPLY_STATUS_TERMINATING)) {
                        if (servicePoint.ConsumerType__c == constantSrv.CONSUMERTYPE_LARGE_BUSINESS) {
                            largeBusinessCount++;
                        }
                        else if (servicePoint.ConsumerType__c == constantSrv.CONSUMERTYPE_SMALL_BUSINESS) {
                            smallBusinessCount++;
                        }
                    }
                }
                if (acc.ServicePoints__r.isEmpty()) {
                    continue;
                }
                if (largeBusinessCount > 0) {
                    newConsumerType = constantSrv.CONSUMERTYPE_LARGE_BUSINESS;
                }
                else if (smallBusinessCount > 0) {
                    newConsumerType = constantSrv.CONSUMERTYPE_SMALL_BUSINESS;
                }
                else {
                    newConsumerType = constantSrv.CONSUMERTYPE_RESIDENTIAL;
                }
                if (newConsumerType != acc.ConsumerType__c) {
                    acc.ConsumerType__c = newConsumerType;
                    accountsToUpdate.add(acc);
                }
            }
            databaseSrv.updateSObject(accountsToUpdate);
        }
    }

    public class AccountDTO {
        public String Name {get; set;}
        public String Email {get; set;}
        public String Key {get; set;}
        public String Phone {get; set;}
        public String AccountNumber {get; set;}
        public String eneltel {get; set;}
        public String servicePointCode {get; set;}
        public String billingAccountNumber {get; set;}
        public String RecordTypeDeveloperName {get; set;}
    }
}