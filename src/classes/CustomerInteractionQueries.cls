/**
 * Created by goudiaby on 06/03/2019.
 */

public with sharing class CustomerInteractionQueries {
    
    public static CustomerInteractionQueries getInstance() {
        return new CustomerInteractionQueries();
    }

    /**
    * @author INSA BADJI
    * @description  Method to get Customer Interaction From account Id
    *
    * @param accountId Id of Account
    * @param  customerInteractionId Id of Customer
    */

    public List<CustomerInteraction__c> listByInteractionIdAndAccountId(String accountId, String interactionId) {
        return [
                SELECT Id, Name, Interaction__c, Customer__c, Contact__c, Interaction__r.Interlocutor__c
                FROM CustomerInteraction__c
                WHERE Interaction__c = :interactionId
                AND Interaction__r.Status__c = 'New'
                AND Customer__c = :accountId
        ];
    }

    public List<CustomerInteraction__c> getInterlocutorByCustomerInteractionId(String customerInteractionId) {
        return [
                SELECT Id, Name, Interaction__c, Customer__c, Contact__c, Interaction__r.Interlocutor__c
                FROM CustomerInteraction__c
                WHERE Id = :customerInteractionId
                AND Interaction__r.Status__c = 'New'
        ];
    }



    /**
    * @author Moussa Fofana
    * @description  Method to get Customer Interaction From Id and  accountId
    * @created at 26/07/2019.
    * @param  accountId Id of Account
    * @param  customerInteractionId Id of CustomerInteraction
    */
    public List<CustomerInteraction__c> getByIdAndAccountId(String accountId, String customerInteractionId) {
        return [
            SELECT Id, Name, Interaction__c, Customer__c
            FROM CustomerInteraction__c
            WHERE Id = :customerInteractionId
            AND Customer__c = :accountId
        ];
    }
    
    /**
     * Query to retrieve a Customer interaction by Interaction Id and Customer
     *
     * @param interactionId
     * @param customerId
     *
     * @return List<CustomerInteraction__c> (1) if exists otherwise a empty list
     */
    public List<CustomerInteraction__c> listCustomerInteractionByInteractionIdAndAccountId(String interactionId, String accountId) {
        return [
            SELECT Id, Name, Interaction__c, Customer__c, Interaction__r.Interlocutor__c, Customer__r.ResidentialCity__c,
                Customer__r.ResidentialStreetName__c, Customer__r.ResidentialStreetType__c, Customer__r.ResidentialAddress__c
            FROM CustomerInteraction__c
            WHERE Interaction__c = :interactionId
            AND Customer__c = :accountId
            LIMIT 1
        ];
    }
    
    /**
     * Query to retrieve a Customer interaction by Interaction Id
     *
     * @param interactionId
     * @return List<CustomerInteraction__c>
     */
    public List<CustomerInteraction__c> listByInteractionId(String interactionId) {
        return [
            SELECT Id, Name, Interaction__c, Interaction__r.Interlocutor__c, Customer__c, Contact__c,
                Customer__r.Name, Customer__r.VATNumber__c, Customer__r.NationalIdentityNumber__pc,
                Customer__r.IsPersonAccount,Customer__r.PersonIndividualId,Customer__r.ResidentialStreetName__c,Customer__r.ResidentialStreetNumber__c,
                Customer__r.ResidentialCity__c, Customer__r.ResidentialStreetType__c, Customer__r.ResidentialAddress__c
            FROM CustomerInteraction__c
            WHERE Interaction__c = :interactionId
            AND Customer__c != NULL
            LIMIT 100
        ];
    }
    
    public List<CustomerInteraction__c> listCustomerInteraction(String interactionId) {
        return [
            SELECT Id, Name, Customer__c, Contact__c, Interaction__c,
                Customer__r.Name, Customer__r.VATNumber__c, Customer__r.IsPersonAccount,
                Customer__r.NationalIdentityNumber__pc, Customer__r.PersonContactId, Interaction__r.Interlocutor__c
            FROM CustomerInteraction__c
            WHERE Interaction__c = :interactionId
        ];
    }
    
    public CustomerInteraction__c findById(String customerInteractionId) {
        List<CustomerInteraction__c> customerInteractionList = [
            SELECT Id, Name, Interaction__c, Customer__c, Contact__c,
                Customer__r.Name, Customer__r.VATNumber__c, Customer__r.NationalIdentityNumber__pc, Customer__r.IsPersonAccount
            FROM CustomerInteraction__c
            WHERE Id = :customerInteractionId
            LIMIT 1
        ];
        if (customerInteractionList.isEmpty()) {
            return null;
        }
        return customerInteractionList.get(0);
    }
}