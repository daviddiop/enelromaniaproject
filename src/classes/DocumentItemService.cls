/**
 * Created by goudiaby on 01/04/2019.
 */

public with sharing class DocumentItemService {

    public static DocumentItemService getInstance() {
        return (DocumentItemService)ServiceLocator.getInstance(DocumentItemService.class);
    }
    
    public DocumentItemDTO mapToDto(DocumentItem__c documentItemObj){
        DocumentItemDTO docItemDTO = new DocumentItemDTO(documentItemObj);
        return docItemDTO;
    }

    public class DocumentItemDTO {
        @AuraEnabled
        public String id { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public Boolean isMandatory { get; set; }
        @AuraEnabled
        public String validatableDocumentField { get; set; }

        public DocumentItemDTO(){}
        public DocumentItemDTO(DocumentItem__c documentItemObj) {
            this.id = documentItemObj.Id;
            this.name = documentItemObj.Name;
            this.isMandatory = documentItemObj.Mandatory__c;
            this.validatableDocumentField = documentItemObj.ValidatableDocumentField__c;
        }
    }
}