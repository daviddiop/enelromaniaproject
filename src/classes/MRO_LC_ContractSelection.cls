/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   feb 28, 2020
 * @desc    
 * @history 
 */

public with sharing class MRO_LC_ContractSelection extends ApexServiceLibraryCnt {
    private static MRO_QR_Contract contractQuery = MRO_QR_Contract.getInstance();

    /**
     * get Active Contracts by Account
     */
    public class getContractsByAccount extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            System.debug('### params: '+params);
            String accountId = params.get('accountId');
            String companyDivisionId = params.get('companyDivisionId');
            String commodity = params.get('commodity');
            String market = params.get('market');
            String includeDraft = params.get('includeDraft');
            String contractType = params.get('contractType');
            String tariffType = params.get('tariffType');
            Boolean fixedContract = (params.get('fixedContract') == null) ? false : Boolean.valueOf(params.get('fixedContract'));
            String contractId = params.get('contractId');
            if (contractId == 'new') {
                contractId = null;
            }
            String contractIdListString = params.get('contractIdList');
            String commodityToExclude = params.get('commodityToExclude');
            String marketToExclude = params.get('marketToExclude');
            System.debug('contractId: '+contractId);
            System.debug('contractIdListString: '+contractIdListString);
            List<String> contractIdList = new List<String>();
            if (String.isNotBlank(contractIdListString)){
                contractIdList = (List<String>) JSON.deserialize(contractIdListString, List<String>.class);
                System.debug('contractIdList: '+contractIdList);
            }
            String excludeContractIdsString = params.get('excludeContractIds');
            Set<String> excludeContractIds = new Set<String>();
            if (String.isNotBlank(excludeContractIdsString)){
                excludeContractIds = (Set<String>)JSON.deserialize(excludeContractIdsString, Set<String>.class);
            }

            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }

            List<Contract> contracts;
            if (fixedContract){
                if(String.isNotBlank(contractId)){
                    Id cId =  Id.valueOf(contractId);
                    Contract supplyContract = contractQuery.getById(cId);
                    if(supplyContract != null){
                        contracts = new List<Contract>();
                        contracts.add(supplyContract);
                        System.debug('contracts: '+contracts);
                    }
                }
                else{
                    contracts = contractQuery.getActiveContractsByAccountAndIds(accountId, contractIdList);
                }
            } else {
                contracts = contractQuery.getActiveContractsByAccount(accountId, companyDivisionId, contractType, commodity,
                                                                      market, tariffType, includeDraft == 'true',
                                                                      excludeContractIds, commodityToExclude, marketToExclude);
            }

            response.put('contracts', contracts);
            response.put('error', false);
            return response;
        }
    }
}