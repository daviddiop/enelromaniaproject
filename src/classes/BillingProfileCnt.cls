public with sharing class BillingProfileCnt extends ApexServiceLibraryCnt {
    static BillingProfileQueries billingProfileQuery = BillingProfileQueries.getInstance();
    static SupplyQueries supplyQuery = SupplyQueries.getInstance();
    static List<ApexServiceLibraryCnt.Option> recordTypePicklist  = new List<ApexServiceLibraryCnt.Option>();

    public class getAddressFields extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();

            Map<String, String> params = asMap(jsonInput);
            String billingProfileRecordId = params.get('billingProfileRecordId');
            if (String.isBlank(billingProfileRecordId)) {
                throw new WrtsException(System.Label.BillingProfile + ' - ' + System.Label.MissingId);
            }
            BillingProfile__c billingProfile = billingProfileQuery.getById(billingProfileRecordId);
            if (billingProfile != null) {
                AddressCnt.AddressDTO addressFromBilling = BillingProfileCnt.copyBillingAddressToDTO(billingProfile);
                response.put('billingAddress', addressFromBilling);
            }
            return response;
        }
    }

    public with sharing class getActiveSupplies extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            String billingProfileRecordId = params.get('billingProfileRecordId');
            if (String.isBlank(billingProfileRecordId)) {
                throw new WrtsException(System.Label.BillingProfile + ' - ' + System.Label.MissingId);
            }

            List<Supply__c> supply = supplyQuery.getActiveSuppliesByBillingProfile(billingProfileRecordId);
            response.put('enableEditButtonBP', supply.isEmpty());
            return response;
        }
    }

    public with sharing class getBillingProfileRecords extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);
            String accountId = params.get('accountId');
            if (String.isBlank(accountId)) {
                throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
            }
            List<BillingProfile__c> listBillingProfile = billingProfileQuery.getByAccountId(accountId);
            response.put('listBillingProfile', listBillingProfile);
            return response;
        }
    }

    public static AddressCnt.AddressDTO copyBillingAddressToDTO(BillingProfile__c billingProfile) {
        AddressCnt.AddressDTO addressDTO = new AddressCnt.AddressDTO();
        addressDTO.streetNumber = billingProfile.BillingStreetNumber__c;
        addressDTO.streetNumberExtn = billingProfile.BillingStreetNumberExtn__c;
        addressDTO.streetName = billingProfile.BillingStreetName__c;
        addressDTO.streetType = billingProfile.BillingStreetType__c;
        addressDTO.apartment = billingProfile.BillingApartment__c;
        addressDTO.building = billingProfile.BillingBuilding__c;
        addressDTO.city = billingProfile.BillingCity__c;
        addressDTO.country = billingProfile.BillingCountry__c;
        addressDTO.floor = billingProfile.BillingFloor__c;
        addressDTO.locality = billingProfile.BillingLocality__c;
        addressDTO.postalCode = billingProfile.BillingPostalCode__c;
        addressDTO.province = billingProfile.BillingProvince__c;
        addressDTO.addressNormalized = billingProfile.BillingAddressNormalized__c;
        return addressDTO;
    }
}