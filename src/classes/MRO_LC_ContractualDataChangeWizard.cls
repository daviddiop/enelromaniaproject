/**
 * Created by Stefano on 03/04/2020.
 */

public with sharing class MRO_LC_ContractualDataChangeWizard extends ApexServiceLibraryCnt  {
    private static MRO_SRV_Dossier      dossierSrv           = MRO_SRV_Dossier.getInstance();
    private static MRO_UTL_Constants    constantsSrv         = MRO_UTL_Constants.getAllConstants();
    private static MRO_QR_Account       accQuery             = MRO_QR_Account.getInstance();
    private static MRO_QR_Case          caseQuery            = MRO_QR_Case.getInstance();
    private static MRO_SRV_Case         caseSrv              = MRO_SRV_Case.getInstance();
    private static MRO_QR_Supply        supplyQuery          = MRO_QR_Supply.getInstance();
    private static MRO_SRV_ScriptTemplate scriptTemplatesrv = MRO_SRV_ScriptTemplate.getInstance();
    private static MRO_QR_ScriptTemplate scriptTemplateQuery = MRO_QR_ScriptTemplate.getInstance();
    private static MRO_SRV_Consumption  consumptionSrv       = MRO_SRV_Consumption.getInstance();
    private static MRO_SRV_DatabaseService  databaseSrv      = MRO_SRV_DatabaseService.getInstance();
    private static MRO_QR_ContractAccount contractAccountQuery = MRO_QR_ContractAccount.getInstance();
    private static MRO_QR_Contract contractQuery             = MRO_QR_Contract.getInstance();
    private static MRO_QR_BillingProfile billingProfileQuery = MRO_QR_BillingProfile.getInstance();
    private static String                   dossierNewStatus = constantsSrv.DOSSIER_STATUS_NEW;
    private static final String         REQUEST_TYPE         = constantsSrv.REQUEST_TYPE_CONTRACTUAL_DATA_CHANGE;
    private static final String CASE_RECORD_TYPE             = constantsSrv.CASE_RECORD_TYPE_CONTRACTUAL_DATA_CHANGE;
    private static MRO_SRV_BillingProfile billingProfileSrv = MRO_SRV_BillingProfile.getInstance();
    private static MRO_QR_Dossier       dossierQuery         = MRO_QR_Dossier.getInstance();


    public with sharing class initialize extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            try{
                String accountId            = params.get('accountId');
                String interactionId        = params.get('interactionId');
                String dossierId            = params.get('dossierId');
                String genericRequestId = params.get('genericRequestId');
                String gdprParentDossierId = params.get('gdprParentDossierId');
                String companyDivisionId    = params.get('companyDivisionId');
                String templateId = params.get('templateId');
                String parentATRCaseId = params.get('parentATRCaseId');
                String recordTypeId = Schema.SObjectType.Dossier__c.getRecordTypeInfosByDeveloperName().get(MRO_UTL_Constants.CHANGE).getRecordTypeId();
                List<Case> cases= new List<Case>();
                Schema.RecordTypeInfo caseRecordTypeInfo = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(CASE_RECORD_TYPE);
                Id caseRecordTypeId = caseRecordTypeInfo.getRecordTypeId();

                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }

                Account account = accQuery.findAccount(accountId);
                Dossier__c dossier = dossierSrv
                        .generateDossier(accountId,dossierId,
                                interactionId,companyDivisionId,
                                recordTypeId, REQUEST_TYPE);

                if(!String.isBlank(genericRequestId)){
                    dossierSrv.updateParentGenericRequest(dossier.Id,genericRequestId);
                }

                if (parentATRCaseId != null) {
                    dossier = generateContractualDataChangeFromATRExtension(parentATRCaseId, caseRecordTypeInfo, dossier);
                }

                if (gdprParentDossierId != null) {
                    dossierSrv.updateParent(dossier.Id, gdprParentDossierId);
                }

                if(!String.isBlank(dossierId)){
                    cases = caseQuery.getCasesByDossierId(dossierId);
                }

                response.put('caseId',          '');                        //TEST
                response.put('dossierId',       dossier.Id);
                response.put('dossier',         dossier);
                response.put('accountId',       accountId);
                response.put('account',         account);
                response.put('isPersonAccount', account.IsPersonAccount);
                response.put('cases', cases);
                response.put('recordType', caseRecordTypeId);
                response.put('error', false);

                String code = 'ContractualDataChangeWizardCodeTemplate_1';
                ScriptTemplate__c scriptTemplate = scriptTemplateQuery.getScriptTemplateByCode(code);
                if (scriptTemplate != null && String.isNotBlank(scriptTemplate.Id)) {
                    response.put('templateId', scriptTemplate.Id);
                }

            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }

        public Dossier__c generateContractualDataChangeFromATRExtension(String parentATRCaseId, Schema.RecordTypeInfo caseRecordTypeInfo, Dossier__c dossier) {
            Case parentCase = caseQuery.getById(parentATRCaseId);
            Dossier__c parentDossier = dossierQuery.getById(parentCase.Dossier__c);

            updateCDCFromATRExtensionCase(dossier, parentCase, caseRecordTypeInfo);
            dossier = updateCDCFromATRExtensionDossier(parentDossier, dossier);

            return dossier;
        }

        private Case updateCDCFromATRExtensionCase(Dossier__c dossier, Case parentCase, Schema.RecordTypeInfo caseRecordTypeInfo) {
            Case contractualDataChange = new Case();

            contractualDataChange.Origin = parentCase.Origin;
            contractualDataChange.Channel__c = parentCase.Channel__c;
            contractualDataChange.Supply__c = parentCase.Supply__c;
            contractualDataChange.Supply__r = parentCase.Supply__r;
            contractualDataChange.Parent = parentCase;
            contractualDataChange.ParentId = parentCase.Id;
            contractualDataChange.Dossier__c = dossier.Id;
            contractualDataChange.Dossier__r = dossier;
            contractualDataChange.SubProcess__c = constantsSrv.SUB_PROCESS_CHNG_CONTRACTUAL_VALIDITY;
            contractualDataChange.EndDate__c = parentCase.Supply__r.ServicePoint__r.ATRExpirationDate__c;
            contractualDataChange.RecordTypeId = caseRecordTypeInfo.getRecordTypeId();
            contractualDataChange.Account = dossier.Account__r;
            contractualDataChange.AccountId = dossier.Account__c;
            contractualDataChange.CaseTypeCode__c = 'C75';

            databaseSrv.insertSObject(contractualDataChange);
            return contractualDataChange;
        }

        private Dossier__c updateCDCFromATRExtensionDossier(Dossier__c parentDossier, Dossier__c dossier) {
            dossier.Origin__c = parentDossier.Origin__c;
            dossier.Channel__c = parentDossier.Channel__c;
            dossier.SendingChannel__c = parentDossier.SendingChannel__c;
            dossier.MailChannelType__c = parentDossier.MailChannelType__c;
            dossier.Phone__c = parentDossier.Phone__c;
            dossier.Email__c = parentDossier.Email__c;
            dossier.Parent__c = parentDossier.Id;
            dossier.Parent__r = parentDossier;
            dossier.SubProcess__c = constantsSrv.SUB_PROCESS_CHNG_CONTRACTUAL_VALIDITY;

            databaseSrv.updateSObject(dossier);
            return dossier;
        }
    }

    public with sharing class setChannelAndOrigin extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            try {
                Map<String, String> params = asMap(jsonInput);
                String dossierId = params.get('dossierId');
                String origin = params.get('origin');
                String channel = params.get('channel');
                dossierSrv.updateDossierChannel( dossierId, channel, origin);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }



    public with sharing class validateSubProcess extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, Object> response = new Map<String, Object>();
            Map<String, String> params = asMap(jsonInput);

            try {
                String dossierId = params.get('dossierId');
                String subProcess =  params.get('subProcess');
                String accountId = params.get('accountId');

                if (String.isBlank(accountId)) {
                    throw new WrtsException(System.Label.Account + ' - ' + System.Label.MissingId);
                }
                if (String.isBlank(dossierId)) {
                    throw new WrtsException(System.Label.Dossier + ' - ' + System.Label.MissingId);
                }
                List<Case> cases = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
                Boolean authorized=true;

                if(authorized){

                    String casePhase = constantsSrv.CASE_START_PHASE;
                    String caseStatus = constantsSrv.CASE_STATUS_DRAFT;
                    String dossierStatus = constantsSrv.DOSSIER_STATUS_DRAFT;

                    response.put('casePhase', casePhase);
                    response.put('caseStatus', caseStatus);
                    response.put('dossierStatus', dossierStatus);
                    dossierSrv.updateDossierSubprocess(dossierId, subProcess);
                    if(!cases.isEmpty() && cases.get(0).SubProcess__c != subProcess){
                        List<Id> caseIds= new List<Id>();
                        List<Id> newContractAccountIds= new List<Id>();
                        List<Id> newContractIds= new List<Id>();
                        List<Id> newBillingProfileIds= new List<Id>();
                        for(Case myCase: cases){
                            caseIds.add(myCase.Id);
                            Id newContractAccountId = myCase.ContractAccount2__c;
                            Id newContractId = myCase.Contract2__c;
                            Id newBillingProfileId = myCase.BillingProfile2__c;
                            if(newContractAccountId != null){
                                newContractAccountIds.add(newContractAccountId);
                            }
                            if(newBillingProfileId != null){
                                newBillingProfileIds.add(newBillingProfileId);
                            }
                            if(newContractId != null){
                                newContractIds.add(newContractId);
                            }
                        }
                        databaseSrv.deleteSObject(caseIds);
                        if(!newContractAccountIds.isEmpty()){
                            databaseSrv.deleteSObject(newContractAccountIds);
                        }
                        if(!newBillingProfileIds.isEmpty()){
                            databaseSrv.deleteSObject(newBillingProfileIds);
                        }
                        if(!newContractIds.isEmpty()){
                            databaseSrv.deleteSObject(newContractIds);
                        }
                        response.put('clear',true);
                    }

                }
                response.put('authorized',authorized);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }


    public with sharing class retrieveCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String caseId = params.get('caseId');
            String dossierId = params.get('dossierId');
            String commodity = params.get('commodity');

            Map<String, Object> response = new Map<String, Object>();
            try {
                Case myCase = caseQuery.getById(caseId);
                Dossier__c myDossier = dossierQuery.getById(dossierId);
                myDossier.Commodity__c = commodity;
                databaseSrv.updateSObject(myDossier);
                response.put('case', myCase);
                response.put('error', false);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            System.debug('output : ' + response.get('cases'));
            return response;
        }
    }

    public with sharing class deleteCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            String recordId = params.get('caseId');

            Map<String, Object> response = new Map<String, Object>();
            try {
                Case myCase = caseQuery.getById(recordId);
                caseSrv.deleteCase(recordId);
                Id newContractAccountId = myCase.ContractAccount2__c;
                Id newContractId=myCase.Contract2__c;
                Id newBillingProfile = myCase.BillingProfile2__c;
                if(newContractAccountId != null){
                    databaseSrv.deleteSObject(newContractAccountId);
                }
                if(newBillingProfile !=null){
                    databaseSrv.deleteSObject(newBillingProfile);
                }
                if(newContractId !=null){
                    databaseSrv.deleteSObject(newContractId);
                }
                response.put('recordId', recordId);
                response.put('error', false);
                System.debug('case deleted with id : ' + recordId);
            } catch (Exception ex) {
                //Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }

    }

    public with sharing class checkSelectedSupply extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String supplyId = params.get('supplyId');
            String dossierId = params.get('dossierId');
            String subProcess = params.get('subProcess');
            try{
                response.put('error', false);
                response.put('isValid', true);

                Supply__c supply = supplyQuery.getById(supplyId);
                response.put('supply', supply);
                Boolean isValid = true;
                if(subProcess == constantsSrv.SUB_PROCESS_CHNG_CONTRACTED_QUANTITIES ){
                    Boolean consumptionConvention = supply.Contract__r.ConsumptionConventions__c;
                    if(consumptionConvention == null || !consumptionConvention){
                        isValid = false;
                        response.put('errorMsg', System.Label.SelectedSupplyHasNotConsumptionConvention);
                    }
                }
                if(isValid){
                    List<Case> cases = caseQuery.getCasesByDossierId(dossierId);

                    if(!cases.isEmpty()){
                        isValid = false;
                        response.put('errorMsg', System.Label.MoreThanOneCaseError);
                    }
                   /* for(Case c : cases){
                        if(c.Supply__c.equals(supplyId)){
                            isValid=false;
                            response.put('errorMsg', System.Label.TheSelectedSupplyIsAlreadyRegisteredToTheCurrentCustomer);
                            break;
                        }
                    }*/
                }

                response.put('isValid', isValid);

            }catch(Exception ex){
                response.put('error', true);
                response.put('isValid', false);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }

    public with sharing class saveContractualDataChange extends AuraCallable {

        protected override Object perform(String jsonInput) {
            System.debug('Class : MRO_LC_ContractualDataChangeWizard --inner : saveContractualDataChange --- method : perform');
            System.debug('input : ' + jsonInput);

            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();

            Savepoint sp = Database.setSavepoint();
            try {
                List<Case> cases = (List<Case>) JSON.deserialize(params.get('caseList'), List<Case>.class);
                String dossierId = params.get('dossierId');
                System.debug('cases : ' + cases);

                Boolean isConsumptionConventionSubProcess = false;
                Dossier__c doss = new Dossier__c();
                doss.Id = dossierId;
                doss.Status__c = dossierNewStatus;
                databaseSrv.upsertSObject(doss);
                List<Case> updatedCases = new List<Case>();
                List<BillingProfile__c> bpToUpdate = new List<BillingProfile__c>();
                Set<Id> billingProfileIds = new Set<Id>();
                Set<Id> supplyIds = new Set<Id>();
                BillingProfile__c billingProfile1;
                BillingProfile__c billingProfile2;
                for (Case c: cases) {
                    if (c.BillingProfile__c != null) {
                        billingProfileIds.add(c.BillingProfile__c);
                    }
                    if (c.BillingProfile2__c != null) {
                        billingProfileIds.add(c.BillingProfile2__c);
                    }
                    supplyIds.add(c.Supply__c);
                }
                Map<Id,BillingProfile__c> mapBillingProfilesByIds = billingProfileQuery.getByIds(billingProfileIds);
                Map<Id,Supply__c> mapSuppliesByIds = supplyQuery.getByIds(supplyIds);
                for (Case c : cases) {
                    Case myCase = new Case(Id = c.Id, Status = constantsSrv.CASE_STATUS_NEW);
                    c.Status = constantsSrv.CASE_STATUS_NEW;
                    if(c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_CONTRACTED_QUANTITIES){
                        myCase.CaseTypeCode__c = 'C14';
                    } else if(c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_BILLING_FREQUENCY){
                        myCase.CaseTypeCode__c = 'C70 ';
                    } else if(c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_BILLING_CHANNEL){
                        myCase.CaseTypeCode__c = 'C126';
                        billingProfile1 = mapBillingProfilesByIds.get(c.BillingProfile__c);
                        billingProfile2 = mapBillingProfilesByIds.get(c.BillingProfile2__c);
                        if (billingProfile2.DeliveryChannel__c == constantsSrv.BILLING_PROFILE_DELIVERY_CHANNEL_EMAIL) {
                            myCase.Reason__c = 'Activate Email';
                            if (billingProfile1.DeliveryChannel__c == constantsSrv.BILLING_PROFILE_DELIVERY_CHANNEL_SMS) {
                                myCase.Reason__c = 'Change SMS to Email';
                            }
                        } else if (billingProfile2.DeliveryChannel__c == constantsSrv.BILLING_PROFILE_DELIVERY_CHANNEL_SMS) {
                            myCase.Reason__c = 'Activate SMS';
                            if (billingProfile1.DeliveryChannel__c == constantsSrv.BILLING_PROFILE_DELIVERY_CHANNEL_EMAIL) {
                                myCase.Reason__c = 'Change Email to SMS';
                            }
                        } else if (billingProfile2.DeliveryChannel__c == constantsSrv.BILLING_PROFILE_DELIVERY_CHANNEL_MAIL) {
                            if (billingProfile1.DeliveryChannel__c == constantsSrv.BILLING_PROFILE_DELIVERY_CHANNEL_EMAIL) {
                                myCase.Reason__c = 'Deactivate Email';
                            } else if (billingProfile1.DeliveryChannel__c == constantsSrv.BILLING_PROFILE_DELIVERY_CHANNEL_SMS) {
                                myCase.Reason__c = 'Deactivate SMS';
                            }
                        }
                    } else if(c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_REFUND_METHOD){
                        myCase.CaseTypeCode__c = 'C126';
                    } else if(c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_INVOICE_ISSUE){
                        myCase.CaseTypeCode__c = 'C70';
                    } else if(c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_CONTRACTUAL_VALIDITY){
                        myCase.CaseTypeCode__c = 'C75';
                    } else if(c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_CONSUMPTION_CONVENTIONS){
                        myCase.CaseTypeCode__c = 'C65';
                    }
                    myCase.EffectiveDate__c = System.today();
                    myCase.SLAExpirationDate__c = System.today().addDays(MRO_UTL_Date.calculateWorkDays(System.today(),5));
                    myCase.Supply__c = c.Supply__c;
                    myCase.CompanyDivision__c = mapSuppliesByIds.get(c.Supply__c).CompanyDivision__c;

                    if(c.BillingProfile__c != null && c.BillingProfile2__c != null){
                        billingProfile1 = mapBillingProfilesByIds.get(c.BillingProfile__c);
                        billingProfile2 = new BillingProfile__c();
                        billingProfile2.Id = c.BillingProfile2__c;
                        billingProfile2.BillingStreetName__c = billingProfile1.BillingStreetName__c != null ? billingProfile1.BillingStreetName__c : '';
                        billingProfile2.BillingStreetNumber__c = billingProfile1.BillingStreetNumber__c != null ? billingProfile1.BillingStreetNumber__c : '';
                        billingProfile2.BillingStreetNumberExtn__c = billingProfile1.BillingStreetNumberExtn__c != null ? billingProfile1.BillingStreetNumberExtn__c : '';
                        billingProfile2.BillingCity__c = billingProfile1.BillingCity__c != null ? billingProfile1.BillingCity__c : '';
                        billingProfile2.BillingProvince__c = billingProfile1.BillingProvince__c != null ? billingProfile1.BillingProvince__c : '';
                        billingProfile2.BillingPostalCode__c = billingProfile1.BillingPostalCode__c != null ? billingProfile1.BillingPostalCode__c : '';
                        billingProfile2.BillingCountry__c = billingProfile1.BillingCountry__c != null ? billingProfile1.BillingCountry__c : '';
                        billingProfile2.BillingAddressKey__c = billingProfile1.BillingAddressKey__c != null ? billingProfile1.BillingAddressKey__c : '';

                        bpToUpdate.add(billingProfile2);
                    }

                    updatedCases.add(myCase);
                    isConsumptionConventionSubProcess = c.SubProcess__c == constantsSrv.SUB_PROCESS_CHNG_CONSUMPTION_CONVENTIONS;
                }
                caseSrv.updateCases(updatedCases);

                if(bpToUpdate.size() > 0){
                    billingProfileSrv.updateBillingProfiles(bpToUpdate);
                }

                if (isConsumptionConventionSubProcess) {
                    consumptionSrv.updateCasesConsumptions(cases.get(0));
                }

                caseSrv.applyAutomaticTransitionOnDossierAndCases(dossierId);

                response.put('cases', cases);
                response.put('error', false);
            } catch (Exception ex) {
                Database.rollback(sp);
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());
            }
            return response;
        }
    }

    public with sharing class AddSupplyTypeToCase extends AuraCallable {
        public override Object perform(final String jsonInput) {
            Map<String, String> params = asMap(jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            String dossierId = params.get('dossierId');
            String selectedSupplyId = params.get('selectedSupplyId');

            try {
                Supply__c supply = supplyQuery.getById(selectedSupplyId);
                List<Case> cases = caseQuery.getCasesByDossierId(dossierId);

                for (Case cdcCase : cases) {
                    cdcCase.SupplyType__c = supply.RecordType.DeveloperName;
                }
                databaseSrv.updateSObject(cases);
            } catch (Exception ex) {
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }
    }

    public with sharing class createCaseComment extends AuraCallable {
        Map<Id, ContractAccount__c> contractAccountMap = new Map<Id, ContractAccount__c>();
        Map<Id, Contract> contractMap = new Map<Id, Contract>();
        Map<Id, Supply__c> supplyMap = new Map<Id, Supply__c>();
        Map<Id, BillingProfile__c> billingProfileMap = new Map<Id, BillingProfile__c>();

        public override Object perform(final String jsonInput) {
            System.debug('Class : MRO_LC_ContractualDataChangeWizard -- inner : createCaseComment --- method : perform');
            System.debug('input : ' + jsonInput);
            Map<String, Object> response = new Map<String, Object>();
            Savepoint sp = Database.setSavepoint();
            try {
                Map<String, String> params = asMap(jsonInput);
                List<Case> cases = (List<Case>) JSON.deserialize(params.get('cases'), List<Case>.class);
                System.debug('cases : ' + cases);

                Map<String,Set<Id>> involvedObjIds = this.getInvolvedObjIds(cases);

                contractAccountMap =
                        contractAccountQuery.getByIds(involvedObjIds.get('CONTRACT_ACCOUNT_IDS'));
                contractMap =
                        contractQuery.getByIds(involvedObjIds.get('CONTRACT_IDS'));
                supplyMap =
                        supplyQuery.getByIds(involvedObjIds.get('SUPPLY_IDS'));
                billingProfileMap =
                        billingProfileQuery.getByIds(involvedObjIds.get('BILLING_PROFILE_IDS'));

                Map<Id,String> comments = new Map<Id,String>();
                for(Case c : cases){
                    String message = this.generateComment(c,c.SubProcess__c);
                    comments.put(c.Id,message);
                }

                List<CaseComment> caseCommentsList = new List<CaseComment>();
                if(!comments.isEmpty())
                    caseCommentsList = this.writeComments(comments);

                if(this.caseCommentRecordsCreated(caseCommentsList)){
                    response.put('caseComments', caseCommentsList);
                    response.put('error', false);
                }
                else {
                    response.put('error', true);
                    response.put('errorMsg', System.Label.ErrorInCaseCommentCreating);
                }

            } catch (Exception ex) {
                Database.rollback(sp);
                System.debug(ex.getStackTraceString() + ' message : ' + ex.getMessage());
                response.put('error', true);
                response.put('errorMsg', ex.getMessage());
                response.put('errorTrace', ex.getStackTraceString());

            }
            return response;
        }

        private String generateComment(Case myCase, String subProcess){
            String subProcessLabel = MRO_UTL_Utils.getFieldLabel('SubProcess__c', 'Case');
            String separator = ' : ';
            String fieldSeparator = ' - ';
            String objectContractAccount = 'ContractAccount__c';
            String objectSupply = 'Supply__c';
            String objectServicePoint = 'ServicePoint__c';
            String objectContract = 'Contract';
            String objectBillingProfile = 'BillingProfile__c';
            String message = subProcessLabel + separator + subProcess + ' - Old Value ';
            if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_BILLING_FREQUENCY)){
                ContractAccount__c ca = contractAccountMap.get(myCase.ContractAccount__c);
                if(ca != null){
                    String billingFrequencyLabel = MRO_UTL_Utils.getFieldLabel('BillingFrequency__c', objectContractAccount);
                    message += billingFrequencyLabel + separator + ca.BillingFrequency__c;
                }
            }else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_BILLING_PERIOD)){
                ContractAccount__c ca = contractAccountMap.get(myCase.ContractAccount__c);
                if(ca != null){
                    String selfReadingPeriodEndLabel = MRO_UTL_Utils.getFieldLabel('SelfReadingPeriodEnd__c', objectContractAccount);
                    message += selfReadingPeriodEndLabel + separator + ca.SelfReadingPeriodEnd__c;
                }
            }else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_CONTRACTUAL_VALIDITY)){
                Supply__c supply = supplyMap.get(myCase.Supply__c);
                if(supply != null){
                    String expirationDateLabel = MRO_UTL_Utils.getFieldLabel('ExpirationDate__c', objectSupply);
                    message += expirationDateLabel + separator + supply.ExpirationDate__c;
                }
            }else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_CONTRACTED_QUANTITIES)){
                Supply__c supply = supplyMap.get(myCase.Supply__c);
                if(supply != null){
                    Decimal oldEstimatedCons = supply.ServicePoint__r.EstimatedConsumption__c;
                    String estimatedConsumptionLabel = MRO_UTL_Utils.getFieldLabel('EstimatedConsumption__c', objectServicePoint);
                    message += estimatedConsumptionLabel + separator + oldEstimatedCons;
                }
            }else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_CONSUMPTION_CONVENTIONS)){
                Contract contract = contractMap.get(myCase.Contract__c);
                if(contract != null){
                    Boolean oldCheck = contract.ConsumptionConventions__c;
                    String consumptionConventionsLabel = MRO_UTL_Utils.getFieldLabel('ConsumptionConventions__c', objectContract);
                    message += consumptionConventionsLabel + separator + (oldCheck ? 'Checked' : 'Unchecked');
                }
            }else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_BILLING_CHANNEL)){
                BillingProfile__c billingProfile = billingProfileMap.get(myCase.BillingProfile__c);
                if(billingProfile != null){
                    String deliveryChannelLabel = MRO_UTL_Utils.getFieldLabel('DeliveryChannel__c', objectBillingProfile);
                    String emailLabel = MRO_UTL_Utils.getFieldLabel('Email__c', objectBillingProfile);
                    String mobilePhoneLabel = MRO_UTL_Utils.getFieldLabel('MobilePhone__c', objectBillingProfile);
                    message+= billingProfile.DeliveryChannel__c != null && String.isNotBlank(billingProfile.DeliveryChannel__c) ?
                            (deliveryChannelLabel +separator + billingProfile.DeliveryChannel__c + fieldSeparator) : '';
                    message+= billingProfile.Email__c != null && String.isNotBlank(billingProfile.Email__c) ?
                            (emailLabel + separator + billingProfile.Email__c + fieldSeparator) : '';
                    message+= billingProfile.MobilePhone__c != null && String.isNotBlank(billingProfile.MobilePhone__c) ?
                            (mobilePhoneLabel + separator + billingProfile.MobilePhone__c + fieldSeparator) : '';
                }
            }else if(subProcess.contains(constantsSrv.SUB_PROCESS_CHNG_REFUND_METHOD)){
                BillingProfile__c billingProfile = billingProfileMap.get(myCase.BillingProfile__c);
                if(billingProfile != null){
                    String refundMethodLabel = MRO_UTL_Utils.getFieldLabel('RefundMethod__c', objectBillingProfile);
                    String refundBankLabel = MRO_UTL_Utils.getFieldLabel('RefundBank__c', objectBillingProfile);
                    String refundIbanLabel = MRO_UTL_Utils.getFieldLabel('RefundIBAN__c', objectBillingProfile);
                    message+= billingProfile.RefundMethod__c != null && String.isNotBlank(billingProfile.RefundMethod__c) ?
                            (refundMethodLabel + separator + billingProfile.RefundMethod__c + fieldSeparator) : '';
                    message+= billingProfile.RefundBank__c != null && String.isNotBlank(billingProfile.RefundBank__c) ?
                            (refundBankLabel + separator + billingProfile.RefundBank__c + fieldSeparator): '';
                    message+= billingProfile.RefundIBAN__c != null && String.isNotBlank(billingProfile.RefundIBAN__c) ?
                            (refundIbanLabel + separator + billingProfile.RefundIBAN__c + fieldSeparator): '';
                }
            }
            return message;
        }

        private List<CaseComment> writeComments(Map<Id,String> comments){
            List<CaseComment> caseComments = new List<CaseComment>();
            try {
                caseComments = caseSrv.createCaseComments(comments);
                System.debug('output : Case Comments created ' + comments);
            } catch(Exception e){
                throw e;
            }
            return caseComments;
        }

        private Map<String,Set<Id>> getInvolvedObjIds(List<Case> cases){
            Map<String,Set<Id>> output = new Map<String,Set<Id>>();

            Set<Id> contractAccountIds = new Set<Id>();
            Set<Id> contractIds = new Set<Id>();
            Set<Id> supplyIds = new Set<Id>();
            Set<Id> billingProfileIds = new Set<Id>();
            Set<Id> caseIds = new Set<Id>();

            for(Case c : cases){
                caseIds.add(c.Id);
                contractAccountIds.add(c.ContractAccount__c);
                contractAccountIds.add(c.ContractAccount2__c);
                contractIds.add(c.Contract__c);
                contractIds.add(c.Contract2__c);
                supplyIds.add(c.Supply__c);
                billingProfileIds.add(c.BillingProfile__c);
                billingProfileIds.add(c.BillingProfile2__c);
            }

            output.put('CONTRACT_ACCOUNT_IDS',contractAccountIds);
            output.put('CONTRACT_IDS',contractIds);
            output.put('SUPPLY_IDS',supplyIds);
            output.put('BILLING_PROFILE_IDS',billingProfileIds);
            output.put('CASE_IDS',caseIds);

            return output;

        }

        private Boolean caseCommentRecordsCreated(List<CaseComment> caseComments){
            return !caseComments.isEmpty();
        }

    }
}