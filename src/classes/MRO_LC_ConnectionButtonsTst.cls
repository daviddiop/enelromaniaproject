/**
 * Created by BADJI on 27/08/2020.
 */

@IsTest
public with sharing class MRO_LC_ConnectionButtonsTst {


    @IsTest
    static void executeArchiveDMSTest(){

        Map<String, String > inputJSON = new Map<String, String>{ };
        Test.startTest();
         TestUtils.exec(
            'MRO_LC_ConnectionButtons', 'executeArchiveDMS', inputJSON, true);
        TestUtils.exec(
            'MRO_LC_ConnectionButtons', 'executeArchiveFilenet', inputJSON, true);
        Test.stopTest();
    }
}