/**
 * @author  Pierandrea Pes (pierandrea.pes@webresults.it)
 * @since   nov 18, 2019
 * @desc    
 * @history 
 */

public with sharing class MRO_SRV_PrintAction {
    private static MRO_SRV_PrintAction singleton;
    private static final MRO_UTL_Constants constants = MRO_UTL_Constants.getAllConstants();
    private static final DatabaseService databaseSrv = DatabaseService.getInstance();
    private static MRO_QR_ValidatableDocument validatableDocumentQry = MRO_QR_ValidatableDocument.getInstance();
    private static MRO_QR_DocumentBundle documentBundleQry = MRO_QR_DocumentBundle.getInstance();

    public static MRO_SRV_PrintAction getInstance() {
        if (singleton == null) {
            singleton = new MRO_SRV_PrintAction();
        }
        return singleton;
    }

    public List<PrintAction__c> generatePrintActionsFromPrintActivities(Map<Id, wrts_prcgvr__Activity__c> printActivitiesMap) {
        Set<Id> documentBundlesIds = new Set<Id>();
        Set<Id> dossierIds = new Set<Id>();
        Set<Id> caseIds = new Set<Id>();
        for (wrts_prcgvr__Activity__c activity : printActivitiesMap.values()) {
            documentBundlesIds.add(activity.DocumentBundle__c);
            if (activity.Case__c != null) {
                caseIds.add(activity.Case__c);
            } else if (activity.Dossier__c != null) {
                dossierIds.add(activity.Dossier__c);
            }
        }

        Map<Id, Dossier__c> dossiersMap = MRO_TR_ActivityHandler.dossiersMap;
        Map<Id, Case> casesMap = MRO_TR_ActivityHandler.casesMap;

        Map<Id, Map<String, List<FileMetadata__c>>> existingDocumentsByDossier = !dossiersMap.isEmpty() ? MRO_QR_FileMetadata.getInstance().listExistingDocumentsByDossierIds(dossierIds) : null;
        Map<Id, Map<String, List<FileMetadata__c>>> existingDocumentsByCase = !casesMap.isEmpty() ? MRO_QR_FileMetadata.getInstance().listExistingDocumentsByCaseIds(caseIds) : null;

        List<PrintAction__c> printActions = new List<PrintAction__c>();
        for (wrts_prcgvr__Activity__c activity : printActivitiesMap.values()) {
            PrintAction__c printAction = new PrintAction__c(
                    PrintActivity__c = activity.Id,
                    DocumentBundle__c = activity.DocumentBundle__c,
                    Phase__c = constants.PRINTACTION_PHASE_NEW,
                    RecordTypeId = Schema.SObjectType.PrintAction__c.getRecordTypeInfosByDeveloperName().get('Print').getRecordTypeId()
            );
            printActions.add(printAction);
        }

        databaseSrv.insertSObject(printActions);


        Map<Id, DocumentBundle__c> documentBundleIdToDocumentBundleMap = new Map<Id, DocumentBundle__c>();
        for (DocumentBundle__c documentBundle : documentBundleQry.findByIds(documentBundlesIds)) {

            documentBundleIdToDocumentBundleMap.put(documentBundle.Id, documentBundle);
        }

        List<Id> caseIdList = new List<Id>();
        for (PrintAction__c printAction : printActions) {
            wrts_prcgvr__Activity__c activity = printActivitiesMap.get(printAction.PrintActivity__c);
            Dossier__c dossier = activity.Case__c != null ? casesMap.get(activity.Case__c).Dossier__r : dossiersMap.get(activity.Dossier__c);
            Case caseRecord = activity.Dossier__c != null && !dossier.Cases__r.isEmpty() ? dossier.Cases__r[0] : casesMap.isEmpty() ? null : casesMap.get(activity.Case__c);
            if (caseRecord != null) {
                caseIdList.add(caseRecord.Id);
            }
        }
        Map<Id, ValidatableDocument__c> dossierIdToValidatableDocumentMap = new Map<Id, ValidatableDocument__c>();
        if (!caseIdList.isEmpty()) {
            for (ValidatableDocument__c validatableDocument : validatableDocumentQry.findByCaseIdAndDocumentType
                    (caseIdList, new List<String>{
                            constants.DOCUMENTTYPE_DOCUMENT_TYPE_CODE_RENUNT_SCHIMB_FURN
                    })) {
                dossierIdToValidatableDocumentMap.put(validatableDocument.Case__c, validatableDocument);
            }
        }

        List<FileMetadata__c> filesMetadata = new List<FileMetadata__c>();
        for (PrintAction__c printAction : printActions) {
            List<FileMetadata__c> printActionFilesMetadata = new List<FileMetadata__c>();
            wrts_prcgvr__Activity__c activity = printActivitiesMap.get(printAction.PrintActivity__c);
            Dossier__c dossier = activity.Case__c != null ? casesMap.get(activity.Case__c).Dossier__r : dossiersMap.get(activity.Dossier__c);
            Case caseRecord = activity.Dossier__c != null && !dossier.Cases__r.isEmpty() ? dossier.Cases__r[0] : casesMap.get(activity.Case__c);
            Account customer = activity.Case__c != null ? caseRecord.Account : dossier.Account__r;
            List<DocumentBundleItem__c> documentBundleItems = MRO_TR_ActivityHandler.documentBundleItemsByBundleId.containsKey(printAction.DocumentBundle__c) ? MRO_TR_ActivityHandler.documentBundleItemsByBundleId.get(printAction.DocumentBundle__c) : new List<DocumentBundleItem__c>();
            for (DocumentBundleItem__c documentBundleItem : documentBundleItems) {
                if (documentBundleItem.SendingChannels__c != null) {
                    Set<String> enabledChannels = new Set<String>(documentBundleItem.SendingChannels__c.split(';'));
                    if (!enabledChannels.contains(activity.SendingChannel__c)) {
                        continue;
                    }
                }
                if (documentBundleItem.Origins__c != null) {
                    Set<String> enabledOrigins = new Set<String>(documentBundleItem.Origins__c.split(';'));
                    if (!enabledOrigins.contains(dossier.Origin__c)) {
                        continue;
                    }
                }
                if (documentBundleItem.Channels__c != null) {
                    Set<String> enabledChannels = new Set<String>(documentBundleItem.Channels__c.split(';'));
                    if (!enabledChannels.contains(dossier.Channel__c)) {
                        continue;
                    }
                }
                if (documentBundleItem.Commodities__c != null) {
                    Set<String> enabledCommodities = new Set<String>(documentBundleItem.Commodities__c.split(';'));
                    if (!(enabledCommodities.contains(constants.COMMODITY_COMBO) && dossier.IsDual__c) && (dossier.Commodity__c == null || !enabledCommodities.contains(dossier.Commodity__c))) {
                        continue;
                    }
                }
                if (documentBundleItem.CustomerTypes__c != null) {
                    Set<String> enabledCustomerTypes = new Set<String>(documentBundleItem.CustomerTypes__c.split(';'));
                    if (customer == null ||
                            (customer.IsPersonAccount && !enabledCustomerTypes.contains('Residential')) ||
                            (!customer.IsPersonAccount && !enabledCustomerTypes.contains('Business'))) {
                        continue;
                    }
                }
                if (documentBundleItem.SubProcess__c != null && dossier.SubProcess__c != documentBundleItem.SubProcess__c) {
                    continue;
                }
                if (documentBundleItem.PrintBundleSelection__c != null) {
                    if (documentBundleItem.PrintBundleSelection__c == constants.BLANK_FIELD_PLACEHOLDER) {
                        if (dossier.PrintBundleSelection__c != null) {
                            continue;
                        }
                    } else if (dossier.PrintBundleSelection__c == null) {
                        continue;
                    } else {
                        Set<String> selectionValues = new Set<String>(dossier.PrintBundleSelection__c.split(';'));
                        if (!selectionValues.contains(documentBundleItem.PrintBundleSelection__c)) {
                            continue;
                        }
                    }
                }
                if (documentBundleItem.EnelArea__c != null && dossier.Opportunity__c != null) {
                    Boolean isEnelArea = caseRecord.Supply__c != null ? caseRecord.Supply__r.InENELArea__c : (caseRecord.Distributor__c != null ? caseRecord.Distributor__r.IsDisCoENEL__c : false);
                    if ((isEnelArea && documentBundleItem.EnelArea__c == 'No') || (!isEnelArea && documentBundleItem.EnelArea__c == 'Yes')) {
                        continue;
                    }
                }
                if (documentBundleItem.Market__c != null) {
                    String market = caseRecord.Market__c != null ? caseRecord.Market__c : (caseRecord.ContractAccount__c != null ? caseRecord.ContractAccount__r.Market__c : caseRecord.Supply__r.Market__c);
                    if (market != documentBundleItem.Market__c) {
                        continue;
                    }
                }
                if (documentBundleItem.ContractAddition__c != null && dossier.Opportunity__c != null) {
                    Boolean isContractAdditionOrPartialRemoval = false;
                    if (dossier.Opportunity__c != null) {
                        isContractAdditionOrPartialRemoval = dossier.ContractAddition__c;
                    } else {
                        isContractAdditionOrPartialRemoval = MRO_TR_ActivityHandler.dossiersWithContractAdditionOrPartialRemoval.get(dossier.Id);
                    }
                    if ((isContractAdditionOrPartialRemoval && documentBundleItem.ContractAddition__c == 'No') || (!isContractAdditionOrPartialRemoval && documentBundleItem.ContractAddition__c == 'Yes')) {
                        continue;
                    }
                }
                String templateCode, templateVersion;
                if (documentBundleItem.ProductTemplateCodeField__c != null) {
                    NE__Product__c commercialProduct = dossier.CommercialProduct__r;
                    if (commercialProduct != null) {
                        templateCode = (String) commercialProduct.get(documentBundleItem.ProductTemplateCodeField__c);
                        if (String.isBlank(templateCode) && documentBundleItem.SkipIfEmptyOnProduct__c) {
                            continue;
                        }
                        templateVersion = 'v1';//(String) commercialProduct.get(documentBundleItem.ProductTemplateCodeField__c.replace('__c', 'Version__c'));
                    }
                } else {
                    templateCode = documentBundleItem.TemplateCode__c;
                    templateVersion = documentBundleItem.TemplateVersion__c;
                }
                if (String.isBlank(templateCode) || String.isBlank(templateVersion)) {
                    throw new WrtsException('Unable to determine the Template Code/Template Version in order to print item with Id ' + documentBundleItem.Id);
                }
                if (documentBundleItem.EnableLanguages__c) {
                    if (dossier.Language__c == null) {
                        throw new WrtsException('Unable to determine the language in order to print item with Id ' + documentBundleItem.Id);
                    }
                    templateCode += '_' + dossier.Language__c;
                }
                if (documentBundleItem.TemplateClass__c == constants.REPRINT_TEMPLATE_CLASS &&
                        documentBundleIdToDocumentBundleMap.get(documentBundleItem.DocumentBundle__c).Recipient__c ==
                                constants.DOCUMENTBUNDLE_RECIPIENT_CUSTOMER) {

                    String documentTypeCode = documentBundleItem.DocumentType__r.DocumentTypeCode__c;
                    List<FileMetadata__c> existingDocuments;
                    if (activity.Case__c != null && existingDocumentsByCase.containsKey(activity.Case__c)
                            && existingDocumentsByCase.get(activity.Case__c).containsKey(documentTypeCode)) {
                        existingDocuments = existingDocumentsByCase.get(activity.Case__c).get(documentTypeCode);
                    } else if (activity.Dossier__c != null && existingDocumentsByDossier.containsKey(activity.Dossier__c)
                            && existingDocumentsByDossier.get(activity.Dossier__c).containsKey(documentTypeCode)) {
                        existingDocuments = existingDocumentsByDossier.get(activity.Dossier__c).get(documentTypeCode);
                    }
                    if (existingDocuments == null) {
                        throw new WrtsException('Unable to find existing and archived document with code ' + documentTypeCode + ' for ' + (activity.Case__c != null ? 'Case with Id ' + activity.Case__c : 'Dossier with Id ' + activity.Dossier__c));
                    }
                    for (FileMetadata__c existingDocument : existingDocuments) {
                        existingDocument.PrintAction__c = printAction.Id;
                        existingDocument.DocumentType__c = documentBundleItem.DocumentType__c;
                        existingDocument.TemplateClass__c = documentBundleItem.TemplateClass__c;
                        existingDocument.TemplateCode__c = templateCode;
                        existingDocument.TemplateVersion__c = templateVersion;
                        existingDocument.Archive__c = documentBundleItem.Archive__c;
                        if (activity.SendingChannel__c == constants.SENDING_CHANNEL_EMAIL) {
                            existingDocument.Master__c = documentBundleItem.Master__c;
                            existingDocument.AttachToEmail__c = documentBundleItem.AttachToEmail__c;
                        } else if (activity.SendingChannel__c == constants.SENDING_CHANNEL_MAIL) {
                            existingDocument.Master__c = true;
                            existingDocument.AttachToEmail__c = false;
                        }
                        printActionFilesMetadata.add(existingDocument);
                    }
                } else {

                    FileMetadata__c fileMetadata = new FileMetadata__c();
                    if (documentBundleIdToDocumentBundleMap.get(documentBundleItem.DocumentBundle__c).Recipient__c ==
                            constants.DOCUMENTBUNDLE_RECIPIENT_TRADER &&
                            documentBundleIdToDocumentBundleMap.get(documentBundleItem.DocumentBundle__c)
                                    .DocumentBundleCode__c == constants.DOCUMENTBUNDLECODE_SWI_OUT_RETENTION_TRADER_OUT &&
                            documentBundleItem.TemplateClass__c == constants.REPRINT_TEMPLATE_CLASS) {

                        ValidatableDocument__c validatableDocument =
                                dossierIdToValidatableDocumentMap.get(activity.Case__c);
                        if (validatableDocument != null) {

                            fileMetadata = new FileMetadata__c(
                                    PrintAction__c = printAction.Id,
                                    DocumentBundleItem__c = validatableDocument.DocumentBundleItem__c,
                                    DocumentType__c = validatableDocument.DocumentBundleItem__r.DocumentType__c,
                                    TemplateClass__c = validatableDocument.DocumentBundleItem__r.TemplateClass__c,
                                    TemplateCode__c = templateCode,
                                    TemplateVersion__c = templateVersion,
                                    Archive__c = validatableDocument.DocumentBundleItem__r.Archive__c,
                                    Dossier__c = dossier.Id,
                                    Case__c = activity.Case__c,
                                    ExternalId__c = validatableDocument.FileMetadata__r.ExternalId__c
                            );
                        }
                    } else {
                        fileMetadata = new FileMetadata__c(
                                PrintAction__c = printAction.Id,
                                DocumentBundleItem__c = documentBundleItem.Id,
                                DocumentType__c = documentBundleItem.DocumentType__c,
                                TemplateClass__c = documentBundleItem.TemplateClass__c,
                                TemplateCode__c = templateCode,
                                TemplateVersion__c = templateVersion,
                                Archive__c = documentBundleItem.Archive__c,
                                Dossier__c = dossier.Id,
                                Case__c = activity.Case__c
                        );
                    }
                    if (activity.SendingChannel__c == constants.SENDING_CHANNEL_EMAIL) {
                        fileMetadata.Master__c = documentBundleItem.Master__c;
                        fileMetadata.AttachToEmail__c = documentBundleItem.AttachToEmail__c;
                    } else if (activity.SendingChannel__c == constants.SENDING_CHANNEL_MAIL) {
                        fileMetadata.Master__c = true;
                        fileMetadata.AttachToEmail__c = false;
                    } else if (activity.SendingChannel__c == constants.SENDING_CHANNEL_SMS) {
                        fileMetadata.Master__c = documentBundleItem.Master__c;
                        fileMetadata.AttachToEmail__c = false;
                    }
                    printActionFilesMetadata.add(fileMetadata);
                }
            }
            if (printActionFilesMetadata.isEmpty()) {
                printAction.Phase__c = constants.PRINTACTION_PHASE_DONE;
            } else {
                filesMetadata.addAll(printActionFilesMetadata);
            }
        }
        databaseSrv.upsertSObject(filesMetadata);

        for (PrintAction__c printAction : printActions) {
            if (printAction.Phase__c != constants.PRINTACTION_PHASE_DONE) {
                printAction.Phase__c = constants.PRINTACTION_PHASE_PRINTING;
            }
        }
        databaseSrv.updateSObject(printActions);

        return printActions;
    }

    /**
     *
     *
     * @param touchPointActivitiesMap
     *
     * @return
     */
    public List<PrintAction__c> generatePrintActionsFromTouchPointActivities(Map<Id, wrts_prcgvr__Activity__c> touchPointActivitiesMap) {

        Set<Id> documentBundlesIds = new Set<Id>();
        Set<Id> dossierIds = new Set<Id>();
        Set<Id> caseIds = new Set<Id>();

        for (wrts_prcgvr__Activity__c activity : touchPointActivitiesMap.values()) {

            documentBundlesIds.add(activity.DocumentBundle__c);
            if (activity.Case__c != null) {

                caseIds.add(activity.Case__c);
            } else if (activity.Dossier__c != null) {

                dossierIds.add(activity.Dossier__c);
            }
        }

        List<DocumentBundleItem__c> documentBundleItems = MRO_QR_DocumentBundleItem.getInstance().listDocumentBundleItemsByBundlesIds(documentBundlesIds);
        Map<Id, List<DocumentBundleItem__c>> documentBundleItemsByBundleId = new Map<Id, List<DocumentBundleItem__c>>();
        for (DocumentBundleItem__c bundleItem : documentBundleItems) {

            if (!documentBundleItemsByBundleId.containsKey(bundleItem.DocumentBundle__c)) {
                documentBundleItemsByBundleId.put(bundleItem.DocumentBundle__c, new List<DocumentBundleItem__c>{
                        bundleItem
                });
            } else {

                documentBundleItemsByBundleId.get(bundleItem.DocumentBundle__c).add(bundleItem);
            }
        }

        Map<Id, Dossier__c> dossiersMap = dossierIds.isEmpty() ? new Map<Id, Dossier__c>() : MRO_QR_Dossier.getInstance().getByIds(dossierIds);
        Map<Id, Case> casesMap = caseIds.isEmpty() ? new Map<Id, Case>() : new Map<Id, Case>(MRO_QR_Case.getInstance().listByIds(caseIds));

        List<PrintAction__c> printActions = new List<PrintAction__c>();
        for (wrts_prcgvr__Activity__c activity : touchPointActivitiesMap.values()) {

            PrintAction__c printAction = new PrintAction__c(
                    PrintActivity__c = activity.Id,
                    DocumentBundle__c = activity.DocumentBundle__c,
                    Phase__c = constants.PRINTACTION_PHASE_NEW,
                    RecordTypeId = Schema.SObjectType.PrintAction__c.getRecordTypeInfosByDeveloperName().get('Print').getRecordTypeId()
            );
            printActions.add(printAction);
        }
        databaseSrv.insertSObject(printActions);

        List<FileMetadata__c> filesMetadata = new List<FileMetadata__c>();
        for (PrintAction__c printAction : printActions) {

            wrts_prcgvr__Activity__c activity = touchPointActivitiesMap.get(printAction.PrintActivity__c);
            Dossier__c dossier = activity.Case__c != null ? casesMap.get(activity.Case__c).Dossier__r : dossiersMap.get(activity.Dossier__c);
            for (DocumentBundleItem__c documentBundleItem : documentBundleItemsByBundleId.get(printAction.DocumentBundle__c)) {

                String templateCode, templateVersion;
                templateCode = documentBundleItem.TemplateCode__c;
                templateVersion = documentBundleItem.TemplateVersion__c;
                if (String.isBlank(templateCode) || String.isBlank(templateVersion)) {

                    throw new WrtsException('Unable to determine the Template Code/Template Version in order to print item with Id ' + documentBundleItem.Id);
                }
//                if (documentBundleItem.EnableLanguages__c) {
//
//                    if (dossier.Language__c == null) {
//
//                        throw new WrtsException('Unable to determine the language in order to print item with Id '+documentBundleItem.Id);
//                    }
//                    templateCode += '_' + dossier.Language__c;
//                }
                filesMetadata.add(new FileMetadata__c(
                        PrintAction__c = printAction.Id,
                        DocumentBundleItem__c = documentBundleItem.Id,
                        DocumentType__c = documentBundleItem.DocumentType__c,
                        TemplateClass__c = documentBundleItem.TemplateClass__c,
                        TemplateCode__c = templateCode,
                        TemplateVersion__c = templateVersion,
                        Archive__c = documentBundleItem.Archive__c,
                        Master__c = documentBundleItem.Master__c,
                        AttachToEmail__c = documentBundleItem.AttachToEmail__c,
                        Dossier__c = dossier.Id//,
                        //Case__c = activity.Case__c
                ));
            }
        }
        databaseSrv.insertSObject(filesMetadata);

        for (PrintAction__c printAction : printActions) {

            printAction.Phase__c = constants.PRINTACTION_PHASE_PRINTING;
        }
        databaseSrv.updateSObject(printActions);

        return printActions;
    }
}