<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <categoryGroupVisibilities>
        <dataCategoryGroup>DataCategoryGroup</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities><custom>true</custom>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
        <recordType>Account.Agency</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
        <recordType>Account.Business</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
        <recordType>Account.BusinessProspect</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
        <recordType>Account.Institution</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
        <recordType>Account.Performer</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
        <recordType>Account.Trader</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-Distributor Layout</layout>
        <recordType>Account.Distributor</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AccountContactRelation-Account Contact Relationship Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AppliedProductOption__c-Applied Product Option Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Asset-Asset Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AssetRelationship-Asset Relationship Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Bank__c-Bank Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BillingProfile__c-Billing Profile Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Campaign-Campaign Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CampaignMember-Campaign Member Page Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CancellationRequest__c-Cancellation Request Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Case Layout</layout>
        <recordType>Case.MeterChangeEle</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Case Layout</layout>
        <recordType>Case.MeterChangeGas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.AccountSituation</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Activation_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Activation_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Activation_SRV</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.BillingProfileChange</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Complaint</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Connection_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Connection_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.ContractMerge</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.CustomerDataChange</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.DemonstratedPayment</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.InvoiceDuplicate</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.MeterCheckEle</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.MeterCheckGas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Meter_Reading_Ele</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Meter_Reading_Gas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.NonDisconnectable_POD_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.NonDisconnectable_POD_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.ProductChange_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.ProductChange_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Reactivation_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Reactivation_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.RegistryChangeFast</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.RegistryChangeSlow</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.RepaymentRequest</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.SelfReading_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.SelfReading_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.ServicePointAddressChange</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Tax_Change</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.TechnicalDataChange_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.TechnicalDataChange_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Termination_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Termination_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Termination_SRV</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Transfer_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Custom Case Layout</layout>
        <recordType>Case.Transfer_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Sw-In Case Layout</layout>
        <recordType>Case.SwitchIn_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Sw-In Case Layout</layout>
        <recordType>Case.SwitchIn_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Sw-Out Case Layout</layout>
        <recordType>Case.SwitchOut_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Sw-Out Case Layout</layout>
        <recordType>Case.SwitchOut_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseClose-Close Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseInteraction-Case Feed Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>City__c-City Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CollaborationGroup-Group Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CompanyDivision__c-RO - Company Division Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contact-Contact Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContentVersion-Content Version</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract-Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContractAccount__c-Contract Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CustomerInteraction__c-Customer Interaction Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Device__c-Device Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentBundleItem__c-Document Bundle Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentBundleItem__c-Input Bundle Item Layout</layout>
        <recordType>DocumentBundleItem__c.InputItem</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentBundleItem__c-Output Bundle Item Layout</layout>
        <recordType>DocumentBundleItem__c.OutputItem</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentBundle__c-Document Bundle Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentBundle__c-Document Bundle Layout</layout>
        <recordType>DocumentBundle__c.InputBundle</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentBundle__c-Document Bundle Layout</layout>
        <recordType>DocumentBundle__c.OutputBundle</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentItem__c-Document Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DocumentType__c-Document Type Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.Acquisition</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.Change</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.Complaint</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.Connection</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.ContractMerge</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.Disconnection</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.GenericRequest</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DuplicateRecordSet-Duplicate Record Set Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>EmailMessage-Email Message Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Event-Event Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ExecutionLog__c-Execution Log Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ExecutionLog__c-Execution Log Layout</layout>
        <recordType>ExecutionLog__c.Debug</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ExecutionLog__c-Execution Log Layout</layout>
        <recordType>ExecutionLog__c.Integration</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FeedItem-Feed Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FileMetadata__c-File Metadata Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Global-Global Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Idea-Idea Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Index__c-Index Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Individual-Individual Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Interaction__c-Interaction Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Lead-Lead Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Macro-Macro Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-Opportunity Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityLineItem-Opportunity Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout</layout>
        <recordType>OpportunityServiceItem__c.Electric</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout</layout>
        <recordType>OpportunityServiceItem__c.Gas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout</layout>
        <recordType>OpportunityServiceItem__c.Service</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Order-Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OrderItem-Order Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PersonAccount-Person Account Layout</layout>
        <recordType>PersonAccount.Person</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PersonAccount-Person Account Layout</layout>
        <recordType>PersonAccount.PersonAccount</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PersonAccount-Person Account Layout</layout>
        <recordType>PersonAccount.PersonProspect</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pricebook2-Price Book Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PricebookEntry-Price Book Entry Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PrivacyChange__c-Privacy Change Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
        <recordType>Product2.Good</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
        <recordType>Product2.Service</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
        <recordType>Product2.Utility</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProductOptionRelation__c-Product Option Relation Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProductOption__c-Product Option Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProductOption__c-Product Option Layout</layout>
        <recordType>ProductOption__c.Family</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProductOption__c-Product Option Layout</layout>
        <recordType>ProductOption__c.Product</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProductOption__c-Product Option Layout</layout>
        <recordType>ProductOption__c.Value</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProfileSkill-Skill Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProfileSkillEndorsement-Endorsement Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProfileSkillUser-Skill User Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuickText-Quick Text Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Quote-Quote Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuoteLineItem-Quote Line Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Scorecard-Scorecard Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardAssociation-Scorecard Association Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardMetric-Scorecard Metric Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScriptTemplateElement__c-Script Template Element Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScriptTemplateElement__c-Script Template Element Layout</layout>
        <recordType>ScriptTemplateElement__c.Iteration</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScriptTemplateElement__c-Script Template Element Layout</layout>
        <recordType>ScriptTemplateElement__c.PlainHtml</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScriptTemplate__c-Script Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Sequencer__c-Sequencer Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServicePoint__c-Service Point Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServicePoint__c-Service Point Layout</layout>
        <recordType>ServicePoint__c.Electric</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServicePoint__c-Service Point Layout</layout>
        <recordType>ServicePoint__c.Gas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServiceSite__c-Service Site Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPersona-Social Persona Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPost-Social Post Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Solution-Solution Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Street__c-Street Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
        <recordType>Supply__c.Electric</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
        <recordType>Supply__c.Gas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
        <recordType>Supply__c.Service</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Task-Task Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Danube_Delta</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Distribution</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Green_certificates</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Heating_subvention</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Miners</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Paying_supply_excise</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Quota_Ministry_Energy</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.Self_producers</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TaxSubsidy__c-Tax%2FSubsidy Layout</layout>
        <recordType>TaxSubsidy__c.VAT</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>User-User Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserAlt-User Profile Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserAppMenuItem-Application Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvAccount-User Provisioning Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningLog-User Provisioning Log Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningRequest-User Provisioning Request Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ValidatableDocument__c-Validatable Document Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkAccess-Access Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkBadge-Badge Received Layout 192</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkBadgeDefinition-Badge Layout 192</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkOrder-Work Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkOrderLineItem-Work Order Line Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkThanks-Thanks Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__ActivityAction__c-wrts_prcgvr__Activity Action Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__ActivityTemplate__c-wrts_prcgvr__Activity Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__Activity__c-wrts_prcgvr__Activity Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__AsyncJob__c-wrts_prcgvr__Async Job Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__CalloutTemplate__c-wrts_prcgvr__Callout Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__CompatibilityCriteriaItem__c-wrts_prcgvr__Compatibility Criteria Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__Compatibility__c-wrts_prcgvr__Compatibility Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__DependencyPush__c-wrts_prcgvr__Dependency Push Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__Dependency__c-wrts_prcgvr__Dependency Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__DynamicButtonCriteriaItem__c-wrts_prcgvr__Dynamic Button Criteria Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__DynamicButton__c-wrts_prcgvr__Dynamic Button Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__DynamicField__c-wrts_prcgvr__Dynamic Field Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__DynamicSection__c-wrts_prcgvr__Dynamic Section Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__EndpointCriteriaItem__c-wrts_prcgvr__Endpoint Criteria Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__EndpointRule__c-wrts_prcgvr__Endpoint Rule Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__FieldsTemplate__c-wrts_prcgvr__Fields Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__Log__c-wrts_prcgvr__Log Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__ObjectCompatibility__c-wrts_prcgvr__Object Compatibility Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__PhaseTransitionDetail__c-wrts_prcgvr__Phase Transition Detail Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__PhaseTransition__c-wrts_prcgvr__Phase Transition Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__Phase__c-wrts_prcgvr__Phase Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__ServiceCriteriaItem__c-wrts_prcgvr__Service Criteria Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__ServiceLinkItem__c-wrts_prcgvr__Service Link Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__ServiceLink__c-wrts_prcgvr__Service Link Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>wrts_prcgvr__TransitionActivityTemplate__c-wrts_prcgvr__Transition Activity Template Layout</layout>
    </layoutAssignments>
    <tabVisibilities>
        <tab>Bank__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>CancellationRequest__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>City__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>CompanyDivision__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>DocumentBundle__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>DocumentType__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Dossier__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Interaction__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Landing_Page</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>NACE__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Sequencer__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>ServicePoint__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Street__c</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Supply__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Account</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Asset</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Campaign</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Case</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Contact</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Contract</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Individual</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Lead</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Opportunity</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Pricebook2</tab>
        <visibility>DefaultOff</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>standard-Product2</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__ActivitiesHome</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__ActivityTemplate__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__Activity__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__CalloutTemplate__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__CatalogHome</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__CompatibilityHome</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__ConfigurationMigration</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__Dependency__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__EndpointRules</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__FieldsTemplate__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__IntegrationHome</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__MigrationHome</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__ObjectCompatibility</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__ObjectCompatibilityLtg</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__PhaseManager</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__PhaseManagerHome</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__PhaseManagerLtg</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__Phase__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__ServiceCatalogLtg</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>wrts_prcgvr__ServiceLink__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <userLicense>Salesforce</userLicense>
    <userPermissions>
        <enabled>true</enabled>
        <name>ActivateContract</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ActivateOrder</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ActivitiesAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AddDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowUniversalSearch</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowViewKnowledge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApexRestServices</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApiEnabled</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApproveContract</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AssignTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterEditOwnPost</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterEditOwnRecordPost</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterFileLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInternalUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInviteExternalUsers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterOwnGroups</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ConnectOrgToEnvironmentHub</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ConvertLeads</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateCustomizeFilters</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateCustomizeReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateLtngTempFolder</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>DataExport</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>DistributeFromPersWksp</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditActivatedOrders</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditCaseComments</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditEvent</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditOppLineItemUnitPrice</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTask</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailMass</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailSingle</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableNotifications</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ExportReport</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ImportCustomObjects</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ImportLeads</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ImportPersonal</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningConsoleAllowedForUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningExperienceUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ListEmailSend</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ManageCases</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ManageDataIntegrations</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ManageLeads</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>MassInlineEdit</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ModifyDataClassification</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>OverrideForecasts</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>PrivacyDataAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RemoveDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RunReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SelectFilesFromSalesforce</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SendSitRequests</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ShowCompanyNameAsUserBadge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SocialInsightsLogoAdmin</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SubmitMacrosAllowed</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SubscribeToLightningDashboards</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SubscribeToLightningReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>TransactionalEmailSend</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>TransferAnyCase</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>TransferAnyEntity</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>TransferAnyLead</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>UseTeamReassignWizards</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>UseWebLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewAllForecasts</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewDataAssessment</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewEventLogFiles</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewHelpLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewMyTeamsDashboards</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewPublicDashboards</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewPublicReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewRoles</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewSetup</name>
    </userPermissions>
</Profile>
