<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <custom>true</custom>
    <layoutAssignments>
        <layout>Account-MRO Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Account Layout</layout>
        <recordType>Account.Agency</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Account Layout</layout>
        <recordType>Account.Business</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Account Layout</layout>
        <recordType>Account.BusinessProspect</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Institution Layout</layout>
        <recordType>Account.Institution</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Payment Agency Layout</layout>
        <recordType>Account.PaymentAgency</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Performer Layout</layout>
        <recordType>Account.Performer</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Trader Layout</layout>
        <recordType>Account.Trader</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-MRO Distributor Layout</layout>
        <recordType>Account.Distributor</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO HardVAS</layout>
        <recordType>Case.Acquisition_Hard_VAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Activation</layout>
        <recordType>Case.Activation_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Activation</layout>
        <recordType>Case.Activation_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO SoftVAS</layout>
        <recordType>Case.Activation_SRV</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Connection</layout>
        <recordType>Case.Connection_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Connection</layout>
        <recordType>Case.Connection_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Connection</layout>
        <recordType>Case.MeterChangeEle</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Connection</layout>
        <recordType>Case.MeterChangeGas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO NonDisconnectable</layout>
        <recordType>Case.NonDisconnectable_POD_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO NonDisconnectable</layout>
        <recordType>Case.NonDisconnectable_POD_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO ProductChange</layout>
        <recordType>Case.ProductChange_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO ProductChange</layout>
        <recordType>Case.ProductChange_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Connection</layout>
        <recordType>Case.TechnicalDataChange_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Connection</layout>
        <recordType>Case.TechnicalDataChange_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Contract Transfer</layout>
        <recordType>Case.Transfer_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Contract Transfer</layout>
        <recordType>Case.Transfer_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Switch-In</layout>
        <recordType>Case.SwitchIn_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Switch-In</layout>
        <recordType>Case.SwitchIn_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Switch-Out</layout>
        <recordType>Case.SwitchOut_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Switch-Out</layout>
        <recordType>Case.SwitchOut_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Termination</layout>
        <recordType>Case.Termination_ELE</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO Termination</layout>
        <recordType>Case.Termination_GAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO HardVAS</layout>
        <recordType>Case.Termination_Hard_VAS</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-MRO SoftVas</layout>
        <recordType>Case.Termination_SRV</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contact-MRO Contact Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract-MRO Contract</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Acquisition</layout>
        <recordType>Dossier__c.Acquisition</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Change</layout>
        <recordType>Dossier__c.Change</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Complaints</layout>
        <recordType>Dossier__c.Complaint</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Connection</layout>
        <recordType>Dossier__c.Connection</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Dossier</layout>
        <recordType>Dossier__c.ContractMerge</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO Disconnection</layout>
        <recordType>Dossier__c.Disconnection</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Dossier__c-MRO GenericRequest</layout>
        <recordType>Dossier__c.GenericRequest</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-MRO Opportunity</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout Electric</layout>
        <recordType>OpportunityServiceItem__c.Electric</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout Gas</layout>
        <recordType>OpportunityServiceItem__c.Gas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityServiceItem__c-Opportunity Service Item Layout</layout>
        <recordType>OpportunityServiceItem__c.Service</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PersonAccount-MRO Person Account Layout</layout>
        <recordType>PersonAccount.Person</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PersonAccount-MRO Person Account Layout</layout>
        <recordType>PersonAccount.PersonAccount</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PersonAccount-MRO Person Account Layout</layout>
        <recordType>PersonAccount.PersonProspect</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServicePoint__c-MRO ServicePointEle</layout>
        <recordType>ServicePoint__c.Electric</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServicePoint__c-MRO ServicePointGas</layout>
        <recordType>ServicePoint__c.Gas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServicePoint__c-MRO ServicePointVAS</layout>
        <recordType>ServicePoint__c.Service</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
        <recordType>Supply__c.Electric</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
        <recordType>Supply__c.Gas</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Supply__c-MRO Supply Layout</layout>
        <recordType>Supply__c.Service</recordType>
    </layoutAssignments>
    <userLicense>Salesforce</userLicense>
    <userPermissions>
        <enabled>true</enabled>
        <name>ActivitiesAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowViewKnowledge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApexRestServices</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApiEnabled</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInternalUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningConsoleAllowedForUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningExperienceUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ShowCompanyNameAsUserBadge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewHelpLink</name>
    </userPermissions>
</Profile>
