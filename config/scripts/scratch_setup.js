/**
 * Creates a custom remote site setting
 */
var fs    = require('fs');
var exec  = require('child_process');
//https://stackoverflow.com/questions/8683895/how-do-i-determine-the-current-operating-system-with-node-js
var isWin = process.platform === "win32"; //windows returns 'win32', even on 64 bit operation systems. i.e. process.platform === 'win32' is sufficient 
var SFDX_COMMAND = isWin ? 'sfdx.cmd' : 'sfdx';
var REMOTESITESETTINGS_DIR = 'force-app/main/default/remoteSiteSettings/';
var REMOTESITESETTINGS_NAME = 'ScratchMyDomain';

//remove this when Summer 19 goes live (because of FieldService scratch settings value)
var TEMP_DO_STEP_1 = false;
var TEMP_DO_STEP_2 = false;

var importDataFile = function(filePath){
    console.log(`Importing: ${filePath}`);
    res = exec.spawnSync(SFDX_COMMAND,['force:data:tree:import','-f', filePath]);
    tmpErr = res.stderr.toString();
    tmpRslt = res.stdout.toString();
    if(tmpErr){
        console.error(`Unexpected exception pushing: ${tmpErr}`)
        process.exit(1);
    }
    console.log(`Completed.`);
}

//org username
var username;
//org instance url
var instanceUrl;

//check arguments
if(process.argv.length < 4){
    console.error('\n\nInvalid parameters. Use: node .\\config\\scripts\\remoteSiteSettings.js myBranchName step1|step2');
    process.exit(1);
}

console.log('Running on '+ process.platform+' ...');

const branchName = process.argv[2].trim();

TEMP_DO_STEP_1 = process.argv[3] === 'step1';
TEMP_DO_STEP_2 = process.argv[3] === 'step2';

if(!TEMP_DO_STEP_1 && !TEMP_DO_STEP_2){
    console.error('\n\nInvalid parameters. Use: node .\\config\\scripts\\remoteSiteSettings.js myBranchName step1|step2');
    process.exit(1);
}

console.log('Branch name: '+branchName);


if(TEMP_DO_STEP_1){
    /*
    * checkout local branch
    */
    console.log('Check out local branch...');
    let res = exec.spawnSync('git',['checkout','-b', branchName]);
    let tmpErr = res.stderr.toString();
    let tmpRslt = res.stdout.toString();
    if(tmpErr && tmpErr.indexOf('Switched to a new branch') < 0){
        console.error(`Unexpected exception checking out local branch: ${tmpErr}`)
        process.exit(1);
    }
    console.log(`Branch ${branchName} created.`);

    /*
    * DevHub authorization
    */
    console.log('Authorizing DevHub...');
    res = exec.spawnSync(SFDX_COMMAND,['force:auth:web:login','--setdefaultdevhubusername']);
    tmpErr = res.stderr.toString();
    tmpRslt = res.stdout.toString();
    if(tmpErr){
        console.error(`Unexpected exception authorizing DevHub: ${tmpErr}`)
        process.exit(1);
    }
    console.log(`Branch ${branchName} created.`);

    /*
    * scratch creation
    */
    console.log('Creating new scratch org w/ file "project-scratch-def-45.0.json"...');
    res = exec.spawnSync(SFDX_COMMAND,['force:org:create','-s', '-f', 'config/project-scratch-def-45.0.json', '-a', branchName]);
    tmpErr = res.stderr.toString();
    tmpRslt = res.stdout.toString();
    if(tmpErr){
        console.error(`Unexpected exception creating scratch: ${tmpErr}`)
        process.exit(1);
    }
    console.log(`Scratch ${branchName} created.`);

    /**
     * THIS IS NEEDED ONLY BEFORE VERSION 45.0
     */
    exec.spawnSync(SFDX_COMMAND,['force:org:open','-u', branchName]);
    console.log('Now jump to "Setup | Field Service Settings" and flag the Work Orders radio button.');
}

if(TEMP_DO_STEP_2){
    /*
    * force:alias:list
    */
    console.log('Getting aliases...');
    res = exec.spawnSync(SFDX_COMMAND,['force:alias:list','--json']);
    tmpErr = res.stderr.toString();
    tmpRslt = res.stdout.toString();
    if(tmpErr){
        console.error(`Unexpected exception getting aliases: ${tmpErr}`)
        process.exit(1);
    }
    try{
        var aliases = JSON.parse(tmpRslt).result;
        for(let i = 0; i < aliases.length; i++){
            if(aliases[i].alias === branchName){
                username = aliases[i].value;
                break;
            }
        }
        if(!username){
            console.error(`\n\nAlias ${branchName} not found ;(`);
            process.exit(1);
        }
    }catch(ex){
        console.error(`Unexpected exception parsing aliases: ${ex}`)
        process.exit(1);
    }
    console.log(`Username found: ${username} for alias ${branchName}`);

    /*
    * force:auth:list
    */
    console.log('Getting authorized orgs...');
    res = exec.spawnSync(SFDX_COMMAND,['force:auth:list','--json']);
    tmpErr = res.stderr.toString();
    tmpRslt = res.stdout.toString();
    if(tmpErr){
        console.error(`Unexpected exception getting autheticated orgs: ${tmpErr}`)
        process.exit(1);
    }
    try{
        var orgs = JSON.parse(tmpRslt).result;
        for(let i = 0; i < orgs.length; i++){
            if(orgs[i].username === username){
                instanceUrl = orgs[i].instanceUrl;
                break;
            }
        }
        if(!instanceUrl){
            console.error(`\n\nOrg with username ${username} not found ;(`);
            process.exit(1);
        }
    }catch(ex){
        console.error(`Unexpected exception parsing aliases: ${ex}`)
        process.exit(1);
    }
    console.log(`Instance URL found: ${instanceUrl} for username ${username}`);

    /*
    * create remote site setting file and folder
    */
    console.log('Creaing remote site setting xml...');
    let xmlContent = `<?xml version="1.0" encoding="UTF-8"?>
    <RemoteSiteSetting xmlns="http://soap.sforce.com/2006/04/metadata">
        <disableProtocolSecurity>false</disableProtocolSecurity>
        <isActive>true</isActive>
        <url>${instanceUrl}</url>
    </RemoteSiteSetting>`;
    try{
        if(!fs.existsSync(REMOTESITESETTINGS_DIR)){
            fs.mkdirSync(REMOTESITESETTINGS_DIR);
        }
        fs.writeFileSync(`${REMOTESITESETTINGS_DIR}${REMOTESITESETTINGS_NAME}.remoteSite-meta.xml`, xmlContent);
    }catch (ex){
        console.error(`Unexpected exception writing xml: ${ex}`)
        process.exit(1);
    }

    /**
     * Push del codice
     */
    console.log('Pushing to org...');
    res = exec.spawnSync(SFDX_COMMAND,['force:source:push','--forceoverwrite']);
    tmpErr = res.stderr.toString();
    tmpRslt = res.stdout.toString();
    if(tmpErr){
        console.error(`Unexpected exception pushing: ${tmpErr}`)
        process.exit(1);
    }
    console.log(`Push completed.`);

    /**
     * Import dati
     */
    importDataFile('.\\sfdx-out\\wrts_prcgvr__PhaseManagerSObjectSetting__c.json');
    importDataFile('.\\sfdx-out\\wrts_prcgvr__ServiceCatalogReportSetting__c.json');

    console.log('🎉🎉Scratch setup completed 🎉🎉');
}
process.exit(0);