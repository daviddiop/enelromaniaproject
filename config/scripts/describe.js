/**
 * Creates a custom remote site setting
 */
var fs     = require('fs');
var exec   = require('child_process');
var config = require('../describe-setup.json');
//https://stackoverflow.com/questions/8683895/how-do-i-determine-the-current-operating-system-with-node-js
const isWin = process.platform === "win32"; //windows returns 'win32', even on 64 bit operation systems. i.e. process.platform === 'win32' is sufficient 
const SFDX_COMMAND = isWin ? 'sfdx.cmd' : 'sfdx';
const CONFIG_FOLDER = 'config/';
const OUTPUT_FOLDER = `${CONFIG_FOLDER}scripts/output/`;

const API_LEVEL = '46.0';

const startTime = (new Date()).getTime();

//check arguments
if(process.argv.length < 3){
    console.error('\n\nInvalid parameters. Use: node .\\config\\scripts\\describe.js username|alias');
    process.exit(1);
}

console.log('Running on '+ process.platform+' ...');

const username = process.argv[2].trim();

if(!username){
    console.error('\n\nInvalid parameters. Use: node .\\config\\scripts\\describe.js username|alias');
    process.exit(1);
}

console.log('## USING API LEVEL '+API_LEVEL+' ##');

//console.log('CONFIG', config);

/*
* describe metadata
*
console.log('Describing org...');
let res = exec.spawnSync(SFDX_COMMAND,['force:mdapi:describemetadata','-u', username, '-a', API_LEVEL, '--json']);
let tmpErr = res.stderr.toString();
let tmpRslt = res.stdout.toString();
if(tmpErr){
    console.error(`Unexpected exception describing org: ${tmpErr}`)
    process.exit(1);
}
console.log(`Describe completed.`);
*/

/**
 * Metadata listing
 */
console.log('Starting metadata listing...');
//const describeObj = JSON.parse(tmpRslt);
const types = {};
let typesBody = '';

for (let i = 0; i < config.includedMetadataTypes.length; i++) {

    const obj = config.includedMetadataTypes[i];

    typesBody += `\n    <types>\n        <name>${obj.name}</name>`;
    if (obj.supportsWildcard) {
        typesBody += '\n        <members>*</members>';
    }

    types[obj.name] = [];

    console.log(`\tListing type ${obj.name}...`);
    
    let res = exec.spawnSync(SFDX_COMMAND,['force:mdapi:listmetadata','-u', username, '-a', API_LEVEL, '-m', obj.name, '--json']);
    let tmpErr = res.stderr.toString();
    let tmpRslt = res.stdout.toString();
    if(tmpErr){
        console.error(`Unexpected exception listing metadata: ${tmpErr}`);
        process.exit(1);
    }

    const result = JSON.parse(tmpRslt).result;
    if (result) {
        const filterFn = (item, index) => (
            (
                !item.hasOwnProperty("namespacePrefix") 
                || item.namespacePrefix === "" 
                || (
                    obj.hasOwnProperty("includedNamespaces") 
                    && obj.includedNamespaces.includes(item.namespacePrefix)
                )
            ) 
            && (
                !obj.supportsWildcard
                || (
                    !item.hasOwnProperty("manageableState") 
                    && obj.hasOwnProperty("includedStandardMembers") 
                    && obj.includedStandardMembers.includes(item.fullName)
                ) 
                || item.manageableState === 'installed'
            )
        );

        if (Array.isArray(result)) {            
            let filteredListType = result.filter(filterFn);
            filteredListType.sort((a, b) => (a.fullName.localeCompare(b.fullName)));
            //types[obj.xmlName] = listType;
            types[obj.name] = filteredListType;            
        }
        else if (result.hasOwnProperty("fullName")) {
            types[obj.name] = [result].filter(filterFn);
        }
        else {
            console.log(`Listing ${obj.name} didn't provide a valid result:`, result);
        }
    }
    else if (obj.name === "StandardValueSet") {
        console.log('Adding standard members: ', obj.includedStandardMembers);
        types[obj.name] = obj.includedStandardMembers.map(item => { return {fullName: item, type: obj.name}; });
    }

    if (types[obj.name] && Array.isArray(types[obj.name]) && types[obj.name].length > 0) {
        let filteredListType = types[obj.name];
        //console.log(filteredListType);
        for (let j = 0; j < filteredListType.length; j++) {
            let member = filteredListType[j];
            if (member) {
                if (!member.hasOwnProperty("fullName")) {
                    console.error("fullName not found", member);
                    process.exit(1);
                }
                typesBody += `\n        <members>${member.fullName}</members>`;
            }
        }
    }

    typesBody += '\n    </types>';
}
console.log('Metadata listing completed.');

/**
 * creates the OUTPUT folder
 */
const ts = (new Date()).getTime();

try{
    if(!fs.existsSync(OUTPUT_FOLDER)){
        fs.mkdirSync(OUTPUT_FOLDER);
    }
    
    fs.writeFileSync(`${OUTPUT_FOLDER}package-${ts}.json`, JSON.stringify(types, null, 2));
}catch (ex){
    console.error(`Unexpected exception creating folder: ${ex}`);
    process.exit(1);
}

const endTime = (new Date()).getTime();
const totalTime = (endTime - startTime) / 60000;
console.log(`Org describe completed in ${totalTime} minutes.`);


const packageXml = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n<Package xmlns="http://soap.sforce.com/2006/04/metadata">${typesBody}\n    <version>${API_LEVEL}</version>\n</Package>`;
try {
    fs.writeFileSync(`${OUTPUT_FOLDER}package-${ts}.xml`, packageXml);
} catch (ex) {
    console.error(`Unexpected exception creating package.xml: ${ex}`);
    process.exit(1);
}

process.exit(0);